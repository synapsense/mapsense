% !TEX TS-program = xelatex
% !TEX encoding = UTF-8

% This is a simple template for a XeLaTeX document using the "article" class,
% with the fontspec package to easily select fonts.

\documentclass[11pt]{synap-article} % use larger type; default would be 10pt



\usepackage{amsfonts}

\title{Multy-DC \\ Putting more than one floor in a MapSense File \\ Jupiter II}
\author{Gabriel L. Helman, Ken Scoggins}
%\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 


\begin{document}
\maketitle
\tableofcontents

\section{Document History}
\begin{tabular}{ | l | c |  p{9cm}  | } \hline
	\textbf{Name} &\textbf{Date} & \textbf{Summary / Reason}  \\ \hline
	Gabriel Helman & 03-19-2012 & Rough Draft \\ \hline
	Gabriel Helman & 03-22-2012 & Hilariously comprehensive rewrite \\ \hline
	Gabriel Helman & 03-23-2012 & More revisions after discussion \\ \hline
	Gabriel Helman & 04-02-2012 & HLD Review Feedback Incorporated \\ \hline
	Gabriel Helman & 04-05-2012 & Further Review Feedback \\ \hline
	Ken Scoggins   & 07-13-2012 & DLImage, etc \\ \hline
	Gabriel Helman & 07-13-2012 & Choose Floorplan, M1 notes \\ \hline
	Gabriel Helman & 09-26-2012 & M5 notes \& eratta \\ \hline
	Gabriel Helman & 10-09-2012 & Component DLIDs, Moving between floorplans \\ \hline
	Gabriel Helman & 01-16-2013 & Jupiter II GA updates \\ \hline

\end{tabular}

\section{Overview}

MapSense needs the ability to work with more than one DataCenter at a time in a single file.  Among other reasons, this is needed for both RTU/Damper Control and Multi-Floor PUE.

\subsection{Disclaimer}

%Pending finalization of the RTU/Damper Control HLD, this design has been provisionally accepted by development.  There remain several details regarding UI and Proxy implementation yet to be determined; it is assumed those will be flushed out either via the RTU/Damper Control spec, or by iterative UI prototypes.

This spec reflects the state of the feature as shipped in the GA release of Jupiter II which did not include Damper Control.


\subsection{Non-Goals}

There are two similar features that have been discussed for DeploymentLab recently; Multiple DCs in the same DLZ file and Multiple Layers in the same DataCenter.  This spec is concerned only with having more than one DC in the same project file; there will be no discussion of layers in this document.\footnote{This is, of course, directly contradictory to this author's original plan of ``Let's solve everything with Layers!"}

Environment Server will still be able to host more than one DLZ file.

We will not be adding the ability to model things about a facility other than as a loose collection of floors.

\subsection{Terminology}


In previous versions, `floorplan', `datacenter', and `floor', `DL file', and 'project' were all roughly synonyms if you squinted at them right. ' Going forward, these terms will need to be defined more exactly.

For the purposes of this feature, `project', `DL file', and `DLZ file' are synonyms.

A `project' is now synonymous with a 'facility.`

Each `project' or `facility', can contain one or more `floorplans'.  Each `floorplan' is synonymous with a single \ob{DC} object and \term{datacenter} component pair.  Previously, each project could contain only a single floorplan by definition.  The Multy-DC feature is adding the ability to have more than one `floorplan' in a `project`.

Each floorplan can have its own background image (see \ref{BGI}) and set of zones, networks, and instrumentation.

If a multiple floor building is to be instrumented in the SynapSense system, each floor of the building will get its own floorplan in the same project file.

Previously, then, a project file roughly mapped to a floor of a building; now each project file represents the building as a whole and contains a floorplan for each physical floor of the building.

Floorplans do not have to be a literal Datacenter.  Utility spaces, basements, roofs, and virtual layouts like PUE graphs will all be modeled as floorplans in a project.  DeploymentLab will not make a strong distinction between floorplans that do not contain physical equipment and those that do.


\sidenote{Floorplan}{What about a floor with two totally separate rooms?  Under the new model, those should both be separate floorplans in the same DLZ file---unless they are going to share a network.  Since networks can't be shared across floorplans, two physically separate rooms that share the same WSN will need to be configured as a single floorplan.}


\subsection{Use Cases \& Functional Requirements}

\begin{itemize}
	\item Associate Components between Floorplans
	\item Allow Control Strategies to use COINs on more than one Floorplan
	\item Have `virtual' floorplans for calculations
	\item Add additional floorplans to a system with less overhead
	\item Reduce DL file `clutter' for big systems
\end{itemize}


\subsection{Further Reading}

Two related features, `Import Floorplan' \& `Optional Background Images / Virtual Layout' got their own documents.  You should go read them.

Also, the RTU-Style control document.


\subsection{Jupiter II Delivery Schedule}

As of this writing, the majority of the multiple-floorplan functionality will be delivered in Milestone 1.  However, Proxies (\ref{proxies}) will not be delivered until Milestone 5, and the full implementation of the Choose Floorplan Dialog (\ref{choosefloorplan}) will not be delivered until Milestone 2.  Importing floorplans and optional background images will also be delivered in Milestone 5.


\section{Functional Design \& Workflow}

Project files will be able to contain more than one datacenter/floorplan. The floorplan being currently worked on is selected via a new UI widget (See \ref{AI}.)  In general, DeploymentLab will work much the same when working on a single floorplan of a multi-DC project as it does when working on a single-datacenter project.  Datasources and groupings will not be allowed to span datacenters.

The mechanism for moving data between floorplans will be Proxy Components. A Proxy is a Component that exists in one floorplan but references an Object that is the child of a different \ob{dc}.  This allows us to have the same `thing' present in multiple floorplans but with different coordinates.  (See section \ref{proxies} for details.)


\subsection{Scenarios}

\subsubsection{RTU Control}

Perry, the SynapSense\textregistered{} Field Services Expert, is instrumenting the Stark Industries Engineering Lab for RTU Control.  The Lab is six stories tall, but has only a pair of RTUs\footnote{These are probably RTU Managers, each of which controls more than one `real' RTU, but we're going to hand-wave away that difference for the moment.} on the roof with a series of ducts leading to the rest of the building.  The first RTU, $RTU\Theta$, cools floors one and two, whereas the second, $RTU\Lambda$, cools floors three, four, and five.  The sixth floor, holding the building's ARC reactor, has its own CRAH.

Perry creates a project with seven datacenters; one for each floor, and a seventh representing the roof.  The roof floorplan has as its background image a blueprint of the roof with the two RTU components placed where the RTUs physically exist.

Environmental monitoring is configured normally on each floor.  The CRAH control on the ARC Reactor's floor is configured normally as well; A REIN is created for the CRAH, Controlled Device components are added to the CRAH component and the AoEs include temperature and pressure sensors on that floor.

To control the RTUs, Ackbar Register Damper Nodes are installed at all the registers in the building.

In MapSense, Perry places an RTU Manager component in the roof floorplan where the two managers physically exist.  These are named, of course, RTUM$\Theta$ and RTUM$\Lambda$.

On each of the first two floors, Perry creates a Proxy Component for RTUM$\Theta$ and places them on those floorplans roughly where the plenum enters the floor.  For floors three though five, Perry does the same with RTUM$\Lambda$.  On each of those floorplans, an Ackbar Component is placed at the physical location of the Ackbars in that room, and the Ackbar is associated with the RTU Manager proxy on that floorplan.  Each Ackbar has its own Area of Effect that includes all the Stratification and Temperature COINs near that register.

\sidenote{Implementation}{See the Damper/RTU Control HLD for more details}


\subsubsection{Three Floors of PUE}

Darth Vader is configuring a PUE graph for the Death Star's Main Computer Center.  The Geonosian Public Utilities Conglomerate is charging the Empire far too much for electricity, and as the main Hypermatter reactor is not yet online, Governor Tarkin has ordered the Death Star to use less power.\footnote{Presumably, Geonosis will serve as the initial test target for the Superlaser.}

The Computer Center has three floors of Quantum Processing Racks, and two main cooling systems: A CRAH system using a Di-Hydrogen Monoxide loop cooled by radiator fins extending into space, and a series of refrigerators with the doors left open.

Lord Vader creates a project in DeploymentLab with three datacenters; one for each floor.  In addition, he adds a fourth virtual datacenter to hold all PUE calculations.

The three floors are configured as follows:
\begin{enumerate}
\item The first floor has Thermanode-based instrumentation added to each Rack and an ION meter attached to the power feeds of the two Refrigerators, as well as the main input line for the floor.
\item The second floor is instrumented with a mix of Sandcrawlers and Duos on each Rack.  An Enthalpy Meter is attached to the Di-Hydrogen Monoxide pipe into the CRAH.
\item The third floor has more Thermanodes, as well as a BCMS attached to the main distribution panel.
\end{enumerate}

The virtual floor contains the PUE Reporter.  For each Ion Meter, BCMS, Enthalpy Meter, Sandcrawler, and Duo, Vader creates a Proxy component in virtual floor. From these, Vader creates a PUE calculation graph.  Vader is able to move all the calculation operations and Proxy components to make a visually appealing flowchart on the virtual floor while on the first three floors the meters and racks stay at their physical locations.

In the Console, the virtual floor shows up as a Datacenter in the navigation tree, but one which only has PUE and Custom Metrics tabs.  The three physical floors have only environmental-based tabs.


\section{Data Model Changes}

\subsection{Object Model}

There will continue to be a single \term{objectModel} instance.  It will, however, be able to support more than one \ob{DC} object as a root.  Only \ob{DC} objects will be used as the root for floorplans.


%The system may need the ability to explicitly model a Facility above the \ob{dc}.  The details of this are, as yet, undefined.

\subsection{Component Model}

There will continue to be a single \term{componentModel} instance.  It will, however, be able to support more than one \term{datacenter} component as a root.

In general, the application will refer to \term{datacenter} components as `floorplans', rather than 'datacenters'.

The background image type in the model will change from a standard Java AWT \term{Image} to a new custom \term{DLImage} to allow for custom memory management of the background images. Details for \term{DLImage} are described in Section \ref{BGI}.

The user will be able to add and delete new \term{datacenter} components (see \ref{UI}.) When deleting a \term{datacenter}, all child components of that \term{datacenter} will also be deleted. This action will be undoable.

\subsubsection{Component DLIDs}

As a sideshow to all of this, components need a unique identifier.  Previously, they had no such thing.  Fortunatly, DeploymentLab already had a mechanism for this, the \term{DLID}.

Going forward, Components will get a \term{DLID} as well.  They will be drawn from the same pool as objects, so every \term{DLID} value will continue to be unique across a DLZ file.



\subsection{Proxy Components}\label{proxies}

The primary use case of a Multy-DC project is to relate components on different floorplans.  The mechanism for this will be Proxy Components.  A Proxy will be a Component that exists as a child of one \term{datacenter} Component, but has no managed object(s) of its own; instead it acts as the Proxy for a Component in another Datacenter, called the Anchor\footnote{Did you know there isn't an English word that means ``The thing that a proxy is the proxy for?"  Neither did I!}.  There is no upper limit on the number of proxies a given anchor can have.  Proxies can be placed on the same floorplan as the anchor, and a single floorplan can have more than one proxy for the same anchor.

%A Component can have as many Proxies as there are other datacenters in the project. %A single floorplan cannot have more than one Proxy for the same Anchor.  %A proxy cannot exist on the same floorplan as its anchor.

\sidenote{Terminology}{Proxy was chosen over other terms like `shortcut' or `symlink' as the implications were closer to the intended design---they're very similar to voting proxies, for example.  And, as 100\% of the intended audience of both this document and feature own Stock Options, the assumption is that, at minimum, they all know what a Shareholder Voting Proxy is.}

Proxies will not have the same properties as their Anchor.  They will only have the properties \term{name}, \term{x}, \term{y}, \term{configuration}, and \term{description}.  The first three can and will have different values than the same values on the Anchor.

%Proxies will be created via a UI widget that will allow the user to specify which floorplan the proxy will be created in.  (Details TBD.)

%All Components will be able to be proxied.  There will be no \term{placeable} components which cannot be proxied.

%Proxies will use the same icon as their anchor, with an extra badge indicating that it is a Proxy.  (Badge design TBD.)

%Most components will be able to be proxied.  A few components will be blacklisted and will not be allowed to have proxies.

On the floorplan itself, Proxies will use the same icon as the Anchor.  If the Anchor is rotatable, the proxy will not be and the rotation arrow will not be displayed on the proxy.  The name of the proxy will be displayed in italics on the floorplan.  If the user does not change the name of the proxy itself, the name will remain synchronized with the name of the anchor.

It's worth making this very clear: Proxies are not a copy.

Proxies have two primary use-cases: Associations and Areas of Effect.

\subsubsection{Proxy Associations}

A Proxy will be able to participate in the same associations as the anchor.  The Associate tool and Association Lines will work the same way between a Component and a Proxy as they do between two Components.  At the object level, the associations will be the same as a standard association, as the Proxy and the anchor share the same objects.  However, each Proxy will manage its own associations, so if, for example, a Proxy is deleted, the associations between that Proxy and other Components will be removed but any associations between the anchor and other components will remain.

\subsubsection{Proxy Areas of Effect}

If a anchor has an AoE, all of its Proxies will also have the same type of Area of Effect.  This allows the same conceptual Area of Effect to spread across multiple floors.\footnote{What about Regions of Influence?  Well, I don't want to spoil the surprise, but you should read the Damper/RTU Control HLD.}

The \term{AOEController} will be responsible for redirecting AoE memberships of COINs to the anchor, not the Proxy.


\subsubsection{Proxy Creation}

There will be two ways to create proxies.  First, a new entry in the Edit menu, Create Proxy (CTRL+K).  The second will be a context-menu item, also Create Proxy.  Both will have the same behavior.

The Create Proxy action works on the current selection.  On invloking the action, DeploymentLab displays the Choose Floorplan Dialog (see \ref{choosefloorplan}).  Proxies for the selected components will be created in the selected floorplans.


\sidenote{UI}{Having both a proxy creation tool and a Paste As Proxy action were also discussed and prototyped, but discarded as unneeded.}


\subsubsection{What can anchor a Proxy?}

Most components will be able to be proxied.  A few components will be blacklisted and will not be allowed to have proxies.  As a general rule, things will only be blacklisted if there is a good reason not to proxy it (rather than the other way around; assuming nothing will be proxied and explicitly choosing to allow it.)

The rules for allowing proxies will be as follows:

\begin{enumerate}
	\item The component MUST have the roll \term{placeable}
	\item The component MUST NOT have any of the following roles:
	\begin{itemize}
		\item \term{gateway}
		\item \term{contained}
		\item \term{controlinput}
		\item \term{staticchildplaceable}
		\item \term{staticchild}
		\item \term{dynamicchildplaceable}
		\item \term{dynamicchild}
	\end{itemize}
\end{enumerate}


\subsection{Select Model} \label{selectModel}
There will continue be one select model.  The current selection will not be allowed to contain items from multiple floorplans.  Changing the active datacenter will clear the current selection.

The \term{selectModel} and \term{SelectionChangeListener} be extended to add a new event, \term{ActiveDatacenterChanged}, that will fire whenever the active datacenter changes.

The \term{selectModel} tracks three Active components: the \term{activeZone}\footnote{Despite the name, this tracks all grouping types.}, the \term{activeNetwork}\footnote{Despite the name, this tracks all data source types.}, and the \term{activeDatacenter}.  While the \term{activeDatacenter} has always been present, it has never been significantly used as there was only ever a single datacenter in a project.

The \term{activeDatacenter} will now be used to select the datacenter being worked on currently.  The \term{activeZone} and \term{activeNetwork} will always be a member of the \term{activeDatacenter}.  See \ref{AI} for the UI implications.


\section{DLZ File Changes}

The DLZ file will contain a single XML document containing the entire object and component model.  Each background image will be stored as a separate PNG image inside the DLZ file.  The background images will be named after the DLID of the \ob{DC} object they belong to, unlike now where they are named after the DLZ file itself.

As the Update Configuration-based upgrade process changes the DLID of all objects, the upgrade process will result in these PNG files being renamed inside the DLZ.

\subsection{DLZ Opening \& Saving} \label{DLZLoad}

Opening and saving the DLZ file will be changed from the existing implementation for handling Zip files to the new Java 7 nio.2 File System implementation. This will greatly simplify the code and should also improve performance. This change will affect the following items that will need to be regression tested:

\begin{itemize}
\item Opening older project file versions.
\item Component upgrades from older version.
\item Proper handling of UTF-8 encoding.
\end{itemize}


\section{Export Process}

The Export process will continue to send a single XML document containing all object information to the DLIS on the server.

The DeploymentLab side of the export process will need to load all the images from the DLZ file to slice the zone images, as only the currently active DC images is guaranteed to reside in RAM.  (See \ref{BGI}.)

The sync check will use all \ob{dc} objects in a project; every \ob{dc}'s \term{lastExportTs} will stay updated on each export.

In Jupiter and earlier, the export process wraps up a zip file containing the object model as an XML document and each datacenter and zone background image (scaled as needed.)  As there was only one datacenter, that image got the same name as the wrapping zip file itself.  Each zone image was named based on the DLID of the zone in the form \term{DLID\_<number>.png}.  The datacenter images will need to be renamed to match the zone style of DLID-based names.\footnote{These changes were all made in Jupiter.}



\section{UI/UX Changes}\label{UI}

\subsection{Menus}
\subsubsection{Project Menu}

Currently, there is no place to put project-wide items in the menu structure. 
Project-wide stuff tends to be scattered across the File and Setup menus.

On a suspiciously-related note, the Setup menu doesn't have a whole lot of setup-related
items in it.  The setup related stuff it does have are things that only effect
the current project - changing the ES URL - or the current floorplan as a whole
- changing the background image.

Therefore, the Setup menu will be renamed to he Project menu, and all project-related items will be put there.\footnote{Going forward, this menu will be the home for these kinds of items, and the Edit menu will continue to handle things that apply to the Current Selection.}

At the same time, Options... will be renamed Project Options..., and Reload Background Image will be renamed Change Background Image.


Five new menu options will be added to the Project menu: Add Floorplan, Delete Floorplan, Import Floorplan, Make New Floorplan From Selection, and Remove Background Image.  Add Floorplan will also be associated with the shortcut \term{CTRL-ALT-F}.  Make New Floorplan From Selection will be associated with the shortcut \term{CTRL-ALT-SHIFT-F}.

Add Floorplan will prompt the user to give the new Datacenter a name and to load a background image.  An empty Datacenter will then be added to the project.  This will be the only way to add a new floorplan.

Delete Floorplan will prompt the user for confirmation before proceeding. If the delete is confirmed, all child components of the currently active Datacenter will be deleted along with the Datacenter itself.


Import Floorplan will import another DL(z) file into the current project file as a new floorplan.  (See the Import Floorplan HLD for details.)

Move to Floorplan will create a new floorplan and move the current selection to it.  See Section \ref{movetofloorplan}.

Remove Background Image will replace the background image with the default empty workspace (see the Virtual Layout HLD for details.)

\subsubsection{Edit Menu}
The Edit menu gain two new options: Create Proxy and Move to Floorplan.  See Sections \ref{proxies} and \ref{movetofloorplan}.

\subsection{Active Items} \label{AI}
A new Active Item Combobox will be added: Active Datacenter.  This will allow the user to select which \term{datacenter} they are currently working on.  The Active Grouping and Active Datasource comboboxes will remain; however, the choices in these lists will be limited to the choices available in the currently active datacenter.  Should the datacenter change, these lists will re-populate and the currently active Grouping and Datasource will revert to the last selected for that Datacenter.

To best optimize screen real-estate, the Active Grouping and Active Datasource comoboboxes will be moved to the New Palette on the left of the Scene Graph.  The new Active Datacenter combobox will be moved to the upper right, along with a new toolbar button to add new datacenters.


\subsection{Scene Graph}\label{SG}

Despite having more than one floorplan in the project, DeploymentLab will still have only a single Scene Graph and will display only a single floorplan at a time.

There will be an Active Datacenter combobox to choose which floorplan the scenegraph will display.  (See \ref{AI}.)

The \term{DeploymentPanel} will need to be adjusted in the following ways:
\begin{itemize}
	\item Accept a \term{datacenter} component as the `root' of the scene graph, rather than assuming it will draw everything in the model
	\item Populate a graph based on the current object graph, rather than populating based on model change events
	\item Respond to the \term{ActiveDatacenterChanged} event to redraw the graph (See \ref{selectModel}).
	\item Support floorplans without a background image
\end{itemize}

\sidenote{Implementation}{The redraw will also need to be fast.  REALLY fast.  See \ref{speed}.}

\subsubsection{Association Lines}
Association Lines between Proxy Components and regular Components will look and behave the same as normal Associations.

\subsubsection{Background Images} \label{BGI}

Background images will become optional for all floorplans.  Any floorplan can be created without a background image to start, just an empty workspace of some default size.  A background image can always be added to a floorplan without one, or removed from a floorplan with one.  See the Virtual Layer HLD for details.

The Reload Background Image menu option will only change the image for the currently active datacenter.

The additional RAM requirements to store more than one background image could be considerable.  To mitigate this as much as possible, a custom \term{DLImage} class will be created to replace the usage of the standard Java AWT \term{BufferedImage} as the background image concrete class.  \term{DLImage} will be a wrapper around a \term{BufferedImage} and implement the same interfaces to minimize the changes to existing code.  Wrapping the actual image allows for custom management of how images are stored in memory, but hides this implementation detail from the rest of the system.

\term{DLImage} will save a copy of the image to a temporary file on the local file system and only maintain a Soft Reference to the image stored in memory.  A Soft Reference means that the JVM is alllowed to remove the image from memory if it is running out of overall memory as long as no Hard References exist.  Only the active Datacenter's background image will have a Hard Reference to it since it will be in use by the Scene Graph and displayed in the UI. When the active Datacenter is changed, its associated \term{DLImage} will check to see if the image is still in memory. If it is not, it will reload it from the saved file.

This method has one significant advantage over enforcing that only one image is in memory at a time: Performance. When memory is plentiful, more images will remain in memory, eliminating the need to reload the image every time the active Datacenter is changed. 

\sidenote{Technical}{We may also need to explore displaying lower-resolution images to save space. Lower-resolution images may also be useful for increasing the display time when changing active Datacenters.}





\subsection{Trees}

Both Trees (Structure and Datasources) will remain.  However, the pair will only apply to a single \term{datacenter} at a time.  This will be the DC selected as the Active Datacenter.  Whenever the active DC changes, both trees will repopulate.

It will never be possible to see items from two or more datacenters in the same tree.

Both Trees will need to be adjusted in the following ways:
\begin{itemize}
	\item Accept a DataCenter component as the `root' of the tree, rather than assuming it will draw everything in the model
	\item Populate the tree based on the current object graph, rather than populating based on model change events
	\item Respond to the \term{ActiveDatacenterChanged} event to redraw the graph (See \ref{selectModel}).
\end{itemize}

Both trees will be changed so that the Datacenter is the displayed root object so that it can be selected and the properties modified. For the Data Sources tree, this means replacing the generic "Data Sources" root with the Datacenter. For the Structure tree, this means removing the generic "Groupings" root and making the Datacenter node the root.

\sidenote{Implementation}{The redraw will also need to be fast.  REALLY fast.  See \ref{speed}.}

\subsection{Properties Editor}

No changes to the properties editor; it will continue to be based on the current selection.

\subsection{Filters}
Filters and view options will continue to work as before.  The filters will remain at their user-set values when changing datacenters rather than returing to a default.

\subsection{Disclosure Buttons}
To improve screen real estate efficiency, add buttons to the tool bar to open and close the two side panes automatically (without having to click those teensy triangles in the divider.  Also, remove those triangles).

\subsection{Choose Floorplan Dialog} \label{choosefloorplan}

Prior to this feature, there was a long list of functions in DeploymentLab that worked on "the whole project" without requiring a selection of some kind first.  Going forward, these can either continue to work on everything, just do the active floorplan, or ask the user to choose which flooplan(s) the action should apply to.  (Also an option is "visible active", which works on the active floorplan but takes the view filters into account.)

Actions that should only work on a single floorplan at a time use the active floorplan, whereas things that can work on more than one floorplan get the dialog.

A new dialog will be added, Choose Floorplan.  When a user action requires them to choose one or more floorplans, the dialog will appear with a list of all floorplans in the project, each with a checkbox.  The currently active floorplan will be checked be default, the others will not.  The user can select any of the floorplans and choose OK.  Choosing Cancel, or choosing OK with no selected floorplans will cancel the action.  The dialog will have two modes: multi-selection and single-selection, which will allow the user to choose one or more than one floorplan.  Unless otherwise specified, the dialog is assumed to always operate in multi-selection mode.


The features that used to work on ``everything" are listed below with how they will behave under the new design:

\begin{description}

\item[Select All] - visible active 
\item[Move to Floorplan] - choose (see \ref{movetofloorplan})
\item[Find] - everything 
\item[Replace] - new scope: choose all,active,selection 
\item[Rotate 90\textdegree{} Right \& Left] - active only 
\item[Reposition secondary nodes] - active only 

\item[New Floorplan From Selection] - choose (see \ref{movetonewfloorplan})
\item[Reload Background Image] - active only 
\item[Set Background Scale] - active only 

\item[Configure Defaults] - everything 
\item[Configure Simulator] - everything 


\item[Pre-Export Validation] - everything 
\item[Export Schema] - everything
\item[Check File Sync] - everything

\item[Config File $\to$ Simulator] - everything
\item[Config File $\to$ Simulator with power] - everything
\item[Config File $\to$ DeviceManager] - everything
\item[Config File $\to$ Node Names] - choose

\item[Clear Object Ids] - choose
\item[Remove Schema From Server] - everything
\item[Reinitialize Schema] - everything

\item[Export Image $\to$ Deployment Plan] - visible active
\item[Export Image $\to$ Background Image] - active
\item[Export Image $\to$ All Images] - everything

\item[debug export options \& lightningbolts] - everything as appropriate

\item[View Options] - everything
\item[View Stats] - list everything %, but broken down by floorplan \textit{(floorplan breakdown not implemented for M1)}

\end{description}


\section{Tools}
Most tools will remain unchanged unless otherwise specified below.

\subsection{Associate Tool}
One of the primary use cases of Multy-DC projects is to associate components in different floorplans.  The Associate Tool will work between Regular Components and Proxy Components on the same floorplan.  

\sidenote{Technical}{This should not be a huge change, as the Associate tool mainly works with the \term{componentModel} and \term{selectModel}, both of which will remain solo.}

\section{Moving Components Between Floorplans}

Of course, having multiple floorplans is a little silly if the user can't move components between them.

\subsection{Move To Floorplan}\label{movetofloorplan}

A new option will be added to the Edit Menu---Move to Floorplan.  This will work on the current selection, and present the user with the Choose Floorplan dialog (see \ref{choosefloorplan} in single-selection mode.

At that point, DeploymentLab attempts to move the selection to the targeted floorplan.  The logic is as follows:

\begin{enumerate}
	\item Build a list of all data sources and groupings (containers) that contain components to be moved.
	\item For each container, are the entire contents of the container going to be moved?
	\begin{itemize}
		\item If yes, move the container, and therefore all of its children, to the target floorplan
		\item if no, make an exact copy of the container in the target floorplan and move the children of the original container there
	\end{itemize}
	\item Move components that are the direct children of the datacenter to the new floorplan
	\item For Associations between something being moved and something that is not being moved, make a Proxy of the component not being moved on the target datacenter, and reassociate the moved component to the proxy.
\end{enumerate}

For components with more than one parent (such as racks), one parent may be moved and one may be copied.  This is both allowed and expected.


\subsection{Make New Floorplan From Selection} \label{movetonewfloorplan}

For the case where a single floorplan is to be split into more than one\footnote{You all know who I'm talking about.} things get a little simpler.  Make New Floorplan From Selection will be added to the Project menu, and will work much like Move to Floorplan, except DeploymentLab will create the floorplan automatically rather than ask for a target.

The new floorplan will be an exact duplicate of the original---same size and background image---and the components being moved will land in the same location on the new floorplan that they were on the old one.

The container-moving/copying logic will be the same as with Move to Floorplan.



\subsection{Copy \& Paste}

Copy \& Paste will continue to work exactly as before---creating new components in the currently active datacenter, grouping, and datasource.



\section{Export Validation}\label{validation}

% This section probably needs more work. I think some items are missing that I can't remember right now.
Most validations that are relative to a single Datacenter and its components will behave as they did in the past with single Datacenter projects. However, the following validations will span multiple Datacenters:
\begin{itemize}
	\item Unique names.
	\item Unique WSN identifiers, such as IP Address and MAC Address. 
\end{itemize}

\section{Speed \& Efficiency}\label{speed}

Several features listed here will cause the already slow DeploymentLab to move even slower.  Therefore, we're setting several performance targets:

\begin{itemize}
	\item Loading a Multy-DC DLZ file will take the same or less time as a single-floor DLZ file with the same number and type of components
	\item Exporting a Multy-DC DLZ file will take the same or less time as a single-floor DLZ file with the same number and type of components
	\item Switching between Active Datacenters will take no more than half a second before the new datacenter can be manipulated
	\item A Multy-DC DLZ file will use the same or less RAM than a single-floor DLZ file with the same number and type of components
\end{itemize}

\sidenote{Future}{Overcoming this speed issue may finally cause us to move away from XML as a file serialization format.}

\section{Open Items}
\begin{itemize}
	%\item Should background images be optional? YES
	%\item Do we need a second object type to replace \ob{DC} for virtual floorplans? NO
	%%\item The UI for creating Proxies
	%%\item How will Proxy icons look different on the floorplan
	%%\item How will Proxies actually \textbf{work} under the hood?
	%\item How will we solve the RAM problems inherent in having more than one image?
	%\item Should supporting Proxies really be opt-in? NO
	%\item Are there any components we shouldn't allow to be proxied? NO
	%\item Should we allows networks to extend across datacenters? NO
	%\item Should we implement server-side zone image slicing?
	%\item Do we need to model which order the floors in a building are in? YES
	%\item Do we need to model things about a building other than the floors themselves? YES
	\item moving components between floorplans
	%\item the no-proxy blacklist
	\item should proxies look more different?  Badges?
\end{itemize}

%\section{Things to Do}
%\begin{itemize}
%	\item \checkmark Adjust the Component and Object models to allow more than one datacenter root\footnote{As of Mars, this is not prevented, just not handled very well when it does happen; allowing this was necessary to make the Update Config-based upgrade work.}
%
%	\item \checkmark The Basics
%	\begin{itemize}
%		\item \checkmark Add the Add Datacenter menu option
%		\item \checkmark Add a Delete Datacenter menu option
%		\item \checkmark Add the Active Datacenter combobox
%		\item \checkmark Add the \term{ActiveDatacenterChanged} event to the \term{SelectionChangeListener} interface
%	\end{itemize}
%
%	\item \checkmark The Scene Graph
%	\begin{itemize}
%
%		\item \checkmark Adjust the Scene Graph to redraw based on \term{ActiveDatacenterChanged} and then begin tracking model change events in that DC only
%		\item \checkmark Adjust the Scene Graph to ignore model events during a file load, instead refreshing based on the active DC once the \term{modelFullyInitialized} event is fired
%		\item \checkmark Change the scene graph to only load the current background image from the DLZ file on demand 
%	\end{itemize}
%
%	\item \checkmark The Trees
%	\begin{itemize}
%		\item \checkmark Adjust the Network tree to redraw based on \term{ActiveDatacenterChanged} and then begin tracking model change events in that DC only
%		\item \checkmark Adjust the Structure tree to redraw based on \term{ActiveDatacenterChanged} and then begin tracking model change events in that DC only
%		\item \checkmark Adjust the Network tree to ignore model events during a file load, instead refreshing based on the active DC once the \term{modelFullyInitialized} event is fired
%		\item \checkmark Adjust the Structure tree to ignore model events during a file load, instead refreshing based on the active DC once the \term{modelFullyInitialized} event is fired
%	\end{itemize}
%
%	\item \checkmark Other UI Elements
%	\begin{itemize}
%		\item \checkmark Adjust the other Active Item Comboboxes to only show members of the Active Datacenter.
%		\item \checkmark Move the three Active Item Comboboxes between the toolbar and the scene graph
%	\end{itemize}
%
%	\item \checkmark Tools
%	\begin{itemize}
%		\item \checkmark Insure all Tools (node adders, selection, associations) work only with the Active Datacenter.
%		\item \checkmark Insure the Search tool works across all datacenters in the \term{componentModel}.
%
%	\end{itemize}
%
%	\item \checkmark Export Process
%	\begin{itemize}
%		\item \checkmark Change the filename used for background images when serialized to the DLZ file
%		\item \checkmark Change the Server-side DLIS to recognize the new filenames for images
%	\end{itemize}
%
%
%	\item \checkmark Make Proxies work
%
%\end{itemize}

\end{document}
