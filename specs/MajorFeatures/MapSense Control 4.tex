% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode
\documentclass[11pt]{article} % use larger type; default would be 10pt
\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{letterpaper} % or letterpaper (US) or a5paper or....

\usepackage{amsmath}
\usepackage{wrapfig}
\usepackage{graphicx} % support the \includegraphics command and options

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

%%% END Article customizations

%%% The "real" document content comes below...

\usepackage{hyperref}
\usepackage{booktabs}

%%\newcommand{name of new command}[number of arguments]{definition} 
\newcommand{\sidenote}[2]{ \begin{quote} \it {#1} Note:\\{#2} \end{quote} }
\newcommand{\on}{\texttt{true}}
\newcommand{\off}{\texttt{false}}
\newcommand{\submsg}[1]{\begin{itemize} \item{``#1''} \end{itemize} }

\newcommand{\ob}[1]{\textsc{\lowercase{#1}}}
\newcommand{\comp}[1]{\textsc{\lowercase{#1}}}

\newcommand{\cre}[1]{\texttt{#1}}
\newcommand{\prop}[1]{\texttt{#1}}
\newcommand{\classname}[1]{\texttt{#1}}
\newcommand{\term}[1]{\texttt{#1}}


\title{MapSense Control Phase 4 \\
AC 1.2  / MapSense 5.3.2 Hermes \& 6.0 Earth \\
Functional Specification}
\author{Gabriel Helman}

\begin{document}

\maketitle

\tableofcontents

\section{Document History}
\begin{tabular}{ | l | c |  p{9cm}  | } \hline

Name &	Date &	Summary / Reason  \\ \hline
Gabriel Helman & 2-25-2010 & 	Initial distro  \\ \hline
Gabriel Helman & 	3-1-2010 & 	Clarified validations, user levels, component deletion, fixed a few typos \\ \hline
Gabriel Helman & 	3-3-2010 & 	Added MRD Delta appendix \\ \hline
Gabriel Helman & 	3-15-2010 & 	Revised “broker” to “device”, spec location, language cleanup, removed references to 6.x \\ \hline
Gabriel Helman & 	4-1-2010 & 	Additional Validations, controller status \\ \hline
Gabriel Helman & 	6-8-2010 & 	Reshaped into Venus / Phase 2 version \\ \hline
Gabriel Helman & 	6-11-2010 & 	Added horizontal row exception in validations \\ \hline
Gabriel Helman & 	6-17-2010 & 	SAT \& RAT connections new datatype \\ \hline
Gabriel Helman & 	6-23-2010 & 	Validations to prevent unassociated controlled devices \\ \hline
Gabriel Helman &  8-20-2010 & Updates for Apollo 1.1  \\ \hline
Gabriel Helman &  9-16-2010 & Ctrl. CRAHs no longer parents  \\ \hline
Gabriel Helman &  3-31-2011 & Updates for AC1.2  \\ \hline
\end{tabular}

\newpage

\section{Overview}
MapSense Control Phase 2 allows for configuration of the first general release (Venus) of the Adaptive Control System.

Five features have been added to support Control: Regions of Influence (REINs), Control Inputs (COINs), the Attach Control Inputs tool, Controlled Devices, and the Configure Adaptive Control dialog.

This spec describes the complete set of control functionality added to MapSense for the 5.3.0 GA release.  This document includes all material that was in the original phase 1 document.

\subsection{Disclaimer}
This spec has been written in parallel with the development and refinement of a prototype implementation and the MRD for the Adaptive Control System as a whole.  This spec is intended to reflect the design decisions made in part based on feedback from the prototype during its first trial installation.

This spec only applies to MapSense, not the control system as a whole.  Issues that are the purview of the Control System, the Console, or other portion of the system are left as an exercise for the MRD or other specs. 


\subsection{Workflow}
Control is assumed to be configured on top of an existing Environmental configuration.  Once Environmentals are configured, the process is as follows:
\begin{enumerate}
\item	Add Regions of Influence
\item	Add a CRAH Device to each Region of Influence
\item	Add a VFD Device to each Region of Influence
\item	Attach Control Inputs to all Environmental sensors to be used in control
\item	Assign the Control Inputs to the correct Regions of Influence
\item	Add more Regions of Influence and Control Inputs as needed
\end{enumerate}
 



\begin{figure}[h!p]
\begin{center}
\leavevmode
\includegraphics[height=6in]{ApolloWorkflow}
\end{center}
\caption{Control Configuration Workflow}
\end{figure}


\sidenote{Side}{In “real life” this will probably happen in parallel with the Environmental Configuration, so this process will be followed in miniature several times, most likely once for each geographical area of the data center.}

Each of these will be discussed in detail below.


\section{Regions of Influence (REIN)}

\sidenote{Side}{REINs are a specific implementation of a prospective new MapSense feature called Sets. REINs are merely a Set that can only contain control-related components.  Some early documentation and under-the-hood implementation details still use the term Set rather than Region of Influence.  However, ``Set'' should not be a word visible to the user in Venus, and any such appearance should be considered a bug.  The intention is to eventually (in the Earth+ timeframe) roll out Sets as a generic grouping mechanism for all Components, but that goal is beyond the scope of both this document and this release.}

REINs represent the area that a given CRAH cools in a data center, and therefore is intended to group the Control Inputs that should be used to control that CRAH with the CRAH itself.  REINs are a third type of grouping, and behave much the same way as the other grouping types – Zones and Calculation Groups – except that a given Control Component can be a member of more than one REIN.  Components that can be places in REINs cannot be placed in either a Zone or a Calculation Group.
 
Each set can contain any number of Control Components (CoCos).  CoCos are either Control Inputs or Controlled Devices.  Control Inputs can be a member of any number of REINs.  Controlled Devices must be in exactly one REIN.  The primary role of REINs is to group Control Inputs with the Devices that will use their data.

REINs appear in the Groupings tree in the upper right corner, alphabetized with the other groupings.  In the tree,  Zones, Calculation Groups, and REINs all have icons representing their grouping type.

 REINs also appear in the ``active groupings'' pull-down with Zones and Calculation Groups.  A REIN can be made the active grouping, and be the default place for new components to land.  However, only CoCos can be placed in REINs, so attempting to place a non-Control Component when the Active Grouping is set to a REIN will result in an error message.  CoCos can only be placed in a REIN, and therefore one must exist in the data model before any CoCos can be added.

The only user-editable properties of REINs are their name and a description fields. 

REINs are fully user definable; users can create and edit REINs at will.

\sidenote{Note}{REINs get a new Component-Object pair.  The Region of Influence Component wraps the basic Set object. Sets are all children of the Datacenter, which itself gets a new childlink to contain Sets.  Sets act like zones, but at the moment only CoCos can be their children. Sets get their own Role in the model: \texttt{set}.  The intention is to add other kinds of Sets to the system beyond Regions of Influence that will use the same basic set object.}

\subsection{REIN Operations}

\subsubsection{Create a new REIN}
\begin{itemize}
\item	Choose Add Region of Influence from the Groupings Menu
\item	Available at User Level 2 (Advanced) and above
\end{itemize}

\subsubsection{Delete a REIN}
\begin{itemize}
\item	Select the REIN in the Groupings Tab, click the Delete button in the tree or press the DELETE key on the keyboard
\item	Components contained within the REIN are not deleted unless they have no other parent containers, in which case they ARE deleted.
\item	Available at User Level 2 (Advanced) and above
\end{itemize}

\subsubsection{Add Components to a REIN}
\begin{itemize}
\item	Right click on icon (or icon selection), choose Add to Grouping, and choose the desired REIN
\item	The components are now members of that REIN in addition to any other REINs they were already members of.
\item	Available at User Level 1 (Intermediate) and above
\end{itemize}

\subsubsection{Move Components to a REIN}
\begin{itemize}
\item	Right click on icon (or icon selection), choose Move to Grouping, and choose the desired REIN
\item	The components are placed in the selected REIN, and removed from any other REINs they may have been members of.
\item	Available at User Level 1 (Intermediate) and above
\end{itemize}

\subsubsection{Remove Components from a REIN}
\begin{itemize}
\item	Right click on an icon (or icon selection), choose Remove from Grouping, and choose the desired REIN
\item	A Component cannot be removed from a REIN if it has no other containers.
\item	Available at User Level 2 (Advanced) and above
\end{itemize}

\subsubsection{Selecting a REIN in the Groupings Tab}
\begin{itemize}
\item	Selects all components in that REIN
\item	Available to all User Levels
\end{itemize}

\subsubsection{Selecting a Component that is a member of one or more REINs}
\begin{itemize}
\item	When the component is selected (either via the Deployment Panel or the structure tree), the component is highlighted in the tree in all places where it appears.
\item	Available to all User Levels
\end{itemize}

\section{Control Inputs (COINs)}
Control Inputs represent a data collection point for the control system.  Control Inputs are matched with a Controlled Device by virtue of being in the same REIN; Devices use the Inputs in their REIN as the data for the control algorithm.

There are four kinds of Control Input Components: Pressure Control Inputs and Top, Middle, and Bottom Temperature Control Inputs.  Temperature COINs provide data to control CRAHs, and Pressure COINs provide data to control VFDs.

\sidenote{Technical}{All COINs wrap an object of type \texttt{controlpoint\_singlecompare} but set different object properties to differentiate between the two.  See the detailed Control Component and Object specs for the details.}

\subsection{COIN Operations}
 
COINs are in the palette at Control $\to$ Control Inputs.

The only properties the user can edit in the COINs are the Name and Notes fields.

Inputs must be placed in a Region of Influence (REIN), and cannot be placed in any other grouping.  COINs can be placed in more than one REIN.  COINs cannot be placed on the deployment plan by themselves; they must be associated with another component.

COINs use the existing association functionality to connect to sources of data.  Temperature COINs can be associated with any component that has a temperature Producer, and Pressure COINs can be associated with any component that has a pressure Producer.  There is no limit on the number of COINs that can be associated with a given component or producer, as with all other MapSense associations, however, having more than one COIN attached to a given component will result in a validation error at export time.

Adding a COIN to the Deployment Plan is as follows:
\begin{enumerate}
\item	Select a REIN as the Active Grouping.  Trying to add a COIN to another type of grouping will result in the standard “\textit{typeOfGrouping} can't contain this type of component!” message.
\item	Select the COIN type to be added in the palette.
\item	Click on the component on the floor to attach the COIN to.  If the component has a single producer of the correct datatype, the COIN is added to the deployment plan and the association is made automatically.  If there is more than one producer of the correct datatype, the standard association dialog is displayed.  If the user clicks the cancel button in this dialog, the operation is canceled and the COIN is not added to the deployment plan.  If there is no possible association to be made, the standard “Objects have nothing in common!” message is displayed.  If the user is trying to add a COIN and clicks on the deployment plan not on a component, the system displays the error “Please select a component to attach this Control Point to.”
\item	The COIN will be automatically named based on the Component it is attached to.  The COIN’s name will be the name of the parent component with the name of the COIN’s type appended to the end.
\item	Once a COIN is added, it can be assigned to or removed from any of the REINs in the deployment plan (as long as it is always in at least one REIN.)
\end{enumerate}

Deleting a COIN from the Deployment Plan can be done in the following ways:
\begin{itemize}
\item	Select the icon on the Deployment Plan and press the Delete key.
\item	Select the name of the COIN in the Groupings Tree and click the Delete button.
\item	Select a REIN in the Groupings Tree and delete it; any COINs that are only in that REIN will be deleted as well.
\end{itemize}
 
All COIN add \& delete operations are available at User Level 1 (Intermediate), as with all other MapSense components.

When a component that has a COIN attached is deleted, the COIN will be deleted as well.

All components that contain a Thermanode and are marked as “Qualified for Adaptive Control” have producers to allow temperature COINs to be attached.

\sidenote{Technical}{Control Inputs have their own role in the data model: \texttt{controlinput}}

\sidenote{Side}{Control Inputs were referred to as Control Points originally, and some older documentation and under-the-hood details may still refer to them as such.  The user should never see the term Control Point, and any place that phrase is exposed to the MapSense user should be considered a bug.}

\sidenote{Future-proofing}{Temperature Stratification COINs were cut from Mercury late in the game, and are intended to be reintroduced at some point in the future.  Strat COINs are an alternative to pressure for VFDs, and will require an evolution to the workflow for COINs.  While older documentation probably refers to stratification COINs, they do not exist in the Mercury or Venus products, and any references to them in the application should be considered bug.}

\subsection{Attach Control Inputs}

In addition to adding COINs one at a time, COINs can also be added to a selection of components at once.   When a group of components is selected, the context menu will have the option Attach Control Inputs if any of the selected components can have a COIN associated with them.  When this option is selected, the Attach Control Inputs dialog is displayed.

The dialog shows a list of all available types of COINs.  The default COINs will be pre-checked.  COINs will be broken out by association type.

\sidenote{Technical}{A COIN’s “default” status is determined by having the role \texttt{defaultcontrolinput}. Being a component role, this is not user-configurable.}

When the user clicks OK, the system will attempt to add the checked COINs to the selected components.  For each component in the selection, MapSense checks each selected COIN to see if it can be attached to that component.  COINs that are a legal association are added to the Deployment Plan and associated.  No COINs are added that would not be a legal association.  If the selection contains a Component that cannot have any COINs attached, none will be.

After completing this process, the number of COINs added will be reported in the status bar in the form "\textit{COINcount} Control Inputs Added".




\section{Controlled Devices}
Controlled Devices represent the devices being controlled by the Control System.  There are two kinds of Devices: Control CRAHs and Control VFDs.  In the real world, a cooling system contains both a CRAH and a VFD in the same physical unit; the split in MapSense is a logical one, not a physical one.  Devices, then, must always operate in pairs: one CRAH and one VFD.

\sidenote{Technical}{Each Device contains two Objects: an Output Driver and a Control Algorithm.  Currently, the system has only one algorithm: trimrespond.  As more algorithms are added, more Device Components will be created to represent the potential pairs of CRAH drivers and algorithms.}

\sidenote{Terminology}{As this document was being revised, the term “Controlled Device” was chosen over the perhaps more obvious “Control Output” to keep from overloading the word “output”.  Output is already used to describe the Output Driver in the object model; using the same word to describe the component containing both the Output Driver and the Algorithm seemed like a recipe for confusion.  The goal here is to avoid the issue we have with terms like “Property”, where there are currently at least four completely separate entities in the system called only a “Property.”}

\sidenote{Future-proofing}{So, why not roll both the CRAH and the VFD into the same Component?  In the future, we want to be able to mix and match CRAH and VFD types, since we know that this will happen, and will happen in unpredictable ways.  With this design, we only need two flexible components: one for CRAHs and one for VFDs, rather than a pantheon of all possible combinations.  Also, the intention is to allow CRAHs and VFDs to have different REINs with different sets of COINs in future versions of the system.}

\subsection{Controlled Device Operations}

Devices are located in the Palette at Control $\to$ Controlled Devices.

Devices have an editable Name and Notes fields.  No algorithm fields are editable in MapSense.  For full details on which Output Driver fields are editable, please see the Control Component Specification.

Devices must be placed in a Region of Influence (REIN), and cannot be placed in any other grouping.  Devices must be in exactly one REIN, they cannot be in more than one.  (The name “Region of Influence” literally refers to the geographical region of the datacenter that is cooled by the cooling system represented by the Device.

Both Control CRAHs and Control VFDs must be associated with an Environmental CRAH.  Control CRAHs have Consumers that accept Return Air Temperature and Supply Air Temperature from the Environmental CRAH.  Control VFDs have a Producer that provides the VFD’s fan speed to the Environmental CRAH.  The fanspeed association is of the same generic datatype used in other Calculation Graph components, and therefore other calculation components can be substituted to provide this value.  The Supply and Return Air Temperature associations are of a unique datatype, therefore Control CRAHs can only be attached to Environmental CRAHs.

\sidenote{Technical}{Fanspeed uses the \texttt{power} datatype. SAT and RAT both use the new \texttt{control\_env\_temperature}.}

Adding a Device to the Deployment Plan is as follows:
\begin{enumerate}
\item	Select a REIN as the Active Grouping.  Trying to add a Device to another type of grouping will result in the standard “<type of grouping> can't contain this type of component!” message.
\item	Select the Device type to be added in the palette.
\item	Click on the CRAH component on the floor to attach the Device to.  Depending on the CRAH, the component will either be made automatically, or the normal association dialog box will open to allow the user to select which producers and consumers to associate.
\item	Once a Device is added, it can be assigned to or removed from any of the REINs in the deployment plan (as long as it is always in at least one REIN, and at export time it is only in a single REIN.)
\end{enumerate}

Deleting a Device from the Deployment Plan can be done in the following ways:
\begin{itemize}
\item	Select the icon on the Deployment Plan and press the Delete key.
\item	Select the name of the Device in the Groupings Tree and click the Delete button.
\item	Select a REIN in the Groupings Tree and delete it; any Devices that are only in that REIN will be deleted as well.
\end{itemize}
 
All Device add \& delete operations are available at User Level 1 (Intermediate), as with all other MapSense components.

When a component that has one or more Controlled Devices attached is deleted, those Controlled Devices will not be deleted and will remain on the deployment plan.



\section{Manually-Configured Controlled Devices}
\label{MCCD}

Manually-Configured Controlled Devices (MCCD) serve the same role as ``classic'' Controlled Devices, but with much greater flexibility.


\section{View Filter}

There is a view filter button in the view filters tool bar that filters out all Control Components (both Devices and COINs).

\section{Exclusion of Control Features}

Control-related features will be present in the SySe version of MapSense, but not the HP Version.   The following features in MapSense will not be present in the HP version: 
\begin{itemize}
\item	All Control Components (2 controlled devices and 4 COINs)
\item	All REIN Operations
\item	The Attach Control Inputs tool
\item	The Control Component View Filter
\end{itemize}

\section{Validations}
\subsection{Errors}
\begin{itemize}
\item	Each REIN must contain at least one Temperature COIN.
\submsg{Each Region of Influence must have at least one Temperature Control Input.}

\item	Each REIN must contain at least one Pressure COIN.
\submsg{Each Region of Influence must have at least one Pressure Control Input.}

\item	Each REIN must contain exactly one CRAH Device.
\submsg{Each Region of Influence must have exactly one Control CRAH.}
\item	Each REIN must contain exactly one VFD Device.
\submsg{Each Region of Influence must have exactly one Control VFD.}
\item	Names for COINs must be unique amongst all COINs in the Deployment Plan.
\submsg{Cannot have more than one Control Input named name}
\item	Device names must be unique amongst all Devices in the Deployment Plan.
\submsg{Cannot have more than one Controlled Device named name.}
\item	Devices (CRAHs and VFDs) must have a model selected
\submsg{Please select a Model.}
\item	Devices (CRAHs and VFDs) must have an IP address
\submsg{Please enter an IP address.}
\item	Devices (CRAHs and VFDs) must have a valid IP address
\submsg{Please enter a valid IP address.}
\item	Devices (CRAHs and VFDs) must have a Modbus ID between 1 and 31
\submsg{Device ID must be between 1 and 31.}
\item	The combination of IP address + modbus ID must be unique for all Devices with no sitelink port configured
\submsg{IP Address and Modbus ID must be a unique combination for each Device.}
\item	The combination of IP address + modbus ID + sitelink port must be unique for all Devices with a sitelink port configured
\submsg{IP Address, Modbus ID, and Sitelink port must be a unique combination for each Device.}

\item	A given component cannot have more than one COIN associated with it.
\submsg{A component can only have a single Control Input attached.}
\item	Exception to previous: the two Horizontal Row-type Components can have more than one COIN attached, just not to the same Producer.  If a Horizontal Row or RowLite Component has >1 COIN attached to any given Producer, the error is:
\submsg{This component can only have a single Control Input attached per Producer.}
\item	A Controlled VFD or Controlled CRAH is not associated with an Environmental CRAH (of any kind)
\submsg{This Controlled Device must be associated with an Environmental CRAH.}
\end{itemize}

\subsection{Warnings}
\begin{itemize}
\item	If a COIN is in exactly one REIN, it issues a warning to that effect.
\submsg{Control Input in only one Region of Influence.}
\item	If CRAHs do not have a sitelink port between 1 and 12 entered, there is a warning to that effect.
\submsg{Sitelink Port must be between 1 and 12.}
\end{itemize}

\sidenote{Technical}{Some CRAHs may use and require a sitelink port, and some may not.  At this point we’re unclear how to configure that particular fact, hence the lack of a sitelink port being only a warning; CRAHs that don’t use sitelink will just ignore this.}

\section{Additional Resources}
At the time of this writing, the full technical specifications for Control Objects are located in the SynapSense wiki, and should be considered authoritative: 

http://wiki.synapsense.int/moinmoin/DeploymentLab/ControlObjects

The full technical details for Control Components are located in the SynapSense DMS at: Engineering $\Rightarrow$ Specifications  $\Rightarrow$ Software  $\Rightarrow$ MapSense   $\Rightarrow$ 5.3  $\Rightarrow$ configurations   $\Rightarrow$ control  $\Rightarrow$ control components.

\appendix
\section{Differences from the Phase 1 MRD}

As the MRD, this Spec, and the implementation were all developed in parallel, some differences crept in between this spec and the MRD.  This appendix details the exact differences between the Phase 1 MRD v3 and this spec.  The implementation conforms to this spec.

Control Components can be added at the intermediate user level; all component operations match existing MapSense pattern, not the elevated levels specified in the MRD.
\begin{description}
\item[4.2.5.2] there is only one Control CRAH Component, with model as a select box property
\item[4.2.5.3] there is no association between Control and Environmental CRAH Components
\item[4.2.5.4] User-editable properties for Control CRAHs are: Name, x, y, Notes, Model, IP Address, Device ID, Sitelink Port
\item[4.2.6.2] there is no association between Control VFD and Environmental CRAH Components
\item[4.2.6.3] User-editable fields for Control VFDs are: Name, x, y, Notes, Model, IP Address, Device ID
\item[4.2.7.1] Control Inputs can associate with any temperature producer; they are not explicitly limited to inlet temperatures should we add non-inlet temp producers
\item[4.2.7.2] target temperature field in control inputs defaults to 72 degrees
\item[4.2.8] Strat not included in Mercury, so 4.2.8.1, 4.2.8.3, 4.2.8.4, and 4.2.8.5 are non-operative
\item[4.2.8.6] Target pressure is 0.03
\item[4.2.9]  MapSense does not have this feature (or any other editing of target values)
\item[4.2.10.3] Non-operative as these associations do not exist
\end{description}


\section{Software Version History}
\begin{tabular}{@{} rlll @{}}
\toprule
Spec \# & Project Codename & MapSense Version & Active Control Version \\
\midrule
1 & Mercury & 5.3.0$\alpha$ & 1.0$\alpha$ \\
2 & Venus & 5.3.0 & 1.0 \\
3 & Apollo & 5.3.1 & 1.1 \\
4 & Hermes & 5.3.2 & 1.2 \\
4 & Earth & 6.0 & 1.2 \\
\bottomrule
\end{tabular}

\sidenote{}{Hermes \& Earth both match the same version of AC; the AC related features in both versions are identical.}


\section{New for Venus (AC 1.0)}

Alert readers of the Mercury version of this spec will note that comparatively little has changed.  For clarity, the following lists the changes from Mercury to Venus:
\begin{itemize}
\item	Middle and bottom temperature COINs
\item	Deleting a component deletes attached COINs
\item	COINS can be attached to all control-approved thermanodes
\item	The Attach Control Inputs feature
\item	Validate that a given component has only one COIN attached
\item	Controlled Devices have required associations to the Environmental CRAH
\item	Controlled Devices are attached like COINs (with auto-associations) and get default names
\item	The Control CRAH is a parent of the Env. CRAH and Control VFD
\item	Control features are not present in the HP Version
\end{itemize}

\section{New for Apollo (AC 1.1)}

\begin{itemize}
\item Removal of the Configure Control dialog
\item Control CRAHs are no longer parents of Control VFDs or Control CRAHs; alerts therefore no longer propagate from one to the other
\item Various object \& component changes to support OrangeBand, new SRI design, and other updates to the control system; see object model and AC specs for details.
\end{itemize}

\section{New for Hermes \& Earth (AC 1.2)}
\begin{itemize}
\item Addition of MCCD (Section \ref{MCCD})
\item Object and Component changes to reflect above
\end{itemize}




\end{document}