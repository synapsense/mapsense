% !TEX TS-program = xelatex
% !TEX encoding = UTF-8

% This is a simple template for a XeLaTeX document using the "article" class,
% with the fontspec package to easily select fonts.

\documentclass[11pt]{synap-article} % use larger type; default would be 10pt

\title{MapSense Container Files: DLZ \& CLZ \\ Project Jupiter}
\author{Gabriel L. Helman}
%\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 


\begin{document}
\maketitle

\section{Document History}

\begin{tabular}{ | l | c |  p{9cm}  | } \hline
	\textbf{Name} &\textbf{Date} & \textbf{Summary / Reason}  \\ \hline
	Gabriel Helman & 6-03-2011 & Init  \\ \hline
	Gabriel Helman & 6-20-2011 & Updated to match Mars B3  \\ \hline
	Gabriel Helman & 3-30-2012 & Updated for Jupiter  \\ \hline
\end{tabular}

\section{Overview}

Replace the existing XML-based \dl \term{.dl} and \term{.dcl} file formats for projects and component libraries with ZIP-based containers.

\subsection{Disclaimer}

This spec represents the state of this feature as it will ship with Jupiter.

\subsection{Non-Goals}

The goal is not to redesign the XML format, the object model, or the way that project files are used.

%%\section{Scenarios}



\section{Compressed Project File (.DLZ)}

\subsection{Rationale: Problems We're Trying to Solve}

Simply put, \term{dl} files are big and slow to load.  Much of the size bloat comes from the fact that they are simply XML files.  XML, being plain text with a lot of repeating data, compresses extremely well, allowing much smaller file sizes.

The unofficial standard for some time has been to zip-up a \term{dl} file before transporting it (in many cases, this is the only way to make it small enough to email.)  This eliminates a step that someone would have to do by automating it.

In a similar vein, there has been an off-and-on problem with moving \term{dl}, or any other large text file across Remote Desktop sessions where the end of the file can be truncated.  Zipping the file resolves this issue, adding one more reason zipped-DL files have become a defacto standard.  Again, this reduces the work needed by automating.

Finally, one of the more expensive things that \dl has to do when loading a \term{dl} file is decode the base64-encoded background image.  Writing to and from a binary \term{PNG} file can be much faster, so storing the image in that format should result in some loading and saving speed gains.


\subsection{Design}

	The new format is a standard Zip-Archive with the extension \term{.dlz}.  The \term{.dlz} contains the following:

\begin{itemize}
	\item The current XML-based project file (but without the image as base64) with the same name as the container file and the extension \term{.xml}
	%%\item The background image stored as a \term{PNG} with the same name as the container file and the extension \term{.png}.
	\item The background image stored as a \term{PNG} named after the DLID of the datacenter and the extension \term{.png}.
\end{itemize}

There will be no manifest files or other `table-of-contents' header files to the \term{dlz}.  \dl will be the only program editing \term{dlz} files, so we can follow a convention-over-configuration approach.

The \term{xml} files are identical to the pre-Mars \term{dl} format with one exception---the images are not be base64 encoded into the XML, they are just be stored directly into the zip archive.

The PNG file names format is \term{DLID\_\emph{<number>}.png}

\sidenote{Technical}{By `Zip', we mean the ZIP file format using the DEFLATE compression algorithm as implemented in Java's \term{java.util.Zip} package.}

%%\sidenote{Implementation}{While conceptually similar to the Java platform JAR archive, these container files are not Jars, and will not use any Jar-specific code in the implementation.}

\subsection{Exports}

The \term{dlz} format will have no effect on the 6.0 and earlier API-based export.  The new DLImportService, however, will send a container very similar to the \term{dlz} file `over the wire' to be decoded on the server.

\subsection{Backwards Compatibility}

Full backwards compatibility with the \term{dl} format will be maintained for the foreseeable future.  %(Indeed, the code paths for reading a \term{dl} vs.{} a \term{dlz} are identical other than having to unpack one XML file from a ZIP archive first.)

The XML file contained within the \term{dlz} can be extracted and used as a \term{dl} file, apart from the background image (which could then be loaded manually, also having been extracted from the archive.)  While of little normal utility, this may open some interesting debugging / troubleshooting workarounds in the future.


As of the release version of Mars, the ability to save projects in the \term{.DL} format has been removed.


%\subsection{Future Directions}
%With the file now a container, this will allow us to embed other things with project files in the future (for example, custom icons, object %definitions, Artisan Bundles, and so forth.)


\section{Compressed Library File (.CLZ)}

\subsection{Rationale}

The current \term{.dcl} files have two basic problems.  The first is the same as that for \term{.dl} files---they're large XML documents.  Changing to a ZIP-archive makes the file smaller, easier to move, and less likely to break over systems like Remote Desktop.

The other issue is that the current \term{.dcl} only contains about a third of what a Component Definition needs.  For \dl to use a component, it needs the component definition XML (which is what the current \term{.dcl} is), any icons needed to display the component on the floorplan, and any custom rules that component requires to function.  Currently, the icons and rules need to be built into \dl when it is compiled, new ones cannot be shipped without a full \dl re-build and re-release.  The massively limits the number of components that can be shipped as a standalone \term{dcl}.  

If a Component Library could contain both icons and rules in addition to component definitions, as well as overriding `built in' components, 5.3.1\_C2, 5.3.2\_C2, and the Venus Yahoo AC experiment (5.3.0\_C1) all could have been shipped as a library rather than as a custom build.  Also, components like the Dual Inlet Rack could have shipped early, rather than having to wait for the next major release to use them.

Essentially, this would allow us to use Component Libraries as a ``poor-man's patch", reducing number of custom \dl builds.


%%In the future, if this includes hooks for PEV \& UI, this would have replaced 5.3.2 all together.

\subsection{Design}
	The new format is a standard Zip-Archive with the extension \term{.clz}.  A single \term{.clz} can only contain a single component. The \term{.clz} contains the following:

	\begin{itemize}
		\item A single XML-based component definition file with the same name as the container file and the extension \term{.dcl}---identical to the pre-Mars \term{dcl} files.
		\item Any \term{svg} icons required by the components, with any name (but must match the shape name inside the dcl, obviously).
		\item Three \term{png} icons, at the three sizes of 16, 24, and 48 pixels.  The names of the \term{png} files must be: \term{\emph{<shapename>}\_\emph{<pixel size>}.png}.
		\item Any Groovy Rules, in the form a groovy script file with the extension \term{.groovy}, with any name.  There is no upper limit on the number of groovy rules in a \term{CLZ}.
	\end{itemize}

As an example, a hypothetical \term{FusionGenerator.clz} would contain:
\begin{tt}
\begin{itemize}

		\item FusionGenerator.dcl
		\item tokamak.svg
		\item tokamak\_16.png
		\item tokamak\_24.png
		\item tokamak\_48.png
		\item plutonium.groovy
		\item meltdown.groovy
		\item cleanupRobots.groovy
\end{itemize}
\end{tt}

	When loading a \term{clz} the xml file will be extracted and run though the exact same code as with the current \term{dcl} files; the components will be loaded into memory and added to the palette.

	When \dl needs to load a \term{svg}, \term{png}, or a custom rule, it searches the sub-folders within the jar file for the file name specified by the component definition.  \dl does this search only once, maintaining a cache of all previously loaded icons and rules.  Now, when searching for one of these items, \dl will first search inside of all loaded \term{clz} files.  \term{clz} files will not provide a manifest, \dl will just make a pass though the zip header to see if a file with the specified name is in the archive.  If the search target is found in an archive it is loaded into the cache; if nothing is found in a \term{clz} \dl will fall back to searching the default internal folders (as it will when no \term{clz}s are loaded.)

This way, we can override existing icons and rules if needed (for example, to patch a buggy rule).


\subsection{Backwards Compatibility}

\dl will maintain full support for loading \term{.dcl} files.

\section{Further Reading}
\begin{itemize}
	\item The Documentation for the \term{java.util.Zip} package: \url{http://download.oracle.com/javase/6/docs/api/java/util/zip/package-summary.html}

	\item The DLImportService Spec 
\end{itemize}

\end{document}
