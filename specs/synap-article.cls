\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{synap-article}

% Passes and class options to the underlying article class
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions

\LoadClass[titlepage,11pt]{article}

\RequirePackage[left=1in,right=1in,top=1in,bottom=1in,letterpaper]{geometry}
\RequirePackage[parfill]{parskip}
\RequirePackage{graphicx}
\RequirePackage{grffile}
\RequirePackage{fontspec}
\RequirePackage{xunicode} % Unicode support for LaTeX character names (accents, European chars, etc)
\RequirePackage{xltxtra} % Extra customizations for XeLaTeX
\RequirePackage[bookmarks=true]{hyperref}
\RequirePackage{fancyhdr}
\RequirePackage{epstopdf}
\RequirePackage{sectsty}
\RequirePackage{fullpage}
\RequirePackage{hyperref}
\RequirePackage{geometry}

\geometry{a4paper}

\defaultfontfeatures{Mapping=tex-text}

\newfontfamily\applefont[Numbers=OldStyle]{Myriad Pro}
\allsectionsfont{\applefont}


%%\setmainfont[Ligatures={Common},Mapping=tex-text,Numbers=OldStyle]{Myriad Pro}
\setmainfont[Numbers=OldStyle]{Palatino Linotype}
%%\setmainfont[Numbers=OldStyle]{Constantia}
%%\setmainfont[Numbers=OldStyle]{Calibri}
\setmonofont[Scale=MatchLowercase,Mapping=tex-text]{Consolas}


\headheight 15pt


\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\RequirePackage{titlesec}

\titleformat{\part}[display]  {\normalfont\Large\filcenter\sffamily}  {\titlerule[1pt]
   \vspace{1pt}
   \titlerule
   \vspace{1pc}
   \LARGE\thepart}
  {1pc}
  {\titlerule
   \vspace{1pc}
   \Huge}

\let\oldpart = \part
\renewcommand{\part}[1]{
	\pagebreak
	\oldpart{#1}
}

% Adjust the title page design
\newcommand{\maketitlepage}{
  \null\vfil
  \vskip 60\p@
  \begin{center}
    {\LARGE \@title \par}
    \vskip 3em
    {\large
     \lineskip .75em
      \begin{tabular}[t]{c}
        Authors: \@author
      \end{tabular}\par}
      
  \end{center}\par
  \@thanks
  \vfil
  
  \begin{center}
  \vskip 33em \vfil
{SynapSense Corporation - Confidential \linebreak
\tt www.synapsense.com}

\vskip 2em
{\small Generated: \today \enspace  by Xe\LaTeX}
\end{center}
  \vfil\null

  \end{titlepage}
}


\renewcommand\maketitle{\begin{titlepage}
  \let\footnotesize\small
  \let\footnoterule\relax
  \let \footnote \thanks
  \maketitlepage
  \setcounter{footnote}{0}
  \global\let\thanks\relax
  \global\let\maketitle\relax
  \global\let\@thanks\@empty
  \global\let\@author\@empty
  \global\let\@date\@empty
  \global\let\@title\@empty
  \global\let\title\relax
  \global\let\author\relax
  \global\let\date\relax
  \global\let\and\relax
}


%%\newcommand{name of new command}[number of arguments]{definition} 
\newcommand{\sidenote}[2]{ \begin{quote} \it {#1} Note:\\{#2} \end{quote} }
\newcommand{\on}{\texttt{true}}
\newcommand{\off}{\texttt{false}}

%\newcommand{\ob}[1]{\texttt{#1}}
\newcommand{\ob}[1]{\textsc{\lowercase{#1}}}
\newcommand{\comp}[1]{\textsc{\lowercase{#1}}}
\newcommand{\term}[1]{\texttt{#1}}
\newcommand{\cre}[1]{\texttt{#1}}
\newcommand{\prop}[1]{\texttt{#1}}
\newcommand{\classname}[1]{\texttt{#1}}

\newcommand{\dl}{DeploymentLab\ }

\endinput