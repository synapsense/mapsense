\include{articlespec}

\title{WebServices via XML Integration Plugin (WSvX) \\
Functional Specification \\
5.3 Venus
}
\author{Gabriel Helman}
%\date{June 15, 2010} % Activate to display a given date or no date (if empty), otherwise the current date is printed 

\begin{document}
\maketitle

\section{Document History}
\begin{tabular}{ | l | c | l | }
\hline
Gabriel Helman & 6-15-2010 & Initial Document \\ \hline
Gabriel Helman & 6-18-2010 & Fixed paragraph breaks \\ \hline
\end{tabular}

\section{Overview}
The WebServices via XML plugin allows integration with arbirtrary XML documents. Values from URI-accessible XML or HTML documents can be used as inputs to a Calculation Graph.
Any XML or HTML document that the Device Manager can access via a URI over the HTTP protocol can be used. Any value in that document than can be addressed by a valid XPath can be returned to the system.
\subsection{Disclaimer}
This document represents the state of the plugin as it shipped with Version 5.3 Venus.  As such, the design incorporates several compromses and specific customer requests.

This document also assumes some basic familiarity with URL / URI Syntax, the HTTP protocol, and the XPath query language.

\subsection{Non-Goals}
This plugin is designed to be a very simple to configure and maintain XML ``screen-scraper'', not a full featued XML data transfer platform.  It is intended to transfer a single datum per request, not a data structure.
\newpage


\section{Scenarios}
\subsection{Dranetz}
The AT\&T Allen Datacenter uses a system called Dranetz to provide power meter information for the PDUs in the datacenter. The Dranetz system collects data from meters in the PDU and provides access to them via a web server. The server has a web interface that operators can use to browse data about the system, but Dranetz also allows for arbitrary data to be queried via a HTTP request. This URI includes the PDU name as a query parameter, and the response is an XML document containing the values requested.

Therefore, to collect the kW used by each of the 53 PDUs in the datacenter for a PUE graph, the system has to send a separate HTTP query to the Dranetz system for each PDU and extract the reported kW load from the XML. These values are then released into the rest of the PUE graph as like any other integration value.
\sidenote{Historical}{Dranetz at Allen was the reason this plugin was originally written.}

\subsection{Arbitrary Web Pages}
A datacenter wants to calculate the efficiency of their Air-Side Economizer. In this calculation, they wish to include the ambient temperature and humidity outside the building, but they have no weather station installed, nor do they intend to install one.
Using the WSvX plugin they can query Weather.com and obtain the local temperature and humidity for their area for use in their metric.

\subsection{RSS / ATOM feeds}
A wily datacenter operator constructs an RSS feed of information about the datacenter's operation that the operations team can subscribe to with their smart phones and street computers. Using the WSvX plugin, PUE or other custom metrics can have access to this same information as inputs.

\subsection{Local XML Files}
A collection of values that the customer wants to use in a custom metric are present in an XML file on the local network. (Either on the local file system of the Environment Server, or served by a web server.) Rather than plug these values into User Entered values and re-export the MapSense DL file every time they change, they can use the WSvX plugin to point at this XML document, which allows them to change the document as they see fit without having to alter the configuration of the system.


\section{Details}
\subsection{Basic Design}
Overall, the basic design of WSvX is very simple. The plugin needs two pieces of information: a URI and an XPath. The XPath is executed against the document represented by that URI, and the resulting value is returned.

\subsection{URI Format}
The syntax of the URIs WSvX is that defined in RFC 2396: Uniform Resource Identifiers (URI): Generic Syntax, and amended by RFC 2732: Format for Literal IPv6 Addresses in URLs.
More simply, the URI \emph{must} have a protocol, host name, and a document path. Port numbers, references, and query parameters are all supported, but optional. Further, for URIs that require authentication, the authority part of the URI can be used. (See section \ref{sec:auth} for details.)
Special characters in a URI must be escaped with the standard XML / HTML escape sequences.
See \texttt{http://www.ietf.org/rfc/rfc2396.txt} and \texttt{http://www.ietf.org/rfc/rfc2732.txt} for the complete RFCs.
\subsection{XPaths}
WSvX implements XPath version 1.0. See \texttt{http://www.w3.org/TR/xpath/} for the full details of the W3C spec.

\subsection{Authentication}
\label{sec:auth}
WSvX supports both the Basic and Digest forms of HTTP-Authentication. A full discussion of HTTP Authentication is beyond the scope of this document. The plugin will automatically determine the correct authentication method to use and provide credentials as needed.
The credentials are to be provided in the authority section of the URI. The authority follows the basic format of \texttt{ \textit{scheme}://username:password@\textit{host}}

\sidenote{Implementation}{Dedicated Students of the Internet will note that this form of authentication via URL is frowned on these days, despite being the standard in the 1990s. However, the authentication section is still part of the URL format, was intended as the primary way to authenticate against HTTP auth, and is still recognized by all current browsers, so the decision was made to retain this format.}

\sidenote{Historical}{This feature was also implemented to support the Dranetz system at AT\&T Allen, which uses Digest Authentication.}

\subsection{MapSense Components}
MapSense provides two Components for integration with WSvX; \texttt{Numeric Web Service Input} and \texttt{Status Web Service Input}. Both are located in the palette at \texttt{Calculations > Wired Integration}.

In addition to the normal Name and Notes fields, both Components have fields named URI, XPath, and Poll Interval. The URI field contains the full URI for the XML resource to be accessed. The XPath field contains the full XPath query expression. The Poll Interval is the number of minutes to wait between querying for data. The Poll Interval is entered in minutes, and defaults to 5.

Numeric Web Service Input Components return a numeric value (specifically, a double-precision floating point.) If the value referred to by the XPath cannot be coerced to a number, the Component returns \texttt{null}.

The Numeric Web Service Input has a single Producer named Web Services Value of the datatype \texttt{power} which can be directly connected to any other Numeric Calculation Graph Component.

Status Web Service Input Components return a status value of true or false. They have an additional Property: Whitelist. This is a user-entered comma separated list of string values. When the plugin polls the source document, the value returned by the XPath is checked against the Whitelist. If the value is on the Whitelist, the Component returns \on, if the value is not in the Whitelist, the Component returns \off.

The Status Web Service Input has a single Producer named Web Services Status of the datatype \texttt{status} which can be directly connected to any other Status Calculation Graph Component.

Both types of WSvX components go in a Calculation Group and a WebService Host DataDource. The WebService Host DataSource has no configurable fields other than its name. As each WSvX Component has its own URI, a given MapSense project only needs one WebService Host for the WSvX plugin to function. More can be added without impacting performance (for organization, for example.)

\subsection{Calculation Graph Integration}
WSvX Components provide a single Producer. The Numeric WSvX Components provide a Producer with the datatype \texttt{power}, the standard datatype used by all numeric calculation graph components. Status WSvX Components provide a singe Producer with the datatype \texttt{status}, which is the datatype used by all Equipment Status-based Components. Therefore, the WSvX Components can be integrated into a PUE or Calculation Graph directly, with the values from the source document being fed directly into the graph.

\subsection{Console Visualization}
There is no direct visualization of WSvX points in The Console. However, Inspectors can be attached directly to any WSvX Component, allowing historical representation of each point in the Custom Metrics tab.

\subsection{Device Manager}
WSvX uses the standard Device Manager plugin architecture. The plugin is not installed by default.

\sidenote{Technical}{Under the hood, the plugin uses the \texttt{java.net.URL} libraries to access the URI, and the \texttt{javax.xml.xpath.XPath} library to resolve the XPath.}
It is important to note that as of Venus, MapSense and other front-end components refer to WSvX as the ``Web Services Plugin``, whereas Device Manager's Installer refers to it as the ``xml plugin''. And, just to make things slightly more complex, Device Manger has a different plugin it calls ``Web Services''; MapSense and others refer to this second plugin as ``SOAP''.

\section{Failure Cases}
\subsection{Invalid URI}
If accessing the URI raises an HTTP Error Code (404 or 503, for example) the Plugin will stop at the error, raise an alert, and return no value. The alert will contain basic details about the HTTP error.

\subsection{Valid URI, Syntactically Invalid XPath}
Stop at the error, raise an alert, and return no value. The Alert will contain information to the effect that there was an XPath syntax error.

\subsection{Valid URI, Syntactically Correct XPath that Resolves to Nothing}
Return no value, do not raise an alert, but log the situation in the DM log file.

\sidenote{Design}{This was not the original design, but the result of AT\&T's request.  The Dranetz system at Allen would become unavailable for 30-40 minutes about once a day while the InfoNode rebooted.  During this time, the request URLs, rather than retuning an HTTP error code, returned a single static HTML page stating that the system was resetting and to please wait.  Rather than returning null data, which made the PUE graph go all screwy, AT\&T requested that the system just continue to use the last known value until Dranetz came back on-line.  By returning no value, Environment Server will continue to use the last known value from the cache when it needs to access the value for charting or calculations.}

\subsection{Value referenced by the XPath cannot be coerced to a Number}
For Numeric Components, the value returned will be \texttt{null} rather than a number. For Status Components, the status value will depend on the Whitelist; the XPath's returned value is never coerced to a number.


\end{document}
