% !TEX TS-program = xelatex
% !TEX encoding = UTF-8

% This is a simple template for a XeLaTeX document using the "article" class,
% with the fontspec package to easily select fonts.

\documentclass[11pt]{synap-article} % use larger type; default would be 10pt




\title{DeploymentLab: \\ Update Configuration \& Revised Upgrade Process  \\Project Jupiter}
\author{Gabriel L. Helman}
%\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\maketitle
\tableofcontents

\section{Document History}

\begin{tabular}{ | l | c |  p{9cm}  | } \hline
	\textbf{Name} &\textbf{Date} & \textbf{Summary / Reason}  \\ \hline
	Gabriel Helman & 4-13-2011 & First Draft \& Initial Distro  \\ \hline
	Gabriel Helman & 4-28-2011 & Minor grammar \& spelling fixes  \\ \hline
	Gabriel Helman & 10-10-2011 & Update to synap-article \\ \hline
	Gabriel Helman & 03-30-2012 & Updates for Jupiter \\ \hline
\end{tabular}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Overview}
Modify the existing replace configuration tool to both be able to maintain object history and replace the current XSLT-based upgrade scripts.

\subsection{Disclaimer}
This document represents the design as was shipped with Mars \& Phobos.


%\subsection{Non-Goals}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Problems We're Trying to Solve}


\subsection{XSLT Upgrade Scripts}
The XSLT-based upgrade system used in Earth and earlier releases has several weak points:

\begin{itemize}
	\item Can't add objects to an existing component
	\item Can't remove objects from an existing component
	\item Changing default values is difficult
	\item XSLT scripts are time-consuming to write
	\item Essentially this creates two Component definitions
	\item Can only upgrade from release build to release build
	\item Difficult to test upgrade correctness
\end{itemize}

There is at least one item targeted for Mars (Bug 5743; CRAH tonnage in PUE) that requires adding objects as part of the upgrade process.  There are several other items in the Mars `nice to have' and Future Release categories that will either require this, or this would make significantly simpler.

In addition, as the DeploymentLab team is down to a single developer full-time, the amount of time the XSLTs take to write and maintain is becoming a liability.


\subsection{Replace Configuration}

In the past, these issues can be partly ameliorated by using the Replace Configuration tool to perform a `manual' upgrade to a component.  However, this has one huge liability, which is that Replace Configuration deletes all history of the Component in question.  For components like CRAHs, deleting all the history is not acceptable, even to gain tonnage in PUE calculations.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{How it works now}

A brief review of how the two tools in question work as of Earth:

\subsection{Replace Configuration}

Replace Configuration is essentially a macro for creating a new component, coping over any user-entered values, and then deleting the old component.  To the server, this is indistinguishable from a create \& delete.  This means that the new objects get new \term{TO}s, which means all old history is lost.

Replace Configuration determines the `replacablility' of two component by checking to see if they having exactly the same types of parents.  That is, to Replace Config on something that goes in a WSN Network and a Zone, it can only be replaced by something that also goes in a WSN Network and a Zone.  (Meaning that a Rack can't be `turned into' a modbus BCMS, for example.)

\subsubsection{Existing Replace Configuration Logic Flow}
\begin{enumerate}
	\item Create New Component
	\item Copy Component Properties
	\item Copy Associations
	\item Make Parent-Child Relationships
	\item Delete Old Component
	\item If a \textit{secondary}\footnote{Secondary Components are those that behave like COINs or Inspectors---they're attached directly to another Component and inherit their ``host's'' name.} component, regenerate name
\end{enumerate}

\subsection{XSLT-based Upgrade Scripts}

XSLT is an XML-based language for performing transformations on XML documents.  As the DL file format is itself an XML document, an XSLT script can transform the components expressed as XML from their old form to a new form.

DeploymentLab has a collection of XSLT scripts, one for the transition between each version of the software.  For upgrades across multiple versions, multiple scripts are run in sequence.  For example, upgrading from 5.3 Venus to 6.0 Earth runs three scripts: Venus$\to$Apollo, Apollo$\to$Hermes, and Hermes$\to$Earth.

Once a version is released, that XSLT is considered frozen, and any bugs found in it at a later date will be fixed by another transformation is a later script.

Which scripts to run is determined by the internal version number of the DL file---DeploymentLab maintains a list of which scripts to run for each older version number to get that file up to date.  This is why the current system can't do partial upgrades or upgrades between internal builds---once a the file's version number has been advanced, those scripts won't be run again, and the version numbers will need to increase again to add another script.

\sidenote{Future-proofing}{Also, depending on XSLT for upgrades prevents us from being able to use a serialization format other than XML in the future.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\section{New: Update Configuration}

The primary shortcoming of Replace Configuration---the loss of history---can be solved by augmenting the tool into Update Configuration.  In short, copying over the object \term{TO}s in addition to the other component-level properties.

\subsection{New Update Configuration Logic Flow}
\begin{enumerate}
	\item Create New Component
	\item Copy Component Properties
	\item \textbf{New: Copy Object Data}
	\begin{enumerate}
		\item For each Root Object:
		\item Copy ES Key
		\item Remove ES Key from old object
		\item Copy all Property \term{oldValue}s
		\item Copy any Property \term{override}s
		\item Copy all CustomTag \term{oldValue}s
		\item Copy any CustomTag \term{override}s
		\item Recurse into child objects using fancy object matching techniques
	\end{enumerate}
	\item Copy Associations
	\item Make Parent-Child Relationships
	\item Delete Old Component
	\item If a \textit{secondary}\footnote{Secondary Components are those that behave like COINs or Inspectors---they're attached directly to another Component and inherit their ``host's'' name.} component, regenerate name
\end{enumerate}

\subsection{Let's talk about that list}
The main change is copying over the ES Key.  This is what the object history in the server is based on, so maintaining the key allows us to keep history; turning a replace into a update.

While Replace Configuration keeps any user-entered values form the Component level, it blows away any changes made directly in the Edit Configuration tool.  While this was the desired behavior for a Replace Config, this needs to be changed for an Update.

With the new Override flagging in Earth, the problem is much reduced.  All Property \term{value}s are left alone to be filled in with either the defaults for the new object or filled in by the component properties.  However, any \term{override}s will be copied over, maintaining their existing override behavior.  So, any new default values will be installed in the new objects, but the user-entered override will be maintained exactly.

\sidenote{Backwards-Compatibility}{Since the Override tracking was only added in Earth, any Edit Config changes entered in a version of DeploymentLab prior to that will be lost in a Update Configuration.}


\subsection{Object Matching Strategies}

The main trick to making this work will be to match objects with each other in the old and new component, so that the \term{TO} and other values can be copied to the right object.

Each Component contains at least one Object, each of which can be the start of a tree of objects.  Each object in a component without a parent inside that component is called a root of that component.

Generalizing across the current component library, there are four basic structures that Update Configuration needs to support.

\paragraph{Roots Only}
Each root has a unique string identifier within the component (The `as' name.).  Roots can therefore be matched based on this name.  In the case of a component with only roots, this is all we need to do.


\paragraph{WSN Nodes}

Each \ob{WSNNode} has a list of \ob{WSNSensor}s as children.  Each sensor, at minimum has a unique \prop{channel}, and a (hopefully) unique \prop{name}.  Matching by \prop{channel} doesn't work, as this fails in cases where, for example, going from a 5 to a 6 thermistor string adds a new thermistor at the top of the list and then `pushes' everything else down.  

However, we can match by object \prop{name} property. Every \ob{sensor} has a unique name under that \ob{wsnnode}.\footnote{The sole exception to this rule caused us some excitement at a certain unnamed customer site; this is now true across the entire component library.}  This doesn't handle the cases where a \ob{wsnsensor} name changes; but this is a rare occurrence.

There are some narrow cases where matching on channel works better than matching on name. (RowLites, for example, use the same name for the two sensors on every sub-rack.)  DeploymentLab has a map of component types in \term{updateByChannel.properties} that indicates specific component to component upgrades that should use channel matching rather than name matching.


\paragraph{Modbus Devices}

Each \ob{modbusdevice} has a list of \ob{modbuspropery} objects as children.  Each \ob{modbusproperty} represents a data value being read from the device.


\paragraph{ProducerPoints}

\ob{producerpoint}s are objects that are used to connect any value to the Calculation Graph system---they can be the child of any other object type, and redirect a single value from the parent object into its \prop{lastValue} field.

Each \ob{producerpoint} has a \prop{fieldname} field that identifies the field from the parent that it should copy.  By definition, each \ob{producerpoint} under an object must have a unique \ob{fieldname}.  We can match \ob{producerpoint} based on this.  \ob{producerpoint}s also have \prop{name} field that, by convention, has the same contents as the \prop{fieldname}.\footnote{The reasons for having two fields have been lost to history.}


\paragraph{Other Deep Trees}

In addition to the above, Mars and Jupiter added several other deep parent-child trees of components (LonTalk, Database Integrators, BacNet).  These use the same name-based matching strategy outlined above.  Update Configuration will recursively descend a set of objects managed by a component to any depth using this strategy.


\paragraph{Only Children}

There are a few cases where a given object has only a single child.  In this case, the other matching strategies are ignored, and as long as the two only children are the same type they're treated as a match.


\paragraph{Bad Ideas} A few matching strategies that have been considered and abandoned:
	\begin{itemize}
		\item  Match by child order---doesn't work because this can change from config to config---if a thermistor is added at the top of the list of children, for example.
		\item  By channel number only---can also change; only applies to WSN
	\end{itemize}


\subsection{Use Cases}

Update Configuration has three major use cases that the current system can't support:

\subsubsection{XSLT-Free upgrades}
In short, we can Update a component to its current version.  This is the driving use-case for this feature.  See Section \ref{XSLTFREE}.

\subsubsection{Software-Hardware Install Mismatch}
A node is installed in one configuration (say, an End-Cap Node) and was always intended as that configuration.  However, in DeploymentLab that node was placed as another configuration (say, a Rear-Exhaust with Subfloor.)  Previously, this would require a Replace Configuration to get a correct visualization, which deletes all history of that node---which is not as desirable if that piece of hardware has been running for weeks or months before the mismatch was noticed.  With an Update Configuration, history would be maintained for the sensors with the same names, and deleted for those whose names changed under the new config, which means only the history that was being stored accurately would be kept.

\sidenote{Implementation}{This might be a case to have a user-facing selection switch for WSN-based nodes to allow them to select a name or channel-based matching strategy; in this case the channel numbers would be correct, and all history would be maintained.}

\subsubsection{Hardware Upgrade}
In the case where a customer currently has a pre-control 5-thermistor string configuration and they want to move to a control-validated 6- or 7-thermistor string configuration, again the current system only provides Replace Configuration.  With Update, the component can add the extra thermistor (and move the history from the node internal temperature sensor to the new external sensor) without losing any history.

\subsection{Other Exceptional Cases}

\subsubsection{Static Children}
Replace/Update Configuration will always skip components with the \term{staticchild} role.  The objects are assumed to be updated as children of the static child parent.

\subsubsection{Irreplaceables}
There is a list of components that will never be turned into another type via Replace/Update Configuration regardless of the other rules.  This list is stored in he \term{components.properties} in the \term{components.irreplaceable} key.  As of Jupiter, that list contains only \term{contained\_rectangle} and \term{contained\_poly}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{XSLT-Free Upgrades}
\label{XSLTFREE}

With the above in place, XSLT-Free project file upgrades become comparatively simple.  File model version numbering continues to work as before.  When DeploymentLab opens a file with a smaller version number than the current version, it automatically calls Update Configuration on every Component in the project, updating them to the current configuration.

To support build-to-build upgrades, we can supply a version of the Atomics button that uses Update rather than Replace configuration.

To maintain full backwards-fidelity, we should also leave the existing XSLT scripts in place to upgrade a file from 5.x to 6.0, only using Update Configuration for 6.0 forward.  This saves us from having to re-test things like the WSN Migration or other existing upgrades.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Further Reading}
The spec for the original Replace Configuration is in the wiki at \url{http://wiki.synapsense.int/moinmoin/ReplaceConfiguration}

\end{document}
