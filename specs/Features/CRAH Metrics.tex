% !TEX TS-program = xelatex
% !TEX encoding = UTF-8

\documentclass[11pt]{synap-article} % use larger type; default would be 10pt

\usepackage{fontspec} % Font selection for XeLaTeX; see fontspec.pdf for documentation
%%\defaultfontfeatures{Mapping=tex-text} % to support TeX conventions like ``---''
\usepackage{xunicode} % Unicode support for LaTeX character names (accents, European chars, etc)
\usepackage{xltxtra} % Extra customizations for XeLaTeX

%%\setmainfont[Numbers=OldStyle]{Myriad Pro}

%%\setmainfont[Numbers=OldStyle]{Baskerville Old Face}
%%\setmonofont[Scale=MatchLowercase]{Consolas}

% other LaTeX packages.....
\usepackage{geometry} % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper} % or letterpaper (US) or a5paper or....
%\usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

\usepackage{graphicx} % support the \includegraphics command and options
\usepackage{fullpage}

\title{CRAH Efficiency Metrics \\MapSense Functional Specification }
\author{Gabriel Helman}
%\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\maketitle


\section{Document History}
\begin{tabular}{ | l | c | l | }
	\hline
	Gabriel Helman & 6-10-2010 & Initial Distro \\ \hline
	Gabriel Helman & 6-25-2010 & Addition of Console Visualization Section \\ \hline
	Gabriel Helman & 3-30-2012 & Formatting; updated to Jupiter design \\ \hline
\end{tabular}

\section{Overview}
These features allow for a MapSense user to configure CRAH components to calculate the Cooling Efficiency metric.

Three new items have been added to CRAH \& CRAC\footnote{For Jupiter, this includes Halley-based CRACs.} Components to support Efficiency Metrics: two data tables and one consumer.  These features have been added to all CRAH \& CRAC Components in the system.  In addition, Controlled VFDs have a producer for Fanspeed.

\subsection{Disclaimer}
This spec is the result of design exercises that took place after the MRD was written, and as a result diverges significantly in a few key places (including correcting one critical math error in the original MRD.)  This spec only applies to MapSense, not the system as a whole.  Issues that are the purview of the Control System, the Console, or other portion of the system are left as an exercise for the MRD or other specs.

\subsection{Non-Goals}
This feature, as with the majority of the Calculation Graph related features in MapSense assumes that less user restriction rather than more restriction is preferred.  Therefore, warnings are preferred to errors, and wherever possible the user should be allowed to do what they wish (as long as they're actions won't damage the system) under the assumption that the user is clever and should be allowed to try things.

\newpage
\section{The Metrics}

The final goal is to calculate Cooling Efficiency.
$$ \textit{Cooling Efficiency} =  (  \frac{\textit{Actual Tons}} { \textit{Rated Tons}}) * 100 $$

 In order to compute this, both Actual Tons and Rated Tons must be computed.  Actual Tons is computed by calculating Actual BTUs, then converting to Tons at a rate of 1 ton = 12000 BTUs. 

$$\textit{Actual BTUs} = \textit{CFM} * 1.085 * (\textit{Return Air Temperature} - \textit{Supply Air Temperature}) $$ 

$$\textit{Actual Tons} =  \frac {\textit{CFM} * 1.085 * (\textit{Return Air Temperature} - \textit{Supply Air Temperature}) }  {12000}  $$

CFM is computed by looking up the current VFD fanspeed on a table of rated CFMs per VFD fanspeed.

$$\textit{CFM} = \mathtt{CFMTable}(\textit{Fanspeed})  $$

Rated Tons is computed by looking up the current Return Air Temperature on a table of CRAH rated BTUs vs. Temperature, and then converting BTUs to Tons at a conversion rate of 1 ton = 12000 BTUs.

$$\textit{Rated Tons} = \frac{ \mathtt{BTUTable}(\textit{Return Air Temperature}) }{12000} $$

\begin{quote}
\it
Technical Note:
The original MRD had the Actual BTUs equation as producing Tons instead.  That has been corrected in both this spec and the final product.
\end{quote}

Each of these three calculations has its own \term{groovyrule} on the \ob{CRAH} object that computes that value.  The inputs to the metrics, then, are the CRAH's internally measured Return and Supply Air Temperatures, the Fanspeed consumer, and two tables entered by the user.

The new metrics are added retroactively to all CRAHs in the system, and are considered optional.  If one or more of the inputs is not provided for a given CRAH component, the metrics will not be computed.

The metrics will be recalculated every five minutes on the hour.

\section{Consumers}

CRAH Components have one new Consumer: Fanspeed.  This has the association datatype \texttt{power}, which is the standard datatype used for all unitless calculation graph associations (and which is increasingly misnamed.)  This allows this  input to be provided from any source: our Controlled Devices, a generic Modbus integration point, a calculation graph computing the value, or even a plain user entered value.  Installations without control can therefore use these metrics by providing this value from another source.  No validations are performed on this Consumer; it is optional and can be connected to any valid Producer.

\section{Producers}
Controlled VFDs now have a producer for Fanspeed.  This produces the current fanspeed percentage value as read from the device itself by the Control Process.  This producer is of the \texttt{power} datatype.  No restrictions on association beyond the datatype is performed.

\section{Tables}

Environmental CRAHs now have two tables that the user can enter: CRAH Rated BTUs an VFD Rated CFMs.  These appear on the Properties Editor in MapSense as \texttt{Sensible Cooling Table} and \texttt{Air Volume Table}.  The value in the properties table is a special button that indicates that a different editor will open when clicked.  The text on this button is \texttt{(Table)}.

When clicking on this button, MapSense opens the Table editor.  Both types of tables share the same basic functionality in their editors.

The BTU / Sensible Cooling Table allows the user to enter rated BTU values for the following temperatures in degrees F: 40, 65, 70, 75, 80, 85, 90, 95, 100.  The temperature values in the left column are user-editable.

The CFM / Air Volume Table behaves the same way, except that the user enters rated CFMs at 20, 30, 40, 50, 60, 70, 80, 90, and 100 \% fanspeed.  Again, the percentage values are editable.

For both tables, only two data points are required for the system to extrapolate a graph.  A single point entered will result in the system returning that value for all inputs.  Obviously, more values result in better accuracy.  Obtaining the values for these two tables is assumed to be the responsibility of the customer or deployment team; due to the variances in CRAH models, VFD configurations, filters, air flow, and any number of other issues every CRAH will have it's own slightly different set of values, preventing us from being able to ``pre-can" any defaults.

No metrics will be calculated if either table is empty.

\section{Validations}

Warnings:
\begin{itemize}
\item If the BTU table has less than two entries, there is a warning ``The Sensible Cooling Table provided in \emph{componentName} has less than two points.''

\item If the CFM table has less than two entries, there is a warning ``The  Air Volume Table provided in \emph{componentName} has less than two points.''

\end{itemize}

\newpage
\appendix
\section{Differences and Clarifications from the MRD}

A significant amount of design and prototyping work took place after the MRD was finalized; as a result the final implementation diverges from the MRD in several ways.  This appendix details the exact differences between the MRD and this spec.  The implementation conforms to this spec.

\begin{description}
	\item 2.8, 2.9. 2.10, 3.7, 3.8, 3.9 - Both table properties are represented in the properties panel with a button displaying the name of the current table.  Clicking the button openes the table editor; all other table editing tasks are performed here.  Specifically, the pull-down list of available tables is in this editor, not the main properties table.
	\item 3.1 - The list of fanspeeds to enter CFMs for has expanded from 4 entries to 9.
	\item 4.1 - The Return Air Temperature used to look up the BTUs is that from the SySe sensor in the Environmental CRAH Component.
	\item 5.1 - The Fanspeed used to look up the CFMs is from a source external to the Environmental CRAH; this is assumed to be the Controlled VFD component in most cases, but could be any integration or calculation graph component.
	\item 5.2 - The Actual Tons equation listed in the MRD is incorrect.  The correct equation is in the section above titled Metrics.
\end{description}

\section{Changes since Venus}

There have been several minor redesigns since this feature first shipped.

First, the tables have both been renamed, \emph{BTU Table} to \emph{Sensible Cooling Table} and \emph{CFM Table} to \emph{Air Volume Table}.

Second, the left hand columns of the tables are now editable, with the formerly hard-coded values as the defaults.

Third, the user used to be able to give each table a name, and use that to copy values from one device to the other. This was under-used and heavily error prone, so it has been removed.  The Copy Settings tool, added in Mars, supersedes this functionality.

Finally, the values in both columns of the table are converted to the currently selected unit system.

\section{Console Visualizations}

Several of the new properties added to CRAHs are now also made available to visualize in the Console, either in the CRAH/CRAC Tab or in the Data Analysis tool.

\begin{tabular}{ | c | c | p{6cm} | c | c | }
\hline
Object Property Name & Displayed Name & Contents  \\ \hline
\texttt{in\_fanspeed} & Fanspeed & The VFD fanspeed as being currently reported to the CRAH object.   \\ \hline
\texttt{tonsDesign} & Cooling Capacity & The result of the Design Tons calculation.  \\ \hline
\texttt{tonsActual} & Actual Cooling Power & The result of the Actual Tons calculation.  \\ \hline
\texttt{coolingEfficiency} & Cooling Efficiency & The final cooling efficiency.  \\ \hline
\end{tabular}

\newpage
\section{Dialog Designs}

\begin{figure}[h!]
\centering
\includegraphics[width=2.45in]{crah_metric_editor}
\caption{Properties Table}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=3.5in]{crah_sensible_cooling}
\caption{BTU Table Editor}
\end{figure}



\end{document}
