% !TEX TS-program = xelatex
% !TEX encoding = UTF-8

% This is a simple template for a XeLaTeX document using the "article" class,
% with the fontspec package to easily select fonts.

\documentclass[11pt]{synap-article} % use larger type; default would be 10pt

\usepackage{fontspec} % Font selection for XeLaTeX; see fontspec.pdf for documentation
%%\defaultfontfeatures{Mapping=tex-text} % to support TeX conventions like ``---''
\usepackage{xunicode} % Unicode support for LaTeX character names (accents, European chars, etc)
\usepackage{xltxtra} % Extra customizations for XeLaTeX

%%\setmainfont[Numbers=OldStyle]{Myriad Pro}
%%\setmonofont[Scale=MatchLowercase]{Consolas}

% other LaTeX packages.....
\usepackage{geometry} % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper} % or letterpaper (US) or a5paper or....
%\usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

\usepackage{graphicx} % support the \includegraphics command and options
\usepackage{fullpage}

\title{MapSense Spotlight Search \\
Jupiter Edition \\
Functional Spec }
%June 9, 2010
\author{ Gabriel Helman }

\begin{document}



\maketitle

\section{Document History}
	\begin{tabular}{ | l | c | l | }
		\hline
			Gabriel Helman & 6-09-2010 & Venus Edition \\ \hline
			Gabriel Helman & 6-22-2010 & Changed Formatting \\ \hline
			Gabriel Helman & 3-30-2012 & Updated for Jupiter \\ \hline
	\end{tabular}

\section{Overview}
Spotlight Search aims to make searching for nodes in the Deployment Plan, easier, faster, more useful, and even fun.

\subsection{Disclaimer}
This spec represents the state of the feature as released in 6.2.0 Jupiter.  “Spotlight Search” is intended as an internal codename, and is a reference to the Apple OS X feature that inspired aspects of its design.  This name will not be exposed externally. 

\subsection{Non-Goals}
Spotlight aims only to make it easier to find and edit things on the deployment plan.  It is not intended as a replacement for the project stats window.

\section{The Problem}
Previously, there was only one search function in MapSense: Find by MAC ID.  This would only search for MAC IDs, and only searched for exact matches.  There was no way to search for node names, configurations, or other properties; or a way to search if the user only knew part of a MAC ID – and as MAC IDs are a lengthy hex number, that was a hefty requirement.  The lack of a full-featured search option consistently featured prominently in user feedback, both from our own internal users as well as customers.

\section{The Solution}
Add a new search option that can search all fields of all nodes in the system, and highlight those nodes on the deployment plan.  This will replace all previously existing search functionality.  It will be available in the Edit menu as Find, and will be accessible via hotkey Control+F.

\subsection{Design}
Philosophically, there are two basic ways to write a search option in a piece of software.  The first is to have a lot of control widgets to control how the search works---the usual “Advanced Search” approach, with controls for case sensitivity, substring versus exact matching, options for regexes, fine grained control over what fields are searched, and other such options.

The second approach is to simply provide the user with a single search box to type their query, let the search engine worry about the details, and just “get it right.”  This is the approach taken by Google and Apple, and the approach taken by this feature, as demonstrated by its name.  The goal is a search tool that is as close to configuration-free as possible.

\subsection{Details}
Spotlight presents the user with a single dialog box with a search field, a pull-down box of field names, a checkbox, a pane of results, and four buttons: Close, Locate on Plan, Export to CSV, and Edit (see \ref{edit}).  The dialog is modeless and appears as it's own window, allowing the rest of the application to be used while the dialog is displayed.

The text entry box allows the user to enter a term to search for.  If no other options are selected, this searches across all components in the deployment plan.  The search is case insensitive and searches for substrings.  If the user enters multiple words separated by spaces, Spotlight does a search on each word, and returns results that have all words present as a substring.\footnote{This is an AND search, not an OR search.}  The words entered are not required to be in the order entered.

Spotlight will either search across all components in the deployment plan, or the current user selection.  The number of components in the current selection is displayed above the top of the results pane, and a checkbox allows the use to choose whether to limit the search to the current selection.  It is unchecked by default.  The dialog will remember the users last setting when the dialog is closed and reopened.

When the Search in Property Name pull-down box is empty---the default---Spotlight does a search across all non-hidden properties of the components to be searched.  The results pane has three columns: Component Name, Component Property, and Value.  When a search is executed, each Component Property that matches the search gets a row in the results pane, listing the name of the Component, the name of the property that contains the matching value, and the value of that property that matches the search term.  Property names are also searched in addition to their values, allowing the user to quickly see all the values of the same property across the deployment plan (MAC ID, for example,) matched with the name of their component.

The pull-down box contains a list of field names, which can be selected to pre-filter the search to just a single property.  When something is selected in this pull-down box, the center column vanishes from the results pane, as it is redundant.  Also, field names will not be searched in this mode. 

When reopened, the search dialog will remember the last search and options.

The search is executed in real time an as a filter, so a blank search box results in a complete list of all components and properties being displayed, which is filtered down as the user types in the search box.  Every character typed results in a new search being executed and the results being repopulated.

The Close button closes the window and the Locate on Plan button highlights and scrolls to the selected components in the result pane.  One or more components may be selected in the results pane.  When a single component is selected in the results pane and the Locate on Plan is clicked, the component is selected on the plan and the plan pans to place that component in the center.  When multiple components are selected in the results pane and the Locate on Plan is clicked, they are all selected, the plan pans to center on the group, and the “camera” zooms to fit that group on the screen.  No zooming is performed when only one is selected, as the user is assumed to be at the correct zoom level already and zooming in one single component zooms in much too far.

One or more results can also be selected and copied to the OS clipboard, and then pasted into another program.  Pasting into Excel will maintain columns and cells.

\subsection{Producers \& Consumers}

In addition to properties, Spotlight also searches Producers and Consumers.  Producer \& Consumer names are listed with the other property names.  The value for these will either be blank (for no associations made) or a list of the names of the components associated with that producer or consumer.  Other than the fact that these values are not present in the properties panel for a given component, Spotlight treats them identically to other properties.


\subsection{DLIDs and Object IDs}\label{ids}

The search results will also list all DLIDs and ES Object IDs for all objects in the project.  Since Spotlight deals with components, and each component can (and frequently does) have more than one object, each component can list more than one of each.

This is done by adding two `virtual' properties to each component, \emph{DLIDs} and \emph{Object IDs}.  Both properties list, as a comma-separated list, all the IDs of that component.  See Appendix \ref{jupiter_dialog} for an example.  The Object IDs property will be blank for any component that has not been exported.  

\subsection{Export to CSV}\label{csv}
The search results can be exported to a CSV file.  Clicking the Export to CSV button opens a file chooser dialog, allowing the user to select a destination file.  When the user clicks OK, the exact results in the table are saved to a CSV file.


\section{Editing}\label{edit}

As of Jupiter, Spotlight now has an edit mode.  Edit mode is entered by clicking the Edit toggle button.  It can be exited by clicking the same button.  The dialog will revert out of edit mode whenever it is closed or reopened.

The search features of the dialog functions the same in Edit mode, except that the value field of the results table is editable where appropriate.  The field is only editable if:

\begin{itemize}
	\item The field is user-editable to begin with
	\item The field is visible to the user
	\item The field does not require a special editor dialog (such as tables)
	\item The field is not a combobox or checkbox
\end{itemize}

Which means that only simple text and number fields can be edited via Spotlight.  Edits are committed immediatly from Spotlight to the data model.  Spotlight has the same validation and undo behavior as the Properties Editor.

\appendix
\section{Changes since Mercury}

Several features have been added or revised since Spotlight shipped in Mercury:

As of Jupiter, there is now an Edit mode (See \ref{edit}).

The results can be exported to a CSV file (See \ref{csv}).

The results include DLIDs and Object IDs (See \ref{ids}).

Finally, originally double-clicking on a result would select that component and close the dialog.  That feature has been removed.

\newpage
\section{Venus Dialog Design}
\begin{figure}[h!]
	\centering
	\includegraphics[width=4.9in]{Venus_Dialog}
	\caption{Original Dialog Box Design}
\end{figure}

\newpage
\section{Jupiter Dialog Design}\label{jupiter_dialog}
\begin{figure}[h!]
	\centering
	\includegraphics[width=4.9in]{Jupiter_Search_Dialog}
	\caption{Revised Dialog Box Design}
\end{figure}


\end{document}