%module jlibusb



%inline %{

	#include <stdint.h>
    #ifdef _WIN64
	typedef __int64         ssize_t;
    #else
    typedef int        ssize_t;
    #endif
	#include "libusb.h"
%}

%include <stdint.i>
%include <arrays_java.i>
%include <typemaps.i>

%apply signed char[] { unsigned char *data};
%apply int *OUTPUT { int *actual_length };
%include "libusb.h"


%include "libusb_helper.h"

