package p3Configuration.ui;

import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import p3Configuration.model.Jawa;
import p3Configuration.model.Server;
import p3Configuration.utility.Variables;

import edu.umd.cs.piccolo.event.PBasicInputEventHandler;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.nodes.PImage;

public class JawaNode extends PImage{
	private boolean isSelected = false;
	private File imgFile = new File("resources/jawa2.png");
	private File selectedImgFile = new File("resources/select_jawa2.png");;
	private Jawa jawa;
	private Server server;
	private ServerNode serverNode;
	
	public JawaNode(Jawa _jawa, ServerNode _serverNode){
		jawa = _jawa;
		server = jawa.getServer();
		serverNode =_serverNode;
		
		
	}
	
	public Jawa getJawa(){
		return jawa;
	}
	
	public void setShape(){
		try {
			this.setImage(ImageIO.read(imgFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int serverLocation = server.getLocation();
		int serverHeight = server.getUheight();
		int imageWidth = Variables.JAWA_IMAGE_WIDTH;
		float imageHeight =serverNode.getImageHeight()/3*2; 
		
		AffineTransform aft = new AffineTransform();
		aft.scale(1, imageHeight/15.0);
		this.setTransform(aft);
		
		System.out.println("jawa scale:" + imageHeight/15.0);
		double x = 0,y = 0.0;
		
		switch(jawa.getIndex()){
			case 1:
				x = -10;
				break;
			case 2:
				x= 250;
				break;
			case 3:
				x = 25;
				break;
			case 4:
				x = 215;
				break;
			case 5:
				x = 60;
				break;
			case 6:
				x = 180;
				break;
			case 7:
				x = 95;
				break;
			case 8:
				x = 145;
				break;
		}
	
		
		//x = Variables.RACK_BOTTOM_X - Variables.JAWA_IMAGE_HSPACE + imageWidth*(jawa.getIndex()-1);
		//y = Variables.RACK_BOTTOM_Y + Variables.JAWA_IMAGE_VSPACE- (serverLocation-1 + serverHeight)*serverNode.getImageHeight();
		
		System.out.println("jawa x:" + x + ":" + this.getOffset().getX());
		System.out.println("jawa y:" + y+ ":" + this.getOffset().getY());
		
		this.setOffset(x, 0.0);
		this.moveToFront();
		
	}
	
	public void setSelected(boolean _selected){
		try {
			if(_selected){
				this.setImage(ImageIO.read(selectedImgFile));
			}else{
				this.setImage(ImageIO.read(imgFile));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//if(_selected)
		//	this.setStrokePaint(Color.YELLOW);
		//else
		//	this.setStrokePaint(Color.black);
		isSelected = _selected;
	}
}
