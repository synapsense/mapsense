package p3Configuration.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;

import p3Configuration.model.Jawa;
import p3Configuration.model.Server;
import p3Configuration.utility.JawaTableModel;

public class JawaDialog extends JDialog implements ActionListener{
	private Server server;
	private JTextField jawaIdTextField;
	private JComboBox jawaFeedComboBox;
	private ArrayList<Jawa> jawaList = new ArrayList<Jawa>();
	private JTable jawaListTable;
	private JawaTableModel jawaTableModel; 
	private Vector jawaVector = new Vector();
	private ConfigurationCanvas canvas;
	
	public JawaDialog(Frame frame, Server _server, ConfigurationCanvas _canvas){
		super(frame, true);
		
		server = _server;
		canvas = _canvas;
		setJawaList();
		//JPanel panel = new JPanel();
		//JLabel label = new JLabel("PlugMeters List");
		
		//panel.add(label);
		
		//this.getContentPane().add(panel, BorderLayout.NORTH);
		JPanel mainPanel = new JPanel();
		mainPanel.setPreferredSize(new Dimension(300,600));
		mainPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		mainPanel.setSize(300, 600);
		
		GridLayout gridLayout = new GridLayout(2,1);
		gridLayout.setHgap(20);
		mainPanel.setLayout(gridLayout);
		
		mainPanel.add(getJawaInputPane());
		mainPanel.add(getTableJawaList());
		
		this.getContentPane().add(mainPanel, BorderLayout.CENTER);
		
		this.setPreferredSize(new Dimension(400, 200));
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.pack();
		this.move(200, 100);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		this.setVisible(false);
		this.dispose();
	}
	
	private JPanel getJawaInputPane(){
		JPanel jawaPanel = new JPanel();
		//jawaPanel.setPreferredSize(new Dimension(400,30));
		
		GridBagLayout gbl= new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		
		jawaPanel.setLayout(gbl);
		jawaPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		//Border border = BorderFactory.createTitledBorder("Enter PlugMeter Info");
		//jawaPanel.setBorder(border);
		
		JLabel jawaConfigLabel = new JLabel("Enter PlugMeter Info");
		jawaConfigLabel.setFont(new Font("sansserif", Font.BOLD, 30));
		
		gbc.weightx = 50;
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.anchor = gbc.CENTER;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth =4;
		gbl.setConstraints(jawaConfigLabel, gbc);
		jawaPanel.add(jawaConfigLabel);
		
		JPanel contentPanel = new JPanel();
		GridLayout gridLayout = new GridLayout(2,2);
		gridLayout.setHgap(20);
		contentPanel.setLayout(gridLayout);
		
		JLabel idLabel = new JLabel("PlugMeter ID");
		idLabel.setFont(new Font("sansserif", Font.BOLD, 20));
		//gbc.gridx = 0;
		//gbc.gridy = 2;
		//gbc.gridwidth =1;
		//gbl.setConstraints(idLabel, gbc);
		//jawaPanel.add(idLabel);
		contentPanel.add(idLabel);
		
		JLabel feedLabel = new JLabel("Feed");
		feedLabel.setFont(new Font("sansserif", Font.BOLD, 20));
		//gbc.gridx = 0;
		//gbc.gridy = 3;
		//gbl.setConstraints(feedLabel, gbc);
		//jawaPanel.add(feedLabel);
		contentPanel.add(feedLabel);
		
		jawaIdTextField = new JTextField();
		jawaIdTextField.setSize(250, 50);
		jawaIdTextField.setText("");
		//gbc.gridx = 1;
		//gbc.gridy = 2;
		//gbl.setConstraints(jawaIdTextField, gbc);
		//jawaPanel.add(jawaIdTextField);
		contentPanel.add(jawaIdTextField);
		
		String feedList[] = {"1", "2", "3", "4", "5", "6", "7", "8"};
		jawaFeedComboBox = new JComboBox(feedList); 
		contentPanel.add(jawaFeedComboBox);
		//gbc.gridx = 1;
		//gbc.gridy = 3;
		//gbl.setConstraints(jawaFeedComboBox, gbc);
		//jawaPanel.add(jawaFeedComboBox);
		
		JPanel buttonPanel = new JPanel();
		FlowLayout flowLayout = new FlowLayout();
		flowLayout.setVgap(1);
		buttonPanel.setLayout(flowLayout);
		
		JButton addButton = new JButton(new ImageIcon("resources/Add.png"));
		addButton.setPreferredSize(new Dimension(80, 60));
		//gbc.gridx = 0;
		//gbc.gridy = 4;
		//gbl.setConstraints(addButton, gbc);
		//jawaPanel.add(addButton);
		buttonPanel.add(addButton);
		
		
		addButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				System.out.println(jawaIdTextField.getText());
				System.out.println(jawaFeedComboBox.getSelectedItem().toString());
				String jid = jawaIdTextField.getText();
				int feed = Integer.parseInt(jawaFeedComboBox.getSelectedItem().toString());
				addJawa(jid, feed);
			}
		});
		
		JButton editButton = new JButton(new ImageIcon("resources/Repair.png"));
		editButton.setPreferredSize(new Dimension(80, 60));
		//gbc.gridx = 1;
		//gbc.gridy = 4;
		//gbc.gridwidth =1;
		//gbc.gridheight = 1;
		//gbl.setConstraints(editButton, gbc);
		//jawaPanel.add(editButton);
		buttonPanel.add(editButton);
		
		editButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				
				//selectedRack.editServer(location, height);
			}
		});
		
		JButton deleteButton = new JButton(new ImageIcon("resources/Delete.png"));
		deleteButton.setPreferredSize(new Dimension(80, 60));
		//gbc.gridx = 2;
		//gbc.gridy = 4;
		//gbl.setConstraints(deleteButton, gbc);
		//jawaPanel.add(deleteButton);
		buttonPanel.add(deleteButton);
		
		deleteButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				//selectedRack.deleteServer();
			}
		});
		

		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth =4;
		gbl.setConstraints(contentPanel, gbc);
		jawaPanel.add(contentPanel);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth =4;
		gbl.setConstraints(buttonPanel, gbc);
		jawaPanel.add(buttonPanel);
		
		return jawaPanel;
		
	}
	
	private JPanel getJawaList(){
		JPanel panel = new JPanel();
		
		GridBagLayout gbl= new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		panel.setLayout(gbl);
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		JLabel idLabel = new JLabel("PlugMeter ID");
		gbl.setConstraints(idLabel, gbc);
		panel.add(idLabel);
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		JLabel feedLabel = new JLabel("Feed");
		gbl.setConstraints(feedLabel, gbc);
		panel.add(feedLabel);
		
		
		for(int i=0; i<jawaList.size(); i++){
			Jawa jawa = jawaList.get(i);
			String jid = jawa.getId();
			int feed = jawa.getFeed();
			
			gbc.gridx = 0;
			gbc.gridy = i+1;
			JTextField idText = new JTextField();
			idText.setText(jid);
			gbl.setConstraints(idText, gbc);
			panel.add(idText);
			
			gbc.gridx = 1;
			gbc.gridy = i+1;
			String feedList[] = {"1", "2", "3", "4", "5", "6", "7", "8"};
			JComboBox feedCombo = new JComboBox(feedList); 
			feedCombo.setSelectedIndex(feed-1);
			gbl.setConstraints(feedCombo, gbc);
			panel.add(feedCombo);
			
			JButton editButton = new JButton(new ImageIcon("resources/16_Repair.png"));
			gbc.gridx = 3;
			gbc.gridy = i+1;
			gbl.setConstraints(editButton, gbc);
			panel.add(editButton);
			
			editButton.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent ev){
					
					//selectedRack.editServer(location, height);
				}
			});
			
			JButton deleteButton = new JButton(new ImageIcon("resources/16_Delete.png"));
			gbc.gridx = 4;
			gbc.gridy = i+1;
			gbl.setConstraints(deleteButton, gbc);
			panel.add(deleteButton);
			
			deleteButton.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent ev){
					//selectedRack.deleteServer();
				}
			});
		}
		
		return panel;
	}
	
	private JPanel getTableJawaList(){
		JPanel panel = new JPanel();
		//panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		GridLayout gridLayout = new GridLayout(2,1);
		gridLayout.setHgap(20);
		panel.setLayout(gridLayout);
		
		JPanel listPanel = new JPanel();
		
//		/listPanel.setPreferredSize(new Dimension(300, 300));
		
		
		JLabel title = new JLabel("PlugMeter List");
		listPanel.add(title, BorderLayout.NORTH);
		
		//jawaTableModel = new JawaTableModel(jawaVector);
		jawaListTable = new JTable(jawaTableModel);
		jawaListTable.setBackground(new Color(239,239,239));
		jawaListTable.setSize(100, 100);
		listPanel.add(jawaListTable, BorderLayout.CENTER);
		
		JPanel buttonPanel = new JPanel();
		FlowLayout flowLayout = new FlowLayout();
		flowLayout.setVgap(1);
		buttonPanel.setLayout(flowLayout);
		
		JButton applyButton = new JButton("Apply");
		buttonPanel.add(applyButton);
		
		
		applyButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				for(int i=0;i<jawaVector.size();i++){
					Jawa jawa = (Jawa) jawaVector.elementAt(i);
					canvas.addJawa(jawa.getId(), jawa.getFeed());
					setDialogVisible(false);
					
				}
			}
		});
		
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				setDialogVisible(false);
				
			}
		});
		buttonPanel.add(cancelButton);
		
		panel.add(listPanel);
		panel.add(buttonPanel);
		return panel;
		
	}
	
	private void addJawa(String id, int feed){
		Jawa jawa = new Jawa(id, feed, server);
		jawaList.add(jawa);
		jawaVector.addElement(jawa);
		jawaTableModel.fireTableChanged(null);
		
	}
	
	private void setJawaList(){
		for(Jawa jawa : server.getJawas()){
			jawaVector.addElement(jawa);
		}
	}
	
	private void setDialogVisible(boolean b){
		this.setVisible(b);
	}
}
