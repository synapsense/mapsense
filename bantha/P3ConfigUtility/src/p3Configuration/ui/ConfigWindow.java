package p3Configuration.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SpinnerNumberModel;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.JTableHeader;

import p3Configuration.model.Configuration;
import p3Configuration.model.Jawa;
import p3Configuration.model.Rack;
import p3Configuration.model.Server;
import p3Configuration.utility.ConfigurationIO;
import p3Configuration.utility.JawaTableModel;
import p3Configuration.utility.Validator;
import p3Configuration.utility.Variables;

import edu.umd.cs.piccolo.PCanvas;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.event.PPanEventHandler;
import edu.umd.cs.piccolo.event.PZoomEventHandler;

public class ConfigWindow extends JFrame{
	
	static final int LEFT = 1;
	static final int CENTER = 2;
	static final int RIGHT = 3;
	
	private Configuration configuration = new Configuration();
	private ConfigurationIO io = new ConfigurationIO();
	
	private ConfigurationCanvas canvas;
	
	//rack configuration UI
	private JRadioButton leftRackButton;
	private JRadioButton centerRackButton;
	private JRadioButton rightRackButton; 
	private JTextField rackUheightTextField;
	
	// server configuration UI
	private JSpinner serverLocationSpinner ;
	private JSpinner serverHeightSpinner;
	
	// jawa configuration UI
	private JTextField jawaIdTextField;
	private JComboBox jawaFeedComboBox;
	
	private Validator validator; 
	private JTable jawaListTable;
	
	private ButtonGroup cabinetButtonGroup;
	
	public ConfigWindow(){
		
		
		initConfigurations();
		
		this.setTitle("PlugMeter Configuration Utility");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		// menubar
		//JMenuBar menuBar = new JMenuBar();
		//this.setJMenuBar(menuBar);
		//JMenu fileMenu = new JMenu("File");
		//menuBar.add(fileMenu);
	
		// add toolbar
		JToolBar toolbar= createToolbar();
		
		JPanel leftPanel = new JPanel();
		GridBagLayout gbl= new GridBagLayout();
		//left.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		leftPanel.setLayout(gbl);
		
		// 1. configure cabinet
		JPanel cabinetPanel = createCabinetConfigUI();
		//cabinetPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		addItem(leftPanel, cabinetPanel, 0, 0, 1, 1,GridBagConstraints.NORTH);
		
		
		// 2. configure server
		JPanel serverPanel = createServerConfigUI();
		//serverPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		addItem(leftPanel, serverPanel, 0, 1, 1, 1,GridBagConstraints.NORTH);
		
		// 3. configure plugmeter
		JPanel jawaPanel = createJawaConfigUI();
		//jawaPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		addItem(leftPanel, jawaPanel, 0, 2, 1, 1,GridBagConstraints.NORTH);
		
		// 4. visualization area
		JPanel rightPanel = new JPanel();
		//rightPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		canvas = new ConfigurationCanvas(configuration, this);
		//canvas.setBorder(BorderFactory.createLineBorder(Color.red));
		//canvas.removeInputEventListener(canvas.getPanEventHandler());
		
		validator = new Validator(this, canvas);
		
		rightPanel.add(canvas);
		
		Box center = new Box(BoxLayout.X_AXIS);
		center = Box.createHorizontalBox();
		center.add(leftPanel);
		center.add(rightPanel);
		
		this.setLayout(new BorderLayout());
		this.add(toolbar, BorderLayout.NORTH);
		this.add(center, BorderLayout.CENTER);
		this.setBounds(50, 50, 900, 900);
		
		centerRackButton.setSelected(true);
		selectRack(CENTER);
	}
	/**
	 * Create Cabinet Selection panel
	 */
	private JPanel createCabinetConfigUI(){
		 
		JPanel cabinetPanel = new JPanel();
		//cabinetPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		//cabinetPanel.setPreferredSize(new Dimension(100,150));
	
		GridBagLayout gbl= new GridBagLayout();
		cabinetPanel.setLayout(gbl);
		
		
		JLabel cabinetConfigLabel = new JLabel("Select Cabinet");
		cabinetConfigLabel.setFont(new Font("sansserif", Font.BOLD, 30));
		cabinetPanel.add(cabinetConfigLabel);
		addItem(cabinetPanel, cabinetConfigLabel, 0, 0, 1, 1,GridBagConstraints.CENTER);
		
		ButtonGroup cabinetButtonGroup = new ButtonGroup();

		JPanel controllerPanel = new JPanel();
		//usizePanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		GridLayout gridLayout = new GridLayout(3,1);
		gridLayout.setVgap(10);
		controllerPanel.setLayout(gridLayout);
		
		leftRackButton = new JRadioButton("Non Controller Left");
		leftRackButton.setFont(new Font("sansserif", Font.BOLD, 20));
		//leftRackButton.setForeground(Color.gray);
		controllerPanel.add(leftRackButton);
		cabinetButtonGroup.add(leftRackButton);
		
		leftRackButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				System.out.println("left rack");
				selectRack(LEFT);

			}
		});
		
		centerRackButton = new JRadioButton("Controller");
		controllerPanel.add(centerRackButton);
		//centerRackButton.setForeground(Color.gray);
		centerRackButton.setFont(new Font("sansserif", Font.BOLD, 20));
		cabinetButtonGroup.add(centerRackButton);
		
		centerRackButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				selectRack(CENTER);
			}
		});
		

		rightRackButton = new JRadioButton("Non Controller Right");
		rightRackButton.setFont(new Font("sansserif", Font.BOLD, 20));
		//rightRackButton.setForeground(Color.gray);
		controllerPanel.add(rightRackButton);
		cabinetButtonGroup.add(rightRackButton);
		
		rightRackButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				System.out.println("right rack");
				selectRack(RIGHT);
			}
		});
		
		addItem(cabinetPanel, controllerPanel, 0, 1, 1, 1,GridBagConstraints.CENTER);
		
		JPanel usizePanel = new JPanel();
		//usizePanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		GridLayout gridLayout2 = new GridLayout(1,3);
		gridLayout2.setHgap(10);
		usizePanel.setLayout(gridLayout2);
		
		JLabel uheightLabel = new JLabel("U Height");
		uheightLabel.setFont(new Font("sansserif", Font.BOLD, 20));
		usizePanel.add(uheightLabel);
		
		rackUheightTextField = new JTextField("");
		usizePanel.add(rackUheightTextField);

		JButton rackUheightButton = new JButton("Apply");
		//rackUheightButton.setPreferredSize(new Dimension(80,60));
		usizePanel.add(rackUheightButton);
			
		addItem(cabinetPanel, usizePanel, 0, 2, 1, 1,GridBagConstraints.CENTER);
		
		
		rackUheightButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				if(validator.checkRackSelection()){
					canvas.setRackUHeight(Integer.parseInt(rackUheightTextField.getText()));
				}
			}
		});
		return cabinetPanel;
	}
	
	private void addItem(JPanel p, JComponent c, int x, int y, int width, int height, int align) {
		GridBagConstraints gc = new GridBagConstraints();
		gc.gridx = x;
		gc.gridy = y;
		gc.gridwidth = width;
		gc.gridheight = height;
		gc.weightx = 10.0;
		gc.weighty = 10.0;
		gc.insets = new Insets(1, 1, 5, 5);
		gc.anchor = align;
		gc.fill = GridBagConstraints.NONE;
		p.add(c, gc);
	}
	
	/*
	 * UI for creating a server panel
	 */
	private JPanel createServerConfigUI(){
		GridBagLayout gbl= new GridBagLayout();
		
		JPanel serverPanel = new JPanel();
		//serverPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		
		serverPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		serverPanel.setLayout(gbl);
		
		JLabel serverConfigLabel = new JLabel("Enter Server Info");
		serverConfigLabel.setFont(new Font("sansserif", Font.BOLD, 30));
		
		addItem(serverPanel, serverConfigLabel, 0, 0, 1, 1,GridBagConstraints.CENTER);
		
		JPanel contentPanel = new JPanel();
		GridLayout gridLayout = new GridLayout(2,2);
		gridLayout.setHgap(20);
		contentPanel.setLayout(gridLayout);
		
		// create u location selection
		JLabel locationLabel = new JLabel("U Location");
		locationLabel.setFont(new Font("sansserif", Font.BOLD, 20));
		//gbc.gridwidth = 1;
		//gbc.gridx = 1;
		//gbc.gridy = 1;
		//gbl.setConstraints(locationLabel, gbc);
		//serverPanel.add(locationLabel);
		contentPanel.add(locationLabel);
		
		// create u height selection
		JLabel heightLabel = new JLabel("U Height");
		heightLabel.setFont(new Font("sansserif", Font.BOLD, 20));
		//gbc.gridx = 2;
		//gbc.gridy = 1;
		//gbl.setConstraints(heightLabel, gbc);
		//serverPanel.add(heightLabel);
		contentPanel.add(heightLabel);
		
		SpinnerNumberModel smodel = new SpinnerNumberModel(1, 1, 51,1);
		serverLocationSpinner = new JSpinner(smodel);
		serverLocationSpinner.setSize(100, 100);
		//gbc.gridx = 1;
		//gbc.gridy = 2;
		//gbl.setConstraints(serverLocationSpinner, gbc);
		//serverPanel.add(serverLocationSpinner);
		contentPanel.add(serverLocationSpinner);
		
		SpinnerNumberModel smodel2 = new SpinnerNumberModel(1, 1, 10,1);
		serverHeightSpinner = new JSpinner(smodel2);
		serverHeightSpinner.setSize(100, 100);
		//gbc.gridx = 2;
		//gbc.gridy = 2;
		//gbl.setConstraints(serverHeightSpinner, gbc);
		//serverPanel.add(serverHeightSpinner);
		contentPanel.add(serverHeightSpinner);
		
		JPanel buttonPanel = new JPanel();
		FlowLayout flowLayout = new FlowLayout();
		flowLayout.setVgap(1);
		buttonPanel.setLayout(flowLayout);
		
		//JButton addButton = new JButton(new ImageIcon("resources/Add.png"));
		JButton addButton = new JButton(new ImageIcon("resources/Add.png"));
		addButton.setPreferredSize(new Dimension(80, 60));
		//addButton.setBorderPainted(false);
		//addButton.setBackground(new Color(239,239,239));
		//gbc.gridx =1;
		//gbc.gridy = 3;
		//gbl.setConstraints(addButton, gbc);
		//serverPanel.add(addButton);
		buttonPanel.add(addButton);
		
		addButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				System.out.println(serverLocationSpinner.getValue());
				System.out.println(serverHeightSpinner.getValue());
				int location = ((Number)(serverLocationSpinner.getValue())).intValue();
				int height = ((Number)(serverHeightSpinner.getValue())).intValue();
				
				canvas.addServer(location, height);
			}
		});
		
		JButton editButton = new JButton(new ImageIcon("resources/Repair.png"));
		editButton.setPreferredSize(new Dimension(80, 60));
		//gbc.gridx = 2;
		//gbc.gridy = 3;
		//gbl.setConstraints(editButton, gbc);
		//serverPanel.add(editButton);
		//editButton.setBorderPainted(false);
		//editButton.setBackground(new Color(239,239,239));
		buttonPanel.add(editButton);
		
		editButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				if(validator.checkServerSelection()){
					System.out.println(serverLocationSpinner.getValue());
					System.out.println(serverHeightSpinner.getValue());
					int location = ((Number)(serverLocationSpinner.getValue())).intValue();
					int height = ((Number)(serverHeightSpinner.getValue())).intValue();
					
					canvas.editServer(location, height);
					
				}
			}
		});
		
		JButton deleteButton = new JButton(new ImageIcon("resources/Delete.png"));
		deleteButton.setPreferredSize(new Dimension(80, 60));
		//gbc.gridx = 3;
		//gbc.gridy = 3;
		//gbl.setConstraints(deleteButton, gbc);
		//serverPanel.add(deleteButton);
		//deleteButton.setBorderPainted(false);
		//deleteButton.setBackground(new Color(239,239,239));
		buttonPanel.add(deleteButton);
		
		deleteButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				if(validator.checkServerSelection()){
					canvas.deleteServer();
				}
			}
		});
		
		JButton skipButton = new JButton(new ImageIcon("resources/Fast-forward.png"));
		skipButton.setPreferredSize(new Dimension(80, 60));
		//gbc.gridx = 3;
		//gbc.gridy = 3;
		//gbl.setConstraints(deleteButton, gbc);
		//serverPanel.add(deleteButton);
		//deleteButton.setBorderPainted(false);
		//deleteButton.setBackground(new Color(239,239,239));
		buttonPanel.add(skipButton);
		
		skipButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				int location = ((Number)(serverLocationSpinner.getValue())).intValue();
				int height = ((Number)(serverHeightSpinner.getValue())).intValue();
				serverLocationSpinner.setValue(location+height);
				serverHeightSpinner.setValue(1);
			}
		});
		
		addItem(serverPanel, contentPanel, 0, 1, 1, 1,GridBagConstraints.CENTER);
		addItem(serverPanel, buttonPanel, 0, 2, 1, 1,GridBagConstraints.CENTER);
		
		return serverPanel;
	}
	/*
	 * UI for creating jawa config panel
	 */
	private JPanel createJawaConfigUI(){
		JPanel jawaPanel = new JPanel();
		//jawaPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		GridBagLayout gbl= new GridBagLayout();
		
		jawaPanel.setLayout(gbl);
		jawaPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		
		JLabel jawaConfigLabel = new JLabel("Enter PlugMeter Info");
		jawaConfigLabel.setFont(new Font("sansserif", Font.BOLD, 30));
		addItem(jawaPanel, jawaConfigLabel, 0, 0, 1, 1,GridBagConstraints.CENTER);
		
		JPanel contentPanel = new JPanel();
		GridLayout gridLayout = new GridLayout(2,2);
		gridLayout.setHgap(20);
		contentPanel.setLayout(gridLayout);
		
		JLabel idLabel = new JLabel("PlugMeter ID");
		idLabel.setFont(new Font("sansserif", Font.BOLD, 20));
		//gbc.gridx = 0;
		//gbc.gridy = 2;
		//gbc.gridwidth =1;
		//gbl.setConstraints(idLabel, gbc);
		//jawaPanel.add(idLabel);
		contentPanel.add(idLabel);
		
		JLabel feedLabel = new JLabel("Feed");
		feedLabel.setFont(new Font("sansserif", Font.BOLD, 20));
		//gbc.gridx = 0;
		//gbc.gridy = 3;
		//gbl.setConstraints(feedLabel, gbc);
		//jawaPanel.add(feedLabel);
		contentPanel.add(feedLabel);
		
		jawaIdTextField = new JTextField();
		jawaIdTextField.setSize(250, 50);
		jawaIdTextField.setText("");
		//gbc.gridx = 1;
		//gbc.gridy = 2;
		//gbl.setConstraints(jawaIdTextField, gbc);
		//jawaPanel.add(jawaIdTextField);
		contentPanel.add(jawaIdTextField);
		
		String feedList[] = {"1", "2", "3", "4", "5", "6", "7", "8"};
		jawaFeedComboBox = new JComboBox(feedList); 
		contentPanel.add(jawaFeedComboBox);
		//gbc.gridx = 1;
		//gbc.gridy = 3;
		//gbl.setConstraints(jawaFeedComboBox, gbc);
		//jawaPanel.add(jawaFeedComboBox);
		
		JPanel buttonPanel = new JPanel();
		FlowLayout flowLayout = new FlowLayout();
		flowLayout.setVgap(1);
		buttonPanel.setLayout(flowLayout);
		
		JButton addButton = new JButton(new ImageIcon("resources/Add.png"));
		addButton.setPreferredSize(new Dimension(80, 60));
		//gbc.gridx = 0;
		//gbc.gridy = 4;
		//gbl.setConstraints(addButton, gbc);
		//jawaPanel.add(addButton);
		buttonPanel.add(addButton);
		
		
		addButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				if(validator.checkServerSelection() && validator.checkJawaID()){
					System.out.println(jawaIdTextField.getText());
					System.out.println(jawaFeedComboBox.getSelectedItem().toString());
					String jid = jawaIdTextField.getText();
					int feed = Integer.parseInt(jawaFeedComboBox.getSelectedItem().toString());
					canvas.addJawa(jid, feed);
					
				}
			}
		});
		
		JButton editButton = new JButton(new ImageIcon("resources/Repair.png"));
		editButton.setPreferredSize(new Dimension(80, 60));
		//gbc.gridx = 1;
		//gbc.gridy = 4;
		//gbc.gridwidth =1;
		//gbc.gridheight = 1;
		//gbl.setConstraints(editButton, gbc);
		//jawaPanel.add(editButton);
		buttonPanel.add(editButton);
		
		editButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				if(validator.checkJawaSelection()){
					String jid = jawaIdTextField.getText();
					int feed = Integer.parseInt(jawaFeedComboBox.getSelectedItem().toString());
					canvas.editJawa(jid, feed);
				}
			}
		});
		
		JButton deleteButton = new JButton(new ImageIcon("resources/Delete.png"));
		deleteButton.setPreferredSize(new Dimension(80, 60));
		//gbc.gridx = 2;
		//gbc.gridy = 4;
		//gbl.setConstraints(deleteButton, gbc);
		//jawaPanel.add(deleteButton);
		buttonPanel.add(deleteButton);
		
		deleteButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				//selectedRack.deleteServer();
				if(validator.checkJawaSelection()){
					canvas.deleteJawa();
				}
			}
		});
		
		addItem(jawaPanel, contentPanel, 0, 1, 1, 1,GridBagConstraints.CENTER);
		addItem(jawaPanel, buttonPanel, 0, 2, 1, 1,GridBagConstraints.CENTER);
		
		JPanel tablePanel = new JPanel();
		GridBagLayout gbl2= new GridBagLayout();
		tablePanel.setLayout(gbl2);
		
		JLabel listLabel = new JLabel("PlugMeter List");
		listLabel.setFont(new Font("sansserif", Font.BOLD, 20));
		jawaListTable = new JTable();
		
		addItem(tablePanel, listLabel, 0, 0, 1, 1,GridBagConstraints.WEST);
		addItem(tablePanel, jawaListTable.getTableHeader(), 0, 1, 1, 1,GridBagConstraints.WEST);
		addItem(tablePanel, jawaListTable, 0, 2, 1, 1,GridBagConstraints.WEST);
		
		addItem(jawaPanel, tablePanel, 0, 3, 1, 1,GridBagConstraints.WEST);
		
		return jawaPanel;
	}
	
	/*
	 * create toolbar
	 */
	private JToolBar createToolbar(){
		// toolbar
		JToolBar toolbar = new JToolBar();
		JButton uploadButton = new JButton(new ImageIcon("resources/Upload.png"));
		toolbar.add(uploadButton);

		uploadButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				
				sendConfigurations();
				
			}
			
		});
		
		JButton saveButton = new JButton(new ImageIcon("resources/Save.png"));
		toolbar.add(saveButton);
		
		saveButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				saveTemplate();
			}
		});
		
		JButton importButton = new JButton(new ImageIcon("resources/Download.png"));
		toolbar.add(importButton);
		
		return toolbar;
	}
	/*
	 * Initialize rack configuration
	 */
	private void initConfigurations(){
		configuration = io.downloadConfiguration();
		
		if(configuration ==null){
			configuration = new Configuration();
			
			Rack leftRack = new Rack(LEFT);
			Rack centerRack = new Rack(CENTER);
			Rack rightRack = new Rack(RIGHT);
			
			configuration.addRack(leftRack);
			configuration.addRack(centerRack);
			configuration.addRack(rightRack);
		}else{
			if(configuration.getRackByType(LEFT)==null){
				configuration.addRack(new Rack(LEFT));
			}
			
			if(configuration.getRackByType(CENTER)==null){
				configuration.addRack(new Rack(CENTER));
			}
			
			if(configuration.getRackByType(RIGHT)==null){
				configuration.addRack(new Rack(RIGHT));
			}
		}
		
				
	}

	/*
	 *  set selected Rack
	 */
	public void selectRack(int _selected){
		switch(_selected){
			case LEFT:
				leftRackButton.setBorder(BorderFactory.createLineBorder(Color.red, 5));
				centerRackButton.setBorder(BorderFactory.createLineBorder(Color.black));
				rightRackButton.setBorder(BorderFactory.createLineBorder(Color.black));
				break;
			case CENTER:
				leftRackButton.setBorder(BorderFactory.createLineBorder(Color.black));
				centerRackButton.setBorder(BorderFactory.createLineBorder(Color.red, 5));
				rightRackButton.setBorder(BorderFactory.createLineBorder(Color.black));

				break;
			case RIGHT:
				leftRackButton.setBorder(BorderFactory.createLineBorder(Color.black));
				centerRackButton.setBorder(BorderFactory.createLineBorder(Color.black));
				rightRackButton.setBorder(BorderFactory.createLineBorder(Color.red, 5));

				break;
		}
		
		canvas.selectRackNode(_selected);
		
	}
	
	
	/*
	 * select a server
	 */
	public void selectServer(Server _server){
		
		serverLocationSpinner.setValue(_server.getLocation());
		serverHeightSpinner.setValue(_server.getUheight());
		
		
		
	}
	
	/*
	 * select a plugmeter (jawa)
	 */
	public void selectJawa(Jawa _jawa){
		jawaIdTextField.setText(_jawa.getId());
		jawaFeedComboBox.setSelectedIndex(_jawa.getFeed()-1);
	}
	
	/*
	 * init a plugmeter input fields
	 */
	public void initJawa(){
		jawaIdTextField.setText("");
		jawaFeedComboBox.setSelectedIndex(0);
	}
	
	/*
	 *  get JTable for jawa list
	 */
	public JTable getJawaListTable(){
		return jawaListTable;
	}
	
	/*
	 * set next server
	 */
	public void setNextServer(int nextLocation){
		
		serverLocationSpinner.setValue(nextLocation);
		serverHeightSpinner.setValue(1);
	}
	
	/*
	 * set selected rack U height
	 */
	public void setRackUheight(Rack _rack){
		System.out.println(_rack.getUheight());
		rackUheightTextField.setText(Integer.toString(_rack.getUheight()));
	}
	
	/*
	 * save configuration 
	 */
	private void sendConfigurations(){
		io.uploadConfiguration(configuration);
		
	}
	
	/*
	 * save configuration template
	 */
	private void saveTemplate(){
		JFileChooser fileChooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("P3 configuration template", "p3");
		fileChooser.setFileFilter(filter);
		File f;
		try {
			f = new File(new File("plugMeterConfig.p3").getCanonicalPath());
			fileChooser.setSelectedFile(f);
			
			int result = fileChooser.showSaveDialog(this);
			
			if(result == JFileChooser.APPROVE_OPTION){
				File templateFile = fileChooser.getSelectedFile();
				String file_path = templateFile.getAbsolutePath();
				
				System.out.println(file_path);
				System.out.println(templateFile.getName());
				
				io.writeTemplate(file_path, configuration);
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public ButtonGroup getCabinetButtonGroup(){
		return cabinetButtonGroup;
	}
	
	public JTextField getJawaIdTextField(){
		return jawaIdTextField;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ConfigWindow window = new ConfigWindow();
		window.setBounds(0,0,1000,1000);
		window.setVisible(true);
		
		
		
	}

	
}
