package p3Configuration.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ValidatorDialog extends JDialog implements ActionListener{

	static final int CONFIRM = 1;
	static final int WARNING = 2;
	static final int ERROR = 3;
	
	String title;
	String message;
	int type;
	
	JPanel messagePanel = new JPanel();
	JPanel buttonPanel = new JPanel();
	
	public ValidatorDialog(JFrame _parent, String _title, String _message, int _type ){
		super(_parent, _title, true);
		
		title = _title;
		message = _message;
		type = _type;
		
		setMessagePanel();
		setButtonPanel();
		
		this.getContentPane().add(messagePanel, BorderLayout.CENTER);
		this.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		pack();
		setVisible(true);
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 * set message panel
	 */
	
	private void setMessagePanel(){
		messagePanel.setPreferredSize(new Dimension(300,100));
		JLabel messageLabel = new JLabel(message);
		messagePanel.add(messageLabel);
	}
	
	private void setButtonPanel(){
		FlowLayout flowlayout = new FlowLayout();
		buttonPanel.setLayout(flowlayout);
		
		if(type==1){
			
			JButton yesButton = new JButton("Yes");
			JButton noButton = new JButton("No");
			
			buttonPanel.add(yesButton);
			buttonPanel.add(noButton);
			
		}else{
			
			JButton okButton = new JButton("OK");
			buttonPanel.add(okButton);
			
			okButton.addActionListener(new ActionListener(){

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					setVisible(false);
				}
				
			});
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		setVisible(false);
		dispose();
	}

}
