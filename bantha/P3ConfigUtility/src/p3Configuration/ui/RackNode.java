package p3Configuration.ui;

import java.awt.Cursor;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import javax.imageio.ImageIO;

import p3Configuration.model.Jawa;
import p3Configuration.model.Rack;
import p3Configuration.model.Server;

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.event.PBasicInputEventHandler;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.event.PPanEventHandler;
import edu.umd.cs.piccolo.nodes.PImage;

public class RackNode extends PNode{
	private Rack rack;
	private PImage rackImage;
	private PNode serverLayer = new PNode();
	
	public RackNode(Rack _rack){
		rack = _rack;
		setImage();
		this.addChild(rackImage);
		this.addChild(serverLayer);
		
	}
	
	public void setImage(){
		File imgFile = new File("resources/wide_rack_2.png");
		
		try {
			rackImage = new PImage(ImageIO.read(imgFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rackImage.setOffset(20, 0);
		rackImage.moveToBack();
		rackImage.setPickable(false);
		
		rackImage.addInputEventListener(new PBasicInputEventHandler(){
			public void mouseClicked(PInputEvent aEvent) {
				System.out.println("rack mouseClicked");
				
				aEvent.setHandled(false);
			}
			public void mousePressed(PInputEvent event){
				System.out.println("rack mousePResse");
				event.setHandled(false);
			}
		});
		
	}

	public Rack getRack(){
		return rack;
	}
	
	public void setRack(Rack _rack){
		this.rack = _rack;
	}
	
	public ServerNode addServerNode(Server server){
		ServerNode serverNode = new ServerNode(server);
		serverLayer.addChild(serverNode);
		serverNode.setShape();
		return serverNode;
	}
	
	public void removeServerNode(ServerNode serverNode){
		rack.deleteServer(serverNode.getServer());
		serverLayer.removeChild(serverNode);
	}
	
	public void repaintChildren(){
		System.out.println("Serverlayer children:" +serverLayer.getChildrenCount() );
		
		Iterator childIterator = serverLayer.getChildrenIterator();
		ArrayList<ServerNode> list = new ArrayList<ServerNode>();
		
		while(childIterator.hasNext()){
			Object node = childIterator.next();
			list.add((ServerNode) node);
		
		}
		
		for(ServerNode serverNode:list){
			serverNode.setShape();
			serverNode.repaintChildren();
			
		}
	}

	
	
	
	
}
