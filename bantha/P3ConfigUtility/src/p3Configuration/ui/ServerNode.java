package p3Configuration.ui;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.imageio.ImageIO;

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.event.PBasicInputEventHandler;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.nodes.PImage;
import edu.umd.cs.piccolo.nodes.PPath;
import edu.umd.cs.piccolo.nodes.PText;

import p3Configuration.model.Jawa;
import p3Configuration.model.Rack;
import p3Configuration.model.Server;
import p3Configuration.utility.Variables;

public class ServerNode extends PPath {
	private Server server;
	private float imageHeight = 30;
	private int imageWidth ; 
	private PNode jawaLayer = new PNode();
	private PImage serverImage = new PImage();
	private PText locationText = new PText();
	
	public ServerNode(Server _server){
		server = _server;
		imageWidth = Variables.SERVER_IMAGE_WIDTH;
		this.setPickable(true);
		this.setChildrenPickable(true);
		//this.addChild(serverImage);
		this.addChild(jawaLayer);
		this.addChild(locationText);
		
		jawaLayer.moveToFront();
		setImage();
		setUlocationText();
		
	}
	
	public void setImage(){
		File imgFile = new File("resources/server_1.png");
		imageHeight = getImageHeight();
		
		try {
			serverImage.setImage(ImageIO.read(imgFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//serverImage.setOffset(20, 30);
		serverImage.moveToBack();
		serverImage.setPickable(false);
		serverImage.setOffset(Variables.RACK_BOTTOM_X, Variables.RACK_BOTTOM_Y - (server.getLocation()-1+server.getUheight())*(imageHeight+1));
		
		
	}

	public void setShape(){
		imageHeight = getImageHeight();
		
		this.setPathToRectangle(0, 0, imageWidth, imageHeight * server.getUheight());
		this.setPaint(Color.GRAY);
		this.setStrokePaint(Color.black);
		
		double x =0, y =0;
		x = Variables.RACK_BOTTOM_X;
		y = Variables.RACK_BOTTOM_Y - (server.getLocation()-1+server.getUheight())*(imageHeight+1);
		
		System.out.println("server x:" + x + "	y:" + y + "	server location:" + server.getLocation());
		
		this.setOffset(x, y);
		this.moveToFront();
	}

	public void setUlocationText(){
		locationText.setText(Integer.toString(server.getLocation()));
		locationText.setFont(new Font("sansserif", Font.BOLD, 15));
		locationText.setTextPaint(Color.BLUE);
		locationText.setOffset(120,0);
	}
	public void setSelected(boolean _selected){
		if(_selected){
			this.setStrokePaint(Color.YELLOW);
		}
		else{
			this.setStrokePaint(Color.black);
		}
	}
	
	public float getImageHeight(){
		return (float)Variables.RACK_IMAGE_HEIGHT/(float)(server.getRack().getUheight());
	}
	
	public Server getServer(){
		return server;
	}
	
	public void addJawaNode(Jawa jawa){
		JawaNode jawaNode = new JawaNode(jawa, this);
		jawaNode.setShape();
		jawaNode.moveToFront();
		jawaLayer.addChild(jawaNode);
	}
	
	public void removeJawaNode(JawaNode jawaNode){
		server.deleteJawa(jawaNode.getJawa());
		jawaLayer.removeChild(jawaNode);
	}
	
	public void repaintChildren(){
		Iterator childIterator = jawaLayer.getChildrenIterator();
		ArrayList<JawaNode> list = new ArrayList<JawaNode>();
		
		while(childIterator.hasNext()){
			Object node = childIterator.next();
			list.add((JawaNode) node);
		
		}
		
		for(JawaNode jawaNode:list){
			jawaNode.setShape();
		}
		
	}

	
}
