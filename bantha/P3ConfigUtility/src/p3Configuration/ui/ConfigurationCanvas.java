package p3Configuration.ui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.table.JTableHeader;

import p3Configuration.model.Configuration;
import p3Configuration.model.Jawa;
import p3Configuration.model.Rack;
import p3Configuration.model.Server;
import p3Configuration.utility.JawaTableModel;
import p3Configuration.utility.Variables;
import edu.umd.cs.piccolo.PCanvas;
import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.event.PBasicInputEventHandler;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.event.PPanEventHandler;
import edu.umd.cs.piccolo.event.PZoomEventHandler;
import edu.umd.cs.piccolo.nodes.PImage;

public class ConfigurationCanvas extends PCanvas{
	
	private PNode rackLayer;
	
	private Configuration configuration;
	
	private RackNode leftRackNode;
	private RackNode centerRackNode;
	private RackNode rightRackNode;
	
	private RackNode selectedRackNode;
	
	private ServerNode selectedServerNode;
	private JawaNode selectedJawaNode;
	private ConfigWindow configWindow;
	
	private JawaTableModel jawaTableModel;
	private JTable jawaListTable;
	
	public ConfigurationCanvas(Configuration _configuration , ConfigWindow _configWindow){
		configuration = _configuration;
		configWindow = _configWindow;
		
		jawaListTable = configWindow.getJawaListTable();
		jawaTableModel = new JawaTableModel();
		
		
		rackLayer = new PNode();
		rackLayer.setChildrenPickable(true);
		
		this.getLayer().addChild(rackLayer);
		this.getLayer().setChildrenPickable(true);
	
		populateConfiguration();
		
		
		setBounds(500, 100, 500, 1000);
		setPreferredSize(new Dimension(500,1000));
		setBackground(new Color(239,239,239));
		
		setZoomEventHandler(new PZoomEventHandler(){
			public void mouseWheelRotated(PInputEvent event){
				System.out.println("mousWheelRotated:" + event.getWheelRotation());
				
				double scale = getCamera().getScale();
				double newScale = 0.0;
				if(event.getWheelRotation() <0){
					newScale = scale + 0.1;
					
				}else{
					newScale = scale - 0.1;
									
				}
				
				if(newScale < 0.5){
					newScale = 0.5;
				}
				//canvas.getCamera().scaleViewAboutPoint(newScale,selectedRackConfig.getFullBounds().width/2,selectedRackConfig.getFullBounds().height/2);
				getCamera().scaleViewAboutPoint(newScale, event.getPosition().getX(), event.getPosition().getY());

			}
		} );
		
		setPanEventHandler(new PPanEventHandler(){
			public void mouseDragged(PInputEvent event){
				if(event.isLeftMouseButton()){
					Cursor cursor = new Cursor(Cursor.MOVE_CURSOR);
					pushCursor(cursor);
					this.pan(event);
					event.setHandled(true);
				}
			}
			
			public void mouseReleased(PInputEvent event){
				pushCursor(null);
				if(this.getDragActivity()!=null){
					this.stopDragActivity(event);
				}
			}
		});
		
		this.addInputEventListener(new PBasicInputEventHandler(){
			public void mouseClicked(PInputEvent aEvent) {
				aEvent.setHandled(true);
			}
			public void mousePressed(PInputEvent event){
				String objectType = event.getPickedNode().getClass().getSimpleName();
				
				if(objectType.equals("ServerNode")){
					selectServer(event);
					System.out.println("server selected");
				}
				else if(objectType.equals("JawaNode")){
					selectJawa(event);
				}else{
					System.out.println("unknown object type");
				}
			}
		});

	}
	
	public void populateConfiguration(){
		
		Rack leftRack = configuration.getRackByType(Variables.LEFT_RACK_TYPE);
		Rack centerRack = configuration.getRackByType(Variables.CENTER_RACK_TYPE); 
		Rack rightRack = configuration.getRackByType(Variables.RIGHT_RACK_TYPE);
		
		leftRackNode = new RackNode(leftRack);
		centerRackNode = new RackNode(centerRack);
		rightRackNode = new RackNode(rightRack);
		
		rackLayer.addChild(leftRackNode);
		rackLayer.addChild(centerRackNode);
		rackLayer.addChild(rightRackNode);

		leftRackNode.setVisible(false);
		centerRackNode.setVisible(false);
		rightRackNode.setVisible(false);
		
		ArrayList<RackNode> rackNodeList = new ArrayList<RackNode>();
		rackNodeList.add(leftRackNode);
		rackNodeList.add(centerRackNode);
		rackNodeList.add(rightRackNode);
	
		
		for(RackNode rackNode: rackNodeList){
			if(rackNode!=null && rackNode.getRack()!=null){
				for(Server server:rackNode.getRack().getServers()){
					ServerNode serverNode = rackNode.addServerNode(server);
					
					for(Jawa jawa:server.getJawas()){
						serverNode.addJawaNode(jawa);
					}
				}
				
			}
		}
	}
	
	
	public void selectRackNode(int rackType){
		if(rackType==1)
			selectedRackNode = leftRackNode;
		else if(rackType==2)
			selectedRackNode = centerRackNode;
		else
			selectedRackNode = rightRackNode;
		
		selectedRackNode.setVisible(true);
		selectedRackNode.moveToFront();
		
		System.out.println("selectedrack"+selectedRackNode.getRack().getType());
		configWindow.setRackUheight(selectedRackNode.getRack());
		
		// initialize setting for new selected rack 
		setNextServer();
		configWindow.initJawa();
		
		if(selectedServerNode!=null ){
			selectedServerNode.setSelected(false);
			selectedServerNode = null;
		}
		
		if(selectedJawaNode !=null){
			selectedJawaNode.setSelected(false);
			selectedJawaNode = null;
		}
		
		jawaTableModel.setTableModel(selectedRackNode.getRack());
		jawaListTable.setModel(jawaTableModel);
		jawaTableModel.fireTableChanged(null);
		
	}
	
	public void setRackUHeight(int uheight){
		selectedRackNode.getRack().setUheight(uheight);
		selectedRackNode.repaintChildren();
	}
	
	/*
	 * Add server to selected rack
	 */
	public void addServer(int _location, int _uheight){
		
		Server server = new Server(_location, _uheight, selectedRackNode.getRack());
		selectedRackNode.getRack().addServer(server);
		
		ServerNode serverNode = selectedRackNode.addServerNode(server);
		
		setNextServer();
	}
	
	/* 
	 * Edit a selected server
	 */
	public void editServer(int _location, int _uheight){
		if(selectedServerNode!=null){
			selectedServerNode.getServer().setLocation(_location);
			selectedServerNode.getServer().setHeight(_uheight);		
			selectedServerNode.setShape();
			
			setNextServer();
		}
	}
	
	/*
	 * Delete a selected server
	 */
	public void deleteServer(){
		if(selectedServerNode!=null){
			//serverLayer.removeChild(selectedServerNode);
			selectedRackNode.removeServerNode(selectedServerNode);
			setNextServer();
		}
	}
	/*
	 * select a server
	 */
	public void selectServer(PInputEvent event){
		PNode selectedNode = event.getPickedNode();
		
		if(selectedServerNode!=null)
			selectedServerNode.setSelected(false);
		
		selectedServerNode = (ServerNode) selectedNode;
		selectedServerNode.setSelected(true);
		configWindow.selectServer(selectedServerNode.getServer());
	}

	public void setNextServer(){
		int nextLocation = selectedRackNode.getRack().getNextULocation();
		configWindow.setNextServer(nextLocation);
	}
	
	public ServerNode getSelectedServerNode(){
		return selectedServerNode;
	}
	
	/* 
	 * Select a jawa
	 */
	public void selectJawa(PInputEvent event){
		PNode selectedNode = event.getPickedNode();
		
		if(selectedJawaNode != null){
			selectedJawaNode.setSelected(false);
		}
		
		selectedJawaNode = (JawaNode)selectedNode;
		selectedJawaNode.setSelected(true);
		configWindow.selectJawa(selectedJawaNode.getJawa());
	}
	
	/*
	 *	Add a jawa 
	 */
	
	public void addJawa(String _jid, int _feed){
		if(selectedServerNode == null){
			System.out.println("Please select a server to add a plugmeter");
			return;
		}
		
		Jawa jawa = new Jawa(_jid, _feed, selectedServerNode.getServer());
		selectedServerNode.getServer().addJawa(jawa);
		selectedServerNode.addJawaNode(jawa);
		
		jawaTableModel.addJawaData(jawa);
		jawaTableModel.fireTableChanged(null);
	}
	
	public void editJawa(String _jid, int _feed){
		Jawa jawa = selectedJawaNode.getJawa();
		jawa.setId(_jid);
		jawa.setFeed(_feed);
		jawaTableModel.editJawa(jawa);
		jawaTableModel.fireTableChanged(null);
	}
	
	public void deleteJawa(){
		jawaTableModel.removeJawa(selectedJawaNode.getJawa());
		jawaTableModel.fireTableChanged(null);
		
		ServerNode parentServer = (ServerNode) selectedJawaNode.getParent().getParent();
		parentServer.removeJawaNode(selectedJawaNode);
	}
	
	public JawaNode getSelectedJawaNode(){
		return selectedJawaNode;
	}
	

}
