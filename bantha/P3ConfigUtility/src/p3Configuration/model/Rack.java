package p3Configuration.model;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import edu.umd.cs.piccolo.event.PBasicInputEventHandler;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.nodes.PImage;

public class Rack extends PImage {
	private int type;
	private ArrayList<Server> servers;
	private int uheight = 41;
	
	public Rack(int _type){
		this.type = _type;
		servers = new ArrayList<Server>();
		
		setImage();
		this.setPickable(false);
		
		this.addInputEventListener(new PBasicInputEventHandler(){
			public void mouseClicked(PInputEvent aEvent) {
				System.out.println("rack mouseClicked");
				
				aEvent.setHandled(false);
			}
			public void mousePressed(PInputEvent event){
				System.out.println("rack mousePResse");
				event.setHandled(false);
			}
		});
	}
	
	public void setImage(){
		File imgFile = new File("resources/wide_rack.png");
		
		try {
			this.setImage(ImageIO.read(imgFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setOffset(20, 30);
		this.moveToBack();
		
	}
	public void setType(int _type){
		this.type = _type;
	}
	
	public int getType(){
		return this.type;
	}
	
	public void addServer(Server _server){
		servers.add(_server);
	}
	
	public ArrayList<Server> getServers(){
		return servers;
	}
	
	public void deleteServer(Server _server){
		servers.remove(_server);
	}
	
	public int getNextULocation(){
		int max = 1;
		
		for(int i=0;i<servers.size();i++){
			int topLocation = servers.get(i).getLocation() + servers.get(i).getUheight();
			if(max < topLocation)
				max = topLocation;
		}
		
		return max;
	}

	public void setUheight(int _uheight){
		this.uheight = _uheight;
		//repaintChildren();
	}
	
	public int getUheight(){
		return uheight;
	}
	
	
	public Server getServer(int ulocation, int uheight){
		for(Server server:servers){
			if((server.getLocation()==ulocation) && (server.getUheight() == uheight) ){
				return server;
			}
		}
		
		return null;
	}
	
}

