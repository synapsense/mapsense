package p3Configuration.model;

import java.util.ArrayList;
import java.util.List;

import p3Configuration.scinter.SCAllConfig;
import p3Configuration.scinter.SCJawaModel;

public class Configuration {
	
	private ArrayList<Rack> racks = new ArrayList();
	
	public Configuration(){
		
	}
	
	public Configuration(SCAllConfig scAllConfig){
		 
		for(SCJawaModel SCJawa:scAllConfig.getJawas()){
			int rackType = SCJawa.getRack();
			
			Rack rack = this.getRackByType(rackType);
			if(rack == null){
				rack = new Rack(rackType);
				this.addRack(rack);
			}else{
				rack = this.getRackByType(rackType);
			}
			
			int ulocation = SCJawa.getuPosition();
			int uheight = SCJawa.getuHeight();
			
			Server server = rack.getServer(ulocation, uheight);
			
			if(server == null){
				server = new Server(ulocation, uheight, rack);
				rack.addServer(server);
			}
			
			long jid = SCJawa.getJid();
			int feed = SCJawa.getFeed();
			String str_jid = Long.toString(jid);
			
			Jawa jawa = new Jawa(str_jid, feed, server);
			server.addJawa(jawa);
		}
	}

	public void addRack(Rack _rack){
		racks.add(_rack);
	}
	
	public ArrayList<Rack> getRacks(){
		return racks;
	}
	
	public Rack getRackByType(int type){
		for(Rack rack:racks){
			if(rack.getType()==type)
				return rack;
		}
		
		return null;
	}
	
	public SCAllConfig parseToSCAllConfig(){
		SCAllConfig scAllConfig = new SCAllConfig();
		
		for(Rack rack :this.getRacks()){
			int rack_type = rack.getType();
			
			for(Server server:rack.getServers()){
				int server_uposition = server.getLocation();
				int server_uheight = server.getUheight();
				
				for(Jawa jawa:server.getJawas()){
					String jawa_id = jawa.getId();
					int jawa_feed = jawa.getFeed();
					long jawa_long_id = Long.parseLong(jawa_id.trim());
					
					SCJawaModel jawaModel = new SCJawaModel(jawa_long_id,server_uposition,server_uheight,rack_type, jawa_feed);
					scAllConfig.addJawa(jawaModel);
				}
			}
		}
		
		
		return scAllConfig;
	}
	
	
	
}
