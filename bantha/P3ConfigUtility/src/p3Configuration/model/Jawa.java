package p3Configuration.model;

import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import edu.umd.cs.piccolo.nodes.PImage;
import edu.umd.cs.piccolo.nodes.PPath;
import p3Configuration.utility.Variables;

public class Jawa extends PImage{
	private int index;
	private String id;
	private int feed;
	private Server server;
	private boolean isSelected = false;
	private File imgFile = new File("resources/jawa.png");
	private File selectedImgFile = new File("resources/select_jawa.png");;
	
	public Jawa(String _id, int _feed, Server _server){
		id = _id;
		feed = _feed;
		server = _server;
		index = server.getMaxJawaIndex();
	}
	
	
	public String getId(){
		return id;
	}
	
	public void setId(String _id){
		id = _id;
	}
	
	public int getFeed(){
		return feed;
	}
	
	public void setFeed(int _feed){
		feed = _feed;
	}
	
	public Server getServer(){
		return server;
	}
	
	public void setServer(Server _server){
		server = _server;
	}
	
	public void setIndex(int _index){
		index = _index;
	}
	
	public int getIndex(){
		return index;
	}
	
	public boolean equals(Jawa compareJawa){
		if(this.getId().equals(compareJawa.getId())){
			return true;
		}
		return false;
	}
}
