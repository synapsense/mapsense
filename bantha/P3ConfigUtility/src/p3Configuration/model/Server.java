package p3Configuration.model;

import java.util.ArrayList;

public class Server{
	private int location;
	private int uheight;
	private ArrayList<Jawa> jawas;
	private Rack rack;
	
	public Server(int _location, int _uheight, Rack _rack){
		location = _location;
		uheight = _uheight;
		rack = _rack;
		jawas = new ArrayList<Jawa>();
	}
	public void setLocation(int _location){
		location = _location;
	}
	
	public void setHeight(int _uheight){
		uheight = _uheight;
	}
	
	public int getLocation(){
		return location;
	}
	
	public int getUheight(){
		return uheight;
	}
	
	public void addJawa(Jawa _jawa){
		jawas.add(_jawa);
	}
	
	public ArrayList<Jawa> getJawas(){
		return jawas;
	}
	
	public void deleteJawa(Jawa _jawa){
		jawas.remove(_jawa);
		
	}
	
	public Rack getRack(){
		return rack;
	}
	
	public int getMaxJawaIndex(){
		int max = 0;
		
		for(int i=0;i<jawas.size();i++){
			if(max<jawas.get(i).getIndex())
				max = jawas.get(i).getIndex();
		}
		
		return max + 1;
	}
	
	public boolean equals(Server compareServer){
		if((this.getLocation() == compareServer.getLocation()) && (this.getUheight() == compareServer.getUheight())){
			return true;
		}else{
			return false;
		}
	}
}
