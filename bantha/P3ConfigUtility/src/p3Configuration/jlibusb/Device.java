package p3Configuration.jlibusb;

import p3Configuration.jlibusb.swig.SWIGTYPE_p_libusb_device_handle;
import p3Configuration.jlibusb.swig.jlibusb;


public class Device {
			     
	static {
		System.loadLibrary("libusb-1.0");
		System.loadLibrary("jlibusb");
		jlibusb.libusb_init(null);
	};
	
	SWIGTYPE_p_libusb_device_handle device;
	
	private Device(SWIGTYPE_p_libusb_device_handle dev) {
		device = dev;
	}
	
	static public Device createFromVidPid(int vid, int pid) throws NoDeviceException {
		SWIGTYPE_p_libusb_device_handle dev = jlibusb.libusb_open_device_with_vid_pid(null, vid, pid);
		if (dev == null)
			throw new NoDeviceException();
		
		return new Device(dev);
	}
	
	public void reset() {
		jlibusb.libusb_reset_device(device);
		//jlibusb.libusb_set_configuration(device, 0);
	}
	
	public void close() {
		jlibusb.libusb_close(device);
	}
	
	public void claimInterface(int inter) {
		jlibusb.libusb_claim_interface(device, inter);
	}
	
	public int bulkTransfer(short endpoint, byte[] data, long timeout) {
		int[] actual_length = new int[1];
		jlibusb.libusb_bulk_transfer(device, endpoint, data, data.length, actual_length, timeout);
		return actual_length[0];
	}
	
	/**
	 * Close and release all known libusb handles - this makes it
	 * impossible to reuse libusb after this point
	 */
	public void closeAll() {
		close();
		jlibusb.libusb_exit(null);
	}
	
}
