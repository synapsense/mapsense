package p3Configuration.jlibusb.test;

import p3Configuration.jlibusb.Device;
import p3Configuration.jlibusb.NoDeviceException;
import p3Configuration.jlibusb.swig.*;

public class CreateTest {

	public static void main(String[] args) {
		Device d = null;

		try {
			d = Device.createFromVidPid(0x9999, 0x2222);
		} catch (NoDeviceException e) {
			e.printStackTrace();
		}
		d.claimInterface(0);
		d.reset();
		while (true) {
			byte[] data = new byte[64];
			int trans = d.bulkTransfer((short) 129, data, 500L);
			System.out.print("Bytes: " + trans + " :");
			for (byte b : data) {
				System.out.print(Integer.toHexString(b) + " ");
			}
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block

			}
			System.out.println();
		}
	}
}
