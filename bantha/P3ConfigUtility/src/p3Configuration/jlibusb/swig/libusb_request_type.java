/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.0
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package p3Configuration.jlibusb.swig;

public final class libusb_request_type {
  public final static libusb_request_type LIBUSB_REQUEST_TYPE_STANDARD = new libusb_request_type("LIBUSB_REQUEST_TYPE_STANDARD", jlibusbJNI.LIBUSB_REQUEST_TYPE_STANDARD_get());
  public final static libusb_request_type LIBUSB_REQUEST_TYPE_CLASS = new libusb_request_type("LIBUSB_REQUEST_TYPE_CLASS", jlibusbJNI.LIBUSB_REQUEST_TYPE_CLASS_get());
  public final static libusb_request_type LIBUSB_REQUEST_TYPE_VENDOR = new libusb_request_type("LIBUSB_REQUEST_TYPE_VENDOR", jlibusbJNI.LIBUSB_REQUEST_TYPE_VENDOR_get());
  public final static libusb_request_type LIBUSB_REQUEST_TYPE_RESERVED = new libusb_request_type("LIBUSB_REQUEST_TYPE_RESERVED", jlibusbJNI.LIBUSB_REQUEST_TYPE_RESERVED_get());

  public final int swigValue() {
    return swigValue;
  }

  public String toString() {
    return swigName;
  }

  public static libusb_request_type swigToEnum(int swigValue) {
    if (swigValue < swigValues.length && swigValue >= 0 && swigValues[swigValue].swigValue == swigValue)
      return swigValues[swigValue];
    for (int i = 0; i < swigValues.length; i++)
      if (swigValues[i].swigValue == swigValue)
        return swigValues[i];
    throw new IllegalArgumentException("No enum " + libusb_request_type.class + " with value " + swigValue);
  }

  private libusb_request_type(String swigName) {
    this.swigName = swigName;
    this.swigValue = swigNext++;
  }

  private libusb_request_type(String swigName, int swigValue) {
    this.swigName = swigName;
    this.swigValue = swigValue;
    swigNext = swigValue+1;
  }

  private libusb_request_type(String swigName, libusb_request_type swigEnum) {
    this.swigName = swigName;
    this.swigValue = swigEnum.swigValue;
    swigNext = this.swigValue+1;
  }

  private static libusb_request_type[] swigValues = { LIBUSB_REQUEST_TYPE_STANDARD, LIBUSB_REQUEST_TYPE_CLASS, LIBUSB_REQUEST_TYPE_VENDOR, LIBUSB_REQUEST_TYPE_RESERVED };
  private static int swigNext = 0;
  private final int swigValue;
  private final String swigName;
}

