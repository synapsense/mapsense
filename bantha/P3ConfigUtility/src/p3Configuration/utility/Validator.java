package p3Configuration.utility;

import java.util.Enumeration;
import java.util.Iterator;

import p3Configuration.ui.ConfigWindow;
import p3Configuration.ui.ConfigurationCanvas;
import p3Configuration.ui.ValidatorDialog;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

public class Validator {
	
	private ConfigWindow configWindow;
	private ConfigurationCanvas canvas;
	private ValidatorDialog dialog;
	
	public Validator(ConfigWindow _configWindow, ConfigurationCanvas _canvas){
		configWindow = _configWindow;
		canvas = _canvas;
	}
	
	public boolean checkRackSelection(){
		ButtonGroup group = configWindow.getCabinetButtonGroup();
		boolean isSelected =false;
		
		return true;
		/*for (Enumeration e = group.getElements() ; e.hasMoreElements() ;) {
	         System.out.println(e.nextElement());
	         JRadioButton button = (JRadioButton) e.nextElement();
	         
	         if(button.isSelected()){
	        	 isSelected = true;
	         }
	     }

		
		if(isSelected){
			return true;
		}else{
			ValidatorDialog dialog = new ValidatorDialog(configWindow, "Error", "Please select Rack", 2);
			return false;
			
		}*/
	}
	
	public boolean checkServerSelection(){
		if(canvas.getSelectedServerNode()==null){
			dialog = new ValidatorDialog(configWindow, "Error", "Please select Server", 2);

			return false;
		}else{
			return true;
		}
	}
	
	public boolean checkJawaID(){
		String jid = configWindow.getJawaIdTextField().getText();
		System.out.println("jid;" + jid );
		System.out.println("jid length;" + jid.length() );
		
		if(jid.trim()=="" || jid.length()==0){
			dialog = new ValidatorDialog(configWindow, "Error", "Please Enter PlugMeter ID", 2);
		
			return false;
		}else{
			return true;
		}
	}
	
	public boolean checkJawaSelection(){
		if(canvas.getSelectedJawaNode()==null){
			dialog = new ValidatorDialog(configWindow, "Error", "Please Select PlugMeter", 2);
			
			return false;
		}else{
			return true;
		}
	}
}
