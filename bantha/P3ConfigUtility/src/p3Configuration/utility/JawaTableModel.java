package p3Configuration.utility;

import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

import p3Configuration.model.Jawa;
import p3Configuration.model.Rack;
import p3Configuration.model.Server;

public class JawaTableModel extends AbstractTableModel {
	public String[] colNames = { "PlugMeter ID", "Feed", "U Location", "U Height"};
    public Class[] colTypes = { String.class, Integer.class, Integer.class, Integer.class};
    
    Vector jawaVector = new Vector();
    Rack rack;
    
    public JawaTableModel(){
    	
    }
    
   @Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return colNames.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return jawaVector.size();
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		Jawa jawa = (Jawa) jawaVector.elementAt(rowIndex);
		
		switch(columnIndex){
		case 0:
			return jawa.getId();
		case 1:
			return jawa.getFeed();
		case 2:
			return jawa.getServer().getLocation();
		case 3:
			return jawa.getServer().getUheight();
		}
		return null;
		
	}
	
	public void setValueAt(Object value, int rowIndex, int columnIndex){
		//Jawa jawa = (Jawa) jawa_data.elementAt(rowIndex);
		
	}
	
	public String getColumnName(int col) {
      return colNames[col];
    }

    public Class getColumnClass(int col) {
      return colTypes[col];
    }

    public void setTableModel(Rack _rack){
    	rack = _rack;
    	
    	jawaVector.removeAllElements();
    	
    	for(Server server: rack.getServers()){
        	ArrayList<Jawa> jawas = server.getJawas();
        	for(Jawa jawa:jawas){
        		jawaVector.addElement(jawa);
        	}
    	}
    }
    
    public void addJawaData(Jawa _jawa){
    	jawaVector.addElement(_jawa);
    }
    
    public void editJawa(Jawa _jawa){
    	for(int i=0;i<jawaVector.size();i++){
    		Jawa currentJawa = (Jawa) jawaVector.elementAt(i);
    		
    		if(currentJawa.equals(_jawa)){
    			jawaVector.setElementAt(_jawa, i);
    			return;
    		}
    	}
    }

    public void removeJawa(Jawa _jawa){
    	for(int i=0;i<jawaVector.size();i++){
    		Jawa currentJawa = (Jawa) jawaVector.elementAt(i);
    		
    		if(currentJawa.equals(_jawa)){
    			jawaVector.remove(i);
    			return;
    		}
    	}
    }
   
}
