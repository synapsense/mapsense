package p3Configuration.utility;

public class Variables {
	public static final int LEFT_RACK_TYPE = 1;
	public static final int CENTER_RACK_TYPE = 2;
	public static final int RIGHT_RACK_TYPE = 3;
	
	public static final int RACK_IMAGE_HEIGHT = 825;
	public static final int SERVER_IMAGE_WIDTH = 265;
	public static final int JAWA_IMAGE_WIDTH = 52;
	public static final double JAWA_IMAGE_VSPACE = 1;
	public static final double JAWA_IMAGE_HSPACE = 5;
	
	public static final double RACK_BOTTOM_X = 52;
	public static final double RACK_BOTTOM_Y = 870;

}
