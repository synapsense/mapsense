package p3Configuration.utility;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import p3Configuration.jlibusb.Device;
import p3Configuration.jlibusb.NoDeviceException;
import p3Configuration.model.Configuration;
import p3Configuration.model.Jawa;
import p3Configuration.model.Rack;
import p3Configuration.model.Server;
import p3Configuration.scinter.NoSandCrawlerException;
import p3Configuration.scinter.SCAllConfig;
import p3Configuration.scinter.SCJawaModel;
import p3Configuration.scinter.SandCrawler;

public class ConfigurationIO {
	
	public ConfigurationIO(){
	}

	/*
	 * Serialize configuration object to xml
	 */
	public Document serializeXml(Configuration configuration){
		DocumentBuilderFactory factory = null;
	    DocumentBuilder builder = null;
	    Document document = null;
	    
	    try {
	    	factory = DocumentBuilderFactory.newInstance();
		    builder = factory.newDocumentBuilder();
		    document = builder.newDocument();
		    Node root = document.createElement("p3configuration");
		    document.appendChild(root);
		    
		    
		    for(Rack rack: configuration.getRacks()){
		    	int type = rack.getType();
		    	int uheight = rack.getUheight();
		    	
		    	Node rackNode = document.createElement("rack");
		    	((Element)rackNode).setAttribute("type", Integer.toString(type));
		    	((Element)rackNode).setAttribute("uheight", Integer.toString(uheight));
		    	
		    	root.appendChild(rackNode);
		    	
		    	for(Server server: rack.getServers()){
		    		int server_uheight = server.getUheight();
		    		int server_location = server.getLocation();
		    		
		    		Node serverNode = document.createElement("server");
		    		((Element)serverNode).setAttribute("uheight", Integer.toString(server_uheight));
		    		((Element)serverNode).setAttribute("location", Integer.toString(server_location));
		    		
		    		rackNode.appendChild(serverNode);
		    		
		    		for(Jawa jawa: server.getJawas()){
		    			String jid = jawa.getId();
		    			int feed = jawa.getFeed();
		    			
		    			Node jawaNode = document.createElement("jawa");
		    			((Element)jawaNode).setAttribute("id", jid);
			    		((Element)jawaNode).setAttribute("feed", Integer.toString(feed));
			    		
			    		serverNode.appendChild(jawaNode);
		    		}
		    	}
		    	
		    }
		    
		    
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		return document;
	}
	
	public void writeTemplate(String filepath, Configuration configuration){
		Document document = serializeXml(configuration);
		
		TransformerFactory tf = TransformerFactory.newInstance();
		try {
			OutputStream os = new FileOutputStream(filepath);
			
			Transformer t = tf.newTransformer();
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.transform(new DOMSource(document.getDocumentElement()),new StreamResult(os));
			os.close();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void showXml(String filename, Configuration configuration){
		Document document = serializeXml(configuration);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ElementToStream(document.getDocumentElement(), baos);
		String result = new String(baos.toByteArray());
		
		System.out.println(result);
	}

	public static void ElementToStream(Element element, OutputStream out) {
	    try {
	      DOMSource source = new DOMSource(element);
	      StreamResult result = new StreamResult(out);
	      TransformerFactory transFactory = TransformerFactory.newInstance();
	      Transformer transformer = transFactory.newTransformer();
	      transformer.transform(source, result);
	    } catch (Exception ex) {
	    }
	  }
	
	
	/*
	 * Send SCAllConfig to plugMeter controller
	 */
	public byte[] getSCallConfigByteData(SCAllConfig _allConfig){
		int data_size = 0;
		int num_jawa = _allConfig.getJawas().size();
		byte[] all_config_byte_data = new byte[7*num_jawa];
		
		int i=0;
		for(SCJawaModel jawaModel:_allConfig.getJawas()){
			byte[] config_byte_data = jawaModel.pack();
			int data_pos = 7*i; 
			
			for(int j=0;j<7;j++){
				all_config_byte_data[data_pos + j] = config_byte_data[j];
			}
			data_size+=config_byte_data.length;
			i++;
		}
		
		System.out.println(data_size);

		for (byte b : all_config_byte_data) {
			System.out.print(Integer.toHexString(b) + " ");
		}
		
		return all_config_byte_data;
		
	}
	
	public void uploadConfiguration(Configuration configuration){
		SCAllConfig allConfig = configuration.parseToSCAllConfig();

		System.out.print("jid	");
		System.out.print("Feed	");
		System.out.print("rack	");
		System.out.print("uposition		");
		System.out.print("uheight	");
		System.out.println("phase	");

		for(SCJawaModel jawa: allConfig.getJawas()){
			System.out.print(jawa.getJid() + "	");
			System.out.print(jawa.getFeed() + "	");
			System.out.print(jawa.getRack() + "	");
			System.out.print(jawa.getuPosition() + "	");
			System.out.print(jawa.getuHeight() + "	");
			System.out.println(jawa.getPhase() + "	");
		}
		
		/*
		try {
			SandCrawler sanCrawler = new SandCrawler();
			sanCrawler.uploadConfiguration(allConfig);
			
		} catch (NoSandCrawlerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	*/	
	}
	
	public Configuration downloadConfiguration(){
		Configuration configuration = null;
		
		/*
		try {
			SandCrawler sanCrawler = new SandCrawler();
			SCAllConfig allConfig = sanCrawler.downloadConfiguration();
			
			configuration = new Configuration(allConfig);
			
		} catch (NoSandCrawlerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		// test code 
		SCAllConfig allConfig = new SCAllConfig();
		for(int i=0;i<10;i++){
			SCJawaModel jawa = new SCJawaModel(i+1, i+1, 1, 2, 0);
			allConfig.addJawa(jawa);
		}
		
		configuration = new Configuration(allConfig);
		return configuration;
	}
	
	
}
