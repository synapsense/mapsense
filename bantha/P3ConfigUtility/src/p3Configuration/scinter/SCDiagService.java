package p3Configuration.scinter;

import p3Configuration.jlibusb.Device;

import java.util.concurrent.BlockingQueue;


public class SCDiagService implements Runnable {
    private SandCrawler sandCrawler;
    private Thread scThread;
    private boolean keepOnRunning;
    private BlockingQueue<SCDiagMessage> queue;

    public SCDiagService(SandCrawler sc, BlockingQueue<SCDiagMessage> queue) {
        sandCrawler = sc;
        keepOnRunning = true;
        this.queue = queue;
        scThread = new Thread(this, "SandCrawler-Diag");
        scThread.start();
    }

    private SCDiagMessage makeMessage(byte[] data) {
        return new SCDiagMessage(data);
    }

    public void run() {
        while (keepOnRunning) {
            byte[] data = new byte[64];
            int trans = sandCrawler.getDevice().bulkTransfer((short) 129, data, 500L);
            try {
                if (trans > 0)
                    queue.put(makeMessage(data));
            } catch (InterruptedException e) {
                keepOnRunning = false;
            }
        }
    }

}
