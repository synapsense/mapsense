package p3Configuration.scinter;

import p3Configuration.jlibusb.Device;
import p3Configuration.jlibusb.NoDeviceException;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class SandCrawler implements Runnable {

    private Device sandCrawler;
    private SCDiagService scDiagService;

    private Thread scThread;
    private boolean keepOnRunning;
    private final static int MAX_QUEUE = 100;


    private BlockingQueue<byte[]> diagInQueue = new ArrayBlockingQueue<byte[]>(MAX_QUEUE);
    /**
     * Messages from sandcrawler to computer
     */
    private BlockingQueue<SCDiagMessage> diagOutQueue = new ArrayBlockingQueue<SCDiagMessage>(MAX_QUEUE);


    public SandCrawler() throws NoSandCrawlerException {
        try {
            sandCrawler = Device.createFromVidPid(0x9999, 0x2222);
        } catch (NoDeviceException e) {
            throw new NoSandCrawlerException();
        }
        keepOnRunning = true;
        sandCrawler.claimInterface(0);
        sandCrawler.reset();
        scThread = new Thread(this, "SandCrawler");
        scThread.start();

        scDiagService = new SCDiagService(this, diagOutQueue);
    }

    public boolean uploadConfiguration(SCAllConfig config) {
        return false;
    }

    public SCAllConfig downloadConfiguration() {
        return null;
    }


    public SCDiagMessage getDiagMessage() {
        SCDiagMessage m = null;
        if (diagOutQueue.isEmpty())
            return null;
        try {
            m = diagOutQueue.poll(10, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            return null;
        }
        return m;
    }

    Device getDevice() {
        return sandCrawler;
    }

    public void run() {
        while (keepOnRunning) {
            try {
                Thread.sleep(1000);

            } catch (InterruptedException e) {
                keepOnRunning = false;
            }
        }
    }

}
