package p3Configuration.scinter;


public class SCDiagMessage {
    public final static int TYPE_PRINT = 0x10;


    public byte[] data;
    public int type;

    public SCDiagMessage(byte[] data) {
        this.data = data;
        type = data[0];
    }

    public String toString() {
        if (type == TYPE_PRINT) {
            byte[] strarray = new byte[63];
            java.lang.System.arraycopy(data, 1, strarray, 0, 63);
            return "<PRINT: " + new String(strarray) + ">";
        }
        return "<Unknown Data Type: " + type + ">";
    }

}
