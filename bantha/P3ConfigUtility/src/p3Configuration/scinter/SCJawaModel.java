package p3Configuration.scinter;

/**
 * SandCralwer representation of a Jawa
 */

public class SCJawaModel {
    public long getJid() {
        return jid;
    }

    public void setJid(long jid) {
        this.jid = jid;
    }

    public int getuPosition() {
        return uPosition;
    }

    public void setuPosition(int uPosition) {
        this.uPosition = uPosition;
    }

    public int getuHeight() {
        return uHeight;
    }

    public void setuHeight(int uHeight) {
        this.uHeight = uHeight;
    }

    public int getRack() {
        return rack;
    }

    public void setRack(int rack) {
        this.rack = rack;
    }

    public int getFeed() {
        return feed;
    }

    public void setFeed(int feed) {
        this.feed = feed;
    }

    public int getPhase() {
        return phase;
    }

    public void setPhase(int phase) {
        this.phase = phase;
    }

    private long jid;
    private int uPosition;
    private int uHeight;
    private int rack;
    private int feed;
    private int phase;

    public SCJawaModel(long jid, int upos, int uheight, int rack, int feed) {
        this.jid = jid;
        this.uPosition = upos;
        this.uHeight = uheight;
        this.rack = rack;
        this.feed = feed;
    }

    public SCJawaModel(byte[] data) {
        jid = ((data[0] << 24) & 0xff000000) | ((data[1] << 16) & 0xff0000) | ((data[2] << 8) & 0xff00) | ((data[3]) & 0xff);
        uPosition = data[4];
        uHeight = data[5];
        rack = data[6] >> 7;
        feed = (data[6] >> 4) & 0x7;
        phase = data[6] & 0x3;

    }

    public byte[] pack() {
        byte[] data = new byte[7];
        data[0] = (byte) ((jid >> 24) & 0xff);
        data[1] = (byte) ((jid >> 16) & 0xff);
        data[2] = (byte) ((jid >> 8) & 0xff);
        data[3] = (byte) (jid & 0xff);
        data[4] = (byte) uPosition;
        data[5] = (byte) uHeight;
        data[6] = (byte) (((rack & 0x3) << 7) | ((feed & 0x7) << 4) | (phase & 0x3));
        return data;
    }
}
