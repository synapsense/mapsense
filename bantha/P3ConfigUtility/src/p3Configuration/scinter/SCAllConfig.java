package p3Configuration.scinter;

import java.util.ArrayList;
import java.util.List;


public class SCAllConfig {
    private List<SCJawaModel> jawas = new ArrayList<SCJawaModel>();

    public void addJawa(SCJawaModel m) {
        jawas.add(m);
    }

    public List<SCJawaModel> getJawas() {
        return jawas;
    }


}
