package p3Configuration.scinter;

import java.util.concurrent.BlockingQueue;

public class SCConfigService implements Runnable {
    private SandCrawler sandCrawler;
    private Thread scThread;
    private boolean keepOnRunning;
    private BlockingQueue<SCConfigMessage> queue;

    public SCConfigService(SandCrawler sc, BlockingQueue<SCConfigMessage> queue) {
        sandCrawler = sc;
        keepOnRunning = true;
        this.queue = queue;
        scThread = new Thread(this, "SandCrawler-Config");
        scThread.start();

    }

    public void run() {
        while (keepOnRunning) {
            byte[] data = new byte[64];
            int trans = sandCrawler.getDevice().bulkTransfer((short) 129, data, 500L);

        }
    }

}
