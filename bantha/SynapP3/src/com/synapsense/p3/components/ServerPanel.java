package com.synapsense.p3.components;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.painter.AlphaPainter;
import org.jdesktop.swingx.painter.CompoundPainter;
import org.jdesktop.swingx.painter.GlossPainter;
import org.jdesktop.swingx.painter.MattePainter;
import org.jdesktop.swingx.painter.Painter;

import com.synapsense.laf.styles.BgImagePainter;
import com.synapsense.laf.styles.ImageMerger;
import com.synapsense.p3.components.RackTable.RackTableModel;
import com.synapsense.p3.exceptions.JawaCreationFailedException;
import com.synapsense.p3.model.Rack;
import com.synapsense.p3.model.Server;
import com.synapsense.p3.model.Jawa;
import com.synapsense.p3.ui.PlugmetersTable.PlugmetersTableModel;
import com.synapsense.p3.utilities.CommonFunctions;
import com.synapsense.p3.utilities.StringConstants;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.border.LineBorder;

//implements DragGestureListener, DragSourceListener  
public class ServerPanel extends JXPanel {

	public static final int slotHeight = 18;
	private static Logger logger = Logger.getLogger(ServerPanel.class.getName());
	private Server server;
	private ArrayList<PlugmeterButton> plugmeters;
	private PlugmetersTableModel p3TableModel;
	private JXTable p3Table;
	private boolean isSelected;
	/**
	 * 
	 */
	public ServerPanel(Server server) {
		setServer(server);
		setBorder(new LineBorder(Color.DARK_GRAY, 2, true));			
		setLayout(null);
	}
	
	public void setServer(Server server){
		this.server = server;
		plugmeters = new ArrayList<PlugmeterButton>();
		
		adjustComponentSize();		
		initDataBindings();
		setBackgroundPainter(getBgPainter());
	}
	
	public Server getServer(){
		return server;
	}
	
	public Painter getBgPainter() {
		if (server ==null) {
			return null;
		}
		String filename = "";
		try{
			if(server.getUheight()>4){
				String headerFileName =  StringConstants.getString("CUSTOM_SEVER_PFX")+ "header.png";
				String bodyFileName =  StringConstants.getString("CUSTOM_SEVER_PFX")+"body.png";
				String footerFileName =  StringConstants.getString("CUSTOM_SEVER_PFX")+"footer.png";
			
				ImageMerger imgMerger = new ImageMerger(headerFileName, bodyFileName, server.getUheight()-2,footerFileName);
				return BgImagePainter.getPainter(imgMerger.getMergedImage());
			}else{
				filename = StringConstants.getString("SERVER_PFX") + new Integer(server.getUheight()).toString() + ".png";
				return BgImagePainter.getImagePainter(filename);
			}
			
		}catch(Exception e){
			System.out.println("error paint server:" + e.getLocalizedMessage());
			System.out.println(filename);
			logger.error(e.getLocalizedMessage());
			return null;
		}
	}
		
	public void setSelected(boolean b){
		isSelected = b;
		setBackgroundPainter(getBgPainter());
		if(b)
			setBorder(new LineBorder(Color.RED, 2, true));
		else
			setBorder(new LineBorder(Color.DARK_GRAY, 2, true));	
		
		this.revalidate();
	}
	
	public boolean getSelected(){
		return isSelected;
	}
	
	/**
	 * 
	 * New implementation loads server sprites from .PNG images
	 * 
	 */
	public int getUHeight() {
		//return uHeight;
		return server.getUheight();
	}

	public void setUHeight(int uHeight) {
		server.setHeight(uHeight);
		adjustComponentSize();
		setBackgroundPainter(getBgPainter());
	}
	
	public void setULocation(int uLocation){
		server.setLocation(uLocation);
	}

	public int getULocation(){
		return server.getLocation();
	}
	public int getRowIndexOnRack(){
		//System.out.println("serverPanel rack height" + server.getRack().getUheight());
		int row = server.getRack().getUheight() - server.getLocation() - server.getUheight() + 1;
		return row;
	}
	
	
	/**
	 * Do not let the SynthTableUIl.paintCell to resize me
	 */
	@Override
	public void setSize(int width, int height) {
		height = slotHeight* server.getUheight();
		super.setSize(width,height);
		
	}

	/**
	 * Do not let the SynthTableUIl.paintCell to resize me
	 */	
	@Override
	public void setBounds(int x, int y, int width, int height) {
		height = slotHeight*server.getUheight();
		super.setBounds(x, y, width, height);
	}

	
	/**
	 * 
	 */
	public void adjustComponentSize() {
		int height = slotHeight*server.getUheight();
		setSize(120, height);
		setPreferredSize(getSize());
		setMinimumSize(getSize());	
	}
	
	public void adjustComponent(int y){
		
		adjustComponentSize();
		this.setBounds(this.getX(), y, this.WIDTH, this.HEIGHT);
	}
	/**
	 * 
	 */
	private void initDataBindings() {
		for(Jawa jawa:server.getJawas()){
			PlugmeterButton plugmeter = new PlugmeterButton(jawa);
			add(plugmeter);
			plugmeters.add(plugmeter);
		}
		CommonFunctions.redeployPlugmeters(this);
	}
	
	private void setPlugmeterPosition(PlugmeterButton pm){
		Dimension size = pm.getPreferredSize();
		int jawaIndex = pm.getJawa().getIndex();
		
		pm.setBounds(jawaIndex*pm.ICON_WIDTH -12, 5, size.width, size.height);
	}
	
	public PlugmeterButton createPlugmeter() throws JawaCreationFailedException{
		Jawa jawa = new Jawa(StringConstants.getString("PLUGMETER_DEFAULT_ID"), 1, server);
		server.addJawa(jawa);

		PlugmeterButton newPlugmeter = new PlugmeterButton(jawa);	
		//setPlugmeterPosition(newPlugmeter);
		add(newPlugmeter);
		plugmeters.add(newPlugmeter);
		
		CommonFunctions.redeployPlugmeters(this);
		
		return newPlugmeter;
	}
	
	public PlugmeterButton createPlugmeter(int rpdu) throws JawaCreationFailedException{
		Jawa jawa = new Jawa(StringConstants.getString("PLUGMETER_DEFAULT_ID"), rpdu, server);
		server.addJawa(jawa);

		PlugmeterButton newPlugmeter = new PlugmeterButton(jawa);	
		//setPlugmeterPosition(newPlugmeter);
		add(newPlugmeter);
		plugmeters.add(newPlugmeter);
		
		CommonFunctions.redeployPlugmeters(this);
		
		return newPlugmeter;
	}
	
	
	public void addPlugmeter(PlugmeterButton pm){
		
		Jawa jawa = pm.getJawa();
		jawa.setServer(server);
		server.addJawa(jawa);
		//System.out.println("plugmeter new index:" + newIndex);
		setPlugmeterPosition(pm);
		add(pm);
		plugmeters.add(pm);
		
		CommonFunctions.redeployPlugmeters(this);
	}
	
	public void deletePlugmeter(PlugmeterButton pm){
		Jawa jawa = pm.getJawa();
		server.deleteJawa(jawa);
		plugmeters.remove(pm);
		remove(pm);
		
		CommonFunctions.redeployPlugmeters(this);
	}
	
	public ArrayList<PlugmeterButton> getPlugmeters(){
		return plugmeters;
	}
	
	public PlugmeterButton getPlugmeter(Jawa jawa){
		for(PlugmeterButton pb: plugmeters){
			if(pb.getJawa().equals(jawa))
				return pb;
		}
		
		return null;
	}
	
	public String toString(){
		String str = "rack:" + Integer.toString(server.getRack().getType()) + ";row:" +  Integer.toString(getRowIndexOnRack()) + ";location:" + server.getLocation();
		return str;
	}
	
	
	public void moveTo(RackTable oldRackTable,RackTable newRackTable, int row){
		//remove from old rack
		//JXTable oldRackTable =((RackPanel)(MainScreen.getRackPanelByType(server.getRack().getType()))).table;
		RackTableModel oldRackModel = (RackTableModel) oldRackTable.getModel();
		oldRackModel.removeFrom(this);
		oldRackModel.fireTableDataChanged();
		
		// Add to new rack
		Rack newRack = (Rack)((RackTable) newRackTable).getRack();
		int newlocation = newRack.getUheight() - row;
		server.setLocation(newlocation);
		server.moveTo(newRack);
	
		// redraw children
		for(PlugmeterButton pb:this.getPlugmeters()){
			pb.repaintIcon();
		}
		
		((RackTableModel)newRackTable.getModel()).moveServerPanel(this, this.getRowIndexOnRack());
		((RackTableModel)newRackTable.getModel()).fireTableDataChanged();
	}
	
	public void addTo(RackTable newRackTable, int row){
		Rack newRack = (Rack)((RackTable) newRackTable).getRack();
		int newlocation = newRack.getUheight() - row;
		server.setLocation(newlocation);
		server.moveTo(newRack);
		
		((RackTableModel)newRackTable.getModel()).moveServerPanel(this, this.getRowIndexOnRack());
		((RackTableModel)newRackTable.getModel()).fireTableDataChanged();
		
	}
	
}
