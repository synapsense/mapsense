package com.synapsense.p3.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JTextField;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXTable;

import com.synapsense.p3.model.Jawa;
import com.synapsense.p3.model.Rack;
import com.synapsense.p3.ui.JXButtonDraggable;
import com.synapsense.p3.ui.MainScreen;
import com.synapsense.p3.ui.PlugmetersPanel;
import com.synapsense.p3.ui.RackPanel;
import com.synapsense.p3.utilities.StringConstants;
import com.synapsense.p3.utilities.Variables;

public class PlugmeterButton extends JXButtonDraggable{
	private static Logger logger = Logger.getLogger(PlugmeterButton.class.getName());
	private Jawa jawa;
	public static int ICON_WIDTH = 21;
	private boolean isRealtime = false;
	
	public PlugmeterButton(Jawa jawa) {
		super();
		this.jawa = jawa;
		createContents();
		
		if(jawa.getPhase()>0){
			//MainScreen.setPhasePlugmeter(this);
		}
		this.setFocusable(true);
		this.setToolTipText("plugmeter");
		this.setRolloverEnabled(true);
	}
	
	public Jawa getJawa(){
		return jawa;
	}
	
	public void createContents() {
		setBorderPainted(false);
		setBorder(null);
		setPaintBorderInsets(false);
		setOpaque(false);
		
		Dimension dim = new Dimension(20,7);
		this.setSize(dim);
		this.setPreferredSize(dim);
		setIcon(getImageIcon());
	}
	
	public void setSelected(boolean b){
		super.setSelected(b);
		setIcon(getImageIcon());
	}
	
	public void paint(Graphics g){
		super.paint(g);
	}
	
	public void repaintIcon(){
		setIcon(getImageIcon());
		super.repaint();
		
	}
	
	public void associatePhase(int phase){
		jawa.setPhase(phase);
		setIcon(getImageIcon());
		
		Rack rack = jawa.getServer().getRack();
		rack.setRpduPhaseAssociation(jawa.getFeed(), jawa);
	}
	
	public void dissociatePhase(){
		jawa.setPhase(0);
		jawa.removeInvalidity(Variables.CONFIGURATION_ERROR_MIXED_PHASE_ASSOCIATION);
		setIcon(getImageIcon());
	}
	
	public int getPhase(){
		return jawa.getPhase();
	}
	
	public String getId(){
		return jawa.getId();
	}
	
	public void setId(String id){
		jawa.setId(id);
	}
	
	public int getRPDU(){
		return jawa.getFeed();
	}
	
	public void setRPDU(int feed){
		int oldRpdu = jawa.getFeed();
		jawa.setFeed(feed);
		
		// if plugmeter associates to phase and change RPDU
		Rack rack = jawa.getServer().getRack();
		rack.updateRpduPhaseAssociation(jawa, oldRpdu, feed);
	}
	
	private ImageIcon getImageIcon(){
		int phase = jawa.getPhase();
		String iconPath = "";
		String prefix = "IMAGE_PLUGMETER_WITH_PHASE";
		//System.out.println("plugmeter id:" + jawa.getId());
		if(phase==0){
			if(this.isSelected()){
				iconPath = StringConstants.getString("IMAGE_PLUGMETER_SELECTED");
			}else{
				//IMAGE_PLUGMETER_UNKNOWN 
				if(jawa.getId().equalsIgnoreCase(StringConstants.getString("PLUGMETER_DEFAULT_ID"))){
					iconPath = StringConstants.getString("IMAGE_PLUGMETER_UNKNOWN");
				}else{
					iconPath = StringConstants.getString("IMAGE_PLUGMETER");
				}
			}
				
		}else if(phase==1){
			if(this.isSelected()){
				iconPath =StringConstants.getString(prefix + "_SELECTED_A");    
			}else{
				iconPath = StringConstants.getString(prefix + "_DEFAULT_A");
			}
		}else if(phase==2){
			if(this.isSelected()){
				iconPath =StringConstants.getString(prefix + "_SELECTED_AB");    
			}else{
				iconPath = StringConstants.getString(prefix + "_DEFAULT_AB");
			}
			
		}else if(phase==3){
			if(this.isSelected()){
				iconPath = StringConstants.getString(prefix + "_SELECTED_B");
			}else{
				iconPath = StringConstants.getString(prefix + "_DEFAULT_B");
			}
		}else if(phase==4){
			if(this.isSelected()){
				iconPath = StringConstants.getString(prefix + "_SELECTED_BC");
			}else{
				iconPath = StringConstants.getString(prefix + "_DEFAULT_BC");
			}
		}else if(phase==5){
			if(this.isSelected()){
				iconPath = StringConstants.getString(prefix + "_SELECTED_C");
			}else{
				iconPath = StringConstants.getString(prefix + "_DEFAULT_C");
			}
			
		}else if(phase==6){
			if(this.isSelected()){
				iconPath = StringConstants.getString(prefix + "_SELECTED_CA");
			}else{
				iconPath = StringConstants.getString(prefix + "_DEFAULT_CA");
			}
		}
		//System.out.println("p3 icon path:" + iconPath);
		return new ImageIcon(PlugmeterButton.class.getResource(iconPath));
	}
	
	public void setRealtime(boolean isRealtime){
		this.isRealtime = isRealtime;
	}
	
	public boolean getRealtime(){
		return this.isRealtime;
	}
}
