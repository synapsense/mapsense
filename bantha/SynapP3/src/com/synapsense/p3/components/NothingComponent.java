/**
 * 
 */
package com.synapsense.p3.components;

import java.awt.Graphics;

import javax.swing.JComponent;

import org.apache.log4j.Logger;

public class NothingComponent extends JComponent {
	private static Logger logger = Logger.getLogger(NothingComponent.class.getName());

	@Override
	public void paint(Graphics g) {
		// does nothing
	}		
	public NothingComponent() {
		setOpaque(true);
		//logger.warn("Unsupported component");
	}
	public boolean isShowing() {
		return false;
	}
	
	public boolean isVisible(){
		return false;
	}
	
	public boolean isFocusable(){
		return false;
	}
}