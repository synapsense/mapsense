package com.synapsense.p3.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.dnd.DragSource;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.DropMode;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.painter.Painter;

import com.synapsense.laf.styles.BgImagePainter;
import com.synapsense.p3.model.Jawa;
import com.synapsense.p3.model.Rack;
import com.synapsense.p3.model.Server;
import com.synapsense.p3.ui.DropTransferHandler;
import com.synapsense.p3.ui.FindDialog;
import com.synapsense.p3.ui.FunctionKeyAdapter;
import com.synapsense.p3.ui.RackIndexLabel;
import com.synapsense.p3.ui.SynapJXTable;
import com.synapsense.p3.utilities.ComponentsManager;
import com.synapsense.p3.utilities.LogManager;
import com.synapsense.p3.utilities.MessageHandler;
import com.synapsense.p3.utilities.SelectionChangeEvent;
import com.synapsense.p3.utilities.SelectionChangeListener;
import com.synapsense.p3.utilities.SelectionManager;
import com.synapsense.p3.utilities.StringConstants;
import com.synapsense.p3.utilities.Validator;
import com.synapsense.p3.utilities.Variables;
import com.synapsense.p3.utilities.Variables.ConfigurationTypes;

public class RackTable extends SynapJXTable implements SelectionChangeListener{

	private static Logger logger = Logger.getLogger(RackTable.class.getName());
	private static final long serialVersionUID = -7655666711936103688L;
	private Rack rack;
	private RackTableModel rackTableModel = new RackTableModel();
	private ComponentsManager cManager;
	private SelectionManager selectionManager;
	/**
	 * JXTable constructor  
	 */
	
	public RackTable(final ComponentsManager cManager, SelectionManager selectionManager) {
		super(cManager, selectionManager);
		this.cManager = cManager;
		this.selectionManager = selectionManager;
		
		setDefaultRenderer(ServerPanel.class, new RackCellRenderer());
		setDefaultRenderer(java.lang.Integer.class, new IndexRenderer());
		setDefaultRenderer(java.lang.Object.class, new ValidationRenderer());
		setModel(rackTableModel);
		setDefaultStyle();
		
		RackTableCellMouseAdapter mouseAdapter = new RackTableCellMouseAdapter(this);
		this.addMouseListener(mouseAdapter);
		this.addMouseMotionListener(mouseAdapter);
		this.addKeyListener(new FunctionKeyAdapter(cManager, this));
		
		
		selectionManager.addSelectionChangeListener(this);
		
		
	}
	
	public RackTableModel getTableModel(){
		return rackTableModel;
	}

	/**
	 * 
	 */
	public void setDefaultStyle() {
		setShowHorizontalLines(true);		// TODO: fix it, z-order, keep the lines shown under servers
		setShowVerticalLines(false);
		
		setRolloverEnabled(false);
		setDoubleBuffered(true);
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		setFillsViewportHeight(false);
		setAlignmentY(Component.TOP_ALIGNMENT);
		setVisibleColumnCount(3);
		setBorder(UIManager.getBorder("TextField.border"));
		setDropMode(DropMode.USE_SELECTION);
		setTransferHandler(new DropTransferHandler(cManager, selectionManager));
		//setRowSelectionAllowed(false);
		//setColumnSelectionAllowed(true);
		//setCellSelectionEnabled(true);
		setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		setFocusable(true);
		setOpaque(false);
		setDragEnabled(true);
		setGridColor(Color.lightGray);
		setBackground(Color.WHITE);
		setForeground(Color.WHITE);
		
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.JTable#getCellRect(int, int, boolean)
	 */
	@Override
    public Rectangle getCellRect(int row, int column, boolean includeSpacing) {
		if(row<0 || column<0)
			return null;
		
		Rectangle rect = super.getCellRect(row, column, includeSpacing);
		try {
			//if(column==1){
				ServerPanel sor = (ServerPanel) rackTableModel.getValueAt(row, 1);
				if (sor!=null) {
					Server value = sor.getServer();
					int uHeight = value.getUheight();
					//System.out.println("uheight:" + uHeight);
					rect.height = rowHeight*uHeight; // TODO: get the actual u-height from model
					
				}else{
					//rect.width = rect.width - 10;
					//rect.height = 0;
					//throw new Exception();
				}

			//}
	    	return rect;

		} catch (Exception ex) {
			System.out.println("getcellRect error");
			logger.warn("Row:" + row + " column:" + column + " fails to getCellRect ");
			return null;
		}
    }
	
	/**
	 * 
	 * This method draws either transparent components or servers on rack
	 * 
	 */
	// @Override
	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {		
		
		Component c = super.prepareRenderer(renderer, row, column);
		
		if (c instanceof JComponent) {
			JComponent comp = (JComponent)c;					
			// - empty cells are transparent
			// - ServerOnRack is not transparent
			comp.setOpaque(comp instanceof ServerPanel);
			comp.setAlignmentY(TOP_ALIGNMENT);
			comp.setFocusable(false);
		}
		return c;
			
		
	}
	
	public Component getComponentAtPoint(TableCellRenderer renderer, int row, int column, MouseEvent mouseEvent){
		Component c = super.prepareRenderer(renderer, row, column);
		
		if(c instanceof ServerPanel){
			
			ServerPanel sor = (ServerPanel)c;
			Rectangle cellBound = this.getCellRect(row, 1, true);
			
			for(PlugmeterButton plugmeter: sor.getPlugmeters()){
				Rectangle plugmeterBound = plugmeter.getBounds();
				int x = this.getLocationOnScreen().x + cellBound.x + plugmeterBound.x;
				int y = this.getLocationOnScreen().y + cellBound.y + plugmeterBound.y;
				
				Rectangle plugmeterBoundOnScreen = new Rectangle(x, y, plugmeterBound.width, plugmeterBound.height);
				
				if(plugmeterBoundOnScreen.contains(mouseEvent.getLocationOnScreen())){
					return plugmeter;
				}
			}
			return sor;
		}
		return c;
	}
	
	public void setColumnWidths(){
		// Specify column widths
		setColumnFixedWidth(columnModel.getColumn(0), 30);	// TODO: define column width constants
		setColumnFixedWidth(columnModel.getColumn(1), 186);
		setColumnFixedWidth(columnModel.getColumn(2), 30);		
	}
	
	public PlugmeterButton getPlugmeter(Jawa jawa){
		PlugmeterButton pm = null;
		Server server = jawa.getServer();
		int rowIndex = rack.getUheight() - server.getLocation();
		
		if(rackTableModel.getValueAt(rowIndex, 1)!=null){
			ServerPanel sor = (ServerPanel) rackTableModel.getValueAt(rowIndex, 1);
			
			ArrayList<PlugmeterButton> plugmeters = sor.getPlugmeters();
			
			for(PlugmeterButton p:plugmeters){
				if(p.getJawa().equals(jawa))
					pm = p;
			}
		}
		return pm;
	}

	/**
	 * 
	 * @param c
	 * @param width
	 * 
	 * Convenience method to restrict column width adjustments
	 * 
	 */
	void setColumnFixedWidth(TableColumn c, Integer width) {
		c.setResizable(false);
		c.setWidth(width);
		c.setPreferredWidth(width);
		c.setMinWidth(width);
		c.setMaxWidth(width);
	}
	/*
	 * Set rack model
	 */
	public void setRack(Rack rack){
		this.rack = rack;
		rackTableModel.bindData(rack);
	}
	
	public Rack getRack(){
		return rack;
	}
	/**
	 * This renderer displays a server on rack
	 * 
	 */
	public class RackCellRenderer extends DefaultTableCellRenderer  {
		public RackCellRenderer(){
		}
		
		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			RackTableModel data = (RackTableModel) table.getModel();
			//System.out.println("cellRenderer verify: row  " + row + " " + isSelected);
			//data.printData();
			
			try{
				if(value==null){
					
					if(data.getServerAtRow(row) instanceof ServerPanel){
					// for extended cell which height is taller than 1
						ServerPanel refSor = data.getServerAtRow(row);
					//	System.out.println("refSor:" + refSor + " selected:" +refSor.getSelected());
					//	System.out.println();
					//	System.out.println("row:" + row + " isSelected:" + isSelected);
					//	System.out.println("RowIndexOnRack:" + refSor.getRowIndexOnRack());
					//	System.out.println(row-refSor.getRowIndexOnRack()+1);
						
						JXPanel panel = new JXPanel();
						panel.setPreferredSize(new Dimension(110,18));
						Painter painter = getBgPainter(refSor.getUHeight(), row-refSor.getRowIndexOnRack()+1, refSor.getSelected());
						if(painter!=null){
							panel.setBackgroundPainter(getBgPainter(refSor.getUHeight(), row-refSor.getRowIndexOnRack()+1, refSor.getSelected()));
						}
						
						return panel;
						//return new NothingComponent();
					}
					
				}else{
					if(value instanceof ServerPanel){
						ServerPanel sor = data.getServerAtRow(row);
						sor.setFocusable(true);
						return sor;	
					}
				}
				
			}catch(Exception e){
				return new NothingComponent();
				
			}
			return new NothingComponent();
		}
		
		private Painter getBgPainter(int uheight, int idx, boolean isSelected) {
			String filename = "";
			String selection = "";
			if(isSelected)
				selection="_selected";
			
			if(uheight>4){
				
				if(idx==1)
					filename = StringConstants.getString("CUSTOM_SEVER_PFX")  +  "header" + selection +".png";
				else if(idx==uheight)
					filename = StringConstants.getString("CUSTOM_SEVER_PFX")  +  "footer" + selection + ".png";
				else
					filename = StringConstants.getString("CUSTOM_SEVER_PFX")  +  "body" + selection+ ".png";
				
			}else{
				filename = StringConstants.getString("SERVER_PFX")  + Integer.toString(uheight) + "_" + Integer.toString(idx)  + selection + ".png";
				
			}
			//System.out.println("filename:" + filename);
			return BgImagePainter.getImagePainter(filename);
		}
		
	}
	/**
	 * 
	 * @author golovanov
	 *
	 * This renderer displays index label, i.e. slot number
	 *
	 */
	class IndexRenderer extends DefaultTableCellRenderer {
		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			if (value!=null && column==0) {
				String s = value.toString();
				RackIndexLabel jl = new RackIndexLabel();
				jl.setText(s);
				return jl;
			}
			return new NothingComponent();
		}
	}
	
	class ValidationRenderer extends DefaultTableCellRenderer {
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			JLabel label = (JLabel)super.getTableCellRendererComponent( table, value, isSelected, hasFocus, row, column  ); 
			
			RackTableModel rackModel = (RackTableModel)table.getModel();
			ServerPanel sor = (ServerPanel) rackModel.getValueAt(row, 1);
			if(sor!=null){
				Server server = sor.getServer();
				int numInvalidJawas = 0;
				
				for(Jawa jawa: server.getJawas()){
					if(jawa.getInvalidities().size()>0)
						numInvalidJawas++;
				}
			
				if(server.getInvalidities().size()>0 || numInvalidJawas>0){
					label.setIcon(new ImageIcon(RackTable.class.getResource(StringConstants.getString("IMAGE_INVALID_SERVER"))));
				}else{
					label.setIcon(null);
				}
			}else{
				label.setIcon(null);
			}
			return label;
		}
	}
	
	public class RackTableModel extends DefaultTableModel {
		
		private static final long serialVersionUID = 1L;
		private Class[] columnTypes = new Class[] {Integer.class, ServerPanel.class, Object.class};
		private final String[] columnNames = { "U#", "Contents", "Invalidity"};
		private boolean[] columnEditables = new boolean[] {false, false, false};
		private Object[][] data ;
		private Object[] serverMap;
		private Rack rack;
		
		public RackTableModel(){
			super();
		}
		
		public void bindData(Rack rack){
			if(rack==null){
				System.out.println("rack null");
				return;
				
			}
		
			this.rack = rack;
			int rackSize = rack.getUheight();
			Object[][] obj = new Object[rackSize][3];
			serverMap = new Object[rackSize];
			
			for (int i=0; i<rackSize; i++) {
				obj[i] = new Object[] {rackSize-i, null, null};
				//obj[i] = new Object[] {i, null, null};
				
				serverMap[i] = null;
			}
			
			for(Server server:rack.getServers()){
				server.setRack(rack);
				int realRowIndex = rackSize-(server.getLocation()+server.getUheight())+ 1;
				ServerPanel sor = new ServerPanel(server);
				obj[realRowIndex] = new Object[] {(rackSize-realRowIndex), sor, null};
				//obj[realRowIndex] = new Object[] {(realRowIndex), sor, null};
				
				for(int j=1;j<=server.getUheight();j++){
					serverMap[realRowIndex + (j-1)] = sor;	
				}
			}
			
			data = obj;
			fireTableDataChanged();
		}
		
		@Override
		public int getColumnCount() {
			// TODO Auto-generated method stub
			return columnNames.length;
		}

		@Override
		public int getRowCount() {
			if(this.data==null)
				return 0;
			else
				return data.length;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			return data[rowIndex][columnIndex];
		}
		
		public void setValueAt(Object aValue, int rowIndex, int columnIndex){
			data[rowIndex][columnIndex] = aValue;
			this.fireTableCellUpdated(rowIndex, columnIndex);
		}
		
		public void move(int from, int to){
			ServerPanel sor = (ServerPanel)data[from][1];
			Server server = ((ServerPanel)data[from][1]).getServer();
			int ulocation = rack.getUheight() - to;
			server.setLocation(ulocation);
			data[to] = new Object[]{ulocation,sor,null};
			data[from][1] = null;
		}
		
		public String getColumnName(int col) {
			return columnNames[col];
		}

		public Class getColumnClass(int col) {
			return columnTypes[col];
		}
		
		public boolean isCellEditable(int row, int column) {
			return columnEditables[column];
		}
		
		public ServerPanel addData(Object[] rowData) throws Exception{
			int row = (Integer) rowData[0];
			int ulocation = rack.getUheight() - row;
			int uheight = (Integer)rowData[1];
			int realRowIndex = row - uheight + 1;
			
			Server server = new Server(ulocation, uheight, rack);
			rack.addServer(server);

			ServerPanel sor = new ServerPanel(server);
			data[realRowIndex] = new Object[]{ ulocation + uheight -1, sor, null };
			
			for(int i=0;i<uheight;i++){
				serverMap[realRowIndex + i] = sor;
			}
			
			fireTableDataChanged();
			return sor;
		}
		
		public void moveRow(int start, int end, int to){
			super.moveRow(start, end, to);
		}
		
		public void removeRow(int row){
			super.removeRow(row);
		}
		
		public ServerPanel getServerAtRow(int row){
			try{
				if(row>=serverMap.length || row<0){
					return null;
				}
				
				if(serverMap[row]==null){
					return null;
				}
				
				return (ServerPanel) serverMap[row];
				
			}catch(Exception e){
				return null;
			}
		}
		
		public ServerPanel getServerAtUlocation(int ulocation){
			int row = rack.getUheight() - ulocation;
			return getServerAtRow(row);
		}
		
		public void resize(ServerPanel sor, int newRow, int newHeight){
			//remove old server info
			int oldRow = sor.getRowIndexOnRack();
			int oldHeight = sor.getUHeight();
			
			setValueAt(null, oldRow, 1 );
			for(int i=0;i<oldHeight;i++){
				serverMap[oldRow + i] = null;
			}
			
			// set new server info
			setValueAt(sor, newRow, 1);
			for(int i=0; i< newHeight;i++){
				serverMap[newRow + i] = sor;
			}
		}
		
		public void moveServerPanel(ServerPanel sor, int newRow ){
			setValueAt(sor, newRow, 1);
			
			int height = sor.getServer().getUheight();
			
			for(int i=0;i<height;i++){
				serverMap[newRow + i] = sor;
			}
		}
		
		public void removeFrom(ServerPanel sor){
			int height = sor.getServer().getUheight();
			int location = sor.getRowIndexOnRack();
			
			//System.out.println("remove from location:" + location);
			
			setValueAt(null, location, 1);
			
			for(int i=0;i<height;i++){
				int existServerLocation = location + i;
				if(existServerLocation<serverMap.length)
					//if(sor.equals(serverMap[existServerLocation])){
						serverMap[existServerLocation] = null;
					//}
				else{
					logger.error("ArrayIndexOutOfBounds error: " + existServerLocation + "/" + serverMap.length);
				}
			}
			
		}
		
		public void moveSeverPanels(){
			
		}
		
		public ArrayList<ServerPanel> getServerPanels(){
			ArrayList<ServerPanel> list = new ArrayList<ServerPanel>();
			
		
			for(int i=0;i<serverMap.length;i++){
				if(serverMap[i]!=null && !list.contains(serverMap[i]))
					list.add((ServerPanel) serverMap[i]);
			}
			
			return list;
		}
		
		public void printData(){
			for(int i=0;i<data.length;i++){
					System.out.println(i + ":" + data[i][1] 	+ "     " + serverMap[i] );
			}
		}
		
		public String getDebugData(){
			String strData="";
			for(int i=0;i<data.length;i++){
				String newStr =i + "[" + (rack.getUheight()-i) + "]"+ ":" + data[i][1] 	+ "     " + serverMap[i] +"\n";
				strData+=newStr;
			}
			
			return strData;
		}
	}
	
	class RackTableCellMouseAdapter extends MouseAdapter{
		RackTable table;
		private Point pointPressed;
		private Point pointReleased;
		private int rowPressed;
		private final static int MOVE = 1;
		private final static int RESIZE = 2;
		private final static int OFFSET = 3;
		private final static int SELECT = 4;
		private Component source;
		private int mode;
		public RackTableCellMouseAdapter(RackTable table){
			this.table = table;
		}
		
		private void updateCursor(boolean on){
			if(on){
				if(mode==MOVE){
					setCursor(DragSource.DefaultCopyDrop);
				}else{
					setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
				}
			}else{
				setCursor(null);
			}
		}

		private void resizeServer(Point locationOnScreen){
			try{
				if(source==null)
					return;
				
				//System.out.println("mouseDragged and resize");
				
				ServerPanel sor = (ServerPanel)source;
				Server server = sor.getServer();

				int newHeight;
				int realRowIndex;
				int newLocation = server.getLocation();
				int selectedRow;
				
				Rectangle rect = table.getCellRect(sor.getRowIndexOnRack(), 1, true);
				Rectangle convertedRect = new Rectangle(rect.x + table.getLocationOnScreen().x, rect.y+table.getLocationOnScreen().y, rect.width, rect.height);
				
				int serverTopY = rect.y+table.getLocationOnScreen().y;
				int serverBottomY = serverTopY + rect.height;
				
				boolean isTop;
				// find where the user pressed for resizing ( top or bottom)
				if(Math.abs(serverTopY-locationOnScreen.y)<Math.abs(serverBottomY-locationOnScreen.y))
					isTop = true;
				else
					isTop = false;
				
				//System.out.println(convertedRect.toString());
				//System.out.println(locationOnScreen.toString());
				//System.out.println(convertedRect.contains(locationOnScreen));
				int row =  sor.getRowIndexOnRack();
				
				int deltaUHeight = 0;
				int deltaY = 0;
				
				if(isTop){
					deltaY = locationOnScreen.y - serverTopY;
					deltaUHeight = deltaY / 18;
					if(deltaUHeight==0){
						return;
					}
					
					// make smaller (from top to bottom)
					if(deltaY>0){
						//System.out.println("make smaller and from top to bottom");
						newHeight = server.getUheight() - deltaUHeight;
						realRowIndex = row + deltaUHeight;
					}else{ // make bigger (from bottom to top)
						//System.out.println("make bigger and from bottom to top");
						deltaUHeight = -deltaUHeight;
						newHeight = server.getUheight() + deltaUHeight;
						realRowIndex = row - deltaUHeight;
					}
					
				}else{
					deltaY = locationOnScreen.y - serverBottomY;
					deltaUHeight = deltaY / 18;
					if(deltaUHeight==0){
						return;
					}
					
					// make bigger ( from top to bottom)
					if(deltaY>0){
						//System.out.println("make bigger and from top to bottom");
						deltaUHeight = -deltaUHeight;
						newHeight = server.getUheight() - deltaUHeight;
						realRowIndex = row;
						newLocation+=deltaUHeight; 	
					}else{// make smaller( from bottom to top)
						//System.out.println("make smaller and from bottom to top");
						deltaUHeight = -deltaUHeight;
						newHeight = server.getUheight() - deltaUHeight;
						realRowIndex = row;
						newLocation+=deltaUHeight; 
					}
				}
				
				selectedRow = realRowIndex + newHeight -1;
				//System.out.println("real row index:" + realRowIndex + " : newHeight:" + newHeight  + " selectedRow:" + selectedRow) ;
				try{
					if(Validator.checkServerLocation(selectedRow, newHeight, server.getRack(), server)){
						((RackTableModel)table.getModel()).resize(sor, realRowIndex, newHeight);
						int oldHeight = server.getUheight();
						sor.setUHeight(newHeight);
						sor.setULocation(newLocation);
						((RackTableModel)(table.getModel())).fireTableDataChanged();
						
						logger.info("resize server u from " + oldHeight + " to " + newHeight);
					}
						
				}catch(Exception e){
					logger.error("Fail to resize server");
					logger.error(e.getStackTrace().toString());
						
				}
				
			}catch(Exception ex){
				logger.error("Fail to resize server");
				logger.error(ex.getStackTrace().toString());
				ex.getStackTrace();
			}
		}
		/*private void resizeServer(MouseEvent e){
			try{
				int deltaY = e.getLocationOnScreen().y - pointPressed.y;
				int deltaUHeight = deltaY / 18;
				
				if(deltaUHeight==0){
					//System.out.println("cannot be 0");
					return;
				}
				
				ServerPanel sor = (ServerPanel)source;
				Server server = sor.getServer();

				int newHeight;
				int realRowIndex;
				int newLocation = server.getLocation();
				int selectedRow;
				
				Rectangle rect = table.getCellRect(sor.getRowIndexOnRack(), 1, true);
				Rectangle convertedRect = new Rectangle(rect.x + table.getLocationOnScreen().x, rect.y+table.getLocationOnScreen().y, rect.width, rect.height);
				int row =  sor.getRowIndexOnRack();
				
				// make smaller
				if(convertedRect.contains(e.getLocationOnScreen())){
					// from top to bottom
					if(deltaY>0){
						newHeight = server.getUheight() - deltaUHeight;
						realRowIndex = row + deltaUHeight;
						
					}else{ //from bottom to top
						deltaUHeight = -deltaUHeight;
						newHeight = server.getUheight() - deltaUHeight;
						realRowIndex = row;
						newLocation+=deltaUHeight; 
					}
					
				}else{//make bigger
					// from top to bottom
					if(deltaY>0){
						newHeight = server.getUheight() + deltaUHeight;
						realRowIndex = row;
						newLocation-=deltaUHeight; 
					}else{//from bottom to top
						deltaUHeight = -deltaUHeight;
						newHeight = server.getUheight() + deltaUHeight;
						realRowIndex = row - deltaUHeight;
					}
				}
				
				selectedRow = realRowIndex + newHeight -1;
				
				if(Validator.checkServerLocation(selectedRow, newHeight, server.getRack(), server)){
					((RackTableModel)table.getModel()).resize(sor, realRowIndex, newHeight);
					int oldHeight = server.getUheight();
					sor.setUHeight(newHeight);
					sor.setULocation(newLocation);
					((RackTableModel)(table.getModel())).fireTableDataChanged();
					
					logger.info("resize server u from " + oldHeight + " to " + newHeight);
				}
			}catch(Exception ex){
				logger.error("Fail to resize server");
				logger.error(ex.getStackTrace().toString());
				ex.getStackTrace();
			}
		}
		*/
		private void movePlugmeter(MouseEvent e){
			try{
				// get target rack
				RackTable targetTable = getTargetRack(pointReleased);
				RackTableModel targetModel = (RackTableModel) targetTable.getModel();
				
				// get target row in target rack
				int newrow = getRowAtTargetRack(targetTable, pointReleased);
				int newcol = getColumnAtTargetRack(targetTable, pointReleased);
				//System.out.println("new row "+newrow + "  new col " + newcol);

				ServerPanel targetServer = targetModel.getServerAtRow(newrow);
				
					
				if(targetServer!=null){
					//System.out.println("got target server");
					
					PlugmeterButton pm = (PlugmeterButton)source;
					ServerPanel oldServer = (ServerPanel) pm.getParent();
					
					if(oldServer.equals(targetServer)){
						//System.out.println("I got new parent");
						return;
					}else{
						if(Validator.checkMaxPlugmeter(targetServer.getServer())){

							oldServer.deletePlugmeter(pm);
							table.changeSelection(oldServer.getRowIndexOnRack(), 1, false, false);
							table.requestFocus();
							
							targetServer.addPlugmeter((PlugmeterButton)source);
							targetTable.changeSelection(newrow, newcol, false, false);
							targetTable.requestFocus();
							
							// need to update mixed phase association
							Validator.checkRpduPhaseAssociationInRack(table.getRack());
							Validator.checkRpduPhaseAssociationInRack(targetTable.getRack());
							
							// select moved plugmeter
							selectionManager.setServerSelection((ServerPanel)pm.getParent(), null);
							selectionManager.setPlugmeterSelection(pm, null);
							
							logger.info("Moved SmartPlug " + oldServer.getServer().toString() + " to " + targetServer.getServer().toString());
						}else{
							MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_MAX_P3_ERROR_MSG"));
						}
						
					}
				}

			}catch(Exception ex){
				logger.error("Failt to move plugmeter");
				logger.error(LogManager.getStackTrace(ex));
			}
		}
		
		private void moveServer(MouseEvent e){
			try{
				System.out.println("move server");
				ServerPanel sor = (ServerPanel)source;
				Server server = sor.getServer();
				
				//System.out.println("=========================");
				//System.out.println("row pressed:" + rowPressed);
				//System.out.println("sor ulocation:" + sor.getULocation());
				//System.out.println("sor base real row:" + sor.getRowIndexOnRack());
				
				// get target rack
				RackTable targetTable = getTargetRack(pointReleased);
				Rack newRack = (Rack)((RackTable) targetTable).getRack();
				
				// get target row in target rack
				int newrow = getRowAtTargetRack(targetTable, pointReleased);
				//System.out.println("newrow:" + newrow);
				//System.out.println("base:" + (sor.getRowIndexOnRack() + sor.getUHeight() -1));
				
				int gap = rowPressed - (sor.getRowIndexOnRack() + sor.getUHeight() -1);
				newrow = newrow - gap;
				//System.out.println("gap:" + gap);
				//System.out.println("newrow:" + newrow);
				
				if(Validator.checkServerLocation(newrow, server.getUheight(),newRack, server)){
					String oldInfo = server.toString();
					sor.moveTo(table,targetTable, newrow);
					// need to update mixed phase association
					Validator.checkRpduPhaseAssociationInRack(table.getRack());
					Validator.checkRpduPhaseAssociationInRack(targetTable.getRack());
					
					String newInfo = server.toString();
					logger.info("Moved a server from  " + oldInfo + " to " + newInfo);
					
				}else{
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_MOVE_SERVER_INSUFFICIENT_SPACE_MSG"));
					
				}
				//((RackTableModel) targetTable.getModel()).printData();
				
			}catch(Exception ex){
				logger.error("Fail to moveServer");
				logger.error(LogManager.getStackTrace(ex));
			}
		}
		
		// move all selected servers
		private void offsetServers(MouseEvent e){
			try{
				ServerPanel baseSor = (ServerPanel)source;
				// get target rack
				RackTable targetTable = getTargetRack(pointReleased);
				Rack newRack = (Rack)((RackTable) targetTable).getRack();
				
				// get target row in target rack
				int newrow = getRowAtTargetRack(targetTable, pointReleased);
				//System.out.println("new row "+newrow );
				//System.out.println("rowPressed:" + rowPressed);
				
				ServerPanel baseServerPanel = rackTableModel.getServerAtRow(rowPressed);
				int gap = rowPressed - (baseServerPanel.getRowIndexOnRack() + baseServerPanel.getUHeight() -1);
				//System.out.println("gap:" + gap);
				newrow = newrow - gap;
				//System.out.println("newrow:" + newrow);
				
				HashMap<Integer, ServerPanel> sortedServers = cManager.getSortedServersByPosition(baseSor);
				
				int validKey = Validator.checkServersLocation(sortedServers, newrow, newRack);
				if(validKey==0){
					logger.info("Offset selected servers");
					HashMap<Integer, ServerPanel> clonedServers = (HashMap<Integer, ServerPanel>) sortedServers.clone();
					
					for(Map.Entry<Integer, ServerPanel> me : sortedServers.entrySet()){
						int offset = me.getKey();
						ServerPanel sor = me.getValue();
						//System.out.println("offset info:" + sor.getServer().toString() + " "  + offset);
						((RackTableModel)table.getModel()).removeFrom(sor);
						//sor.moveTo(table, targetTable, newrow + offset);
					}
					((RackTableModel)table.getModel()).fireTableDataChanged();
					
					for(Map.Entry<Integer, ServerPanel> me : clonedServers.entrySet()){
						int offset = me.getKey();
						ServerPanel sor = me.getValue();
						//System.out.println("offset info:" + sor.getServer().toString() + " "  + offset);
						String oldInfo = sor.getServer().toString();
						sor.addTo(targetTable, newrow+offset);
						String newInfo = sor.getServer().toString();
						logger.info("     From " + oldInfo + " to " + newInfo);
					}

					//rackTableModel.printData();
				}else if(validKey == 1){
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_OFFSET_SERVERS_MULTIRACK_MSG"));
				}else{
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_MOVE_SERVER_INSUFFICIENT_SPACE_MSG"));
				}
				

			}catch(Exception ex){
				logger.error("Fail to offset selected servers");
				logger.error(LogManager.getStackTrace(ex));
			}
		}
		
		private int getRowAtTargetRack(RackTable targetTable, Point targetPoint){
			int row = -1;
			
			Point relativePoint = new Point(targetPoint.x - targetTable.getLocationOnScreen().x, targetPoint.y - targetTable.getLocationOnScreen().y);
			//System.out.println("new relative point : " + relativePoint);
			row = targetTable.rowAtPoint(relativePoint);
			return row;
		}

		private int getColumnAtTargetRack(RackTable targetTable, Point targetPoint){
			int col = -1;
			
			Point relativePoint = new Point(targetPoint.x - targetTable.getLocationOnScreen().x, targetPoint.y - targetTable.getLocationOnScreen().y);
			//System.out.println("new relative point : " + relativePoint);
			col = targetTable.columnAtPoint(relativePoint);
			return col;
		}

		private RackTable getTargetRack(Point targetPoint){
			RackTable targetTable = null;
			
			// get target rack
			JXTable rackTable1 = cManager.getRackTableByType(Variables.LEFT_RACK_TYPE);
			JXTable rackTable2 = cManager.getRackTableByType(Variables.CENTER_RACK_TYPE);
			JXTable rackTable3 = cManager.getRackTableByType(Variables.RIGHT_RACK_TYPE);
		
			//System.out.println(" rack 1 table: " + rackTable1.getLocationOnScreen().x);
			//System.out.println(" rack 2 table: " + rackTable2.getLocationOnScreen().x);
			//System.out.println(" rack 3 table: " + rackTable3.getLocationOnScreen().x);
			
			//System.out.println(" current loc: " + targetPoint);
			
			if(rackTable2.getLocationOnScreen().x > targetPoint.x){
				targetTable = (RackTable) rackTable1;
				//System.out.println(" tarbettable:1 ");
				
			}else if(rackTable3.getLocationOnScreen().x >targetPoint.x){
				targetTable = (RackTable) rackTable2;
				//System.out.println(" tarbettable:2 ");

			}else{
				targetTable = (RackTable) rackTable3;
				//System.out.println(" tarbettable:3 ");
			}

			
			return targetTable;
		}
		
		private void getShiftSelection(int lastRow){
			int rackType = ((RackTable)table).getRack().getType();
			int firstRow;
			
			ArrayList<ServerPanel> selectedServers = selectionManager.getSelectedServers();
			if(selectedServers.size()>0){
				ServerPanel lastSor = selectedServers.get(selectedServers.size()-1);
				if(lastSor.getServer().getRack().getType()==rackType){
					firstRow = lastSor.getRowIndexOnRack();
				}else{
					return;
				}
			}else{
				return;
			}
			
			int start, end;
			
			if(firstRow<lastRow){
				end = lastRow;
				start = firstRow;
			}else{
				end = firstRow;
				start = lastRow;
			}

			RackTableModel model = (RackTableModel) table.getModel();

			for(int i=start;i<=end;i++){
				if(model.getValueAt(i, 1) instanceof ServerPanel){
					selectionManager.addServerSelection((ServerPanel) model.getValueAt(i, 1), table);
					
				}
			}
		}
		
		public void mouseClicked(MouseEvent e){
			System.out.println("mouseClicked");
			
			pointPressed = e.getLocationOnScreen();

			Point p = e.getPoint();
			int row = table.rowAtPoint(p);
			int col = table.columnAtPoint(p);
			rowPressed = row;
			//System.out.println("pressed row in mousePressed function:"  + row);
			
			RackTableModel rackModel = (RackTableModel) table.getModel();
			ServerPanel sor = rackModel.getServerAtRow(row);
	
			if(sor==null){
				// add new configuration
				boolean isAdded = cManager.addConfiguration(table, row);
				if(!isAdded)
					selectionManager.clearSelection();
				
				pointPressed = null;
				pointReleased = null;
				source = null;
				mode = 0;
				rowPressed = -1;
				
			}else{
				Component c = ((RackTable)table).getComponentAtPoint(table.getCellRenderer(row, col), row, col, e);
				
				if(c instanceof PlugmeterButton){
					PlugmeterButton p3 = ((PlugmeterButton)c);
					source = p3;
					
					if(e.isControlDown()){
						if(selectionManager.getSelectedPlugmeters().contains(p3)){
							selectionManager.removePlugmeterSelection(p3, table);
						}else{
							selectionManager.addServerSelection((ServerPanel)p3.getParent(), null);
							selectionManager.addPlugmeterSelection(p3, null);							
						}
						
					}else{
						selectionManager.setServerSelection((ServerPanel)p3.getParent(), null);
						selectionManager.setPlugmeterSelection(p3, null);
						setCursor(DragSource.DefaultCopyDrop);
					}
					
					
				}else{
					if(cManager.getSelectedConfigType()!=null && cManager.getSelectedConfigType().equals(ConfigurationTypes.PLUGMETER)){
						cManager.addConfiguration(table, row);
						e.consume();
						return;
					}
					if(selectionManager.getSelectedServers().contains(sor)){
						if(e.isControlDown()){
							selectionManager.removeServerSelection(sor, null);
						}
					}else{
						if(e.isControlDown()){
							selectionManager.addServerSelection(sor, null);
							
						}else if(e.isShiftDown()){
							getShiftSelection(row);
						}else{
							selectionManager.clearPlugmeterSelection(null);
							selectionManager.setServerSelection(sor, null);
						}
						
					}
					
					if(mode == RESIZE){
						
					}else{
						source = sor;
						setCursor(DragSource.DefaultCopyDrop);
					}
				}
			}
		}

		public void mousePressed(MouseEvent e){
			//System.out.println("mousePRessed");
			pointPressed = e.getLocationOnScreen();

			Point p = e.getPoint();
			int row = table.rowAtPoint(p);
			int col = table.columnAtPoint(p);
			rowPressed = row;
			//System.out.println("pressed row in mousePressed function:"  + row);
			
			RackTableModel rackModel = (RackTableModel) table.getModel();
			ServerPanel sor = rackModel.getServerAtRow(row);
	
			if(sor!=null){
				// edit existing configuration
				Component c = ((RackTable)table).getComponentAtPoint(table.getCellRenderer(row, col), row, col, e);
				
				if(c instanceof PlugmeterButton){
					PlugmeterButton p3 = ((PlugmeterButton)c);
					source = p3;
					
				}else{
					
					if(mode == RESIZE){
						
					}else{
						source = sor;
						setCursor(DragSource.DefaultCopyDrop);
					}
				}
			}
		}
		
		public void mouseDragged(MouseEvent e){
				if(source!=null){
					if(mode==RESIZE){
						resizeServer(e.getLocationOnScreen());
					}else{
						if(source instanceof PlugmeterButton){
							if(selectionManager.getSelectedPlugmeters().size()==1){
								mode = MOVE;
							}
						}else if(source instanceof ServerPanel){ 
							if(selectionManager.getSelectedServers().size()>1){
								mode = OFFSET;
							}else{
								mode = MOVE;
							}
						}
					}
				}	
		}

		public void mouseEntered(MouseEvent e){
		}

		public void mouseExited(MouseEvent e){
		}
		
		public void mouseMoved(MouseEvent e){
			//System.out.println("mouseMove");
			Point p = e.getPoint();
			int row = table.rowAtPoint(p);
			int col = table.columnAtPoint(p);
			
			//System.out.println("mouse entered" + row);
			
			RackTableModel rackModel = (RackTableModel) table.getModel();
			ServerPanel server = rackModel.getServerAtRow(row);
	
			if(server!=null){
				Rectangle area = table.getCellRect(server.getRowIndexOnRack(), 1, false);

				//System.out.println("minY" + area.getMinY());
				//System.out.println("maxY" + area.getMaxY());
				//System.out.println("loc" + p.y);
				
				int gap = 5;
				Rectangle bottomLine = new Rectangle(area.x, (int) (area.getMaxY()-gap), area.width, gap*2);
				Rectangle topLine = new Rectangle(area.x, (int) (area.getMinY() -gap), area.width, gap*2);

				//System.out.println("minY" + topLine);
				//System.out.println("maxY" + bottomLine);

				if(topLine.contains(p) || bottomLine.contains(p)){
					source = server;
					//System.out.println("let's resize it");
					mode = RESIZE;
					updateCursor(true);
				}else if(mode==MOVE || mode==OFFSET){
				}else{
					source = null;
					mode = 0;
					updateCursor(false);
				}
			}else{
				source = null;
				mode = 0;
				updateCursor(false);
			}
		}
		
		public void mouseReleased(MouseEvent e){
			//System.out.println("mouseReleased");
			
			pointReleased = e.getLocationOnScreen();
			
			if(source instanceof PlugmeterButton){
				movePlugmeter(e);
			}else if(source instanceof ServerPanel){
				if(mode==RESIZE){
					//resizeServer(e);
				}else if(mode == MOVE){
					//System.out.println("let's move server");
					moveServer(e);
				}else if(mode == OFFSET){
//					System.out.println("let's offset all selected ones");
					offsetServers(e);
				}
			}
			pointPressed = null;
			pointReleased = null;
			source = null;
			updateCursor(false);
			mode = 0;
			rowPressed = -1;
		}
	}

	@Override
	public void selectionChanged(SelectionChangeEvent e) {
		// TODO Auto-generated method stub
		ArrayList<Object> selections = e.getAddtions();
		
		for(Object o:selections){
			if(o instanceof PlugmeterButton){
				PlugmeterButton p = (PlugmeterButton)o;
				p.setSelected(true);
			}else if(o instanceof ServerPanel){
				ServerPanel s = (ServerPanel)o;
				s.setSelected(true);
			}
		}
		
		ArrayList<Object> deselections = e.getRemovals();
		
		for(Object o:deselections){
			if(o instanceof PlugmeterButton){
				PlugmeterButton p = (PlugmeterButton)o;
				p.setSelected(false);
			}else if(o instanceof ServerPanel){
				ServerPanel s = (ServerPanel)o;
				s.setSelected(false);
			}
		}
		this.repaint();
	}
	
	
}