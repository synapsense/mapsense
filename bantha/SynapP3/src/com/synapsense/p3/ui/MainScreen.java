package com.synapsense.p3.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import javax.swing.OverlayLayout;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.ScrollPaneConstants;
import java.awt.event.WindowAdapter;
import org.jdesktop.swingx.JXImagePanel;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXImagePanel.Style;
import org.apache.log4j.Logger;

import com.synapsense.p3.components.RackTable;
import com.synapsense.p3.model.Configuration;
import com.synapsense.p3.model.ConfigurationError;
import com.synapsense.p3.utilities.BuildInfo;
import com.synapsense.p3.utilities.ComponentsManager;
import com.synapsense.p3.utilities.ConfigurationIO;
import com.synapsense.p3.utilities.MessageHandler;
import com.synapsense.p3.utilities.SelectionManager;
import com.synapsense.p3.utilities.StringConstants;
import com.synapsense.p3.utilities.Variables;
import com.synapsense.p3.ui.RackPanel;

public class MainScreen extends JFrame  {
	private static final int FRAME_WIDTH  		= StringConstants.getIntValue("FRAME_WIDTH");
	private static final int FRAME_HEIGHT 		= StringConstants.getIntValue("FRAME_HEIGHT");
	private int TOOLBAR_HEIGHT = StringConstants.getIntValue("TOOLBAR_HEIGHT");
	private int BANNER_HEIGHT = StringConstants.getIntValue("BANNER_HEIGHT");
	private int TITLEBAR_HEIGHT = StringConstants.getIntValue("TITLEBAR_HEIGHT");
	private static final int SCROLLPANE_WIDTH		= StringConstants.getIntValue("SCROLLPANE_WIDTH");
	//private int SCROLLPANE_HEIGHT		= StringConstants.getIntValue("SCROLLPANE_HEIGHT");
	private int SCROLLPANE_HEIGHT		= StringConstants.getIntValue("SCROLLPANE_HEIGHT");
	private int RACKTABLE_HEADER_HEIGHT = StringConstants.getIntValue("RACKTABLE_HEADER_HEIGHT");
	private int RACKTABLE_FOOTER_HEIGHT = StringConstants.getIntValue("RACKTABLE_FOOTER_HEIGHT");
	private int RACKTABLE_HEIGHT = StringConstants.getIntValue("RACKTABLE_HEIGHT");
	private int SLOT_HEIGHT = StringConstants.getIntValue("SLOT_HEIGHT");
	private int P3TABLE_WIDTH = StringConstants.getIntValue("P3TABLE_WIDTH");
	private int RACKTABLE_MIN_HEIGHT = StringConstants.getIntValue("RACKTABLE_MIN_HEIGHT");
	
	
	private static Logger logger = Logger.getLogger(MainScreen.class.getName());
	
	private ConfigurationIO io = new ConfigurationIO();
	//private Configuration configuration;
	private SelectionManager selectionManager = new SelectionManager();
	private ComponentsManager cManager = new ComponentsManager(selectionManager);
	
	private JPanel centerMainPanel = new JPanel();
	private final JPanel top = new JPanel();
	private final JXImagePanel banner = new JXImagePanel();
	private MainToolbar toolbar = new MainToolbar(cManager, selectionManager);
	private final JSplitPane mainPane = new JSplitPane();
	private final JScrollPane scrollpane = new JScrollPane();
	private final JPanel rackSizePanel = new JPanel();
	private final JPanel racks = new JPanel();
	private JScrollBar scrollbar;
	
	private JXLabel rack1IndicatorLabel = new JXLabel(StringConstants.getString("LABEL_NOT_CONFIG"));
	private JXLabel rack2IndicatorLabel = new JXLabel(StringConstants.getString("LABEL_NOT_CONFIG"));
	private JXLabel rack3IndicatorLabel = new JXLabel(StringConstants.getString("LABEL_NOT_CONFIG"));
	
	private RackPanel rack1;
	private RackPanel rack2;
	private RackPanel rack3;
	
	private JXPanel rack1Panel = new JXPanel();
	private JXPanel rack2Panel = new JXPanel();
	private JXPanel rack3Panel = new JXPanel();
	
	private PlugmetersPanel plugmetersPanel = new PlugmetersPanel(cManager, io, selectionManager);
	
	/**
	 * Launch the application
	 * 
	 * @param args
	 */
	public static void main(final String args[]) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					setSynapSenseLAF();
					
					final MainScreen frame = new MainScreen();
					frame.setVisible(true);
					frame.loadConfiguration();
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	 /* 
	 * Loads custom Look and Feel from synapsense_laf.jar
	 * 
	 */
	
	static void setSynapSenseLAF() {
		// String defaultLAF = UIManager.getCrossPlatformLookAndFeelClassName();
		final String laf = StringConstants.getString("SYNAPSENSE_LAF"); //$NON-NLS-1$
		try {
			UIManager.setLookAndFeel(laf);
		} catch (final UnsupportedLookAndFeelException e) {
			//e.printStackTrace();
			logger.error(e.getLocalizedMessage());
		} catch (final ClassNotFoundException e) {
			// TODO Auto-generated catch block
			logger.error(e.getLocalizedMessage());

		} catch (final InstantiationException e) {
			// TODO Auto-generated catch block
			logger.error(e.getLocalizedMessage());
		} catch (final IllegalAccessException e) {
			// TODO Auto-generated catch block
			logger.error(e.getLocalizedMessage());
		}
	}
	 
	/**
	 * Create the frame
	 */
	public MainScreen() {
		super();
		
		rack1 = new RackPanel(cManager, rack1IndicatorLabel, io, selectionManager, this);
		rack2 = new RackPanel(cManager, rack2IndicatorLabel, io, selectionManager, this);
		rack3 = new RackPanel(cManager, rack3IndicatorLabel, io, selectionManager, this);
		
		setName("MainScreen");
		
		logger.info("\n\nStart Powersuite configuration utility");	
		logger.info("Version:" + BuildInfo.getString("build.revision"));
		//getContentPane().setForeground(Color.BLACK);
		//getContentPane().setBackground(Color.WHITE);
		//this.setBackground(Color.white);
		
		
		setSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
		setMinimumSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));	// minimum resolution 1024x768 as per requirements spec
		setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
		
		setResizable(true);
		
		setTitle(StringConstants.getString("TITLE")); //$NON-NLS-1$
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setIconImage(new ImageIcon(MainScreen.class.getResource("/resources/banner/appicon.png")).getImage());
		
		
		
		// Add resize handler to maximize the component and adjust its children sizes
		this.addComponentListener(new java.awt.event.ComponentAdapter() {
			public void componentResized(final ComponentEvent e)
			{
				//System.out.println("componentResized");
				adjustChildrenSizes();
			}
		});
		
		this.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				
				if(!io.isSavedConfiguration(toolbar.getConfiguration())){
					String options[] = {"Yes", "No"};
					int response = MessageHandler.showOptionMessage(MessageHandler.MESSAGE_TYPE_CONFIRMATION, StringConstants.getString("VALIDATION_UNSAVED_CONFIGURATION_MSG"), options);
					if(response == JOptionPane.YES_OPTION){
						io.disconnectController();
						System.exit(0);
					}else{
						return;
					}
					
				}else{
					io.disconnectController();
					System.exit(0);
				}
			}
			
			public void windowDeactivated(WindowEvent e){
				//System.out.println("windowDeactivated");
			}
			
			public void windowLostFocus(WindowEvent e){
				//System.out.println("windowLostFocus");
			}
			
			public void windowActivated(WindowEvent e){
				//System.out.println("windowActivated");
				((MainScreen)(MainScreen.getFrames()[0])).getRootPane().requestFocus();
			}
		});
		
		try {
			initComponents();
		} catch (final Throwable e) {
			logger.error(e.getLocalizedMessage());
		}
		
		selectionManager.addSelectionChangeListener(plugmetersPanel);
		//selectionManager.addSelectionChangeListener(cManager);
		
	}
	
	
	private void initComponents(){
		//scrollpane_height = MIN_SCROLLPANE_HEIGHT;
		this.setContentPane(new BackgroundPanel());
		getContentPane().setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		initTop();
		getContentPane().add(top);
		
		// Set initial main panel boundaries
		mainPane.setBorder(null);
		mainPane.setDividerSize(0);
		final int iw = getInsets().left + getInsets().right;
		final int ih = getInsets().top + getInsets().bottom;		
		mainPane.setDividerLocation(SCROLLPANE_WIDTH);
		// Add main to content pane
		getContentPane().add(mainPane);
		
		scrollpane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollpane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollpane.setColumnHeader(null);
		//scrollpane.setBackground(Color.white);
		scrollpane.setBorder(null);
		scrollbar = scrollpane.getVerticalScrollBar();
		scrollbar.setUnitIncrement(22);
		scrollbar.setSize(3, SCROLLPANE_HEIGHT);
	
		plugmetersPanel.setMaximumSize(new Dimension(P3TABLE_WIDTH, 2000));
		GridBagLayout gbl = new GridBagLayout();
		gbl.columnWidths = new int[] { 250, 250, 250 };
		gbl.columnWeights = new double[] { 0.0, 0.0, 0.0 };
		
		rackSizePanel.setLayout(gbl);
		rackSizePanel.setBorder(null);
		
		rack1IndicatorLabel.setAlignmentX(0.5f);
		rack2IndicatorLabel.setAlignmentX(0.5f);
		rack3IndicatorLabel.setAlignmentX(0.5f);
		
		racks.setLayout(new RackPanel.RackGridBagLayout());		
		//racks.setBackground(Color.WHITE);
		
		LayoutManager overlay = new OverlayLayout(rack1Panel);
		
		rack1Panel.setLayout(overlay);
		rack1Panel.add(rack1IndicatorLabel);
		rack1Panel.add(rack1);
		
		LayoutManager overlay2 = new OverlayLayout(rack2Panel);
		rack2Panel.setLayout(overlay2);
		rack2Panel.add(rack2IndicatorLabel);
		rack2Panel.add(rack2);
		
		LayoutManager overlay3 = new OverlayLayout(rack3Panel);
		rack3Panel.setLayout(overlay3);
		rack3Panel.add(rack3IndicatorLabel);
		rack3Panel.add(rack3);

		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.insets = new Insets(0, 0, 0, 0);
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbl.setConstraints(rack1.header, gbc);
		rackSizePanel.add(rack1.header);
		racks.add(rack1Panel, new RackPanel.RackGridBagConstraints(0));
		racks.add(rack1.pedestal,        new RackPanel.RackGridBagConstraints(0, 1));
		gbc.gridx = 1;
		gbl.setConstraints(rack2.header, gbc);
		rackSizePanel.add(rack2.header,  gbc);		
		racks.add(rack2Panel, new RackPanel.RackGridBagConstraints(1));
		racks.add(rack2.pedestal,        new RackPanel.RackGridBagConstraints(1, 1));		
		
		gbc.gridx = 2;
		gbl.setConstraints(rack3.header, gbc);
		rackSizePanel.add(rack3.header,  gbc);
		racks.add(rack3Panel, new RackPanel.RackGridBagConstraints(2));								
		racks.add(rack3.pedestal,        new RackPanel.RackGridBagConstraints(2, 1));
		
		scrollpane.setColumnHeaderView(rackSizePanel);
		scrollpane.setViewportView(racks);
		
		mainPane.setLeftComponent(scrollpane);		
		mainPane.setRightComponent(plugmetersPanel);
		
		// bind all components to componentManager
		cManager.bindComponents(this, rack1.table, rack2.table, rack3.table, plugmetersPanel);
		
		// handling key stroke
		this.getRootPane().getActionMap().put("allP3WorkingRack", new AbstractAction(){
			public void actionPerformed(ActionEvent arg0) {
				cManager.selectAllP3InWorkingRack();
			}
		});
		
		this.getRootPane().getActionMap().put("allP3", new AbstractAction(){
			public void actionPerformed(ActionEvent arg0) {
				cManager.selectAllP3();
			}
		});
		
		this.getRootPane().getActionMap().put("deleteSelections", new AbstractAction(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				//System.out.println("mainframe delete key");
				cManager.deleteSelections();
			}
			
		});
		
		this.getRootPane().getActionMap().put("rackTableInfo1", new AbstractAction(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				cManager.showRackInfo(Variables.LEFT_RACK_TYPE);
			}
		});

		this.getRootPane().getActionMap().put("rackTableInfo2", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				cManager.showRackInfo(Variables.CENTER_RACK_TYPE);
			}
		});

		this.getRootPane().getActionMap().put("rackTableInfo3", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				cManager.showRackInfo(Variables.RIGHT_RACK_TYPE);
			}
		});

		this.getRootPane().getActionMap().put("p3SelectionInfo", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				cManager.showPlugmeterSelection();
			}
		});
		
		this.getRootPane().getActionMap().put("serverSelectionInfo", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				cManager.showServerSelection();
			}
		});
		
		this.getRootPane().getActionMap().put("find", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//System.out.println("find key");
				cManager.showFindJawaDialog();
			}
		});
		
		KeyStroke controlA = KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK);
		this.getRootPane().getInputMap().put(controlA, "allP3WorkingRack");

		KeyStroke controlShiftA = KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK  | InputEvent.SHIFT_DOWN_MASK);
		this.getRootPane().getInputMap().put(controlShiftA, "allP3");
	
		KeyStroke deleteStroke = KeyStroke.getKeyStroke((char) KeyEvent.VK_DELETE);
		this.getRootPane().getInputMap().put(deleteStroke, "deleteSelections");
		
		KeyStroke alt1 = KeyStroke.getKeyStroke(KeyEvent.VK_0, InputEvent.ALT_MASK);
		this.getRootPane().getInputMap().put(alt1, "rackTableInfo1");

		KeyStroke alt2 = KeyStroke.getKeyStroke(KeyEvent.VK_1, InputEvent.ALT_MASK);
		this.getRootPane().getInputMap().put(alt2, "rackTableInfo2");

		KeyStroke alt3 = KeyStroke.getKeyStroke(KeyEvent.VK_2, InputEvent.ALT_MASK);
		this.getRootPane().getInputMap().put(alt3, "rackTableInfo3");

		KeyStroke altp = KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.ALT_MASK);
		this.getRootPane().getInputMap().put(altp, "p3SelectionInfo");

		KeyStroke alts = KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.ALT_MASK);
		this.getRootPane().getInputMap().put(alts, "serverSelectionInfo");
		
		KeyStroke ctrF = KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK);
		this.getRootPane().getInputMap().put(ctrF, "find");
	}

	/**
	 * 
	 */
	private void initTop() {
		final int iw = (getInsets().left+getInsets().right);

		top.setAlignmentX(Component.LEFT_ALIGNMENT);
		top.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		top.setPreferredSize(new Dimension(FRAME_WIDTH-iw, TOOLBAR_HEIGHT+BANNER_HEIGHT));
		//top.setMinimumSize(new Dimension(MIN_FRAME_WIDTH, MIN_TOP_HEIGHT));
		//top.setMaximumSize(new Dimension(this.getWidth() - iw, MIN_TOP_HEIGHT));		
		top.setName("panel"); //$NON-NLS-1$
		top.setLayout(new BorderLayout(0, 0));
		top.setBorder(null);	
		initBanner();
		initToolbar();

		top.add(banner,  BorderLayout.NORTH);
		top.add(toolbar, BorderLayout.SOUTH);
	};

	/**
	 * 
	 */
	private void initBanner() {
		FlowLayout flowLayout = (FlowLayout) banner.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		
		banner.setStyle(Style.SCALED);
		banner.setRequestFocusEnabled(false);
		banner.setPaintBorderInsets(false);
		banner.setOpaque(false);
		banner.setBorder(null);
		banner.setImage(UISettings.BANNER_IMAGE); //$NON-NLS-1$
		final int iw = (getInsets().left+getInsets().right);
		banner.setPreferredSize(new Dimension(FRAME_WIDTH-iw, BANNER_HEIGHT));
		// Workaround for the banner not showing up on start:
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				banner.paintImmediately(0, 0, banner.getWidth(), banner.getHeight());
			}
		});
	}

	/**
	 * 
	 */
	private void initToolbar() {
		//toolbar.setBorderPainted(false);
		toolbar.setBorder(null);
		toolbar.setAlignmentY(Component.CENTER_ALIGNMENT);
		toolbar.setFloatable(false);
		// 60 - fixed size, move out into separate constant
		final int iw = (getInsets().left+getInsets().right);
		toolbar.setPreferredSize(new Dimension(FRAME_WIDTH-iw, TOOLBAR_HEIGHT));

		//toolbar.setMinimumSize(new Dimension(MIN_FRAME_WIDTH - iw, MIN_TOOLBAR_HEIGHT));
		//toolbar.setMaximumSize(new Dimension(this.getWidth() - iw, MIN_TOOLBAR_HEIGHT));
	}

	
	
	public JXPanel getPlugmeters() {
		return plugmetersPanel;
	}

	
	
	private PropertyChangeListener rackSizeListener = new PropertyChangeListener() {
		@Override
		public void propertyChange(final PropertyChangeEvent propertychangeevent) {
			//System.out.println(propertychangeevent.getPropertyName() + " "
				//	+ propertychangeevent.getNewValue().toString());
		}
	};
	
	/**
	 * 
	 * This function should detect the maximum frame size
	 * 
	 */
	void detectMaxSize() {
		JFrame.setDefaultLookAndFeelDecorated(true);
		final GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		this.setMaximizedBounds(ge.getMaximumWindowBounds());		
		this.setExtendedState(this.getExtendedState()|MAXIMIZED_BOTH);
		setMaximumSize(new Dimension(this.getWidth(), this.getHeight()));				
	}
	
	/**
	 * Adjust children sizes after parent frame resize
	 */
	void adjustChildrenSizes() {
		System.out.println("adjustChildrenSizes");
		final int iw = (getInsets().left+getInsets().right);
		final int ih = (getInsets().top+getInsets().bottom);		
		final int newWidth = this.getWidth() - iw;
		
		top.setSize(new Dimension(FRAME_WIDTH -iw, top.getHeight()));
		banner.setSize(new Dimension(FRAME_WIDTH-iw, banner.getHeight()));
		toolbar.setSize(new Dimension(FRAME_WIDTH -iw, toolbar.getHeight()));
		//System.out.println("top.getHeight()" + top.getHeight());
		//System.out.println("banner.getHeight()" + banner.getHeight());
		//System.out.println("toolbar.getHeight()" + toolbar.getHeight());
		//System.out.println("this.getHeight()" + this.getHeight());
		
		//System.out.println("before scrollpane_height:" + SCROLLPANE_HEIGHT);
		int new_scrollpane_height;
		//int maxRackTableHeight = SLOT_HEIGHT * cManager.getMaxRackUheight() + RACKTABLE_HEADER_HEIGHT + RACKTABLE_FOOTER_HEIGHT;
		int maxRackTableHeight = getMaxRackTableHeight();
		//System.out.println("maxRackTableHeight:" + maxRackTableHeight);
		int current_scrollpane_height = this.getHeight()- TITLEBAR_HEIGHT - BANNER_HEIGHT-TOOLBAR_HEIGHT;
		if(current_scrollpane_height>maxRackTableHeight)
			new_scrollpane_height = maxRackTableHeight;
		else{
			new_scrollpane_height = current_scrollpane_height;
			System.out.println("else");
		}
		//System.out.println("after scrollpane_height:" + new_scrollpane_height);	
		mainPane.setSize(new Dimension(FRAME_WIDTH-iw,  new_scrollpane_height));
		scrollpane.setSize(new Dimension(scrollpane.getWidth(),  mainPane.getHeight()));
		racks.setSize(racks.getWidth(), new_scrollpane_height);
		
		plugmetersPanel.setSize(new Dimension(P3TABLE_WIDTH, mainPane.getHeight()));
		
		//rack1.setSize(new Dimension(250, MIN_RACKBODY_HEIGHT));
		//rack2.setSize(new Dimension(250, MIN_RACKBODY_HEIGHT));
		//rack3.setSize(new Dimension(250, MIN_RACKBODY_HEIGHT));
	}
	
	public void setMainPaneHeight(){
		System.out.println("setMainPaneHeight()");
		final int iw = (getInsets().left+getInsets().right);
		final int ih = (getInsets().top+getInsets().bottom);		
		final int newWidth = this.getWidth() - iw;
		//System.out.println("main frame height:" + this.getHeight());
		//System.out.println("top height:" + top.getHeight());
		//System.out.println("main frame - top -1h height:" + (this.getHeight()-top.getHeight()-ih));
		
		//System.out.println("mainPane height:" + mainPane.getHeight());
		//System.out.println("scrollpane height:" + scrollpane.getHeight());
		//System.out.println("racks height:" + racks.getHeight());
				
		
		//int new_height = getMaxRackTableHeight(); 
		int max_table_height = cManager.getMaxRackUheight()* SLOT_HEIGHT;
		
		if(max_table_height<RACKTABLE_MIN_HEIGHT){
			max_table_height = RACKTABLE_MIN_HEIGHT;
		}
		
		System.out.println("max_table_height:" + max_table_height);
		int new_height = max_table_height+RACKTABLE_HEADER_HEIGHT+RACKTABLE_FOOTER_HEIGHT; 
		
		System.out.println("setMainPaneHeight");
		System.out.println("new_height:" + new_height);
		
		
		int height;
		// should not less than minimum height
		//if(new_height<RACKTABLE_MIN_HEIGHT){
		//	height = RACKTABLE_MIN_HEIGHT;
		//}else{
			if(new_height>(this.getHeight()-top.getHeight()-ih)){
				height = this.getHeight()-top.getHeight()-ih;
			}else{
				height = new_height;
			}
		//}
		System.out.println("height:" + height);
		mainPane.setSize(new Dimension(FRAME_WIDTH-iw, height));
		scrollpane.setSize(new Dimension(scrollpane.getWidth(),  mainPane.getHeight()));
		racks.setSize(racks.getWidth(), height);
		plugmetersPanel.setSize(new Dimension(P3TABLE_WIDTH, mainPane.getHeight()));
	}
	
	public int getMainPaneHeight(){
		return mainPane.getHeight();
	}
	
	public void setRackPanelsHeight(){
		System.out.println("setRackPanelsHeight");
		int height = cManager.getMaxRackUheight()* SLOT_HEIGHT;
		System.out.println("height:" + height);
		System.out.println("RACKTABLE_MIN_HEIGHT:" + RACKTABLE_MIN_HEIGHT);
		
		
		if(height<RACKTABLE_MIN_HEIGHT)
			height = RACKTABLE_MIN_HEIGHT;
			//return;
		rack1.setSize(new Dimension(rack1.getWidth(), height));
		rack2.setSize(new Dimension(rack2.getWidth(), height));
		rack3.setSize(new Dimension(rack3.getWidth(), height));
		
		RackTable rack1Table = ((RackPanel) rack1).getRackTable();
		RackTable rack2Table = ((RackPanel) rack2).getRackTable();
		RackTable rack3Table = ((RackPanel) rack3).getRackTable();
		
		rack1Table.setSize(rack1Table.getWidth(), height);
		rack2Table.setSize(rack2Table.getWidth(), height);
		rack3Table.setSize(rack3Table.getWidth(), height);	
		
		racks.setSize(racks.getWidth(), height + RACKTABLE_FOOTER_HEIGHT);
		
		//System.out.println("rack1 table height " + rack1Table.getHeight());
		//System.out.println("rack2 table height " + rack2Table.getHeight());
		//System.out.println("rack3 table height " + rack3Table.getHeight());
		
		rack1Table.repaint();
		rack2Table.repaint();
		rack3Table.repaint();
	}
	
	public int getMaxRackTableHeight(){
		int max = 0;
		
		ArrayList<RackPanel> rackPanels = new ArrayList<RackPanel>();
		rackPanels.add(rack1);
		rackPanels.add(rack2);
		rackPanels.add(rack3);
		
		for(RackPanel rackPanel:rackPanels){
			//System.out.println(rackPanel.getRackTable().getRack().getType() + " width:" + rackPanel.getRackTable().getSize().getWidth() + " height:" + rackPanel.getRackTable().getSize().getHeight() );
			if(max<rackPanel.getSize().getHeight())
				max = (int)rackPanel.getSize().getHeight();
		}
		//System.out.println("max:" + max);
		return max+RACKTABLE_HEADER_HEIGHT+RACKTABLE_FOOTER_HEIGHT;
		
	}
	
	private void loadConfiguration(){
		ConfigurationError err = io.downloadConfiguration();
		Configuration configuration = new Configuration();
		initDataBindings(configuration);
		
		
		if(err==null){

		}else{
			configuration = (Configuration)err.getObject();
			
			if(err.getInvalidity()==Variables.CONFIGURATION_NO_ERROR){
				initDataBindings(configuration);
				
			}else if(err.getInvalidity()==Variables.CONFIGURATION_ERROR_BUSY_CONTROLLER){
				MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_BUSY_CONTROLLER"));
				
				initDataBindings(configuration);
				
			}else{
				if(err.getInvalidity()==Variables.CONFIGURATION_ERROR_DISCONNECTED_CONTROLLER){
					
					String[] options = {"Retry", "Cancel"};
					
					int response = MessageHandler.showOptionMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_DOWNLOAD_TRY_MSG"),options);
					if(response == JOptionPane.YES_OPTION){
						err = io.downloadConfiguration();
						configuration = (Configuration)err.getObject();
						if(err.getInvalidity()==Variables.CONFIGURATION_ERROR_DISCONNECTED_CONTROLLER){
							MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_NO_CONTROLLER"));
						}else if(err.getInvalidity()==Variables.CONFIGURATION_NO_ERROR){
							initDataBindings(configuration);
						}
					}
				}else if(err.getInvalidity()==Variables.CONFIGURATION_ERROR_TRANSFER_CONFIGURATION){
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_UPLOAD_FAIL_MSG"));
				}
			}
			
		}

	}
	
	public void initDataBindings(Configuration configuration) {
		if(configuration==null)
			return;
		cManager.updataConfiguration(configuration);
		toolbar.setConfiguration(configuration, io);
		
//		System.out.println("-----------------------");
//		System.out.println(mainPane.getHeight());
//		System.out.println(scrollpane.getHeight());
//		System.out.println(rack1.getHeight());
//		System.out.println(rack2.getHeight());
//		System.out.println(rack3.getHeight());
//		System.out.println(racks.getHeight());
	}
	
	/*
	public void maximizeScreen(){
		scrollpane_height = MAX_SCROLLPANE_HEIGHT;

		final int iw = (getInsets().left+getInsets().right);
		final int newWidth = this.getWidth() - iw;
		
		top.setSize(new Dimension(newWidth, MIN_TOP_HEIGHT));
		banner.setSize(newWidth, MIN_BANNER_HEIGHT);
		banner.setImage(UISettings.BANNER_SMALL_IMAGE);
		
		banner.paintImmediately(0, 0, banner.getWidth(), banner.getHeight());
		
		toolbar.setSize(new Dimension(newWidth, toolbar.getHeight()));
		top.validate();
		
		mainPane.validate();
		this.validate();
		adjustChildrenSizes();
	}

	public void minimizeScreen(){
		scrollpane_height = MIN_SCROLLPANE_HEIGHT;
		final int iw = (getInsets().left+getInsets().right);
		final int newWidth = this.getWidth() - iw;
		
		top.setSize(new Dimension(newWidth, MAX_TOP_HEIGHT));
		banner.setSize(newWidth, MAX_BANNER_HEIGHT);
		banner.setImage(UISettings.BANNER_IMAGE);
		banner.paintImmediately(0, 0, banner.getWidth(), banner.getHeight());
		toolbar.setSize(new Dimension(newWidth, toolbar.getHeight()));
		top.validate();
		mainPane.validate();
		this.validate();
		adjustChildrenSizes();
	}*/
	
}
