package com.synapsense.p3.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.CellEditor;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultListSelectionModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EtchedBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.text.JTextComponent;

import org.jdesktop.swingx.JXTable;

import com.synapsense.p3.components.PlugmeterButton;
import com.synapsense.p3.components.ServerPanel;
import com.synapsense.p3.model.Configuration;
import com.synapsense.p3.model.Jawa;
import com.synapsense.p3.model.Server;
import com.synapsense.p3.utilities.CommonFunctions;
import com.synapsense.p3.utilities.ComponentsManager;
import com.synapsense.p3.utilities.IDFormat;
import com.synapsense.p3.utilities.MessageHandler;
import com.synapsense.p3.utilities.SelectionChangeEvent;
import com.synapsense.p3.utilities.SelectionChangeListener;
import com.synapsense.p3.utilities.SelectionManager;

import com.synapsense.p3.utilities.StringConstants;
import com.synapsense.p3.utilities.Validator;
import com.synapsense.p3.utilities.Variables;

import org.apache.log4j.Logger;

public class PlugmetersTable extends SynapJXTable  implements SelectionChangeListener{
	private static Logger logger = Logger.getLogger(PlugmetersTable.class.getName());
	
	private PlugmetersTableModel plugmeterTableModel;
	private ComponentsManager cManager;
	private Configuration configuration;
	private SelectionManager selectionManager;
	private static final int DEF_PANEL_HEIGHT = StringConstants.getIntValue("P3TABLE_HEIGHT");
	private static final int DEF_PANEL_WIDTH = StringConstants.getIntValue("P3TABLE_WIDTH");
	
	public PlugmetersTable(ComponentsManager cManager, SelectionManager selectionManager){
		super(cManager, selectionManager);
		this.cManager = cManager;
		this.selectionManager = selectionManager;
		plugmeterTableModel = new PlugmetersTableModel(this);
		initTable();
		
	}
	
	public void setConfiguration(Configuration config){
		configuration = config;
	}
	
	/**
	 *  Initialize plugmeter list table
	 */
	private void initTable() {
		int PREF_WIDTH_COL0 = 30;
		int PREF_WIDTH_COL1 = 100;
		int PREF_WIDTH_COL2 = 30;
		
		setCellSelectionEnabled(false);
		setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		setFont(new Font("Courier New", Font.PLAIN, 12)); //$NON-NLS-1$
		setBackground(Color.WHITE);
		setForeground(Color.BLACK);
		setSelectionBackground(Color.LIGHT_GRAY);
		setSelectionForeground(Color.BLACK);
		//table.setPreferredScrollableViewportSize(new Dimension(0, 200));
		setModel(plugmeterTableModel);
		getColumnModel().getColumn(0).setResizable(false);
		getColumnModel().getColumn(0).setPreferredWidth(PREF_WIDTH_COL0);
		getColumnModel().getColumn(1).setResizable(false);
		getColumnModel().getColumn(1).setPreferredWidth(PREF_WIDTH_COL1);
		getColumnModel().getColumn(2).setResizable(false);
		getColumnModel().getColumn(2).setPreferredWidth(PREF_WIDTH_COL2);
		setVisibleRowCount(10);
		setVisibleColumnCount(3);		
		//plugmetersTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setRowSelectionAllowed(true);
		setCellSelectionEnabled(true);
		setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		setRowHeightEnabled(false);
		setColumnControlVisible(true);
		setDefaultRenderer(PlugmeterButton.class, new ValidationRenderer());
		setDefaultRenderer(Integer.class, new ComboBoxRenderer());
		setDefaultEditor(Integer.class, new ComboBoxEditor());
		setDefaultEditor(String.class, new TextFieldCellEditor(new TextFieldCell(this)));
		//if(this.getParent()!=null)
		setPreferredSize(new Dimension( DEF_PANEL_WIDTH, DEF_PANEL_HEIGHT));
		setRowSorter(null);
		putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

		setSelectionModel(new PlugmeterListSelectionModel(this));
		
		//addKeyListener(new FunctionKeyAdapter(cManager));
		addKeyListener(new KeyAdapter(){
			public void keyPressed(KeyEvent evt){
				if(evt.getKeyCode()==KeyEvent.VK_ENTER){
					TableCellEditor tce = getCellEditor();
					if (tce != null) {
						tce.stopCellEditing();
					}
				}else if(evt.getKeyCode()== KeyEvent.VK_DELETE){
					//when press delete key, try to delete selected smart plug instead of deleting id
					cManager.deleteSelections();
				}
			}
		});
		getSelectionModel().addListSelectionListener(new PlugmeterSelectionListener(this));
		
		JTableHeader header = this.getTableHeader();
		header.addMouseListener(new TableHeaderMouseAdapter(this));
	}
	
	public Component prepareEditor(TableCellEditor editor, int row, int column){
		Component c = super.prepareEditor(editor, row, column);
	    if (c instanceof JTextComponent) {
	        ((JTextComponent) c).selectAll();
	    } 
	    return c;
	}
	
	public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend){
		
		if(plugmeterTableModel.isCellEditable(rowIndex, columnIndex)){
			super.changeSelection(rowIndex, columnIndex, toggle, extend);
		}else{
			super.changeSelection(rowIndex, columnIndex + 1, toggle, extend);
		}
	}
	
	
	public class PlugmetersTableModel extends DefaultTableModel{
		private Class[] columnTypes = new Class[] {PlugmeterButton.class, String.class, Integer.class};
		private final String[] columnNames = {"", StringConstants.getString("PLUGMETER_ID"), StringConstants.getString("PLUGMETER_RPDU")};
		private boolean[] columnEditables = new boolean[] {false, true, true};
		private ServerPanel serverOnRack;
		private PlugmetersTable table;
		private Vector p3Vector = new Vector();
		private int sortKey = 1;
		private HashMap<Integer, Boolean> isAscMap = new HashMap<Integer, Boolean>(){{put(1, true);put(2, true);}};
		
		public PlugmetersTableModel(PlugmetersTable table){
			this.table = table;
		}
		
		public void bindData(ServerPanel sor){
			serverOnRack = sor;
			p3Vector.removeAllElements();
			if(serverOnRack!=null){
				for(PlugmeterButton  pb: sor.getPlugmeters()){
					p3Vector.addElement(pb);
				}				
			}
			
			//sortByColumn(sortKey, isAscMap.get(sortKey));
		}
		
		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		@Override
		public int getRowCount() {
			if(serverOnRack==null || serverOnRack.getPlugmeters().isEmpty() )
				return 0;
			else
				return p3Vector.size();
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			if(columnIndex==0){
				return null;
			}else if(columnIndex==1){
				return ((Jawa)((PlugmeterButton)p3Vector.get(rowIndex)).getJawa()).getId();
				//return serverOnRack.getPlugmeters().get(rowIndex).getJawa().getId();
				
			}else if(columnIndex ==2){
				return ((Jawa)((PlugmeterButton)p3Vector.get(rowIndex)).getJawa()).getFeed();
				//return serverOnRack.getPlugmeters().get(rowIndex).getJawa().getFeed();
			}
			
			return null;
		}
		
		public PlugmeterButton getPlugmeter(int row){
			
			if(serverOnRack==null || row<0 || row>=this.getRowCount())
				return null;
			
			if(p3Vector.size()==0)
				return null;
			else
				return (PlugmeterButton)p3Vector.get(row);
		}
		
		public void setValueAt(Object value, int row, int column){
			System.out.println(value);
			System.out.println(table.getValueAt(row, column));
			System.out.println("table is editing:" );
			if(value.equals(table.getValueAt(row, column)))
				return;
			
			PlugmeterButton pm = getPlugmeter(row);
			boolean validValue = false;
			
			if(pm==null)
				return;
			if(column ==1){
				if(pm.getPhase()>0){
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_P3_PROHIBITED_RESCAN_ID_MSG"));
					return;
				}
				
				String oldValue = (String) getValueAt(row,column);
				
				String newValue = "";
				String strValue = ((String)value).toUpperCase();
				//String strValue = (String)value;
				
				if(Validator.checkScannerCodeValue(strValue)){
					try{
						logger.info("User scan barcode:" + value + " for plugmeter Id");
						
						long longValue = Long.parseLong(strValue,16);
						logger.info("Long formatted value scanned or entered is " + longValue );
						newValue = IDFormat.codeToHr(longValue);
						logger.info("Human readable value is " + newValue);
					}catch(NumberFormatException nfe){
						//MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_P3_INVALID_FORMAT_ID_MSG"));
						logger.error("Wrong formatted ID, Cannot convered it");
						newValue = strValue;
					}
				}else{
					logger.info("entered some value and the value is not from barcode scanning " + strValue);
					newValue = strValue;
				}
				//System.out.println("newValue" + newValue);
				//System.out.println("oldValue" + oldValue);

				if(Validator.checkUniqueJawaId(newValue, pm.getJawa(), configuration)){
					pm.setId(newValue);
					validValue = true;
				}else{
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_P3_UNIQUE_ID_MSG"));
				}
				
				if(pm.getJawa().getInvalidities().contains(Variables.CONFIGURATION_ERROR_P3_UNIQUEID)){
					Validator.updateUniquePlugmeterIdValidation(configuration);
				}
			}else if(column==2){
				if(pm.getPhase()>0){
					int response = MessageHandler.showConfirmationMessage(StringConstants.getString("VALIDATION_P3_CONFIRM_REMOVE_PHASE_MSG"));
					
					if(response == JOptionPane.YES_OPTION){
						if(!Validator.checkDuplicateRpduPhaseAssocation(pm.getJawa(), (Integer)value)){
							pm.dissociatePhase();
						}
							
						pm.setRPDU((Integer)value);
						validValue = true;
						if(!Validator.checkJawaFeed(pm.getJawa())){
							pm.dissociatePhase();
							//cManager.removePhasePlugmeter();
						}
						
						
					}else{
						return;
					}
				}else{
					pm.setRPDU((Integer)value);
					validValue = true;
				}
				CommonFunctions.redeployPlugmeters(serverOnRack);
			}
			if(validValue){
				ArrayList<ServerPanel> selectedServers = (ArrayList<ServerPanel>) selectionManager.getSelectedServers().clone();
				ArrayList<PlugmeterButton> selectedPlugmeters = (ArrayList<PlugmeterButton>) selectionManager.getSelectedPlugmeters().clone();
				
				selectionManager.setServerSelection(selectedServers, null);
				selectionManager.setPlugmeterSelection(selectedPlugmeters, null);
				
			}
		}
		
		public String getColumnName(int col) {
			return columnNames[col];
		}

		public Class getColumnClass(int col) {
			return columnTypes[col];
		}
		
		public boolean isCellEditable(int row, int column) {
			return columnEditables[column];
		}
		
		public void addRow(Server server, PlugmeterButton pm){
			serverOnRack.getPlugmeters().add(pm);
		}
		
		public void removeRow(int row){
			PlugmeterButton pm = getPlugmeter(row);
			serverOnRack.deletePlugmeter(pm);
			super.fireTableDataChanged();
		}
		
		public ServerPanel getServerOnRack(){
			return serverOnRack;
		}
		
		public int getRowByJawaIndex(int index){
			if(p3Vector!=null){
				for(int i=0;i< p3Vector.size();i++){
					PlugmeterButton pm = (PlugmeterButton)p3Vector.get(i);
					if(pm.getJawa().getIndex() == index)
						return i;
				}
			}
			return -1;
		}
		
		public int getRowByPlugmeterButton(PlugmeterButton pb){
			if(serverOnRack==null){
				return -1;
			}
			for(int i=0;i<p3Vector.size();i++){
				if(p3Vector.get(i).equals(pb)){
					return i;
				}
				
			}
			return -1;
		}
		
		public void sortByColumn(int column, boolean isAsc){
			sortKey = column;
			isAscMap.put(sortKey, isAsc);
			
			if(column==1){
				Collections.sort(p3Vector, new PlugmeterIdComparator(isAsc));
			}else if(column==2){
				Collections.sort(p3Vector, new PlugmeterRPDUComparator(isAsc));
			}
			
			table.tableChanged(new TableModelEvent(this));
			table.repaint();
			
			//ArrayList<PlugmeterButton> selectedPlugmeters = (ArrayList<PlugmeterButton>) selectionManager.getSelectedPlugmeters().clone();
			//selectionManager.setServerSelection(serverOnRack, null);
			//selectionManager.setPlugmeterSelection(selectedPlugmeters, null);
		}
	}
	
	class PlugmeterListSelectionModel extends DefaultListSelectionModel {
		private JXTable table;
		public PlugmeterListSelectionModel(JXTable table){
			super();
			this.table = table;
		}
		
		public void addSelectionInterval(int index0, int index1){
			super.addSelectionInterval(index0, index1);
			//System.out.println("addSelectionInterval " + index0 + " " + index1);
			selectionManager.addPlugmeterSelection(plugmeterTableModel.getPlugmeter(index0), (SelectionChangeListener)table);
		}
		
		public void	removeSelectionInterval(int index0, int index1){
			super.removeSelectionInterval(index0, index1);
			//System.out.println("removeSelectionInterval " + index0 + " " + index1);
			selectionManager.removePlugmeterSelection(plugmeterTableModel.getPlugmeter(index0), (SelectionChangeListener)table);
		}
		
		public void setSelectionInterval(int index0, int index1) {
			super.setSelectionInterval(index0, index1) ;
			int first, last;
			if(index0<index1){
				first = index0;
				last = index1;
			}else{
				first = index1;
				last = index0;
			}
			
			ArrayList<PlugmeterButton> selection = new ArrayList<PlugmeterButton>();
			
			
			for(int i=first;i<=last;i++)
				selection.add(plugmeterTableModel.getPlugmeter(i));
			
			selectionManager.setServerSelection(plugmeterTableModel.serverOnRack, (SelectionChangeListener)table);
			selectionManager.setPlugmeterSelection(selection ,(SelectionChangeListener)table);
			
			//System.out.println("removeSelectionInterval " + index0 + " " + index1);
		}
	}
	
	class PlugmeterSelectionListener implements ListSelectionListener{
		private PlugmetersTable table;
		
		
		public PlugmeterSelectionListener(PlugmetersTable table){
			this.table = table;
		}
		
		@Override
		public void valueChanged(ListSelectionEvent e) {
			/*ListSelectionModel lsm = (ListSelectionModel) e.getSource();
					
			int firstIndex = e.getFirstIndex();
			int lastIndex = e.getLastIndex();
			boolean isAdjusting = e.getValueIsAdjusting();
			
			ArrayList<PlugmeterButton> selectedPlugmeters = new ArrayList<PlugmeterButton>();
			ServerPanel selectedServer = plugmeterTableModel.serverOnRack;
			if(isAdjusting){
				System.out.println(" -------------- " );
				if (lsm.isSelectionEmpty()) {
					System.out.println(" <none>");
				}else{
				// Find out which indexes are selected.
					int minIndex = lsm.getMinSelectionIndex();
					int maxIndex = lsm.getMaxSelectionIndex();
					
					System.out.println("minIndex:" + minIndex);
					System.out.println("maxIndex:" + maxIndex);
					
					for (int i = minIndex; i <= maxIndex; i++) {
						if (lsm.isSelectedIndex(i)) {
							selectedPlugmeters.add(plugmeterTableModel.getPlugmeter(i));
							System.out.println(" selected " + i);
						}else{
							System.out.println(" unselected " + i);
						}
					}
					selectionManager.setServerSelection(selectedServer, table);
					selectionManager.setPlugmeterSelection(selectedPlugmeters, table);
				}
			}		
			
			

			//cManager.reloadRackDisplay();
			*/
		}
		
	}
	
	class ValidationRenderer extends DefaultTableCellRenderer {
		public ValidationRenderer(){
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			JLabel label = (JLabel)super.getTableCellRendererComponent( table, value, isSelected, hasFocus, row, column  ); 
			PlugmeterButton pm = plugmeterTableModel.getPlugmeter(row);
			if(pm!=null){
				Jawa jawa = pm.getJawa();
		
				if(jawa.getInvalidities().size()>0){
					label.setIcon(new ImageIcon(PlugmetersPanel.class.getResource(StringConstants.getString("IMAGE_INVALID_SERVER"))));
				}else{
					label.setIcon(null);
				}
			}else{
				label.setIcon(null);
			}
			return label;
		}
	}
	
	class ComboBoxRenderer extends JComboBox implements TableCellRenderer  {

		public ComboBoxRenderer(){
			super(new Integer[] {1,2,3,4,5,6,7,8});
			this.setEditable(true);
			
			
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			
			PlugmeterButton pm = plugmeterTableModel.getPlugmeter(row);
			
			if(pm!=null){
				Jawa jawa = pm.getJawa();
				this.setSelectedIndex(jawa.getFeed()-1);
			}
			
			return this;
		}
	}
	
	class ComboBoxEditor extends DefaultCellEditor {
		public ComboBoxEditor(){
			super(new JComboBox(new Integer[]{1,2,3,4,5,6,7,8}));
		}
	}
	
	class TextFieldCell extends JTextField {
		
		public TextFieldCell(final JTable table){
			super();
						
			this.addFocusListener(new FocusListener(){
				@Override
				public void focusGained(FocusEvent arg0) {
					System.out.println("focusGain" + table.getSelectedRow());
				}

				@Override
				public void focusLost(FocusEvent e) {
					System.out.println("focusLost" + table.getSelectedRow());
					/*CellEditor cellEditor = table.getCellEditor();
					if(table.getSelectedRow()==-1)
						return;
					
					if(cellEditor!=null){
						if(cellEditor.getCellEditorValue()!=null){
							cellEditor.stopCellEditing();
						}else{
							cellEditor.cancelCellEditing();
						}
					}
					*/
				}
				
			});
		}
		
		public String getValue(){
			return this.getText();
		}
	}
	
	class TextFieldCellEditor extends DefaultCellEditor{
		private JTextField textfield;
		private JTable table;
		
		public TextFieldCellEditor(final JTextField textfield) {
			super(textfield);
			this.textfield = textfield;
		}
		
		public Component getTableCellEditor(JTable table, Object value, boolean isSelected, int row, int column){
			Component c = super.getTableCellEditorComponent(table, value, isSelected, row, column);
			
			if(isSelected){
				System.out.println("isSelected true");
				textfield.selectAll();
			}
			return c;
		}
		
		public Component getComponent(){
			return textfield;
		}
	}
	
	class TableHeaderMouseAdapter extends MouseAdapter{
		private JXTable table;
		private boolean isAscending = true;
		
		public TableHeaderMouseAdapter(JXTable table){
			this.table = table;
		}
		
		public void mouseClicked(MouseEvent e){
			TableColumnModel colModel = table.getColumnModel();
			int colModelIndex = colModel.getColumnIndexAtX(e.getX());
			isAscending = !isAscending;
			
			PlugmetersTableModel model =(PlugmetersTableModel) table.getModel();
			model.sortByColumn(colModelIndex, isAscending);
		}
	}
	
	class PlugmeterIdComparator implements Comparator{
		protected boolean isAsc;
		
		public PlugmeterIdComparator(boolean isAsc){
			this.isAsc = isAsc;
		}
		
		@Override
		public int compare(Object obj1, Object obj2) {
			if(!(obj1 instanceof PlugmeterButton) || !(obj2 instanceof PlugmeterButton)){
				return 0;
			}
			
			Jawa jawa1 = ((PlugmeterButton)obj1).getJawa();
			Jawa jawa2 = ((PlugmeterButton)obj2).getJawa();
			
			String id1 = jawa1.getId();
			String id2 = jawa2.getId();
			
			int result = 0;
			
			result = id1.compareTo(id2);
			if(!isAsc)
				result = -result;
			
			return result;
		}
	}
	
	class PlugmeterRPDUComparator implements Comparator{
		protected boolean isAsc;
		
		public PlugmeterRPDUComparator(boolean isAsc){
			this.isAsc = isAsc;
		}
		@Override
		public int compare(Object obj1, Object obj2) {
			if(!(obj1 instanceof PlugmeterButton) || !(obj2 instanceof PlugmeterButton)){
				return 0;
			}
			
			Jawa jawa1 = ((PlugmeterButton)obj1).getJawa();
			Jawa jawa2 = ((PlugmeterButton)obj2).getJawa();

			
			Integer rpdu1 = jawa1.getFeed();
			Integer rpdu2 = jawa2.getFeed();
			
			int result = 0;
			result = rpdu1.compareTo(rpdu2);
			
			if(!isAsc)
				result = -result;
			
			return result;
		}
		
	}

	@Override
	public void selectionChanged(SelectionChangeEvent e) {
		

		ArrayList<ServerPanel> selectedServers = selectionManager.getSelectedServers();
		
		if(selectedServers.size()==1){
				
			ServerPanel currentServer = selectedServers.get(0);

			if(currentServer.getServer().getJawas().size() ==0){
						
			}else{
				plugmeterTableModel.bindData(currentServer);
				plugmeterTableModel.fireTableDataChanged();
			}
						
		}else{
			plugmeterTableModel.bindData(null);
			plugmeterTableModel.fireTableDataChanged();
		}
		
		ArrayList<PlugmeterButton> selectedP3s = new ArrayList<PlugmeterButton>();
		ArrayList<Object> selections = e.getAddtions();
		
		for(Object o:selections){
			if(o instanceof PlugmeterButton){
				int row = plugmeterTableModel.getRowByPlugmeterButton((PlugmeterButton)o);
				if(row>-1){
					this.addRowSelectionInterval(row, row);
					
					selectedP3s.add((PlugmeterButton)o);
				}
			}
		}
		
		ArrayList<Object> deselections = e.getRemovals();
		
		for(Object o:deselections){
			if(o instanceof PlugmeterButton){
				int row = plugmeterTableModel.getRowByPlugmeterButton((PlugmeterButton)o);
				if(row>-1){
					if(row<this.getRowCount())
						this.removeRowSelectionInterval(row, row);
				}
			}
		}
		
		// please focus to id
		if(selectedP3s.size()==1){
			System.out.println("one selection");
			int row = plugmeterTableModel.getRowByPlugmeterButton(selectedP3s.get(0));
			this.changeSelection(row, 1, false, false);
			this.requestFocus();
		}
	}
}
