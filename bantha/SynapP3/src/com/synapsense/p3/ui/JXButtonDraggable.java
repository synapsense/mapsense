package com.synapsense.p3.ui;

import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.event.MouseEvent;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXButton;
import org.jdesktop.swingx.JXTable;

public class JXButtonDraggable extends JXButton implements DragGestureListener, DragSourceListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(JXButtonDraggable.class.getName());
	
	DragSource dragSource;
	
	public JXButtonDraggable() {
		super();
		dragSource = new DragSource();
        dragSource.createDefaultDragGestureRecognizer(
                this, DnDConstants.ACTION_MOVE, this);
		setCursor(UISettings.CURSOR_HAND_PAN);    
	}
	
	@Override
	public void dragDropEnd(DragSourceDropEvent evt) {
		// custom server needs to be resized
		//if(this.getRackType()==RackTypes.SRVCUSTOM){
			
			/*JXTable rackTable1 = ((RackPanel)(MainScreen.getRackPanelByType(1))).table;
			JXTable rackTable2 = ((RackPanel)(MainScreen.getRackPanelByType(2))).table;
			JXTable rackTable3 = ((RackPanel)(MainScreen.getRackPanelByType(3))).table;
			
			JXTable targetTable;
			
			if(rackTable2.getLocationOnScreen().x >evt.getLocation().x)
				targetTable = rackTable1;
			else if(rackTable3.getLocationOnScreen().x >evt.getLocation().x)
				targetTable = rackTable2;
			else
				targetTable = rackTable3;
			
			targetTable.dispatchEvent(new MouseEvent(targetTable, MouseEvent.MOUSE_CLICKED, 1000, 0, evt.getLocation().x -targetTable.getLocationOnScreen().x, evt.getLocation().y -targetTable.getLocationOnScreen().y - 18*5, 2, false, 1));
			*/
		//}
		// Called when the user finishes or cancels the drag operation.	
	    
	}

	@Override
	public void dragEnter(DragSourceDragEvent arg0) {
		// Called when the user is dragging this drag source and enters
        // the drop target.
	}

	@Override
	public void dragExit(DragSourceEvent arg0) {
		// Called when the user is dragging this drag source and leaves
        // the drop target.
	}

	@Override
	public void dragOver(DragSourceDragEvent arg0) {
		 this.getModel().setRollover(false);
		 // Called when the user is dragging this drag source and moves
        // over the drop target.
	}

	@Override
	public void dropActionChanged(DragSourceDragEvent arg0) {
		// Called when the user changes the drag action between copy or move.		
	}

	@Override
	public void dragGestureRecognized(DragGestureEvent evt) {
        Transferable t = new StringSelection(this.getRackType().toString());
        dragSource.startDrag(evt, DragSource.DefaultCopyDrop, t, this);		
        //System.out.println("dragGestureRecognized");
	}
	
	public enum RackTypes {
		PLUGMETER,
		SRV1U,
		SRV2U,
		SRV3U,
		SRV4U,
		SRV5U,
		SRVCUSTOM,
		SRV1U1P,
		SRV1U2P
	}

	public RackTypes rackType;
	
	public RackTypes getRackType() {
		return rackType;
	}

	public void setRackType(RackTypes rackType) {
		this.rackType = rackType;
	}
}
