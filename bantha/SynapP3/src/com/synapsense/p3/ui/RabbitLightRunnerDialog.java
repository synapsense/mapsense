package com.synapsense.p3.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import org.apache.log4j.Logger;

import com.synapsense.p3.scinter.BusySandCrawlerException;
import com.synapsense.p3.scinter.NoSandCrawlerException;
import com.synapsense.p3.scinter.SCConfigMessage;
import com.synapsense.p3.scinter.SCConfigService;
import com.synapsense.p3.scinter.SandCrawler;
import com.synapsense.p3.utilities.MessageHandler;
import com.synapsense.p3.utilities.StringConstants;

public class RabbitLightRunnerDialog extends JDialog{
private static Logger logger = Logger.getLogger(CommandRunnerDialog.class.getName());
	
	private JProgressBar progressBar = new JProgressBar();
	private JButton cancelButton;
	private JLabel statusLabel;
	
	private volatile boolean keepRunning;
	private volatile int currentQ;
	private int totalQues = 18;
	private int rackId =0;
	private SandCrawler sandCrawler;
	private SCConfigMessage msg = null;
	private SCConfigService configService;
	
		
	public RabbitLightRunnerDialog(JFrame parent, int rackId, SandCrawler sandCrawler){
		super(parent, StringConstants.getString("VALIDATION_INFORMATION_TITLE"), true);
		
		this.sandCrawler = sandCrawler;
		configService = sandCrawler.getConfigService();
		keepRunning = true;
		this.rackId = rackId;
		
		buildComponents();
		
		this.setLocationRelativeTo(parent);
		
		Runnable run = new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				executeOutletOrder();
			}
			
		};
		Thread thread = new Thread(run, "progressBarThread");
		thread.start();	
		
	}
	
	private void buildComponents(){
		JPanel panel = new JPanel();
		panel.setLayout(null);
		
		
		progressBar.setMinimum(0);
		progressBar.setMaximum(totalQues);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		progressBar.setIndeterminate(false);
		progressBar.setBounds(10, 10, 300, 20);
		
		statusLabel = new JLabel();
		statusLabel.setText(StringConstants.getString("VALIDATION_OUTLETORDER_STATUS_MSG1"));
		statusLabel.setBounds(10, 30, 300, 30);
		
		
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				cancelCommand();
			}
			
		});
		cancelButton.setVisible(false);
		cancelButton.setBounds(240, 60, 70, 25);
		
		panel.add(progressBar);
		panel.add(statusLabel);
		panel.add(cancelButton);
		getContentPane().add(panel);
		
		setSize(330,125);
	}
	
	private void updateRabbitLight(int sec){
		String statusStr = StringConstants.getString("VALIDATION_OUTLETORDER_STATUS_MSG2") + sec + " seconds";
		statusLabel.setText(statusStr);
	}
	
	private void cancelCommand(){
		//cancelButton.setEnabled(false);
		try {
			Thread.currentThread().sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		keepRunning = false;
		this.setVisible(false);
	}
	
	private void informStatus(Exception e){
		if(e.getClass().equals(NoSandCrawlerException.class)){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_NO_CONTROLLER"));
			logger.error("Cannot reset plugmeters because no controller");
		}else if(e.getClass().equals(IOException.class)){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_ERROR_ACTIVATE_P3_MSG"));
			logger.error("cannot identify plugmeters");
		}else if(e.getClass().equals(BusySandCrawlerException.class)){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_BUSY_CONTROLLER"));
			logger.error("Cannot issue command because the controller is busy");
			
		}
	}
	
	private void informStatus(int errorCode){
		if(errorCode==SCConfigMessage.TYPE_COMRESP_FAILURE){
			// command was not sent successfully.
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_COMMAND_0"));
			logger.error("command was not sent successfully.");
		}else if(errorCode == SCConfigMessage.TYPE_COMRESP_PENDING){
			// rabbit light is already pennding 
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_COMMAND_1"));
		}else if(errorCode == SCConfigMessage.TYPE_COMRESP_FULL){
			// command was not accepted becuase SC queue was full
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_COMMAND_2"));
		}else if(errorCode == SCConfigMessage.TYPE_BUSYSMOTA){
			// the controller is busy because of SMOTA mode
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_BUSY_CONTROLLER"));

		}
	}
	
	private void executeOutletOrder(){
		
		try {
			System.out.println("rack id:" + rackId);
			sandCrawler.sendRabbitLights(rackId);
		} catch (NoSandCrawlerException e) {
			cancelCommand();
			informStatus(e);
			return;
		} 
		
		int retry = 100;
		
		while(keepRunning){
			if(msg ==null){
				System.out.println("no response yet");
				
				if (retry-- <= 0) {
					keepRunning = false;
				}
				
				try {
					msg = configService.receive();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					cancelCommand();
					informStatus(e);
				} catch (NoSandCrawlerException e) {
					// TODO Auto-generated catch block
					cancelCommand();
					informStatus(e);
				}
			}else{
				if(msg.type == SCConfigMessage.TYPE_R_COMMAND){
					System.out.println(" command response is " + msg.commandresponse);
					
					if(msg.commandresponse == 1){
						cancelButton.setVisible(true);
						
						int count = 0;
						
						while(count<totalQues){
							
							try {
								Thread.currentThread().sleep(1000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							count++;
							progressBar.setValue(count);
							updateRabbitLight(totalQues-count);
							
						}
						
						keepRunning = false;
						
						cancelCommand();
					}else {
						cancelCommand();
						informStatus(msg.commandresponse);
						
					}
					
				}
			}
		}
	}
}
