package com.synapsense.p3.ui;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.net.URL;
import java.awt.DisplayMode;
import javax.swing.JComponent;

import com.synapsense.p3.utilities.StringConstants;

/**
 * 
 * @author golovanov
 *
 */
public class UISettings extends JComponent {
	
	/**
	 * 
	 * @return
	 */
	public static Dimension getScreenSize() {
		DisplayMode dMode= null;
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();	
		GraphicsDevice gd = gs[0]; // gc.getDevice();
		dMode = gd.getDisplayMode();
		int width = dMode.getWidth();
		int height = dMode.getHeight();
		return new Dimension(width, height);
	}
	
	/**
	 * 
	 * @return
	 */
	public static Dimension getDesktopSize() {
		Dimension result = new Dimension(0,0);		
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		try {
			Rectangle r = ge.getMaximumWindowBounds();
			result.width  = r.width;
			result.height = r.height;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	public static Font      DEFAULT_FONT    = new Font("Lucida Sans", Font.PLAIN, 11);	
	public static Toolkit   TOOLKIT         = Toolkit.getDefaultToolkit();	
	public static Dimension SCREENSIZE      = getDesktopSize();
	public static Cursor    CURSOR_HAND_PAN = getCursorHandPan();
	public static Image     BANNER_IMAGE    = TOOLKIT.getImage(UISettings.class.getResource(StringConstants.getString("NEW_IMAGE_BANNER")));
	public static Image     BANNER_SMALL_IMAGE    = TOOLKIT.getImage(UISettings.class.getResource(StringConstants.getString("SMALL_IMAGE_BANNER")));
	
	public static int       BANNER_WIDTH    = BANNER_IMAGE.getWidth(null);	
	public static int       BANNER_HEIGHT   = BANNER_IMAGE.getHeight(null);
	/**
	 * 
	 * @return
	 */
	static Cursor getCursorHandPan() {
		URL url = UISettings.class.getResource(StringConstants.getString("IMAGE_CURSOR_HANDOPEN"));		
		return TOOLKIT.createCustomCursor(
			TOOLKIT.createImage(url),
			new Point(0,0),
			"CURSOR_HAND_PAN");
	}	
}
