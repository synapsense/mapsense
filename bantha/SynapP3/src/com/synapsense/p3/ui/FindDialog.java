package com.synapsense.p3.ui;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import com.synapsense.p3.components.PlugmeterButton;
import com.synapsense.p3.utilities.ComponentsManager;
import com.synapsense.p3.utilities.IDFormat;
import com.synapsense.p3.utilities.SelectionManager;
import com.synapsense.p3.utilities.StringConstants;

/**
 * A Dialgo to find Jawa by MacID
 * @author HLim
 *
 */
public class FindDialog extends JDialog{
	private static Logger logger = Logger.getLogger(FindDialog.class.getName());
	private ComponentsManager cManager;
	private SelectionManager selectionManager;
	private JLabel resultLabel = new JLabel("");
	
	/**
	 * Constructor
	 * @param cManager
	 * @param selectionManager
	 */
	public FindDialog(ComponentsManager cManager, SelectionManager selectionManager){
		super(MainScreen.getFrames()[0], "Find", true);
		this.cManager = cManager;
		this.selectionManager = selectionManager;
		
		buildComponents();
	}
	
	/**
	 * build components: find label, keyword textfield, find and close buttons
	 */
	private void buildComponents(){
		setIconImage(new ImageIcon(MainScreen.class.getResource("/resources/banner/appicon.png")).getImage());
		
		// mainPanel include northpanel and southpanel
		// northpanel is for lable and keyword textfield
		// sothpanel is for buttons
		JPanel mainPanel = new JPanel();
		JPanel northPanel = new JPanel();
		JPanel southPanel = new JPanel();
		
		BorderLayout borderLayout = new BorderLayout();
		mainPanel.setLayout(borderLayout);
		mainPanel.add(northPanel, BorderLayout.CENTER);
		mainPanel.add(southPanel, BorderLayout.SOUTH);
		
		// build northPanel
		northPanel.setLayout(new FlowLayout());
		northPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5));
		JLabel findLabel = new JLabel("Find: ");
		
		final JTextField keywordTxtField = new JTextField("");
		keywordTxtField.setPreferredSize(new Dimension(200,25));
		
		northPanel.add(findLabel);
		northPanel.add(keywordTxtField);
		
		northPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		// buidl southPanel
		JButton findButton = new JButton();
		findButton.setText("Find");
		
		JButton closeButton = new JButton();
		closeButton.setText("Close");
		
		resultLabel.setPreferredSize(new Dimension(150,25));
		southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.LINE_AXIS));
		southPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
		//southPanel.add(Box.createHorizontalGlue());
		southPanel.add(resultLabel);
		southPanel.add(findButton);
		southPanel.add(Box.createRigidArea(new Dimension(8, 0)));
		southPanel.add(closeButton);
		
		// add ActionListener to find, close button
		findButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String jawaId = keywordTxtField.getText();
				findPlugmeters(jawaId);
			}
			
		});
		
		closeButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				closeDialog();
			}
			
		});
		
		keywordTxtField.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String jawaId = keywordTxtField.getText();
				findPlugmeters(jawaId);
			}
			
		});
		getContentPane().add(mainPanel);
		
		setSize(270,100);
		this.setResizable(false);
		this.setLocationRelativeTo(this.getParent());
	}
	
	/**
	 * close the dialog
	 */
	private void closeDialog(){
		this.setVisible(false);
	}
	
	
	/**
	 * find plugmeters and if found, focus plugmeteres
	 * @param keyword
	 */
	private void findPlugmeters(String keyword){
		String hrCode = keyword;
		resultLabel.setText("");
		if (keyword.length() < 10) {
			// assume the value was scanned
			try {
				long longValue = Long.parseLong(keyword, 16);
				hrCode = IDFormat.codeToHr(longValue);
			} catch (NumberFormatException e) {
				logger.warn(e.getLocalizedMessage());
			}
		}
		ArrayList<PlugmeterButton> plugmeters = cManager.findJawaById(keyword, hrCode);
		
		if(plugmeters.size()==0){
			resultLabel.setText(StringConstants.getString("VALIDATION_NOT_FOUND_P3"));
		}else{
			selectionManager.setPlugmeterSelection(plugmeters, null);
		}
	}
	
}
