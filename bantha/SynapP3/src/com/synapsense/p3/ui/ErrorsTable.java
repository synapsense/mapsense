package com.synapsense.p3.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JToolTip;
import javax.swing.ListSelectionModel;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.jdesktop.swingx.JXTable;

import com.synapsense.p3.components.PlugmeterButton;
import com.synapsense.p3.components.ServerPanel;
import com.synapsense.p3.model.ConfigurationError;
import com.synapsense.p3.model.Jawa;
import com.synapsense.p3.model.Server;
import com.synapsense.p3.utilities.ComponentsManager;
import com.synapsense.p3.utilities.SelectionChangeEvent;
import com.synapsense.p3.utilities.SelectionChangeListener;
import com.synapsense.p3.utilities.SelectionManager;
import com.synapsense.p3.utilities.StringConstants;
import com.synapsense.p3.utilities.Variables;

public class ErrorsTable extends SynapJXTable implements SelectionChangeListener{
	private ErrorsTableModel errorTableModel = new ErrorsTableModel(); 
	private ComponentsManager cManager;
	private SelectionManager selectionManager;
	private static final int DEF_PANEL_HEIGHT = StringConstants.getIntValue("P3TABLE_HEIGHT");
	private static final int DEF_PANEL_WIDTH = StringConstants.getIntValue("P3TABLE_WIDTH");
	
	public ErrorsTable(ComponentsManager cManager, SelectionManager selectionManager){
		super(cManager, selectionManager);
		this.setModel(errorTableModel);
		this.cManager = cManager;
		this.selectionManager = selectionManager;
		initTable();
		
	}
	
	public String getToolTipText(MouseEvent e){
		String tip = null;
		Point p = e.getPoint();
		int row = rowAtPoint(p);
		int col = columnAtPoint(p);
		
		if(row>=0 && col>=0){
			TableModel model = this.getModel();
			tip = (String) model.getValueAt(row, col);
		}
		return tip;
	}
	
	public Point getToolTipLocation(MouseEvent e){
		return new Point(e.getX()-100, e.getY());
	}
	
	private void initTable(){
		int PREF_WIDTH_COL0 = 53;
		int PREF_WIDTH_COL1 = 154;
		
		setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		setFont(new Font("Courier New", Font.PLAIN, 12)); //$NON-NLS-1$
		setBackground(Color.WHITE);
		setForeground(Color.BLACK);
		setSelectionBackground(Color.LIGHT_GRAY);
		setSelectionForeground(Color.BLACK);
		setPreferredScrollableViewportSize(new Dimension(0, 300));
		getColumnModel().getColumn(0).setResizable(false);
		getColumnModel().getColumn(0).setPreferredWidth(PREF_WIDTH_COL0);
		getColumnModel().getColumn(1).setResizable(false);
		getColumnModel().getColumn(1).setPreferredWidth(PREF_WIDTH_COL1);
		//setPreferredSize(new Dimension(, DEF_PANEL_HEIGHT));
		setVisibleRowCount(10);
		setVisibleColumnCount(2);		
		setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		setRowHeightEnabled(false);
		setColumnControlVisible(true);
		setRowSelectionAllowed(true);
		setRowSorter(null);
		setSelectionModel(new ErrorsTableSelectionModel(this));
		getSelectionModel().addListSelectionListener(new ErrorSelectionListener(this));
		
		//this.addMouseListener(new ErrorsTableMouseListener(this));
	}
	
	
	public class ErrorsTableModel extends DefaultTableModel{
		private Class[] columnTypes = new Class[] {String.class, String.class, Object.class};
		private final String[] columnNames = { StringConstants.getString("ERROR_TABLE_HEADER_ID"), StringConstants.getString("ERROR_TABLE_HEADER_ERROR")};
		private boolean[] columnEditables = new boolean[] {false, false, false};
		private ArrayList<ConfigurationError> errors = new ArrayList<ConfigurationError>();
		private ServerPanel currentServer;
		
		public ErrorsTableModel(){
		}
		
		public void bindData(ServerPanel currentServer){
			this.currentServer = currentServer;
			errors.clear();
			if(currentServer==null)
				return;
			
			for(int errorNumber : currentServer.getServer().getInvalidities()){
				ConfigurationError error = new ConfigurationError(currentServer, errorNumber);
				errors.add(error);
			}
			
			if(currentServer.getServer().getJawas().size()>0){
				for(PlugmeterButton p3:currentServer.getPlugmeters()){
					Jawa jawa = p3.getJawa();
					for(int errorNumber:jawa.getInvalidities()){
						ConfigurationError error = new ConfigurationError(p3, errorNumber);
						errors.add(error);
					}
				}
			}
		}
		
		public ServerPanel getCurrentServer(){
			return currentServer;
		}
		
		@Override
		public int getColumnCount() {
			return 2;
		}

		@Override
		public int getRowCount() {
			if(currentServer==null)
				return 0;
			else
				return errors.size();
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			if(columnIndex==0){
				Object o = errors.get(rowIndex).getObject();
				if(o instanceof ServerPanel){
					return "N/A";
				}else if(o instanceof PlugmeterButton){
					Jawa jawa = ((PlugmeterButton)o).getJawa();
					return jawa.getId();
				}
				return errors.get(rowIndex).getObject();
				
			}else if(columnIndex ==1){
				int invalidity = errors.get(rowIndex).getInvalidity();
				return StringConstants.getString("ERROR_MESSAGE_" + Integer.toString(invalidity) );
			}else if(columnIndex ==2){
				Object o = errors.get(rowIndex).getObject();
				return o;
			}
			return null;
		}
		
		public void setValueAt(Object value, int row, int column){
			
		}
		
		public String getColumnName(int col) {
			return columnNames[col];
		}

		public Class getColumnClass(int col) {
			return columnTypes[col];
		}
		
		public boolean isCellEditable(int row, int column) {
			return columnEditables[column];
		}
		
		public ArrayList<Integer> getRowsByObject(Object o){
			ArrayList<Integer> rows = new ArrayList<Integer>();
			
			for(int i=0;i<getRowCount();i++){
				if(getValueAt(i,2).equals(o)){
					rows.add(i);
				}
			}
			return rows;
		}
	}
	
	public class ErrorsTableSelectionModel extends DefaultListSelectionModel{
		private JXTable table;
		public ErrorsTableSelectionModel(JXTable table){
			super();
			this.table = table;
			
		}
		
		public void addSelectionInterval(int index0, int index1){
			super.addSelectionInterval(index0, index1);
			Object obj = errorTableModel.getValueAt(index0, 2);
			
			/*ArrayList<Object> selection = new ArrayList<Object>();
			selection.add(obj);
			selectionManager.addSelection(selection, (SelectionChangeListener)table);
			*/
			/*if(obj instanceof PlugmeterButton){
				cManager.addPlugmeterInTable((PlugmeterButton)obj);	
			}else if( obj instanceof ServerPanel){
				System.out.println("server selection");
				cManager.addServer((ServerPanel)obj, false);
			}*/
			
			System.out.println("addSelectionInterval " + index0 + " " + index1);
		}
		
		public void	removeSelectionInterval(int index0, int index1){
			super.removeSelectionInterval(index0, index1);
			Object obj = errorTableModel.getValueAt(index0, 2);
			
			/*if(obj instanceof PlugmeterButton){
				cManager.removePlugmeterInTable((PlugmeterButton)obj);	
			}else if( obj instanceof ServerPanel){
				System.out.println("server selection");
				cManager.removeServer((ServerPanel)obj);
			}*/
			
			/*ArrayList<Object> selection = new ArrayList<Object>();
			selection.add(obj);
			selectionManager.removeSelection(selection,(SelectionChangeListener)table);
			*/
			
		
		}
		
		public void setSelectionInterval(int index0, int index1) {
			System.out.println("setSelectionInterval " + index0 + " " + index1);
			
			super.setSelectionInterval(index0, index1) ;
			int first, last;
			if(index0<index1){
				first = index0;
				last = index1;
			}else{
				first = index1;
				last = index0;
			}
			
			//cManager.clearPlugmeters();
			ArrayList<ServerPanel> servers = new ArrayList<ServerPanel>();
			ArrayList<PlugmeterButton> plugmeters = new ArrayList<PlugmeterButton>();
			
			for(int i=first;i<=last;i++){
				Object obj = errorTableModel.getValueAt(i, 2);
				
				/*if(obj instanceof PlugmeterButton){
					cManager.removePlugmeterInTable((PlugmeterButton)obj);	
				}else if( obj instanceof ServerPanel){
					System.out.println("server selection");
					cManager.removeServer((ServerPanel)obj);
				}*/
				
				
				if(obj instanceof ServerPanel)
					servers.add((ServerPanel)obj);
				else{
					plugmeters.add((PlugmeterButton)obj);
					servers.add(errorTableModel.getCurrentServer());
				}
			}
			selectionManager.setServerSelection(servers ,(SelectionChangeListener)table);
			selectionManager.setPlugmeterSelection(plugmeters ,(SelectionChangeListener)table);
			
		}
		
		
	}
	class ErrorSelectionListener implements ListSelectionListener{
		private JXTable table;
		
		
		public ErrorSelectionListener(JXTable table){
			this.table = table;
		}
		
		@Override
		public void valueChanged(ListSelectionEvent e) {
			//cManager.reloadRackDisplay();
		}
		
	}
	class ErrorsTableMouseListener implements MouseListener{
		private JXTable table;
		
		public ErrorsTableMouseListener(JXTable table){
			this.table = table;
		}
		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			System.out.println (table.getSelectedRow());
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}

	@Override
	public void selectionChanged(SelectionChangeEvent e) {
		ArrayList<Object> selections = e.getAddtions();
		
		ServerPanel currentServer = null;
		ArrayList<ServerPanel> selectedServers= selectionManager.getSelectedServers();
		if(selectedServers.size()==1){
				currentServer = selectedServers.get(0);
			
		}
		errorTableModel.bindData(currentServer);
		errorTableModel.fireTableDataChanged();
		
		for(Object o:selections){
			if(o instanceof PlugmeterButton || o instanceof ServerPanel){
				ArrayList<Integer> rows = errorTableModel.getRowsByObject(o);
				for(int row:rows )
					this.addRowSelectionInterval(row, row);
			}
		}
		
		ArrayList<Object> deselections = e.getRemovals();
		
		for(Object o:deselections){
			if(o instanceof PlugmeterButton || o instanceof ServerPanel){
				ArrayList<Integer> rows = errorTableModel.getRowsByObject(o);
				for(int row:rows )
					this.removeRowSelectionInterval(row, row);
			}
		}
	}

	
	
}
