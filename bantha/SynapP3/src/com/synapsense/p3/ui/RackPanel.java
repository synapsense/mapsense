package com.synapsense.p3.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;


import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXButton;
import org.jdesktop.swingx.JXComboBox;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXPanel;

import com.synapsense.laf.styles.BgImagePainter;

import com.synapsense.p3.components.RackTable;
import com.synapsense.p3.components.RackTable.RackCellRenderer;
import com.synapsense.p3.components.ServerPanel;
import com.synapsense.p3.components.RackTable.RackTableModel;
import com.synapsense.p3.model.ConfigurationError;
import com.synapsense.p3.model.Jawa;
import com.synapsense.p3.model.Rack;
import com.synapsense.p3.model.Server;
import com.synapsense.p3.utilities.ComponentsManager;
import com.synapsense.p3.utilities.ConfigurationIO;
import com.synapsense.p3.utilities.LogManager;
import com.synapsense.p3.utilities.MessageHandler;
import com.synapsense.p3.utilities.SelectionManager;
import com.synapsense.p3.utilities.StringConstants;
import com.synapsense.p3.utilities.Validator;
import com.synapsense.p3.utilities.Variables;

public class RackPanel extends JXPanel {
	private static Logger logger = Logger.getLogger(RackPanel.class.getName());

	private static final String RACK_PATCHPANEL_BOT_PNG = StringConstants.getString("IMAGE_RACK_BOTTOM");
	private static final String RACK_PATCHPANEL_MID_PNG = StringConstants.getString("IMAGE_RACK_MIDDLE");
	private static final String RACK_PATCHPANEL_TOP_PNG = StringConstants.getString("IMAGE_RACK_TOP");
	
	private static final int RACKSIZE_COMBOBOX_HEIGHT = StringConstants.getIntValue("RACKSIZE_COMBOBOX_HEIGHT");		// combobox size to adjust rack height
	private static final int RACKSIZE_COMBOBOX_WIDTH  = StringConstants.getIntValue("RACKSIZE_COMBOBOX_WIDTH");		// combobox width to adjust rack height
	private static final int RACKPANEL_WIDTH          = StringConstants.getIntValue("RACKPANEL_WIDTH");		// rack table width
	private int RACKPANEL_HEIGHT = StringConstants.getIntValue("RACKPANEL_HEIGHT");
	
	private static final int RACKTABLE_HEIGHT         = StringConstants.getIntValue("RACKTABLE_HEIGHT");	// rack table height
	private static final int RACKTABLE_WIDTH           = StringConstants.getIntValue("RACKTABLE_WIDTH");
	private static final int RACKTABLE_HEADER_HEIGHT   = StringConstants.getIntValue("RACKTABLE_HEADER_HEIGHT");
	private static final int RACKTABLE_FOOTER_HEIGHT   = StringConstants.getIntValue("RACKTABLE_FOOTER_HEIGHT");
	private int RACKTABLE_MIN_HEIGHT = StringConstants.getIntValue("RACKTABLE_MIN_HEIGHT");
	
	private int SLOT_HEIGHT = StringConstants.getIntValue("SLOT_HEIGHT");
	public static final int RACK_PANEL_RESIZED = 0x12345678;
	private Integer rackSize = Variables.RACK_DEFAULT_HEIGHT;
	
	//public JXPanel title = new JXPanel();
	public JXComboBox comboBox = new JXComboBox();
	private final JSpinner rackSizeSelector = new JSpinner();	
	public final JXButton rabitButton = new JXButton();
	
	public RackTable table;	
	private Rack rack;
	private JXLabel indicatorLable;
	private ComponentsManager cManager;
	
	private ConfigurationIO io;
	private MainScreen mainFrame;
	
	public RackPanel(ComponentsManager cManager, JXLabel label, ConfigurationIO io, SelectionManager selectionManager, MainScreen mainFrame) {
		indicatorLable = label;
		table = new RackTable(cManager, selectionManager);
		table.setSize(new Dimension(RACKPANEL_WIDTH, RACKTABLE_HEIGHT-RACKTABLE_FOOTER_HEIGHT));
		this.io = io;
		this.cManager = cManager;
		this.mainFrame = mainFrame;
		setLayout(new BorderLayout(0, 0));
		//setBorder(new LineBorder(Color.red));
		initSelector();
		
		add(table, BorderLayout.SOUTH);
		setBackground(Color.WHITE);
		setOpaque(false);
		setBackgroundPainter(BgImagePainter.getTiledPainter(RACK_PATCHPANEL_MID_PNG)); // rack slots
		
		initTable();
	}

	public RackTable getRackTable(){
		return table;
	}
	/**
	 * 
	 * This private static class renders rack pedestal at the bottom of the panel
	 *
	 */
	static public class RackPedestal extends JXPanel {

		public RackPedestal() {
			setBackgroundPainter(BgImagePainter.getImagePainter(RACK_PATCHPANEL_BOT_PNG));
			Dimension size = new Dimension(RACKTABLE_WIDTH, RACKTABLE_FOOTER_HEIGHT);	// patchpanel_bottom size	
			setPreferredSize(size);
		}
	}
	
	public class RackHeader extends JXPanel {
		public RackHeader(){
			setBackgroundPainter(BgImagePainter.getImagePainter(RACK_PATCHPANEL_TOP_PNG));
			Dimension size = new Dimension(RACKTABLE_WIDTH, RACKTABLE_HEADER_HEIGHT);	// patchpanel_bottom size	
			setPreferredSize(size);
			//setBorder(new LineBorder(Color.red));
			
			GridBagLayout gbl = new GridBagLayout();
			gbl.columnWidths = new int[] { 100, 150 };
			setLayout(gbl);
			//setLayout(new FlowLayout(FlowLayout.LEFT, 9, 1));
			
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(0, 10, 0, 10);
			gbc.anchor = GridBagConstraints.WEST;
			//gbc.fill = GridBagConstraints.BOTH;
			gbc.gridx = 0;
			gbc.gridy = 0;
			gbl.setConstraints(rackSizeSelector, gbc);
			add(rackSizeSelector);
			
			rabitButton.setIcon(new ImageIcon(MainToolbar.class.getResource(StringConstants.getString("TOOLBAR_ICON_DIR")+"icon_rabit.png")));
			Dimension btnSize = new Dimension(rabitButton.getIcon().getIconWidth(), rabitButton.getIcon().getIconHeight());
			rabitButton.setPreferredSize(btnSize);
			rabitButton.setToolTipText(StringConstants.getString("TOOLBAR_TOOLTIP_OUTLETLIGHTING"));
			rabitButton.addActionListener(new ActionListener(){
		
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					activateOutletOrder();
				}
				
			});
			
			gbc.gridx = 1;
			gbl.setConstraints(rabitButton, gbc);
			add(rabitButton);
		}
	}
	
	public RackPedestal pedestal = new RackPanel.RackPedestal();
	
	public RackHeader header = new RackPanel.RackHeader();
	
	static public class RackGridBagLayout extends GridBagLayout {
		public RackGridBagLayout() {
			super();
			columnWidths = new int[] { RACKPANEL_WIDTH, RACKPANEL_WIDTH, RACKPANEL_WIDTH };
			rowHeights = new int[] {  RACKTABLE_HEIGHT, RACKTABLE_FOOTER_HEIGHT};
			columnWeights = new double[] { 0.0, 0.0, 0.0 };
			rowWeights = new double[] { Double.MAX_VALUE, Double.MIN_VALUE};
		}
	}
	
	/**
	 * 
	 * gridx is a column for a new rack to be added: 1 (left), 2 (center) or 3 (right)
	 *
	 */
	static public class RackGridBagConstraints extends GridBagConstraints {
		
		/**
		 * Add to row zero, column x
		 * 
		 * @param x
		 */
		public RackGridBagConstraints(int x) {
			init(x,0);
		}
		
		/**
		 * Add to row y, column x
		 * 
		 * @param x
		 * @param y
		 */
		public RackGridBagConstraints(int x, int y) {
			init(x,y);
		}

		/**
		 * 
		 * Create constraints for GBC layout
		 * 
		 * @param x
		 * @param y
		 */
		protected void init(int x, int y) {
			insets = new Insets(0, 0, 0, 0);
			fill = GridBagConstraints.BOTH;
			gridx = x;
			gridy = y;
		}
	
	}	
	
	private void activateOutletOrder(){
		io.activateOrderingLighting(rack);
	}
	
	/**
	 * 
	 * @param on
	 */
	protected void turn(Boolean on) {
		float alpha = (on)?1f:0.5f;
		header.setBackgroundPainter(BgImagePainter.getAlphaPainter(RACK_PATCHPANEL_TOP_PNG, alpha));		
		setBackgroundPainter(BgImagePainter.getTiledAlphaPainter(RACK_PATCHPANEL_MID_PNG, alpha));		
		pedestal.setBackgroundPainter(BgImagePainter.getAlphaPainter(RACK_PATCHPANEL_BOT_PNG, alpha));
		
		if(on){
			indicatorLable.setVisible(false);
		}else{
			indicatorLable.setVisible(true);
		}
	}
	
	private void initSelector() {
		rackSizeSelector.setModel(getRackSpinnerModel());
		rackSizeSelector.setPreferredSize(new Dimension(RACKSIZE_COMBOBOX_WIDTH, RACKSIZE_COMBOBOX_HEIGHT));
		rackSizeSelector.setMinimumSize(new Dimension(RACKSIZE_COMBOBOX_WIDTH,   RACKSIZE_COMBOBOX_HEIGHT));
		rackSizeSelector.setBounds(0, 0, RACKSIZE_COMBOBOX_WIDTH, RACKSIZE_COMBOBOX_HEIGHT);
		((JSpinner.DefaultEditor)rackSizeSelector.getEditor()).getTextField().addKeyListener(new KeyListener(){

			@Override
			public void keyPressed(KeyEvent ke) {
				// TODO Auto-generated method stub
				if(ke.getKeyCode()==KeyEvent.VK_ENTER){
					try{
						int newValue = Integer.parseInt(((JSpinner.DefaultEditor)rackSizeSelector.getEditor()).getTextField().getText());
						if(!Validator.checkRackSizeLimitation(newValue)){
							MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_RACKSIZE_LIMITATION_MSG"));
							((JSpinner.DefaultEditor)rackSizeSelector.getEditor()).getTextField().setText(Integer.toString((rack.getUheight())));
						}
						
					}catch(Exception e){
						logger.debug(LogManager.getStackTrace(e));
						MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_RACKSIZE_LIMITATION_MSG"));
						((JSpinner.DefaultEditor)rackSizeSelector.getEditor()).getTextField().setText(Integer.toString((rack.getUheight())));
				
					}
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		rackSizeSelector.addChangeListener(new ChangeListener() {			
			@Override
			public void stateChanged(ChangeEvent e) {
				int newHeight = (Integer)(rackSizeSelector.getValue());
				int rackModelSize = rack.getUheight();
				if(Validator.checkResizableRack(rack, newHeight)){
					setRackSize(newHeight);
				}else{
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_INVALID_RACKSIZE_MSG"));
					rackSizeSelector.setValue(rackModelSize);
					repaint();
				}
			}
		});
	}
	
	private SpinnerNumberModel getRackSpinnerModel() {
		return new SpinnerNumberModel(this.rackSize.intValue(), Variables.RACK_MIN_HEIGHT, Variables.RACK_MAX_HEIGHT, 1);
	}
	
	/**
	 * 
	 */
	private void initTable() {
		table.setColumnWidths();
	}
	
	private void adjustTableSize() {
		System.out.println("adjustTableSize()---------------------");
		
		int rackSize = getRackSize();
		// rack height should be set before
		table.setRack(rack);
		table.setVisibleRowCount(rackSize);
		/*
		
		
		int calculatedHeight = table.getRowHeight() * rackSize;
		
		if(calculatedHeight>RACKTABLE_MIN_HEIGHT){
			table.setSize(table.getWidth(),calculatedHeight);
		}else{
			table.setSize(table.getWidth(),RACKTABLE_MIN_HEIGHT);
		}
		this.setSize(table.getSize());
		*/
		mainFrame.setMainPaneHeight();
		mainFrame.setRackPanelsHeight();
		
	}	
		
	
	public void setRack(Rack rack){
		this.rack = rack;
		table.setRack(rack);
		rackSize = rack.getUheight();
		rackSizeSelector.setValue(getRackSize());
		
		if(rack.getUheight()==0){
			indicatorLable.setVisible(true);
		}else{
			indicatorLable.setVisible(false);
		}
		
		setRackSize(rack.getUheight());
	}

	public void setRackSize(Integer rackSize) {
		logger.info("set rack size from " + rack.getUheight() + " to " + rackSize);
		try{
			this.turn(rackSize!=0);	// change 
			this.rackSize = rackSize;
			rack.setUheight(rackSize);
			rackSizeSelector.setValue(getRackSize());
			adjustTableSize();
			this.repaint();
		}catch(Exception e){
			logger.error("Fail to change rack size");
			logger.error(e.getStackTrace().toString());
		}
	}

	public Integer getRackSize() {
		return rackSize;
	}	
}
