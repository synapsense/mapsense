package com.synapsense.p3.ui;

import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import com.synapsense.p3.utilities.ComponentsManager;
import com.synapsense.p3.utilities.Variables;

public class FunctionKeyAdapter extends KeyAdapter{
	ComponentsManager cManager;
	Component component;
	
	public FunctionKeyAdapter(ComponentsManager cManager, Component c ){
		this.cManager = cManager;
		component = c;
	}
	
	public void keyPressed(KeyEvent e){
		if(e.getKeyCode()==KeyEvent.VK_DELETE){
			cManager.deleteSelections();
		}else if(e.isControlDown() && !e.isShiftDown() && e.getKeyCode()==KeyEvent.VK_A){
			System.out.println("selectAllP3InWorkingRack");
			cManager.selectAllP3InWorkingRack();
		}else if(e.isControlDown() && e.isShiftDown() && e.getKeyCode()==KeyEvent.VK_A){
			System.out.println("selectAllP3");
			cManager.selectAllP3();
		}else if(e.isAltDown()&& e.getKeyChar()==KeyEvent.VK_0)
			cManager.showRackInfo(Variables.LEFT_RACK_TYPE);
		else if(e.isAltDown() && e.getKeyChar()==KeyEvent.VK_1)
			cManager.showRackInfo(Variables.CENTER_RACK_TYPE);
		else if(e.isAltDown() && e.getKeyChar()==KeyEvent.VK_2)
			cManager.showRackInfo(Variables.RIGHT_RACK_TYPE);
		else if(e.isAltDown() && e.getKeyChar()=='p')
			cManager.showPlugmeterSelection();
		else if(e.isAltDown() && e.getKeyChar()=='s')
			cManager.showServerSelection();
		else if(e.isControlDown() && e.getKeyChar()==KeyEvent.VK_0){
			cManager.showRackPhaseInfo(Variables.LEFT_RACK_TYPE);
		}else if(e.isControlDown() && e.getKeyChar()==KeyEvent.VK_1){
			cManager.showRackPhaseInfo(Variables.CENTER_RACK_TYPE);
		}else if(e.isControlDown() && e.getKeyChar()==KeyEvent.VK_2){
			cManager.showRackPhaseInfo(Variables.RIGHT_RACK_TYPE);
		}
	
	
		//else if(e.getKeyCode()==KeyEvent.VK_F11){
		//	cManager.maximizeScreen();
		//}else if(e.getKeyCode()==KeyEvent.VK_ESCAPE){
		//	cManager.minimizeScreen();
		//else if(e.isAltDown() &&  e.getKeyCode()==KeyEvent.VK_A){
		//	System.out.println("select all servers");
		//	cManager.selectAllServers();
		//}
		//System.out.println("keyPressed in functionKeyAdapter");
		//System.out.println("component:" + component.getClass().getSimpleName());
	}
	
	public void keyTyped(KeyEvent e) {
	}

}
