package com.synapsense.p3.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;

import org.jdesktop.swingx.JXButton;

import com.synapsense.p3.utilities.ComponentsManager;
import com.synapsense.p3.utilities.StringConstants;
import com.synapsense.p3.utilities.Variables;
import com.synapsense.p3.utilities.Variables.ConfigurationTypes;

/**
 * A JXbutton class for selecting configuration button
 * @author HLim
 *
 */
public class ConfigurationButton extends JXButton implements MouseListener {
	ConfigurationTypes configurationType;		// selected configuration type
	ComponentsManager componentsManager;		// utility function class to select configuration type and add selected one
	
	/**
	 * construct
	 * @param configType
	 * @param cManager
	 */
	public ConfigurationButton(ConfigurationTypes configType, ComponentsManager cManager){
		super();
		configurationType = configType;
		componentsManager = cManager;
		setButtonIcon();
		this.addMouseListener(this);
	}

	/**
	 * set button icon image
	 */
	private void setButtonIcon() {
		
		String down_icon_name = "down_" + configurationType.toString().toLowerCase() + ".png";
		String default_icon_name = "norm_" + configurationType.toString().toLowerCase() + ".png";
		
		setIcon(new ImageIcon(MainToolbar.class.getResource(StringConstants.getString("TOOLBAR_ICON_DIR")+default_icon_name)));
		setSelectedIcon(new ImageIcon(MainToolbar.class.getResource(StringConstants.getString("TOOLBAR_ICON_DIR")+down_icon_name)));
		setRolloverIcon(new ImageIcon(MainToolbar.class.getResource(StringConstants.getString("TOOLBAR_ICON_DIR")+down_icon_name)));
		setRolloverSelectedIcon(new ImageIcon(MainToolbar.class.getResource(StringConstants.getString("TOOLBAR_ICON_DIR")+down_icon_name)));
		setPressedIcon(new ImageIcon(MainToolbar.class.getResource(StringConstants.getString("TOOLBAR_ICON_DIR")+down_icon_name)));
	}

	
	/**
	 * get assigned configuration Type
	 * @return
	 */
	public ConfigurationTypes getConfigurationType(){
		return configurationType;
	}
	
	/**
	 * set new configuration type for this button
	 * @param configType
	 */
	public void setConfigurationType(ConfigurationTypes configType){
		this.configurationType = configType;
	}

	/**
	 * when clicking button, add a configuration
	 * the number of clicks determines selection mode
	 */
	@Override
	public void mouseClicked(MouseEvent me) {
		// TODO Auto-generated method stub
		//System.out.println("push " + configurationType.toString());
		
		int clicks = me.getClickCount();
		if(clicks==1){		// one click -> add a configuration and deactivate config selection
			if(this.isSelected()){ // if this button is already selected, deactivate it
				componentsManager.removeSelectedConfigType();
			}else{		// if not, activate selection
				componentsManager.setSelectedConfigType(this, Variables.CONFIGURATION_SELECTION_MODE_SINGLE);
			}
		}else if(clicks==2){	// double click -> add a configuration and keep config selection for adding multiple ones
			componentsManager.setSelectedConfigType(this, Variables.CONFIGURATION_SELECTION_MODE_MULTIPLE);
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
