package com.synapsense.p3.ui;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXButton;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.painter.AlphaPainter;
import org.jdesktop.swingx.painter.CompoundPainter;
import org.jdesktop.swingx.painter.GlossPainter;
import org.jdesktop.swingx.painter.MattePainter;
import org.jdesktop.swingx.painter.Painter;

import com.synapsense.p3.components.PlugmeterButton;
import com.synapsense.p3.components.ServerPanel;
import com.synapsense.p3.model.Configuration;
import com.synapsense.p3.model.ConfigurationError;
import com.synapsense.p3.model.Jawa;
import com.synapsense.p3.utilities.ComponentsManager;
import com.synapsense.p3.utilities.ConfigurationIO;
import com.synapsense.p3.utilities.InvalidChecksum;
import com.synapsense.p3.utilities.LogManager;
import com.synapsense.p3.utilities.MessageHandler;
import com.synapsense.p3.utilities.PreferencesUtil;
import com.synapsense.p3.utilities.SelectionManager;
import com.synapsense.p3.utilities.StringConstants;
import com.synapsense.p3.utilities.Validator;
import com.synapsense.p3.utilities.Variables;
import com.synapsense.p3.utilities.Variables.ConfigurationTypes;

public class MainToolbar extends JToolBar {

	public final RelocatableTooltipButton openButton = new RelocatableTooltipButton();
	public final RelocatableTooltipButton saveButton = new RelocatableTooltipButton();
	//
	public final RelocatableTooltipButton retrieveButton = new RelocatableTooltipButton();
	public final RelocatableTooltipButton transferButton = new RelocatableTooltipButton();
	public final RelocatableTooltipButton deleteButton = new RelocatableTooltipButton();
	public final RelocatableTooltipButton clearButton = new RelocatableTooltipButton();
	
	public final RelocatableTooltipButton resetButton = new RelocatableTooltipButton();
	public final RelocatableTooltipButton activateButton = new RelocatableTooltipButton();
	
	public final RelocatableTooltipButton phaseButton = new RelocatableTooltipButton();
	public final RelocatableTooltipButton helpButton = new RelocatableTooltipButton();
	//
	public final ConfigurationButton plugmeterButton;
	//
	public final ConfigurationButton srv1UButton;
	public final ConfigurationButton srv1U1PButton;
	public final ConfigurationButton srv1U2PButton;
	public final ConfigurationButton customButton;
	
	
	private int buttonOffsetX = 10;
	private int buttonOffsetY = 54;
	
	private Configuration configuration;
	private ConfigurationIO configurationIO;
	private ComponentsManager cManager;
	private SelectionManager selectionManager; 
	
	private static Logger logger = Logger.getLogger(MainToolbar.class.getName());
	
	public MainToolbar(ComponentsManager selector, SelectionManager selectionManager) {
		setFloatable(false);
		this.cManager = selector;
		this.selectionManager = selectionManager;
		
		
		plugmeterButton = new ConfigurationButton(ConfigurationTypes.PLUGMETER, cManager);
		srv1UButton = new ConfigurationButton(ConfigurationTypes.SRV1U, cManager);
		srv1U1PButton = new ConfigurationButton(ConfigurationTypes.SRV1U1P, cManager);
		srv1U2PButton = new ConfigurationButton(ConfigurationTypes.SRV1U2P, cManager);
		customButton = new ConfigurationButton(ConfigurationTypes.SRVCUSTOM, cManager);
		
		cManager.addConfiguarationButton(plugmeterButton);
		cManager.addConfiguarationButton(srv1UButton);
		cManager.addConfiguarationButton(srv1U1PButton);
		cManager.addConfiguarationButton(srv1U2PButton);
		cManager.addConfiguarationButton(customButton);
		
		
		// open configuration template
		setBorderPainted(false);
		// Set button labels and icons		
		setButtonIcon(openButton, "open");
		applyDefaultStyle(openButton);
		openButton.setToolTipText(StringConstants.getString("TOOLBAR_TOOLTIP_OPEN"));
		openButton.setToolTipLocation(openButton.getLocation(), -buttonOffsetX, -buttonOffsetY );
		openButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				open();
			}
		});
		
		// save configuration template
		setButtonIcon(saveButton, "save");
		applyDefaultStyle(saveButton);
		saveButton.setToolTipText(StringConstants.getString("TOOLBAR_TOOLTIP_SAVE"));
		saveButton.setToolTipLocation(saveButton.getLocation(), buttonOffsetX, -buttonOffsetY);
		saveButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				save();
			}
			
		});
		
		// receive configuration from controller
		setButtonIcon(retrieveButton, "retrieve");
		applyDefaultStyle(retrieveButton);
		retrieveButton.setToolTipText(StringConstants.getString("TOOLBAR_TOOLTIP_DOWNLOAD"));
		retrieveButton.setToolTipLocation(retrieveButton.getLocation(), buttonOffsetX, -buttonOffsetY);
		retrieveButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				receive();
			}
			
		});
		
		// submit configuration to controller
		setButtonIcon(transferButton, "transfer");
		applyDefaultStyle(transferButton);
		transferButton.setToolTipText(StringConstants.getString("TOOLBAR_TOOLTIP_UPLOAD"));
		transferButton.setToolTipLocation(transferButton.getLocation(), buttonOffsetX, -buttonOffsetY);
		transferButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				transfer();
			}
			
		});
		
		// delete selected servers
		setButtonIcon(deleteButton, "delete");
		applyDefaultStyle(deleteButton);
		deleteButton.setToolTipText(StringConstants.getString("TOOLBAR_TOOLTIP_DELETE"));
		//deleteButton.setText("Delete");
		deleteButton.setToolTipLocation(deleteButton.getLocation(), buttonOffsetX, -buttonOffsetY);
		deleteButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				deleteServers();
			}
			
		});
		
		// clear configuration from the controller
		setButtonIcon(clearButton, "clear");
		applyDefaultStyle(clearButton);
		clearButton.setToolTipText(StringConstants.getString("TOOLBAR_TOOLTIP_CLEAR"));
		clearButton.setToolTipLocation(clearButton.getLocation(), buttonOffsetX, -buttonOffsetY);
		clearButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				clearConfiguration();
			}
			
		});
		
		// add plugmeter
		//setButtonIcon(plugmeterButton, "plugmeter");
		applyDefaultStyle(plugmeterButton);	
		//plugmeterButton.setRackType(RackTypes.PLUGMETER);
		plugmeterButton.setToolTipText(StringConstants.getString("TOOLBAR_TOOLTIP_PLUG"));
		
		// set phase to selected plugmeter
		setButtonIcon(phaseButton, "phase");
		applyDefaultStyle(phaseButton);
		phaseButton.setToolTipText(StringConstants.getString("TOOLBAR_TOOLTIP_PHASE"));
		phaseButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				associatePhase();
			}
			
		});
		phaseButton.setToolTipLocation(phaseButton.getLocation(), buttonOffsetX, -buttonOffsetY);
		
		// set help
		setButtonIcon(helpButton, "help");
		applyDefaultStyle(helpButton);
		helpButton.setToolTipText(StringConstants.getString("TOOLBAR_TOOLTIP_HELP"));
		helpButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				help();
				
			}
			
		});
		helpButton.setToolTipLocation(helpButton.getLocation(), buttonOffsetX, -buttonOffsetY);
		
		// set reset
		setButtonIcon(resetButton, "reset");
		applyDefaultStyle(resetButton);
		resetButton.setToolTipText(StringConstants.getString("TOOLBAR_TOOLTIP_RESET"));
		resetButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				reset();
			}
			
		});
		resetButton.setToolTipLocation(resetButton.getLocation(), buttonOffsetX, -buttonOffsetY);		
		
		// set activate
		setButtonIcon(activateButton, "activate");
		applyDefaultStyle(activateButton);
		activateButton.setToolTipText(StringConstants.getString("TOOLBAR_TOOLTIP_ACTIVATE"));
		activateButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				activate();
			}
			
		});
		activateButton.setToolTipLocation(activateButton.getLocation(), 25, -buttonOffsetY);
		
		applyDefaultStyle(srv1UButton);
		srv1UButton.setToolTipText(StringConstants.getString("TOOLBAR_TOOLTIP_1U"));
	
		
		applyDefaultStyle(srv1U1PButton);
		srv1U1PButton.setToolTipText(StringConstants.getString("TOOLBAR_TOOLTIP_1U_1P"));
		
		applyDefaultStyle(srv1U2PButton);		
		srv1U2PButton.setToolTipText(StringConstants.getString("TOOLBAR_TOOLTIP_1U_2P"));
		
		applyDefaultStyle(customButton);
		customButton.setToolTipText(StringConstants.getString("TOOLBAR_TOOLTIP_CUSTOM"));
		
		// Add buttons to the toolbar
		add(openButton);
		add(saveButton);
		add(retrieveButton);
		add(transferButton);
		add(clearButton);
		addSeparator(new Dimension(3,this.getHeight()));
		add(plugmeterButton);
		add(srv1UButton);
		add(srv1U1PButton);
		add(srv1U2PButton);
		//add(srv4UButton);
		add(customButton);
		addSeparator(new Dimension(3,this.getHeight()));
		add(phaseButton);
		add(deleteButton);
		add(resetButton);
		add(activateButton);
		addSeparator(new Dimension(3,this.getHeight()));
		add(helpButton);
	}
	
	public void setConfiguration(Configuration config,ConfigurationIO configIO){
		configuration = config;
		configurationIO = configIO;
	}
	
	public Configuration getConfiguration(){
		return configuration;
	}

	/**
	 * @param button
	 */
	public void applyDefaultStyle(JXButton button) {
		//Dimension size = new Dimension(80, 54);
		Dimension size = new Dimension(button.getIcon().getIconWidth(), button.getIcon().getIconHeight());
		button.setPreferredSize(size);
		button.setFocusable(true);
		button.setFocusTraversalKeysEnabled(false);
		button.setFocusPainted(false);
		button.setRequestFocusEnabled(true);
		button.setContentAreaFilled(false);
		//button.setHorizontalTextPosition(SwingConstants.CENTER);
		//button.setBorder(new LineBorder(Color.red));
		button.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		button.setRolloverEnabled(true);
		
		
		//button.setBackgroundPainter(new LargeButtonBackground());
	}
	
	
	public Painter getBackgroundPainter(){
		MattePainter mp = new MattePainter(new Color(199,199,199));
		GlossPainter gp = new GlossPainter(new Color(235, 235, 235), GlossPainter.GlossPosition.BOTTOM);
		AlphaPainter ap = new AlphaPainter();
		ap.setAlpha(0.5f);
		return (new CompoundPainter(mp, gp, ap));
	}
	
	/**
	 * @param button
	 * @param filename
	 */
	public void setButtonIcon(JXButton button, String toolname) {
		
		String down_icon_name = "down_" + toolname + ".png";
		String default_icon_name = "norm_" + toolname + ".png";
		
		button.setIcon(new ImageIcon(MainToolbar.class.getResource(StringConstants.getString("TOOLBAR_ICON_DIR")+default_icon_name)));
		button.setSelectedIcon(new ImageIcon(MainToolbar.class.getResource(StringConstants.getString("TOOLBAR_ICON_DIR")+default_icon_name)));
		button.setRolloverIcon(new ImageIcon(MainToolbar.class.getResource(StringConstants.getString("TOOLBAR_ICON_DIR")+down_icon_name)));
		button.setRolloverSelectedIcon(new ImageIcon(MainToolbar.class.getResource(StringConstants.getString("TOOLBAR_ICON_DIR")+default_icon_name)));
		button.setPressedIcon(new ImageIcon(MainToolbar.class.getResource(StringConstants.getString("TOOLBAR_ICON_DIR")+default_icon_name)));
	
	}
	
	private void doOpenfile(){
		JFileChooser fileChooser;
		
		if(PreferencesUtil.getSavedDirPath()==null){
			fileChooser = new JFileChooser();
		}else{
			fileChooser = new JFileChooser(PreferencesUtil.getSavedDirPath());
		}
		
		//fileChooser.setAccessory(new CustomAccessaryPanel(fileChooser));
		
		FileNameExtensionFilter filter1 = new FileNameExtensionFilter(StringConstants.getString("TEMPLATE1_NAME"), StringConstants.getString("TEMPLATE1_EXTENTION"));
		FileNameExtensionFilter filter2 = new FileNameExtensionFilter(StringConstants.getString("TEMPLATE2_NAME"), StringConstants.getString("TEMPLATE2_EXTENTION"));
		fileChooser.addChoosableFileFilter(filter1);
		fileChooser.addChoosableFileFilter(filter2);
		fileChooser.setFileFilter(filter1);
		fileChooser.setAcceptAllFileFilterUsed(false);
		
		File f;
		String fileName ="";
		fileChooser.setDialogTitle(StringConstants.getString("DIALOG_OPEN_TEMPLATE_TITLE"));
		int result = fileChooser.showDialog(MainScreen.getFrames()[0],StringConstants.getString("DIALOG_OPEN_TEMPLATE_BUTTON"));
	
		if(result == JFileChooser.APPROVE_OPTION){
			MainScreen.getFrames()[0].setCursor(new Cursor(Cursor.WAIT_CURSOR));
			
			try{
				File templateFile = fileChooser.getSelectedFile();
				String file_path = templateFile.getAbsolutePath();
				fileName = templateFile.getName();
				PreferencesUtil.setSavedFilePath(file_path, templateFile.getParentFile().getAbsolutePath());
				configuration = configurationIO.importTemplate(file_path);
				
				if(configuration==null){
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_INVALID_CONFIGURATION_FILE_MSG"));
					MainScreen.getFrames()[0].setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					return;
				}
				
				cManager.updataConfiguration(configuration);
				setMainTitle(fileName);
				logger.info("open the saved configuration file " + file_path);
			}catch(Exception e){
				MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_INVALID_CONFIGURATION_FILE_MSG"));
				logger.error("Fail to open a configuration file");
				logger.debug(LogManager.getStackTrace(e));
			}
			MainScreen.getFrames()[0].setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		}
	}
	
	private void open(){
		
		if(configurationIO.isSavedConfiguration(configuration)){
			doOpenfile();
		}else{
			String options[] = {"Yes", "No"};
			int response = MessageHandler.showOptionMessage(MessageHandler.MESSAGE_TYPE_CONFIRMATION, StringConstants.getString("VALIDATION_UNSAVED_CONFIGURATION_MSG"), options);
			if(response == JOptionPane.YES_OPTION){
				doOpenfile();
			}else{
				return;
			}
		}
	}
	
	private void save(){
		JFileChooser fileChooser = new JFileChooser();
		FileNameExtensionFilter filter1 = new FileNameExtensionFilter(StringConstants.getString("TEMPLATE1_NAME"), StringConstants.getString("TEMPLATE1_EXTENTION"));
		FileNameExtensionFilter filter2 = new FileNameExtensionFilter(StringConstants.getString("TEMPLATE2_NAME"), StringConstants.getString("TEMPLATE2_EXTENTION"));
		
		fileChooser.addChoosableFileFilter(filter1);
		fileChooser.addChoosableFileFilter(filter2);
		
		fileChooser.setAcceptAllFileFilterUsed(false);
		fileChooser.setFileFilter(filter1);
		File f;
		String fileName = "";
		try {
			
			String filePref = PreferencesUtil.getSavedFilePath();
			
			f = new File(new File(filePref).getCanonicalPath());
			fileChooser.setSelectedFile(f);
			
			int result = fileChooser.showSaveDialog(this);
			
			if(result == JFileChooser.APPROVE_OPTION){
				MainScreen.getFrames()[0].setCursor(new Cursor(Cursor.WAIT_CURSOR));
				
				File templateFile = fileChooser.getSelectedFile();
				String file_path = templateFile.getAbsolutePath();
				fileName = templateFile.getName();
				int savingOption = 0;
				
				String filter_extention = "";
				if(fileChooser.getFileFilter().getDescription().equals(StringConstants.getString("TEMPLATE1_NAME"))){
					filter_extention = StringConstants.getString("TEMPLATE1_EXTENTION");
					savingOption = 0;
				}else{
					filter_extention = StringConstants.getString("TEMPLATE2_EXTENTION");
					savingOption = 1;
				}
					
				String file_extention = "";
				int dotPos = file_path.lastIndexOf(".");
				if(dotPos==-1){
					file_path+="." + filter_extention;
				}else{
					file_extention = file_path.substring(dotPos+1);
					
					if(!file_extention.equals(filter_extention)){
						file_path+="." + filter_extention;
					}
				}
				
				configurationIO.writeTemplate(file_path, configuration, savingOption);
				MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_SAVE_CONFIGURATION_FILE_MSG"));
				logger.info("saved the configuration at" +file_path );
				PreferencesUtil.setSavedFilePath(file_path, templateFile.getParentFile().getAbsolutePath());
				
				MainScreen.getFrames()[0].setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				setMainTitle(fileName);
			}
		} catch (IOException e) {
			logger.error("fail to save a configuration to file");
			logger.debug(LogManager.getStackTrace(e));
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_SAVE_CONFIGURATION_FILE_FAIL_MSG"));
			MainScreen.getFrames()[0].setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("fail to save a configuration to file");
			logger.debug(LogManager.getStackTrace(e));
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_SAVE_CONFIGURATION_FILE_FAIL_MSG"));
			MainScreen.getFrames()[0].setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		}
	}
	
	private void transfer(){
		
		String options[] = {"Yes", "No"};
		int response = MessageHandler.showOptionMessage(MessageHandler.MESSAGE_TYPE_CONFIRMATION, StringConstants.getString("VALIDATION_UPLOAD_CONFIRM_MSG"), options);
		if(response == JOptionPane.NO_OPTION){
			return;
		}
		
		ConfigurationError validationResult = Validator.checkConfiguration(configuration);
		//boolean serverValidationResult = Validator.checkServers(configuration);
		
		/*if(!serverValidationResult){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, Variables.configurationErrors.get(Variables.CONFIGURATION_ERROR_SERVER_LOCATION));
			return;
		}*/
		
		System.out.println("validationResult.getInvalidity():" + validationResult.getInvalidity());
		
		if(validationResult.getInvalidity()==Variables.CONFIGURATION_WARNING_UNKNOWN_ID_P3S){
			// find jawas which has unknown id
			int partialUpload = MessageHandler.showOptionMessage(MessageHandler.MESSAGE_TYPE_CONFIRMATION, StringConstants.getString("VALIDATION_WARNING_UNKNOWN_ID_P3S"), options);
			if(partialUpload == JOptionPane.NO_OPTION){
				return;
			}
		}else if(validationResult.getInvalidity()>Variables.CONFIGURATION_NO_ERROR){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, validationResult.getDescription());
			return;
		}
			
			
		MainScreen.getFrames()[0].setCursor(new Cursor(Cursor.WAIT_CURSOR));
		ConfigurationError err = configurationIO.uploadConfiguration(configuration);
		
		if(err == null){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_UPLOAD_FAIL_MSG"));
		}else{
			if(err.getInvalidity()==Variables.CONFIGURATION_NO_ERROR){
				MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_UPLOAD_SUCCESS_MSG"));
			}else{
				if(err.getInvalidity()==Variables.CONFIGURATION_ERROR_DISCONNECTED_CONTROLLER){
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_NO_CONTROLLER"));
				}else if(err.getInvalidity()==Variables.CONFIGURATION_ERROR_TRANSFER_CONFIGURATION){
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_UPLOAD_FAIL_MSG"));
				}else if(err.getInvalidity()==Variables.CONFIGURATION_ERROR_BUSY_CONTROLLER){
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_BUSY_CONTROLLER"));
				}
			}
		}
		
		MainScreen.getFrames()[0].setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		
	}
	
	private void receive(){
		String options[] = {"Yes", "No"};
		int response = MessageHandler.showOptionMessage(MessageHandler.MESSAGE_TYPE_CONFIRMATION, StringConstants.getString("VALIDATION_DOWNLOAD_CONFIRM_MSG"), options);
		if(response == JOptionPane.YES_OPTION){
			doReceive();
		}else{
			return;
		}
		
	}
	
	private void doReceive(){
		MainScreen.getFrames()[0].setCursor(new Cursor(Cursor.WAIT_CURSOR));
		
		ConfigurationError err = configurationIO.downloadConfiguration();
		configuration = (Configuration)err.getObject();
		
		if(err == null){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_DOWNLOAD_FAIL_MSG"));
		}else{
			if(err.getInvalidity()==Variables.CONFIGURATION_NO_ERROR){
				cManager.updataConfiguration(configuration);
				setMainTitle("");
				MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_DOWNLOAD_SUCCESS_MSG"));
			}else{
				if(err.getInvalidity()==Variables.CONFIGURATION_ERROR_DISCONNECTED_CONTROLLER){
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_NO_CONTROLLER"));
				}else if(err.getInvalidity()==Variables.CONFIGURATION_ERROR_RECEIVE_CONFIGURATION){
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_DOWNLOAD_FAIL_MSG"));
				}
			}
		}
		
		MainScreen.getFrames()[0].setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}
	
	private void deleteServers(){
		cManager.deleteSelections();
	}
	
	private void associatePhase(){
		
		if(selectionManager.getSelectedPlugmeters().size()==0){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_P3_SELECTION_PHASE_MSG"));
		}else if(selectionManager.getSelectedPlugmeters().size()>1){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_P3_SELECTION_PHASE_MSG"));
					
		}else{
			PlugmeterButton pm = selectionManager.getSelectedPlugmeters().get(0);
			Jawa jawa = pm.getJawa();
			
			if(!Validator.checkJawaId(jawa) || !Validator.checkJawaFeed(jawa)){
				MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_P3_PROHIBITED_PHASE_MSG"));
				return;
			}

			try{
				cManager.associatePhase();
				selectionManager.setServerSelection((ServerPanel)pm.getParent(), null);
				selectionManager.setPlugmeterSelection(pm, null);
				
			}catch(Exception e){
				logger.error("Fail to associate phase");
				logger.debug(LogManager.getStackTrace(e));
			}
			
		}
		
	}
	
	private void help(){
		Desktop deskTop = Desktop.getDesktop();
		logger.info("open helpfile");
		try {
			File helpFile = new File(StringConstants.getString("FILE_HELP"));
			//logger.info(MainToolbar.class.getResource(StringConstants.getString("FILE_HELP")).getPath());
			//logger.info(helpFile.exists());	
			//System.out.println(MainToolbar.class.getResource(StringConstants.getString("FILE_HELP")).getFile());
			deskTop.open(helpFile);
		} catch (IOException e) {
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_ERROR_NO_PDF_VIEWER"));
			logger.error("Cannot load help file");
			logger.debug(LogManager.getStackTrace(e));
		} catch(Exception e){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_ERROR_NO_USER_MANUAL"));
			
			logger.error("Cannot load help file because of exception");
			logger.debug(LogManager.getStackTrace(e));
		}
		
	}
	
	private void reset(){
		if(selectionManager.getSelectedPlugmeters().size()==0){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_NO_P3_SELECTION_RESET_MSG"));
		}else{
			ArrayList<Jawa> jawas = cManager.getSelectedJawas();
			try {
				configurationIO.resetP3(jawas);
			} catch (InvalidChecksum e) {
				// TODO Auto-generated catch block
				MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_COMMAND_WRONG_FORMAT_ID"));
				logger.debug(LogManager.getStackTrace(e));
			}
		}
	}
	
	private void activate(){
		if(selectionManager.getSelectedPlugmeters().size()==0){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_NO_P3_SELECTOPN_ACTIVATE_MSG"));
		}else{
			ArrayList<Jawa> jawas = cManager.getSelectedJawas();
			
			try {
				configurationIO.activateP3(jawas);
			} catch (InvalidChecksum e) {
				// TODO Auto-generated catch block
				MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_COMMAND_WRONG_FORMAT_ID"));
				logger.debug(LogManager.getStackTrace(e));
			}
			
		}
	}
	
	private void clearConfiguration(){
		MainScreen.getFrames()[0].setCursor(new Cursor(Cursor.WAIT_CURSOR));
		String options[] = {"Yes", "No"};
		
		if(!configurationIO.isControllerActive()){
			try {
				configurationIO.connectController();
			} catch (Exception e) {
				logger.error("Fail to connect to the controller");
			}
		}
		
		if(configurationIO.isControllerActive()){ // clear configuration in the controller
			int response = MessageHandler.showOptionMessage(MessageHandler.MESSAGE_TYPE_CONFIRMATION, StringConstants.getString("VALIDATION_CONFIRM_CLEAR_CONFIGURATION"), options);
			if(response == JOptionPane.YES_OPTION){
				ConfigurationError err = configurationIO.clearConfiguration();
				configuration = (Configuration)err.getObject();
				
				if(err == null){
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("CONFIGURATION_ERROR_CLEAR_CONFIGURATION"));
				}else{
					if(err.getInvalidity()==Variables.CONFIGURATION_NO_ERROR){
						cManager.updataConfiguration(configuration);
						setMainTitle("");
					}else{
						if(err.getInvalidity()==Variables.CONFIGURATION_ERROR_DISCONNECTED_CONTROLLER){
							MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_NO_CONTROLLER"));
						}else if(err.getInvalidity()==Variables.CONFIGURATION_ERROR_CLEAR_CONFIGURATION){
							MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_ERROR_CLEAR_CONFIGURATION"));
						}else if(err.getInvalidity()==Variables.CONFIGURATION_ERROR_BUSY_CONTROLLER){
							MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_ERROR_BUSY_CONTROLLER"));
						}
					}
				}
			}	
		}else{ // new local configuration
			int response = MessageHandler.showOptionMessage(MessageHandler.MESSAGE_TYPE_CONFIRMATION, StringConstants.getString("VALIDATION_CONFIRM_NEW_CONFIGURATION"), options);
			if(response == JOptionPane.YES_OPTION){
				configuration = new Configuration();
				cManager.updataConfiguration(configuration);
				setMainTitle("");
			}
		}
		MainScreen.getFrames()[0].setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		
	}
	
	private void createNewConfiguration(){
		configuration = new Configuration();
		cManager.updataConfiguration(configuration);
	}
	
	private void setMainTitle(String fileName){
		if(fileName.length()==0){
			MainScreen.getFrames()[0].setTitle(StringConstants.getString("TITLE"));
		}else{
			MainScreen.getFrames()[0].setTitle(StringConstants.getString("TITLE") + " - <" + fileName + ">");
		}
	}
	
	class  RelocatableTooltipButton extends JXButton{
		Point location;
		
		RelocatableTooltipButton(){
			super();
			location = null;
		}
		
		public Point getToolTipLocation(MouseEvent e){
			return location;
		}
		
		public void setToolTipLocation(Point location, int offset_x, int offset_y){
			Point newLocation = new Point(location.x-offset_x, location.y-offset_y);
			this.location = newLocation;
		}
	}
	
	class CustomAccessaryPanel extends JPanel{
		public CustomAccessaryPanel(JFileChooser fileChooser){
			final JFileChooser chooser = fileChooser;
			FlowLayout layout = new FlowLayout();
			layout.setAlignment(NORTH);
			setLayout(layout);
			this.setPreferredSize(new Dimension(100,90));
			this.setBorder(new LineBorder(Color.GRAY));
			JPanel panel = new JPanel();
			panel.setPreferredSize(new Dimension(100,30));
			
			JXButton newButton = new JXButton("New");
			newButton.setPreferredSize(new Dimension(80,25));
			panel.add(newButton, BorderLayout.CENTER);
			
			JPanel panel2= new JPanel();
			panel2.setPreferredSize(new Dimension(100,60));
			JXLabel descLabel = new JXLabel();
			descLabel.setText("Create New \nConfiguration.");
			descLabel.setLineWrap(true);
			descLabel.setMaxLineSpan(5);
			descLabel.setPreferredSize(new Dimension(80, 60));
			panel2.add(descLabel);
			this.add(panel2);
			this.add(panel);
			
			newButton.addActionListener(new ActionListener(){

				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					createNewConfiguration();
					chooser.cancelSelection();
				}
				
			});
		}
	}
}
