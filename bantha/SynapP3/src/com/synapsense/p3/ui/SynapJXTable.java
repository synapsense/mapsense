package com.synapsense.p3.ui;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.jdesktop.swingx.JXTable;

import com.synapsense.p3.utilities.ComponentsManager;
import com.synapsense.p3.utilities.SelectionManager;

public class SynapJXTable extends JXTable {
	private ComponentsManager cManager;
	private SelectionManager sManager;
	public SynapJXTable(final ComponentsManager cManager, SelectionManager sManager){
		super();
		this.cManager = cManager;
		this.sManager = sManager;
		
		//this.getActionMap().remove("find");
		// add find function
		this.getActionMap().put("find", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				cManager.showFindJawaDialog();
			}
		});
		
		
	}
	
}
