package com.synapsense.p3.ui;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

import org.apache.log4j.Logger;

import com.synapsense.p3.components.PlugmeterButton;
import com.synapsense.p3.components.RackTable;
import com.synapsense.p3.components.ServerPanel;
import com.synapsense.p3.components.RackTable.RackTableModel;
import com.synapsense.p3.exceptions.ServerCreationFailedException;
import com.synapsense.p3.model.Server;
import com.synapsense.p3.ui.JXButtonDraggable.RackTypes;
import com.synapsense.p3.utilities.ComponentsManager;
import com.synapsense.p3.utilities.LogManager;
import com.synapsense.p3.utilities.MessageHandler;
import com.synapsense.p3.utilities.SelectionManager;
import com.synapsense.p3.utilities.StringConstants;
import com.synapsense.p3.utilities.Validator;

public class DropTransferHandler extends TransferHandler {
	private static final long serialVersionUID = -4657677141570657868L;
	private ComponentsManager cManager;
	private SelectionManager selectionManager;
	
	private static Logger logger = Logger.getLogger(DropTransferHandler.class.getName());
	
	public DropTransferHandler(ComponentsManager cManager, SelectionManager selectionManager){
		this.cManager = cManager;
		this.selectionManager = selectionManager;
	}
	
	public int getSourceActions(JComponent c) {
         return DnDConstants.ACTION_MOVE;
     }
   
	public Transferable createTransferable(JComponent comp){
		 /*
		JTable table=(JTable)comp;
    	 int row=table.getSelectedRow();
    	 int col=table.getSelectedColumn();
    	
    	 if(col==1){
    		 ServerOnRack sor = (ServerOnRack)table.getModel().getValueAt(row,col);
    		 
    		 if(sor!=null){
        		 Transferable t = new StringSelection(sor.toString());
                 return t;
    			 
    		 }
         }*/
         return null;
     }
	
	@Override
	public boolean importData(TransferSupport support) {
		// javax.swing.JTable.DropLocation:
		// - java.awt.Point dropPoint
		// - row
		// - column
		
		Transferable dropItem = support.getTransferable();
		try {
			String s = (String) dropItem.getTransferData(DataFlavor.stringFlavor);
			
			if(s.equals("PLUGMETER") || s.equals("SRV1U") || s.equals("SRV1U1P") || s.equals("SRV1U2P") || s.equals("SRV4U") || s.equals("SRV5U") || s.equals("SRVCUSTOM")){
				addElementToRackPanel(s,(RackTable)support.getComponent(),(javax.swing.JTable.DropLocation)support.getDropLocation());
			}
			 
		} catch (UnsupportedFlavorException e) {
			logger.error(e.getLocalizedMessage());
		} catch (IOException e) {
			logger.error(e.getLocalizedMessage());
		}
		return super.importData(support);
	}

	/**
	 * 
	 */

	public boolean canImport(JComponent dest, DataFlavor[] flavors) {
		return true;
	}

	private void addElementToRackPanel(String type, RackTable destination, javax.swing.JTable.DropLocation location) {
		
		int row = location.getRow();
		int col = location.getColumn();			
		RackTableModel data = (RackTableModel) destination.getModel();
		
		if(col!=1)
			return;
		
		Integer serverValue = null;
		Integer plugmeterValue = null;
		switch (RackTypes.valueOf(type)) {		
		case PLUGMETER:
			try{
//				ServerOnRack sor = (ServerOnRack) data.getValueAt(row, col); 
				ServerPanel sor = data.getServerAtRow(row);
				
				if(sor==null){
					logger.error("Cannot add smartplug without server");
					return;
				}
				
				Server server = sor.getServer();
				
				if(Validator.checkMaxPlugmeter(server)){
					PlugmeterButton pm = sor.createPlugmeter();
		
					selectionManager.setServerSelection(sor, null);
					selectionManager.setPlugmeterSelection(pm, null);
					cManager.setFocusPlugmeterID(pm);
				}else{
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_MAX_P3_ERROR_MSG"));
					
					return;
				}
				
			}catch(Exception e){
				logger.error("Fail to add new plugmeter");
				logger.debug(LogManager.getStackTrace(e));
			}
			
			break;		
		case SRV1U:
			plugmeterValue = 0;
			serverValue = 1;
			break;		
		case SRV2U:
			plugmeterValue = 0;
			serverValue = 2;
			break;			
		case SRV3U:
			plugmeterValue = 0;
			serverValue = 3;
			break;			
		case SRV4U:
			plugmeterValue = 0;
			serverValue = 4;
			break;			
		case SRV5U:
			plugmeterValue = 0;
			serverValue = 5;
			break;
		case SRVCUSTOM:
			plugmeterValue = 0;
			serverValue = 1;
			break;
		case SRV1U1P:
			serverValue = 1;
			plugmeterValue = 1;
			break;
		case SRV1U2P:
			serverValue = 1;
			plugmeterValue =2;
			break;
		default:
			logger.warn(StringConstants.getString("LOGGER_WARNING_UNKNOWN_OBJECT_DRAGGED"));
			break;
		}
		
		if (serverValue!=null) {
			try{
				if(Validator.checkServerLocation(row, serverValue, destination.getRack(), null)){
					ServerPanel serverPanel = ((RackTableModel)data).addData(new Object[]{row, serverValue,null});
					selectionManager.setServerSelection(serverPanel, null);
					
					// add plugmeter
					if(plugmeterValue!=null){
						if(plugmeterValue==0){
							selectionManager.clearPlugmeterSelection(null);
						}
						else if(plugmeterValue==1){
							PlugmeterButton pm = serverPanel.createPlugmeter(1);
							selectionManager.setPlugmeterSelection(pm, null);
							cManager.setFocusPlugmeterID(pm);
						}else if(plugmeterValue==2){
							PlugmeterButton pm1 = serverPanel.createPlugmeter(1);
							PlugmeterButton pm2 = serverPanel.createPlugmeter(2);
							selectionManager.setPlugmeterSelection(pm1, null);
							cManager.setFocusPlugmeterID(pm1);
						}
					}
				}else{
					//destination.repaint();
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_ADD_SERVER_INSUFFICIENT_SPACE_MSG"));
					System.out.println("cannot locate server");
				}
				
			}catch(ServerCreationFailedException e){
				MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_ADD_SERVER_INSUFFICIENT_SPACE_MSG"));
				
			}
			catch(Exception e){
				logger.error("Fail to add new server");
				logger.debug(LogManager.getStackTrace(e));
					
			}
			
			
		}
		
	}
	
	/*
	private void moveElementInRackPanel(String dragData, RackSlotsTable rackTable, javax.swing.JTable.DropLocation location){
		int toRow = location.getRow();
		
		String[] serverInfo = dragData.split(";");
		int rackType = Integer.parseInt(serverInfo[0].replace("rack:", ""));
		int row = Integer.parseInt(serverInfo[1].replace("row:", ""));
		
		JXTable oldParent = ((RackPanel)(MainScreen.getRackPanelByType(rackType))).table;
		RackTableModel oldParentModel = (RackTableModel) oldParent.getModel();
		ServerOnRack sor = (ServerOnRack) oldParentModel.getValueAt(row, 1);
		oldParentModel.removeFrom(sor);
		
		Rack oldRack = sor.getServer().getRack();
		oldRack.deleteServer(sor.getServer());
		
		Rack newRack = (Rack)rackTable.getRack();
		sor.getServer().setRack(newRack);
		int newlocation = newRack.getUheight() - toRow;
		newRack.addServer(sor.getServer());
		sor.getServer().setLocation(newlocation);
		 
		((RackTableModel)rackTable.getModel()).moveTo(sor, toRow);
		((RackTableModel)rackTable.getModel()).fireTableDataChanged();
	}
	*/
	protected void exportDone(JComponent source, Transferable data, int action){
	}
}
