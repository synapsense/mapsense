 package com.synapsense.p3.ui;

import java.awt.Dimension;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.ComponentOrientation;
import java.awt.GridBagLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.JTableHeader;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.painter.CompoundPainter;
import org.jdesktop.swingx.painter.GlossPainter;
import org.jdesktop.swingx.painter.MattePainter;
import org.jdesktop.swingx.JXLabel.TextAlignment;
import org.jdesktop.swingx.JXMultiSplitPane;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.MultiSplitLayout;

import com.synapsense.p3.components.PlugmeterButton;
import com.synapsense.p3.components.ServerPanel;
import com.synapsense.p3.exceptions.JawaCreationFailedException;
import com.synapsense.p3.model.Configuration;
import com.synapsense.p3.model.ConfigurationError;
import com.synapsense.p3.scinter.JawaData;
import com.synapsense.p3.ui.ErrorsTable.ErrorsTableModel;
import com.synapsense.p3.ui.PlugmetersTable.PlugmetersTableModel;
import com.synapsense.p3.utilities.*;

public class PlugmetersPanel extends JXPanel implements HierarchyListener,SelectionChangeListener {
	private static Logger logger = Logger.getLogger(PlugmetersPanel.class.getName());
	
	private static final int DEF_PANEL_HEIGHT = StringConstants.getIntValue("P3TABLE_HEIGHT");
	private static final int DEF_PANEL_WIDTH = StringConstants.getIntValue("P3TABLE_WIDTH");
	
	private JXMultiSplitPane splitPane = new JXMultiSplitPane();
	private JXPanel northPanel = new JXPanel();
	private JXPanel southPanel = new JXPanel();
	private JXPanel centerPanel = new JXPanel();
	
	// server detail title
	private final JXLabel northCaption = new JXLabel();	
	private final JXLabel southCaption = new JXLabel();	
	private final JXLabel centerCaption = new JXLabel();
	
	// when a user clicks server, showing server detail panel
	private JPanel serverDetailPanel = new JPanel();
	private CardLayout cardLayout = new CardLayout();
	
	// when a user add new server, by default facePlate panel will show up
	private JXPanel faceplatePanel = new JXPanel();
	private JLabel lblCurrent = new JLabel(StringConstants.getString("FACEPLATE_CURRENT_LABEL")); 
	public NumberFormattedTextField currentTxt = new NumberFormattedTextField();
	
	// when a user add a plugmeter into a server, plugmeter list will show up
	private final JXPanel plugmeterListPanel = new JXPanel();	
	private final JXPanel plugmeterTablePanel = new JXPanel();
	private PlugmetersTable plugmetersTable;
	private PlugmetersTableModel plugmeterTableModel;
	
	
	// buttons panel for plugmeter list panel
	private final JXPanel addDelPanel = new JXPanel();
	private JButton btnRemove = new JButton(StringConstants.getString("REMOVE"));
	private JButton btnAdd = new JButton(StringConstants.getString("ADD"));
	
	// configuration error panel
	private JXPanel errorPanel = new JXPanel();
	private ErrorsTable errorTable ;
	private ErrorsTableModel errorTableModel ;
	
	// realtime data panel
	private JXPanel dataPanel = new JXPanel();
	private JXLabel downloadLabel = new JXLabel(StringConstants.getString("LABEL_ENABLED_DATA_DOWNLOAD"));
	private JXLabel phaseLabel = new JXLabel(StringConstants.getString("LABEL_PHASE"));
	private JXLabel demandpowerLabel = new JXLabel(StringConstants.getString("LABEL_DEMAND_POWER"));
	private JXLabel avgcurrentLabel = new JXLabel(StringConstants.getString("LABEL_AVG_CURRENT"));
	private JXLabel voltageLabel = new JXLabel(StringConstants.getString("LABEL_VOLTAGE"));
	
	private JXLabel phaseValueLabel = new JXLabel();
	private JXLabel demandpowerValueLabel = new JXLabel();
	private JXLabel avgcurrentValueLabel = new JXLabel();
	private JXLabel voltageValueLabel = new JXLabel();
	
	// model objects
	private Configuration configuration;
	private ConfigurationIO cIo;
	private ComponentsManager cManager;
	private SelectionManager selectionManager ;
	private ServerPanel currentServer;
	private PlugmeterButton currentPlugmeter;
	
	private JCheckBox downloadCheckbox = new JCheckBox();

	private Timer timer = null;
	
	private final String FACE_PLATE = "faceplate";
	private final String PLUGMETER_LIST = "plugmeterlist";
	
	public PlugmetersPanel(ComponentsManager cManager, ConfigurationIO io, SelectionManager selectionManager) {
		this.cManager = cManager;
		this.cIo = io;
		this.selectionManager = selectionManager;
		
		plugmetersTable = new PlugmetersTable(cManager, selectionManager);
		plugmeterTableModel = (PlugmetersTableModel)plugmetersTable.getModel();
		
		errorTable = new ErrorsTable(cManager, selectionManager);
		errorTableModel = (ErrorsTableModel)errorTable.getModel();
		
		selectionManager.addSelectionChangeListener(plugmetersTable);
		selectionManager.addSelectionChangeListener(errorTable);
		
		setPaintBorderInsets(false);
		setBorder(new LineBorder(Color.LIGHT_GRAY));
		
		setInheritAlpha(false);
		setForeground(Color.BLACK);
		adjustSize();
		initComponents();
		this.addHierarchyListener(this);
	}
	
	public JXTable getPlugmetersTable(){
		return plugmetersTable;
	}
	
	public JXTable getErrorsTable(){
		return errorTable;
	}
	
	public void setConfiguration(Configuration config){
		configuration = config;
		plugmetersTable.setConfiguration(config);
		//bindData();
	}

	public void initComponents() {					
		setBackground(Color.WHITE);
		//setBorder(new EmptyBorder(2, 2, 2, 2));
		setOpaque(true);
		
		initNorthCaption();
		initSouthCaption();
		initCenterCaption();
		initSplitPane();
		
		initPlugmetersPanel();
		initFaceplatePanel();
		initAddDelPanel();
		
		initErrorTable();
		initDataPanel();
		
		plugmeterListPanel.setOpaque(false);
		//plugmeterListPanel.setBorder(new EtchedBorder(Color.LIGHT_GRAY, Color.DARK_GRAY)); // EmptyBorder(2, 2, 2, 2));
		plugmeterListPanel.setLayout(new BorderLayout());
		plugmeterListPanel.setPreferredSize(new Dimension(DEF_PANEL_WIDTH, DEF_PANEL_HEIGHT));
		plugmeterListPanel.add(plugmeterTablePanel, BorderLayout.CENTER);
		plugmeterListPanel.add(addDelPanel, BorderLayout.SOUTH);
		//
		//serverDetailPanel.setPreferredSize(new Dimension(DEF_PANEL_WIDTH, DEF_NORTHPANEL_HEIGHT));
		serverDetailPanel.setLayout(cardLayout);
		serverDetailPanel.setBorder(new EtchedBorder(Color.LIGHT_GRAY, Color.DARK_GRAY));
		serverDetailPanel.add(plugmeterListPanel, PLUGMETER_LIST);
		serverDetailPanel.add(faceplatePanel, FACE_PLATE);
		
		cardLayout.show(serverDetailPanel, PLUGMETER_LIST);
		northPanel.setLayout(new BorderLayout());
		//northPanel.setPreferredSize(new Dimension(DEF_PANEL_WIDTH, DEF_NORTHPANEL_HEIGHT));
		northPanel.add(northCaption, BorderLayout.NORTH);
		northPanel.add(serverDetailPanel, BorderLayout.CENTER);
		
		southPanel.setLayout(new BorderLayout());
		//southPanel.setPreferredSize(new Dimension(DEF_PANEL_WIDTH, 300));
		southPanel.add(southCaption, BorderLayout.NORTH);
		southPanel.add(errorPanel, BorderLayout.CENTER);
		
		centerPanel.setLayout(new BorderLayout());
		centerPanel.add(centerCaption, BorderLayout.NORTH);
		centerPanel.add(dataPanel, BorderLayout.CENTER);
		
		add(splitPane, BorderLayout.CENTER);
		splitPane.setOpaque(false);
		
		this.addKeyListener(new FunctionKeyAdapter(cManager , this));
		this.addComponentListener(new java.awt.event.ComponentAdapter() {
			public void componentResized(final ComponentEvent e)
			{
				//System.out.println("componentResized");
				adjustChildrenSizes();
			}
		});
	}

	public void adjustChildrenSizes(){
		System.out.println("adjustChildrenSizes p3tablePanel:" + this.getWidth());
		//this.setSize(new Dimension(DEF_PANEL_WIDTH, this.getHeight()));
		//northPanel.setSize(this.getWidth(), northPanel.getHeight());
		splitPane.setSize(new Dimension(DEF_PANEL_WIDTH, this.getHeight()));
		//System.out.println("this height:" + this.getHeight());
		//System.out.println("calculated southpanel:" + (this.getHeight()-northPanel.getHeight()-centerPanel.getHeight()));
		
		//System.out.println("southPanel height:" + southPanel.getHeight());
		//System.out.println("errorPanel height:" + errorPanel.getHeight());
		//System.out.println("errorTable height:" + errorTable.getHeight());
		int calculated_southPanel_height = this.getHeight()-northPanel.getHeight()-centerPanel.getHeight()-3;
		southPanel.setSize(southPanel.getWidth(),calculated_southPanel_height);
		errorPanel.setSize(errorPanel.getWidth(), southPanel.getHeight()-22);
		errorTable.setSize(errorTable.getWidth(), southPanel.getHeight()-46);
		splitPane.repaint();	
	}
	
	private void initPlugmetersPanel(){
		BorderLayout bl_panel = new BorderLayout();
		//bl_panel.setHgap(5);
		JTableHeader header = plugmetersTable.getTableHeader();
		plugmeterTablePanel.setLayout(bl_panel);
		plugmeterTablePanel.add(header, BorderLayout.NORTH);
		plugmeterTablePanel.add(plugmetersTable,  BorderLayout.CENTER);		
		plugmeterTablePanel.setPreferredSize(new Dimension(DEF_PANEL_WIDTH, 300));
	}
	
	private void initFaceplatePanel(){
		final JXPanel filler = new JXPanel();
		Dimension phasePanelPrefSize = new Dimension(DEF_PANEL_WIDTH-10, DEF_PANEL_HEIGHT); // square
		filler.setPreferredSize(phasePanelPrefSize);
		
		final GridBagLayout gbl_phasePanel = new GridBagLayout();
		//gbl_phasePanel.columnWidths = new int[] {  0, 0 };
		gbl_phasePanel.rowHeights = new int[] { 20, 20, 30 };
		//gbl_phasePanel.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gbl_phasePanel.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		filler.setLayout(gbl_phasePanel);
		//lblCurrent.setLocation(new Point(10, 0));
		lblCurrent.setHorizontalTextPosition(SwingConstants.RIGHT);
		final GridBagConstraints gbc_lblCurrent = new GridBagConstraints();
		gbc_lblCurrent.insets = new Insets(5, 5, 5, 5);
		gbc_lblCurrent.fill = GridBagConstraints.VERTICAL;
		gbc_lblCurrent.gridx = 0;
		gbc_lblCurrent.gridy = 0;
		filler.add(lblCurrent, gbc_lblCurrent);
	
		lblCurrent.setLabelFor(currentTxt);
		final GridBagConstraints gbc_currentTxt = new GridBagConstraints();
		gbc_currentTxt.insets = new Insets(5, 5, 5, 0);
		gbc_currentTxt.fill = GridBagConstraints.VERTICAL;
		gbc_currentTxt.gridx = 1;
		gbc_currentTxt.gridy = 0;
		currentTxt.setSelectedTextColor(Color.WHITE);
		currentTxt.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		currentTxt.setPreferredSize(new Dimension(30,15));
		currentTxt.setColumns(5);
		currentTxt.setFocusable(true);
		
		currentTxt.addKeyListener(new KeyListener(){

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if(e.getKeyCode()==KeyEvent.VK_ENTER){
					currentTxt.commitEditQuit();
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
			}

			@Override
			public void keyTyped(KeyEvent arg0) {
			}
			
		});
		
		
		currentTxt.addPropertyChangeListener(new PropertyChangeListener(){

			@Override
			public void propertyChange(PropertyChangeEvent e) {
				if(e.getPropertyName().equals("value") ){
					if(currentServer==null){
						return;
					}
					
					if(e.getNewValue()!=null){
						String newValue = e.getNewValue().toString();
						double powerValue = Double.parseDouble(newValue);
						
						currentServer.getServer().setPower(powerValue);
						
					}
					selectionManager.setServerSelection(currentServer, null);
				}
			}
		});
		
		currentTxt.addFocusListener(new FocusListener(){
			int loss = 0;
		    int gain = 0;
		    boolean valid = false;
			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				if (!e.isTemporary()) {
					if (!valid) {
						currentTxt.commitEditQuit();
						currentTxt.requestFocus();
					}
				}
			}
		});
		
		filler.add(currentTxt, gbc_currentTxt);
		faceplatePanel.add(filler);
		filler.setFocusable(true);
		
		filler.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent evt){
				currentTxt.commitEditQuit();
			}
		});
	}
	
	/*
	 * 
	 */
	private void initDataPanel(){
		dataPanel.setBorder(new EtchedBorder(Color.LIGHT_GRAY, Color.DARK_GRAY));
		dataPanel.setLayout(new BorderLayout());
		dataPanel.setPreferredSize(new Dimension(DEF_PANEL_WIDTH, 125));
		
		downloadCheckbox.setEnabled(false);
		downloadCheckbox.addItemListener(new ItemListener(){

			@Override
			public void itemStateChanged(ItemEvent arg0) {
				
				if(currentPlugmeter!=null){
					currentPlugmeter.setRealtime(downloadCheckbox.isSelected());
					bindRealtimeData();
				}
			}
			
		});
		
		final GridBagLayout gbl1 = new GridBagLayout();
		gbl1.columnWidths = new int[] { 10, 150};
		gbl1.rowHeights = new int[] { 20 };
		final JXPanel filler1 = new JXPanel();
		Dimension dataPanelPrefSize = new Dimension(DEF_PANEL_WIDTH, 25); // square
		filler1.setPreferredSize(dataPanelPrefSize);
		filler1.setLayout(gbl1);
		//filler1.setBorder(new LineBorder(Color.red));
		
		final GridBagConstraints gbc= new GridBagConstraints();
		gbc.insets = new Insets(0, 10, 0, 0);
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = 0;
		gbc.gridy = 0;
		filler1.add(downloadCheckbox, gbc);
		
		gbc.gridx = 1;
		filler1.add(downloadLabel, gbc);
		dataPanel.add(filler1, BorderLayout.NORTH);
	

		final GridBagLayout gbl2 = new GridBagLayout();
		gbl2.columnWidths = new int[] { 50, 150};
		gbl2.rowHeights = new int[] { 25, 25, 25, 25 };

		JXPanel filler2 = new JXPanel();
		Dimension dataPanelPrefSize2 = new Dimension(DEF_PANEL_WIDTH, 100); // square
		filler2.setPreferredSize(dataPanelPrefSize2);
		filler2.setLayout(gbl2);
		//filler2.setBorder(new LineBorder(Color.blue));
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		filler2.add(phaseLabel, gbc);
		
		gbc.gridx = 1;
		filler2.add(phaseValueLabel, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		filler2.add(demandpowerLabel, gbc);
		
		gbc.gridx = 1;
		filler2.add(demandpowerValueLabel, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		filler2.add(avgcurrentLabel, gbc);
		
		gbc.gridx = 1;
		filler2.add(avgcurrentValueLabel, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		filler2.add(voltageLabel, gbc);
		
		gbc.gridx = 1;
		filler2.add(voltageValueLabel, gbc);
		
		dataPanel.add(filler2, BorderLayout.CENTER);
	}
	
	public void showRealtimeData(){
		//PlugmeterButton pm = cManager.getPlugmeters().get(0);
		//Jawa jawa = pm.getJawa();
		System.out.println("try to get realtime data" +  currentPlugmeter.getJawa().getId());
		ConfigurationError err = cIo.getRealtimeData(currentPlugmeter.getJawa());
		
		if(err==null){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_ERROR_REALTIME_DATA_MSG"));
			logger.error("Cannot get realtime data of smartPlug");
		}else{
			if(err.getInvalidity()==Variables.CONFIGURATION_NO_ERROR){
				// show real time data 
				JawaData data = (JawaData) err.getObject();
				
				String phase = CommonFunctions.getPhaseFromRealtimeData(data.delta, data.phase);
				phaseValueLabel.setText(phase);
				demandpowerValueLabel.setText(Double.toString(data.kwDemand));
				avgcurrentValueLabel.setText(Double.toString(data.avgCurrent));
				voltageValueLabel.setText(Double.toString(data.volts));
				logger.info("Get real time data of selected plugmeters successfully");
				
				if(timer==null){
					timer = new Timer(60000, new PlugmeterDataTimerListener());
					timer.start();
					System.out.println("timer started");
				}
			}else{
				setBlankRealtimeData();
				
				if(err.getInvalidity()==Variables.CONFIGURATION_ERROR_DISCONNECTED_CONTROLLER){
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_REALTIME_DATA_NO_CONTROLLER_MSG"));
					logger.error("Cannot get real time data of plugmeter because no controller");
					downloadCheckbox.setSelected(false);
					bindRealtimeData();
				}else if(err.getInvalidity()==Variables.CONFIGURATION_ERROR_REALTIME_DATA){
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_ERROR_REALTIME_DATA_MSG"));
					logger.error("cannot get real time data of plugmeter");
					downloadCheckbox.setSelected(false);
					bindRealtimeData();
				}else if(err.getInvalidity()==Variables.CONFIGURATION_ERROR_P3_ID){
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_ERROR_REALTIME_DATA_MSG"));
					logger.error("cannot get real time data of plugmeter because of invalid id");
					downloadCheckbox.setSelected(false);
					bindRealtimeData();
				}else if(err.getInvalidity()== Variables.CONFIGURATION_ERROR_BUSY_CONTROLLER){
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_BUSY_CONTROLLER"));
					logger.error("Cannot issue command because the controller is busy");
					downloadCheckbox.setSelected(false);
					bindRealtimeData();
				}
			}
		}
	}
	
	public void bindPlugmeter(){
		if(selectionManager.getSelectedPlugmeters().size()==1){
			currentPlugmeter = selectionManager.getSelectedPlugmeters().get(0);
			downloadCheckbox.setEnabled(true);
			downloadCheckbox.setSelected(currentPlugmeter.getRealtime());
			
		}else{
			currentPlugmeter = null;
		}
		bindRealtimeData();
	}
	
	public void bindRealtimeData(){
		if(currentPlugmeter!=null){
			
			if(downloadCheckbox.isSelected()){
				showRealtimeData();		
			}else{
				if(timer!=null){
					timer.stop();
					timer = null;
					System.out.println("timer stopped");
				}
				setBlankRealtimeData();
			}
		//	dataPanel.repaint();
		}else{
			downloadCheckbox.setSelected(false);
			downloadCheckbox.setEnabled(false);
			setBlankRealtimeData();
			if(timer!=null){
				timer.stop();
				timer = null;
				System.out.println("timer stopped");
			}
		
		}
	}
	
	private void setBlankRealtimeData(){
		phaseValueLabel.setText("");
		demandpowerValueLabel.setText("");
		avgcurrentValueLabel.setText("");
		voltageValueLabel.setText("");
	
	}
	
	/**
	 * 
	 */
	private void initAddDelPanel() {
		final FlowLayout flowLayout = (FlowLayout) addDelPanel.getLayout();
		flowLayout.setVgap(10);
		flowLayout.setHgap(10);
		flowLayout.setAlignment(FlowLayout.RIGHT);					
		addDelPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		
		Dimension buttonSize = new Dimension(72, 24);
		btnRemove.setSize(buttonSize);
		btnRemove.setPreferredSize(buttonSize);
		btnRemove.setEnabled(false);
		btnRemove.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				deletePlugmeter();
			}
			
		});
		
		addDelPanel.add(btnRemove);
		
		btnAdd.setSize(buttonSize);
		btnAdd.setPreferredSize(buttonSize);
		btnAdd.setEnabled(false);
		btnAdd.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				try{
					addPlugmeter();
				}catch(JawaCreationFailedException e){
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_MAX_P3_ERROR_MSG"));
					return;
				}
			}
			
		});
		addDelPanel.add(btnAdd);
	}

	/**
	 * 
	 */
	private void initSplitPane() {
		splitPane.setBackground(Color.WHITE);
		splitPane.setForeground(Color.BLACK);
		splitPane.setFocusable(false);
		splitPane.setDividerSize(2); // hide divider
		splitPane.setBorder(null);
	
		String layoutDef ="(COLUMN north center south)";
		MultiSplitLayout.Node modelRoot = MultiSplitLayout.parseModel(layoutDef);
		//List childrenPanel = Arrays.asList(new Leaf("north"), new Divider(), new Leaf("center"), new Divider(), new Leaf("south"));
		//Split modelRoot = new Split();
		//modelRoot.setChildren(childrenPanel);
		
		splitPane.getMultiSplitLayout().setModel(modelRoot);
		splitPane.add(northPanel, "north");
		splitPane.add(centerPanel, "center");
		splitPane.add(southPanel, "south");
		
		
		//splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);		
		//splitPane.setDividerLocation(DEF_NORTHPANEL_HEIGHT);
		//splitPane.setLeftComponent(northPanel);
		//splitPane.setRightComponent(southPanel);
	}

	/**
	 * @param gloss
	 * @param matte
	 */
	private void initNorthCaption() {
	    GlossPainter gloss = new GlossPainter(new Color(1.0f, 1.0f, 1.0f, 0.2f), GlossPainter.GlossPosition.TOP);
	    MattePainter matte = new MattePainter(new Color(51, 51, 51));
		setLayout(new BorderLayout(0, 0));
		
		northCaption.setTextAlignment(TextAlignment.JUSTIFY);
		northCaption.setHorizontalTextPosition(SwingConstants.RIGHT);
		northCaption.setAlignmentX(Component.CENTER_ALIGNMENT);
		northCaption.setFont(new Font("Lucida Sans Unicode", Font.BOLD, 12));		
		northCaption.setForeground(Color.WHITE);
		northCaption.setText(StringConstants.getString("PLUGMETERS_LIST")); //$NON-NLS-1$
		northCaption.setBackgroundPainter(new CompoundPainter(matte, gloss));
		
		Dimension dim = new Dimension(DEF_PANEL_WIDTH, 18);
		northCaption.setSize(dim);
		northCaption.setPreferredSize(dim);
	}

	private void initSouthCaption() {
	    GlossPainter gloss = new GlossPainter(new Color(1.0f, 1.0f, 1.0f, 0.2f), GlossPainter.GlossPosition.TOP);
	    MattePainter matte = new MattePainter(new Color(51, 51, 51));
		setLayout(new BorderLayout(0, 0));
		
		southCaption.setTextAlignment(TextAlignment.JUSTIFY);
		southCaption.setHorizontalTextPosition(SwingConstants.RIGHT);
		southCaption.setAlignmentX(Component.CENTER_ALIGNMENT);
		southCaption.setFont(new Font("Lucida Sans Unicode", Font.BOLD, 12));		
		southCaption.setForeground(Color.WHITE);
		southCaption.setText(StringConstants.getString("TITLE_CONFIGURATION_ERROR")); //$NON-NLS-1$
		southCaption.setBackgroundPainter(new CompoundPainter(matte, gloss));
		
		Dimension dim = new Dimension(DEF_PANEL_WIDTH, 18);
		southCaption.setSize(dim);
		southCaption.setPreferredSize(dim);
	}
	
	private void initCenterCaption(){
		GlossPainter gloss = new GlossPainter(new Color(1.0f, 1.0f, 1.0f, 0.2f), GlossPainter.GlossPosition.TOP);
		MattePainter matte = new MattePainter(new Color(51, 51, 51));
		setLayout(new BorderLayout(0, 0));
		
		centerCaption.setTextAlignment(TextAlignment.JUSTIFY);
		centerCaption.setHorizontalTextPosition(SwingConstants.RIGHT);
		centerCaption.setAlignmentX(Component.CENTER_ALIGNMENT);
		centerCaption.setFont(new Font("Lucida Sans Unicode", Font.BOLD, 12));		
		centerCaption.setForeground(Color.WHITE);
		centerCaption.setText(StringConstants.getString("TITLE_REALTIME_DATA"));
		centerCaption.setBackgroundPainter(new CompoundPainter(matte, gloss));
		
		Dimension dim = new Dimension(DEF_PANEL_WIDTH, 18);
		centerCaption.setSize(dim);
		centerCaption.setPreferredSize(dim);
	}
	

	
	/**
	 *  Initialize configuration error table
	 */
	private void initErrorTable() {
		BorderLayout bl_panel = new BorderLayout();
		JTableHeader errorTableHeader = errorTable.getTableHeader();
		
		bl_panel.setHgap(5);
		errorPanel.setLayout(bl_panel);
		errorPanel.setBorder(new EtchedBorder(Color.LIGHT_GRAY, Color.DARK_GRAY));
		errorPanel.add(errorTableHeader, BorderLayout.NORTH);
		errorPanel.add(errorTable,  BorderLayout.CENTER);		
	}

	@Override
	public void hierarchyChanged(final HierarchyEvent e) {
		if ((e.getChangeFlags() & (long)(HierarchyEvent.PARENT_CHANGED))!=0) {
			adjustSize();
		}
	}
	
	void adjustSize() {
		setMinimumSize(new Dimension(DEF_PANEL_WIDTH, 36));
		setMaximumSize(new Dimension(DEF_PANEL_WIDTH, 2147483647));
		
		final Component parent = this.getParent();
		
		if (parent!=null) {
			final int newHeight = parent.getHeight();
			final int newWidth  = DEF_PANEL_WIDTH;
			
			setPreferredSize(new Dimension(newWidth, newHeight));
		}
	}
	
	/*public void bindData(){
		ArrayList<ServerPanel> sors = cManager.getServers();
		
		if((currentServer!=null) && (currentServer.getServer().getJawas().size()==0)){
				currentTxt.commitEditQuit();
		}
		if(sors.size() == 1){
			btnAdd.setEnabled(true);
			btnRemove.setEnabled(true);
			currentServer = sors.get(0);
			if(currentServer.getServer().getJawas().size() ==0){
				cardLayout.show(serverDetailPanel, FACE_PLATE);
				northCaption.setText(StringConstants.getString("FACEPLATE")); //$NON-NLS-1$
				currentTxt.setText(String.valueOf(currentServer.getServer().getPower()));
				
			}else{
				cardLayout.show(serverDetailPanel, PLUGMETER_LIST);
				northCaption.setText(StringConstants.getString("PLUGMETERS_LIST")); //$NON-NLS-1$
				plugmeterTableModel.bindData(sors.get(0));
				plugmeterTableModel.fireTableDataChanged();
				//table.requestFocus();
			}
			
			bindErrorData();
		}else{
			currentServer = null;
			btnAdd.setEnabled(false);
			btnRemove.setEnabled(false);
			cardLayout.show(serverDetailPanel, PLUGMETER_LIST);
			
			northCaption.setText(StringConstants.getString("PLUGMETERS_LIST")); //$NON-NLS-1$
			plugmeterTableModel.bindData(null);
			plugmeterTableModel.fireTableDataChanged();

			bindErrorData();
		}
	}*/
	
	public void bindErrorData(){
		errorTableModel.bindData(currentServer);
		errorTableModel.fireTableDataChanged();
	}
	
	public void addPlugmeter() throws JawaCreationFailedException{
		ServerPanel sor = plugmeterTableModel.getServerOnRack();
		PlugmeterButton pm = sor.createPlugmeter();
		
		// select new plugmeter
		selectionManager.setServerSelection((ServerPanel)pm.getParent(), null);
		
		selectionManager.setPlugmeterSelection(pm, null);
		
		cManager.setFocusPlugmeterID(pm);
		//selector.reloadScreen();
	}
	
	void deletePlugmeter(){
		
		if(plugmetersTable.getSelectedRow()>-1){
			
			ArrayList<PlugmeterButton> clonePlugmeters = (ArrayList<PlugmeterButton>) selectionManager.getSelectedPlugmeters().clone();
			for(PlugmeterButton pm:clonePlugmeters){

				int selectedRow = plugmeterTableModel.getRowByJawaIndex(pm.getJawa().getIndex());
				
				if(selectedRow>=0){		
					//cManager.deselectPlugmeter(pm);
					plugmeterTableModel.removeRow(selectedRow);
					selectionManager.clearPlugmeterSelection(null);
				}
			}
		}else{
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_P3_SELECTION_REMOVE_MSG"));
		}
	}
	
		
	class PlugmeterDataTimerListener implements ActionListener{
		PlugmeterDataTimerListener(){
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			showRealtimeData();
		}
	}
	
	class NumberFormattedTextField extends JFormattedTextField{
		public NumberFormattedTextField(){
			super(new DecimalFormat("##.###"));
		}
		
		public void commitEditQuit(){
			System.out.println("commitEditQuit");
			try {
				if(this.getValue()==null)
					return;
				if(isEditValid()){
					
					double oldValue = currentServer.getServer().getPower();
					double newValue = Double.parseDouble(this.getValue().toString());
					
					//System.out.println("new value:" +  this.getValue());
					//System.out.println("old value:" +  currentServer.getServer().getPower());	
					if(oldValue == newValue)
						return;
					
					super.commitEdit();
				}else{
					System.out.println("old power value:" + this.getValue());
					this.setValue(this.getValue());
					MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_FACEPLATE_POWER_LIMITATION_MSG"));
					
				}	
			} catch (ParseException e) {
				logger.error("Invalid faceplate power value");
				logger.debug(LogManager.getStackTrace(e));
				MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_FACEPLATE_POWER_LIMITATION_MSG"));
			}
		}
		
		public boolean isEditValid(){
			
			if(currentServer==null)
				return true;
			if(currentServer.getServer().getJawas().size()>0)
				return true;

			AbstractFormatter formatter = this.getFormatter();
			
			if(formatter != null) {
				String text = this.getText();
				try {
					formatter.stringToValue(text);
					
					if(Validator.checkFaceplatePowerLimit(text)){
						return true;
					}else{
					   
						return false;
					}
				} catch (ParseException pe) {
					return false;
				}
			}else
				return false;
			//return (this.getInputVerifier().verify(this));
		}
		
	}

	@Override
	public void selectionChanged(SelectionChangeEvent e) {
		if(currentServer==null){
			System.out.println("null in selectionChanged");
		}else{
			if(currentServer.getServer().getJawas().size()==0 &&  currentTxt.isValid())
				currentTxt.commitEditQuit();
			//System.out.println("not null in selectionChanged");
		}
		//System.out.println("selectionChanged");
		// TODO Auto-generated method stub
		ArrayList<ServerPanel> selectedServers = selectionManager.getSelectedServers();
		ArrayList<PlugmeterButton> selectedPlugmeters = selectionManager.getSelectedPlugmeters();
		
		
		if(selectedServers.size()==1){
			// select one serverPanel
			//if((currentServer!=null) && (currentServer.getServer().getJawas().size()==0)){
			//	currentTxt.commitEditQuit();
			//}
			
			btnAdd.setEnabled(true);
			btnRemove.setEnabled(true);
			currentServer = selectedServers.get(0);
			
			// no plugmeter in selected server
			if(selectedServers.get(0).getPlugmeters().size()==0){
				cardLayout.show(serverDetailPanel, FACE_PLATE);
				northCaption.setText(StringConstants.getString("FACEPLATE")); //$NON-NLS-1$
				currentTxt.setText(String.valueOf(currentServer.getServer().getPower()));
				
			}else{
				cardLayout.show(serverDetailPanel, PLUGMETER_LIST);
				northCaption.setText(StringConstants.getString("PLUGMETERS_LIST")); //$NON-NLS-1$
				
				if(selectedPlugmeters.size()==1){
					currentPlugmeter =selectedPlugmeters.get(0); 
					downloadCheckbox.setEnabled(true);
					downloadCheckbox.setSelected(currentPlugmeter.getRealtime());
				}
				
			}
			
		}else if(selectedServers.size()>1){
			// multi servers selection
			currentPlugmeter = null;
			currentServer = null;
			btnAdd.setEnabled(false);
			btnRemove.setEnabled(false);
			cardLayout.show(serverDetailPanel, PLUGMETER_LIST);
			
			northCaption.setText(StringConstants.getString("PLUGMETERS_LIST")); //$NON-NLS-1$
			
		}else{
			currentPlugmeter = null;
			currentServer = null;
			btnAdd.setEnabled(false);
			btnRemove.setEnabled(false);
			cardLayout.show(serverDetailPanel, PLUGMETER_LIST);
			
			northCaption.setText(StringConstants.getString("PLUGMETERS_LIST")); //$NON-NLS-1$
			
		}
		
		/*
		ArrayList<Object> selections = e.getAddtions();
		
		if(selections.size()==1){
			if(selections.get(0) instanceof PlugmeterButton){
				currentPlugmeter =(PlugmeterButton) selections.get(0); 
				downloadCheckbox.setEnabled(true);
				downloadCheckbox.setSelected(currentPlugmeter.getRealtime());
					
			}else if(selections.get(0) instanceof ServerPanel){
				
				if((currentServer!=null) && (currentServer.getServer().getJawas().size()==0)){
					currentTxt.commitEditQuit();
				}
				
				btnAdd.setEnabled(true);
				btnRemove.setEnabled(true);
				currentServer = (ServerPanel)selections.get(0);

				if(currentServer.getServer().getJawas().size() ==0){
					cardLayout.show(serverDetailPanel, FACE_PLATE);
					northCaption.setText(StringConstants.getString("FACEPLATE")); //$NON-NLS-1$
					currentTxt.setText(String.valueOf(currentServer.getServer().getPower()));
					
				}else{
					cardLayout.show(serverDetailPanel, PLUGMETER_LIST);
					northCaption.setText(StringConstants.getString("PLUGMETERS_LIST")); //$NON-NLS-1$
					//plugmeterTableModel.bindData(currentServer);
					//plugmeterTableModel.fireTableDataChanged();
				}
					
				//bindErrorData();
			}			
			
		}else{
			currentPlugmeter = null;
			
			currentServer = null;
			btnAdd.setEnabled(false);
			btnRemove.setEnabled(false);
			cardLayout.show(serverDetailPanel, PLUGMETER_LIST);
			
			northCaption.setText(StringConstants.getString("PLUGMETERS_LIST")); //$NON-NLS-1$
			//plugmeterTableModel.bindData(null);
			//plugmeterTableModel.fireTableDataChanged();

			//bindErrorData();

		}
		*/
		bindRealtimeData();
		
	}
}