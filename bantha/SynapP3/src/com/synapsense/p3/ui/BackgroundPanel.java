package com.synapsense.p3.ui;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JComponent;

import com.synapsense.p3.utilities.StringConstants;

/**
 * background panel for mainPane
 * @author HLim
 *
 */
public class BackgroundPanel extends JComponent {
	
	public BackgroundPanel(){
		
	}
	// set up background color
	protected void paintComponent(Graphics g){
		//g.drawImage(image, 0,0, this.getWidth(), this.getHeight(), this);
		g.setColor(new Color(StringConstants.getIntValue("MAIN_BG_COLOR_R"),StringConstants.getIntValue("MAIN_BG_COLOR_G"),StringConstants.getIntValue("MAIN_BG_COLOR_B")));
		//g.drawRect(0, 0, this.getWidth(), this.getHeight());
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
	}
	
}
