package com.synapsense.p3.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ContainerEvent;

import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXLabel.TextAlignment;
import javax.swing.SwingConstants;

public class RackIndexLabel extends JXLabel {

	public RackIndexLabel() {
		setHorizontalAlignment(SwingConstants.RIGHT);
		setVerticalAlignment(SwingConstants.TOP);
		setTextAlignment(TextAlignment.RIGHT);
		setForeground(Color.white);
	}

}