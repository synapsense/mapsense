package com.synapsense.p3.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import org.apache.log4j.Logger;

import com.synapsense.p3.scinter.BusySandCrawlerException;
import com.synapsense.p3.scinter.NoSandCrawlerException;
import com.synapsense.p3.scinter.SCConfigMessage;
import com.synapsense.p3.scinter.SCConfigService;
import com.synapsense.p3.scinter.SandCrawler;
import com.synapsense.p3.utilities.MessageHandler;
import com.synapsense.p3.utilities.StringConstants;

public class CommandRunnerDialog extends JDialog{
	private static Logger logger = Logger.getLogger(CommandRunnerDialog.class.getName());
	
	private JProgressBar progressBar = new JProgressBar();
	private JButton cancelButton;
	private JLabel statusLabel;
	
	private final int commandId;
	private volatile boolean keepRunning;
	private volatile int currentQ;
	private int totalQues = 0;
	private long jid ;
	private ArrayList<Long> jawaids;
	private SandCrawler sandCrawler;
	private SCConfigMessage msg = null;
	private SCConfigService configService;
	
	public CommandRunnerDialog(JFrame parent, ArrayList<Long> jawaids, final int commandId, SandCrawler sandCrawler){
		super(parent, StringConstants.getString("VALIDATION_INFORMATION_TITLE"), true);
		this.sandCrawler = sandCrawler;
		configService = sandCrawler.getConfigService();
		keepRunning = true;
		currentQ = 0;
		this.totalQues = jawaids.size();
		this.jawaids = jawaids;
		this.commandId = commandId;
		
		buildComponents();
		
		setLocationRelativeTo(parent);
		
		Runnable run = new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				executeCommand();
			}
			
		};
		Thread thread = new Thread(run, "progressBarThread");
		thread.start();	
	}
	
	private void buildComponents(){
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(null);

		progressBar.setIndeterminate(false);
		progressBar.setStringPainted(false);
		progressBar.setMinimum(0);
		progressBar.setMaximum(totalQues);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		progressBar.setBounds(10, 10, 300, 20);
		
		mainPanel.add(progressBar);
		
		statusLabel = new JLabel();
		updateStatus();
		statusLabel.setBounds(10, 30, 300, 30);
		mainPanel.add(statusLabel);
		
		cancelButton = new JButton("Cancel");
		cancelButton.setBounds(240, 60, 70, 25);
		mainPanel.add(cancelButton);
		cancelButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				cancelCommand();
			}
			
		});


		getContentPane().add(mainPanel);
		
		setSize(330,125);
		
	}
	
	private void updateStatus(){
		String commandStr = "";
		
		if(commandId==1){
			commandStr = StringConstants.getString("VALIDATION_RESET_STATUS_MSG");
		}else if(commandId==2){
			commandStr = StringConstants.getString("VALIDATION_IDENTIFY_STATUS_MSG");
		}

		String statusStr = commandStr + currentQ + " of " + totalQues;
		statusLabel.setText(statusStr);
	}
	
	
	private void command() throws NoSandCrawlerException{
		if(commandId==1){
			sandCrawler.sendResetCommand(jid);
		}else{
			sandCrawler.sendIdentifyCommand(jid);
		}
		
	}
	
	private void cancelCommand(){
		//cancelButton.setEnabled(false);
		try {
			Thread.currentThread().sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		keepRunning = false;
		this.setVisible(false);
	}
	
	private void informStatus(Exception e){
		if(e.getClass().equals(NoSandCrawlerException.class)){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_NO_CONTROLLER"));
			logger.error("Cannot issue command because no controller");
		}else if(e.getClass().equals(BusySandCrawlerException.class)){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_BUSY_CONTROLLER"));
			logger.error("Cannot issue command because the controller is busy");
			
		}else if(e.getClass().equals(IOException.class)){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_ERROR, StringConstants.getString("VALIDATION_ERROR_COMMAND"));
			logger.error("cannot issue commands");
		}
	}
	
	private void executeCommand(){
		
		jid = jawaids.get(currentQ);
		try {
			command();
		} catch (NoSandCrawlerException e1) {
			// TODO Auto-generated catch block
			cancelCommand();
			informStatus(e1);
			return;
		} 
		System.out.println("current q:" + currentQ);
		System.out.println("current jawa:" + jid);
	
		int retry = 100;
		
		while(keepRunning){
			if(msg ==null){
				System.out.println("no response yet" + jid);
				
				if (retry-- <= 0) {
					keepRunning = false;
				}
				
				try {
					msg = configService.receive();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					keepRunning = false;
				} catch (NoSandCrawlerException e) {
					// TODO Auto-generated catch block
					keepRunning = false;
				}
			}else{
				if(msg.type == SCConfigMessage.TYPE_R_COMMAND){
					System.out.println(jid+ " command response is " + msg.commandresponse);
					
					// sandCrawler is busy, stop command.
					if(msg.commandresponse==SCConfigMessage.TYPE_COMRESP_BUSYSMOTA){
						cancelCommand();
						informStatus(new BusySandCrawlerException());
					}
					
					currentQ++;
					progressBar.setValue(currentQ);
					updateStatus();
					
					if(currentQ<totalQues){
						jid = jawaids.get(currentQ);
						msg = null;
						System.out.println("current q:" + currentQ);
						System.out.println("current jawa:" + jid);
						
						
						try {
							command();
							msg = configService.receive();
						} catch (NoSandCrawlerException e) {
							cancelCommand();
							informStatus(e);
						} catch (IOException e) {
							cancelCommand();
							informStatus(e);
						} 
					}else{
						
						cancelCommand();
					}
				}
			}
		}
	}
}
