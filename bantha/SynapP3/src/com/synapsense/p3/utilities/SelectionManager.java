package com.synapsense.p3.utilities;

import java.util.ArrayList;

import com.synapsense.p3.components.PlugmeterButton;
import com.synapsense.p3.components.RackTable;
import com.synapsense.p3.components.ServerPanel;
import com.synapsense.p3.model.Rack;

public class SelectionManager {
	private ArrayList<SelectionChangeListener> selectionChangeListeners = new ArrayList<SelectionChangeListener>();
	//private ArrayList<Object> selectedObjects = new ArrayList<Object>();
	private boolean interrupted = false;
	
	private ArrayList<ServerPanel> selectedServers = new ArrayList<ServerPanel>();
	private ArrayList<PlugmeterButton> selectedPlugmeters = new ArrayList<PlugmeterButton>();
	private Rack selectedRack;
	
	
	public SelectionManager(){
		
	}
	
	public ArrayList<Object>getAllSelection(){
		ArrayList<Object> selectedObjects = new ArrayList<Object>();
		selectedObjects.addAll(selectedPlugmeters);
		selectedObjects.addAll(selectedServers);
		selectedObjects.add(selectedRack);
		
		return selectedObjects;
	}
	
	public ArrayList<ServerPanel> getSelectedServers(){
		return selectedServers;
	}
	
	public ArrayList<PlugmeterButton> getSelectedPlugmeters(){
		return selectedPlugmeters;
	}
	
	public Rack getSelectedRack(){
		return selectedRack;
	}
	
	public void setSelectedRack(Rack r){
		selectedRack = r;
	}
	
	public void clearSelection(){
		if(interrupted)
			return;
		SelectionChangeEvent ev = new SelectionChangeEvent();
		
		for(Object o:getAllSelection()){
			ev.remove(o);
		}
		
		selectedServers.clear();
		selectedPlugmeters.clear();
		selectedRack = null;
		
		notifyListeners(ev, null);
		
	}
	
	public void addServerSelection(ServerPanel object, SelectionChangeListener self){
		if(interrupted)
			return;
		SelectionChangeEvent ev = new SelectionChangeEvent();
		
		selectedServers.add(object);
		ev.add(object);
		setSelectedRack(object.getServer().getRack());
		
		notifyListeners(ev, self);
	}
	
	public void addPlugmeterSelection(PlugmeterButton object, SelectionChangeListener self){
		if(interrupted)
			return;
		SelectionChangeEvent ev = new SelectionChangeEvent();
		
		selectedPlugmeters.add(object);
		ev.add(object);
		
		setSelectedRack(object.getJawa().getServer().getRack());
		
		notifyListeners(ev, self);
	}
	
	
	public void removeServerSelection(ServerPanel object,SelectionChangeListener self){	
		if(interrupted)
			return;
		
		SelectionChangeEvent ev = new SelectionChangeEvent();
		
		selectedServers.remove(object);
		ev.remove(object);
		
		notifyListeners(ev, self);
	}
	
	public void removeServerSelection(ArrayList<ServerPanel> objects, SelectionChangeListener self){
		if(interrupted)
			return;
		
		SelectionChangeEvent ev = new SelectionChangeEvent();
		
		for(ServerPanel object:objects){
			selectedServers.remove(object);
			ev.remove(object);
		}
		
		notifyListeners(ev, self);
	}
	
	public void removePlugmeterSelection(PlugmeterButton object,SelectionChangeListener self){	
		if(interrupted)
			return;
		
		SelectionChangeEvent ev = new SelectionChangeEvent();
		
		selectedPlugmeters.remove(object);
		ev.remove(object);
			
		
		notifyListeners(ev, self);
	}
	
	public void clearServerSelection(SelectionChangeListener self){
		if(interrupted)
			return;
		
		SelectionChangeEvent ev = new SelectionChangeEvent();
		for(ServerPanel o:selectedServers){
			ev.remove(o);
		}
		
		selectedServers.clear();
		
		notifyListeners(ev, self);
	}
	
	public void clearPlugmeterSelection(SelectionChangeListener self){
		if(interrupted)
			return;
		
		SelectionChangeEvent ev = new SelectionChangeEvent();
		for(PlugmeterButton o:selectedPlugmeters){
			ev.remove(o);
		}
		
		selectedPlugmeters.clear();
		
		notifyListeners(ev, self);
	}
	
	public void setServerSelection(ArrayList<ServerPanel> objects,SelectionChangeListener self){
		if(interrupted)
			return;
		
		SelectionChangeEvent ev = new SelectionChangeEvent();
		
		for(ServerPanel o: selectedServers){
			if(!objects.contains(o))
				ev.remove(o);
		}
		
		selectedServers.clear();
		
		for(ServerPanel o: objects){
			selectedServers.add(o);
			ev.add(o);
			setSelectedRack(o.getServer().getRack());
		}
		
		notifyListeners(ev, self);
	}
	
	public void setServerSelection(ServerPanel object, SelectionChangeListener self){
		if(interrupted)
			return;
		
		SelectionChangeEvent ev = new SelectionChangeEvent();
		
		for(ServerPanel o: selectedServers){
			if(!object.equals(o))
				ev.remove(o);
		}
		
		selectedServers.clear();
		
		selectedServers.add(object);
		ev.add(object);
		setSelectedRack(object.getServer().getRack());
		
		notifyListeners(ev, self);
	}
	
	public void setPlugmeterSelection(PlugmeterButton object, SelectionChangeListener self, boolean check){
		if(interrupted)
			return;
		
		SelectionChangeEvent ev = new SelectionChangeEvent();
		
		for(PlugmeterButton o: selectedPlugmeters){
			if(!object.equals(o))
				ev.remove(o);
		}
		
		selectedPlugmeters.clear();
		selectedPlugmeters.add(object);
		ev.add(object);
		
		ServerPanel parentObject = (ServerPanel)object.getParent();
		for(ServerPanel o: selectedServers){
			if(!parentObject.equals(o))
				ev.remove(o);
		}
		
		selectedServers.clear();
		selectedServers.add(parentObject);
		
		setSelectedRack(object.getJawa().getServer().getRack());
		notifyListeners(ev, self);
		
	}
	
	public void setPlugmeterSelection(ArrayList<PlugmeterButton> objects,SelectionChangeListener self){
		if(interrupted)
			return;
		
		SelectionChangeEvent ev = new SelectionChangeEvent();
		
		for(PlugmeterButton o: selectedPlugmeters){
			if(!objects.contains(o))
				ev.remove(o);
		}
		
		selectedPlugmeters.clear();
		
		for(PlugmeterButton o: objects){
			
			selectedPlugmeters.add(o);
			ev.add(o);
			setSelectedRack(o.getJawa().getServer().getRack());
		}
		
		notifyListeners(ev, self);
	}
	

	public void setPlugmeterSelection(PlugmeterButton object, SelectionChangeListener self){
		if(interrupted)
			return;
		
		SelectionChangeEvent ev = new SelectionChangeEvent();
		
		for(PlugmeterButton o: selectedPlugmeters){
			if(!object.equals(o))
				ev.remove(o);
		}
		
		selectedPlugmeters.clear();
		
			
		selectedPlugmeters.add(object);
		ev.add(object);
		setSelectedRack(object.getJawa().getServer().getRack());
		notifyListeners(ev, self);
	}
	
	public void addSelectionChangeListener(SelectionChangeListener l){
		selectionChangeListeners.add(l);
	}
	
	public void removeSelectionChangeListener(SelectionChangeListener l){
		selectionChangeListeners.remove(l);
	}
	
	public void notifyListeners(SelectionChangeEvent ev,SelectionChangeListener self){
		interrupted = true;
		
		if(self==null){
			for(SelectionChangeListener l: selectionChangeListeners){
					l.selectionChanged(ev);
			}
			
		}else{
			for(SelectionChangeListener l: selectionChangeListeners){
				if(!l.equals(self) )
					l.selectionChanged(ev);
			}
			
		}
		interrupted = false;
	}
	
	
}
