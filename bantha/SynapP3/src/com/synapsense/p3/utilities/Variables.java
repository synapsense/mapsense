package com.synapsense.p3.utilities;

import java.util.HashMap;

import com.synapsense.p3.model.PhaseAssociation;

public class Variables {
	public static final int LEFT_RACK_TYPE = 0;
	public static final int CENTER_RACK_TYPE = 1;
	public static final int RIGHT_RACK_TYPE = 2;

	public static final int RACK_MIN_HEIGHT = 0;
	public static final int RACK_MAX_HEIGHT = 96;
	public static final int RACK_DEFAULT_HEIGHT = 42;
	
	
	public static final double FACEPLATE_MIN_POWER = 0;
	public static final double FACEPLATE_MAX_POWER = 1000;
	public static final int P3_MAX_NUMBER = 8;
	public static final int P3_MAX_TOTAL_NUMBER = 252;
	
	public static final int COMMAND_RESET =1;
	public static final int COMMAND_IDENTIFY = 2;
	public static final int COMMAND_OUTLETORDER = 3;
	
	public static final int CONFIGURATION_NO_ERROR = 0;
	public static final int CONFIGURATION_ERROR_SERVER_FACEPLATE = 1;
	public static final int CONFIGURATION_ERROR_P3_ID = 2;
	public static final int CONFIGURATION_ERROR_P3_FEED = 3;
	public static final int CONFIGURATION_ERROR_MISSING_PHASE_ASSOCIATION = 4;
	public static final int CONFIGURATION_ERROR_P3_UNIQUEID = 5;
	public static final int CONFIGURATION_ERROR_P3 = 6;
	public static final int CONFIGURATION_ERROR_DISCONNECTED_CONTROLLER = 7;
	public static final int CONFIGURATION_ERROR_TRANSFER_CONFIGURATION = 8;
	public static final int CONFIGURATION_ERROR_RECEIVE_CONFIGURATION = 9;
	public static final int CONFIGURATION_ERROR_UNSATISFIEDLINK_DLL = 10;
	public static final int CONFIGURATION_ERROR_RESET_P3 = 11;
	public static final int CONFIGURATION_ERROR_ACTIVATE_P3 = 12;
	public static final int CONFIGURATION_ERROR_ACTIVATE_OUTLET_ORDER = 13;
	public static final int CONFIGURATION_ERROR_REALTIME_DATA = 14;
	public static final int CONFIGURATION_ERROR_OVERSIZED_P3 = 15;
	public static final int CONFIGURATION_ERROR_BUSY_CONTROLLER = 16;
	public static final int CONFIGURATION_ERROR_CLEAR_CONFIGURATION = 17;
	public static final int CONFIGURATION_ERROR_SERVER_LOCATION = 18;
	public static final int CONFIGURATION_ERROR_MIXED_PHASE_ASSOCIATION = 19;
	public static final int CONFIGURATION_WARNING_UNKNOWN_ID_P3S = 20;
	
	public static final int CONFIGURATION_SELECTION_MODE_SINGLE = 1;
	public static final int CONFIGURATION_SELECTION_MODE_MULTIPLE = 2;
	
	public static HashMap<Integer, String> configurationErrors = new HashMap<Integer, String>();
	public static HashMap<Integer, PhaseAssociation> phaseAssociations = new HashMap<Integer, PhaseAssociation>();
	public static HashMap<Integer, String> rackNames = new HashMap<Integer, String>();
	static{
		setConfigurationErrors();
		setPhaseAssociations();
		setRackNames();
	}

	public static void setRackNames(){
		rackNames.put(LEFT_RACK_TYPE, "Left");
		rackNames.put(CENTER_RACK_TYPE, "Center");
		rackNames.put(RIGHT_RACK_TYPE, "Right");
	}
	
	public static void setPhaseAssociations(){
		phaseAssociations.put(1, new PhaseAssociation(1, false, "A"));
		phaseAssociations.put(2, new PhaseAssociation(1, true, "A-B"));
		phaseAssociations.put(3, new PhaseAssociation(2, false, "B"));
		phaseAssociations.put(4, new PhaseAssociation(2, true, "B-C"));
		phaseAssociations.put(5, new PhaseAssociation(3, false, "C"));
		phaseAssociations.put(6, new PhaseAssociation(3, true, "C-A"));
	}
	
	public static void setConfigurationErrors(){
		configurationErrors.put(CONFIGURATION_ERROR_SERVER_FACEPLATE, StringConstants.getString("VALIDATION_CONFIG_ERROR_MSG"));
		configurationErrors.put(CONFIGURATION_ERROR_P3_ID, StringConstants.getString("VALIDATION_CONFIG_ERROR_MSG"));
		configurationErrors.put(CONFIGURATION_ERROR_P3_FEED, StringConstants.getString("VALIDATION_CONFIG_ERROR_MSG"));
		configurationErrors.put(CONFIGURATION_ERROR_MISSING_PHASE_ASSOCIATION, StringConstants.getString("VALIDATION_MISSING_PHASE_ERROR_MSG"));
		configurationErrors.put(CONFIGURATION_ERROR_P3_UNIQUEID, StringConstants.getString("VALIDATION_CONFIG_ERROR_MSG"));
		configurationErrors.put(CONFIGURATION_ERROR_P3, StringConstants.getString("VALIDATION_CONFIG_ERROR_MSG"));
		configurationErrors.put(CONFIGURATION_ERROR_DISCONNECTED_CONTROLLER, StringConstants.getString("VALIDATION_NO_CONTROLLER"));
		configurationErrors.put(CONFIGURATION_ERROR_TRANSFER_CONFIGURATION, StringConstants.getString("VALIDATION_CONFIG_ERROR_MSG"));
		configurationErrors.put(CONFIGURATION_ERROR_RECEIVE_CONFIGURATION, StringConstants.getString("VALIDATION_UPLOAD_FAIL_MSG"));
		configurationErrors.put(CONFIGURATION_ERROR_UNSATISFIEDLINK_DLL, StringConstants.getString("VALIDATION_UNSATISFIEDLINK_DLL"));
		configurationErrors.put(CONFIGURATION_ERROR_RESET_P3, StringConstants.getString("VALIDATION_ERROR_RESET_P3_MSG"));
		configurationErrors.put(CONFIGURATION_ERROR_ACTIVATE_P3, StringConstants.getString("VALIDATION_ERROR_ACTIVATE_P3_MSG"));
		configurationErrors.put(CONFIGURATION_ERROR_ACTIVATE_OUTLET_ORDER, StringConstants.getString("VALIDATION_ERROR_ACTIVATE_OUTLET_ORDER_MSG"));
		configurationErrors.put(CONFIGURATION_ERROR_REALTIME_DATA, StringConstants.getString("VALIDATION_ERROR_REALTIME_DATA_MSG"));
		configurationErrors.put(CONFIGURATION_ERROR_OVERSIZED_P3, StringConstants.getString("VALIDATION_ERROR_OVERSIZE_P3_MSG"));
		configurationErrors.put(CONFIGURATION_ERROR_BUSY_CONTROLLER, StringConstants.getString("VALIDATION_ERROR_BUSY_CONTROLLER"));
		configurationErrors.put(CONFIGURATION_ERROR_CLEAR_CONFIGURATION, StringConstants.getString("VALIDATION_ERROR_CLEAR_CONFIGURATION"));
		configurationErrors.put(CONFIGURATION_ERROR_SERVER_LOCATION, StringConstants.getString("VALIDATION_ERROR_SERVER_LOCATION"));
		configurationErrors.put(CONFIGURATION_ERROR_MIXED_PHASE_ASSOCIATION, StringConstants.getString("VALIDATION_MIXED_PHASE_ERROR_MSG"));
		configurationErrors.put(CONFIGURATION_WARNING_UNKNOWN_ID_P3S, StringConstants.getString("VALIDATION_WARNING_UNKNOWN_ID_P3S"));

	}
	
	public static PhaseAssociation getPhaseAssociation(int phase, boolean isDelta){
		for(PhaseAssociation pa:phaseAssociations.values()){
			if(pa.getPhase()==phase && pa.getDelta()==isDelta)
				return pa;
		}
		return null;
	}
	
	public enum ConfigurationTypes {
		PLUGMETER,
		SRV1U,
		SRV2U,
		SRV3U,
		SRV4U,
		SRV5U,
		SRVCUSTOM,
		SRV1U1P,
		SRV1U2P
	}
	
	public enum ValidationLevels{
		VALIDNESS,
		WARNING,
		ERROR
	}
}
