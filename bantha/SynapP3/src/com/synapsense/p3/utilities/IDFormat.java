package com.synapsense.p3.utilities;

import java.util.Formatter;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public class IDFormat {

	private static Map<Integer, String> manufCode;
	private static Map<String, Integer> manufCodeRev;
	
	private static void insertManuf(int code, String token) {
		manufCode.put(code, token);
		manufCodeRev.put(token, code);
	}
	
	static {
		manufCode = new TreeMap<Integer, String>();
		manufCodeRev = new TreeMap<String, Integer>();
		insertManuf(0, "AM");
		insertManuf(1, "PR");
		insertManuf(2, "SY");
	}
	
	public static long hrToCode(String hr) throws InvalidChecksum {
		long code = 0;
		try{
			int manuf = manufCodeRev.get(hr.substring(0, 2));
			int yr = Integer.parseInt(hr.substring(2,4));
			yr = yr - 10;
			int wk = Integer.parseInt(hr.substring(4,6));
			int serial = Integer.parseInt(hr.substring(6, 11), 16);
			int cksum = Integer.parseInt(hr.substring(11,13), 16);
			
			code = (manuf & 0x3) << 30;
			code |= (yr & 0xf) << 26;
			code |= (wk & 0x3f) << 20;
			code |= serial & 0xfffff;

			byte crc = CRC8.crc8((byte)0, (byte)(((code >> 24) & 0xff)));
			crc = CRC8.crc8(crc, (byte)( (code >> 16) & 0xff));
			crc = CRC8.crc8(crc, (byte)( (code >> 8) & 0xff));
			crc = CRC8.crc8(crc, (byte)( (code) & 0xff));
			
			if (crc != (byte)cksum) {
				throw new InvalidChecksum();
			}
			
		}catch(Exception e){
			throw new InvalidChecksum();
		}
		
		return code & 0xFFFFFFFFL;
	}
	
	public static String codeToHr(long code) {
		code = code & 0xffffffffL;
		int manuf = (int)((code >> 30) & 0x3);
		int yr = (int)((code >> 26) & 0xf);
		yr += 10;
		int wk = (int)((code >> 20) & 0x3f);
		int serial = (int)(code & 0xfffff);
		byte crc = CRC8.crc8((byte)0, (byte)(((code >> 24) & 0xff)));
		crc = CRC8.crc8(crc, (byte)( (code >> 16) & 0xff));
		crc = CRC8.crc8(crc, (byte)( (code >> 8) & 0xff));
		crc = CRC8.crc8(crc, (byte)( (code) & 0xff));
		
		StringBuilder sb = new StringBuilder();
		Formatter f = new Formatter(sb, Locale.US); /* Fixed locale */
		f.format("%s%02d%02d%05X%02X", manufCode.get(manuf), yr, wk, serial, crc);
		return sb.toString();
		
	}

};