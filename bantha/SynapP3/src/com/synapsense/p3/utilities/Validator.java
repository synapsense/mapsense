package com.synapsense.p3.utilities;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.synapsense.p3.components.ServerPanel;
import com.synapsense.p3.model.*;
import com.synapsense.p3.ui.*;

import javax.swing.ButtonGroup;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXTable;

public class Validator {
	private static Logger logger = Logger.getLogger(Validator.class.getName());
	
	static {
	}
	
	public static boolean checkServerLocation(int row, int height, Rack rack, Server server){
		int location = rack.getUheight() - row;
		System.out.println("location:" + location);
		System.out.println("row:" + row);
		
		if(((rack.getUheight()+ 1)< (location+height)) || (location<1) ){
			logger.warn("Server cannot move to " + row + " in " + rack.toString() + " because rack size is smaller than sever's ulocation");
			System.out.println("Server cannot move to " + row + " in " + rack.toString() + " because rack size is smaller than sever's ulocation");
			
			return false;
		}
		
		int y = rack.getUheight() - location - height + 1;
		Rectangle serverArea = new Rectangle(0,y,10,height);
		
		for(Server s:rack.getServers()){
			
			if(server==null || (server!=null && !server.equals(s))){
				int current_location = s.getLocation();
				int current_height = s.getUheight();
				int current_y = rack.getUheight() - current_location - current_height + 1;
				
				Rectangle currentArea = new Rectangle(0, current_y, 10, current_height);
				
				if(currentArea.intersects(serverArea)){
					logger.warn("cannot place a server " + row + " in " + rack.toString() + " because of insuffucuent U space");
					System.out.println("cannot place a server " + row + " in " + rack.toString() + " because of insuffucuent U space");

					return false;
				}
			}
		}
		return true;
	}
	
	public static boolean checkMaxPlugmeter(Server server){
		if(server.getJawas().size()<8){
			return true;
		}
		else{
			logger.warn(server.toString() +  "has more than maximum number of plugmeter");
			return false;
		}
	}
	
	public static int checkServersLocation(HashMap<Integer, ServerPanel>servers, int baseRow, Rack newRack){
		int validKey = 0;
		int rackCheck = 0;
		
		ArrayList<Server> serverGroup = new ArrayList<Server>();
		
		for(Map.Entry<Integer, ServerPanel> me : servers.entrySet()){
			serverGroup.add(me.getValue().getServer());
		}
		
		for(Map.Entry<Integer, ServerPanel> me : servers.entrySet()){
			int offset = me.getKey();
			ServerPanel sor = me.getValue();
			int rackType = sor.getServer().getRack().getType();
			
			if(rackCheck!=0 && rackCheck!=rackType){
				logger.warn("cannot place server(s) because only servers in a same rack can be moved");
				System.out.println("cannot place server(s) because only servers in a same rack can be moved");
				validKey =1;
				
				return validKey;
			}else{
				rackCheck = rackType;
			}
				
			int newRow = baseRow + offset;
			
			if(!checkServerLocation(newRow, sor.getUHeight(), newRack, sor.getServer(), serverGroup)){
				logger.warn("cannot place server(s) because one of servers has insufficient u space");
				System.out.println("cannot place server(s) because one of servers has insufficient u space");
				validKey = 2;
				
				return validKey;
			}
		}
		return validKey;
	}
	
	public static boolean checkServerLocation(int row, int height, Rack rack, Server server, ArrayList<Server> serverGroup){
		int location = rack.getUheight() - row;
		
		if(((rack.getUheight()+1)< (location+height)) || (location<1) ){
			logger.warn("Server cannot move to " + row + " in " + rack.toString() + " because rack size is smaller than sever's ulocation");
			System.out.println("Server cannot move to " + row + " in " + rack.toString() + " because rack size is smaller than sever's ulocation");
			
			return false;
		}
		
		int y = rack.getUheight() - location - height + 1;
		Rectangle serverArea = new Rectangle(0,y,10,height);
		
		ArrayList<Server> servers = (ArrayList<Server>) rack.getServers().clone();
		for(Server s:serverGroup){
			servers.remove(s);
		}
		
		for(Server s:servers){
			
			if(server==null || (server!=null && !server.equals(s))){
				int current_location = s.getLocation();
				int current_height = s.getUheight();
				int current_y = rack.getUheight() - current_location - current_height + 1;
				
				Rectangle currentArea = new Rectangle(0, current_y, 10, current_height);
				
				if(currentArea.intersects(serverArea)){
					logger.warn("cannot place a server " + row + " in " + rack.toString() + " because of insuffucuent U space");
					System.out.println("cannot place a server " + row + " in " + rack.toString() + " because of insuffucuent U space");

					return false;
				}
			}
		}
		return true;
	}
	public static boolean checkServerLocation(int _uheight, int _location, Rack _rack){
		if(_location+_uheight > (_rack.getUheight()+1)){
			logger.error("sever location:" + _location + " uheight: " + _uheight + " rack size: " + _rack.getUheight());
			return false;
			
		}
		
		for(Server server:_rack.getServers()){
			int location = server.getLocation();
			if(location==_location){
				return false;
			}
		}
		
		return true;
	}
	/**
	 * Check all plugmeters to verify missing phase association, 
	 * invalid plugmeter, invalid server, duplicate plugmeter id, mixed phase in a rack
	 * @param config
	 * @return error message
	 */
	
	public static ConfigurationError checkConfiguration(Configuration config){
		
		HashMap<String, Integer> idMap = new HashMap<String, Integer>();
		boolean isMissingPhaseAssociated = true;		// missing phase assocation for a RPDU
		boolean isValidP3s = true;
		boolean isValidServerFacePlate = true;
		boolean isUniqueP3Ids = true;
		boolean isMixedPhase = true;
		boolean isValidServerLocation = true;
		String invalidServerLocationInfo = "";
		
		int totalP3 = config.getJawas().size();
		ArrayList<Jawa> unknownIdJawas = new ArrayList<Jawa>();
		
		if(totalP3>Variables.P3_MAX_TOTAL_NUMBER){
			logger.warn("cannot upload more than 252");
			
			return new ConfigurationError(config, Variables.CONFIGURATION_ERROR_OVERSIZED_P3, Variables.configurationErrors.get(Variables.CONFIGURATION_ERROR_OVERSIZED_P3)) ;
		}
		
		String phaseAssociationErrMsg = Variables.configurationErrors.get(Variables.CONFIGURATION_ERROR_MISSING_PHASE_ASSOCIATION);
		
		for(Rack rack:config.getRacks()){
			// check missing RPUD phase association for valid mac id smart plug
			ArrayList<Integer> missingRpdus = checkRpduPhaseAssociation(rack);
			if(missingRpdus.size()>0){
				isMissingPhaseAssociated = false;
				phaseAssociationErrMsg+= "\nRack: " + Variables.rackNames.get(rack.getType()) +  "    RPDU: " + missingRpdus.toString().replace("[", "").replace("]", "");  
			}
			
			if(!checkMixedRpduPhaseAssociation(rack)){
				isMixedPhase = false;
			}
			
			// to check server location
			HashMap<Integer, Integer> location_servers_map = new HashMap<Integer, Integer>();
			
			for(Server server:rack.getServers()){
				if(server.getInvalidities().size()>0)
					isValidServerFacePlate = false;
				
				int ulocation = server.getLocation();
				if(location_servers_map.containsKey(ulocation)){
					int value = location_servers_map.get(ulocation)+1;
					if(value>1){
						invalidServerLocationInfo = "Wrong Server location [ulocation :" + ulocation + " Rack: " + rack.getType() + "] is duplicated";
						isValidServerLocation = false;
					}
					location_servers_map.put(ulocation, value); 
				}else{
					location_servers_map.put(ulocation, 1);
				}
				// add jawa list which has unknown id
				//System.out.println("unknown id:" +server.getJawasByIdValidation(false).size() );
				unknownIdJawas.addAll(server.getJawasByIdValidation(false));
				
				for(Jawa jawa:server.getJawasByIdValidation(true)){
					if(jawa.getInvalidities().size()>0 && jawa.getInvalidities().contains(Variables.CONFIGURATION_ERROR_P3_FEED)){
						isValidP3s = false;
					}
					
					String jid = jawa.getId();
					if(idMap.containsKey(jid)){
						int cnt =idMap.get(jid) + 1;
						idMap.put(jid, cnt);
					}else{
						idMap.put(jid, 1);
					}
				}
			}
		}
		
		for(int i:idMap.values()){
			if(i>1)
				isUniqueP3Ids = false;
		}
		
		if(!isMissingPhaseAssociated){
			String errMsg = phaseAssociationErrMsg;
			String description = "";
			if(phaseAssociationErrMsg.endsWith(";")){
				description = errMsg.substring(0, errMsg.length()-1) + ".";
			}else
				description = errMsg;
			
			
			return new ConfigurationError(config, Variables.CONFIGURATION_ERROR_MISSING_PHASE_ASSOCIATION, description) ;

		}
		
		if(!isValidP3s){
			logger.warn("cannot upload because a smartplug has error ");
			return new ConfigurationError(config, Variables.CONFIGURATION_ERROR_P3, Variables.configurationErrors.get(Variables.CONFIGURATION_ERROR_P3)) ;
		}
		if(!isValidServerFacePlate){
			logger.warn("cannot upload because a missing server faceplate info");
			return new ConfigurationError(config, Variables.CONFIGURATION_ERROR_SERVER_FACEPLATE, Variables.configurationErrors.get(Variables.CONFIGURATION_ERROR_SERVER_FACEPLATE)) ;
		}
		if(!isUniqueP3Ids){
			logger.warn("cannot upload because duplicated smartplug id");
			return new ConfigurationError(config, Variables.CONFIGURATION_ERROR_P3_UNIQUEID, Variables.configurationErrors.get(Variables.CONFIGURATION_ERROR_P3_UNIQUEID)) ;
		}
		
		if(!isMixedPhase){
			logger.warn("cannot upload because mixed phase association in a rack");
			return new ConfigurationError(config, Variables.CONFIGURATION_ERROR_MIXED_PHASE_ASSOCIATION, Variables.configurationErrors.get(Variables.CONFIGURATION_ERROR_MIXED_PHASE_ASSOCIATION)) ;
		}
		
		if(!isValidServerLocation){
			logger.warn(invalidServerLocationInfo);
			return new ConfigurationError(config, Variables.CONFIGURATION_ERROR_SERVER_LOCATION, Variables.configurationErrors.get(Variables.CONFIGURATION_ERROR_SERVER_LOCATION)) ;
		}
		
		//System.out.println("unknownIdJawas:" +unknownIdJawas.size() );
		if(unknownIdJawas.size()>0){
			logger.warn("warning: has plugmeter which has UNKNOWN id");
			return new ConfigurationError(config, Variables.CONFIGURATION_WARNING_UNKNOWN_ID_P3S, StringConstants.getString("VALIDATION_WARNING_UNKNOWN_ID_P3S")) ;
		}
		return new ConfigurationError(config, Variables.CONFIGURATION_NO_ERROR, "") ;
	}
	
	public static boolean checkServers(Configuration config){
		for(Rack rack:config.getRacks()){

			HashMap<Integer, Integer> location_servers_map = new HashMap<Integer, Integer>();
			
			for(Server server:rack.getServers()){
				int ulocation = server.getLocation();
				if(location_servers_map.containsKey(ulocation)){
					int value = location_servers_map.get(ulocation)+1;
					if(value>1){
						logger.error("Wrong Server location [ulocation :" + ulocation + " Rack: " + rack.getType() + "] is duplicated");
						return false;
					}
					location_servers_map.put(ulocation, value); 
				}else{
					location_servers_map.put(ulocation, 1);
				}
			}
			
		}
		
		return true;
		
	}
	
	public static boolean checkPhaseAssociation(Configuration config){
		for(Jawa jawa:config.getJawas()){
			if(jawa.getPhase()>0)
				return true;
		}
		
		return false;
	}
	
	public static boolean checkJawaId(Jawa jawa){
		if(jawa.getId()==null || jawa.getId()=="" || jawa.getId().equals(StringConstants.getString("PLUGMETER_DEFAULT_ID"))){
			logger.warn("plugmeter id is invalid because of null, empty or unknown value");
			return false;
		}
		try{
			String hrId = jawa.getId();
			long code = IDFormat.hrToCode(hrId);
			
		}catch(NumberFormatException nfe){
			logger.warn("plugmeter id is invalid because of NumberFormatException");
			return false;
		} catch (InvalidChecksum e) {
			logger.warn("plugmeter id is invalid because of invalid checksum");
			return false;
		}
		
		return true;
	}
	
	public static boolean checkUniqueJawaId( String value, Jawa jawa,Configuration config){
		System.out.println("value:" + value);
		if(value.toUpperCase().equals("UNKNOWN"))
			return true;
		for(Jawa j:config.getJawas()){
			if(!jawa.equals(j) && j.getId().equals(value)){
				return false;
			}
		}

		return true;
	}
	
	public static boolean checkJawaFeed(Jawa jawa){
		if(jawa.getFeed()<1 || jawa.getFeed()>8)
			return false;
		return true;
	}
	
	public static boolean checkServer(Server server){
		if(server.getJawas().size()==0){
			if(server.getPower()<Variables.FACEPLATE_MIN_POWER || server.getPower()>Variables.FACEPLATE_MAX_POWER)
				return false;
		}
			
		return true;
	}
	
	public static boolean checkFaceplatePower(String power){
		if(power==null || power.equals("")){
			return false;
		}else{
			try{
				int powerNumber = Integer.parseInt(power);
			}catch(NumberFormatException e){
				return false;
			}
		}
		
		return true;
	}
	
	
	public static boolean checkResizableRack(Rack rack, int newHeight){
		
		
		for(Server server:rack.getServers()){
			if((server.getLocation()+ server.getUheight()) >(newHeight+1)){
				logger.warn("a server's location in the rack is bigger than rack's height");
				return false;
			}
		}
		return true;
	}
	
	public static boolean checkRackSizeLimitation(int newHeight){
		if(newHeight<Variables.RACK_MIN_HEIGHT)
			return false;
		
		if(newHeight>Variables.RACK_MAX_HEIGHT)
			return false;
		return true;
	}
	
	public static boolean checkScannerCodeValue(String value){
		if(value.length()==8){
			if(value.matches("^[\\x20-\\x7E]+$")){
				return true;
			}else
				return false;
		}else
			return false;
	}
	
	public static boolean checkFaceplatePowerLimit(String value){
		try{
			double doubleValue = Double.parseDouble(value);
			if(doubleValue>=Variables.FACEPLATE_MIN_POWER && doubleValue<=Variables.FACEPLATE_MAX_POWER)
				return true;
			else
				return false;
			
		}catch(NumberFormatException nfe){
			return false;
		}
	}
	
	public static boolean checkFaceplatePowerLimit(double value){
			if(value>=Variables.FACEPLATE_MIN_POWER && value<=Variables.FACEPLATE_MAX_POWER)
				return true;
			else
				return false;
	}
	
	public static boolean checkRpduPhaseAssocation(Jawa jawa, int phase){
		if(phase==0)
			return true;
		Rack rack = jawa.getServer().getRack();
		PhaseAssociation pa = Variables.phaseAssociations.get(phase);
		
		HashMap<Integer, Jawa> rpduPhaseMap = (HashMap<Integer, Jawa>) rack.getRpduPhaseMap().clone();
		rpduPhaseMap.remove(jawa.getFeed());
		
		if(rpduPhaseMap.size()<1){
			return true;
		}else{
			for(Jawa j:rpduPhaseMap.values()){
				int comparedPhase = j.getPhase();
				PhaseAssociation comparedPa = Variables.phaseAssociations.get(comparedPhase);
				if(comparedPa.getDelta()!=pa.getDelta()){
					return false;
				}
			}
			return true;
		}
	}
	
	public static boolean checkDuplicateRpduPhaseAssocation(Jawa jawa, int rpdu ){
		Rack rack = jawa.getServer().getRack();
		HashMap<Integer, Jawa> rpduPhaseMap = (HashMap<Integer, Jawa>) rack.getRpduPhaseMap();
		
		for(int r:rpduPhaseMap.keySet()){
			if(r==rpdu && !rpduPhaseMap.get(r).equals(jawa))
				return false;
			
		}
		
		return true;
		
	}
	
	/**
	 * check rpdu phase association for a rack
	 * only checking plugmeter which has valid mac id.
	 * @param rack
	 * @return
	 */
	public static ArrayList<Integer> checkRpduPhaseAssociation(Rack rack){
		HashMap<Integer, Jawa> rpduPhaseMap = rack.getRpduPhaseMap();
		ArrayList<Integer> rpdus = new ArrayList<Integer>();
		
		for(Server s:rack.getServers()){
			for(Jawa j:s.getJawasByIdValidation(true))
				if(!rpduPhaseMap.containsKey(j.getFeed()) && !rpdus.contains(j.getFeed()))
					rpdus.add(j.getFeed());
		}
		
		return rpdus;
	}
	
	/**
	 * Check all jawas phase association in a same rack
	 * and update all errors
	 *
	 * @param jawa
	 * @return
	 */
	public static void checkRpduPhaseAssociation(Jawa jawa){
		Rack rack = jawa.getServer().getRack();
		
		// arraylist of jawa associated with delta
		ArrayList<Jawa> deltaJawas = new ArrayList<Jawa>();
		// arrayList of jawa associated with wye 
		ArrayList<Jawa> wyeJawas = new ArrayList<Jawa>();
		
		
		for(Server s:rack.getServers()){
			for(Jawa j:s.getJawas()){
				int phase = j.getPhase();
				// if a jawa associated with phase, get PhaseAssociation object to get WYE and delta
				if(phase>0){
					PhaseAssociation pa = Variables.phaseAssociations.get(phase);
					boolean isDelta = pa.getDelta();
					if(isDelta){
						deltaJawas.add(j);
					}else{
						wyeJawas.add(j);
					}
				}
			}
		}
		
		// clear all rpdu phase association error first before adding new errors
		for(Server s:rack.getServers()){
			for(Jawa j:s.getJawas())
				j.removeInvalidity(Variables.CONFIGURATION_ERROR_MIXED_PHASE_ASSOCIATION);
		}
		
		// if the number of delta jawa is 0 or one of wye jawas is 0, that means rack has homogeneous phase association
		if(deltaJawas.size()==0 || wyeJawas.size()==0){
			return;
		}
		
		// determine which phase associated jawa list is more than the another one
		if(deltaJawas.size()>wyeJawas.size()){
			// if the number of jawas associated with delta is larger than the other one
			// major phase association in this rack is delta. 
			// so all wye phase association jawas should have a error about this.
			
			for(Jawa j:wyeJawas){
				j.addInvalidity(Variables.CONFIGURATION_ERROR_MIXED_PHASE_ASSOCIATION);
			}
		}else{
			
			for(Jawa j:deltaJawas){
				j.addInvalidity(Variables.CONFIGURATION_ERROR_MIXED_PHASE_ASSOCIATION);
			}
		}
		
		
	}
	
	/**
	 * Check mixed phase association in a same rack
	 *
	 * @param jawa
	 * @return
	 */
	public static boolean checkMixedRpduPhaseAssociation(Rack rack){
		
		// arraylist of jawa associated with delta
		ArrayList<Jawa> deltaJawas = new ArrayList<Jawa>();
		// arrayList of jawa associated with wye 
		ArrayList<Jawa> wyeJawas = new ArrayList<Jawa>();
		
		
		for(Server s:rack.getServers()){
			for(Jawa j:s.getJawas()){
				int phase = j.getPhase();
				// if a jawa associated with phase, get PhaseAssociation object to get WYE and delta
				if(phase>0){
					PhaseAssociation pa = Variables.phaseAssociations.get(phase);
					boolean isDelta = pa.getDelta();
					if(isDelta){
						deltaJawas.add(j);
					}else{
						wyeJawas.add(j);
					}
				}
			}
		}
		
		// if the number of delta jawa is 0 or one of wye jawas is 0, that means rack has homogeneous phase association
		if(deltaJawas.size()==0 || wyeJawas.size()==0){
			return true;
		}
		
		return false;
		
	}
	
	public static void updateUniquePlugmeterIdValidation(Configuration config){
		HashMap<String, ArrayList<Jawa>> map = new HashMap<String, ArrayList<Jawa>>();
		
		for(Jawa jawa:config.getJawas()){
			if(jawa.getInvalidities().contains(Variables.CONFIGURATION_ERROR_P3_UNIQUEID)){
				if(map.keySet().contains(jawa.getId())){
					map.get(jawa.getId()).add(jawa);
					
				}else{
					ArrayList<Jawa> list = new ArrayList<Jawa>();
					list.add(jawa);
					map.put(jawa.getId(), list);
				}
			}
		}
		
		for(String jid:map.keySet()){
			if(map.get(jid).size()==1){
				Jawa jawa = map.get(jid).get(0);
				jawa.removeInvalidity(Variables.CONFIGURATION_ERROR_P3_UNIQUEID);
			}
				
		}
	}
	
	public static void checkUniquePlugmeterIdValidation(Configuration config){
		HashMap<String, ArrayList<Jawa>> map = new HashMap<String, ArrayList<Jawa>>();
		boolean isPhaseAssigned = false;
		
		for(Jawa jawa:config.getJawas()){
			String jid = jawa.getId();
			if(!jid.equals(StringConstants.getString("P3_DEFAULT_NAME"))){
				if(map.containsKey(jid)){
					ArrayList<Jawa> jlist = map.get(jid);
					jlist.add(jawa);
				}else{
					ArrayList<Jawa> jlist = new ArrayList<Jawa>();
					jlist.add(jawa);
					map.put(jid, jlist);
					
				}
				
			}
			
			// check multiple phase association
			//int phase = jawa.getPhase();
			
			//if(phase>0 && isPhaseAssigned)
			//	jawa.setPhase(0);
			
			//if(phase>0)
			//	isPhaseAssigned = true;
			
		}
		
		for(String id:map.keySet()){
			if(map.get(id).size()>1){
				for(Jawa jawa:map.get(id)){
					jawa.addInvalidity(Variables.CONFIGURATION_ERROR_P3_UNIQUEID);
				}
			}
		}
	}
	
	/**
	 * Check all jawas phase association in whole configuration
	 * and update all errors
	 *
	 * @param rack
	 * @return 
	 */
	public static void checkAllRpduPhaseAssociation(Configuration configuration){
		
		for(Rack rack:configuration.getRacks()){
			checkRpduPhaseAssociationInRack(rack);
		}
	}

	/**
	 * Check all jawas phase association in whole configuration
	 * and update all errors
	 *
	 * @param rack
	 * @return 
	 */
	public static void checkRpduPhaseAssociationInRack(Rack rack){
		
		// arraylist of jawa associated with delta
		ArrayList<Jawa> deltaJawas = new ArrayList<Jawa>();
		// arrayList of jawa associated with wye 
		ArrayList<Jawa> wyeJawas = new ArrayList<Jawa>();
		
		
		for(Server s:rack.getServers()){
			for(Jawa j:s.getJawas()){
				int phase = j.getPhase();
				// if a jawa associated with phase, get PhaseAssociation object to get WYE and delta
				if(phase>0){
					PhaseAssociation pa = Variables.phaseAssociations.get(phase);
					boolean isDelta = pa.getDelta();
					if(isDelta){
						deltaJawas.add(j);
					}else{
						wyeJawas.add(j);
					}
				}
			}
		}
		
		// if the number of delta jawa is 0 or one of wye jawas is 0, that means rack has homogeneous phase association
		if(deltaJawas.size()==0 || wyeJawas.size()==0){
			return;
		}
		
		// determine which phase associated jawa list is more than the another one
		if(deltaJawas.size()>wyeJawas.size()){
			// if the number of jawas associated with delta is larger than the other one
			// major phase association in this rack is delta. 
			// so all wye phase association jawas should have a error about this.
			
			for(Jawa j:wyeJawas){
				j.addInvalidity(Variables.CONFIGURATION_ERROR_MIXED_PHASE_ASSOCIATION);
			}
		}else{
			
			for(Jawa j:deltaJawas){
				j.addInvalidity(Variables.CONFIGURATION_ERROR_MIXED_PHASE_ASSOCIATION);
			}
		}
	}
}
