package com.synapsense.p3.utilities;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.Border;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.synapsense.p3.exceptions.JawaCreationFailedException;
import com.synapsense.p3.exceptions.ServerCreationFailedException;
import com.synapsense.p3.model.*;
import com.synapsense.p3.scinter.BusySandCrawlerException;
import com.synapsense.p3.scinter.JawaData;
import com.synapsense.p3.scinter.NoSandCrawlerException;
import com.synapsense.p3.scinter.SCAllConfig;
import com.synapsense.p3.scinter.SCConfigMessage;
import com.synapsense.p3.scinter.SCConfigService;
import com.synapsense.p3.scinter.SCJawaModel;
import com.synapsense.p3.scinter.SCRackModel;
import com.synapsense.p3.scinter.SCServerModel;
import com.synapsense.p3.scinter.SandCrawler;
import com.synapsense.p3.ui.CommandRunnerDialog;
import com.synapsense.p3.ui.MainScreen;
import com.synapsense.p3.ui.RabbitLightRunnerDialog;

public class ConfigurationIO {
	
	private static Logger logger = Logger.getLogger(ConfigurationIO.class.getName());
	private SandCrawler sandCrawler;
	private Configuration synchConfiguration = null;
	
	public ConfigurationIO(){
		
	}

	/*
	 * Serialize configuration object to xml
	 */
	public Document serializeXml(Configuration configuration, int option){
		DocumentBuilderFactory factory = null;
	    DocumentBuilder builder = null;
	    Document document = null;
	    
	    try {
	    	factory = DocumentBuilderFactory.newInstance();
		    builder = factory.newDocumentBuilder();
		    document = builder.newDocument();
		    Node root = document.createElement("p3configuration");
		    document.appendChild(root);
		    
		    
		    for(Rack rack: configuration.getRacks()){
		    	int type = rack.getType();
		    	int uheight = rack.getUheight();
		    	
		    	Node rackNode = document.createElement("rack");
		    	((Element)rackNode).setAttribute("type", Integer.toString(type));
		    	((Element)rackNode).setAttribute("uheight", Integer.toString(uheight));
		    	
		    	root.appendChild(rackNode);
		    	
		    	for(Server server: rack.getServers()){
		    		int server_uheight = server.getUheight();
		    		int server_location = server.getLocation();
		    		double server_power = server.getPower();
		    		
		    		Node serverNode = document.createElement("server");
		    		((Element)serverNode).setAttribute("uheight", Integer.toString(server_uheight));
		    		((Element)serverNode).setAttribute("location", Integer.toString(server_location));
		    		((Element)serverNode).setAttribute("power", Double.toString(server_power));
		    		
		    		rackNode.appendChild(serverNode);
		    		
		    		for(Jawa jawa: server.getJawas()){
		    			// for two template format including IDs or without IDs
		    			String jid = StringConstants.getString("PLUGMETER_DEFAULT_ID");
		    			int phase = 0;
		    			if(option==0){
		    				jid = jawa.getId();
		    				phase = jawa.getPhase();
			    		}
		    			
		    			int feed = jawa.getFeed();
		    			
		    			Node jawaNode = document.createElement("jawa");
		    			((Element)jawaNode).setAttribute("id", jid);
			    		((Element)jawaNode).setAttribute("feed", Integer.toString(feed));
			    		((Element)jawaNode).setAttribute("phase", Integer.toString(phase));
			    		
			    		serverNode.appendChild(jawaNode);
		    		}
		    	}
		    }
		} catch (ParserConfigurationException e) {
			logger.error("Fail to serialize configuration to xml");
			logger.debug(LogManager.getStackTrace(e));
		}
	    
		return document;
	}
	
	public Configuration deserializeXml(Document doc) throws Exception{
		Configuration config = null;
		try{
			config = new Configuration();
			Element element = doc.getDocumentElement();
			NodeList racklist = element.getElementsByTagName("rack");
			
			if(racklist!=null && racklist.getLength()>0){
				for(int i=0; i<racklist.getLength();i++){
					Element rackElement = (Element)racklist.item(i);
					String rackType = rackElement.getAttribute("type");
					String rackUheight = rackElement.getAttribute("uheight");
					
					if(Integer.parseInt(rackType) <Variables.LEFT_RACK_TYPE || Integer.parseInt(rackType)>Variables.RIGHT_RACK_TYPE){
						throw new Exception("invalid rack type " + rackType);
					}
					Rack rack = new Rack(Integer.parseInt(rackType));
					rack.setUheight(Integer.parseInt(rackUheight));
					
					NodeList serverList = rackElement.getElementsByTagName("server");
					if(serverList!=null && serverList.getLength()>0){
						for(int j=0;j<serverList.getLength();j++){
							Element serverElement = (Element)serverList.item(j);
							String serverLocation = serverElement.getAttribute("location");
							String serverUheight = serverElement.getAttribute("uheight");
							String serverPower = serverElement.getAttribute("power");

							Server server = new Server(Integer.parseInt(serverLocation), Integer.parseInt(serverUheight), Double.parseDouble(serverPower), rack);
							rack.addServer(server);
							System.out.println(serverLocation);
							System.out.println(serverUheight);
							
							NodeList jawaList = serverElement.getElementsByTagName("jawa");
							
							if(jawaList!=null && jawaList.getLength()>0){
								for(int k=0;k<jawaList.getLength();k++){
									Element jawaElement = (Element)jawaList.item(k);
									String jawaFeed = jawaElement.getAttribute("feed");
									String jawaId = jawaElement.getAttribute("id");
									String jawaPhase = jawaElement.getAttribute("phase");
									
									Jawa jawa = new Jawa(jawaId, Integer.parseInt(jawaFeed), Integer.parseInt(jawaPhase), server);
									server.addJawa(jawa);
									
									if(Integer.parseInt(jawaPhase)>0){
										rack.setRpduPhaseAssociation(Integer.parseInt(jawaFeed), jawa);
									}
										
								}
							}
						}
					}
					
					config.setRack(Integer.parseInt(rackType), rack);
				}
			}
			
			// check unique plugmeter id
			Validator.checkUniquePlugmeterIdValidation(config);
			// check mixed phase asssociation
			Validator.checkAllRpduPhaseAssociation(config);
			
		}catch(ServerCreationFailedException e){
			logger.error("Fail to deserialize xml");
			throw e;
		}catch(JawaCreationFailedException e){
			logger.error("Fail to deserialize xml");
			throw e;
		}
		return config;
	}
	
	/**
	 * @param filepath
	 * @param configuration
	 * @param option : 0-P3 SmartPlug Configuration, 1- P3 SmartPlug Configuration Template
	 * @throws Exception
	 */
	public void writeTemplate(String filepath, Configuration configuration, int option) throws Exception{
		Document document = serializeXml(configuration, option);
		TransformerFactory tf = TransformerFactory.newInstance();
		File file = null;
		OutputStream os = null;
		try {
			file = new File(filepath);
			os = new FileOutputStream(file);
			if(file.exists()){
				Transformer t = tf.newTransformer();
				t.setOutputProperty(OutputKeys.INDENT, "yes");
				t.transform(new DOMSource(document.getDocumentElement()),new StreamResult(os));
				os.close();
				synchConfiguration = new Configuration(configuration);
			}
		} catch (TransformerConfigurationException e) {
			logger.error("Fail to write configuration file template ");
			logger.debug(LogManager.getStackTrace(e));
			os.close();
			file.delete();
			throw e;
		} catch (FileNotFoundException e) {
			file.delete();
			logger.error("Fail to write configuration file template ");
			logger.debug(LogManager.getStackTrace(e));
			os.close();
			throw e;
		} catch (TransformerException e) {
			os.close();
			file.delete();
			logger.error("Fail to write configuration file template ");
			logger.debug(LogManager.getStackTrace(e));
			throw e;
		} catch (IOException e) {
			logger.error("Fail to write configuration file template ");
			logger.debug(LogManager.getStackTrace(e));
			os.close();
			file.delete();
			throw e;
		}
	}
	
	public Configuration importTemplate(String filepath){
		Document doc = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Configuration config = null;
		
		try {
			DocumentBuilder db =dbf.newDocumentBuilder();
			doc = db.parse(new File(filepath));
			config = deserializeXml(doc);
			synchConfiguration = new Configuration(config);
				
		} catch (ParserConfigurationException e) {
			logger.error("Fail to import configuration file template ");
			logger.debug(LogManager.getStackTrace(e));
		} catch (SAXException e) {
			logger.error("Fail to import configuration file template ");
			logger.debug(LogManager.getStackTrace(e));
		} catch (IOException e) {
			logger.error("Fail to import configuration file template " );
			logger.debug(LogManager.getStackTrace(e));
		} catch (ServerCreationFailedException e) {
			// TODO Auto-generated catch block
			logger.error("Fail to import configuration file template " );
		} catch (JawaCreationFailedException e) {
			// TODO Auto-generated catch block
			logger.error("Fail to import configuration file template " );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Fail to import configuration file template " );
			logger.debug(LogManager.getStackTrace(e));
		}
		
		return config;
	}
	
	public void showXml(String filename, Configuration configuration){
		Document document = serializeXml(configuration, 0);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ElementToStream(document.getDocumentElement(), baos);
		String result = new String(baos.toByteArray());
		System.out.println(result);
	}

	public static void ElementToStream(Element element, OutputStream out) {
	    try {
	      DOMSource source = new DOMSource(element);
	      StreamResult result = new StreamResult(out);
	      TransformerFactory transFactory = TransformerFactory.newInstance();
	      Transformer transformer = transFactory.newTransformer();
	      transformer.transform(source, result);
	    } catch (Exception ex) {
	    	logger.debug(LogManager.getStackTrace(ex));
	    }
	  }
	
	public void connectController() throws Exception{
		sandCrawler = new SandCrawler();
		System.out.println("connect to the controller");
	}
	
	public boolean isControllerActive(){
		if(sandCrawler==null){
			System.out.println("controller is not active");
			return false;
		}else{
			if(sandCrawler.isAlive()){
				System.out.println("controller is active");
				return true;
			}else{
				System.out.println("controller not active");
				return false;
			}
		}
		
	}
	
	public void disconnectController(){
		if(sandCrawler!=null){
			sandCrawler.stop();
			System.out.println("stop the controller");
		}
	}
	
	public ConfigurationError uploadConfiguration(Configuration configuration){
		ConfigurationError error = null;
		
		try {
			//sandCrawler = new SandCrawler();
			if(!isControllerActive()){
				connectController();
			}
			SCAllConfig allConfig = configuration.parseToSCAllConfig();
			
			logger.info("Parsed SCAllConfig object info for uploading:" );
			logger.info(allConfig.toString());
			
			logger.info("Total jawa numbers to upload:" + allConfig.getJawas().size());
			System.out.println("Total jawa numbers to upload:" + allConfig.getJawas().size());
			
			for(SCJawaModel jawa: allConfig.getJawas()){
				logger.info("Try to upload Jawa: " + getJawaLog(jawa));
				System.out.println("Try to upload Jawa: " + getJawaLog(jawa));
			}
			
			for(SCServerModel server: allConfig.getServers()){
				logger.info("Try to upload server: " + getServerLog(server));
				System.out.println("Try to upload server: " + getServerLog(server));
			}
			
			for(SCRackModel rack: allConfig.getRacks()){
				logger.info("Try to upload rack: " + getRackLog(rack));
				System.out.println("Try to upload rack: " + getRackLog(rack));
				
			}
			
			sandCrawler.uploadConfiguration(allConfig);
			synchConfiguration = new Configuration(allConfig);
			//sandCrawler.stop();
			
			error = new ConfigurationError(configuration, Variables.CONFIGURATION_NO_ERROR);
		} catch (NoSandCrawlerException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			//logger.error(LogManager.getStackTrace(e1));
			error = new ConfigurationError(configuration, Variables.CONFIGURATION_ERROR_DISCONNECTED_CONTROLLER);
			logger.error(StringConstants.getString("VALIDATION_NO_CONTROLLER"));
		} catch (UnsatisfiedLinkError e){
			error = new ConfigurationError(configuration, Variables.CONFIGURATION_ERROR_DISCONNECTED_CONTROLLER);
			logger.error(StringConstants.getString("VALIDATION_UNSATISFIEDLINK_DLL"));
			
		} catch(BusySandCrawlerException e){
			logger.error("Fail to upload configuration to the controller since the controller is busy");
			error = new ConfigurationError(configuration, Variables.CONFIGURATION_ERROR_BUSY_CONTROLLER);
	
		} catch(Exception e){
			logger.error("Fail to upload configuration to the controller");
			logger.debug(LogManager.getStackTrace(e));
			error = new ConfigurationError(configuration, Variables.CONFIGURATION_ERROR_TRANSFER_CONFIGURATION);
		}
		return error;
	}
	
	public ConfigurationError downloadConfiguration(){
		logger.info("try to download configuration from sandcrawler");
		Configuration configuration = null;
		ConfigurationError error = null;
		try {
			//sandCrawler = new SandCrawler();
			if(!isControllerActive()){
				connectController();
			}
			
			// just for testing
			//sandCrawler.setSMOTAmode();
			//Thread.sleep(10000);
			//System.out.println("set SMOTA mode");
			
			SCAllConfig allConfig = sandCrawler.downloadConfiguration();
			logger.info("Downloaded SCAllConfig object info :" );
			logger.info(allConfig.toString());
			logger.info("Total jawa numbers to download:" + allConfig.getJawas().size());
			System.out.println("Total jawa numbers to download:" + allConfig.getJawas().size());

			logger.info("Total server numbers to download:" + allConfig.getServers().size());
			System.out.println("Total server numbers to download:" + allConfig.getServers().size());

			logger.info("Total rack numbers to download:" + allConfig.getRacks().size());
			System.out.println("Total rack numbers to download:" + allConfig.getRacks().size());

			configuration = new Configuration(allConfig);
			// check unique plugmeter id
			Validator.checkUniquePlugmeterIdValidation(configuration);
			// check mixed phase asssociation
			Validator.checkAllRpduPhaseAssociation(configuration);
			
			if(sandCrawler.isBusy()){
				error = new ConfigurationError(configuration, Variables.CONFIGURATION_ERROR_BUSY_CONTROLLER);
			}else{
				error = new ConfigurationError(configuration, Variables.CONFIGURATION_NO_ERROR);
			}
			
		} catch (NoSandCrawlerException e) {
			configuration = new Configuration();
			error = new ConfigurationError(configuration, Variables.CONFIGURATION_ERROR_DISCONNECTED_CONTROLLER);
			logger.debug(LogManager.getStackTrace(e));
			logger.error(StringConstants.getString("VALIDATION_NO_CONTROLLER"));
		} catch (UnsatisfiedLinkError e){
			configuration = new Configuration();
			error = new ConfigurationError(configuration, Variables.CONFIGURATION_ERROR_DISCONNECTED_CONTROLLER);
			System.out.println("unsatisfiedLinkError configuration");
			
		} catch (Exception e){
			configuration = new Configuration();
			error = new ConfigurationError(configuration, Variables.CONFIGURATION_ERROR_RECEIVE_CONFIGURATION);
			logger.debug(LogManager.getStackTrace(e));
			logger.error("Fail to download configuration from the controller");
		}
		
		synchConfiguration = new Configuration(configuration);
		
		return error;
	}

	private String getJawaLog(SCJawaModel jawa){
		return "ID: " + Long.toString(jawa.getJid()) + "phase: " + Integer.toString(jawa.getPhase())+ " Feed:" + Integer.toString(jawa.getFeed()) +  " Rack: " + jawa.getRack()+ " Ulocation: " + jawa.getuPosition() + " UHeight:" + jawa.getuHeight();
		
	}
	
	private String getServerLog(SCServerModel server){
		return "Ulocation: " + Integer.toString(server.getuPos()) + " Height:" + Integer.toString(server.getuHeight()) +  " FacePlate: " + server.getFaceplateWatts();
		
	}
	
	private String getRackLog(SCRackModel rack){
		return "Type: "+ rack.rack + " Height:" + rack.height + " deltaOrWye: " +rack.deltawye;
	}

	
	
	private boolean checkConfigurationValidation(Configuration config){
		boolean isValid = true;
		
		for(int i=0;i<3;i++){
			Rack rack = config.getRackByType(i);
			for(Server server:rack.getServers()){
				
			}
		}
		
		return isValid;
	}
	
	public boolean isSavedConfiguration(Configuration configuration){
		if(synchConfiguration==null || configuration==null){
			if(configuration==null)
				return true;
			else
				return false;
		}else{
			return synchConfiguration.equals(configuration);
		}
	}
	
	/*
	 * reset method 
	 */
	public void resetP3(ArrayList<Jawa> jawas) throws InvalidChecksum{
		try {
			if(sandCrawler.isBusy()){
				MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_BUSY_CONTROLLER"));
				logger.error("Cannot issue command because the controller is busy");
				return;
			}
		} catch (NoSandCrawlerException e1) {
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_NO_CONTROLLER"));
			logger.error("Cannot issue command because no controller");
			return;
		}
		
		ArrayList<Long> jawaids = new ArrayList<Long>();
		
		try {
			for(Jawa jawa:jawas){
				logger.info("Attempt to reset plugmeter " + jawa.toString());
				jawaids.add(IDFormat.hrToCode(jawa.getId().trim()));
			}
		}catch (InvalidChecksum e) {
			throw e;
		}

		
		// reset jawas
		final CommandRunnerDialog dialog = new CommandRunnerDialog((JFrame)(MainScreen.getFrames()[0]), jawaids, 1, sandCrawler);
		
		Thread t = new Thread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				dialog.setVisible(true);
			}
			
		});
		
		t.start();
	}
	
	/*
	 * activate plugmeters
	 */
	public void activateP3(ArrayList<Jawa> jawas) throws InvalidChecksum{
		try {
			if(sandCrawler.isBusy()){
				MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_BUSY_CONTROLLER"));
				logger.error("Cannot issue command because the controller is busy");
				return;
			}
		} catch (NoSandCrawlerException e1) {
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_NO_CONTROLLER"));
			logger.error("Cannot issue command because no controller");
			return;
		}
		
		ArrayList<Long> jawaids = new ArrayList<Long>();
		try {
			for(Jawa jawa:jawas){
				jawaids.add(IDFormat.hrToCode(jawa.getId().trim()));
				logger.info("Attempt to identify plugmeter " + jawa.toString());
			}
			
		}catch(InvalidChecksum e){
			throw e;
		}

		final CommandRunnerDialog dialog = new CommandRunnerDialog((JFrame)(MainScreen.getFrames()[0]), jawaids,2, sandCrawler);
		
		Thread t = new Thread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				dialog.setVisible(true);
			}
			
		});
		
		t.start();
			
	
	}
	
	public void activateOrderingLighting(Rack rack){
		try {
			if(sandCrawler.isBusy()){
				MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_BUSY_CONTROLLER"));
				logger.error("Cannot issue command because the controller is busy");
				return;
			}
		} catch (NoSandCrawlerException e1) {
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_NO_CONTROLLER"));
			logger.error("Cannot issue command because no controller");
			return;
		}
		
		final RabbitLightRunnerDialog dialog = new RabbitLightRunnerDialog((JFrame)(MainScreen.getFrames()[0]), rack.getType(),sandCrawler);
		
		Thread t = new Thread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				dialog.setVisible(true);
			}
			
		});
		
		t.start();
			
	}

	public ConfigurationError getRealtimeData(Jawa jawa){
		ConfigurationError error = null;
		JawaData data = null;
		
		try {
			
			if(!isControllerActive()){
				connectController();
			}
			
			long jawa_long_id = 0;
			jawa_long_id = IDFormat.hrToCode(jawa.getId());
			
			System.out.println("getRealtimeData jawa " + jawa_long_id);
			data = sandCrawler.getCurrentJawaData(jawa_long_id);
			
			long timeStamp = data.timestamp;
			//System.out.println("current timestamp:" + System.currentTimeMillis());
			//System.out.println("realtime data timestamp:" + timeStamp);
			long timeGap = System.currentTimeMillis() - timeStamp;
			System.out.println("time gap:" + timeGap);
			
			// time gap is bigger than 1min(60000miliseconds)
			if(timeGap>60000){
				data = null;
			}
			//sandCrawler.stop();
			
			if(data==null){
				error = new ConfigurationError(data, Variables.CONFIGURATION_ERROR_REALTIME_DATA);
			}else{
				error = new ConfigurationError(data, Variables.CONFIGURATION_NO_ERROR);
			}
			
		} catch (NoSandCrawlerException e) {
			error = new ConfigurationError(data, Variables.CONFIGURATION_ERROR_DISCONNECTED_CONTROLLER);
			logger.debug(LogManager.getStackTrace(e));
			
		} catch (UnsatisfiedLinkError e){
			error = new ConfigurationError(data, Variables.CONFIGURATION_ERROR_DISCONNECTED_CONTROLLER);
			System.out.println("unsatisfiedLinkError configuration");
			
		}catch(BusySandCrawlerException e){
			logger.error("Fail to upload configuration to the controller since the controller is busy");
			error = new ConfigurationError(data, Variables.CONFIGURATION_ERROR_BUSY_CONTROLLER);
	
		}catch (InvalidChecksum e) {
			// TODO Auto-generated catch block
			logger.error("Invalid Checksum " + jawa.getId());
			logger.debug(LogManager.getStackTrace(e));
			error = new ConfigurationError(data, Variables.CONFIGURATION_ERROR_P3_ID);
		}catch (Exception e) {
			error = new ConfigurationError(data, Variables.CONFIGURATION_ERROR_REALTIME_DATA);
			logger.debug(LogManager.getStackTrace(e));
		}
		
		return error;
		
	}
	
	public ConfigurationError clearConfiguration(){
		ConfigurationError error = null;
		Configuration configuration = null;
				
		try {
			if(!isControllerActive()){
				connectController();
			}
			
			sandCrawler.clearConfig();
			
			SCAllConfig allConfig = sandCrawler.downloadConfiguration();
			logger.info("Downloaded SCAllConfig object info :" );
			logger.info(allConfig.toString());
			logger.info("Total jawa numbers to download:" + allConfig.getJawas().size());
			System.out.println("Total jawa numbers to download:" + allConfig.getJawas().size());

			logger.info("Total server numbers to download:" + allConfig.getServers().size());
			System.out.println("Total server numbers to download:" + allConfig.getServers().size());

			logger.info("Total rack numbers to download:" + allConfig.getRacks().size());
			System.out.println("Total rack numbers to download:" + allConfig.getRacks().size());


			if(allConfig.isEmpty()){
				configuration = new Configuration(allConfig);
				error = new ConfigurationError(configuration, Variables.CONFIGURATION_NO_ERROR);
			}else{
				error = new ConfigurationError(configuration, Variables.CONFIGURATION_ERROR_CLEAR_CONFIGURATION);
			}
			
			synchConfiguration = configuration;
			
		} catch (NoSandCrawlerException e) {
			error = new ConfigurationError(configuration, Variables.CONFIGURATION_ERROR_DISCONNECTED_CONTROLLER);
			logger.debug(LogManager.getStackTrace(e));
			
		} catch (UnsatisfiedLinkError e){
			error = new ConfigurationError(configuration, Variables.CONFIGURATION_ERROR_DISCONNECTED_CONTROLLER);
			System.out.println("unsatisfiedLinkError configuration");
			
		}catch(BusySandCrawlerException e){
			logger.error("Fail to upload configuration to the controller since the controller is busy");
			error = new ConfigurationError(configuration, Variables.CONFIGURATION_ERROR_BUSY_CONTROLLER);
	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return error;
	}
	
	class CommandRun implements Runnable{
		private int totalQues = 0;
		private long jid ;
		private int currentQ;
		
		private ArrayList<Long> jawaids;
		private boolean keepRunning;
		
		public CommandRun(ArrayList<Long> jawaids){
			this.totalQues = jawaids.size();
			this.jawaids = jawaids;
			keepRunning = true;
			currentQ = 0;
		}
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			SCConfigMessage msg = null;
			SCConfigService configService = sandCrawler.getConfigService();
			jid = jawaids.get(currentQ);
			try {
				sandCrawler.sendResetCommand(jid);
			} catch (NoSandCrawlerException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return;
			} 
			System.out.println("current q:" + currentQ);
			System.out.println("current jawa:" + jid);
		
			
			while(keepRunning){
				if(msg ==null){
					System.out.println("no response yet" + jid);
					
					try {
						msg = configService.receive();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						
						keepRunning = false;
					} catch (NoSandCrawlerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						keepRunning = false;
					}
				}else{
					if(msg.type == SCConfigMessage.TYPE_R_COMMAND){
						System.out.println(jid+ " command response is " + msg.commandresponse);
						currentQ++;
						
						if(currentQ<totalQues){
							jid = jawaids.get(currentQ);
							msg = null;
							System.out.println("current q:" + currentQ);
							System.out.println("current jawa:" + jid);
							
							
							try {
								sandCrawler.sendResetCommand(jid);
								msg = configService.receive();
							} catch (NoSandCrawlerException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								keepRunning = false;
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								keepRunning = false;
							} 
						}else{
							keepRunning = false;
						}
					}
				}
			}
		}
	}
}
