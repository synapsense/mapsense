package com.synapsense.p3.utilities;


public class ThreadTest implements Runnable{
	private int totalQues = 0;
	private volatile int currentQues;
	private volatile long jid;
	
	private Thread commandThread;
	private volatile boolean noStopCommand;
	
	public ThreadTest(int totalQues){
		this.totalQues = totalQues;
		
		
		noStopCommand = true;
		currentQues = 0;
		commandThread = new Thread(this, "runCommand");
		commandThread.start();
	}
	
	
	private int command(long id){
		try{
			Thread.sleep(1000);
			
			return 1;
		}catch(InterruptedException x){
			x.printStackTrace();
			return -3;
		}
	}
	

	
	
	
	private void cancelCommand(){
		
		noStopCommand = false;
		
		if(commandThread!=null)
			commandThread.interrupt();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(noStopCommand){
			
			try {
				Thread.sleep(1000);
				jid = 12121212;
				currentQues++;
				
				int result = command(jid);
				System.out.println("jid:" + jid);
				
				System.out.println("currentQues:" + currentQues);
				
				
				
				if(currentQues==totalQues)
					noStopCommand = false;
				
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			} 
			
			
		}
		
		System.out.println("done");
	}
}
