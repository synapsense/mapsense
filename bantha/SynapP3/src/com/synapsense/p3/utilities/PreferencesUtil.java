package com.synapsense.p3.utilities;

import java.util.prefs.Preferences;

public class PreferencesUtil {
	private static Preferences prefs = Preferences.userRoot();
	private static final String PREF_FILE_KEY = "synapp3.user.filepath";
	private static final String PREF_DIR_KEY = "synapp3.user.dirpath";
	
	public static void setSavedFilePath(String filePath, String dirPath){
		prefs.put(PREF_FILE_KEY, filePath);
		prefs.put(PREF_DIR_KEY, dirPath);
	}
	
	public static String getSavedFilePath(){
		if(prefs.get(PREF_FILE_KEY, null)==null)
			return StringConstants.getString("FILE_DEFAULT_SAVE");
		else
			return prefs.get(PREF_FILE_KEY, null);
	}
	
	public static String getSavedDirPath(){
		return prefs.get(PREF_DIR_KEY, null);
	}
}
