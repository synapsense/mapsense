package com.synapsense.p3.utilities;

import java.io.PrintWriter;
import java.io.StringWriter;

public class LogManager {
	static{
		
	}
	
	public static String getStackTrace(Exception e){
		//e.getStackTrace();
		
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString();
	}
}
