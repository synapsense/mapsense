package com.synapsense.p3.utilities;

import java.awt.Dimension;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.synapsense.p3.components.PlugmeterButton;
import com.synapsense.p3.components.ServerPanel;
import com.synapsense.p3.model.PhaseAssociation;
import com.synapsense.p3.model.Server;

public class CommonFunctions {
	private static Logger logger = Logger.getLogger(CommonFunctions.class.getName());
	
	static {
	}
	
	public static void redeployPlugmeters(ServerPanel serverPanel){
		Server server = serverPanel.getServer();
		server.resetJawaOrders();
		
		for(PlugmeterButton pm:serverPanel.getPlugmeters()){
			Dimension size = pm.getPreferredSize();
			int jawaIndex = pm.getJawa().getIndex();
			
			pm.setBounds(jawaIndex*pm.ICON_WIDTH -12, 5, size.width, size.height);
		}
		
	}
	
	public static String getPhaseFromRealtimeData(boolean delta, int phase){
		String phaseInfo = "";
		
		 HashMap<Integer, PhaseAssociation> phaseAssociations = Variables.phaseAssociations;
		 
		 for(PhaseAssociation pa: phaseAssociations.values()){
			 if(pa.getDelta()==delta && pa.getPhase()==phase)
				 phaseInfo = pa.getName();
		 }
		
		
		return phaseInfo;
	}
}
