package com.synapsense.p3.utilities;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.border.LineBorder;
import javax.swing.table.AbstractTableModel;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXTable;

import com.synapsense.p3.components.PlugmeterButton;
import com.synapsense.p3.components.RackTable;
import com.synapsense.p3.components.RackTable.RackTableModel;
import com.synapsense.p3.components.ServerPanel;
import com.synapsense.p3.exceptions.ServerCreationFailedException;
import com.synapsense.p3.model.Configuration;
import com.synapsense.p3.model.Jawa;
import com.synapsense.p3.model.Rack;
import com.synapsense.p3.model.Server;
import com.synapsense.p3.ui.ConfigurationButton;
import com.synapsense.p3.ui.ErrorsTable.ErrorsTableModel;
import com.synapsense.p3.ui.FindDialog;
import com.synapsense.p3.ui.MainScreen;
import com.synapsense.p3.ui.RackPanel;
import com.synapsense.p3.ui.PlugmetersPanel;
import com.synapsense.p3.ui.PlugmetersTable.PlugmetersTableModel;
import com.synapsense.p3.utilities.Variables.ConfigurationTypes;

public class ComponentsManager {
	private static Logger logger = Logger.getLogger(ComponentsManager.class.getName());

	// binded components 
	private MainScreen mainScreen;
	private HashMap<Integer, RackTable> rackTables = new HashMap<Integer, RackTable>();
	private PlugmetersPanel plugmetersPanel;
	
	// plugmeter for phase association
	//private PlugmeterButton phasePlugmeter = null;
	
	private JXTable p3Table;
	private PlugmetersTableModel p3TableModel;
			
	private JXTable errorTable;
	private ErrorsTableModel errorTableModel;
	
	private SelectionManager selectionManager;
	
	private ConfigurationTypes selectedConfigType = null;
	private int selectedConfigMode = Variables.CONFIGURATION_SELECTION_MODE_SINGLE;
	private ConfigurationButton selectedConfigButton;
	private ArrayList<ConfigurationButton> configurationButtons = new ArrayList<ConfigurationButton>();
	
	public ComponentsManager(SelectionManager selectionManager){
		this.selectionManager = selectionManager;
	}
	
	public void bindComponents(MainScreen main, RackTable rack1, RackTable rack2, RackTable rack3, PlugmetersPanel _plugmetersPanel){
		mainScreen = main;
		rackTables.put(Variables.LEFT_RACK_TYPE, rack1);
		rackTables.put(Variables.CENTER_RACK_TYPE, rack2);
		rackTables.put(Variables.RIGHT_RACK_TYPE, rack3);
		plugmetersPanel = _plugmetersPanel;
		p3Table = plugmetersPanel.getPlugmetersTable();
		p3TableModel = (PlugmetersTableModel)(p3Table.getModel());
		errorTable = plugmetersPanel.getErrorsTable();
		errorTableModel = (ErrorsTableModel)errorTable.getModel();
	}

	public RackTable getRackTableByType(int type){
		return rackTables.get(type);
	}
	
	public void deleteServers(){
		try{
			ArrayList<ServerPanel> clonedServers= (ArrayList<ServerPanel>) selectionManager.getSelectedServers().clone();
			for(ServerPanel sor:clonedServers){
				
				Rack rack = sor.getServer().getRack();
				int rackType = rack.getType();
				
				Server server = sor.getServer();
				rack.deleteServer(server);
				
				
				JXTable rackTable = rackTables.get(rackType);
				RackTableModel model = (RackTableModel)rackTable.getModel();
				model.removeFrom(sor);
				model.fireTableDataChanged();
			}
			selectionManager.removeServerSelection(clonedServers, null);
			
		}catch(Exception e){
			logger.error("Fail to delete selected server(s)");
			logger.debug(LogManager.getStackTrace(e));
		}
		
	}
	public void deletePlugmeters(){
			
		ArrayList<PlugmeterButton> selectedPlugmeters = selectionManager.getSelectedPlugmeters();
		
		
		for(PlugmeterButton p:selectedPlugmeters){
			ServerPanel serverPanel = (ServerPanel)p.getParent();
			serverPanel.deletePlugmeter(p);
		}
		
		selectionManager.clearPlugmeterSelection(null);
	}
	
	public HashMap<Integer, ServerPanel> getSortedServersByPosition(ServerPanel base){
		HashMap<Integer, ServerPanel> sortedMap = new HashMap<Integer, ServerPanel>();
		
		int baseLocation = base.getULocation();
		
		for(ServerPanel sor:selectionManager.getSelectedServers()){
			int ulocation = sor.getServer().getLocation();
			sortedMap.put((ulocation -baseLocation)*-1, sor);
		}
		
		return sortedMap;
	}
	
	public void selectAllP3InWorkingRack(){
		if(selectionManager.getSelectedRack()==null)
			return;
		
		RackTable workingRack = getRackTableByType(selectionManager.getSelectedRack().getType());
		if(workingRack==null)
			return;
		
		selectionManager.clearSelection();
		
		for(ServerPanel sp:((RackTableModel) workingRack.getModel()).getServerPanels()){
			for(PlugmeterButton pb:sp.getPlugmeters()){
				selectionManager.addServerSelection((ServerPanel)pb.getParent(), null);
				selectionManager.addPlugmeterSelection(pb, null);
			}
		}
	}
	
	public void selectAllP3(){
		selectionManager.clearSelection();
		
		for(int i=Variables.LEFT_RACK_TYPE;i<=Variables.RIGHT_RACK_TYPE;i++){
			RackTable currentRack = getRackTableByType(i);

			for(ServerPanel sp:((RackTableModel) currentRack.getModel()).getServerPanels()){
				for(PlugmeterButton pb:sp.getPlugmeters()){
					selectionManager.addServerSelection((ServerPanel)pb.getParent(), null);
					selectionManager.addPlugmeterSelection(pb, null);
				}
			}
		}
	}
	
	public void selectAllServers(){
		selectionManager.clearSelection();

		for(int i=Variables.LEFT_RACK_TYPE;i<=Variables.RIGHT_RACK_TYPE;i++){
			RackTable currentRack = getRackTableByType(i);

			for(ServerPanel sp:((RackTableModel) currentRack.getModel()).getServerPanels()){
				selectionManager.addServerSelection(sp, null);
				
			}
			
		}
	}
	
	public void setFocusPlugmeterID(PlugmeterButton pm){
		int selectedRow = p3TableModel.getRowByJawaIndex(pm.getJawa().getIndex());
		p3Table.changeSelection(selectedRow, 1, false, false);
		p3Table.requestFocus();	
		//plugmetersPanel.adjustChildrenSizes();
	}
	
	public void associatePhase(){
		PlugmeterButton pm = selectionManager.getSelectedPlugmeters().get(0);
		Jawa jawa = pm.getJawa();
		
		// get associated jawa with current selected RPDU
		Rack rack = jawa.getServer().getRack();
		int rpdu = jawa.getFeed();
		Jawa phaseJawa = rack.getRpduPhaseAssociation(rpdu);

		// get phase information which a user wants to change
		int phase = jawa.getPhase();
		phase++;
		
		if(phase>6)
			phase =0;

		if(phaseJawa==null){
			
		}else{
			if(jawa.equals(phaseJawa)){
				
			}else{
				PlugmeterButton oldPb = getPlugmeterButton(phaseJawa);
				oldPb.dissociatePhase();
			}
		}
		
		pm.associatePhase(phase);
		
		selectionManager.setServerSelection((ServerPanel)pm.getParent(), null);
		selectionManager.setPlugmeterSelection(pm, null);
	
		Validator.checkRpduPhaseAssociation(jawa);
		
	}
	
	public PlugmeterButton getPlugmeterButton(Jawa jawa){
		PlugmeterButton pb = null;
		Server server = jawa.getServer();
		Rack rack = server.getRack();
		
		RackTableModel rackTableModel = rackTables.get(rack.getType()).getTableModel();
		ServerPanel serverPanel = rackTableModel.getServerAtUlocation(server.getLocation());
		pb = serverPanel.getPlugmeter(jawa);
		
		return pb;
	}
	
	/*public PlugmeterButton getPhasePlugmeter(){
		return phasePlugmeter;
	}*/
	
	/*public void setPhasePlugmeter(PlugmeterButton pm){
		phasePlugmeter = pm;
	}*/
	
	/*
	public void setPhasePlugmeter(Jawa jawa){
		if(jawa==null)
			return;
		
		int rackType = jawa.getServer().getRack().getType();
		PlugmeterButton plugmeter = null;
		
		RackTable rackTable = rackTables.get(rackType);
		RackTableModel rackTableModel = (RackTableModel) rackTable.getModel();
		for(ServerPanel serverPanel: rackTableModel.getServerPanels()){
			for(PlugmeterButton pb:serverPanel.getPlugmeters()){
				if(pb.getJawa().equals(jawa)){
					plugmeter = pb;
				}
			}
		}
		
		if(plugmeter!=null)
			setPhasePlugmeter(plugmeter);
		
	}*/
	
	/*public void removePhasePlugmeter(){
		phasePlugmeter = null;
	}*/
	
	public void updataConfiguration(Configuration configuration){
		((RackPanel) getRackTableByType(Variables.LEFT_RACK_TYPE).getParent()).setRack(configuration.getRackByType(Variables.LEFT_RACK_TYPE));
		((RackPanel) getRackTableByType(Variables.CENTER_RACK_TYPE).getParent()).setRack(configuration.getRackByType(Variables.CENTER_RACK_TYPE));
		((RackPanel) getRackTableByType(Variables.RIGHT_RACK_TYPE).getParent()).setRack(configuration.getRackByType(Variables.RIGHT_RACK_TYPE));
		
		selectionManager.clearSelection();
		plugmetersPanel.setConfiguration(configuration);
	}
	
	public ArrayList<Jawa> getSelectedJawas(){
		ArrayList<Jawa> jawas = new ArrayList<Jawa>();
		
		for(PlugmeterButton pm:selectionManager.getSelectedPlugmeters()){
			jawas.add(pm.getJawa());
		}
		
		return jawas;
	}
	
	public void showRackInfo(int rackType){
		String rackInfo =((RackTableModel)(rackTables.get(rackType).getModel())).getDebugData();
		MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, rackInfo);
	}
	
	public void showPlugmeterSelection(){
		String p3Info ="";
		
		for(PlugmeterButton pm: selectionManager.getSelectedPlugmeters()){
			p3Info+=pm.getJawa().toString() + "\n";
		}
		
		MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, p3Info);
		
	}
	
	public void showServerSelection(){
		String serverInfo ="";
		
		for(ServerPanel server: selectionManager.getSelectedServers()){
			serverInfo+=server.toString() + "\n";
		}
		
		MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, serverInfo);
		
	}
	
	public void deleteSelections(){
		if(selectionManager.getSelectedServers().size()==0){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_P3_SELECTION_SERVER_MSG"));
			return;
		}
		
		if(selectionManager.getSelectedPlugmeters().size()>0){
			int response = MessageHandler.showConfirmationMessage(StringConstants.getString("VALIDATION_DELETE_P3_MSG"));
			if(response == JOptionPane.YES_OPTION){
				deletePlugmeters();
			}

		}else{
			int response = MessageHandler.showConfirmationMessage(StringConstants.getString("VALIDATION_DELETE_SERVER_MSG"));
			if(response == JOptionPane.YES_OPTION){
				deleteServers();
			}
		}
	}
	
	/*public void maximizeScreen(){
		mainScreen.maximizeScreen();
	}
	
	public void minimizeScreen(){
		mainScreen.minimizeScreen();
	}*/
	
	public void showRackPhaseInfo(int rackType){
		
		String phaseInfo = rackTables.get(rackType).getRack().printPhaseAssociationInfo();
		MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, phaseInfo);
	}
	
	/**
	 * show dialog to find a jawa(s)
	 */
	public void showFindJawaDialog(){
		FindDialog findDialog = new FindDialog(this, selectionManager);
		findDialog.setVisible(true);
	
	}
	
	/**
	 * Find Jawas by mac id
	 * @param jawaId
	 * @return
	 */
	public ArrayList<PlugmeterButton> findJawaById(String keyword, String hrCode){
		ArrayList<PlugmeterButton> plugmeters = new ArrayList<PlugmeterButton>();
		
		for(RackTable rackTable:rackTables.values()){
			RackTableModel rackModel = rackTable.getTableModel();
			
			for(ServerPanel serverPanel: rackModel.getServerPanels()){
				for(PlugmeterButton pb: serverPanel.getPlugmeters()){
					/*if(pb.getJawa().getId().equals(jawaId.toUpperCase())){
						plugmeters.add(pb);
					}*/
					String jawaId = pb.getJawa().getId();
					if(jawaId.contains(keyword.toUpperCase()) || jawaId.contains(hrCode)){
						plugmeters.add(pb);
					}
				}
			}
		}
		return plugmeters;
	}
	
	/**
	 * set selected configuration button, deselect other ones
	 * @param configButton
	 */
	public void setSelectedConfigType(ConfigurationButton configButton, int selectionMode){
		selectedConfigButton = configButton;
		this.selectedConfigType = configButton.getConfigurationType();
		selectedConfigMode = selectionMode;
		
		for(ConfigurationButton cb: configurationButtons){
			if(cb.equals(selectedConfigButton))
				cb.setSelected(true);
			else
				cb.setSelected(false);
		}
	}
	
	public void removeSelectedConfigType(){
		selectedConfigButton.setSelected(false);
		selectedConfigType = null;
		selectedConfigButton = null;
		
			
	}
	/**
	 * get selected configuration type
	 * @return
	 */
	public ConfigurationTypes getSelectedConfigType(){
		return selectedConfigType;
	}
	
	/**
	 * add configuration button to arrayList to interact with configuration selection
	 * @param button
	 */
	public void addConfiguarationButton(ConfigurationButton button){
		configurationButtons.add(button);
	}
	
	/**
	 * Add new configuration (add plugmeter, 1 server with 1 plugmeter, 1 server with 2 plugmeters or custom sever
	 * @param rackTable
	 * @param selectedRow
	 * @return
	 */
	public boolean addConfiguration(RackTable rackTable, int selectedRow){
		if(selectedConfigType==null){
			return false;
		}
		
		RackTableModel data = (RackTableModel) rackTable.getModel();
		switch(selectedConfigType){
			case PLUGMETER:		// add a plugmeter
				return addPlugmenter(rackTable, selectedRow);
			case SRV1U:			// add a server without plugmeter
				return addServerPlugmenter(1, 0, selectedRow, rackTable);
			case SRV1U1P:		// add a server with one plugmeter
				return addServerPlugmenter(1, 1, selectedRow, rackTable);
			case SRV1U2P:		// add a server with two plugmeters
				return addServerPlugmenter(1, 2, selectedRow, rackTable);
			case SRVCUSTOM:		// add a custom server
				return addServerPlugmenter(1, 0, selectedRow, rackTable);
		}
		return false;
	}
	
	/**
	 * Add a server with plugmeter (no, one or two)
	 * @param serverValue
	 * @param plugmeterValue
	 * @param selectedRow
	 * @param rackTable
	 * @return
	 */
	private boolean addServerPlugmenter(int serverValue, int plugmeterValue, int selectedRow, RackTable rackTable){
		try{
			RackTableModel data = (RackTableModel) rackTable.getModel();
			
			if(Validator.checkServerLocation(selectedRow, 1, rackTable.getRack(), null)){
				// add a server first
				ServerPanel serverPanel = ((RackTableModel)data).addData(new Object[]{selectedRow, 1,null});
				selectionManager.setServerSelection(serverPanel, null);
				
				// add plugmeter
				if(plugmeterValue==0){
					selectionManager.clearPlugmeterSelection(null);
				}
				else if(plugmeterValue==1){
					PlugmeterButton pm = serverPanel.createPlugmeter(1);
					selectionManager.setPlugmeterSelection(pm, null);
					setFocusPlugmeterID(pm);
				}else if(plugmeterValue==2){
					PlugmeterButton pm1 = serverPanel.createPlugmeter(1);
					PlugmeterButton pm2 = serverPanel.createPlugmeter(2);
					selectionManager.setPlugmeterSelection(pm1, null);
					setFocusPlugmeterID(pm1);
				}
				if(selectedConfigMode==Variables.CONFIGURATION_SELECTION_MODE_SINGLE)
					removeSelectedConfigType();
				return true;
			}else{
				MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_ADD_SERVER_INSUFFICIENT_SPACE_MSG"));
				System.out.println("cannot locate server");
				return false;
			}
			
		}catch(ServerCreationFailedException e){
			MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_ERROR_ADD_SERVER_INSUFFICIENT_SPACE_MSG"));
			return false;
		}
		catch(Exception e){
			logger.error("Fail to add new server");
			logger.debug(LogManager.getStackTrace(e));
			return false;
		}	
	}
	
	/**
	 * Add a plugmeter alone
	 * A server should exist in selected row to add a new plugmeter
	 * @param rackTable
	 * @param selectedRow
	 * @return
	 */
	private boolean addPlugmenter(RackTable rackTable, int selectedRow){
		RackTableModel data = (RackTableModel) rackTable.getModel();
		try{
			ServerPanel sor = data.getServerAtRow(selectedRow);
			
			if(sor==null){
				logger.error("Cannot add smartplug without server");
				return false;
			}
			
			Server server = sor.getServer();
			
			if(Validator.checkMaxPlugmeter(server)){
				PlugmeterButton pm = sor.createPlugmeter();
	
				selectionManager.setServerSelection(sor, null);
				selectionManager.setPlugmeterSelection(pm, null);
				setFocusPlugmeterID(pm);
				if(selectedConfigMode==Variables.CONFIGURATION_SELECTION_MODE_SINGLE)
					removeSelectedConfigType();
				return true;
				
			}else{
				MessageHandler.showGeneralMessage(MessageHandler.MESSAGE_TYPE_INFORMATION, StringConstants.getString("VALIDATION_MAX_P3_ERROR_MSG"));
				return false;
			}
			
		}catch(Exception e){
			logger.error("Fail to add new plugmeter");
			logger.debug(LogManager.getStackTrace(e));
			return false;
		}
	}

	public int getMaxRackUheight(){
		int max = 0;
		
		for(RackTable rackTable:rackTables.values()){
			Rack rack = rackTable.getRack();
			if(max<rack.getUheight())
				max = rack.getUheight();
		}
		
		return max;
	}
	
	public int getMinRackUheight(){
		int min = 999;
		
		for(RackTable rackTable:rackTables.values()){
			Rack rack = rackTable.getRack();
			if(min>rack.getUheight())
				min = rack.getUheight();
		}
		
		return min;
	}
	
	public void shrinkRackTables(int height){
		for(RackTable rackTable:rackTables.values()){
			rackTable.setSize(rackTable.getWidth(), height);
			rackTable.repaint();
		}
	}
	
}

