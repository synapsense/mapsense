package com.synapsense.p3.utilities;

import javax.swing.JOptionPane;

import com.synapsense.p3.ui.MainScreen;

public class MessageHandler {
	public static final int MESSAGE_TYPE_ERROR = 0;
	public static final int MESSAGE_TYPE_INFORMATION = 1;
	public static final int MESSAGE_TYPE_CONFIRMATION = 2;
	
	public static void showGeneralMessage(int msgType, String msg){
		JOptionPane.showMessageDialog(MainScreen.getFrames()[0], msg, getTitle(msgType), msgType);
	}
	
	public static int showConfirmationMessage(String msg){
		int response = JOptionPane.showConfirmDialog(MainScreen.getFrames()[0], msg, StringConstants.getString("VALIDATION_CONFIRMATION_TITLE"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		return response;
	}
	
	public static int showOptionMessage(int msgType, String msg, String[] options){
		int response = JOptionPane.showOptionDialog(MainScreen.getFrames()[0], msg, getTitle(msgType), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
		return response;
	}
	
	private static String getTitle(int msgType){
		String title = "";
		if(msgType==MESSAGE_TYPE_ERROR)
			title = StringConstants.getString("VALIDATION_ERROR_TITLE");
		else if(msgType==MESSAGE_TYPE_INFORMATION){
			title = StringConstants.getString("VALIDATION_INFORMATION_TITLE");
		}else if(msgType==MESSAGE_TYPE_CONFIRMATION){
			title = StringConstants.getString("VALIDATION_CONFIRMATION_TITLE");
		}
		return title;
	}
	
	private static void setFocusOnMainScreen(){
		((MainScreen)(MainScreen.getFrames()[0])).getRootPane().requestFocus();
	}
}
