package com.synapsense.p3.utilities;

public interface SelectionChangeListener {
	void selectionChanged(SelectionChangeEvent e);
}
