package com.synapsense.p3.utilities;

import java.util.ArrayList;

public class SelectionChangeEvent {
	private ArrayList<Object> addedObjects = new ArrayList<Object>();
	private ArrayList<Object> removedObjects = new ArrayList<Object>();
	
	public SelectionChangeEvent(){
		
	}
	
	public void add(Object o){
		addedObjects.add(o);
	}
	
	public void remove(Object o){
		removedObjects.add(o);
	}
	
	public ArrayList<Object> getAddtions(){
		return addedObjects;
	}

	public ArrayList<Object> getRemovals(){
		return removedObjects;
	}
}
