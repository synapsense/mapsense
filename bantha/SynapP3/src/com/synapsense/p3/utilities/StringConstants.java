package com.synapsense.p3.utilities;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class StringConstants {
	private static final String BUNDLE_NAME = "resources.strings"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	private StringConstants() {
	}

	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	
	public static int getIntValue(String key){
		try {
			String str= RESOURCE_BUNDLE.getString(key);
			int intValue = Integer.parseInt(str);
			return intValue;
		} catch (MissingResourceException e) {
			return 0;
		} catch (NumberFormatException nfe){
			return 0;
		}
	}
}
