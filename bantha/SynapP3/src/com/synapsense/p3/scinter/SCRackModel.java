package com.synapsense.p3.scinter;

public class SCRackModel implements Comparable<SCRackModel> {
	public int rack;
	public int height;
	public int deltawye;

	public SCRackModel(int rack, int height, int delta) {
		this.rack = rack;
		this.height = height;
		this.deltawye = delta;
	}


	public SCRackModel(int rack, int height) {
		this.rack = rack;
		this.height = height;
		this.deltawye = 0;
	}

	public SCRackModel(byte[] d) {
		rack = d[0] & 0xff;
		height = d[1] & 0xff;
		deltawye = d[2] & 0xff;
	}

	public int compareTo(SCRackModel rhs) {
		return this.rack - rhs.rack;
	}
	
	public void setAllDelta() {
		deltawye = 0xff;
	}
	
	public void clearAllDelta() {
		deltawye = 0x00;
	}
	
	public boolean isDelta() {
		if (deltawye == 0xff) 
			return true;
		return false;
	}

	public void setRPDUDelta(int rpdu) {
		deltawye |= (1 << rpdu);
	}

	public void clearRPDUDelta(int rpdu) {
		deltawye &= ~(1 << rpdu);
	}

	public boolean isRPDUDelta(int rpdu) {
		return (deltawye & (1 << rpdu)) != 0;
	}

	public byte[] pack() {
		byte[] data = new byte[3];
		data[0] = (byte)(rack);
		data[1] = (byte)height;
		data[2] = (byte)(deltawye & 0xff);
		return data;
	}
	
	public String toString() {
		return "<RACK: " + rack + " height " + height + "deltawaye " + deltawye  +">";
	}

}