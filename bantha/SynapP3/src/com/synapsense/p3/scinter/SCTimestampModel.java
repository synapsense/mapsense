package com.synapsense.p3.scinter;

import org.apache.log4j.Logger;
import java.sql.Timestamp;
import java.util.Date;

public class SCTimestampModel implements Comparable<SCTimestampModel> {
	private int ts32; // 32 bit 
	private long ts64;  // you guessed it, 64 bit
	
	private static Logger logger = Logger.getLogger(SCTimestampModel.class);
	
	private void logger (String s){
		System.out.println(s);
	}

	public SCTimestampModel() {
		Date date = new Date();
		this.ts64 = date.getTime(); 
		this.ts32 =  (int) (this.ts64 / 1000);
	}
	
	public SCTimestampModel(int ts) {
		this.ts32 = ts;
		this.ts64 =  (this.ts32  & 0xffffffffL )*1000;
	}

	public int compareTo (SCTimestampModel rhs) {
		if (rhs.getTs32() != ts32)
			return ts32 - rhs.getTs32();
		return 0;
	}

	public long getTs64() {
		return ts64;
	}
	
	public int getTs32() {
		return ts32;
	}

	public void setTs(int ts ) {
		this.ts32 = ts;
		this.ts64 =  (this.ts32  & 0xffffffffL )*1000;
	}
	
	public void setTs(long ts ) {
		this.ts64 = ts;
		this.ts32 =  (int) this.ts64 / 1000;
	}

	public SCTimestampModel(byte[] data) {
		//logger.info("Timestamp :  " + Integer.toHexString(data[0]) + " " + Integer.toHexString(data[1])+ " " + Integer.toHexString(data[2])+ " " + Integer.toHexString(data[3]));
		ts32 = (data[0] << 24) & 0xff000000;
		ts32 |= (data[1] << 16) & 0xff0000;
		ts32 |= (data[2] << 8) & 0xff00;
		ts32 |= data[3] & 0xff;
		this.ts64 =  this.ts32  & 0xffffffffL ;
		this.ts64 =  this.ts64 * 1000L;
		logger.info(this.toString());   
	}

	public byte[] pack() {
		byte[] data = new byte[4];
		data[0] = (byte) ((ts32 >> 24) & 0xff);
		data[1] = (byte) ((ts32 >> 16) & 0xff);
		data[2] = (byte) ((ts32 >> 8) & 0xff);
		data[3] = (byte) ts32;
		//logger.info("Sending TS :  " + Integer.toHexString(data[0]) + " " + Integer.toHexString(data[1])+ " " + Integer.toHexString(data[2])+ " " + Integer.toHexString(data[3]));
		return data;
	}
	
	public String toString() {
		Timestamp ts = new Timestamp(this.ts64);
		return "<SCTimestamp: local "+ ts.toLocaleString()+" (" + ts.toGMTString() +") ts32:"+ Integer.toHexString(ts32) + " ts64:" + Long.toHexString(ts64)  + ">";
	}


}