package com.synapsense.p3.scinter;

import org.apache.log4j.Logger;


public class SCConfigMessage {
	
	/* Messages to the SC */
	
	public final static int TYPE_DL = 0x50;
	public final static int TYPE_DLCOUNT = 0x55;
    public final static int TYPE_STARTCONFIG = 0x10;
    public final static int TYPE_STOPCONFIG = 0x11;
    public final static int TYPE_JAWAPRESENT = 0x20;
	
    /* Unused message types */
    public final static int TYPE_JAWAADD = 0x21;
    public final static int TYPE_JAWADEL = 0x22;

	/* Faceplate power types */
    public final static int TYPE_SERVERPRESENT = 0x33;
	public final static int TYPE_RACKPRESENT = 0x34;
	public final static int TYPE_TIMESTAMPPRESENT = 0x35;
	
	/* Rabbit Lights and other commands */
	public final static int TYPE_STARTRABBIT = 0x40;
	public final static int TYPE_STARTBLINK = 0x41;
	public final static int TYPE_RESETJAWA = 0x42;
	public final static int TYPE_BUSYSMOTA = 0x43;
	public final static int TYPE_CLEARCONFIG = 0x44;
	public final static int TYPE_SETSMOTA = 0x45;

    /* Responses from the SC */
	public final static int TYPE_CONFIGOBJ = 0x80;
    public final static int TYPE_SERVEROBJ = 0x82;
	public final static int TYPE_RACKOBJ   = 0x83;
    public final static int TYPE_CONFIGDLDONE = 0x81;
    public final static int TYPE_R_COMMAND = 0x84;
	public final static int TYPE_R_TIMESTAMP  = 0x85;
    
    /* Command Response from the SC */
    public final static int TYPE_COMRESP_SUCCESS = 1;
    public final static int TYPE_COMRESP_FULL = 0;
    public final static int TYPE_COMRESP_PENDING = -1;
    public final static int TYPE_COMRESP_FAILURE = -2;
    public final static int TYPE_COMRESP_BUSYSMOTA = -3;

	private static Logger logger = Logger.getLogger(SCConfigMessage.class);

    
    public byte[] data = new byte[64];;
    public int type;
    public int numberSent;
    public int commandresponse;

    public SCJawaModel jawa;
	public SCServerModel server;
	public SCRackModel rack;
	public SCTimestampModel ts; 
    
    private void unserialize() throws NoSandCrawlerException {
    	logger.info("UNSERIALIZING type of " + type);
    	if (type == TYPE_CONFIGOBJ) {

    		byte[] jdata = new byte[64];
    		java.lang.System.arraycopy(data, 1, jdata, 0, 63);
    		jawa = new SCJawaModel(jdata);

		} else if (type == TYPE_SERVEROBJ) {

			byte[] sdata = new byte[64];
			java.lang.System.arraycopy(data, 1, sdata, 0, 63);
			server = new SCServerModel(sdata);

		} else if (type == TYPE_RACKOBJ) {

			byte[] rdata = new byte[64];
			java.lang.System.arraycopy(data, 1, rdata, 0, 63);
			rack = new SCRackModel(rdata);
		
		} else if (type == TYPE_R_TIMESTAMP) {

			byte[] rdata = new byte[64];   // why so large? it's only 4 bytes!!
			java.lang.System.arraycopy(data, 1, rdata, 0, 63);
			ts = new SCTimestampModel(rdata);

    	} else if (type == TYPE_CONFIGDLDONE) {
    		numberSent = data[1];
    	} else if (type == TYPE_R_COMMAND) {
    		commandresponse = data[1];
    	} else if (type == 0) {
			throw new NoSandCrawlerException();
		}
    }
    
    public SCConfigMessage(byte[] data) throws NoSandCrawlerException {
        this.data = data;
        type = data[0] & 0xff;
       
        unserialize();
    }

    public SCConfigMessage(int action, SCJawaModel jawa) {
    	this.jawa = jawa;
    	this.data[0] = (byte)(action & 0xff);
    	byte[] jp = jawa.pack();
    	java.lang.System.arraycopy(jp, 0, this.data, 1, jp.length);
    }

    public SCConfigMessage(int action, SCServerModel server) {
    	this.server = server;
    	this.data[0] = (byte)(action & 0xff);
    	byte[] jp = server.pack();
    	java.lang.System.arraycopy(jp, 0, this.data, 1, jp.length);
    }

    public SCConfigMessage(int action, SCRackModel rack) {
    	this.rack = rack;
    	this.data[0] = (byte)(action & 0xff);
    	byte[] jp = rack.pack();
    	java.lang.System.arraycopy(jp, 0, this.data, 1, jp.length);
    }

    public SCConfigMessage(int action, SCTimestampModel ts) {   	
    	this.ts = ts;
    	//logger.info("Making SCConfigMessage for Timestamp " + this.ts.toString());
    	this.data[0] = (byte)(action & 0xff);
    	byte[] jp = ts.pack();
    	java.lang.System.arraycopy(jp, 0, this.data, 1, jp.length);
    }
    
    
    public SCConfigMessage(int action) {
    	this.data[0] = (byte)(action & 0xff);
    	
    }
    
    public SCConfigMessage(int action, long jid) {
    	this.data[0] = (byte)(action & 0xff);
    	this.data[1] = (byte) ((jid >> 24) & 0xff);
    	this.data[2] = (byte) ((jid >> 16) & 0xff);
    	this.data[3] = (byte) ((jid >> 8) & 0xff);
    	this.data[4] = (byte) (jid & 0xff);
    }
    
    public SCConfigMessage(int action, byte racknumber) {
    	this.data[0] = (byte)(action & 0xff);
    	this.data[1] = racknumber;
    }
    
    public String toString() {
        return "";
    }

}
