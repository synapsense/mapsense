package com.synapsense.p3.scinter;

import java.io.IOException;

public class SCConfigService {
    private SandCrawler sandCrawler;
    
    public SCConfigService(SandCrawler sc) {
        sandCrawler = sc;
    }

    public void send(SCConfigMessage msg) {
    	int retry = 5;
    	int trans = sandCrawler.getDevice().bulkTransfer((short)6, msg.data, 500L);
    	while(trans == 0 && retry >= 1) {
    		trans = sandCrawler.getDevice().bulkTransfer((short)6, msg.data, 500L);
    		retry--;
    	}
    	if (trans == 0) {
    		// Transfer failed...
    	}
    	try {
    		Thread.sleep(10);
    	} catch (Exception e) 
    	{ 
    		
    	}
    }
    
    public SCConfigMessage receive() throws IOException, NoSandCrawlerException {
    	byte[] data = new byte[64];
    	int trans = sandCrawler.getDevice().bulkTransfer((short) 133, data, 5000L);
    	if (trans > 0 && data[0] == 0x00) {
    		sandCrawler.stop();
    		throw new IOException();
    	}
    	if (trans > 0)
    		return new SCConfigMessage(data);
    	return null;
    }
    
   

}
