package com.synapsense.p3.scinter;

/**
 * SandCralwer representation of a Jawa
 */

public class SCJawaModel implements Comparable<SCJawaModel> {
   
	public long getJid() {
        return jid;
    }

    public void setJid(long jid) {
        this.jid = jid;
    }

    public int getuPosition() {
        return uPosition;
    }

    public void setuPosition(int uPosition) {
        this.uPosition = uPosition;
    }

    public int getuHeight() {
        return uHeight;
    }

    public void setuHeight(int uHeight) {
        this.uHeight = uHeight;
    }

    public int getRack() {
        return rack;
    }

    public void setRack(int rack) {
        this.rack = rack;
    }

    public int getFeed() {
        return feed;
    }

    public void setFeed(int feed) {
        this.feed = feed;
    }

    public int getPhase() {
        return phase;
    }

    public void setPhase(int phase) {
        this.phase = phase;
    }

    private long jid;
    private int uPosition;
    private int uHeight;
    private int rack;
    private int feed;
    private int phase;

    @Override
	public int compareTo(SCJawaModel rhs) {
    	if (rhs.rack != rack)
    		return rhs.rack - rack;
    	if (rhs.uPosition != uPosition)
    		return rhs.uPosition - uPosition;
    	if (jid > rhs.jid)
    		return 1;
    	if (jid < rhs.jid)
    		return -1;
    	return 0;
	}
    
    public SCJawaModel(long jid, int upos, int uheight, int rack, int feed) {
        this.jid = jid;
        this.uPosition = upos;
        this.uHeight = uheight;
        this.rack = rack;
        this.feed = feed;
    }

    public SCJawaModel(byte[] data) {
        jid = ((data[0] << 24) & 0xff000000L) | ((data[1] << 16) & 0xff0000) | ((data[2] << 8) & 0xff00) | ((data[3]) & 0xff);
        jid = jid & 0xffffffffL;
        uPosition = data[4] & 0xFF;
        uHeight = data[5] & 0xFF;
        rack = ((data[6] >> 6) & 0x3);
        feed = ((data[6] >> 3) & 0x7) + 1;
        phase = data[6] & 0x3;

    }

    public byte[] pack() {
        byte[] data = new byte[7];
        data[0] = (byte) ((jid >> 24) & 0xff);
        data[1] = (byte) ((jid >> 16) & 0xff);
        data[2] = (byte) ((jid >> 8) & 0xff);
        data[3] = (byte) (jid & 0xff);
        data[4] = (byte) uPosition;
        data[5] = (byte) uHeight;
        data[6] = (byte) ((((rack ) & 0x3) << 6) | (((feed - 1) & 0x7) << 3) | (phase & 0x3));
        return data;
    }
    
    public String toString() {
    	return "<SCJawaModel: JID " + Long.toHexString(jid) + " uPos:" + 
    			uPosition + " uHeight: " + uHeight + " rack:" + rack + 
    			" feed:" + feed + " phase:" + phase + ">";
    	
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (jid ^ (jid >>> 32));
		return result;
	}

	/**
	 * Jawas are considered equal if their JID is equal.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SCJawaModel other = (SCJawaModel) obj;
		
		if (jid != other.jid)
			return false;
		
		return true;
	}
}
