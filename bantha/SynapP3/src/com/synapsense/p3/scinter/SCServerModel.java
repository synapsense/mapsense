package com.synapsense.p3.scinter;

import org.apache.log4j.Logger;

public class SCServerModel implements Comparable<SCServerModel> {
	private int uPos;
	private int uHeight;
	private int faceplateWatt;
	private int rack;
	private int feed;
	private int phase;

	private static Logger logger = Logger.getLogger(SCServerModel.class);

	
	public SCServerModel(int uPos, int uHeight, double faceplatekW, int rack, int feed, int phase) {
		this.uPos = uPos;
		this.uHeight = uHeight;
		this.faceplateWatt = (int)(faceplatekW * 1000.0);
		this.rack = rack;
		this.feed = feed;
		this.phase = phase;
	}

	public int compareTo(SCServerModel rhs) {
		if (rhs.getRack() != rack)
			return rack - rhs.getRack();

		if (rhs.getuPos() != uPos)
			return uPos - rhs.getuPos();

		return 0; /* Blind guess */
	}

	public double getFaceplateWatts() {
		return faceplateWatt / 1000.0;
	}

	public void setFaceplateWatts(double faceplateWatt) {
		this.faceplateWatt = (int) (faceplateWatt * 1000.0);
	}

	public int getRack() {
		return rack;
	}

	public void setRack(int rack) {
		this.rack = rack;
	}

	public int getFeed() {
		return feed;
	}

	public void setFeed(int feed) {
		this.feed = feed;
	}

	public int getPhase() {
		return phase;
	}

	public void setPhase(int phase) {
		this.phase = phase;
	}

	public int getuPos() {

		return uPos;
	}

	public void setuPos(int uPos) {
		this.uPos = uPos;
	}

	public int getuHeight() {
		return uHeight;
	}

	public void setuHeight(int x) {
		uHeight = x;
	}

	public SCServerModel(byte[] data) {
		logger.info("Server model faceplate:  " + Integer.toHexString(data[0]) + " " + Integer.toHexString(data[1]));
		faceplateWatt = (data[0] << 8) & 0xff00;
		faceplateWatt |= data[1] & 0xff;
		uPos = data[2] & 0xFF;
		uHeight = data[3] & 0xFF;
		rack = ((data[4] >> 6) & 0x3);
		feed = ((data[4] >> 3) & 0x7) + 1;
		phase = data[4] & 0x3;

	}

	public byte[] pack() {
		byte[] data = new byte[5];
		data[0] = (byte) ((faceplateWatt >> 8) & 0xff);
		data[1] = (byte) faceplateWatt;
		data[2] = (byte) uPos;
		data[3] = (byte) uHeight;
		data[4] = (byte) ((((rack) & 0x3) << 6) | (((feed - 1) & 0x7) << 3) | (phase & 0x3));
		return data;
	}
	
	public String toString() {
		return "<SERVER: Rack: "+ rack + " uPos " + uPos + " height: " + uHeight + " faceplatekw " + faceplateWatt + ">";
	}

}