package com.synapsense.p3.scinter;

import jlibusb.Device;
import jlibusb.NoDeviceException;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.synapsense.p3.scinter.SCAllConfig;
import com.synapsense.p3.scinter.SCJawaModel;
import com.synapsense.p3.scinter.SCServerModel;

public class SandCrawler implements Runnable {
	private static Logger logger = Logger.getLogger(SandCrawler.class);

	private Device sandCrawler;
	private SCDiagService scDiagService;
	private SCConfigService scConfigService;

	private Thread scThread;
	private boolean keepOnRunning;
	private boolean alive;
	private boolean busy;
	private final static int MAX_QUEUE = 100;

	/**
	 * Messages from sandcrawler to computer
	 */
	private BlockingQueue<SCDiagMessage> diagOutQueue = new ArrayBlockingQueue<SCDiagMessage>(MAX_QUEUE);

	public SandCrawler() throws NoSandCrawlerException, UnsatisfiedLinkError, Exception {
		logger.info("SandCrawler connection starting...");
		try {
			sandCrawler = Device.createFromVidPid(0x9999, 0x2222);
		} catch (NoDeviceException e) {
			throw new NoSandCrawlerException();
		} catch (UnsatisfiedLinkError e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		keepOnRunning = true;
		sandCrawler.claimInterface(0);
		sandCrawler.reset();
		scThread = new Thread(this, "SandCrawler");
		scThread.start();

		scDiagService = new SCDiagService(this, diagOutQueue);

		scDiagService.askForVersionPrint();

		scConfigService = new SCConfigService(this);
		alive = true;
		busy = false;
	}

	public JawaData getCurrentJawaData(long jid) throws NoSandCrawlerException, BusySandCrawlerException {
		if (!alive)
			throw new NoSandCrawlerException();

		if (isBusy())
			throw new BusySandCrawlerException();

		return scDiagService.getCurrentJawaData(jid);
	}

	public boolean isBusy() throws NoSandCrawlerException {
		if (!alive)
			throw new NoSandCrawlerException();

		scConfigService.send(new SCConfigMessage(SCConfigMessage.TYPE_BUSYSMOTA));

		while (true) {
			SCConfigMessage msg = null;
			try {
				msg = scConfigService.receive();
			} catch (IOException e) {
				logger.info("IOException from config service.");
				throw new NoSandCrawlerException();
			}
			if (msg == null) {
				throw new NoSandCrawlerException();
			}

			if (msg.type == SCConfigMessage.TYPE_R_COMMAND) {
				System.out.println("command response is " + msg.commandresponse);
				if (msg.commandresponse == SCConfigMessage.TYPE_COMRESP_BUSYSMOTA) {
					busy = true;
					return true;
				} else
					return false;
			}
		}
	}

	public boolean uploadConfiguration(SCAllConfig config) throws NoSandCrawlerException, BusySandCrawlerException {

		if (!alive)
			throw new NoSandCrawlerException();

		if (isBusy())
			throw new BusySandCrawlerException();

		logger.info("Starting upload");
		logger.info(config);

		logger.info("Asking for configuration first");
		SCAllConfig download = this.downloadConfiguration();

		Set<SCJawaModel> downSet = new HashSet<SCJawaModel>(download.getJawas());
		Set<SCJawaModel> upSet = new HashSet<SCJawaModel>(config.getJawas());

		upSet.removeAll(downSet);

		int changed = upSet.size() + downSet.size();

		if (changed >= SCAllConfig.MAX_JAWAS) {
			/*
			 * Build a set which is the intersection of existing jawas and jawas
			 * in the current config
			 */
			/* This will cause a jawa removal phase before new jawas are added */
			logger.info("I have detected " + changed + " jawas. Going to perform a difference operation.");
			upSet.clear();
			upSet.addAll(config.getJawas());
			downSet.retainAll(upSet);
			SCAllConfig removeConfig = new SCAllConfig();
			removeConfig.setJawas(downSet);
			removeConfig.setServers(config.getServers());
			removeConfig.setRacks(config.getRacks());
			doConfigUpload(removeConfig);
		}

		return doConfigUpload(config);
	}

	private boolean doConfigUpload(SCAllConfig config) {
		logger.info("Starting configuration upload");

		scConfigService.send(new SCConfigMessage(SCConfigMessage.TYPE_STARTCONFIG));

		logger.info("Loading Jawas...");
		for (SCJawaModel jawa : config.getJawas()) {
			SCConfigMessage jawaPresent = new SCConfigMessage(SCConfigMessage.TYPE_JAWAPRESENT, jawa);
			scConfigService.send(jawaPresent);
			logger.info("Sent: " + jawaPresent.toString());
			logger.info("Sent: " + jawa.getJid());
			System.out.println("Sent: " + jawa.getJid());
		}

		{
			config.setTs(new SCTimestampModel());
			logger.info("Loading Timestamp...");// + config.getTs().toString());
			SCConfigMessage tsPresent = new SCConfigMessage(SCConfigMessage.TYPE_TIMESTAMPPRESENT, config.getTs());
			scConfigService.send(tsPresent);
			logger.info("Sent: " + tsPresent.ts);
			System.out.println("Sent: " + tsPresent.ts);
		}

		logger.info("Load servers...");

		for (SCServerModel server : config.getServers()) {
			SCConfigMessage serverPresent = new SCConfigMessage(SCConfigMessage.TYPE_SERVERPRESENT, server);
			scConfigService.send(serverPresent);
			logger.info("Sent: " + serverPresent);
			System.out.println("Sent: server" + server.getuPos());
		}

		logger.info("Loading racks...");

		for (SCRackModel rack : config.getRacks()) {
			SCConfigMessage rackPresent = new SCConfigMessage(SCConfigMessage.TYPE_RACKPRESENT, rack);
			scConfigService.send(rackPresent);
			logger.info("Sent: " + rackPresent);
			System.out.println("Sent: Rack" + rack.rack);
		}

		logger.info("Sending commit message.");

		scConfigService.send(new SCConfigMessage(SCConfigMessage.TYPE_STOPCONFIG));
		logger.info("Config upload complete");

		return true;
	}

	public SCAllConfig downloadConfiguration() throws NoSandCrawlerException {
		int retry = 100;
		if (!alive)
			throw new NoSandCrawlerException();

		logger.info("Starting download...");
		scConfigService.send(new SCConfigMessage(SCConfigMessage.TYPE_DL));

		SCAllConfig config = new SCAllConfig();

		while (true) {
			SCConfigMessage msg = null;
			try {
				msg = scConfigService.receive();
			} catch (IOException e) {
				logger.info("IOException from config service.");
				throw new NoSandCrawlerException();
			}
			if (msg == null) {
				logger.warn("No config message yet - SC broke?");
				System.out.println("No config message yet - SC broke?");
				if (retry-- <= 0) {
					this.stop();
					throw new NoSandCrawlerException();
				}
				continue;
			}

			if (msg.type == SCConfigMessage.TYPE_CONFIGDLDONE) {
				logger.info("Config download complete!");
				break;
			} else if (msg.type == SCConfigMessage.TYPE_CONFIGOBJ) {
				logger.info("Got jawa: " + msg.jawa);
				config.addJawa(msg.jawa);
			} else if (msg.type == SCConfigMessage.TYPE_SERVEROBJ) {
				logger.info("Got server: " + msg.server);
				config.addServer(msg.server);
			} else if (msg.type == SCConfigMessage.TYPE_RACKOBJ) {
				logger.info("Got rack: " + msg.server);
				config.addRack(msg.rack);
			} else if (msg.type == SCConfigMessage.TYPE_R_TIMESTAMP) {
				logger.info("Got SC_Timestamp: " + msg.ts);
				config.setTs(msg.ts);
			}

		}
		logger.info(config);
		return config;
	}

	public void clearConfig() throws NoSandCrawlerException, BusySandCrawlerException {
		if (!alive)
			throw new NoSandCrawlerException();

		if (busy)
			throw new BusySandCrawlerException();

		scConfigService.send(new SCConfigMessage(SCConfigMessage.TYPE_CLEARCONFIG));
	}

	public boolean isAlive() {
		return alive;
	}

	public SCDiagMessage getDiagMessage() {
		SCDiagMessage m = null;
		if (diagOutQueue.isEmpty())
			return null;
		try {
			m = diagOutQueue.poll(10, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			return null;
		}
		return m;
	}

	public int getCommandResponse() throws NoSandCrawlerException {
		int retry = 100;
		while (true) {
			SCConfigMessage msg = null;
			try {
				msg = scConfigService.receive();
			} catch (IOException e) {
				logger.info("IOException from config service.");
				throw new NoSandCrawlerException();
			}
			if (msg == null) {
				logger.warn("No Response Yet");
				System.out.println("No Response Yet");
				if (retry-- <= 0) {
					this.stop();
					throw new NoSandCrawlerException();
				}
				continue;
			}
			if (msg.type == SCConfigMessage.TYPE_R_COMMAND) {
				System.out.println("command response is " + msg.commandresponse);
				return msg.commandresponse;
			}
		}
	}

	public SCConfigService getConfigService() {
		return scConfigService;
	}

	public void sendResetCommand(long jid) throws NoSandCrawlerException {
		if (!alive)
			throw new NoSandCrawlerException();

		scConfigService.send(new SCConfigMessage(SCConfigMessage.TYPE_RESETJAWA, jid));
	}

	public void sendIdentifyCommand(long jid) throws NoSandCrawlerException {
		if (!alive)
			throw new NoSandCrawlerException();

		scConfigService.send(new SCConfigMessage(SCConfigMessage.TYPE_STARTBLINK, jid));
	}

	public void sendRabbitLights(int rackId) throws NoSandCrawlerException {
		if (!alive)
			throw new NoSandCrawlerException();

		scConfigService.send(new SCConfigMessage(SCConfigMessage.TYPE_STARTRABBIT, (byte) (rackId))); // Jawa
		                                                                                              // has
		                                                                                              // rack
		                                                                                              // id
		                                                                                              // starting
		                                                                                              // from
		                                                                                              // 0.
	}

	// Only for testing
	public void setSMOTAmode() {
		scConfigService.send(new SCConfigMessage(SCConfigMessage.TYPE_SETSMOTA));
	}

	Device getDevice() {
		return sandCrawler;
	}

	public void stop() {
		keepOnRunning = false;
		alive = false;
		scDiagService.stop();
		sandCrawler.close();
		logger.info("SandCrawler connection stopped.");
	}

	public void run() {
		while (keepOnRunning) {
			try {
				Thread.sleep(1000);

			} catch (InterruptedException e) {
				keepOnRunning = false;
			}
		}
	}

	/*
	 * public static void main(String[] args) { try { SandCrawler sc = new
	 * SandCrawler(); SCAllConfig config = new SCAllConfig();
	 * 
	 * // long jid, int upos, int uheight, int rack, int feed SCJawaModel m =
	 * new SCJawaModel(0x12131212, 2, 2, 1, 1); config.addJawa(m); m = new
	 * SCJawaModel(0x34343434, 1, 2, 1, 1); config.addJawa(m); m = new
	 * SCJawaModel(0x34443434, 1, 2, 1, 1); config.addJawa(m);
	 * 
	 * SCServerModel sm; sm = new SCServerModel(1, 1, 1, 1, 1, 1);
	 * config.addServer(sm); sm = new SCServerModel(2, 2, 1, 1, 1, 1);
	 * config.addServer(sm);
	 * 
	 * SCRackModel rm = new SCRackModel(1,2); config.addRack(rm); rm = new
	 * SCRackModel(0, 45); //config.addRack(rm); rm = new SCRackModel(2, 93);
	 * //config.addRack(rm);
	 * 
	 * sc.uploadConfiguration(config); System.out.println("Config uploaded");
	 * SCAllConfig config2 = sc.downloadConfiguration();
	 * System.out.println("Config downloaded");
	 * System.out.println("***************************************************"
	 * ); System.out.println("Uploaded: "); System.out.println(config);
	 * 
	 * System.out.println("***************************************************");
	 * System.out.println("Downloaded:"); System.out.println(config2);
	 * 
	 * 
	 * 
	 * } catch (Exception e) { e.printStackTrace(); } }
	 */

}
