package com.synapsense.p3.scinter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class SCDiagMessage {
	public final static int TYPE_PRINT = 0x10;
    public final static int TYPE_FORMAT = 0x11;

    public final static int TYPE_JAWADATACURRENT = 0x21;
    public final static int TYPE_JAWADATAVOLTS = 0x22;

    public byte[] data;
    public int type;
  
    private HashMap<String, String> decodeMap = new HashMap<String, String>(){
    	{
	    	//put("c", "i");
	    	//put("C", "i");
	    	//put("x", "i");
	    	//put("X", "i");
	    	//put("d", "i");
	    	//put("i", "i");
	    	//put("o", "i");
	    	put("u", "d");

	    	//put("e", "d");
	    	//put("E", "d");
	    	//put("f", "d");
	    	//put("F", "d");
	    	//put("g", "d");
	    	//put("G", "d");
	    	//put("a", "d");
	    	//put("A", "d");
    	}
    };
    
    public SCDiagMessage(byte[] data) {
        this.data = data;
        type = data[0];
    }

	public JawaData getJawaData(JawaData original) {
		if (type != TYPE_JAWADATACURRENT && type != TYPE_JAWADATAVOLTS)
			return null;

		byte[] jd = new byte[63];
		java.lang.System.arraycopy(data, 1, jd, 0, 63);
		return JawaData.update(original, type, jd);
	}
	
	public long getJidForJawaData() {
		if (type == TYPE_JAWADATACURRENT || type == TYPE_JAWADATAVOLTS) {
			byte[] jd = new byte[63];
			java.lang.System.arraycopy(data, 1, jd, 0, 63);
			return JawaData.getJidFromData(jd);
		}
		return -1;
	}

    public String toString() {
        if (type == TYPE_PRINT) {
            byte[] strarray = new byte[63];
            java.lang.System.arraycopy(data, 1, strarray, 0, 63);
            return "<PRINT1: " + new String(strarray) + ">";
        } else if (type == TYPE_FORMAT) { // TODO: Implement format printing!
        	byte[] strarray = new byte[63];
            java.lang.System.arraycopy(data, 1, strarray, 0, 63);
            String out =  "<PRINT2: " + new String(strarray) + ">";
            StringBuilder sb = new StringBuilder();
            StringBuilder bytes = new StringBuilder();
            
            for (byte b : strarray) {
            	if (b == 0)
            		break;
            		char c = (char)b;
                	sb.append(c);
                	bytes.append(b);
            }
            //return out;//"<PRINT: " + sb.toString() +  ">";
            return getRawFormat(sb.toString(), strarray);
        } else if (type == TYPE_JAWADATACURRENT || type == TYPE_JAWADATAVOLTS) {
			return "<JAWA DATA TYPE " + type + ">";
		}
        return "<Unknown Data Type: " + type + ">";
    }
    
    private String getRawFormat(String str, byte[] data){
    	String msg = str;
    	try{
            StringBuilder format = new StringBuilder();
            
            int pos = 0;
            int	maxpos = str.length();
            int num_format = 0;
            
            boolean pfmt = false;
            
            while(pos < maxpos){
                String c = str.substring(pos, pos+1);
                if(pfmt){
                	if(decodeMap.containsKey(c))
                		c = decodeMap.get(c);
    				pfmt = false;
                }
            	format.append(c);
    			
                if(c.equals("%")){
                    pfmt = true;
                    num_format++;
                }
                pos += 1;
            }
            
            //System.out.println("original format " + str);
            //System.out.println("change format " + format.toString());
            
            int data_index = 0;
            for(int i=0;i<data.length;i++){
            	if(data[i]==0){
            		data_index = i + 1;
            		break;
            	}
            }
            
        	/*int i=0;
        	for (byte b : data) {
        		 System.out.println(i + ": " +b);
        		 i++;
        	 }*/
        	
        	
        	if(num_format>0){
        		Object[] values = new Object[num_format];
            	pos = data_index;
            	for(int j=0;j<num_format;j++){
            		long value;
            		value = ((data[pos++] << 24) & 0xff000000L) | 
            		((data[pos++] << 16) & 0xff0000) | 
            		((data[pos++] << 8) & 0xff00) | 
            		((data[pos++]) & 0xff);
            		values[j] = value;
            	}
            	String formatStr = format.toString();
            	
            	if(values.length>0)
            		msg = convertFormat(formatStr, values);
        	}
    	}catch(Exception e){
    		e.printStackTrace();
    	}	
    	
    	return msg;
       
    }
    
    private String convertFormat(String formatStr, Object[] args){
    	String result = "";
    	
    	Class c = String.class;
    	Class[] parameterTypes = new Class[]{String.class, Object[].class};
    	Method formatMethod;
    	Object[] arguments = new Object[]{formatStr, args};
    	
    	try {
			formatMethod = c.getDeclaredMethod("format", parameterTypes);
			result = (String) formatMethod.invoke(c.newInstance(), arguments);
			//System.out.println(result);
    	} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return result;
    	
    }

}
