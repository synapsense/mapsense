package com.synapsense.p3.scinter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Collections;


public class SCAllConfig {
	private List<SCServerModel> servers = new ArrayList<SCServerModel>();
    private List<SCJawaModel> jawas = new ArrayList<SCJawaModel>();
	private List<SCRackModel> racks = new ArrayList<SCRackModel>();
	private SCTimestampModel ts = new SCTimestampModel(0);

	public static final int MAX_JAWAS = 252;
	public static final int MAX_SERVERS = 255;
	public static final int MAX_RACKS = 3;
	
	public boolean addRack(SCRackModel r) {
		if (racks.contains(r) == true) {
			racks.remove(r);
		}
		
		if (racks.size() >= MAX_RACKS)
			return false;
		
		racks.add(r);
		return true;
	}
	
	void setRacks(Collection<SCRackModel> r) {
		racks = new ArrayList<SCRackModel>(r);
	}
	
	void setServers(Collection<SCServerModel> s) {
		servers = new ArrayList<SCServerModel>(s);
	}
	
	void setJawas(Collection<SCJawaModel> j) {
		jawas = new ArrayList<SCJawaModel>(j);
	}

	public List<SCRackModel> getRacks() {
		Collections.sort(racks);
		return Collections.unmodifiableList(racks);
	}

	public boolean addServer(SCServerModel m) {
		if (servers.size() >= MAX_SERVERS)
			return false;
		servers.add(m);
		return false;
	}

	public List<SCServerModel> getServers() {
		Collections.sort(servers);
		return Collections.unmodifiableList(servers);
	}

    public boolean addJawa(SCJawaModel m) {
		if (jawas.size() >= MAX_JAWAS)
			return false;
        jawas.add(m);
		return true;
    }

    public List<SCJawaModel> getJawas() {
    	Collections.sort(jawas);
        return Collections.unmodifiableList(jawas);
    }
    
 
    public boolean setTs(SCTimestampModel t) {
    	this.ts = t;
		return true;
    }    
    
    
    public SCTimestampModel getTs() {
        return this.ts;
    }

       
    public String toString() { /* Inefficient, for debugging only */
    	String out = "JAWAS:\n";
    	
    	for (SCJawaModel jawa : getJawas()) {
    		out += jawa + "\n";
		}
    	out += "SERVERS: \n";
		for (SCServerModel jawa : getServers()) {
			out += jawa + "\n";
		}
		out += "RACKS: \n";
		for (SCRackModel rack : getRacks()) {
			out += rack + "\n";
		}
		out+= "TS: "+ts.toString() + "\n";
		return out;
    }

    public boolean isEmpty(){
    	return (racks.size()==0 && servers.size() == 0 && jawas.size()==0);
    }

}
