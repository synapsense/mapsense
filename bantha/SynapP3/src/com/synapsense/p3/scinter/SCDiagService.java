package com.synapsense.p3.scinter;

import java.util.concurrent.BlockingQueue;
import java.util.Map;
import java.util.HashMap;

import org.apache.log4j.Logger;

public class SCDiagService implements Runnable {
	private static Logger logger = Logger.getLogger(SCDiagService.class);

	private SandCrawler sandCrawler;
	private Thread scThread;
	private boolean keepOnRunning;
	private BlockingQueue<SCDiagMessage> queue;
	
	private Map<Long, JawaData> currentJawaData = new HashMap<Long, JawaData>();

	public SCDiagService(SandCrawler sc, BlockingQueue<SCDiagMessage> queue) {
		sandCrawler = sc;
		keepOnRunning = true;
		this.queue = queue;
		scThread = new Thread(this, "SandCrawler-Diag");
		scThread.start();
	}

	public JawaData getCurrentJawaData(long jid) {
		return currentJawaData.get(jid);
	}

	private SCDiagMessage makeMessage(byte[] data) {
		return new SCDiagMessage(data);
	}
	
	public void stop() {
		keepOnRunning = false;
	}

	private void addOrReplaceJawaData(JawaData jd) {
		logger.info("Insert JawaData for JID " + Long.toHexString(jd.jid) + " data: " + jd);
		currentJawaData.put(jd.jid, jd);

	}
	
	public void askForVersionPrint() {
		byte[] out = new byte[1];
		out[0] = 0x20;
		sandCrawler.getDevice().bulkTransfer((short) 2, out, 12000L);
	}
	
	public void run() {
		int failCount = 10;
		while (keepOnRunning) {
			byte[] data = new byte[64];
			int trans = sandCrawler.getDevice().bulkTransfer((short) 129, data,
					12000L);
			if (trans > 0) {
				
				if (data[0] == 0) {
					failCount--;
					if (failCount <= 0) {
						logger.info("Read a 0 from the diag service, assuming device detached");
						sandCrawler.stop();
						return;
					}
				}
					
					
				SCDiagMessage msg = makeMessage(data);
				//queue.offer(msg);
				logger.info("Diag message: " + msg.toString());

				long jdjid = msg.getJidForJawaData();
				if (jdjid > 0) {
					logger.info("JID Lookup: " + Long.toHexString(jdjid));
					JawaData old = currentJawaData.get(jdjid);
					logger.info("Previous JawaData: " + old);
					JawaData jd = msg.getJawaData(old);
					if (jd != null) {
						addOrReplaceJawaData(jd);
						
					}
				}
			}

		}
	}

}
