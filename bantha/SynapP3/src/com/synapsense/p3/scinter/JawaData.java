package com.synapsense.p3.scinter;

import org.apache.log4j.Logger;

import com.synapsense.p3.utilities.IDFormat;

public class JawaData implements Cloneable {
	public long jid;
	public double kwDemand;
	public double avgCurrent;
	public double maxCurrent;
	public double volts;
	public int phase;
	public boolean delta;
	public long timestamp;
	private static Logger logger = Logger.getLogger(JawaData.class);


	public static long getJidFromData(byte[] data) {
		int i = 0;
		long jid = ((data[i++] << 24) & 0xff000000L) | 
		((data[i++] << 16) & 0xff0000) | 
		((data[i++] << 8) & 0xff00) | 
		((data[i++]) & 0xff);
		jid = jid & 0xffffffffL;
		return jid;
	}

	public static JawaData update(JawaData injawaData, int type, byte[] data) {
		logger.info("Running JawaData.update()");
		JawaData jawaData = null;
		if (injawaData == null) {
			jawaData = new JawaData();
			logger.info("Created new JawaData - no data yet");
		} else {
			try {
				jawaData = (JawaData)injawaData.clone();
				logger.info("Cloned JawaData");
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		}
		/* Deserialize version */
		int i = 0;
		jawaData.jid = ((data[i++] << 24) & 0xff000000L) | 
		((data[i++] << 16) & 0xff0000) | 
		((data[i++] << 8) & 0xff00) | 
		((data[i++]) & 0xff);
		jawaData.jid = jawaData.jid & 0xffffffffL;

		if (type == SCDiagMessage.TYPE_JAWADATAVOLTS) {
			logger.info("Deserializing volts packet");
			jawaData.volts = ((data[i++] << 8) & 0xff00) | ( data[i++] & 0xff);
			jawaData.volts /= 10.0;
			jawaData.kwDemand = ((data[i++] << 8) & 0xff00) | ( data[i++] & 0xff);
			jawaData.kwDemand /= 1000.0; /* Wh -> kWh */
			jawaData.kwDemand *= 7.5; /* Convert to 15min demand */

		} else if (type == SCDiagMessage.TYPE_JAWADATACURRENT) {
			logger.info("Deserializing current packet");
			jawaData.avgCurrent = ((data[i++] << 8) & 0xff00) | ( data[i++] & 0xff);
			jawaData.avgCurrent /= 10.0;
			jawaData.maxCurrent = ((data[i++] << 8) & 0xff00) | ( data[i++] & 0xff);
			jawaData.maxCurrent /= 10.0;
		}
		jawaData.phase = (int)(data[i++]);
		
		if (jawaData.phase > 3) {
			jawaData.delta = true;
			jawaData.phase -= 3;
		}
			
		jawaData.timestamp = System.currentTimeMillis();
		logger.info("Current state of JawaData is : " + jawaData);
		
		return jawaData;
	}

	public JawaData(){
		jid = 11111111;
		kwDemand = 0.0;
		avgCurrent = 0.0;
		maxCurrent = 0.0;
		timestamp = System.currentTimeMillis();
	}

	public String toString() {
		return "<JawaData: JID " + IDFormat.codeToHr(jid) + "[" + Long.toHexString(jid) +"]" + " kwD " + kwDemand + " avgCurrent " + avgCurrent + " maxCurrent " + maxCurrent + " volts " + volts + " timestamp " + timestamp + ">";
	}
}