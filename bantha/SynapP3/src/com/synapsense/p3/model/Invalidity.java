package com.synapsense.p3.model;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.synapsense.p3.utilities.LogManager;
import com.synapsense.p3.utilities.Variables;

public abstract class Invalidity {
	private static Logger logger = Logger.getLogger(Invalidity.class.getName());
	
	ArrayList<Integer> invalidities = new ArrayList<Integer>();
	
	public void addInvalidity(int invalidity){
		
		try{
			if(!invalidities.contains(invalidity))
				invalidities.add(invalidity);
			logger.info("Add invalidity[id:" + Integer.toString(invalidity) + " ;detail:" + Variables.configurationErrors.get(invalidity) + "]");
		}catch(Exception e){
			logger.error("Fail to add invalidity");
			logger.debug(LogManager.getStackTrace(e));
		}
	}
	
	public void removeInvalidity(int invalidity){
		try{
			for(int i = 0; i<invalidities.size();i++){
				if(invalidities.get(i)==invalidity)
					invalidities.remove(i);
			}
			
			logger.info("remove invalidity[id:" + Integer.toString(invalidity) + " ;detail:" + Variables.configurationErrors.get(invalidity) + "]");
			
		}catch(Exception e){
			logger.error("Fail to remove invalidity");
			logger.debug(LogManager.getStackTrace(e));
			
		}
		
	}
	
	public ArrayList<Integer> getInvalidities(){
		return invalidities;
	}
		
}
