package com.synapsense.p3.model;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.synapsense.p3.exceptions.JawaCreationFailedException;
import com.synapsense.p3.utilities.LogManager;
import com.synapsense.p3.utilities.Validator;
import com.synapsense.p3.utilities.Variables;


public class Jawa extends Invalidity{
	private static Logger logger = Logger.getLogger(Server.class.getName());
	
	private int index;
	private String id;
	private int feed;
	private Server server;
	private int phase =0;
	
	public Jawa(String _id, int _feed, Server _server) throws JawaCreationFailedException{
		if(!Validator.checkMaxPlugmeter(_server)){
			logger.error("Fail to create new jawa");
			logger.error("jawa info: id" + _id + " feed:" + _feed );
			throw new JawaCreationFailedException();
		}
		
		id = _id;
		feed = _feed;
		server = _server;
		index = server.getMaxJawaIndex();

		if(!Validator.checkJawaId(this))
			addInvalidity(Variables.CONFIGURATION_ERROR_P3_ID);
		
		if(!Validator.checkJawaFeed(this))
			addInvalidity(Variables.CONFIGURATION_ERROR_P3_FEED);

		logger.info("Create new " + this.toString());
	}
	
	public Jawa(String _id, int _feed, int _phase, Server _server) throws JawaCreationFailedException{
		if(!Validator.checkMaxPlugmeter(_server)){
			logger.error("Fail to create new jawa");
			logger.error("jawa info: id" + _id + " feed:" + _feed );
			throw new JawaCreationFailedException();
		}
		
		id = _id;
		feed = _feed;
		phase = _phase;
		server = _server;
		index = server.getMaxJawaIndex();

		if(!Validator.checkJawaId(this))
			addInvalidity(Variables.CONFIGURATION_ERROR_P3_ID);
		
		if(!Validator.checkJawaFeed(this))
			addInvalidity(Variables.CONFIGURATION_ERROR_P3_FEED);

		logger.info("Create new " + this.toString());
	}
	
	public Jawa(Server _server){
		server = _server;
		index = server.getMaxJawaIndex();
	}
	
	public String getId(){
		return id;
	}
	
	public void setId(String _id){
		try{
			id = _id;
			
			if(Validator.checkJawaId(this)){
				removeInvalidity(Variables.CONFIGURATION_ERROR_P3_ID);
			}else{
				addInvalidity(Variables.CONFIGURATION_ERROR_P3_ID);
			}
			
			logger.info("set id to " + _id + " " + this.toString());
		}catch(Exception e){
			logger.error("Cannot set id " + _id + "to " + this.toString());
			logger.debug(LogManager.getStackTrace(e));
		}
	}
	
	public int getFeed(){
		return feed;
	}
	
	public void setFeed(int _feed){
		try{
			feed = _feed;
			
			//int newIndex = server.getJawaIndex(this);
			//setIndex(newIndex);
			
			if(Validator.checkJawaFeed(this)){
				removeInvalidity(Variables.CONFIGURATION_ERROR_P3_FEED);
			}else{
				addInvalidity(Variables.CONFIGURATION_ERROR_P3_FEED);
			}
			logger.info("set RPDU to " + _feed + " " + this.toString());
			
		}catch(Exception e){
			logger.error("Cannot set RPDU " + _feed + "to " + this.toString());
			logger.debug(LogManager.getStackTrace(e));
		}
	}
	
	public int getPhase(){
		return phase;
	}
	
	public void setPhase(int _phase){
		try{
			phase = _phase;
			logger.info("set phase to " + _phase + " " + this.toString());
			
		}catch(Exception e){
			logger.error("Cannot set phase " + _phase + "to " + this.toString());
			logger.debug(LogManager.getStackTrace(e));
		}
	}
	
	public Server getServer(){
		return server;
	}
	
	public void setServer(Server _server){
		server = _server;
	}
	
	public void setIndex(int _index){
		index = _index;
	}
	
	public int getIndex(){
		return index;
	}
	
	public boolean equals(Jawa compareJawa){
		if(this.getId().equalsIgnoreCase(compareJawa.getId()) && this.getFeed()==compareJawa.getFeed() && this.getPhase()==compareJawa.getPhase()){
			return true;
		}else{
			return false;
		}
	}
	
	public String toString(){
		String info = "Jawa[Id:" + id + ";RPDU:" + Integer.toString(feed) + ";Phase:"  + Integer.toString(phase) + "]";
		return info;
	}
}
