package com.synapsense.p3.model;

public class ConfigurationError {
	// a object which has a error
	private Object object;
	// error
	private int invalidity;
	// description
	private String description = "";
	
	/**
	 * Constructor with object, invalidity
	 * @param object
	 * @param invalidity
	 */
	public ConfigurationError(Object object,int invalidity ){
		this.object=object;
		this.invalidity=invalidity;
	}
	
	public ConfigurationError(Object object,int invalidity, String description ){
		this.object=object;
		this.invalidity=invalidity;
		this.description = description;
	}

	
	/**
	 * get invalidity number, invalidity is defined in Variables class
	 * @return
	 */
	public int getInvalidity(){
		return invalidity;
	}
	
	/**
	 * get a object has a error
	 * @return
	 */
	public Object getObject(){
		return object;
	}
	
	public void setDescription(String description){
		this.description = description;
	}
	
	public String getDescription(){
		return description;
	}
}
