package com.synapsense.p3.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;

import com.synapsense.p3.exceptions.JawaCreationFailedException;
import com.synapsense.p3.exceptions.ServerCreationFailedException;
import com.synapsense.p3.model.Rack;

import com.synapsense.p3.scinter.SCAllConfig;
import com.synapsense.p3.scinter.SCJawaModel;
import com.synapsense.p3.scinter.SCRackModel;
import com.synapsense.p3.scinter.SCServerModel;
import com.synapsense.p3.utilities.IDFormat;
import com.synapsense.p3.utilities.InvalidChecksum;
import com.synapsense.p3.utilities.LogManager;
import com.synapsense.p3.utilities.Variables;

public class Configuration {
	private static Logger logger = Logger.getLogger(Configuration.class.getName());
	// Three racks in a configuration : Left, center and right
	Rack leftRack = new Rack(Variables.LEFT_RACK_TYPE);
	Rack centerRack = new Rack(Variables.CENTER_RACK_TYPE);
	Rack rightRack = new Rack(Variables.RIGHT_RACK_TYPE);
	
	public Configuration(){
	}
	
	/**
	 * Create new configuration and copy all rack properties
	 * @param Rack from 
	 */
	public Configuration(Configuration from){
		for(int i=Variables.LEFT_RACK_TYPE;i<=Variables.RIGHT_RACK_TYPE;i++){
			Rack rack = this.getRackByType(i);
			Rack fromRack = from.getRackByType(i);
			rack.setUheight(fromRack.getUheight());
			
			for(Server fromServer:fromRack.getServers()){
				try {
					Server server = new Server(fromServer.getLocation(),fromServer.getUheight(), fromServer.getPower(), rack);
					rack.addServer(server);
					
					for(Jawa fromJawa:fromServer.getJawas()){
						Jawa jawa = new Jawa(fromJawa.getId(), fromJawa.getFeed(), fromJawa.getPhase(), server);
						server.addJawa(jawa);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		
	}
	
	/**
	 * Constructor by SCAllConfig object (Parsing)
	 * @param scAllConfig
	 * @throws Exception
	 */
	public Configuration(SCAllConfig scAllConfig) throws Exception{
		
		logger.info("convert from SCAllConfig object to Configuration object");
		try{
			
			
			for(SCRackModel SCRack:scAllConfig.getRacks()){
				//System.out.println("parsed rack:" + getRackLog(SCRack));
				// Convert SCRack object to Rack object
				// type, height properties should be set
				int type = SCRack.rack;
				int height = SCRack.height;
				boolean isDelta = SCRack.isDelta();
				
				// if rack type is not defined, throw exception, invalid rack info
				if(type<Variables.LEFT_RACK_TYPE || type>Variables.RIGHT_RACK_TYPE){
					throw new Exception("invalid rack type " + type);
				}
				
				Rack rack = this.getRackByType(type);
				rack.setUheight(height);
				rack.setDelta(isDelta);
				
			}
			
			for(SCServerModel SCServer:scAllConfig.getServers()){
				//System.out.println("parsed server:" + getServerLog(SCServer));
				// Covert SCServerModel Object to Server Object
				// location, uheight, power value should be set
				int location = SCServer.getuPos();
				int uheight = SCServer.getuHeight();
				double power = SCServer.getFaceplateWatts();
				
				int rackType = SCServer.getRack();
				Rack rack = this.getRackByType(rackType);
				
				// if server location is wrong, server object will not be created.
				try{
					Server server = new Server(location, uheight, power, rack);
					rack.addServer(server);
					
				}catch(ServerCreationFailedException e){
					logger.error("Fail to convert from SCAllConfig object to Configuration object because of ServerCreationFailedException");
				}

			}
			
			for(SCJawaModel SCJawa:scAllConfig.getJawas()){
				// Convert SCJawaModel object to Jawa object
				// id, RPDU(feed) and phase should be set
				// Find parent server with SCJawaModel object's location and height
				//System.out.println("parsed jawa:" + getJawaLog(SCJawa));
			
				int ulocation = SCJawa.getuPosition();
				int uheight = SCJawa.getuHeight();
				
				int rackType = SCJawa.getRack();
				Rack rack = this.getRackByType(rackType);
				Server server = rack.getServer(ulocation, uheight);
				if(server == null){
					server = new Server(ulocation, uheight, rack);
					rack.addServer(server);
					//System.out.println("parsed server:" + server.toString());
				}
				
				long jid = SCJawa.getJid();
				int feed = SCJawa.getFeed();
				int phase = SCJawa.getPhase();
				String str_jid = IDFormat.codeToHr(jid);
				
				// set phase, Get phase info by combining wye or delta info in parent rack and phase number in SCJawaModel Object
				if(phase>0){
					int convertedPhase = 0;
					for(int i :Variables.phaseAssociations.keySet()){
						PhaseAssociation pa = Variables.phaseAssociations.get(i);
						if(pa.getPhase()==phase && pa.getDelta()==rack.isDelta()){
							convertedPhase = i;
						}
					}
					
					phase = convertedPhase;
					
				}
				// throw creating Faile error if any exception happens during creation
				try{
					Jawa jawa = new Jawa(str_jid, feed, phase, server);
					server.addJawa(jawa);
					//set rpdu and phase association in parent rack
					if(phase>0)
						rack.setRpduPhaseAssociation(feed, jawa);
				}catch(JawaCreationFailedException e){
					logger.error("Fail to convert from SCAllConfig object to Configuration object because of JawaCreationFailedException" );
				}
			}
			
			
		}catch(Exception e){
			logger.error("Fail to convert from SCAllConfig object to Configuration object" );
			logger.debug(LogManager.getStackTrace(e));
			throw e;
		}
	}
	
	/**
	 * Debug purpose : get SCJawaModel object detailed info
	 * @param jawa
	 * @return
	 */
	private String getJawaLog(SCJawaModel jawa){
		return "ID: " + Long.toString(jawa.getJid()) + "phase: " + Integer.toString(jawa.getPhase())+ " Feed:" + Integer.toString(jawa.getFeed()) +  " Rack: " + jawa.getRack()+ " Ulocation: " + jawa.getuPosition() + " UHeight:" + jawa.getuHeight();
		
	}
	
	/**
	 * Debug purpose : get SCServerModel object detailed info
	 * @param server
	 * @return
	 */
	private String getServerLog(SCServerModel server){
		return "Ulocation: " + Integer.toString(server.getuPos()) + " Height:" + Integer.toString(server.getuHeight()) +  " FacePlate: " + server.getFaceplateWatts();
		
	}
	
	/**
	 * Debug purpose : get SCRackModel object detailed info
	 * @param rack
	 * @return
	 */
	private String getRackLog(SCRackModel rack){
		return "Type: "+ rack.rack + " Height:" + rack.height + " deltaOrWye: " +rack.deltawye;
	}
	
	/**
	 * get all racks 
	 * @return array of Racks
	 */
	public Rack[] getRacks(){
		Rack[] racks = {leftRack, centerRack, rightRack};
		return  racks;
	}
	
	/**
	 * get rack by type
	 * @param type
	 * @return
	 */
	public Rack getRackByType(int type){
		/*for(Rack rack:racks){
			if(rack.getType()==type)
				return rack;
		}
		
		return null;*/
		if(type==Variables.LEFT_RACK_TYPE)
			return leftRack;
		else if(type==Variables.CENTER_RACK_TYPE)
			return centerRack;
		else if(type == Variables.RIGHT_RACK_TYPE)
			return rightRack;
		else
			return null;
	}
	
	/**
	 * Parse this configuration object to SCAllConfig object
	 * @return
	 */
	public SCAllConfig parseToSCAllConfig(){
		SCAllConfig scAllConfig = new SCAllConfig();
		 
		logger.info("convert from Configuration object to SCAllConfig object");
		for(Rack rack :this.getRacks()){
			// Rack Object => SCRackModel Object
			int rack_type = rack.getType();
			int rack_height = rack.getUheight();
			SCRackModel rackModel = new SCRackModel(rack_type,rack_height);
		
			boolean isDelta =false;
			
			// get Wye or delta phase info from children Jawas
			if(rack.getRpduPhaseMap().size()>0){
				Jawa[] jawas = rack.getRpduPhaseMap().values().toArray(new Jawa[rack.getRpduPhaseMap().size()]);
				PhaseAssociation pa = Variables.phaseAssociations.get(jawas[0].getPhase());
				isDelta = pa.getDelta();
			}
			
			System.out.println("rack" + rack_type + " isdelta:" + isDelta );
			
			// set isDelta property
			if(isDelta)
				rackModel.setAllDelta();

			//System.out.println("deltawye info after setting: " + rackModel.isDelta() + " for rack " + rackModel.rack);

			scAllConfig.addRack(rackModel);
			
			// Server object => SCServerModel object
			for(Server server:rack.getServers()){
				int server_uposition = server.getLocation();
				int server_uheight = server.getUheight();
				double server_power = server.getPower();
				
				if(server.getJawas().size()==0){
					SCServerModel serverModel = new SCServerModel(server_uposition,server_uheight, server_power, rack_type, 0, 0);
					scAllConfig.addServer(serverModel);
				}
				
				// Jawa object => SCJawaModel object
				for(Jawa jawa:server.getJawasByIdValidation(true)){
					String jawa_id = jawa.getId();
					int jawa_feed = jawa.getFeed();
					int jawa_phase = jawa.getPhase();
					//long jawa_long_id = Long.parseLong(jawa_id.trim());
					long jawa_long_id;
					try {
						jawa_long_id = IDFormat.hrToCode(jawa_id.trim());
						SCJawaModel jawaModel = new SCJawaModel(jawa_long_id,server_uposition,server_uheight,rack_type, jawa_feed);
						
						// need to real phase info from combination value of wye, delta and phase 
						if(jawa_phase>0){
							PhaseAssociation phaseAssociation = Variables.phaseAssociations.get(jawa_phase);
							int phase = phaseAssociation.getPhase();
							jawaModel.setPhase(phase);
							
						}
						
						scAllConfig.addJawa(jawaModel);
					} catch (InvalidChecksum e) {
						// TODO Auto-generated catch block
						logger.error("Invalid Checksum " + jawa_id);
						logger.debug(LogManager.getStackTrace(e));
					}
				}
			}
		}
		
		return scAllConfig;
	}
	
	/**
	 * get all jawas from all racks
	 * @return
	 */
	public ArrayList<Jawa> getJawas(){
		ArrayList<Jawa> jawas = new ArrayList();
		
		for(Rack rack:this.getRacks()){
			for(Server server:rack.getServers()){
				for(Jawa jawa:server.getJawas()){
					jawas.add(jawa);
				}
			}
		}
		
		return jawas;
	}
	
	/**
	 * Get all jawas from a rack
	 * @param type
	 * @return
	 */
	public ArrayList<Jawa> getJawasByRack(int type){
		ArrayList<Jawa> jawas = new ArrayList<Jawa>();
		
		Rack rack = this.getRackByType(type);
		for(Server server:rack.getServers()){
			for(Jawa jawa:server.getJawas()){
				jawas.add(jawa);
			}
		}
		
		return jawas;
	}
	
	/**
	 * set Rack with type number and rack object
	 * @param type
	 * @param rack
	 */
	public void setRack(int type, Rack rack){
		if(type==Variables.LEFT_RACK_TYPE)
			leftRack = rack;
		else if(type==Variables.CENTER_RACK_TYPE)
			centerRack = rack;
		else
			rightRack = rack;
	}

	/**
	 * Query a jawa from this whole configuration by id
	 * @param id
	 * @return
	 */
	public Jawa getJawaById(String id){
		
		for(Jawa jawa:getJawas()){
			if(jawa.getId().equals(id))
				return jawa;
		}
		return null;
	}
	
	
	/*public Jawa getPhaseJawa(){
		for(Jawa jawa:getJawas()){
			if(jawa.getPhase()>0)
				return jawa;
		}
		
		return null;
	}*/

	
	/**
	 * Clone configuration for test purpose
	 */
	public void cloneFrom(Configuration config){
		
		for(Rack rack: getRacks()){
			// copy rack properties
			Rack fromRack = config.getRackByType(rack.getType());
			rack.setUheight(fromRack.getUheight());
			
			// copy servers
			rack.clearServers();
			if(fromRack.getServers().size()>0){
				for(Server server:config.getRackByType(rack.getType()).getServers()){
					rack.addServer(server);
				}
			}
		}
		
	}
	
	/**
	 * Compare a configuration (Compare all children objects)
	 * @param config
	 * @return
	 */
	public boolean equals(Configuration config){
		for(int i=Variables.LEFT_RACK_TYPE;i<=Variables.RIGHT_RACK_TYPE;i++){
			Rack rack1 = this.getRackByType(i);
			Rack rack2 = config.getRackByType(i);
			
			if(!rack1.equals(rack2, true))
				return false;
		}
		
		return true;
	}
}
