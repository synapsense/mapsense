package com.synapsense.p3.model;

public class PhaseAssociation {
	private int phase;
	private boolean isDelta;
	private String name;
	
	public PhaseAssociation(int phase, boolean isDelta, String name){
		this.phase = phase;
		this.isDelta = isDelta;
		this.name = name;
	}
	
	public void setPhase(int phase){
		this.phase = phase;
	}
	
	public void setDelta(boolean isDelta){
		this.isDelta = isDelta;
	}

	public void setName(String name){
		this.name = name;
	}
	
	public int getPhase(){
		return phase;
	}
	
	public boolean getDelta(){
		return isDelta;
	}
	
	public String getName(){
		return name;
	}

}
