package com.synapsense.p3.model;

import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

import com.synapsense.p3.utilities.LogManager;
import com.synapsense.p3.utilities.Variables;

public class Rack {
	private static Logger logger = Logger.getLogger(Rack.class.getName());
	// 0: Left, 1: Center, 2:Right
	private int type;
	// The list of child servers
	private ArrayList<Server> servers;
	// Rack's u height
	private int uheight = Variables.RACK_DEFAULT_HEIGHT;
	// each rpdu should have one jawa phase association
	private HashMap<Integer, Jawa> rpduPhaseAssociation = new HashMap<Integer, Jawa>();
	// isDelta (true: delta phase, false: wye)
	private boolean isDelta;
	
	public Rack(int _type){
		try{
			this.type = _type;
			servers = new ArrayList<Server>();
			logger.info("Create new rack " + Integer.toString(type));
		}catch(Exception e){
			logger.error("Fail to create new rack");
			logger.debug(LogManager.getStackTrace(e));
		}
	}
	
	public void setType(int _type){
		this.type = _type;
	}
	
	public int getType(){
		return this.type;
	}
	
	public void setDelta(boolean isDelta){
		this.isDelta = isDelta;
	}
	
	public boolean isDelta(){
		return isDelta;
	}
	
	/**
	 * Add a child server
	 * @param _server
	 */
	public void addServer(Server _server){
		try{
			servers.add(_server);
			addRpduPhaseAssociations(_server);
			_server.setRack(this);
			
			logger.info("Add " + _server.toString() + " into " + this.toString());
		}catch(Exception e){
			logger.debug(LogManager.getStackTrace(e));
		}
	}
	
	/**
	 * get list of servers belong to this rack
	 * @return arraylist of child servers
	 */
	public ArrayList<Server> getServers(){
		return servers;
	}
	
	/**
	 * delete server from this rack
	 * and needs to update rpdu & phase association, too
	 * @param _server
	 */
	public void deleteServer(Server _server){
		try{
			removeRpduPhaseAssociations(_server);
			String serverinfo = _server.toString();
			servers.remove(_server);
			logger.info("Delete " + serverinfo + " from " + this.toString());
			
		}catch(Exception e){
			logger.debug(LogManager.getStackTrace(e));
		}
	}
	
	/**
	 * delete all servers from server list'
	 * 
	 */
	public void clearServers(){
		servers.clear();
		
	}
	
	/**
	 * To retrieve max ulocation and return it
	 * @return
	 */
	public int getNextULocation(){
		int max = 1;
		
		for(int i=0;i<servers.size();i++){
			int topLocation = servers.get(i).getLocation() + servers.get(i).getUheight();
			if(max < topLocation)
				max = topLocation;
		}
		
		return max;
	}

	/**
	 * set uheight
	 * @param _uheight
	 */
	public void setUheight(int _uheight){
		this.uheight = _uheight;
	}
	
	/**
	 * get uheight
	 * @return uheight
	 */
	public int getUheight(){
		return uheight;
	}
	
	/**
	 * get a child server by ulocation and uheight
	 * @param ulocation
	 * @param uheight
	 * @return
	 */
	public Server getServer(int ulocation, int uheight){
		for(Server server:servers){
			if((server.getLocation()==ulocation) && (server.getUheight() == uheight) ){
				return server;
			}
		}
		
		return null;
	}
	
	/**
	 * get a child server by only ulocation
	 * @param ulocation
	 * @return server
	 */
	public Server getServer(int ulocation){
		for(Server server:servers){
			if((server.getLocation()==ulocation)){
				return server;
			}
		}
		return null;
	}
	
	/**
	 * compare param rack is same one or not 
	 * (if isChildrenChecked is true, it will go through all children)
	 * @param rack
	 * @param isChildrenChecked
	 * @return
	 */
	public boolean equals(Rack rack, boolean isChildrenChecked){
		if(this.type != rack.getType())
			return false;
		
		if(this.uheight!=rack.getUheight())
			return false;
		
		if(!isChildrenChecked)
			return true;
		
		if(this.getServers().size()!=rack.getServers().size())
			return false;
		
		
		for(Server server1:this.getServers()){
			Server server2 = rack.getServer(server1.getLocation());
			if(server2==null)
				return false;
			if(!server1.equals(server2))
				return false;
		}
		return true;
	}
	
	/**
	 * print rack information for debugging
	 */
	public String toString(){
		String info = "Rack[type; " + Integer.toString(type) + ";uheight:" + Integer.toString(uheight) + "]";
		return info;
	}
	
	/**
	 * update rpdu and phase association hashmap
	 * @param rpdu
	 * @param jawa
	 */
	public void setRpduPhaseAssociation(int rpdu, Jawa jawa ){
		if(jawa.getPhase()==0)
			rpduPhaseAssociation.remove(rpdu);
		else
			rpduPhaseAssociation.put(rpdu, jawa);
	}
	
	/**
	 * get jawa which associate ref phase with specific rpdu
	 * @param rpdu
	 * @return
	 */
	public Jawa getRpduPhaseAssociation(int rpdu){
		return rpduPhaseAssociation.get(rpdu);
	}
	
	/**
	 * get rpdu and phase association map 
	 * @return hashMap<rpdu#, Jawa>
	 */
	public HashMap<Integer, Jawa> getRpduPhaseMap(){
		return rpduPhaseAssociation;
	}
	
	/**
	 * check phase association for a server and delete it
	 * @param _server
	 */
	public void removeRpduPhaseAssociations(Object target){
		if(target instanceof Server){
			Server server = (Server)target;
			for(Jawa j:server.getJawas()){
				//if a jawa has ref phase, need to remove it from rpdu and phase association mapping
				if(j.getPhase()>0){
					Jawa jawa = rpduPhaseAssociation.get(j.getFeed());
					if(j.equals(jawa))
						rpduPhaseAssociation.remove(j.getFeed());
				}
			}
			
		}else if(target instanceof Jawa){
			Jawa jawa = (Jawa)target;
			//if a jawa has ref phase, need to remove it from rpdu and phase association mapping
			if(jawa.getPhase()>0){
				Jawa jawa2 = rpduPhaseAssociation.get(jawa.getFeed());
				if(jawa.equals(jawa2))
					rpduPhaseAssociation.remove(jawa.getFeed());
			}
		}
	}
	
	/**
	 * add new phase association after adding new jawa or server
	 * @param target
	 */
	public void addRpduPhaseAssociations(Object target){
		if(target instanceof Server){
			Server server = (Server)target;
			for(Jawa j:server.getJawas()){
				//if a jawa has ref phase, need to remove it from rpdu and phase association mapping
				if(j.getPhase()>0){
					// if current rack already has ref phase association with same RPDU, delte new phase association
					if(rpduPhaseAssociation.containsKey(j.getFeed())){
						j.setPhase(0);
					}else{
						rpduPhaseAssociation.put(j.getFeed(),j);
					}
				}
			}
			
		}else if(target instanceof Jawa){
			Jawa jawa = (Jawa)target;
			//if a jawa has ref phase, need to remove it from rpdu and phase association mapping
			if(jawa.getPhase()>0){
				// if current rack already has ref phase association with same RPDU, delte new phase association
				if(rpduPhaseAssociation.containsKey(jawa.getFeed())){
					jawa.setPhase(0);
				}else{
					rpduPhaseAssociation.put(jawa.getFeed(),jawa);
				}
			}
		}
	}
	
	/**
	 *	update rpdu key in RpduPhaseAssociation map. when changing rpdu value 
	 * @param jawa
	 * @param oldRpdu
	 * @param newRpdu
	 */
	public void updateRpduPhaseAssociation(Jawa jawa, int oldRpdu, int newRpdu){
		//remove old key
		if(rpduPhaseAssociation.containsValue(jawa)){
			if(rpduPhaseAssociation.get(oldRpdu).equals(jawa))
				rpduPhaseAssociation.remove(oldRpdu);
			
		}
		// put new key
		if(jawa.getPhase()>0){
			rpduPhaseAssociation.put(newRpdu, jawa);
		}
	}
	
	public String printPhaseAssociationInfo(){
		String info = "";
		
		for(int r:rpduPhaseAssociation.keySet()){
			info+="rpud:" + r + rpduPhaseAssociation.get(r).toString() + "\n";
		}
		
		return info;
	}
}

