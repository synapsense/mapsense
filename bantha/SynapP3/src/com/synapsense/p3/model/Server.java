package com.synapsense.p3.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.synapsense.p3.exceptions.ServerCreationFailedException;
import com.synapsense.p3.utilities.LogManager;
import com.synapsense.p3.utilities.Validator;
import com.synapsense.p3.utilities.Variables;

public class Server extends Invalidity{
	private static Logger logger = Logger.getLogger(Server.class.getName());
	// server's location in a rack
	private int location;
	// server's uheight default is 1
	private int uheight;
	// power value for faceplate
	private double power;
	// the list of child Jawa (plugmeter)
	private ArrayList<Jawa> jawas;
	// parent, rack belong to this server
	private Rack rack;

	/**
	 * Constructor with location, uheight and parent rack
	 * Before creating server object, check server location . if valid location, create a server
	 * @param _location
	 * @param _uheight
	 * @param _rack
	 * @throws Exception
	 */
	public Server(int _location, int _uheight, Rack _rack) throws Exception{
		if(!Validator.checkServerLocation(_uheight, _location, _rack)){
			logger.error("Fail to create new server because of invalid location");
			logger.error("server info: uheight" + _uheight + " location:" + _location + " rack size:" + _rack.getUheight());
			throw new ServerCreationFailedException();
		}
		
		location = _location;
		uheight = _uheight;
		rack = _rack;
		jawas = new ArrayList<Jawa>();
		
		// validate faceplate power value
		if(!Validator.checkServer(this))
			addInvalidity(Variables.CONFIGURATION_ERROR_SERVER_FACEPLATE);
		
		logger.info("Create new " + this.toString());
		
	}
	
	/**
	 * Constructor with location, uheight, powervalue and parent rack
	 * validate server location before creating a server
	 * @param _location
	 * @param _uheight
	 * @param _power
	 * @param _rack
	 * @throws Exception
	 */
	public Server(int _location, int _uheight, double _power, Rack _rack) throws Exception{
		if(!Validator.checkServerLocation(_uheight, _location, _rack)){
			logger.error("Fail to create new server because of invalid location");
			logger.error("server info: uheight" + _uheight + " location:" + _location + " rack size:" + _rack.getUheight());
			throw new ServerCreationFailedException();
		}
		
		location = _location;
		uheight = _uheight;
		power = _power;
		rack = _rack;
		jawas = new ArrayList<Jawa>();
		
		// validate faceplate power value
		if(!Validator.checkServer(this))
			addInvalidity(Variables.CONFIGURATION_ERROR_SERVER_FACEPLATE);

		logger.info("Create new " + this.toString());
			
		
	}
	
	/**
	 * set server location
	 * @param _location
	 */
	public void setLocation(int _location){
		location = _location;
	}
	
	/**
	 * set servher uheight
	 * @param _uheight
	 */
	public void setHeight(int _uheight){
		uheight = _uheight;
	}
	
	/**
	 * return server location
	 * @return location
	 */
	public int getLocation(){
		return location;
	}
	
	/**
	 * return server's uheight
	 * @return uheight
	 */
	public int getUheight(){
		return uheight;
	}
	
	/**
	 * set faceplate's power value
	 * @param _power
	 */
	public void setPower(double _power){
		power = _power;
		
		if(Validator.checkServer(this))
			removeInvalidity(Variables.CONFIGURATION_ERROR_SERVER_FACEPLATE);
		else
			addInvalidity(Variables.CONFIGURATION_ERROR_SERVER_FACEPLATE);

	}
	
	/**
	 * get faceplate powervalue
	 * @return power value
	 */
	public double getPower(){
		return power;
	}
	
	/**
	 * add a child jawa (plugmeter) to children list
	 * 	- if a server has faceplate power error, remove it
	 * 	- Update parent rack's phase associations
	 * @param _jawa
	 */
	public void addJawa(Jawa _jawa){
		try{
			// add rpdu phase association if a jawa has phase association
			rack.addRpduPhaseAssociations(_jawa);
			jawas.add(_jawa);
			
			if(Validator.checkServer(this))
				removeInvalidity(Variables.CONFIGURATION_ERROR_SERVER_FACEPLATE);
			else
				addInvalidity(Variables.CONFIGURATION_ERROR_SERVER_FACEPLATE);
			logger.info("Add " + _jawa.toString() + " to " + this.toString());
			
		}catch(Exception e){
			logger.debug(LogManager.getStackTrace(e));
		}
		
			
	}
	
	/**
	 * get all child jawa(Plugmeter) list
	 * @return
	 */		
	public ArrayList<Jawa> getJawas(){
		return jawas;
	}
	
	/**
	 * delete a jawa from this server
	 * before removing jawa, need to update phase association info from parent rack
	 * @param _jawa
	 */
	public void deleteJawa(Jawa _jawa){
		try{
			// update phase association info from parent rack
			rack.removeRpduPhaseAssociations(_jawa);
			
			String jawaInfo = _jawa.toString();
			jawas.remove(_jawa);

			// if all children was removed, need to check faceplate power value.
			if(Validator.checkServer(this))
				removeInvalidity(Variables.CONFIGURATION_ERROR_SERVER_FACEPLATE);
			else
				addInvalidity(Variables.CONFIGURATION_ERROR_SERVER_FACEPLATE);

			logger.info("Delete " + jawaInfo + " from " + this.toString());

			
		}catch(Exception e){
			logger.debug(LogManager.getStackTrace(e));
		}
		
	}
	
	/**
	 * return parent rack
	 * @return rack
	 */
	public Rack getRack(){
		return rack;
	}
	
	/**
	 *  update parent 
	 *  In a case moving a server from a rack to another
	 * @param rack
	 */
	public void setRack(Rack rack){
		this.rack = rack;
	}
	
	/**
	 * get Max index number of child jawa list
	 * It should be between 0 to 7 
	 * @return
	 */
	public int getMaxJawaIndex(){
		ArrayList<Integer> arraylist = new ArrayList<Integer>();
		
		for(int i=1;i<=Variables.P3_MAX_NUMBER;i++){
			arraylist.add(i);
		}
		
		for(int i=0;i<jawas.size();i++){
			arraylist.remove((Integer)jawas.get(i).getIndex());
		}
		
		Integer array[] = new Integer[arraylist.size()];
		array = arraylist.toArray(array);
		Arrays.sort(array);
		return array[0];
	}
	
	/**
	 * reset jawa position by comparing rdpu value
	 * position should RPDU#1,RPDU#7,RPDU#5,RPDU#3,RPDU#4,RPDU#6,RPDU#8,RPDU#2
	 * Everytime new plugmeter is added or change RPDU value, needs to reset jawa's orders
	 */
	public void resetJawaOrders(){
		HashMap<Integer, Jawa> orderedJawas = new HashMap<Integer, Jawa>();
		
		ArrayList<Jawa> feed1 = new ArrayList<Jawa>();
		ArrayList<Jawa> feed2 = new ArrayList<Jawa>();
		ArrayList<Jawa> feedEtc = new ArrayList<Jawa>();
		int[] etcOrders = {4,5,3,6,2,7,1,8};
		
		// RPDU 1 should be aligned to left side
		// RPDU 2 should be aligned to right side
		// other RPUD should be aligned center side
		for(Jawa j:getJawas()){
			if(j.getFeed()==1){
				feed1.add(j);
			}else if(j.getFeed()==2){
				feed2.add(j);
			}else{
				feedEtc.add(j);
			}
		}
		
		// set index for RPDU #1, left side's index start from 0
		int index = 0;
		for(Jawa j:feed1){
			j.setIndex(++index);
			orderedJawas.put(j.getIndex(), j);
		}
		
		// set order for RPDU #2, most right side's index is 8
		index = 8;
		for(Jawa j:feed2){
			j.setIndex(index--);
			orderedJawas.put(j.getIndex(), j);
		}
		
		// set other RPDU jawas. Base point is the center
		index = 0;
		for(Jawa j:feedEtc){
			boolean foundPosition = false;
			
			int position = 0;
			while(!foundPosition){
				position = etcOrders[index];
				if(orderedJawas.containsKey(position)){
					index++;
				}else{
					foundPosition = true;
				}
			}
			j.setIndex(position);
			orderedJawas.put(j.getIndex(), j);
		}
	}
	
	/**
	 * Move this server from current parent rack to new one
	 * After move it, delete this server from old parent and add it to new one.
	 * @param newRack
	 */
	public void moveTo(Rack newRack){
		if(newRack==null){
			System.out.println("null exception at moveTo");
			return;
		}
		if(newRack.equals(rack, false))
			return;
		
		rack.deleteServer(this);
		newRack.addServer(this);
	}
	
	/**
	 * Query a Plugmeter(Jawa) by id
	 * @param id
	 * @return
	 */
	public Jawa getJawaById(String id){
		for(Jawa jawa:this.getJawas()){
			if(jawa.getId().equalsIgnoreCase(id))
				return jawa;
		}
		return null;
	}
	
	/**
	 * Get Jawa by index in jawa arraylist
	 * @param index
	 * @return
	 */
	public Jawa getJawaByIndex(int index){
		for(Jawa jawa:this.getJawas()){
			if(jawa.getIndex() == index)
				return jawa;
		}
		return null;
		
	}
	
	/**
	 * Compare this server to another one
	 * when location, uheight and the number of children is same, returns true
	 * @param compareServer
	 * @return
	 */
	public boolean equals(Server compareServer){
		if((this.getLocation() == compareServer.getLocation()) && (this.getUheight() == compareServer.getUheight())){
			
			if(this.jawas.size()!=compareServer.getJawas().size())
				return false;
			
			for(Jawa jawa1:this.getJawas()){
				Jawa jawa2 = compareServer.getJawaByIndex(jawa1.getIndex());

				if(jawa2==null || !jawa1.equals(jawa2)){
					return false;
				}
			}
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Get server detail info for usually debugging purpose
	 */
	public String toString(){
		String str = "Server[Rack Type: " + Integer.toString(rack.getType()) + " Location:" + Integer.toString(location) + " Uheight:" + Integer.toString(uheight) + " FacePlatePower:" + Double.toString(power) +  "]";
		return str;
 	}
	
	/**
	 * get list of jawa which has valid or invalid id
	 * @param isValid
	 * @return
	 */
	public ArrayList<Jawa> getJawasByIdValidation(boolean isValid){
		ArrayList<Jawa> list = new ArrayList<Jawa>();
		
		for(Jawa j:jawas){
			if(j.getInvalidities().contains(Variables.CONFIGURATION_ERROR_P3_ID)!=isValid)
				list.add(j);
		}
		
		return list;
	}
	
	
}
