package com.synapsense.laf.styles;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.jdesktop.swingx.graphics.GraphicsUtilities;
import org.jdesktop.swingx.painter.ImagePainter;

public class ImageMerger {
	private String headerImageFilePath;
	private String tiledImageFilePath;
	private String footerImageFilePath;
	private int num;
	
	public ImageMerger(String headerImageFile, String tiledImageFile, int tileNumber, String footerImageFile){
		headerImageFilePath = headerImageFile;
		tiledImageFilePath = tiledImageFile;
		num = tileNumber;
		footerImageFilePath = footerImageFile;
	}
	
	public BufferedImage getMergedImage(){
		BufferedImage headerImage = null;
        
        try {
			headerImage = ImageIO.read(ImageMerger.class.getResource("/resources/" + headerImageFilePath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        BufferedImage headerBufferImage = GraphicsUtilities.createCompatibleTranslucentImage(headerImage.getWidth(), headerImage.getHeight());
        Graphics2D g = headerBufferImage.createGraphics();
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
        g.drawImage(headerImage, 0, 0, headerImage.getWidth(), headerImage.getHeight(), null);
        g.dispose();
        
        // tiledImage
        BufferedImage tiledImage = null;
        
        try {
        	tiledImage = ImageIO.read(ImageMerger.class.getResource("/resources/" + tiledImageFilePath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        BufferedImage tiledBufferImage = GraphicsUtilities.createCompatibleTranslucentImage(tiledImage.getWidth(), tiledImage.getHeight());
        Graphics2D g2 = tiledBufferImage.createGraphics();
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
        g2.drawImage(tiledImage, 0, 0, tiledImage.getWidth(), tiledImage.getHeight(), null);
        g2.dispose();

        BufferedImage footerImage = null;
        try{
        	footerImage = ImageIO.read(ImageMerger.class.getResource("/resources/" + footerImageFilePath));
        }catch(IOException e){
        	e.printStackTrace();
        }
        BufferedImage footerBufferImage = GraphicsUtilities.createCompatibleTranslucentImage(footerImage.getWidth(), footerImage.getHeight());
        Graphics2D g3 = footerBufferImage.createGraphics();
        g3.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
        g3.drawImage(footerImage, 0, 0, footerImage.getWidth(), footerImage.getHeight(), null);
        g3.dispose();

        int w = headerImage.getWidth();
        int h = headerImage.getHeight() + tiledImage.getHeight()*num + footerImage.getHeight();
        
        BufferedImage mergedImage = new BufferedImage(w,h,BufferedImage.TYPE_INT_ARGB);
        Graphics2D mergedG2 = mergedImage.createGraphics();
        mergedG2.drawImage(headerBufferImage, 0, 0,null);
        
        for(int i=0;i<num;i++){
        	int x = 0;
        	int y = headerBufferImage.getHeight() + i * tiledBufferImage.getHeight();
            mergedG2.drawImage(tiledBufferImage, x, y, null);
        }
        
        mergedG2.drawImage(footerBufferImage, 0, headerBufferImage.getHeight() + num * tiledBufferImage.getHeight(), null);
        
        mergedG2.dispose();
        
        return mergedImage;
		
	}
}
