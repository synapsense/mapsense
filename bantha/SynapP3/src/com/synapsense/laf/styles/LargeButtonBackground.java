package com.synapsense.laf.styles;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.jdesktop.swingx.graphics.GraphicsUtilities;
import org.jdesktop.swingx.painter.ImagePainter;
import org.jdesktop.swingx.painter.AbstractAreaPainter;

public class LargeButtonBackground extends ImagePainter {
	
	public static final String BACKGROUND_FILENAME = "buttons/largebutton2.png";
	public static final String RESPATH = "/resources/";
	
    /**
     * @todo Refactor
     */
    public LargeButtonBackground() {
        BufferedImage image = null;
        try {
            image = ImageIO.read(this.getClass().getResource(RESPATH + BACKGROUND_FILENAME));
            BufferedImage mod = GraphicsUtilities.createCompatibleTranslucentImage(
                      image.getWidth(), 
                      image.getHeight());
            
            Graphics2D g = mod.createGraphics();
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.8f));
            g.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
            g.dispose();
            
            this.setImage(mod);
            this.setScaleToFit(true);
            this.setHorizontalAlignment(HorizontalAlignment.CENTER);
            this.setVerticalRepeat(false);
            this.setBorderPaint(Color.blue);
            this.setBorderWidth(2.0f);
            
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }        
        this.setStyle(AbstractAreaPainter.Style.BOTH );
    }	

}
