package com.synapsense.laf.styles;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.jdesktop.swingx.graphics.GraphicsUtilities;
import org.jdesktop.swingx.painter.ImagePainter;
import org.jdesktop.swingx.painter.AbstractLayoutPainter.HorizontalAlignment;
import org.jdesktop.swingx.painter.AbstractAreaPainter;

public class BgImagePainter {

    static public ImagePainter getImagePainter(String string) {
    	return getPainter(string, false, 1f);
    }
    
    static public ImagePainter getTiledPainter(String string) {
    	return getPainter(string, true, 1f);
    }
    
    static public ImagePainter getAlphaPainter(String string, float alpha) {
    	return getPainter(string, false, alpha);    	
    }
    
    static public ImagePainter getTiledAlphaPainter(String string, float alpha) {
    	return getPainter(string, true, alpha);
    } 
    
    /**
     * @param string
     * @return
     * 
     * @todo Refactor, move this ImagePainter out of RackPanel class
     */
    static private ImagePainter getPainter(String string, Boolean tiled, float alpha) {
        ImagePainter imagePainter = null;
        BufferedImage image = null;
        try {
            image = ImageIO.read(BgImagePainter.class.getResource("/resources/" + string));
            BufferedImage mod = GraphicsUtilities.createCompatibleTranslucentImage(
                      image.getWidth(), 
                      image.getHeight());
            Graphics2D g = mod.createGraphics();
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
            g.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
            g.dispose();
            imagePainter = new ImagePainter(mod);
            imagePainter.setHorizontalAlignment(HorizontalAlignment.LEFT);
            imagePainter.setVerticalRepeat(tiled);
            imagePainter.setStyle(AbstractAreaPainter.Style.FILLED);
        } catch (IOException e) {
        	System.out.println("null image:" + string);
            // TODO Auto-generated catch block
           // e.printStackTrace();
            return null;
        }
        
        
        return imagePainter;
    }	
    
    static public ImagePainter getPainter(BufferedImage image){
    	ImagePainter imagePainter = null;
        BufferedImage mod = GraphicsUtilities.createCompatibleTranslucentImage(
                  image.getWidth(), 
                  image.getHeight());
        Graphics2D g = mod.createGraphics();
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
        g.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
        g.dispose();
        imagePainter = new ImagePainter(mod);
        imagePainter.setHorizontalAlignment(HorizontalAlignment.LEFT);
        imagePainter.setVerticalRepeat(false);
        imagePainter.setStyle(AbstractAreaPainter.Style.FILLED);
        return imagePainter;
    }
	
}
