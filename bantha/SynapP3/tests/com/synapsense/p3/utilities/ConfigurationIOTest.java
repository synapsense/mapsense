/**
 * 
 */
package com.synapsense.p3.utilities;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.p3.model.Configuration;
import com.synapsense.p3.model.ConfigurationError;
import com.synapsense.p3.model.Jawa;
import com.synapsense.p3.model.Rack;
import com.synapsense.p3.model.Server;

/**
 * @author HLim
 *
 */
public class ConfigurationIOTest {
	private ConfigurationIO io;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		io = new ConfigurationIO();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.synapsense.p3.utilities.ConfigurationIO#ConfigurationIO()}.
	 */
	@Test
	public void testConfigurationIO() {
		//fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.synapsense.p3.utilities.ConfigurationIO#writeTemplate(java.lang.String, com.synapsense.p3.model.Configuration)}.
	 */
	@Test
	public void testWriteTemplate() {
		//fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.synapsense.p3.utilities.ConfigurationIO#importTemplate(java.lang.String)}.
	 */
	@Test
	public void testImportTemplate() {
		//fail("Not yet implemented");
	}

	@Test
	public void testSimpleCommunication(){
		try{
			for(int i=0;i<1;i++){
				
				ConfigurationError err = io.downloadConfiguration();
				assertEquals(err.getInvalidity(), Variables.CONFIGURATION_NO_ERROR);
				Configuration configuration1 = (Configuration)err.getObject();
				
				ConfigurationError downErr = io.uploadConfiguration(configuration1);
				assertEquals(downErr.getInvalidity(), Variables.CONFIGURATION_NO_ERROR);
				
				ConfigurationError err2 = io.downloadConfiguration();
				assertEquals(err2.getInvalidity(), Variables.CONFIGURATION_NO_ERROR);
				Configuration configuration2 = (Configuration)err2.getObject();
				
				//assertEquals(configuration, configuration2);
				if(!configuration1.equals(configuration2))
					fail("configuration is not synchronized");
			}
			
		}catch(Exception e){
			fail("cannot communicate");
		}
	}
	
	@Test
	public void testCommunicationAfterChange() throws Exception{
		ConfigurationError err = io.downloadConfiguration();
		assertEquals(err.getInvalidity(), Variables.CONFIGURATION_NO_ERROR);

		Configuration configuration1 = (Configuration)err.getObject();
		Configuration configuration2 = new Configuration();
		configuration2.cloneFrom(configuration1);
		
		Rack rack1 = configuration2.getRackByType(3);
		Server server = new Server(5, 2, 0, rack1);
		Jawa jawa = new Jawa("AM1029CDF24FE", 1, 0, server);
		server.addJawa(jawa);
		rack1.addServer(server);
		
		ConfigurationError err2 = io.uploadConfiguration(configuration2);
		assertEquals(err2.getInvalidity(), Variables.CONFIGURATION_NO_ERROR);
		
		ConfigurationError err3 = io.downloadConfiguration();
		assertEquals(err3.getInvalidity(), Variables.CONFIGURATION_NO_ERROR);
		Configuration configuration3 = (Configuration)err3.getObject();
		
		//assertEquals(configuration, configuration2);
		if(configuration1.equals(configuration3))
			fail("uploading or downloading is wrong");
	}
}
