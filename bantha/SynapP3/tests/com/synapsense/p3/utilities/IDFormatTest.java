package com.synapsense.p3.utilities;

import static org.junit.Assert.*;

import org.junit.Test;

public class IDFormatTest {

	@Test
	public void testHrToCode() throws InvalidChecksum {

assertEquals(IDFormat.hrToCode("AM1022D3F0506"), 0x16d3f05);
assertEquals(IDFormat.hrToCode("AM1242ED12FA5"), 0xaaed12f);
assertEquals(IDFormat.hrToCode("AM170256BE912"), 0x1c256be9);
assertEquals(IDFormat.hrToCode("AM1001181B8E1"), 0x1181b8);
assertEquals(IDFormat.hrToCode("AM1236B60FF48"), 0xa4b60ff);
assertEquals(IDFormat.hrToCode("AM15031D795E6"), 0x1431d795);
assertEquals(IDFormat.hrToCode("AM1340B085729"), 0xe8b0857);
assertEquals(IDFormat.hrToCode("AM17325268D32"), 0x1e05268d);
assertEquals(IDFormat.hrToCode("AM12474F12403"), 0xaf4f124);
assertEquals(IDFormat.hrToCode("AM1111433F986"), 0x4b433f9);
assertEquals(IDFormat.hrToCode("AM17462B56B9F"), 0x1ee2b56b);
assertEquals(IDFormat.hrToCode("AM11095CCDB51"), 0x495ccdb);
assertEquals(IDFormat.hrToCode("AM1834536DFF6"), 0x222536df);
assertEquals(IDFormat.hrToCode("AM161526E7DE8"), 0x18f26e7d);
assertEquals(IDFormat.hrToCode("AM16106DE5C1D"), 0x18a6de5c);
assertEquals(IDFormat.hrToCode("AM1649CF3E77F"), 0x1b1cf3e7);
assertEquals(IDFormat.hrToCode("AM1230078BA03"), 0x9e078ba);
assertEquals(IDFormat.hrToCode("AM11116F76F5D"), 0x4b6f76f);
assertEquals(IDFormat.hrToCode("AM12272C09D34"), 0x9b2c09d);
assertEquals(IDFormat.hrToCode("AM150192D371E"), 0x14192d37);

	}

	@Test
	public void testCodeToHr() {
assertEquals(IDFormat.codeToHr(0x1284b35c), "AM14404B35C64");
assertEquals(IDFormat.codeToHr(0x57f0720), "AM1123F072026");
assertEquals(IDFormat.codeToHr(0x19cb1783), "AM1628B17839B");
assertEquals(IDFormat.codeToHr(0xf173b55), "AM134973B5525");
assertEquals(IDFormat.codeToHr(0x2451869), "AM10365186963");
assertEquals(IDFormat.codeToHr(0x11b784d0), "AM1427784D0C8");
assertEquals(IDFormat.codeToHr(0x19413a6e), "AM162013A6EA3");
assertEquals(IDFormat.codeToHr(0x109375ba), "AM1409375BAE5");
assertEquals(IDFormat.codeToHr(0x4968bae), "AM110968BAEB1");
assertEquals(IDFormat.codeToHr(0x15febeab), "AM1531EBEAB6B");
assertEquals(IDFormat.codeToHr(0x2faa122), "AM1047AA122D8");
assertEquals(IDFormat.codeToHr(0x19d89ad0), "AM162989AD0AC");
assertEquals(IDFormat.codeToHr(0xde0b79), "AM1013E0B790C");
assertEquals(IDFormat.codeToHr(0x1d56276b), "AM17216276B83");
assertEquals(IDFormat.codeToHr(0xf470976), "AM1352709763D");
assertEquals(IDFormat.codeToHr(0x22df111d), "AM1845F111D2A");
assertEquals(IDFormat.codeToHr(0x132e61f4), "AM1450E61F46D");
assertEquals(IDFormat.codeToHr(0x14f69110), "AM15156911087");
assertEquals(IDFormat.codeToHr(0x203dea95), "AM1803DEA95D9");
assertEquals(IDFormat.codeToHr(0x1695dfae), "AM15415DFAEC8");
	}

}
