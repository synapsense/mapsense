package com.synapsense.p3.utilities;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite(AllTests.class.getName());
		//$JUnit-BEGIN$
		//suite.addTest(new TestSimpleConfigurationIO("simple IO test"));
		//$JUnit-END$
		return suite;
	}

}
