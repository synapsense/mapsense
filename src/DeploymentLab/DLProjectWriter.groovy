package DeploymentLab

import groovy.xml.MarkupBuilderHelper
import groovy.xml.MarkupBuilder
import java.nio.file.Files
import java.nio.charset.StandardCharsets
import java.nio.file.StandardOpenOption
import DeploymentLab.channellogger.Logger
import java.nio.file.Path

/**
 * Utility class for saving a DLProject to disk. Really, it's just the parts of DLProject that use Groovy.
 *
 * @author Ken Scoggins
 * @since Jupiter
 */
class DLProjectWriter {
    private static final Logger log = Logger.getLogger(DLProjectWriter.class.getName());

    private DLProjectWriter() {}

    public static void saveModel(DLProject project, Path file, boolean includeAll = true, boolean accessModel = false) {
        BufferedWriter writer = null;
        try {
            DLStopwatch timer = new DLStopwatch();

            writer = Files.newBufferedWriter(file, StandardCharsets.UTF_8);
            def builder = new MarkupBuilder(writer)

            //install the XML declaration at the top of the file
            //<?xml version="1.0" encoding="UTF-8"?>
            def helper = new MarkupBuilderHelper(builder)
            helper.xmlDeclaration('version': 1.0, encoding: "UTF-8")

            builder.model('version': DeploymentLabVersion.getModelVersion()) {
                if (accessModel) {
                    project.getAccessModel().getObjectModel().serialize(builder, includeAll)
                } else {
                    project.getProjectSettings().save(builder)
                    project.getObjectModel().serialize(builder, includeAll)
                    project.getComponentModel().serialize(builder, false, includeAll)
                    CentralCatalogue.getDeploymentPanel().serialize( builder )
                }
            }

            log.trace(timer.finishMessage("Saved model " + file.toAbsolutePath()));
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
}
