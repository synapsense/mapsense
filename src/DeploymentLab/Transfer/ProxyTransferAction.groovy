package DeploymentLab.Transfer

import javax.swing.AbstractAction
import java.awt.event.ActionEvent
import javax.swing.JComponent
import javax.swing.TransferHandler
import java.awt.datatransfer.Clipboard
import javax.swing.TransferHandler.TransferSupport
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.channellogger.Logger
import DeploymentLab.CentralCatalogue
import java.awt.datatransfer.DataFlavor
import DeploymentLab.Model.Dlid
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLProxy


/**
 * Transfer handler for processing proxy related paste events. This separate handler was necessary because the handler
 * used for normal copy/paste actions, DeploymentPanelTransferHandler, extends javax.swing.TransferHandler.
 * TransferHandler's actionPerformed method has hardcoded expectations that the action command is one of the standard
 * cut/copy/paste commands. This does not allow us to add custom action command processng to the default transfer
 * handler.
 *
 * @author Ken Scoggins
 * @since  Jupiter 2
 */
public class ProxyTransferAction extends AbstractAction {
    private static final Logger log = Logger.getLogger(ProxyTransferAction.class.getName());

    private DataFlavor flavor
    private DeploymentPanel deploymentPanel
    private ComponentModel componentModel

    /**
     * Default constructor. Creates a "Paste As Proxy" handler.
     */
    ProxyTransferAction() {
        super("pasteAsProxy")
    }

    /**
     * Sets the various application references necessary for the handler to function. These must be set before the
     * handler will process events.
     *
     * @param cm      The ComponentModel that contains the anchor component to proxy.
     * @param dp      The common local DeploymentPanel instance.
     * @param flavor  The transfer data flavor to expect.
     */
    public void setReferences( ComponentModel cm, DeploymentPanel dp, DataFlavor flavor ) {
        this.flavor = flavor
        this.deploymentPanel = dp
        this.componentModel = cm
    }

    /**
     * Entry point for incoming events.
     *
     * @param event  The event to be processed.
     */
    public void actionPerformed(final ActionEvent event) {
        // I hate this, but we need a static instance before all those references can be assigned.
        if( deploymentPanel == null ) {
            throw new IllegalStateException("References have not been assigned.")
        }

        Object src = event.getSource()
        if( src instanceof JComponent ) {
            JComponent c = (JComponent) src
            TransferHandler th = c.getTransferHandler()
            Clipboard clipboard = c.getToolkit().getSystemClipboard()
            TransferSupport support = new TransferSupport( c, clipboard.getContents(null) )

            // Cheat and use the existing paste canImport().
            if( th.canImport( support ) ) {
                importData( support )
            }
        }
    }

    /**
     * Create proxies for the components in the provided TransferSupport container.
     *
     * @param support  The container of components.
     * @return  True if the request was handled without errors. This does not mean that all components now have proxies.
     *          Components will be skipped if they are not proxyable or the anchor component is not found.
     */
    public boolean importData( TransferSupport support ) {

        def xml
        try{
            DLCopyVessel incoming = (DLCopyVessel) support.getTransferable().getTransferData(flavor)
            String payload = incoming.getPayload()
            xml = new XmlParser().parseText( payload )

        } catch( org.xml.sax.SAXParseException sax ) {
            log.debug("Bad xml parse: $sax")
        } catch (Exception e) {
            log.error( CentralCatalogue.formatUIS('transfer.pasteReadError', e.getMessage() ), "message")
            log.error( log.getStackTrace(e) )
            return false
        }

        boolean result = false;

        // Make sure this came from our local instance.
        if( !CentralCatalogue.APPLICATION_INSTANCE_ID.toString().equals( xml.@appId ) ) {
            log.error( CentralCatalogue.getUIS('proxy.illegalAlien'), 'message')
        } else {
            // No need for this since ProxyAdder will handle the undo buffer.
            //undoBuffer.startOperation()

            xml.TransferComponent.each { tc ->
                DLComponent anchor = componentModel.getComponentFromDLID( Dlid.getDlid( tc.@dlid ) )

                if( anchor == null ) {
                    log.debug("Anchor component not found! This is bad unless it was just deleted after copying. (DLID = '${tc.@dlid}')" )
                } else if( !DLProxy.canProxy( anchor ) ) {
                    log.warn( CentralCatalogue.formatUIS("proxy.cannotProxySpecific", anchor.getDisplaySetting('name') ), "message")
                } else {
                   deploymentPanel.getToolManager().getTool('proxyadder').addProxy( anchor )
                }
            }

            result = true
        }

        return result
    }
}
