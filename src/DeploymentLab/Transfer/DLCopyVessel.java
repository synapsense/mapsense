package DeploymentLab.Transfer;


import DeploymentLab.CentralCatalogue;

import java.io.Serializable;

/**
 * Wraps a payload of DL Components serialized to XML for moving via the clipboard.
 * @author Gabriel Helman
 * Date: 1/4/11
 * Time: 10:11 AM
 * @see SelectionTransferForge
 * @see DeploymentPanelTransferHandler
 */
public class DLCopyVessel implements Serializable{
	private static final long serialVersionUID = 3145715080285930916L;
	private String payload;
	private Double versionNumber;

	public DLCopyVessel(String XML, Double version){
		payload = XML;
		versionNumber = version;
	}

	public DLCopyVessel( String XML ){
		this(XML, Double.parseDouble(CentralCatalogue.getApp("model.version")));
	}

	public DLCopyVessel( ){
		this( "" );
	}


	public String getPayload() {
		return payload;
	}

	public void setPayload(String copyXML) {
		this.payload = copyXML;
	}

	public Double getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(Double versionNumber) {
		this.versionNumber = versionNumber;
	}

	@Override
	public String toString() {
		return "DLCopyVessel{" +
				"payload='" + payload + '\'' +
				", versionNumber=" + versionNumber +
				'}';
	}


}
