package DeploymentLab.Transfer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * @author Gabriel Helman
 * @since Earth
 * Date: Sep 20, 2010
 * Time: 11:47:41 AM
 * Based on the example at http://download.oracle.com/javase/tutorial/uiswing/dnd/listpaste.html with much thanks to Sun Microsystems.
 * Tracks which JComponent has the focus at any given time and dispatches action events there.
 * An instance of this class should be registered as an ActionListener on any "thing" that will be firing action events.
 *
 * As a bonus, if a JComponent is specified in the constructor, that instance of TransferActionListener will always
 * reroute the Actions to that component, rather than the "currently focused" component.
 *
 */
public class TransferActionListener implements ActionListener, PropertyChangeListener {
    private JComponent focusOwner = null;

    public TransferActionListener(JComponent defaultTarget) {
		if ( defaultTarget == null ){
			KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
			manager.addPropertyChangeListener("permanentFocusOwner", this);
		} else {
			focusOwner = defaultTarget;
		}
    }

    public void propertyChange(PropertyChangeEvent e) {
		//System.out.println( "Action Listener fired on property change " + e.getPropertyName() + " : "  + e.toString() );
		Object o = e.getNewValue();
        if (o instanceof JComponent) {
            focusOwner = (JComponent)o;
        } else {
            focusOwner = null;
        }
		/*
		if ( focusOwner == null ){
			System.out.println("Focus Owner is now "  + focusOwner);
		} else {
			System.out.println("Focus Owner is now " +  Integer.toHexString(focusOwner.hashCode()) + " "  + focusOwner);
		}
		*/
    }

    public void actionPerformed(ActionEvent e) {
		//System.out.println( "Action Listener fired on actionevent "  + e.toString() );
        if (focusOwner == null) {return;}
		//System.out.println( "Focused component for action is " + focusOwner.toString() );
        String action = (String)e.getActionCommand();
        Action a = focusOwner.getActionMap().get(action);
        if (a != null) {
            a.actionPerformed(new ActionEvent(focusOwner, ActionEvent.ACTION_PERFORMED, null));
        }
    }
}
