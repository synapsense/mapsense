package DeploymentLab.Transfer

import DeploymentLab.Exceptions.OperationCancelledException
import DeploymentLab.Exceptions.UnknownComponentException
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.Tools.NodeAdder
import DeploymentLab.Tools.NodeMover
import DeploymentLab.Tools.NodeOperator
import DeploymentLab.channellogger.Logger
import DeploymentLab.*
import DeploymentLab.Model.*
import edu.umd.cs.piccolo.event.PInputEvent

/**
 * For the purpose of clipboard-based copy&paste, this class handles turning a stack of components into serializable XML and back again.
 * @author Gabriel Helman
 * @since Earth
 * Date: Sep 20, 2010
 * Time: 12:12:12 PM
 *
 */
class SelectionTransferForge {
	private static final Logger log = Logger.getLogger(SelectionTransferForge.class.getName())

   	 /**
	 * Serializes the current selection into something that the transfer pipes can use.
	 what we need to serialize:

	 component-type-name
		 ID unique to this slug of xml
		 visible property : value
		 associations:
			 producers: list of consumer IDs (from above)
			 consumers: list of producer IDs (from above)

	 * Serialization format as follows:
	 * <pre>
	 * {@code
	 *
	 <ComponentTransfer>
	 <TransferComponent type="sometype transferID="42" >
		 <property name="something" value="something else" />
		 <property name="something" value="something else" />
		 <producer producerID="whatever">
			 <producerConsumer transferID="50" consumerID="whomever" />
			 <producerConsumer transferID="51" consumerID="whomever" />
			 <producerConsumer transferID="52" consumerID="whomever" />
		 <producer>
		 <consumer consumerID="whatever">
			 <consumerProducer transferID="60" producerID="else" />
			 <consumerProducer transferID="61" producerID="else" />
			 <consumerProducer transferID="62" producerID="else" />
		 <consumer>
         <children>
            <TransferChildComponent type="sometype" transferID="43" childID="child1">
                 <property name="something" value="something else" />
                 <property name="something" value="something else" />
                 <producer producerID="whatever">
                     <producerConsumer transferID="50" consumerID="whomever" />
                     <producerConsumer transferID="51" consumerID="whomever" />
                     <producerConsumer transferID="52" consumerID="whomever" />
                 <producer>
                 <consumer consumerID="whatever">
                     <consumerProducer transferID="60" producerID="else" />
                     <consumerProducer transferID="61" producerID="else" />
                     <consumerProducer transferID="62" producerID="else" />
                 <consumer>
            </TransferChildComponent>
         </children>

	 </TransferComponent>
	 ...
	 </ComponentTransfer>
	 }
	 </pre>
	 *
	 * @param the Select Model we're copying from
	 * @return a String holding the XML representation of the selected components.
	 */
	public static String copyToXML(SelectModel selectModel) {
		String xmlToShip
		def writer = new StringWriter()
		def builder = new groovy.xml.MarkupBuilder(writer)

		List<DLComponent> selection = new ArrayList<DLComponent>()

		// cannot copy staticchild or dynamicchild individually without parent
		selectModel.getExpandedSelection().each {c ->
			if (c.hasRole("staticchild") || c.hasRole("dynamicchild")) {
				if (selectModel.getExpandedSelection().intersect(c.getParentComponents()).size() == 0)
					selection.add(c)
			} else {
				selection.add(c)
			}

		}

		//first, build a map of components to copy : copyID
		int currentID = 0
		Map<DLComponent, Integer> copyIDs = new HashMap<DLComponent, Integer>();
		selection.each { c ->
			copyIDs[c] = currentID
			currentID += 1

			// This was meant for composite components, not children of containers. So don't auto copy the children of a room.
			if (( c.getType() != ComponentType.ROOM ) && ( c.getChildComponents().size() > 0 )) {
				c.getChildComponents().each { child ->
					copyIDs[child] = currentID
					currentID += 1
				}
			}

		}

		//now, make some XML!
		builder.ComponentTransfer( appId: CentralCatalogue.APPLICATION_INSTANCE_ID ) {
			selection.each { c ->
				//println "selection:" + c.toString()
				builder.TransferComponent(type: c.getType(), dlid: c.getDlid(), transferID: copyIDs[c]) {
					//properties (visible only)
					c.mapProperties().each { k, v ->
						//builder.property( name: k , value: v.getValue() )
						builder.property(name: k, value: PropertyConverter.toXml(v.getType(), v.getValue()))


					}
					//producers
					c.listProducers(true).each { p ->
						Producer currentProducer = c.getProducer(p)
						builder.producer(producerID: p) {
							//for each consumer, we need the component and that component's consumer ID
							currentProducer.mapConsumers().each { k, v ->
								builder.producerConsumer(transferID: copyIDs[k], consumerID: v)
							}
						}
					}
					//consumers
					c.listConsumers(true).each { con ->
						Consumer currentConsumer = c.getConsumer(con)
						builder.consumer(consumerID: con) {
							currentConsumer.mapProducers().each { k, v ->
								builder.consumerProducer(transferID: copyIDs[k], producerID: v)
							}
						}
					}
					// for children component
					//println c.toString()
					//println "copyToComponentXML: number of children:" + c.getChildComponents().size()

					// This was meant for composite components, not children of containers. So don't auto copy the children of a room.
					if (( c.getType() != ComponentType.ROOM ) && (c.getChildComponents().size() > 0)) {
						builder.children() {
							c.getChildComponents().each {child -> copyToChildComponentXML(child, builder, copyIDs)}
						}
					}
				}
			}
		}

		xmlToShip = writer.toString()
		//println "xml:"
		//println xmlToShip
		return xmlToShip;
	}

	/**
	 *  Serializes the current selection's child components into something that the transfer pipes can use.
	 * @param c
	 * @param builder
	 * @param copyIDs
	 */
	private static void copyToChildComponentXML(DLComponent c, groovy.xml.MarkupBuilder builder, Map<DLComponent, Integer> copyIDs) {
		builder.TransferChildComponent(type: c.getType(), transferID: copyIDs[c], childID: c.getPropertyValue("child_id")) {
			//properties (visible only)
			c.mapProperties().each { k, v ->
				builder.property(name: k, value: v.getValue())
			}
			//producers
			c.listProducers(true).each { p ->
				Producer currentProducer = c.getProducer(p)
				builder.producer(producerID: p) {
					//for each consumer, we need the component and that component's consumer ID
					currentProducer.mapConsumers().each { k, v ->
						builder.producerConsumer(transferID: copyIDs[k], consumerID: v)
					}
				}
			}
			//consumers
			c.listConsumers(true).each { con ->
				Consumer currentConsumer = c.getConsumer(con)
				builder.consumer(consumerID: con) {
					currentConsumer.mapProducers().each { k, v ->
						builder.consumerProducer(transferID: copyIDs[k], producerID: v)
					}
				}
			}
			// for children component

			if (c.getChildComponents().size() > 0) {
				builder.children() {
					c.getChildComponents().each {child -> copyToChildComponentXML(child, builder, copyIDs)}
				}
			}
		}
	}

	/**
	 * Takes the serialized components and installs them back in the component model.
	 * The paste should be atomic - either it works without a hitch, or nothing is pasted.
	 * Note also that the paste targets the current active grouping / network, with the usual restrictions there.
	 * @param transferred the component model to paste into
	 * @param cmodel the component model to paste into
	 * @param selectModel the select model to paste into
	 * @param undoBuffer the undo buffer
	 * @param no the Node Operator that will do the work
	 * @param deploymentPanel the DeploymentPanel that will have the nodes pasted
	 * @return true if the paste was successful, false if not.
	 * @see #copyToXML(SelectModel)
	 */
	public static boolean pasteFromXML(String transferred, ComponentModel cmodel, SelectModel selectModel, UndoBuffer undoBuffer, NodeOperator no, DeploymentPanel deploymentPanel) throws UnknownComponentException {

		def incoming
		try {
			incoming = new XmlParser().parseText(transferred)
		} catch (org.xml.sax.SAXParseException sax) {
			//log.warn("Bad Paste Attempt: ${sax.getMessage()}")
			println "bad xml parse"
			return false
		}

		//get all the types, see if the paste will work at all
		Set<String> types = new HashSet<String>()
		incoming.TransferComponent.each { tc ->
			//println "type loaded:" + tc.@type
			types.add(tc.@type.toString())
		}

		//println types
		//this will throw the UCE if something is totally unknown
		if (!no.canPaste(types)) {
			//log messages handled by NodeOperator
			return false
		}

		//at this point, we're good to go!
		DLComponent activeDrawing = selectModel.getActiveDrawing()
		DLComponent activeNetwork = selectModel.getActiveNetwork()
		DLComponent activeZone = selectModel.getActiveZone()

		Map<String, DLComponent> newComponents = [:] //transferID: component
		//use the progressmanager to get the main wait screen
		//also, since we actually know ahead of time, do a progress indicator
		int totalComponents = incoming.TransferComponent.size()
		int count = 1
		ProgressManager.doTask {
			def watch = new DLStopwatch()
			log.trace(String.format(CentralCatalogue.getUIS("progress.pasting"), count, totalComponents), "progress")
			try {
				undoBuffer.startOperation()

				//create and install all new components
				// copy all properties
				incoming.TransferComponent.each { tc ->
					log.trace(String.format(CentralCatalogue.getUIS("progress.pasting"), count, totalComponents), "progress")
					DLComponent currentComponent = copyComponent(tc, activeDrawing, activeNetwork, activeZone, deploymentPanel, cmodel, newComponents)
					newComponents[tc.@transferID] = currentComponent

					//println "newComponent: " + tc.@transferID
					//println "newComponent value:" +   newComponents[tc.@transferID]
					count++
				}

				//now do any and all associations
				count = 0
				incoming.TransferComponent.each { tc ->
					log.trace(String.format(CentralCatalogue.getUIS("progress.pasting.associate"), count, totalComponents), "progress")
					copyAssociation(tc, newComponents)
					count++
				}

				// select copied components
				if (newComponents.values().size() > 0) {
					selectModel.setSelection(newComponents.values())
					//move those to the center of the drawing
					//println "moving pasted nodes to center of drawing"
					NodeMover nm = new NodeMover(undoBuffer, deploymentPanel, selectModel)
					nm.moveNodesToCenter()
				}

			} catch( OperationCancelledException e ) {
				// Undo, but no need to propagate. The exception is just a quick way to bail out deep in the operation.
				undoBuffer.rollbackOperation()
				log.info( e.getMessage() )

			} catch (Exception e) {
				undoBuffer.rollbackOperation()
				// println e.getStackTrace()
				throw e
			} finally {
				undoBuffer.finishOperation()
			}
			log.trace(watch.finishMessage("Paste"))
		}

		return true
	}

	/**
	 * create new component and copy all properties
	 * @param tc
	 * @param activeDrawing
	 * @param activeNetwork
	 * @param activeZone
	 * @param cmodel
	 * @param newComponents
	 * @return
	 */
	private static DLComponent copyComponent(def tc, DLComponent activeDrawing, DLComponent activeNetwork, DLComponent activeZone, DeploymentPanel dp, ComponentModel cmodel, Map<String, DLComponent> newComponents) {

		// Use NodeAdder since it does the special stuff necessary. Would be nice to refactor it so we don't have to fake an event, though.
		NodeAdder adder = (NodeAdder) dp.getToolManager().getTool('adder')
		adder.type = tc.@type.toString()

		def propXml = tc.property.find { prop -> prop.@name.toString() == 'name' }
		String fromName = ( propXml ? propXml.@value.toString() : null )

		if( !adder.isAddable( new PInputEvent(null,null), fromName ) ) {
			// Bit of a hack to rollback the entire paste without refactoring. isAddable will log the reason why.
			throw new OperationCancelledException("Paste cancelled! See previous messages for details.")
		}

		//construct new
		DLComponent currentComponent = cmodel.newComponent(tc.@type.toString())

		//install into the model
		adder.addToParent( currentComponent )

		//properties!
		copyProperties(tc, currentComponent)

		//println currentComponent.toString()
		//println "created child components:" + currentComponent.getChildComponents().size()
		//println "transfer child components :" +  tc.children.TransferComponent.size()

		// for static and dynamicchildplaceable component. copy child component, too
		if (currentComponent.getChildComponents().size() == 0 && tc.children.TransferChildComponent.size() > 0) {
			// for staticchildplaceable component : create new child component and copy properties
			tc.children.TransferChildComponent.each {childTc ->
				//println "child transferComponent:" + childTc.@type
				DLComponent childComponent = copyComponent(childTc, activeDrawing, activeNetwork, activeZone, dp, cmodel, newComponents)
				currentComponent.addChild(childComponent)
				newComponents[childTc.@transferID] = childComponent
			}
		} else if (currentComponent.getChildComponents().size() > 0 && tc.children.TransferChildComponent.size() > 0) {
			// for dynamicchildplaceable component: don't create new child component ( child components are already created when creating new parent component
			// all things to do is copy properties for child component
			tc.children.TransferChildComponent.each {childTc ->
				//println "child id:" + childTc.@childID
				def childComponent = currentComponent.getChildComponents().find {it.getPropertyValue("child_id").equals(childTc.@childID)}
				if (childComponent != null) {
					copyProperties(childTc, childComponent)
					newComponents[childTc.@transferID] = childComponent
				}
			}
		}
		return currentComponent
	}

	/**
	 * copy properties only
	 * @param tc
	 * @param childComponent
	 */
	private static void copyProperties(def tc, DLComponent childComponent) {
		Map<String,Object> incomingProps = [:]
		tc.property.each { prop ->
			incomingProps.put( prop.@name.toString(), prop.@value )
		}

		// Do some work to sort the props. We need to process non-parcel props first, and inherited props before all.
		Queue<ComponentProperty> propsToSet = new ArrayDeque<>( childComponent.listSortedProperties() )
		while( !propsToSet.isEmpty() ) {
			ComponentProperty prop = propsToSet.remove()

			if( prop.getName() != 'configuration' ) {
				if( prop.value != null ) {
					if( incomingProps.containsKey( prop.getName() ) ) {
						def beforeProps = childComponent.listProperties()

						childComponent.setPropertyValue( prop.getName(), incomingProps.get( prop.getName() ) )

						// See if this prop triggered a parcel that has new properties we need to now consider.
						def deltaProps = childComponent.listProperties()
						deltaProps.removeAll( beforeProps )
						for( String p : deltaProps ) {
							propsToSet.add( childComponent.getComponentProperty(p) )
						}
					}
				}
			}
		}

		// Copy any dynamic attributes that aren't normal properties.
		if( ( tc.@exportable != null ) && ( !tc.@exportable.text().isEmpty() ) ) {
			childComponent.setIsExportable( Boolean.parseBoolean( tc.@exportable.text() ) )
		}
	}

	/**
	 * copy association
	 * @param tc
	 * @param newComponents
	 */
	private static void copyAssociation(def tc, Map<String, DLComponent> newComponents) {
		//we actually only need to play back the consumers, as that still gets us all of the associations within the pasted selection
		tc.consumer.each { cXML ->
			//println "working on consumer: " + cXML.@consumerID
			//void associate(String consumerId, DLComponent producer, String producerId) {
			DLComponent currentConsumer = newComponents[tc.@transferID]
			//println "hosted by $currentConsumer"
			cXML.consumerProducer.each { cpXML ->
				//println "to producers: ${cpXML.@producerID} on ${cpXML.@transferID}"
				if (cpXML.@transferID.trim().length() > 0) {
					currentConsumer.associate(cXML.@consumerID, newComponents[cpXML.@transferID], cpXML.@producerID)
				}
			}
		}
	}

}
