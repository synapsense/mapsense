package DeploymentLab.Transfer;

import DeploymentLab.CentralCatalogue;
import DeploymentLab.Exceptions.UnknownComponentException;
import DeploymentLab.SceneGraph.DeploymentPanel;
import DeploymentLab.Model.ComponentModel;
import DeploymentLab.SelectModel;
import DeploymentLab.Tools.NodeOperator;
import DeploymentLab.UndoBuffer;
import DeploymentLab.channellogger.Logger;

import javax.activation.DataHandler;
import javax.swing.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;

/**
 * Copies components from one DL scene graph to another (or to the same one, really.)
 * @author Gabriel Helman
 * @since Earth
 * Date: Sep 20, 2010
 * Time: 11:26:30 AM
 *
 * This is assumed to be attached to the DeploymentPanel's PCanvas, which is itself an extension of JComponent.
 * This means that the (JComponent c) parameter that shows up in the methods below is assumed to be DeploymentPanel's PCanvas.
 *
 * @see SelectionTransferForge
 * @see DLCopyVessel
 */
public class DeploymentPanelTransferHandler extends TransferHandler {
	private static final Logger log = Logger.getLogger(DeploymentPanelTransferHandler.class.getName());

    // Custom actions.
    private static final ProxyTransferAction PASTEASPROXY_ACTION = new ProxyTransferAction();

	private DataFlavor flavor;
	private ComponentModel componentModel;
	private UndoBuffer undoBuffer;
	private SelectModel selectModel;
	private NodeOperator nodeOperator;
	private DeploymentPanel deploymentPanel;


	public DeploymentPanelTransferHandler(ComponentModel model, UndoBuffer ub, SelectModel sm, NodeOperator no, DeploymentPanel dp ) {
		componentModel = model;
		undoBuffer = ub;
		selectModel = sm;
		nodeOperator = no;
		deploymentPanel = dp;
		//with a flavor based on DLCopyVessel, nothing else will be able to interfere
		flavor = new DataFlavor(DLCopyVessel.class, "DL Components as XML");

        // HACK! We want a static instance of the action, but don't have all these references until now.
        PASTEASPROXY_ACTION.setReferences( model, dp, flavor );
	}

    /**
     * Returns an {@code Action} that creates a component proxy from the component
     * that resides in the clipboard.
     *
     * @return an {@code Action} for creating a proxy from the clipboard
     */
    public static Action getPasteAsProxyAction() {
        return PASTEASPROXY_ACTION;
    }


	//Export Methods:

	@Override
	public int getSourceActions(JComponent c) {
		return COPY;
	}

	@Override
	protected Transferable createTransferable(JComponent c) {
		//pull the list of selected nodes from the select model in some kind of magic xml format
		String toCopy = SelectionTransferForge.copyToXML(selectModel);
		//copying via DLCopyVessel insures that no other program can get a copy
		DLCopyVessel spaceShip = new DLCopyVessel(toCopy);
		Transferable xfer = new DataHandler( spaceShip, flavor.getMimeType() );
		return xfer;
	}

	@Override
	protected void exportDone(JComponent source, Transferable data, int action) {
		//ideally, all the work should be done at this point
		//log.info("CCP DONE", "default");
		log.trace("Components copied to clipboard.", "default");
	}


	//Import Methods:

	@Override
	public boolean importData(TransferSupport support) {
		//need to call canImport() ourselves for a paste operation
		if (! canImport(support) ) { return false; }

		String payload = "";
		try{
			DLCopyVessel incoming = (DLCopyVessel) support.getTransferable().getTransferData(flavor);
			payload = incoming.getPayload();
		} catch (Exception e) {
            log.error( CentralCatalogue.formatUIS("transfer.pasteReadError", e.getMessage() ), "message");
			log.error(log.getStackTrace(e), "default");
			return false;
		}

		if (payload.length() == 0 ) { return false; }
		/*
		log.info("checking to see if we can import" + payload, "default");
		if ( ! payload.trim().startsWith("<ComponentTransfer>")  ){
			log.info("And we can't!", "");
			return false;
		}
		*/

		//return SelectionTransferForge.pasteFromXML(payload, componentModel, selectModel, undoBuffer, nodeOperator, deploymentPanel );

		boolean result = false;

		try{
			result = SelectionTransferForge.pasteFromXML(payload, componentModel, selectModel, undoBuffer, nodeOperator, deploymentPanel );
		} catch (UnknownComponentException uce){
			log.error( String.format(CentralCatalogue.getUIS("clone.notLoaded.withType"), uce.getMessage()), "message" );
		} catch (Exception e){
			log.error( CentralCatalogue.getUIS("clone.notLoaded"), "message" );
			log.error( log.getStackTrace(e), "default" );
		}

		log.trace("Paste Result was " + result, "default");
		return result;
	}

	@Override
	public boolean canImport(TransferSupport support) {
		boolean result = true;
		String payload = "";
		try{
			/*
			log.info(  support.getTransferable().getClass().getName(), "default" );
			log.info(flavor.toString(), "default");
			for ( DataFlavor df :  support.getTransferable().getTransferDataFlavors() ){
				log.info("DF: " + df.toString() , "default");
			}
            */

			DLCopyVessel incoming = (DLCopyVessel) support.getTransferable().getTransferData(flavor);
			if ( Double.parseDouble(CentralCatalogue.getApp("model.version")) != incoming.getVersionNumber() ){
				log.error("Version Mismatch, cannot paste.", "message");
				return false;
			}

			payload = incoming.getPayload();
		} catch (java.awt.datatransfer.UnsupportedFlavorException ufe ){
			log.trace("Unknown data flavor in paste source.", "default");
			return false;
		} catch (Exception e) {
			log.error("Error pasting components:\n" + e.getMessage(), "message");
			log.error(log.getStackTrace(e), "default");
			return false;
		}

		if ( (payload == null ) || (payload.length() == 0) ) {
			log.trace("Paste payload is empty.", "default");
			return false;
		}

		//log.info("checking to see if we can import" + payload, "default");
		log.trace("checking to see if we can import payload", "default");
		if ( ! payload.trim().startsWith("<ComponentTransfer ")  ){
			log.trace("And we can't!", "default");
			result = false;
		}
		return result;
	}

}
