package DeploymentLab;

import DeploymentLab.Model.ComponentType;
import DeploymentLab.SceneGraph.LineIntersection;
import DeploymentLab.channellogger.Logger;
import edu.umd.cs.piccolo.nodes.PPath;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: HLim
 * Date: 6/2/11
 * Time: 11:49 AM
 * To change this template use File | Settings | File Templates.
 */
public class StaticValidator {
    private static final Logger log = Logger.getLogger(StaticValidator.class.getName());

    public enum ShapeValidations{
        VALID_SHAPE,
        LESS_MIN_POINTS,
        CROSSED_EDGE,
        UNKNOWN_INVALID_SHAPE
     }

    /**
     * Validate shape based on given points
     * @param points
     * @return 0 : valid shape
     *         1: the minimum number of points is 3
     *         2: Polygon edges cannot cross
     */

    public static ShapeValidations validateShape( ArrayList<Point2D> points, int minPoints, String type ){
        //log.trace("number of points:" + points.size() );
        // the number of points should be more and equal than 3
        if( points.size() < minPoints )
            return ShapeValidations.LESS_MIN_POINTS;
        // construct line list
		ArrayList<Line2D.Double> lineList = new ArrayList<Line2D.Double>();
        for(int i=0;i<points.size()-1;i++){
			//log.info("index:" + i + "[ " + points.get(i).getX() + "," + points.get(i).getY());
			Line2D.Double line = new Line2D.Double(points.get(i),(points.get(i+1)));
			lineList.add(line);
		}

	    if( !type.equals( ComponentType.DRAWING_LINE ) ) {
		    Line2D.Double line = new Line2D.Double(points.get(points.size()-1), points.get(0));
		    lineList.add(line);
	    }

        PPath tempPoly = PPath.createPolyline(points.toArray(new Point2D[points.size()])) ;
        tempPoly.closePath();


		for(Line2D l1:lineList){
			for(Line2D l2:lineList ){
				if(!l1.equals(l2)){
                   LineIntersection lineIntersetion = new LineIntersection(l1.getP1(),l1.getP2(),l2.getP1(),l2.getP2()) ;
                   Point2D intersection =  lineIntersetion.getInterPoint();

                    if(intersection!=null){
                       //log.info("intersection x:" + intersection.getX() + " y:" + intersection.getY());
                       Rectangle2D.Float rect = new Rectangle2D.Float((float)intersection.getX(), (float)intersection.getY(),1,1);
                       if(tempPoly.intersects(rect)){
                                //log.info("inside intersection");
                                return ShapeValidations.CROSSED_EDGE;
                       }
                   }
                }
			}
		}

		return ShapeValidations.VALID_SHAPE;
    }

    /**
     * Check arbitrary shape when drawing is incomplete
     * @param points
     * @return
     */
    public static ShapeValidations validateIncompleteShape(ArrayList<Point2D> points){
        ArrayList<Line2D.Double> lineList = new ArrayList<Line2D.Double>();
        for(int i=0;i<points.size()-1;i++){
			//log.info("index:" + i + "[ " + points.get(i).getX() + "," + points.get(i).getY());
			Line2D.Double line = new Line2D.Double(points.get(i),(points.get(i+1)));
			lineList.add(line);
		}

		PPath tempPoly = PPath.createPolyline(points.toArray(new Point2D[points.size()])) ;
        tempPoly.closePath();

		for(Line2D l1:lineList){
			for(Line2D l2:lineList ){
				if(!l1.equals(l2)){
                   LineIntersection lineIntersetion = new LineIntersection(l1.getP1(),l1.getP2(),l2.getP1(),l2.getP2()) ;
                   Point2D intersection =  lineIntersetion.getInterPoint();

                    if(intersection!=null){
                       //log.info("intersection x:" + intersection.getX() + " y:" + intersection.getY());
                       Rectangle2D.Float rect = new Rectangle2D.Float((float)intersection.getX(), (float)intersection.getY(),1,1);
                       if(tempPoly.intersects(rect)){
                                //log.info("inside intersection");
                                return ShapeValidations.CROSSED_EDGE;
                       }
                   }
                }
			}
		}

		return ShapeValidations.VALID_SHAPE;
    }
}
