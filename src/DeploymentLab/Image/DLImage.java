package DeploymentLab.Image;

import java.awt.*;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Vector;

import javax.imageio.ImageIO;

import DeploymentLab.CentralCatalogue;
import DeploymentLab.channellogger.Logger;


/**
 * DLImage is a cache-backed image container. It is similar to {@link java.awt.image.VolatileImage} in the
 * sense that the memory may be reclaimed by the garbage collector when necessary. However, it is not quite as volatile
 * as that and will attempt to keep the image in memory as long as it can as long as there is memory to spare.
 *
 * The image is cached to disk as a backup. If the memory is reclaimed by the garbage collector, it will be reloaded
 * from the cached source the next time it is referenced.
 *
 * This class implements the same interfaces as a {@link BufferedImage} and can be used as a memory sensitive
 * replacement in most places. I say "most" because @#$! Sun's AWT package has been known to hardcode logic that
 * assumes the BufferedImage concrete class. In those cases, DLImage provides {@link #getImage()} that returns a
 * BufferedImage.
 *
 * @author Ken Scoggins
 * @since Jupiter
 */
public class DLImage extends Image implements WritableRenderedImage, Transparency {
    private static final Logger log = Logger.getLogger(DLImage.class.getName());

    /** Use a Soft Reference wrapper around the actual image. This allows the GC to reclaim it as necessary. */
    private SoftReference<BufferedImage> imageRef = null;

    /** The location of the cached image. */
    private File imageFile = null;


    /**
     * Constructs a new instance from the provided image file. The image is actually lazy loaded and will not consume
     * memory until the first time it is referenced.
     *
     * @param source  The file containing the image.
     *
     * @throws IOException  If the file does not exist or could not be read.
     */
    public DLImage( Path source ) throws IOException {
        log.debug("New DLImage from file: " + source, "default");

        // At least make sure the file exists.
        if( !Files.isReadable( source ) || Files.isDirectory( source ) ) {
            throw new IOException( String.format( CentralCatalogue.getUIS("image.readError"), source ) );
        }

	    // Make sure it isn't too big.
	    try {
		    if( !ARGBConverter.isReadableSizeImage( source.toFile() ) ) {
			    throw new IOException( CentralCatalogue.formatUIS( "image.tooBig",
			                                                       ARGBConverter.getImagePixelSize( source.toFile() ),
			                                                       ARGBConverter.MAX_IMAGE_SIZE_PIXELS ) );
		    }
	    } catch( UnsupportedOperationException e ) {
		    // Probably just a zip file system, which means it's a DLZ and we checked the size when it was originally created anyway.
		    log.trace("Skipping image size check for file system: " + source.getFileSystem() );
	    }

        // Create a copy of the file as our cached version so we don't depend on the original.
        // Note: createTempFile creates an actual file, so REPLACE is needed on copy. If only they had an exposed a createTempFileName.
        imageFile = File.createTempFile("img" + source.hashCode(), ".png");
        Files.copy( source, imageFile.toPath(), StandardCopyOption.REPLACE_EXISTING );
        imageFile.deleteOnExit();

        // We used to lazy load this, but then we don't catch corrupt images until later when we want to handle them here.
        getImage();
    }


    /**
     * Constructs a new instance from the provided image file. The image is actually lazy loaded and will not consume
     * memory until the first time it is referenced.
     *
     * @param source  The file containing the image.
     *
     * @throws IOException  If the file does not exist or could not be read.
     */
    public DLImage( File source ) throws IOException {
        this( source.toPath() );
    }


    /**
     * Constructs a new instance using the provided image. The image will be cached in non-volatile memory to support the
     * ability for the local reference to be reclaimed by the garbage collector.
     *
     * @param source  The source image wrapped by this class.

     * @throws IOException  If the image cache could not be created.
     */
    public DLImage( BufferedImage source ) throws IOException {
        log.debug("New DLImage from BufferedImage: " + source, "default");

	    // Make sure it isn't too big.
	    if( !ARGBConverter.isReadableSizeImage( source.getWidth(), source.getHeight() ) ) {
		    throw new IOException( CentralCatalogue.formatUIS( "image.tooBig",
		                                                       (source.getHeight() * source.getWidth()),
		                                                       ARGBConverter.MAX_IMAGE_SIZE_PIXELS ) );
	    }

        // Save the image to a temp file for later loading, if needed.
        imageFile = File.createTempFile( "img" + source.hashCode(), ".png");
        ImageIO.write( source, "png", imageFile );
        imageFile.deleteOnExit();

        imageRef = new SoftReference<BufferedImage>( source );
    }


    /**
     * Constructs an empty instance of one of the predefined image types.  The <code>ColorSpace</code> for the image is
     * the default sRGB space.
     *
     * @param width     Width of the created image.
     * @param height    Height of the created image.
     * @param imageType Type of the created image. Must be one of the BufferedImage types.
     *
     * @see java.awt.color.ColorSpace
     * @see BufferedImage#TYPE_INT_RGB
     * @see BufferedImage#TYPE_INT_ARGB
     * @see BufferedImage#TYPE_INT_ARGB_PRE
     * @see BufferedImage#TYPE_INT_BGR
     * @see BufferedImage#TYPE_3BYTE_BGR
     * @see BufferedImage#TYPE_4BYTE_ABGR
     * @see BufferedImage#TYPE_4BYTE_ABGR_PRE
     * @see BufferedImage#TYPE_BYTE_GRAY
     * @see BufferedImage#TYPE_USHORT_GRAY
     * @see BufferedImage#TYPE_BYTE_BINARY
     * @see BufferedImage#TYPE_BYTE_INDEXED
     * @see BufferedImage#TYPE_USHORT_565_RGB
     * @see BufferedImage#TYPE_USHORT_555_RGB
     *
     * @throws IOException  If the image cache could not be created.
     */
    public DLImage( int width, int height, int imageType ) throws IOException {
        this( new BufferedImage( width, height, imageType ) );
    }


    /**
     * Checks to see if the image is already in memory or would need to be loaded from the cache the next time it is
     * referenced.
     *
     * @return  True if the image is in memory.
     */
    public boolean isInMemory() {
        return ( imageRef != null ) && ( imageRef.get() != null );
    }

    
    /**
     * Returns the BufferedImage that is wrapped by this class. Since our local reference is soft, this method will load
     * the image from the file if it is null.
     *
     * @return  The internally wrapped image.
     */
    public BufferedImage getImage() throws IOException {
        if( ( imageRef == null ) || ( imageRef.get() == null ) ) {
            log.debug("Loading image from cache (no " + ((imageRef == null) ? "ref" : "image") + "): " + imageFile, "default");
            BufferedImage img = loadImage( imageFile );
            if( img != null ) {
                imageRef = new SoftReference<BufferedImage>( img );
            }
        }
        return ( (imageRef == null) ? null : imageRef.get() );
    }


    /**
     * Loads an image from the provided file.
     *
     * @param imageFile  The file containing the image.
     *
     * @return  The image read from the file.
     */
    protected BufferedImage loadImage( File imageFile ) throws IOException {
        BufferedImage newBg = null;

        try {
            // check image size before loading it
            if( !ARGBConverter.isReadableSizeImage( imageFile ) ) {
                log.info( CentralCatalogue.formatUIS( "image.tooBig",
                                                      ARGBConverter.getImagePixelSize( imageFile ),
                                                      ARGBConverter.MAX_IMAGE_SIZE_PIXELS ), "message" );
            } else {
                newBg = ImageIO.read( imageFile );
                if( newBg == null ) {
                    log.error( CentralCatalogue.formatUIS("image.readError", imageFile.getPath()), "message" );
                } else {
                    newBg = ARGBConverter.convert( newBg );
                }
            }
        } catch( Exception e ) {
            log.error( CentralCatalogue.formatUIS("image.readError", imageFile.getPath()) );
            log.error( log.getStackTrace(e), "default" );
            throw e;
        }

        return newBg;
	}


    /**
     * Returns a <code>String</code> representation of this image and its values.
     *
     * @return  A <code>String</code> representing this image.
     */
    public String toString() {
        // The same as BufferedImage
        try {
            return this.getClass().getName() + "@"+ Integer.toHexString(hashCode())
               + ": type = " + getImage().getType() + " " + getImage().getColorModel() + " " + getImage().getRaster();
        } catch( Exception e ) {
            return "[image load error]";
        }
    }


    //////////////////////     Interface impls that just pass-through to the wrapped image.      ///////////////////////

    /**
     * Returns the image type.  If it is not one of the known types, TYPE_CUSTOM is returned.
     *
     * @return the image type of this <code>DLImage</code>.
     *
     * @see BufferedImage#TYPE_INT_RGB
     * @see BufferedImage#TYPE_INT_ARGB
     * @see BufferedImage#TYPE_INT_ARGB_PRE
     * @see BufferedImage#TYPE_INT_BGR
     * @see BufferedImage#TYPE_3BYTE_BGR
     * @see BufferedImage#TYPE_4BYTE_ABGR
     * @see BufferedImage#TYPE_4BYTE_ABGR_PRE
     * @see BufferedImage#TYPE_BYTE_GRAY
     * @see BufferedImage#TYPE_BYTE_BINARY
     * @see BufferedImage#TYPE_BYTE_INDEXED
     * @see BufferedImage#TYPE_USHORT_GRAY
     * @see BufferedImage#TYPE_USHORT_565_RGB
     * @see BufferedImage#TYPE_USHORT_555_RGB
     * @see BufferedImage#TYPE_CUSTOM
     */
    public int getType() {
        try {
            return getImage().getType();
        } catch( Exception e ) {
            return BufferedImage.TYPE_CUSTOM;
        }
    }

    @Override
    public int getWidth( ImageObserver observer ) {
        try {
            return getImage().getWidth(observer);
        } catch( Exception e ) {
            return 0;
        }
    }

    @Override
    public int getHeight( ImageObserver observer ) {
        try {
            return getImage().getHeight(observer);
        } catch( Exception e ) {
            return 0;
        }
    }

    @Override
    public ImageProducer getSource() {
        try {
            return getImage().getSource();
        } catch( Exception e ) {
            return null;
        }
    }

    @Override
    public Graphics getGraphics() {
        try {
            return getImage().getGraphics();
        } catch( Exception e ) {
            return null;
        }
    }

    @Override
    public Object getProperty( String name, ImageObserver observer ) {
        try {
            return getImage().getProperty(name, observer);
        } catch( Exception e ) {
            return null;
        }
    }

    @Override
    public void addTileObserver( TileObserver to ) {
        try {
            getImage().addTileObserver( to );
        } catch( Exception e ) {}
    }

    @Override
    public void removeTileObserver( TileObserver to ) {
        try {
            getImage().removeTileObserver(to);
        } catch( Exception e ) {}
    }

    @Override
    public WritableRaster getWritableTile( int tileX, int tileY ) {
        try {
            return getImage().getWritableTile( tileX, tileY );
        } catch( Exception e ) {
            return null;
        }
    }

    @Override
    public void releaseWritableTile( int tileX, int tileY ) {
        try {
            getImage().releaseWritableTile( tileX, tileY );
        } catch( Exception e ) {}
    }

    @Override
    public boolean isTileWritable( int tileX, int tileY ) {
        try {
            return getImage().isTileWritable( tileX, tileY );
        } catch( Exception e ) {
            return false;
        }
    }

    @Override
    public Point[] getWritableTileIndices() {
        try {
            return getImage().getWritableTileIndices();
        } catch( Exception e ) {
            return null;
        }
    }

    @Override
    public boolean hasTileWriters() {
        try {
            return getImage().hasTileWriters();
        } catch( Exception e ) {
            return false;
        }
    }

    @Override
    public void setData( Raster r ) {
        try {
            getImage().setData( r );
        } catch( Exception e ) {}
    }

    @Override
    public Vector<RenderedImage> getSources() {
        try {
            return getImage().getSources();
        } catch( Exception e ) {
            return null;
        }
    }

    @Override
    public Object getProperty( String name ) {
        try {
            return getImage().getProperty(name);
        } catch( Exception e ) {
            return null;
        }
    }

    @Override
    public String[] getPropertyNames() {
        try {
            return getImage().getPropertyNames();
        } catch( Exception e ) {
            return null;
        }
    }

    @Override
    public ColorModel getColorModel() {
        try {
            return getImage().getColorModel();
        } catch( Exception e ) {
            return null;
        }
    }

    @Override
    public SampleModel getSampleModel() {
        try {
            return getImage().getSampleModel();
        } catch( Exception e ) {
            return null;
        }
    }

    @Override
    public int getWidth() {
        try {
            return getImage().getWidth();
        } catch( Exception e ) {
            return 0;
        }
    }

    @Override
    public int getHeight() {
        try {
            return getImage().getHeight();
        } catch( Exception e ) {
            return 0;
        }
    }

    @Override
    public int getMinX() {
        try {
            return getImage().getMinX();
        } catch( Exception e ) {
            return 0;
        }
    }

    @Override
    public int getMinY() {
        try {
            return getImage().getMinY();
        } catch( Exception e ) {
            return 0;
        }
    }

    @Override
    public int getNumXTiles() {
        try {
            return getImage().getNumXTiles();
        } catch( Exception e ) {
            return 0;
        }
    }

    @Override
    public int getNumYTiles() {
        try {
            return getImage().getNumYTiles();
        } catch( Exception e ) {
            return 0;
        }
    }

    @Override
    public int getMinTileX() {
        try {
            return getImage().getMinTileX();
        } catch( Exception e ) {
            return 0;
        }
    }

    @Override
    public int getMinTileY() {
        try {
            return getImage().getMinTileY();
        } catch( Exception e ) {
            return 0;
        }
    }

    @Override
    public int getTileWidth() {
        try {
            return getImage().getTileWidth();
        } catch( Exception e ) {
            return 0;
        }
    }

    @Override
    public int getTileHeight() {
        try {
            return getImage().getTileHeight();
        } catch( Exception e ) {
            return 0;
        }
    }

    @Override
    public int getTileGridXOffset() {
        try {
            return getImage().getTileGridXOffset();
        } catch( Exception e ) {
            return 0;
        }
    }

    @Override
    public int getTileGridYOffset() {
        try {
            return getImage().getTileGridYOffset();
        } catch( Exception e ) {
            return 0;
        }
    }

    @Override
    public Raster getTile( int tileX, int tileY ) {
        try {
            return getImage().getTile(tileX, tileY);
        } catch( Exception e ) {
            return null;
        }
    }

    @Override
    public Raster getData() {
        try {
            return getImage().getData();
        } catch( Exception e ) {
            return null;
        }
    }

    @Override
    public Raster getData( Rectangle rect ) {
        try {
            return getImage().getData( rect );
        } catch( Exception e ) {
            return null;
        }
    }

    @Override
    public WritableRaster copyData( WritableRaster raster ) {
        try {
            return getImage().copyData( raster );
        } catch( Exception e ) {
            return null;
        }
    }

    @Override
    public int getTransparency() {
        try {
            return getImage().getTransparency();
        } catch( Exception e ) {
            return 0;
        }
    }
}
