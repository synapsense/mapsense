package DeploymentLab.Image;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import DeploymentLab.Model.DLComponent;


/**
 * DLZoneImage uses a base image as the source and crops it to be just large enough to contain the children of the
 * associated component.
 *
 * Memory management is similar to {@link java.awt.image.VolatileImage} in the sense that the memory may be reclaimed by
 * the garbage collector when necessary. However, it is not quite as volatile as that and will attempt to keep the image
 * in memory as long as it can as long as there is memory to spare.
 *
 * If the memory is reclaimed by the garbage collector, it will be reloaded from the source the next time it is
 * referenced.
 *
 * @author Ken Scoggins
 * @since Jupiter
 */
public class DLZoneImage extends DLImage {

    /** The component associated with this image. */
    private DLComponent component = null;


    /**
     * Creates a new instance from the provided image file. The image is actually lazy loaded and will not consume
     * memory until the first time it is referenced.
     *
     * @param baseImageFile  The file containing the image.
     * @param component      The component whose children are used to crop the image.
     *
     * @throws IOException  If the file does not exist or could not be read.
     */
    public DLZoneImage( File baseImageFile, DLComponent component ) throws IOException {
        super( baseImageFile );
        this.component = component;
    }


    /**
     * Creates a new instance using the provided image. The image will be cached in non-volatile memory to support the
     * ability for the local reference to be reclaimed by the garbage collector.
     *
     * @param baseImage  The source image wrapped by this class.
     * @param component  The component whose children are used to crop the image.

     * @throws IOException  If the image cache could not be created.
     */
    public DLZoneImage( BufferedImage baseImage, DLComponent component ) throws IOException {
        super( baseImage );
        this.component = component;
    }


    /**
     * Returns the cropped version of the source image.
     *
     * @return  The image.
     */
    public BufferedImage getImage() throws IOException {
        BufferedImage fullImage = super.getImage();

        // todo: Create the cropped image and return that.
        return fullImage;
    }
}
