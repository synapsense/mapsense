package DeploymentLab.Image;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.io.*;
import java.util.Iterator;
import java.util.NoSuchElementException;

import DeploymentLab.Model.DLComponent;
import DeploymentLab.Model.PropertyConverter;
import DeploymentLab.Pair;
import DeploymentLab.channellogger.*;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileCacheImageInputStream;
import javax.imageio.stream.ImageInputStream;

import com.synapsense.dto.BinaryData;
import org.apache.commons.codec.binary.Base64;

/**
 * Helper class to convert and handle images.
 */
public class ARGBConverter {
	private static final Logger log = Logger.getLogger(ARGBConverter.class.getName());

	public static final int MAX_IMAGE_SIZE_PIXELS = 67108864;
	public static final int MAX_IMAGE_SIZE_BYTES = 268435456;


	/**
	 * Returns the height and width of the image in the provided file.
	 *
	 * @param f  The image file.
	 * @return   The height and width, stored in the Pair class. Pair.a is he height and Pair.b is the width.
	 */
	public static Pair<Integer, Integer> getImageHeightWidth( File f ) {
		Pair<Integer, Integer> size = new Pair<>( 0, 0 );
		try {
			ImageInputStream ins = ImageIO.createImageInputStream(f);
			Iterator readers = ImageIO.getImageReaders(ins);
			if (readers.hasNext()) {
				ImageReader reader = (ImageReader) readers.next();
				reader.setInput(ins);
				size = new Pair<>( reader.getHeight(0), reader.getWidth(0) );
				reader.dispose();
			}
			ins.close();
		} catch (IOException e) {
			log.error(log.getStackTrace(e), "default");
		}
		return size;
	}

	/**
	 * Returns the number of pixels for the image in the provided file.
	 *
	 * @param f  The image file.
	 * @return   The number of pixels, defined as the image height x width.
	 */
	public static int getImagePixelSize( File f ) {
		Pair<Integer,Integer> size = getImageHeightWidth( f );
		return (size.a * size.b);
	}

	/**
	 * Check to see if the given image file is small enough to be read
	 * @param f a File holding an image we want to look at
	 * @return true if the image is less than MAX_IMAGE_SIZE_PIXELS total pixels (which is MAX_IMAGE_SIZE_BYTES bytes in ARGB)
	 */
	public static boolean isReadableSizeImage(File f){
		int height =0;
		int width=0;

		try {
			ImageInputStream ins = ImageIO.createImageInputStream(f);
			Iterator readers = ImageIO.getImageReaders(ins);
	        if (readers.hasNext()) {
	        	ImageReader reader = (ImageReader) readers.next();
                reader.setInput(ins);
                height = reader.getHeight(0);
                width = reader.getWidth(0);
                reader.dispose();
	        }
			ins.close();
		} catch (IOException e) {
			log.error(log.getStackTrace(e), "default");
		}

		return isReadableSizeImage( width, height );
	}

	/**
	 * Check to see if the given image size is small enough to be read
	 * @param width the width of the image
	 * @param height the height of the image
	 * @return true if the image is less than MAX_IMAGE_SIZE_PIXELS total pixels (which is MAX_IMAGE_SIZE_BYTES bytes in ARGB)
	 */
	public static boolean isReadableSizeImage( int width, int height ){
		return ((height * width) < MAX_IMAGE_SIZE_PIXELS);
	}

	/**
	 * Converts a BufferedImage to TYPE_INT_ARGB, insuring that we know what type the backing array is.
	 * @param source any BufferedImage
	 * @return a BufferedImage containing the same image as source, but stored as TYPE_INT_ARGB.
	 */
	public static BufferedImage convert( BufferedImage source ){
		/*
		bufferedImage types:
		0 = custom
		2 = int_argb
		5 = 3byte_bgr
		6 = 4byte_bgr

		databuffer types:
		0 = byte
		3 = int

		*/

		log.trace( "Source type is " + source.getType(), "default"  );
		log.trace( "databuffer class is " + source.getData().getDataBuffer().getClass().getName()  ,"default"  );
		log.trace( "databuffer type is " + source.getData().getDataBuffer().getDataType()  ,"default"  );

		if ( source.getType() == BufferedImage.TYPE_INT_ARGB && source.getData().getDataBuffer().getDataType() == DataBuffer.TYPE_INT ){
			//this is what we want, so don't bother doing the conversion.
			return source;
		}

		BufferedImage result = new BufferedImage(source.getWidth(null), source.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		final Graphics2D g2 = result.createGraphics();
        //g2.drawImage(source, 0, 0, null);
		//use the "faster" graphics2d pipeline by calling in this order:
		g2.drawImage(source, null, 0, 0);
        g2.dispose();

		log.trace( "result type is " + result.getType(), "default"  );
		log.trace( "databuffer class is " + result.getData().getDataBuffer().getClass().getName()  ,"default"  );
		log.trace( "databuffer type is " + result.getData().getDataBuffer().getDataType()  ,"default"  );

		return result;
	}


	public static BufferedImage loadImageFile( File imageFile ) throws IOException {
		BufferedImage i = null;
		int h = 0;
		int w = 0;

		log.trace("in load image file", "default");

//		try {
			ImageInputStream ins = ImageIO.createImageInputStream(imageFile);
			//FileCacheImageInputStream ins = new FileCacheImageInputStream( new FileInputStream(imageFile) , null);

			Iterator readers = ImageIO.getImageReaders(ins);
	        if (readers.hasNext()) {
	        	ImageReader reader = (ImageReader) readers.next();
                reader.setInput(ins, true);
                h = reader.getHeight(0);
                w = reader.getWidth(0);

				log.trace("reading image with width " + w + "and height " + h, "default");

				ImageReadParam p = reader.getDefaultReadParam();
				BufferedImage target = new BufferedImage( w, h , BufferedImage.TYPE_INT_ARGB );

				log.trace(" target is  " + target.toString(), "default");

				p.setDestination(target);
				i = reader.read(0, p);


				log.trace( "after read, i is  " + i.toString(), "default");
				log.trace( "after read, width = " + target.getWidth(), "default" );

                reader.dispose();
	        }
			ins.close();
//		} catch (IOException e) {
//			log.error(log.getStackTrace(e), "default");
//		}


		return i;
	}

    public static void loadComponentImage( DLComponent c )  throws IOException {
		loadComponentImage( c, c.getComponentProperty("image_data").getBase64Image() );
    }

	/**
	 * Takes a component (most likely a drawing, and installs the image property
	 * @param c
     * @param value  Base64 image string.
	 * @throws IOException if the string can't be successfully decoded as a base64 encoded png, or if the cache file can't be made
	 */
	public static void loadComponentImage( DLComponent c, String value )  throws IOException {
		FileCacheImageInputStream input = null;
		ImageReader reader = null;
		BufferedImage i;

        if( value.isEmpty() ) {
            //Nothing to do.
            return;
        }

		try{

			log.trace("Loading Component Image from Base64 Value", "default");

			input =  new FileCacheImageInputStream( new ByteArrayInputStream( Base64.decodeBase64(value.getBytes()) ), null );
			reader = ImageIO.getImageReaders(input).next();
			reader.setInput(input, true);
			ImageReadParam p = reader.getDefaultReadParam();
			//the stream will tell us the size of the image before we load it, so we can set the target size right
			Integer w = reader.getWidth(0);
			Integer h = reader.getHeight(0);
			BufferedImage target = new BufferedImage( w, h , BufferedImage.TYPE_INT_ARGB );
			p.setDestination(target);
			i = reader.read(0, p);

		} catch ( IllegalArgumentException iae ){
			log.warn("Was unable to deserialize image directly, must do conversion.", "default");
			i = loadImageString(value);

		} catch ( NoSuchElementException nse ){
			log.warn("Was unable to find a reader, must do conversion.", "default");
			i = loadImageString(value);

		} finally{
			if ( reader != null ){
				reader.dispose();
			}
			if ( input != null ){
				input.close();
			}
		}
        // todo: MULTIDC - Investigate the voodoo above to see if it can be encapsulated in DLImage. For now, just create DLImage here after the fact.
		c.setPropertyValue("image_data", new DLImage( i ));

		c.getComponentProperty("image_data").finishImage();
		//let's just make sure the GC knows it can flush the image
		i = null;
		//reader.dispose();
		//input.close();
	}


	/**
	 * The "old style" load, where we load the image and then convert to the format we want.
	 * @param value
	 * @return
	 * @throws IOException
	 */
	public static BufferedImage loadImageString( String value ) throws IOException {
		BufferedImage temp = ImageIO.read(
			new ByteArrayInputStream(
				Base64.decodeBase64(value.getBytes())
			)
		);

        if (temp == null){ return null; }
		//turn temp into .type_int_arbg
		BufferedImage result = ARGBConverter.convert(temp);
		return result;
	}

	/**
	 * @param zis an InputStream, presumably from the insides of the dlz file
	 * @return an INT_ARGB BufferedImage holding the image from input
	 * @throws IOException if this doesn't work
	 */
	public static BufferedImage loadZipImageStream( InputStream zis )  throws IOException {
		FileCacheImageInputStream input = null;
		ImageReader reader = null;
		BufferedImage i;

		try{
			log.trace("Loading Component Image from ZipFile input stream", "default");
			input =  new FileCacheImageInputStream( zis , null );
			reader = ImageIO.getImageReaders(input).next();
			reader.setInput(input, true);
			ImageReadParam p = reader.getDefaultReadParam();
			//the stream will tell us the size of the image before we load it, so we can set the target size right
			Integer w = reader.getWidth(0);
			Integer h = reader.getHeight(0);
			BufferedImage target = new BufferedImage( w, h , BufferedImage.TYPE_INT_ARGB );
			p.setDestination(target);
			i = reader.read(0, p);

		} catch ( IllegalArgumentException iae ){
			log.warn("Was unable to deserialize image directly, must do conversion.", "default");
			i =  convert( ImageIO.read( input ) );

		} catch ( NoSuchElementException nsee){
			log.warn("Was unable to find a a reader to deserialize image directly, will try the hard way.", "default");
			i =  convert( ImageIO.read( input ) );

		} finally{
			if ( reader != null ){
				reader.dispose();
			}
			if ( input != null ){
				input.close();
			}
		}
		return i;
	}

	/**
	 * The old style of loading images.  Takes any inputstream that has an image in it, uses the default reader to read it in,
	 * and then converts to the INT_ARGB format afterwards.
	 * While this works on darn near everything, this will cause the system to hold two copies of the image (plus conversion overhead)
	 * in almost all cases.
	 * @param input any InputStream holding image data
	 * @return an INT_ARGB BufferedImage holding the image from input
	 * @throws IOException if this doesn't work
	 */
	public static BufferedImage basicLoadInputStream(InputStream input) throws IOException{
		return convert( ImageIO.read( input ) );
	}

	/**
	 * Converts the BinaryData blob (as stored in the object model, or wherever, back into a buffered image
	 * @param source
	 * @return
	 */
	public static BufferedImage binaryDataToBufferedImage(BinaryData source) throws IOException{
		if (source == null) { return null; }
		return loadZipImageStream(  new BufferedInputStream( new ByteArrayInputStream(source.getValue()) ) );
	}

}
