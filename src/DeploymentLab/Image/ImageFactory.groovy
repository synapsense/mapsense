package DeploymentLab.Image

import java.awt.image.BufferedImage
import java.awt.Graphics
import java.awt.Color
import DeploymentLab.CentralCatalogue
import DeploymentLab.Model.DLComponent
import DeploymentLab.channellogger.Logger

/**
 * Generates images out of whole cloth.  Migrated from DLW.
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 10/5/12
 * Time: 12:26 PM
 */
class ImageFactory {
	private static final Logger log = Logger.getLogger(ImageFactory.class.getName())

	public static DLImage getBlankBgImage(){
		return getBlankBgImage(Color.WHITE, 255)
	}

	public static DLImage getBlankBgImage(DLComponent drawing){
		return getBlankBgImage(drawing.getPropertyValue("color"), drawing.getPropertyValue("alpha"))
	}

	public static DLImage getBlankBgImage(Color backgroundColor, Integer alpha) {
		// let's make a fake image (1000x1000)
		log.debug("FACTORIES ARE HARD AT WORK")
		int width = Integer.parseInt(CentralCatalogue.getApp("virtuallayer.bg.width"))
		int height = Integer.parseInt(CentralCatalogue.getApp("virtuallayer.bg.height"))

		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)
		Graphics g = bi.getGraphics()
		//g.setColor(Color.white)
		Color colorToUse = new Color(backgroundColor.getRed(), backgroundColor.getGreen(), backgroundColor.getBlue(), alpha)
		g.setColor(colorToUse)
		g.fillRect(0, 0, width, height)

		DLImage bgImage = new DLImage(bi)
		return bgImage
	}

}
