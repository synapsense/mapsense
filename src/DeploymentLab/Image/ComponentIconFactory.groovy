package DeploymentLab.Image

import DeploymentLab.ComponentLibrary
import DeploymentLab.Model.DLComponent
import DeploymentLab.channellogger.Logger
import java.awt.Image
import javax.imageio.ImageIO
import javax.swing.ImageIcon

/**
 * Generates ImageIcons for use in Swing for components.
 *
 * The factory now checks for the presence of a .png icon with the same name as the shape in the resources directory.
 * Only if that isn't found will the hard-coded names be used.  In the future, we should transition out of using the hard-coded names. 
 *
 */
public class ComponentIconFactory {
	static private iconMap = [:]
	private static final Logger log = Logger.getLogger(ComponentIconFactory.class.getName())

	public static final int SMALL = 16
	public static final int MEDIUM = 24
	public static final int LARGE = 48

	/**
	 * Default factory method, tries to find a 16x16 icons for this component, and returns the default if it can't find one.
	 * The same as calling getComponentIcon(ComponentIconFactory.SMALL)
	 * @param dlc an Object to find a representation for
	 * @return an ImageIcon, ready for use
	 */
	static ImageIcon getComponentIcon(Object dlc) {
		return getComponentIcon(dlc, SMALL)
	}

	/**
	 * Returns an ImageIcon to be used to represent <code>dlc</code>.  This will always return some kind of icon.
	 * @param dlc an Object to find a representation for
	 * @param size the requested size of the icon
	 * @return an ImageIcon, ready for use
	 */
	static ImageIcon getComponentIcon(Object dlc, int size) {
		String type = null

		if (dlc instanceof DLComponent) {
			if (dlc.getShape() != null) {
				def icn = null;
				def clib = ComponentLibrary.INSTANCE;
				InputStream zis = clib.getStreamFromLibraryFile("${dlc.getShape()}_${size}.png")
				if (zis != null) {
					icn = getImageIconFromStream("${dlc.getShape()}_${size}.png", zis)
				}
				if (icn == null) {
					//if we have a shape we can use, just return that and bail out
					//def icn = getImageIcon("/resources/${dlc.getShape()}.png" )
					icn = getImageIcon("/resources/components/${dlc.getShape()}_${size}.png")
				}
				if (icn != null) {
					return icn
				}
			}
			type = dlc.getType()
		} else if (dlc instanceof String) {
			type = dlc
		}

		if (type != null) {
			//well, there wasn't a shape we could use, so we need to go to the lookup table 
			///def type = dlc.getType()
			switch (type) {
				//groupings:
				case 'zone':
					return (getImageIcon("/resources/add_li_zone.png"))
					//break
				case 'logicalzone':
					return (getImageIcon("/resources/add_logical_group.png"))
					//break
				case 'planninggroup':
					return (getImageIcon("/resources/add_planning_group.png"))
					//break

				//Data sources:
				case 'network':
					return (getImageIcon("/resources/DS_WSN.png"))
					//break
				case 'modbus-tcp-network':
				//case 'modbus-rtu-network':
					return (getImageIcon("/resources/DS_MODBUS.png"))
					//break

				case 'snmp-v3-agent':
				case 'snmp-v2c-agent':
				case 'snmp-v1-agent':
					return (getImageIcon("/resources/DS_SNMP.png"))
					//break
				case 'bacnetnetwork':
					return (getImageIcon("/resources/DS_BACNET.png"))
					//break
				case 'lonnetwork':
					return (getImageIcon("/resources/DS_LONTALK.png"))
					//break

                case 'lontalk-router':
                    return (getImageIcon("/resources/DS_LONTALK.png"))
                    break
				case 'ipminetwork':
					return (getImageIcon("/resources/DS_IPMI.png"))
					break

				//other:
				case 'drawing':
					return (getImageIcon("/resources/office_16.png"))
					//break

				// Below are the deprecated ones. Added here to support upgrade scenarios.
				case 'soapservice':
					return (getImageIcon("/resources/DS_SOAP.png"))
					//break
				case 'database':
					return (getImageIcon("/resources/DS_DB.png"))
					//break
				case 'webservicehost':
					return (getImageIcon("/resources/DS_WebServices.png"))
					//break

				default:
					return (getDefaultIcon(size))
			}

		} else if (dlc instanceof IconNameProvider) {
			def icon = getImageIcon("/resources/" + dlc.getIconName())
			if (icon) {
				return icon
			}
		}

		//If we've made it this far, we have NO IDEA what this thing is.
		//return the default icon for things that are NOT a DLComponent
		return (getDefaultIcon(size))
	}

	static public ImageIcon getShapeIcon(String shapeName) {
		return getShapeIcon(shapeName, SMALL)
	}

	/**
	 * Finds the correct image icon to go with a given SVG shape
	 * @param shapeName
	 * @param
	 * @return
	 */
	static public ImageIcon getShapeIcon(String shapeName, int iconSize) {
		def icn = getImageIcon("/resources/components/${shapeName}_${iconSize}.png")
		if (icn == null) {
			icn = getDefaultIcon(iconSize)
		}
		return icn
	}

	/**
	 * Fetches an Icon out of the resources bucket based on the name of the icon.
	 * @param name the name of the icon (without an extension)
	 * @return an ImageIcon, or null if the icon cannot be found
	 */
	static public ImageIcon getIconByName(String name) {
		return getImageIcon("/resources/${name}.png")
	}

	/** Returns an ImageIcon based on path, or the default icon if the path was invalid.
	 *  Ideally, we should be using this for all ImageIcons in the app, so that we get to take advantage of the cache. 
	 */
	static public ImageIcon getImageIcon(String path) {
		if (iconMap.containsKey(path)) {
			return (iconMap[path])
		} else {
			java.net.URL imgURL = ComponentIconFactory.class.getResource(path);
			if (imgURL != null) {
				def beebelbrox = new ImageIcon(imgURL);
				iconMap[path] = beebelbrox
				return beebelbrox
			} else {
				//System.err.println("Couldn't find file: " + path);
				log.error("Couldn't find Icon File $path")
				iconMap[path] = null
				return null
			}
		}
	}

	/**
	 * Returns an ImageIcon with the given name based on the InputStream.
	 * @param name
	 * @param source
	 * @return
	 */
	static private ImageIcon getImageIconFromStream(String name, InputStream source) {
		if (source == null) { return null; }

		if (iconMap.containsKey(name)) {
			return (iconMap[name])
		} else {
			try {
				Image img = ImageIO.read(source);
				def beebelbrox = new ImageIcon(img);
				iconMap[name] = beebelbrox
				return beebelbrox
			} catch (Exception e) {
				log.error("Couldn't find Icon File $name")
				iconMap[name] = null
				return null;
			}
		}
	}

	/**
	 * Convenience function to get the warning message icon.
	 * @return an Icon of the warning symbol.
	 */
	static public ImageIcon getWarningIcon() {
		return ComponentIconFactory.getImageIcon('/resources/warning_16.png')
	}

	/**
	 * Convenience function to get the information message icon.
	 * @return an Icon of the info symbol.
	 */
	static public ImageIcon getInfoIcon() {
		return ComponentIconFactory.getImageIcon('/resources/about_16.png')
	}

	/**
	 * Hands out the default icon to be used when there isn't anything better at the default size.
	 * Functionally identical to calling getDefaultIcon(ComponentIconFactory.SMALL)
	 * @return an ImageIcon of the default icon at small size
	 */
	static public ImageIcon getDefaultIcon() {
		return getDefaultIcon(ComponentIconFactory.SMALL)
	}

	/**
	 * Hands out the default icon to be used when there isn't anything better.
	 * @param size Should be one of ComponentIconFactory.SMALL, ComponentIconFactory.MEDIUM, or ComponentIconFactory.LARGE
	 * @return an ImageIcon at the requested size.
	 */
	static public ImageIcon getDefaultIcon(int size) {
		return (getImageIcon("/resources/network_${size}.png"))
	}

	static public ImageIcon getAppIcon(){
		return getImageIcon('/cfg/appicon.png')
	}

}