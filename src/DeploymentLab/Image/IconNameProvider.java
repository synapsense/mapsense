package DeploymentLab.Image;

/**
 * User: Gabriel Helman
 * Date: Oct 7, 2010
 * Time: 9:57:32 AM
 * Allows objects to indicate that they provide an icon name to use with the ComponentIconFactory.
 * If the factory is handed an object that implements this interface, it will use the result of getIconName as the icon to go find.
 *
 */
public interface IconNameProvider {

	/**
	 * Gets the resource name of the icon to use for this object.
	 * Object classes that will need to be displayed in the UI should implement this method an then use the ComponentIconFactory to get the icon itself.
	 * @return the name of an image to wrap in an icon.
	 */
	public String getIconName();

}
