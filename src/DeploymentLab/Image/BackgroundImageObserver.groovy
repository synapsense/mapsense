package DeploymentLab.Image

import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.MetamorphosisEvent
import DeploymentLab.Model.ModelChangeListener
import DeploymentLab.UIDisplay
import DeploymentLab.channellogger.Logger

/**
 * Updates the background image of a drawing if the user changes one of the image_data stats.
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 10/25/12
 * Time: 11:13 AM
 */
class BackgroundImageObserver implements ModelChangeListener {
	private static final Logger log = Logger.getLogger(BackgroundImageObserver.class.getName())

	private boolean kafka = false

	@Override
	void componentAdded(DLComponent component) {
		if (component.hasRole("drawing")) {
			component.addPropertyChangeListener(this, this.&componentPropertyChanged)
		}
	}

	@Override
	void componentRemoved(DLComponent component) {
		if (component.hasRole("drawing")) {
			component.removePropertyChangeListener(this)
		}
	}

	@Override
	void childAdded(DLComponent parent, DLComponent child) {}

	@Override
	void childRemoved(DLComponent parent, DLComponent child) {}

	@Override
	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	@Override
	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	@Override
	void modelMetamorphosisStarting(MetamorphosisEvent event) {
		kafka = true
	}

	@Override
	void modelMetamorphosisFinished(MetamorphosisEvent event) {
		kafka = false
	}

	void componentPropertyChanged(DLComponent who, String prop, oldVal, newVal) {
		if(kafka){return}

		if (((prop.equals("color") || prop.equals("alpha")) && !who.getPropertyValue("hasImage"))) {
			log.debug("the watcher is redecorating $who")
			//who.setPropertyValue('image_data', ImageFactory.getBlankBgImage(who))
			who.getModel().resetImages() //need to reset all images, not just the drawing
		} else if (prop.equals("hasImage")){
			UIDisplay.INSTANCE.setProperty('useBackgroundImage', newVal )
			UIDisplay.INSTANCE.setProperty('useCanvas', !newVal)
//			who.getComponentProperty("alpha").setDisplayed( !who.getPropertyValue("hasImage") )
//			who.getComponentProperty("color").setDisplayed( !who.getPropertyValue("hasImage") )
		}
	}

}
