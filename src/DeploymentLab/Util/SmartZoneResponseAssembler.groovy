package DeploymentLab.Util

import DeploymentLab.CentralCatalogue
import DeploymentLab.Export.ImageExporter
import DeploymentLab.Image.DLImage
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentType
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLObject
import DeploymentLab.Model.ObjectType
import DeploymentLab.SceneGraph.DeploymentPanel
import com.panduit.sz.api.shared.ByteArray
import com.panduit.sz.api.ss.assets.Floorplan
import com.panduit.sz.api.ss.assets.Rack

/**
 * Created by LNI on 1/11/2016.
 */
public final class SmartZoneResponseAssembler {

    static Floorplan fp

    private SmartZoneResponseAssembler() {

    }

    static Floorplan getFloorplan() {
        return fp
    }

    public static Floorplan buildFloorplan(final ComponentModel componentModel, final DLComponent eachDrawing, final File filePath) {

        DeploymentPanel dp = CentralCatalogue.getDeploymentPanel();
        String locName = eachDrawing.getPropertyValue('name')
        int szLocId = eachDrawing.getSmartZoneId()
        double widthInInches
        double heightInInches
        DLImage img
        ByteArray bg

        Boolean hasImage = eachDrawing.getPropertyValue('hasImage')
        if (hasImage) {
            img = eachDrawing.getPropertyValue('image_data')
        } else {
            img = new DLImage(dp.generateBackgroundImage( eachDrawing ))
        }
        bg = ByteArray.of(ImageExporter.getByteData(img.getImage()))
        widthInInches = getFloorplanWidth(eachDrawing)
        heightInInches = getFloorplanHeight(eachDrawing)

        List racks = new ArrayList()
        for (DLComponent eachComp : componentModel.getComponentsInDrawing(eachDrawing)) {
            if (eachComp.canSmartZoneLink() && eachComp.hasManagedObjectOfType(ObjectType.RACK)) {
                Rack rack = buildRack(eachComp)
                racks.add(rack)
            }
        }
        if (filePath) {
            // Linked mode
            fp = Floorplan.builder().id(szLocId).name(locName).width(widthInInches).height(heightInInches).background(bg).racks(racks.asImmutable()).lastSyncFilePath(filePath.getAbsolutePath()).build()
        } else {
            // Advanced editor mode
            fp = Floorplan.builder().id(szLocId).name(locName).width(widthInInches).height(heightInInches).background(bg).racks(racks.asImmutable()).build()
        }
        return fp
    }

    private static Rack buildRack(final DLComponent rackComponent) {
        String rackName = rackComponent.getPropertyValue('name')
        double rackWidth = rackComponent.getPropertyValue('width')
        double rackDepth = rackComponent.getPropertyValue('depth')
        double x = rackComponent.getPropertyValue('x')
        double y = rackComponent.getPropertyValue('y')
        double rackRotation = rackComponent.getPropertyValue('rotation')
        int rackId = rackComponent.getSmartZoneId()

        return Rack.builder().id(rackId).name(rackName).width(rackWidth).depth(rackDepth).location(com.panduit.sz.api.shared.Point2D.builder().x(x).y(y).build()).rotation(rackRotation).build()
    }

    /**
     * Returns the new width in natural units of the floorplan based on the scale.
     * @param eachDrawing
     * @return width in inches
     */
    private static double getFloorplanWidth(final DLComponent eachDrawing) {
        final double scale = eachDrawing.getPropertyValue('scale')
        final int widthInPixels = eachDrawing.getPropertyValue('width')
        return scale * widthInPixels
    }

    /**
     * Returns the new height in natural units of the floorplan based on the scale
     * @param eachDrawing
     * @return
     */
    private static double getFloorplanHeight(final DLComponent eachDrawing) {
        final double scale = eachDrawing.getPropertyValue('scale')
        final int heightInPixels = eachDrawing.getPropertyValue('height')
        return scale * heightInPixels
    }
}
