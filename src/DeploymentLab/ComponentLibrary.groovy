package DeploymentLab

import DeploymentLab.channellogger.*
import DeploymentLab.Model.*

import javax.xml.XMLConstants
import java.util.Properties
import java.util.zip.ZipFile
import java.util.zip.ZipEntry
import java.nio.file.Files
import java.nio.file.Paths
import org.apache.xml.resolver.tools.CatalogResolver

/**
 * Stores everything we want to know about loaded component libraries.
 */
public enum ComponentLibrary{
	INSTANCE;

	/**
	 *  The common catalog resolver to use when parsing the component XML files. Public just in case a custom parser is
	 *  used somewhere, but you should really just use the helper method {@link #parseXmlFile}
	 */
	public static CatalogResolver CATALOG_RESOLVER = new CatalogResolver()

	private static final Logger log = Logger.getLogger(ComponentLibrary.class.getName())
	private ComponentModel componentModel
	private Properties componentsProperties
	def paletteTreeModels = []
	def treeModels = []
	private String dirPath = ""
	private componentLibXmlMap = [:]  //filename : xml
	private List<String> loadedComponentTypes = []
	private List<File> loadedFiles = []



	private ComponentLibrary(){}

	public void init(ComponentModel cmodel, Object _paletteTreeModels, Object _treeModels){
		componentModel = cmodel
		paletteTreeModels = _paletteTreeModels
		treeModels = _treeModels
		componentsProperties = CentralCatalogue.getInstance().getComponentsProperties()
		dirPath = componentsProperties.getProperty('components.path')
		componentModel.setCLib(this)
	}

	/**
	 * Scans the component tree and removes anything that is no longer loaded.
	 */
	public void pruneExtinctComponents() {
		treeModels.each{m-> m.pruneConfigurations()}
	}

	public void loadDefaultComponents(){
		String[] componentLibraries = componentsProperties.getProperty('componentLibraries.list').split(",")
		log.debug("Loading Default Components")
		for(String componentLibrary: componentLibraries){
			String filePath = dirPath + componentLibrary
			def componentLibraryXml = parseXmlFile( filePath )
			componentLibraryXml.component.each{componentXml->
				componentModel.loadComponent(componentXml)
				loadConfigurationToPaletteTreeModel(componentXml)
				loadConfigurationToTreeModel(componentXml)
			}
		}
	}

	/**
	 * Shortcut for the XmlSlurper pattern that uses an entity resolver to properly handle our XML files that use custom
	 * entities defined in external DTDs. It will first attempt to read the file directly. If it is not accessible, it
	 * will use the Class Loader to read the file.
	 *
	 * @param filename  The XML file to parse.
	 *
	 * @return The parsed XML in the same format as XmlSlurper.
	 *
	 * @see XmlSlurper
	 */
	public static def parseXmlFile( String filename ) {
		// Use the contructor that lets us reactivate DOCTYPE since Groovy 2.4 disallows it by default.
		// And force the access external DTD to allow all. I guess this could be a security risk
		XmlSlurper slurper = new XmlSlurper( false, false, true )
		slurper.setEntityResolver( CATALOG_RESOLVER )
		slurper.setProperty( XMLConstants.ACCESS_EXTERNAL_DTD, "all" )

		// Access it directly if possible, otherwise use the class loader.
		def result
		if( Files.exists( Paths.get( filename ) ) ) {
			log.trace("Loading XML file (direct): $filename")
			result = slurper.parse( new File( filename ) )
		} else {
			log.trace("Loading XML file (class loader): $filename")
			result = slurper.parse( slurper.getClass().getClassLoader().getResourceAsStream( filename ) )
		}
		return result
    }

	public static def parseXmlFile( File file ) {
		return parseXmlFile( file.getPath() )
	}

	public void loadCustomComponentLibrary(File libfile, def comlibXml){
		String filename = libfile.getName()
		componentLibXmlMap[filename] = comlibXml
		loadedFiles += libfile
		loadCustomComponentLibrary(comlibXml)
	}

	public void loadCustomComponentLibrary(def comlibXml){
		comlibXml.component.each{ componentXml ->
			loadedComponentTypes += componentXml.@type.text()

			componentModel.loadComponent(componentXml)
			loadConfigurationToPaletteTreeModel(componentXml)
			loadConfigurationToTreeModel(componentXml)
		}
		
	}

	public void unloadCustomComponentLibrary(String fileName){
		unloadCustomComponentLibraryXML(componentLibXmlMap[fileName])
		componentLibXmlMap.remove(fileName)

		File target;
		for( File f : loadedFiles ){
			if( f.getName().equals( fileName ) ){
				target = f;
			}
		}
		loadedFiles.remove(target)
	}

	private void unloadCustomComponentLibraryXML(def comlibXml){
		comlibXml.component.each{ componentXml ->
			loadedComponentTypes -= componentXml.@type.text()
			unloadConfigurationToPaletteTreeModel(componentXml)
		}
	}
	
	public Object getComponentTypeXml(String componentType){
		//String filePath = dirPath + componentType + ".xml"
		String filePath = CentralCatalogue.getInstance().getComponentsProperties().getProperty("components.backcompatability.path") + componentType + ".xml"
		try{

			log.trace("loading backcompat file for $componentType")
			def componentXml = parseXmlFile( filePath)
			if ( componentXml != null ) {
				return componentXml
			}else{
				log.warn("Attempted to load backcompat file for $componentType but no file was present")
				return null
			}
		}catch(Exception e){
			log.error("Cannot load $filePath")
			return null
		}
	}

	public Object makeMeAPrototype(DLComponent instance){
		String compXML
		def writer = new StringWriter()
		def builder = new groovy.xml.MarkupBuilder(writer)
		instance.serializeAsPrototype(builder)
		compXML = writer.toString()

		XmlSlurper slurper = new XmlSlurper( false, false, true )
		slurper.setEntityResolver( CATALOG_RESOLVER )
		slurper.setProperty( XMLConstants.ACCESS_EXTERNAL_DTD, "all" )
		def result = slurper.parseText(compXML)

		return result
	}
	
	public void loadConfigurationToPaletteTreeModel(def componentXml){
		paletteTreeModels.each{m-> m.loadConfiguration(componentXml)}
	}

	public void unloadConfigurationToPaletteTreeModel(def componentXml){
		paletteTreeModels.each{m-> m.unloadConfiguration(componentXml)}
	}
	
	public void loadConfigurationToTreeModel(def componentXml){
		treeModels.each{m-> m.loadConfiguration(componentXml)}
	}


	public List<String> getLoadedComponentTypes(){
		return loadedComponentTypes
	}

	/**
	 * Searches all loaded component library files for something with the given name, and returns a stream to it if found.
	 * Obviously, this will only search in zip-based CLZ files, and not so much with the xml-based DCL files.
	 * @param memberName the filename of something to look for in the loaded libraries
	 * @return an InputStream to memberName, or null if nothing is found
	 */
	public InputStream getStreamFromLibraryFile(String memberName){
		//println "getStreamFromLibraryFile($memberName)"

		InputStream zis = null;
		for( File f : loadedFiles ){
			if ( ! f.getName().endsWith( FileFilters.ZipLibrary.getFilter().getExtensions().first())  ){
				//println "file ${f.getName()} not a clz, continue"
				continue;
			}

			//println "looking inside of ${f.getName()} for $memberName"
			ZipFile zf =  new ZipFile(f);
			for( ZipEntry e : zf.entries() ){
				//println "current entry: ${e.getName()}"
				if ( e.getName().equals( memberName ) ){
					zis = zf.getInputStream(e)
					//println "target found in lib ${f.getName()}"
				}
			}
		}
		return zis;
	}
	
}