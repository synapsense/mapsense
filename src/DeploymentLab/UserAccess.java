package DeploymentLab;

import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

import DeploymentLab.channellogger.Logger;

public class UserAccess {
	private Properties accessCodeProperties;
	private static final Logger log = Logger.getLogger(UserAccess.class.getName());
	
	public UserAccess(){
		accessCodeProperties = new Properties();

		try {
			InputStream propStream = new FileInputStream("accesscode.properties");
			accessCodeProperties.load(propStream);
			propStream.close();
		} catch (IOException e) {
			log.error(log.getStackTrace(e), "default");
		}
	}
	
	public boolean isMatchedAccessCode(int level, String clearCode){
		String accessCode;
		boolean result = false;
		accessCode = accessCodeProperties.getProperty("accesscode." + Integer.toString(level));
		String digestedCode = GetHashedAccessCode(clearCode);
		
		if(digestedCode.equals(accessCode)) {
			log.info("Access code is correct", "default");
			result = true;
		} else {
			log.info("Access code is incorrect.", "default");
			result = false;
		}
		return result;
	} 
	
	public void setAccessCode(int level, String newCode){
		String newDigestedCode = GetHashedAccessCode(newCode);
		accessCodeProperties.setProperty("accesscode." + Integer.toString(level), newDigestedCode);
		
		FileOutputStream out = null;
		try {
			out = new FileOutputStream("accesscode.properties");
			accessCodeProperties.store(out, "Change accesscode");
			out.close();
			log.info("Change access code successfully", "default");
		} catch (FileNotFoundException e) {
			log.error("Cannot change access code" + e.getMessage(), "default");
			log.error(log.getStackTrace(e), "default");
		} catch (IOException e) {
			log.error("Cannot change access code" + e.getMessage(), "default");
			log.error(log.getStackTrace(e), "default");
		}
	}
	
	private String GetHashedAccessCode(String clearCode){
		String digestedCode = "";
		try{
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(clearCode.getBytes());
			
			byte[] b = md.digest();
			BigInteger hash = new BigInteger(1, b);
			digestedCode = hash.toString(16);
			
		}catch(NoSuchAlgorithmException e){
			log.error(log.getStackTrace(e), "default");
		}
		return digestedCode;
	}
}
