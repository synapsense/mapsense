package DeploymentLab;

import DeploymentLab.Exceptions.ModelCatastrophe;
import DeploymentLab.Exceptions.UnknownObjectException;
import DeploymentLab.channellogger.Logger;
import DeploymentLab.channellogger.NoLogException;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.NoSuchElementException;

public class Main {
	private static final Logger log = Logger.getLogger(Main.class.getName());

	static String applianceHost = null;
	static String fileToLoad = null;
	static JWindow splashWindow;
	static int level = 2;
	static boolean useStdOutLog = false;
	static boolean makeSplashScreen = true;

	public static void main(String[] args) {

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-appliance")) {
				if (i + 1 >= args.length)
					printUsage();
				applianceHost = args[i + 1];
				i++;
			}
			if (args[i].equals("-basic")) {
				level = 0;
			}
			if (args[i].equals("-advanced")) {
				level = 2;
			}
			if (args[i].equals("-expert")) {
				level = 3;
			}
			if (args[i].equals("-IDDQD")) {
				level = 4;
			}
			if (args[i].equals("-occulo")) {
				level = 5;
			}
			if (new File(args[i]).isFile()) {
				fileToLoad = args[i];
			}
			if (args[i].equals("-stdout")) {
				useStdOutLog = true;
			}
			if (args[i].equals("-rd")) {
				makeSplashScreen = false;
			}
		}

		/* OS X compatibility */

		try {
			log.info("System is " + System.getProperty("os.name"), "default");
			if (System.getProperty("os.name").startsWith("Mac")) {
				System.setProperty("apple.laf.useScreenMenuBar", "true");
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				// Get the menu bar UI items
				Object menuBarUI = UIManager.get("MenuBarUI");
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
				// Put the menu bar item back...
				UIManager.put("MenuBarUI", menuBarUI);
				log.info("MacOSX + Nimbus menu bar hack deployed.", "default");
			} else {
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				//try to fall-back to the system default LaF if Nimubs is a flame-out
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}

		boolean badJava = true;
		Float version = Float.parseFloat(System.getProperty("java.specification.version"));
		if( version >= 1.7 ) {
			// We also require at least update 40 if it is 1.7.0 due to some bugs that break MapSense.
			String[] vstr = System.getProperty("java.version").split("_");
			if( !( vstr[0].equals("1.7.0") && Integer.parseInt(vstr[1]) < 40 ) ) {
				badJava = false;
			}
		}

		if( badJava ) {
			throw new UnknownError("Java Version must be 7u40 (1.7.0_40) or higher. Version in use is " + System.getProperty("java.version") );
		}
		System.out.println("Java Version: " + System.getProperty("java.version") + " (" + System.getProperty("java.specification.version") + ") " +
		         System.getProperty("java.vm.name") + " from " + System.getProperty("java.vendor") );

		if (level <= 4 && makeSplashScreen) {
			splashWindow = createSplashScreen();
			splashWindow.setVisible(true);
		}

		Runnable doStart = new Runnable() {
			public void run() {
				try {
					DeploymentLabWindow w = new DeploymentLabWindow(applianceHost, useStdOutLog);
					w.start(fileToLoad, level);
				} catch (NoLogException nle) {
					//In this special case, we want to do some feedback to the user that they see for sure
					JOptionPane.showMessageDialog(null, nle.getMessage(), "Fatal Error", JOptionPane.ERROR_MESSAGE);
					System.exit(-1);
				} catch (OutOfMemoryError oome) {
					JOptionPane.showMessageDialog(null, "There is insufficient memory to perform the requested operation.  Please see the log file for more details.  \nMapSense will exit.", "Fatal Error", JOptionPane.ERROR_MESSAGE);
					System.exit(-1);
				} catch (ModelCatastrophe ooe) {
					JOptionPane.showMessageDialog(null, ooe.getMessage() + "\nPlease see the log file for more details.  \nMapSense will exit.", "Fatal Error", JOptionPane.ERROR_MESSAGE);
					System.exit(-1);
				} catch (Throwable t) {
					//in this case, we may not want to show a dialog
					log.error("Exception caught by Main: " + log.getStackTrace(t), "default");
					System.out.println("Exception caught by Main: " + t);
					JOptionPane.showMessageDialog(null, t.getMessage(), "Fatal Error", JOptionPane.ERROR_MESSAGE);
					//System.out.println("Exception caught by Main: " + log.getStackTrace(t));
					t.printStackTrace();
					System.exit(-1);
				} finally {
					//no matter what, we want the splash window to go away.
					if (splashWindow!=null) {
						splashWindow.setVisible(false);
						splashWindow = null;
					}
				}
			}
		};
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			public void uncaughtException(Thread t, Throwable e) {
				if (e instanceof ArrayIndexOutOfBoundsException || e instanceof NoSuchElementException) {
					//these are the very odd exceptions that emerge from deep within swing when we delete a whole bunch of things at once
					//while still something of a mystery, there's no reason to fill customer log files with these (useless) stack traces
                    // ... except when there is a DeploymentLab class in the mix somewhere that probably did do something bad.
                    if( log.getStackTrace(e).contains("DeploymentLab") ) {
                        log.error(log.getStackTrace(e));
                    } else {
                        log.trace("Swing Glitch");
                    }
					return;
				}
				//log.error( e.getClass().getName() );
				log.error("Uncaught exception in thread " + t + " : " + log.getStackTrace(e), "default");
				if (e instanceof OutOfMemoryError || e.getCause() instanceof OutOfMemoryError) {
					JOptionPane.showMessageDialog(null, "There is insufficient memory to perform the requested operation.  Please see the log file for more details.  \nMapSense will exit.", "Fatal Error", JOptionPane.ERROR_MESSAGE);
					System.exit(-1);
				} else if (e instanceof ModelCatastrophe || e instanceof UnknownObjectException || e.getCause() instanceof ModelCatastrophe || e.getCause() instanceof UnknownObjectException) {
					JOptionPane.showMessageDialog(null, e.getMessage() + "\nPlease see the log file for more details.  \nMapSense will exit.", "Fatal Error", JOptionPane.ERROR_MESSAGE);
					System.exit(-1);
				}

			}
		});
		SwingUtilities.invokeLater(doStart);
	}

	private static void printUsage() {
		System.out.println("Incorrect number or format of command-line arguments.");
		System.exit(-1);
	}

	private static JWindow createSplashScreen() {
		JWindow splashWindow = new JWindow();
		splashWindow.getContentPane().setBackground(Color.white);

		ImageIcon splashImage = new ImageIcon(Main.class.getResource("/resources/splash.jpg"));
		int width = splashImage.getIconWidth();
		int height = splashImage.getIconHeight();

		JLabel mainScreen = new JLabel(splashImage);
		splashWindow.getContentPane().add(mainScreen);

		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (screen.width - width) / 2;
		int y = (screen.height - height) / 2;
		splashWindow.setBounds(x, y, width, height);

		return splashWindow;
	}
}

