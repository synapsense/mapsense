
package DeploymentLab.Model;

import java.util.*;

import DeploymentLab.channellogger.*;
import DeploymentLab.Exceptions.*;

public class Setting extends Property  implements MutableModelValue{
	private String valuetype; // string, integer, double, date
	private Object _value;
	private Object _oldValue;
	private boolean _historic;

	private Object _override;

	/**
	 * Original "pre-tags" constructor.  This Setting's tags are left empty.
	 * @param _belongsTo The DLObject that contains this setting
	 * @param n The name of this Setting
	 * @param vt The Value Type of this Setting (should be string, integer, double, or date)
	 * @param h Flag if this Setting is historic.
	 */
	public Setting(DLObject _belongsTo, String n, String vt, boolean h) {
		super(_belongsTo, n);
		valuetype = vt;
		_historic = h;
		//this.tags = new HashMap<String, CustomTag>();
	}

	/**
	 * Expanded constructor with support for an intial batch tags.
	 * @param _belongsTo The DLObject that contains this setting
	 * @param n The name of this Setting
	 * @param vt The Value Type of this Setting (should be string, integer, double, or date)
	 * @param h Flag if this Setting is historic.
	 * @param _tags A List<CustomTag> of tags to init this Setting with.
	 */
	public Setting(DLObject _belongsTo, String n, String vt, boolean h, List<CustomTag> _tags) {
		super(_belongsTo, n);
		valuetype = vt;
		_historic = h;
		this.tags = _tags;
	}

	public String getType() {
		return "setting";
	}

	public void deserialize(Object value, Object oldValue) {
		_value = value;
		_oldValue = oldValue;
		_override = null;
	}

	public void deserialize(Object value, Object oldValue, Object override) {
		_value = value;
		_oldValue = oldValue;
		_override = override;
	}


	public boolean getHistoric() {
		return _historic;
	}

	public void setValue(Object _newValue) {
		_value = PropertyConverter.fromObject(valuetype, _newValue);
	}

	public void setValue(String _newValue) {
		_value = PropertyConverter.fromString(valuetype, _newValue);
	}

	public Object getValue() {
		//return _value;

		if ( _override != null ){
			return _override;
		} else {
			return _value;
		}
	}

	public Object getRealValue(){
		return _value;
	}

	public Object getOldValue() {
		return _oldValue;
	}

	public void setOldValue(Object _newValue) {
		_oldValue = PropertyConverter.fromObject(valuetype, _newValue);
	}

	public boolean valueChanged() {
		//return _value != null && ! _value.equals(_oldValue);

		return this.getValue() != null && !this.getValue().equals(_oldValue);

//		return _value != null && ! _value.equals(_oldValue) && _value != "\u0007";

	}

	public void resetValue() {
		//_oldValue = _value;

		//binary data should always have null oldvalues
		if ( ! this.valuetype.equalsIgnoreCase("com.synapsense.dto.BinaryData")){
			_oldValue = this.getValue();
		}
	}

	public Object getOverride() {
		return _override;
	}

	public void setOverride(Object value) {
		this._override = PropertyConverter.fromObject(valuetype, value);
	}

	// TODO: make valuetype into an enum?
	public String getValueType() {
		return valuetype;
	}

	public String toString() {
		return "Setting(name='" + name + "', valuetype='" + valuetype + "', value='" + _value + "')";
    }
}

