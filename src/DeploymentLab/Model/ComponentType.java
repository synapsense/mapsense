package DeploymentLab.Model;

/**
 * @author Ken Scoggins
 * @since Aireo
 */
public class ComponentType {
	public static final String DRAWING = "drawing";
	public static final String ROOM = "room";
	public static final String ORPHANAGE = "orphanage";
	public static final String WSN_NETWORK = "network";
	public static final String WSN_GATEWAY = "remote-gateway";
	public static final String PLANNING_GROUP = "planninggroup";
	public static final String DRAWING_LINE = "drawing_line";
	public static final String DRAWING_POLY = "drawing_polygon";
	public static final String DRAWING_RECT = "drawing_rectangle";
	public static final String DRAWING_TEXT = "drawing_text";
	public static final String DRAWING_ICON = "drawing_icon";
	public static final String PRESSURE = "pressure2";
	public static final String RACK_UNINSTRUMENTED = "rack-uninstrumented";
}
