package DeploymentLab.Model

import DeploymentLab.SceneGraph.ArbitraryShapeNode
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.channellogger.Logger

/**
 * @author Vadim Gusev
 * @since Aireo
 * Date: 16.01.14
 * Time: 14:20
 */
class FloorTilesViewController implements ModelChangeListener {

	private static final Logger log = Logger.getLogger(FloorTilesViewController.class.getName())

	private DeploymentPanel deploymentPanel

	FloorTilesViewController(DeploymentPanel dp) {
		deploymentPanel = dp
	}

	@Override
	void componentAdded(DLComponent component) {
	}

	@Override
	void componentRemoved(DLComponent component) {
	}

	@Override
	void childAdded(DLComponent parent, DLComponent child) {
		if (parent.getType().equals(ComponentType.ROOM) && child.getType().equals(ComponentType.ROOM)){
			parent.addPropertyChangeListener("parentFloorTilesListener", this.&parentRoomPropChanged)
			child.addPropertyChangeListener("childFloorTilesListener", this.&childRoomPropChanged)
			updateRoomTilesView(parent)
		}
	}

	@Override
	void childRemoved(DLComponent parent, DLComponent child) {
		if (parent.getType().equals(ComponentType.ROOM) && child.getType().equals(ComponentType.ROOM)){
			parent.removePropertyChangeListener("parentFloorTilesListener")
			child.removePropertyChangeListener("childFloorTilesListener")
			updateRoomTilesView(parent)
		}
	}

	@Override
	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void modelMetamorphosisStarting(MetamorphosisEvent event) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void modelMetamorphosisFinished(MetamorphosisEvent event) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	public void parentRoomPropChanged(def who, def propertyName, def oldVal, def newVal){
		switch (propertyName) {
			case "x":
			case "y":
			case "points":
					this.updateRoomTilesView(who)
				break

		}
	}

	public void childRoomPropChanged(def who, def propertyName, def oldVal, def newVal){
		switch (propertyName) {
			case "x":
			case "y":
			case "points":
					def parentRoom = ((DLComponent)who).getParentsOfType(ComponentType.ROOM)
					if (parentRoom.size() == 1){
						this.updateRoomTilesView(parentRoom.first())
					}
				break
		}
	}

	private void updateRoomTilesView(DLComponent room){
		ArbitraryShapeNode pnode = (ArbitraryShapeNode)deploymentPanel.getPNodeFor(room)

		// TODO: FIXME! This sometimes gets called during file load before the node exists. Need to figure out why. Skip if null for mow.
		if( pnode ) {
			pnode.resetClipBounds();
		}
	}
}
