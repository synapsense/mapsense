package DeploymentLab.Model;

/**
 * Common component property names.
 *
 * @author Ken Scoggins
 * @since Aireo
 */
public class ComponentProp {
	public static final String AIR_VOLUME_TBL = "cfm_table";
	public static final String ALTITUDE = "altitude";
	public static final String AREA = "area";
	public static final String CONFIGURATION = "configuration";
	public static final String GROSS_AREA = "grossArea";
	public static final String MIN_SHAPE_PTS = "minShapePoints";
	public static final String POLY_POINTS = "points";
	public static final String ROTATION = "rotation";
	public static final String WSN_OVERRIDE = "manualWSNConfig";
}
