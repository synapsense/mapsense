package DeploymentLab.Model;

/**
 * Valid constants for the component "classes" attribute in the XML definition.
 *
 * @author Ken Scoggins
 * @since Aireo
 */
public class ComponentRole {
	public static final String PLACEABLE = "placeable";
	public static final String PROXY = "proxy";
	public static final String DRAWING = "drawing";
	public static final String NETWORK = "network";
	public static final String ARBITRARY_SHAPE = "arbitraryshaped";
	public static final String RECTANGLE_SHAPE = "rectangleshaped";
	public static final String CONTAINED = "contained";
	public static final String ROOM = "room";
	public static final String ZONE = "zone";
	public static final String TEXT = "text";
	public static final String LOGICAL_GROUP = "logicalgroup";
	public static final String CONTROLLABLE = "controllable";
	public static final String CONTROL_INPUT = "controlinput";
	public static final String CONTROL_DEVICE = "controlleddevice";
	public static final String CONTROL_STRATEGY = "controlstrategy";
	public static final String CONTROLTYPE_TEMP = "temperaturecontrol";
	public static final String CONTROLTYPE_PRESSURE = "pressurecontrol";
	public static final String CONTROLTYPE_AIRFLOW = "airflowcontrol";
	public static final String MANUAL_EXPRESSION = "manualexpressions";
	public static final String STATIC_CHILD = "staticchild";
}
