package DeploymentLab.Model;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * @author Ken Scoggins
 * @since Aireo
 */
public class RoomType {
	public static final String _PROPERTY = "roomType";

	public static final String UNSPECIFIED = "unspecified";
	public static final String RAISED_FLOOR = "raisedfloor";
	public static final String SLAB = "slab";

	public static final List<String> _CONTROLLABLE_TYPES = Arrays.asList( RAISED_FLOOR, SLAB );
}
