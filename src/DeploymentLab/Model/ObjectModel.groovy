package DeploymentLab.Model

import DeploymentLab.CentralCatalogue
import DeploymentLab.ComponentLibrary
import DeploymentLab.DeploymentLabVersion
import DeploymentLab.Exceptions.UnknownObjectException
import DeploymentLab.ProjectSettings
import DeploymentLab.StringUtil
import DeploymentLab.channellogger.Logger
import com.google.common.collect.HashMultimap
import com.google.common.collect.Multimap
import com.google.gag.annotation.disclaimer.HandsOff
import com.google.gag.enumeration.Consequence
import org.apache.commons.jxpath.JXPathContext

public class ObjectModel {
	private static final Logger log = Logger.getLogger(ObjectModel.class.getName())

	public final String version = DeploymentLabVersion.getObjectsVersion()
	public static final allowedVersions = DeploymentLabVersion.getAllowedVersions()
	private Map<Dlid,Set<DLObject>> deletedObjects = new HashMap<Dlid, Set<DLObject>>()

	private Map<String,Object> objTypes = new HashMap<String,Object>() //Map object type --> xml type
	private Map<Dlid,DLObject> _objects = new HashMap<Dlid,DLObject>() //Map Dlid --> Object

	/*
	 * Dude!  What's this for? This guy stores a multimap of object types : all instances of that type.
	 * It should be updated for addition / removal whenever _objects is, and mainly exists to speed up
	 * getObjectsByType(), which gets called a LOT and is really slow.
	 */
	private Multimap<String,DLObject> _objectTypeToInstances = HashMultimap.create()

	/**
	 * Make a new blank one!
	 */
	protected ObjectModel(){
		objTypes = new HashMap<String,Object>()
	}

	ObjectModel(def typesXml) {
		this.addTypes(typesXml)
	}


	public void addTypes(def typesXml){
		String v = typesXml.@version.text()
		log.info("Loading object model version '$v'")
		typesXml.object.each{ typeXml ->
			String oType = typeXml.@type.text()
			objTypes[oType] = typeXml
		}
	}

	String[] getTypes() {
		return objTypes.keySet()
	}

	void serialize(groovy.xml.MarkupBuilder builder, boolean includeAll = true) {
		builder.objects('version': version, 'lastid': Dlid.getFactoryState()) {
			_objects.values().each{ o ->
				if( includeAll || o.isExportable() ) {
					o.serialize(builder, includeAll)
				} else {
					log.trace("Skipping serialization for $o in ${CentralCatalogue.getOpenProject().getComponentModel().getOwner(o)}")
				}
			}
			//deletedObjects.each{ o -> o.serialize(builder) }
			//don't bother saving deleted objects we never exported
			for (Set<DLObject> deletedObjectsPerDrawing : deletedObjects.values()) {
				for (DLObject o : deletedObjectsPerDrawing) {
					if (o.key != null && o.key != '') {
						o.serialize(builder)
					}
				}
			}
		}
	}

	void deserialize(def objectsXml) {
		String v = objectsXml.@version.text()
		log.info("Deserializing objects created with model version '$v'")
		//log.info(allowedVersions.size().toString())
		if(allowedVersions.indexOf(v) == -1)
			throw new Exception("Incompatible model version '$v'.")
		int lastID = Integer.parseInt(objectsXml.@lastid.text())
		Dlid.resetFactory(lastID)
		_objects = [:]
		deletedObjects.clear()
		for(Object objXml: objectsXml.object) {
			deserializeObject(objXml)
		}
	}

	void clear() {
		_objects = [:]
		deletedObjects.clear()
		_objectTypeToInstances = HashMultimap.create()
	}

	void clearKeysForDrawing(List<DLObject> drawings){
		log.info("Clearing ObjectModel keys for these drawings: $drawings")

		for(DLObject o : _objects.values()){
			if(! drawings.contains(o.getAncestor())){
				continue
			}

			o.setKey('')
			o.getPropertiesByType('childlink').each{ p ->
				p.clearOldChildren()
			}
			o.getPropertiesByType('groovyrule').each{ p ->
				p.setRuleTo(null)
				p.setServerInstanceName(null)
			}
			o.getPropertiesByType('rule').each{ p ->
				p.setServerInstanceName(null)
			}
			o.getPropertiesByType('dockingpoint').each{ p ->
				p.makeDirty()
			}
			o.getPropertiesByType('collection').each{ p ->
				p.makeDirty()
			}
			o.getPropertiesByType('pier').each{ p ->
				p.makeDirty()
			}
		}

		for (DLObject drawing : drawings) {
			Set<DLObject> deletedObjectsForDrawing = getDeletedObjectsList(drawing)
			deletedObjectsForDrawing.clear()
		}

		//for the DLIS, we also want to make sure this is scrubbed out at the same time
		this.makeEverythingDirty(drawings)
		this.clearOldValues(drawings)

	}

	void clearKeys() {
		log.info("Clearing ObjectModel keys")
		_objects.each{ oid, o ->
			o.setKey('')
			o.getPropertiesByType('childlink').each{ p ->
				p.clearOldChildren()
			}
			o.getPropertiesByType('groovyrule').each{ p ->
				p.setRuleTo(null)
				p.setServerInstanceName(null)
			}
			o.getPropertiesByType('rule').each{ p ->
				p.setServerInstanceName(null)
			}
			o.getPropertiesByType('dockingpoint').each{ p ->
				p.makeDirty()
			}
			o.getPropertiesByType('collection').each{ p ->
				p.makeDirty()
			}
			o.getPropertiesByType('pier').each{ p ->
				p.makeDirty()
			}
		}
		deletedObjects.clear()

		//for the DLIS, we also want to make sure this is scrubbed out at the same time
		this.makeEverythingDirty()
		this.clearOldValues()
	}

	void clearLogicalIDs() {
		log.info("Clearing ObjectModel logical ids")
		getObjectsByType("wsnnode").each{ o ->
			if ( o.hasObjectProperty("id") ){
				o.getObjectProperty("id").setValue("")
			}
		}
	}

	void makeEverythingDirty() {
		// do for all drawings
		makeEverythingDirty(getObjectsByType( ObjectType.DRAWING ))
	}

	void makeEverythingDirty(List<DLObject> drawings){
		for(DLObject o : _objects.values()) {
			if (!drawings.contains(o.getAncestor())) {
				continue
			}

			o.getPropertiesByType('dockingpoint').each { p ->
				p.makeDirty()
			}
			o.getPropertiesByType('collection').each { p ->
				p.makeDirty()
			}
			o.getPropertiesByType('pier').each { p ->
				p.makeDirty()
			}
		}
	}

	void clearOldValues() {
		this.clearOldValues(getObjectsByType( ObjectType.DRAWING ))
	}

	void clearOldValues(List<DLObject> drawings){
		for(DLObject o : _objects.values()){
			if(! drawings.contains(o.getAncestor())){
				continue
			}

			o.getPropertiesByType("setting").each { s ->
				((Setting) s).setOldValue(null);
			}
			o.getProperties().each { p ->
				p.getTags().each { t ->
					t.setOldValue(null);
				}
			}
		}
	}


	int getObjectCount() {
		return _objects.size()
	}

	/**
	 * Deserializes an object from it's XML representation
	 * @param objXml
	 * @return [DLObject, [list of objects created as a result of deserializing the first]]
	 */
	def deserializeObject(def objXml){
		DLObject _o = createObject(objXml.@type.text())
        def created = _o.deserialize(objXml)
		if(_o.deleted){
			if(_o.key != null && _o.key != ''){
				// assumption is drawing was deserialized before its children
				Set deletedObjectsPerDrawing = getDeletedObjectsList(_o)
				deletedObjectsPerDrawing.add(_o)
			}else{
				log.debug("Throwing away deleted object with no ES key: ${_o}")
			}
		}else{
			_objects[_o.dlid] = _o
			_objectTypeToInstances.put(_o.getType(), _o)
		}
		return [_o, created]
	}

	/**
	 * Returns a list of the objects with no parents.
	 *
	 * @return  The objects with no parent.
	 */
	def getRoots() {
		Set<DLObject> roots = new HashSet<DLObject>()
		for( DLObject o : getObjects() ) {
			if( o.getParents().isEmpty() ) {
				roots.add( o )
			}
		}

		return roots
	}

	/**
	 * Stores a cache of objectType : [all legal child types].
	 * This never changes at runtime, and can always be constructed from loading the object definitions.
	 */
	Map<String,List<String>> childObjectTypeCache = new HashMap<String, List<String>>()
	
	/**
	 * 
	 * @param parentType
	 * @return a list of strings holding the names of all child types
	 */
    List<String> listChildObjectTypes(String parentType, boolean deep = false) {
        //return objTypes[parentType].property.findAll{it.@type == 'childlink'}.collect{it.reftype.collect{it.text()}.flatten()}.flatten().unique()
        if(!childObjectTypeCache.containsKey(parentType)){
            Set<String> result = recursiveSearchOfChildlinks(parentType, deep)
            childObjectTypeCache.put(parentType, new ArrayList<String>(result))
        }
        return childObjectTypeCache.get(parentType)
    }

    private Set<String> recursiveSearchOfChildlinks(String parentType, boolean deep){
        Set<String> result = new HashSet<String>()
        for(def p : objTypes[parentType].property){
            if(p.@type == "childlink"){
                for(def r: p.reftype){
                    result.add(r.text())
                    if (deep) {
                        result.addAll(recursiveSearchOfChildlinks(r.text(), deep))
                    }
                }
            }
        }
        return result
    }

	def listParentTypes(String childType){
		def types = []
		objTypes.each{ type, xml ->
		    if ( this.listChildObjectTypes(type).contains(childType)) {
				types += type
			}
		}
		return types
	}


	// So that Dlid can be made an inner class later.
	// Right now, the Dlid class is a delegate (sort of...)
	Dlid getDlid(String _id) {
		return Dlid.getDlid(_id)
	}

	/**
	 * Creates an object but does NOT add it to the object model.  Never call this.
	 * @param _type the type name of the object to create
	 * @return the fresh DLObject
	 */
	public DLObject createObject(String _type){
		if(!objTypes.containsKey(_type))
			throw new UnknownObjectException("Unknown object type '$_type'")
		return new DLObject(this, objTypes[_type])
	}

	void remove(DLObject o) {
		log.trace("remove object $o")

		if ( o == null ){
			log.trace("attempted to delete a null object, that's probably really a problem.")
			return
		}

		Set deletedObjectsPerDrawing = getDeletedObjectsList(o);
		//do some checking here
		if ( o.getDeleted() || deletedObjectsPerDrawing.contains( o ) ){
			log.trace("Trying to delete object $o that has already been deleted!")
			return
		}
		deletedObjectsPerDrawing.add(o)
		o.setDeleted(true)
		_objects.remove(o.dlid)
		_objectTypeToInstances.remove(o.getType(), o)
	}

	private Set<DLObject> getDeletedObjectsList(DLObject o) {
		DLObject drawing = o.getAncestor()
		if (deletedObjects.containsKey(drawing.getDlid())) {
			return deletedObjects.get(drawing.getDlid())
		}
		Set<DLObject> ls = new HashSet<>()
		deletedObjects.put(drawing.getDlid(), ls)
		return ls
	}

	void undelete(DLObject o) {
		Set deletedObjectsPerDrawing = getDeletedObjectsList(o);
		if ( ! deletedObjectsPerDrawing.contains( o ) ){
			log.error("Tried to undelete an object that wasn't deleted! $o")
			return
		}
		deletedObjectsPerDrawing.remove(o)
		o.setDeleted(false)
		_objects[o.dlid] = o
		_objectTypeToInstances.put(o.getType(), o)
	}

	List<DLObject> listDeletedObjects() {
		List ls = new ArrayList<DLObject>();
		for (Set<DLObject> deletedObjectsPerDrawing  : deletedObjects.values()){
			ls.addAll(deletedObjectsPerDrawing);
		}
		return ls;
	}

	boolean isDeleted( Dlid _id ) {
		if( _id == null ) throw new IllegalArgumentException("'id' is null")
		for( Set<DLObject> deletedObjectsPerDrawing  : deletedObjects.values() ) {
			for( DLObject o : deletedObjectsPerDrawing ) {
				if( o.dlid == _id ) {
					return true
				}
			}
		}
		return false
	}

	def listDeletedObjectsToBePruned(){
		def results = []
		for (Set<DLObject> deletedObjectsPerDrawing  : deletedObjects.values()) {
			for (DLObject o : deletedObjectsPerDrawing) {
				if (o.key == null || o.key == '') {
					results += o
				}
			}
		}
		return results
	}


	void prune(DLObject o) {
		if(o.getDeleted()) {
			log.debug("Pruning object $o")
			DLObject drawing = o.getAncestor();
			if (deletedObjects.containsKey(drawing.getDlid())) {
				Set<DLObject> deletedObjectsPerDrawing = deletedObjects.get(drawing.getDlid());
				deletedObjectsPerDrawing.remove(o)
			}
		} else {
			log.error("Tried to prune object '$o' not marked for deletion!")
			throw new Exception("Tried to prune object '$o' not marked for deletion!")
		}
	}

	/**
	 * Scans the list of deleted objects that have never been exported and prunes them from the model.
	 *
	 * Trivia note!  A pollard is a tree that has had all it's upper branched pruned off.  Also, pollard is the verb for doing that to a tree.
	 * In other words - a mass prune.
	 */
	void pollard(){
		if ( deletedObjects.size() == 0 ) { return; }
		log.debug("Scanning deleted objects for objects to prune", "default")
		//def watch = new DLStopwatch()
		for (Set<DLObject> deletedObjectsPerDrawing  : deletedObjects.values()) {
			List<DLObject> toPrune = new ArrayList<DLObject>(deletedObjectsPerDrawing.size());
			for (DLObject o : deletedObjectsPerDrawing) {
				if (o.key == null || o.key == '') {
					toPrune.add(o)
				}
			}
		}

		for (DLObject o : toPrune){
			this.prune(o)
		}
		//println watch.finishMessage("pollard")
	}


	/*
	DLObject getObject(Dlid _id) {
		if(_id == null)
			return null
		if(_objects.containsKey(_id))
			return _objects[_id]
		log.warn("Object with ID '$_id' does not exist!")
		return null
	}
	*/
	DLObject getObject(Dlid _id) {
		if(_id == null) { return null }

		DLObject o = _objects[ _id ]

		if( o == null ){
			Dlid newId = ProjectSettings.getInstance().findNewDlid( _id )
			if( newId != null ) {
				log.trace("Attempted to get object with pre-upgrade id $_id. Checking new id $newId")
				o = _objects[ newId ]
			}

			if( o == null ) {
				log.trace("Attempted to get a non-existant object for $_id")
			}
		}

		return o
	}

	/**
	 * Finds an object based on a string representation of the DLID
	 * @param dlid a String representation of a DLID, for example "DLID:1234"
	 * @return the DLObject with the given DLID, or {@code NULL} if no such object exists
	 */
	DLObject getObjectFromDlid(String dlid){
		return getObjectFromDlid( Dlid.getDlid(dlid) )
	}

	DLObject getObjectFromDlid(Dlid dlid){
		return this.getObject( dlid )
	}

	DLObject getObjectDeleted(Dlid _id) {
		if(_id == null) { return null }

		DLObject o = getObject( _id )

		if( o == null ){
			// See if it has been deleted.
			for( Set<DLObject> deletedObjectsPerDrawing  : deletedObjects.values() ) {
				def del = deletedObjectsPerDrawing.find { it.dlid == _id }
				if( del != null ) {
					log.trace("Found object in the deleted pile: $_id")
					o = del
					break
				}
			}

			if( o == null ) {
				log.trace("Object not found in the deleted pile: $_id")
			}
		}

		return o
	}

	java.util.Collection<DLObject> getObjects() {
		return _objects.values()
	}

	ArrayList<DLObject> getObjectsByType(String type) {
//		return _objects.values().findAll{ it.type == type }
/*
		List<DLObject> result = new ArrayList<DLObject>()
		for ( DLObject o : _objects.values() ){
			if ( o.type.equalsIgnoreCase(type) ){
				result.add(o)
			}
		}
		return result
		*/
		if(!objTypes.containsKey(type)){
			throw new UnknownObjectException("Attempted to find all objects of unknown type '$type'")
		}
		return new ArrayList<DLObject>(_objectTypeToInstances.get(type))
	}

	/**
	 * Finds all the objects of a given type that are present in the specified drawings.
	 * @param type the object type to search for
	 * @param drawings a Set of DLObjects that the results should be the descendant of; probably drawings
	 * @return an ArrayList of resulting objects; an empty list if none are found matching the search criteria
	 * @since Jupiter 2
	 */
	ArrayList<DLObject> getObjectsByTypeInDrawings(String type, Set<DLObject> drawings) {
		def result = new ArrayList<>()
		for(DLObject o : this.getObjectsByType(type)){
			if(drawings.contains(o.getAncestor())){
				result.add(o)
			}
		}
		return result
	}

	List<DLObject> getObjectsByTag(String propertyName, String tagName, String tagValue){
		List<DLObject> result = []
		_objects.values().each{ o ->
			if ( o.hasObjectProperty(propertyName)){
				def prop = o.getObjectProperty(propertyName)
				if ( prop.hasTag(tagName)){
					def tag = prop.getTag(tagName)
					if ( tag.getValue() == tagValue ){
						result += o
					}
				}
			}
		}	
		return result
	}




	JXPathContext getXPathContext() {
		JXPathContext ctx = JXPathContext.newContext(this)
		return ctx
	}

	/**
	 * Sets all specified values to something else.
	 * @param objectType
	 * @param fieldName
	 * @param flushValue
	 */
	public void flushField(String objectType, String fieldName, Object flushValue){
		this.getObjectsByType(objectType).each{ o ->
			if ( o.hasObjectProperty(fieldName) ){
				o.getObjectProperty(fieldName).setValue(flushValue)
				((Setting)o.getObjectProperty(fieldName)).setOldValue(null)
			}
		}
	}	
	
	/**
	 * Construct the objects.xml file, BUT - all as one file, and with the contents of the rules threaded in.
	 * @param builder a Builder to write the XML into
	 * @param justUsedInProject if true, only generate XML for objects used in this project file; if false, generate for all object types known
	 * (False, then, should get you the original objects.xml back)
	 */
	void makeDefinitionFile(def builder, boolean justUsedInProject = true){

		def objectTypesToLookAt
		if (justUsedInProject){
			objectTypesToLookAt = new HashMap<String,Object>();
			def usedTypes = new HashSet<String>();
			_objects.each{ k, v ->
				usedTypes.add(v.getType())
			}
			usedTypes.each{ t ->
				objectTypesToLookAt[t] = objTypes[t]
			}
			//also grab any default types
			CentralCatalogue.getApp("default.object.types").split(",").each{ t ->
				objectTypesToLookAt[t] = objTypes[t]
			}
		} else {
			objectTypesToLookAt = objTypes
		}


		builder.objects(){
			objectTypesToLookAt.each{ k, v->
				builder.object(type: k){
					v.property.each{ p->
						builder.property(type: p.@type){
							builder.name( p.name.text() )

							switch( p.@type ){
								case "setting":
									builder.historic( p.historic.text() )
									builder.valuetype( p.valuetype.text() )
									//<customtag name="pluginId" tagType="java.lang.String" >SNMP</customtag>
									//p.customtag.each{ tag ->
									//	builder.customtag(name: tag.@name, tagType: tag.@tagType)
									//}

									break;
								case "dockingpoint":
								case "collection":
								//childink?
									//builder.reftype( p.reftype.text() )
									break;

								case "rule":
									/*
									<name>cTROfChange</name>
												<historic>true</historic>
												<valuetype>java.lang.Double</valuetype>
												<ruleclass>com.synapsense.rulesengine.core.script.metric.RateOfChange</ruleclass>
												<schedule>0 25,55 * * * ?</schedule>
												<binding rulevar="sensor" property="cTop" />

									 */
									builder.historic( p.historic.text() )
									builder.valuetype( p.valuetype.text() )
									builder.ruleclass( p.ruleclass.text() )
									builder.schedule( p.schedule.text() )
									//bindings?
									break;

								case "groovyrule":
									builder.historic( p.historic.text() )
									builder.valuetype( p.valuetype.text() )
									builder.ruleclass( "${k}_${p.name.text()}" )
									//builder.rulefile( URLEncoder.encode(FileUtil.fileToString( "cfg/rules/" + p.rulefile.text() ), "UTF-8" ) )
									//builder.rulefile( FileUtil.fileToString( "cfg/rules/" + p.rulefile.text() ) )
									//builder.rulefile( p.getRuleContents() )
									//need to ask the object model for the rule contents, not just haul it out of the jar

									builder.rulefile( findRuleContents( p.rulefile.text()) )


									builder.schedule( p.schedule.text() )
									//bindings? - handled by instance, not type
									break;
							}

							//I AM THE ATOMIC POWERED CUSTOM TAG!  GIVE MY REGARDS TO EVERYBODY!
							p.customtag.each{ tag ->
								builder.customtag(name: tag.@name, tagType: tag.@tagType)
							}

						}
					}
				}
			}
		}
	}

	/**
	 * Finds the contents of a rule file and returns it as a string.  Three possible places are searched:
	 * First, any loaded .CLZ files are searched.  Then, the local file system.  Finally, the main jar file.
	 * @param ruleFileName the filename to search for, eg "{@code somerule.groovy}"
	 * @return a String containing the contents of the file, or null if it could not be found.
	 */
	private String findRuleContents(String ruleFileName){
		String contents
		InputStream zis = ComponentLibrary.INSTANCE.getStreamFromLibraryFile(ruleFileName);
		if ( zis != null ){
			contents = StringUtil.convertStreamToString(zis);
		} else {
			File f = new File( "cfg/rules/$ruleFileName" )
			contents = this.getRuleContents(f)
		}
		return contents
	}


	private String getRuleContents(File f) throws IOException {
		BufferedReader reader;
		if(f.canRead()) {
			reader = new BufferedReader(new FileReader(f));
		} else {
			log.warn("Can't read '" + f.getPath() + "' from filesystem, defaulting to JAR file.", "default");
			try {
				ClassLoader cl = this.getClass().getClassLoader();
				String path = f.getPath().replace('\\', '/');
				InputStream is = cl.getResourceAsStream(path);
				reader = new BufferedReader(new InputStreamReader(is));
			} catch(Exception e) {
				log.error("Error loading source file " + f.getPath() + " : " + log.getStackTrace(e), "default");
				return null;
			}
		}
		StringBuffer sb = new StringBuffer();
		String line;
		String sep = System.getProperty("line.separator");
		while((line = reader.readLine()) != null) {
			sb.append(line);
			sb.append(sep);
		}
		return sb.toString();
	}

	public String makeDefinitionFileString(boolean justUsedInProject = true){
		StringBuilder allObjects

		def writer = new StringWriter()
		def builder = new groovy.xml.MarkupBuilder(writer)
		this.makeDefinitionFile(builder,justUsedInProject)

		allObjects = new StringBuilder( writer.getBuffer().toString() )
		//return( allObjects )

		//return "this is a string"
		return allObjects.toString()

	}

	/**
	 * Serializes the object half of the model and returns it as a string.
	 * @return
	 */
	public String makeObjectsString(){
		def strwriter = new StringWriter()
		def strbuilder = new groovy.xml.MarkupBuilder(strwriter)
		this.serialize(strbuilder)
		return( strwriter.toString() )
	}




	/**
	 * Makes a new ObjectModel containing only the objects descended from the roots provided (inclusive).
	 * Note that this submodel has all the typedef info from the source, so that it CAN be used to create new objects.
	 * @param roots
	 * @return
	 */
	public ObjectModel makeModelSubset(java.util.Collection<DLObject> roots){
		def result = new ObjectModel()
		result.clear()

		//copy all typeinfo
		result.objTypes = this.objTypes

		for(DLObject r : roots){
			result.addObjectAndChild(r)
		}
		return result
	}

	/**
	 * Recursively add an object and it's children to this object model.
	 * Should only be used when slicing our a new sub-model
	 * @param o
	 */
	@HandsOff(byOrderOf="Gabe Helman", onPainOf=Consequence.VOGON_POETRY_RECITAL)
	protected void addObjectAndChild(DLObject o){
		this.addObject(o)
		for(DLObject c : o.getChildren()){
			this.addObjectAndChild(c)
		}
	}

	@HandsOff(byOrderOf="Gabe Helman", onPainOf=Consequence.VOGON_POETRY_RECITAL)
	protected void addObject(DLObject _o){
		//from deserialize:
		_objects[_o.dlid] = _o
		_objectTypeToInstances.put(_o.getType(), _o)
	}

}

