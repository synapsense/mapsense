
package DeploymentLab.Model;

import java.io.*;
import java.util.Map;

import DeploymentLab.ComponentLibrary;
import DeploymentLab.StringUtil;
import DeploymentLab.channellogger.Logger;
import com.synapsense.dto.RuleType;

/**
 * Represents an object property that is calculated by a groovy-based CRE Rule.
 */
public class GroovyRule extends Property implements CREMember  {
	private static final Logger log = Logger.getLogger(GroovyRule.class.getName());
	private String valuetype;
	private String ruleFile;
	private String ruleSchedule;
	private boolean _historic;
	private Map<String, String> varBindings; //bindings[binding.@rulevar.text()] = binding.@property.text()
	private String ruleTo;
	private String serverInstanceName;
	private String contents = null;

	public GroovyRule(DLObject _belongsTo, String n, String vt, String rf, String rs, boolean h, Map<String, String> vb) {
		super(_belongsTo, n);
		valuetype = vt;
		ruleFile = rf;
		ruleSchedule = rs;
		_historic = h;
		varBindings = vb;
	}

	public String getType() {
		return "groovyrule";
	}

	public String getValueType() {
		return valuetype;
	}

	public String getRuleFile() {
		return ruleFile;
	}

	/**
	 * Gets the schedule of this rule.
	 * @return A string containing the CRON configuration of this rule's schedule.
	 */
	public String getSchedule() {
		return ruleSchedule;
	}

	/**
	 * Checks to see if this rule is scheduled.
	 * @return true if this rule has a schedule.
	 */
	public boolean hasSchedule(){
		return ( ruleSchedule.length() > 0 );
	}

	public Map<String, String> getBindings() {
		return varBindings;
	}

	public boolean getHistoric() {
		return _historic;
	}

	public String getRuleTo() {
		return ruleTo;
	}

	public void setRuleTo(String to) {
		ruleTo = to;
	}

	public String toString() {
		return "GroovyRule(name=" + name + ", type='" + valuetype + "', rulefile='" + ruleFile + "', schedule='" + ruleSchedule + "', historic='" + _historic + "', variables=" + varBindings + ")";
	}

	/**
	 * Gets the contents of this rule's source file as a string.
	 * @return a String containing the contents of the source file.
	 * @throws IOException if the file cannot be read.
	 */
	public String getRuleContents() throws IOException {
		if ( contents != null ){
			return contents;
		}
		ComponentLibrary clib = ComponentLibrary.INSTANCE;
		InputStream zis = clib.getStreamFromLibraryFile(this.ruleFile);
		if ( zis != null ){
			contents = StringUtil.convertStreamToString(zis);
		} else {
			File f = new File("cfg/rules", this.ruleFile);
			contents =  this.getRuleContents(f);
		}
		return contents;
		//File f = new File("cfg/rules", this.ruleFile);
		//return ( this.getRuleContents(f) );
	}

	/**
	 * Gets the contents of a source file as a string.
	 * @param f a File to read
	 * @return a String containing the contents of f
	 * @throws IOException if the file cannot be read.
	 */
	private String getRuleContents(File f) throws IOException {
		BufferedReader reader;
		if(f.canRead()) {
			reader = new BufferedReader(new FileReader(f));
		} else {
			log.warn("Can't read '" + f.getPath() + "' from filesystem, defaulting to JAR file.", "default");
			try {
				ClassLoader cl = this.getClass().getClassLoader();
				String path = f.getPath().replace('\\', '/');
				InputStream is = cl.getResourceAsStream(path);
				reader = new BufferedReader(new InputStreamReader(is));
			} catch(Exception e) {
				log.error("Error loading source file " + f.getPath() + " : " + log.getStackTrace(e), "default");
				return null;
			}
		}
		StringBuffer sb = new StringBuffer();
		String line;
		String sep = System.getProperty("line.separator");
		while((line = reader.readLine()) != null) {
			sb.append(line);
			sb.append(sep);
		}
		return sb.toString();
	}

	/**
	 * Generates a CRE RuleType for this rule.
	 * Uses the value of this.getServerTypeName() as the name, sets the contents to the contents of the specified .groovy file,
	 * hard codes the source type as non-compiled "Groovy", and sets the schedule if there is one.
	 * @return a CRE RuleType object, ready for export to ES.
	 */
	public RuleType createRuleType() {
		//System.out.println("making an rt: " +this.getSchedule() );
		RuleType result = null;
		try{

			if ( this.hasSchedule() ){
				//System.out.println("constructing a schedule rule " + this.getSchedule() );
				result = new RuleType( this.getServerTypeName(), this.getRuleContents(), "Groovy", false, this.getSchedule() );
			} else {
				//System.out.println("constructing a no schedule rule" );
				result = new RuleType( this.getServerTypeName(), this.getRuleContents(), "Groovy", false );
			}
		} catch (IOException ioe){
			log.error("IOException exporting groovy rule:" + ioe.getMessage(), "default");
			log.error( log.getStackTrace(ioe), "default");
		} catch (IllegalArgumentException ioe){
			log.error("IllegalArgumentException exporting groovy rule:" + ioe.getMessage(), "default");
			log.error( log.getStackTrace(ioe), "default");
		}
		return result;
	}

	/**
	 * Gets the unique rule type name to be used on the server for this rule.
	 * Follows the pattern (object type name)_(this property name).
	 * For example, a groovyrule property named "lastValue" on an object type of "unlikelyOperation" would
	 * result in a server type name of "unlikelyOperation_lastValue"
	 * @return a String holding the unique server rule type name.
	 */
	public String getServerTypeName(){
		return (belongsTo.getType() + "_" +  this.getName());
	}

	/**
	 * Gets the unique name that this rule instance will use on the server.
	 * Generally. this follows the pattern (TO)(property name), so a rule property named lastValue on an object with a TO of 13:SOMETHING
	 * would have a server instance name of 13lastValue.
	 * This should not be populated until the rule is exported for the first time - the presence of a name here is used
	 * as an indicator that the rule has been created on the server.
	 * @return a String containing this rule instance's unique name.
	 */
	public String getServerInstanceName() {
		return serverInstanceName;
	}

	/**
	 * Sets the unique name that this rule instance will use on the server.
	 * @param serverInstanceName the new instance name to use for this rule.
	 */
	public void setServerInstanceName(String serverInstanceName) {
		this.serverInstanceName = serverInstanceName;
	}

	/**
	 * Gets the name of the source file that supplies the guts of this groovyrule.
	 * @return a String holding the name of the groovy file.
	 */
	public String getSourceName(){
		return ( ruleFile );
	}

}

