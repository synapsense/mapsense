package DeploymentLab.Model

import DeploymentLab.CentralCatalogue
import groovy.xml.MarkupBuilder
import DeploymentLab.channellogger.Logger
import org.apache.commons.jxpath.JXPathContext
import groovy.xml.StreamingMarkupBuilder
import org.mvel2.MVEL
import org.mvel2.compiler.CompiledExpression

/**
 *
 * @author Gabriel Helman
 * @since Io
 * Date: 11/6/12
 * Time: 11:55 AM
 */
class ComponentParcel implements ComponentTrait, DLBinding {
	private static final Logger log = Logger.getLogger(ComponentParcel.class.getName())

	protected DLComponent belongsTo
	protected String name
	protected String dependentVar
	protected String displayName
	protected List<String> addedRoles
	protected Map<String,String> displaySettings
	protected def viscera
	protected List<ComponentTrait> relics //list of Traits
	private boolean currentlyEnabled
	private JXPathContext context
	String parcel

	private static HashMap<String, CompiledExpression> expressionRegistry = new HashMap<String, CompiledExpression>()
	private String valueExpr
	private Serializable compiledExpr

	ComponentParcel(Object bXml, JXPathContext _ctx, DLComponent _owner) {
		belongsTo = _owner
		context = _ctx
		dependentVar = bXml.@var.text()
		name = bXml.@name.text()
		displayName = bXml.@display.text()
		currentlyEnabled = Boolean.parseBoolean(bXml.@active.text())
		parcel = bXml.@parcel.text()

		valueExpr = bXml.value.text()
		belongsTo = _owner
		if(!expressionRegistry.containsKey(valueExpr)){
			expressionRegistry[valueExpr] = MVEL.compileExpression(valueExpr)
		}
		compiledExpr = expressionRegistry[valueExpr]

		addedRoles = []
		bXml.addrole.each{
			addedRoles.add( it.text() )
		}
		displaySettings = [:]
		bXml.display.each{
			displaySettings.put( it.@setting.text(), it.text() )
		}

		viscera = bXml.viscera
		relics = []
	}

	public boolean isEnabled() {
		return currentlyEnabled
	}

	@Override
	String getTraitId() {
		return name
	}

	@Override
	String getTraitDisplayName() {
		return displayName
	}

	@Override
	DLComponent getOwner() {
		return belongsTo
	}

	@Override
	void serialize(MarkupBuilder builder) {
		String visceraXML = new StreamingMarkupBuilder().bind{out << viscera}
		builder.parcel(name : name, display: displayName, var: dependentVar, active: currentlyEnabled, parcel: parcel) {
			builder.value(valueExpr)
			displaySettings.each{ k, v ->
				builder.display(setting: k, v)
			}
			addedRoles.each{ r ->
				builder.addrole(r)
			}
			mkp.yieldUnescaped (visceraXML)
		}
	}

	@Override
	void serializeAsPrototype(MarkupBuilder builder) {
		this.serialize(builder)
	}

	void setEnabled(boolean enable){
		def v = new HashMap()
		v[dependentVar] = enable
		this.eval( v )
	}

	@Override
	void eval(Object vars) {
		//def choice = vars[dependentVar]
		//choice should be a boolean for turn it on or off
		log.debug("Eval on parcel $name($vars): '$valueExpr'")

		def result
		try {
			result = MVEL.executeExpression(compiledExpr, vars)
		} catch(Exception e) {
			log.error(log.getStackTrace(e))
			log.error("Couldn't evaluate expression '$valueExpr'.")
			log.error("vars: $vars")
			throw e
		}
		log.debug("eval result is $result")

		// Don't track parcel (de)materialization since the parcels handles it's own values and also things may disappear and make undo blow-up
		CentralCatalogue.getUndoBuffer().stopTracking()
		try {
			if(result){
				this.materialize()

				// Determine a display level to make life for the property model a little easier.
				def parentProp = owner.getComponentProperty( dependentVar )
				int level = ( parentProp.displayed ? parentProp.getDisplayLevel() + 1 : parentProp.getDisplayLevel() )

				for( ComponentTrait r : relics ) {
					if( r instanceof ComponentProperty ) {
						r.setDisplayLevel( level )
					}
				}
			} else {
				this.dematerialize()
			}
		} finally {
			CentralCatalogue.getUndoBuffer().resumeTracking( false )
		}
	}

	@Override
	def listVariables() {
		return [dependentVar]
	}

	@Override
	public String toString() {
		return "ComponentParcel{" +
				"belongsTo=" + belongsTo +
				", name='" + name + '\'' +
				", dependentVar='" + dependentVar + '\'' +
				", displayName='" + displayName + '\'' +
				", addedRoles=" + addedRoles +
				", displaySettings=" + displaySettings +
				", viscera=" + viscera +
				", relics=" + relics +
				", currentlyEnabled=" + currentlyEnabled +
				", context=" + context +
				", parcel='" + parcel + '\'' +
				'}';
	}

	protected void materialize(){
		log.debug("materialize parcel $name")
		if( currentlyEnabled ) {
			if( relics.isEmpty() ) {
				// Might be deserializing. We need to get our existing relics from our owner.
				for( ComponentTrait r : belongsTo.getAllTraits() ) {
					if( r.getParcel() == name ) {
						relics.add( r )
					}
				}
				log.debug("deserialization? Grabbed ${relics.size()} relics from $belongsTo.")
			} else {
				log.debug("no double-stuffed parcels today, thank you.")
			}
			return;
		}

		belongsTo.metamorphosisStarting()

		for(def r : addedRoles){
			log.debug("Adding role $r")
			belongsTo.addRole(r)
		}
		displaySettings.each{ k, v ->
			belongsTo.setDisplaySetting(k, v)
		}

		List<ComponentTrait> storedRelics = relics
		relics = belongsTo.processComponentXML(viscera, name)
		for( def r : relics){

			if( r instanceof ComponentProperty ) {
				// This magic stops us from losing settings when the parcel dematerializes for some reason, but then
				// materializes again. Could be from user accident, moving something around, or just from doing an UNDO.
				// Since we never wiped out the relics upon dematerialize, we can check them and reset the values.
				// Otherwise, we still need to force a prop change so listeners get notified of the new property.
				def storedProp = storedRelics.find { it instanceof ComponentProperty && it.getTraitId() == r.getTraitId() }

				if( storedProp != null ) {
					log.trace("Using cached parcel prop: ${storedProp.getTraitId()} '${storedProp.getValue()}'")
					belongsTo.setPropertyValue( r.getName(), storedProp.getValue() )
				} else {
					belongsTo.setPropertyValue( r.getName(), r.getValue() )
				}
			}
		}

		belongsTo.getModel().updateComponentParts(belongsTo)

		//solve parents!
		List<DLObject> parents = belongsTo.getParentObjects()
		//Set<String> possibleSiblings = new HashSet<String>()
		//parents.each{ p ->
		//	possibleSiblings.addAll(belongsTo.getOModel().listChildObjectTypes(p.getType()))
		//}
		for(def r : relics){
			if (r instanceof DLObject){
//				if (possibleSiblings.contains(r.getType())){
					//we're a family!

//				}
				DLObject ob = (DLObject)r
				if (ob.getParents().size() == 0){
					log.debug("hook up object $ob of type ${ob.getType()}")

					// Check the existing component parents.
					log.debug("first, the existing parents...")
					parents.each{ p->
						List<String> childTypes = belongsTo.getOModel().listChildObjectTypes(p.getType())
						log.debug("childtypes for $p are $childTypes")
						if (childTypes.contains(ob.getType())){
							log.debug("attempt to add parent")
							p.getPropertiesByType("childlink").each{ cl ->
								if (((ChildLink)cl).reftypes.contains(ob.getType())){
									((ChildLink)cl).addChild(ob)
								}
							}
						}
					}

					// We sometimes have objects that are children of our own component roots. Hook those up.
					log.debug("second, our own roots...")
					belongsTo.getRoots().each { internalObj ->
						List<String> childTypes = belongsTo.getOModel().listChildObjectTypes( internalObj.getType() )
						log.debug("childtypes for $internalObj are $childTypes")
						if (childTypes.contains(ob.getType())){
							log.debug("attempt to add parent")
							internalObj.getPropertiesByType("childlink").each{ cl ->
								if (((ChildLink)cl).reftypes.contains(ob.getType())){
									((ChildLink)cl).addChild(ob)
								}
							}
						}
					}
				}
			}
		}

		currentlyEnabled = true

		belongsTo.metamorphosisFinished()
	}

	protected void dematerialize(){
		log.debug("dematerialize parcel $name")
		//remove all relics
		if(!currentlyEnabled){
			log.debug("no double-un-stuffed parcels today, thank you.")
			return;
		}

		belongsTo.metamorphosisStarting()

		displaySettings.each{ k, v ->
			log.debug("disable displaySetting $k")
			belongsTo.setDisplaySetting(k, null)
		}

		// May not be needed now that we sync the relics when materialized.
		if(relics.size() == 0){
			for(def t : belongsTo.getAllTraits()){
				if(t.getParcel() == name){
					log.debug("adding trait to parcel relics: $t")
					relics += t
				}
			}
		}

		// Remove the traits from the component. Do DLObjects last since others may reference them.
		def objs = []
		for(def r : relics){
			if( r instanceof DLObject ) {
				// Add to the list so that children are removed before their parent or we can get orphans.
				int idx = Integer.MAX_VALUE
				for( DLObject o : r.getParents() ) {
					int i = objs.indexOf( o )
					if( i != -1 && i < idx ) {
						idx = i
					}
				}
				if( idx < Integer.MAX_VALUE ) {
					objs.add( idx, r )
				} else {
					objs.add( r )
				}
			} else {
				belongsTo.removeTrait(r)
			}
		}

		for( def o : objs ) {
			belongsTo.removeTrait( o )
		}

		// See what roles other active parcels may have added and only remove those that are only for this parcel.
		Set<String> otherActiveRoles = [] as Set
		for( ComponentParcel p : belongsTo.getEnabledParcels() ) {
			if( p != this ) {
				otherActiveRoles.addAll( p.addedRoles )
			}
		}

		for(def r : addedRoles){
			if( !otherActiveRoles.contains( r ) ) {
				belongsTo.removeRole(r)
			}
		}

		belongsTo._roots = null
		currentlyEnabled = false

		belongsTo.metamorphosisFinished()
	}

	boolean hasRole( String role ) {
		return addedRoles.contains( role )
	}

	boolean hasRoles( java.util.Collection<String> roles ) {
		return addedRoles.containsAll( roles )
	}
}
