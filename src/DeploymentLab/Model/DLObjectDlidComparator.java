package DeploymentLab.Model;

import java.util.Comparator;


/**
 * Sorts DLObjects based on their DLIDs.  Since DLIDs are handed out in order, this is also the same as sorting in creation order.
 * AND, since components are created in one pass from the definition file, this is ALSO the same as sorting them in DCL order.
 * @author Gabriel Helman
 * @since Mars
 * Date: 3/4/11
 * Time: 5:11 PM
 */
public class DLObjectDlidComparator implements Comparator<DLObject> {
	@Override
	public int compare(DLObject o1, DLObject o2) {
		Integer me =  o1.getDlid().hashCode();
		Integer other = o2.getDlid().hashCode();
		return me.compareTo(other);
	}
}
