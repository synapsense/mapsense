package DeploymentLab.Model

import DeploymentLab.CentralCatalogue
import DeploymentLab.channellogger.Logger

/**
 * Manages Planning Groups and the components that are members of the group.
 *
 * @author Ken Scoggins
 * @since Aireo
 */
public class LogicalGroupController implements ModelChangeListener {
	private static final Logger log = Logger.getLogger( LogicalGroupController.class.getName() )

	public LogicalGroupController() {
	}

	/**
	 * Check the given components for members of a Logical Group and remove them from the group.
	 *
	 * @param components  Potential excommunicated members of a Logical Group.
	 */
	public static void removeFromGroup( ArrayList<DLComponent> components ) {
		try {
			CentralCatalogue.getUndoBuffer().startOperation()

			for( DLComponent c : components ) {
				for( DLComponent p : c.getParentsOfRole( ComponentRole.LOGICAL_GROUP ) ) {
					p.removeChild( c )
				}
			}
		} catch( Exception e ) {
			CentralCatalogue.getUndoBuffer().rollbackOperation()
		} finally {
			CentralCatalogue.getUndoBuffer().finishOperation()
		}
	}

	/**
	 * Listens for Planning Groups and hooks itself up as a listener.
	 *
	 * @param The new Component.
	 */
	@Override
	void componentAdded(DLComponent component) {
		if( component.hasRole( ComponentRole.LOGICAL_GROUP ) ) {
			log.trace("Logical Group added $component")
			component.addPropertyChangeListener( this, this.&groupPropertyChanged )
		}
	}

	/**
	 * Detaches itself as a listener if needed.
	 *
	 * @param component the DLComponent just removed.
	 */
	@Override
	void componentRemoved(DLComponent component) {
		if( component.hasRole( ComponentRole.LOGICAL_GROUP ) ) {
			log.trace("Logical Group removed $component")
			component.removePropertyChangeListener( this )
		}
	}

	@Override
	void childAdded(DLComponent parent, DLComponent child) {
		if( parent.getModel().hasActiveState( ModelState.INITIALIZING ) ) return

		if( parent.hasRole( ComponentRole.LOGICAL_GROUP ) ) {
			// Make sure the kid has the same exportable setting as the parent 'export' property...
			child.setIsExportable( parent.getPropertyValue('export') )
		}
	}

	@Override
	void childRemoved(DLComponent parent, DLComponent child) {
		if( parent.getModel().hasActiveState( ModelState.INITIALIZING ) ) return

		if( parent.hasRole( ComponentRole.LOGICAL_GROUP ) ) {
			child.setIsExportable( true )
		}
	}

	@Override
	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	@Override
	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	@Override
	public void modelMetamorphosisStarting( MetamorphosisEvent event ) {}

	@Override
	public void modelMetamorphosisFinished( MetamorphosisEvent event ) {}


	/**
	 * Acts as the PropertyChanged event closure for controlled devices with AoEs.
	 * @param who the control device DLComponent which has changed
	 * @param propertyName the name of the changing property
	 * @param oldVal the old value of the property
	 * @param newVal the new value of the property
	 */
	public void groupPropertyChanged(def who, def propertyName, def oldVal, def newVal) {
		//we don't want to get in the way if replace config is running right now
		if( CentralCatalogue.getOpenProject().getComponentModel().isMetamorphosizing() ) { return }

		switch( propertyName ) {
			case "export":
				// Flag the component and its children as being exportable or not.
				who.setIsExportable( newVal )
				for( DLComponent c : who.getChildComponents() ) {
					c.setIsExportable( newVal )
				}
				break
		}
	}
}
