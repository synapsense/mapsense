package DeploymentLab.Model;

import DeploymentLab.channellogger.*
import DeploymentLab.Exceptions.UnknownComponentException
import DeploymentLab.Pair
import DeploymentLab.CentralCatalogue
import DeploymentLab.Exceptions.ModelCatastrophe

/**
 * Copies an imported project model into a target project model.
 *
 * @author Hyeeun Lim
 * @author Gabriel Helman
 * Date: 7/9/12
 * Time: 10:34 AM
 * 
 */
public class DLComponentCloner {
    private static final Logger log = Logger.getLogger(DLComponentCloner.name)
    		
    private ComponentModel importedComponentModel        // imported componentModel
    private ComponentModel targetComponentModel        // original componentModel

    private ObjectModel importedObjectModel        // imported ObjectModel
    private ObjectModel targetObjectModel        // original ObjectModel
	
    private HashMap<DLComponent, DLComponent> componentMap = new HashMap<DLComponent, DLComponent>()   // map old component to new component

    private DLComponent activeDrawing = null

    public DLComponentCloner(ComponentModel importedComponentModel, ComponentModel originalComponentModel, ObjectModel importedObjectModel, ObjectModel targetObjectModel){
    	this.importedComponentModel = importedComponentModel
    	this.targetComponentModel = originalComponentModel
    	this.importedObjectModel = importedObjectModel
    	this.targetObjectModel = targetObjectModel
    	setLastDLId()
    }

    public DLComponent getActiveDrawing(){
        return activeDrawing
    }

    private void setLastDLId(){

        int lastID = 0

        for(DLObject o:targetObjectModel.getObjects()){

            String text = o.getDlid().toString().substring(5);
			int _id = Integer.parseInt(text);
            //log.trace("${o.getDlid()}")
            if(_id>lastID ){
                lastID = _id
            }
        }
        log.trace("setLastDLId = $lastID")

        Dlid.resetFactory(lastID)

    }
    /**
     * clone all components in importedComponentModel to targetComponentModel
     */
    public List<DLComponent> cloneAll(){
	    List<DLComponent> newDrawings = []

        // create new components
	    //get all drawing components
	    def drawings = importedComponentModel.getComponentsInRole("drawing")
	    for (DLComponent drawing: drawings) {
    	    try{
    	        // clone component
                log.trace("clone drawing:" + drawing.toString())
                DLComponent newDrawing = cloneComponent(drawing)
                componentMap.put(drawing, newDrawing)
                log.trace("after cloning:" + newDrawing.toString())
                cloneRecursiveChildren(drawing, newDrawing)
                //set active drawing
                activeDrawing = newDrawing
		        newDrawings.add( newDrawing )
    	    }catch(Exception e){
                log.error(log.getStackTrace(e))
                throw new ModelCatastrophe(e)
    	    }

    	}

        // Clone proxies. Must be done separately and not during anchor cloning, like we did in ConfigurationReplacer,
        // because the proxy may be on a drawing that hasn't been cloned at the time that the anchor is being cloned. Must
        // also be before the association iterator since they all need to be in place before be start associating.
        //importedComponentModel.getComponents().each{c->
		for(def c : importedComponentModel.getComponents() ){
            cloneProxies(c)
        }

        // associate new components
       // importedComponentModel.getComponents().each{c->
		for(def c : importedComponentModel.getComponents() ){
            log.trace("old component:($c)")
            log.trace("new component mapped old one:" + componentMap.get(c).toString())
            cloneAssociation(c)
        }

	    return newDrawings
    }

    private void cloneRecursiveChildren(DLComponent aComponent, DLComponent bComponent){
		// get child components
		def childComponents = aComponent.getChildComponents()
		for (DLComponent child: childComponents) {

            // Skip proxies. They get handled later after all concrete components have been created.
            if( child.hasRole("proxy") ) {
                continue
            }

		//childComponents.each{child->
		    try{
		        DLComponent newChild
                if(componentMap.containsKey(child)){
                    newChild = componentMap.get(child)
                }else{
                    log.trace("clone component:" + child.toString())
                    newChild = cloneComponent(child)

                    //map old and new component
                    if(newChild!=null){
                       componentMap.put(child, newChild)
                    }
                }

                if(newChild!=null){
                    bComponent.addChild(newChild)
                    cloneRecursiveChildren(child,newChild)
                }
		    }catch(Exception e){

		        log.error(log.getStackTrace(e))
                continue

		    }

		}
    }



	/**
	 * clone a component from importedComponentModel to targetComponentModel
	 */
	public DLComponent cloneComponent(DLComponent c){

		//if staticchild component, pass
		if(c.hasRole("staticchild")){
			return null
		}

		// for staticchildplacable component
		if(c.hasRole("staticchildplaceable")){
			return cloneStaticChildrenComponent(c)
		}

		//1. create new component and copy properties
		DLComponent newComponent
		try {
			newComponent = targetComponentModel.newComponent(c)
		}catch(UnknownComponentException e) {
			//todo: issue a failure report at the end of this process
			log.error("Attempted to clone unknown component type ${c.getType()}", "default")
			return null
		}

    	//2. copy ES keys and oldVals
        c.listRootNames().each { root ->
            if (newComponent.hasRoot(root)) {
                log.trace("copyObjectData: root:" + root)
                DLObject newRootObj = newComponent.getRoot(root)
                DLObject oldRootObj = c.getRoot(root)
                log.trace("old DLObject $oldRootObj")
                log.trace("new DLObject $newRootObj")

                copyObject(oldRootObj, newRootObj)

                //don't recurs into children for these guys, we'll get there in due course
                if (!c.isContainerOrNetwork() && ! c.hasClass("dynamicchildplaceable")) {
                    copyChildren(oldRootObj, newRootObj, c.getType())
                }

            }
        }

		return newComponent
	}
	
	/**
	 * Clone staticchildplacable role component
	 */
	public DLComponent cloneStaticChildrenComponent(DLComponent c){
	    // for staticchildplacable component, componentModel will create parent component and its child components together
        DLComponent newComponent
	    try {
			newComponent = targetComponentModel.newComponent(c)
		}catch(UnknownComponentException e) {
			//todo: issue a failure report at the end of this process
			log.error("Attempted to clone unknown component type ${c.getType()}", "default")
			return false
		}

        //2. copy ES keys and oldVals for parent component
        if(newComponent!=null)
            copyRootObjects(c, newComponent)

        // 2.1 copy ES keys and oldVals for child components
        for( DLComponent oldChild : c.getChildComponents() ) {
            String childId = oldChild.getPropertyValue("child_id")
            log.trace("child id:" + childId)
            DLComponent newChild
            newComponent.getChildComponents().each{it->
                if(it.getPropertyValue("child_id").equals(childId)){
                    newChild=it
                }
            }
            log.trace("oldChild ($oldChild)")
            log.trace("newChild ($newChild)")
            if(newChild!=null){
                componentMap.put(oldChild, newChild)
                copyRootObjects(oldChild, newChild)
                addToContainer(newComponent, newChild)
            }
        }

        return newComponent
	}

    /**
    *   Add child component to parent component for staticchildrenplacable component
    **/
    void addToContainer(DLComponent parent,DLComponent child){
        log.trace("attempt to add rack child ($child) to $parent containers")

        def containers = parent.getParentContainers()
        containers.each{container->
            container.addChild(child)
            log.trace("add rack child ($child) to container $container")

        }

    }

	/**
	    copy ES keys and oldVals for component
	**/
	private void copyRootObjects(DLComponent oldComponent, DLComponent newComponent){
         oldComponent.listRootNames().each { root ->
            if (newComponent.hasRoot(root)) {
                log.trace("copyObjectData: root:" + root)
                DLObject newRootObj = newComponent.getRoot(root)
                DLObject oldRootObj = oldComponent.getRoot(root)
                copyObject(oldRootObj, newRootObj)

                //don't recurse into children for these guys, we'll get there in due course
                if (!oldComponent.isContainerOrNetwork() && ! oldComponent.hasClass("dynamicchildplaceable")) {
                    copyChildren(oldRootObj, newRootObj, oldComponent.getType())
                }
            }
        }
	}

    /**
     * Creates new proxies for each of the proxies in the given imported component.
     *
     * @param cOld  The original imported component being cloned.
     */
    private void cloneProxies( DLComponent cOld ) {

        for( DLProxy oldProxy : cOld.getProxies() ) {
            log.trace("cloning proxy: ${oldProxy}  Anchor: $cOld")

            DLComponent cNew = componentMap.get( cOld )

            // New proxy with the new component as the anchor.
            DLProxy newProxy = DLProxy.cloneProxy( cNew, oldProxy )

            // Assign the parents to the new instance of each parent.
            for( DLComponent oldProxyParent : oldProxy.getParentComponents() ) {
                componentMap.get( oldProxyParent ).addChild( newProxy )
            }

            componentMap.put( oldProxy, newProxy )
        }
    }


    /**
        copy association
    **/
    private void cloneAssociation(DLComponent c){
        log.trace("cloneAssociation:" +c.toString())
        for(Consumer consumer : c.getAllConsumers()){
            for(Pair<DLComponent,String> oip: consumer.listProducerComponents()) {

                DLComponent producer = oip.a

                log.trace("old producer:" + producer.toString() + " :: ${oip.b}")

                // get consumerId
                String consumerId = consumer.getId()

                // get new producer DLComponent
                //DLObject newProducerObj = dlIdMap.get(oip.a)
                DLComponent newProducer = componentMap.get(producer)
                log.trace("new producer: " + newProducer)

                // get producerId
                String producerId = oip.b

                DLComponent newConsumer = componentMap.get(c)
                //DLComponent void associate(String consumerId, DLComponent producer, String producerId) {
                // associate
                log.trace("associate with new component:" + newConsumer.toString() + " with " + newProducer.toString())
                log.trace("consumer Id:" + consumerId + " producer Id:" + producerId)
                newConsumer.associate(consumerId, newProducer, producerId)
            }
        }

         //same for conducers
        def conducerCheck = []
        for ( Conducer d : c.getAllConducers() ){
            Consumer consumer = d.getConsumer()

            for(Pair<DLComponent,String> oip: consumer.listProducerComponents()) {

                if ( ! oip in conducerCheck ){

                    DLComponent producer = oip.a

                    // get consumerId
                    String consumerId = consumer.getId()

                    // get new producer component
                    DLComponent newProducer = componentMap.get(producer)
                    //get producerId
                    String producerId = oip.b

                    // get new consumer
                    DLComponent newConsumer = componentMap.get(c)

                    log.trace("associate with new component:" + newConsumer.toString() + " with " + newProducer.toString())
                    log.trace("consumer Id:" + consumerId + " producer Id:" + producerId)
                    newConsumer.associate(consumerId, newProducer, producerId)

                    conducerCheck += oip

                }
            }
        }
    }

    /**
	 * To copy a DLObject, we need to move three things over:
	 * The ES Key (if present)
	 * Any oldValues
	 * And overrides on values
	 * @param oldAndBusted
	 * @param newHotness
	 */
	private void copyObject(DLObject oldAndBusted, DLObject newHotness) {

		if (!oldAndBusted.getType().equals(newHotness.getType())) {
			log.trace("Objects not same type, not updating: $oldAndBusted $newHotness")
			return
		}

		//log.trace("Copy Object Contents: $oldAndBusted to $newHotness")
		//log.trace("old Key:" + oldAndBusted.getKey())
		newHotness.setKey(oldAndBusted.getKey())

		oldAndBusted.getPropertiesByType("setting").each { s ->
			Setting oldProp = (Setting) s
			if (oldProp.getOldValue() != null && newHotness.hasObjectProperty(oldProp.getName())) {
				Setting hotProp = (Setting) newHotness.getObjectProperty(oldProp.getName())
				hotProp.setOldValue(oldProp.getOldValue())
			}

			if (oldProp.getOverride() != null && newHotness.hasObjectProperty(oldProp.getName())) {
				Setting hotProp = (Setting) newHotness.getObjectProperty(oldProp.getName())
				hotProp.setOverride(oldProp.getOverride())
			}

			//special case for sync timestamp
			if (oldProp.getName().equalsIgnoreCase("lastExportTs")) {
				Setting hotProp = (Setting) newHotness.getObjectProperty(oldProp.getName())
				hotProp.setValue(oldProp.getValue())
			}

		}

		//same thing, but for oldvalues and overrides on tags
		oldAndBusted.getProperties().each { oldProp ->
			if (newHotness.hasObjectProperty(oldProp.getName())) {
				if (oldProp.hasTags()) {
					Property newProp = newHotness.getObjectProperty(oldProp.getName())
					oldProp.getTags().each { oldTag ->
						if (newProp.hasTag(oldTag.getTagName())) {
							def newTag = newProp.getTag(oldTag.getTagName())
							newTag.setOldValue(oldTag.getOldValue())
							newTag.setOverride(oldTag.getOverride())
						}
					}
				}
			}
		}
	}

	private void copyChildren(DLObject oldRootObj, DLObject newRootObj, String componentType) {
		//oldRootObj.getChildren().each { childObj ->
		for(def childObj : oldRootObj.getChildren() ){
			def matchingChild = findMatchingChild(childObj, newRootObj.getChildren(), componentType)
			if (matchingChild != null) {
				copyObject(childObj, matchingChild)
				//also recurse all the way down
				copyChildren(childObj,matchingChild,componentType)
			} else {
				log.warn("No matching child found for $childObj in $componentType")
			}
		}
	}

	/**
	    Find matching child object.
	    came from ConfigurationReplacer tool
	**/
	private DLObject findMatchingChild(DLObject from, List<DLObject> to, String componentType) {
		//match based on name
		//then child order
		//then give up
		DLObject result = null
		boolean useChannel = false
		def updateByChannel = CentralCatalogue.INSTANCE.getUpdateByChannelProperties()
		//println "updateByChannel has ${updateByChannel.stringPropertyNames()}"
		if (updateByChannel.stringPropertyNames().contains(componentType)) {
			log.info("Updating $componentType will use channel numbers, not sensor names")
			useChannel = true
		}

		if (from.hasObjectProperty("name") && !useChannel) {
			String fromName = from.getObjectProperty("name").getValue()

			for (DLObject t: to) {
				if (t.hasObjectProperty("name") && t.getObjectProperty("name").getValue().equals(fromName) && t.getType().equals(from.getType())) {
					result = t
					break; //from for loop
				}

				//if no names match but there is only one child of the same type, let's assume we want to use that
				if (result == null && to.size() == 1 && to.first().getType().equals(from.getType() )){
					log.warn("No matching names, but ${to.first()} is the only child, so that is being matched to $to")
					result = to.first()
				}


			}
		} else if (from.hasObjectProperty("channel") && useChannel) {
			int fromChannel = from.getObjectProperty("channel").getValue()
			for (DLObject t: to) {
				if (t.hasObjectProperty("channel") && t.getObjectProperty("channel").getValue().equals(fromChannel)) {
					result = t
					break; //from for loop
				}
			}

		} else if(to.size() == 1 && to.first().getType().equals(from.getType() )) {
			log.warn("No matching names, but ${to.first()} is the only child, so that is being matched to $from")
			result = to.first()
		} else {
			//giving up!
			result = null
		}

		//if(result){
		//	log.trace("Found matching child for $from '${from?.getName()}' as $result '${result?.getName()}'")
		//}
		return result;
	}

}
