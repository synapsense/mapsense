package DeploymentLab.Model

import DeploymentLab.Exceptions.ModelCatastrophe
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.Security.NetworkKey
import DeploymentLab.Security.PanIDFactory
import DeploymentLab.SelectModel
import DeploymentLab.UndoBuffer
import DeploymentLab.channellogger.Logger

/**
 * @author Vadim Gusev
 * @since Aireo
 * Date: 28.11.13
 * Time: 13:48
 */
class WSNController implements ModelChangeListener{
	private static final Logger log = Logger.getLogger(WSNController.class.getName())

	private static final WSN_NETWORK_TYPE = "network"
	private static final WSN_GATEWAY_TYPE = "remote-gateway"
	private static final ROOM_TYPE = "room"

	private ComponentModel cmodel
	private SelectModel smodel
	private UndoBuffer undoBuffer
	private DeploymentPanel deploymentPanel

	WSNController(ComponentModel cm, SelectModel sm, UndoBuffer ub, DeploymentPanel dp) {
		cmodel = cm
		deploymentPanel = dp
		smodel = sm
		undoBuffer = ub
	}

	public void componentPropertyChanged(def who, def propertyName, def oldVal, def newVal) {
		//we don't want to get in the way if replace config is running right now
		if (cmodel.isMetamorphosizing() || who.isMetamorphosizing()) { return }
		//log.trace("property changed on $who : $propertyName")
		switch (propertyName) {
			case "metamorphosizing":
			case "manualWSNConfig":
			case "x":
			case "y":
				this.updateComponent(who)
				break
		}
	}

	public void gatewayPropertyChanged(def who, def propertyName, def oldVal, def newVal) {
		//we don't want to get in the way if replace config is running right now
		// and not if the gateway is being dragged since that makes the drag stutter or freeze.
		if (cmodel.isMetamorphosizing() || who.isMetamorphosizing()) { return }
		//log.trace("property changed on $who : $propertyName")
		switch (propertyName) {
			case "metamorphosizing":
			case "x":
			case "y":
				this.updateGateway(who)
				break
		}
	}

	@Override
	void componentAdded(DLComponent component) {
		if (cmodel.typeCanBeChild(WSN_NETWORK_TYPE, component.getType(), true)) {
			if (component.getType().equals(WSN_GATEWAY_TYPE)){
				//assignGatewayToNetwork(component)
				component.addPropertyChangeListener(this, this.&gatewayPropertyChanged)
			}else {
				component.addPropertyChangeListener(this, this.&componentPropertyChanged)
			}
		}
	}

	@Override
	void componentRemoved(DLComponent component) {
		if (cmodel.typeCanBeChild(WSN_NETWORK_TYPE, component.getType(), true)) {
			component.removePropertyChangeListener(this)
		}
	}

	@Override
	void childAdded(DLComponent parent, DLComponent child) {
		if (cmodel.isMetamorphosizing()) { return }
		if (parent.getType().equals(ROOM_TYPE) && cmodel.typeCanBeChild(WSN_NETWORK_TYPE, child.getType(), true)){
			log.trace("Yo yo! We add something to room!")
			if (child.getType().equals(WSN_GATEWAY_TYPE)){
				updateGateway(child)
			}   else{
				updateComponent(child)
			}
		}

		if (parent.getType().equals(WSN_NETWORK_TYPE) && child.getType().equals(WSN_GATEWAY_TYPE)){
			updateGateway(child)
		}
	}

	@Override
	void childRemoved(DLComponent parent, DLComponent child) {
		if (cmodel.isMetamorphosizing()) { return }
		if (parent.getType().equals(ROOM_TYPE) && cmodel.typeCanBeChild(WSN_NETWORK_TYPE, child.getType(), true)){
			log.trace("Yo yo! We remove something from room!")
			if (child.getType().equals(WSN_GATEWAY_TYPE)){
				updateGateway(child, parent)
			}   else{
				updateComponent(child)
			}
		}

		if (parent.getType().equals(WSN_NETWORK_TYPE) && child.getType().equals(WSN_GATEWAY_TYPE)){
			updateGateway(child)
		}
	}

	@Override
	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void modelMetamorphosisStarting(MetamorphosisEvent event) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void modelMetamorphosisFinished(MetamorphosisEvent event) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	public static void updateComponent(DLComponent component) {
		// Ignore proxies and static children (rowlite & hrow kinds) since they are based on their host component.
		if( component.hasRole( ComponentRole.PROXY ) || component.hasRole( ComponentRole.STATIC_CHILD) ) return

		log.trace("Check '${component.getName()}' component WSN assignment...")

		 // Don't process if it is being deleted anyway
		if( component.isDeleting ) {
			log.trace("'${component.getName()}' ignored since it is being deleted.")
			return
		}

		if ((Boolean) component.getPropertyValue("manualWSNConfig")){
			log.trace("'${component.getName()}' component has manual WSN assignment.")
			return
		}

		DLComponent network = findNearestNetworkForComponent(component)
		if (network){
			assignComponentToNetwork(component, network)
			return
		}

		throw new ModelCatastrophe("Project has no WSN Networks. It must has at least one network when WSN component placed on Drawing.")
	}

	protected static DLComponent createNetwork(DLComponent component){
		DLComponent network = component.getModel().newComponent('network')
		network.setPropertyValue('name', "WSN Network")

		List<DLComponent> drawings = component.getModel().getComponentsByType('drawing')
		if (drawings.size() == 0) {
			throw new ModelCatastrophe("There are no Drawings in this Project File!")
		}
		DLComponent activeDrawing = component.myDrawing // drawings.get(drawings.size() - 1)
		activeDrawing.addChild(network)
		//smodel.getActiveDrawing().addChild(network)

		//generate a PAN ID for WSN Networks
		network.setPropertyValue('pan_id', PanIDFactory.makePanID(component.getModel()))

		//also make new network keys
		def p = NetworkKey.makeNetworkKeys()
		network.setPropertyValue('networkPublicKey', p.a)
		network.setPropertyValue('networkPrivateKey', p.b)

		return network
	}

	public static DLComponent findNearestNetworkForComponent(DLComponent component){
		//in a room?
		List<DLComponent> rooms = component.getParentsOfType(ROOM_TYPE)
		if (rooms.size() == 1){
			//yes, in a room
			List<DLComponent> gateways = rooms.first().getChildrenOfType(WSN_GATEWAY_TYPE)
			if (!gateways.isEmpty()){
				//find nearest gateway in a room
				DLComponent nearest = findNearestComponent(component, gateways)
				DLComponent network = nearest?.getParentsOfType(WSN_NETWORK_TYPE)?.first()
				if (network){
					log.trace("Node will assigned to network '${network.getName()}' in a room '${rooms.first().getName()}'.")
					return network //if we found gateway assigned to network
				}
				log.trace("Room '${rooms.first()}' has no gateways with network.")
			}
			log.trace("Room '${rooms.first()}' has no gateways.")
		}
		// no, out of room or room has no gateway
		//drawing has a gateway?
		List<DLComponent> gateways = component.getModel().getComponentsByType(WSN_GATEWAY_TYPE, component.getDrawing())
		if (!gateways.isEmpty()){
			//find nearest gateway on drawing
			DLComponent nearest = findNearestComponent(component, gateways)
			DLComponent network = nearest?.getParentsOfType(WSN_NETWORK_TYPE)?.first()
			if (network){
				log.trace("Node will assigned to '${nearest.getName()}' Gateway's network '${network.getName()}'.")
				return network //if we found gateway assigned to network
			}
			log.trace("No one gateway has no network.")
		}
		//drawing has no gateways, looking for any network
		List<DLComponent> networks = component.getModel().getComponentsByType(WSN_NETWORK_TYPE, component.getDrawing())
		if (!networks.isEmpty()){
			log.trace("Node will assigned to network '${networks.first().getName()}' on Drawing.")
			return networks.first()
		}

		return createNetwork(component);
	}

	private void updateGateway(DLComponent gateway, DLComponent additionalRoom = null){
		if (cmodel.isMetamorphosizing()) { return }
		log.trace("Check Gateway ${gateway.name} WSN assignment")
		// every time when gateway is moving we should check following components:
		// 1. WSN Components that not related to any room
		// 2. WSN Components that located at the same room
		// 3. WSN Components that located in a room, which we left

		Set<DLComponent> roomToCheck = new HashSet<>()
		// add all rooms without GW
		cmodel.getComponentsByType(ROOM_TYPE, smodel.activeDrawing).each {
			if (it.getChildrenOfType(WSN_GATEWAY_TYPE).isEmpty()){
				roomToCheck.add(it)
			}
		}
		// add orphanage
		roomToCheck.add( cmodel.getOrphanage( smodel.activeDrawing ) )
		// add additional room
		if (additionalRoom) roomToCheck.add(additionalRoom)
		// add parent room
		roomToCheck.addAll(gateway.getParentsOfType(ROOM_TYPE))

		// check components
		roomToCheck.each { room ->
			room.getChildComponents().each {
				if (cmodel.typeCanBeChild(WSN_NETWORK_TYPE, it.getType()) && !it.getType().equals(WSN_GATEWAY_TYPE)){
					updateComponent(it)
				}
			}
		}
	}

	private static DLComponent findNearestComponent(DLComponent from, List<DLComponent> to){
		DLComponent nearest = null;
		double nearestPath = Double.POSITIVE_INFINITY;
		for (DLComponent dlc : to){
		    double path = getPath(from, dlc);
			def nets =  dlc.getParentsOfType(WSN_NETWORK_TYPE)
			if (path < nearestPath && !nets.isEmpty() && nets.first()){
				nearestPath = path;
				nearest = dlc;
			}
		}

		return nearest;
	}

	private static double getPath(DLComponent from, DLComponent to){
		double x1 = (double) from.getPropertyValue("x")
		double y1 = (double) from.getPropertyValue("y")
		double x2 = (double) to.getPropertyValue("x")
		double y2 = (double) to.getPropertyValue("y")

		return Math.sqrt(Math.pow((x2-x1), 2)+ Math.pow((y2-y1), 2))
	}

	private static void assignComponentToNetwork(DLComponent component, DLComponent network){
		if (!component.isChildComponentOf(network)){
			List<DLComponent> ntws = component.getParentsOfType(WSN_NETWORK_TYPE)
			if (!ntws.isEmpty() && ntws.first()){
				ntws.first().removeChild(component)
			}
			network.addChild(component)
		}
	}
}
