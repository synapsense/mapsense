package DeploymentLab.Model

import DeploymentLab.channellogger.Logger
import org.apache.commons.jxpath.JXPathContext
import DeploymentLab.Exceptions.ModelCatastrophe
import DeploymentLab.CentralCatalogue

/**
 * Represents a DLComponent proxy
 * @author Gabriel Helman
 * @author Ken Scoggins
 * @since Jupiter 2
 * Date: 7/16/12
 * Time: 5:09 PM
 */
public class DLProxy extends DLComponent {
	private static final Logger log = Logger.getLogger(DLProxy.class.getName())

	// Loaded from components.props.
	private static Set<String> proxyableMustHaveRoles
	private static Set<String> proxyableExcludedRoles

    private static final List<String> rolesToDitch = ["rotatable"]
	private static final List<String> secretProxyRoles = ["proxy"]
    private static final List<String> secretProxyObjects = ["proxy"]
	private static final List<String> secretProxyProperties = ["name","x","y","configuration"]

    /**
     * Checks to see if the component is one that can have a proxy.
     *
     * @param anchor  The component to check.
     * @return        True if the component can have a proxy.
     */
    public static boolean canProxy( DLComponent anchor ) {

	    // Dynamically load the lists we need.
	    if( proxyableMustHaveRoles == null ) {
		    proxyableMustHaveRoles = new HashSet<String>()
		    proxyableMustHaveRoles.addAll( CentralCatalogue.getComp('proxy.mustHaveRoles').split(',') )
	    }

	    if( proxyableExcludedRoles == null ) {
		    proxyableExcludedRoles = new HashSet<String>()
		    proxyableExcludedRoles.addAll( CentralCatalogue.getComp('proxy.excludedRoles').split(',') )
	    }

	    // Check the roles it must have in order to have a proxy.
	    if( !anchor.rolesSet().containsAll( proxyableMustHaveRoles ) ) {
		    return false
	    }

	    // Check the roles that are never allowed. Iterate on the component roles since it should be a smaller list.
	    for( String role : anchor.rolesSet() ) {
		    if( proxyableExcludedRoles.contains( role ) ) {
			    return false
		    }
	    }

	    // The primary reason for an anchor is for associations. So if it can't do that, then don't bother. This
	    // eliminates a lot of things, like the Drawing icons, without explicitly listing a bunch of roles and types.
	    if( anchor.getNumberOfProducers() == 0 && anchor.getNumberOfConsumers() == 0 ) {
		    return false
	    }

	    return true
    }


    /**
     * Factory method to generate a proxy based on the specified anchor component. This version accepts an existing
     * proxy and creates another proxy of its anchor, essentially making a copy of the proxy.
     *
     * @param proxy  The proxy to copy.
     * @return       A proxy of the provided proxy's anchor component.
     */
    public static DLProxy createProxy( DLProxy proxy ){
         return createProxy( proxy.getAnchor() )
    }


	/**
	 * Factory method to generate a proxy based on the specified anchor component.
     *
	 * @param anchor  The component to proxy.
     * @return        A proxy for the provided component.
	 */
	public static DLProxy createProxy( DLComponent anchor ){
		def cm = anchor.getModel()
		//def om = anchor.getOModel()
		def typeXML = cm.getComponentType( anchor.getType() )

		//some magic needs to happen here?

		DLProxy proxy = cm.deserializeComponentProxy(typeXML,anchor)

		//also here, possibly?

		return proxy
	}


    /**
     * Factory method to generate a proxy based on the specified anchor component and also copy the properties from
     * the specified proxy.
     *
     * @param anchor   The component to proxy.
     * @param toClone  The proxy whose properties will be cloned.
     *
     * @return         A proxy for the provided component.
     */
    public static DLProxy cloneProxy( DLComponent anchor, DLProxy toClone ) {
        DLProxy proxy = createProxy( anchor )

        proxy.mapProperties().each { pName, pObj ->
            if( pObj.isEditable() && toClone.getPropertyValue( pName ) != null ) {
                proxy.setPropertyValue( pName, toClone.getPropertyValue( pName ) )
            }
        }

        return proxy
    }


	/**
	 * The anchor component for this proxy.
	 */
	DLComponent anchor

	/**
	 * Private (and only) constructor.  If you need a proxy, use the factory method.
	 * @param _cm
	 * @param _om
	 * @param cXml
	 * @param proxyAnchor
	 */
	protected DLProxy(ComponentModel _cm, ObjectModel _om, def cXml, DLComponent proxyAnchor) {
		super() //as a reminder, we're calling the no-arg constructor implicitly

        // Self preservation. Should have been checked before it ever gets here.
        if( !canProxy( proxyAnchor ) ) {
            throw new IllegalArgumentException( CentralCatalogue.getUIS("proxy.cannotProxy") )
        }

		//we want the objects in the xml to reference the anchor component (?)
		this.anchor = proxyAnchor

		cmodel = _cm
		omodel = _om
		type = cXml.@type.text()
		classes = new HashSet<String>()
		cXml.@classes.text().split(",").each{ r ->
			if(!rolesToDitch.contains(r)){
				classes.add(r)
			}
		}

		// Add any secret proxy roles.
		secretProxyRoles.each { role ->
			classes.add( role )
		}

		grouppath = cXml.@grouppath.text()
		filters = cXml.@filters.text()

		// Proxies are never exported.
		_exportable = false

		_dlid = Dlid.getDlid(cXml.@dlid.text())

        if( _dlid == null ) {
            // No DLID? Instead of forcing an upgrade for old non-DLID comps, just drop in a new DLID.
            _dlid = Dlid.getDlid()
            log.debug("Gave a DLID-less component a new DLID: $_dlid  $type");
        }

		//log.debug("deserialize component of type $type")

		for(def dXml :cXml.display.children() ){
			displaySettings[dXml.name()] = dXml.text()
		}

		displaySettings["proxy"] = "true"

		myContext = JXPathContext.newContext(this)

        // Grab the anchor objects.
        _objects = new HashMap<>(anchor._objects)

        // Now for the secret proxy objects. This is the magic making unserializing vs new component work.
        for( def oXml : cXml.object ){
            String asVar = oXml.@as.text()
            assert asVar != ""
            if( secretProxyObjects.contains( asVar ) ) {
                if( oXml.@dlid != '' ) {
                    def obj = omodel.getObject( omodel.getDlid( oXml.@dlid.text() ) )
                    _objects[asVar] = obj
                    _managedObjects += obj
                } else {
                    log.debug("Deserialized proxy XML has a $asVar object with no DLID. Upgrade or broky?")
                }
            }
		}

        // If our secret objects still don't exist after the above, create new ones.
        for( String secretObj : secretProxyObjects ) {
            if( _objects[secretObj] == null ) {
                // todo: Can we get these templates from the object model instead and not have a hardcoded switch?
                def proxyXml = null
                switch( secretObj ) {
                    case "proxy" :
                        proxyXml = new XmlSlurper().parseText("<object type='proxy' exportable='false,false'><dlid>DLID:-1</dlid></object>")
                        break;
                    default :
                        log.debug("Unsupported proxy object: $secretObj")
                }

                if( proxyXml != null ) {
                    def obj
                    def created = []
                    (obj, created) = omodel.deserializeObject(proxyXml)
                    _objects[ secretObj ] = obj
                    _managedObjects += created + obj

	                // Set the special proxy object exportability.
	                obj.setIsExportable( _exportable )
	                for( DLObject co : created ) {
		                co.setIsExportable( _exportable )
	                }
                }
            }
        }

        // Register the objects for JXPath usage.
		_objects.each{ asVar, _o ->
			myContext.getVariables().declareVariable(asVar, _o)
		}

		/*
		//no dockings for proxies
		for( def pbXml : cXml.docking ){
			String sFrom = pbXml.@from.text()
			String sTo = pbXml.@to.text()
			Property pFrom = myContext.getValue(sFrom)
			if(pFrom instanceof Dockingpoint) {
				DLObject pTo = myContext.getValue(sTo)
				pFrom.setReferrent(pTo)
			} else if(pFrom instanceof DeploymentLab.Model.Collection) {
				for(def it : myContext.iterate(sTo)){
					pFrom.add(it)
				}
			} else if (pFrom instanceof Pier){
				//String sProp = pbXml.@property.text()
				//DLObject pTo = myContext.getValue(sTo)
				Property pTo = myContext.getValue(sTo)

				//println "constructing pier docking: pto=$pTo, owner is ${pTo.getOwner()}"

				pFrom.setReferent(pTo)
			} else {
				throw new IncompatibleTypeException("Docking 'from' must be a Dockingpoint or Collection")
			}
		}
		*/

		for(def pXml : cXml.property){
			if(secretProxyProperties.contains(pXml.@name.text())){
				_properties[pXml.@name.text()] = new ComponentProperty(pXml)
				_varMap[pXml.@name.text()] = []
			}
		}

		/*
		// no bindings in proxies either
		for( def bXml : cXml.varbinding ){
			ComponentBinding binding = new ComponentBinding(bXml, myContext, this)
			for(String varName : binding.listVariables()){
				//println "$type: constructing binding $varName "
				try {
					_varMap[varName] += binding
				} catch (NullPointerException e){
					log.fatal("Variable '$varName' does not exist for component type $type", "message")
				}
			}
			_varBindings += binding
		}

		for(ComponentBinding binding : _varBindings){
			binding.eval(getPropertyValueMap(binding.listVariables()))
		}
		*/
		_varBindings = []

		for(def xml : cXml.consumer){
			_consumers[xml.@id.text()] = new Consumer(this, xml, myContext)
		}

		for(def xml : cXml.producer){
			//log.info("add a producer:" + xml.@id.text())
			_producers[xml.@id.text()] = new Producer(this, xml, myContext)
		}

		for(def xml : cXml.conducer){
			Producer pro = new Producer(this, xml.producer, myContext)
			Consumer con = new Consumer(this, xml.consumer, myContext)
			_conducers[xml.@id.text()] = new Conducer(this, xml, pro, con)
			_producers[ pro.id ] = pro
			_consumers[ con.id ] = con
		}

		_managedObjects += anchor._managedObjects

		//no macids in proxies either
		macids = []

		//do we allow placeable children proxies?  (probably not)

		//placeablechildren
		//used for pre-Mars hrow components
		if(cXml.placeablechildren.@value.text().length() > 0)
			placeableChildren = cXml.placeablechildren.@value.text().split(',')
		else
			placeableChildren = []


		if(cXml.children.size()>0){
			placeableChildType = cXml.children.@type.text()

			for(def childXml : cXml.children.child){
				String name=childXml.@name.text()
				String channel = childXml.@channel.text()
				String display = childXml.@display.text()

				//_placeableChildren[name] = channel
				_placeableChildren[name] = [:]
				_placeableChildren[name]["channel"] = channel
				_placeableChildren[name]["display"] = display
			}
		}

		cXml.synchronizedproperties.property.each{ p->
			this.synchronizedProperties.put(p.@parent.text().toString(), p.@child.text().toString())
		}

        // Tweak the default name a little so it's not the same as the anchor.
        setPropertyValue('name', generateDefaultName( anchor.getPropertyStringValue('name') ))

        // Listen for anchor property changes that we want to keep in sync.
        anchor.addPropertyChangeListener( this, this.&anchorPropertyChanged )

        // Notify our anchor that we exist.
        anchor.addProxy( this )
	}



	void serialize(groovy.xml.MarkupBuilder builder, boolean serializeImages = true) {
		builder.proxy('type': type, 'classes': classes.join(','), 'grouppath': grouppath, 'filters': filters, 'dlid': _dlid, 'exportable': _exportable) {
			display{
				displaySettings.each{ n, v ->
					"$n"(v)
				}
			}

            _objects.each{ k, _o ->
                if( secretProxyObjects.contains(k) ) {
                    object('type': _o.type, 'as': k, 'dlid': _o.dlid)
                }
            }

			_properties.each{ k, p ->
				p.serialize(builder, serializeImages)
			}
			_varBindings.each{ b ->
				b.serialize(builder)
			}
			_producers.each{ id, p ->
				if ( ! p.hasConducer() ){
					p.serialize(builder)
				}
			}
			_consumers.each{ id, c ->
				if ( ! c.hasConducer() ){
					c.serialize(builder)
				}
			}

			_conducers.each{ id, d ->
				d.serialize(builder)
			}


			macids('value': macids.join(','))
			placeablechildren('value': placeableChildren.join(','))

			children('type':placeableChildType){
				_placeableChildren.each{k,v->
					child('name':k, 'channel':v['channel'], 'display':v['display'])

				}
			}

			if(synchronizedProperties.size() > 0){
				builder.synchronizedproperties(){
					synchronizedProperties.each{ k, v ->
						builder.property( parent : k, child: v )
					}
				}
			}

            anchor('value': anchor.getDlid() )
		}
	}

    void anchorPropertyChanged( def who, String prop, def oldVal, def newVal ) {
        if( prop == 'name') {
            // Keep the names in sync if it is still the default.
            if( getPropertyStringValue( prop ).equals( generateDefaultName( oldVal ) ) ) {
                setPropertyValue( prop, generateDefaultName( newVal ) )
            }
        }
    }

    private String generateDefaultName( String baseName ) {
        return "(Proxy) $baseName"
    }

	void setIsExportable( boolean exportable ) {
		// Proxies are always false and can't be changed.
	}

    /**
     * @inheritDoc
     *
     * DLProxy overrides this method so that it only removes its personal objects and not the anchor's objects. It also
     * needs to notify its anchor that it is being removed.
     */
    public void remove() {
        for( DLObject o : getRoots() ) {
            omodel.remove( o )
        }
        anchor.removeProxy( this )
    }

    /**
     * @inheritDoc
     *
     * DLProxy overrides this method so that it only undeletes its personal objects and not the anchor's objects. It also
     * needs to notify its anchor that it has returned from the dead.
     */
    void undelete() {
        for( DLObject o : getRoots() ) {
            omodel.undelete(o)
        }
        anchor.addProxy( this )
    }

    /**
     * @inheritDoc
     *
     * DLProxy overrides this method to stop root objects of the anchor component from being included. Only direct
     * root objects of the proxy itself will be returned by this version. This happens in the super class because
     * the anchor's objects are included in the proxies object list and need to be there for other things to work,
     * including varbindings. However, having them included as a "root" causes other problems, especially with
     * (de)serialization and proxy deletion.
     */
    ArrayList<DLObject> getRoots() {
        if(_roots == null){
            _roots = new ArrayList<DLObject>( secretProxyObjects.size() )

            for( String objName : secretProxyObjects ) {
                _roots.add( getRoot( objName ) )
            }
        }
        return _roots
    }

    /**
     * @inheritDoc
     *
     * DLProxy overrides this method to stop parents of the anchor component from being included. Only direct parents of
     * the proxy itself will be returned by this version. This happens in the super class because parents of all objects
     * are returned, including the anchor's objects that are contained in the proxy. One place where this is a problem
     * is when deleting a proxy. Without this override, it would try to delete itself from the parents of the anchor,
     * which it is not a child of.
     */
    ArrayList<DLObject> getParentObjects() {
        Set<DLObject> sett = new HashSet<DLObject>()
        for( DLObject o: getRoots() ) {
            sett.addAll( o.parents )
        }
        return new ArrayList<DLObject>(sett)
    }

    /**
     * @inheritDoc
     *
     * DLProxy overrides this method to stop the drawing of the anchor from being found as well as the drawing of
     * the proxy. This happens in the super class because drawings are found based on the objects and the proxy
     * contains the anchor's objects as well as its own. This produced 2 drawings, which triggers a ModelCatastrophe.
     */
    public DLComponent getDrawing() {
        if( myDrawing == null ) {
            // todo: PROXY - Should actually do for each of the secretObjects, for future safety.
            java.util.Collection<DLObject> parentObjs = _objects["proxy"].getParents()

            // Should never happen, but I'd like to know if it does!
            if( parentObjs.size() != 1 ) {
                String extra
                if( parentObjs.size() > 1 ) {
                    extra = "PARENTS: " + parentObjs
                }
                throw new ModelCatastrophe("Component is associated with ${parentObjs.size()} Drawings instead of 1. $this'\n$extra" )
            }

            myDrawing = cmodel.getOwner( parentObjs.iterator().next() )
        }

        return myDrawing
    }

    /**
     * @inheritDoc
     *
     * DLProxy overrides this method so that it can filter out the anchor. The super class implementation properly
     * filters out 'this', which is the proxy, not the anchor.
     */
    ArrayList<DLComponent> getChildComponents() {
        List<DLComponent> result = super.getChildComponents()

        // The super impl will filter out 'this' as a child but not the anchor, so filter it out.
        if( result != null ) {
            result.remove( anchor )
        }

        return result
    }

	/**
	 * @inheritDoc
	 */
	public java.util.Collection<DLComponent> getCollectiveDrones() {
		java.util.Collection<DLComponent> drones = anchor.getProxies()
		drones.add( this.getAnchor() )
		return drones
	}

	String toString() {
		if(_properties.containsKey('name')) {
			return "DLProxy('$type' $_dlid > name:" + getPropertyStringValue('name') + ")" //@" + Integer.toHexString(hashCode())
		} else {
			return "DLProxy('$type' $_dlid)" //@" + Integer.toHexString(hashCode())
		}
	}


}
