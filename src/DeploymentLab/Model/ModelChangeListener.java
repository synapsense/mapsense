package DeploymentLab.Model;

/**
 * Defines a listener to the ComponentModel.
 */
public interface ModelChangeListener {
	void componentAdded(DLComponent component);
	void componentRemoved(DLComponent component);

	void childAdded(DLComponent parent, DLComponent child);
	void childRemoved(DLComponent parent, DLComponent child);

	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId);
	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId);

    /**
     * Called when the Component Model is about to be changed in a manner more significant than basic adding and
     * removing of components or changing their properties. This allows listeners to perform special handling or ignore
     * subsequent model change events until the {@link #modelMetamorphosisFinished(MetamorphosisEvent)} notification is
     * received.
     *
     * @param event  The specific change event that is about to start.
     *
     * @since Jupiter
     */
    void modelMetamorphosisStarting( MetamorphosisEvent event );

    /**
     * Called when the Component Model has completed the changes performed after the
     * {@link #modelMetamorphosisStarting(MetamorphosisEvent)} notification.
     *
     * @param event  The specific change event that has just completed.
     *
     * @since Jupiter
     */
    void modelMetamorphosisFinished( MetamorphosisEvent event );
}
