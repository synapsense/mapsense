package DeploymentLab.Model

import DeploymentLab.channellogger.Logger
import DeploymentLab.Pair

/**
 * Represents a pair of producer & consumer objects.
 * @author Gabriel Helman
 * Date: Nov 2, 2010
 * Time: 3:38:16 PM
 */
class Conducer implements Comparable<Conducer>, ComponentTrait {
	private static final Logger log = Logger.getLogger(Conducer.class.getName())
	private DLComponent belongsTo
	private Producer producer;
	private Consumer consumer;

	private String id
	private String name
	private boolean required
	String parcel

	/**
	 * A reference to the other Conducers that this Conducer is connected to.
	 * never access this directly; always use getProsumer().
	 */
	private Set<Conducer> bestFriends = new HashSet<Conducer>()

	public Conducer(DLComponent owner, def pXml, Producer p, Consumer c){
		belongsTo = owner
		id = pXml.@id.text().intern()
		name = pXml.@name.text().intern()
		required = Boolean.parseBoolean(pXml.@required.text())
		parcel = pXml.@parcel.text()
		producer = p
		consumer = c

		producer.addConducer(this)
		consumer.addConducer(this)

//println "New Conducer: $id " //, $datatype"

	}

	public void serialize(groovy.xml.MarkupBuilder builder) {
		builder.conducer('id': id, 'name': name, 'required': required, parcel: parcel ){
			producer.serialize( builder )
			consumer.serialize( builder )
		}
	}

	public void serializeAsPrototype(groovy.xml.MarkupBuilder builder) {
		builder.conducer('id': id, 'name': name, 'required': required, parcel: parcel ){
			producer.serializeAsPrototype( builder )
			consumer.serializeAsPrototype( builder )
		}
	}

	@Override
	String getTraitId() {
		return this.getId()
	}

	@Override
	String getTraitDisplayName() {
		return this.getName()
	}

	public String getName() {
		return name
	}

	public String getId() {
		return id
	}

	public DLComponent getOwner(){
		return belongsTo
	}

	public Producer getProducer(){
		return producer
	}

	public Consumer getConsumer(){
		return consumer
	}

	boolean isRequired() {
		return required
	}

	public String toString(){
		return name
	}

	/**
	 * Add a Conducer to this Conducer's list of associations.
	 * Note that this does *NOT* change the nested Producers and Consumers.
	 * @param newFriend a Conducer that this Conducer will now Prosume.
	 */
	public void prosume(Conducer newFriend){
		bestFriends.add( newFriend )
		log.trace( "Prosuming with $newFriend ::: ${this.toString()} (${bestFriends.size()} prosumers)", "default" )
	}

	/**
     * Remove the provided Conducer from this Conducer's list of associations, and do the same to the Prosumer.
     *
     * @param enemy  The Prosumer to remove.
     * @see #detach(Conducer)
	 */
	public void unProsume( Conducer enemy ) {
        if( enemy != null ) {
            detach( enemy )
            enemy.detach( this )
        }
	}

	/**
     * Remove the provided Conducer from this Conducer's list of associations, leaving the guts of the Prosumer alone.
     *
     * @param enemy  The Conducer to remove.
     * @see #unProsume(Conducer)
	 */
    public void detach( Conducer enemy ) {
        bestFriends.remove( enemy )
        log.trace( "Detached from $enemy ::: ${this.toString()} (${bestFriends.size()} prosumers)", "default" )
	}

    /**
     * Returns all the Conducers that are associated with this Conducer. This method returns a copy of the list.
     * Therefor, if Prosume or unProsume are called while iterating over the returned list, those changes would not be
     * seen until this method is called again.
     *
     * @return The associated Conducers.
     */
	public java.util.Collection<Conducer> getProsumers(){
		//allows us to lazily determine the prosumers if we need to
		if ( bestFriends.isEmpty() ) {
            for( Pair<DLComponent,String> c : producer.listConsumerComponents() ) {
                Consumer producersConsumer = c.a.getConsumer( c.b )
                bestFriends.add( producersConsumer.getConducer() )
            }
		}

        // Defensive copy. Also eliminates ConcurrentModificationException if the caller (un)Prosumes while iterating.
        return (java.util.Collection<Conducer>) bestFriends.clone()
	}

    /**
     * Returns the Components that have Conducers that are associated with this Conducer.
     *
     * @return The associated Components.
     */
	public java.util.Collection<Pair<DLComponent, String>> getProsumerComponents(){
		List<Pair<DLComponent,String>> result = new ArrayList<>()

        for( Conducer other : this.getProsumers() ) {
            result.add( new Pair(other.belongsTo, other.name ) )
        }
		return result
	}

	public java.util.Collection<DLComponent> getAssociatedComponents(){
		def result = new HashSet<DLComponent>()
		for( Conducer other : this.getProsumers() ) {
			result.add( other.belongsTo )
		}
		return result
	}

	/**
	 * Should never be used. This only exists because Groovy 2.4 changed the behavior of how it handles null arguments
	 * for overloaded methods. A null argument means that the Groovy dynamic type logic can't determine the type and
	 * can't determine which overloaded method to use. Prior to 2.4, it would just pick one anyway. As long as they all
	 * handled null inputs with the same result, it was ok. As of 2.4, Groovy will now throw an exception due to the
	 * ambiguous overloading. This could break a lot of things and since sprinkling casts all over the code is kinda
	 * anti-Groovy and really sucks, having a catchall with a non-specific argument is an ugly workaround.
	 *
	 * @param ghost
	 * @return Always false when ghost is null. Throws an exception otherwise since it shouldn't be used.
	 */

	public boolean isProsuming( def ghost ) {
		if( ghost != null ) throw new MissingMethodException("isProsuming", this.getClass(), [ghost])
		return false
	}

    /**
     * Checks to see if the provided Conducer is associated with this Conducer.
     *
     * @param potentialLifePartner  The other Conducer to check.
     *
     * @return  True if the provided Conducer is a associated with this Conducer.
     */
	public boolean isProsuming( Conducer potentialLifePartner ) {
        return getProsumers().contains( potentialLifePartner )
	}

    /**
     * Checks to see if the provided Component has any Conducer that is associated with this Conducer.
     *
     * @param otherComponent  The Component to check.
     *
     * @return  True if the provided Component has a Conducer that is associated with this Conducer.
     */
	public boolean isProsuming( DLComponent otherComponent ){
		if( otherComponent == null ) {
			return false
		}
		boolean result = false
		otherComponent.getAllConducers().each{ omega ->
			if ( this.isProsuming(omega) ){
				result = true
			}
		}
		return result
	}

    /**
     * Checks to see if this Conducer has any known associations.
     *
     * @return True if there is at least 1 association with another Conducer.
     */
	public boolean hasProsumer(){
		return getProsumers().size() > 0
	}

    /**
     * Checks if the provided Conducer can be associated with this conducer.

     * @param blindDate  The other Prosumer to check.

     * @return  True if the provided Conducer can be a Prosumer of this Conducer.
     */
	public boolean canProsume( Conducer blindDate ){
		//println "check for prosuming"
		//println producer.datatype + " == " + blindDate.getConsumer().getDatatype()
		//println consumer.datatype + " == " + blindDate.getProducer().getDatatype()

		if ( producer.canAssociate(blindDate.getConsumer()) && consumer.canAssociate(blindDate.getProducer()) ){
			return true
		}
		return false
	}

	/**
	 * Checks if any association is possible between this conducer and that otherComponent
	 * @param otherComponent  The other Prosumer to check.
	 * @return  True if the provided Component can be a Prosumer of this Conducer.
	 */
	public boolean canProsume( DLComponent otherComponent ){
		boolean result = false;
		otherComponent.getAllConducers().each{ omega ->
			if ( this.canProsume(omega) ){
				result = true
			}
		}
		return result
	}


	/**
	 * The natural ordering of Conducers is considered to be that of their names.
	 * @param o another Conducer to compare to
	 * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
	 */
	int compareTo(Conducer o) {
		return this.getName().compareTo( o.getName() )

	}

	public String getDatatype(){
		return producer.getDatatype()
	}

}
