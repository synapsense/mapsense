package DeploymentLab.Model;

/**
 * Indicates a member of the object model whose value can be edited.
 * Essentially, this is a way to hack duck-typing into a java object model.
 * @author Gabriel Helman
 * Date: 1/26/11
 * Time: 11:48 AM
 * @since Earth
 */
public interface MutableModelValue {
	public void setValue(Object value);
	public Object getValue();
	//to support CE overrides
	public void setOverride(Object value);
	public Object getRealValue();

	//access to the old value
	public Object getOldValue();
	public void setOldValue(Object value);
}
