
package DeploymentLab.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Base class for properties
public abstract class Property implements Comparable<Property> {
	protected final DLObject belongsTo;	// The object that this property belongs to
	protected String name;		// name of the property
	protected ObjectModel oModel;
	//now we're doing tags as a list
	protected List<CustomTag> tags;

	public Property(DLObject _belongsTo, String n) {
		belongsTo = _belongsTo;
		assert belongsTo != null;
		name = n;
		//tags = new HashMap<String,CustomTag>();
		tags = null; //only init if we need it
		//tags = new ArrayList<CustomTag>(1);
		oModel = belongsTo.getObjectModel();
	}

	public String getName() {
		return name;
	}

	public abstract String getType();

	public DLObject getOwner() {
		return belongsTo;
	}

	protected ObjectModel getObjectModel() {
		//return belongsTo.getObjectModel();
		return oModel;
	}


	//custom tag support (should be the same for all property subtypes)

	/**
	 * Retrieves a tag with the specified tag name
	 * @param tagName a tag identifier
	 * @return the CustomTag with that name
	 */
	public CustomTag getTag( String tagName ){
		//return ( tags.get(tagName) );
		if ( this.tags == null ){ return null; }

		for( CustomTag t : tags ){
		    if ( t.getTagName().equals(tagName) ){
				return t;
			}
		}
		return null;
	}

	/**
	 * Removes a tag with the specified tag name
	 * @param tagName a tag identifier
	 * @return the CustomTag that was removed
	 */
	public CustomTag removeTag( String tagName ) {
		//return ( tags.remove(tagName) );
		if ( this.tags == null ){ return null; }
		//for( CustomTag t : tags ){
		for ( int i = 0; i < tags.size(); i ++){
		    if ( tags.get(i).getTagName().equals(tagName) ){
				return tags.remove(i);
			}
		}
		return null;

	}

	/**
	 * Gets the tags as a Collection of CustomTag objects.  This is mainly here to support the JXPath addresses in varbindings.
	 * @return A Collection of this Property's CustomTag objects. 
	 */
	public java.util.Collection<CustomTag> getTags(){
		//return ( tags.values() );
		return tags;
	}

	/**
	 * Checks to see if this property has a tag with the given name.
	 * @param tagName The name of a tag to look for.
	 * @return True if the given tag exists, regardless of content or type.
	 */
	public boolean hasTag( String tagName ){
		//return ( tags.containsKey(tagName) );
		if ( this.tags == null ){ return false; }
		for( CustomTag t : tags ){
		    if ( t.getTagName().equals(tagName) ){
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if a property has any tags at all.
	 * @return True if the property has 1 or more tags.
	 */
	public boolean hasTags(){
		if ( this.tags == null ){ return false; }
		return ( tags.size() > 0 );
	}

	/**
	 * Updates the value of an existing custom tag - will NOT add a new tag if the name is incorrect.
	 * Currently, this is only called during deserialization of a DL file
	 * @param tagName The tag to update
	 * @param value The new value to load into this tag.
	 * @param oldValue
	 * @throws Exception
	 */
	public void updateTag( String tagName, Object value, Object oldValue ) throws Exception {
		//tags.get(tagName).setValue(value);

		if ( this.tags == null ){ return; }
		/*
		for( CustomTag t : tags ){
		    if ( t.getTagName().equals(tagName) ){
				t.setValue(value);
				t.setOldValue(oldValue);
				return;
			}
		}
		*/
		CustomTag t = this.getTag(tagName);
		if ( t == null ) { throw new Exception("Tried to Update Unknown Tag " + tagName + " on Property " + this.getName()); }
		//don't bother with null values; the values are already null, and we don't want the property converter to cry
		if ( value != null ){
			t.setValue(value);
		}

		if (oldValue != null){
			t.setOldValue(oldValue);
		}
		return;

	}

	/**
	 * Adds a new tag to this Property from the parts provided.
	 * Should the tag list be null when this is called, the list will be initialized.
	 * @param tn the name of the new tag
	 * @param tt the tag type of the new tag
	 * @param v the value of the new tag
	 */
	public void addTag( String tn, String tt, Object v ){
		CustomTag added = new CustomTag( this, tn, tt, v );
		//this.putTag(tn, added);
		if ( this.tags == null ){ this.tags = new ArrayList<CustomTag>(1); }
		tags.add(added);
	}

	/**
	 * Adds the CustomTag provided to this Property's list of tags.
	 * Should the tag list be null when this is called, the list will be initialized.
	 * @param t a CustomTag to add to this Property
	 */
	public void addTag( CustomTag t ){
		//this.putTag(t.getTagName(), t);
		if ( this.tags == null ){ this.tags = new ArrayList<CustomTag>(1); }
		tags.add(t);
	}

	/**
	 * Generates a human-readable string of all the tags on this property
	 * @return
	 */
	public String printTags(){
		if (tags == null ) { return ""; }
		String result = "";
		///for ( CustomTag t : tags.values() ){
		for ( CustomTag t : tags ){
			result += t.toString() + " ";
		}
		return ( result );
	}



	@Override
	public int compareTo(Property o) {
		return this.getName().compareTo(o.getName());
	}
}

