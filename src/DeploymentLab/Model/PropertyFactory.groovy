
package DeploymentLab.Model

import org.apache.commons.jxpath.JXPathContext

import DeploymentLab.channellogger.*
import DeploymentLab.Exceptions.*

public class PropertyFactory {
	private static final Logger log = Logger.getLogger(PropertyFactory.class.getName())

	/**
	 * For instantiating new dlobjects from the objects.xml definition
	 * @param _belongsTo the containing DLObject
	 * @param propertyXml the xml from objects.xml
	 * @return an instance of the correct Property subclass
	 */
	static Property createProperty(DLObject _belongsTo, def propertyXml) {
		String type = propertyXml.@type.text();
		Property result
		if(type.equals("setting")) {
			String n = propertyXml.name.text()
			String vt = propertyXml.valuetype.text()
			boolean h = Boolean.parseBoolean(propertyXml.historic.text())
			result = new Setting(_belongsTo, n, vt, h);
			/*
			//having built the setting, now tack some tags onto it
			propertyXml.customtag.each { tag ->
				//println "I'M MAKING TAG '$tag' FOR $n; tag type ${tag.@tagType.text()}"
				result.addTag( tag.@name.text(), tag.@tagType.text(), PropertyConverter.fromXml( tag.@tagType.text(), tag )  )
			}
			//return new Setting(_belongsTo, n, vt, h);
			return result
			*/

		} else if(type.equals("childlink")) {
			String n = propertyXml.name.text()
			def rt = propertyXml.reftype.collect{it.text()}
			return new ChildLink(_belongsTo, n, rt as Set<String>);
			//childinks, as they are not exported to the server as properties, do not get tags
			//result =  new ChildLink(_belongsTo, n, rt as Set<String>);

		} else if(type.equals("collection")) {
			String n = propertyXml.name.text()
			def rt = propertyXml.reftype.collect{it.text()}
			//return new DeploymentLab.Model.Collection(_belongsTo, n, rt as Set<String>);
			result = new DeploymentLab.Model.Collection(_belongsTo, n, rt as Set<String>);

		} else if(type.equals("dockingpoint")) {
			String n = propertyXml.name.text()
			def rt = propertyXml.reftype.collect{it.text()}
			//return new Dockingpoint(_belongsTo, n, rt as Set<String>);
			result = new Dockingpoint(_belongsTo, n, rt as Set<String>);

		} else if(type.equals("pier")) {
			String n = propertyXml.name.text()
			def rt = propertyXml.reftype.collect{it.text()}

			result = new Pier(_belongsTo, n, rt as Set<String>);


		} else if(type.equals("rule")) {
			String n = propertyXml.name.text()
			String vt = propertyXml.valuetype.text()
			String rc = propertyXml.ruleclass.text()
			String rs = propertyXml.schedule.text()
			boolean h = Boolean.parseBoolean(propertyXml.historic.text())
			def bindings = [:]
			propertyXml.binding.each{ binding ->
				bindings[binding.@rulevar.text()] = binding.@property.text()
			}
			//return new Rule(_belongsTo, n, vt, rc, rs, h, bindings);
			result = new Rule(_belongsTo, n, vt, rc, rs, h, bindings);

		} else if(type.equals("groovyrule")) {
			String n = propertyXml.name.text()
			String vt = propertyXml.valuetype.text()
			String rf = propertyXml.rulefile.text()
			String rs = propertyXml.schedule.text()
			boolean h = Boolean.parseBoolean(propertyXml.historic.text())
			def bindings = [:]
			propertyXml.binding.each{ binding ->
				bindings[binding.@rulevar.text()] = binding.@property.text()
			}
			//return new GroovyRule(_belongsTo, n, vt, rf, rs, h, bindings);
			result = new GroovyRule(_belongsTo, n, vt, rf, rs, h, bindings);

		} else {
			throw new UnknownPropertyTypeException("Unknown property type " + type);
		}

		//having built the property, tack any tags onto it
		propertyXml.customtag.each { tag ->
			//println "I'M MAKING TAG '$tag' FOR $result; tag type ${tag.@tagType.text()}"
			result.addTag( tag.@name.text(), tag.@tagType.text(), PropertyConverter.fromXml( tag.@tagType.text(), tag )  )
		}
		return result
	}

	/**
	 * For deserializing an instance out of a DL file into an object that's already been created
	 * @param s
	 * @param xml
	 */
	static void deserialize(Setting s, def xml) {
		Object v = PropertyConverter.fromXml(s.getValueType(), xml.value)
		Object ov = null
		Object override = null

		if(xml.oldvalue){
			ov = PropertyConverter.fromXml(s.getValueType(), xml.oldvalue)
		}

		if(xml.override.size() > 0){
			override = PropertyConverter.fromXml(s.getValueType(), xml.override)
		}

		s.deserialize(v, ov, override)

		//also handle any custom tags
		//def settingTags = s.getAllTags()
		xml.customtag.each{ tag ->
			if ( s.hasTag( tag.@name.text() ) ){
				//update
				//s.putTag(tag.@name.text(), new CustomTag( tag.@name.text(), tag.@tagType.text(), tag.text() ) )
				def currentTag = s.getTag(tag.@name.text())
				//println "updating tag ${tag.@name.text()}  ${currentTag.tagType}  $tag  ${tag.@oldVal}"
				s.updateTag(tag.@name.text(),  PropertyConverter.fromXml( currentTag.tagType, tag ) , PropertyConverter.fromXml( currentTag.tagType, tag.@oldVal ));

				if ( tag.@override.text() != "" ){
					s.getTag(tag.@name.text()).setOverride(  PropertyConverter.fromXml( currentTag.tagType, tag.@override ) )
				}

				//s.updateTag(tag.@name.text(), tag, tag.@oldVal );
			} else {
				//add new, baby!  (really this should never happen.)
				log.warn("I don't know what custom tag named '${tag.@name.text()}' is, so I'm not loading it.")
				//s.putTag(tag.@name.text(), new CustomTag( tag.@name.text(), tag.@tagType.text(), tag.text() ) )
			}
		}

	}

	static void serialize(Setting s, groovy.xml.MarkupBuilder builder) {
		builder.'property'('name': s.getName(), type: "setting", valuetype: s.getValueType() ) {
			//'value'(PropertyConverter.toXml(s.getValueType(), s.getValue()))
			'value'(PropertyConverter.toXml(s.getValueType(), s.getRealValue()))
			'oldvalue'(PropertyConverter.toXml(s.getValueType(), s.getOldValue()))

			if ( s.getOverride() != null ){
				builder.override(PropertyConverter.toXml(s.getValueType(), s.getOverride()))
			}

			//and spit out customtags
			//<customtag name="poll" oldVal="something else">something</customtag>
			s.getTags().each{ tag ->
				//builder.customtag(name: tag.getTagName(), oldVal:tag.getOldValue() , tag.getValue()  )
				if ( tag.getOverride() != null ){
					builder.customtag(name: tag.getTagName(), oldVal: PropertyConverter.toXml(tag.getTagType(), tag.getOldValue() ), override: PropertyConverter.toXml(tag.getTagType(), tag.getOverride() )  , PropertyConverter.toXml(tag.getTagType(), tag.getRealValue() )  )

				} else {
					builder.customtag(name: tag.getTagName(), oldVal: PropertyConverter.toXml(tag.getTagType(), tag.getOldValue() ) , PropertyConverter.toXml(tag.getTagType(), tag.getValue())  )
				}
			}
		}
	}

	/*
	@return [list of objects created as a result of deserialization]
	*/
	static ArrayList deserialize(ChildLink c, def xml) {
		DLObject owner = c.getOwner()
		ObjectModel objectModel = owner.getObjectModel()
		def children = xml.current.child.collect{ idXml -> objectModel.getDlid(idXml.text()) }
		def oldChildren = xml.old.child.collect{ idXml -> objectModel.getDlid(idXml.text()) }
		c.deserialize(children as Set<Dlid>, oldChildren as Set<Dlid>)
		def created = []
		xml.object.each{ oXml ->
			DLObject n
			def subcreate = []
			(n, subcreate) = objectModel.deserializeObject(oXml)
			c.addChild(n)
			created += n
			created += subcreate
		}
		return created
	}

	static void serialize(ChildLink c, groovy.xml.MarkupBuilder builder, boolean includeAll = true) {
		Set<Dlid> kids
		if( includeAll ) {
			kids = c.getChildIds()
		} else {
			// Filter out the references that won't be exported since those objects won't exist.
			// But not if it has previously been exported since that reference will still be valid.
			kids = [] as Set
			for( DLObject child : c.getChildren() ) {
				if( child.isExportable() || child.hasKey() ) {
					kids.add( child.getDlid() )
				}
			}
		}

		builder.'property'('name': c.getName(), type: "childlink", valuetype:"") {
			'current' {
				kids.each{ dlid -> 'child'(dlid) }
			}
			'old' {
				// No need to filter like we do for children since these are already filtered
				// to only be those that have been exported in the past.
				c.getOldChildIds().each{ dlid -> 'child'(dlid) }
			}
		}
	}

	/*
	@return [list of objects created as a result of deserialization]
	*/
	static ArrayList deserialize(DeploymentLab.Model.Collection c, def xml) {
		DLObject owner = c.getOwner()
		ObjectModel objectModel = owner.getObjectModel()
		def ids = xml.item.collect{ x -> objectModel.getDlid(x.text()) }
		// TODO: is this defaulting correctly?
		c.deserialize(ids, Boolean.parseBoolean(xml.@dirty.text()))
		xml.customtag.each{ tag ->
			if(c.hasTag(tag.@name.text())){
				def currentTag = c.getTag(tag.@name.text())
				c.updateTag(tag.@name.text(), PropertyConverter.fromXml(currentTag.tagType, tag) , PropertyConverter.fromXml(currentTag.tagType, tag.@oldVal));
				if (tag.@override.text() != "" ){
					c.getTag(tag.@name.text()).setOverride(PropertyConverter.fromXml(currentTag.tagType, tag.@override))
				}
			}
		}

		def created = []
		xml.object.each{ oXml ->
			DLObject n
			def subcreate = []
			(n, subcreate) = objectModel.deserializeObject(oXml)
			c.add(n)
			created += n
			created += subcreate
		}
		return created
	}

	/**
	Puts a collection property into XML.  Adjusted to use new java-based classes, not the groovy ones.
	*/
	static void serialize(DeploymentLab.Model.Collection c, groovy.xml.MarkupBuilder builder) {
		builder.'property'('name': c.getName(), type: "collection", valuetype:"", 'dirty': c.isDirty()) {
			c.getItemIds().each{ 'item'(it) }
		}
		c.getTags().each{ tag ->
			if ( tag.getOverride() != null ){
				builder.customtag(name: tag.getTagName(), oldVal: PropertyConverter.toXml(tag.getTagType(), tag.getOldValue() ), override: PropertyConverter.toXml(tag.getTagType(), tag.getOverride() )  , PropertyConverter.toXml(tag.getTagType(), tag.getRealValue() )  )
			} else {
				builder.customtag(name: tag.getTagName(), oldVal: PropertyConverter.toXml(tag.getTagType(), tag.getOldValue() ) , PropertyConverter.toXml(tag.getTagType(), tag.getValue())  )
			}
		}
	}

	static void deserialize(Dockingpoint d, def xml) {
		DLObject owner = d.getOwner()
		ObjectModel objectModel = owner.getObjectModel()
		String vt = xml.@valuetype.text()
		if(vt.equals('xpath')) {
			try {
				JXPathContext ctx = objectModel.getXPathContext()
				DLObject obj = ctx.getValue(xml.text())
				d.setReferrent(obj)
			} catch(Exception e) {
				log.error("Error looking up XPath expression: " + log.getStackTrace(e))
				throw new DeserializationException("Error looking up XPath expression '" + xml.text() + "'")
			}
		} else if(vt.equals('dlid')) {
			d.deserialize(objectModel.getDlid(xml.text()), Boolean.parseBoolean(xml.@dirty.text()))
		} else {
			throw new DeserializationException("Can't determine property value type: " + vt)
		}
		xml.customtag.each{ tag ->
			if(d.hasTag(tag.@name.text())){
				def currentTag = d.getTag(tag.@name.text())
				d.updateTag(tag.@name.text(), PropertyConverter.fromXml(currentTag.tagType, tag) , PropertyConverter.fromXml(currentTag.tagType, tag.@oldVal));
				if (tag.@override.text() != "" ){
					d.getTag(tag.@name.text()).setOverride(PropertyConverter.fromXml(currentTag.tagType, tag.@override))
				}
			}
		}
	}

	/**
	Puts a dockingpoint object property into xml.
	Unused dockingpoints gets a DLID of null, which *should* work - just leaving the tag empty in the dl file.
	*/
	static void serialize(Dockingpoint d, groovy.xml.MarkupBuilder builder) {
		builder.'property'('name': d.getName(), type: "dockingpoint", 'valuetype': 'dlid', 'dirty': d.isDirty() , d.getReferrent()?.getDlid() )
		d.getTags().each{ tag ->
			if ( tag.getOverride() != null ){
				builder.customtag(name: tag.getTagName(), oldVal: PropertyConverter.toXml(tag.getTagType(), tag.getOldValue() ), override: PropertyConverter.toXml(tag.getTagType(), tag.getOverride() )  , PropertyConverter.toXml(tag.getTagType(), tag.getRealValue() )  )
			} else {
				builder.customtag(name: tag.getTagName(), oldVal: PropertyConverter.toXml(tag.getTagType(), tag.getOldValue() ) , PropertyConverter.toXml(tag.getTagType(), tag.getValue())  )
			}
		}
	}

	/**
	 * Load a propertyDock from a DL file
	 * @param d
	 * @param xml
	 */
	static void deserialize(Pier d, def xml) {
		DLObject owner = d.getOwner()
		ObjectModel objectModel = owner.getObjectModel()
		String vt = xml.@valuetype.text()
		if(vt.equals('xpath')) {
			throw new UnsupportedOperationException("Xpath Pier")
		} else if(vt.equals('dlid')) {
			d.deserialize(objectModel.getDlid(xml.text()), xml.@property.text(), Boolean.parseBoolean(xml.@dirty.text()))
		} else {
			throw new DeserializationException("Can't determine property value type: " + vt)
		}
		xml.customtag.each{ tag ->
			if(d.hasTag(tag.@name.text())){
				def currentTag = d.getTag(tag.@name.text())
				d.updateTag(tag.@name.text(), PropertyConverter.fromXml(currentTag.tagType, tag) , PropertyConverter.fromXml(currentTag.tagType, tag.@oldVal));
				if (tag.@override.text() != "" ){
					d.getTag(tag.@name.text()).setOverride(PropertyConverter.fromXml(currentTag.tagType, tag.@override))
				}
			}
		}
	}

	/**
	 Puts a pier object property into xml.
	 Unused pier get a DLID of null, which *should* work - just leaving the tag empty in the dl file.
	 */
	static void serialize(Pier d, groovy.xml.MarkupBuilder builder) {
		builder.'property'('name': d.getName(), type: d.getType(), 'valuetype': 'dlid', 'dirty': d.isDirty(), property: d.getReferentName(), d.getReferentObject()?.getDlid() )
		d.getTags().each{ tag ->
			if ( tag.getOverride() != null ){
				builder.customtag(name: tag.getTagName(), oldVal: PropertyConverter.toXml(tag.getTagType(), tag.getOldValue() ), override: PropertyConverter.toXml(tag.getTagType(), tag.getOverride() )  , PropertyConverter.toXml(tag.getTagType(), tag.getRealValue() )  )
			} else {
				builder.customtag(name: tag.getTagName(), oldVal: PropertyConverter.toXml(tag.getTagType(), tag.getOldValue() ) , PropertyConverter.toXml(tag.getTagType(), tag.getValue())  )
			}
		}
	}

	static void deserialize(Rule r, def xml) {
		xml.customtag.each{ tag ->
			if(r.hasTag(tag.@name.text())){
				def currentTag = r.getTag(tag.@name.text())
				r.updateTag(tag.@name.text(), PropertyConverter.fromXml(currentTag.tagType, tag) , PropertyConverter.fromXml(currentTag.tagType, tag.@oldVal));
				if (tag.@override.text() != "" ){
					r.getTag(tag.@name.text()).setOverride(PropertyConverter.fromXml(currentTag.tagType, tag.@override))
				}
			}

		}
	}

	static void serialize(Rule r, groovy.xml.MarkupBuilder builder) {

		//special case: if a src prop is a dockingpoint, and null, don't bother serializing at all

		def hasNulls = false
		def o = r.getOwner()
		r.bindings.each{bvar, plink ->
			if ( o.getObjectProperty(plink) instanceof Dockingpoint && o.getObjectProperty(plink).getReferrent() == null ){
				hasNulls = true
			}
		}
		if ( hasNulls ){
			return
		}

		//builder.'property'('name': r.getName(), type: "rule", ruleTypeName: r.getServerTypeName(), valuetype:"" )
		builder.'property'('name': r.getName(), type: "rule", valuetype:"" ){
			ruleTypeName( r.getServerTypeName() )

			r.bindings.each{ rv, prop ->
				builder.binding( rulevar: rv, property: prop )
			}

			r.getTags().each{ tag ->
				if ( tag.getOverride() != null ){
					builder.customtag(name: tag.getTagName(), oldVal: PropertyConverter.toXml(tag.getTagType(), tag.getOldValue() ), override: PropertyConverter.toXml(tag.getTagType(), tag.getOverride() )  , PropertyConverter.toXml(tag.getTagType(), tag.getRealValue() )  )
				} else {
					builder.customtag(name: tag.getTagName(), oldVal: PropertyConverter.toXml(tag.getTagType(), tag.getOldValue() ) , PropertyConverter.toXml(tag.getTagType(), tag.getValue())  )
				}
			}
		}
	}

	static void deserialize(GroovyRule r, def xml) {
		DLObject owner = r.getOwner()
		ObjectModel objectModel = owner.getObjectModel()

		String to = xml.text()
		if(!to.equals("")) {
			r.setRuleTo(to)
		}
		xml.customtag.each{ tag ->
			if(r.hasTag(tag.@name.text())){
				//println "I'M MAKING TAG '$tag' FOR $r; tag type ${tag.@tagType.text()}"
				def currentTag = r.getTag(tag.@name.text())
				r.updateTag(tag.@name.text(), PropertyConverter.fromXml(currentTag.tagType, tag) , PropertyConverter.fromXml(currentTag.tagType, tag.@oldVal));
				if (tag.@override.text() != "" ){
					r.getTag(tag.@name.text()).setOverride(PropertyConverter.fromXml(currentTag.tagType, tag.@override))
				}
			}

		}
	}

	static void serialize(GroovyRule r, groovy.xml.MarkupBuilder builder) {
		//builder.'property'('name': r.getName(), type: "groovyrule", ruleTypeName: r.getServerTypeName(), r.getRuleTo(), valuetype:"")
		builder.'property'('name': r.getName(), type: "groovyrule", valuetype:""){
			ruleTypeName( r.getServerTypeName() )
			//bindings[binding.@rulevar.text()] = binding.@property.text()
			r.bindings.each{ rv, prop ->
				builder.binding( rulevar: rv, property: prop )
			}


			r.getTags().each{ tag ->
				if ( tag.getOverride() != null ){
					builder.customtag(name: tag.getTagName(), oldVal: PropertyConverter.toXml(tag.getTagType(), tag.getOldValue() ), override: PropertyConverter.toXml(tag.getTagType(), tag.getOverride() )  , PropertyConverter.toXml(tag.getTagType(), tag.getRealValue() )  )
				} else {
					builder.customtag(name: tag.getTagName(), oldVal: PropertyConverter.toXml(tag.getTagType(), tag.getOldValue() ) , PropertyConverter.toXml(tag.getTagType(), tag.getValue())  )
				}
			}
		}
	}
}

