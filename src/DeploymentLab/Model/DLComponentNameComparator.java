package DeploymentLab.Model;

import com.google.gag.annotation.team.Blame;

import java.util.Comparator;

/**
 * Comparator to sort DLComponents by name
 *
 * @author Gabriel Helman
 * @see DLComponent
 * @since Jupiter 2
 *        Date: 6/14/12
 *        Time: 12:06 PM
 */
@Blame(person = "James Gosling")
public class DLComponentNameComparator implements Comparator<DLComponent> {
	@Override
	public int compare(DLComponent o1, DLComponent o2) {
		return o1.getName().compareTo(o2.getName());
	}
}
