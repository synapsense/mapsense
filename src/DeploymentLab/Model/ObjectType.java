package DeploymentLab.Model;

/**
 * @author Ken Scoggins
 * @since Aireo
 */
public class ObjectType {
	public static final String DRAWING  = "dc";
	public static final String DRAWING_ICON = "drawing_icon";
	public static final String RACK = "rack";
	public static final String WSN_NETWORK = "wsnnetwork";
	public static final String WSN_NODE = "wsnnode";
	public static final String WSN_GATEWAY = "wsngateway";
	public static final String ROOM = "room";
	public static final String CRAC_CRAH = "crac";
	public static final String CONTROLPROTO_MODBUS = "controlproto_modbus";
	public static final String CONTROLPROTO_BACNET = "controlproto_bacnet";
	public static final String CONTROLCFG_LIEBERT = "controlcfg_liebert_ahu";
}
