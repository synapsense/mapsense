
package DeploymentLab.Model;

import DeploymentLab.Exceptions.ModelCatastrophe;
import DeploymentLab.channellogger.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Dlid {
	private static final Logger log = Logger.getLogger(Dlid.class.getName());

	private static int lastID = 0;

	private final int id;

	static  void resetFactory(int id) {
		lastID = id;
	}

	static  int getFactoryState() {
		return lastID;
	}

	static public Dlid getDlid() {
		return new Dlid();
        }

	static public Dlid getDlid(String _sid) {
		if(_sid == null || _sid.length() == 0){
			return null;
		}
		try {
			String text = _sid.substring(5);
			int _id = Integer.parseInt(text);
			if(_id <= 0)
				return new Dlid();
			else
				return new Dlid(_id);
		} catch(java.lang.NumberFormatException e) {
			log.error("NumberFormatException creating DLID from '" + _sid + "'.  This will likely create problems down the road.", "default");
			//return null;
			throw new ModelCatastrophe("NumberFormatException creating DLID from '" + _sid + "'.  This will likely create problems down the road.", e);
		} catch(java.lang.StringIndexOutOfBoundsException e) {
			log.error("StringIndexOutOfBoundsException creating DLID from '" + _sid + "'.  This will likely create problems down the road.", "default");
			//return null;
			throw new ModelCatastrophe("StringIndexOutOfBoundsException creating DLID from '" + _sid + "'.  This will likely create problems down the road.", e);
		}
	}

	static public boolean isDlid(String dlid){
		return Pattern.compile("DLID:([0-9]+)").matcher(dlid).matches();
	}

	private Dlid() {
		id = ++ lastID;
	}

	private Dlid(int _id) {
		id = _id;
	}

	@Override
	public boolean equals(Object o) {
		Dlid _id = (Dlid)o;
		if(_id == null)
			return false;
		return id == _id.id;
	}

	public String toString() {
		return "DLID:" + id;
        }

	@Override
	public int hashCode() {
		return id;
	}
}

