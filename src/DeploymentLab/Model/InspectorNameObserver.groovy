package DeploymentLab.Model

import DeploymentLab.channellogger.Logger

/**
 * Tracks inspectors and syncs their name with their parent, as appropriate.
 * Based on the similar logic for proxies in DLProxy, and the pattern from the testing ModelObserver & BackgroundImageObserver.
 * @author Gabriel Helman
 * @since Io
 * Date: 11/8/12
 * Time: 12:47 PM
 * @see DLProxy
 * @see DeploymentLab.Image.BackgroundImageObserver
 */
class InspectorNameObserver implements ModelChangeListener {
	private static final Logger log = Logger.getLogger(InspectorNameObserver.class.getName())
	private static final String baseName = "Inspector"
	private boolean kafka = false

	/**
	 * Map of inspector : host
	 */
	private Map<DLComponent, DLComponent> suspects
	//private Map<DLComponent,DLComponent> hostToInspectors

	InspectorNameObserver() {
		suspects = new HashMap<DLComponent, DLComponent>()
	}

	@Override
	void componentAdded(DLComponent component) {
		if (component.hasRole("inspector")) {
			suspects.put(component, null)

			//on a load, we need to look for associations here
			if (component.getAllAssociatedComponents().size() == 1) {
				suspects.put(component, component.getAllAssociatedComponents().first())
			}

		}
	}

	@Override
	void componentRemoved(DLComponent component) {
		if (component.hasRole("inspector")) {
			suspects.remove(component)
		}
	}

	@Override
	void childAdded(DLComponent parent, DLComponent child) {}

	@Override
	void childRemoved(DLComponent parent, DLComponent child) {}

	@Override
	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
		if (suspects.containsKey(consumer)) {
			suspects[consumer] = producer
			producer.addPropertyChangeListener(this, this.&componentPropertyChanged)
		}

	}

	@Override
	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
		if (suspects.containsKey(consumer)) {
			suspects[consumer] = null
			producer.removePropertyChangeListener(this)
		}
	}

	@Override
	void modelMetamorphosisStarting(MetamorphosisEvent event) {
		kafka = true
	}

	@Override
	void modelMetamorphosisFinished(MetamorphosisEvent event) {
		kafka = false
	}

	/**
	 * Called when the host component of a coin has a property change event
	 * @param who the host component of a COIN
	 * @param prop the property that has changed
	 * @param oldVal
	 * @param newVal
	 */
	void componentPropertyChanged(DLComponent who, String prop, oldVal, newVal) {
		if (kafka) {return}

		if (!suspects.values().contains(who)) {return}

		if (prop.equals("name")) {
			DLComponent inspector
			for (def e : suspects.entrySet()) {
				if (e.value == who) {
					inspector = e.key
				}
			}
			String oldDefaultName = this.generateDefaultName(oldVal)
			String inspectorName = inspector.getPropertyStringValue(prop)
			log.debug("name changed on $who, old default inspector name is '$oldDefaultName', inspector's current name is '$inspectorName'")
			if (oldDefaultName.equals(inspectorName)) {
				inspector.setPropertyValue(prop, this.generateDefaultName(newVal))
			}
		}
	}

	protected String generateDefaultName(String hostName) {
		return "$hostName - $baseName"
	}

}
