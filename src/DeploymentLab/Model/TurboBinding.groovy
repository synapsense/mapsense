package DeploymentLab.Model

import DeploymentLab.channellogger.Logger
import groovy.xml.MarkupBuilder

import org.mvel2.compiler.CompiledExpression
import org.mvel2.MVEL

/*
<varbinding vars="name">
<property>$node/properties[name="name"]</property>
<value>'Node ' + name</value>
</varbinding>

<turbobinding vars="name">
self.getObject("node").getObjectSetting("name").setValue("Node " + name)
</turbobinding>

*/


/**
 *
 * A bigger, scarier varbinding.
 *
 * Essentially, an MVEL procedure with no "return value" like a varbinding, and full access to the internal api via "self".
 *
 * @author Gabriel Helman
 * @since Io
 * Date: 7/19/12
 * Time: 5:40 PM
 */
class TurboBinding implements DLBinding, ComponentTrait {
	private static final Logger log = Logger.getLogger(TurboBinding.class.getName())
	private static HashMap<String, CompiledExpression> expressionRegistry = new HashMap<String, CompiledExpression>()

	private dependentVars = []
	private String valueExpr
	private Serializable compiledExpr
	protected DLComponent belongsTo
	String parcel

	TurboBinding(Object bXml, DLComponent _owner) {
		dependentVars = bXml.@vars.text().split(',')
		parcel = bXml.@parcel.text()
		valueExpr = bXml.text()
		belongsTo = _owner
		if(!expressionRegistry.containsKey(valueExpr)){
			expressionRegistry[valueExpr] = MVEL.compileExpression(valueExpr)
		}
		compiledExpr = expressionRegistry[valueExpr]
	}

	@Override
	String getTraitId() {
		return null
	}

	@Override
	String getTraitDisplayName() {
		return null
	}

	@Override
	DLComponent getOwner() {
		return belongsTo
	}

	def listVariables() {
		return dependentVars
	}

	@Override
	void serialize(MarkupBuilder builder) {
		builder.turbobinding(vars: dependentVars.join(','), parcel: parcel, valueExpr) {
		}
	}

	@Override
	void serializeAsPrototype(MarkupBuilder builder) {
		this.serialize(builder)
	}

	void eval(Object vars) {
		//log.trace("Evaluating expression '$valueExpr' on vars: $vars")
		def nulls = dependentVars.findAll{vars[it] == null}
		if(nulls.size() > 0) {
			log.info("Skipping evaluation of expression '$valueExpr' because some variables $nulls were null")
			return
		}
		vars["self"] = belongsTo
		vars["log"] = log
		def result
		try {
			MVEL.executeExpression(compiledExpr, vars)
		} catch(Exception e) {
			log.error(log.getStackTrace(e))
			log.error("Couldn't evaluate expression '$valueExpr'.")
			log.error("vars: $vars")
			throw e
		}
	}

	@Override
	public String toString() {
		return "TurboBinding(vars:$dependentVars,owner:$belongsTo,parcel:$parcel,expr:$valueExpr)"
	}
}
