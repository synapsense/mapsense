package DeploymentLab.Model

import DeploymentLab.CentralCatalogue
import DeploymentLab.channellogger.Logger

/**
 * Takes care of some magic with control manual expressions that are a pain to deal with in the XML definition.
 *
 * @author Ken Scoggins
 * @since Aireo
 */
public class ManualExpressionController implements ModelChangeListener {
	private static final Logger log = Logger.getLogger( ManualExpressionController.class.getName() )

	public ManualExpressionController() {
	}

	/**
	 * Listens for potential control devices.
	 *
	 * @param The new Component.
	 */
	@Override
	void componentAdded(DLComponent component) {
		if( component.hasManagedObjectOfType( ObjectType.CRAC_CRAH ) ) {
			component.addPropertyChangeListener( this, this.&propertyChanged )
		}
	}

	/**
	 * Detaches itself as a listener if needed.
	 *
	 * @param component the DLComponent just removed.
	 */
	@Override
	void componentRemoved(DLComponent component) {
		if( component.hasManagedObjectOfType( ObjectType.CRAC_CRAH ) ) {
			component.removePropertyChangeListener( this )
		}
	}

	@Override
	void childAdded(DLComponent parent, DLComponent child) {}

	@Override
	void childRemoved(DLComponent parent, DLComponent child) {}

	@Override
	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	@Override
	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	@Override
	public void modelMetamorphosisStarting( MetamorphosisEvent event ) {}

	@Override
	public void modelMetamorphosisFinished( MetamorphosisEvent event ) {}


	/**
	 * Acts as the PropertyChanged event closure for controlled devices with AoEs.
	 * @param who the control device DLComponent which has changed
	 * @param propertyName the name of the changing property
	 * @param oldVal the old value of the property
	 * @param newVal the new value of the property
	 */
	public void propertyChanged(DLComponent who, def propertyName, def oldVal, def newVal) {
		//we don't want to get in the way if replace config is running right now
		if( CentralCatalogue.getOpenProject().getComponentModel().isMetamorphosizing() ) { return }

		if( who.hasRole( ComponentRole.MANUAL_EXPRESSION ) && ( oldVal != newVal ) ) {
			// If the Device address changes, invalidate the test results for expressions.
			if( propertyName.endsWith("_ip") || propertyName.endsWith("_network") ||
			    propertyName.endsWith("_devid") || propertyName.endsWith("_device") ) {

				for( String pname : who.listProperties( who.getComponentProperty( propertyName ).getParcel() ) ) {
					if( pname.endsWith("Valid") && who.getPropertyStringValue( pname.replace("Valid", "") ) ) {
						who.setPropertyValue( pname, 0 )
					}
				}
			}
		}
	}
}
