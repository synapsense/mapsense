
package DeploymentLab.Model;

import com.synapsense.dto.RuleType;

import java.io.IOException;
import java.util.Map;

/**
 * Represents an object property that is calculated by a pre compiled java-based CRE Rule.
 */
public class Rule extends Property implements CREMember {
	private String valuetype;
	private String ruleClass;
	private String ruleSchedule;
	private boolean _historic;
	private Map<String, String> varBindings;
	private String serverInstanceName;

	Rule(DLObject _belongsTo, String n, String vt, String rc, String rs, boolean h, Map<String, String> vb) {
		super(_belongsTo, n);
		valuetype = vt;
		ruleClass = rc;
		ruleSchedule = rs;
		_historic = h;
		varBindings = vb;
	}

	public String getType() {
		return "rule";
	}

	public String getValueType() {
		return valuetype;
	}

	public String getRuleClass() {
		return ruleClass;
	}

	/**
	 * Gets the schedule of this rule.
	 * @return A string containing the CRON configuration of this rule's schedule.
	 */
	public String getSchedule() {
		return ruleSchedule;
	}

	/**
	 * Checks to see if this rule is scheduled.
	 * @return true if this rule has a schedule.
	 */
	public boolean hasSchedule(){
		return ( ruleSchedule.length() > 0 );
	}

	public Map<String, String> getBindings() {
		return varBindings;
	}

	public boolean getHistoric() {
		return _historic;
	}

	public String toString() {
		return "Rule(name=" + name + ", type='" + valuetype + "', class='" + ruleClass + "', schedule='" + ruleSchedule + "', historic='" + _historic + "', variables=" + varBindings + ")";
	}

	/**
	 * Creates an ES RuleType object that represents this rule property.
	 * @return a RuleType, ready for exporting to the server.
	 */
	public RuleType createRuleType() {
		//System.out.println("making an rt: " +this.getSchedule() );
		RuleType result = null;


			if ( this.hasSchedule() ){
				//System.out.println("constructing a schedule rule " + this.getSchedule() );
				result = new RuleType( ruleClass, null, "Java", true, this.getSchedule() );
			} else {
				//System.out.println("constructing a no schedule rule" );
				result = new RuleType( ruleClass, null, "Java", true );
			}

		return result;
	}

	/**
	 * Gets the unique rule type name to be used on the server for this rule.
	 * Follows the pattern (object type name)_(this property name).
	 * For example, a groovyrule property named "lastValue" on an object type of "unlikelyOperation" would
	 * result in a server type name of "unlikelyOperation_lastValue"
	 * @return a String holding the unique server rule type name.
	 */
	public String getServerTypeName(){
		return (ruleClass);
	}

	/**
	 * Gets the unique name that this rule instance will use on the server.
	 * Generally. this follows the pattern (TO)(property name), so a rule property named lastValue on an object with a TO of 13:SOMETHING
	 * would have a server instance name of 13lastValue.
	 * This should not be populated until the rule is exported for the first time - the presence of a name here is used
	 * as an indicator that the rule has been created on the server.
	 * @return a String containing this rule instance's unique name.
	 */
	public String getServerInstanceName() {
		return serverInstanceName;
	}

	/**
	 * Sets the unique name that this rule instance will use on the server.
	 * @param serverInstanceName the new instance name to use for this rule.
	 */
	public void setServerInstanceName(String serverInstanceName) {
		this.serverInstanceName = serverInstanceName;
	}

	/**
	 * Gets the name of the pre-compiled java class on the server that runs this rule.
	 * @return a String holding the name of the java class.
	 */
	public String getSourceName(){
		return ( ruleClass );
	}

}


