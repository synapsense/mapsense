package DeploymentLab.Model;

/**
 * @author Ken Scoggins
 * @since Aireo
 */
public class RoomControlType {
	public static final String _PROPERTY = "controlType";

	public static final String NONE = "none";
	public static final String PRESSURE = "pressure";
	public static final String PRESSURE_TEMP = "pressuretemperature";
	public static final String REMOTE = "remote";
}
