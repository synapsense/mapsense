package DeploymentLab.Model;


/**
 * The interface that all component traits implement.
 * All "things" that can be part of a Component are a Trait: Properties, Consumers, Producers, Varbindings, etc.
 *
 * Based on the ArtisanComponentTrait abstract class in the original Artisan proto.
 *
 * There's an argument to be made that DLObjects should also be Traits.
 *
 * Also, we'll  probably need something for both macids and dockings
 *
 * @author Gabriel Helman
 * @since Jupiter Io
 * Date: 7/30/12
 * Time: 2:36 PM
 */
public interface ComponentTrait {
	/**
	 * The Trait ID is the "internal name" of this trait.  Some classes call this "name", others "id".
	 * Required to be unique across all members of that trait type in a given component, but not required to be
	 * unique across all traits.  (although that's a pretty good idea.
	 * @return a String holding the id.
	 */
	public String getTraitId();

	/**
	 * The user-facing display name for this trait, as apropos.
	 * @return a String holding the name.
	 */
	public String getTraitDisplayName();

	/**
	 * The DLComponent that this trait is contained by.
	 * @return The owning DLComponent.
	 */
	public DLComponent getOwner();

	//public void setOwner();

	/**
	 * Serialize this trait to the provided markup builder.
	 * Note that there is no *deserialize* method; the constructor is assumed to handle that.
	 * @param builder a MarkupBuilder to blow XML into.
	 */
	public void serialize(groovy.xml.MarkupBuilder builder);


	/**
	 * Serialize the trait in the prototype/definition format.
	 * In many cases, the definition xml is *slightly* different than the DL save file format.
	 * With this, we can reverse engineer a prototype from an instance.
	 * @param builder a MarkupBuilder to blow XML into.
	 */
	public void serializeAsPrototype(groovy.xml.MarkupBuilder builder);

	//getTraitType?

	/*
	//from the original:

	//All Traits must provide a factory methods, but the logic of said method is left as an exercise for the individual trait.
	ArtisanComponentTrait getNewTrait()

	//All Traits needs this function, but only TrailMembers should return a non-null value.
	public TrailMember getTrailRoot() {
		return ( null )
	}

see also the original TrailMember interface for objects

	*/

	/**
	 * If the member of a parcel, return the name of the parcel
	 * @return name  of this trait's parcel, if any
	 */
	public String getParcel();

	/**
	 * If the member of a parcel, return the name of the parcel
	 * @return name  of this trait's parcel, if any
	 */
	public void setParcel( String parcel );
}
