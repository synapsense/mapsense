package DeploymentLab.Model

import DeploymentLab.channellogger.Logger
import groovy.xml.MarkupBuilder

/**
 * Represents a docking.  Used to regenerate prototypes and not much else since dockings are resolved once at runtime.
 * (But!  This leaves us a hook to fix that.)
 *
 * @author Gabriel Helman
 * @since Io
 * Date: 1/3/13
 * Time: 1:25 PM
 */
class Docking implements ComponentTrait {
	private static final Logger log = Logger.getLogger(Docking.class.getName())
	private DLComponent belongsTo
	String parcel
	String from
	String to
	def context
	private def target = null
	private def source = null

	public Docking(DLComponent owner, String from, String to, String parcel, def ctx){
		//log.trace("Made a new Docking! $owner $from $to")
		belongsTo = owner
		this.from = from
		this.to = to
		this.parcel = parcel
		this.context = ctx
	}

	@Override
	String getTraitId() {
		return null
	}

	@Override
	String getTraitDisplayName() {
		return null
	}

	@Override
	DLComponent getOwner() {
		return this.owner
	}

	//<docking from='$rack/properties[name="cBot"]' to='$coldBottomNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]'/>
	@Override
	void serialize(MarkupBuilder builder) {
		builder.docking(from: this.from, to: this.to, parcel: parcel ) {}
	}

	@Override
	void serializeAsPrototype(MarkupBuilder builder) {
		builder.docking(from: this.from, to: this.to, parcel: parcel )
	}

	def resolveFrom(){
		if (source == null){
			source = context.getValue(from)
		}
		return source
	}

	def resolveTo(){
		if (target == null){
			target = context.getValue(to)
		}
		return target
	}


	public void set(){
		this.resolveFrom().setReferrent(this.resolveTo())
	}

	public void clear(){
		this.resolveFrom().setReferrent(null)
	}

	@Override
	public String toString() {
		return "Docking{" +
				", parcel='" + parcel + '\'' +
				", from='" + from + '\'' +
				", to='" + to + '\'' +
				'}';
	}
}
