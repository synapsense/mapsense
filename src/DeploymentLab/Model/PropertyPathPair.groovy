package DeploymentLab.Model

/**
 * Stores a JXPath and the resolved *thing* for that path.
 *
 * Value should can be either a DLObject or any Object Property.
 *
 * @author Gabriel Helman
 * @since Io
 * Date: 10/16/12
 * Time: 3:21 PM
 */
public class PropertyPathPair {

	//properties:
	String path
	def value
	String payload
	Map displaySettings
	List subOptions

	PropertyPathPair(String path) {
		this(path,null)
	}

	PropertyPathPair(String path, def value) {
		this.path = path
		this.value = value
		this.displaySettings = new HashMap(4)
		this.subOptions = []
	}


// oughta add something like this:
	def resolveValue(def context){
		this.value = context.getValue(path)
		return this.value
	}


	@Override
	public String toString() {
		return "PropertyPathPair{" +
				"path='" + path + '\'' +
				", value=" + value +
				", payload='" + payload + '\'' +
				", displaySettings=" + displaySettings +
				", subOptions=" + subOptions +
				'}';
	}
}
