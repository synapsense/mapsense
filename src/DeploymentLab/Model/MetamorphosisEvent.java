package DeploymentLab.Model;


/**
 * Holds the information related to model metamorphosis events that are triggered when a model is going through a
 * significant change.
 *
 * @author Ken Scoggins
 * @since Jupiter
 */
public class MetamorphosisEvent {

    private ModelState mode;

    public MetamorphosisEvent( ModelState mode ) {
        this.mode = mode;
    }

    public ModelState getMode() {
        return mode;
    }
}
