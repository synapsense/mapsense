package DeploymentLab.Model

import java.awt.Color

import java.text.SimpleDateFormat

import javax.imageio.ImageIO

import org.apache.commons.codec.binary.Base64
import DeploymentLab.Exceptions.*
import com.synapsense.dto.BinaryData
import com.synapsense.dto.TO
import com.synapsense.service.impl.dao.to.BinaryDataImpl
import java.awt.image.BufferedImage
import DeploymentLab.channellogger.*
import javax.imageio.ImageReader
import javax.imageio.stream.FileCacheImageInputStream
import javax.imageio.ImageReadParam
import com.synapsense.dto.TOFactory
import DeploymentLab.Image.DLImage

public class PropertyConverter {

	//static converters = [:]
	static Map<String, PropertyValueConverter> converters = new HashMap<String, PropertyValueConverter>();

	static {
		converters['java.lang.String'] = new StringValueConverter()
		converters['java.lang.Integer'] = new IntegerValueConverter()
		converters['java.lang.Short'] = new ShortValueConverter()
		converters['java.lang.Float'] = new FloatValueConverter()
		converters['java.lang.Double'] = new DoubleValueConverter()
		converters['java.lang.Long'] = new LongValueConverter()
		converters['java.lang.Boolean'] = new BooleanValueConverter()
		converters['java.awt.Image'] = new ImageValueConverter()
		converters['java.awt.Color'] = new ColorValueConverter()
		converters['java.util.Date'] = new DateValueConverter()
		converters['DeploymentLab.ChartContents'] = new ChartContentsConverter()
		converters['DeploymentLab.MultiObjectTable'] = new ChartContentsConverter()
		converters['DeploymentLab.MultiLineContents'] = new MultiLineContentsConverter()
		converters['com.synapsense.dto.BinaryData'] = new BinaryDataConverter()
		converters['DeploymentLab.ChartContentsCRAH'] = new ChartContentsCRAHConverter()
		converters['DeploymentLab.ChartContentsBTU'] = new ChartContentsCRAHConverter()
		converters['DeploymentLab.ChartContentsCFM'] = new ChartContentsCRAHConverter()
		converters['com.synapsense.dto.TO'] = new TOObjectConverter()
		converters['DeploymentLab.ConfigModbus'] = new ConfigModbusConverter()
		converters['DeploymentLab.ConfigModbusRead'] = new ConfigModbusConverter()
		converters['DeploymentLab.ConfigModbusWrite'] = new ConfigModbusConverter()
		converters['DeploymentLab.ConfigBacnet'] = new ConfigBacnetConverter()
		converters['DeploymentLab.ConfigBacnetRead'] = new ConfigBacnetConverter()
		converters['DeploymentLab.ConfigBacnetWrite'] = new ConfigBacnetConverter()
		converters['DeploymentLab.DatabaseTableContents'] = new DatabaseTableContentsConverter()
		converters['DeploymentLab.BacnetObject'] = new BacnetObjectConverter()
		converters['DeploymentLab.LookupTable'] = new LookupTableConverter()
        converters['DeploymentLab.Image'] = new DLImageConverter()
		converters['DeploymentLab.ValidatedString'] = new ValidatedStringConverter()
		converters['DeploymentLab.ValidatedMultiLineContents'] = new ValidatedMultiLineContentsConverter()

		//converters['DeploymentLab.ChartContentsBTU'] = new ChartContentsBTUConverter()

	}

	static def fromXml(String type, def xml) {
		if (!converters.containsKey(type))
			throw new Exception("No converter found for '$type'!")
		return converters[type].fromXml(xml)
	}

	static String toXml(String type, def value) {
		if (!converters.containsKey(type))
			throw new Exception("No converter found for '$type'!")
		return converters[type].toXml(value)
	}

	static def fromString(String type, String value) {
		if (!converters.containsKey(type))
			throw new Exception("No converter found for '$type'!")
		return converters[type].fromString(value)
	}

	static def fromObject(String type, def value) {
		if (!converters.containsKey(type))
			throw new Exception("No converter found for '$type'!")
		return converters[type].fromObject(value)
	}


	static String toSimpleName(String type) {
		if (!converters.containsKey(type))
			throw new Exception("No converter found for '$type'!")
		return converters[type].toSimpleName()
	}

	static boolean isLocalizable(String type) {
		if (!converters.containsKey(type))
			throw new Exception("No converter found for '$type'!")
		return converters[type].isLocalizable()
	}

	/**
	 * Complex types require a "fancy" editor other than just a text box
	 * @param type a String holding the type to check
	 * @return true if this type is complex
	 */
	static boolean isComplex(String type) {
		if (!converters.containsKey(type))
			throw new Exception("No converter found for '$type'!")
		return converters[type].isComplex()
	}


}

public interface PropertyValueConverter {
	def fromXml(def xml)

	String toXml(def value)

	def fromString(String value)

	def fromObject(def value)

	String toSimpleName()

	boolean isLocalizable()

	boolean isComplex()
}

public class StringValueConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return xml.text().trim()
	}

	String toXml(def value) {
		if (value == null)
			return null
		return value.toString()
	}

	def fromString(String value) {
		return value.trim()
	}

	def fromObject(def value) {
		if (value == null) { return null }
		return value.toString().trim()
	}

	String toSimpleName() {
		return "Text"
	}

	boolean isLocalizable() {
		return false
	}

	boolean isComplex() {
		return false
	}

}

public class IntegerValueConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return fromString(xml.text())
	}

	String toXml(def value) {
		if (value == null)
			return ''
		return "$value"
	}

	def fromString(String value) {
		if (value == '')
			return null
		return Double.parseDouble(value).intValue()
	}

	def fromObject(def value) {
		if (value == '' || value == null) { return null; }

		if (value instanceof Color) {
			return value.getRGB()
		}

		return Double.parseDouble("$value").intValue()
	}

	String toSimpleName() {
		return "Integer"
	}

	boolean isLocalizable() {
		return true
	}

	boolean isComplex() {
		return false
	}

}

public class ShortValueConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return fromString(xml.text())
	}

	String toXml(def value) {
		if (value == null)
			return ''
		return "$value"
	}

	def fromString(String value) {
		if (value == '')
			return null
		return Double.parseDouble(value).shortValue()
	}

	def fromObject(def value) {
		if (value == '' || value == null) { return null; }
		return Double.parseDouble("$value").shortValue()
	}

	String toSimpleName() {
		return "Short Integer"
	}

	boolean isLocalizable() {
		return true
	}

	boolean isComplex() {
		return false
	}

}

public class FloatValueConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return fromString(xml.text())
	}

	String toXml(def value) {
		if (value == null)
			return ''
		return "$value"
	}

	def fromString(String value) {
		if (value == '')
			return null
		return Float.parseFloat(value)
	}

	def fromObject(def value) {
		if (value == '' || value == null) { return null; }
		return Float.parseFloat("$value")
	}

	String toSimpleName() {
		return "Number"
	}

	boolean isLocalizable() {
		return true
	}

	boolean isComplex() {
		return false
	}

}

public class DoubleValueConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return fromString(xml.text())
	}

	String toXml(def value) {
		if (value == null)
			return ''
		return "$value"
	}

	def fromString(String value) {
		if (value == '')
			return null
		return Double.parseDouble(value)
	}

	def fromObject(def value) {
		if (value == '' || value == null) { return null; }
		return Double.parseDouble("$value")
	}

	String toSimpleName() {
		return "Number"
	}

	boolean isLocalizable() {
		return true
	}

	boolean isComplex() {
		return false
	}

}

public class LongValueConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return fromString(xml.text())
	}

	String toXml(def value) {
		if (value == null)
			return ''
		return "$value"
	}

	def fromString(String value) {
		if (value == '')
			return null
		return Long.parseLong(value)
	}

	def fromObject(def value) {
		if (value == '' || value == null) { return null; }
		//return Long.parseLong("$value")

		if (value instanceof Long) {
			return value
		}

		Long result = null
		try {
			result = Long.parseLong("$value")
		} catch (NumberFormatException nfe) {
			//probably this means we got a string with a decimal point in it
			result = (Long) value.toString().toDouble()
		}
		return result
	}

	String toSimpleName() {
		return "Long Integer"
	}

	boolean isLocalizable() {
		return true
	}

	boolean isComplex() {
		return false
	}

}

public class ImageValueConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		if (xml.text() == "") { return null}

		String value = xml.text()
		return this.fromString(value);

/*
		def temp = ImageIO.read(
			new ByteArrayInputStream(
				Base64.decodeBase64(xml.text().getBytes())
			)
		)

        if (temp == null){ return null}
		//turn temp into .type_int_arbg
		BufferedImage result = ARGBConverter.convert(temp)
		return result;
*/
	}

	String toXml(def value) {
		if (value == null)
			return ''
		ByteArrayOutputStream output = new ByteArrayOutputStream()
		ImageIO.write(value, "png", output)
		String imageXml = new String(Base64.encodeBase64(output.toByteArray()))
		output.reset() // empty out buffer
		return imageXml
	}

	def fromString(String value) {
		//throw new ConversionNotSupportedException(value, 'java.awt.Image')
		//assume the contents of the XML tag
		//load twice; the first time just to calculate the width & height

		if (value == "") { return null}

		def temp2 = ImageIO.read(
				new ByteArrayInputStream(
						Base64.decodeBase64(value.getBytes())
				)
		)

		def w = temp2.getWidth()
		def h = temp2.getHeight()
		temp2 = null

		def input = new FileCacheImageInputStream(new ByteArrayInputStream(Base64.decodeBase64(value.getBytes())), null)
		ImageReader reader = ImageIO.getImageReaders(input).next()
		reader.setInput(input, true)
		ImageReadParam p = reader.getDefaultReadParam()
		BufferedImage target = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		p.setDestination(target)

		def temp = reader.read(0, p)

		return temp;

/*
        if (temp == null){ return null}
		//turn temp into .type_int_arbg
		BufferedImage result = ARGBConverter.convert(temp)
		return result;
*/

	}

	def fromObject(def value) {
		if (value.class.getName() == 'java.awt.Image' || value.class.getName() == 'java.awt.image.BufferedImage') {
			return value
		}
		throw new ConversionNotSupportedException(value, 'java.awt.Image')
	}

	String toSimpleName() {
		return "Image"
	}

	boolean isLocalizable() {
		return false
	}

	boolean isComplex() {
		return true
	}

}


public class DLImageConverter extends ImageValueConverter {
    def fromXml(def xml) {
        if (xml.text() == "") { return null }
        return this.fromString( xml.text() )
    }

    def fromString(String value) {
        def image = super.fromString( value )
        if( image == null ) {
            return null
        }
        return new DLImage( image )
    }

    def fromObject(def value) {
        if ((value == null) || (value.class.getName() == 'DeploymentLab.Image.DLImage')) {
            return value
        }
        throw new ConversionNotSupportedException(value, 'DeploymentLab.Image.DLImage')
    }
}


public class ColorValueConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		//def c = xml.text().split(',')
		//println c
		//return new Color(Float.parseFloat(c[0]), Float.parseFloat(c[1]), Float.parseFloat(c[2]), Float.parseFloat(c[3]))
		//return new Color(Integer.parseInt(c[0]), Integer.parseInt(c[1]), Integer.parseInt(c[2]), Integer.parseInt(c[3]))

		//return new Color(Integer.parseInt( xml.text().toString(), 16 ))
		return new Color(Integer.parseInt(xml.text().toString()))
	}

	String toXml(def value) {
		if (value == null)
			return ''
		//return value.getComponents(null).collect{"$it"}.join(',')
		//return Integer.toHexString( value.getRGB() )
		return value.getRGB().toString()
	}

	def fromString(String value) {
		//throw new ConversionNotSupportedException(value, 'java.awt.Color')
		return new Color(Integer.parseInt(value.toString()))
	}

	def fromObject(def value) {
		if (value.class.getName() == 'java.awt.Color')
			return value
		//throw new ConversionNotSupportedException(value, 'java.awt.Color')
		return new Color(Integer.parseInt(value.toString()))
	}

	String toSimpleName() {
		return "Color"
	}

	boolean isLocalizable() {
		return false
	}

	boolean isComplex() {
		return true
	}

}

public class DateValueConverter implements PropertyValueConverter {
	SimpleDateFormat sdf

	DateValueConverter() {
		sdf = new SimpleDateFormat("M/d/yy HH:mm")
	}

	def fromXml(def xml) {
		return fromString(xml.text())
	}

	String toXml(def value) {
		if (value == null)
			return ''
		if (value.class.getName() == 'java.util.Date') {
			return sdf.format(value)
		}
		throw new IncompatibleTypeException("Can't convert value '$value' of type '" + value.class.getName() + "' to XML form.")
	}

	def fromString(String value) {
		if (value == '') {
			return new Date()
		} else {
			try {
				//return sdf.parse(value)
				return new Date(value)
			} catch (Exception e) {
				// if parsing in the standard locale fails, try the european locale.  This should handle older files created in different locales.
				//TODO:: no clue why using eurpean locale if fails..
				//SimpleDateFormat europeanLocale = new SimpleDateFormat("d.M.yy h:mm")
				//return europeanLocale.parse(value)
				return sdf.parse(value)
			}
		}
	}

	def fromObject(def value) {
		if (value.class.getName() == 'java.util.Date')
			return value
		throw new ConversionNotSupportedException(value, 'java.util.Date')
	}

	String toSimpleName() {
		return "Date"
	}

	boolean isLocalizable() {
		// Theoretically true, but we are not going to localize dates right now.
		return false
	}

	boolean isComplex() {
		return true
	}

}

public class BooleanValueConverter implements PropertyValueConverter {
	private static final Logger log = Logger.getLogger(BooleanValueConverter.name)

	def fromXml(def xml) {
		return fromString(xml.text())
	}

	String toXml(def value) {
		if (value == null)
			return ''
		return value.toString()
	}

	def fromString(String value) {
		return Boolean.parseBoolean(value)
	}

	def fromObject(def value) {
		if (value.class.getName() == 'java.lang.Boolean') {
			return value
		} else {
			log.trace("Value '" + value.toString() + "' coerced to boolean value " + Boolean.parseBoolean(value.toString()), "default")
			return Boolean.parseBoolean(value.toString())

		}
	}

	String toSimpleName() {
		return "True/False"
	}

	boolean isLocalizable() {
		return false
	}

	boolean isComplex() {
		return true
	}

}

public class ChartContentsConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return xml.text().trim()
	}

	String toXml(def value) {
		if (value == null)
			return null
		return value.toString()
	}

	def fromString(String value) {
		return value.trim()
	}

	def fromObject(def value) {
		return value.toString().trim()
	}

	String toSimpleName() {
		return "Chart"
	}

	boolean isLocalizable() {
		return false
	}

	boolean isComplex() {
		return true
	}

}

public class MultiLineContentsConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return xml.text().trim()
	}

	String toXml(def value) {
		if (value == null)
			return null
		return value.toString()
	}

	def fromString(String value) {
		return value.trim()
	}

	def fromObject(def value) {
		return value.toString().trim()
	}

	String toSimpleName() {
		return "Text"
	}

	boolean isLocalizable() {
		return false
	}

	boolean isComplex() {
		return true
	}

}

public class BinaryDataConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		if (xml.text() == "") { return null; }
		String value = xml.text()
		return this.fromString(value);
	}

	String toXml(def value) {
		//return null;
		if (value == null) { return ''; }
		//base64 encode and return that as a string
		String dataXml = new String(Base64.encodeBase64(((BinaryDataImpl) value).getValue()))
		return dataXml
	}

	def fromString(String value) {
		if (value == null) { return null; }
		//assume a base64-encoded string of bytes
		return new BinaryDataImpl(Base64.decodeBase64(value.getBytes()));
	}

	def fromObject(def value) {
		if (value == null) { return null; }

		if (value instanceof BinaryData) {
			return value;
		} else if (value instanceof String) {
			return this.fromString(value)
		} else {
			try {
				BinaryData data = new BinaryDataImpl(value)
				return data
			} catch (Exception ex) {
				throw new ConversionNotSupportedException(value, "BinaryData");
			}
		}
	}

	String toSimpleName() {
		return "BinaryData"
	}

	boolean isLocalizable() {
		return false
	}

	boolean isComplex() {
		return true
	}

}

public class TOObjectConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return null
	}

	String toXml(def value) {
		if (value == null)
			return null
		return ''
	}

	def fromString(String value) {
		return ''
	}

	def fromObject(def value) {
		TO<?> empty_to = TOFactory.EMPTY_TO;
		return empty_to
	}

	String toSimpleName() {
		return "Reference"
	}

	boolean isLocalizable() {
		return false
	}

	boolean isComplex() {
		return true
	}
}

public class ChartContentsCRAHConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return xml.text().trim()
	}

	String toXml(def value) {
		if (value == null)
			return null
		return value.toString()
	}

	def fromString(String value) {
		return value.trim()
	}

	def fromObject(def value) {
		return value.toString().trim()
	}

	String toSimpleName() {
		return "Chart"
	}

	boolean isLocalizable() {
		return false
	}

	boolean isComplex() {
		return true
	}
}

public class LookupTableConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return xml.text().trim()
	}

	String toXml(def value) {
		if (value == null)
			return null
		return value.toString()
	}

	def fromString(String value) {
		return value.trim()
	}

	def fromObject(def value) {
		return value.toString().trim()
	}

	String toSimpleName() {
		return "Table"
	}

	boolean isLocalizable() {
		return false
	}

	boolean isComplex() {
		return true
	}
}

public class ChartContentsBTUConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return xml.text().trim()
	}

	String toXml(def value) {
		if (value == null)
			return null
		return value.toString()
	}

	def fromString(String value) {
		return value.trim()
	}

	def fromObject(def value) {
		return value.toString().trim()
	}

	String toSimpleName() {
		return "Chart"
	}

	boolean isLocalizable() {
		return false
	}

	boolean isComplex() {
		return true
	}
}

public class ConfigModbusConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return (xml.text().trim()).intern()
	}

	String toXml(def value) {
		if (value == null)
			return null
		return value.toString()
	}

	def fromString(String value) {
		return (value.trim()).intern()
	}

	def fromObject(def value) {
		return (value.toString().trim()).intern()
	}

	String toSimpleName() {
		return "Modbus Expression"
	}

	boolean isLocalizable() {
		return false
	}

	boolean isComplex() {
		return true
	}
}

public class ConfigBacnetConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return (xml.text().trim()).intern()
	}

	String toXml(def value) {
		if (value == null)
			return null
		return value.toString()
	}

	def fromString(String value) {
		return (value.trim()).intern()
	}

	def fromObject(def value) {
		return (value.toString().trim()).intern()
	}

	String toSimpleName() {
		return "Bacnet Expression"
	}

	boolean isLocalizable() {
		return false
	}

	boolean isComplex() {
		return true
	}
}

public class DatabaseTableContentsConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return xml.text().trim()
	}

	String toXml(def value) {
		if (value == null)
			return null
		return value.toString()
	}

	def fromString(String value) {
		return value.trim()
	}

	def fromObject(def value) {
		return value.toString().trim()
	}

	String toSimpleName() {
		return "Database Table"
	}

	boolean isLocalizable() {
		return false
	}

	boolean isComplex() {
		return true
	}
}

public class BacnetObjectConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return xml.text().trim()
	}

	String toXml(def value) {
		if (value == null)
			return null
		return value.toString()
	}

	def fromString(String value) {
		return value.trim()
	}

	def fromObject(def value) {
		return value.toString().trim()
	}

	String toSimpleName() {
		return "Bacnet Object"
	}

	boolean isLocalizable() {
		return false
	}

	boolean isComplex() {
		return true
	}
}

public class ValidatedStringConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return xml.text()
	}

	String toXml(def value) {
		if (value == null)
			return null
		return value.toString()
	}

	def fromString(String value) {
		String s = value.trim()
		if (s.isEmpty()){
			throw new IllegalArgumentException("Text property must not be empty.")
		}
		return s
	}

	def fromObject(def value) {
		String s = value.toString().trim()
		if (s.isEmpty()){
			throw new IllegalArgumentException("Text property must not be empty.")
		}
		return s
	}

	String toSimpleName() {
		return "Text"
	}

	boolean isLocalizable() {
		return false
	}

	boolean isComplex() {
		return false
	}
}

public class ValidatedMultiLineContentsConverter implements PropertyValueConverter {
	def fromXml(def xml) {
		return xml.text().trim()
	}

	String toXml(def value) {
		if (value == null)
			return null
		return value.toString()
	}

	def fromString(String value) {
		String s = value.trim()
		if (s.isEmpty()){
			throw new IllegalArgumentException("Text property must not be empty.")
		}
		return s
	}

	def fromObject(def value) {
		String s = value.toString().trim()
		if (s.isEmpty()){
			throw new IllegalArgumentException("Text property must not be empty.")
		}
		return s
	}

	String toSimpleName() {
		return "Text"
	}

	boolean isLocalizable() {
		return false
	}

	boolean isComplex() {
		return true
	}

}
