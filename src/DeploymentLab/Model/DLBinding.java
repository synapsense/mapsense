package DeploymentLab.Model;

/**
 * Represents a Trait that contains a script or other action that will be executed when the listened-to properties change.
 * @author Gabriel Helman
 * @since Io
 */
interface DLBinding extends ComponentTrait {
	public void eval(Object vars);
	public Object listVariables();
}
