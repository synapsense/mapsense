package DeploymentLab.Model;

import com.synapsense.dto.RuleType;

/**
 * Represents a DL Object Property that can be exported as a CRE Rule to the server.
 * All methods in the export process that interface with the CRE API should be limited to these methods.
 * @author Gabriel Helman
 */
public interface CREMember {
	/**
	 * Creates an ES RuleType object that represents this rule property.
	 * @return a RuleType, ready for exporting to the server.
	 */
	public RuleType createRuleType();

	/**
	 * Gets the schedule of this rule.
	 * @return A string containing the CRON configuration of this rule's schedule.
	 */
	public String getSchedule();

	/**
	 * Checks to see if this rule is scheduled.
	 * @return true if this rule has a schedule.
	 */
	public boolean hasSchedule();

	/**
	 * Gets the type name that this rule's type will use on the server.
	 * @return a String containing the rule's unique type name.
	 */
	public String getServerTypeName();

	/**
	 * Gets the unique name that this rule instance will use on the server.
	 * @return a String containing this rule instance's unique name.
	 */
	public String getServerInstanceName();

	/**
	 * Sets the unique name that this rule instance will use on the server.
	 * @param newName
	 */
	public void setServerInstanceName( String newName );

	/**
	 * Finds the name of the code source for this rule
	 * (Generally, this will either be a local .groovy file or a pre-compiled java class on the server.)
	 * @return a String containing the source for this rule.
	 */
	public String getSourceName();

}
