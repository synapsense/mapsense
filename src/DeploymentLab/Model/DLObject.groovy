
package DeploymentLab.Model

import DeploymentLab.channellogger.*
import DeploymentLab.Exceptions.ModelCatastrophe
import groovy.xml.MarkupBuilder

public class DLObject implements ComponentTrait {
	private static final Logger log = Logger.getLogger(DLObject.class.getName())

	private ObjectModel oModel
	private Dlid _dlid
	private String _type
	private String asName
	private boolean autochild
	private String key
	private boolean deleted
	//private def _properties = [:]
	//private def _childProperties = [:]
	private def parentIds = []

	// Keep the original value unchanged as well as the current value, that may change.
	private boolean exportableDefault = true
	private boolean _exportable = true

	//experiment to see if this makes it load faster
	private Map<String,Property> _properties = new HashMap<String,Property>()
	private Map<String,Property> _childProperties = new HashMap<String,Property>()

	private ArrayList<String> propertyListCache = new ArrayList<String>()
	private ArrayList<Property> propertyCache = new ArrayList<Property>()

	String parcelLocation
	String parcel

	DLObject(ObjectModel oModel, def objectXml) {
		this.oModel = oModel
		_type = objectXml.@type.text()
		asName = ''
		parcel = objectXml.@parcel.text()
		key = ''
		autochild = true
		deleted = false

		parseExportable( objectXml.@exportable )

		objectXml.property.collect{ propertyXml ->
			Property property = PropertyFactory.createProperty(this, propertyXml)
			if(property.getType() == 'childlink')
				_childProperties[property.getName()] = property
			else
				_properties[property.getName()] = property
		}
	}

	ObjectModel getObjectModel() {
		return oModel
	}

	void serialize(groovy.xml.MarkupBuilder builder, boolean includeAll = true) {
		builder.object('type': _type, 'as': asName, 'autochild': autochild, parcel:parcel, 'exportable': "$_exportable,$exportableDefault") {
			_properties.each { pName, p ->
				PropertyFactory.serialize(p, builder)
			}
			_childProperties.each { pName, p ->
				PropertyFactory.serialize( p, builder, includeAll )
			}
			'dlid'(_dlid)
			'key'(key)
			'deleted'(deleted)
			parentIds.each{ 'parentid'(it) }
		}
	}

	@Override
	void serializeAsPrototype(MarkupBuilder builder) {
		//see DLComponent.serializeManagedObjectAsPrototype
	}
/**
	Deserialize xml into an object
	@param objXml the XML from the DL file to use to construct this DLObject.
	@return [list of objects created as a result of deserializing this one]
	*/
	def deserialize(def objXml) {
		dlid = oModel.getDlid(objXml.dlid.text())
		key = objXml.key.text()
		asName = objXml.@as.text()
		deleted = Boolean.parseBoolean(objXml.deleted.text())
		parcel = objXml.@parcel.text()
		def created = []

		if( !objXml.@autochild.text().isEmpty() ) {
			autochild = Boolean.parseBoolean(objXml.@autochild.text())
		}

		parseExportable( objXml.@exportable )

		for( Object propXml : objXml.property ) {
			String name = propXml.@name.text()
			Property p // FIXME: start here
			if(_properties.containsKey(name)) {
				p = _properties[name]
				def c = PropertyFactory.deserialize(_properties[name], propXml)
				if(p instanceof DeploymentLab.Model.Collection)
					created += c
			} else if(_childProperties.containsKey(name)) {
				created += PropertyFactory.deserialize(_childProperties[name], propXml)
			} else {
				// Make this info since it is common during upgrades. If it ain't in the schema, then we probably don't care anyway.
				log.info("Property '${_type}.${name}' in project file was not in schema!  Not loading it.")
			}
		}
		//parentIds = objXml.parentid.collect{ it -> oModel.getDlid(it.text()) }
        parentIds = []
        for( Object it : objXml.parentid ){
            if(it.text()!=""){
            	parentIds += oModel.getDlid(it.text())
            }
        }

		propertyListCache.addAll(_properties.keySet())
		propertyListCache.addAll(_childProperties.keySet())

		propertyCache.addAll(_properties.values())
		propertyCache.addAll(_childProperties.values())

		return created
	}

	void parseExportable( def xml ) {
		if( xml.text().isEmpty() ) {
			_exportable = true
			exportableDefault = true
		} else {
			String[] vals = xml.text().split(',')
			_exportable = Boolean.parseBoolean( vals[0] )
			exportableDefault = Boolean.parseBoolean( vals[1] )
		}
	}

	void setIsExportable( boolean exportable ) {
		// If setting to false, do so. If true, really set it to the default since some should never be true.
		if( exportable ) {
			_exportable = exportableDefault
		} else {
			_exportable = exportable
		}
	}

	boolean isExportable() {
		return _exportable
	}

	boolean isExportableDefault() {
		return exportableDefault
	}

	def listChildTypes() {
		return _childProperties.collect{ n, prop -> prop.reftypes }.flatten().unique()
	}

   List<DLObject> getChildren(){
	   Set<DLObject> children = new LinkedHashSet<DLObject>()
	   for ( ChildLink p : _childProperties.values() ) {
		   children.addAll( p.children )
	   }
		return new ArrayList<DLObject>(children)
   }

	List<DLObject> getChildrenByType( String childType ) {
		Set<DLObject> children = new LinkedHashSet<DLObject>()
		for ( ChildLink p : _childProperties.values() ) {
			for( DLObject o : p.children ) {
				if( o.getType() == childType ) {
					children.add( o )
				}
			}
		}
		return new ArrayList<DLObject>(children)
	}

	ArrayList<DLObject> getSortedChildren(){
		Set<DLObject> children = new LinkedHashSet<DLObject>()
		for ( ChildLink p : _childProperties.values() ) {
			children.addAll( p.sortedChildren )
		}
		 return new ArrayList<DLObject>(children)
	}

	// FIXME: this returns all child objects.
	// FIXME: it is used when creating new objects to determine which objects a component manages
	// FIXME: now that components can specify objects in collections, not just parent/child, this method is insufficient
	def getDescendents() {
		return (children + children.collect{it.descendents}.flatten()).unique()
	}

	void setDlid(Dlid _id) {
		if(_dlid == null) {
			_dlid = _id
		} else{
			throw new Exception("Cannot reset a Dlid!")
		}
	}

	Dlid getDlid() {
		return _dlid
	}

	String getKey() {
		return key
	}

	void setKey(String _key) {
		key = _key
	}

	boolean hasKey(){
		if ( key != null && key.length() > 0){
			return true
		}
		return ( false )
	}

	boolean getDeleted() {
		return deleted
	}

	void setDeleted(boolean d) {
		deleted = d
	}

	String getType() {
		return _type
	}

	String getAsName() {
		return asName
	}

	boolean getAutoChild() {
		return autochild
	}

	ArrayList<String> listProperties() {
		return propertyListCache
	}

	ArrayList<Property> getProperties() {
		return propertyCache
 	}

	java.util.Collection<Property> getPropertiesByType(String type) {
		if(type == 'childlink')
			return _childProperties.values()
		else
			return _properties.values().findAll{it.type == type}
	}

	Property getObjectProperty(String name) {
		if(_properties.containsKey(name))
			return _properties[name]
		if(_childProperties.containsKey(name))
			return _childProperties[name]

		throw new Exception("No property named '" + name + "' in $this")
	}

	boolean hasObjectProperty(String name) {
		if(_properties.containsKey(name))
			return true
		if(_childProperties.containsKey(name))
			return true
		return false
	}

	Map<String,String> mapPropertyNamesValues(){
		def result = [:]
		//_properties.each{ k, v ->
		//	result[k] = v.getValue().toString()
		//}
		this.getPropertiesByType("setting").each{ s ->
			result[s.getName()] = s.getValue().toString()
		}
		return result
	}
	
	Setting getObjectSetting(String name){
		if(_properties.containsKey(name) && _properties[name].type.equalsIgnoreCase("setting")){
			return (Setting)_properties[name]
		} else {
			return null
		}
	}

	List<DLObject> getParents() {
		//return parentIds.collect{ oModel.getObject(it) }
		Set<DLObject> sett = new HashSet<DLObject>()
		for( Dlid id : parentIds){
			DLObject parent = oModel.getObject(id)
			if( parent != null ) {
				sett.add( parent )
			}
		}
		return new ArrayList<DLObject>(sett)
	}

	void addParent(DLObject _p) {
		parentIds += _p.dlid
	}

	void removeParent(DLObject _p) {
		parentIds -= _p.dlid
	}

	void makeDirty() {
		this.getPropertiesByType("dockingpoint").each{ p ->
		    p.makeDirty()
		}
		this.getPropertiesByType("collection").each{ p ->
		    p.makeDirty()
		}
	}

	///* NOTE: sucks performance.  Disabled for production
	String toString() {
		//return "DLObject('${this.getType()}'>$_dlid)@" + Integer.toHexString(hashCode())
		return "$_dlid>${_type}" + (asName ? ">${asName}" : '')
	}
	//*/

	String getName(){
		if ( this.hasObjectProperty("name") ){
			return this.getObjectProperty("name").getValue()
		} else {
			return this.toString()
		}
	}

	private DLObject _ancestor = null

	/**
	 * Finds this object's highest-level ancestor, which should be the drawing it is on.
	 * @return a DLObject representing this object's drawing, or null if it's an orphan
	 * @since Jupiter 2
	 */
	DLObject getAncestor(){
		if(_ancestor == null){
			if( this.getParents().isEmpty() ) {
				this._ancestor = this
			} else{
				Set<DLObject> drawingSet = new HashSet<>()
				for( DLObject parent : this.getParents() ) {
					drawingSet.add( parent.getAncestor() )
				}
				// Should never happen, but I'd like to know if it does!
				if( drawingSet.size() != 1 ) {
					String extra = "DRAWINGS: $drawingSet"
					throw new ModelCatastrophe("Object is associated with ${drawingSet.size()} Drawings instead of 1. $this'\n$extra" )
				}
				_ancestor = drawingSet.iterator().next()
			}
		}
		return _ancestor
	}

	public void setAncestor( DLObject ancestor ) {
		if( ancestor.getType() != ObjectType.DRAWING ) {
			throw new IllegalArgumentException("Unable to set ancestor for $this: $ancestor is not a ${ObjectType.DRAWING}.")
		}
		_ancestor = ancestor
	}

	public void clearAncestor(){
		_ancestor = null
	}

	//cause it's a trait, yo!

	@Override
	String getTraitId() {
		return dlid.toString()
	}

	@Override
	String getTraitDisplayName() {
		return this.getName()
	}

	@Override
	DLComponent getOwner() {
		//todo: return managing component
		return null
	}

	public boolean canSmartZoneLink() {
		return hasObjectProperty('smartZoneId')
	}

	boolean isSmartZoneLinked() {
		return getSmartZoneId() > 0
	}

	/**
	 * Gets the SmartZone Id.  A value of 0 means that the component is not linked and has no id.
	 * @return SmartZone Id or 0 if it has no Id.
	 */
	public int getSmartZoneId() {
		Setting prop = getObjectSetting('smartZoneId')
		if( prop == null ) {
			return 0
		}
		Integer val = prop.getValue()
		return ( val != null ) ? val : 0
	}

	/* No (un)setSmartZoneId since it is set via the component varbindings and should never be manually modified. */
}

