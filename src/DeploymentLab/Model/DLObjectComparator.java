package DeploymentLab.Model;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: ghelman
 * Date: Dec 3, 2010
 * Time: 4:47:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class DLObjectComparator  implements Comparator<DLObject> {
	@Override
	public int compare(DLObject o1, DLObject o2) {


		if ( o1.hasObjectProperty("name") && o2.hasObjectProperty("name") ){
			//return ( ((Setting)o1.getObjectProperty("name")).getValue().compareTo( ((Setting)o2.getObjectProperty("name")).getValue() ) );
			String propVal1 = (String) ((Setting)o1.getObjectProperty("name")).getValue();
			String propVal2 = (String) ((Setting)o2.getObjectProperty("name")).getValue();
			return propVal1.compareTo(propVal2);			

		} else {
			return o1.getType().compareTo( o2.getType() );
		}

	}
}
