
package DeploymentLab.Model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import DeploymentLab.Exceptions.*;

// A collection of references to other objects
public class Collection extends Property {
	private Set<String> reftypes;
	private List<Dlid> _contents;
	private boolean dirty;

	public Collection(DLObject _belongsTo, String n, Set<String> rt) {
		super(_belongsTo, n);
		reftypes = rt;
		_contents = new ArrayList<Dlid>();
		dirty = true;
        }

	public String getType() {
		return "collection";
	}

	public Set<String> getReftypes() {
		return reftypes;
        }

	public List<DLObject> getItems() {
		ArrayList<DLObject> rv = new ArrayList<DLObject>();
		for(Dlid id: _contents) {
			rv.add(getObjectModel().getObject(id));
		}
		return rv;
	}

	public List<Dlid> getItemIds() {
		return _contents;
	}

	public void deserialize(List<Dlid> ids, boolean d) {
		_contents.clear();
		_contents.addAll(ids);
		dirty = d;
	}

	public boolean isDirty() {
		return dirty;
	}

	public void clean() {
		dirty = false;
	}

	public void makeDirty(){
		dirty = true;
	}

	public void add(DLObject _o) throws IncompatibleTypeException, DuplicateObjectException {
		if(reftypes.size() > 0 && ! reftypes.contains(_o.getType()))
			throw new IncompatibleTypeException("Tried to add item of type " + _o.getType() + " but was expecting a type of " + reftypes);
		if(_contents.contains(_o.getDlid()))
			throw new DuplicateObjectException("Duplicate Item: " + _o);
		_contents.add(_o.getDlid());
		dirty = true;
	}

	public void remove(DLObject _o) {
		if(!_contents.contains(_o.getDlid()))
			return;
		_contents.remove(_o.getDlid());
		dirty = true;
	}

	public void clear() {
		_contents.clear();
		dirty = true;
	}

	public String toString() {
		return "Collection(name='" + name + "', reftypes=" + reftypes + ", dirty=" + dirty + ")";
	}
}

