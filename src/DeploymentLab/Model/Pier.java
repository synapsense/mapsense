package DeploymentLab.Model;

import DeploymentLab.Exceptions.IncompatibleTypeException;
import DeploymentLab.channellogger.Logger;

import java.util.Set;

/**
 * Represents a PropertyTO in the ES object model.  A PropertyTO is essentially a tuple of a TO and a property name.
 * In fact, the constructor is literally {@code PropertyTO(TO objId, java.lang.String propertyName) }
 * @author Gabriel Helman
 * @since Jupiter
 * Date: 1/6/12
 * Time: 2:44 PM
 */
public class Pier extends Property {
	private static final Logger log = Logger.getLogger(Pier.class.getName());
	protected final Set<String> reftypes;

	protected Dlid referentId;
	protected String referentName;
	
	protected DLObject referentObj; // This is a cached lookup of referentId
	protected Property referentProp; //and a cached lookup of the property itself
	
	boolean dirty;


	public Pier(DLObject _belongsTo, String n, Set<String> rt){
		super(_belongsTo, n);
		reftypes = rt;
		dirty = true;
	}

	@Override
	public String getType() {
		return "pier";
	}


	public Property getReferent() {
		if(referentId == null){
			return null;
		}
		if(referentObj == null){
			referentObj = getObjectModel().getObject(referentId);
			referentProp = referentObj.getObjectProperty(referentName);
		}
		return referentProp;
	}

	public DLObject getReferentObject() {
		if(referentId == null){
			return null;
		}
		if(referentObj == null){
			referentObj = getObjectModel().getObject(referentId);
			if (referentObj!= null){
				//log.trace("getReferentObject: name=" + referentName + " obj=" + referentObj.toString());
				referentProp = referentObj.getObjectProperty(referentName);
			}
		}
		return referentObj;
	}
	
	public String getReferentName(){
		return referentName;
	}

	public void deserialize(Dlid _id, String name, boolean d) {
		referentId = _id;
		referentObj = null;
		dirty = d;
		referentName = name;
		referentProp = null;
	}

	public boolean isDirty() {
		return dirty;
	}

	public void clean() {
		dirty = false;
	}

	public void makeDirty(){
		dirty = true;
	}

	public void setReferent(DLObject _o, String propertyName) throws IncompatibleTypeException {
		log.trace("setReferent:" + _o.toString() + " " + propertyName, "default");
		
		if(_o == null) {
			referentId = null;
			referentObj = null;
			referentName = null;
			referentProp = null;
			dirty = true;
			return;
		}
		if( reftypes.size() > 0 && !reftypes.contains(_o.getType()) ){
			throw new IncompatibleTypeException("Tried to set reference to " + _o.getType() + " but was expecting one of " + reftypes);
		}
		referentId = _o.getDlid();
		referentObj = _o;
		referentName = propertyName;
		dirty = true;
	}
	
	public void setReferent(Property target) throws IncompatibleTypeException {
		if(target == null){
			referentId = null;
			referentObj = null;
			referentName = null;
			referentProp = null;
			dirty = true;
			return;
		}

		DLObject _o = target.getOwner();
		log.trace("setReferent to point at:" + target + "owner=" + _o.toString(), "default");
		if( reftypes.size() > 0 && !reftypes.contains(_o.getType()) ){
			throw new IncompatibleTypeException("Tried to set reference to " + _o.getType() + " but was expecting one of " + reftypes);
		}
		referentId = _o.getDlid();
		referentObj = _o;
		referentName = target.getName();
		referentProp = target;
		dirty = true;
	}

	public void clear() throws IncompatibleTypeException{
		log.debug("Clearing Pier", "default");
		this.setReferent(null);
	}

	@Override
	public String toString() {
		return "Pier{" +
				"reftypes=" + reftypes +
				", referentId=" + referentId +
				", referentName='" + referentName + '\'' +
				", referentObj=" + referentObj +
				", referentProp=" + referentProp +
				", dirty=" + dirty +
				'}';
	}
}
