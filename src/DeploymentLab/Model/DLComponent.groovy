
package DeploymentLab.Model

import DeploymentLab.ProjectSettings
import com.panduit.sz.api.ss.assets.Floorplan
import com.panduit.sz.api.ss.assets.Rack
import org.apache.commons.jxpath.JXPathContext

import org.mvel2.MVEL
import org.mvel2.compiler.CompiledExpression

import DeploymentLab.Exceptions.*
import DeploymentLab.channellogger.*
import DeploymentLab.Pair
import DeploymentLab.NamePair
import DeploymentLab.CentralCatalogue
import DeploymentLab.Quadruple
import DeploymentLab.Localizer

import java.awt.geom.Point2D
import DeploymentLab.PropertyEditor.PropertySorter

public class DLComponent {
	private static final Logger log = Logger.getLogger(DLComponent.name)

	protected _managedObjects = []		// All objects that were created by this component
	protected _objects = [:]			// Map asName of DLObject --> DLObject

	protected _producers = [:]		// Map producer ID to Producer
	protected _consumers = [:]		// Map consumer ID to consumer

	protected Map<String,Conducer>_conducers = new HashMap<String,Conducer>() //conducer id to conducer

	protected ObjectModel omodel
	protected ComponentModel cmodel

	protected propertyChangeListeners = [:]

	protected String type
	//private def classes
	//private List<String> classes
	protected Set<String> classes
	protected def macids = []
	protected String grouppath
	protected String filters
	protected boolean _exportable = true

	//used for pre-mars hrow components
	protected def placeableChildren = []

	// Keep track of our proxies for easy access.
    private Set<DLComponent> _proxies

	// Keep track of our parcel instances so we can restore values if needed.
	private Map<String,ComponentParcel> parcelCache = [:]

	/**
	 * Map of static children; [child internal name: [ 'channel':channel#, 'display':displayName ]
	 */
	protected LinkedHashMap<String, LinkedHashMap<String, String>>_placeableChildren = new LinkedHashMap<String, LinkedHashMap<String, String>>()			// staticchildren component

	/**
	 * The type of any staticchild components
	 */
	protected String placeableChildType = null		// staticchild component type

	/**
	 * Stores all properties that should be kept synchronized from the parent component to any static or dynamic child components.
	 */
	HashMap<String,String> synchronizedProperties = new HashMap<String,String>()

	protected Map<String,ComponentProperty>_properties = [:]	//name : Property
	protected _propertyRefs = [:]
	protected _varBindings = []
	protected _varMap = [:]			// Maps variable name to a list of bindings

	protected dockings = []

	protected displaySettings = [:]

	protected JXPathContext myContext

	/**Represents the name of the virtual ES TO Key property during searches */
	protected static final String KEYS_NAME = "Object IDs"

	/**Represents the name of the virtual DLID property during searches */
	protected static final String DLIDS_NAME = "DLIDs"

    // flag to know the component is on the process of deleting
	protected boolean isDeleting = false;

    // Cached items for performance reasons.
	protected DLComponent myDrawing = null

	protected DLComponent(){}

	/* Need to add to Component a way to look at all of it's objects
	* and figure out what kind of child objects they can have.
	* ie, introspection on the data model
	*/
	DLComponent(ComponentModel _cm, ObjectModel _om, def cXml) {
		cmodel = _cm
		omodel = _om
		type = cXml.@type.text()
		//classes = cXml.@classes.text().split(",").toList()
		classes = new HashSet<String>(cXml.@classes.text().split(",").toList())
		grouppath = cXml.@grouppath.text()
		filters = cXml.@filters.text()
		_exportable = ( cXml.@exportable.text().isEmpty() ? true : Boolean.parseBoolean(cXml.@exportable.text()) )
		_dlid = Dlid.getDlid(cXml.@dlid.text())

        if( _dlid == null ) {
            // No DLID? Instead of forcing an upgrade for old non-DLID comps, just drop in a new DLID.
            _dlid = Dlid.getDlid()
            log.debug("Gave a DLID-less component a new DLID: $_dlid  $type");
        }

		//log.debug("deserialize component of type $type")

		for(def dXml :cXml.display.children() ){
			displaySettings[dXml.name()] = dXml.text()
		}
		myContext = JXPathContext.newContext(this)

		this.processComponentXML(cXml)

		// Use the setter since it will also propogate to the objects.
		setIsExportable( _exportable )
	}

	protected List<ComponentTrait> processComponentXML(def cXml, String forParcel = null){
		if( cXml.@type.text().isEmpty() ) {
			log.debug("processing parcel viscera xml")
		} else {
			log.debug("processing component xml: ${cXml.@type.text()}")
		}

		def relics = []
		this._roots = null


		for(def oXml : cXml.object){
			String asVar = oXml.@as.text()
			//assert asVar != ""

			String location = oXml.@location.text() //we need to do some real sauce here

			// this is the magic making unserializing vs new component work
			DLObject _o
			if(oXml.@dlid != '') {
                _o = omodel.getObject(omodel.getDlid(oXml.@dlid.text()))
			} else {
				// Safety first!
				if( hasRoot( asVar ) ) {
					// Only display an error during loading since some old projects might have this issue and we don't want loads to fail.
					if( cmodel.hasActiveState( ModelState.INITIALIZING ) ) {
						log.error("Duplicate object '$asVar' in component $this")
					} else {
						throw new ModelCatastrophe("Duplicate object '$asVar' in component $this")
					}
				}
				def created = []
                (_o, created) = omodel.deserializeObject(oXml)
				_managedObjects += created + _o

				relics.add(_o)
				relics.addAll(created)

				// Make sure exportable flag matches the owner
				_o.setIsExportable( this.isExportable() )
			}

			if( asVar ) {
				myContext.getVariables().declareVariable(asVar, _o)
			}

			// Parcels use "location" to force it to child to something rather than becoming a root by default.
			if(!location){
				_objects[asVar] = _o
			} else {
				_o.parcelLocation = location
				def ppp = new PropertyPathPair(_o.getParcelLocation())
				ChildLink target = (ChildLink)ppp.resolveValue(myContext)
				target.addChild(_o)
			}
		}

		// managedObjects is only populated above during a new (why?). So now we need to fill it during a deserialize.
		if( cXml.managedobjects.dlid.size() > 0 ) {
			_managedObjects = cXml.managedobjects.dlid.collect{omodel.getObject(omodel.getDlid(it.text()))}
		}

		// Promote any managed objects to root if they had an "as" name but are not a "pure" root.
		for( DLObject obj : _managedObjects ) {
			if( !obj.getAsName().isEmpty() ) {

				// Register objects with an "as" for reference in xpath statements.
				if( !myContext.getVariables().isDeclaredVariable( obj.getAsName() ) ) {
					myContext.getVariables().declareVariable( obj.getAsName(), obj )
				}

				// todo: This is a bit of a hack filter. The above is picking up a couple things, like controlout_damper
				//       that I'm not sure we want to have promoted. Until it can be analyzed, only promote COIN and Rack objects.
				if( !_objects.containsKey( obj.getAsName() ) ) {
					if( obj.getType().startsWith('controlpoint_') || obj.getType().equals('wsnnode') ) {
						_objects[obj.getAsName()] = obj
					}
				}
			}
		}

		for( def pbXml : cXml.docking ){
			String sFrom = pbXml.@from.text()
			String sTo = pbXml.@to.text()

			// Safety first!
			if( hasDocking( sFrom, sTo ) ) {
				// Only display an error during loading since some old projects might have this issue and we don't want loads to fail.
				if( cmodel.hasActiveState( ModelState.INITIALIZING ) ) {
					log.error("Duplicate docking in component $this: $sFrom -> $sTo")
				} else {
					throw new ModelCatastrophe("Duplicate docking in component $this: $sFrom -> $sTo")
				}
			}

			Property pFrom = myContext.getValue(sFrom)
			if(pFrom instanceof Dockingpoint) {
				DLObject pTo = myContext.getValue(sTo)
				pFrom.setReferrent(pTo)
			} else if(pFrom instanceof DeploymentLab.Model.Collection) {
				for(def it : myContext.iterate(sTo)){
					pFrom.add(it)
				}
			} else if (pFrom instanceof Pier){
				//String sProp = pbXml.@property.text()
				//DLObject pTo = myContext.getValue(sTo)
				Property pTo = myContext.getValue(sTo)
				
				//println "constructing pier docking: pto=$pTo, owner is ${pTo.getOwner()}"
				
				pFrom.setReferent(pTo)
			} else {
				throw new IncompatibleTypeException("Docking 'from' must be a Dockingpoint or Collection ($sFrom -> $sTo)")
			}
			def d = new Docking(this,sFrom,sTo,pbXml.@parcel.text(), myContext)
			this.dockings += d
			relics.add(d)
		}

		// Process inherited ones at the same time so we maintain property order without much effort.
		for( def pXml : cXml.children().findAll { it.name() in ['property','inheritedproperty'] } ){
			String pName = pXml.@name.text()

			// Safety first!
			if( hasProperty( pName ) ) {
				// Only display an error during loading since some old projects might have this issue and we don't want loads to fail.
				if( cmodel.hasActiveState( ModelState.INITIALIZING ) ) {
					log.error("Duplicate property '$pName' in component $this.")
				} else {
					throw new ModelCatastrophe("Duplicate property '$pName' in component $this.")
				}
			}

			_properties[pName] = ( pXml.name() == 'property' ? new ComponentProperty(pXml) : new ComponentInheritedProperty( pXml, this ) )
			_varMap[pName] = []
			relics.add(_properties[pName])
		}
		flushSortedPropsCache()

		for( def bXml : cXml.varbinding ){
			ComponentBinding binding = new ComponentBinding(bXml, myContext, this)
			for(String varName : binding.listVariables()){
				//println "$type: constructing binding $varName "
				try {
					_varMap[varName] += binding
				} catch (NullPointerException e){
					log.fatal("Variable '$varName' does not exist for component type $type", "message")
				}
			}
			_varBindings += binding
			relics.add(binding)
		}

		for( def bXml : cXml.turbobinding ){
			TurboBinding binding = new TurboBinding(bXml, this)
			for(String varName : binding.listVariables()){
				//println "$type: constructing binding $varName "
				try {
					_varMap[varName] += binding
				} catch (NullPointerException e){
					log.fatal("Variable '$varName' does not exist for component type $type", "message")
				}
			}
			_varBindings += binding
			relics.add(binding)
		}


		for(def dXml : cXml.multidocking){
			Multidocking td = new Multidocking(dXml,myContext,this)
			_varBindings += td
			for(String varName : td.listVariables()){
				try {
					_varMap[varName] += td
				} catch (NullPointerException e){
					log.fatal("Variable '$varName' does not exist for component type $type", "message")
				}
			}
			relics.add(td)
		}

		List<ComponentParcel> newParcels = []
		for(def dXml : cXml.parcel){
			// Safety first!
			if( hasParcel( dXml.@name.text() ) ) {
				throw new ModelCatastrophe("Duplicate parcel '${dXml.@name.text()}' in component $this.")
			}

			// If we have a cached one, use it. This allows us to not lose parcel property values anytime a parcel is
			// dematerialized then materialized again, which can happen very easily and is really annoying.
			ComponentParcel cp = parcelCache.get( dXml.@name.text() )
			if( cp == null ) {
				cp = new ComponentParcel(dXml,myContext,this)
				parcelCache.put( cp.name, cp )
			} else {
				log.trace("Using cached parcel: ${cp.name}")
			}

			_varBindings += cp
			for(String varName : cp.listVariables()){
				try {
					_varMap[varName] += cp
				} catch (NullPointerException e){
					log.fatal("Variable '$varName' does not exist for component type $type", "message")
				}
			}
			relics.add(cp)
			newParcels.add(cp)
		}

		for(DLBinding binding : _varBindings){
			if(!(binding instanceof ComponentParcel)){
				binding.eval(getPropertyValueMap(binding.listVariables()))
			}
		}

		for(def xml : cXml.consumer){
			_consumers[xml.@id.text()] = new Consumer(this, xml, myContext)
			relics.add(_consumers[xml.@id.text()])
		}

		for(def xml : cXml.producer){
			//log.info("add a producer:" + xml.@id.text())
            _producers[xml.@id.text()] = new Producer(this, xml, myContext)
			relics.add(_producers[xml.@id.text()])
		}

		for(def xml : cXml.conducer){
			Producer pro = new Producer(this, xml.producer, myContext)
			Consumer con = new Consumer(this, xml.consumer, myContext)
			_conducers[xml.@id.text()] = new Conducer(this, xml, pro, con)
			_producers[ pro.id ] = pro
			_consumers[ con.id ] = con
			relics.add(_conducers[xml.@id.text()])
		}

		// Moved up so that managedObjects is populated about the same time on deserialize as it does with new.
		//if(cXml.managedobjects.dlid.size() > 0)
		//	_managedObjects = cXml.managedobjects.dlid.collect{omodel.getObject(omodel.getDlid(it.text()))}

		if(cXml.macids.@value.text().length() > 0){
			macids.addAll( cXml.macids.@value.text().split(',') )
		}
		
		//placeablechildren
		//used for pre-Mars hrow components
		if(cXml.placeablechildren.@value.text().length() > 0)
			placeableChildren = cXml.placeablechildren.@value.text().split(',')

		
		if(cXml.children.size()>0){
			placeableChildType = cXml.children.@type.text()
			
			for(def childXml : cXml.children.child){
				String name=childXml.@name.text()
				String channel = childXml.@channel.text()
				String display = childXml.@display.text()
				
				//_placeableChildren[name] = channel
				_placeableChildren[name] = [:]
				_placeableChildren[name]["channel"] = channel
				_placeableChildren[name]["display"] = display
			}
		}

		cXml.synchronizedproperties.property.each{ p->
			this.synchronizedProperties.put(p.@parent.text().toString(), p.@child.text().toString())
		}

		// For parcel traits, set what parcel they belong to.
		if( forParcel != null ) {
			for( ComponentTrait t : relics ) {
				t.setParcel( forParcel )
			}
		}

		automaticObjectAdoption()

		// Now that everything is loaded, process the parcels so they can setup things if they should be active already.
		for( ComponentParcel cp : newParcels ) {
			cp.eval( getPropertyValueMap( cp.listVariables() ) )
		}

		return relics
	}

	/**
	 * Check the managed objects and see if there are any that should be children of other objects in the component
	 * based on the object model definitions and creates the relationships. This eliminates the need to physically
	 * represent that relationship in the component XML. Defining childlink attributes in a given object is all that is
	 * needed.
	 *
	 * This was created to get around all the XML modifications to dozens of components that were necessary because we
	 * wanted to make something a child that wasn't before. The main culprit was the move to make wsnnode a child of the
	 * rack. That would have been a whole bunch of XML work.
	 */
	protected void automaticObjectAdoption() {

		_objects.values().each { internalParent ->
			// Skip objects with explicit directions to not autochild.
			if( !internalParent.getAutoChild() ) return

			List<String> childTypes = omodel.listChildObjectTypes( internalParent.getType() )

			// Build a list of potential children. For the staticchild abominations, we need to also consider the parent objects.
			def internalObjects = _objects.values().asList()
			internalObjects.remove( internalParent )
			if( !getParentsOfRole("staticchildplaceable").isEmpty() ) {
				internalObjects.addAll( getStaticParent()._objects.values() )
			}

			// See if we have any potential children that can be a child of the parent object.
			internalObjects.each { internalChild ->
				if( childTypes.contains( internalChild.getType( ) ) ) {
					internalParent.getPropertiesByType("childlink").each{ cl ->
						ChildLink link = (ChildLink) cl
						if( link.reftypes.contains( internalChild.getType() ) && !link.hasChild( internalChild ) ) {
							boolean adopt = true

							if( internalChild.getType() == 'wsnnode' ) {
								// For nodes, some components have multiple racks. Make sure the rack adopts only nodes
								// for which it has a dockingpoint to at least one sensor on that node.
								adopt = false
								for( Dockingpoint dp : internalParent.getPropertiesByType('dockingpoint') ) {
									if( dp.getReferrent()?.getType() == 'wsnsensor' ) {
										if( dp.getReferrent().getParents().contains( internalChild ) ) {
											adopt = true
											break
										}
									}
								}
							} else {
								// Skip adoption if it already has a parent of this type. This stops things like Dual Inlets from
								// getting accidental hookups where both racks have both sets of controlpoint objs as children.
								for( DLObject p : internalChild.getParents() ) {
									if( p.getType() == internalParent.getType() ) {
										adopt = false
										break
									}
								}
							}

							if( adopt ) {
								log.debug("Auto-adoption: $internalParent --> $internalChild")
								link.addChild( internalChild )
							} else {
								log.debug("Skipping auto-adoption: $internalParent --> $internalChild")
							}
						}
					}
				}
			}
		}
	}


	String getDisplaySetting(String name) {
		return displaySettings[name]
	}

	public void setDisplaySetting(String name, String value) {
		log.trace("setDisplaySetting '$name' := '$value'")
		def old = displaySettings[name]
		displaySettings[name] = value
		firePropertyChanged(name, old, value)
	}

	void serialize(groovy.xml.MarkupBuilder builder, boolean serializeImages = true) {
		builder.component('type': type, 'classes': classes.join(','), 'grouppath': grouppath, 'filters': filters, 'dlid': _dlid, 'exportable': _exportable) {
			display{
				displaySettings.each{ n, v ->
					"$n"(v)
				}
			}
			_objects.each{ k, _o ->
				object('type': _o.type, 'as': k, 'dlid': _o.dlid)
                        }
			_properties.each{ k, p ->
				p.serialize(builder, serializeImages)
			}
			_varBindings.each{ b ->
				b.serialize(builder)
			}
			_producers.each{ id, p ->
				if ( ! p.hasConducer() ){
					p.serialize(builder)
				}
			}
			_consumers.each{ id, c ->
				if ( ! c.hasConducer() ){
					c.serialize(builder)
				}
			}

			_conducers.each{ id, d ->
				d.serialize(builder)
			}

			managedobjects{
				_managedObjects.each{ o ->
					dlid(o.dlid)
				}
			}
			macids('value': macids.join(','))
			placeablechildren('value': placeableChildren.join(','))
			
			children('type':placeableChildType){
				_placeableChildren.each{k,v->
					child('name':k, 'channel':v['channel'], 'display':v['display'])
					
				}
			}

			if(synchronizedProperties.size() > 0){
				builder.synchronizedproperties(){
					synchronizedProperties.each{ k, v ->
						builder.property( parent : k, child: v )
					}
				}
			}
		}
	}

	void serializeManagedObjectAsPrototype(groovy.xml.MarkupBuilder builder, DLObject o){
		builder.object('type': o.type, 'as': o.getAsName() ){
			builder.dlid("DLID:-1")
			((DLObject)o).getPropertiesByType("setting").each{ s ->
				PropertyFactory.serialize(s,builder)
			}

			((DLObject)o).getPropertiesByType("childlink").each{ cl ->
				builder.property(name: cl.getName()){
					((ChildLink)cl).getChildren().each{ c ->
						if(managedObjects.contains(c)){
							this.serializeManagedObjectAsPrototype(builder, c)
						}
					}
				}
			}
		}

	}

	/**
	 * Regenerate the prototype XML from this instance.
	 * @param builder the builder to throw the XML into.
	 */
	void serializeAsPrototype(groovy.xml.MarkupBuilder builder) {
		builder.component('type': type, 'classes': classes.join(','), 'grouppath': grouppath, 'filters': filters ) {
			display{
				displaySettings.each{ n, v ->
					"$n"(v)
				}
			}

			for(DLObject o : _objects.values()){
				this.serializeManagedObjectAsPrototype(builder, o)
			}

			_properties.each{ k, p ->
				p.serializeAsPrototype(builder)
			}
			_varBindings.each{ b ->
				b.serializeAsPrototype(builder)
			}

			for(Docking d : dockings){
				d.serializeAsPrototype(builder)
			}

			_producers.each{ id, p ->
				if ( ! p.hasConducer() ){
					p.serializeAsPrototype(builder)
				}
			}
			_consumers.each{ id, c ->
				if ( ! c.hasConducer() ){
					c.serializeAsPrototype(builder)
				}
			}

			_conducers.each{ id, d ->
				d.serializeAsPrototype(builder)
			}


			macids('value': macids.join(','))
			placeablechildren('value': placeableChildren.join(','))

			children('type':placeableChildType){
				_placeableChildren.each{k,v->
					child('name':k, 'channel':v['channel'], 'display':v['display'])

				}
			}

			if(synchronizedProperties.size() > 0){
				builder.synchronizedproperties(){
					synchronizedProperties.each{ k, v ->
						builder.property( parent : k, child: v )
					}
				}
			}
		}
	}

	void remove() {
		log.trace("removing managed objects")
		_managedObjects.each{ o ->
			omodel.remove(o)
			ProjectSettings.getInstance().removeDlidMapping( o.getDlid() )
		}
	}

	void undelete() {
		_managedObjects.each{ o ->
			omodel.undelete(o)
		}
	}

	/**
	 * @deprecated
	 * Checks to see if the given role is in the list of roles for this component.
	 * Roles used to be called Classes, but this was confusing.  Use hasRole() instead. 
	 */
	@Deprecated
	boolean hasClass(String className) {
		return this.hasRole(className)
	}

	/**
	 * Returns true if the component is a Container.
	 * At the moment, Containers are zones (both  kinds) and sets.
	 */
	boolean isContainer() {
		return( this.hasRole( ComponentRole.ZONE ) || this.hasRole( ComponentRole.LOGICAL_GROUP ) )
	}

	boolean isContainerOrNetwork(){
		return( this.isContainer() || this.hasRole( ComponentRole.NETWORK ) || this.hasRole( ComponentRole.DRAWING ) )
	}

	boolean isProxy() {
		return hasRole( ComponentRole.PROXY )
	}

	void setIsExportable( boolean exportable ) {
		// If setting to false, do so. If true, really set it to the default since some should never be true.
		if( exportable ) {
			String dflt = cmodel.getTypeAttribute( this.type, 'exportable' )
			_exportable = ( dflt.isEmpty() ? true : Boolean.parseBoolean( dflt ) )
		} else {
			_exportable = exportable
		}

		// Propogate to the objects.
		for( DLObject o : _managedObjects ) {
			o.setIsExportable( _exportable )
		}
	}

	boolean isExportable() {
		return _exportable
	}

	boolean isExportableDefault() {
		String dflt = cmodel.getTypeAttribute( this.type, 'exportable' )
		return ( dflt.isEmpty() ? true : Boolean.parseBoolean( dflt ) )
	}

	public boolean canSmartZoneLink() {
		return hasProperty('smartZoneId')
	}

	boolean isSmartZoneLinked() {
		return getSmartZoneId() > 0
	}

	public Rack getSmartZoneRack(Floorplan fp) {
		if (this.isSmartZoneLinked()) {
			for (Rack rack : fp.getRacks()) {
				if (rack.id == this.getPropertyValue('smartZoneId')) {
					return rack
				}
			}
		}
	}

	/**
	 * Gets the SmartZone Id.  A value of 0 means that the component is not linked and has no id.
	 * @return SmartZone Id, or null if it is not set.
	 */
	public int getSmartZoneId() {
		Integer val = getPropertyValue('smartZoneId')
		return ( val != null ) ? val : 0
	}

	/**
	 * Sets the SmartZone Id.
	 * @param id
	 */
	public void setSmartZoneId( int id ) {
		if( hasProperty('smartZoneId') ) {
			setPropertyValue( 'smartZoneId', id )
		}
	}

	public void unsetSmartZoneId() {
		if( isSmartZoneLinked() ) {
			setSmartZoneId( 0 )
		}
	}


	/**
	 * Checks to see if this component has been exported to the server already.  This method does not check that all
	 * objects have been exported, only that at least one has been exported.  Due to dynamic object creation via Artisan,
	 * it is possible for a component to have a mix of exported and non-exported objects. Therefore, never use this to
	 * check if the entire component has been exported.
	 *
	 * @return  True if it was exported and exists on the server, false if it has never been exported.
	 */
	public boolean isExported() {
		// Check objects for a TO. It is possible
		for( DLObject o : getManagedObjects() ) {
			if( o.hasKey() ) {
				return true
			}
		}
		return false
	}

	/**
	 * Checks to see if the given role is in the list of roles for this component.
	 */
	boolean hasRole(String roleName) {
		return classes.contains(roleName)
	}

	boolean hasRoles( java.util.Collection<String> roleNames ) {
		return classes.containsAll( roleNames )
	}

	/**
	 * Returns this component's roles as a list.
	 * rolesSet() should be used instead.
	 * @return an ArrayList containing the roles of this component
	 * @see #rolesSet
	 */
	@Deprecated
	ArrayList<String> listRoles() {
		return new ArrayList<String>(classes)
	}

	/**
	 * Returns a Set of this component's roles.
	 * @return a HashSet holding the roles of this component
	 */
	Set<String> rolesSet(){
		return classes
	}

	/**
	 * Checks if any of the children of this component have the role specified.
	 * Intended to be used for things like "duck-typing" sets - if a set has a control component in it, it's a control set, etc.
	 * Note that this component is not checked for the role in question.
	 * @param roleName the name of the role to check.
	 * @return True if this component has a child with the specified role.
	 * @see DeploymentLab.Model.DLComponent#hasRole(String)
	 */
	boolean hasChildRole(String roleName){
		boolean result = false
		this.getChildComponents().each{ c ->
		    if ( c.hasRole(roleName)){
			    result = true
		    }
		}
		return ( result )
	}


	ComponentModel getModel() {
		return cmodel
	}

	ObjectModel getOModel() {
		return omodel
	}

	String getType() {
		return type
	}

	String getGrouppath(){
		return grouppath
	}

	String getFilters(){
		return filters
	}


	String getDescription() {
		return displaySettings['description']
	}

	String getShape() {
		return displaySettings['shape']
	}

	Map<String,String> getBadges() {
		Map<String,String> badges = [:]
		for( Map.Entry<String,String> setting : displaySettings.entrySet() ) {
			if( setting.getKey().contains('badge') ) {
				badges.put( setting.getKey(), setting.getValue() )
			}
		}
		return badges
	}

	boolean isDeprecated(){
		if ( displaySettings.containsKey("deprecated") && displaySettings["deprecated"] == "true" ){
			return true
		}
		return false
	}

	DLObject getObject(String name) {
		return _objects[name]
	}
	
	/**
	 * Caches a list of objects that are the roots of this component.
	 * Whenever the managed obejcts list changes (such as adding/removing parcels)
	 * this needs to become invalidated
	 */
	protected ArrayList _roots = null;
	ArrayList<DLObject> getRoots() {
		//return _objects.collect{ k, v -> v }
		if(_roots == null){
			_roots = new ArrayList<DLObject>(_objects.values())
		}
		return _roots
	}

	Set<String> listRootNames() {
		return _objects.keySet()
	}

	Boolean hasRoot(String rootName){
		return _objects.containsKey(rootName)
	}

	Boolean hasRoot(DLObject root){
		return getRoots().contains( root )
	}

	DLObject getRoot(String rootName){
		return _objects[rootName]
	}


	ArrayList<DLObject> getObjectsOfType(String objectType) {
		Set<DLObject> sett = new HashSet<DLObject>()
		for(DLObject o: _objects.values() ){
			if ( o.getType() == objectType ){
				sett.add( o )
			}
		}
		return new ArrayList<DLObject>(sett)
	}
	
	Map<String, DLObject> getObjectMapOfType(String objectType){
		Map<String, DLObject> map = new HashMap<String, DLObject>()
		
		for(String asName: _objects.keySet()){
			DLObject o = _objects[asName]
			if ( o.getType() == objectType ){
				map.put(asName, o)
			}
		}
		
		return map
		
	}

	/**
	 * Constructs a list of object properties within this component that do not have varbindings attached to them.
	 *
	 * Currently, this only looks at settings that are numbers (and ignores lastValue), and only at root level objects
	 * (sorry wsnsensors)
	 *
	 * @return a List of Quadruples, each being: componentTypeName, objectAsName, propertyName, default value
	 *
	 * @see Quadruple
	 */
	List<Quadruple<String,String,String,Object>> getObjectSettingsWithoutVarbindings(){
		//get all settings
		//remove the ones that are varbound
		//return the result

		List<Pair<String,Setting>> allSettings = []

		//to start, just the root level objects
		_objects.each{ asName, mo ->
			 mo.getPropertiesByType("setting").each{ ps ->
				 Setting s = ps
				 //exception: ignore lastValue, and lets only do numbers
				 //if (! s.getName().equals("lastValue") && ! s.getValueType().equalsIgnoreCase("java.lang.String") && ! s.getValueType().equalsIgnoreCase("com.synapsense.dto.BinaryData") ){

				//new exception: clamp to list in component.properties
				List allowedSettings = CentralCatalogue.getComp("configureDefaults.settings").split(",")
				if( allowedSettings.contains( s.getName() ) ){
					 allSettings.add( new Pair(asName, s) )
				 }
			 }
		}
		def toRemove = []
		_varBindings.each{ b ->
			List<Setting> settings = b.getTargetSettings()
			settings.each{ s ->
				allSettings.each{ all ->
					if ( all.b == s ){
						toRemove.add( all )
					}
				}
			}
		}
		toRemove.each{ r ->
			allSettings.remove(r)
		}
		//Quad: componentTypeName, objectAsName, propertyName, default value
		List<Quadruple<String,String,String,Object>> results = []
		allSettings.each{ s ->
			results += new Quadruple(this.getType(), s.a, s.b.getName(), s.b.getValue() )
		}
		return results
	}

	/** Return a list of property names */
	ArrayList<String> listProperties() {
		return _properties.collect{ k,v -> k }
	}

	ArrayList<String> listProperties( String forParcel ) {
		ArrayList<String> result = []
		for( Map.Entry<String,ComponentProperty> entry : _properties ) {
			if( entry.getValue().getParcel() == forParcel ) {
				result.add( entry.getKey() )
			}
		}
		return result
	}

	protected List<ComponentProperty> sortedProperties
	protected List<String> sortedPropertyNames

	List<ComponentProperty> listSortedProperties() {
		cacheSortedProps()
		return new ArrayList( sortedProperties )
	}

	List<String> listSortedPropertyNames() {
		cacheSortedProps()
		return new ArrayList( sortedPropertyNames )
	}

	private void flushSortedPropsCache() {
		sortedProperties = null
		sortedPropertyNames = null
	}

	private void cacheSortedProps() {
		if( sortedProperties == null || sortedPropertyNames == null ) {
			// Split out the root properties from the parcels.
			sortedProperties = []
			Map<String,List<ComponentProperty>> parcelProps = [:]

			_properties.each { propName, prop ->

				if( prop.parcel.isEmpty() ) {
					sortedProperties.add( prop )
				} else {
					def pList = parcelProps.get( prop.parcel )
					if( pList == null ) {
						pList = []
						parcelProps.put( prop.parcel, pList )
					}
					pList.add( prop )
				}
			}

			// Sort the root properties in a consistent order
			Collections.sort( sortedProperties, PropertySorter.INSTANCE )

			// Sort each set of parcel props.
			Map <String,String> parcelOwners = [:]
			parcelProps.each { parcelName, pList ->
				Collections.sort( pList, PropertySorter.INSTANCE )

				// While we're here, find out what property it belongs to.
				if( !parcelOwners.containsKey( parcelName ) ) {
					parcelOwners.put( parcelName, getParcelWithName( parcelName ).dependentVar )
				}
			}

			// Insert the parcel props just after the prop they belong to. May need multiple passes since a parcel
			// prop may belong to a prop that is also a parcel that wasn't inserted yet. tryAgain used as a fuse cutout.
			boolean tryAgain = true
			while( parcelProps.size() > 0 && tryAgain ) {
				tryAgain = false
				def parcelNames = new ArrayList( parcelProps.keySet() )
				parcelNames.each { parcelName ->
					for( int i = 0; i < sortedProperties.size(); i++ ) {
						if( sortedProperties.get(i).getName() == parcelOwners.get( parcelName ) ) {
							tryAgain = true
							sortedProperties.addAll( i+1, parcelProps.get( parcelName ) )
							parcelProps.remove( parcelName )
							break // from .each closure
						}
					}
				}
			}

			if( !parcelProps.isEmpty() ) {
				// This is really bad. Props aren't getting set to the correct parcel or something.
				log.error("Parcel owners not found. Properties not in Property Table. $parcelProps")
			}

			// Also cache the names as a shortcut since it is used often.
			sortedPropertyNames = sortedProperties.collect{ it.getName() }
		}
	}


	/** return the map of property names : property objects */
	Object mapProperties(){
		return _properties
	}
	
	/**
	 * Gets a list of all visible property display names as strings.
	 * @return a list of strings containing the visible display names.
	 */
	ArrayList<String> listVisiblePropertyNames() {
		def results = []
		for( ComponentProperty v : _properties.values()  ) {
			if ( v.isDisplayed() ){
					results += v.getDisplayName()
				}
		}
		return ( results )
	}

	/**
	 * Gets all visible property names as a list of NamePair objects holding both the display and internal names.
	 * Also grab producer/consumer names.
	 * @return the list of NamePairs.  This list is not sorted.
	 */
	ArrayList<NamePair> listVisiblePropertyNamePairs(boolean simpleEditableOnly = false) {
		def results = []
		for( ComponentProperty v : _properties.values()  ) {
			if ( v.isDisplayed() ){
				if(!simpleEditableOnly){
					results += new NamePair( v.getName(), v.getDisplayName() )

				}else if ( simpleEditableOnly && v.isEditable() ){
					//for the simpleedit case, we also want to screen out "fancy" properties
					if(v.getValueChoices() == null && ! PropertyConverter.isComplex(v.getType())){
						results += new NamePair( v.getName(), v.getDisplayName() )
					}
				}
			}
		}
		if(!simpleEditableOnly){
		this._producers.each { pname, prod ->
			results += new NamePair( prod.getId(), prod.getName() )
		}
		this._consumers.each { pname, prod ->
			results += new NamePair( prod.getId(), prod.getName() )
		}
		//also keys and dlids
		results += new NamePair(KEYS_NAME, KEYS_NAME)
		results += new NamePair(DLIDS_NAME, DLIDS_NAME)
		}

		return ( results )
	}


	/**
	 * Returns all of this component's properties as a map of [display name: value].
	 * This map only contains strings, not the properties themselves.
	 * This is intended to support quick searches or displays of data, not to do any real processing.
	 * Groovy collection Closures converted to java for:each loops as those proved to be measurably faster.
	 * Now also includes the list of producer and consumer names, with their values as a list of names of associated components
	 * @param returnHidden Whether to include non-displayed values in the results.
	 * @return A map of [property display names : values] for all of this component's properties
	 * @see DeploymentLab.Model.ComponentModel#spotlight(String, String, boolean, List<DeploymentLab.Model.DLComponent>)
	 */
	Map<String,String> mapPropertyNamesValues(boolean returnHidden, boolean includeAssociations = true) {
		Map<String,String> results = [:]
		for( ComponentProperty v : _properties.values()  ) {
			// ignore hidden fields if they aren't wanted.
			if( returnHidden || v.isDisplayed() ) {
				def val = v.getValue()
				if( val == null ) {
					val = ""
				} else {
					// Make sure it is formatted correctly for the locale settings then save it in the results.
					// Probably shouldn't be here since this is technically the model and localization should be in the view
					// but this is used in the search and, short of major refactoring, this is the most efficient way.
					if( v.isUnitConvertable() && val != null ) {
						val = Localizer.convert( val, v.dimensionName )
					}

					if( Localizer.isLocalizable( v ) ) {
						val = Localizer.format( val )
					}
				}

				results[v.getDisplayName()] = val.toString()
			}
		}

		if ( includeAssociations ) {
			this._producers.each { pname, prod ->
				def links =  ""
				prod.listConsumerComponents().each{ pair ->
					links += pair.a.getName() + ", "
				}
				results[ prod.getName() ] = links
			}
			this._consumers.each { cname, con ->
				def links =  ""
				con.listProducerComponents().each{ pair ->
					links += pair.a.getName() + ", "
				}
				results[ con.getName() ] = links
			}

			//include keys!
			results[DLIDS_NAME] = this.getPropertyNamedValue(DLIDS_NAME)
			results[KEYS_NAME] = this.getPropertyNamedValue(KEYS_NAME)
		}
		return ( results )
	}


	// For a component X, i need to know what other components it can be a child of
	// ie, if me.type in component.listChildTypes()
	boolean canChild(DLComponent c) {
		return listChildObjectTypes().intersect(c.listManagedTypes()).size() > 0
	}

	// This is the type of objects that this component manages
	public def listManagedTypes() {
		return roots.collect{it.type}.unique()
	}

	// This is the type of child *objects* that this component manages
	public def listChildObjectTypes() {
		return roots.collect{ it.listChildTypes() }.flatten().unique()
	}

	ComponentProperty getComponentProperty( String pName, ComponentParcel forParcel ) {
		return getComponentProperty( pName, forParcel.getTraitId() )
	}

	// Effing Groovy now needs this to handle ambiguity created by null arguments *sigh* #dynamic!
	ComponentProperty getComponentProperty(String pName, def nullarg ) {
		return getComponentProperty( pName )
	}

	/* forParcel is only necessary in cases where generic code is written to expect certain property names, but Artisan has messed that up. */
	ComponentProperty getComponentProperty(String pName, String forParcel = null ) {
		if( forParcel == null || forParcel.isEmpty() ) {
			// The standard easy pre-Artisan case.
			return _properties[pName]
		}

		// Looks like we need to go the painful route and find one from a parcel. You see... to avoid duplicate property
		// names in components using Artisan, we prepended something like a namespace to the property name. This breaks
		// a whole bunch of code that has hardcoded property names. This really needs to be rethought since this sucks.
		// Maybe internally prepend the parcel name and modify everything in DLComponent to manage it without needing
		// the XML to explicitly prepend a uniquifier to parcel properties? But for now...
		for( ComponentProperty p : _properties.values() ) {
			if( p.getParcel().equals( forParcel ) && p.getName().endsWith("_$pName") ) {
				return p
			}
		}

		log.debug("Property Not Found in $this: pName '$pName'  forParcel '$forParcel'\n")
		return null
	}

	def listMacIdProperties() {
		return macids
	}

	def getPlaceableChildren(){
		return placeableChildren
	}
	
	public Map<String, Map<String,String>> getPlaceableChildrenMap(){
		return _placeableChildren
	}
	
	public String getPlaceableChildType(){
		return placeableChildType
	}

	/**
	 * Returns a placeable child component
	 * @param name the name of the component, the same as the key in _placeableChildren
	 * @return the matching DLComponent, or null if nothing matches
	 */
	public DLComponent getPlaceableChild(String name){
		if ( _placeableChildren.size() == 0 || !_placeableChildren.containsKey(name) ){
			return null
		}
		for(DLComponent c : this.getChildComponents()){
			if( c.getPropertyStringValue("child_id").equals(name) ){
				return c
			}
		}
		return null
	}

	public List<DLComponent> getStaticChildren(){
		def result = []
		for(def name : _placeableChildren.keySet()){
			def k = this.getPlaceableChild(name)
			if(k != null){
				result += k
			}
		}
		return result
	}

	public DLComponent getStaticParent(){
		List parents
		if(this.hasRole("staticchild")){
			parents = this.getParentsOfRole("staticchildplaceable")
		} else {
			return null
		}
		if(parents.size() == 1){
			return parents.first()
		} else {
			log.error("Component $this has ${parents.size()} static parents?")
			return null
		}
	}


	public List<DLComponent> getDynamicChildren(){
		def result = []
		for(def c : this.getChildComponents()){
			if(c.hasRole("dynamicchild")){
				result += c
			}
		}
		return result
	}


	public DLComponent getDynamicParent(){
		List parents
		if(this.hasRole("dynamicchild")){
			parents = this.getParentsOfRole("dynamicchildplaceable")
		} else {
			return null
		}
		if(parents.size() == 1){
			return parents.first()
		} else {
			log.error("Component $this has ${parents.size()} dynamic parents?")
			return null
		}
	}
	
//////////////////////////////////////

	/**
	 * Returns a List of all this component's producer's names.
	 * @return A List of the names of the producers of this component.
	 */
	ArrayList<String> listProducers(boolean includeConducerMembers = false) {
		//return _producers.collect{k,v -> k}
		def result = new ArrayList<String>()
		_producers.each{ k, v ->
			if ( (!v.hasConducer()) || includeConducerMembers ){
				result += k
			}
		}
		return result
	}

	public int getNumberOfProducers() {
		return ( _producers.size() )
	}

	public int getNumberOfConsumers() {
		return _consumers.size()
	}

	/**
	 * Returns a list of the component's producers that match a given datatype.
	 * @param datatype the association datatype in question.
	 * @return a List of producerIds of producers that have the requested datatype.
	 */
	ArrayList<String> producersOfType(String datatype) {
		return _producers.findAll{pId, p -> p.getDatatype().equals(datatype)}.collect{k,v -> k}
	}

	/**
	 * Finds a list of Producers of the given datatype
	 * @param datatype the datatype name to look for
	 * @return a List of Producer instances that are traits of this DLComponent
	 */
	ArrayList<Producer> getProducerObjectsOfType(String datatype) {
		def ids = this.producersOfType(datatype)
		return ids.collect { this.getProducer(it) }
	}

	/**
	 * Checks if the component has producers of a given datatype.
	 */
	boolean hasProducerOfType(String datatype) {
		boolean result = false
		if ( this.producersOfType(datatype).size() > 0 ) {
			result = true
		}
		return result
	}

	/**
	 * Returns the Producer object for this component's producer with the supplied name.
	 * Does no checking to make sure a producer with that name actually exists (so you should probably use some of the other producer inspecting methods first.
	 * @param id the Name of the producer to return.
	 * @return the Producer object with the supplied id (if one exists)
	 */
	Producer getProducer(String id) {
		return _producers[id]
	}

	ArrayList<Producer> getAllProducers(){
		return new ArrayList<Producer>(_producers.values())
	}
	
	boolean hasDefaultProducer(){
		boolean result = false
		for(Producer p : _producers.values()){
			if(p.defaultProducer){
				result = true
			}
		}
		return result
	}

	/**
	 * Checks if the component has an component consuming one of its producers of the given type.
	 * @param typeName the text name of the type to look for
	 * @return true if there is a consuming component of the supplied type
	 */
	boolean hasProducerConsumerOfType( String typeName ) {
		boolean result = false
		_producers.each{ id, pro ->
			pro.listConsumerComponents().each{ pair ->
				if ( pair.a.getType().equals(typeName) ){
					result = true
				}
			}
		}
		return ( result )
	}

	/**
	 * Returns a list of all Components that consume one of this Component's producers
	 */
	ArrayList<DLComponent> getProducerConsumers() {
		def result = []
		//println _producers
		_producers.each{ id, pro ->
            result.addAll( pro.mapConsumers().keySet() )
		}
		return result
	}

	/**
	 * Returns a list of all components that consume the producer on this component with the specified name.
	 * Returns an empty list if there is no such producer.
	 */
	ArrayList<DLComponent> getProducerConsumers(String producerName) {
		def result = []
		if (_producers.containsKey(producerName) ){
			def pro = _producers[producerName]
            result.addAll( pro.mapConsumers().keySet() )
		}
		return result
	}

	ArrayList<DLComponent> getProducerConsumersOfRole(String role) {
		def firstCut = this.getProducerConsumers()
		def filtered = []
		firstCut.each{ c ->
			if ( c.hasRole(role) ) {
				filtered += c
			}
		}
		return filtered
	}
	/**
	 * Gets a list of all dlcomponents associated with this component's consumers
	 */
	ArrayList<DLComponent> getConsumerProducers() {
		def result = []
		//println _producers
		_consumers.each{ id, con ->
            result.addAll( con.mapProducers().keySet() )
		}
		return result
	}


	/**
	 * Returns a list of all components that produce to the consumer on this component with the specified name.
	 */
	ArrayList<DLComponent> getConsumerProducers(String consumerName) {
		def result = []
		if (_consumers.containsKey(consumerName) ){
			def con = _consumers[consumerName]
            result.addAll( con.mapProducers().keySet() )
		}
		return result
	}

	/**
	 * Finds all components below this one on an association graph
	 */
	ArrayList<DLComponent> getAllConsumerProducers() {
		ArrayList<DLComponent> result = []
		ArrayList<DLComponent> belowMe = []

		belowMe += this.getConsumerProducers()
		result.addAll( belowMe )
		belowMe.each{ b ->
			result.addAll( b.getAllConsumerProducers() )
		}
		return result
	}

	ArrayList<DLComponent> getConducerProsumers(){
		def result = new ArrayList<DLComponent>()
		for(def conducer: _conducers.values()){
			result.addAll(conducer.getAssociatedComponents())
		}
		return result
	}

	/**
	 * Finds all components associated with this component, either via Producers or Consumers.
	 * @return a List of associated components. Empty if there are none.  This list will contain no duplicates; if another component is associated with this one "twice", such as across a conducer, it will only be listed once.
	 */
	ArrayList<DLComponent> getAllAssociatedComponents(){
		//return (this.getProducerConsumers() + this.getConsumerProducers()).flatten()
		def result = new HashSet<DLComponent>()
		result.addAll(this.getProducerConsumers())
		result.addAll(this.getConsumerProducers())
		result.addAll(this.getConducerProsumers())
		return new ArrayList<DLComponent>(result)
	}

	ArrayList<String> listConsumers(boolean includeConducerMembers = false) {
		//return _consumers.collect{k,v -> k}
		def result = new ArrayList<String>()
		_consumers.each{ k, v ->
			if ( (! v.hasConducer()) || includeConducerMembers ){
				result += k
			}
		}
		return result
	}

	ArrayList<String> consumersOfType(String datatype) {
		return _consumers.findAll{cId, c -> c.getDatatype().equals(datatype)}.collect{k,v -> k}
	}

	ArrayList<Consumer> getConsumerObjectsOfType(String datatype) {
		def ids = this.consumersOfType(datatype)
		return ids.collect { this.getConsumer(it) }
	}

	Consumer getConsumer(String id) {
		return _consumers[id]
	}

	ArrayList<Consumer> getAllConsumers(){
		return new ArrayList<Consumer>(_consumers.values())
	}

	DLObject getProducerObject(String id) {
		return _producers[id].getProducerObject()
	}

	DLObject getConsumerObject(String id) {
		return _consumers[id].getConsumerObject()
	}

	ArrayList<String> listConducers(){
		return new ArrayList<String>(_conducers.keySet())
	}

	Conducer getConducer(String id) {
		return _conducers[id]
	}

	ArrayList<Conducer> getAllConducers(){
		return new ArrayList<Conducer>(_conducers.values())
	}
	
	// add new producer to DLComponent
	public void addProducer(String id, String dataType, String name, String objectPath){
		//'$dbtable/properties[name="fields"]/children[properties[name="name"][value="test"]]'
		_producers[id] = new Producer(this, id, dataType , name, objectPath, myContext)
	}


	public void addProducer(Producer newProducer){
		_producers[newProducer.getId()] = newProducer
	}

	public void addConsumer(Consumer newConsumer){
		_consumers[newConsumer.getId()] = newConsumer
	}

	public List<ComponentBinding> getBindingsForProp( String propName ) {
		// Currently not used much. Could cache the results if that ever changes.
		List<ComponentBinding> result = []
		for( def b : _varBindings ) {
			if( b.listVariables().contains( propName ) ) {
				result.add( b )
			}
		}
		return result
	}

	//add Traits:

	public java.util.Collection<ComponentProperty> getAllProperties(){
		return _properties.values()
	}

	/**
	 * Returns a set of all Component Traits currently in this component.
	 * This will include the members of any active parcels.
	 * @return a Set of Component Traits.
	 */
	public Set<ComponentTrait> getAllTraits(){
		def result = new HashSet<ComponentTrait>()
		result.addAll(_properties.values())
		result.addAll(_producers.values())
		result.addAll(_conducers.values())
		result.addAll(_consumers.values())
		result.addAll(_managedObjects)
		result.addAll(_varBindings)
		return result
	}

	public ComponentTrait getTraitWithName(String name){
		return this.getAllTraits().find {it.getTraitId().equals(name)}
	}

	public ComponentParcel getParcelWithName(String name){
		return _varBindings.find {it.getTraitId().equals(name)}
	}

	public Set<ComponentParcel> getAllParcels() {
		Set<ComponentParcel> parcels = [] as Set
		for( def vb : _varBindings ) {
			if( vb instanceof ComponentParcel ) {
				parcels += vb
			}
		}
		return parcels
	}

	public Set<ComponentParcel> getEnabledParcels() {
		Set<ComponentParcel> parcels = [] as Set
		for( def vb : _varBindings ) {
			if( vb instanceof ComponentParcel && vb.isEnabled()) {
				parcels += vb
			}
		}
		return parcels
	}

	public Set<ComponentParcel> getEnabledParcelsInRole( String role ) {
		Set<ComponentParcel> parcels = [] as Set
		for( def vb : _varBindings ) {
			if( vb instanceof ComponentParcel && vb.isEnabled() && vb.hasRole( role ) ) {
				parcels += vb
			}
		}
		return parcels
	}

	public Set<ComponentParcel> getEnabledParcelsInRoles( java.util.Collection<String> roles ) {
		Set<ComponentParcel> parcels = [] as Set
		for( def vb : _varBindings ) {
			if( vb instanceof ComponentParcel && vb.isEnabled() && vb.hasRoles( roles ) ) {
				parcels += vb
			}
		}
		return parcels
	}

	public void addRole(String role){
		classes.add(role)
	}

	public void removeRole(String role){
		classes.remove(role)
	}

	public void addRoot(String asName, DLObject root){
		this._roots = null
		this._objects[asName] = root
		this.managedObjects.add(root)

		myContext.getVariables().declareVariable(asName, root)

	}

	public void removeRoot(String asName){
		this._roots = null
		this.managedObjects.remove(this._objects[asName])
		this._objects[asName] = null
		this._objects.remove(asName)
		myContext.getVariables().undeclareVariable(asName)
	}

	public void detatchRoot(DLObject formerRoot){
		def asName
		this._objects.each{ k, v ->
			if(v == formerRoot){
				asName = k
			}
		}
		if (asName == null){
			throw new IllegalArgumentException("No such root to detatch!")
		}
		this._roots = null
		this._objects[asName] = null
		this._objects.remove(asName)
		myContext.getVariables().undeclareVariable(asName)
	}


	public void addTrait(ComponentProperty property){
		_properties[property.getTraitId()] = property
		_varMap[property.getTraitId()] = []

		flushSortedPropsCache()
	}

	public void removeTrait(ComponentProperty property){
		_properties[property.getTraitId()] = null
		_properties.remove(property.getTraitId())
		_varMap[property.getTraitId()] = null
		_varMap.remove(property.getTraitId())

		flushSortedPropsCache()
	}

	public void addTrait(Docking docking){
		docking.set()
		this.dockings.add(docking)
	}

	public void removeTrait(Docking docking){
		docking.clear()
		this.dockings.remove(docking)
	}

	public void addTrait(Producer newProducer){
		_producers[newProducer.getId()] = newProducer
		getModel().updateComponentParts(this)
	}

	public void removeTrait(Producer deadToMe){
		//detatch all consumers
		for( Pair<DLComponent, String> consumerHookup : deadToMe.listConsumerComponents() ) {
			consumerHookup.a.unassociate( consumerHookup.b, this, deadToMe.getId() )
		}

		_producers[deadToMe.getId()] = null
		_producers.remove(deadToMe.getId())
		getModel().removeComponentParts(this, deadToMe)
	}

	public void addTrait(Consumer newConsumer){
		_consumers[newConsumer.getId()] = newConsumer
		getModel().updateComponentParts(this)
	}

	public void removeTrait(Consumer deadToMe){
		//detatch all producers
		for( Pair<DLComponent, String> producerHookup : deadToMe.listProducerComponents() ) {
			this.unassociate( deadToMe.getId(), producerHookup.a, producerHookup.b )
		}

		_consumers[deadToMe.getId()] = null
		_consumers.remove(deadToMe.getId())
		getModel().removeComponentParts(this, deadToMe)
	}

	public void addTrait(Conducer newConducer){
		_conducers[newConducer.getId()] = newConducer
		_producers[newConducer.getProducer().getId()] = newConducer.getProducer()
		_consumers[newConducer.getConsumer().getId()] = newConducer.getConsumer()
		getModel().updateComponentParts(this)
	}

	public void removeTrait(Conducer deadToMe){
		//detatch all conducers
		for( Pair<DLComponent, String> consumerHookup : deadToMe.producer.listConsumerComponents() ) {
			consumerHookup.a.unassociate( consumerHookup.b, this, deadToMe.producer.getId() )
		}

		for( Pair<DLComponent, String> producerHookup : deadToMe.consumer.listProducerComponents() ) {
			this.unassociate( deadToMe.consumer.getId(), producerHookup.a, producerHookup.b )
		}

		_consumers[deadToMe.getConsumer().getId()] = null
		_consumers.remove(deadToMe.getConsumer().getId())

		_producers[deadToMe.getProducer().getId()] = null
		_producers.remove(deadToMe.getProducer().getId())

		_conducers[deadToMe.getId()] = null
		_conducers.remove(deadToMe.getId())

		getModel().removeComponentParts(this, deadToMe)
	}

	public void addTrait(DLBinding binding){
		binding.belongsTo = this
		for(String varName : binding.listVariables()){
			//println "$type: constructing binding $varName "
			try {
				_varMap[varName] += binding
			} catch (NullPointerException e){
				log.fatal("Variable '$varName' does not exist for component type $type", "message")
			}
		}
		_varBindings += binding
	}

	public void removeTrait( ComponentParcel parcel ) {
		parcel.dematerialize()
		removeTrait( (DLBinding) parcel )
	}

	public void removeTrait(DLBinding binding){
		for(String varName : binding.listVariables()){
			//println "$type: constructing binding $varName "
			/*
			try {
				_varMap[varName] -= binding
			} catch (NullPointerException e){
				log.fatal("Variable '$varName' does not exist for component type $type", "message")
			}
			*/
			if (_varMap.containsKey(varName)){
				_varMap[varName] -= binding
			}
		}
		_varBindings -= binding
	}

	public void removeTrait(DLObject o){
		//log.debug("NO SPEAK")

		if(this.getRoots().contains(o)){
			def root
			_objects.each{ k, v ->
				if(v == o){
					//this.removeRoot(k)
					root = k
				}
			}
			this.removeRoot(root)
		} else{
			managedObjects.remove(o)
			_objects.remove(o)
		}

		//remove from parents
		for(def parent : o.getParents()){
			for(ChildLink prop : parent.getPropertiesByType('childlink') ){
				if(prop.reftypes.contains(o.type)){
					log.debug("Removing child object '${o.type}' from my object '${parent.type}' property '${prop.name}'")
					prop.removeChild(o)
				}
			}
		}

		omodel.remove(o)
		getModel().removeComponentParts(this, o)
	}

	public void addPartialComponent(boolean shouldBeThere, String partialType){
		log.info("addPartialComponent: $shouldBeThere $partialType", "statusbar")

		if(shouldBeThere){
			//add it
			def partial = cmodel.newComponent(partialType)
			partial.listRoles().each{
				this.addRole(it)
			}
			partial.displaySettings.each{ k, v ->
				this.displaySettings[k] = v
			}
			partial.listProperties().each{ pid ->
				if(!this.hasProperty(pid)){
					ComponentProperty p = partial.getComponentProperty(pid)
					if(!p.isSecret){
						this.addTrait(p)
					}

				}
			}
			def newBindings = []
			partial._varBindings.each{
				this.addTrait(((DLBinding)it))
				newBindings += it
			}

			partial.listRootNames().each{ rn ->
				def root = partial.getRoot(rn)
				this.addRoot(rn, root)
			}
			//this.managedObjects.addAll(partial.getManagedObjects())




			//can't use associationcontroller to hook them up, 'cus the scenegraph FREAKS OUT




			//re-fire all bindings at this point?
			for(DLBinding binding : newBindings){
				binding.eval(getPropertyValueMap(binding.listVariables()))
			}

			cmodel.remove(partial)
		} else {
			//remove it
			def partial = cmodel.newComponent(partialType)
			partial.listRoles().each{
				this.removeRole(it)
			}

			partial.displaySettings.each{ k, v ->
				this.displaySettings.remove(k)
			}


			partial.listProperties().each{ pid ->
				ComponentProperty p = partial.getComponentProperty(pid)
				if(!p.isSecret){
					this.removeTrait(p)
				}
			}

			partial._varBindings.each{ this.removeTrait(((DLBinding)it)) }

			partial.listRootNames().each{ rn ->
				this.removeRoot(rn)
			}


			//this doesn't *technically* work
			//this.managedObjects.removeAll(partial.getManagedObjects())


			cmodel.remove(partial)
		}
	}

	public void setDocking(String sFrom, String sTo){
		log.debug("docking from $sFrom to $sTo")
		//String sFrom = pbXml.@from.text()
		//String sTo = pbXml.@to.text()
		Property pFrom = myContext.getValue(sFrom)
		//if(pFrom instanceof Dockingpoint) {
		assert pFrom instanceof Dockingpoint
		DLObject pTo = myContext.getValue(sTo)
		pFrom.setReferrent(pTo)
	}

	public boolean hasDocking( String sFrom, String sTo ) {
		for( Docking d : dockings ) {
			if( ( d.from == sFrom ) && ( d.to == sTo ) ) {
				return true
			}
		}
		return false
	}

	public boolean hasParcel( String name ) {
		for( def vb : _varBindings ) {
			if( ( vb instanceof ComponentParcel ) && ( vb.name == name ) ) {
				return true
			}
		}
		return false
	}

	ArrayList<Conducer> getConducersThatMatch(Conducer omega){
		def result = []
		_conducers.values().each{ c->
			if ( c.canProsume(omega) ){
				result += c
			}
		}
		return result.sort();
	}

	public boolean isAssociatiable(){
		int associations = listConsumers().size() + listProducers().size() + listConducers().size()
		if(associations ==0)
			return false
		else
			return true
	}

	/**
	 * Returns all the components that manage the same objects as this component. In other words, this returns the
	 * anchor plus all proxies.
	 *
	 * @return
	 */
	public java.util.Collection<DLComponent> getCollectiveDrones() {
		java.util.Collection<DLComponent> drones = this.getProxies()
		drones.add( this )
		return drones
	}

	void associate(String consumerId, DLComponent producer, String producerId) {
		log.trace("Adding association: $this, $consumerId --> $producer, $producerId")

		Consumer consumer = _consumers[consumerId]
		if(consumer == null) {
			log.error("Can't lookup consumer ID '$consumerId' on " + this)
			return
		}

		// We need to check existing associations across the anchor and its proxies, not just this and the producer arg.
		List<DLComponent> consumerDrones = this.getCollectiveDrones()
		List<DLComponent> producerDrones = producer.getCollectiveDrones()

		for( DLComponent conDrone : consumerDrones ) {
			Consumer c = conDrone.getConsumer( consumerId )

			// Disconnect an existing association if the consumer is not a collection.
			if( !c.isCollection() ) {
				for( Pair<DLComponent, String> oip : c.listProducerComponents() ) {
					log.debug("Removing old association from non-Collective Consumer")
					conDrone.unassociate( consumerId, oip.a, oip.b )
				}
			}

			// Disconnect an existing association if the producer component already has the same anchor/proxy connected to this component.
			for( Pair<DLComponent, String> oip : c.listProducerComponents() ) {
				if( ( oip.b == producerId ) && producerDrones.contains( oip.a ) ) {
					log.debug("Removing duplicate association: Consumer${conDrone}  Producer${oip.a}")
					conDrone.unassociate( consumerId, oip.a, oip.b )
				}
			}
		}

		for( DLComponent proDrone : producerDrones ) {
			Producer p = proDrone.getProducer( producerId )

			// Disconnect an existing association if the producer is exclusive.
			if( p.isExclusive() ) {
				for( Pair<DLComponent, String> oip : p.listConsumerComponents() ) {
					log.debug("Removing old association from Exclusive Producer")
					oip.a.unassociate( oip.b, proDrone, producerId )
				}
			}

			// Disconnect an existing association if this component already has the same anchor/proxy connected to the producer.
			for( Pair<DLComponent, String> oip : p.listConsumerComponents() ) {
				if( ( oip.b == consumerId ) && consumerDrones.contains( oip.a ) ) {
					log.debug("Removing duplicate association: Producer$proDrone  Consumer${oip.a}")
					oip.a.unassociate( oip.b, proDrone, producerId )
				}
			}
		}

		// Finally! Do the work we came here to do.
		consumer.addProducer(producer, producerId)
		cmodel.fireAssociationAdded(producer, producerId, this, consumerId)
	}

	void unassociate(String consumerId, DLComponent producer, String producerId) {
		log.trace("Removing association: $consumerId, $producer, $producerId")
		_consumers[consumerId].removeProducer(producer, producerId)

		Producer prod = producer.getProducer(producerId)
		if ( prod.hasConducer() ){
            prod.getConducer().unProsume( this.getConsumer( consumerId ).getConducer() )
		}
		cmodel.fireAssociationRemoved(producer, producerId, this, consumerId)
	}


	void associate( Conducer alpha, DLComponent other, Conducer omega) {

		// We need to check existing associations across the anchor and its proxies, not just this and the producer arg.
		List<DLComponent> alphaDrones = this.getCollectiveDrones()
		List<DLComponent> omegaDrones = other.getCollectiveDrones()

		for( DLComponent alphaDrone : alphaDrones ) {
			Conducer conducer = alphaDrone.getConducer( alpha.getId() )
			Consumer consumer = conducer.getConsumer()
			Producer producer = conducer.getProducer()

			// Disconnect if the alpha Conducer fits either condition:
			//     - Consumer is not a collection and already has a producer.
			//     - Producer is exclusive and already has a consumer.
			if(( !consumer.isCollection() && !consumer.listProducers().isEmpty() ) ||
			   ( producer.isExclusive() && !producer.listConsumers().isEmpty() )) {
				for( Conducer mate : conducer.getProsumers() ) {
					log.debug("Removing old association from non-Collective Conducer")
					alphaDrone.unassociate( conducer, mate.getOwner(), mate )
				}
			}

			// Disconnect an existing association if the omega component already has the same anchor/proxy connected to this component.
			for( Pair<DLComponent, String> oip : conducer.getProsumerComponents() ) {
				if( ( oip.b == omega.getId() ) && omegaDrones.contains( oip.a ) ) {
					log.debug("Removing duplicate association: Alpha$alphaDrone  Omega${oip.a}")
					alphaDrone.unassociate( conducer, oip.a, oip.a.getConducer( omega.getId() ) )
				}
			}
        }

		for( DLComponent omegaDrone : omegaDrones ) {
			Conducer conducer = omegaDrone.getConducer( omega.getId() )
			Consumer consumer = conducer.getConsumer()
			Producer producer = conducer.getProducer()

			// Disconnect if the omega Conducer fits either condition:
			//     - Consumer is not a collection and already has a producer.
			//     - Producer is exclusive and already has a consumer.
			if(( !consumer.isCollection() && !consumer.listProducers().isEmpty() ) ||
			   ( producer.isExclusive() && !producer.listConsumers().isEmpty() )) {
				for( Conducer mate : conducer.getProsumers() ) {
					omegaDrone.unassociate( conducer, mate.getOwner(), mate )
				}
			}

			// Disconnect an existing association if the alpha component already has the same anchor/proxy connected to this component.
			for( Pair<DLComponent, String> oip : conducer.getProsumerComponents() ) {
				if( ( oip.b == alpha.getId() ) && alphaDrones.contains( oip.a ) ) {
					log.debug("Removing duplicate association: Alpha${oip.a}  Omega${omegaDrone}")
					omegaDrone.unassociate( conducer, oip.a, oip.a.getConducer( alpha.getId() ) )
				}
			}
        }

		//associate this.alpha.consumer -- other.omega.producer
		//associate this.alpha.producer -- other.omega.consumer
		alpha.prosume(omega)
		omega.prosume(alpha)

		this.associate( alpha.getConsumer().getId(), other, omega.getProducer().getId()  )
		other.associate( omega.getConsumer().getId(), this, alpha.getProducer().getId()  )
	}

	void unassociate( Conducer alpha, DLComponent other, Conducer omega ) {
		log.info("Removing conducer: $alpha, $other, $omega")

		this.unassociate( alpha.getConsumer().getId(), other, omega.getProducer().getId() )
		other.unassociate( omega.getConsumer().getId(), this, alpha.getProducer().getId() )
		
		alpha.unProsume( omega )
		omega.unProsume( alpha )
	}

	void clearProducers() {
		_producers.each{ id, p ->
			p.listConsumerComponents().each{ oip ->
				oip.a.unassociate(oip.b, this, id)
			}
		}
	}

	void clearConsumers() {
		_consumers.each{id, c ->
			c.listProducerComponents().each{ oip ->
				unassociate(id, oip.a, oip.b)
			}
		}
	}

	void clearConducers(){
		_conducers.each{ id, d->

			d.producer.listConsumerComponents().each{ oip ->
				oip.a.unassociate(oip.b, this, id)
			}

			d.consumer.listProducerComponents().each{ oip ->
				unassociate(id, oip.a, oip.b)
			}

		}
	}

//////////////////////////////////////

	boolean hasProperty( String pName, ComponentParcel forParcel ) {
		return hasProperty( pName, forParcel.getTraitId() )
	}

	// Effing Groovy now needs this to handle ambiguity created by null arguments *sigh* #dynamic!
	boolean hasProperty( String pName, def nullarg ) {
		return hasProperty( pName )
	}

	/* forParcel is only necessary in cases where generic code is written to expect certain property names, but Artisan has messed that up. */
	boolean hasProperty( String pName, String forParcel = null ) {
		if( forParcel == null || forParcel.isEmpty() ) {
			// The standard easy pre-Artisan case.
			return _properties.containsKey( pName )
		}

		// Looks like we need to go the painful route and find one from a parcel. You see... to avoid duplicate property
		// names in components using Artisan, we prepended something like a namespace to the property name. This breaks
		// a whole bunch of code that has hardcoded property names. This really needs to be rethought since this sucks.
		// Maybe internally prepend the parcel name and modify everything in DLComponent to manage it without needing
		// the XML to explicitly prepend a uniquifier to parcel properties? But for now...
		for( String p : listProperties( forParcel ) ) {
			if( p.endsWith("_$pName") ) {
				return true
			}
		}

		return false
	}

	/**
	 * Checks for the presence of a property with the specified display name.
	 * Including producers & consumers.
	 */
	boolean hasPropertyNamed(String name) {
		for( Map.Entry e : _properties.entrySet() ){
			if ( e.getValue().getDisplayName().toLowerCase().trim().equals( name.toLowerCase().trim() ) ) {
				return true
			}
		}
		for( Map.Entry e : _producers.entrySet() ){
			if ( e.getValue().getName().toLowerCase().trim().equals( name.toLowerCase().trim() ) ) {
				//println "$this has $name!"
				return true;
			}
		}
		for( Map.Entry e : _consumers.entrySet() ){
			if ( e.getValue().getName().toLowerCase().trim().equals( name.toLowerCase().trim() ) ) {
				return true;
			}
		}
		//support for KEYs and DLIDs
		if(name.equals(DLIDS_NAME) || name.equals(KEYS_NAME)){
			return true
		}
		return false;
	}

	/**
	 * Returns the contents of a property with the specified display name.
	 * Including producers & consumers, the value being a list of the associated component names (if any).
	 */
	def getPropertyNamedValue(String name) {
		def result
		_properties.each{ k, v ->
			if ( v.getDisplayName().toLowerCase().trim().equals( name.toLowerCase().trim() ) ) {
				result = v.value

                // Make sure it is formatted correctly for the locale settings.
                // Probably shouldn't be here since this is technically the model and localization should be in the view
                // but this is used in the search and, short of major refactoring, this is the most efficient way.
                if( v.isUnitConvertable() && result != null ) {
                    result = Localizer.convert( result, v.dimensionName )
                }

                if( Localizer.isLocalizable( v ) ) {
                    result = Localizer.format( result )
                }

				return
			}
		}
		//_producers.each{ id, prod ->
		for( Producer prod : _producers.values() ){
			if ( prod.getName().toLowerCase().trim().equals( name.toLowerCase().trim() ) ) {
				def links =  ""
				prod.listConsumerComponents().each{ pair ->
					links += pair.a.getName() + ", "
				}
				result = links
				//return
			}
		}
		//_consumers.each{ id, con ->
		for( Consumer con : _consumers.values() ){
			if ( con.getName().toLowerCase().trim().equals( name.toLowerCase().trim() ) ) {
				def links =  ""
				con.listProducerComponents().each{ pair ->
					links += pair.a.getName() + ", "
				}
				result = links
				//return
			}
		}

		//include keys and dlids
		if (name.equals(DLIDS_NAME)){
			def dlids = []
			this.getManagedObjects().each{ dlids += it.getDlid().toString() }
			result = dlids.toString()
		}

		if(name.equals(KEYS_NAME)){
			def keys = []
			this.getManagedObjects().each{ if(it.hasKey()){ keys += it.getKey().toString()} }
			if ( keys.size() == 0 ){
				result = ""
			} else {
				result = keys.toString()
			}
		}

		return (result)
	}
	
	String getPropertyNameFromDisplayName(String displayName){
		String result = null
		_properties.each{ k, v ->
			if( v.getDisplayName().equals(displayName) ){
				result = k
			}
		}
		return result
	}

	def getPropertyValue( String pName, ComponentParcel forParcel ) {
		getPropertyValue( pName, forParcel.getTraitId() )
	}

	// Effing Groovy now needs this to handle ambiguity created by null arguments *sigh* #dynamic!
	def getPropertyValue( String pName, def nullarg ) {
		return getPropertyValue( pName )
	}

	def getPropertyValue( String pName, String forParcel = null ) {
		return getComponentProperty( pName, forParcel )?.value
	}

	/**
	 * Shortcut method to return the display name of the given property
	 * @param pName
	 * @return
	 */
	String getPropertyDisplayName( String pName, String forParcel = null ) {
		return getComponentProperty( pName, forParcel )?.getDisplayName()
	}

	// Effing Groovy now needs this to handle ambiguity created by null arguments *sigh* #dynamic!
	String getPropertyDisplayName( String pName, def nullarg ) {
		return getPropertyDisplayName( pName )
	}

	String getPropertyDisplayName( String pName, ComponentParcel forParcel ) {
		return getPropertyDisplayName( pName, forParcel.getTraitId() )
	}

	protected def getPropertyValueMap(def varNames) {
		// TODO: cache results
		def map = [:]
		for(String varName: varNames) {
			if(_properties.containsKey(varName))
				map[varName] = _properties[varName].getValue()
			else if(_propertyRefs.containsKey(varName))
				map[varName] = _propertyRefs[varName].getValue()
			else
				log.error("Variable '$varName' could not be found!")
		}
		return map
	}

	DLComponent setPropertyValue(String pName, def pValue) {
        def oldVal = null
		boolean iStartedIt = false
		try {
            oldVal = _properties[pName].value
			_properties[pName].value = pValue
			def newVal = _properties[pName].value // To get the converted value

			// Process the currently active parcels first so they can deactivate if needed.
			_varMap[pName].each{ binding ->
				if( ( binding instanceof ComponentParcel ) && binding.isEnabled() ) {
					binding.eval(getPropertyValueMap(binding.listVariables()))
				}
			}

			_varMap[pName].each{ binding ->
				binding.eval(getPropertyValueMap(binding.listVariables()))
			}
			if(_properties[pName].badge && pValue){
				this.setDisplaySetting(_properties[pName].badge, _properties[pName].badgeShape)
			} else if(_properties[pName].badge && !pValue) {
				this.setDisplaySetting(_properties[pName].badge, null)
				this.displaySettings.remove(_properties[pName].badge)
			}
			
			//any synchronized children?
			if(synchronizedProperties.containsKey(pName)){
				log.trace("SP contains $pName")
				log.trace("my children are ${getChildComponents()}")
				def childProperty = synchronizedProperties.get(pName)
				this.getChildComponents().each{ child ->
					if(child.hasProperty(childProperty)){
						log.trace("setting $childProperty to $pValue on $child")
						child.setPropertyValue(childProperty, pValue)
					}
				}
			}

			// One change may now trigger others due to all the listeners, like the Room area calculations. Treat them all
			// as a single operation so that a single undo action works without neededing to sprinkle start/stopOperation everywhere.
			if( !CentralCatalogue.getUndoBuffer().operationInProgress() ) {
				CentralCatalogue.getUndoBuffer().startOperation()
				iStartedIt = true
			}

			firePropertyChanged(pName, oldVal, newVal)
		} catch(Exception e) {
			log.error("Exception setting $pName property value " + (pName.contains('image') ? ((pValue == null) ? "null" : '<image>') : "as '$pValue'!") )
			if( !(e instanceof IllegalArgumentException) ) {
				log.error( "$this: ${log.getStackTrace(e)}" )
			}
            if( oldVal != null ) {
                _properties[pName].value = oldVal
            }
			if( iStartedIt ) CentralCatalogue.getUndoBuffer().rollbackOperation()
			throw e
		} finally {
			if( iStartedIt ) CentralCatalogue.getUndoBuffer().finishOperation()
		}

		return this // Yay chaining interfaces
    }

	public void setObjectSettingByShotgun( String objectType, String keySetting, String valSetting,
	                                       String encodedValues, String defaultVal = "" ) {
		// Parse the value string
		HashMap<String,String> table = [:]
		if( !encodedValues.isEmpty() ) {
			for( String encodedKeyVal : encodedValues.split(";") ) {
				String[] keyval = encodedKeyVal.split(",");
				if( keyval.length == 2 ) {
					table.put( keyval[0], keyval[1] );
				}
			}
		}

		// A default value in the encoded string takes priority over the one passed in.
		if( table.containsKey('default') ) {
			defaultVal = table.get('default')
		}

		// Set the properties
		for( DLObject o : getManagedObjectsOfType( objectType ) ) {
			Setting setting = o.getObjectSetting( keySetting );
			String val = table.get( setting.getValue().toString() );
			if( val == null ) {
				val = defaultVal
			}
			o.getObjectSetting( valSetting ).setValue( val );
		}
	}

	/**
	 * Helps minimize prop change events during things like drags or other multiple prop changes. It's a quick hacky
	 * way of handling it without needing a new interface. Still better than some of the other hacks, like setting a
	 * value to the same value after a drag just to trigger a prop change.
	 */
	private boolean isMetamorphosizing = false
	public boolean isMetamorphosizing() {
		return isMetamorphosizing
	}

	public void metamorphosisStarting() {
		isMetamorphosizing = true
		firePropertyChanged('metamorphosizing', false, true )
	}

	public void metamorphosisFinished() {
		isMetamorphosizing = false
		firePropertyChanged('metamorphosizing', true, false )
	}

	public void quickName(String newName){
		_properties['name'].value = newName
	}

	/**
	 * Copies property values from another component into this one.
	 * @param source another DLComponent to copy values from
	 * @param includeXAndYAndName If false, don't copy the X, Y, or Name properties.  If true, copy everything.
	 */
	public void copySettingsFrom( DLComponent source, Boolean includeXAndYAndName ){
		for ( String pname : source.listProperties() ){
			//String displayName = source.getComponentProperty(pname).getDisplayName()
			//println "checking property $pname "
			if ( ! this.hasProperty(pname) ){
				//println "nothing named that, continue"
				continue;
			}

			if (!this.getComponentProperty(pname).isEditable()){
				continue
			}

			if ( ! includeXAndYAndName && ( pname.equalsIgnoreCase("x") || pname.equalsIgnoreCase("y") || pname.equalsIgnoreCase("name") ) ){
				//println "exclusion list, continue"
				continue;
			}

			//also skip macids
			if ( this.listMacIdProperties().contains( pname ) ){
				continue
			}

			//println "copy $pname!"
			Object val = source.getPropertyValue(pname)
			this.setPropertyValue(pname, val)
		}
	}


	private void firePropertyChanged(String pName, def oldVal, def newVal) {
		propertyChangeListeners.clone().each{ who, listener ->
			try {
				listener.call(this, pName, oldVal, newVal)
			} catch(Exception e) {
				log.error("Exception delivering property change event: ${e.getMessage()}")
				throw e
			}
		}
	}

	public String getPropertyStringValue( String pName, ComponentParcel forParcel ) {
		return getPropertyStringValue( pName, forParcel.getTraitId() )
	}

	// Effing Groovy now needs this to handle ambiguity created by null arguments *sigh* #dynamic!
	public String getPropertyStringValue( String pName, def nullarg ) {
		return getPropertyStringValue( pName )
	}

	public String getPropertyStringValue( String pName, String forParcel = null ) {
		return getPropertyValue( pName, forParcel)?.toString()
	}

	boolean addChild( DLComponent childComponent, java.util.Collection<DLObject> objectsToSkip = null ) {
		//TODO: make sure the component can actually be a child before trying to add it.  But the process is the same...
		log.trace("entering addChild($childComponent) me " + this)

        if(childComponent.isChildComponentOf(this)){
            log.trace("$childComponent is already a child of " + this)
            return
        }

		int childed = 0
		childComponent.roots.each { childObject ->
			_objects.each{ k, obj ->
				for( Property prop : obj.getPropertiesByType('childlink') ) {
					if(childObject.type in prop.reftypes) {

						// A bit of a hack to allow an explicit exclusion when childing objects since there are cases
						// where natural child selection may grab objects that it shouldn't.
						if( ( objectsToSkip != null ) && objectsToSkip.contains( childObject ) ) {
							log.debug("Skipping child object '${childObject.type}'")
							continue
						}

						log.debug("Childing object '${childObject.type}' to my object '${obj.type}' property '${prop.name}'")
						prop.addChild(childObject)
						childed++
					}
				}
			}
		}
		if(childed > 0) {
			// Set the child's drawing to mine just in case it has moved drawings.
			if( !childComponent.hasRole( ComponentRole.DRAWING ) ) {
				childComponent.setDrawing( this.getDrawing() )
			}

			if(childComponentCache == null){
				childComponentCache = new HashSet<DLComponent>()
				childComponentCache.add(childComponent)
			}else{
				childComponentCache.add(childComponent)
			}
			cmodel.fireChildAdded(this, childComponent)
		} else {
			log.warn("No children added!  ${childComponent.type}")
		}
		log.trace("exiting addChild($childComponent) me " + this)

		return (childed > 0)
	}

	void removeChild(DLComponent childComponent) {
		log.trace("entering removeChild($childComponent) me " + this)
		int removed = 0
		if( this.getChildComponents().contains(childComponent) ){
			for(DLObject childObject : childComponent.roots){
				for(DLObject obj : _objects.values()){
					for(ChildLink prop : obj.getPropertiesByType('childlink') ){
						//if(prop.reftypes.contains(childObject.type)){  FAILS during upgrade when reftypes have changed.
						if(prop.hasChild(childObject)){
							log.debug("Removing child object '${childObject.type}' from my object '${obj.type}' property '${prop.name}'")
							prop.removeChild(childObject)
							removed++
						}
					}
				}
			}
			if(removed > 0) {
				if(childComponentCache != null) {
					childComponentCache.remove(childComponent)
				}
				// Don't clear the drawing here. Too many things depend on it. Cleared when being childed instead.
				// childComponent.clearDrawing()
				cmodel.fireChildRemoved(this, childComponent)
			} else {
				log.error("Couldn't find any child objects to remove!")
			}
		} else {
			log.warn("Component '${childComponent.type}' was never a child!")
		}
		log.trace("exiting removeChild($childComponent) me " + this)
	}

	/**
	 * Stores a cache of child components for this component so the list doesn't have to be rebuilt every time.
	 * Note that as of Jupiter, this is a SET, not a LIST, which means that sort order is not maintained.
	 * Which shouldn't matter.
	 *
	 */
	protected Set<DLComponent> childComponentCache = null
	ArrayList<DLComponent> getChildComponents() {
		// O: why does 'this' have to be filtered out
		// A: 'this' has to be filtered out because getOwner() looks at binding posts as well as root objects
//		if(childComponentCache == null)
//			childComponentCache = getChildObjects().collect{ child -> cmodel.getOwner(child) }.findAll{ it != null && it != this }.unique()

		if(childComponentCache == null){
			childComponentCache = new HashSet<DLComponent>()
			getChildObjects().each{ child ->
				def c = cmodel.getOwner(child)

				// *sigh* filter out staticchildplaceable. They create a circular reference because of wsnnodes being a child of a rack.
				if (c != null && c != this && !( this.hasRole('staticchild') && c.hasRole('staticchildplaceable') ) ) {
					childComponentCache.add(c)
				}
			}
		}
		//todo: refactor this method to return the set itself and all methods that call this to deal with sets, not lists
		return new ArrayList<DLComponent>(childComponentCache)
	}


    /**
     * Returns a collection of all direct child components with a given role.
     * @param childRole the role to check child components for.
     * @return a List of DLComponents that have the specified role that are children of this DLComponent.
     */
    List<DLComponent> getChildrenOfRole( String role ) {
        List<DLComponent> kidsWithRole = new ArrayList<DLComponent>()

        for( DLComponent child : getChildComponents() ) {
            if( child.hasRole( role ) ) {
                kidsWithRole.add( child )
            }
        }
        return kidsWithRole
    }

	List<DLComponent> getChildrenOfType( String type ) {
		List<DLComponent> kidsWithType = new ArrayList<DLComponent>()

		for( DLComponent child : getChildComponents() ) {
			if( child.getType().equals( type ) ) {
				kidsWithType.add( child )
			}
		}
		return kidsWithType
	}


    /**
	 * Returns true if 'this' is a child of component p.
	 * @param p The DLComponent to test for parenthood.
	 * @return True if P is a parent of this.
	 */
	boolean isChildComponentOf( DLComponent p ) {
		def allParents = this.getParentComponents()
		return ( p in allParents )
	}


	ArrayList<DLComponent> getParentComponents() {
		//return getParentObjects().collect{ parent -> cmodel.getOwner(parent) }.unique()
/*
		def temp = getParentObjects().collect{ parent -> cmodel.getOwner(parent) }
		Set<DLComponent> sett = new HashSet<DLComponent>(temp)

		ArrayList<DLComponent> finalList = new ArrayList<DLComponent>(sett)
		return finalList
*/


		Set<DLComponent> sett = new HashSet<DLComponent>()
		for(DLObject o: getParentObjects() ){
			def p = cmodel.getOwner(o)

			// *sigh* filter out staticchild. They create a circular reference because of wsnnodes being a child of a rack.
			if ( p && p != this && !( this.hasRole('staticchildplaceable') && p.hasRole('staticchild') ) ) {
				sett.add( p )
			}
		}

		return new ArrayList<DLComponent>(sett)
		

	}

    /**
	 * Returns a collection of all parent components that are logical component containers, such as a zone.
	 * @return a List of DLComponents that are containers and are parents of this DLComponent.
	 */
	List<DLComponent> getParentContainers() {
		def allParents = this.getParentComponents()
		def containers = []
		allParents.each{ p ->
			if( p.isContainer() ) {
				containers += p
			}
		}
		return containers
	}


	/**
	 * Returns a collection of all parent components with a given role.
	 * @param parentRole the role to check parent components for.
	 * @return a List of DLComponents that have the specified role that are parents of this DLComponent.
	 */
	def getParentsOfRole(String parentRole) {
		def allParents = this.getParentComponents()
		def withRole = []
		allParents.each{ p ->
			if ( p.hasRole(parentRole)  ) {
				withRole += p
			}
		}
		return withRole
	}

	public boolean hasParentOfRole( String parentRole ) {
		return !getParentsOfRole( parentRole ).isEmpty()
	}

    /**
     * returns a collection of all parent components with a given type
     * @param parentType
     * @return
     */
    List<DLComponent> getParentsOfType(String parentType) {
        def allParents = this.getParentComponents()
		def withType = []
		allParents.each{ p ->
			if ( p.getType().equals(parentType)  ) {
				withType += p
			}
		}
		return withType
    }

    /**
     * Returns the Drawing Component that this component belongs to.
     * @return This component's parent drawing component.
     */
    DLComponent getDrawing() {

        if( myDrawing == null ) {
	        // Need to still have datacenter here so upgrades don't explode.
            if( this.hasRole('drawing') || this.hasRole('datacenter') ) {
	            setDrawing( this )
            } else {
				//log.trace("Attempting to compute drawing for $this  PARENTS ${getParentComponents()}")
                Set<DLComponent> drawingSet = new HashSet<>()
                for( DLComponent parent : getParentComponents() ) {
                    drawingSet.add( parent.getDrawing() )
                }

                // Should never happen, but I'd like to know if it does!
                if( drawingSet.size() != 1 ) {
                    String extra
                    if( drawingSet.size() == 0 ) {
                        extra = "PARENTS: " + getParentComponents()
                    } else {
                        extra = "DRAWINGS: $drawingSet"
                    }
                    throw new ModelCatastrophe("Component is associated with ${drawingSet.size()} Drawings instead of 1. $this'\n$extra" )
                }

	            setDrawing( drawingSet.iterator().next() )
            }
        }
        return myDrawing
    }

	public void setDrawing( DLComponent drawing ) {
		if( !drawing.hasRole( ComponentRole.DRAWING ) ) {
			// Need to check the pre-7.0 role name to handle upgrades.
			if( !drawing.hasRole('datacenter') ) {
				throw new IllegalArgumentException("Unable to set Drawing for $this: Component $drawing is not a Drawing.")
			}
		}
		this.myDrawing = drawing

		// Propogate to the objects.
		DLObject drawingObj = drawing.getManagedObjectsOfType( ObjectType.DRAWING ).first()
		for( DLObject o : getManagedObjects() ) {
			o.setAncestor( drawingObj )
		}
	}

	public void clearDrawing(){
		this.myDrawing = null

		// Propogate to the objects.
		for( DLObject o : getManagedObjects() ) {
			o.clearAncestor()
		}
	}

	public DLComponent getRoom() {
		List<DLComponent> p = getParentsOfRole( ComponentRole.ROOM )
		if( p.size() == 1 ) {
			return p.get(0)
		} else if( p.isEmpty() ) {
			return null
		}
		throw new ModelCatastrophe("Component is associated with ${p.size()} Rooms instead of 1. $this'\nPARENTS: ${getParentComponents()}" )
	}

	public double getScale() {
		getDrawing().getPropertyValue("scale")
	}

	/**
	 * Returns the total number of parent containers for this component.
	 * @return the number of DLComponents that are containers and are parents of this DLComponent.
	 */
	int getNumberOfParentContainers() {
		return ( this.getParentContainers().size() )	
	}

	ArrayList<DLObject> getChildObjects() {
		//return _objects.values().collect{ obj -> obj.children }.flatten().unique()
		Set<DLObject> sett = new HashSet<DLObject>()
		for(DLObject o: _objects.values())
			sett.addAll(o.children)
		return new ArrayList<DLObject>(sett)
	}

	ArrayList<DLObject> getManagedObjects() {
		return _managedObjects
	}

	boolean hasManagedObject( DLObject obj ) {
		return getManagedObjects().contains( obj )
	}

	boolean hasManagedObjectOfType( String objectType ){
		boolean result = false
		this.getManagedObjects().each{ mo ->
			if ( mo.getType().equals(objectType) ){
				result = true
			}
		}
		return ( result )
	}

	/**
	 * Returns true if this Component has a managed objects with the matching Parcel. Unlike other Parcel specific
	 * searches, using NULL here will only match those not owned by a Parcel. To match any object regardless of Parcel,
	 * use hasManagedObjectOfType( String )
	 */
	boolean hasManagedObjectOfType( String objectType, String forParcel ){
		boolean result = false
		this.getManagedObjects().each{ mo ->
			if ( mo.getType().equals(objectType) && forParcel == mo.getParcel() ) {
				result = true
			}
		}
		return ( result )
	}

	boolean hasManagedObjectOfType( String objectType, ComponentParcel forParcel ) {
		hasManagedObjectOfType( objectType, forParcel.getTraitId() )
	}

	// Effing Groovy now needs this to handle ambiguity created by null arguments *sigh* #dynamic!
	boolean hasManagedObjectOfType( String objectType, def nullarg ) {
		hasManagedObjectOfType( objectType, (String)nullarg )
	}

	ArrayList<DLObject> getManagedObjectsOfType( String objectType ){
		ArrayList<DLObject> result = []
		this.getManagedObjects().each{ mo ->
			if ( mo.getType().equals(objectType) ){
				result.add( mo )
			}
		}
		return ( result )
	}

	/**
	 * Returns the managed objects with the matching Parcel. Unlike other Parcel specific searches, using NULL here
	 * will only match those not owned by a Parcel. To match any object regardless of Parcel, use getManagedObjectsOfType( String )
	 */
	ArrayList<DLObject> getManagedObjectsOfType( String objectType, String forParcel ){
		ArrayList<DLObject> result = []
		this.getManagedObjects().each{ mo ->
			if ( mo.getType().equals(objectType) && forParcel == mo.getParcel() ){
				result.add( mo )
			}
		}
		return ( result )
	}

	ArrayList<DLObject> getManagedObjectsOfType( String objectType, ComponentParcel forParcel ){
		return getManagedObjectsOfType( objectType, forParcel.getTraitId() )
	}

	// Effing Groovy now needs this to handle ambiguity created by null arguments *sigh* #dynamic!
	ArrayList<DLObject> getManagedObjectsOfType( String objectType, def nullarg ){
		return getManagedObjectsOfType( objectType, (String)nullarg )
	}

	Set<DLObject> getAllDescendantObjects() {
		Set<DLObject> results = new HashSet<DLObject>()
		this.getChildObjects().each{ o ->
            if(o!=null){
                results.addAll( o.getDescendents() )
            }
		}
		return results
	}

	// todo: Probably needs to be optimized.
	ArrayList<DLObject> getParentObjects() {
		// O: why do objects of 'this' have to be filtered out
		// A: Because _objects is a bit incestuous and has some objects where their parent is really a sibling object
		//    in this same component due to the way we promote some managed objects to 'root' that aren't "pure" roots.
		Set<DLObject> sett = new HashSet<DLObject>()
		for( DLObject o : _objects.values() ) {
			for( DLObject p : o.parents ) {
				if( !_managedObjects.contains( p ) ) {
					sett.add( p )
				}
			}
		}
		return new ArrayList<DLObject>(sett)
	}

	ArrayList<DLObject> getChildObjectsOfType(String objectType){
		ArrayList<DLObject> results = new ArrayList<DLObject>()
		getChildObjects().each{ o->
	        if ( o.getType().equalsIgnoreCase(objectType) ){
				results += o
			}
		}
		return results
	}


	boolean descendentOf(DLComponent ancestor) {
		def parents = getParentComponents()
		if(ancestor in parents)
			return true
		for(DLComponent parent: parents) {
			if(parent.descendentOf(ancestor))
				return true
		}
		return false
	}

	public JXPathContext getContext() {
		return myContext
	}

	// Closure must take 4 arguments: who, propertyName, oldVal, newVal
	void addPropertyChangeListener(def who, Closure c) {
		propertyChangeListeners[who] = c
	}

	void removePropertyChangeListener(def who) {
		propertyChangeListeners.remove(who)
	}

	///* NOTE: sucks performance.  disabled for production
	String toString() {
		if(_properties.containsKey('name')) {
			return "DLComponent('$type' $_dlid > name:" + getPropertyStringValue('name') + ")" //@" + Integer.toHexString(hashCode())
		} else {
			return "DLComponent('$type' $_dlid)" //@" + Integer.toHexString(hashCode())
		}
	}
	//*/

	public String getName(){
		String displayname = ""
		if ( _properties.containsKey("name") ){
			displayname = getPropertyStringValue('name')
		}
		return ( displayname )
	}
	
	public String getFullName(){
		if(!this.classes.contains("staticchild") ){
			return this.getName()
		} else{
			//go find the parent
			DLComponent parent
			for(DLComponent p :this.getParentComponents()){
				if(p.hasRole("staticchildplaceable")){
					parent = p
				}
			}
			return parent?.getName() + "-" + this.getName()
		}
	}

	/**
	 * Stores the list of roles that imply secondary semantics, we cache this so we don't have to keep redoing that string split
	 */
	static List<String> secondaryRoles = null

	/**
	 * Checks to see if this component's roles imply "secondary" behavior.
	 * @return True if this is a component with a secondary role.
	 */
	public boolean isSecondary(){
		boolean result = false
		if ( ! secondaryRoles ){
			secondaryRoles = CentralCatalogue.getComp("components.secondary").split(",").toList()
		}
		if ( ! secondaryRoles.disjoint(this.classes) ){
				result = true
			}
		return result
	}

	/**
	 * Finds the host component for a secondary-class component.  If not secondary, returns itself.
	 * @return a reference to the host component (or itself, if this is not secondary)
	 */
	public DLComponent getHostComponent(){
		if ( ! this.isSecondary() ){
			return this;
		}
		DLComponent host = null;

		//for the moment (Mars) there are no secondary components that hook up via conducers, so we can ignore those for now

		//in cases where there is only a single connection, we can just return the other one
		def pc = this.getProducerConsumers()
		def cp = this.getConsumerProducers()
		if ( pc.size() + cp.size() == 1 ){
			if ( pc.size() == 1 ){
				return pc.first()
			} else {
				return cp.first()
			}

		//account for stratification coins and/or control cooler devices
		} else if (pc.size() + cp.size() == 2) {
			if (pc.size() == 2) {
				return pc.first()
			} else {
				return cp.first()
			}

		} else {
			///ahhh.  Trouble.
			//we've got a controlled crah or one of the Mars-era standalone resources or strategies
			if ( this.listRoles().contains("controlleddevice") ){
				//regardless of what else, this should only be connected to 1 not-secondary thing.
				//go find that
				def combo = []
				combo.addAll(pc)
				combo.addAll(cp)
				for( DLComponent c : combo ){
					if ( ! c.isSecondary() ){
						return c
					}
				}

			} else if ( this.listRoles().contains("controlstrategy") ){
				//strategies consume a producer on their host (a controlleddevice)
				if ( cp.size() == 1 ){
					return cp.first()
				} else {
					log.warn("Looking for host for $this and it doesn't seem to have one", "default")
					host = this;
				}

			} else {
				log.warn("Looking for host for $this and it doesn't seem to have one", "default")
				host = this;
			}

		}
		return host
	}

	/**
	 * Like it says on the tin: checks for either the {@code dynamicchild} or {@code staticchild} roles.
	 * @return True if either a static or dynamic child.
	 */
	public boolean isStaticOrDynamicChild(){
		return(this.hasRole("dynamicchild") || this.hasRole("staticchild"))
	}

	/**
	 * Checks to see if this component can be contained by another component of the specified type name.
	 * @param parentTypeName type of the potential parent to check
	 * @return True if an instance of {@code parentTypeName} can be the parent of this component
	 */
	public boolean canGoIn( String parentTypeName ){
		 return ( this.cmodel.typeCanBeChild( parentTypeName, this.type ) )
	}

	public boolean canGoIn( DLComponent parentComponent ){
		return ( this.canGoIn( parentComponent.getType() ) )
	}

	public void addDockingpoint(Property pFrom, DLObject pTo ){
		pFrom.setReferrent(pTo)
		
	}
    // get isDeleting flag to check if the component is deleting now
    public boolean getDeleting(){
        return isDeleting
    }
    // set isDeleting
    public void setDeleting(boolean b){
        isDeleting = b;
    }

    void addProxy( DLComponent proxy ) {
        if(_proxies == null){
            _proxies = new HashSet<DLComponent>()
        }
        _proxies.add( proxy )
    }

    void removeProxy( DLComponent proxy ) {
        _proxies.remove( proxy )

        // very few things will have a proxy, so release the memory if we don't need it.
        if( _proxies.isEmpty() ) {
            _proxies = null
        }
    }

    public java.util.Collection<DLComponent> getProxies() {
        // Defensive copy or empty list to avoid nulls in the wild.
        if( _proxies == null ) {
            return new ArrayList<DLComponent>(0)
        }
        return new ArrayList<DLComponent>( _proxies )
    }

    public boolean hasProxy() {
        return _proxies != null
    }

	protected Dlid _dlid
	void setDlid(Dlid _id) {
		if(_dlid == null) {
			_dlid = _id
		} else{
			throw new ModelCatastrophe("Cannot reset a Dlid!")
		}
	}

	Dlid getDlid() {
		return _dlid
	}


	public Point2D.Double getXY(){
		return new Point2D.Double( this.getPropertyValue("x"), this.getPropertyValue("y") )
	}

	public void setXY(Point2D loc){
		boolean iStartedIt = false
		try {
			if( !CentralCatalogue.getUndoBuffer().operationInProgress() ) {
				CentralCatalogue.getUndoBuffer().startOperation()
				iStartedIt = true
			}

			this.setPropertyValue("x", loc.x)
			this.setPropertyValue("y", loc.y)

		} catch( Exception e ) {
			if( iStartedIt ) CentralCatalogue.getUndoBuffer().rollbackOperation()
			throw e
		} finally {
			if( iStartedIt ) CentralCatalogue.getUndoBuffer().finishOperation()
		}
	}

	// todo: This should really be a Property Converter type so it is handled automatically, but that would break too
	// much right now and I'm tired of this pattern being everywhere.
	public List<Point2D> getPolygonPoints() {

		List<Point2D> pointList = []

		String pointStr = getPropertyValue( ComponentProp.POLY_POINTS )

		if( pointStr ) {
			for( String xyStr : pointStr.split(';') ) {
				String[] strParts = xyStr.split(",")

				if( strParts.size() != 2 ) {
					log.trace("Incomplete point string '${xyStr}' from '${pointStr}' in $this")
				} else {
					try {
						pointList += new Point2D.Float( Float.parseFloat( strParts[0] ), Float.parseFloat( strParts[1] ) );

					} catch( NumberFormatException e ) {
						log.trace("Invalid point '${xyStr}' from '${pointStr}' in $this")
					}
				}
			}
		}

		return pointList
	}

	public String setPolygonPoints( List<Point2D> pointList ) {
		String pointStr = ""

		for( int i = 0; i < pointList.size(); i++ ) {
			pointStr += pointList.get(i).x + "," + pointList.get(i).y

			if( i < ( pointList.size() - 1 ) ) {
				pointStr += ";"
			}
		}

		setPropertyValue( ComponentProp.POLY_POINTS, pointStr );

		return pointStr
	}

}

public class ComponentProperty implements ComponentTrait {
	private String name
	private String display
	private boolean editable
	private boolean displayed
	private String type
	protected def _valueChoices = null
	private def value
	private boolean unitConvertable
	private String dimension

	protected String imageXMLGuts
	protected boolean isSecret

	private boolean centerbadge = false
	public boolean hasCenterbadge(){
		return centerbadge
	}
	String badge
	String badgeShape
	String parcel
	int displayLevel = 0

	protected ComponentProperty() {}

	ComponentProperty(def cpXml) {
		parseXml( cpXml )
	}

	protected parseXml( def cpXml ) {
		name = cpXml.@name.text()
		display = cpXml.@display.text()
		displayed = Boolean.parseBoolean(cpXml.@displayed.text())
		editable = Boolean.parseBoolean(cpXml.@editable.text())
		dimension = cpXml.@dimension.text()
		isSecret = Boolean.parseBoolean(cpXml.@secret.text())
		centerbadge = Boolean.parseBoolean(cpXml.@centerbadge.text())
		badge = cpXml.@badge.text()
		badgeShape = cpXml.@badgeshape.text()
		parcel = cpXml.@parcel.text()
		
		if(dimension==''){
			unitConvertable = false
		}else{
			unitConvertable = true
		}

		type = cpXml.@type.text()
		String sValChoices = cpXml.@valueChoices.text()
		if(sValChoices != '') {
			_valueChoices = [:]
			sValChoices.split(',').each{ it ->
				def kv = it.split(':', 2)
				_valueChoices[kv[0]] = PropertyConverter.fromString(type, kv[1])
			}
		}
		//setXmlValue(cpXml)

		if ( !isImageType() ){
			setXmlValue(cpXml)
			imageXMLGuts = null
		} else {
			value = null
			imageXMLGuts = new String(cpXml.text())
		}

	}

	@Override
	String getTraitId() {
		return this.getName()
	}

	@Override
	String getTraitDisplayName() {
		return this.getDisplayName()
	}

	@Override
	DLComponent getOwner() {
		return null  //To change body of implemented methods use File | Settings | File Templates.
	}

	boolean isInherited() {
		return false
	}

	public String getBase64Image(){
		return imageXMLGuts
	}

	public void finishImage(){
		imageXMLGuts = null
	}

	void serialize(groovy.xml.MarkupBuilder builder, boolean serializeImages = true) {
		if(valueChoices != null && valueChoices.size() > 0) {
			builder.property('name': name, 'display': display, 'type': type, 'editable': editable, 'displayed': displayed, 'dimension': dimension, 'centerbadge': centerbadge, 'badge': badge, 'badgeshape': badgeShape, parcel: parcel, 'valueChoices': _valueChoices.collect{k,v -> "${k}:${PropertyConverter.toXml(type,v)}"}.join(","), PropertyConverter.toXml(type, value))
		} else {

			if ( isImageType() && ! serializeImages ){
				builder.property('name': name, 'display': display, 'type': type, 'editable': editable, 'displayed': displayed, 'dimension': dimension, parcel: parcel, "" )

			} else {
				//"normal"
				builder.property('name': name, 'display': display, 'type': type, 'editable': editable, 'displayed': displayed, 'dimension': dimension, 'centerbadge': centerbadge, 'badge': badge, 'badgeshape': badgeShape, parcel: parcel, PropertyConverter.toXml(type, value))
			}


		}
	}

	void serializeAsPrototype(groovy.xml.MarkupBuilder builder){
		this.serialize(builder,false);
	}

	String getType() {
		return type
	}

	def getValue() {
		return value
	}

	String[] getValueChoices() {
		if(_valueChoices != null)
			return _valueChoices.collect{k,v -> k} as String[]
		return null
	}

	public boolean hasValueChoices(){
		return (_valueChoices != null)
	}

	def valueForChoice(String choice) {
		return _valueChoices[choice]
	}

	String choiceForValue(def value) {
		String rv
		_valueChoices.each{k,v -> if(v == value) rv = k}
		return rv
	}

	String choiceForValueAsString( String value ) {
		String rv
		_valueChoices.each{k,v -> if(v.toString() == value) rv = k}
		return rv
	}

	boolean isDisplayed() {
		return displayed
	}

	void setDisplayed(boolean d){
		this.displayed = d
	}

	boolean isEditable() {
		return editable
	}

	void setEditable(boolean e) {
		this.editable = e
	}

	boolean isUnitConvertable(){
		return unitConvertable
	}
	
	String getDisplayName() {
		return display
	}

	String getName(){
		return name
	}
	
	String getDimensionName(){
		return dimension
	}
	protected void setXmlValue(def xml) {
		def val = PropertyConverter.fromXml(type, xml)
		value = val
	}

	void setValue(String newValue) {
		def val = PropertyConverter.fromString(type, newValue)
		value = val
	}

	void setValue(def newValue) {
		def val = PropertyConverter.fromObject(type, newValue)
		value = val
	}

    boolean isImageType() {
        return ( type.equalsIgnoreCase('DeploymentLab.Image') || type.equalsIgnoreCase('java.awt.Image') )
    }

	String toString() {
		return "[name:$name, display:$display, editable:$editable, displayed:$displayed, type:$type, " +
		       "value:$value, unitConvertable:$unitConvertable, dimension:$dimension, isSecret:$isSecret, parcel:$parcel]"
	}
}

public class ComponentInheritedProperty extends ComponentProperty implements ModelChangeListener {
	private static final Logger log = Logger.getLogger(ComponentInheritedProperty.class.getName())

	private Deque<DLComponent> inheritedPath = new ArrayDeque<>()
	private boolean isConnected = false
	private String inheritedFromRole

	ComponentInheritedProperty( def cpXml, DLComponent _owner ) {
		inheritedPath.push( _owner )
		String inheritedName = cpXml.@name.text()
		inheritedFromRole = cpXml.@inheritedrole.text()

		// Find possible parent types to get the inherited property.
		for( String ptype : _owner.getModel().listTypesWithRole( inheritedFromRole ) ) {
			// TODO: First one wins. May want to warn if there are conflicting definitions or handle it somehow.
			def otherProp = _owner.getModel().getTypeProp( ptype, inheritedName )
			if ( otherProp != null ) {
				parseXml( otherProp )
			}
		}

		if( name == null ) {
			throw new UnknownPropertyTypeException("Inherited property not found in possible parent types for '$inheritedName'")
		}

		// We need to make some adjustments since this is inherited.
		editable = false
		_valueChoices = null
		displayed = Boolean.parseBoolean( cpXml.@displayed.text() )
		parcel = cpXml.@parcel.text()

		if ( !isImageType() ){
			setXmlValue(cpXml)
			imageXMLGuts = null
		} else {
			value = null
			imageXMLGuts = new String(cpXml.text())
		}

		// Need to listen for new parents.
		_owner.getModel().addModelChangeListener( this )
	}

	@Override
	void serialize(groovy.xml.MarkupBuilder builder, boolean serializeImages = true) {
		builder.inheritedproperty('name': name, 'inheritedrole': inheritedFromRole, 'displayed': displayed, parcel: parcel, PropertyConverter.toXml(type, value))
	}

	boolean isInherited() {
		return true
	}

	@Override
	void setEditable(boolean e) {
		// Ignore. Inherited so not directly editable.
	}

	@Override
	String toString() {
		return "[inheritedrole:$inheritedFromRole, displayed:$displayed, parcel:$parcel, value:$value] -> ${super.toString()}}"
	}


	@Override
	void componentRemoved( DLComponent component ) {}

	@Override
	void childAdded( DLComponent parent, DLComponent child ) {

		if( inheritedPath.contains( child ) ) {
			// We can sometimes get add before the remove when a parent is deleting. So see if we need to disconnect and
			// accept this add. Otherwise, the remove will clear the listener and we miss the add so we have no listener.
			Iterator<DLComponent> iter = inheritedPath.iterator()
			DLComponent c = iter.next()
			while( c != child ) {
				DLComponent next = iter.next()
				if( c.isDeleting && ( next == child ) ) {
					childRemoved( c, child )
					break
				}
				c = next
			}

			// Check for the inherited property if this parent connects the missing link in our path. This handles
			// inheriting from multiple generations away and having any link along that path change on us.
			if( !isConnected ) {
				DLComponent inheritedFrom = findInheritedComponent( parent, inheritedPath )

				if( inheritedFrom != null ) {
					inheritedFrom.addPropertyChangeListener( this, this.&inheritedPropertyChanged )
					log.debug("${getOwner()}.$name inheriting from $inheritedFrom")
					getOwner().setPropertyValue( name, inheritedFrom.getPropertyValue( name ) )
					isConnected = true
				}
			}
		}
	}

	private DLComponent findInheritedComponent( DLComponent c, Deque<DLComponent> path ) {
		path.push( c )

		// Is this our guy?
		if( c.hasRole( inheritedFromRole ) && c.hasProperty( name ) ) {
			return c
		}

		// Nope, so check the parents.
		for( DLComponent p : c.getParentComponents() ) {
			DLComponent found = findInheritedComponent( p, path )
			if( found != null ) {
				return found
			}
		}

		// End of the trail and we found nothing. Rollback to another branch.
		path.pop()
		return null
	}

	void inheritedPropertyChanged( def who, String prop, def oldVal, def newVal ) {
        if( prop == name ) {
	        log.trace("InheritPropChange: ${getOwner()}.$name inherited value '$newVal' from ${getOwner()}")
	        getOwner().setPropertyValue( name, newVal )
        }
    }

	@Override
	void childRemoved( DLComponent parent, DLComponent child ) {
		// If this relationship is along our inherited path, we need to disconnect.
		if( inheritedPath.containsAll( [ parent, child ] ) ) {
			DLComponent popped = inheritedPath.pop()
			popped.removePropertyChangeListener( this )
			isConnected = false
			log.debug("${getOwner()}.$name stopped inheriting from $popped")

			// Pop down to the child so we can build it back up once a new relationship is established at this broken point.
			while( popped != parent ) {
				popped = inheritedPath.pop()
			}
		}
	}

	@Override
	DLComponent getOwner() {
		return inheritedPath.peekLast()
	}

	@Override
	void componentAdded( DLComponent component ) {}

	@Override
	void associationAdded( DLComponent producer, String producerId, DLComponent consumer, String consumerId ) {}

	@Override
	void associationRemoved( DLComponent producer, String producerId, DLComponent consumer, String consumerId ) {}

	@Override
	void modelMetamorphosisStarting( MetamorphosisEvent event ) {}

	@Override
	void modelMetamorphosisFinished( MetamorphosisEvent event ) {}
}

public class ComponentBinding implements DLBinding, ComponentTrait {
	private static final Logger log = Logger.getLogger(ComponentBinding.class.getName())

	private static HashMap<String, CompiledExpression> expressionRegistry = new HashMap<String, CompiledExpression>()

	private dependentVars = []
	private propertyPaths = []
	private String valueExpr
	private Serializable compiledExpr

	private cachedProperties = []
	private DLComponent belongsTo
	String parcel

	ComponentBinding(def bXml, JXPathContext _ctx, DLComponent _owner) {
		dependentVars = bXml.@vars.text().split(',')
		//propertyPaths = bXml.property.collect{ it.text() }
		for( def it : bXml.property ){
			propertyPaths.add(it.text())
		}
		valueExpr = bXml.value.text()
		parcel = bXml.@parcel.text()
		belongsTo = _owner

		if(!expressionRegistry.containsKey(valueExpr)){
			expressionRegistry[valueExpr] = MVEL.compileExpression(valueExpr)
		}

		compiledExpr = expressionRegistry[valueExpr]

		for(String path: propertyPaths) {
			try {
				// TODO: make sure the path has at least 1 item in it!
				//_ctx.iterate(path).each { cachedProperties << it }
				int targets = 0
				for( def it : _ctx.iterate(path) ){
					//println "iterate on $path"
					cachedProperties.add(it)
					targets ++
				}
				if(targets == 0){
					log.warn("No destinations found for path $path in ${belongsTo.type}", "default")
				}
				//cachedProperties << _ctx.getValue(path)
			} catch(Exception e) {
				log.error("Error looking up XPath expression '$path'")
				log.error("Variables: " + _ctx.getVariables())
				log.error(e.getCause() + log.getStackTrace(e))
				throw e
			}
		}
	}

	@Override
	String getTraitId() {
		return null  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	String getTraitDisplayName() {
		return null  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	DeploymentLab.Model.DLComponent getOwner() {
		return belongsTo
	}

	def listVariables() {
		return dependentVars
	}

	void serialize(groovy.xml.MarkupBuilder builder) {
		builder.varbinding(vars: dependentVars.join(','), parcel: parcel) {
			propertyPaths.each{ propertyPath ->
				property(propertyPath)
			}
			value(valueExpr)
		}
	}

	void serializeAsPrototype(groovy.xml.MarkupBuilder builder){
		this.serialize(builder);
	}

	void eval(def vars) {
		/* Not sure why it needed to do this, but it gets in the way of valid times we want to set to null. Maybe an
		   old MVEL bonked on nulls and this was a workaround? We'll try it without the check and see what happens

		//log.trace("Evaluating expression '$valueExpr' on vars: $vars")
		def nulls = dependentVars.findAll{vars[it] == null}
		if(nulls.size() > 0) {
			log.info("Skipping evaluation of expression '$valueExpr' because some variables $nulls were null")
			return
		}
		*/
		vars["self"] = belongsTo
		vars["lid"] = belongsTo.getModel().getLid()
		//vars["log"] = log
		def result
		try {
			result = MVEL.executeExpression(compiledExpr, vars)
		} catch(Exception e) {
			log.error(log.getStackTrace(e))
			log.error("Couldn't evaluate expression '$valueExpr'.")
			log.error("vars: $vars")
			throw e
		}

		for(MutableModelValue s: cachedProperties) {
			s.setValue(result)
		}
	}

	/**
	 * Gets the Settings that this varbinding points to.
	 * @return
	 */
	List<Setting> getTargetSettings(){
		return cachedProperties
	}

}

public class Consumer implements ComponentTrait {
	private static final Logger log = Logger.getLogger(Consumer.class.getName())

	private DLComponent belongsTo

	private String id
	private String datatype
	private String name
	private String propertyPath
	private boolean required
	String parcel

	// the property, dockingpoint or collection, that holds the association
	private Property consumerDestination

    // Pair<ProducerObject,ProducerId> -> Producer Component DLID
	private Map<Pair<DLObject, String>,Dlid> _producers = [:]

	Consumer(DLComponent owner, def cXml, JXPathContext ctx) {
		belongsTo = owner

		id = cXml.@id.text()
		datatype = cXml.@datatype.text()
		name = cXml.@name.text()
		propertyPath = cXml.@property.text()
		required = Boolean.parseBoolean(cXml.@required.text())
		parcel = cXml.@parcel.text()

		cXml.producer.each{ pXml ->
			ObjectModel om = belongsTo.getOModel()
			DLObject o = om.getObject(om.getDlid(pXml.@objectid.text()))
			String id = pXml.@id.text()

            // Get the component DLID if we have one. Keep it null if its an older file without this info.
            Dlid cDlid = null
            String cId = pXml.@componentid.text()
            if( cId != null ) {
                cDlid = Dlid.getDlid( cId )
            }
            _producers[ new Pair<DLObject, String>(o, id) ] = cDlid
		}

		try {
            // ## Should this get the same treatment as Producer did?
			consumerDestination = ctx.getValue(propertyPath)
		} catch(Exception e) {
			log.error("Error looking up XPath expression '$propertyPath'")
			log.error("Variables: " + ctx.getVariables())
			log.error(e.getCause() + log.getStackTrace(e))
			throw e
		}
		assert ((consumerDestination instanceof DeploymentLab.Model.Collection) || (consumerDestination instanceof DeploymentLab.Model.Dockingpoint)), "Consumer object must be a property of type Collection or Dockingpoint!"
	}
	
	public Consumer(DLComponent _owner, String _id, String _datatype, String _name, String _propertyPath, boolean _required, JXPathContext ctx){
		belongsTo = _owner
		id = _id
		datatype = _datatype
		name = _name
		propertyPath = _propertyPath
		required = _required;

		try {
			consumerDestination = ctx.getValue(propertyPath)
		} catch(Exception e) {
			log.error("Error looking up XPath expression '$propertyPath'")
			log.error("Variables: " + ctx.getVariables())
			log.error(e.getCause() + log.getStackTrace(e))
			throw e
		}
		assert ((consumerDestination instanceof DeploymentLab.Model.Collection) || (consumerDestination instanceof DeploymentLab.Model.Dockingpoint)), "Consumer object must be a property of type Collection or Dockingpoint!"
	}

	@Override
	String getTraitId() {
		return this.getId()
	}

	@Override
	String getTraitDisplayName() {
		return this.getName()
	}

	@Override
	DLComponent getOwner() {
		return belongsTo
	}

	void serialize(groovy.xml.MarkupBuilder builder) {
		builder.'consumer'('id': id, 'datatype': datatype, 'name': name, 'property': propertyPath, 'required': required, parcel: parcel) {
			_producers.each{ pair, cId ->
				'producer'('objectid': pair.a.getDlid(), 'id': pair.b, 'componentid': findProducerComponent( cId, pair ).getDlid())
			}
		}
	}

	void serializeAsPrototype(groovy.xml.MarkupBuilder builder) {
		builder.'consumer'('id': id, 'datatype': datatype, 'name': name, 'property': propertyPath, 'required': required, parcel: parcel) {}
	}

	String getName() {
		return name
		//return belongsTo.getName() + ".$name"
	}

	String getDatatype() {
		return datatype
	}

	String getId() {
		return id
	}
	
	String getPropertyPath(){
		return propertyPath
	}

	boolean isCollection() {
		return (consumerDestination instanceof DeploymentLab.Model.Collection)
	}

	boolean isRequired() {
		return required
	}

	ArrayList< Pair<DLObject, String> > listProducers() {
        return new ArrayList<Pair<DLObject, String>>( _producers.keySet() )
	}

	boolean hasProducer(DLComponent c, String id) {
		Pair<DLComponent, String> searcher = new Pair<DLComponent, String>(c, id)
		def result = listProducerComponents().find{ pair -> pair.equals(searcher) }
		return result != null
	}

	boolean hasProducer(){
		return (this.listProducers().size() > 0)
	}

	void addProducer(DLComponent producer, String producerId) {
		DLObject pObject = producer.getProducerObject(producerId)
		Pair<DLObject, String> pair = new Pair<DLObject, String>(pObject, producerId)
		if(consumerDestination instanceof DeploymentLab.Model.Collection) {
			consumerDestination.add(pObject)
		} else {
			if(_producers.size() > 0) {
				log.error("Can't have more than 1 producer!")
				return
			}
			consumerDestination.setReferrent(pObject)
		}
		_producers[ pair ] = producer.getDlid()
		producer.getProducer(producerId).addConsumer(belongsTo, id)
	}

	void removeProducer(DLComponent producer, String producerId) {
		log.trace("removeProducer($producer, $producerId)")
		if(consumerDestination instanceof DeploymentLab.Model.Collection) {
			DLObject pObject = producer.getProducerObject(producerId)
			Pair<DLObject, String> pair = new Pair<DLObject, String>(pObject, producerId)
			Pair<DLObject, String> toRemove = _producers.keySet().find{it.equals(pair)}
			if(toRemove != null) {
				if(! _producers.remove(toRemove))
					log.error("Nothing removed!")
			} else {
				log.info("Didn't find anything to remove!")
			}
			consumerDestination.remove(pObject)
		} else {
			if(_producers.size() == 0) {
				log.error("No producer to remove!")
			}
			_producers.clear()
			consumerDestination.setReferrent(null)
		}
		producer.getProducer(producerId).removeConsumer(belongsTo, id)
	}

	public boolean canAssociate( Producer p ){
		if ( p.getDatatype().equalsIgnoreCase(datatype) ){
			return true
		}
		return false
	}

	void clear() {
		oldProducers = _producers
		_producers = [:]
		oldProducers.each{ pPair, cId ->
			DLComponent pComponent = findProducerComponent( cId, pPair )
            if( pComponent != null ) {
                String oldId = pPair.b
                pComponent.getProducer(oldId).removeConsumer(belongsTo, id)
            }
		}
		if(consumerDestination instanceof DeploymentLab.Model.Collection) {
			consumerDestination.clear()
		} else {
			consumerDestination.setReferrent(null)
		}
	}

	DLObject getConsumerObject() {
		return consumerDestination.belongsTo
	}

	/**
	 * Returns the producer objects this is consuming.
	 *
	 * @return  A list of the objects this Consumer is consuming.
	 */
	List<DLObject> getProducerObjects() {
		List<DLObject> pObjects = []

		_producers.keySet().each { pair ->
			pObjects.add( pair.a )
		}

		return pObjects
	}

	String toString() {
		//return "${belongsTo.getName()}::Consumer($name, $datatype, $id, $collection, $propertyPath)"
		return name;
	}

	private conducer = null
	void addConducer( Conducer d){
		conducer = d
	}
	boolean hasConducer(){
		if ( conducer ){ return true }
		return false
	}
	Conducer getConducer(){
		return conducer
	}


	public Map<DLComponent, String> mapProducers(){
		def result = [:]
		_producers.each{ pair, cId ->
            DLComponent c = findProducerComponent( cId, pair )
            if( c != null ) {
                result[ c ] = pair.b
            }
		}
		return result
	}

    public List<Pair<DLComponent,String>> listProducerComponents(){
        def result = []
        _producers.each{ pair, cId ->
            DLComponent c = findProducerComponent( cId, pair )
            if( c != null ) {
                result += new Pair( c, pair.b )
            }
        }

        return result
    }

    /**
     * Just a hack to handle our strange situation. This is needed because we can't lookup a component at load time
     * since not all components have been loaded yet. So we save the DLID instead and look it up as needed. We also
     * might not have the DLID at first if this is from an older pre-Proxy project. So we lookup the object owner.
     *
     * Why don't we lookup the object owner all the time? That doesn't work for proxies where the anchor is the owner
     * but the association is with the proxy component.
     *
     * @param cId   The Producer component DLID to lookup, or NULL if we don't have one.
     * @param pObj  The Producer object this consumer is associated with.
     *
     * @return  The Component with the given DLID or owns the given object.
     */
    private DLComponent findProducerComponent( Dlid cId, Pair<DLObject, String> pObj ) {
        DLComponent c = null

        if( cId == null ) {
            // Get the object owner, then update the map with the DLID for.
            c = this.belongsTo.getModel().getOwner( pObj.a )
            if( c != null ) {
                _producers[ pObj ] = c.getDlid()
            }
        } else {
            c = this.belongsTo.getModel().getComponentFromDLID( cId )
        }

        if( c == null ) {
            log.trace("Producer Component not found. Usually OK during a delete/close operation. DLID = $cId  Obj = $pObj")
        }

        return c
    }
}

public class Producer implements ComponentTrait {
	private static final Logger log = Logger.getLogger(Producer.class.getName())

	private DLComponent belongsTo

	private String id
	private String datatype
	private String name
	private String objectPath
	private boolean exclusive
	/**
	 * If this producer should be the default producer to use.
	 * @since Jupiter
	 */
	boolean defaultProducer

	// the object that holds the association
	private DLObject provider

    // Pair<ConsumerObject,ConsumerId> -> Consumer Component DLID
	private Map<Pair<DLObject, String>,Dlid> _consumers = [:]

	private JXPathContext ctx = null

	String parcel

	Producer(DLComponent owner, def pXml, JXPathContext ctx) {
		belongsTo = owner
		
		id = pXml.@id.text()
		datatype = pXml.@datatype.text()
		name = pXml.@name.text()
		objectPath = pXml.@object.text()
		exclusive = Boolean.parseBoolean(pXml.@exclusive.text())
		defaultProducer = Boolean.parseBoolean(pXml.@defaultProducer.text())
		parcel = pXml.@parcel.text()
		this.ctx = ctx

		pXml.consumer.each{ cXml ->
			ObjectModel om = belongsTo.getOModel()
			DLObject o = om.getObject(om.getDlid(cXml.@objectid.text()))
			String id = cXml.@id.text()

            // Get the component DLID if we have one. Keep it null if its an older file without this info.
            Dlid cDlid = null
            String cId = cXml.@componentid.text()
            if( cId != null ) {
                cDlid = Dlid.getDlid( cId )
            }
			_consumers[ new Pair<DLObject, String>(o, id) ] = cDlid
		}
/*
		try {
			provider = ctx.getValue(objectPath)
		} catch(Exception e) {
			log.error("Error looking up XPath expression '$objectPath'")
			log.error("Variables: " + ctx.getVariables())
			log.error(e.getCause() + log.getStackTrace(e))
			throw e
		}
*/
	}

	Producer(DLComponent _owner, String _id, String _datatype, String _name, String _objectPath, JXPathContext ctx){
		belongsTo = _owner
		id = _id
		datatype = _datatype
		name = _name
		objectPath = _objectPath
		exclusive = false
		//parcel?

		this.ctx = ctx
/* ## PROXY: What's this? Didn't see this before.
		try {
			provider = ctx.getValue(objectPath)
		} catch(Exception e) {
			log.error("Error looking up XPath expression '$objectPath'")
			log.error("Variables: " + ctx.getVariables())
			log.error(e.getCause() + log.getStackTrace(e))
			throw e
		}
*/

	}

	@Override
	String getTraitId() {
		return this.getId()
	}

	@Override
	String getTraitDisplayName() {
		return this.getName()
	}

	void serialize(groovy.xml.MarkupBuilder builder) {
		builder.'producer'('id': id, 'datatype': datatype, 'name': name, 'object': objectPath, 'exclusive': exclusive, 'defaultProducer': defaultProducer, parcel: parcel) {
			_consumers.each{ pair, cId ->
				'consumer'('objectid': pair.a.getDlid(), 'id': pair.b, 'componentid': findConsumerComponent( cId, pair ).getDlid())
			}
		}
	}

	void serializeAsPrototype(groovy.xml.MarkupBuilder builder) {
		builder.'producer'('id': id, 'datatype': datatype, 'name': name, 'object': objectPath, 'exclusive': exclusive, 'defaultProducer': defaultProducer, parcel: parcel) {}
	}

	String getName() {
		return name
		//return belongsTo.getName() + ".$name"
	}

	String getDatatype() {
		return datatype
	}

	String getId() {
		return id
	}

	Boolean isExclusive(){
		return exclusive
	}

	ArrayList< Pair<DLObject, String> > listConsumers() {
		return new ArrayList<Pair<DLObject, String>>( _consumers.keySet() )
	}

	boolean hasConsumer(DLComponent c, String id) {
		Pair<DLComponent, String> searcher = new Pair<DLComponent, String>(c, id)
		def result = listConsumerComponents().find{ pair -> pair.equals(searcher) }
		return result != null
	}

	boolean hasConsumer(){
		return (this.listConsumers().size() > 0)
	}

	void addConsumer(DLComponent c, String id) {
		_consumers[ new Pair<DLObject, String>(c.getConsumerObject(id), id) ] = c.getDlid()
	}

	void removeConsumer(DLComponent c, String id) {
		log.trace("removeConsumer($c, $id)")
		Pair<DLObject, String> searcher = new Pair<DLObject, String>(c.getConsumerObject(id), id)
		Pair<DLObject, String> oip = _consumers.keySet().find{it.equals(searcher)}
		if(oip != null) {
			if(! _consumers.remove(oip))
				log.error("Nothing removed")
		} else {
			log.error("Nothing found to remove!")
		}
	}

	public boolean canAssociate( Consumer c ){
		if ( c.getDatatype().equalsIgnoreCase(datatype) ){
			return true
		}
		return false
	}

	DLObject getProducerObject() {
		//log.trace("getProducerObject: provider=$provider; ctx=$ctx")
		if (provider == null){
			if(ctx == null){
                // ## PROXY: What's this? Didn't see this before.
				//wow, do something clever
				if(this.belongsTo.hasRole("proxy")){
					ctx = this.belongsTo.getZomboComponent().getContext()
					
					log.trace("I just got a context from ${this.belongsTo.getZomboComponent()}")
					
				}else{
					throw new ModelCatastrophe("Bad Context!")
				}
			}

			try {
				provider = ctx.getValue(objectPath)
			} catch(Exception e) {
				log.error("Error looking up XPath expression '$objectPath'")
				log.error("Variables: " + ctx.getVariables())
				log.error(e.getCause() + log.getStackTrace(e))
				throw e
			}
		}


		return provider
	}

	/**
	 * Returns the consumer objects this is producing to.
	 *
	 * @return  A list of the objects this Producer has as a consumer.
	 */
	List<DLObject> getConsumerObjects() {
		List<DLObject> cObjects = []

		_consumers.keySet().each { pair ->
			cObjects.add( pair.a )
		}

		return cObjects
	}

	DLComponent getOwner(){
		return belongsTo
	}

	String getObjectPath(){
		return objectPath
	}

	String toString() {
		//return "${belongsTo.getName()}::Producer($name, $datatype, $id, $objectPath)"
		return name;
	}

	public Map<DLComponent, String> mapConsumers(){
		def result = [:]
		_consumers.each{ pair, cId ->
            DLComponent c = findConsumerComponent( cId, pair )
            if( c != null ) {
                result[ c ] = pair.b
            }
		}		
		return result
	}

	public List<Pair<DLComponent,String>> listConsumerComponents(){
		def result = []
		_consumers.each{ pair, cId ->
            DLComponent c = findConsumerComponent( cId, pair )
            if( c != null ) {
                result += new Pair( c, pair.b )
            }
		}

		return result
	}

	private conducer = null
	void addConducer( Conducer d ){
		conducer = d
	}
	boolean hasConducer(){
		if ( conducer ){ return true }
		return false
	}
	Conducer getConducer(){
		return conducer
	}

    /**
     * Just a hack to handle our strange situation. This is needed because we can't lookup a component at load time
     * since not all components have been loaded yet. So we save the DLID instead and look it up as needed. We also
     * might not have the DLID at first if this is from an older pre-Proxy project. So we lookup the object owner.
     *
     * Why don't we lookup the object owner all the time? That doesn't work for proxies where the anchor is the owner
     * but the association is with the proxy component.
     *
     * @param cId   The Consumer component DLID to lookup, or NULL if we don't have one.
     * @param cObj  The Consumer object this producer is associated with.
     *
     * @return  The Component with the given DLID or owns the given object.
     */
    private DLComponent findConsumerComponent( Dlid cId, Pair<DLObject, String> cObj ) {
        DLComponent c = null

        if( cId == null ) {
            // Get the object owner, then update the map with the DLID for the component.
            c = this.belongsTo.getModel().getOwner( cObj.a )
            if( c != null ) {
                _consumers[ cObj ] = c.getDlid()
            }
        } else {
            c = this.belongsTo.getModel().getComponentFromDLID( cId )
        }

        if( c == null ) {
            log.trace("Consumer Component not found. Usually OK during a delete/close operation. DLID = $cId  Obj = $cObj")
        }

        return c
    }
}



