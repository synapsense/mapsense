package DeploymentLab.Model

import DeploymentLab.SceneGraph.ArbitraryShapeNode
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.SelectModel
import DeploymentLab.channellogger.Logger
import edu.umd.cs.piccolo.nodes.PPath
import java.awt.Shape
import java.awt.geom.Point2D
import java.util.regex.Pattern

/**
 * @author Vadim Gusev
 * @since Aireo
 * Date: 30.10.13
 * Time: 14:01
 */
class RoomController implements ModelChangeListener {
	private static final Logger log = Logger.getLogger(RoomController.class.getName())

	private ComponentModel cmodel
	private SelectModel smodel
	private DeploymentPanel deploymentPanel

	RoomController(ComponentModel cm, SelectModel sm, DeploymentPanel dp) {
		cmodel = cm
		deploymentPanel = dp
		smodel = sm
	}

	@Override
	void componentAdded(DLComponent component) {
		 if( component.getType().equals( ComponentType.ROOM ) ) {
			 component.addPropertyChangeListener(this, this.&roomPropertyChanged)
		 } else if( cmodel.typeCanBeChild( ComponentType.ROOM, component.getType() ) && !component.getType().equals( ComponentType.ORPHANAGE ) ) {
			 component.addPropertyChangeListener(this, this.&componentPropertyChanged)
		 } /* else if (component.getType().equals(ComponentType.DRAWING)){
			 component.addPropertyChangeListener(this, this.&drawingPropChanged)
		 }*/
	}

	@Override
	void componentRemoved(DLComponent component) {
		component.removePropertyChangeListener(this)
	}

	@Override
	void childAdded(DLComponent parent, DLComponent child) {
		if (cmodel.isMetamorphosizing()) { return }

		if( child.getType() == ComponentType.ROOM ) {
			// Update the parent Room or Drawing area to include the new child.
			if( parent.getType() == ComponentType.ROOM ) {
				updateRoomNetArea( parent )
			} else if( parent.getType() == ComponentType.DRAWING ) {
				updateDrawingArea( parent )
			}
		}
	}

	@Override
	void childRemoved(DLComponent parent, DLComponent child) {
		if (cmodel.isMetamorphosizing()) { return }

		if( parent.getType() == ComponentType.ROOM && parent.isDeleting ) {
			// Find a new room parent for the child.
			List<DLComponent> grandparentRooms = parent.getParentsOfType( ComponentType.ROOM )
			if( grandparentRooms.size() > 0 ) {
				// Let grandma raise the kid.
				log.trace("Moved $parent child $child to the grandparent ${grandparentRooms.first()}.")
				grandparentRooms.first().addChild( child )
			} else {
				// No grandparent room, so child to the drawing if that is possible or to the orphanage if it isn't.
				if( smodel.getActiveDrawing().canChild( child ) ) {
					log.trace("Moved $parent child $child to the drawing.")
					smodel.getActiveDrawing().addChild( child )
				} else {
					log.trace("Moved $parent child $child to the orphanage.")
					cmodel.getOrphanage( parent ).addChild( child )
				}
			}
		}

		if( child.getType() == ComponentType.ROOM ) {
			// Update the parent Room or Drawing area to include the new child.
			if( parent.getType() == ComponentType.ROOM ) {
				updateRoomNetArea( parent )
			} else {
				updateDrawingArea( parent )
			}
		}
	}

	@Override
	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	@Override
	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	@Override
	void modelMetamorphosisStarting(MetamorphosisEvent event) {}

	@Override
	void modelMetamorphosisFinished(MetamorphosisEvent event) {}

	public void roomPropertyChanged(def who, def propertyName, def oldVal, def newVal) {
		//we don't want to get in the way if replace config is running right now
		if (cmodel.isMetamorphosizing()) { return }
		if( deploymentPanel.getPNodeFor( who )?.isDragging() ) { return }
		//log.trace("property changed on $who : $propertyName")
		switch (propertyName) {
			case "points":
				if (!newVal.isEmpty()){
					this.updateRoomComponent(who)
				}
				updateRoomGrossArea( who )
				// Net area and parent area will get updated on the next prop change notification.
				break

			case ComponentProp.GROSS_AREA :
				updateRoomNetArea( who )

				// Update the parent. Might be a room or a drawing.
				for( DLComponent c : who.getParentsOfType( ComponentType.ROOM ) ) {
					updateRoomNetArea( c )
				}

				for( DLComponent c : who.getParentsOfType( ComponentType.DRAWING ) ) {
					updateDrawingArea( c )
				}
				break
		}
	}

	public void componentPropertyChanged(def who, def propertyName, def oldVal, def newVal) {
		//we don't want to get in the way if replace config is running right now
		if (cmodel.isMetamorphosizing() || who.isMetamorphosizing()) { return }
		//log.trace("property changed on $who : $propertyName")
		switch (propertyName) {
			case "metamorphosizing":
			case "x":
			case "y":
				this.updateComponent( who )
				break
		}
	}
/*
	public void drawingPropChanged(def who, def propertyName, def oldVal, def newVal) {
		//we don't want to get in the way if replace config is running right now
		if (cmodel.isMetamorphosizing()) { return }
		//log.trace("property changed on $who : $propertyName")
	}
*/
	private void updateRoomGrossArea( DLComponent room ) {

		String pointStr = room.getPropertyValue('points')

		if( pointStr == null || pointStr.isEmpty() ) {
			log.debug("Can't update Room area: points string is empty")
		} else {
			ArrayList<Point2D.Float> points = []

			for( String xyStr : pointStr.split(";") ) {
				String[] strParts = xyStr.split(",")
				if( strParts.size() == 2 ) {
					points += new Point2D.Float( Float.parseFloat( strParts[0] ), Float.parseFloat( strParts[1] ) )
				}
			}

			double grossArea = 0

			for( int i = 0; i < points.size(); i++ ) {
				Point2D.Float p1 = points.get(i)
				Point2D.Float p2 = points.get((i == points.size()-1) ? 0 : i+1 )

				grossArea += ( p1.x * p2.y - p1.y * p2.x ) / 2
			}

			// Scale it, convert to square feet, and make it positive.
			grossArea = Math.abs( grossArea /** Math.pow( room.getScale(), 2 )*/ ) / 144

			// Only if needed since it triggers other updates.
			if( room.getPropertyValue( ComponentProp.GROSS_AREA ) != grossArea ) {
				room.setPropertyValue( ComponentProp.GROSS_AREA, grossArea )
			}
		}
	}

	private void updateRoomNetArea( DLComponent c ) {
		// Get the total area of the child rooms.
		double kidArea = 0

		for( DLComponent kid : c.getChildrenOfType( ComponentType.ROOM ) ) {
			kidArea += kid.getPropertyValue( ComponentProp.GROSS_AREA )
		}

		double area = ( c.getPropertyValue( ComponentProp.GROSS_AREA ) - kidArea )

		// Only if needed since it triggers other updates.
		if( c.getPropertyValue( ComponentProp.AREA ) != area ) {
			c.setPropertyValue( ComponentProp.AREA, area )
		}
	}

	private void updateDrawingArea( DLComponent drawing ) {
		double area = 0

		for( DLComponent kid : drawing.getChildrenOfType( ComponentType.ROOM ) ) {
			area += kid.getPropertyValue( ComponentProp.GROSS_AREA )
		}

		// Only if needed since it triggers other updates.
		if( drawing.getPropertyValue( ComponentProp.AREA ) != area ) {
			drawing.setPropertyValue( ComponentProp.AREA, area )
		}
	}

	private void updateRoomComponent(DLComponent room) {
		//log.trace("Update room: $room interior...")
		// first set relations between rooms
		ArbitraryShapeNode pnode = deploymentPanel.getPNodeFor(room)
		Point2D p1
		if (pnode.points.isEmpty()){
			p1 = parseRoomPoints(room).first()
		} else {
			p1 = pnode.points.first()
		}

		// check if room placed into another room
		updateRoomComponent(room, p1)

		//check if another rooms got into moved room
		cmodel.getComponentsInDrawing(smodel.getActiveDrawing()).each {dlc ->
			if (dlc.type.equals(ComponentType.ROOM) && !dlc.equals(room)){
				Point2D p2 = deploymentPanel.getPNodeFor(dlc).points.first()
				updateRoomComponent(dlc, p2)
			}
		}
		// then check other components
		cmodel.getComponentsInDrawing(smodel.getActiveDrawing()).each {dlc ->
			if (cmodel.typeCanBeChild(ComponentType.ROOM, dlc.getType()) && !dlc.type.equals(ComponentType.ROOM) && !dlc.type.equals(ComponentType.ORPHANAGE)){
				updateComponent( dlc )
			}
		}
	}

	private void updateComponent( DLComponent component ) {
		// Ignore proxies.
		if( component.hasRole( ComponentRole.PROXY ) ) return

		//log.trace("Check component: $component rooms' placement...")

		def parent = getRootRoomByPoint( new Point2D.Double( component.getPropertyValue("x"), component.getPropertyValue("y") ) )

		if( !parent ) {
			// No room, so child to the drawing if possible. Otherwise, child to the orphanage.
			if( smodel.getActiveDrawing().canChild( component ) ) {
				parent = smodel.getActiveDrawing()
			} else {
				parent = cmodel.getOrphanage( component )
			}
		}

		if( !component.isChildComponentOf( parent ) ) {
			component.getParentComponents().each {
				if( it.hasRole( ComponentRole.ROOM ) || it.hasRole( ComponentRole.DRAWING ) ) {
					log.debug("UC: Remove $component from $it")
					it.removeChild(component)
				}
			}
			log.debug("UC: Add $component to $parent")
			parent.addChild( component )
		}
	}

	private void updateRoomComponent(DLComponent component, def point) {
		//log.trace("Check room component: $component rooms' placement...")
		def room = getRootRoomByPoint( point, component )
		if (room){
			//log.trace("ROOM: $component in room: $room")
			if (!component.isChildComponentOf(room)){
				component.getParentComponents().each {
					if (it.hasRole( ComponentRole.ROOM ) || it.type.equals(ComponentType.DRAWING)) {
						log.debug("URR: Remove $component from $it")
						it.removeChild(component)
					}
				}
				log.debug("URR: Add $component to room $room")
				room.addChild(component)
			}
		} else {
			//log.trace("ROOM: $component out of room")
			component.getParentComponents().each {
				if( it.hasRole( ComponentRole.ROOM ) ) {
					log.debug("UR: Remove $component from room $it")
					it.removeChild(component)
				}
			}

			if (!component.isChildComponentOf(smodel.activeDrawing)) {
				smodel.activeDrawing.addChild(component)
			}
		}
	}

	private DLComponent getRootRoomByPoint(Point2D p, DLComponent excludeRoom = null){
		def result = null
		def rooms = cmodel.getComponentsInDrawing(smodel.getActiveDrawing()).findAll {dlc ->
			dlc.type.equals(ComponentType.ROOM) && (dlc != excludeRoom) && !dlc.isDeleting && isPointInRoom(dlc, p)
		}
		if (rooms.size() == 1){
			//log.trace("Point in one room. [${rooms}]")
			result = rooms.first()
		}else if (rooms.size() > 1){
			//log.trace("Point in multiple rooms. [${rooms}]")
			def first = rooms.first();
			rooms.remove(first)
			result = findChildOfRoom(rooms, first)
		}

		return result
	}

	private static DLComponent findChildOfRoom(ArrayList<DLComponent> rooms, DLComponent parentRoom){
		def child = rooms.find {k ->
			k.isChildComponentOf(parentRoom)
		}

		if (child){
			rooms.remove(child)
			child = findChildOfRoom(rooms, child)
			return child
		} else {
			return parentRoom
		}
	}

	private boolean isPointInRoom(DLComponent room, Point2D point){
		List<Point2D> points = room.getPolygonPoints()
		PPath polygon = new PPath();
		polygon.setPathToPolyline(points.toArray(new Point2D[points.size()]))
		if (polygon) {
			Shape roomShape = polygon.getPathReference()
			return roomShape.contains( point.getX(), point.getY() )
		}
	}

	private static ArrayList<Point2D.Float> parseRoomPoints(DLComponent room){
		def result = []
		room.getPropertyStringValue('points').split(Pattern.quote(';')).each {point ->
			String[] xy = point.split(Pattern.quote(','))
			result.add(new Point2D.Float(Float.parseFloat(xy[0]), Float.parseFloat(xy[1])))
		}

		return result
	}
}
