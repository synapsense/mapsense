
package DeploymentLab.Model;

import java.util.Set;

import DeploymentLab.Exceptions.*;

// This is a link to another object
public class Dockingpoint extends Property {
	private Set<String> reftypes;

	private Dlid referrentId;
	private DLObject referrentObj; // This is a cached lookup of referrentId
	boolean dirty;

	public Dockingpoint(DLObject _belongsTo, String n, Set<String> rt) {
		super(_belongsTo, n);
		reftypes = rt;
		dirty = true;
        }

	public String getType() {
		return "dockingpoint";
	}

	public DLObject getReferrent() {
		if(referrentId == null)
			return null;
		if(referrentObj == null)
			referrentObj = getObjectModel().getObject(referrentId);
		return referrentObj;
	}

	public Dlid getReferrentId() {
		return referrentId;
	}

	public void deserialize(Dlid _id, boolean d) {
		referrentId = _id;
		referrentObj = null;
		dirty = d;
	}

	public boolean isDirty() {
		return dirty;
	}

	public void clean() {
		dirty = false;
	}

	public void makeDirty(){
		dirty = true;
	}

	/**
	 * Sets the referrent to whatever another dockingpont is pointing at.
	 * @param d another dockingpoint
	 * @throws IncompatibleTypeException
	 */
	public void setReferrent(Dockingpoint d) throws IncompatibleTypeException {
		this.setReferrent(d.getReferrent());
	}

	public void setReferrent(DLObject _o) throws IncompatibleTypeException {
		if(_o == null) {
			referrentId = null;
			referrentObj = null;
			dirty = true;
			return;
		}
		if( reftypes.size() > 0 && !reftypes.contains(_o.getType()) )
			throw new IncompatibleTypeException("Tried to set reference to " + _o.getType() + " but was expecting one of " + reftypes);
		referrentId = _o.getDlid();
		referrentObj = _o;
		dirty = true;
	}

	public void clear() throws IncompatibleTypeException{
		this.setReferrent((DLObject)null);
	}

	@Override
	public String toString() {
		return "Dockingpoint{" +
				"reftypes=" + reftypes +
				", referrentId=" + referrentId +
				", referrentObj=" + referrentObj +
				", dirty=" + dirty +
				'}';
	}
}

