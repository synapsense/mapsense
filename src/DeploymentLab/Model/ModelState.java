package DeploymentLab.Model;

/**
 * Defines the various states of a model.
 *
 * @author Ken Scoggins
 * @since Jupiter
 */
public enum ModelState {

    /** The model is behaving normally and change events should only occur as they happen in real time. */
    STABLE,

    /**
     *  The model is being initialized, which typically means it is a new project being created, a project is being
     *  loaded, or a full model upgrade is happening.
     */
    INITIALIZING,

    /**
     * The model is replacing one or more components. Replacing typically involves multiple model events related to the
     * creation of a new component, copying the properties from the old to the new component, then deleting the old
     * component.
     */
    REPLACING,

    /** The model is being cleared. Classes that maintain state based on the model should reset themselves. */
    CLEARING,

    /** Undo operation
    */
    UNDOING,

    /** redo operation*/
    REDOING

}
