package DeploymentLab.Model

import DeploymentLab.channellogger.Logger
import groovy.xml.MarkupBuilder
import org.apache.commons.jxpath.JXPathContext
import javax.naming.OperationNotSupportedException

/**
 * Implements variable pointers.  Based on the mode, this can either be one pointer with multiple possible targets,
 * or multiple pointers one of which is pointing at a target.
 * @author Gabriel Helman
 * @since Io
 * Date: 10/3/12
 * Time: 4:01 PM
 */
class Multidocking implements ComponentTrait, DLBinding {
	private static final Logger log = Logger.getLogger(Multidocking.class.getName())

	private String mode //either "from" | "to"

	private DLComponent belongsTo
	private String dependentVar
	private String targetPath
	String parcel

	private Map<String, PropertyPathPair> options = [:]

	private def targetThingie

	private String currentSelection = null


	//private String id
	//private String name

	Multidocking(def bXml, JXPathContext _ctx, DLComponent _owner) {
		belongsTo = _owner
		dependentVar = bXml.@var.text()//.split(',')
		assert !dependentVar.contains(",")
		targetPath = bXml.@target.text()
		parcel = bXml.@parcel.text()

		mode = bXml.@mode.text()

		for (def it : bXml.option) {
			//options[it.@name.text()] = new PropertyPathPair(it.@target.text(), null)

			def pnp = new PropertyPathPair(it.@path.text(), null)

			for(def d : it.display){
				pnp.displaySettings[d.@setting.text()] = d.text()
			}
			options[it.@name.text()] = pnp

			for(def subX: it.setproperty){
				def subOption = new PropertyPathPair( subX.@path.text() )
				subOption.payload = subX.text()
				pnp.subOptions += subOption
			}

		}

		//resolve target
		try {
			targetThingie = _ctx.getValue(targetPath)
			if (targetThingie == null) {
				log.warn("No destination found for targetThingie xpath '${targetPath}'")
			}
			if(mode=="from"){
				assert targetThingie instanceof Property
			} else if (mode=="to"){
				//assert targetThingie instanceof DLObject
			}

		} catch (Exception e) {
			log.error("Error looking up target XPath expression '${targetPath}' with dependantvar '$dependentVar' and mode '$mode'")
			log.error("Variables: " + _ctx.getVariables())
			log.error(e.getCause() + log.getStackTrace(e))
			throw e
		}

		//resolve all options
		for (PropertyPathPair o : options.values()) {
			try {
				if( o.path ) {
					o.value =  _ctx.getValue(o.path)

					if( o.value == null ) {
						log.warn("No destination found for xpath '${o.path}'")
					}
				}

				for(PropertyPathPair sub : o.subOptions){
					if( sub.path ) {
						sub.value = _ctx.getValue(sub.path)
						if (sub.value == null) {
							log.warn("No destination found for xpath '${sub.path}'")
						}
					}
				}
			} catch (Exception e) {
				log.error("Error looking up XPath expression '${o.path}'")
				log.error("Variables: " + _ctx.getVariables())
				log.error(e.getCause() + log.getStackTrace(e))
				throw e
			}
		}
	}

	@Override
	String getTraitId() {
		return null //id
	}

	@Override
	String getTraitDisplayName() {
		return null //name
	}

	@Override
	DLComponent getOwner() {
		return belongsTo
	}

	@Override
	def listVariables() {
		return [dependentVar]
	}

	/**
	 * Execute the multidocking
	 * @param vars a map of propertyname : value (should only have one entry)
	 */
	@Override
	void eval(Object vars) {
		log.debug("execute MultiDocking: with vars = $vars and targetThingie $targetThingie in mode '$mode'")

		def choice = vars[dependentVar]
		log.debug("choice is $choice")

		if(mode=="from"){
			//multidocking
			//def target = vars[dependentVar]
			if (targetThingie instanceof Dockingpoint) {
				targetThingie.setReferrent(options[choice].value)

			} else {
				throw new OperationNotSupportedException("CONFUSED")
			}

		} else if (mode == "to"){
			//singledocking
			/*
			for(def o : options.values()){
				o.value.setReferrent(null)
				o.displaySettings.each{ setting, value ->
					this.belongsTo.setDisplaySetting(setting, null)
				}
			}
			*/
			if( options[choice].value == null ) {
				// Just null out the old one.
				if (currentSelection){
					if( options[currentSelection].value instanceof Dockingpoint ) {
						options[currentSelection].value.setReferrent(null)
					} else if( options[currentSelection].value instanceof Pier ) {
						options[currentSelection].value.clear()
					}

					options[currentSelection].displaySettings.each{ setting, value ->
						this.belongsTo.setDisplaySetting(setting, null)
					}
				}
			} else if (options[choice].value instanceof Dockingpoint){
				Dockingpoint from = (Dockingpoint)options[choice].value

				if( from.getReferrent() != null ){
					log.warn("UNABLE TO COMPLY: null ref on dockingpoint")
					return
				}

				//clear the old setting only
				if (currentSelection){
					if( options[currentSelection].value ) {
						options[currentSelection].value.setReferrent(null)
					}

					options[currentSelection].displaySettings.each{ setting, value ->
						this.belongsTo.setDisplaySetting(setting, null)
					}
				}

				//def choice = vars[dependentVar]
				log.debug("selected option is ${options[choice]}")

				from.setReferrent(targetThingie)

			} else if (options[choice].value instanceof Pier){
				Pier from = (Pier)options[choice].value

				//clear the old setting only
				if (currentSelection){
					if( options[currentSelection].value ) {
						options[currentSelection].value.clear()
					}

					options[currentSelection].displaySettings.each{ setting, value ->
						this.belongsTo.setDisplaySetting(setting, null)
					}
				}

				log.debug("selected option is ${options[choice]}")

				//Pier from = options[choice].value
				from.setReferent(targetThingie)

			} else {
				log.fatal("UNABLE TO COMPLY")
				throw new UnsupportedOperationException("Multidocking `to' field not Pier or Dockingpoint")
			}
		} else {
			throw new OperationNotSupportedException("WAY CONFUSED")
		}

		options[choice].displaySettings.each{ setting, value ->
			this.belongsTo.setDisplaySetting(setting, value)
		}

		for(PropertyPathPair sub: options[choice].subOptions){
			log.debug("Setting ${sub.value} to ${sub.payload}")
			//sub.value <= sub.payload
			((Setting)sub.value)?.setValue(sub.payload)
		}

		currentSelection = choice
	}

	@Override
	void serialize(MarkupBuilder builder) {
		String path
		builder.multidocking(var: dependentVar, target: targetPath, mode: mode, parcel: parcel) {
			options.each{ k, v ->
				path = v.path ? v.path : ''
				builder.option(name: k, path: path){
					v.displaySettings.each { set, val ->
						builder.display(setting: set, val)
					}
					v.subOptions.each{ so->
						path = so.path ? so.path : ''
						builder.setproperty( path: path, so.payload)
					}
				}
			}
		}
	}

	@Override
	void serializeAsPrototype(MarkupBuilder builder) {
		this.serialize(builder)
	}
}
