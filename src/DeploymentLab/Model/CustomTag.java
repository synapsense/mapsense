package DeploymentLab.Model;

import com.synapsense.dto.Tag;

/**
 * MapSense datamodel implementation of ES custom tags.  Custom tags can be applied to any object property in ES.
 *
 * Allowed types are integer, long, string, double
 *
		<property type="setting">
	            <name>name</name>
	            <historic>false</historic>
	            <valuetype>java.lang.String</valuetype>
				<customtag name="pluginId" tagType="java.lang.String" >wsn</customtag>
	    </property>
 *
 *
 */
public class CustomTag implements MutableModelValue {
	private Property parent;
	private String tagName;
	private String tagType;
	private Object value;
	private Object oldValue;

	private Object override;

	/**
	 * Generate an empty, nameless string-type custom tag with no parent.
	 */
	public CustomTag(){
		this( null, "", "java.lang.String", "" );
	}

	/**
	 * Generate a new custom tag.
	 * @param p	The containing Property of this tag.
	 * @param tn The name of the tag.
	 * @param tt The type of the tag as a CustomTagTypes
	 * @param v The value of the tag.
	 */
	public CustomTag( Property p, String tn, String tt, Object v ){
		this.parent = p;
		this.tagName = tn;
		this.tagType = tt;
		this.value = v;

	}

	/**
	 * Populate a new custom tag.
	 * @param tn The name of the tag.
	 * @param tt The type of the tag as a CustomTagTypes
	 * @param v The value of the tag.
	 */
	public void deserialize( String tn, String tt, Object v ){
		this.tagName = tn;
		this.tagType = tt;
		this.value = v;
	}


	/**
	 * Retrieves the value of this CustomTag, correctly cast to its type.
	 * @return An Object containing the value of this tag, cast to the datatype this tag is set to.
	 */
	public Object getValue(){
		if ( this.value == null ) { return null; }

		if ( this.override != null ) { return override; }

		if ( !this.tagType.equals("java.lang.String") && this.value.toString().equals("") ){
			//empty numeric values should be null, not coerced to zero or somesuch
			return null;
		}

		if ( this.tagType.equals("java.lang.String") ){
			return ( this.value.toString()  );
		} else if ( this.tagType.equals("java.lang.Integer") ){
			return ( Integer.parseInt(this.value.toString()));
		} else if ( this.tagType.equals("java.lang.Long") ){
			return ( Long.parseLong(this.value.toString()));
		} else if ( this.tagType.equals("java.lang.Double") ){
			return ( Double.parseDouble(this.value.toString()));
		}
		//if none of those worked, just hand our mysterious value out without a cast
		return value;
	}


	public void setValue(Object value) {
		this.value = PropertyConverter.fromObject( tagType, value );
	}

	public String getTagType() {
		return tagType;
	}

	public void setTagType(String tagType) {
		this.tagType = tagType;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	@Override
	public String toString() {
		return "CustomTag{" +
				"tagName='" + tagName + '\'' +
				", tagType='" + tagType + '\'' +
				", value='" + value + '\'' +
				'}';
	}

	/**
	 * Constructs an Environment Server Tag for export.
	 * @return an ES Tag object.
	 */
	public Tag makeESTag(){
		Tag result = new Tag(parent.name, this.getTagName(), this.getValue() );
		return ( result );
	}

	public Object getOldValue() {
		return oldValue;
	}

	public void setOldValue(Object oldValue) {
		this.oldValue = PropertyConverter.fromObject( tagType, oldValue );
	}

	public boolean valueChanged(){
//		System.out.println( "comparing '" + value.toString() + "' of type " + value.getClass().getName()  );
//		System.out.println( "to '" + oldValue.toString() + "' of type " + oldValue.getClass().getName()  );
//		System.out.println( "comparing '" + this.getValue().toString() + "' of type " + this.getValue().getClass().getName()  );
//		System.out.println( "to '" + this.getOldValue().toString() + "' of type " + this.getOldValue().getClass().getName()  );
		return ( this.getValue() != null && ! this.getValue().equals(oldValue) );
	}

	public void resetValue() {
		//oldValue = value;
		oldValue = this.getValue();
	}

	@Override
	public void setOverride(Object val) {
		override = PropertyConverter.fromObject( tagType, val );
	}

	public Object getOverride(){
		return override;
	}

	public Object getRealValue(){
		return value;
	}
}

