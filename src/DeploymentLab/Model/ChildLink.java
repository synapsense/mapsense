
package DeploymentLab.Model;

import java.util.*;

import DeploymentLab.channellogger.*;
import DeploymentLab.Exceptions.*;

public class ChildLink extends Property {
	protected static final Logger log = Logger.getLogger(ChildLink.class.getName());

	private Set<String> reftypes;	// the type of children objects
	private Set<Dlid> _children;	// list of children Dlids
	private Set<Dlid> _oldChildren;

	private Set<DLObject> childCache;

	public ChildLink(DLObject _belongsTo, String n, Set<String> rt) {
		super(_belongsTo, n);
		reftypes = rt;
		_children = new HashSet<Dlid>();
		_oldChildren = new HashSet<Dlid>();
		childCache = new HashSet<DLObject>();
	}

	public String getType() {
		return "childlink";
	}

	public Set<String> getReftypes() {
		return reftypes;
        }

	public void deserialize(Set<Dlid> c, Set<Dlid> oc) {
		_children.clear();
		_children.addAll(c);
		_oldChildren.clear();
		_oldChildren.addAll(oc);
	}

	public Set<DLObject> getChildren() {

		if (childCache.size() == _children.size()){
			return childCache;
		}


		Set<DLObject> rv = new HashSet<DLObject>();
		for(Dlid id: _children) {
			rv.add(getObjectModel().getObject(id));
		}
		//return rv;
		childCache = rv;

		return childCache;
	}

	public Set<DLObject> getOldChildren() {
		// Doesn't happen enough (only at export?) to need a cache.
		Set<DLObject> rv = new HashSet<>();
		for( Dlid id: _oldChildren ) {
			// Use the getObject that also looks in the deleted pile.
			rv.add( getObjectModel().getObjectDeleted( id ) );
		}
		return rv;
	}

	public ArrayList<DLObject> getSortedChildren() {
		ArrayList<DLObject> result = new ArrayList<DLObject>( this.getChildren() );
		Collections.sort( result, new DLObjectDlidComparator() );
		return (result);
	}


	public boolean valueChanged() {
		return ! (_children.size() == _oldChildren.size() && _children.containsAll(_oldChildren));
	}

	public void completeRemoval(Dlid _id) {
		if(_oldChildren.contains(_id) && ! _children.contains(_id)) {
			_oldChildren.remove(_id);
		} else {
			log.error("Tried to complete removal of " + _id + " which didn't make sense", "default");
		}
	}

	public void completeAddition(Dlid _id) {
		if(_children.contains(_id) && ! _oldChildren.contains(_id)) {
			_oldChildren.add(_id);
		} else {
			log.error("Tried to complete addition of " + _id + " which didn't make sense", "default");
		}
	}

	public void completeAllAdditionAndRemoval(){
		_oldChildren.clear();

		// Don't copy those that were never exported to oldChildren.
		// WARNING! Since this may be called before the children have had their TO set, this assumes it is getting called
		// right after an export and also looks at the isExportable flag. That would be wrong if called at any other time.
		for( Dlid childId : _children ) {
			DLObject child = getObjectModel().getObject( childId );
			if( child.isExportable() || child.hasKey() ) {
				_oldChildren.add( childId );
			}
		}
	}

	public void clearOldChildren() {
		_oldChildren.clear();
	}

	public Set<Dlid> getRemovedChildIds() {
		// All child IDs in oldChildren that are not in children
		Set<Dlid> rv = new HashSet<Dlid>();
		for(Dlid id: _oldChildren) {
			if(! _children.contains(id))
				rv.add(id);
		}
		return rv;
	}

	public Set<Dlid> getAddedChildIds() {
		Set<Dlid> rv = new HashSet<Dlid>();
		for(Dlid id: _children) {
			if(! _oldChildren.contains(id))
				rv.add(id);
		}
		return rv;
	}

	public Set<Dlid> getChildIds() {
		return _children;
	}

	public Set<Dlid> getOldChildIds() {
		return _oldChildren;
	}

	public Set<DLObject> getChildrenOfType(String type) {
		Set<DLObject> rv = new HashSet<DLObject>();
		for(Dlid id: _children) {
			DLObject o = getObjectModel().getObject(id);
			if(o.getType().equals(type))
				rv.add(o);
		}
		return rv;
	}

	public boolean hasChild( DLObject _child ) {
		return _children.contains( _child.getDlid() );
	}

	public void addChild(DLObject _child) throws IncompatibleTypeException, DuplicateObjectException {
		if(! reftypes.contains(_child.getType()))
			throw new IncompatibleTypeException("Tried to set child to " + _child.getType() + " but was expecting one of " + reftypes);
		if(_children.contains(_child.getDlid()))
			throw new DuplicateObjectException("Duplicate Child!");
		_children.add(_child.getDlid());
		_child.addParent(belongsTo);
		childCache.add(_child);
	}

	public void removeChild(DLObject _child) {
		if(!_children.contains(_child.getDlid()))
			return;
		_children.remove(_child.getDlid());
		_child.removeParent(belongsTo);
		childCache.remove(_child);
	}

	public String toString() {
		return "ChildLink(name='" + name + "', reftypes=" + reftypes + ")";
	}


	public DLObject getChildWithSetting(String propertyName, Object value){
		DLObject result = null;
		for(DLObject o : this.getChildren()){
			if (o.hasObjectProperty(propertyName) && o.getObjectSetting(propertyName).getValue() == value){
				result = o;
				break;
			}
		}
		return result;
	}
}

