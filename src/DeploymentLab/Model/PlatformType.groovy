package DeploymentLab.Model

import DeploymentLab.channellogger.Logger

/**
 * @author Vadim Gusev
 * @since Europa
 */
public enum PlatformType {

	FUSION( 10, "Fusion" ),
	THERMANODE( 11, "ThermaNode", "T1" ),
	PRESSURENODE( 12, "Pressure Node" ),
	PRESSURENODE_2( 13, "Pressure Node 2", "P2" ),
	CONSTELLATION( 14, "Constellation" ),
	CONSTELLATION_2( 15, "Constellation 2", "C2" ),
	ENTHALPY_NODE( 16, "Enthalpy Node" ),
	THERMANODE_2( 17, "ThermaNode 2", "T2" ),
	THERMANODE_DX( 20, "ThermaNode DX", "DX" ),
	SMARTPLUG( 51, "P3 SmartPlug", "SC" ),
	SMARTLINK( 52, "SmartLink", "D1" ),
	DAMPER_CONTROLLER( 80, "Damper Controller", "DA" ),
	THERMANODE_EZ( 82, "ThermaNode EZ", "EZ" ),
	THERMANODE_EZH( 92, "ThermaNode EZ-H", "EH" )

	private static final Logger log = Logger.getLogger( PlatformType.getClass().getName() )

	private int id
	private String name
	private String productCode
	private static def pattern = "([\\w]{10})([\\w]{2})"

	private PlatformType( int id, String name ) {
		this( id, name, "" )
	}

	private PlatformType( int id, String name, String productCode ){
		this.id = id
		this.name = name
		this.productCode = productCode
	}

	public int getId(){
		return id
	}

	public String getName(){
		return name
	}

	public String getProductCode(){
		return productCode
	}

	public static PlatformType getTypeByProperty(def prop){
		def m = prop =~pattern
		def p = (m.matches()) ? m.group(2) : prop    //if serial, get product code only

		for(PlatformType pt : values()){
			if (pt.id.toString().equals(p) || pt.name.equals(p) || pt.productCode.equals(p)){
				log.trace("Type found: $p -> $pt")
				return pt
			}
		}
		log.trace("Type not found: $p")
		return null
	}

	public String toString(){
		return "$name ($id)"
	}
}
