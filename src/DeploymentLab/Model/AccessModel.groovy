package DeploymentLab.Model

import DeploymentLab.CentralCatalogue
import DeploymentLab.ComponentLibrary
import DeploymentLab.DLProject
import DeploymentLab.MapHelper
import DeploymentLab.Security.LogicalId
import DeploymentLab.SpringUtilities
import DeploymentLab.channellogger.Logger
import groovy.swing.SwingBuilder
import groovy.xml.MarkupBuilder

import javax.swing.*
import java.awt.*
import java.util.List

/**
 * Created with IntelliJ IDEA.
 * User: vgusev
 * Date: 2/19/13
 * Time: 1:01 PM
 * To change this template use File | Settings | File Templates.
 */
class AccessModel implements ModelChangeListener {
	private static final Logger log = Logger.getLogger(AccessModel.class.getName())

	private static final String NETWORK_TYPE = "wsnnetwork"
	private static final String GATEWAY_TYPE = "wsngateway"
	private static final String NODE_TYPE = "wsnnode"
	private static final String SENSOR_TYPE = "wsnsensor"
	private static final String ACTUATOR_TYPE = "wsnactuator"

	private Map<Dlid, DLObject> drawings = new HashMap<Dlid, DLObject>()
	private Map<Dlid, DLObject> network = new HashMap<Dlid, DLObject>()
	private Map<Dlid, DLObject> gateway = new HashMap<Dlid, DLObject>()
	private Map<Dlid, DLObject> nodes = new HashMap<Dlid, DLObject>()
	private Map<Dlid, DLObject> sensors = new HashMap<Dlid, DLObject>()
	private Map<DLObject, Long> nodesToMac = new HashMap<DLObject, Long>()
	private DLProject belongsTo

	private ObjectModel objectModel;

	private Map<PlatformType, Object> typeToXmlMap = new HashMap<PlatformType, Object>()
	private Map<PlatformType, Map<String, Object>> defaultValues = new HashMap<PlatformType, Map<String, Object>>()

	private JDialog dialog
	private JComboBox cb
	private DLObject selectedNetwork

	private LogicalId lid

	public AccessModel(DLProject belongsTo, def accessModelXml = null) {
		log.trace("Access model init")
		this.belongsTo = belongsTo
		this.belongsTo.componentModel.addModelChangeListener(this)
		this.lid = this.belongsTo.componentModel.getLid()

		//load object model
		def objectsXml = ComponentLibrary.parseXmlFile(CentralCatalogue.getApp("objects.definitions.main"))
		this.objectModel = new ObjectModel(objectsXml)
		for (String it : CentralCatalogue.getApp("objects.definitions.extra").split(",")) {
			this.objectModel.addTypes(ComponentLibrary.parseXmlFile(it.trim()))
		}

		//load nodes definitions
		def types = [
				'pressure2',
				'crah-single-plenumrated-thermanode',
				'rack-control-rearexhaust-sf-thermanode2',
				'crac-thermanode-dx',
				'ez',
				'h-wing',
				'control_register_damper',
				'leakdetector-constellation2'];

		types.each {
		   loadObjectDescription(it)
		}

		if (accessModelXml) {
			loadFromXml(accessModelXml)
		}

	}

	public void updateAccessModel() {
		log.info("Update Access Model!")
		objectModel.clear()
		drawings.clear()
		network.clear()
		gateway.clear()

		//create required objects: drawing, network and gateway
		belongsTo.objectModel.getObjectsByType( ObjectType.DRAWING ).each {
			addDrawing(it)
		}
		belongsTo.objectModel.getObjectsByType(NETWORK_TYPE).each {
			addNetwork(it)
		}
		belongsTo.objectModel.getObjectsByType(GATEWAY_TYPE).each {
			addGateway(it)
		}

		//restore old nodes and sensors
		Set<Dlid> nodesToRemove = new HashSet<Dlid>()
		Set<Dlid> sensorsToRemove = new HashSet<Dlid>()

		/**
		 * Node can be in two state:
		 * 1. Has a parent - save in model
		 * 2. Hasn't a parent - remove from model
		 * 3. Hasn't a parent, has a key - mark as deleted
		 */
		for (DLObject node : nodes.values()) {
			DLObject parentNet = node.getParents().find {
				if (it) {
					it.type.equals(NETWORK_TYPE)
				}
			}

			if (parentNet) {
				DLObject parent = network.get(parentNet.dlid)
				if (parent) {
					// first state
					log.trace("Parent network found! Save node $node in access model")
					objectModel.addObject(node)
					node.removeParent(parent)
					setRelation(parent, node)
					continue
				} else {
					node.removeParent(parentNet)
				}
			}

			if (node.hasKey()) {
				// third state
				node.deleted = false
				objectModel.addObject(node)
				objectModel.remove(node)
				log.trace("Node $node marked as deleted!")
				continue
			}

			// second state
			nodesToRemove.add(node.dlid)
			log.warn("Node $node has no parent! Removing from access model!")
		}

		nodesToRemove.each {
			nodesToMac.remove(nodes.get(it))
			nodes.remove(it)
		}

		/**
		 * Sensor can be in three state:
		 *  1. Has a parent - save sensor in model
		 *  2. Hasn't a parent, hasn't a key - remove from model (or doesn't add to model)
		 *  3. Hasn't a parent, has a key - mark as deleted, remove from server then remove from model
		 */
		for (DLObject sensor : sensors.values()) {
			DLObject parentNode = sensor.getParents().find {
				if (it) {
					it.getType().equals(NODE_TYPE)
				}
			}

			if (parentNode) {
				//looking for parent in access model
				DLObject parent = nodes.get(parentNode.dlid)
				if (parent) {
					// first state
					log.trace("Parent node found! Save sensor $sensor in access model!")
					sensor.deleted = false
					objectModel.addObject(sensor)
					if (parent.deleted){
						objectModel.remove(sensor)
					}
					continue
				} else {
					sensor.removeParent(parentNode)
				}
			}

			if (sensor.hasKey()) {
				// third state
				sensor.deleted = false
				objectModel.addObject(sensor)
				objectModel.remove(sensor)
				log.trace("Sensor $sensor marked as deleted!")
				continue
			}

			sensorsToRemove.add(sensor.dlid)
			log.warn("Sensor $sensor has no parent and has no key! Removing from access model!")
		}

		sensorsToRemove.each {
			sensors.remove(it)
		}
	}

	public void loadFromXml(def accessXml) {
		log.trace("Load Access model from xml")
		objectModel.deserialize(accessXml)

		//Load objects from objectmodel to access model
		objectModel.getObjectsByType( ObjectType.DRAWING ).each {
			drawings.put(it.dlid, it)
			log.trace("DRAWING object added: $it.dlid `$it.name`")
		}
		objectModel.getObjectsByType(NETWORK_TYPE).each {
			network.put(it.dlid, it)
			log.trace("NETWORK object added: $it.dlid `$it.name`")
		}
		objectModel.getObjectsByType(GATEWAY_TYPE).each {
			gateway.put(it.dlid, it)
			log.trace("GATEWAY object added: $it.dlid `$it.name`")
		}
		def node = objectModel.getObjectsByType(NODE_TYPE)
		def deletedNodes = objectModel.listDeletedObjects().findAll {
			it.getType().equals(NODE_TYPE)
		}
		node.addAll(deletedNodes)
		node.each {
			nodes.put(it.dlid, it)
			Long macid = (Long) it.getObjectSetting('mac').getValue()
			nodesToMac.put(it, macid)
			log.trace("NODE object added: $it.dlid `$it.name` MAC ID: $macid deleted = $it.deleted")
		}

		def sensor = objectModel.getObjectsByType(SENSOR_TYPE)
		def actuators = objectModel.getObjectsByType(ACTUATOR_TYPE)
		def deletedSensors = objectModel.listDeletedObjects().findAll {
			it.getType().equals(SENSOR_TYPE)
		}
		sensor.addAll(actuators)
		sensor.addAll(deletedSensors)
		sensor.each {
			sensors.put(it.dlid, it)
			log.trace("SENSOR object added: $it `$it.name` deleted = $it.deleted")
		}
	}

	/**
	 * list format should be : [platformId, macId] or [platformName, macId]
	 */
	public void importAccessList(List<String[]> accessList, JFrame parent) {
		boolean successful = true
		updateAccessModel()
		//choosing  network to import access list
		selectNetwork(parent)
		if (!selectedNetwork) {
			log.trace("Network not selected")
			return
		}

		Map<PlatformType, Integer> objectsCount = [:]
		int skipped = 0;
		for (String[] line : accessList) {
			if (line.length < 2) {
				log.info("Line has less then 2 arguments. Skip the line")
				continue
			}
			String type = line[0].trim()
			String mac = line[1].trim()
			if (line[0].equals("Serial") && line[1].equals("MACID")) continue
			if (type == null || type.isEmpty()) {
				log.warn("Supplied node type is null or empty. Skip the line")
				successful = false
				skipped++
				continue
			}
			if (mac == null || mac.isEmpty()) {
				log.warn("Supplied mac id is null or empty. Skip the line")
				successful = false
				skipped++
				continue
			}

			//mac id is correct?
			Long macid
			try{
				macid = Long.parseLong(mac, 16)
			}catch (NumberFormatException e){
				log.warn("Unsupported MAC ID: $mac")
				successful = false
				skipped++
				continue
			}

			//check existing mac
			if (nodesToMac.containsValue(macid)) {
				log.warn("Node with mac id: $mac already exist")
				successful = false
				skipped++
				continue
			}

			PlatformType pt = PlatformType.getTypeByProperty(type)

			//create node object
			if (pt){
				createNode(pt, mac, selectedNetwork)
				MapHelper.increment(objectsCount, pt)
			} else {
				log.warn("Unable find platform type by input string: $type")
				successful = false
				skipped++
			}

		}

		//log.trace("OUTPUT")
		//log.trace(objectModel.makeObjectsString())
		StringBuilder sb = new StringBuilder()
		for (PlatformType pt : objectsCount.keySet().sort {a,b -> a.id<=>b.id}) {
			sb.append("${pt.name}:  ${objectsCount.get(pt)}\n")
		}
		sb.append("Unknown (skipped):  $skipped")
		if (successful) {
			log.info("Access List successfully imported.\n\n${sb.toString()}", "message")
		} else {
			log.warn("Access List import completed with warnings. See log for details. \n\n${sb.toString()}", "message")
		}
	}

	private void createNode(PlatformType pt, String mac, DLObject network){
		Long macId = convertMacToLong(mac)
		DLObject dlo = createObject(pt)
		if (dlo) {
			dlo.getObjectSetting("mac").setValue(macId)
			nodesToMac.put(dlo, macId)
			setRelation(network, dlo)

			//set unique node name
			dlo.getObjectSetting("name").setValue("${dlo.getName()} ${dlo.getDlid().toString()}")
			dlo.getObjectSetting("status").setValue(2)
			dlo.getObjectSetting("id").setValue(lid.generateLogicalId(mac, null))
			dlo.getObjectSetting("period").setValue("5 min")
			log.trace("Node name updated to `${dlo.getName()}`")

			//configure actuator
			if (pt == PlatformType.DAMPER_CONTROLLER){
				log.trace("FIND ACKBAR!!!!")
				DLObject actuator = dlo.getChildren().find {it.type.equals(ACTUATOR_TYPE)}
				DLObject sensor = dlo.getChildren().find {it.type.equals(SENSOR_TYPE)}
				actuator.getPropertiesByType("dockingpoint").first().setReferrent(sensor)
				log.trace(actuator.getPropertiesByType("dockingpoint").first().toString())

				actuator.getObjectSetting("maximumSetting").setValue(100)
				actuator.getObjectSetting("minimumSetting").setValue(0)
				actuator.getObjectSetting("direction").setValue(1)
				actuator.getObjectSetting("travelTime").setValue(15)
				actuator.getObjectSetting("failsafeSetPoint").setValue(100)
			}
		}
	}

	public DLProject getProject() {
		return belongsTo
	}

	public void setProject(DLProject belongsTo) {
		this.belongsTo = belongsTo
	}

	public ObjectModel getObjectModel() {
		updateAccessModel()
		return objectModel
	}

	public DLObject getNodeByMac(String mac) {
		Long macId = convertMacToLong(mac)
		return nodesToMac.find { it.value == macId }?.getKey()
	}

	public void syncKeys() {
		drawings.values().each {
			def dlo = belongsTo.objectModel.getObject(it.dlid)
			dlo.setKey(it.key)
			log.trace("Key $it.key set to $dlo")
		}
		network.values().each {
			def ntw = belongsTo.objectModel.getObject(it.dlid)
			ntw.setKey(it.key)
			log.trace("Key $it.key set to $ntw")
		}
		gateway.values().each {
			def gtw = belongsTo.objectModel.getObject(it.dlid)
			gtw.setKey(it.key)
			log.trace("Key $it.key set to $gtw")
		}
	}

	public void importScanList(List<String[]> scanList) {
		boolean successful = true

		for (String[] component : scanList) {
			log.trace("Try to sync component: ${component[0]} name=${component[2]} prop=${component[3]} mac=${component[4]}")

			if (!Dlid.isDlid(component[0])){
				log.info("Skip line: ${component[0]} ${component[1]} ${component[2]} ${component[3]} ${component[4]}")
				continue
			}

			if (!component[4]){
				log.info("Skip empty MAC ID: ${component[0]} name=${component[2]} prop=${component[3]}")
				continue
			}

			DLComponent originComponent = belongsTo.componentModel.getComponentFromDLID(Dlid.getDlid(component[0]))
			if (!originComponent) {
				log.warn("Origin component with ${component[0]} not found!")
				successful = false
				continue
			}
			log.trace("Origin component found: $originComponent")
			originComponent.setPropertyValue(originComponent.getPropertyNameFromDisplayName(component[3]), component[4].toUpperCase())
		}

		if (successful) {
			log.info("MAC ID Scan List successfully imported.", "message")
		} else {
			log.warn("Scan List import completed with warnings. See log for details", "message")
		}
	}

	private DLObject createObject(PlatformType pt) {
		log.trace("Create `$pt` object")
		def oXml = typeToXmlMap.get(pt)
		if (oXml) {
			DLObject dlo
			List<DLObject> children
			(dlo, children) = objectModel.deserializeObject(oXml)
			for (Map.Entry e : defaultValues.get(pt)) {
				//log.trace("Try to set: prop=`${e.getKey()}` value=`${e.getValue()}`")
				if (e.getValue() != null && !e.getValue().toString().isEmpty()) {
					Setting s = dlo.getObjectSetting(e.getKey().toString())
					if (s != null) {
						s.setValue(e.getValue().toString())
						//log.trace("DONE")
					}
				}
			}
			nodes.put(dlo.dlid, dlo)
			log.trace("New node:  ${dlo.toString()}")
			children.each {
				sensors.put(it.dlid, it)
				log.trace("New sensor:  ${it.toString()}")
			}

			return dlo
		}

		log.trace("Unsupported object type: `$pt`")
		return null
	}

	private void loadObjectDescription(String componentType) {
		ComponentModel cmodel = belongsTo.componentModel

		if (!cmodel.isLoadedComponentType(componentType)) {
			log.warn("Component type $componentType didn't load!")
			return
		}

		def object = cmodel.getTypeDefaultNode(componentType)
		Map<String, Object> valueMap = cmodel.getTypeDefaultProperties(componentType)

		String platformId = cmodel.getTypeDefaultNodeValue(componentType, "platformId")
		PlatformType pt = PlatformType.getTypeByProperty(platformId)

		typeToXmlMap.put(pt, object)
		defaultValues.put(pt, valueMap)
		log.trace("Platform=`$pt` definition loaded!")
	}

	private void setRelation(DLObject parent, DLObject child) {
		for (ChildLink prop : parent.getPropertiesByType("childlink")) {
			if (child.type in prop.reftypes) {
				prop.addChild(child)
				log.trace("Child added: `${child.getName()}` to `${parent.getName()}`")
			}
		}
	}

	private void removeRelation(DLObject parent, DLObject child) {
		for (ChildLink prop : parent.getPropertiesByType("childlink")) {
			if (child.type in prop.reftypes) {
				prop.removeChild(child)
				log.trace("Child removed: `${child.getName()}` from `${parent.getName()}`")
			}
		}
	}

	private void selectNetwork(JFrame parent) {
		selectedNetwork = null
		SwingBuilder sb = new SwingBuilder()

		dialog = sb.dialog(owner: parent, title: 'Select WSN Network', minimumSize: new Dimension(210, 110), layout: new BorderLayout(), modalityType: Dialog.ModalityType.APPLICATION_MODAL, locationRelativeTo: null) {
			def okAction = { selectedNetwork = network.values().find { it.name.equals(cb.getSelectedItem()) }; dialog.setVisible(false) }
			def cancelAction = { dialog.setVisible(false) }

			def mainPanel = panel(layout: new SpringLayout(), constraints: BorderLayout.CENTER) {

				emptyBorder(5, 5, 5, 5)
				label(text: "WSN Network:")
				cb = comboBox(id: 'comboBox', items: network.collect { it.value.name }.toArray())
				((JTextField)cb.getEditor().getEditorComponent()).setColumns(10)
			}
			panel(layout: new FlowLayout(FlowLayout.RIGHT), constraints: BorderLayout.SOUTH) {
				button(text: 'OK', actionPerformed: okAction)
				button(text: 'Cancel', actionPerformed: cancelAction)
			}
			SpringUtilities.makeCompactGrid(mainPanel,
					1, 2,
					6, 6,
					5, 5)
		}
		dialog.pack()
		dialog.show()
	}

	private DLObject cloneObject(DLObject object) {
		StringWriter sw = new StringWriter()
		MarkupBuilder mb = new MarkupBuilder(sw)
		object.serialize(mb)
		def xml = new XmlSlurper().parseText(sw.toString())
		return (DLObject) objectModel.deserializeObject(xml)[0]
	}

	private void addDrawing(DLObject drawingObject) {
		def drawing = cloneObject(drawingObject)
		for (ChildLink c : drawing.getPropertiesByType("childlink")) {
			if (!c.getName().equals("networks")) {
				c.getChildren().clear()
				c.getChildIds().clear()
				c.getOldChildIds().clear()
			}
		}
		drawings.put(drawing.dlid, drawing)

		log.trace("New drawing added: $drawing.dlid $drawing.name")
	}

	private void addNetwork(DLObject ntwObject) {
		def ntw = cloneObject(ntwObject)
		ChildLink c = (ChildLink) ntw.getObjectProperty("nodes")
		c.getChildren().clear()
		c.getChildIds().clear()
		c.getOldChildIds().clear()
		network.put(ntw.dlid, ntw)

		log.trace("New network added: $ntw.dlid $ntw.name")
	}

	private void addGateway(DLObject gtwObject) {
		def gtw = cloneObject(gtwObject)
		gateway.put(gtw.dlid, gtw)

		log.trace("New gateway added: $gtw.dlid $gtw.name")
	}

	public void deleteObject(Dlid dlid) {
		def node = nodes.get(dlid)
		if (node){
			nodesToMac.remove(node)
			nodes.remove(dlid)
		}
		sensors.remove(dlid)
	}

	public boolean isEmpty() {
		if (nodes.isEmpty() && sensors.isEmpty()) return true
		return false
	}

	private void macIdPropertyChanged(def who, String prop, oldVal, newVal) {
		Set<Dlid> markedSensors = new HashSet<Dlid>()

		if (prop.toLowerCase().contains("mac")) {
			log.info("Handle mac id property changes: [$who] Prop=$prop Old=$oldVal New=$newVal")
			DLObject node = getNodeByMac(newVal)
			if (node) {
				log.trace("Node with macid=$newVal found! $node")
				if (node.hasKey()) {
					log.trace("Node has a key! Set key to origin node.")
					DLObject originNode = who.getObjectsOfType(NODE_TYPE).find {
						((Long) it.getObjectSetting("mac").getValue()) == convertMacToLong(newVal)
					}
					if (originNode){
						log.trace("Origin node found: $originNode")
						originNode.setKey(node.key)

						node.getChildren().each { exportedSensor ->
							if (exportedSensor){
								log.trace("Looking for origin sensor $exportedSensor")
								DLObject originSensor
								if (exportedSensor.getType().equals(SENSOR_TYPE)){
									def sensChannel = exportedSensor.getObjectSetting("channel").getValue()
									originSensor = originNode.getChildren().find { it.getObjectSetting("channel").getValue().equals(sensChannel) }
								} else {
									originSensor = originNode.getChildren().find { it.getType().equals(ACTUATOR_TYPE) }
								}
								if (originSensor) {
									log.trace("Origin sensor found: $originSensor")
									if (exportedSensor.hasKey()) {
										log.trace("ID found $exportedSensor.key")
										originSensor.setKey(exportedSensor.key)
									}
								} else {
									markedSensors.add(exportedSensor.dlid)
									log.trace("Origin sensor not found! Exported sensor marked as deleted!")
								}
							} else {
								log.trace("Null child!")
							}
						}
					} else {
						log.warn("Origin node not found, but AccessModel contains node with this mac id! Duplicate detected!")
						return
					}
				} else {
					log.trace("Node has no key! Just remove from access model!")
				}

				node.getChildren().each { sensor ->
					if (sensor){
						if (markedSensors.contains(sensor.dlid)) {
							sensor.setDeleted(true)
							sensor.removeParent(node)
							markedSensors.remove(sensor.dlid)
						} else {
							sensors.remove(sensor.dlid)
							log.trace("Sensor $sensor was removed from access model")
						}
					}
				}
				nodes.remove(node.dlid)
				nodesToMac.remove(node)
				log.trace("Node $node removed from access model!")
			} else {
				log.trace("Node with mac=$newVal not found in access model!")
			}
		}
	}

	@Override
	void componentAdded(DLComponent component) {
		if (!component.listMacIdProperties().isEmpty()){
			component.addPropertyChangeListener(this, this.&macIdPropertyChanged)
			log.trace("Mac Id listener added to $component")
		}
	}

	@Override
	void componentRemoved(DLComponent component) {
	}

	@Override
	void childAdded(DLComponent parent, DLComponent child) {
	}

	@Override
	void childRemoved(DLComponent parent, DLComponent child) {
	}

	@Override
	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
	}

	@Override
	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
	}

	@Override
	void modelMetamorphosisStarting(MetamorphosisEvent event) {
	}

	@Override
	void modelMetamorphosisFinished(MetamorphosisEvent event) {
	}

	public Map<Dlid, DLObject> getNodes(){
		return nodes
	}

	public Map<Dlid, DLObject> getNetworks(){
		return network
	}

	public Map<Dlid, DLObject> getDrawings(){
		return drawings
	}

	public void deleteNode(Dlid dlid){
		DLObject node = nodes.get(dlid)
		if (node.deleted){
			log.trace("Try to delete node that has already deleted!")
			return
		}
		deleteSensorsOfNode(node.dlid)
		if (node.hasKey()){
			node.deleted = true
		} else {
			nodes.remove(dlid)
			nodesToMac.remove(node)
		}
	}

	public void deleteAllNodes(){
		def nodesToRemove = []
		for (Dlid dlid : nodes.keySet()){
			DLObject node = nodes.get(dlid)
			if (node.deleted){
				log.trace("Try to delete node that has already deleted!")
				continue
			}
			if (node.hasKey()){
				node.deleted = true
			} else {
				nodesToRemove.add(node)
			}
		}
		for (def node : nodesToRemove){
			deleteSensorsOfNode(node.dlid)
			nodes.remove(node.dlid)
			nodesToMac.remove(node)
		}
	}

	private void deleteSensorsOfNode(Dlid dlid){
		def node = nodes.get(dlid)
		if (node){
			node.getChildren().each {
				if (it.hasKey()){
					it.deleted = true
				} else {
					sensors.remove(it.dlid)
				}
			}
		}
	}

	public void undeleteNode(Dlid dlid){
		DLObject node = nodes.get(dlid)
		if (!node.deleted){
			log.trace("Try to undelete node that not deleted!")
			return
		}
		node.deleted = false;
	}

	public void addNode(PlatformType type, String macId, String networkName){
		log.trace("Try to add new Node to AccessModel: Type = $type MAC ID = $macId Network = $networkName")
		def network = network.values().find {it.name.equals(networkName)}
		if (network){
			createNode(type, macId, network)
		} else {
			log.error("Parent network not found in Access Model")
		}
	}

	public boolean isExist(String macId){
		Long mac = convertMacToLong(macId)
		return nodesToMac.values().contains(mac)
	}

	public boolean isDirty(){
		for (DLObject node : nodes.values()){
			if (!node.hasKey() || node.deleted){
				return true
			}
		}
		return false
	}


	public static String convertLongToMac(Long l){
		String mac = Long.toHexString(l)
		if (mac.length()<16){
			mac = "0"*(16-mac.length())+mac
		}
		return mac.toUpperCase()
	}

	public static Long convertMacToLong(String macId){
		Long mac
		try{
			mac = Long.parseLong(macId, 16)
		} catch (NumberFormatException e){
			throw new IllegalArgumentException("Incorrect MAC ID: $macId")
		}
		return mac
	}
}
