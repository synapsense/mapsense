
package DeploymentLab.Model

import DeploymentLab.CentralCatalogue
import DeploymentLab.NamePair
import DeploymentLab.channellogger.*
import DeploymentLab.DeploymentLabVersion
import DeploymentLab.Pair
import DeploymentLab.ComponentLibrary

import DeploymentLab.Security.LogicalId
import DeploymentLab.Exceptions.UnknownComponentException
import DeploymentLab.ProjectSettings
import DeploymentLab.Exceptions.DuplicateObjectException
import DeploymentLab.Exceptions.IncompatibleTypeException
import DeploymentLab.Exceptions.ModelCatastrophe
import com.google.gag.annotation.disclaimer.HandsOff
import com.google.gag.enumeration.Consequence
import DeploymentLab.Image.BackgroundImageObserver
import DeploymentLab.Exceptions.ProjectFileCatastrophe

public class ComponentModel {
	private static final Logger log = Logger.getLogger(ComponentModel.class.getName())

	public final String version = DeploymentLabVersion.getComponentsVersion()

	private ObjectModel oModel

	private changeListeners = []

//	private componentTypes = [:]		// Map component type --> xml type
	private components = []
//	private oMap = [:]			// Map DLObject --> DLComponent of Component roots
//	private bMap = [:]			// Map DLObject --> DLComponent of Componnet binding posts
	private LogicalId lid

	private Map<DLObject,DLComponent> oMap = new HashMap<DLObject,DLComponent>()
	private Map<DLObject,DLComponent> bMap = new HashMap<DLObject,DLComponent>()
	Map<String,Object> componentTypes = new HashMap<String,Object>()

	// Map of Drawing -> Orphanage
    private Map<DLComponent,DLComponent> orphanageMap

    // Map of Component DLID -> Component
    private Map<Dlid,DLComponent> dlidMap = new HashMap<Dlid,DLComponent>()

	private ComponentLibrary cLib

    // Model state used together with the fireMetamorphosis events so that each listener doesn't need to maintain this state.
    // This also solves listener timing issues where, for example, fireMetamorphosisFinished is called and a listener
    // does something that triggers an event in another listener that still thinks the model is being changed because
    // it hasn't yet received the fireMetamorphosisFinished event.
    // It's a stack so we can keep track of all start/stop pairs performed during an operation to minimize listener events.
    private Deque<ModelState> currentState = new ArrayDeque<>()

	//load and attach model observers:
	BackgroundImageObserver imageWatcher = new BackgroundImageObserver()
	InspectorNameObserver inspectorNameObserver = new InspectorNameObserver()

	protected ComponentModel(){}

	ComponentModel(ObjectModel _o, def cXml) {
        currentState.push( ModelState.STABLE )
		oModel = _o
		cXml.component.each{ typeXml ->
			String typeName = typeXml.@type.text()
			log.trace("Loading configuration '$typeName'")
			componentTypes[typeName] = typeXml
		}
		lid = new LogicalId(this, oModel)
		this.addModelChangeListener(imageWatcher)
		this.addModelChangeListener(inspectorNameObserver)
	}

	public LogicalId getLid(){
		return lid
	}

	public setCLib( ComponentLibrary _clib ){
		cLib = _clib;
	}

	void loadComponent(def cXml){
		String typeName = cXml.@type.text()
		if(componentTypes.containsKey(typeName)) {
			log.warn("Configuration '$typeName' is being replaced by the loading component libarary.")
		}
		log.trace("Loading configuration '$typeName'")
		componentTypes[typeName] = cXml
	}
	
	
	DLComponent newComponent(DLComponent toClone) {
		DLComponent newComponent = newComponent(toClone.type)
		for(def p : newComponent.listProperties() ){
			if(toClone.getPropertyValue(p) != null){
				//log.trace("property:" + p)
				//log.trace("value:" + toClone.getPropertyValue(p))
				newComponent.setPropertyValue(p, toClone.getPropertyValue(p))
			}
		}

        // copy child components properties
        if(newComponent.hasRole('staticchildplaceable')){
            Map<String, Map<String, String>> childrenMap = newComponent.getPlaceableChildrenMap();
            for (String childName: childrenMap.keySet()) {
                DLComponent oldChildComponent = toClone.getChildComponents().find {child -> child.getPropertyValue("child_id").equals(childName)}
                DLComponent newChildComponent = newComponent.getChildComponents().find {child -> child.getPropertyValue("child_id").equals(childName)}

                newChildComponent.listProperties().each { p ->
                    if(oldChildComponent.getPropertyValue(p) != null)
                    //log.trace("property:" + p)
                   // log.trace("value:" + toClone.getPropertyValue(p))
                    newChildComponent.setPropertyValue(p, oldChildComponent.getPropertyValue(p))
                }

            }
        }
        return newComponent
	}

	DLComponent newComponent(String type) {
		//if(!componentTypes.containsKey(type))
		if(! isLoadedComponentType(type) ){
			throw new UnknownComponentException("Unknown component type $type")
		}
		//return deserializeComponent(componentTypes[type])
		DLComponent c = deserializeComponent(componentTypes[type])

		//thread in the projectSettings configurable defaults
		def ps = ProjectSettings.getInstance()
		ps.configuredDefaults.each{ cd ->
			if( cd.a.equals(type) ){
				log.trace("Installing configured default into new component: $cd")
				c.getObject(cd.b).getObjectProperty(cd.c).setValue(cd.d)
			}
		}

          // staticchildplaceable component should create child components
        if(c.hasRole('staticchildplaceable')){
           // log.trace('new component staticchild')
            newStaticChildPlaceableComponent(c)
        }

		return c
	}

	private DLComponent deserializeComponent(def cXml, ComponentLibrary componentLibrary) {
		DLComponent c = new DLComponent(this, oModel, cXml)
		log.trace("Loading ${c.getDisplaySetting("name") }...", "progress")
		components += c
		dlidMap[c.getDlid()] = c
		updateComponentParts(c)

		if(!isLoadedComponentType(c.getType())){
			//assume that isLoadedComponentType() handles any backcompat loading
/*
			//here, what we want to do is gin up a replacement component prototype based on the xml we DO have
			//for the moment, this is NOT what we want to have happen, but I'm leaving this here as an example
			def componentTypeXml = componentLibrary.makeMeAPrototype(c)
			loadComponent(componentTypeXml)
			componentLibrary.loadConfigurationToTreeModel(componentTypeXml)//as a last step, this gets it into all the other trees, but not the palette
*/

			def componentTypeXml = componentLibrary.getComponentTypeXml(c.getType())
			if(componentTypeXml==null){
				// It's probably one that is no longer supported.
				c.setDisplaySetting('deprecated','true')
				componentLibrary.loadConfigurationToTreeModel(cXml)
			}else{
				loadComponent(componentTypeXml)
				componentLibrary.loadConfigurationToTreeModel(componentTypeXml)
			}
		}
		fireComponentAdded(c)
		return c
	}

	protected void removeComponentParts( DLComponent c, DLObject o ) {
		if( oMap[o] == c ) {
			oMap.remove(o)
		} else {
			log.debug("Trying to remove oMap[$o] from $c but the current mapping is for ${oMap[o]}")
		}
	}

	protected void removeComponentParts( DLComponent c, Producer producer ) {
		DLObject o = producer.getProducerObject()
		if( bMap[o] == c ) {
			bMap.remove(o)
		} else {
			log.debug("Trying to remove bMap[$o] from $c but the current mapping is for ${bMap[o]}")
		}
	}

	protected void removeComponentParts( DLComponent c, Consumer consumer ) {
		DLObject o = consumer.getConsumerObject()
		if( bMap[o] == c ) {
			bMap.remove(o)
		} else {
			log.debug("Trying to remove bMap[$o] from $c but the current mapping is for ${bMap[o]}")
		}
	}

	protected void removeComponentParts( DLComponent c, Conducer conducer ) {
		removeComponentParts( c, conducer.getConsumer() )
		removeComponentParts( c, conducer.getProducer() )
	}

	protected void updateComponentParts(DLComponent c){

		for(DLObject o: c.getRoots()) {
			if(oMap.containsKey(o)) {
				if(oMap[o] == c)
					continue
				String message = "Object '$o' is already owned by '${oMap[o]} and cannot be owned by '$c'!"
				log.fatal(message)
				throw new Exception(message)
			}
			oMap[o] = c
		}
		for(String pId: c.listProducers(true)) {
			DLObject o = c.getProducerObject(pId)
			if(bMap.containsKey(o)) {
				if(bMap[o] == c)
					continue
				String message = "Object '$o' is already owned by '${bMap[o]} and cannot be owned by '$c'!"
				log.fatal(message)
				throw new Exception(message)
			}
			bMap[o] = c
		}
		for(String cId: c.listConsumers(true)) {
			DLObject o = c.getConsumerObject(cId)
			if(bMap.containsKey(o)) {
				if(bMap[o] == c)
					continue
				String message = "Object '$o' is already owned by '${bMap[o]} and cannot be owned by '$c'!"
				log.fatal(message)
				throw new Exception(message)
			}
			bMap[o] = c
		}

//		if(!isLoadedComponentType(c.getType())){
//			def componentTypeXml = componentLibrary.getComponentTypeXml(c.getType())
//			if(componentTypeXml==null){
//				componentLibrary.loadConfigurationToTreeModel(cXml)
//			}else{
//				loadComponent(componentTypeXml)
//				componentLibrary.loadConfigurationToTreeModel(componentTypeXml)
//			}
//		}

		//fireComponentAdded(c)
		//return c
	}

	protected DLProxy deserializeComponentProxy(def cXml, DLComponent anchor) {
		DLProxy c = new DLProxy(this, oModel, cXml, anchor)
		log.trace("Loading Proxy For ${c.getDisplaySetting("name") }...", "progress")
		components += c
		dlidMap[c.getDlid()] = c

        for(DLObject o: c.getRoots()) {
            if(oMap.containsKey(o)) {
                if(oMap[o] == c)
                    continue
                String message = "Object '$o' is already owned by '${oMap[o]} and cannot be owned by '$c'!"
                log.fatal(message)
                throw new Exception(message)
            }
            oMap[o] = c
        }

/*
		for(String pId: c.listProducers(true)) {
			DLObject o = c.getProducerObject(pId)
			if(bMap.containsKey(o)) {
				if(bMap[o] == c)
					continue
				String message = "Object '$o' is already owned by '${bMap[o]} and cannot be owned by '$c'!"
				log.fatal(message)
				throw new Exception(message)
			}
			bMap[o] = c
		}
		for(String cId: c.listConsumers(true)) {
			DLObject o = c.getConsumerObject(cId)
			if(bMap.containsKey(o)) {
				if(bMap[o] == c)
					continue
				String message = "Object '$o' is already owned by '${bMap[o]} and cannot be owned by '$c'!"
				log.fatal(message)
				throw new Exception(message)
			}
			bMap[o] = c
		}
*/

		fireComponentAdded(c)
		return c
	}

	private DLComponent deserializeComponent(def cXml) {
		DLComponent c = new DLComponent(this, oModel, cXml)
		components += c
		dlidMap[c.getDlid()] = c

		for(DLObject o: c.getRoots()) {
			if(oMap.containsKey(o)) {
				if(oMap[o] == c)
					continue
				String message = "Object '$o' is already owned by '${oMap[o]} and cannot be owned by '$c'!"
				log.fatal(message)
				throw new Exception(message)
			}
			oMap[o] = c
		}
		for(String pId: c.listProducers(true)) {
			DLObject o = c.getProducerObject(pId)
			if(bMap.containsKey(o)) {
				if(bMap[o] == c)
					continue
				String message = "Object '$o' is already owned by '${bMap[o]} and cannot be owned by '$c'!"
				log.fatal(message)
				throw new Exception(message)
			}
			bMap[o] = c
		}
		for(String cId: c.listConsumers(true)) {
			DLObject o = c.getConsumerObject(cId)
			if(bMap.containsKey(o)) {
				if(bMap[o] == c)
					continue
				String message = "Object '$o' is already owned by '${bMap[o]} and cannot be owned by '$c'!"
				log.fatal(message)
				throw new Exception(message)
			}
			bMap[o] = c
		}
		
		fireComponentAdded(c)
		return c
	}

    /**
	 * Indicates if the model is in the middle of changing significantly and is not in a stable state. This becomes true
     * just before firing the {@link ModelChangeListener#modelMetamorphosisStarting} notification and will become false
     * just before firing the {@link ModelChangeListener#modelMetamorphosisFinished} notification.
     *
	 * @see ModelState
     *
     * @return  True if the model is not in a stable state.
     */
	public boolean isMetamorphosizing(){
		return currentState.peek() != ModelState.STABLE
	}

    /**
     * Returns the current modification state of the model.
     * @return The current model state.
     */
    public ModelState getCurrentState() {
        return currentState.peek()
    }

    /**
     * Convenience method for checking if the model has started an operation for the provided state but has not yet
     * finished it. A given operation is defined as all the work performed between the model leaving the stable state
     * and returning to the stable state. This method essentially checks the state stack maintained by
     * {@link #metamorphosisStarting} and {@link #metamorphosisFinished} to see if the provided state exists anywhere
     * in the stack.
     *
     * @param state  The state to check.
     *
     * @return  True if the provided state is in the state stack for the current operation.
     */
    public boolean hasActiveState( ModelState state ) {
        return currentState.contains( state )
    }

	// NOTE: except for these methods, ComponentModel doesn't have anything to do with the component xml structure
	// These methods are required to allow reflection on the component model before creating instances

	public Set<String> listManagedObjectTypes(String componentType) {
		if(componentTypes.containsKey(componentType)){
			//return componentTypes[componentType].object.collect{ it.@type.text() }

			//since roots are sometimes now not the top of the object tree in a component (thanks, integrated COINs)
			//we need to look for any object with an 'as' name, rather than just the top layer
			//the '**' operator walks through all the descendant nodes of the GPathResult
			Set<String> results = new HashSet<String>()
			results.addAll( componentTypes[componentType].object.collect{ it.@type.text() } )
			this.componentTypes[componentType].'**'.each{
				if( it.name() == 'object' ) {
					String otype = it.@type.text()
					if( !results.contains( otype ) && canPromoteToRoot( otype, it.@as.text() ) ) {
						results.add( otype )
					}
				}
			}
			return results
		}

		log.error("listManagedObjectTypes: Component type '$componentType' not loaded!")
		return [] as Set
	}

	boolean canPromoteToRoot( String objType, String asName ) {
		if( asName != null && !asName.isEmpty() ) {
			// todo: This is a bit of a hack filter. The above is picking up a couple things, like controlout_damper that we're
			//       not sure we want to have promoted. Until it can be analyzed, only explicitly promote those we know are ok.
			if( objType.equals('wsnnode') || objType.startsWith('controlpoint_') ||  objType.startsWith('controlalg_') ) {
				return true
			}
		}

		return false
	}

	def listChildObjectTypes(String componentType, boolean deep = false) {
        return listManagedObjectTypes(componentType)?.collect{ oModel.listChildObjectTypes(it, deep) }.flatten().unique()
	}

	def listParentComponentsOfType(String childComponentType) {
		def childObjectTypes = listManagedObjectTypes(childComponentType)
		if(childObjectTypes==null)
			return

		return componentTypes.keySet().findAll{ parent -> listChildObjectTypes(parent)!=null && childObjectTypes.intersect(listChildObjectTypes(parent)).size() > 0 }
	}

	/**
	 * Finds the kind of grouping the given type requires.
	 */
	def listGroupings(String componentType, boolean excludeOptional = false){
		def grouping = []
		this.listTypesWithRole("zone").each{ zone->
			// Optionally exclude groups that are not required. This is typically those with the role 'logicalgroup'.
			if( !excludeOptional || ( zone != ComponentType.PLANNING_GROUP ) ) {
				if ( this.typeCanBeChild(zone, componentType) ) {
					grouping += zone
				}
			}
		}
	    return grouping
	}

	/**
	 * Finds the kind of datasources the given type requires. 
	 */
	def listDatasources(String componentType){
		def ds = []
		this.listTypesWithRole("network").each{ network->
			if ( this.typeCanBeChild(network, componentType, true) ) {
				ds += network
			}
		}
	    return ds
	}

	/**
	 * Returns true if childType can be a child of parentType.
	 */
    Boolean typeCanBeChild(String parentType, String childType, boolean deep = false){
        def result = false
        if ( ! this.listChildObjectTypes(parentType, deep).disjoint( this.listManagedObjectTypes(childType) ) ) {
            result = true
        }
        return result
    }

	@Deprecated
	def listComponentClasses(String componentType) {
		return this.listComponentRoles(componentType)
	}

	private Map<String,Set<String>> componentRoles = new HashMap<String, Set<String>>()

	Set<String> componentRolesAsSet(String componentType) {
		if(!componentRoles.containsKey(componentType)){
			if(componentTypes.containsKey(componentType)){
				componentRoles[componentType] = new HashSet<String>()
				componentRoles[componentType].addAll(componentTypes[componentType].@classes.text().split(',').collect{it})

			}else{
				log.error("componentRolesAsSet: Component type '$componentType' not loaded!")
				return null
			}
		}
		return componentRoles[componentType]
	}

	List<String> listComponentRoles(String componentType) {
		def s = this.componentRolesAsSet(componentType)
		if(s){
			def result = new ArrayList<String>(s.size())
			result.addAll(s)
			return result
		}
		return []
	}

	Set<String> listComponentTypes(){
		return ( componentTypes.keySet() )
	}

    def getComponentType( String name ) {
        return componentTypes[ name ]
    }

	def listTypesWithRole( String roleNme ){
		def result = new ArrayList<String>()
		for(def ct: this.listComponentTypes()){
			if (  this.componentRolesAsSet(ct).contains(roleNme) ){
				result += ct
			}
		}
		return ( result )
	}

	List<String> listTypeProducerDatatypes( String componentType ) {
		//<xml>.producer.each{ p.@datatype.text()
		if(componentTypes.containsKey(componentType)) {
			//return componentTypes[componentType].@classes.text().split(',').collect{it}
			def pd = []
			componentTypes[componentType].producer.each{ p->
				pd += p.@datatype.text()
			}
			return pd
		} else {
			log.error("listTypeProducerDatatypes: Component type '$componentType' not loaded!")
			return []
		}
	}

	/**
	 * Investigates a given component type to find the datatype of its consumers
	 * @param componentType the component type name to investigate
	 * @return a list of association datatype that componentType has as consumers
	 *
	 */
	List<String> listTypeConsumerDatatypes( String componentType ) {
		//<xml>.producer.each{ p.@datatype.text()
		if(componentTypes.containsKey(componentType)) {
			//return componentTypes[componentType].@classes.text().split(',').collect{it}
			def pd = []
			componentTypes[componentType].consumer.each{ c ->
				pd += c.@datatype.text()
			}
			return pd
		} else {
			log.error("listTypeConsumerDatatypes: Component type '$componentType' not loaded!")
			return []
		}
	}

	/**
	 * Investigates a given component type to find the datatype of its conducers. Since Conducers don't have their own
	 * type and are really a composite type of its Producer and Consumer, a Pair is returned for each where Pair.a is
	 * the Producer and Pair.b is the Consumer.
	 *
	 * @param componentType the component type name to investigate
	 * @return a list of association datatype that componentType has as conducer
	 */
	List<Pair> listTypeConducerDatatypes( String componentType ) {
		if(componentTypes.containsKey(componentType)) {
			def pd = []
			componentTypes[componentType].conducer.each{ c ->
				pd += new Pair( c.producer.@datatype.text(), c.consumer.@datatype.text() )
			}
			return pd
		} else {
			log.error("listTypeConducerDatatypes: Component type '$componentType' not loaded!")
			return []
		}
	}

	String getTypeDisplayName( String componentType ){
		if(componentTypes.containsKey(componentType)) {
			def name = componentTypes[componentType].display.name.text()
			return name
		} else {
			log.error("listTypeProducerDatatypes: Component type '$componentType' not loaded!")
			return ''
		}
	}

	String getTypeDisplayEntry( String componentType, String displaySetting ){
		if(componentTypes.containsKey(componentType)) {
			def thingie = componentTypes[componentType].display[displaySetting].text()
			return thingie
		} else {
			log.error("getTypeDisplayEntry: Component type '$componentType' not loaded!")
			return ''
		}
	}

	String getTypeAttribute( String componentType, String typeAttribute ){
		if(componentTypes.containsKey(componentType)) {
			def thingie = componentTypes[componentType]["@$typeAttribute"].text()
			return thingie
		} else {
			log.error("getTypeAttribute: Component type '$componentType' not loaded!")
			return ''
		}
	}

	/**
	 * Finds the default value for a component property.
	 * The default value is the value that the property would have on a newly instantiated component.
	 * @param componentType the type of a component.  Must already be loaded if from a component library.
	 * @param propertyName the internal (not display) name of a property
	 * @return the String representation of the default value as in the XML definition,
	 * OR "" if the property or type cannot be found
	 */
	String getTypeDefaultValue( String componentType, String propertyName ){
		if(componentTypes.containsKey(componentType)) {
			//def thingie = componentTypes[componentType]["@$typeAttribute"].text()
			//return thingie

			def thingie = ""
			// Thanks to parcels, we need to do a deep search for properties.
			//componentTypes[componentType].property.each{ p ->
			componentTypes[componentType].'**'.findAll { it.name() == 'property' }.each{ p ->
				if ( p["@name"].equals( propertyName ) ){
					thingie = p.text()
				}
			}
			return thingie
		} else {
			log.error("getTypeDefaultValue: Component type '$componentType' not loaded!")
			return ""
		}
	}

	def getTypeProp( String componentType, String propertyName ){
		if(componentTypes.containsKey(componentType)) {
			def thingie = null
			// Thanks to parcels, we need to do a deep search for properties.
			componentTypes[componentType].'**'.findAll { it.name() == 'property' }.each{ p ->
				if ( p["@name"].equals( propertyName ) ){
					thingie = p
				}
			}
			return thingie
		} else {
			log.error("getTypeProp: Component type '$componentType' not loaded!")
			return null
		}
	}

	Map<String,String> getTypeDisplayPropNames(String componentType){
		if(componentTypes.containsKey(componentType)) {

			def thingie = [:]
			// Thanks to parcels, we need to do a deep search for properties.
			//componentTypes[componentType].property.each{ p ->
			componentTypes[componentType].'**'.findAll { it.name() == 'property' }.each{ p ->
				if ( p["@displayed"].equals( true ) ){
					thingie.put(p["@name"].text(), p["@display"].text())
				}
			}
			return thingie
		} else {
			log.error("getTypeDisplayPropNames: Component type '$componentType' not loaded!")
			return [:]
		}
	}

	List<String> getComponentTypesForFilter(String filterName){
		List result = []
		for ( String type : componentTypes.keySet() ){
			List current = []
			current = this.getTypeAttribute(type, "filters").split(",")
			if ( current.contains(filterName)) {
				result += type
			}
		}
		return result
	}
	
	String listStaticChildTypeForType(String parentType){
		String result
		if(componentTypes.containsKey(parentType)) {
			result = componentTypes[parentType].children.@type.text()
		} else {
			log.error("listStaticChildTypeForType: Component type '$parentType' not loaded!")
		}
		return result
	}
	

	// End model reflective methods

	// check if a component type is loaded or not
	boolean isLoadedComponentType(String componentType){
		if(componentTypes.containsKey(componentType)){
			return true
		} else {
			//return false
			//before admitting failure, see if we can find it in the backcompat path
			def componentTypeXml = cLib.getComponentTypeXml(componentType)
			if(componentTypeXml==null){
				return false
			}else{
				loadComponent(componentTypeXml)
				cLib.loadConfigurationToTreeModel(componentTypeXml )
				return true
			}
		}
	}
	
	void remove(DLComponent c) {
        if ( ! components.contains(c) ){
			log.trace("Trying to remove Component $c which has already been removed!")
			return
		}


		log.trace("Entering remove component ($c)")

        c.setDeleting(true)

        log.trace("Removing proxies ($c)")
        for( DLComponent proxy : c.getProxies() ) {
            remove( proxy )
        }

		//remove any "secret" children from control crahs before deleting one
		if ( c.getType().equals("control_crah_trim") ){
			c.getChildComponents().each{ cc ->
				log.trace("removing $cc from $c")
				c.removeChild(cc)
			}
		}

		log.trace("Clearing out consumers ($c)")
		c.clearConsumers()
		log.trace("Clearing out producers ($c)")
		c.clearProducers()

        def staticChildren = []
        if(c.hasRole("staticchildplaceable")){
            // should not delete children component first
            // for undo operation, delete parent first and then delete children.
            staticChildren = c.getChildComponents()

        } else if( c.hasRole( ComponentRole.LOGICAL_GROUP ) || ( c.getType() == ComponentType.ROOM && !hasActiveState(ModelState.CLEARING) ) ) {
	        // Free our children rather than delete them.
	        for( DLComponent kid : c.getChildComponents() ) {
		        c.removeChild( kid )
	        }
        }else{
            c.getChildComponents().each{ child ->
	            // Do orphanages last since they could get created/used when deleting other things.
	            if( child.getType() != ComponentType.ORPHANAGE ) {
		            remove(child)
	            }
            }

	        // Now get rid of the leftovers... which should just be the orphanages.
	        c.getChildComponents().each{ child ->
		        remove(child)
	        }
        }


		log.trace("removing self from parent components ($c)")
		for(DLComponent parent: c.getParentComponents()) {
            if(parent!=null)
			    parent.removeChild(c)
		}

		fireComponentRemoved(c)

		for(DLObject o: c.roots) oMap.remove(o)
		for(String pId: c.listProducers(true)) bMap.remove(c.getProducerObject(pId))
		for(String cId: c.listConsumers(true)) bMap.remove(c.getConsumerObject(cId))
		dlidMap.remove( c.getDlid() )
		if( ( orphanageMap != null ) && ( c.getType() == ComponentType.ORPHANAGE ) ) {
			// Unfortunately, this needs to be a search by value.
			Iterator<Map.Entry<DLComponent,DLComponent>> iter = orphanageMap.entrySet().iterator()
			while( iter.hasNext() ) {
				if( c == iter.next().getValue() ) {
					iter.remove()
					break
				}
			}
		}
		c.remove()
		components -= c

		ProjectSettings.getInstance().removeDlidMapping( c.getDlid() )

        if(staticChildren.size()>0){
			log.trace("removing static children")
            staticChildren.each{remove(it)}
        }

		log.trace("exiting remove($c)")
	}

	void undelete(DLComponent c) {
		log.trace("entering undelete($c)")
		c.setDeleting(false)
      	c.undelete()
		components += c
		dlidMap[c.getDlid()] = c
		c.roots.each{ o -> oMap[o] = c }
		c.listProducers(true).each{ pId -> if(c.getProducerObject(pId) != null) bMap[c.getProducerObject(pId)] = c}
		c.listConsumers(true).each{ cId -> if(c.getConsumerObject(cId) != null) bMap[c.getConsumerObject(cId)] = c}
		fireComponentAdded(c)
      	log.trace("exiting undelete($c)")
	}

	void clear() {
        metamorphosisStarting( ModelState.CLEARING )
		log.trace("entering clear()")
		getRoots().each{ remove(it) }
		if(components.size() > 0 || oMap.size() > 0 || bMap.size() > 0) {
			log.error("Clearing the ComponentModel was unsuccessful!")
			/*
            if(components.size()>0){
                log.trace("compoent size is not zero")
                components.each{log.trace(it)}
            }
            */
			log.error("components: " + components)
			log.error("oMap: " + oMap)
			log.error("bMap: " + bMap)
			throw new ModelCatastrophe("Clear Model left things behind!")
		}
		oModel.clear()
		lid = new LogicalId(this, oModel) //reset the lid assigner
        metamorphosisFinished( ModelState.CLEARING )
		log.trace("exiting clear()")
	}

	void serialize(groovy.xml.MarkupBuilder builder, boolean serializeImages = true, boolean includeAll = true) {
		builder.components('version': version) {
			for(def c : components){
				try{
					if( includeAll || c.isExportable() ) {
						c.serialize(builder, serializeImages)
					} else {
						log.trace("Skipping serialization for $c")
					}
				} catch (Exception e) {
					log.error("exception while saving component $c")
					throw new ProjectFileCatastrophe("Serialization error on component $c", e)
				}
			}
		}
	}

	void deserialize(def componentsXml, ComponentLibrary _componentLibrary) {

		String v = componentsXml.@version.text()
		log.info("Loading component model version $v")
		if(v != version)
			throw new Exception("Incompatible version number '$v' should be one of " + version + "!")

        metamorphosisStarting( ModelState.INITIALIZING )

		log.trace("Starting model deserialization")
		componentsXml.component.each{ cXml ->
			deserializeComponent(cXml, _componentLibrary)
		}

        // Keep proxies after components since they need to reconnect with their anchor, which needs to exist already.
		componentsXml.proxy.each{ cXml ->
            DLComponent anchor = getComponentFromDLID( Dlid.getDlid( cXml.anchor.@value.text() ) )
            deserializeComponentProxy( cXml, anchor )
		}

		log.trace("Firing childAdded events")
		for(DLComponent root: roots) {
			log.trace("Adding children for ${root.getName()}...", "progress")
			recChildEvents(root)
		}

		log.trace("Firing associationAdded events")
		for(DLComponent c: components) {
			log.trace("Associating component ${c.getName()}...", "progress")

			//for(String consumerId: c.listConsumers()) {
			//	Consumer consumer = c.getConsumer(consumerId)
			for(Consumer consumer : c.getAllConsumers() ){
				for(Pair<DLComponent,String> oip: consumer.listProducerComponents()) {
					fireAssociationAdded(oip.a, oip.b, c, consumer.getId() )//consumerId)
				}
			}

			//same for conducers
			def conducerCheck = []
			for ( Conducer d : c.getAllConducers() ){
				Consumer consumer = d.getConsumer()

				for(Pair<DLComponent,String> oip: consumer.listProducerComponents()) {

					if ( ! oip in conducerCheck ){
						fireAssociationAdded(oip.a, oip.b, c, consumer.getId())
						conducerCheck += oip
					}
				}
			}
		}

        metamorphosisFinished( ModelState.INITIALIZING )
	}

    /**
     * Notifies the Component Model that it is about to be significantly modified enough that listeners may decide to
     * wait until all changes are complete before reacting.
     *
     * This method maintains a stack of the metamorphosis notifications and will only fire the listener event if this is
     * the first notification in a series. This allows activities that may result in multiple embedded calls to this
     * method to still result in only one modelMetamorphosisStarting event as the model is leaving the stable state.
     *
     * @param mode  Metamorphosis mode about to begin.
     */
    public void metamorphosisStarting( ModelState mode ) {
        ModelState oldState = currentState.peek()
        currentState.push( mode );
        if( oldState == ModelState.STABLE ) {
            fireMetamorphosisStarting( mode )
        }
    }

    /**
     * Notifies the Component Model that the action which caused significant modifications has completed.
     *
     * This method maintains a stack of the metamorphosis notifications and will only fire the listener event if
     * there are no more active metamorphosis actions in a series. This allows activities that may result in multiple
     * embedded calls to this method to still result in only one modelMetamorphosisFinished event as the model returns
     * to the stable state.
     *
     * @param mode  Metamorphosis mode that has just finished.
     */
	public void metamorphosisFinished( ModelState mode ) {
        if( currentState.peek() == ModelState.STABLE ) {
            log.debug("Got more metamorphosisFinished() than metamorphosisStarting: $mode  StateStack: $currentState")
        } else {
            if( currentState.pop() != mode ) {
                log.debug("Got a mismatched metamorphosisFinished: $mode  StateStack: $currentState")
            }
            if( currentState.peek() == ModelState.STABLE ) {
	            if( mode == ModelState.REPLACING ) {
		            // Clear some local caches since ids have been changed.
		            orphanageMap == null
	            }
                fireMetamorphosisFinished( mode )
            }
        }
    }

	private void recChildEvents(DLComponent root) {
		for( DLComponent child : root.getChildComponents() ) {
            //log.trace("recChildEvents: child [" + child + "] to " + root)
			fireChildAdded(root, child)
			recChildEvents(child)
		}
	}

	Set<DLComponent> getRoots() {
		Set<DLComponent> rv = [] as Set
		for(DLObject root: oModel.getRoots()) {
			DLComponent owner = getOwner(root)
			if(owner != null)
				rv.add(owner)
		}
		return rv
	}

	/**
	 * Returns a list of all DLComponents in the project.
	 * @return An ArrayList of DLComponents.
	 */
	ArrayList<DLComponent> getComponents() {
		return components
	}

	ArrayList<DLComponent> getNonProxyComponents() {
		// Clone or proxies are removed from the master list!
		return stripProxies( components.clone() )
	}


	/**
	 * Removes all Proxy components from the given collection. This does not return a stripped copy, but modifies the
	 * collection directly. For convenience, the same collection is also returned.
	 *
	 * @param components  The collection of components to strip.
	 * @return The same collection object, with proxies removed.
	 */
	public static java.util.Collection<DLComponent> stripProxies( java.util.Collection<DLComponent> components ) {
		Iterator<DLComponent> it = components.iterator()
		while( it.hasNext() ) {
			if( it.next().hasRole("proxy") ) {
				it.remove()
			}
		}

		return components
	}

	/**
	 * Gets all the components in the specified drawing
	 * @param drawing the drawing we care about
	 * @return a List of components on that drawing
	 */
	ArrayList<DLComponent> getComponentsInDrawing(DLComponent drawing) {
		def result = new ArrayList<DLComponent>()
		for(DLComponent c : components){
			if(c.getDrawing().equals(drawing)){
				result.add(c)
			}
		}
		return result
	}


	//todo: these getComponentsBy<whatever> methods get called a lot; we need to do something to speed them up:
	//sorted data structure, index, cache, something.

	/**
	 * Returns a list of all DLComponents in the project whose types begin with the specified prefix.
	 *
	 * @param typePrefix  The start of the type name to search for.
	 * @param forDrawing  Filter the results to be only components on the same Drawing as this parameter's Drawing.
	 *
	 * @return  A list of components whose types begin with the prefix specified.
	 */
	ArrayList<DLComponent> getComponentsByTypePrefix( String typePrefix, DLComponent forDrawing = null ) {
		List<DLComponent> build = []
		for(DLComponent c : components){
			if(c.getType().startsWith(typePrefix)){
				if( ( forDrawing == null ) || ( c.getDrawing() == forDrawing ) ) {
					build.add(c)
				}
			}
		}
		return new ArrayList<DLComponent>(build)
	}

	/**
	 * Returns a list of all DLComponents in the project with the specified type.
	 *
	 * @param type Component type
	 * @param forDrawing  Filter the results to be only components on the same Drawing as this parameter's Drawing.
	 *
	 * @return  A list of components of the provided type.
	 */
	ArrayList<DLComponent> getComponentsByType( String type, DLComponent forDrawing = null ) {
		List<DLComponent> build = []
		for(DLComponent c : components){
			if (c.getType().equals(type)) {
				if( ( forDrawing == null ) || ( c.getDrawing() == forDrawing.getDrawing() ) ) {
					build.add(c)
				}
			}
		}
		return new ArrayList<DLComponent>(build)
	}

	/**
	 * Returns a list of all DLComponents in the project whose types begin with the specified prefix, excluding Proxy components.
	 * @param typePrefix The start of the typename to search for
	 * @return An ArrayList of DLComponents whose types begin with the prefix specified and are not Proxies.
	 */
	ArrayList<DLComponent> getNonProxyComponentsByType(String cType ) {
		return stripProxies( getComponentsByType( cType ) )
	}

	ArrayList<String> getComponentTypesByProducerDatatype(String datatype){
		def types = []
		this.listComponentTypes().each{ type ->
			if ( this.listTypeProducerDatatypes(type).contains(datatype) ){
				types += type
			}
		}
		return types
	}

	ArrayList<DLComponent> getComponentsByProducerDatatype(String datatype){
		def results = []
		def types = getComponentTypesByProducerDatatype( datatype )
		types.each{ t ->
			this.getComponentsByType(t).each{ c->
			    results += c
			}
		}
		return ( results )
	}

	ArrayList<String> getComponentTypesByConsumerDatatype(String datatype){
		def types = []
		this.listComponentTypes().each{ type ->
			if ( this.listTypeConsumerDatatypes(type).contains(datatype) ){
				types += type
			}
		}
		return types
	}

	ArrayList<String> getComponentTypesByConducerDatatype( Pair datatype ){
		def types = []
		this.listComponentTypes().each{ type ->
			for( Pair conducerType : this.listTypeConducerDatatypes(type) ) {
				if(( conducerType.a == datatype.a ) && (conducerType.b == datatype.b ) ) {
					types += type
					break
				}
			}
		}
		return types
	}

	/**
	 * Finds all components with the exact name given.
	 * This is here so we can remember to make this faster, and only do it once
	 * @param name
	 * @param forDrawing  (Optional) The drawing to search A Component whose drawing we want to check
	 * @return
	 */
	List<DLComponent> getComponentsByName(String name, DLComponent forDrawing = null){
		def result = []
		for(def c : this.getComponents()){
			if( ( c.getName() == name ) && (( forDrawing == null ) || ( c.getDrawing() == forDrawing.getDrawing() )) ) {
				result += c
			}
		}
		return result
	}

	/**
	 * Searches the component model for any and all components that have a field that matches the search term.
	 * If the searchField parameter is specified, only that property is searched (and components without that property are ignored.)
	 * Search is case insensitive and substring.
	 * Hidden fields are ignored.
	 * Groovy collection Closures converted to java for:each loops as those proved to be measurably faster.
	 * @param searchTerm The string to search the component model for.
	 * @param searchField The component property to search (if blank search all properties).  This must be the internal name of the property, not the display name.
	 * @param returnHidden true if hidden fields should be returned in search results
	 * @param selection an optional list of components to search on instead of the entire model (pass [] in to get the full search) 
	 * @return A List of search results.  Each result is Map<String,Object> with three keys: 'component', 'fieldname', and 'value'.
	 * ['component'] maps to the actual DLComponent object in the model that is the hit result.
	 * ['fieldname'] is a string of the display name of the property that is the hit result.
	 * ['value'] is a string of the contents of the property that is the hit result.
	 * @see DLComponent#mapPropertyNamesValues
	 * @see DeploymentLab.Dialogs.SearchDialog
	 *
	 * Spotlight now does property matching based on display name, not internal name, which is more accurate but can be slower
	 * (On a happy-design level, this shouldn't happen, but we all know that it will.)
	 *
	 * Also, Spotlight now searches on Producers and Consumers as well, listing the names of the linked components ( if any ) as the value. 
	 */
	public List<Map<String,Object>> spotlight(String searchTerm, String searchField, boolean returnHidden, List<DLComponent> selection) {
		List<Map<String,Object>> results = []
		searchTerm = searchTerm.toLowerCase().trim()
		if ( !selection || selection.size() == 0 ) {
			selection = components
		}
		for ( DLComponent c : selection ) {
			if ( searchField == null || searchField.trim().equalsIgnoreCase("") ) {
				for ( Map.Entry<String, DLComponent> entry : c.mapPropertyNamesValues(returnHidden).entrySet() ) {
					String currentValue = entry.getValue().toString().toLowerCase().trim()
					String currentName = entry.getKey().toString().toLowerCase().trim()
					//if ( currentValue.contains(searchTerm) || currentName.contains(searchTerm) ) {
					if ( searchCompare( searchTerm, currentValue ) || searchCompare( searchTerm, currentName ) ) {
							def thisHit = [:]
							thisHit['component'] = c
							thisHit['fieldname'] = entry.getKey()
							thisHit['value'] = entry.getValue()
							results += thisHit
					}
				}
			} else {
				//we're only searching on the searchField, so we get to cut some corners
				//on the other hand, we're searching by value, not key, so that slows us back down
				if ( c.hasPropertyNamed(searchField) ) {
					String currentValue = c.getPropertyNamedValue(searchField).toString()
					//if ( currentValue.toLowerCase().trim().contains( searchTerm ) ) {
					//note: currentValue has to be "search normalized" here so we can use the value in the variable in the thisHit hash
					if ( searchCompare( searchTerm, currentValue.toLowerCase().trim() ) ) {
						def thisHit = [:]
						thisHit['component'] = c
						thisHit['fieldname'] = searchField
						thisHit['value'] = currentValue
						results += thisHit
					}
				}
			}
		}
		return ( results )
	}

	/**
	 * Handles the search comparison when searching across properties.
	 * Simply, this searches for the presence of searchingFor in searchingIn.
	 * However, rather than doing a simple .contains() call, searchingFor is tokenized on whitespace,
	 * and each token is searched for in searchingIn separately.  A true result is returned if all tokens are present
	 * in searchingIn somewhere, but not necessarily contiguously or in order.
	 * @param searchingFor The search term to look for.  Tokenized on whitespace.
	 * @param searchingIn The string to look inside of.
	 * @return true if searchingIn contains all the tokens in searchingFor
	 */
	private boolean searchCompare( String searchingFor, String searchingIn ){
		boolean result = true //in case things really go haywire, we want true to come out
		int hits = 0
		//now, lets try that with some tokenizing
		def searches = searchingFor.tokenize()
		for ( String t : searches ){
			if ( searchingIn.contains( t ) ){
				hits ++
			}
		}
		//note: we want true for 0==0 here
		if ( hits == searches.size() ){
			result = true
		} else {
			result = false
		}
		return ( result )
	}

	/**
	 * Gets a list of all the names of all visible properties of all components in the model.
	 * The list is not sorted, but it is filtered for uniqueness.
	 * @param selection an optional list of components to use instead of the entire model
	 * @return a List of NamePair objects holding the display and internal names of all visible properties.
	 */
	public List<NamePair> getPropertyNames(List<DLComponent> selection, boolean simpleEditableOnly = false) {
		Set<NamePair> megaList = new TreeSet<NamePair>();
		//Set<String> seen = new HashSet<String>();

		if ( !selection || selection.size() == 0 ) {
			selection = components
		}
		for (DLComponent c : selection) {
			//if ( ! seen.contains( c.getType() ) ){
				megaList.addAll( c.listVisiblePropertyNamePairs(simpleEditableOnly) )
				//seen.add(c.getType())
			//}

		}
		return( new ArrayList(megaList) )
	}


	List<DLComponent> getComponentsInRole(String roleName, DLComponent forDrawing = null ) {
		def result = []
		for( DLComponent c : components ) {
			if( c.hasRole( roleName ) ) {
				if( ( forDrawing == null ) || ( c.getDrawing() == forDrawing.getDrawing() ) ) {
					result.add(c)
				}
			}
		}
		return result
	}

	/**
	 * Finds all the components with the given role, excluding Proxy components.
	 * @param roleName
	 * @return
	 */
	List<DLComponent> getNonProxyComponentsInRole(String roleName, DLComponent forDrawing = null ) {
		return stripProxies( getComponentsInRole( roleName, forDrawing ) )
	}

	/**
	 * Finds all the components that have any of the roles indicated
	 * @param roleNames
	 * @return
	 */
	Set<DLComponent> getComponentsInAnyRole(java.util.Collection<String> roleNames) {
		Set<DLComponent> result = new HashSet<DLComponent>()
		for (String role: roleNames) {
			result.addAll(getComponentsInRole(role))
		}
		return result
	}
	
	/**
	 * Returns a list of components that have all the roles listed
	 * @param roleNames
	 * @return
	 */
	List<DLComponent> getComponentsInRoles(java.util.Collection<String> roleNames){
		def result = []
		List<List<DLComponent>> firstLookup = []
		roleNames.each{ r ->
			firstLookup.add( this.getComponentsInRole(r) )
		}
		//println "rolenames: $roleNames"
		//println "firstlookup: $firstLookup"
		result.addAll(firstLookup[0])
		//firstLookup.each{ l ->
		for(int i = 1; i < firstLookup.size(); i ++){
			result = result.intersect(firstLookup[i])
		}
		//println result
		return result
	}

	/**
	 * Returns a list of components that have all the roles listed, excluding Proxy components.
	 * @param roleNames
	 * @return
	 */
	List<DLComponent> getNonProxyComponentsInRoles(java.util.Collection<String> roleNames) {
		return stripProxies( getComponentsInRoles( roleNames ) )
	}

	/**
	 * Get the Orphanage for the Drawing associated with the given component. If one does not exist, it will be
	 * created if <code>create</code> is true.
	 *
	 * @param comp            The component that needs an orphanage.
	 * @param createIfNeeded  If true (default) a new Orphanage will be created if one does not exist.
	 *
	 * @return The Orphanage or null if one was not found and <code>createIfNeeded</code> is false.
	 */
	public DLComponent getOrphanage( DLComponent comp, boolean createIfNeeded = true ) {

		// See if one already exists. Cache is used since this can get called a whole bunch.
		if( orphanageMap == null ) {
			// Build the initial Drawing -> Orphanage mapping.
			orphanageMap = new HashMap<DLComponent, DLComponent>()
			for( DLComponent orphanage : getComponentsByType( ComponentType.ORPHANAGE ) ) {
				orphanageMap.put( orphanage.getDrawing(), orphanage )
			}
		}

		DLComponent orphanage = orphanageMap.get( comp.getDrawing() )

		if( (orphanage == null) && createIfNeeded ) {
			// None found, so create one.
			orphanage = newComponent( ComponentType.ORPHANAGE )
			orphanage.setPropertyValue('name', "Orphanage")
			orphanage.setPropertyValue("points", "0,0;10,0;10,10;0,10");

			comp.getDrawing().addChild( orphanage )
			orphanageMap.put( comp.getDrawing(), orphanage )

			log.debug("Creating new Orphanage for : ${comp.getDrawing().name}")
		}

		return orphanage
	}

	/**
	 * Checks to see if an Orphanage exists for the drawing associated with the provided component.
	 *
	 * @param   The component to check for an associated orphanage.
	 * @return  True if the orphanage exists.
	 */
	public boolean hasOrphanage( DLComponent comp ) {
		return getOrphanage( comp, false ) != null
	}

	String getDisplaySetting(String configName, String setting) {
		String t = componentTypes[configName]?.display?."$setting"?.text()
		if(t == null)
			log.error("No display setting '$setting' for configuration '$configName'.")
		return t
	}

	/**
	* Try to find the "owner" of a DLObject.  Not all objects are owned.
	* Objects that are roots of components or exposed as a component binding site are considered "owned".
	* Generally, (always?) objects that are roots and objects that are binding sites will never be the same type of object.
	* Perhaps then they should be looked up via different methods?
	*/
	DLComponent getOwner(DLObject _o) {
		//StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace()
		//def filtered = stackTraceElements.findAll { !it.toString().contains("java.lang") && !it.toString().contains("groovy") && !it.toString().contains("sun")&& !it.toString().contains("closure") }
		//log.debug("getOwner($_o) called from: ${ stackTraceElements[0..10] }")
		//log.debug("getOwner($_o) called from: ${ filtered }")

		if(oMap.containsKey(_o)) {
			//log.trace("getOwner($_o) returning '${oMap[_o]}' from oMap")
			return oMap[_o]
		} else if(bMap.containsKey(_o)) {
			//log.trace("getOwner($_o) returning '${bMap[_o]}' from bMap")
			return bMap[_o]
		}
		//log.trace("getOwner($_o) returning null")

		//log.trace( log.getStackTrace(new Exception() ) )

		return null
	}

	/**
	 * Tries to find which component manages this DLObject, even if it is not a root of a componet.
	 * That is, this will find the component for sensors.
	 * @param o
	 * @return
	 */
	DLComponent getManagingComponent(DLObject o){
		DLComponent result = this.getOwner(o)
		if ( result != null ){ return result; }
		//if we made it this far, that means we gotta do some work.
		DLComponent parentComponent = null
		for( DLObject p : o.getParents() ) {
			DLComponent owner = getOwner(p)
			if( owner && owner.hasRole("placeable") ) {
				parentComponent = owner
				break
			}
		}
		return parentComponent
	}

	/**
	 * Resets image delta tracking.  Should be called whenever the omodel.clearKeys is.
	 *
	 */
	public void resetImages(){
		log.debug("Reset image tracking", "default")
		this.getComponentsByType( ComponentType.DRAWING ).each{ it.setPropertyValue("rect_spec", null) }
		this.getComponentsByType( ComponentType.ROOM ).each{ it.setPropertyValue("rect_spec", null) }
	}

	/**
	 * Compute and assign logical IDs to all components.
	 * Not used for production; this is intended as a testing function.
	 */
	void assignLogicalIDs(){
		def start = System.currentTimeMillis()
		def wsnnodes = oModel.getObjectsByType("wsnnode")
		def gatews = oModel.getObjectsByType("wsngateway")
		def total = wsnnodes.size() + gatews.size()
		lid.assignNodeLids( wsnnodes )
		lid.assignGatewayLids( gatews )
		def stop = System.currentTimeMillis()
		def delta = (stop - start) / 1000
		log.info("$total Logical IDs assigned in $delta seconds.")
	}

	/**
	 * Compute and assign logical IDs to all components without lids.
	 * Not used for production; this is intended as a testing function.
	 */
	void assignMissingLogicalIDs(){
		//log.trace("Checking for nodes with no logical ids", "default")
		def start = System.currentTimeMillis()
		List<DLObject> wsnnodes = new ArrayList<DLObject>()
		for ( DLObject n : oModel.getObjectsByType("wsnnode") ){
			if ( n.getObjectProperty("id").getValue() == 0 ){
				wsnnodes.add(n)
			}
		}
		List<DLObject> gatews = new ArrayList<DLObject>()
		for ( DLObject n : oModel.getObjectsByType("wsngateway") ){
			if ( n.getObjectProperty("id").getValue() == 0 ){
				wsnnodes.add(n)
			}
		}
		def total = wsnnodes.size() + gatews.size()
		lid.assignNodeLids( wsnnodes )
		lid.assignGatewayLids( gatews )
		def stop = System.currentTimeMillis()
		def delta = (stop - start) / 1000
		log.info("$total Logical IDs assigned in $delta seconds.")
	}

	/**
	 * Checks to see if this project is a control project.
	 * @return True if there are control components.
	 */
	boolean isControl(){
		def result = false
		if ( this.getComponentsInRole('controlstrategy').size() > 0 ) {
			result = true
		}
		return result
	}

	List<DLComponent> getDeprecatedComponents(){
		return components.findAll { it.isDeprecated() }.toList()
	}

    /**
     * Create children component for staticchildplaceable component
     * @param component
     */
    public void newStaticChildPlaceableComponent(DLComponent component){
        LinkedHashMap<String, String> childrenMap = component.getPlaceableChildrenMap()
        String childComponentType = component.getPlaceableChildType()

        for(String childName:childrenMap.keySet()){
            DLComponent childRackComponent = this.newComponent(childComponentType)

            childRackComponent.setPropertyValue("parent_name",component.getPropertyStringValue("name"))
            //childRackComponent.setPropertyValue("name",childName)
			childRackComponent.setPropertyValue("name",childrenMap[childName]['display'])
            childRackComponent.setPropertyValue("child_id", childName)
            //childRackComponent.setPropertyValue("channel", Integer.parseInt(childrenMap.get(childName)))
			childRackComponent.setPropertyValue("channel", Integer.parseInt(childrenMap[childName]['channel']))

            addRackSensorToParentNode(component, childRackComponent)
            addDockingpoint(component, childRackComponent)

	        // Force this again since it wouldn't have worked during creation since the parent/child component links didn't exist.
	        childRackComponent.automaticObjectAdoption()
        }
    }

    /**
     * add rack sensor to parent horizontal row component
     * @param parentComponent
     * @param childComponent
     */
    private void addRackSensorToParentNode(DLComponent parentComponent, DLComponent childComponent){

        Map<String ,DLObject> nodeMap = parentComponent.getObjectMapOfType("wsnnode")

        for(String name: nodeMap.keySet()){
            log.trace("found wsnnode:" + name)
            DLObject node = nodeMap.get(name)
            ChildLink sensors = (ChildLink) node.getObjectProperty("sensepoints");

            String sensorName = "";
            if(name.replace("node", "").length()==0){
                sensorName = "sensor";
            }else{
                sensorName = name.replace("_node", "") + "_sensor";
            }

            DLObject sensor = childComponent.getObject(sensorName);

            try {
                sensors.addChild(sensor);
            } catch (IncompatibleTypeException e) {
                // TODO Auto-generated catch block
                log.trace("error here")
                log.trace(e.getMessage());
            } catch (DuplicateObjectException e) {
                // TODO Auto-generated catch block
                log.trace(e.getMessage());
            }
        }

    }

    /**
     * Add a docking point to each sensor for horizontal row components
     * @param parentComponent
     * @param childComponent
     */
    private void addDockingpoint(DLComponent parentComponent, DLComponent childComponent){
        DLObject rackObject = childComponent.getObject("rack");

        if(rackObject!=null){
            Dockingpoint pFrom_nodeTemp = (Dockingpoint) rackObject.getObjectProperty("nodeTemp");
            Dockingpoint pFrom_rh = (Dockingpoint)rackObject.getObjectProperty("rh");

            DLObject intake_node_object = parentComponent.getObject("intake_node");
            ChildLink sensors = (ChildLink) intake_node_object.getObjectProperty("sensepoints");

            DLObject oTo_internalTemp = null;
            DLObject oTo_humidity = null;
            for(DLObject o: sensors.getChildren()){
                log.trace("wsnsensor object:" + o.getName());
                if(o.getName().equals("internal temp")){
                    oTo_internalTemp = o;
                }else if(o.getName().equals("humidity")){
                    oTo_humidity = o;
                }
            }

            if(pFrom_nodeTemp!=null){
                if(oTo_internalTemp!=null){
                    try {
                        pFrom_nodeTemp.setReferrent(oTo_internalTemp);
                    } catch (IncompatibleTypeException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            }

            if(pFrom_rh!=null){
                try {
                    pFrom_rh.setReferrent(oTo_humidity);
                } catch (IncompatibleTypeException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * copy child object for dynamicchildplaceable component
     * @param component
     * @param clonedDlId
     */
    private void newDynamicChildPlaceableComponent(DLComponent component, String clonedDlId){

        DLComponent cloneComponent = getOwner(oModel.getObject(oModel.getDlid(clonedDlId)))
        if(cloneComponent.getChildComponents().size()>0){
            cloneComponent.getChildComponents().each{child->
                DLComponent newChild = newComponent(child)
                component.addChild(newChild)
            }
        }
    }

	public DLComponent getComponentFromDLID(int idpart){
		return this.getComponentFromDLID(Dlid.getDlid("DLID:$idpart"))
	}

	public DLComponent getComponentFromDLID( String id ) {
		return this.getComponentFromDLID( Dlid.getDlid( id ) )
	}

	public DLComponent getComponentFromDLID(Dlid uid){
		return dlidMap[uid]
	}

	public DLComponent getComponentFromDLID(Object nullObj) {
		if (null == nullObj) {
			return null
		}
		throw new IllegalArgumentException("object type does not represent a valid DLID")
	}

	void addModelChangeListener(ModelChangeListener l) {
		changeListeners += l
	}

	void removeModelChangeListener(ModelChangeListener l) {
		changeListeners -= l
	}

	void fireComponentAdded(DLComponent component) {
		//log.trace("fireComponentAdded($component)")
		log.info("Component Added $component")
		for(ModelChangeListener l: changeListeners)
			l.componentAdded(component) 
	}

	void fireComponentRemoved(DLComponent component) {
		//log.trace("fireComponentRemoved($component)")
		log.info("Component Removed $component")
		for(ModelChangeListener l: changeListeners)
			l.componentRemoved(component)
	}

	void fireChildAdded(DLComponent parent, DLComponent child) {
		//log.trace("fireChildAdded($parent, $child)")
		for(ModelChangeListener l: changeListeners)
			l.childAdded(parent, child)
	}

	void fireChildRemoved(DLComponent parent, DLComponent child) {
		//log.trace("fireChildRemoved($parent, $child)")
		for(ModelChangeListener l: changeListeners){
			//log.debug("fireChildRemoved($parent, $child) -> $l")
			l.childRemoved(parent, child)
		}
	}

	void fireAssociationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
		//log.trace("fireAssociationAdded($producer, $producerId, $consumer, $consumerId)")
		for(ModelChangeListener l: changeListeners)
		l.associationAdded(producer, producerId, consumer, consumerId)
	}

	void fireAssociationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
		//log.trace("fireAssociationRemoved($producer, $producerId, $consumer, $consumerId)")
		for(ModelChangeListener l: changeListeners)
			l.associationRemoved(producer, producerId, consumer, consumerId)
	}

    private void fireMetamorphosisStarting( ModelState mode ) {
        log.debug("Model Metamorphosis Starting: $mode")
        MetamorphosisEvent event = new MetamorphosisEvent( mode )
        for( ModelChangeListener l: changeListeners ) {
            l.modelMetamorphosisStarting( event )
        }
    }

    private void fireMetamorphosisFinished( ModelState mode ) {
        log.debug("Model Metamorphosis Finished: $mode")
        MetamorphosisEvent event = new MetamorphosisEvent( mode )
        for( ModelChangeListener l: changeListeners ) {
            l.modelMetamorphosisFinished( event )
        }
    }


	public ComponentModel makeModelSubset(ObjectModel cutDownObjectModel){
		def result = new ComponentModel()
		result.oModel = cutDownObjectModel
		result.cLib = this.cLib
		result.lid = this.lid

		def rootComponents = []
		for( DLObject o : cutDownObjectModel.getRoots()){
			rootComponents.add( this.getManagingComponent(o) )
		}

		for(DLComponent r : rootComponents){
			result.addComponentAndChild(r)

		}

		return result
	}

	/**
	 * Recursively add a component and it's children to this component model.
	 * Should only be used when slicing our a new sub-model
	 * @param o
	 */
	@HandsOff(byOrderOf="Gabe Helman", onPainOf=Consequence.VOGON_POETRY_RECITAL)
	protected void addComponentAndChild(DLComponent c){
		this.addComponent(c)
		for(DLComponent k : c.getChildComponents()){
			this.addComponentAndChild(k)
		}
	}

	@HandsOff(byOrderOf="Gabe Helman", onPainOf=Consequence.VOGON_POETRY_RECITAL)
	protected void addComponent(DLComponent c){
		components += c
		for(DLObject o: c.getRoots()) {
			if(oMap.containsKey(o)) {
				if(oMap[o] == c)
					continue
				String message = "Object '$o' is already owned by '${oMap[o]} and cannot be owned by '$c'!"
				log.fatal(message)
				throw new Exception(message)
			}
			oMap[o] = c
		}

		// Don't do the rest for proxies. Only the anchor gets mapped to the non-root objects.
		if( !c.hasRole("proxy") ) {
			for(String pId: c.listProducers(true)) {
				DLObject o = c.getProducerObject(pId)
				if(bMap.containsKey(o)) {
					if(bMap[o] == c)
						continue
					String message = "Object '$o' is already owned by '${bMap[o]} and cannot be owned by '$c'!"
					log.fatal(message)
					throw new Exception(message)
				}
				bMap[o] = c
			}
			for(String cId: c.listConsumers(true)) {
				DLObject o = c.getConsumerObject(cId)
				if(bMap.containsKey(o)) {
					if(bMap[o] == c)
						continue
					String message = "Object '$o' is already owned by '${bMap[o]} and cannot be owned by '$c'!"
					log.fatal(message)
					throw new Exception(message)
				}
				bMap[o] = c
			}
		}
	}

	/**
	 * Return a Set of drawings that contain the components in input
	 * @param inputs a Collection of DLComponents
	 * @return a HashSet of DLComponents representing the drawings the components in input are in
	 */
	Set<DLComponent> computeDrawingsForComponents(java.util.Collection<DLComponent> inputs){
		def result = new HashSet<DLComponent>()
		for(DLComponent c : inputs){
			result.add( c.getDrawing() )
		}
		return result
	}

	Set<DLComponent> computeDrawingsForComponents(DLComponent input){
		return this.computeDrawingsForComponents([input])
	}

	def getTypeDefaultNode(String componentType) {
		return componentTypes[componentType]?.object?.find { object ->
			object.@type.text().equals("wsnnode")
		}
	}

	Map<String, Object> getTypeDefaultProperties(String componentType) {
		Map<String, Object> valueMap = new HashMap<String, Object>()
		getComponentType(componentType)?.property?.each { prop ->
			valueMap.put(prop.@name.text(), prop)
		}
		return valueMap
	}

	String getTypeDefaultNodeValue(String componentType, String propName) {
		return getTypeDefaultNode(componentType)?.property?.find { prop ->
			prop.@name.text().equals(propName)
		}
	}

}
