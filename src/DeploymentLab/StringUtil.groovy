package DeploymentLab

//TODO: replace this with a real library

/**
 * Utility functions for Strings.
 * @author Gabriel Helman
 * Date: Nov 12, 2010
 * Time: 10:31:43 AM
 * @since 6.0
 */
class StringUtil {

	/**
	 * Converts a List of strings into a comma-separated text string.
	 * Optionally, a conjunction can be specified to place between the last two items in the list if there are
	 * three or more items.  By default, the conjunction is "or"
	 * @param sourceList a list of strings to turn into an english language-style list
	 * @param cnj a conjunction to use at the end of the list, defaults to "or"
	 * @return a string containing the values, ready for printing
	 * @author Gabriel Helman
	 * @since 6.0
	 */
	public static String listFormat(def sourceList, String cnj = "or" ){
		String result = ""
		sourceList.eachWithIndex{ s, i ->
			result += s
			if ( sourceList.size() > 1 && i < ( sourceList.size() - 1 ) && sourceList.size() > 2 ){
				result += ", "
			}
			if ( i == (sourceList.size() - 2 ) ){
				result += " ${cnj.trim()} "
			}
		}
		return result
	}

	@Deprecated
	public static String padRight(String str, int size, String padChar) {
		StringBuilder padded = new StringBuilder(str);
		while (padded.length() < size) {
			padded.append(padChar);
		}
		return padded.toString();
	}

	@Deprecated
	public static String padLeft(String str, int size, String padChar) {
		StringBuilder padded = new StringBuilder(str);
		while (padded.length() < size) {
			padded.insert(0,padChar);
		}
		return padded.toString();
	}

	/**
	 * Replace Null String: if val is empty or null, defaultValue will be returned, otherwise val will pass-through.
	 * @param val
	 * @param defaultValue
	 * @return
	 */
	public static String rns( String val, String defaultValue ){
		if (val == null || val == "") {
			return defaultValue;
		} else {
			return val;
		}
	}

	public static InputStream stringToInputStream(String input){
		return new ByteArrayInputStream(input.getBytes("UTF-8"));
	}

	public static String convertStreamToString(InputStream is) {
    	return new Scanner(is,"UTF-8").useDelimiter("\\A").next();
	}

	public static String replaceExtension( String filename, String extension ){
		int period = filename.lastIndexOf(".")
		if ( period == -1 ){
			period = filename.length()
		}

		def justName = filename.substring(0, period)
		return justName + "." + extension
	}

}
