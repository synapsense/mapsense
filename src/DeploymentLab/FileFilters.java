
package DeploymentLab;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;
import javax.swing.filechooser.FileNameExtensionFilter;

public enum FileFilters {
	BackgroundImage("Background Image", "jpg", "png", "bmp"),
	DLProject("5.0-6.0 Project File (.dl)", "dl"),
	SQLScript("SQL script", "sql"),
	SVGFile("SVG file", "svg"),
	CSVFile("CSV file", "csv"),
	PNGFile("PNG file", "png"),
	XMLFile("XML file", "xml"),
	LuaFile("Lua file", "lua"),
	ComponentLibrary("Component Library (.dcl)", "dcl"),

	ZipProject("Project File (.dlz)", "dlz"),
	ZipLibrary("Component Library (.clz)", "clz"),
	ExcelFile("Excel File (.xlsx)", "xlsx")
	;

	private final FileNameExtensionFilter filter;

	FileFilters(String name, String ext) {
		this.filter = new FileNameExtensionFilter(name, ext);
	}

	FileFilters(String name, String ext1, String ext2, String ext3) {
		this.filter = new FileNameExtensionFilter(name, ext1, ext2, ext3);
	}

    /**
     * Returns a filter that is usable in File Chooser dialogs.
     * @return
     */
	public FileNameExtensionFilter getFilter() {
		return this.filter;
	}

    /**
     * Returns a filter that can be used with {@link DirectoryStream}.
     * @return
     */
    public DirectoryStream.Filter<Path> getDirectoryFilter() {
        return new DirectoryStream.Filter<Path>() {
            @Override
            public boolean accept( Path entry ) throws IOException {
                for( String ext : getFilter().getExtensions() ) {
                    if( entry.toString().endsWith( ext ) ) {
                        return true;
                    }
                }
                return false;
            }
        };
    }
}

