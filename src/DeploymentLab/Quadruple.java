package DeploymentLab;


/**
 * A four-field tuple.
 * @author Gabriel Helman
 * @since Mars
 * Date: 6/29/11
 * Time: 10:26 AM
 */
public class Quadruple<S,T,U,V> {

	public S a;
	public T b;
	public U c;
	public V d;

	public Quadruple(S _a, T _b, U _c, V _d) {
		a = _a;
		b = _b;
		c = _c;
		d = _d;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Quadruple quadruple = (Quadruple) o;

		if (a != null ? !a.equals(quadruple.a) : quadruple.a != null) return false;
		if (b != null ? !b.equals(quadruple.b) : quadruple.b != null) return false;
		if (c != null ? !c.equals(quadruple.c) : quadruple.c != null) return false;
		if (d != null ? !d.equals(quadruple.d) : quadruple.d != null) return false;

		return true;
	}

	/**
	 * Compates two Quadruples for equality based only on the first three fields.
	 * @param o an object to compare to
	 * @return true if the first three fields are equal
	 */
	public boolean equalsFirstThree(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Quadruple quadruple = (Quadruple) o;

		if (a != null ? !a.equals(quadruple.a) : quadruple.a != null) return false;
		if (b != null ? !b.equals(quadruple.b) : quadruple.b != null) return false;
		if (c != null ? !c.equals(quadruple.c) : quadruple.c != null) return false;

		return true;
	}


	@Override
	public int hashCode() {
		int result = a != null ? a.hashCode() : 0;
		result = 31 * result + (b != null ? b.hashCode() : 0);
		result = 31 * result + (c != null ? c.hashCode() : 0);
		result = 31 * result + (d != null ? d.hashCode() : 0);
		return result;
	}


	@Override
	public String toString() {
		return "Quadruple{" +
				"a=" + a +
				", b=" + b +
				", c=" + c +
				", d=" + d +
				'}';
	}

	public Object[] toArray(){
		Object[] result = new Object[4];
		result[0] = a;
		result[1] = b;
		result[2] = c;
		result[3] = d;
		return result;
	}
}
