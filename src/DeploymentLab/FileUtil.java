package DeploymentLab;

import DeploymentLab.channellogger.Logger;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

/**
 * General file utilities, intended to provide static helper methods for dealing with the file system.
 * Eventually, anything that deals with files should migrate to here.
 * @author Gabriel Helman
 * Date: Nov 5, 2010
 * Time: 4:45:20 PM
 * @since 6.0
 */
public class FileUtil {
	private static final Logger log = Logger.getLogger(FileUtil.class.getName());

	/**
	 * Get a file from a user via a filechooser. Migrated from DLW.
	 * @param userPreferences The current user preferences so we can remember where the user looked last
	 * @param dialogTitle the title of the file chooser dialog
	 * @return a File that the user selected, or null if they hit cancel
	 */
	public static File askForFile(UserPreferences userPreferences, String dialogTitle ) {
		return askForFile(userPreferences,dialogTitle,null);
	}

	/**
	 * Get a file from a user via a filechooser.  Migrated from DLW.
	 * @param userPreferences The current user preferences so we can remember where the user looked last
	 * @param dialogTitle the title of the file chooser dialog
	 * @param filter the file filter, if any (can be null)
	 * @return a File that the user selected, or null if they hit cancel
	 */
	public static File askForFile(UserPreferences userPreferences,  String dialogTitle, FileNameExtensionFilter filter ) {
		File selectedFile = null;
		JFileChooser fileChooser = new JFileChooser();

		if( userPreferences != null ) {
			fileChooser.setCurrentDirectory( new File( userPreferences.getUserPreference( UserPreferences.FILE ) ) );
		}
		fileChooser.setSelectedFile( new File("") );
		fileChooser.resetChoosableFileFilters();
		if( filter != null ) {
			fileChooser.setFileFilter( filter );
		}
		fileChooser.setDialogTitle( dialogTitle );
		int choice = fileChooser.showOpenDialog( CentralCatalogue.getParentFrame() );
		if( choice == JFileChooser.APPROVE_OPTION ) {
			selectedFile = fileChooser.getSelectedFile();
			if( ! selectedFile.exists() ) {
				log.error( CentralCatalogue.formatUIS("file.totalExistenceFailure", selectedFile ), "message");
				selectedFile = null;
			} else if( userPreferences != null ) {
				userPreferences.setUserPreference( UserPreferences.FILE, selectedFile.getParent() );
				userPreferences.setUserPreference( UserPreferences.FILENAME, FileUtil.stripExtension(selectedFile).getName() );
			}
		}
		return selectedFile;
	}


	public static void backupFile(File source, String backupExtension) throws IOException{
		File backupFile = changeExtension(source, backupExtension);
		copyFile(source, backupFile);
		log.info(source.toString() + " backed up to " + backupFile.toString(), "default");
	}

	public static void copyFile(File sourceFile, File destFile) throws IOException {
		if(!destFile.exists()) {
			destFile.createNewFile();
		}

		FileChannel source = null;
		FileChannel destination = null;
		try {
			source = new FileInputStream(sourceFile).getChannel();
			destination = new FileOutputStream(destFile).getChannel();
			destination.transferFrom(source, 0, source.size());
		}
		finally {
			if(source != null) {
				source.close();
			}
			if(destination != null) {
				destination.close();
			}
		}
	}

	static File changeExtension(File original, String newExtension) {
		String originalName = original.getAbsolutePath();
		String newName;
		int lastDot = originalName.lastIndexOf(".");
		if (lastDot != -1) {
			newName =  originalName.substring(0, lastDot) + "." + newExtension;
		} else {
			newName = originalName + "." + newExtension;
		}
		return new File(newName);
	}

	public static File stripExtension(File original){
		String originalName = original.getAbsolutePath();
		String newName;
		int lastDot = originalName.lastIndexOf(".");
		if (lastDot != -1) {
			newName =  originalName.substring(0, lastDot);
		} else {
			newName = originalName;
		}
		return new File(newName);

	}


	/**
	* Inhales a text file to a String.
	* This method does not perform enconding conversions
	*
	* @param file The input file
	* @return The file contents as a <code>String</code>
	* @exception IOException IO Error
	*/
	public static String fileToString(File file) throws IOException {
		int len;
		char[] chr = new char[4096];
		final StringBuffer buffer = new StringBuffer();
		final FileReader reader = new FileReader(file);
		try {
			while ((len = reader.read(chr)) > 0) {
				buffer.append(chr, 0, len);
			}
		} finally {
			reader.close();
		}
		return buffer.toString();
	}
/*
	public static String fileToString(String filename) throws IOException {
		File f = new File(filename);
		return ( fileToString(f) );
	}
*/

	/**
	 * Stores the contents of any text file to a String
	 * @param filename the name of a file to inhale
	 * @return a String containing the contents of the file
	 * @throws IOException if something gets bustimated
	 */
	public static String fileToString(String filename) throws IOException {
		StringBuilder contents = new StringBuilder();
		BufferedReader input =  new BufferedReader(new FileReader( filename ));
		String line = null; //not declared within while loop
		while (( line = input.readLine()) != null){
			contents.append(line);
			contents.append(System.getProperty("line.separator"));
		}
		input.close();
		return contents.toString();
	}

	/**
	 * Stores the contents of any InputStream to a String
	 * @param in an InputStream
	 * @return a String containing the contents of the stream
	 * @throws IOException if something gets bustimated
	 */
	public static String inputStreamToString( InputStream in ) throws IOException {
		StringBuilder contents = new StringBuilder();
		BufferedReader input =  new BufferedReader(new InputStreamReader( in ));
		String line = null; //not declared within while loop
		while (( line = input.readLine()) != null){
			contents.append(line);
			contents.append(System.getProperty("line.separator"));
		}
		input.close();
		return contents.toString();
	}

	public static List<String> fileToList(String filename) throws IOException {
		List<String> result = new ArrayList<String>();

		BufferedReader input =  new BufferedReader(new FileReader( filename ));
		String line = null; //not declared within while loop
		while (( line = input.readLine()) != null){
			result.add(line);
		}
		input.close();
		return result;
	}

	/**
	 * Converts a file to a byte array.
	 * @param file the File to read in
	 * @return a byte[] containing the bytes of the file
	 * @throws IOException if things go wrong
	 */
	public static byte[] getBytesFromFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);

		// Get the size of the file
		long length = file.length();

		// You cannot create an array using a long type.
		// It needs to be an int type.
		// Before converting to an int type, check
		// to ensure that file is not larger than Integer.MAX_VALUE.
		if (length > Integer.MAX_VALUE) {
			// File is too large
		}

		// Create the byte array to hold the data
		byte[] bytes = new byte[(int)length];

		// Read in the bytes
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length
			   && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
			offset += numRead;
		}

		// Ensure all the bytes have been read in
		if (offset < bytes.length) {
			throw new IOException("Could not completely read file "+file.getName());
		}

		// Close the input stream and return bytes
		is.close();
		return bytes;
	}

}
