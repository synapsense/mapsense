
package DeploymentLab

import DeploymentLab.Model.*

import DeploymentLab.channellogger.*
import javax.swing.JFrame
import java.awt.Cursor

public class UndoBuffer implements ModelChangeListener, SelectionChangeListener {
	private static final Logger log = Logger.getLogger(UndoBuffer.class.getName())

	private def undoStack = []
	private def redoStack = []

	private ComponentModel cmodel
	private SelectModel selectModel

	private boolean undoing = false
	private boolean redoing = false
	private boolean batching = false

	private def currentBatch = []
	private ComponentPropUndoAtom cpua = null

	private int stopTrackingCnt = 0
	private int operationCnt = 0

	private long savePoint = 0

	Random ids
	JFrame parent

	UndoBuffer(ComponentModel cm, SelectModel sm) {
		cmodel = cm
		ids = new Random()
		cmodel.addModelChangeListener(this)
		selectModel = sm
		selectModel.addSelectionChangeListener(this)
	}

	void setParent( JFrame dlf ){
		parent = dlf
	}

	/**
	 * Resets the internal save point. This will cause hasUnsavedChanges to always return true until a new save point
	 * is marked.
	 */
	void resetSavePoint() {
		savePoint = -1
	}

	/**
	 * Marks the current top of the undo buffer as the point where the project was last saved.
	 */
	void markSavePoint() {
		savePoint = getUndoState()
	}

	/**
	 * Returns true if the undo buffer has been changed since the last save point was marked or if the save point has
	 * been reset since the last mark.
	 *
	 * @return True if the undo buffer has unsaved changes.
	 */
	boolean hasUnsavedChanges() {
		return getUndoState() != savePoint
	}

	void stopTracking() {
		stopTrackingCnt++
		if( stopTrackingCnt == 1 ) {
			log.debug("Halting undo/redo tracking.")
		}
	}

	void resumeTracking( boolean resetBuffer = true ) {
		stopTrackingCnt--
		if( stopTrackingCnt < 0 ) {
			log.error(log.getStackTrace(new Exception("Resume Tracking Mismatch! Too many resumes!")))
			stopTrackingCnt = 0
		}

		if( stopTrackingCnt == 0 ) {
			if( resetBuffer ) clear()
			log.debug("Resuming undo/redo tracking. Buffers ${resetBuffer ? "" : "not"} cleared.")
		}
	}

	boolean isTracking() {
		return stopTrackingCnt == 0
	}

	long getUndoState() {
		if(undoStack.size() == 0)
			return 0
		return undoStack.last().getId()
	}

	/**
	 * Begins a multi-part undo operation.
	 */
	void startOperation() {
		operationCnt++
		if( operationCnt == 1 ) {
			if(!isTracking())
				return

			log.trace("startOperation()")
			parent?.setCursor( Cursor.WAIT_CURSOR )

			if(batching)
				throw new Exception("Recursive Undo batching not supported!")
			batching = true
		}
	}

	/**
	 * Finishes recording the current multi-part undo operation.
	 */
	void finishOperation() {
		operationCnt--
		if( operationCnt < 0 ) {
			log.error(log.getStackTrace(new Exception("Group Operation Mismatch! Too many finishes!")))
			operationCnt = 0
		}

		if( operationCnt == 0 ) {
			if(!isTracking())
				return

			log.trace("finishOperation()")
			parent?.setCursor( null )

			if(!batching)
				throw new Exception("Undo group not started!")

			batching = false

			if(currentBatch.size() == 0)
				return

			if(undoing) {
				redoStack.push(new UndoAction(currentBatch, ids.nextLong()))
			} else {
				undoStack.push(new UndoAction(currentBatch, ids.nextLong()))
				checkRedoStack()
			}

			cpua = null
			currentBatch = []
		}
	}

	/**
	 * Rolls back an undo operation during construction.
	 *
	 * Handles the case where an error happens during the creation of an undo operation, and the system needs to reset.
	 * The intended use case is that this goes in the catch-block of an exception handler that includes a startOperation
	 * in the try-block and a finishOperation in the catch.
	 *
	 * Parses backwards through the undo operation in progress and undoes each operation, then clears out the current batch.
	 * The final state after a roll-back should be the same as just before the operation was started (including nothing new on the undo stack.)
	 *
	 * Important: finishOperation must still be called after rollbackOperation to finish resetting the undo buffer.
	 */
	void rollbackOperation() {
		if(!isTracking()){return}
        log.debug("Rolling back an undo operation.")
        if(!batching) { throw new Exception("Undo group not started!") }
        if(currentBatch.size() == 0) { return }
		//now, parse back through the current undo operation in progress
		stopTracking() //turn tracking off during the rollback
		try {
			currentBatch.reverseEach{ op ->
				op.execute()
			}
		} catch( Exception e ) {
			log.error("Rollback encountered an error.", "message")
			log.error( log.getStackTrace(e) )
		}
        cpua = null
		currentBatch = []
		resumeTracking( false )
		//note: batching needs to stay true so that finishOperation will close us out normally
	}

	/**
	 * Returns true if there is currently an undo operation being saved, so we don't nest undos inside each other.
	 */
	boolean operationInProgress() {
		if(!isTracking())
			return false

		return ( batching )
	}

	void undo() {
		//println "undo()"
		if(!isTracking())
			return

		if ( undoing ){
			log.warn("Can't undo in the middle of an undo!")
			return
		}
		if ( redoing ){
			log.warn("Can't undo in the middle of a redo!")
			return
		}

		log.trace("undo()")

		if(batching)
			throw new Exception("Undo op started in the middle of a batch!")

		if (undoStack.size() == 0) {
			log.info("The Undo buffer is empty.", 'statusbar')
			return
		}
		cpua = null

		undoing = true

        cmodel.metamorphosisStarting(ModelState.UNDOING)

		ProgressManager.doTask {
			//def s = new DLStopwatch()
			log.trace("Undoing...", "progress")
			parent?.setCursor( Cursor.WAIT_CURSOR )
			UndoAction ua = undoStack.pop()
			if(ua.multiAction()) {
				startOperation()
				ProgressManager.setMaxProgress( ua.size() )
			}

			try {
				ua.execute()
			} catch( Exception e ) {
				log.error("Undo failed due to an unexpected error. Operation has been removed from the undo buffer.", "message")
				log.error( log.getStackTrace(e) )
			}

			if(batching){
				finishOperation()
			}
			undoing = false
			cpua = null

            cmodel.metamorphosisFinished(ModelState.UNDOING)

			parent?.setCursor( null )
			//log.info("undo completed in ${s.finish()} sec", "message")
		}
	}

	void redo() {
		if(!isTracking())
			return

		if ( undoing ){
			log.warn("Can't redo in the middle of an undo!")
			return
		}
		if ( redoing ){
			log.warn("Can't redo in the middle of a redo!")
			return
		}

		log.trace("redo()")

		if(batching)
			throw new Exception("Redo op started in the middle of a batch!")

		if (redoStack.size() == 0) {
			log.info("The Redo buffer is empty.", 'statusbar')
			return
		}
		cpua = null
		redoing = true

        //set medelState redoing operation
        cmodel.metamorphosisStarting(ModelState.REDOING)

		ProgressManager.doTask {
			log.trace("Redoing...", "progress")

			parent?.setCursor( Cursor.WAIT_CURSOR )

			UndoAction ua = redoStack.pop()
			if(ua.multiAction()) {
				startOperation()
			}

			try {
				ua.execute()
			} catch( Exception e ) {
				log.error("Redo failed due to an unexpected error. Operation has been removed from the undo buffer.", "message")
				log.error( log.getStackTrace(e) )
			}

			if(batching)
				finishOperation()
			redoing = false

            cmodel.metamorphosisFinished(ModelState.REDOING)
			cpua = null
            parent?.setCursor( null )
		}
	}

	void clear() {
		if(!isTracking())
			return

		log.info("Clearing undo buffer.")
		undoStack.clear()
		redoStack.clear()
		cpua = null
		currentBatch = []
		markSavePoint()
	}

	void checkRedoStack() {
		if (!undoing && !redoing){
			redoStack.clear()
		}
	}

	private void addAction(def undoAtom) {
		log.trace("addAction() batching = $batching")

		if(batching) {
			currentBatch.push(undoAtom as UndoAtom)
		} else {
			if(undoing) {
				redoStack.push(new UndoAction([undoAtom as UndoAtom], ids.nextLong()))
			} else {
				undoStack.push(new UndoAction([undoAtom as UndoAtom], ids.nextLong()))
				checkRedoStack()
			}
		}
	}

	void componentAdded(DLComponent component) {
		component.addPropertyChangeListener(this, this.&componentPropertyChanged)

		if(!isTracking())
			return

		def undoAction = {cmodel.remove(component)}
		addAction(undoAction)
	}

	void componentRemoved(DLComponent component) {
		component.removePropertyChangeListener(this)

		if(!isTracking())
			return

		def undoAction = {cmodel.undelete(component)}
		addAction(undoAction)
	}

	void childAdded(DLComponent parent, DLComponent child) {
		if(!isTracking())
			return

		def undoAction = {parent.removeChild(child)}
		addAction(undoAction)
	}

	void childRemoved(DLComponent parent, DLComponent child) {
		if(!isTracking())
			return

		def undoAction = {parent.addChild(child)}
		addAction(undoAction)
	}

	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
		if(!isTracking())
			return

		log.trace("associationAdded($producer, $producerId, $consumer, $consumerId)")
		def undoAction = {consumer.unassociate(consumerId, producer, producerId)}
		addAction(undoAction)
	}

	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
		if(!isTracking())
			return

		log.trace("associationRemoved($producer, $producerId, $consumer, $consumerId)")
		def undoAction = {consumer.associate(consumerId, producer, producerId)}
		addAction(undoAction)
	}

    @Override
    public void modelMetamorphosisStarting( MetamorphosisEvent event ) {
		if(!isTracking()){return}
		if(!batching){return}
		def undoAction = {cmodel.metamorphosisFinished(event.getMode())}
		addAction(undoAction)
	}

    @Override
    public void modelMetamorphosisFinished( MetamorphosisEvent event ) {
		if(!isTracking()){return}
		if(!batching){return}
		def undoAction = {cmodel.metamorphosisStarting(event.getMode())}
		addAction(undoAction)
	}

	@Override
	void selectionChanged(SelectionChangeEvent e) {}

	@Override
	void activeDrawingChanged(DLComponent oldDrawing, DLComponent newDrawing) {
		//what if we tracked active drawing changes?
		if(!isTracking()){return}
		if(!batching){return}

		log.trace("activeDrawingChanged($oldDrawing -> $newDrawing)")
		def undoAction = {selectModel.setActiveDrawing(oldDrawing)}
		addAction(undoAction)
	}

	void componentPropertyChanged(def who, String pName, def oldVal, def newVal) {
		// Skip the metamorphasizing property since it's really just a trigger, not a property. #haxxerz
		if(!isTracking() || pName == 'metamorphosizing' || who.getComponentProperty( pName )?.isInherited() )
			return

		//println "1 componentPropertyChanged($who, $pName, $oldVal, $newVal)"
		//println "2 cpua is $cpua"

		def undoAction
		if(pName == 'image_data') // Takes too much memory to undo this
			return
		if(cpua == null || cpua.component != who) {
			log.trace("Creating new CPUA prop $pName oldVal $oldVal newVal $newVal")
			cpua = new ComponentPropUndoAtom()
			cpua.component = who
			cpua.properties[pName] = oldVal
			undoAction = cpua
		} else {
			//note: the code below squashes multiple sequential x,y,rotation changes to the same component
			if(pName == 'rotation' && cpua.properties['rotation'] != null)
				return
			if((pName == 'x' || pName == 'y' || pName == ComponentProp.POLY_POINTS) &&
			   (cpua.properties['x'] != null || cpua.properties['y'] != null || cpua.properties[ComponentProp.POLY_POINTS] != null)) {
				if( cpua.properties[ pName ] == null ) {
					log.trace("Updating CPUA with prop $pName oldVal $oldVal newVal $newVal")
					cpua.properties[ pName ] = oldVal
				}
				return
			} else if((pName == 'depth' || pName == 'width') && (cpua.properties['depth'] != null || cpua.properties['width'] != null)) {
				if(pName == 'depth' && cpua.properties['depth'] == null) {
					log.trace("Updating CPUA with prop $pName oldVal $oldVal newVal $newVal")
					cpua.properties['depth'] = oldVal
				} else if(pName == 'width' && cpua.properties['width'] == null) {
					log.trace("Updating CPUA with prop $pName oldVal $oldVal newVal $newVal")
					cpua.properties['width'] = oldVal
				}
				return
            }

			log.trace("Creating new CPUA prop $pName oldVal $oldVal newVal $newVal")
			cpua = new ComponentPropUndoAtom()
			cpua.component = who
			cpua.properties[pName] = oldVal
			undoAction = cpua
		}

		//println "3 now cpua is $cpua"

		addAction(undoAction)
	}

	/**
	 * For debugging purposes, echo the undo stack to the log.
	 */
	public void display(){
		String display = ""
		display += "UndoBuffer undoing=$undoing , redoing=$redoing, batching=$batching, tracking=$tracking \n" ;
        display += "Active CPUA: ${cpua?.display()} \n"
        display += "Active Batch: ${currentBatch.size()}\n"
        currentBatch.each{ us ->
            display += us.display() + "\n"
        }
		display += "Undo Stack: ${undoStack.size()} \n"
		undoStack.each{ us ->
			display += us.display() + "\n"
		}
		display += "Redo Stack: ${redoStack.size()} \n"
		redoStack.each{ us ->
			display += us.display() + "\n"
		}
		log.info(display,'message')
	}


}

interface UndoAtom {
	void execute()
	String display()
	int size()
}

class UndoAction implements UndoAtom {
	private List actions
	private long id

	UndoAction(def actions, long id) {
		this.actions = actions
		this.id = id
	}

	void execute() {
		actions.reverseEach{ op ->
			op.execute()
			//ProgressManager.bump(true)
		}
	}

	boolean multiAction() {
		return actions.size() > 1
	}

	long getId() {
		return id
	}

	/**
	 * Basic toString implementation so that UndoBuffer.display() will work.
	 */
	public String toString() {
		return( id + ": " + actions)
	}

	public String display(){
		String result = ""
		result += "UA size = ${actions.size()} \n"

		actions.each{ a ->
			result += "---- $a \n"
		}
		return result
	}

	int size(){
		return actions.size()
	}

}


class ComponentPropUndoAtom implements UndoAtom {
	private static final Logger log = Logger.getLogger(ComponentPropUndoAtom.class.getName())

	def component
	def properties = [:]

	void execute() {
		properties.each{ prop, val ->
			log.trace("Setting $prop to $val")
			component.setPropertyValue(prop, val)
			ProgressManager.bump(true)
		}
	}

	public String display(){
        String result = "CPUA ${properties.size()} properties changed on $component"
        for( def prop : properties ) {
            result += "\n       $prop"
        }
        result += "\n"
        return  result
	}

	int size(){
		return properties.size()
	}

	String toString(){
		return this.display()
	}

}

