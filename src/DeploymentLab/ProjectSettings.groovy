
package DeploymentLab

import DeploymentLab.Model.PropertyConverter
import DeploymentLab.channellogger.Logger
import com.google.common.collect.BiMap
import DeploymentLab.Model.Dlid
import com.google.common.collect.HashBiMap

public class ProjectSettings {
	private static final Logger log = Logger.getLogger(ProjectSettings.class.getName())

	private String _esHost
	private String _unitSystem

	String esHost
	String unitSystem
	Boolean refreshAllLinks
	Boolean refreshAllRules
	Boolean refreshAllObjects
	Boolean hasBeenUpgraded

	List editHistory = [] //list of maps[ {date,revision,builddate,upgrade,export} ]
	String currentUpgrade
	Boolean currentExport

	/**
	 * These will be non-blank if the project was saved after opening from smart zone server
	 */
	String smartZoneHost = ""
	String smartZonePort = ""

	/**
	 * Allows us to store a list of things that the user did that we might want to know about.
	 * Currently:
	 * <pre>
	 * Forced full export
	 * Reset Object IDs
	 * Reset Logical IDs
	 * </pre>
	 */
	String currentAnnotation

	String acHost

    /** DLID of the active drawing. Used to initialize the Select Model. */
    String activeDrawing = ""

    /**
     * A map of Current DLIDs to Pre-Upgrade DLIDs. Only maintains IDs from the last upgrade performed.
     * This will be empty if no upgrade has ever been done on this project. Key = Current DLID. Value = Old DLID.
     * Unlike the rest, it is private to force use of the methods to better control the mapping.
     */
    private BiMap<Dlid,Dlid> upgradeDlidMap = HashBiMap.create()


	/**
	 * Stores this project's configured defaults list
	 */
	List<Quadruple<String,String,String,Object>> configuredDefaults


	/**
	 * Stores this project's configured simulator list
	 */
	List<Map<String,String>> configuredSimulator

	/**
	* SingletonHolder is loaded on the first execution of Singleton.getInstance()
	* or the first access to SingletonHolder.INSTANCE, not before.
	*/
	private static class SingletonHolder {
		public static final ProjectSettings INSTANCE = new ProjectSettings();
	}

	/**
	* Get the instance of the ProjectSettings.
	* @return the ProjectSettings instance.
	*/
	public static ProjectSettings getInstance() {
		return SingletonHolder.INSTANCE;
	}

	private ProjectSettings() {
		_esHost = esHost = "localhost"
		_unitSystem = unitSystem = "Imperial US"

		acHost = ""

	}

	void save(def builder) {
        StringBuilder dlidMapStr = new StringBuilder( upgradeDlidMap.size() * 25 )
        for( Map.Entry<Dlid,Dlid> entry : upgradeDlidMap.entrySet() ) {
            dlidMapStr.append("${entry.getKey()},${entry.getValue()};")
        }

		builder.settings {
			'esHost'(esHost)
			'unit'(unitSystem)
			'acHost'(acHost)
//			'createRevision'(createRevision)
//			'createBuildDate'(createBuildDate)
//			'createDate'(createDate)
//			'editRevision'(editRevision)
//			'editBuildDate'(editBuildDate)
//			'editDate'(editDate)
			'refreshAllLinks' (refreshAllLinks)
			'refreshAllRules' (refreshAllRules)
			'refreshAllObjects' (refreshAllObjects)
			'hasBeenUpgraded' (hasBeenUpgraded)
            'activeDrawing' (activeDrawing)
			'smartZoneHost' (smartZoneHost)
			'smartZonePort' (smartZonePort)

			builder.editHistory(){
				editHistory.each{ h ->
					builder.history( date: h["date"], revision: h["revision"], buildDate: h["buildDate"], upgrade: h["upgrade"], export: h["export"], annotation: h["annotation"], filename: h['filename'] )
				}
			}

			builder.configuredDefaults(){
				configuredDefaults.each{ cd ->
					builder.configuredDefault( component: cd.a, asName: cd.b, setting: cd.c, val: cd.d )
				}
			}

			builder.configuredSimulator(){
				configuredSimulator.each{
					builder.configuredSim( dlid: it["dlid"], value : it["value"], time : it["time"] )
				}
			}

            'upgradeDlidMap' (dlidMapStr.toString())
		}

		_esHost = esHost
		_unitSystem = unitSystem
	}

	void load(def xml) {
		_esHost = esHost = xml.settings.esHost.text()
		_unitSystem = unitSystem = xml.settings.unit.text()

		acHost = xml.settings.acHost.text()

        activeDrawing = xml.settings.activeDrawing.text()

		smartZoneHost = xml.settings.smartZoneHost.text()
		smartZonePort = xml.settings.smartZonePort.text()

		editHistory = []

		if ( xml.settings.createRevision?.text() ) {
			//createRevision = xml.settings.createRevision?.text()
			//createBuildDate = xml.settings.createBuildDate?.text()
			//createDate = xml.settings.createDate?.text()

			//slug the old create and edit revisions into the new history list
			Map<String,String> editMap = [:]
			editMap["date"] = xml.settings.createDate?.text()
			editMap["revision"] = xml.settings.createRevision?.text()
			editMap["buildDate"] = xml.settings.createBuildDate?.text()
			editMap["upgrade"] = "created"
			editMap["export"] = "false"

			editHistory += editMap
		}

		if ( xml.settings.editRevision?.text() ) {

			//slug the old create and edit revisions into the new history list
			Map<String,String> editMap = [:]
			editMap["date"] = xml.settings.editDate?.text()
			editMap["revision"] = xml.settings.editRevision?.text()
			editMap["buildDate"] = xml.settings.editBuildDate?.text()
			editMap["upgrade"] = ""
			editMap["export"] = "false"
			editHistory += editMap
		}


		refreshAllLinks = xml.settings.refreshAllLinks?.text() == "" ? false : Boolean.parseBoolean(xml.settings.refreshAllLinks?.text())
		//rules can get set to true by the upgrade process, so we don't want to override it
		if (! refreshAllRules ){
			refreshAllRules = xml.settings.refreshAllLinks?.text() == "" ? true : Boolean.parseBoolean(xml.settings.refreshAllRules?.text())
		}
		refreshAllObjects = xml.settings.refreshAllLinks?.text() == "" ? false : Boolean.parseBoolean(xml.settings.refreshAllObjects?.text())

		hasBeenUpgraded = xml.settings.hasBeenUpgraded?.text() == "" ? true : Boolean.parseBoolean(xml.settings.hasBeenUpgraded?.text())

		//editHistory = []
		xml.settings.editHistory.history.each{ hist->
			if ( hist.@date.text() ){
				//new style
				Map<String,String> editMap = [:]
				editMap["date"] = hist.@date.text()
				editMap["revision"] = hist.@revision.text()
				editMap["buildDate"] = hist.@buildDate.text()
				editMap["upgrade"] = hist.@upgrade.text()
				editMap["export"] = "false"
				editMap["annotation"] =  hist.@annotation.text()
				editMap["filename"] = hist.@filename.text()

			    editHistory += editMap
			}
		}

		configuredDefaults = []
		xml.settings.configuredDefaults.configuredDefault.each{ cdx ->
			configuredDefaults += new Quadruple(cdx.@component.text(), cdx.@asName.text(), cdx.@setting.text(), cdx.@val.text())
		}

		configuredSimulator = []

		xml.settings.configuredSimulator.configuredSim.each{ cdx ->
			//configuredSimulator += new Quadruple(cdx.@component.text(), cdx.@asName.text(), cdx.@setting.text(), cdx.@val.text())
			def current = [:]
			current["dlid"] = cdx.@dlid.text()
			current["time"] = cdx.@time.text()
			current["value"] = cdx.@value.text()
			configuredSimulator += current
		}

        upgradeDlidMap.clear()
        for( String entry : xml.settings.upgradeDlidMap.text().split(";") ) {
            if( !entry.isEmpty() ) {
                String[] kv = entry.split(",")
                upgradeDlidMap.put( Dlid.getDlid( kv[0] ), Dlid.getDlid( kv[1] ) )
            }
        }
	}

	boolean needSave() {
		return _esHost != esHost || _unitSystem != unitSystem;
	}
	
	void reset(){
		_esHost = esHost = "localhost"
		acHost = ""
		activeDrawing = ""
		smartZoneHost = ""
		smartZonePort = ""
		_unitSystem = unitSystem = "Imperial US"
//		createRevision = ""
//		createBuildDate = ""
//		createDate = ""
		refreshAllLinks = true
		refreshAllRules = true
		refreshAllObjects = true
		hasBeenUpgraded = false
		log.debug("Project Settings have been reset!")
		editHistory = []
		currentUpgrade = ""
		currentExport = false
		currentAnnotation = ""
		configuredDefaults = []
		configuredSimulator = []
        upgradeDlidMap.clear()
	}
	
	void create(){
		// create new project log

        // todo: Dead code? Reset() is called right after this, wiping it out?
		Map<String,String> editMap = [:]
		editMap["date"] = PropertyConverter.toXml('java.util.Date', new Date())
		editMap["revision"] = CentralCatalogue.getApp("build.revision")
		editMap["buildDate"] = CentralCatalogue.getApp("build.date")
		editMap["upgrade"] = "created"
		editMap["export"] = "false"
		editMap["annotation"] = ""
		editMap["filename"] = ""
		currentUpgrade = ""
		currentExport = false
		currentAnnotation = ""
		editHistory += editMap

		this.reset()
		this.upgrade("created")
	}
	
	void edit(String fileName = ""){
		//edit project log
		Map<String,String> editMap = [:]
		editMap["date"] = PropertyConverter.toXml('java.util.Date', new Date())
		editMap["revision"] = CentralCatalogue.getApp("build.revision")
		editMap["buildDate"] = CentralCatalogue.getApp("build.date")
		editMap["upgrade"] = currentUpgrade
		editMap["export"] = currentExport
		editMap["annotation"] = currentAnnotation
		editMap["filename"] = fileName

		currentUpgrade = ""
		currentExport = false
		currentAnnotation = ""
		editHistory += editMap
	}

	void upgrade(String upgradePath){
		currentUpgrade += " $upgradePath"
		log.trace("current upgrade set to $currentUpgrade")
		hasBeenUpgraded = true
	}

	void exported(){
		currentExport = true
		log.trace("current export set to $currentExport")
		hasBeenUpgraded = false
	}

	/**
	 * Adds an annotation to the current history annotation list.  Multiple annotations will be automatically formatted in one comma-separated string.
	 * @param moreExtended the new annotation to add
	 */
	void addAnnotation(String moreExtended){
		if ( currentAnnotation.length() > 0 ){
			currentAnnotation += " , $moreExtended"
		} else {
			currentAnnotation = moreExtended
		}

	}

    /**
     * Removes all upgrade DLID mappings in preperation for a fresh replacement of all components in the model.
     */
    void resetDlidMap() {
        upgradeDlidMap.clear()
    }

	/**
	 * Lookup the pre-upgraded DLID for a component using its current DLID.
	 *
	 * @param newDlid  The component's current DLID.
	 * @return  The component's pre-upgrade DLID, or null if none exists.
	 */
	void removeDlidMapping( Dlid currentDlid ) {
		upgradeDlidMap.remove( currentDlid )
	}

    /**
     * Captured the mapping between a components old DLID and new DLID when it has been upgraded. If the old DLID is
     * currently in the lookup table from a previous upgrade, that mapping will be removed.
     *
     * @param oldDlid  The DLID being replaced.
     * @param newDlid  The new DLID.
     */
    void replaceDlid( Dlid oldDlid, Dlid newDlid ) {
        // Remove the old one if there was already a mapping for it.
        upgradeDlidMap.remove( oldDlid )
        upgradeDlidMap.put( newDlid, oldDlid )
    }

    /**
     * Lookup the pre-upgraded DLID for a component using its current DLID.
     *
     * @param newDlid  The component's current DLID.
     * @return  The component's pre-upgrade DLID, or null if none exists.
     */
    Dlid findOldDlid( Dlid newDlid ) {
        return upgradeDlidMap.get( newDlid )
    }

    /**
     * Lookup the current DLID for a component using its pre-upgraded DLID.
     *
     * @param oldDlid  The component's pre-upgraded DLID.
     * @return  The component's current DLID, or null if none exists.
     */
    Dlid findNewDlid( Dlid oldDlid ) {
        return upgradeDlidMap.inverse().get( oldDlid )
    }

    /**
     * Lookup the mapped DLID regardless of whether it is the old or the new one.
     *
     * @param dlid The DLID to lookup for its mapped DLID.
     * @return  The mapped DLID, or null if none exists.
     */
    Dlid findOtherDlid( Dlid dlid ) {
        Dlid otherDlid = findOldDlid( dlid )
        if( otherDlid == null ) {
            otherDlid = findNewDlid( dlid )
        }
        return otherDlid
    }

	public String toString() {
		def writer = new StringWriter()
		def builder = new groovy.xml.MarkupBuilder(writer)
		this.save(builder)
		return( writer.toString() )
	}

	public String historyAsHTML(){
		def sb = new StringBuilder()
		//builder.history( date: h["date"], revision: h["revision"], buildDate: h["buildDate"], upgrade: h["upgrade"], export: h["export"], annotation: h["annotation"], filename: h['filename'] )

		sb.append("<html>")
		sb.append("<table>")

		sb.append("<tr><th>Save Date</th><th>Software Version</th><th>Upgraded?</th><th>Exported?</th><th>Annotation</th><th>File Name</th></tr>")

		for(Map h : editHistory){
			sb.append("<tr>")
			sb.append("<td>")
			sb.append("${h['date']}")
			sb.append("</td>")

			sb.append("<td>")
			sb.append("${h['revision']}")
			sb.append("</td>")

			sb.append("<td>")
			sb.append("${h['upgrade']}")
			sb.append("</td>")

			sb.append("<td>")
			sb.append("${h['export']}")
			sb.append("</td>")

			sb.append("<td>")
			sb.append("${h['annotation']}")
			sb.append("</td>")

			sb.append("<td>")
			sb.append("${h['filename']}")
			sb.append("</td>")

			sb.append("</tr>")
		}

		sb.append("</table>")
		sb.append("</html>")
		return sb.toString()
	}

}

