package DeploymentLab.SmartZone

import DeploymentLab.Dialogs.MergeConfiguration.MergeConfigurationDialog
import DeploymentLab.Dialogs.MergeConfiguration.MergeConflictData
import DeploymentLab.Dialogs.MergeConfiguration.MergeModel
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentType
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLObject
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Model.ObjectType
import DeploymentLab.UndoBuffer
import DeploymentLab.channellogger.Logger
import com.panduit.sz.api.shared.Point2D
import com.panduit.sz.api.ss.assets.Floorplan
import com.panduit.sz.api.ss.assets.Rack

import javax.swing.JFrame
import javax.swing.WindowConstants
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import java.math.RoundingMode
import java.text.DecimalFormat

/**
 * Created by LNI on 2/29/2016.
 */
public class MergeLogicController {

    private static final Logger log = Logger.getLogger(MergeLogicController.class.getName())

    private JFrame parent
    private ComponentModel componentModel
    private ObjectModel objectModel
    private UndoBuffer undoBuffer
    private static DLComponent drawing
    private static Floorplan fp

    private static MergeProcessCompleteListener listener
    private static SyncWithServerListener syncListener

    private static boolean conflictsResolved = false
    static boolean isConflictsResolved() {
        return conflictsResolved
    }

    private MergeConfigurationDialog dialog

    def szTableDataList = new ArrayList<MergeConflictData>()
    MergeModel szModel = new MergeModel(true)

    def msTableDataList = new ArrayList<MergeConflictData>()
    MergeModel msModel = new MergeModel(false)

    String[] szColumnNamesList = ["Name", "Position(x, y)", "Size(WxD)", "Rotation", ""]
    String[] msColumnNamesList = ["", "Name", "Position(x, y)", "Size(WxD)", "Rotation"]

    Set<Integer> szConflictColumns = new HashSet<Integer>()
    Set<Integer> msConflictColumns = new HashSet<Integer>()

    private static HashMap<DLComponent, HashMap<String, Object>> orgCompMap = new HashMap<DLComponent, HashMap<String, Object>>()


    private static final DecimalFormat one_df = new DecimalFormat("0.#");
    private static final DecimalFormat two_df = new DecimalFormat("0.##");

    // This map holds the list of all the SZ Racks and MS racks (DLComponents) that needs to be merged.
    private Map<DLComponent, Rack> mergeResults = new HashMap<DLComponent, Rack>()

    public MergeLogicController(DLComponent drawing,
                                Floorplan fp,
                                ComponentModel componentModel,
                                ObjectModel objectModel,
                                UndoBuffer undoBuffer,
                                JFrame parent) {
        this.drawing = drawing
        this.fp = fp
        this.componentModel = componentModel
        this.objectModel = objectModel
        this.undoBuffer = undoBuffer
        this.parent = parent

        this.listener = null
        this.syncListener = null

        one_df.setRoundingMode(RoundingMode.DOWN)
        two_df.setRoundingMode(RoundingMode.DOWN)
    }

    public void runMergeLogic() {
        // Per component merge

        drawing.setPropertyValue('name', fp.getName())
        for (Rack szRack : fp.getRacks()) {
            DLComponent msComponent = getRackComponentInMS(szRack.getId())
            //Returns the msComponent only if it is not deleted.
            if (msComponent) {
                mergeResults.put(msComponent, szRack)
            } else if (!msComponent && !isDeletedObject(szRack.getId())) {
                //Create in MS
                createSZNewComponent(szRack, drawing)
            }
        }

        for (DLComponent component : componentModel.getComponentsInDrawing(drawing)) {
            if (component.hasManagedObjectOfType(ObjectType.RACK)) {
                if (component.isSmartZoneLinked() && !component.getSmartZoneRack(fp) && !isDeletedObject(component.getSmartZoneId())) {
                    componentModel.remove(component)
                }
            }
        }

        doPerPropertyMerge()
        szModel.setColumnNamesList(szColumnNamesList)
        msModel.setColumnNamesList(msColumnNamesList)

        szModel.setTableDataList(szTableDataList)
        msModel.setTableDataList(msTableDataList)
    }

    public void showDialog() {
        if (hasConflicts()) {
            conflictsResolved = false
            dialog = new MergeConfigurationDialog(parent, getSzModel(), getMsModel())
            dialog.show()
            dialog.getContinueButton().addActionListener(new ActionListener() {
                @Override
                void actionPerformed(ActionEvent e) {
                    if (dialog.hasUnSelectedRows()) {
                        showErrorMsg()
                    } else {
                        onNoConflicts()
                        dialog.close()
                    }
                }
            })
            dialog.getCancelButton().addActionListener(new ActionListener() {
                @Override
                void actionPerformed(ActionEvent e) {
                    onCancel()
                }
            })

            dialog.getMergeDialog().setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            dialog.getMergeDialog().addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent arg0) {
                    onCancel()
                }
            });
        } else {
            onNoConflicts()
        }
    }

    private void onCancel() {
        dialog.close()
        undoBuffer.rollbackOperation()
        undoBuffer.finishOperation()
    }

    public void onNoConflicts() {
        conflictsResolved = true
        if(syncListener)
            syncListener.synced()
        if(listener)
            listener.isComplete()
        undoBuffer.finishOperation()
    }

    private void showErrorMsg() {
        log.error('Reconcile all configuration changes to continue.', 'message')
    }

    private DLComponent createSZNewComponent(Rack rack, DLComponent drawing) {
        def newRackComp
        newRackComp = componentModel.newComponent(ComponentType.RACK_UNINSTRUMENTED)
        newRackComp.setDrawing(drawing)
        newRackComp.setSmartZoneId(rack.getId())
        newRackComp.setPropertyValue('name', rack.getName())
        newRackComp.setPropertyValue('x', rack.getLocation().getX())
        newRackComp.setPropertyValue('y', rack.getLocation().getY())

        newRackComp.setPropertyValue('rotation', rack.getRotation())
        newRackComp.setPropertyValue('width', rack.getWidth())
        newRackComp.setPropertyValue('depth', rack.getDepth())

        return newRackComp
    }

    private void doPerPropertyMerge() {
        for (Map.Entry<DLComponent, Rack> entry : mergeResults.entrySet()){
            szConflictColumns = new ArrayList<Integer>()
            msConflictColumns = new ArrayList<Integer>()

            DLComponent eachComponent = entry.getKey()
            Rack eachRack = entry.getValue()

            //Always apply the remote width and depth.
            eachComponent.setPropertyValue('width', eachRack.getWidth())
            eachComponent.setPropertyValue('depth', eachRack.getDepth())

            List<DLObject> rackObjs = eachComponent.getObjectsOfType( ObjectType.RACK )

            int szIdMatch = 0
            for (DLObject rackObj: rackObjs) {
                if(rackObj.isSmartZoneLinked() && rackObj.getObjectSetting('smartZoneId').getValue() == eachRack.getId()){
                    szIdMatch++
                    compareProperty('name', eachRack, rackObj, eachComponent)
                    compareProperty('xy', eachRack, rackObj, eachComponent)
                    compareProperty('rotation', eachRack, rackObj, eachComponent)
                }
            }

            if (szIdMatch>1) {
                // Future, if multiple objects in a component represent same SmartZone component. Need to see how to handle then.
                log.error("Multiple Objects have same smartZoneId for component: " + eachComponent)
            }

            if(szConflictColumns.size()>0 && msConflictColumns.size()>0) {
                generateRowData(eachComponent)
            }
        }
    }

    private void compareProperty(String propertyName, Rack rack, DLObject rackObj, DLComponent component){
        switch (propertyName) {
            case "name":
                Object oldVal = rackObj.getObjectSetting('name').getOldValue()
                Object newVal = component.getPropertyValue('name')
                if (rack.getName() != oldVal && oldVal == newVal && rack.getName() != newVal) {
                    component.setPropertyValue(propertyName, rack.getName())
                } else if (rack.getName() != oldVal && oldVal != newVal && rack.getName() != newVal) {
                    szConflictColumns.add(0)
                    msConflictColumns.add(1)
                }
            case "xy":
                Double xOldVal = rackObj.getObjectSetting('x').getOldValue()
                if (xOldVal == null || xOldVal == "") {
                    xOldVal = Double.NaN
                }
                Double yOldVal = rackObj.getObjectSetting('y').getOldValue()
                if (yOldVal == null || yOldVal == "") {
                    yOldVal = Double.NaN
                }

                Double xNewVal = component.getPropertyValue('x')
                Double yNewVal = component.getPropertyValue('y')

                if (!rack.getLocation().equals(new Point2D(xOldVal, yOldVal)) && xOldVal == xNewVal && yOldVal == yNewVal && !rack.getLocation().equals(new Point2D(xNewVal, yNewVal))) {
                    component.setPropertyValue('x', rack.getLocation().getX())
                    component.setPropertyValue('y', rack.getLocation().getY())
                } else if (!rack.getLocation().equals(new Point2D(xOldVal, yOldVal)) && (xOldVal != xNewVal || yOldVal != yNewVal) && !rack.getLocation().equals(new Point2D(xNewVal, yNewVal))) {
                    szConflictColumns.add(1)
                    msConflictColumns.add(2)
                }
            case "rotation":
                Double oldVal = rackObj.getObjectSetting('rotation').getOldValue()
                if (oldVal == null || oldVal == "") {
                    oldVal = Double.NaN
                }
                Double newVal = component.getPropertyValue('rotation')
                if(!rack.rotationEquals(oldVal) && oldVal == newVal && !rack.rotationEquals(newVal)) {
                    component.setPropertyValue('rotation', rack.getRotation())
                } else if(!rack.rotationEquals(oldVal) && oldVal != newVal && !rack.rotationEquals(newVal)) {
                    szConflictColumns.add(3)
                    msConflictColumns.add(4)
                }
        }
    }

    private void generateRowData(DLComponent component) {
        HashMap<String, Object> origCompProperties = new HashMap<String, Object>()
        origCompProperties.put('name', component.getPropertyValue('name'))
        origCompProperties.put('x', component.getPropertyValue('x'))
        origCompProperties.put('y', component.getPropertyValue('y'))
        origCompProperties.put('width', component.getPropertyValue('width'))
        origCompProperties.put('depth', component.getPropertyValue('depth'))
        origCompProperties.put('rotation', component.getPropertyValue('rotation'))

        HashMap<String, Object> origCompPropertiesUnmodifiable = Collections.unmodifiableMap(origCompProperties)

        orgCompMap.put(component, origCompPropertiesUnmodifiable)

        Rack szRack = mergeResults.get(component)

        def remoteProperties = new ArrayList()
        remoteProperties.add(szRack.getName())
        remoteProperties.add(one_df.format(szRack.getLocation().getX()) + ", " + one_df.format(szRack.getLocation().getY()))
        remoteProperties.add(one_df.format(szRack.getWidth()) + "x" + one_df.format(szRack.getDepth()))
        remoteProperties.add(two_df.format(szRack.getRotation()))
        remoteProperties.add(false)

        MergeConflictData conflictRemoteData = new MergeConflictData(component, remoteProperties, szConflictColumns)
        szTableDataList.add(conflictRemoteData)

        def localProperties = new ArrayList()
        localProperties.add(false)
        localProperties.add(component.getPropertyValue('name'))
        localProperties.add(one_df.format(component.getPropertyValue('x')) + ", " + one_df.format(component.getPropertyValue('y')))
        localProperties.add(one_df.format(component.getPropertyValue('width')) + "x" + one_df.format(component.getPropertyValue('depth')))
        localProperties.add(two_df.format(component.getPropertyValue('rotation')))

        MergeConflictData conflictLocalData = new MergeConflictData(component, localProperties, msConflictColumns)
        msTableDataList.add(conflictLocalData)
    }

    public static void updateDLComponent(DLComponent component, boolean isRemote) {
        List<DLObject> msObjects = component.getObjectsOfType( ObjectType.RACK )
        for(DLObject obj : msObjects) {
            // Checking only one object in the component
            if (!obj.getDeleted()) {
                if (isRemote) {
                    Rack szProperties = component.getSmartZoneRack(fp)
                    component.setPropertyValue('name', szProperties.getName())
                    component.setPropertyValue('x', szProperties.getLocation().getX())
                    component.setPropertyValue('y', szProperties.getLocation().getY())
                    component.setPropertyValue('rotation', szProperties.getRotation())
                } else {
                    HashMap<String, Object> orgProp = orgCompMap.get(component)
                    component.setPropertyValue('name', orgProp.get('name'))
                    component.setPropertyValue('x', orgProp.get('x'))
                    component.setPropertyValue('y', orgProp.get('y'))
                    component.setPropertyValue('rotation', orgProp.get('rotation'))
                }
                break
            }
        }
    }

    public boolean hasConflicts() {
        if (szTableDataList.size()>0 && msTableDataList.size()>0) {
            return true
        }
        return false
    }

    MergeModel getSzModel() {
        return szModel
    }

    MergeModel getMsModel() {
        return msModel
    }

    DLComponent getRackComponentInMS(Integer szId) {
        for (DLComponent component : componentModel.getComponentsInDrawing(drawing)) {
            if (component.hasManagedObjectOfType(ObjectType.RACK) && component.isSmartZoneLinked() && component.getSmartZoneId() == szId) {
                return component
            }
        }
        return null
    }

    private boolean isDeletedObject(Integer szId) {
        for (DLObject deletedObject : objectModel.listDeletedObjects()) {
            if (deletedObject.isSmartZoneLinked() && deletedObject.getObjectSetting('smartZoneId').getValue() == szId) {
                return true
            }
        }
        return false
    }

    public void setProcessCompleteListener(MergeProcessCompleteListener listener) {
        this.listener = listener;
    }

    public void setSyncWithServerListener(SyncWithServerListener syncListener) {
        this.syncListener = syncListener
    }
}

public interface MergeProcessCompleteListener {
    public void isComplete()
}

public interface SyncWithServerListener {
    public void synced()
}