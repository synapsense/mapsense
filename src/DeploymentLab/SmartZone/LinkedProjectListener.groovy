package DeploymentLab.SmartZone

import DeploymentLab.Model.ComponentType
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.MetamorphosisEvent
import DeploymentLab.Model.ModelChangeListener

/**
 * Created by LNI on 1/29/2016.
 */
class LinkedProjectListener implements ModelChangeListener {

    LinkedProjectListener() {

    }

    public void componentPropertyChanged(def who, def propertyName, def oldVal, def newVal) {
        switch (propertyName) {
            case "smartZoneId":
                this.updateEditableProperty(who, newVal)
                break
        }
    }

    /**
     * Updates the editable property for width/depth based on the smartZoneId property. If the smartZoneId is set,
     * then the width/depth are not editable.
     * @param who - The DLComponent with the smartZoneId.
     * @param newVal - smartZoneId value
     */
    private void updateEditableProperty(def who, def newVal) {
        if (newVal == null || newVal == "") {
            who.getComponentProperty('width').setEditable(true)
            who.getComponentProperty('depth').setEditable(true)
        } else {
            who.getComponentProperty('width').setEditable(false)
            who.getComponentProperty('depth').setEditable(false)
        }
    }

    @Override
    void componentAdded(DLComponent component) {
        if (( component.getType() != ComponentType.DRAWING ) && component.canSmartZoneLink()) {
            component.addPropertyChangeListener(this, this.&componentPropertyChanged)
        }
    }

    @Override
    void componentRemoved(DLComponent component) {
        if (( component.getType() != ComponentType.DRAWING ) && component.canSmartZoneLink()) {
            component.removePropertyChangeListener(this)
        }
    }

    @Override
    void childAdded(DLComponent parent, DLComponent child) {

    }

    @Override
    void childRemoved(DLComponent parent, DLComponent child) {

    }

    @Override
    void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {

    }

    @Override
    void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {

    }

    @Override
    void modelMetamorphosisStarting(MetamorphosisEvent event) {

    }

    @Override
    void modelMetamorphosisFinished(MetamorphosisEvent event) {

    }
}
