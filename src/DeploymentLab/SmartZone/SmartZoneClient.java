package DeploymentLab.SmartZone;

import DeploymentLab.channellogger.Logger;
import com.panduit.sz.api.shared.IdNameTuple;
import com.panduit.sz.api.ss.assets.Floorplan;
import com.panduit.sz.api.ss.assets.FloorplanService;
import com.panduit.sz.api.ss.assets.Location;
import com.panduit.sz.api.ss.assets.LocationService;
import com.panduit.sz.api.ss.assets.SsAssets;
import com.panduit.sz.client.shared.SmartZoneRestClient;
import com.panduit.sz.client.shared.SslHelper;
import java.util.List;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.client.RestTemplate;

/**
 * Implementation of the SmartZone services using the SmartZone Rest Client. This is simplified for use by DeploymentLab
 * and may contain DeploymentLab specifics.
 *
 * @author Ken Scoggins
 * @since SmartZone 8.0.0
 */
public class SmartZoneClient implements LocationService, FloorplanService {
    private static final Logger log = Logger.getLogger(SmartZoneClient.class.getName());


    private final SmartZoneRestClient szRestClient;

    // Maybe get these from the rest client instead, but they are not accessible right now, so...
    private final String host;
    private final int port;
    private final String username;
    private final String password;


    static {
        SslHelper.configure();
    }

    public SmartZoneClient( String host, int port, String username, String password ) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;

        szRestClient = new SmartZoneRestClient( host, port, username, password );

        log.debug("New SmartZone Client: " + username + "@" + host + ":" + port );
    }

    /**
     * The SmartZone server host used by this session.
     *
     * @return The SmartZone host name or IP address.
     */
    public String getHost() {
        return host;
    }

    /**
     * The SmartZone server port used by this session.
     *
     * @return The SmartZone port number.
     */
    public int getPort() {
        return port;
    }

    /**
     * The SmartZone username used by this session.
     *
     * @return The SmartZone username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * The SmartZone user password used by this session.
     *
     * @return The SmartZone user plaintext password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Expose the underlying {@code RestTemplate} for testing.
     */
    protected RestTemplate getTemplate() {
        return szRestClient.getRestTemplate();
    }

    @Override
    public List<Location> getLocationTree() {
        return szRestClient.getForObject( SsAssets.locationsPath(), new ParameterizedTypeReference<List<Location>>() {} );
    }

    @Override
    public Floorplan getFloorplan( Integer floorplanId ) {
        return szRestClient.getForObject( SsAssets.floorplanPath( floorplanId ), Floorplan.class );
    }

    @Override
    public Integer createFloorplan( Integer parentLocationId, Floorplan floorplan ) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<IdNameTuple> updateFloorplan( Floorplan floorplan ) {
       return szRestClient.putForObject(SsAssets.floorplanPath(floorplan.getId()), floorplan, new ParameterizedTypeReference<List<IdNameTuple>>() {});
    }

    @Override
    public boolean instrumentFloorplan( Integer floorplanId, String filePath ) {
        return szRestClient.putForObject(SsAssets.instrumentationPath(floorplanId), filePath, new ParameterizedTypeReference<Boolean>() {});
    }

    @Override
    public boolean uninstrumentFloorplan( Integer floorplanId ) {
        return szRestClient.deleteForObject(SsAssets.instrumentationPath(floorplanId), Boolean.class);
    }
}
