package DeploymentLab

import DeploymentLab.channellogger.*
import javax.swing.JComponent

public enum UIDisplay {
		INSTANCE;

	private static final Logger log = Logger.getLogger(UIDisplay.class.getName())

	private properties = [:]
	private UIChangeListeners = []
	private ClassLoader cl
	def UIDisplayXml
	
	private UIDisplay(){
		properties['accessLevel'] = 0
		ClassLoader cl = UIDisplay.class.getClassLoader()
		UIDisplayXml = new XmlSlurper().parse(cl.getResourceAsStream('cfg/uidisplays.xml'))
	}

	public void setProperty(String pName, Object pValue) {
		properties[pName] = pValue
		fireUIChanged()
	}

	public Object getProperty(String pName) {
		if(properties[pName]==null){
			log.info("UIDisplay: no property for '$pName'")
			return false
		} else {
			return properties[pName]
		}
	}

	void addUIChangeListener(def who, String UIID) {
		def uiObj = getUIObject(UIID, who)
		
		if(uiObj!=null){
			UIChangeListeners.add(uiObj) 
			uiObj.setDisplay(isEnabled(uiObj), isVisible(uiObj))
		}else{
			log.info('cannot find UI object :' + UIID)
			who.setEnabled(false)
		}
	}

	private Object getUI(String UIID){
		if(UIDisplayXml.UIs.UI.findAll{it.@id==UIID}.size()>0){
			return UIDisplayXml.UIs.UI.find{it.@id==UIID}
		}else
			return null
	}

	private UIObject getUIObject(String UIID, def who){
		if(UIDisplayXml.UIs.UI.findAll{it.@id==UIID}.size()>0){
			def UI = UIDisplayXml.UIs.UI.find{it.@id==UIID}
			
			def enabledProperties = []
			if(UI.@enabledproperties.text().length() > 0)
				enabledProperties = UI.@enabledproperties.text().split(',')
			else
				enabledProperties = []
			
			def visibleProperties = []
			if(UI.@visibleproperties.text().length() > 0)
				visibleProperties = UI.@visibleproperties.text().split(',')
			else
				visibleProperties = []
			
			int level = Integer.parseInt(UI.@level.text())
			
			return new UIObject(UIID, who, level, enabledProperties, visibleProperties)
		}else
			return null
	
	}

	public void fireUIChanged(){
		UIChangeListeners.each{obj ->
			obj.setDisplay(isEnabled(obj), isVisible(obj))
		}
	}

	public boolean isEnabled(UIObject obj){
		boolean accessibilityByProperty = true
		
		def enabledProperties = obj.getEnabledProperties()
		if(enabledProperties.size()==0){
			return true
		}else{
			boolean accessibilityByLevel = true
			enabledProperties.each{p ->
				if(p.equals("level")){
					int possibleAccessLevel = obj.getLevel();
					accessibilityByLevel = (getProperty('accessLevel')>=possibleAccessLevel)
					
				}else{
					accessibilityByProperty&=getProperty(p)
				}
			}
			return (accessibilityByProperty & accessibilityByLevel)
		}
	}

	public boolean isVisible(UIObject obj){
		boolean accessibilityByProperty = true
		def visibleProperties = obj.getVisibleProperties()
		
		if(visibleProperties.size()==0){
			return true
		}else{
			boolean accessibilityByLevel = true
			visibleProperties.each{p ->
				if(p.equals("level")){
					int possibleAccessLevel = obj.getLevel();
					accessibilityByLevel = (getProperty('accessLevel')>=possibleAccessLevel)
					
				}else{
					accessibilityByProperty&=getProperty(p)
				}
			}
			return (accessibilityByProperty & accessibilityByLevel)
		}
	}
	
	public boolean isVisible(String UIID){
		boolean accessibilityByProperty = true
		def obj = getUIObject(UIID, null)
		if(obj==null){
			return false
		}else{
			def visibleProperties = obj.getVisibleProperties()
			
			if(visibleProperties.size()==0){
				return true
			}else{
				boolean accessibilityByLevel = true
				visibleProperties.each{p ->
					if(p.equals("level")){
						int possibleAccessLevel = obj.getLevel();
						accessibilityByLevel = (getProperty('accessLevel')>=possibleAccessLevel)
						
					}else{
						accessibilityByProperty&=getProperty(p)
					}
				}
				return (accessibilityByProperty & accessibilityByLevel)
			}
		}
	}
	
	public boolean isEnabled(String UIID){
		boolean accessibilityByProperty = true
		def obj = getUIObject(UIID, null)
		if(obj==null){
			return false
		}else{
			def enabledProperties = obj.getEnabledProperties()
			if(enabledProperties.size()==0){
				return true
			}else{
				boolean accessibilityByLevel = true
				enabledProperties.each{p ->
					if(p.equals("level")){
						int possibleAccessLevel = obj.getLevel();
						accessibilityByLevel = (getProperty('accessLevel')>=possibleAccessLevel)
						
					}else{
						accessibilityByProperty&=getProperty(p)
					}
				}
				return (accessibilityByProperty & accessibilityByLevel)
			}
		}
	}
	
	public void setDisplay(String UIID, boolean _isEnabled, boolean _isVisible){
		def UIObjs = UIChangeListeners.findAll{obj-> obj.getId().equals(UIID)}
		
		UIObjs.each{it->
			it.setDisplay(_isEnabled, _isVisible)
		}

	}
	
	public void setDisplay(String UIID){
		def UIObjs = UIChangeListeners.findAll{obj-> obj.getId().equals(UIID)}
		
		UIObjs.each{it->
			it.setDisplay(isEnabled(UIID), isVisible(UIID))
		}
	}
	
	public void setDisplay(String UIID, def who){
		who.setEnabled(isEnabled(UIID))
		who.setVisible(isVisible(UIID))
	}
	
	public boolean isAccessible(String UIID){
		/*def UI = getUI(UIID)
		
		if(UI!=null ){
			int possibleAccessLevel = 0
			possibleAccessLevel = Integer.parseInt(UI.@level.text());
			return (getProperty('accessLevel')>=possibleAccessLevel)
		}else
			return false
			*/
		return (isEnabled(UIID) & isVisible(UIID)) 
	}

	public int getUserLevelId() {
		return (int) properties['accessLevel']
	}

	public String getUserLevel() {
		String userLevelName = ""
		String accessLevel = properties['accessLevel']
		try{
			if(properties['accessLevel']!=null && UIDisplayXml.userlevels.userlevel.findAll{it.@id==accessLevel}.size()>0){
				userLevelName = UIDisplayXml.userlevels.userlevel.find{it.@id==accessLevel}.@name.text()
			}
		} catch(IOException e) {
			log.error("Cannot get user level")
		} finally {
			return userLevelName
		}
	}
	
	public void setUserLevel(int level) {
		setProperty('accessLevel', level)
		log.info('[' + getUserLevel() + ']	', 'userLevel')
	}
	
	
}

public class UIObject{
	private String id
	private JComponent component
	def enabledProperties = []
	def visibleProperties = []
	private int level
	
	UIObject(String _id, JComponent _component, int _level, Object _enabledProperties, Object _visibleProperties){
		id = _id
		component = _component
		level = _level
		enabledProperties = _enabledProperties
		visibleProperties = _visibleProperties
	}
	
	String getId(){
		return id
	}

	Object getEnabledProperties() {
		return enabledProperties
	}
	
	Object getVisibleProperties(){
		return visibleProperties
	}
	
	JComponent getComponent(){
		return component
	}
	
	int getLevel(){
		return level
	}
	
	void setDisplay(boolean isEnabled, boolean isVisible){
		component.setEnabled(isEnabled)
		component.setVisible(isVisible)
	}

}