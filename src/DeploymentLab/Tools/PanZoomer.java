package DeploymentLab.Tools;

import edu.umd.cs.piccolo.event.*;

import DeploymentLab.SceneGraph.DeploymentPanel;

public class PanZoomer extends PBasicInputEventHandler {
	private DeploymentPanel deploymentPanel;

	public PanZoomer(DeploymentPanel dp) {
		deploymentPanel = dp;
	}

	public void mouseWheelRotated(PInputEvent e) {
		super.mouseWheelRotated(e);
		double amount = deploymentPanel.getZoom() - (e.getWheelRotation() * 0.05);
		deploymentPanel.setZoom(amount, e.getPosition());
		e.setHandled(true);
	}

	public void mouseDragged(PInputEvent e) {
		super.mouseDragged(e);

		if(!e.isRightMouseButton())
			return;

		deploymentPanel.pan(e.getDelta());
	}

	public void activate() {}
	public void activate(boolean isModal) {}
	public void deactivate() {}
}

