package DeploymentLab.Tools

import java.awt.geom.Point2D

import edu.umd.cs.piccolo.nodes.PPath

import edu.umd.cs.piccolo.event.*
import edu.umd.cs.piccolo.util.PBounds

import DeploymentLab.SceneGraph.DeploymentNode
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.SelectModel

import DeploymentLab.channellogger.*
import DeploymentLab.CentralCatalogue

class RectangularSelect extends PDragSequenceEventHandler {
	private static final Logger log = Logger.getLogger(RectangularSelect.class.getName())

	private SelectModel selectModel
	private DeploymentPanel deploymentPanel
	private PPath selectShape
	private Point2D startPoint = null
	private Point2D endPoint = null

	private boolean modal

	RectangularSelect(PPath ss, DeploymentPanel dp, SelectModel sm) {
		selectShape = ss
		selectModel = sm
		deploymentPanel = dp
		this.setMinDragStartDistance( CentralCatalogue.getApp("tools.minDragDistance").toInteger() )
	}

	public boolean shouldStartDragInteraction(PInputEvent e) {
		if ( ! super.shouldStartDragInteraction(e) ){
			return false
		}

		if((!e.isLeftMouseButton()) || e.getPickedNode() instanceof DeploymentNode){
			return false
		}
		//log.trace("sould start drag")
		return true
	}

	public void mousePressed(PInputEvent e) {
		super.mousePressed(e);

		// Get start point here rather than drag() so we get the initial position, not the position after minDragDistance is satisfied.
		startPoint = e.getPosition()
	}

	public void startDrag(PInputEvent e) {
		super.startDrag(e)
	}

	public void drag(PInputEvent e) {
		super.drag(e)
		//log.trace("dragging")
		endPoint = e.getPosition()
		PBounds b = new PBounds()
		b.add(startPoint)
		b.add(endPoint)
		selectShape.setPathTo(b)
		selectShape.setVisible(true)
	}

	public void endDrag(PInputEvent e) {
		super.endDrag(e)
		if(startPoint == null || endPoint == null)
			return

		//log.trace("ending drag")
		def components = selectedComponents()
		if(e.isControlDown())
			selectModel.toggleSelect(components)
		else
			selectModel.setSelection(components)
		endPoint = startPoint = null
		selectShape.setVisible(false)
	}

	private def selectedComponents() {
		// Something happened during the Aireo canvas refactor where scaling during the drag was broken,
		// but we still need to scale the points to correctly select.
		Point2D start = CentralCatalogue.getDeploymentPanel().scalePoint( startPoint )
		Point2D end = CentralCatalogue.getDeploymentPanel().scalePoint( endPoint )

		int x1 = Math.min(start.x, end.getX())
		int y1 = Math.min(start.y, end.getY())
		int x2 = Math.max(start.x, end.getX())
		int y2 = Math.max(start.y, end.getY())

		return deploymentPanel.listNodesInBox(x1, y1, x2, y2).collect{it.getComponent()}
	}

	void activate(boolean isModal = true) {
		modal = isModal
	}
	void deactivate() {
		startPoint = null
		endPoint = null
	}
}
