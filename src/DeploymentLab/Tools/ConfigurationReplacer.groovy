package DeploymentLab.Tools

import DeploymentLab.Dialogs.Association.AssociationController
import DeploymentLab.Exceptions.DuplicateObjectException
import DeploymentLab.Exceptions.IncompatibleTypeException
import DeploymentLab.Exceptions.UnknownComponentException
import DeploymentLab.Image.ComponentIconFactory
import DeploymentLab.Tree.ConfigurationReplacerTree
import DeploymentLab.Upgrade.UpgradeController
import DeploymentLab.channellogger.Logger
import groovy.swing.SwingBuilder
import java.awt.BorderLayout
import java.awt.Dialog.ModalityType
import java.awt.FlowLayout
import java.awt.GridBagConstraints

import net.miginfocom.swing.MigLayout
import DeploymentLab.*
import DeploymentLab.Model.*
import javax.swing.*
import DeploymentLab.Security.NetworkKey
import DeploymentLab.Image.ARGBConverter
import DeploymentLab.Exceptions.ModelCatastrophe


public class ConfigurationReplacer {
	private static final Logger log = Logger.getLogger(ConfigurationReplacer.class.getName())
	private SelectModel selectModel
	private ComponentModel componentModel
	def oldComponents = []
	private SwingBuilder builder
	private JDialog dialog
	private ConfigurationReplacerTree configTree
	private String oldConfiguration
	private UndoBuffer undoBuffer
	private JFrame parent
	private UpgradeController upgrader


	ConfigurationReplacer(SelectModel _selectModel, ComponentModel _componentModel, ConfigurationReplacerTree tree, UndoBuffer ub, JFrame _parent) {
		selectModel = _selectModel
		componentModel = _componentModel
		// tree mode and tree with componentModel
		configTree = tree
		undoBuffer = ub
		parent = _parent
		builder = new SwingBuilder()
		getConfigDialog()
	}

	public void setUpgradeController( UpgradeController uc ) {
		upgrader = uc
	}

	/**
	 * Create dialog box to choose new configuration
	 */
	public void getConfigDialog() {
		//oldConfiguration = getOldConfiguration()
		GridBagConstraints gbc = new GridBagConstraints()

		dialog = builder.dialog(owner: parent, layout: new BorderLayout(), resizable: false, title: 'Replace Configuration', modalityType: ModalityType.APPLICATION_MODAL) {

			panel(layout: new BorderLayout(), constraints: BorderLayout.NORTH) {
				label(text: "Select New Configuration", constraints: BorderLayout.NORTH)
				scrollPane(layout: new ScrollPaneLayout(), constraints: BorderLayout.CENTER, preferredSize: [400, 300]) {
					widget(configTree)
				}
			}

			panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.SOUTH) {
				button(text: 'OK', actionPerformed: {confirmConfigurationSelection()})
				button(text: 'Cancel', actionPerformed: {dialog.setVisible(false)})
			}
		}
		dialog.pack()

	}

	public void openConfigDialog() {
		dialog.setLocationRelativeTo(parent)
		ComponentLibrary.INSTANCE.pruneExtinctComponents()
		dialog.show()
	}

	private void confirmConfigurationSelection() {
		oldComponents = selectModel.getExpandedSelection()
		def newConfiguration = configTree.getConfiguration()

		if (oldComponents.size() == 0) {
			log.info("Please select a component to replace", 'message')
			return
		} else if (newConfiguration == null) {
			log.info("Please select new configuration to replace", 'message')
			return
		} else {
			dialog.setVisible(false);
			showMessages()
		}
	}

	private Object getOldConfiguration() {
		def selection = selectModel.getExpandedSelection()
		def types = selection.collect {it.getType()}.flatten().unique()
		return types
	}

	private void showMessages() {
		oldComponents = selectModel.getExpandedSelection()

		def replacableConfiguration = []
		def nonreplacableConfiguration = []
		def sameConfiguration = []
		def newConfiguration = configTree.getConfiguration()
		//def oldConfigurations = oldComponents.collect {it.getType()}.flatten().unique()
        boolean needProxyWarning = false

		//oldComponents.each {c ->
		for(def c : oldComponents){
            if( c.hasRole("proxy") ) {
                needProxyWarning = true
            } else if (newConfiguration.equals(c.getType())) {
				sameConfiguration.add(c)
				replacableConfiguration.add(c)
			} else {
				if (isReplacable(c, newConfiguration)) {
					replacableConfiguration.add(c)
				} else {
					nonreplacableConfiguration.add(c)
				}
			}
		}

		//oldComponents.clear()

		// show update and replace button  if only replacable configuration exists
		boolean canShowReplaceButton = false
		if (replacableConfiguration.size() > 0)
			canShowReplaceButton = true

		MessagePanel messagePanel = new MessagePanel(replacableConfiguration, nonreplacableConfiguration, sameConfiguration, needProxyWarning, newConfiguration, componentModel)
		// create dialog 
		JDialog infoDialog
		JButton okButton
		JButton cancelButton
		JButton updateButton

		def okAction = {infoDialog.hide(); replaceConfigTask(replacableConfiguration, false)}
		def updateAction = {infoDialog.hide(); replaceConfigTask(replacableConfiguration, true)}
		def cancelAction = {infoDialog.hide()}
		GridBagConstraints gbc = new GridBagConstraints()
		infoDialog = builder.dialog(owner: parent, layout: new BorderLayout(), resizable: true, title: 'Replace / Update Configuration', modalityType: ModalityType.APPLICATION_MODAL) {
			panel(layout: new BorderLayout(), constraints: BorderLayout.NORTH) {
				//scrollPane(autoscrolls:true, constraints:BorderLayout.CENTER,preferredSize: [350, 150]) {
				scrollPane(autoscrolls: true, constraints: BorderLayout.CENTER, preferredSize: [450, 250]) {
					widget(messagePanel)
				}
			}
			panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.SOUTH, visible: canShowReplaceButton) {
				updateButton = button(text: 'Update', actionPerformed: updateAction)
				okButton = button(text: 'Replace', actionPerformed: okAction)
				button(text: 'Cancel', actionPerformed: cancelAction)
			}
		}

		// Hide the UPDATE button if the new component will never be able to export since we want to purge the old
		// component from the server and doing an update won't allow for that. One use case is converting a SZ linked &
		// instrumented component back to an uninstrumented component. We want to update the server, but that won't happen
		// if the uninstrumented rack is updated since it will never be exported.
		String canExport = componentModel.getTypeAttribute( newConfiguration, 'exportable' )
		if( !(canExport.isEmpty() || Boolean.parseBoolean( canExport )) ) {
			updateButton.setEnabled( false )
		}

		infoDialog.pack()
		okButton.requestFocusInWindow()
		infoDialog.setLocationRelativeTo(parent)
		infoDialog.show()
	}

	private static HashSet<String> irreplaceables = null

	/**
	 * Determine if @{code oldComponent} can be converted to {@code newConfiguration}.
	 * The basic rules are that the two types must have the same parent types, must not be
	 * a child-class component, and must not be on the irreplaceable list.
	 * Note that the "same parents" rule cares about *possible* parents; the current parent list is not important.
	 * @param oldComponent the component that will be replaced
	 * @param newConfiguration the type that {@code oldComponent} will be turned in to
	 * @return true if the replacement is allowed
	 */
	protected boolean isReplacable(DLComponent oldComponent, String newConfiguration) {

		// child component is not replacable
		if (oldComponent.hasRole("staticchild")){
			return false
		}

		// check this is replaceable or not from components property
		if(!irreplaceables){
			irreplaceables = new HashSet<String>()
			irreplaceables.addAll(CentralCatalogue.getInstance().getComponentsProperties().getProperty("components.irreplaceable").split(','))
		}
		if ((irreplaceables.contains(oldComponent.getType()) || irreplaceables.contains(newConfiguration) ) && !oldComponent.getType().equals(newConfiguration)) {
			return false
		}

		// See if the subset of common objects matches one of the types (once we filter the misc. objects we don't care about).
		// This allows for like-things to be replaceable without there being a perfect match on both sides. Like converting
		// between uninstrumented and instrumented racks and crahs, or instrumented power racks and power-only racks.
		Set<String> newTypes = getObjectTypesToCheck( newConfiguration )
		Set<String> oldTypes = getCompObjectTypesToCheck( oldComponent )
		Set<String> intersection = newTypes.intersect( oldTypes )

		if( !intersection.isEmpty() && ( newTypes.equals( intersection ) || oldTypes.equals( intersection ) ) ){
			return true
		}

		println("isReplacable FAIL: ${oldComponent.getType()} --> $newConfiguration  OLD$oldTypes  NEW$newTypes")

		return false
	}

	private Set<String> getObjectTypesToCheck( String type ) {
		Set<String> otypes = [] as Set
		for( String ot : componentModel.listManagedObjectTypes( type ) ) {
			// Don't care about some of the objects that weren't roots prior to a few releases ago.
			if( !ot.startsWith('controlpoint_') && !ot.startsWith('controlalg_') ) {
				otypes.add( ot )
			}
		}
		return otypes
	}

	private Set<String> getCompObjectTypesToCheck( DLComponent comp ) {
		Set<String> otypes = [] as Set
		for( String oldCompManagedType : comp.listManagedTypes() ) {
			// Don't care about some of the objects that weren't roots prior to a few releases ago.
			if( !oldCompManagedType.startsWith('controlpoint_') && !oldCompManagedType.startsWith('controlalg_') ) {
				otypes.add( oldCompManagedType )
			}
		}
		return otypes
	}

	public void replaceConfigTask(Object components, Boolean copyObjectData = false) {
		ProgressManager.doTask {
			replaceConfig(components, copyObjectData)
		}
	}

	/**
	 * Does a Replace / Update Configuration
	 * @param components a List of components to replace
	 * @param copyObjectData if true, also copy object level data (TOs, overrides, oldValues)
	 * @param showProgress if true, replaceConfig() will update the progress pane message.  Set to false if the calling method wants to handle that
	 */
	public void replaceConfig(Object components, Boolean copyObjectData = false, Boolean showProgress = true) {
		List<DLComponent> resultingComponents = []
		String newConfiguration
		try {
			newConfiguration = configTree.getConfiguration()
			def successComponents = []
			def failComponents = []

			if (copyObjectData) {
				log.trace("Updating Configuration:")
				log.trace("From: $components")
				log.trace("To: $newConfiguration")
				//ProjectSettings.getInstance().addAnnotation("Update Configuration: $components")
			}


			int numComponents = components.size()
			int i = 1
			List<DLComponent> newComponents = new ArrayList<>()
			try {
				undoBuffer.startOperation()
				componentModel.metamorphosisStarting( ModelState.REPLACING )
				//components.each{c->
				for (DLComponent c: components) {
					if (c.hasRole("staticchild")) {
						log.warn("cannot replace staticchild ${c.getType()}")
						continue
					}

					if (showProgress) {
						if (!copyObjectData) {
							//log.info('Replacing Configuration ' + i + '/' + numComponents, 'progress')

							if (c.getType().equals(newConfiguration)) {
								log.info("Replacing Configuration on '$newConfiguration': $i / $numComponents", 'progress')
							} else {
								log.info("Replacing Configuration from '${c.getType()}' to '$newConfiguration': $i / $numComponents", 'progress')
							}

						} else {
							log.info("Updating Configuration '$newConfiguration': $i / $numComponents", 'progress')
						}
					}

					//println "U/R C on $c"
					log.trace("Update or Replace on $c")

					// 1. get parents
					def parents = c.getParentComponents()

					// 2. create new component
					DLComponent newComponent
					try {
						newComponent = componentModel.newComponent(newConfiguration)
						newComponents.add(newComponent)
					} catch (UnknownComponentException e) {
						//todo: issue a failure report at the end of this process
						log.error("Attempted to Replace unknown component $newConfiguration", "default")
						continue
					}

					if( upgrader != null ) {
						upgrader.preComponentUpdate( c, newComponent )
					}

					// 3. copy properties
					log.trace("Copy Properties: $c --> $newComponent")
					copyProperties(c, newComponent)

                    // Not the cleanest place to cram this, but... if upgrading from something that does not yet have
                    // security keys, generate some. Otherwise, do nothing so that the copied keys are used.
                    if( newComponent.hasProperty("networkPublicKey") && ( newComponent.getPropertyValue("networkPublicKey") == null ) ) {
                        def p = NetworkKey.makeNetworkKeys()
                        newComponent.setPropertyValue('networkPublicKey', p.a )
                        newComponent.setPropertyValue('networkPrivateKey', p.b )
                    }

					// Dirty path for rowlite and horizontal row
					if (newComponent.hasClass("staticchildplaceable")) {

						for (DLComponent childRackComponent: newComponent.getChildComponents()) {
							String childName = childRackComponent.getPropertyStringValue("child_id")
							addToParent(newComponent, childRackComponent);

							// if old component is the one after Mars

							//if oldChild is null, we need to treat this like the pre-mars version
							def oldChildComponent = c.getChildComponents().find {child -> child.getPropertyValue("child_id").equals(childName)}
							if (c.getChildComponents().size() > 0 && oldChildComponent!= null ) {
								//DLComponent oldChildComponent = c.getChildComponents().find {child -> child.getPropertyValue("child_id").equals(childName)}

								// copy child component properties
								//BUT, only copy non-synchronized properties

								log.trace("Copy Child Properties: $oldChildComponent --> $childRackComponent")
								copyProperties(oldChildComponent, childRackComponent, newComponent.synchronizedProperties)
								
								
							} else {
								if (newComponent.getPropertyValue(childName + "_name") == null || newComponent.getPropertyStringValue(childName + "_name").trim().length() == 0)
									childRackComponent.setPropertyValue("name", childName);
								else
									childRackComponent.setPropertyValue("name", newComponent.getPropertyStringValue(childName + "_name"));
								// copy parent name
								childRackComponent.setPropertyValue("parent_name", c.getPropertyStringValue("name"));
							}
							//log.trace("child name:" + childRackComponent.getPropertyStringValue("name"));
							//log.trace("child id:" + childRackComponent.getPropertyStringValue("child_id"));
							//log.trace("child channel:" + childRackComponent.getPropertyStringValue("channel"));

						}

					} else if (newConfiguration.equalsIgnoreCase("drawing")) {
						//never copy the rect-spec
						newComponent.setPropertyValue("rect_spec", "")
					}

					// 3-2 reset mac ids => change function requirement: preserve mac ids
					//newComponent.listMacIdProperties().each{macid->newComponent.setPropertyValue(macid,0)}

					//SUPER SECRET MODE: copy over ES Keys and oldVals
					//if ( copyObjectData &&  ( newConfiguration.equalsIgnoreCase( c.getType() ) ) ){
					if (copyObjectData) {
						c.listRootNames().each { root ->
							def newRoot = root
							if( !newComponent.hasRoot(root) ) {
								// See if we have a conversion mapping for the root name.
								if( CentralCatalogue.hasObjectNameConversion( c.getType(), root ) ) {
									newRoot = CentralCatalogue.getObjectNameConversion( c.getType(), root )
									log.info("Object name change detected: $root -> $newRoot")
								}
							}

							if (newComponent.hasRoot(newRoot)) {
								log.trace("copyObjectData: root:" + root + " -> " + newRoot )
								DLObject newRootObj = newComponent.getRoot(newRoot)
								DLObject oldRootObj = c.getRoot(root)
								copyObject(oldRootObj, newRootObj)

								//don't recurse into children for these guys, we'll get there in due course
								if (!c.isContainerOrNetwork() && ! c.hasClass("dynamicchildplaceable")) {
									copyChildren(oldRootObj, newRootObj, c.getType(), newComponent.getType(), c)
								}

							} else {
								log.warn("Object '$root' not found. Data not copied. New: $newComponent  Old: $c")
							}
						}

						if (newComponent.getType().equals("dual-horizontal-row-thermanode2")) {
							//log.trace("start children object key in dual horizontal row")
							ArrayList<DLComponent> childComponentList = newComponent.getChildComponents();
							Map<String, Map<String, String>> childrenMap = newComponent.getPlaceableChildrenMap();
							log.info("$childrenMap")
							log.info("$childComponentList")

							for (String childName: childrenMap.keySet()) {
								String oldRootName = childName
								log.info("$oldRootName")
								DLComponent childRackComponent = getComponentByProperty(childComponentList, "child_id", childName);

								DLObject oldRootObj = c.getRoot(oldRootName)
								//in the case of NOT an upgrade, but a regular UC,
								// since racks were RowLite roots only in Earth and earlier
								if (oldRootObj==null){
									oldRootObj = getComponentByProperty(c.getChildComponents(), "child_id", oldRootName).getRoot("rack")
								}
								DLObject newRootObj = childRackComponent.getRoot("rack")

								if (oldRootObj != null && newRootObj != null) {
									copyObject(oldRootObj, newRootObj)
									DLComponent oldChildComp = componentModel.getManagingComponent(oldRootObj)
									copyChildren( oldRootObj, newRootObj, oldChildComp.getType(), childRackComponent.getType(), oldChildComp )

									// Update the rest of the objects.
									oldChildComp.listRootNames().each { rootName ->
										if( !( rootName in [ oldRootName, 'rack' ] ) && childRackComponent.hasRoot( rootName ) ) {
											log.trace("copyObjectData: root:" + rootName)
											DLObject oldObj = oldChildComp.getRoot( rootName )
											DLObject newObj = childRackComponent.getRoot( rootName )
											copyObject( oldObj, newObj )
											copyChildren( oldObj, newObj, oldChildComp.getType(), childRackComponent.getType(), oldChildComp )
										} else {
											log.warn("Object '$rootName' not found. Data not copied. New: $childRackComponent  Old: $c")
										}
									}
								}
							}
						}

						if( c.getType().equals("single-horizontal-row-thermanode2") ) {
							for( String childName : c.getPlaceableChildrenMap().keySet()) {
								DLComponent oldChild = getComponentByProperty( c.getChildComponents(), "child_id", childName );
								DLComponent newChild = getComponentByProperty( newComponent.getChildComponents(), "child_id", childName );

								if( oldChild && newChild ) {
									oldChild.listRootNames().each { rootName ->
										if( newChild.hasRoot( rootName ) ) {
											log.trace("copyObjectData: root:" + rootName)
											DLObject oldObj = oldChild.getRoot( rootName )
											DLObject newObj = newChild.getRoot( rootName )
											copyObject( oldObj, newObj )
											copyChildren( oldObj, newObj, oldChild.getType(), newChild.getType(), oldChild )
										}
									}
								}
							}
						}
					}

					// Old 6. add new component to parents (Moved here so getDrawing on the new component works during association adds.)
					log.trace("UC: add to parents $newComponent to $parents")
					parents.each {parent -> parent.addChild(newComponent)}

					// 4. copy associations
					// 4-1 copy common producer
					copyProducers(c, newComponent)

					// 4-2 copy common consumer
					copyConsumers(c, newComponent)

					// 4-3 copy common conducers
					copyConducers(c, newComponent)

					// Dirty producer path for rowlite and horizontal row
					if (newComponent.hasClass("staticchildplaceable")) {
						ArrayList<DLComponent> childComponentList = newComponent.getChildComponents();

						// if horizontal row component is after Mars
						if (c.getChildComponents().size() > 0) {

							Map<String, Map<String, String>> childrenMap = newComponent.getPlaceableChildrenMap();

							for (String childName: childrenMap.keySet()) {
								DLComponent oldChildComponent = c.getChildComponents().find {child -> child.getPropertyValue("child_id").equals(childName)}
								DLComponent newChildComponent = newComponent.getChildComponents().find {child -> child.getPropertyValue("child_id").equals(childName)}
								
								if (oldChildComponent && newChildComponent){
									// 4. copy associations
									// 4-1 copy common producer
									copyProducers(oldChildComponent, newChildComponent)

									// 4-2 copy common consumer
									copyConsumers(oldChildComponent, newChildComponent)

									// 4-3 copy common conducers
									copyConducers(oldChildComponent, newChildComponent)
								}
							}
						} else {	  // for legacy horizontal row
							def oldProducers = c.listProducers()
							oldProducers.each {producerId ->
								String childKeyword = ""
								String newProducerId = ""
								if (newComponent.getType().equals("single-horizontal-row-thermanode2")) {
									String channelStr = producerId.replace("temp", "")
									childKeyword = "c" + channelStr
									newProducerId = "temp"

								} else if (newComponent.getType().equals("dual-horizontal-row-thermanode2")) {
									String rackStr = producerId.replace("rack", "")
									rackStr = rackStr.replace("topinlet", "")
									childKeyword = "rack" + rackStr
									newProducerId = "racktopinlet"
								}
								DLComponent childComponent = getComponentByProperty(childComponentList, "child_id", childKeyword);

								def producer = c.getProducer(producerId)
								if (producer != null) {
									def consumerComponents = producer.listConsumerComponents()
									consumerComponents.each {pair ->
										String consumerId = pair.b;
										DLComponent consumerComponent = pair.a
										Consumer newConsumer = consumerComponent.getConsumer(consumerId)
										Producer newProducer = childComponent.getProducer(newProducerId)

										if( newConsumer == null ) {
											log.warn("Skipping association due to missing Consumer '$consumerId' in $consumerComponent.")
										} else if( newProducer == null ) {
											log.warn("Skipping association due to missing Producer '$newProducerId' in $childComponent.")
										} else {
											def consumerDatatype = newConsumer.getDatatype()
											def datatype = newProducer.getDatatype()

											if (datatype.equals(consumerDatatype)) {
												consumerComponent.associate(consumerId, childComponent, newProducerId)
											}
											//we're not going to do the else-case that we have down below, since that should really only apply
											//when replacing secondary components (COINs, etc.) and those should just be consumers.
										}
									}
								}
							}
						}


					}

					// 6. add new component to parents
					//println "UC: add to parents $newComponent"
					//parents.each {parent -> parent.addChild(newComponent)}  // Moved to just befre Step 4

					//make sure our kids get moved over to our replacement
					//if (c.isContainerOrNetwork() || c.hasRole("dynamicchildplaceable")) {
					if(!c.hasRole("staticchildplaceable")){
						//log.trace("Moving my children over to my clone from: $c to $newComponent")
						//println "moving children for $c"
						//println "my children are ${c.getChildComponents()}"

						//in order to keep all the listeners happy, we need to remove children before we add them
						//so that the addition is the last thing we do

						def childList = c.getChildComponents()
						log.trace("Removing my children")

						for(def k : childList){
							log.trace("remove $k from $c")
							c.removeChild(k)
						}
						log.trace("Hooking them up to my clone")

						for(def k : childList){
							log.trace("add $k to $newComponent")
							if( !newComponent.addChild(k) ) {
								// Maybe there was a model change for what is a valid parent? Try to plug it in somewhere else.
								Set<String> ptypes = componentModel.listParentComponentsOfType( k.getType() )
								boolean foundaparent = false
								for( DLComponent p : newComponent.getParentComponents() ) {
									if( ptypes.contains( p.getType() ) ) {
										if( p.addChild( k ) ) {
											foundaparent = true
											break
										}
									}
								}

								if( !foundaparent && componentModel.listParentComponentsOfType( k.getType() ).contains( ComponentType.ORPHANAGE ) ) {
									foundaparent = componentModel.getOrphanage( newComponent ).addChild( k )
								}

								if( !foundaparent ) {
									log.error("Failed to find a parent for $k. Old parent $newComponent")
								}
							}
						}
					}

					//and remove from old parents, so the remove operation doesn't take our kids with us
					//println "UC: remove from parents $c"
					log.trace("UC: remove $c from $parents")
					parents.each {parent -> parent.removeChild(c)}

					// Dirty path for rowlite and horizontal row
					if (newComponent.hasClass("staticchildplaceable")) {
						Map<String, Map<String, String>> childrenMap = newComponent.getPlaceableChildrenMap();
						String childComponentType = newComponent.getPlaceableChildType();
						ArrayList<DLComponent> childComponentList = newComponent.getChildComponents();
						for (String childName: childrenMap.keySet()) {
							//log.trace("add childComponent to parent" + childName);

							DLComponent childRackComponent = getComponentByProperty(childComponentList, "child_id", childName);
							addToParent(newComponent, childRackComponent);
						}
					}

                    // 4.5 create new proxies if the old one had any.
                    if( c.hasProxy() ) {
                        log.trace("Updating Proxies for $c")
                        for( DLProxy oldProxy : c.getProxies() ) {

                            // New proxy with the new component as the anchor.
                            DLProxy newProxy = DLProxy.cloneProxy( newComponent, oldProxy )

                            // ##: Something seems fishy here. Investigate. It's working, but isn't this childing to the old parent instance?
                            // Sign the adoption papers.
                            oldProxy.getParentComponents().each { parent -> parent.addChild( newProxy ) }

                            // Copy all the other tidbits.
                            copyProducers( oldProxy, newProxy )
                            copyConsumers( oldProxy, newProxy )
                            copyConducers( oldProxy, newProxy )

                            // No need to delete it here. The old one will get removed when the anchor is removed.
                        }
                    }

					// 5. delete old component
					log.trace("Remove $c")
					componentModel.remove(c)

					// 7. If a "secondary" component, regenerate the name
					//BUT, we don't want to do that on an update, just a replace
					if (newComponent.isSecondary() && !copyObjectData) {
						//first, we have to find the host component:
						log.trace("Replacing name on Secondary-class component $newComponent!")
						DLComponent host = null
						def CPs = newComponent.getConsumerProducers()
						if (CPs.size() > 0) {
							host = CPs.first()
						} else {
							def PCs = newComponent.getProducerConsumers()
							if (PCs.size() > 0) {
								host = PCs.first()
							} else {
								log.warn("Unable to find host to base new name on for $newComponent")
							}
						}
						//only recalc the secondary name if there is a host to base it on
						///println "basing new name on $host"
						if (host != null) {
							//naming the secondary component now outsourced to the Association Controller
							def ac = new AssociationController(host, newComponent)
							String cpName = ac.calculateSecondaryName(host, newComponent)
							newComponent.setPropertyValue("name", cpName)
						}
					}
					i++
					resultingComponents.add(newComponent)

                    // Keep track of the changing DLIDs just in case something is still using the old one.
                    ProjectSettings.getInstance().replaceDlid( c.getDlid(), newComponent.getDlid() )
					if( newComponent.hasClass("staticchildplaceable") ) {
						for( String childName : newComponent.getPlaceableChildrenMap().keySet() ) {
							DLComponent oldChildComponent = c.getChildComponents().find {child -> child.getPropertyValue("child_id").equals(childName)}
							DLComponent newChildComponent = newComponent.getChildComponents().find {child -> child.getPropertyValue("child_id").equals(childName)}

							// If null, it's probably a pre-Mars version where they were root objects rather than child components. So nothing to map.
							if( oldChildComponent != null ) {
								ProjectSettings.getInstance().replaceDlid( oldChildComponent.getDlid(), newChildComponent.getDlid() )
							}
						}
					}

					// Upgrades with changes to parent/child relationships may leave a component with no parent.
					// Try to assign a default parent so we don't have abandoned components.
					if( newComponent.getParentComponents().isEmpty() ) {

						// Make sure it is a valid drawing since the old component may be referencing the old one.
						DLComponent drawing = c.getDrawing()
						if( !componentModel.getComponentFromDLID( drawing.getDlid() ) ) {
							drawing = componentModel.getComponentFromDLID( ProjectSettings.getInstance().findNewDlid( drawing.getDlid() ) )
						}

						def parentTypes = componentModel.listParentComponentsOfType( newComponent.getType() )

						if( ComponentType.DRAWING in parentTypes ) {
							drawing.addChild( newComponent )
						} else if( ComponentType.ROOM in parentTypes ) {
							componentModel.getOrphanage( drawing ).addChild( newComponent )
						} else {
							log.debug("New component has no parent: $newComponent  OLD: $c")
						}
					}

					if( upgrader != null ) {
						upgrader.postComponentUpdate( c, newComponent )
					}

					//log.trace("I'm $newComponent, and my parents are: ${newComponent.getParentComponents()}")
				}
				selectModel.setSelection(resultingComponents)
			} finally {
				componentModel.metamorphosisFinished( ModelState.REPLACING )
				for (DLComponent newComponent:newComponents) {
					if (newComponent.getComponentProperty("x") != null) {
						newComponent.setPropertyValue("x", newComponent.getComponentProperty("x").value)
					}
				}
				undoBuffer.finishOperation()
			}
		} catch (Exception e) {
			log.error("Error replacing configuration to '$newConfiguration', could not complete request!", "message")
			log.info(log.getStackTrace(e))
			//all exceptions during a replace config are now Catastrophes
			throw new ModelCatastrophe("Error replacing configuration to '$newConfiguration', could not complete request!", e)
		}
	}

	/**
	 * Copy common properties between old and new component
	 * @param oldComponent
	 * @param newComponent
	 */
	private void copyProperties(DLComponent oldComponent, DLComponent newComponent, Map<String,String> syncMap = null) {
		// 3. copy properties
		HashSet<String> oldProperties = new HashSet<String>(oldComponent.listProperties())
		HashSet<String> newProperties = new HashSet<String>(newComponent.listProperties())

		if(syncMap){
			Set sync = new HashSet<String>(syncMap.values())
			if(sync.size() > 0){
				newProperties.removeAll(sync)
			}
		}

		// 3-1 copy common properties. sort so parcel props are after non-parcel props.
		Queue<ComponentProperty> commonProperties = new ArrayDeque<>()
		for( String pname : oldProperties.intersect(newProperties) ) {
			if( pname != 'configuration') {
				ComponentProperty prop = oldComponent.getComponentProperty(pname)
				if( prop.getParcel() == null || prop.getParcel().isEmpty() ) {
					commonProperties.addFirst( prop )
				} else {
					commonProperties.addLast( prop )
				}
			}
		}

		while( !commonProperties.isEmpty() ) {
			ComponentProperty prop = commonProperties.remove()

			if( prop.value != null ) {

				def beforeProps = newComponent.listProperties()

				if (!prop.hasValueChoices()){
					newComponent.setPropertyValue( prop.getName(), prop.value )
				} else {
					// See if the old value is still a valid option.
					if ( newComponent.getComponentProperty( prop.getName() ).choiceForValueAsString( prop.value.toString() ) != null ){
						newComponent.setPropertyValue( prop.getName(), prop.value )
					} else {
						//otherwise, leave at default
						log.warn("Value choices for '${prop.getName()}' are different. Old value not copied to $newComponent")
					}
				}

				// See if this prop triggered a parcel that has new properties we need to now consider.
				def deltaProps = newComponent.listProperties()
				deltaProps.removeAll( beforeProps )
				if( !deltaProps.isEmpty() ) {
					for( String pname : deltaProps ) {
						ComponentProperty addedProp = oldComponent.getComponentProperty(pname)
						if( addedProp != null ) {
							commonProperties.addLast( addedProp )
						}
					}
				}

			} else if( prop.getBase64Image() != null ) {
				// As of Mars, images are handled all special n stuff inside ComponentProperty. The "value" will be
				// null and we need to do a special load of the encoded image, not a standard setProp.
				// NOTE: This wasn't needed here prior to DLProject/DLImage cuz the loadImage was done BEFORE the upgrade.
				ARGBConverter.loadComponentImage( newComponent, prop.getBase64Image() )
				prop.finishImage()
			}
		}
	}

	/**
	 * Copy common producers
	 * @param oldComponent
	 * @param newComponent
	 */
	private void copyProducers(DLComponent oldComponent, DLComponent newComponent) {
		def oldProducers = oldComponent.listProducers()
		def newProducers = newComponent.listProducers()
		def commonProducers = oldProducers.intersect(newProducers)
		commonProducers.each {producerId ->
			//log.trace("common producer:" + producerId)
			def producer = oldComponent.getProducer(producerId)
			def datatype = newComponent.getProducer(producerId).getDatatype()
			if (producer != null) {
				def consumerComponents = producer.listConsumerComponents()
				consumerComponents.each {pair ->
					String consumerId = pair.b;
					DLComponent consumerComponent = pair.a
					def consumerDatatype = consumerComponent.getConsumer(consumerId).getDatatype()
					//println "$datatype =?= $consumerDatatype"
					if (datatype.equals(consumerDatatype)) {
						consumerComponent.associate(consumerId, newComponent, producerId)
					}
					//we're not going to do the else-case that we have down below, since that should really only apply
					//when replacing secondary components (COINs, etc.) and those should just be consumers.
				}
			}
		}
	}

	/**
	 * copy common consumers
	 * @param oldComponent
	 * @param newComponent
	 */
	private void copyConsumers(DLComponent oldComponent, DLComponent newComponent) {
		def oldConsumers = oldComponent.listConsumers()
		def newConsumers = newComponent.listConsumers()
		def commonConsumers = newConsumers.intersect(oldConsumers) //all consumers with the same name on both versions
		commonConsumers.each {consumerId ->
			//log.trace("common consumer:" + consumerId)
			Consumer consumer = oldComponent.getConsumer(consumerId)
			//make sure we have the same datatype as we used to
			//find the new datatype
			def datatype = newComponent.getConsumer(consumerId).getDatatype()
			if (consumer != null) {
				def producerComponents = consumer.listProducerComponents()
				//log.trace("producerObjects are: $producerObjects")
                producerComponents.each { pair ->
					String producerId = pair.b;
                    DLComponent producerComponent = pair.a
					//log.trace("producer comp is '$producerComponent'")
					def producerDatatype = producerComponent.getProducer(producerId).getDatatype()
					//log.trace("'$datatype' =?= '$producerDatatype'")
					if (datatype.equals(producerDatatype)) {
						newComponent.associate(consumerId, producerComponent, producerId)
					} else {
						//SO!  This is the tricky one.  We have a datatype shift, but we need to see if we should make the association anyway.
						def possibleHosts = producerComponent.producersOfType(datatype)
						//log.trace("possible hosts are $possibleHosts")
						if (possibleHosts.size() == 1) {
							def newProdId = possibleHosts.first()
							newComponent.associate(consumerId, producerComponent, newProdId)
						}
					}
				}
			}
		}
	}

	/**
	 * copy common conducers
	 * @param oldComponent
	 * @param newComponent
	 */
	private void copyConducers(DLComponent oldComponent, DLComponent newComponent) {
		def oldConducers = oldComponent.listConducers()
		def newConducers = newComponent.listConducers()
		def commonConducers = newConducers.intersect(oldConducers) //all consumers with the same name on both versions
		commonConducers.each { conducerId ->
			//log.trace("common conducer:" + conducerId)
			Conducer conducer = oldComponent.getConducer(conducerId)
			//make sure we have the same datatype as we used to
			//find the new datatype
			if (conducer != null) {
                Conducer newConducer = newComponent.getConducer(conducerId)
                for( Conducer oldBuddy : conducer.getProsumers() ) {
                    if( oldBuddy.canProsume( newConducer ) ) {
                        def other = oldBuddy.getOwner()
                        newComponent.associate( newConducer, other, oldBuddy )
                    }
                }
			}
		}
	}
	/**
	 * Automatically call replace configuration on everything in the project with itself.
	 * @param copyObjectData if true also copy object-level data, making this an Update Config, rather than a replace Config
	 * @param showProgress if true, the process will update the progressPane status bar; if false, that will be left up to the calling method
	 */
	public void replaceEverything(boolean copyObjectData = false, boolean showProgress = true) {
		//map [component-type: list of components of that type]
		//for each:
		//call replace config

        // Clear the old DLID map since we are burning the whole place down.
        ProjectSettings.getInstance().resetDlidMap()

        // Remember the current Active Drawing so we can reactivate it.
        Dlid oldActiveDrawingId = selectModel.getActiveDrawing()?.getDlid()

        // Notify the world that some major model work is about to start.
        componentModel.metamorphosisStarting( ModelState.REPLACING )

		def start = new DLStopwatch()

		Map<String, List<DLComponent>> comps = [:]

		// A list of roles to convert first before random conversion of everything else, in order of precedence.
		// Don't forget the old names if a role changes, like datacenter -> drawing in v7.0
		final List<String> PRIORITY_ROLES = ["datacenter","drawing","network","zone","room","controlleddevice"]

		for( String role : PRIORITY_ROLES ) {
			for( DLComponent c: componentModel.getComponentsInRole( role ) ) {
				MapHelper.addList( comps, c.getType(), c )
			}
		}

		comps.each {type, clist ->
			log.info("First step: Containers - Replacing Configuration on all $type")

			//check to see if we need to change a type during upgrade
			def newType = type
			if( CentralCatalogue.hasComponentConversion( type ) ) {
				newType = CentralCatalogue.getComponentConversion( type )
				log.info("Automatically converting $type to $newType")
			}

			if( componentModel.isLoadedComponentType( newType ) ) {
				configTree.setConfiguration( newType )
				this.replaceConfig(clist, copyObjectData, showProgress)
			} else {
				log.warn("Type $newType not a known type, configuration was not updated")
			}
		}

		comps = [:]
		for (DLComponent c: componentModel.getComponents()) {
			// Skip those already processed as well as proxies since they'll get updated as part of the anchor upgrade.
			if( c.rolesSet().disjoint( PRIORITY_ROLES ) ) {
                if( !c.hasRole("proxy") ) {
                    def type = c.getType()
                    MapHelper.addList(comps, type, c)
                }
			}
		}

		//check to see if we need to change a type during upgrade
		comps.each {type, clist ->
			log.info("Second step: Components - Replacing Configuration on all $type")
			def newType = type
			if( CentralCatalogue.hasComponentConversion( type ) ) {
				newType = CentralCatalogue.getComponentConversion( type )
				log.info("Automatically converting $type to $newType")
			}

			if (componentModel.isLoadedComponentType(newType)) {
				configTree.setConfiguration(newType)
				this.replaceConfig(clist, copyObjectData, showProgress)
			} else {
				log.warn("Type $newType not a known type, configuration was not updated")
			}
		}

		selectModel.clearSelection()

		// Flush extinct and unused components from the Component Library.
		ComponentLibrary.INSTANCE.pruneExtinctComponents()

		log.info("Config updated in ${start.finishMessage()}")

        // Let the world know that we are done remodeling.
        componentModel.metamorphosisFinished( ModelState.REPLACING )

        // If there was an Active Drawing before we started this, find the new drawing component and make it active.
        if( oldActiveDrawingId != null ) {
            Dlid newId = ProjectSettings.instance.findNewDlid( oldActiveDrawingId )
            DLComponent newActiveDrawing = componentModel.getComponentFromDLID( newId ? newId : oldActiveDrawingId )
            selectModel.setActiveDrawing( newActiveDrawing )
        }
	}

	/**
	 * To copy a DLObject, we need to move three things over:
	 * The ES Key (if present)
	 * Any oldValues
	 * And overrides on values
	 * @param oldAndBusted
	 * @param newHotness
	 */
	private void copyObject(DLObject oldAndBusted, DLObject newHotness) {

		if (!oldAndBusted.getType().equals(newHotness.getType())) {
			// Unless it is an approved object conversion. */
			def approvedHotness = CentralCatalogue.getObjectTypeConversion( oldAndBusted.getType() )
			if( approvedHotness != newHotness.getType() ) {
				log.trace("Objects not same type, not updating: $oldAndBusted $newHotness")
				return
			}
		}

		//log.trace("Copy Object Contents: $oldAndBusted to $newHotness")
		//log.trace("old Key:" + oldAndBusted.getKey())
		newHotness.setKey(oldAndBusted.getKey())
		oldAndBusted.setKey(null)

		oldAndBusted.getPropertiesByType("setting").each { s ->
			Setting oldProp = (Setting) s

			if (oldProp.getOldValue() != null && newHotness.hasObjectProperty(oldProp.getName())) {
				Setting hotProp = (Setting) newHotness.getObjectProperty(oldProp.getName())
				hotProp.setOldValue(oldProp.getOldValue())
			}

			if (oldProp.getOverride() != null && newHotness.hasObjectProperty(oldProp.getName())) {
				Setting hotProp = (Setting) newHotness.getObjectProperty(oldProp.getName())
				hotProp.setOverride(oldProp.getOverride())
			}

			//special case for sync timestamp
			if (oldProp.getName().equalsIgnoreCase("lastExportTs") && newHotness.hasObjectProperty(oldProp.getName())) {
				Setting hotProp = (Setting) newHotness.getObjectProperty(oldProp.getName())
				hotProp.setValue(oldProp.getValue())
			}

		}

		//same thing, but for oldvalues and overrides on tags
		oldAndBusted.getProperties().each { oldProp ->
			if (newHotness.hasObjectProperty(oldProp.getName())) {
				if (oldProp.hasTags()) {
					Property newProp = newHotness.getObjectProperty(oldProp.getName())
					oldProp.getTags().each { oldTag ->
						if (newProp.hasTag(oldTag.getTagName())) {
							def newTag = newProp.getTag(oldTag.getTagName())
							newTag.setOldValue(oldTag.getOldValue())
							newTag.setOverride(oldTag.getOverride())
						}
					}
				}
			}
		}

/*
		for ( int i = 0; i < oldAndBusted.getSortedChildren().size(); i++ ){
			copyObject( oldAndBusted.getSortedChildren()[i], newHotness.getSortedChildren()[i] )
		}
*/
        // Save the mapping between the old and new DLID.
        ProjectSettings.getInstance().replaceDlid( oldAndBusted.getDlid(), newHotness.getDlid() )
	}

	private DLObject findMatchingChild(DLObject from, List<DLObject> to, String fromComponentType, String toComponentType) {
		DLObject result = null

		def updateByChannel = CentralCatalogue.INSTANCE.getUpdateByChannelProperties()

		if( updateByChannel.stringPropertyNames().contains(fromComponentType) &&
		    updateByChannel.getProperty(fromComponentType).split(",").toList().contains(toComponentType) ) {
			log.info("Updating from $fromComponentType to $toComponentType will use channel numbers, not sensor names")

			if( from.hasObjectProperty("channel") ) {
				int fromChannel = from.getObjectProperty("channel").getValue()
				for (DLObject t: to) {
					if (t.hasObjectProperty("channel") && t.getObjectProperty("channel").getValue().equals(fromChannel)) {
						result = t
						break; //from for loop
					}
				}
			}
		} else {
			// Can we match by "name" property?
			if( from.hasObjectProperty("name") ) {
				String fromName = from.getObjectProperty("name").getValue()

				for( DLObject t: to ) {
					if( t.hasObjectProperty("name") && t.getObjectProperty("name").getValue().equals(fromName) && t.getType().equals(from.getType()) ) {
						result = t
						break; //from for loop
					}
				}
			}

			// If not, what about by 'as' attribute?
			if( !result && from.getAsName() ) {
				for( DLObject t: to ) {
					if( ( from.getAsName() == t.getAsName() ) && ( from.getType() == t.getType() ) ) {
						result = t
						break; //from for loop
					}
				}
			}
		}

		if( !result ) {
			// Welp, no luck... if there is only one child of the same type, let's assume we want to use that
			boolean alreadyFoundByType = false
			for( DLObject t: to ) {
				if( from.getType() == t.getType() ) {
					if( !alreadyFoundByType ) {
						alreadyFoundByType = true
						result = t
					} else {
						// Damn... we found two of them, no bueno
						result = null
						break; //from for loop
					}
				}
			}
		}

		return result;
	}


	private void copyChildren(DLObject oldRootObj, DLObject newRootObj, String fromComponentType, String toComponentType, DLComponent oldComponent) {
		//oldRootObj.getChildren().each { childObj ->
		for(def childObj : oldRootObj.getChildren() ){
			// Skip children that were promoted to roots since they'll get copied as a root.
			// *sigh* Thanks to HRow/RowLites, we have children that are owned by another component. Skip those.
			if( !oldComponent.hasRoot( childObj ) && oldComponent.hasManagedObject( childObj ) ) {
				def matchingChild = findMatchingChild(childObj, newRootObj.getChildren(), fromComponentType, toComponentType)
				if (matchingChild != null) {
					log.trace("copyObjectData: child:" + oldRootObj.getType())
					copyObject(childObj, matchingChild)
					//also recurse all the way down
					copyChildren(childObj,matchingChild,fromComponentType,toComponentType,oldComponent)
				} else {
					log.warn("No matching child found for $childObj in $fromComponentType")
				}
			} else {
				log.debug("copyChildren skipping $oldRootObj child $childObj since it " +
				          ( oldComponent.hasRoot( childObj ) ? "was promoted to a root" :
				            "is owned by another component ${componentModel.getManagingComponent(childObj)}" ))
			}
		}
	}

	//todo: fold into component creation
	private void addToParent(DLComponent parentComponent, DLComponent childComponent) {
		ArrayList<DLComponent> parentComponents = parentComponent.getParentComponents();
		//log.trace(parentComponent.getName() + " number of parent of parentComponents" + parentComponents.size());

		DLComponent activeDrawing;
		DLComponent activeZone;
		DLComponent activeNetwork;

		for (DLComponent c: parentComponents) {
			//log.trace("parent of parentcomponent:" + c.getType());
			if (c.hasClass("drawing")) {
				activeDrawing = c;
				activeDrawing.addChild(childComponent);
			} else if (c.hasClass("zone")) {
				activeZone = c;
				activeZone.addChild(childComponent);
			} else if (c.hasClass("network")) {
				activeNetwork = c;
				activeNetwork.addChild(childComponent);
			}else{
				log.trace("addToParent did nothing")
			}
		}
	}

	//todo: why was this here?
	private void addRackSensorToParentNode(DLComponent parentComponent, DLComponent childComponent) {

		Map<String, DLObject> nodeMap = parentComponent.getObjectMapOfType("wsnnode");

		for (String name: nodeMap.keySet()) {
			//log.trace("found wsnnode:" + name);
			DLObject node = nodeMap.get(name);
			ChildLink sensors = (ChildLink) node.getObjectProperty("sensepoints");

			String sensorName = "";
			if (name.replace("node", "").length() == 0) {
				sensorName = "sensor";
			} else {
				sensorName = name.replace("_node", "") + "_sensor";
			}

			DLObject sensor = childComponent.getObject(sensorName);

			try {
				sensors.addChild(sensor);
			} catch (IncompatibleTypeException e) {
				// TODO Auto-generated catch block
				log.trace("error here");
				log.trace(e.getMessage());
			} catch (DuplicateObjectException e) {
				// TODO Auto-generated catch block
				log.trace(e.getMessage());
			}
		}

	}

	//todo: why was this here?
	private void addDockingpoint(DLComponent parentComponent, DLComponent childComponent) {
		DLObject rackObject = childComponent.getObject("rack");

		if (rackObject != null) {
			Dockingpoint pFrom_nodeTemp = (Dockingpoint) rackObject.getObjectProperty("nodeTemp");
			Dockingpoint pFrom_rh = (Dockingpoint) rackObject.getObjectProperty("rh");

			DLObject intake_node_object = parentComponent.getObject("intake_node");
			ChildLink sensors = (ChildLink) intake_node_object.getObjectProperty("sensepoints");

			DLObject oTo_internalTemp = null;
			DLObject oTo_humidity = null;
			for (DLObject o: sensors.getChildren()) {
				//log.trace("wsnsensor object:" + o.getName());
				if (o.getName().equals("internal temp")) {
					oTo_internalTemp = o;
				} else if (o.getName().equals("humidity")) {
					oTo_humidity = o;
				}
			}

			if (pFrom_nodeTemp != null) {
				if (oTo_internalTemp != null) {
					try {
						pFrom_nodeTemp.setReferrent(oTo_internalTemp);
					} catch (IncompatibleTypeException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}

			if (pFrom_rh != null) {
				try {
					pFrom_rh.setReferrent(oTo_humidity);
				} catch (IncompatibleTypeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private DLComponent getComponentByProperty(ArrayList<DLComponent> cList, String pName, String pValue) {
		for (DLComponent c: cList) {
			String value = c.getPropertyStringValue(pName);
			if (value.equals(pValue))
				return c;
		}
		return null;
	}
}

class MessagePanel extends JPanel {
	private static final Logger log = Logger.getLogger(MessagePanel.class.getName())

	def replacableConfiguration = []
	def nonreplacableConfiguration = []
	def sameConfiguration = []
	String newConfiguration
	ComponentModel componentModel

	MessagePanel(Object _replacableConfig, Object _nonreplacableConfig, Object _sameConfig, boolean needProxyWarning, String _newConfiguration, ComponentModel _componentModel) {
		replacableConfiguration = _replacableConfig
		nonreplacableConfiguration = _nonreplacableConfig
		sameConfiguration = _sameConfig
		newConfiguration = _newConfiguration
		componentModel = _componentModel

		def panel = new JPanel()
		panel.setLayout(new MigLayout())
		this.add(panel)

		def failConfDisplaynames = nonreplacableConfiguration.collect {it.getDisplaySetting('name')}.flatten().unique()

		def successConfiguralations = replacableConfiguration.collect {it.getType()}.flatten().unique()
		def success = []
		successConfiguralations.each {config -> success.add(replacableConfiguration.find {it.getType() == config})}

        if( needProxyWarning ) {
            panel.add(new JLabel( CentralCatalogue.getUIS('replaceConfig.noProxies')), "span 3, wrap")
        }
/*
//lets not bother with this message, shall we?
		if(sameConfiguration.size()>0){
			String sameConfigMsg = ""
			sameConfigMsg+= "The configuration type of " + sameConfiguration.size() + " Object(s) are same. Will update current version of configuration."
			panel.add( new JLabel(sameConfigMsg), "span 3, wrap" )
		}
*/
		if (nonreplacableConfiguration.size() > 0) {
			String failText = failConfDisplaynames.join(',') + " cannot be replaced with " + componentModel.getDisplaySetting(newConfiguration, 'name') + " ."
			panel.add(new JLabel(failText), "span 3, wrap")
		}

		// log.trace("replacableConfiguration.size():" + replacableConfiguration.size())
		if (replacableConfiguration.size() > 0) {
			panel.add(new JLabel("Replace:"), "span 2")
			panel.add(new JLabel("With:"), "wrap")

			success.eachWithIndex {c, i ->

				if (i == 0) {
					panel.add(new JLabel(c.getDisplaySetting('name'), ComponentIconFactory.getComponentIcon(c, ComponentIconFactory.LARGE), JLabel.LEFT))

					panel.add(new JSeparator(SwingConstants.VERTICAL), "spany ${success.size()}".toString()) //?why doesn't this work?

					//def icn = ComponentIconFactory.getImageIcon("/resources/components/${ componentModel.getDisplaySetting(newConfiguration, 'shape')}_16.png" )
					def icn = ComponentIconFactory.getShapeIcon(componentModel.getDisplaySetting(newConfiguration, 'shape'), ComponentIconFactory.LARGE)
					panel.add(new JLabel(componentModel.getDisplaySetting(newConfiguration, 'name'), icn, JLabel.LEFT), "wrap")
				} else {
					panel.add(new JLabel(c.getDisplaySetting('name'), ComponentIconFactory.getComponentIcon(c), JLabel.LEFT), "wrap")
				}

			}

		}

	}

}