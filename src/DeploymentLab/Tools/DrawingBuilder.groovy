package DeploymentLab.Tools

import DeploymentLab.Model.ObjectType
import DeploymentLab.Dialogs.Association.AssociationController
import DeploymentLab.Dialogs.ChooseDrawing.ChooseDrawingController
import DeploymentLab.Image.DLImage
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentType
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLProxy
import DeploymentLab.channellogger.Logger
import com.google.common.collect.HashMultimap
import com.panduit.sz.api.ss.assets.Floorplan
import com.panduit.sz.api.ss.assets.Rack
import groovy.swing.SwingBuilder

import javax.imageio.ImageIO
import java.awt.GridLayout
import javax.swing.JCheckBox
import javax.swing.JOptionPane
import javax.swing.JTextField

import DeploymentLab.*
import DeploymentLab.Model.ModelState
import com.google.common.collect.ArrayListMultimap

import java.awt.image.BufferedImage

/**
 *
 * @author Gabriel Helman
 * @since
 * Date: 10/5/12
 * Time: 12:15 PM
 */
class DrawingBuilder {
	private static final Logger log = Logger.getLogger(DrawingBuilder.class.getName())

	private def builder

	private ComponentModel componentModel
	private UserPreferences userPreferences
	private ProxyAdder proxyAdder
	private SelectModel selectModel

	public DrawingBuilder(ComponentModel cm, UserPreferences up, ProxyAdder pa, SelectModel sm) {
		componentModel = cm
		//undoBuffer = ub
		userPreferences = up
		proxyAdder = pa
		selectModel = sm

		builder = new SwingBuilder()
	}

	/**
	 * Adds a new empty drawing to a project, giving the user the option to specify a name and a background image.
	 * Returns true if add drawing is successful else false.
	 * Migrated from DLW.
	 */
	public boolean addDrawing(Floorplan fp) {
		if (null != fp) {
			addDrawingWithFloorPlan(fp)
		} else {
			while (true) {
				JTextField drawingNameField
				JCheckBox bgImageCheckBox
				def addDrawingPanel = builder.panel(layout: new GridLayout(3, 1)) {
					label(text: CentralCatalogue.getUIS("drawing.new.prompt"))
					drawingNameField = textField(text: "")
					bgImageCheckBox = checkBox(label: CentralCatalogue.getUIS("drawing.new.usebgimage"), selected: false)
				}
				drawingNameField.addAncestorListener(new RequestFocusListener());
				int result = JOptionPane.showOptionDialog(CentralCatalogue.getParentFrame(), addDrawingPanel, CentralCatalogue.getUIS("drawing.new.dialogtitle"), JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null)
				log.trace("result:$result")
				String drawingName = drawingNameField.getText()
				if (result != JOptionPane.OK_OPTION) {
					log.warn(CentralCatalogue.getUIS("drawing.new.cancel"), "statusbar")
					return false
				} else {
					boolean isBackground = bgImageCheckBox.isSelected()
					log.trace("drawingName:$drawingName")
					log.trace("image? : $isBackground")
					drawingName = drawingName.trim()
					if (drawingName.size() > 0) {
						DLImage bgImage = null
						// if check background image option
						if (isBackground) {
							// Now get the background image file.
							File imageFile = FileUtil.askForFile(userPreferences, CentralCatalogue.getUIS("setBGImage.filechooser.title"), FileFilters.BackgroundImage.getFilter())
							if (imageFile == null) {
								log.warn(CentralCatalogue.getUIS("drawing.new.cancel"), "statusbar")
								return false
							}
							bgImage = new DLImage(imageFile)
						}

						createNewDrawing(drawingName, bgImage)

						break
					} else {
						log.warn(CentralCatalogue.getUIS("drawing.new.entryerror"), "message")
					}
				}
			}
		}
		return true
	}

	/** Creates a new drawing with or without a background image.*/
	private void createNewDrawing(String drawingName, DLImage bgImage) {
		DLComponent drawing = componentModel.newComponent(ComponentType.DRAWING)
		drawing.setPropertyValue('name', drawingName)
		if (bgImage != null) {
			drawing.setPropertyValue("hasImage", true)
			drawing.setPropertyValue('image_data', bgImage)
			drawing.setPropertyValue('width', bgImage.getWidth())
			drawing.setPropertyValue('height', bgImage.getHeight())
		} else {
			drawing.setPropertyValue("hasImage", false)
		}
	}

	/**
	 * This creates a new drawing with a floor plan. If the Floorplan has a background image, then calculates the scale
	 * and the multiplier else creates a mapsense blank canvas with default scale.
	 * @param fp - The Floorplan of the of the selected SZ location to import.
     */
	private void addDrawingWithFloorPlan(final Floorplan fp) {
		DLImage bgImage = null
		final int widthInPixels
		final int heightInPixels

		double widthInInches
		double heightInInches

		double scale = 1.0
		double multiplier = 1.0
		boolean isXMultiplier = false
		boolean isYMultiplier = false

		if (fp.getBackground().isPresent()) {
			final byte[] byteA = fp.getBackground().get().getBytes()
			BufferedImage bi = ImageIO.read(new ByteArrayInputStream(byteA))
			bgImage = new DLImage(bi)
			widthInPixels = bgImage.getWidth()
			heightInPixels = bgImage.getHeight()

			widthInInches = fp.getWidth()
			heightInInches = fp.getHeight()

			final double widthScale = widthInInches / widthInPixels
			final double heightScale = heightInInches / heightInPixels

			if (widthScale > heightScale) {
				isYMultiplier = true
				multiplier = widthScale / heightScale
				heightInInches = heightInInches * multiplier
			} else if (widthScale < heightScale) {
				isXMultiplier = true
				multiplier = heightScale / widthScale
				widthInInches = widthInInches * multiplier
			}
			scale = widthInInches / widthInPixels
		}
		createNewDrawing(fp.getName(), bgImage)
		selectModel.getActiveDrawing().setPropertyValue('scale', scale)
		selectModel.getActiveDrawing().setSmartZoneId(fp.getId())

		for (Rack rack : fp.getRacks()) {
			def newRackComp = createSZNewComponent(rack)

			if (isXMultiplier) {
				newRackComp.setPropertyValue('x', rack.getLocation().getX() * multiplier)
				newRackComp.setPropertyValue('y', rack.getLocation().getY())
			} else if (isYMultiplier) {
				newRackComp.setPropertyValue('x', rack.getLocation().getX())
				newRackComp.setPropertyValue('y', rack.getLocation().getY() * multiplier)
			}
		}
	}

	/**
	 * Creates a new mapsense component of type "rack-uninstrumented" for all the components/objects in the imported
	 * SZ floorplan. And sets the properties of the newly created component. Sets the editable property of the width and
	 * depth as false
	 *
	 * @param fp - The Floorplan object that includes all the SZ components/objects.
     */
	private DLComponent createSZNewComponent(Rack rack) {
		def newRackComp
		newRackComp = componentModel.newComponent(ComponentType.RACK_UNINSTRUMENTED)
		newRackComp.setDrawing(selectModel.getActiveDrawing())
		newRackComp.setSmartZoneId(rack.getId())
		newRackComp.setPropertyValue('name', rack.getName())
		newRackComp.setPropertyValue('x', rack.getLocation().getX())
		newRackComp.setPropertyValue('y', rack.getLocation().getY())

		newRackComp.setPropertyValue('rotation', rack.getRotation())
		newRackComp.setPropertyValue('width', rack.getWidth())
		newRackComp.setPropertyValue('depth', rack.getDepth())

		return newRackComp
	}

	public void moveSelectionToAnotherDrawing(Collection<DLComponent> incomingSelection, NodeMover nm) {

		//HACXXXXXX:
		//don't move lontalk anything
		def removals = []
		incomingSelection.each{
			if(it.getType().contains("lon")){
				removals.add(it)
			}
		}
		incomingSelection.removeAll(removals)

		if (incomingSelection.size() == 0) {
			log.error(CentralCatalogue.getUIS("moveDrawing.noSelection"), "message")
			return
		}

		def selection = new HashSet<DLComponent>()
		selection.addAll(incomingSelection)

		if( !confirmCrazyMoveTimes( selection ) ) {
			return
		}

		DLComponent sourceDrawing = selection.iterator().next().getDrawing()

		def drawings = ChooseDrawingController.chooseOneDrawing(selectModel, componentModel)

		if( drawings.size() > 0 ) {
			DLComponent targetDrawing = drawings.first()

			crazyMoveTimes(selection, sourceDrawing, targetDrawing)

			//NodeMover nm = new NodeMover(undoBuffer, deploymentPanel, selectModel)
			nm.moveNodesToCenter()
		}
	}

	/**
	 * Makes a new drawing containing the selected components
	 * @param source
	 */
	public void makeDrawingFromSelection(Collection<DLComponent> incomingSelection) {

		//HACXXXXXX:
		//don't move lontalk anything
		def removals = []
		incomingSelection.each{
			if(it.getType().contains("lon")){
				removals.add(it)
			}
		}
		incomingSelection.removeAll(removals)

		if (incomingSelection.size() == 0) {
			log.error(CentralCatalogue.getUIS("moveDrawing.noSelection"), "message")
			return
		}

		def selection = new HashSet<DLComponent>()
		selection.addAll(incomingSelection)

		if( !confirmCrazyMoveTimes( selection ) ) {
			return
		}

		//first!  make a new drawing
		//this.addDrawing()
		//but!  it should be the exact same size as the old one

		//note: we want the step to change away from the old drawing to be OUTSIDE the INIT event
		//componentModel.metamorphosisStarting(ModelState.INITIALIZING)

		DLComponent sourceDrawing = selection.iterator().next().getDrawing()
		DLComponent targetDrawing = componentModel.newComponent(sourceDrawing)

		log.debug("ADDING DRAWING NOW: source='$sourceDrawing' target='$targetDrawing'")

		log.debug("SET DRAWING TO NULL")
		//we don't want an active drawing while this is happening, and we want to swap to the target when we're done
		//Bill of Service:
		//$10 One line of code.
		//$9,990 Knowing that this line is needed to avoid much pain.
		selectModel.setActiveDrawing(null)

		targetDrawing.setPropertyValue("name", targetDrawing.getPropertyStringValue("name") + " clone")

		crazyMoveTimes(selection, sourceDrawing, targetDrawing)

		//okay, NOW set the background image, because HAXXX
		componentModel.metamorphosisStarting(ModelState.INITIALIZING)
		targetDrawing.copySettingsFrom(sourceDrawing,false)
		componentModel.metamorphosisFinished(ModelState.INITIALIZING)
		selectModel.setActiveDrawing(targetDrawing)
	}


	private boolean confirmCrazyMoveTimes( Set<DLComponent> selection ) {
		boolean justdoit = true;

		// Make sure associations won't be lost.
		StringBuilder badFriends = new StringBuilder()
		for( DLComponent c : selection ) {
			// See if the move will leave any associates behind.
			if( !selection.containsAll( c.getAllAssociatedComponents() ) ) {
				// Make sure we can leave behind a proxy to maintain the association.
				if( !DLProxy.canProxy( c ) ) {
					badFriends.append('    ')
					badFriends.append( c.getName() )
					badFriends.append('\n')
				}
			}
		}

		if( badFriends.size() > 0 ) {
			int result = JOptionPane.showConfirmDialog( CentralCatalogue.getParentFrame(),
			                                            CentralCatalogue.formatUIS("moveDrawing.dialog.text.lostAssociations", badFriends ),
			                                            CentralCatalogue.getUIS("moveDrawing.dialog.title"),
			                                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE )
			justdoit = (result == JOptionPane.YES_OPTION)
		}

		return justdoit
	}

	private void crazyMoveTimes(Set<DLComponent> selection, DLComponent sourceDrawing, DLComponent targetDrawing) {

		if(componentModel.getCurrentState() != ModelState.INITIALIZING){
			componentModel.metamorphosisStarting(ModelState.INITIALIZING)
		}

		//screen for child components without their parents
		//roles: staticchild, dynamicchild
		//what we want is that if we get any part of static of dynamic family we want to get all of the rest
		//also grab any secondary nodes
		def newParents = []
		for(def c: selection){
			if(c.hasRole("staticchild")){
				newParents.add(c.getStaticParent())
				log.debug("added static parent for $c to selection as ${c.getStaticParent()}")
			}
			if(c.hasRole("dynamicchild")){
				newParents.add(c.getDynamicParent())
				log.debug("added dyn parent for $c to selection as ${c.getDynamicParent()}")
			}

			for(def a : c.getAllAssociatedComponents()){
				if (a.isSecondary()){
					newParents.add(a)
				}
			}
		}
		selection.addAll(newParents)

		List<DLComponent> sParents = []
		List<DLComponent> dParents = []
		for(def c : selection){
			if(c.hasRole("staticchildplaceable")){
				sParents.add(c)
			} else if(c.hasRole("dynamicchildplaceable")){
				dParents.add(c)
			}
		}

		for(def c : sParents){
			log.debug("adding all static children for $c as ${c.getStaticChildren()}")
			selection.addAll(c.getStaticChildren())
		}
		for(def c : dParents){
			log.debug("adding all dyn children for $c as ${c.getDynamicChildren()}")
			selection.addAll(c.getDynamicChildren())
		}


		//now! examine the selection
		//find all the zones, sets, or networks we care about
		def buckets = new HashSet<DLComponent>()
		def drawingChildren = []
		for (def c : selection) {
			c.getParentComponents().each { p ->
				if (p.isContainerOrNetwork() && !p.hasRole("drawing")) {
					buckets.add(p)
				}
				if (p.hasRole("drawing")) {
					drawingChildren.add(c)
				}
			}
		}

		//okay, so buckets now holds all of the zones, sets, and networks that our selection spans
		//how many buckets can we move whole cloth?
		def wholeBuckets = new HashSet<DLComponent>()
		buckets.each { b ->
			if (selection.containsAll(b.getChildComponents())) {
				wholeBuckets.add(b)
			}
		}
		def partialBuckets = buckets.minus(wholeBuckets)
		Map<DLComponent, DLComponent> pbClones = [:]

		log.trace("Make Drawing Report:")
		log.trace("wholeBuckets = $wholeBuckets")
		log.trace("partialBuckets = $partialBuckets")
		log.trace("drawing children to move: $drawingChildren")

		//now we can do PROBLEMS
		//clone all partial buckets.
		//move the children over

		for (def pb : partialBuckets) {
			// See if we already have one with the same name at the target.
			def pbClone = null
			for( def other : componentModel.getComponentsByType( pb.getType(), targetDrawing ) ) {
				if( other.getName() == pb.getName() ) {
					pbClone = other
					break
				}
			}

			// If not, create one.
			if( pbClone == null ) {
				pbClone = componentModel.newComponent(pb)
				targetDrawing.addChild(pbClone)
			}

			pbClones[pb] = pbClone
		}

		//the whole buckets are easy.  Move them over.
		wholeBuckets.each { wb ->
			sourceDrawing.removeChild(wb)

			// ... unless it already exists from a previous partial move
			for( def other : componentModel.getComponentsByType( wb.getType(), targetDrawing ) ) {
				if( other.getName() == wb.getName() ) {
					// Found, so treat it like a partial.
					pbClones[wb] = other
					break
				}
			}

			// If not found, do the easy full move.
			if( pbClones[wb] == null ) {
				targetDrawing.addChild(wb)
			}
		}

		//reset the childrens
		//why are we doing this?  We moved the parents over, and so the object graph is now correct, right?
		//YES, but the DeploymentPanel can't rebuild after the fact - it needs a set of remove/add child events to get things right
		//AND, we need a way that's undo-stack-trackable
		//so, I say unto you: HAXXXXX

		//do all removes
		drawingChildren.each { child ->
			sourceDrawing.removeChild(child)
		}

		HashMultimap<DLComponent, DLComponent> childrenToAdd = HashMultimap.create()
		partialBuckets.each { pb ->
			pb.getChildComponents().each { cc ->
				if (selection.contains(cc)) {
					pb.removeChild(cc)
					childrenToAdd.put(pbClones[pb], cc)
				}
			}
		}

		wholeBuckets.each { wb ->
			//reset children the lame way
			wb.getChildComponents().each { cc ->
				wb.removeChild(cc)
				// See if we are treating this like a partial or a full move.
				if( pbClones[wb] != null ) {
					childrenToAdd.put( pbClones[wb], cc )
				} else {
					childrenToAdd.put( wb, cc )
				}
			}
		}

		//static and dynamic childrens!
		ArrayListMultimap<DLComponent,DLComponent> staticAndDynamic = ArrayListMultimap.create()
		selection.each{ s ->
			if(s.hasRole("staticchildplaceable")  ){
				staticAndDynamic.putAll(s, s.getStaticChildren())
			}
			if(s.hasRole("dynamicchildplaceable")  ){
				staticAndDynamic.putAll(s, s.getDynamicChildren())
			}
		}

		staticAndDynamic.keySet().each{ p ->
			staticAndDynamic.get(p).each{ child ->
				p.removeChild(child)
			}
		}

		//then do all adds


		drawingChildren.each { child ->
			targetDrawing.addChild(child)
		}

		childrenToAdd.keySet().each { parent ->
			def children = childrenToAdd.get(parent)
			children.each { child ->
				parent.addChild(child)
			}
		}

		staticAndDynamic.keySet().each{ p ->
			staticAndDynamic.get(p).each{ child ->
				p.addChild(child)
			}
		}

		//lets fix associations!
		def createdProxies = []
		for (def c : selection) {
			DLComponent proxy = null
			//first, all associations on c's producers
			///c is the producerComponent
			for(def pc : c.getProducerConsumers() ){
				//log.trace("checking associations from $c to $pc")

				def sc = new AssociationController(c, pc)
				for (def bundle : sc.calculateAssociations(c, pc, false)) {
					//log.trace("I see this bundle $bundle")

					if (!selection.contains( bundle.consumerComponent )) {
						// Disconnect the originals that are on different drawings now.
						bundle.consumerComponent.unassociate(bundle.consumerName, bundle.producerComponent, bundle.producerName)

						// If possible, reconnect with proxy of the moved component.
						if( (proxy == null) && DLProxy.canProxy( c ) ) {
							proxy = proxyAdder.createProxy( c, sourceDrawing )
							createdProxies.add( proxy )
						}

						if( proxy ) {
							bundle.consumerComponent.associate( bundle.consumerName, proxy, bundle.producerName )
						}
					}
				}
			}

			//now, all associations on c's consumers
			///c is the consumerComponent
			c.getConsumerProducers().each { cp ->
				//log.trace("checking associations from $cp to $c")
				def sc = new AssociationController(cp, c)
				for (def bundle : sc.calculateAssociations(cp, c, false)) {
					//log.trace("I see this bundle $bundle")

					if (!selection.contains( bundle.producerComponent )) {
						// Disconnect the originals that are on different drawings now.
						bundle.consumerComponent.unassociate(bundle.consumerName, bundle.producerComponent, bundle.producerName)

						// If possible, reconnect with proxy of the moved component.
						if( (proxy == null) && DLProxy.canProxy( c ) ) {
							proxy = proxyAdder.createProxy( c, sourceDrawing )
							createdProxies.add( proxy )
						}

						if( proxy ) {
							proxy.associate( bundle.consumerName, bundle.producerComponent, bundle.producerName )
						}
					}
				}
			}


			c.getConducerProsumers().each{ pc ->
				//log.trace("checking conducers from $c to $pc")
				def sc = new AssociationController(c, pc)
				for (def bundle : sc.calculateConducers(false)) {
					//log.trace("I see this bundle $bundle")

					if (!selection.contains( bundle.omegaComponent )) {
						// Disconnect the originals that are on different drawings now.
						bundle.omegaComponent.unassociate(bundle.omega, bundle.alphaComponent, bundle.alpha)

						// If possible, reconnect with proxy of the moved component.
						if( (proxy == null) && DLProxy.canProxy( c ) ) {
							proxy = proxyAdder.createProxy( c, sourceDrawing )
							createdProxies.add( proxy )
						}

						if( proxy ) {
							bundle.omegaComponent.associate( bundle.omega, proxy, proxy.getConducer( bundle.alpha.id ) )
						}
					}
				}
			}


		}
		componentModel.metamorphosisFinished(ModelState.INITIALIZING)
		selection.addAll(createdProxies)
		log.debug("SWAP BACK TO NEW")
		//now we need the world to reset?
		selectModel.setActiveDrawing(targetDrawing)
		selectModel.setSelection(selection)
	}


}
