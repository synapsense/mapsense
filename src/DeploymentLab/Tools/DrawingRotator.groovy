package DeploymentLab.Tools

import DeploymentLab.Image.DLImage
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentRole
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.ModelState
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.SelectModel
import DeploymentLab.UndoBuffer
import java.awt.Graphics2D
import java.awt.geom.AffineTransform
import java.awt.geom.Point2D
import java.awt.image.BufferedImage

/**
 * Created by LNI on 4/11/2016.
 */
class DrawingRotator {

    private ComponentModel componentModel
    private SelectModel selectModel
    private UndoBuffer undoBuffer
    private DeploymentPanel deploymentPanel

    private DLComponent drawing

    private AffineTransform rotator = new AffineTransform()

    DrawingRotator(ComponentModel cModel, SelectModel sm, UndoBuffer ub, DeploymentPanel dp) {
        componentModel = cModel
        selectModel = sm
        undoBuffer = ub
        deploymentPanel = dp

        drawing = selectModel.getActiveDrawing()
    }

    public void editRotate90(double theta) {
        // theta must be +/- 90 degrees (+/- pi/2)
        undoBuffer.stopTracking()
        try {

            DLImage bg = (DLImage)drawing.getPropertyValue('image_data')

            Object w = deploymentPanel.getBlankCanvasWidth()
            Object h = deploymentPanel.getBlankCanvasHeight()
            drawing.setPropertyValue('width', h)
            drawing.setPropertyValue('height', w)
            deploymentPanel.fitView()

            if (bg) {
                w = bg.getWidth()
                h = bg.getHeight()
            }

            rotator.translate(h / 2.0, w / 2.0)
            rotator.rotate(theta)
            rotator.translate(-w / 2.0, -h / 2.0)

            rotateBgIfExists(bg)

            double scale = (Double)drawing.getPropertyValue('scale')
            rotator.setToIdentity()
            rotator.translate((h / 2.0) * scale, (w / 2.0) * scale)
            rotator.rotate(theta)
            rotator.translate((-w / 2.0) * scale, (-h / 2.0) * scale)

            setComponentPosition()
            setComponentRotation(theta)
            rotateRectangleShapedComp()
            rotateArbitraryShapedComp(theta)

        } finally {
            undoBuffer.resumeTracking()
        }
    }

    private void rotateBgIfExists(DLImage bg) {
        if (bg) {
            DLImage rotated = new DLImage(bg.getHeight(), bg.getWidth(), bg.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : bg.getType())
            // swap width/height for 90 degree rotation
            Graphics2D g = rotated.getGraphics()
            g.drawImage(bg.getImage(), rotator, null)
            drawing.setPropertyValue('image_data', rotated)
        }
    }

    private void setComponentPosition() {

        componentModel.getComponentsInRole('placeable').each { component ->
            Point2D.Double pt = new Point2D.Double()
            pt.setLocation((Double)component.getPropertyValue('x'),(Double)component.getPropertyValue('y'))
            rotator.transform(pt, pt)

            // So that the property change listener is triggered only after setting the both x and y properties.
            componentModel.metamorphosisStarting( ModelState.REPLACING );
            component.setPropertyValue('x', pt.getX())
            component.setPropertyValue('y', pt.getY())
            componentModel.metamorphosisFinished( ModelState.REPLACING );
        }
    }

    private void setComponentRotation(double theta) {
        componentModel.getComponentsInRole('rotatable').each { component ->
            BigDecimal rotation = component.getPropertyValue('rotation') - Math.toDegrees(theta)
            if (rotation > 360.0) {
                rotation -= 360.0
            }
            if (rotation < 0) {
                rotation += 360
            }
            component.setPropertyValue('rotation', rotation)
        }
    }

    private void rotateArbitraryShapedComp(double theta) {
        componentModel.getComponentsInRole(ComponentRole.ARBITRARY_SHAPE).each { component ->
            Object centerX = component.getPropertyValue("x")
            Object centerY = component.getPropertyValue("y")

            String pointsStr = (String) component.getPropertyValue("points");
            StringBuilder newPointsStr = new StringBuilder( pointsStr.size() )

            for(String xyStr:pointsStr.split(";")) {
                String[] strParts = xyStr.split(",")

                float x = Float.valueOf(strParts[0]).floatValue()
                float y = Float.valueOf(strParts[1]).floatValue();
                Point2D.Float point = new Point2D.Float(x, y);

                double[] pt = new double[strParts.size()]
                pt[0] = point.getX()
                pt[1] = point.getY()
                 AffineTransform.getRotateInstance(theta, centerX, centerY)
                        .transform(pt, 0, pt, 0, 1);

                // Make sure to keep the points as whole numbers since other parts of the system require it.
                pt[0] = Math.rint( pt[0] )
                pt[1] = Math.rint( pt[1] )

                if( newPointsStr.length() > 0 ) {
                    newPointsStr.append( ";" )
                }
                newPointsStr.append( Double.toString(pt[0]) )
                newPointsStr.append( "," )
                newPointsStr.append( Double.toString(pt[1]) )
             }

            component.setPropertyValue("points", newPointsStr.toString())
        }
    }

    private void rotateRectangleShapedComp() {
        componentModel.getComponentsInRole(ComponentRole.RECTANGLE_SHAPE).each { component ->
            Object width = component.getPropertyValue('width')
            Object depth = component.getPropertyValue('depth')
            component.setPropertyValue('width', depth)
            component.setPropertyValue('depth', width)
        }
    }
}
