package DeploymentLab.Tools

import DeploymentLab.channellogger.*
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.UndoBuffer
import DeploymentLab.Model.*
import DeploymentLab.SelectModel
import java.awt.geom.Point2D
import java.awt.geom.Rectangle2D
import DeploymentLab.Exceptions.UnknownComponentException
import DeploymentLab.CentralCatalogue
import DeploymentLab.Dialogs.ChangeParents.ChangeParentsController
import DeploymentLab.Dialogs.ChangeParents.ParentCategory

//TODO: prune dead code here

class NodeOperator{
	private static final Logger log = Logger.getLogger(NodeOperator.class.getName())
	private SelectModel selectModel
	private UndoBuffer undoBuffer
	private ComponentModel componentModel
	private DeploymentPanel deploymentPanel
	List<DLComponent> components = []
	
	NodeOperator(SelectModel sm, UndoBuffer ub, ComponentModel cm, DeploymentPanel dp){
		selectModel = sm
		undoBuffer = ub
		componentModel = cm
		deploymentPanel = dp
	}
	
	public void doCopy(List<DLComponent> incomingSelection){
		if(canCopy()){
			components = incomingSelection
		}

		def groupCPC = new ChangeParentsController(components,ParentCategory.GROUPING, undoBuffer,selectModel)
		def netCPC = new ChangeParentsController(components,ParentCategory.NETWORK, undoBuffer,selectModel)

		log.trace("potential parent groups are: ${groupCPC.getData()}")
		log.trace("potential parent nets are: ${netCPC.getData()}")
	}

	public void doMove(Point2D position) {
		if (components != null && components.size() > 0 && canCopy() && canPaste()) {
			def centerMap = [:]

			DLComponent activeDrawing = selectModel.getActiveDrawing()
			DLComponent activeNetwork = selectModel.getActiveNetwork()
			DLComponent activeZone = selectModel.getActiveZone()

			components.each {centerMap[it] = new Point2D.Double(it.getPropertyValue('x'), it.getPropertyValue('y'))}

			Point2D centerPt
			if (position == null) {
				centerPt = deploymentPanel.getViewCenter()
			} else {
				centerPt = position
			}

			Point2D viewCenterPt = CentralCatalogue.getDeploymentPanel().scalePoint( centerPt )
			Point2D selectionCenterPt = getSelectionRectangleCenter(components)

			try {
				undoBuffer.startOperation()
				for (def component : components) {
					double positionX = centerMap[component].x - selectionCenterPt.x + viewCenterPt.x
					double positionY = centerMap[component].y - selectionCenterPt.y + viewCenterPt.y

					if (component != null) {
						for (def p : component.getParentComponents()) {
							log.trace("removing $component from $p")
							p.removeChild(component)
						}
						component.clearDrawing()

						component.setPropertyValue('x', positionX)
						component.setPropertyValue('y', positionY)

						if (activeDrawing.canChild(component)) {
							log.trace("add $component to $activeDrawing")
							activeDrawing.addChild(component)
						}
						// Handled automatically now.
						if (activeNetwork != null && activeNetwork.canChild(component)) {
							log.trace("add $component to $activeNetwork")
							activeNetwork.addChild(component)
						}
						if (activeZone != null && activeZone.canChild(component)) {
							log.trace("add $component to $activeZone")
							activeZone.addChild(component)
						}

					}
				}

				selectModel.setSelection(components)

			} catch (Exception ex) {
				log.error("Moving component failed: " + ex, 'message')
				log.error(log.getStackTrace(ex))
				undoBuffer.rollbackOperation()
			} finally {
				undoBuffer.finishOperation()
			}

		}else{
			log.warn("failure!", "message")
		}

	}


	public void doPaste(Point2D position){
		if(components!=null && components.size()>0 && isCopied && canPaste()){
			def centerMap = [:]
			
			DLComponent activeDrawing = selectModel.getActiveDrawing()
			DLComponent activeNetwork = selectModel.getActiveNetwork()
			DLComponent activeZone = selectModel.getActiveZone()
				
			components.each{centerMap[it] = new Point2D.Double(it.getPropertyValue('x'), it.getPropertyValue('y'))}
			
			Point2D centerPt
			if(position==null){
				centerPt = deploymentPanel.getViewCenter()
			}else
				centerPt = position
			
			Point2D viewCenterPt = CentralCatalogue.getDeploymentPanel().scalePoint( centerPt )
			Point2D selectionCenterPt = getSelectionRectangleCenter(components)
			def newComponents = []
			
			undoBuffer.startOperation()
			components.each{ sourceComponent ->
				try {
					double positionX = centerMap[sourceComponent].x - selectionCenterPt.x + viewCenterPt.x 
					double positionY = centerMap[sourceComponent].y - selectionCenterPt.y + viewCenterPt.y  

					if(sourceComponent != null) {
						DLComponent component = componentModel.newComponent(sourceComponent)
						component.setPropertyValue('x', positionX)
						component.setPropertyValue('y', positionY)

						if(activeDrawing.canChild(component))
							activeDrawing.addChild(component)
						if(activeNetwork != null && activeNetwork.canChild(component))
							activeNetwork.addChild(component)
						if(activeZone != null && activeZone.canChild(component))
							activeZone.addChild(component)
						
						newComponents+=component
					}
				} catch(Exception ex) {
					log.error("Pasting component failed: " + ex, 'message')
					log.error(log.getStackTrace(ex))
				}
			}
			selectModel.setSelection(newComponents)
			undoBuffer.finishOperation()		
		}
	}
	
	private Point2D getSelectionRectangleCenter(def components) {
		double rectangleXLeft = components.first().getPropertyValue('x')
		double rectangleYLow = components.first().getPropertyValue('y')
		double rectangleXRight = rectangleXLeft
		double rectangleYHigh = rectangleYLow

		components.tail().each{ it ->
			double x = it.getPropertyValue('x')
			double y = it.getPropertyValue('y')
			if (x < rectangleXLeft)
				rectangleXLeft = x
			if (y < rectangleYLow)
				rectangleYLow = y
			if (x > rectangleXRight)
				rectangleXRight = x
			if (y > rectangleYHigh)
				rectangleYHigh = y
		}

		Rectangle2D rect = new Rectangle2D.Double(rectangleXLeft, rectangleYLow, rectangleXRight - rectangleXLeft, rectangleYHigh - rectangleYLow)
		return new Point2D.Double(rect.centerX, rect.centerY)
	}

	public boolean canCopy(){
		for(DLComponent component: components) {
			String type = component.getType()
			//DLComponent activeDrawing = selectModel.getActiveDrawing()
			//DLComponent activeDatasource = selectModel.getActiveNetwork()
			//DLComponent activeGroup = selectModel.getActiveZone()
			
			if(componentModel.isLoadedComponentType(type)){
				def parentTypes = componentModel.listParentComponentsOfType(type)
				def parentRoles = parentTypes.collect{ componentModel.listComponentClasses(it).flatten() }.flatten().unique()

				if(parentRoles.size() == 0) {
					log.error("Object has no parents, cannot be added to the deployment!", 'message')
					return false
				}

			}else{
				String componentDisplayName = component.getDisplaySetting('name')
				log.info("Cannot Copy. The component type $componentDisplayName is not loaded.\n Please Load the component library!", 'message')
				return false
			}

		}
		return true
	}
	
	protected boolean canPaste(){
		return this.canPaste( components.collect{it.getType()} )
	}

	/**
	 * Checks to see if a given component type is pasteable given the current environment (active grouping & DS, etc.)
	 * @param type the type-name of a component to see if we know how to paste
	 * @return if the type can be pasted
	 * @throws UnknownComponentException if the type is something we've never heard of before
	 */
	public boolean canPaste( String type ) throws UnknownComponentException{
		DLComponent activeDrawing = selectModel.getActiveDrawing()
		DLComponent activeDatasource = selectModel.getActiveNetwork()
		DLComponent activeGroup = selectModel.getActiveZone()
		if(componentModel.isLoadedComponentType(type)){
			//def parentTypes = componentModel.listParentComponentsOfType(type)
			//def parentRoles = parentTypes.collect{ componentModel.listComponentClasses(it).flatten() }.flatten().unique()

			def parentTypes = new HashSet<>()
			parentTypes.addAll(componentModel.listParentComponentsOfType(type))

			// Remove parent types that are secondary/logical parents or are determined automatically, thus don't need a manual selection now.
			parentTypes.remove( ComponentType.WSN_NETWORK )
			parentTypes.remove( ComponentType.PLANNING_GROUP )

			def parentRoles = new HashSet<>()
			for(String t : parentTypes){
				List<String> roles = componentModel.listComponentRoles(t)
				if( !roles.contains( ComponentRole.ROOM ) ) {
					parentRoles.addAll(componentModel.listComponentRoles(t))
				}
			}

			// Skip WSN Network checks since it's handled automatically now.
			if( parentRoles.contains( ComponentRole.NETWORK ) ) {
				if(activeDatasource == null) {
					log.error(CentralCatalogue.getUIS("NodeOperator.noDatasource"), 'message')
					return false
				}
				if(! (activeDatasource.type in parentTypes)) {
					log.error(CentralCatalogue.getUIS("NodeOperator.badDatasource"), 'message')
					return false
				}
			}

			// Don't check if the parent can be a drawing or room since auto child will handle it will child to that if there is no zone.
			if( parentRoles.contains( ComponentRole.ZONE ) && !parentRoles.contains( ComponentRole.DRAWING ) ) {
				if(activeGroup == null) {
					log.error(CentralCatalogue.getUIS("NodeOperator.noGroup"), 'message')
					return false
				}
				if(! (activeGroup.type in parentTypes)) {
					log.error(CentralCatalogue.getUIS("NodeOperator.badGroup"), 'message')
					return false
				}
			}

            if(componentModel.listComponentRoles(type).contains('staticchild') || componentModel.listComponentRoles(type).contains('dynamicchild')){
                log.error(CentralCatalogue.getUIS("NodeOperator.noDynamicChildren"), 'message')
                return false
            }
		} else {
			//component is not loaded and we couldn't find a way TO load it
			throw new UnknownComponentException( type )
			//return false;
		}
		return true
	}

	public boolean canPaste( java.util.Collection<String> types){
		for (String type : types){
			if(!this.canPaste(type)){
				return false
			}
		}
		return true
	}
	
}