package DeploymentLab.Tools

import java.awt.Color
import java.awt.Shape
import java.awt.event.KeyEvent

import java.awt.geom.Path2D
import java.awt.geom.Point2D

import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.event.*
import edu.umd.cs.piccolo.nodes.PPath;

import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.channellogger.*

class ScaleSetter extends PBasicInputEventHandler {
	private static final Logger log = Logger.getLogger(ScaleSetter.class.getName())
	private PPath shape
	private DeploymentPanel dp

	Point2D startPoint = null
	Point2D curPoint = null
	Point2D endPoint = null

	private boolean modal

	ScaleSetter(DeploymentPanel dp, PPath displayShape) {
		this.dp = dp
		shape = displayShape
		shape.setStrokePaint(Color.black)
	}
	
	public void mouseEntered(PInputEvent e){
		e.getInputManager().setKeyboardFocus(e.getPath())
	}
	
	public void mouseMoved(PInputEvent e) {
		super.mouseMoved(e)
		PNode node = e.getPickedNode()

		if(startPoint != null) {
			curPoint = e.getPosition()
			shape.setPathTo(getPath(startPoint, curPoint))
			e.setHandled(true)
		}
	}
	
	public void keyPressed(PInputEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
			startPoint = null
			dp.cancelScalingOp()
			e.setHandled(true)
		}
	}

	public void mouseClicked(PInputEvent e) {
		super.mouseClicked(e)
		if(!e.isLeftMouseButton()){
			//cancel on other mouse buttons
			startPoint = null
			dp.cancelScalingOp()
			e.setHandled(true)
			return
		}

		PNode node = e.getPickedNode()

		if(startPoint == null) {
			startPoint = e.getPosition()
			e.setHandled(true)
		} else if(curPoint != null) {
			endPoint = e.getPosition()
			shape.setPathTo(getPath(startPoint, endPoint))
			javax.swing.SwingUtilities.invokeLater( { dp.finishScalingOp() } as Runnable )
			e.setHandled(true)
		}
	}

	def void mousePressed(PInputEvent pInputEvent) {
		super.mousePressed(pInputEvent);
		pInputEvent.setHandled( true );
	}


	public void mouseDragged(PInputEvent e) {
		super.mouseDragged(e)
		e.setHandled(true)
	}

	protected Shape getPath(Point2D s, Point2D e) {
		Path2D line = new Path2D.Double()
		line.moveTo(s.getX(), s.getY())
		line.lineTo(e.getX(), e.getY())
		return line
	}

	void activate(boolean isModal = true) {
		modal = isModal
	}
	void deactivate() {
		startPoint = null
		curPoint = null
		endPoint = null
	}
}

