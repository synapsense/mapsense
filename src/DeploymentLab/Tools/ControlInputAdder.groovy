package DeploymentLab.Tools

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.event.*;

import DeploymentLab.SceneGraph.DeploymentNode
import DeploymentLab.SelectModel
import DeploymentLab.channellogger.*
import DeploymentLab.Model.*
import DeploymentLab.UndoBuffer
import DeploymentLab.Tree.*

import DeploymentLab.UIDisplay
import javax.swing.JFrame
import DeploymentLab.DisplayProperties
import DeploymentLab.SceneGraph.DeploymentPanel
import javax.swing.JOptionPane
import DeploymentLab.Dialogs.ViewOptionDialog
import DeploymentLab.Dialogs.Association.AssociationController
import DeploymentLab.CentralCatalogue

class ControlInputAdder extends PBasicInputEventHandler {
	private static final Logger log = Logger.getLogger(ControlInputAdder.class.getName())

	ToolManager manager

	//set by the ConfigurationPanel before being handed control
	public String type
	public List<String> roles

	private SelectModel selectModel
	private ComponentModel componentModel
	private UndoBuffer undoBuffer
	private ViewOptionTree viewOptionTree
	private UIDisplay uiDisplay
	private boolean modal
	private DeploymentPanel deploymentPanel
	private DisplayProperties displayProperties
	private JFrame parent
	private Properties uiStrings
	private ViewOptionDialog viewOptionDialog

	DLComponent activeDrawing
	DLComponent activeZone
	DLComponent activeNetwork


	/**
	 * Stores the list of roles that imply secondary semantics
	 */
	static List<String> secondaryRoles = null

	ControlInputAdder(SelectModel sm, ComponentModel cm, UndoBuffer ub, ViewOptionTree vt, UIDisplay ud, DeploymentPanel dp, DisplayProperties p, JFrame _parent, ViewOptionDialog vod ) {
		componentModel = cm
		selectModel = sm
		undoBuffer = ub
		viewOptionTree = vt
		uiDisplay = ud
		deploymentPanel = dp
		displayProperties = p
		parent = _parent
		viewOptionDialog = vod
		uiStrings = CentralCatalogue.getInstance().getUIStrings()
	}

	/**
	 * Entry point from the COIN adder tool.  Places other than the tool that want to glue on COINs (or other
	 * secondary-class components) should use attachControlPoint( DLComponent parentComponent, String COINType, boolean reportErrors )
	 * @param e Piccolo input event from the Tool Manager
	 * @see ControlInputAdder#attachControlPoint
	 */
	void mousePressed(PInputEvent e) {
		super.mousePressed(e)

        //populate the actives
        activeDrawing = selectModel.getActiveDrawing()
        activeDrawing = selectModel.getActiveDrawing()
        activeZone = selectModel.getActiveZone()
        activeNetwork = selectModel.getActiveNetwork()


        log.info("component roles: " + roles)
		if(!e.isLeftMouseButton())
			return

		if(!uiDisplay.isAccessible('addComponent')){
			log.info(uiDisplay.getUserLevel() +  " cannot add component!", 'message')
			return
		}

		if(type == null) {
			log.warn("Please select a configuration!", 'message')
			return
		}

		if ( ! secondaryRoles ){
			secondaryRoles = CentralCatalogue.getComp("components.secondary").split(",").toList()
		}
		if ( secondaryRoles.disjoint(this.roles) ){
			log.warn(uiStrings.getProperty("ControlInputAdder.selectCI"), 'statusbar')
			return
		}

		def visibleObjectTypes = viewOptionTree.getSelectedNodes()
		if(!visibleObjectTypes.contains(type)){
			def reply = JOptionPane.showConfirmDialog(parent, uiStrings.getProperty("ControlInputAdder.filterOffConfirm"), uiStrings.getProperty("ControlInputAdder.filterOffConfirmTitle"), JOptionPane.YES_NO_OPTION)
			if ( reply == JOptionPane.YES_OPTION ){
				viewOptionDialog.toggleComponentType(type)
			} else {
				return
			}
		}


        def parentTypes = componentModel.listParentComponentsOfType(type)
        def parentRoles = parentTypes.collect{ componentModel.listComponentClasses(it).flatten() }.flatten().unique()
        def typeDisplayName = componentModel.getTypeDisplayName(type)

		if(parentRoles.size() == 0) {
			log.error("Object has no parents, cannot be added to the deployment!", 'message')
			e.setHandled(true)
			return
		}

		if(!isValidGrouping(activeZone)){
			e.setHandled(true)
			return
		}

		PNode node = e.getPickedNode()
		if(! (node instanceof DeploymentNode) ){
			if ( roles.contains("inspector") ) {
				log.warn(uiStrings.getProperty("ControlInputAdder.selectTargetInspector"), "statusbar")
			} else if ( roles.contains("controlleddevice") ) {
			    log.warn(uiStrings.getProperty("ControlInputAdder.selectTargetDevice"), "statusbar")
			} else {
				//the new general case
			    log.warn( String.format(uiStrings.getProperty("ControlInputAdder.selectTarget.general"), typeDisplayName ) , "statusbar")
			}
			return
		}
		//get the component wrapped by the picked node - this will be the thing we attach the CP to.
		DLComponent parentComponent = node.getComponent()


		//construct the component for the control point to be added
        DLComponent cp;
		try {
			undoBuffer.startOperation()
			cp = componentModel.newComponent(type)
            log.trace("activeZone before attachControlPoint()" + activeZone.toString())
			//add the control point
			def resultingComponent = this.attachControlPointComponent( parentComponent , cp, true ) //in here, we really don't care if the operation succeeds.
			if ( resultingComponent != null ){
				selectModel.setSelection(resultingComponent)
			}
			e.setHandled(true) //setHandled no longer based on association success
		} catch (Exception ex){
			log.error("Error while adding Control Inputs!", "message")
			log.error(log.getStackTrace(ex))
			undoBuffer.rollbackOperation()
		} finally {
			undoBuffer.finishOperation()
		}
	}

	/**
	 * Alternate entry point from mouseClicked(), used by the AttachCoinDialog (and anywhere else in the app
	 * that wants to glue on COINs from code)
	 *
	 * Note that the calling method is responsible for wrapping this in an undo operation.
	 *
	 * @param parentComponent The DLComponent to attach the COIN to
	 * @param Producer the producer on the parentComponent to bolt this COIN onto
	 * @param COINType a String holding the type of COIN to add to parentComponent
	 * @param reportErrors currently unused
	 * @return the DLComponent that was added, or NULL if nothing was added (for any reason)
	 * @see ControlInputAdder#mouseClicked
	 */
	public DLComponent attachControlPoint( DLComponent parentComponent, Producer p, String COINType, boolean reportErrors ){
        //log.trace("another attachControlPoint function")
		DLComponent result = null
		type = COINType

        //log.trace("type:" + type)

		if (viewOptionTree){
			def visibleObjectTypes = viewOptionTree.getSelectedNodes()
			if(!visibleObjectTypes.contains(type)){
				def reply = JOptionPane.showConfirmDialog(parent, uiStrings.getProperty("ControlInputAdder.filterOffConfirm"), uiStrings.getProperty("ControlInputAdder.filterOffConfirmTitle"), JOptionPane.YES_NO_OPTION)
				if ( reply == JOptionPane.YES_OPTION ){
					viewOptionDialog.toggleComponentType(type)
				} else {
					return result
				}
			}
		}

		//populate the actives - since mouseClicked() haven't been called, we can't use the ones filled in from there
		activeDrawing = selectModel.getActiveDrawing()
		roles = componentModel.listComponentRoles(COINType) //we also need to fill in roles, since we didn't go through the palette
		activeZone = selectModel.getActiveZone()
		activeNetwork = selectModel.getActiveNetwork()

		def parentTypes = componentModel.listParentComponentsOfType(type)
		def parentRoles = parentTypes.collect{ componentModel.listComponentClasses(it).flatten() }.flatten().unique()

		if(parentRoles.size() == 0) {
			log.error("Object has no parents, cannot be added to the deployment!", 'message')
			return result
		}

        if(!isValidGrouping(activeZone)){
			return result
		}

		//Note that there is no undo operation here, as it's assumed that the calling method does that
		DLComponent cp = componentModel.newComponent(type) //construct the component for the control point to be added
		//add the control point
		result = this.attachControlPointComponent( parentComponent , cp, reportErrors, p ) //in here, we really don't care if the operation succeeds.
		return ( result )
	}


	/**
	 * Add a control point component to the deployment plan and associate it with an appropriate producer.
	 * Operation will fail if there is no appropriate association or if the user cancels in the association dialog.
	 * @param parentComponent The component to instrument with a control point.
	 * @param component The control point component to add.
	 * @return the DLComponent that was added, or NULL if nothing was added (for any reason)
	 */
	private DLComponent attachControlPointComponent( DLComponent parentComponent, DLComponent component, boolean reportErrors, Producer targetProducer = null ){
		DLComponent result = null
		boolean associationResult //this used to be used to set the handled state of the mouse event that triggered the adding operation, but it's being ignored at the moment.

		def parentTypes = componentModel.listParentComponentsOfType(type)
		def parentRoles = parentTypes.collect{ componentModel.listComponentClasses(it).flatten() }.flatten().unique()

		//calculate the parent component's location on the game grid
		//this is based on the location of the parent component, not the mouse click that started the operation.
		def newX = (parentComponent.getPropertyValue("x") )
		def newY = (parentComponent.getPropertyValue("y") )
		//after this, the X&Y location is adjusted by the deploymentNode itself to make sure we avoid overlaps.

		try {
            //Handled automatically now.  if('network' in parentRoles) { activeNetwork.addChild(component) }
			if('drawing' in parentRoles) { activeDrawing.addChild(component) }

			// This is after adding the object to the graph so that drawing scale can be read properly
			//also, jog the new control point to the side a little
			component.setPropertyValue("x", newX)
			component.setPropertyValue("y", newY)

            // changes order ( set coordinate and add to REIN to calculate REIN boundary) by HLim
            if( 'zone' in parentRoles ) {
                activeZone.addChild(component)
                //addToRegions((Double)newX, (Double)newY, component)
            }

			//call the associate tool
			//via SORCERY
			//actually, we get to replace sorcery with the AssociationController class, so that's convenient
			log.trace("new Secondary Node added, now attempting the association...")
			def ac = new AssociationController(parentComponent, component)

			if ( targetProducer == null ){
				associationResult = ac.associate(parent)
			} else {
				associationResult = ac.associateToProducer(parentComponent, targetProducer, component)
			}


			if ( ! associationResult ){
				//some kinda fancy rollback
				componentModel.remove(component)
				result = null
			} else {
				//if we managed to get the node all the way into the plan, adjust it so that it's not right on top of anything
				def dn = deploymentPanel.getNodeFor(component)
                log.trace("dn adjust: $component")
                if(dn==null){
                    log.trace("no deploymentnode")
                }
				dn.adjustOffset()
				result = component
				//naming the secondary component now outsourced to the Association Controller
				component.setPropertyValue("name", ac.calculateSecondaryName(parentComponent, component) )
			}
		} catch(Exception ex) {
			log.error(log.getStackTrace(ex))
			log.error("Error adding component!\nSee logfile.", 'message')
		}
		return ( result )
	}


	void activate(boolean isModal = true) {
		modal = isModal
	}
	void deactivate() {}

    /**
     * Check current active group can be a parent group or not
     * if not, show message and return false
     * @param activeZone
     * @return
     */
    private boolean isValidGrouping(DLComponent activeZone){
        def parentTypes = componentModel.listParentComponentsOfType(type)
		def parentRoles = parentTypes.collect{ componentModel.listComponentClasses(it).flatten() }.flatten().unique()
        def typeDisplayName = componentModel.getTypeDisplayName(type)
        if( 'zone' in parentRoles ) {
			if(! (activeZone?.type in parentTypes) ) {
				def groupings = componentModel.listGroupings(type)

				def names = []
				groupings.each{ gr->
					names += componentModel.getTypeDisplayName(gr)
				}
				String message = ""
				if ( names.size() > 1 ){
					message = "$typeDisplayName must go in one of the following groupings: $names"
				} else {
					message = "$typeDisplayName must go in a ${names.first()}."
				}
				log.warn(message, 'statusbar')
				return false
			}
            return true
		}
        return true
    }

}