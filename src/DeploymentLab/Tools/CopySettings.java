package DeploymentLab.Tools;

import java.awt.event.KeyEvent;

import DeploymentLab.CentralCatalogue;
import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.event.*;

import DeploymentLab.channellogger.*;
import DeploymentLab.Model.*;
import DeploymentLab.SceneGraph.DeploymentNode;
import DeploymentLab.SelectModel;
import DeploymentLab.UndoBuffer;


/**
 * Copies all properties of a component to other components, except for X, Y, and name.
 * @author Gabriel Helman
 * @since Mars
 * Date: 5/12/11
 * Time: 5:00 PM
 */
public class CopySettings extends PBasicInputEventHandler  {
	private static final Logger log = Logger.getLogger(CopySettings.class.getName());
	private DeploymentNode selected;
	private UndoBuffer undoBuffer;
	private SelectModel selectModel;
	private boolean modal;

	public CopySettings(SelectModel sm, UndoBuffer ub) {
		undoBuffer = ub;
		selectModel = sm;
		selected = null;
	}

	public void mouseClicked(PInputEvent e) {
		super.mouseClicked(e);


		if(modal && !e.isLeftMouseButton()){
			return;
		}

		PNode node = e.getPickedNode();

		DLComponent first;
		DLComponent second;

		if(node instanceof DeploymentNode) {
			if(selected == null) {
				selected = (DeploymentNode)node;
				first = selected.getComponent();
				selectModel.setSelection(first);
				if(modal)
					e.setHandled(true);
				return;
			}

			if(selected == node) {
				selected = null;
				selectModel.clearSelection();
				if(modal)
					e.setHandled(true);
				return;
			}

			first = selected.getComponent();
			second = ((DeploymentNode)node).getComponent();
		} else {
            // Clear and reset.
            selected = null;
            selectModel.clearSelection();
            e.setHandled(true);
			return;
		}

		if(first!=null && second!=null){

			if ( ! first.getType().equalsIgnoreCase( second.getType() ) ){
				log.error(CentralCatalogue.getUIS("copySettings.needSameType"), "message");
				first = null;
				second = null;
				selected = null;
				selectModel.clearSelection();
                e.setHandled(true);
				return;
			}

			try {
				undoBuffer.startOperation();
				second.copySettingsFrom( first, false );
				e.setHandled(true);

			} catch (Exception ex) {
				log.error(CentralCatalogue.getUIS("copySettings.fail"), "message");
				log.error(log.getStackTrace(ex), "default");
				undoBuffer.rollbackOperation();
			} finally {
				undoBuffer.finishOperation();

                // Reset if this is the non-modal version. Otherwise keep the copy going into more targets.
                if( modal ) {
                    selectModel.setSelection(first);
                } else {
                    selected = null;
                    selectModel.clearSelection();
                }
				second = null;
				log.info(CentralCatalogue.getUIS("copySettings.done"), "statusbar");
			}
		}else{
			log.info(CentralCatalogue.getUIS("copySettings.null"), "default");
		}
	}

    /**
     * Handles keyboard and mouse events when performing a Copy Settings action.
     *
     * @param e  The input event details.
     */
    public void keyPressed( PInputEvent e ) {
        super.keyPressed( e );

        if( e.getKeyCode() == KeyEvent.VK_ESCAPE ) {
            // Clear and reset.
            selected = null;
            selectModel.clearSelection();
            e.setHandled(true);
        }
    }

	void activate(){
		this.activate(true);
	}

	void activate(boolean isModal) {
		modal = isModal;
		selected = null;
		if(modal) {
			selectModel.clearSelection();
		}
	}

	void deactivate() {
		selected = null;
		selectModel.clearSelection();
	}
}
