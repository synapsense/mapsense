package DeploymentLab.Tools

import java.awt.BorderLayout
import java.awt.Color
//import java.awt.Font
//import java.awt.GridLayout


import java.awt.Dialog.ModalityType

import java.awt.event.KeyEvent
//import java.awt.event.MouseEvent

import java.awt.geom.Point2D

import javax.swing.BorderFactory
//import javax.swing.ButtonGroup
import javax.swing.JDialog
import javax.swing.SwingConstants

import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.event.*

import edu.umd.cs.piccolo.util.PPickPath

import DeploymentLab.channellogger.*
import DeploymentLab.Model.*
import DeploymentLab.SceneGraph.DeploymentAssociation
import DeploymentLab.SceneGraph.DeploymentNode
import DeploymentLab.SelectModel
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.UndoBuffer
import DeploymentLab.UIDisplay

import DeploymentLab.Pair
import DeploymentLab.DisplayProperties

import groovy.swing.SwingBuilder
//import javax.swing.table.DefaultTableModel
import javax.swing.JTable
//import javax.swing.DefaultCellEditor
import javax.swing.table.DefaultTableCellRenderer
//import javax.swing.event.TableModelListener
import javax.swing.table.AbstractTableModel   
import javax.swing.table.TableCellEditor
import javax.swing.AbstractCellEditor
import javax.swing.JCheckBox
import java.awt.Component
import java.awt.event.*
import javax.swing.ImageIcon
import javax.swing.JFrame
import javax.swing.JLabel
//import java.awt.event.ItemEvent;
//import java.awt.event.ItemListener;
import javax.swing.table.TableCellRenderer
import javax.swing.KeyStroke
import javax.swing.JComponent
import javax.swing.JButton
import javax.swing.JOptionPane
import java.awt.FlowLayout
import DeploymentLab.Dialogs.ViewOptionDialog
import DeploymentLab.Image.ComponentIconFactory
import java.awt.Dimension
import java.awt.Toolkit

class NodeAssociator extends PBasicInputEventHandler {
	private static final Logger log = Logger.getLogger(NodeAssociator.class.getName())

	private DeploymentNode selected

	private UndoBuffer undoBuffer
	private SelectModel selectModel
	private DeploymentPanel deploymentPanel
	private DisplayProperties displayProperties
	private UIDisplay uiDisplay
	private activities = []
	private JFrame parent
	private boolean modal
	private ViewOptionDialog viewOptionDialog

	def allAssociatorTypes = []

	boolean failed = false

	private Properties uiStrings
	private def activeProducerNames = []

	def getProducerNames() {
		return activeProducerNames
	}

	NodeAssociator(SelectModel sm, DeploymentPanel dp, UndoBuffer ub, DisplayProperties p, UIDisplay ud, JFrame _parent, ViewOptionDialog vod, Properties uis) {
		undoBuffer = ub
		selectModel = sm
		deploymentPanel = dp
		displayProperties = p
		uiDisplay = ud
		parent = _parent
		viewOptionDialog = vod
		selected = null
		uiStrings = uis
	}

	private multiAssociate(DLComponent component) {
		def multiSelect = selectModel.getExpandedSelection()

		DLComponent volunteer = multiSelect.first()

		def producerIds = volunteer.listProducers()
		def consumerIds = volunteer.listConsumers()

		def commonProducerIds = producerIds.findAll{ pId -> multiSelect.every{pId in it.listProducers()} }
		def commonConsumerIds = consumerIds.findAll{ cId -> multiSelect.every{cId in it.listConsumers()} }

		log.info("Common producers: ${commonProducerIds}, common consumers: ${commonConsumerIds}")

		def producerMatches = [] // List of (producerId, consumerId) pairs.  list(producer) -> single(consumer)
		def consumerMatches = [] // List of (consumerId, producerId) pairs.  list(consumer) -> single(producer)

		commonProducerIds.each{ pId ->
			String pType = volunteer.getProducer(pId).getDatatype()
			component.consumersOfType(pType).each{ cId ->
				producerMatches += new Pair(pId, cId)
			}
		}

		commonConsumerIds.each{ cId ->
			String cType = volunteer.getConsumer(cId).getDatatype()
			component.producersOfType(cType).each{ pId ->
				consumerMatches += new Pair(cId, pId)
			}
		}

		if(producerMatches.size() + consumerMatches.size() == 1) {
			if(producerMatches.size() == 1) {

				try {
					undoBuffer.startOperation()
					String producerId = producerMatches[0].a
					String consumerId = producerMatches[0].b
					multiSelect.each{ it ->
						component.associate(consumerId, it, producerId)
					}
				} catch (Exception ex) {
					log.error("Error while performing association!", "message")
					log.error(log.getStackTrace(ex))
					undoBuffer.rollbackOperation()
				} finally {
					undoBuffer.finishOperation()
				}
			} else {
				try {
					undoBuffer.startOperation()
					String consumerId = consumerMatches[0].a
					String producerId = consumerMatches[0].b
					multiSelect.each{ it ->
						it.associate(consumerId, component, producerId)
					}
				} catch (Exception ex) {
					log.error("Error while performing association!", "message")
					log.error(log.getStackTrace(ex))
					undoBuffer.rollbackOperation()
				} finally {
					undoBuffer.finishOperation()
				}
			}
		} else {
			log.error("Too many possibilities!\nTry the associate tool.", 'message')
		}
	}

	public void mouseClicked(PInputEvent e) {
		super.mouseClicked(e)
		if(modal && !e.isLeftMouseButton()){
			return
		}

		PNode node = e.getPickedNode()

		if(!modal && selectModel.getExpandedSelection().size() > 1) {
			if(node instanceof DeploymentNode && e.isLeftMouseButton()) {
				multiAssociate(node.getComponent())
				e.setHandled(true)
			}
			return
		}

		DLComponent first
		DLComponent second

		if(node instanceof DeploymentAssociation) {
			first = node.getProducer()
			second = node.getConsumer()
			selectModel.setSelection([first, second])
		} else if(node instanceof DeploymentNode) {
			if(selected == null) {
				selected = (DeploymentNode)node
				first = selected.getComponent()
				selectModel.setSelection([first])
				if(modal)
					e.setHandled(true)
				return
			}

			if(selected == node) {
				selected = null
				selectModel.setSelection([])
				if(modal)
					e.setHandled(true)
				return
			}

			first = selected.getComponent()
			second = node.getComponent()
		} else {
			return
		}
		
		if(first!=null && second!=null){
			try {
				undoBuffer.startOperation()
				e.setHandled( this.performAssociation( first, second) )
			} catch (Exception ex) {
				log.error("Error while performing association!", "message")
				log.error(log.getStackTrace(ex))
				undoBuffer.rollbackOperation()
			} finally {
				undoBuffer.finishOperation()
			}
		}else{
			log.info("cannot associate null object")
		}
	}


	public boolean performAssociation(DLComponent first, DLComponent second, boolean reportErrors = true ) {
		boolean externalUndo = false
		activeProducerNames = []

		activities.each{a -> a.terminate()}
		activities = []

		//log.info("First's Producers: " + first.listProducers().collect{first.getProducer(it)})
		//log.info("First's Consumers: " + first.listConsumers().collect{first.getConsumer(it)})

		//log.info("Second's Producers: " + second.listProducers().collect{second.getProducer(it)})
		//log.info("Second's Consumers: " + second.listConsumers().collect{second.getConsumer(it)})

		//Must be a default hash, as that preserves ordering for enumeration
		def firstSecond = [:] // pair(first's producer id, second's consumer id) -> active?
		first.listProducers().each{ pId ->
			Producer p = first.getProducer(pId)
			second.listConsumers().each{ cId ->
				Consumer c = second.getConsumer(cId)
				if(p.datatype == c.datatype){
					// is this pair currently associated or not?
					firstSecond[new Pair<Object, Object>(p, c)] = first.getProducer(pId).hasConsumer(second, cId)
					allAssociatorTypes.add(c.datatype)
				}
			}
		}
		//also install any conducers into the firstSecond choices
		first.listConducers().sort().each{ did ->
			Conducer alpha = first.getConducer(did)
			second.listConducers().sort().each{ oid ->
				Conducer currentOmega = second.getConducer(oid)
				if ( alpha.canProsume(currentOmega) ){
					firstSecond[new Pair<Object, Object>(alpha, currentOmega)] = alpha.isProsuming(currentOmega)
					//firstSecond[new Pair<Object, Object>(currentOmega, alpha)] = alpha.isProsuming(currentOmega)
					allAssociatorTypes.add(alpha.getProducer().getDatatype())
					allAssociatorTypes.add(alpha.getConsumer().getDatatype())
				}
			}
		}




		//Must be a default hash, as that preserves ordering for enumeration
		def secondFirst = [:]
		second.listProducers().each{ pId ->
			Producer p = second.getProducer(pId)
			first.listConsumers().each{ cId ->
				Consumer c = first.getConsumer(cId)
				if(p.datatype == c.datatype){
					secondFirst[new Pair<Object, Object>(p, c)] = second.getProducer(pId).hasConsumer(first, cId)
					allAssociatorTypes.add(c.datatype)
				}
			}
		}

/*
		//AND install any conducers into the secondFirst choices
		second.getAllConducers().each{ omega ->
		    second.getAllConducers().each{ currentAlpha ->
				if ( omega.canProsume(currentAlpha) ){
					secondFirst[new Pair<Object, Object>(omega, currentAlpha)] = omega.isProsuming(currentAlpha)
					allAssociatorTypes.add(omega.getProducer().getDatatype())
					allAssociatorTypes.add(omega.getConsumer().getDatatype())
				}
			}
		}
*/


		def unselectedAssociationTypes = displayProperties.getProperty('invisibleAssociations')
		if(unselectedAssociationTypes!=null && allAssociatorTypes.intersect(unselectedAssociationTypes).size()>0){
			def reply = JOptionPane.showConfirmDialog(parent, uiStrings.getProperty("NodeAssociator.filterOffConfirm"), uiStrings.getProperty("NodeAssociator.filterOffConfirmTitle"), JOptionPane.YES_NO_OPTION)
			if ( reply == JOptionPane.YES_OPTION ){
				allAssociatorTypes.each{ a ->
					viewOptionDialog.toggleAssociationType(a)
				}
			} else {
				failed = true
				return
			}
		}

//		println firstSecond
//		println secondFirst

		if(firstSecond.size() + secondFirst.size() == 0) {
			if ( reportErrors ) {
				log.error(uiStrings.getProperty("NodeAssociator.nothingInCommon"), 'statusbar')
			} else {
				log.error(uiStrings.getProperty("NodeAssociator.nothingInCommon") )
			}
			failed = true

		} else if((firstSecond.size() + secondFirst.size() == 1) && (firstSecond.size() == 1 ? (!firstSecond.values().toArray()[0]) : (!secondFirst.values().toArray()[0])) ) {
			firstSecond.each{ p, active ->

				if ( p.a instanceof Conducer ){
					first.associate( p.a, second, p.b )
				} else {
					second.associate(p.b.getId(), first, p.a.getId())
				}
			}
			secondFirst.each{ p, active ->

				if ( p.a instanceof Conducer ){
					second.associate( p.b, first, p.a )
				} else {
					first.associate(p.b.getId(), second, p.a.getId())
				}
			}
		} else {
			def firstSecondChoices = firstSecond.clone()
			def secondFirstChoices = secondFirst.clone()
			SwingBuilder builder = new SwingBuilder()
			boolean cancel = true
			String firstName = first.getPropertyValue('name')
			String secondName = second.getPropertyValue('name')

			def columns =["","${firstName}", "","${secondName}"]
			//def data = [:]
			//int dataIdx = 0
			//note: must also be a Linked map so that it enumerates in the same order as the other maps
			Map<Integer,associationObject> data = new LinkedHashMap<Integer,associationObject>()
			Integer dataIdx = 0

			//associationObject
			firstSecondChoices.each{ pair, active ->
				boolean isdefault = false
				if ( pair.a.getName().equals( pair.b.getName() ) ) {
					isdefault = true
				}

				def direction = "f"
				if ( pair.a instanceof Conducer ){
					direction = "both"
				}

				data[dataIdx] = new associationObject(pair, active, firstSecondChoices, direction, isdefault);
				dataIdx++;
			}

			secondFirstChoices.each{ pair, active ->
				boolean isdefault = false
				if ( pair.a.getName().equals( pair.b.getName() ) ) {
					isdefault = true
				}

				def direction = "b"
				if ( pair.a instanceof Conducer ){
					direction = "both"
				}
				data[dataIdx] = new associationObject(pair, active, secondFirstChoices, direction, isdefault);
				dataIdx++;
			}

			AssociationModel model = new AssociationModel(data, columns);
			AssociationTable associationTable = new AssociationTable(model, data);

			JDialog selectDialog
			JButton okButton

			def okAction = {selectDialog.hide(); cancel = false}
			def cancelAction = {selectDialog.hide(); cancel = true}

			//size was [400, 200]
			//hilariously large was [400,700]
			selectDialog = builder.dialog(owner:parent, resizable: true, title: 'Select Association Type', layout: new BorderLayout(), modalityType: ModalityType.APPLICATION_MODAL ) {

				scrollPane(constraints: BorderLayout.CENTER, border: BorderFactory.createEtchedBorder()) {
					widget(associationTable)
				}
				def btnPanel = panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.SOUTH) {
					okButton = button(text: 'OK', defaultButton: true, actionPerformed: okAction)
					button(text: 'Cancel', actionPerformed: cancelAction)
				}
				btnPanel.registerKeyboardAction(okAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
				btnPanel.registerKeyboardAction(cancelAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			}

			selectDialog.pack()
			selectDialog.setLocationRelativeTo(null)
			okButton.requestFocusInWindow()
			selectDialog.show()

			boolean keepLooping = true //we're going to keep asking for associations until the user gets it right
			while ( keepLooping ) {
				if ( cancel ) { failed = true; break; } //if the user changes their mind, we just bail on out.

				//having got the user's choices, let's make sure they got it right.
				//that is, that the association is only going one way.
				boolean hasFirstSecond = false
				boolean hasSecondFirst = false
				firstSecondChoices.each{ k, v -> if ( v ) { hasFirstSecond = true } }
				secondFirstChoices.each{ k, v -> if ( v ) { hasSecondFirst = true } }

//				println firstSecondChoices
//				println secondFirstChoices
				//map [pair object : active boolean]
//				firstSecondChoices.each {  k, v -> println "${k.a} , ${k.b}"}
//				secondFirstChoices.each {  k, v -> println "${k.a} , ${k.b}"}

				if ( hasFirstSecond && hasSecondFirst ) {
					//the user has it going both ways; tell them about it and give them another shot
					log.info("User tried to create a bi-directional association.")
					JOptionPane.showMessageDialog(null, "Bi-directional Associations between Components are not allowed.  Please revise your selections.", "Information", JOptionPane.WARNING_MESSAGE )
					selectDialog.show()
					keepLooping = true
				} else {

					//the user made a 1-was association.  Lock and load.
					keepLooping = false
					// Do unassociations first, then associations
					firstSecondChoices.each{ p, active ->
						if(!active && firstSecond[p]) {
							try {
								log.info("Unassociating: ${firstName}.${p.a.getId()} -> ${secondName}.${p.b.getId()}")
								if ( p.a instanceof Conducer ){
									second.unassociate( p.b, first, p.a  )
								} else {
									second.unassociate(p.b.getId(), first, p.a.getId())
								}
							} catch(Exception ex) {
								log.error("Unassociation failed: " + ex, 'message')
								log.error(log.getStackTrace(ex))
							}
						}
					}

					secondFirstChoices.each{ p, active ->
						if(!active && secondFirst[p]) {
							try {
								log.info("Unassociating: ${secondName}.${p.a.getId()} -> ${firstName}.${p.b.getId()}")
								if ( p.a instanceof Conducer ){
									first.unassociate( p.a, second, p.b  )
									//second.unassociate( p.b, first, p.a  )
								} else {
									first.unassociate(p.b.getId(), second, p.a.getId())
								}

							} catch(Exception ex) {
								log.error("Unassociation failed: " + ex, 'message')
								log.error(log.getStackTrace(ex))
							}
						}
					}

					firstSecondChoices.each{ p, active ->
						if(active && !firstSecond[p]) {
							try {
								log.info("Associating: ${firstName}.${p.a.getId()} -> ${secondName}.${p.b.getId()}")
								//second.associate(p.b.getId(), first, p.a.getId())

								if ( p.a instanceof Conducer ){
									first.associate( p.a, second, p.b )
								} else {
									second.associate(p.b.getId(), first, p.a.getId())
									activeProducerNames += first.getProducer( p.a.getId() ).getName()
								}


							} catch(Exception ex) {
								log.error("Association failed: " + ex, 'message')
								log.error(log.getStackTrace(ex))
							}
						}
					}

					secondFirstChoices.each{ p, active ->
						if(active && !secondFirst[p]) {
							try {
								log.info("Associating: ${secondName}.${p.a.getId()} -> ${firstName}.${p.b.getId()}")
								//first.associate(p.b.getId(), second, p.a.getId())


								if ( p.a instanceof Conducer ){
									second.associate( p.b, first, p.a )
								} else {
									first.associate(p.b.getId(), second, p.a.getId())
									activeProducerNames += second.getProducer( p.a.getId() ).getName()
								}


							} catch(Exception ex) {
								log.error("Association failed: " + ex, 'message')
								log.error(log.getStackTrace(ex))
							}
						}
					}
				} //end of if ( hasFirstSecond && hasSecondFirst )
			} //end of while ( keepLooping )
		} //end of else

		selected = null
		selectModel.setSelection([])
		return true
	}


	void keyPressed(PInputEvent e) {
		super.keyPressed(e)
		Point2D pos = e.getCanvasPosition()
		PPickPath ppath = e.getCamera().pick(pos.x, pos.y, (double)1.0)
		PNode picked = ppath.getPickedNode()
		if(picked instanceof DeploymentAssociation &&  ( e.getKeyCode() == KeyEvent.VK_DELETE  ||  e.getKeyCode() == KeyEvent.VK_PERIOD )) {
			if(!uiDisplay.isAccessible('associate')){
				log.info(uiDisplay.getUserLevel() + ' cannot delete association' )
				return
			}
			log.info("Deleting all associations between ${picked.getConsumer()} and ${picked.getProducer()}")
			def allAssociations = picked.listAssociations().clone() //to avoid a concurrent modification error
			allAssociations.each{ pair->

				if ( pair.a instanceof Conducer ){

					picked.getConsumer().unassociate( pair.b, picked.getProducer(), pair.a )


				} else{
					picked.getConsumer().unassociate( pair.b.getId(), picked.getProducer(), pair.a.getId() )
				}

			}
			e.setHandled(true)
		}
	}

	void activate(boolean isModal = true) {
		modal = isModal
		selected = null
		if(modal) {
			selectModel.setSelection([])
		}
	}

	void deactivate() {
		selected = null
		selectModel.setSelection([])
	}
}

class AssociationTable extends JTable {
	AbstractTableModel tableModel
	Object data
	
	public AssociationTable(AbstractTableModel tm, Object d){
		super(tm)
		tableModel = tm
		data = d
		
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF)
		getColumnModel().getColumn(0).setPreferredWidth(20)
		getColumnModel().getColumn(1).setPreferredWidth(150)
		getColumnModel().getColumn(2).setPreferredWidth(50)
		getColumnModel().getColumn(3).setPreferredWidth(150)


		int defaultHeight = tm.getRowCount() * this.getRowHeight()
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize()
		if ( defaultHeight > dim.getHeight() / 2 ){
			defaultHeight = dim.getHeight() / 2
		}

		this.setPreferredScrollableViewportSize( new Dimension( 370, defaultHeight ) )

	}
	public TableCellEditor getCellEditor(int row, int col) {
		if(col ==0)
			return new CheckBoxCellEditor(data[row])
		else
			return super.getCellEditor(row, col);
	}

	public TableCellRenderer getCellRenderer(int row, int col) {

		if(col == 0){
			return new CheckBoxCellRenderer();
		}
		else if(col == 2){
			return new ImageRenderer()
		}else{
			return super.getCellRenderer(row, col);
		}
	}
}
class AssociationModel extends AbstractTableModel {   
    
	protected Object[][] data;   
	private columns = [];   
	    
	public AssociationModel(Object d, Object columns) {   
		this.columns = columns;   
		
		int dataSize = d.size()
		data = new Object[dataSize][4]
		
		int i=0
		d.each{ idx, ascObj ->
			//data[i][0] = ascObj.active;
			if ( ascObj.defaultChecked ) {
				data[i][0] = ascObj.defaultChecked;
				//data[i].choice[data.pair] = ascObj.defaultChecked;
				//d[idx].choice[data.pair] = ascObj.defaultChecked;
			} else {
				data[i][0] = ascObj.active;
			}

			data[i][2] = ascObj.direction
			
			if(ascObj.direction =="f"){
				data[i][1] = ascObj.pair.a.getName();
				data[i][3] = ascObj.pair.b.getName();
				
			}else{
				data[i][1] = ascObj.pair.b.getName();
				data[i][3] = ascObj.pair.a.getName();
			}
			i++
		}
	}   
	    
	public Class getColumnClass(int columnIndex) {   
		return data[0][columnIndex].getClass();   
	}   
	
	String getColumnName(int col) {
		return columns[col]
	}

	public int getColumnCount() {   
		return columns.size();   
	}   
	
	public int getRowCount() {   
		return data.size();   
	}   
	
	public Object getValueAt(int rowIndex, int columnIndex) {   
		return data[rowIndex][columnIndex];   
	}   
	
	public boolean isCellEditable(int rowIndex, int columnIndex) {   
		return (columnIndex == 0);   
	}
	
	public void setValueAt(Object aValue, int rowIndex, int columnIndex){
		if(columnIndex == 0){
			data[rowIndex][columnIndex] = aValue
		}
	}
}   
    
class CheckBoxCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {   
	protected JCheckBox checkBox = new JCheckBox();   
    private Object data
    
    public CheckBoxCellEditor(Object d) {   
		data = d
		checkBox.addActionListener(this)
		checkBox.setHorizontalAlignment(SwingConstants.CENTER);   
		checkBox.setBackground(Color.white);   
	}   
	    
	public void actionPerformed(ActionEvent e){
		data.choice[data.pair] = checkBox.isSelected()
	}
	
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) { 
		checkBox.setSelected(((Boolean) value).booleanValue());
		return checkBox;   
	}
	
	public Object getCellEditorValue() {   
		return Boolean.valueOf(checkBox.isSelected());   
	}   
}   


class CheckBoxCellRenderer extends JCheckBox implements TableCellRenderer {
	public CheckBoxCellRenderer(){
		super();
		this.setHorizontalAlignment(SwingConstants.CENTER);   
		this.setBackground(Color.white);   
	}
	
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
		boolean newVal = (boolean)value;
		this.setSelected(newVal);
		return this;
	}
    public void validate() {}
    public void revalidate() {}
    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {}
    public void firePropertyChange(String propertyName, boolean oldValue, boolean newValue) {}
}


class ImageRenderer extends DefaultTableCellRenderer {
	JLabel lbl = new JLabel();

//ImageIcon forwardIcon = new ImageIcon(getClass().getResource("/resources/forward.png"));
//ImageIcon backwardIcon = new ImageIcon(getClass().getResource("/resources/back.png"));
//ImageIcon bothIcon = new ImageIcon(getClass().getResource("/resources/both.png"));

	ImageIcon forwardIcon = ComponentIconFactory.getImageIcon("/resources/forward.png");
	ImageIcon backwardIcon = ComponentIconFactory.getImageIcon("/resources/back.png");
	ImageIcon bothIcon = ComponentIconFactory.getImageIcon("/resources/both.png");

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		switch (value){
			case "f":
				lbl.setIcon(forwardIcon);
				break;
			case "b":
				lbl.setIcon(backwardIcon);
				break;
			case "both":
				lbl.setIcon(bothIcon);
				break;
		}
		lbl.setHorizontalAlignment(SwingConstants.CENTER);
		return lbl;
	}
}


class associationObject {
	Pair pair
	Boolean active
	Object choice
	String direction
	Boolean defaultChecked
	associationObject(Pair p, Boolean a, Object c, String d, Boolean pre){
		pair = p
		active = a
		choice = c
		direction = d
		defaultChecked = pre

		if (defaultChecked){
			choice[pair] = defaultChecked

		}
	}
}