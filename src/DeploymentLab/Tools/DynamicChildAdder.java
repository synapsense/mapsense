package DeploymentLab.Tools;

import java.util.Properties;

import javax.swing.JFrame;

import DeploymentLab.DisplayProperties;
import DeploymentLab.SelectModel;
import DeploymentLab.UIDisplay;
import DeploymentLab.UndoBuffer;
import DeploymentLab.Dialogs.ViewOptionDialog;
import DeploymentLab.Model.ComponentModel;
import DeploymentLab.Model.DLComponent;
import DeploymentLab.SceneGraph.DeploymentNode;
import DeploymentLab.SceneGraph.DeploymentPanel;
import DeploymentLab.Tree.ViewOptionTree;
import DeploymentLab.channellogger.Logger;
import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.event.PInputEvent;

public class DynamicChildAdder extends AbstractNodeAdder{
	private static final Logger log = Logger.getLogger(DynamicChildAdder.class.getName());

	private DLComponent parentComponent;
	private DeploymentPanel deploymentPanel;
    private final String CHILDREN_PROPERTY_NAME="children";
	
	public DynamicChildAdder(SelectModel sm, ComponentModel cm, UndoBuffer ub, ViewOptionTree vt, UIDisplay ud, DeploymentPanel dp, DisplayProperties p, JFrame _parent, ViewOptionDialog vod, Properties uis ){
		super(sm,cm,ub,vt,ud,_parent,vod,uis, dp);
        deploymentPanel = dp;
	}
	
	public void addNode(PInputEvent e) {

        PNode node = e.getPickedNode();

		if ( ! (node instanceof DeploymentNode) ){
			log.warn(componentModel.getTypeDisplayName(type) + " cannot be added by itself.", "statusbar");
			return;
		}

        parentComponent = ((DeploymentNode)node).getComponent();

		if ( ! parentComponent.hasRole("dynamicchildplaceable") ){
			log.warn("Cannot add " + componentModel.getTypeDisplayName(type) + " to " + componentModel.getTypeDisplayName(parentComponent.getType()), "statusbar");
			return;
		}

		try {
			undoBuffer.startOperation();
			addChildToParent();
			e.setHandled(true);
		} catch (Exception ex){
			log.error("Cannot add component", "message");
			log.error(log.getStackTrace(ex), "default");
			undoBuffer.rollbackOperation();
		} finally {
			undoBuffer.finishOperation();
		}
		return;
	}
	
	private void addChildToParent(){
		//log.trace("dynamicchild type =" + type);
		DLComponent childComponent = componentModel.newComponent(type);
        childComponent.setPropertyValue("x", -1.0);
        childComponent.setPropertyValue("y", -1.0);

        parentComponent.addChild(childComponent);
        selectModel.setSelection(childComponent);

	}
	
}
