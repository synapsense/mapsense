package DeploymentLab.Tools;

/**
 * Indicates which direction a move operation should nudge a selection of nodes.
 * @author Gabriel Helman
 * Date: 3/8/11
 * Time: 5:36 PM
 * @since Earth
 * @see KeyboardCommands#move(MoveCode)
 */
public enum MoveCode {
	UP, DOWN, RIGHT, LEFT
}
