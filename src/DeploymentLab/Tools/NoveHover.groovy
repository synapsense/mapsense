package DeploymentLab.Tools

import edu.umd.cs.piccolo.*

import edu.umd.cs.piccolo.event.*

import DeploymentLab.SceneGraph.DeploymentAssociation
import DeploymentLab.SceneGraph.DeploymentNode
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.DisplayProperties
import DeploymentLab.Model.ModelChangeListener
import DeploymentLab.Model.DLComponent
import DeploymentLab.channellogger.Logger
import DeploymentLab.Model.MetamorphosisEvent

class NodeHover extends PBasicInputEventHandler implements ModelChangeListener {
	private static final Logger log = Logger.getLogger(NodeHover.class.getName());

    private DeploymentPanel deploymentPanel
	private DisplayProperties displayProperties
	private boolean modal
	private List hoverNodes = []
	private List hoverAssociations = []
	private List pinnedHoverNodes = []
	private List pinnedHoverAssociations = []

	NodeHover(DeploymentPanel dp, DisplayProperties disp) {
		deploymentPanel = dp
		displayProperties = disp
	}

	public void mouseEntered(PInputEvent e) {
		super.mouseEntered(e)
		PNode node = e.getPickedNode()

		if(node instanceof DeploymentNode || node instanceof DeploymentAssociation) {
			//node.setHovered(true)
			if ( node instanceof DeploymentAssociation ) {
				hoverAssociations += node
			} else {
				hoverNodes += node
			}		

			if ( displayProperties.getProperty("augmentHover") != "NONE" ){
				//now, hover the node's friends

				if ( node instanceof DeploymentAssociation ) {
					if ( displayProperties.getProperty("augmentHover") == "CLOSE" ){
						def p = node.getProducer()
					  	hoverNodes += deploymentPanel.getNodeFor(p)
						def c = node.getConsumer()
					  	hoverNodes += deploymentPanel.getNodeFor(c)

					}  else { //augmentHover is "SUBGRAPH"
						//I think we still want to hover the line's consumer, even in subgraph mode
						def c = node.getConsumer()
					  	hoverNodes += deploymentPanel.getNodeFor(c)

					    def p = node.getProducer()
						hoverNodes += deploymentPanel.getNodeFor(p)
						hoverNodes += deploymentPanel.getConsumersFor(p)
						def buddies = p?.getAllConsumerProducers()?.unique()
						buddies.each{ b->
							hoverNodes += deploymentPanel.getNodeFor(b)
							hoverAssociations += deploymentPanel.getConsumersFor(b)
						}
					}

				} else {

					if ( displayProperties.getProperty("augmentHover") == "CLOSE" ){
						//find all associations
						def buddies = node.getComponent()?.getAllAssociatedComponents()
						buddies.each{ b->
							hoverNodes += deploymentPanel.getNodeFor(b)
						}
						hoverAssociations += deploymentPanel.getAssociationsFor(node.getComponent())
					} else { //augmentHover is "SUBGRAPH"
						//second version: hover everything below that node in the tree
						hoverNodes += deploymentPanel.getConsumersFor(node.getComponent())
						def buddies = node.getComponent()?.getAllConsumerProducers()?.unique()
						buddies.each{ b->
							hoverNodes += deploymentPanel.getNodeFor(b)
							hoverAssociations += deploymentPanel.getConsumersFor(b)
						}
					}
				}

			}
			hoverNodes.each{ if(it!=null){it.setHovered(true)} }
			hoverAssociations.each{ it.setHovered(true) }
		}
	}

	public void mouseExited(PInputEvent e) {
		super.mouseExited(e)
		PNode node = e.getPickedNode()

		//println "mouseExit:"
		//println "hoverNodes: $hoverNodes"
		//println "hoverAssoc: $hoverAssociations"

		if(node instanceof DeploymentNode || node instanceof DeploymentAssociation) {
			//node.setHovered(false)

			//make sure we don't prematurely clear the pin
			def nodes = hoverNodes.intersect(pinnedHoverNodes)
			def assoc = hoverAssociations.intersect(pinnedHoverAssociations)
			//println "hoverAssociations: $hoverAssociations"
			//println "pinnedA: $pinnedHoverAssociations"
			//println "intersect: $assoc"
			hoverNodes.removeAll( nodes )
			hoverAssociations.removeAll( assoc )
			//println "after remove: $hoverAssociations"
			//we've stored the nodes we hovered, so we can just turn them back off and clear the list out
			hoverNodes.each{if(it!=null){ it.setHovered(false)} }
			hoverAssociations.each{if(it!=null){ it.setHovered(false)} }
			hoverNodes.clear()
			hoverAssociations.clear()
		}
	}

	void activate(boolean isModal = true) {
		modal = isModal
	}
	void deactivate() {}

	/**
	 * Take the current hovered items and "pin" them, such that they stay hovered.
	 */
	public void pinHover(){
		pinnedHoverNodes.addAll( hoverNodes )
		pinnedHoverAssociations.addAll( hoverAssociations )
		hoverNodes.clear()
		hoverAssociations.clear()
	}

	public void unPinHover(){
		pinnedHoverNodes.removeAll( hoverNodes )
		pinnedHoverAssociations.removeAll( hoverAssociations )
	}

	/**
	 * Clear out the list of hovered items.
	 */
	public void clearPin(){
		pinnedHoverNodes.each{ it.setHovered(false) }
		pinnedHoverAssociations.each{ it.setHovered(false) }
		pinnedHoverNodes.clear()
		pinnedHoverAssociations.clear()
	}

	/**
	 * Checks to see if any nodes are currently pinned. 
	 * @return True if any nodes are currently pinned.
	 */
	public boolean isPinned(){
		if ( pinnedHoverNodes.size() > 0 || pinnedHoverAssociations.size() > 0 ){
			return true
		}
		return false
	}

	@Override
	void componentAdded(DLComponent component) {}

	@Override
	void componentRemoved(DLComponent component) {
		//check both hover lists
		DeploymentNode theProblem
		if ( hoverNodes.size() > 0) {
			hoverNodes.each{ hn ->
				if ( hn.getComponent() == component ){
					theProblem = hn
				}
			}
			if ( theProblem ){
				hoverNodes.remove(theProblem)
			}
			theProblem = null
		}

		if ( pinnedHoverNodes.size() > 0 ){
			pinnedHoverNodes.each{ hn ->
 				if ( hn.getComponent() == component ){
					theProblem = hn
				}
			}
			if ( theProblem ){
				pinnedHoverNodes.remove(theProblem)
			}
		}

		//also check for nodes that have already been disconnected

		List<DeploymentNode> killList = []
		for(def hn: hoverNodes){
			if ( hn.getComponent() == null ){ killList += hn }
		}
		hoverNodes.removeAll(killList)

		killList = []
		for(def hn: pinnedHoverNodes){
			if ( hn.getComponent() == null ){ killList += hn }
		}
		pinnedHoverNodes.removeAll(killList)

		List<DeploymentAssociation> killAssocList = []
		for(DeploymentAssociation ha: hoverAssociations){
			if ( ha.getProducer() == null || ha.getConsumer() == null ){ killAssocList += ha }
		}
		hoverAssociations.removeAll(killAssocList)

	}

	@Override
	void childAdded(DLComponent parent, DLComponent child) {}

	@Override
	void childRemoved(DLComponent parent, DLComponent child) {}

	@Override
	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	@Override
	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
		//check both hover lists - just remove all associations with an invalid connection
		def removeIt = []
		if ( hoverAssociations.size() > 0 ){
			hoverAssociations.each{ ha ->
				if ( !ha.getProducer() || !ha.getConsumer() ){
					removeIt += ha
				}
			}
			removeIt.each{ hoverAssociations.remove( it ) }
			removeIt.clear()
		}

		if ( pinnedHoverAssociations.size() > 0 ){
			pinnedHoverAssociations.each{ ha ->
				if ( !ha.getProducer() || !ha.getConsumer() ){
					removeIt += ha
				}
			}
			removeIt.each{ pinnedHoverAssociations.remove( it ) }
		}
	}

    @Override
    public void modelMetamorphosisStarting( MetamorphosisEvent event ) {}

    @Override
    public void modelMetamorphosisFinished( MetamorphosisEvent event ) {}
}
