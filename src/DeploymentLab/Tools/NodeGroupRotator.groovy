package DeploymentLab.Tools

import DeploymentLab.CentralCatalogue
import DeploymentLab.Model.DLComponent
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.SelectModel
import DeploymentLab.UndoBuffer
import DeploymentLab.channellogger.Logger
import edu.umd.cs.piccolo.event.PDragSequenceEventHandler
import edu.umd.cs.piccolo.event.PInputEvent

import javax.swing.*
import java.awt.geom.AffineTransform
import java.awt.geom.Point2D

/**
 * Rotate one or more components around a the center of the selection
 * @author Gabriel Helman
 * @since Io
 * Date: 12/10/12
 * Time: 10:22 AM
 */
class NodeGroupRotator extends PDragSequenceEventHandler {
	private static final Logger log = Logger.getLogger(NodeGroupRotator.class.getName())
	private boolean modal
	private SelectModel selectModel
	private UndoBuffer undoBuffer

	private static final double gravity = 22.5

	private Point2D startPoint = null
	private Point2D endPoint = null
	private Point2D superStartPoint = null

	public static final SMALL_LEFT = 22.5
	public static final SMALL_RIGHT = -22.5
	public static final BIG_LEFT = 90
	public static final BIG_RIGHT = -90

	NodeGroupRotator(SelectModel sm, UndoBuffer ub) {
		selectModel = sm
		undoBuffer = ub
	}


	void activate(boolean isModal = true) {
		modal = isModal
	}


	void deactivate() {
		modal = false
	}


	public boolean shouldStartDragInteraction(PInputEvent e) {
		if (!super.shouldStartDragInteraction(e)) {
			return false
		}

		//if ((e.isLeftMouseButton()) && e.isShiftDown()) {
		if (e.isLeftMouseButton() && selectModel.getExpandedSelection().size() > 0 ) {
			return true
		} else {
			return false
		}
	}


	public void startDrag(PInputEvent e) {
		super.startDrag(e)

		if (selectModel.getExpandedSelection().size() == 0){
			return
		}
	}


	public void drag(PInputEvent e) {
		super.drag(e)
		if (superStartPoint == null){
			superStartPoint = CentralCatalogue.getDeploymentPanel().scalePoint(e.getPosition())
			log.trace("setting ssp to $superStartPoint")
			undoBuffer.startOperation()
		}

		if (startPoint == null) {
			startPoint = CentralCatalogue.getDeploymentPanel().scalePoint(e.getPosition())
		}
		log.trace("dragging the new rotatator")
		endPoint = CentralCatalogue.getDeploymentPanel().scalePoint(e.getPosition())
		//calc distance

		def dist = (startPoint.x - endPoint.x) / 2

		if (e.isControlDown()) {
			if (dist > 0) {
				dist = gravity
			} else {
				dist = -gravity
			}
		}
		//we never want 360 degrees when it's back to the starting point, so coerce that to zero
		if (dist > 360) {
			dist -= 360
		}
		if (dist < 0) {
			dist += 360
		}
		this.rotateSelection(selectModel.getExpandedSelection(), dist)
		startPoint = endPoint
	}


	public void endDrag(PInputEvent e) {
		super.endDrag(e)
		if (startPoint == null || endPoint == null) {
			return
		}
		endPoint = startPoint = superStartPoint = null
		undoBuffer.finishOperation()
	}


	public void rotateSelection(List<DLComponent> selection, Double degrees, boolean useCommonCenter = false) {
		if (selection.size() == 0) {
			log.warn(CentralCatalogue.getUIS("rotateSelection.noSelection"),"statusbar")
			return
		}

		AffineTransform at
		if (useCommonCenter){
			//find common center
			List<Point2D.Double> points = []
			selection.each { points.add(it.getXY()) }
			def center = DeploymentPanel.findCenterPoint(points)
			at = AffineTransform.getRotateInstance(-Math.toRadians(degrees), center.x, center.y)
		} else {
			at = AffineTransform.getRotateInstance(-Math.toRadians(degrees), superStartPoint.x, superStartPoint.y)
		}

		for (def c : selection) {
			c.setXY(at.transform(c.getXY(), null))
			if (c.hasProperty("rotation")) {
				double rackrot = c.getPropertyValue("rotation") + degrees
				if (rackrot >= 360) {
					rackrot -= 360.0
				}
				c.setPropertyValue("rotation", rackrot)
			}
		}
	}

	public void rotationOperation(Double degrees, boolean useCommonCenter = false){
		try {
			undoBuffer.startOperation()
			this.rotateSelection(selectModel.getExpandedSelection(), degrees, useCommonCenter)
		} catch (Exception e) {
			log.error("error while rotating")
			undoBuffer.rollbackOperation()
			throw e
		} finally {
			undoBuffer.finishOperation()
		}

	}


	public void rotateFromDialogBox() {
		if (selectModel.getExpandedSelection().size() == 0){
			log.warn(CentralCatalogue.getUIS("rotateSelection.noSelection"),"statusbar")
			return
		}
		String input = JOptionPane.showInputDialog(CentralCatalogue.getParentFrame(), CentralCatalogue.getUIS("rotateSelection.prompt"), CentralCatalogue.getUIS("rotateSelection.title"), JOptionPane.QUESTION_MESSAGE)
		if( input && input.isDouble() ) {
			Double degrees = Double.parseDouble(input)
			this.rotationOperation(degrees,true)
		}
	}


}