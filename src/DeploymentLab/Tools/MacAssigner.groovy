package DeploymentLab.Tools

import javax.swing.JOptionPane

import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.event.*

import DeploymentLab.Model.*
import DeploymentLab.channellogger.*
import DeploymentLab.SceneGraph.DeploymentNode

import DeploymentLab.SelectModel
import javax.swing.JFrame

class MacAssigner extends PBasicInputEventHandler {
	private static final Logger log = Logger.getLogger(MacAssigner.class.getName())
	private SelectModel selectModel
	private boolean modal
	def macMap = [:]
	private JFrame parent
	
	MacAssigner(SelectModel sm, JFrame _parent) {
		selectModel = sm
		parent = _parent
	}

	void mouseClicked(PInputEvent e) {
		if(modal && !e.isLeftMouseButton())
			return
		PNode node = e.getPickedNode()

		if(node instanceof DeploymentNode) {
			DLComponent component = node.getComponent()
			String name = component.getPropertyValue('name')
			selectModel.setSelection([component])
			
			macMap.clear()
			for(String propName: component.listMacIdProperties()) {
				ComponentProperty cp = component.getComponentProperty(propName)
				if(cp.getValue()!=null && cp.getValue()!='')
					macMap[propName] = cp.getValue()
			}
			
			for(String propName: component.listMacIdProperties()) {
				ComponentProperty cp = component.getComponentProperty(propName)
				def oldVal = cp.getValue()
				if(oldVal.equals('0')){
					oldVal = ''
				}	

				String mac = JOptionPane.showInputDialog(parent, "$name - " + cp.getDisplayName() + " - Scan MAC Address :", oldVal)
				
				while(macMap.findAll{k,v->v.equals(mac) && !k.equals(propName)}.size()>0){
					log.error('Duplicate MAC Address', 'message')
					mac = JOptionPane.showInputDialog(parent, "$name - " + cp.getDisplayName() + " - Scan MAC Address :", oldVal)
				}
				
				while(mac==''){
					log.error('Mac Address is required', 'message')
					mac = JOptionPane.showInputDialog(parent, "$name - " + cp.getDisplayName() + " - Scan MAC Address :", oldVal)
				}
				
				if(mac==null){
					break
				}else{
					macMap[propName] = mac
					component.setPropertyValue(propName, mac)
				}
			}
			e.setHandled(true)
		}
	}

	void activate(boolean isModal = true) {
		modal = isModal
	}
	void deactivate() {}
}

