package DeploymentLab.Tools

import DeploymentLab.CentralCatalogue
import DeploymentLab.Dialogs.ViewOptionDialog
import DeploymentLab.Dialogs.WSNNetworkController
import DeploymentLab.Exceptions.ModelCatastrophe
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentRole
import DeploymentLab.Model.DLComponent
import DeploymentLab.SceneGraph.DeploymentNode
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.SceneGraph.DynamicChildrenNode
import DeploymentLab.SceneGraph.StaticChildrenNode
import DeploymentLab.Security.NetworkKey
import DeploymentLab.Security.PanIDFactory
import DeploymentLab.SelectModel
import DeploymentLab.StringUtil
import DeploymentLab.Tree.ViewOptionTree
import DeploymentLab.UIDisplay
import DeploymentLab.UndoBuffer
import DeploymentLab.channellogger.Logger
import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.event.PBasicInputEventHandler
import edu.umd.cs.piccolo.event.PInputEvent

import javax.swing.*
import java.awt.geom.Point2D

/**
 * Abstract class to provide the basic skeleton of a Node Adder.
 * @author Hyeeun Lim
 * Date: 5/24/11
 * Time: 2:45 PM
 * @since Mars
 */
abstract class AbstractNodeAdder extends PBasicInputEventHandler {
    private static final Logger log = Logger.getLogger(AbstractNodeAdder.class.getName())

	ToolManager manager

    protected String type
    protected SelectModel selectModel
    protected ComponentModel componentModel
    protected UndoBuffer undoBuffer
    protected ViewOptionTree viewOptionTree
    protected UIDisplay uiDisplay
    protected boolean modal
    protected JFrame parent
    protected Properties uiStrings
    protected ViewOptionDialog viewOptionDialog
	protected DeploymentPanel deploymentPanel

    protected DLComponent activeDrawing
	protected DLComponent activeZone
	protected DLComponent activeNetwork
    protected def parentTypes
	protected def parentRoles

    private final String CLASS_ATTRIBUTE_NAME = "class";
    private final String STATICCHILDRENNNODE_CLASS_VALUE = "StaticChildrenNode"  ;
    private final String DYNAMICCHILDRENNNODE_CLASS_VALUE = "DynamicChildrenNode"  ;

    public AbstractNodeAdder(SelectModel sm, ComponentModel cm, UndoBuffer ub, ViewOptionTree vt, UIDisplay ud, JFrame _parent, ViewOptionDialog vod, Properties uis, DeploymentPanel dp){
        componentModel = cm
        selectModel = sm
        undoBuffer = ub
        viewOptionTree = vt
        uiDisplay = ud
        parent = _parent
        viewOptionDialog = vod
        uiStrings = uis
	    deploymentPanel = dp
    }

    public void mousePressed(PInputEvent e) {
        if(!e.isLeftMouseButton())
            return
        addNode(e)
        e.setHandled(true);
    }

    public abstract void addNode(PInputEvent e)

    void activate(boolean isModal = true) {
        modal = isModal
    }

    public void deactivate() {}

    boolean isAddable( PInputEvent e, String forComp = null ){
        activeDrawing = selectModel.getActiveDrawing()
	    activeZone = selectModel.getActiveZone()
        activeNetwork = selectModel.getActiveNetwork()

        parentTypes = componentModel.listParentComponentsOfType(type)
        parentRoles = parentTypes.collect{ componentModel.listComponentClasses(it).flatten() }.flatten().unique()


	    if(!uiDisplay.isAccessible('addComponent')){
		    if ( uiDisplay.getProperty("isFileLoaded") ){
			    log.warn( CentralCatalogue.getUIS("NodeAdder.notAccessible.lowLevel"), "message" )
		    } else {
			    log.warn( CentralCatalogue.getUIS("NodeAdder.notAccessible.noFile"), "message" )
		    }
		    e.setHandled(true)
		    return false
	    }

        if(type == null) {
            log.warn( uiStrings.getProperty("NodeAdder.noComponentSelected"), 'statusbar')
            e.setHandled(true)
        }

	    // Only do this if we have the UI parts since it's nice to unit test things without it.
	    if( viewOptionTree ) {
		    def visibleObjectTypes = viewOptionTree.getSelectedNodes()
		    if(!visibleObjectTypes.contains(type)){
			    def reply = JOptionPane.showConfirmDialog(parent,uiStrings.getProperty("NodeAdder.filterOffConfirm"), uiStrings.getProperty("NodeAdder.filterOffConfirmTitle"), JOptionPane.YES_NO_OPTION)
			    if ( reply == JOptionPane.YES_OPTION ){
				    viewOptionDialog.toggleComponentType(type)
			    } else {
				    return false
			    }
		    }
	    }

	    // Generic way to skip parenting to a group, but was originally done as a hack way to handle things like the
	    // Bender PDU that has embedded calculations that we don't want to be added to a calculation group.
	    if( componentModel.getTypeAttribute( type, 'noGroupParent')?.equalsIgnoreCase('true') ) {
		    parentRoles.removeAll( CentralCatalogue.GROUP_TYPE_ROLES )
	    }

		if(parentRoles.size() == 0) {
			log.error("Object has no parents, cannot be added to the deployment!", 'message')
			e.setHandled(true)
			return false
		}

	    def typeDisplayName = componentModel.getTypeDisplayName(type)
	    if ('network' in parentRoles && !('network' in parentTypes)) {
		    if (!(selectModel.getActiveNetwork()?.type in parentTypes)) {
			    def datasources = componentModel.listDatasources(type)

			    //check for exactly one matching option and switch to it if possible
			    def allowedDS = componentModel.getComponentsByType(datasources.first(), selectModel.activeDrawing)
			    if (allowedDS.size() == 1) {
				    selectModel.setActiveNetwork(allowedDS.first())
			    } else {
				    def names = []
				    datasources.each { ds ->
					    names += componentModel.getTypeDisplayName(ds)
				    }
				    String message = ""
				    if (names.size() > 1) {
					    message = "$typeDisplayName must go in a ${StringUtil.listFormat(names)} Data Source."
				    } else {
					    message = "$typeDisplayName must go in a ${names.first()} Data Source."
				    }
				    log.warn(message, 'statusbar')
				    if (undoBuffer.operationInProgress()){
					    undoBuffer.rollbackOperation()
				    }
				    e.setHandled(true)
				    return false
			    }
		    }
	    }

	    if ('network' in parentRoles && 'network' in parentTypes) {
		    def datasources = componentModel.listDatasources(type)
		    def allowedDS = componentModel.getComponentsByType(datasources.first(), selectModel.activeDrawing)

		    if (type.equals('remote-gateway')) { // if gateway - always show dialog
			    WSNNetworkController.showDialog(componentModel, selectModel, forComp, true)
			    activeNetwork = WSNNetworkController.getSelectedNetwork()
			    if (!activeNetwork) {
				    log.warn("Add Gateway canceled.", "statusbar")
				    return false
			    }
		    } else { // if regular wsn component - just create new if no one exists
			    if (allowedDS.size() == 0) {
				    activeNetwork = createNetwork()
				    if (!activeNetwork) {
					    throw new ModelCatastrophe("Unable create/find network for new component. Please contact administrator.")
				    }
			    }
		    }
	    }

	    if( ComponentRole.ZONE in parentRoles && !parentRoles.contains( ComponentRole.LOGICAL_GROUP ) ) {
		    if( ! (selectModel.getActiveZone()?.type in parentTypes) ) {
			    def groupings = componentModel.listGroupings(type)
			    def names = []
			    groupings.each{ gr->
				    names += componentModel.getTypeDisplayName(gr)
			    }
			    String message = ""
			    if ( names.size() > 1 ){
				    message = "$typeDisplayName must go in one of the following groupings: $names"
			    } else {
				    message = "$typeDisplayName must go in a ${names.first()}."
			    }
			    log.warn(message, 'statusbar')
			    e.setHandled(true)
			    return false
		    }
	    }

        PNode node = e.getPickedNode()
        if(node && (node instanceof DeploymentNode || node instanceof  StaticChildrenNode || node instanceof DynamicChildrenNode || node.getAttribute(CLASS_ATTRIBUTE_NAME).equals(STATICCHILDRENNNODE_CLASS_VALUE) || node.getAttribute(CLASS_ATTRIBUTE_NAME).equals(DYNAMICCHILDRENNNODE_CLASS_VALUE)))
            return

        return true
    }

    void addToParent(DLComponent component){

        if('network' in parentRoles && activeNetwork ) {
            activeNetwork.addChild(component)
        }

	    if('zone' in parentRoles && activeZone ) {
		    activeZone.addChild(component)
	    }

        if('drawing' in parentRoles) {
            activeDrawing.addChild(component)
        } else if( ComponentRole.ROOM in parentRoles ) {
	        // Child to the orphanage for now. RoomController will pick the correct room later, but some things barf
	        // prior to the controller taking action when a component has no parent.
	        componentModel.getOrphanage( activeDrawing ).addChild( component )
        }
    }

	protected DLComponent createNetwork(){
		try {
			DLComponent network = componentModel.newComponent('network')
			network.setPropertyValue('name', "WSN Network")
			selectModel.getActiveDrawing().addChild(network)

			//generate a PAN ID for WSN Networks
			network.setPropertyValue('pan_id', PanIDFactory.makePanID(componentModel))

			//also make new network keys
			def p = NetworkKey.makeNetworkKeys()
			network.setPropertyValue('networkPublicKey', p.a)
			network.setPropertyValue('networkPrivateKey', p.b)

			return network
		} catch (Exception ex) {
			log.error("Error while creating Data Source!", "message")
			log.error(log.getStackTrace(ex))
		}

		return null
	}
}
