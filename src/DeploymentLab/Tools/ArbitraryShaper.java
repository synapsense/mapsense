package DeploymentLab.Tools;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Properties;

import javax.swing.*;

import DeploymentLab.*;
import DeploymentLab.Dialogs.ViewOptionDialog;
import DeploymentLab.Image.ComponentIconFactory;
import DeploymentLab.Model.ComponentModel;
import DeploymentLab.Model.ComponentProp;
import DeploymentLab.Model.ComponentType;
import DeploymentLab.Model.DLComponent;
import DeploymentLab.SceneGraph.ArbitraryShapeNode;
import DeploymentLab.SceneGraph.DeploymentPanel;
import DeploymentLab.Tree.ViewOptionTree;
import DeploymentLab.channellogger.Logger;
import DeploymentLab.StaticValidator.ShapeValidations;
import edu.umd.cs.piccolo.PCanvas;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.nodes.PPath;

/***
 *  A class to generate arbitaryshaped node ( contained polygon)
 *  @author Hyeeun Lim
 *  @since Mars
 */
public class ArbitraryShaper extends AbstractNodeAdder{
	private static final Logger log = Logger.getLogger(ArbitraryShaper.class.getName());

	private static final BasicStroke GUIDELINE_STROKE = new BasicStroke( 1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
	                                                                     2.0f, new float[]{5}, 5f );

	private DeploymentPanel deploymentPanel;
    private ToolManager toolManager;

	private ArrayList<Point2D> points = new ArrayList<>();    // the list of temporary points ( every time to click a spot, it stores the point
	private ArrayList<PPath> paths = new ArrayList<PPath>();    // the list of sides connects a dot to another
    private final int EDGE_WIDTH = 10;  // edge size (pixel)
    private boolean inProgress;
    private boolean isOrthoMode = false;    // ortho mode to draw orthogonal line ( shift key down)
    private Properties uiStrings;

	private PPath guideline = null;

    /**
     * A tool to add arbitrary shape node ( contained polygon)
     * @param sm
     * @param cm
     * @param ub
     * @param vt
     * @param ud
     * @param _parent
     * @param vod
     * @param uis
     * @param deploymentPanel
     */
	public ArbitraryShaper(SelectModel sm, ComponentModel cm, UndoBuffer ub, ViewOptionTree vt, UIDisplay ud, JFrame _parent, ViewOptionDialog vod, Properties uis, DeploymentPanel deploymentPanel,ToolManager tm){
		super(sm,cm,ub,vt,ud,_parent,vod,uis, deploymentPanel);
		this.deploymentPanel = deploymentPanel;
        this.toolManager = tm;
        uiStrings = CentralCatalogue.getInstance().getUIStrings();
    }

	/**
	 * mousePressed event: add a point
	 *
	 * @param e
	 */
	public void mousePressed(PInputEvent e) {
		//log.info("mousePresssed in arbitaryShaper");
		if( e.isLeftMouseButton() && ( e.getClickCount() == 1 ) ) {
			//always load the current scale in before drawing a point
			addNode(e);
		} else if( !points.isEmpty() && ( e.isRightMouseButton() || ( e.getClickCount() > 1 ) )) {
			completeShape();
		}

		e.setHandled(true);
	}

    /**
     *  mouseMoved event: if shift key is pressed, set ortho mode
     * @param e
     */
    public void mouseMoved(PInputEvent e){
        //log.info("mouse moved");
        if(e.isShiftDown()){
            //log.trace("shift is down start ortho mode");
            setOrthoMode(true);
        } else{
            setOrthoMode(false);
        }

	    Point2D thisPt = getAdjustedPoint( e.getPosition() );

	    if( !points.isEmpty() ) {
		    Point2D lastPt = points.get( points.size() - 1 );
		    if( guideline == null ) {
			    guideline = PPath.createLine( (float) thisPt.getX(), (float) thisPt.getY(),
			                                  (float) lastPt.getX(), (float) lastPt.getY() );
			    guideline.setStroke( GUIDELINE_STROKE );
			    guideline.setPickable( false );
			    deploymentPanel.addPoint( guideline );
		    } else {
			    guideline.setPathToPolyline( new Point2D[] { thisPt, lastPt } );
		    }
	    }
    }

    /**
     * set ortho mode, change deploymentPanel's cursor for ortho mode or normal mode
     * @param isOrtho
     */
    public void setOrthoMode(boolean isOrtho){
        if(isOrtho){
            deploymentPanel.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
            isOrthoMode = true;
           // log.info("Containment Polygon Drawing Ortho Mode", "statusbar");
        }else{
            deploymentPanel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            isOrthoMode = false;
           // log.info("", "statusbar");
        }
    }

    /**
     * Add a point
     * when click the existing point, get closing point and close the path to create polygon
     * if user tries to draw invalid shape, it will ask if continue to draw or not
     * @param e
     */
	public void addNode(PInputEvent e){
        if(!isAddable(e)){
	        return;
        }
        //log.trace("inProgress:"+inProgress);
        if( !inProgress ) {
            DLComponent[] emptySet = {};
            selectModel.setSelection(emptySet);
        }

		inProgress = true;

		Point2D point = getAdjustedPoint( e.getPosition() );

        //log.trace("closing index:" + closedIndex);
		if( !type.equals( ComponentType.DRAWING_LINE ) && isClosingPoint( point ) ) {
			completeShape();
			e.setHandled(true);
		}else{  // continue drawing

            // check points validation in real time

            ShapeValidations tempValidation = validateIncompleteShape(points, point);
            //log.trace("temp validation:" + tempValidation.toString());
            // if current shape is not valid, ask the user if continueing or not
            if(tempValidation!=ShapeValidations.VALID_SHAPE){
                int choice = JOptionPane.showConfirmDialog(null, uiStrings.getProperty("validator.containedArea.confirmDeleteInvalidShape"), "Invalid Shape", JOptionPane.OK_CANCEL_OPTION);
                // if user confirms to finish to draw and remove it
                if(choice == JOptionPane.CANCEL_OPTION) {
                    e.setHandled(true);
			        endDrawing();

                }
            }else{
                // create the connection line
                if(points.size()>0){
	                 PPath line;
                     Point2D prevPoint = points.get(points.size()-1);
                     line = PPath.createLine( (float) prevPoint.getX(), (float) prevPoint.getY(),
                                              (float) point.getX(), (float) point.getY() );
                     paths.add(line);
                     deploymentPanel.addPoint(line);
                }
                points.add(point);
            }
		}
		//log.info("ArbitraryPolygon point:");
		//log.info(Double.toString(point.getX()));
		//log.info(Double.toString(point.getY()));
        setOrthoMode(false);
	}

    /**
     * finish drawing, clear points, and path.
     * set pointStr empty
     */
	private void endDrawing(){
		deploymentPanel.removePoints( paths, null );
		deploymentPanel.removePoint( guideline );
		guideline = null;
		points.clear();
		paths.clear();
		inProgress = false;
	}

	private Point2D getAdjustedPoint( Point2D truePt ) {
		Point2D pt = CentralCatalogue.getDeploymentPanel().scalePoint( truePt );

		if( isOrthoMode ) {
			pt = getOrthoPoint( pt );
		}

		return ((NodeMover)this.getManager().getTool("mover")).getPossiblePosition( pt );
	}

    /**
     * get orthogonal point for next one
     * @param p
     * @return
     */
    private Point2D getOrthoPoint(Point2D p){
        if(points.size()>0){
			//should be scaled already
			Point2D prevPoint = points.get(points.size()-1);

            float prevX = (float) prevPoint.getX();
            float prevY = (float) prevPoint.getY();

            float curX = (float) p.getX();
            float curY = (float) p.getY();

            Point2D possiblePoint1 = new Point2D.Float(prevX, curY);
            Point2D possiblePoint2 = new Point2D.Float(curX, prevY);

            double d1 = p.distance(possiblePoint1);
            double d2 = p.distance(possiblePoint2);

            if(d1<d2){
                return possiblePoint1;
            }else{
                return possiblePoint2;
            }
        }

        return p;
    }

    /**
     * Checks to see if the point is on the first point, closing the shape.
     * @param point  The point to check to see if it is on the shape's first point.
     * @return True if the point is on the first point, false otherwise.
     */
	private boolean isClosingPoint( Point2D point ) {
		if( !points.isEmpty() ) {
			return points.get(0).distance( point ) <= EDGE_WIDTH / 2;
		}
		return false;
	}

    /**
     * Validate and create the shape node.
     */
	private void completeShape() {

		ShapeValidations shapeValidation = validateCompleteShape();

		if( shapeValidation == ShapeValidations.VALID_SHAPE ) {

			// Valid shape, so create a new component and get out of the drawing state.
			PPath polygon = new PPath();
			polygon.setPathToPolyline( points.toArray(new Point2D[points.size()]) );

			if( !type.equals( ComponentType.DRAWING_LINE ) ) {
				polygon.closePath();
			}

			Point2D centerPoint = polygon.getFullBounds().getCenter2D();

			undoBuffer.startOperation();
			try {
				DLComponent component = componentModel.newComponent( type );

				addToParent( component );

				// Don't track this stuff since an undo will just remove the component anyway.
				undoBuffer.stopTracking();

				component.setXY( centerPoint );
				component.setPolygonPoints( points );

				undoBuffer.resumeTracking( false );

				selectModel.setSelection( component );

			} catch( Exception ex ) {
				log.error(log.getStackTrace(ex));
				log.error("Error adding component!\nSee logfile.", "message");
				undoBuffer.rollbackOperation();
			} finally {
				undoBuffer.finishOperation();
				endDrawing();

				// The standard handling for singleShot doesn't work for the shaper, so manually manage it.
				if( toolManager.isSingleShotMode() ) {
					toolManager.activateDefaults();
				}
			}
		}else if( shapeValidation == ShapeValidations.LESS_MIN_POINTS || shapeValidation == ShapeValidations.CROSSED_EDGE ) {
			// The last point made an invalid shape. Ask the user if they want to continue drawing or not.
			int choice = JOptionPane.showConfirmDialog( null, uiStrings.getProperty("validator.containedArea.confirmDeleteInvalidShape"),
			                                            "Invalid Shape", JOptionPane.OK_CANCEL_OPTION );
			// if user confirms to finish to draw and remove it
			if( choice == JOptionPane.CANCEL_OPTION ) {
				endDrawing();
			}
		} else {
			// We somehow got a bad shape where removing the last point won't even fix it, so just end it.
			log.info(uiStrings.getProperty("PopupMenu.containedArea.invalidShape"),"message");
			endDrawing();
		}
	}


    /**
     * validate polygon shape
     * 0: valid shape
     * 1: the number of points is less than the minimum required points
     * 2: crossed line
     * 3: unknown invalid issue
     * @return
     */
	private ShapeValidations validateCompleteShape(){
		// Need at least 2 points, regardless of the shape type.
        if( points.size() < 2 ){
            return ShapeValidations.LESS_MIN_POINTS;
        }else{
	        Line2D.Double l1 = new Line2D.Double(points.get(points.size()-2), points.get(points.size()-1));
	        if (intersectsOtherShapes(l1)){
		        return ShapeValidations.CROSSED_EDGE;
	        }

	        int minPoints = 3;

	        String valStr = componentModel.getTypeDefaultValue( type, ComponentProp.MIN_SHAPE_PTS );
	        if( valStr != null && !valStr.isEmpty() ) {
		        try {
			        minPoints = Integer.parseInt( valStr );
		        } catch( NumberFormatException e ) {
			        log.error("Bad " + ComponentProp.MIN_SHAPE_PTS + " default value '" +
			                  valStr +"' in '" + type + "'. Using default 3.");
		        }
	        }

            return StaticValidator.validateShape( points, minPoints, type );
        }

    }

    /**
     * validate incomplete polygon shape for real time validation
     * @param existingPoints
     * @param newPoint
     * @return
     */
    private ShapeValidations validateIncompleteShape(ArrayList<Point2D> existingPoints, Point2D newPoint){
        ArrayList<Point2D> tempPoints = (ArrayList<Point2D>)existingPoints.clone();
        tempPoints.add(newPoint);

	    if (!existingPoints.isEmpty()){
		    Line2D.Double l1 = new Line2D.Double(newPoint, existingPoints.get(existingPoints.size()-1));
		    if (intersectsOtherShapes(l1)){
			    return ShapeValidations.CROSSED_EDGE;
		    }
	    }

        return StaticValidator.validateIncompleteShape(tempPoints);
    }

	private boolean intersectsOtherShapes(Line2D line){
		for (DLComponent dlc : selectModel.getActiveDrawing().getChildComponents()){
			if (dlc.getType().equals("room")){
				ArbitraryShapeNode roomNode = (ArbitraryShapeNode)deploymentPanel.getPNodeFor(dlc);
				if (roomNode != null && roomNode.intersects(line)){
					return true;
				}
			}
		}
		return false;
	}

	public void activate() {
		super.activate();

		// Skip for unit tests.
		if( deploymentPanel != null ) {
			// Load a better cursor for precision operations.
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			Image image = ComponentIconFactory.getImageIcon( "/resources/scalingcursor.png" ).getImage();
			Point hotSpot = new Point(16, 16);
			Cursor cursor = toolkit.createCustomCursor(image, hotSpot, "rescaling");
			((PCanvas) deploymentPanel.getViewport().getView()).pushCursor(cursor);
		}
	}

	/**
     * deactivate this tool
     */
	public void deactivate() {
        if( inProgress ) {
		    endDrawing();
        }
		((PCanvas) deploymentPanel.getViewport().getView()).pushCursor(null);
    }
}
