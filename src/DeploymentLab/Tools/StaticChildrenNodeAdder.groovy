package DeploymentLab.Tools

import DeploymentLab.CentralCatalogue
import DeploymentLab.Dialogs.ViewOptionDialog
import DeploymentLab.Exceptions.DuplicateObjectException
import DeploymentLab.Exceptions.IncompatibleTypeException
import DeploymentLab.SceneGraph.DeploymentNode
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.SelectModel
import DeploymentLab.StringUtil
import DeploymentLab.Tree.ViewOptionTree
import DeploymentLab.UIDisplay
import DeploymentLab.UndoBuffer
import DeploymentLab.channellogger.Logger
import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.event.PBasicInputEventHandler
import edu.umd.cs.piccolo.event.PInputEvent
import java.awt.geom.Point2D
import javax.swing.JFrame
import javax.swing.JOptionPane
import DeploymentLab.Model.*
import DeploymentLab.SceneGraph.DynamicChildrenNode
import DeploymentLab.SceneGraph.StaticChildrenNode
import edu.umd.cs.piccolo.nodes.PImage

/**
 * Created by IntelliJ IDEA.
 * User: HLim
 * Date: 5/23/11
 * Time: 3:38 PM
 * To change this template use File | Settings | File Templates.
 */
class StaticChildrenNodeAdder extends AbstractNodeAdder{
    private static final Logger log = Logger.getLogger(StaticChildrenNodeAdder.class.getName())


	public StaticChildrenNodeAdder(SelectModel sm, ComponentModel cm, UndoBuffer ub, ViewOptionTree vt, UIDisplay ud, JFrame _parent, ViewOptionDialog vod, Properties uis, DeploymentPanel dp ) {
		super(sm,cm,ub,vt,ud,_parent,vod,uis, dp)
 	}

	void addNode(PInputEvent e) {
        // check node can be added or not
		undoBuffer.startOperation()
        if(!isAddable(e)) {
	        undoBuffer.finishOperation()
            return
        }
        //log.trace(node.getAttribute(CLASS_ATTRIBUTE_NAME))

		Point2D pt = e.getPosition()

        DLComponent component

		try {
			component = componentModel.newComponent(type)
            addToParent(component)
            for(DLComponent childRackComponent:component.getChildComponents()){
                addToParent(childRackComponent)
            }

			// This is after adding the object to the graph so that drawing scale can be read properly
			def p = CentralCatalogue.getDeploymentPanel().scalePoint( pt )
			def adjusted = ((NodeMover)this.getManager().getTool("mover")).getPossiblePosition(p)
			component.setPropertyValue("x", adjusted.x)
			component.setPropertyValue("y", adjusted.y)
		} catch(Exception ex) {
			log.error(log.getStackTrace(ex))
			log.error("Error adding component!\nSee logfile.", 'message')
			undoBuffer.rollbackOperation()
		} finally {
			undoBuffer.finishOperation()
		}

        if ( component != null ){
            selectModel.setSelection(component)
		}
		e.setHandled(true)
	}
}
