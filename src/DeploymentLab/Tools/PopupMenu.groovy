package DeploymentLab.Tools

import DeploymentLab.ComponentEditor.ComponentEditor
import DeploymentLab.Dialogs.ChangeParents.ChangeParentsController
import DeploymentLab.Dialogs.ChangeParents.ParentCategory
import DeploymentLab.Dialogs.ViewOptionDialog
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentRole
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.LogicalGroupController
import DeploymentLab.SceneGraph.DeploymentNode
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.SceneGraph.StaticChildrenNode
import DeploymentLab.SceneGraph.StaticChildrenNode.StaticChildNode
import DeploymentLab.channellogger.Logger
import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.event.PBasicInputEventHandler
import edu.umd.cs.piccolo.event.PInputEvent
import groovy.xml.MarkupBuilderHelper

import java.awt.Component
import java.awt.Cursor
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.awt.geom.Point2D
import java.awt.geom.Rectangle2D
import javax.swing.JFrame
import javax.swing.JMenu
import javax.swing.JMenuItem
import javax.swing.JPopupMenu

import DeploymentLab.*
import DeploymentLab.Model.Conducer
import DeploymentLab.Dialogs.FindReplace.FindReplaceController
import DeploymentLab.Model.DLProxy
import DeploymentLab.Transfer.TransferActionListener
import javax.swing.TransferHandler
import javax.swing.Action
import java.util.concurrent.CopyOnWriteArrayList

class PopupMenu extends PBasicInputEventHandler {
	private static final Logger log = Logger.getLogger(PopupMenu.name)

	private SelectModel selectModel
	private ToolManager toolManager
	private ComponentModel componentModel
	private UIDisplay uiDisplay
	private UndoBuffer undoBuffer
	private ConfigurationReplacer configurationReplacer
	private DeploymentPanel deploymentPanel
    private UserPreferences userPreferences
    private ViewOptionDialog viewOptionDialog

	PInputEvent currentIE
	private JFrame parent
	
	def saveActiveTools
	def saveActiveButton
	String activeAction


	PopupMenu(def dw, UndoBuffer ub, UIDisplay ud, ConfigurationReplacer cr, DeploymentPanel dp) {
		undoBuffer = ub
		selectModel = dw.selectModel
		toolManager = dw.toolManager
		componentModel = dw.componentModel
		configurationReplacer = cr
		uiDisplay = ud
		deploymentPanel = dp
        userPreferences = dw.userPreferences
        viewOptionDialog = dw.viewOptionDialog
	}

	public void setParent( JFrame _parent ){
		parent = _parent
	}

	private static DLComponent getSingleSelection(PInputEvent e, SelectModel selectModel) {
		DLComponent component = null
		if (e != null) {
			component = e.getPickedNode().getComponent()
		} else if (selectModel.getExpandedSelection().size() == 1) {
			List<DLComponent> selected = selectModel.getExpandedSelection()
			component = selected.get(0)
		}

		return component
	}

	public void mouseClicked(PInputEvent e) {
		super.mouseClicked(e)

		if(activeAction != null) {
			toolManager.getTool(activeAction).mouseClicked(e)
			if(e.isHandled())
				deactivate()
			return
		}

		if(!e.isRightMouseButton())
			return

		PNode node = e.getPickedNode()

		if(node instanceof DeploymentNode) {
			DLComponent component = node.getComponent()
			
			if(! (component in selectModel.getExpandedSelection()))
				selectModel.setSelection([component])

			currentIE = e

			parent.setCursor( new Cursor( Cursor.WAIT_CURSOR ) )

			this.showPopupOptionsMenu(e, selectModel.getExpandedSelection().size())
			parent.setCursor(null)
		} else {
			selectModel.setSelection([])
		}
		e.setHandled(true)
	}

	public static JMenuItem makeMenuItem (String label, Closure action) {
		JMenuItem item = new JMenuItem (label)
		item.addActionListener(action as ActionListener)
		return item
	}

	private void setRotateMenu(JPopupMenu parentMenu, PInputEvent e){
		DLComponent component = PopupMenu.getSingleSelection(e, selectModel)
		if (component == null) {
			return
		}

		if(component.hasRole('rotatable')){
			JMenuItem rotateMenuItem = makeMenuItem ("Rotate", this.&actionPerformed)
			uiDisplay.setDisplay('rotate', rotateMenuItem)
			parentMenu.add (rotateMenuItem )
		}
	}

	private void setRotate2Menu(JPopupMenu parentMenu, PInputEvent e){
		JMenuItem rotateMenuItem = makeMenuItem ("Rotate Selection around Center...", this.&rotateAroundCenter)
		uiDisplay.setDisplay('rotate', rotateMenuItem)
		parentMenu.add (rotateMenuItem )
	}

	private void rotateAroundCenter(ActionEvent e){
		((NodeGroupRotator)toolManager.getTool("rotator2")).rotateFromDialogBox()
	}
	
	private void setAssociateMenu(JPopupMenu parentMenu, PInputEvent e){
		DLComponent component = PopupMenu.getSingleSelection(e, selectModel)
		if (component == null) {
			return
		}

		if(component.isAssociatiable()){
			JMenuItem associateMenuItem = makeMenuItem ("Associate", this.&actionPerformed) 
			uiDisplay.setDisplay('associate', associateMenuItem)
			parentMenu.add(associateMenuItem)
		}
	}
	
	private void setMacAssignMenu(JPopupMenu parentMenu, PInputEvent e){
		DLComponent component = PopupMenu.getSingleSelection(e, selectModel)
		if (component == null) {
			return
		}

		if(component.listMacIdProperties().size()>0){
			JMenuItem macAssignMenuItem = makeMenuItem ("Mac Assign", this.&actionPerformed)
			uiDisplay.setDisplay('macassign', macAssignMenuItem)
			parentMenu.add (macAssignMenuItem)
		}
	}

    private void setCopySettingsMenu(JPopupMenu parentMenu, PInputEvent e){
        // No condition to check. Show for all component types.
        JMenuItem copySettingsMenuItem = makeMenuItem (CentralCatalogue.getUIS('copySettings.name'), this.&actionPerformed)
        uiDisplay.setDisplay('copySettings', copySettingsMenuItem)
        parentMenu.add (copySettingsMenuItem)
    }

	private void setCopyPasteMenu( JPopupMenu parentMenu, PInputEvent e ) {
		JMenuItem copyItem = new JMenuItem( CentralCatalogue.getUIS('menu.component.copy.label') )
		uiDisplay.setDisplay('copySettings', copyItem)
		parentMenu.add( copyItem )

		// Use the transfer handler instead of the typical action handler.
		copyItem.setActionCommand( (String)TransferHandler.getCopyAction().getValue(Action.NAME) )
		copyItem.addActionListener( new TransferActionListener(deploymentPanel.getCanvas()) )

		// No paste since right now we only get a context menu when a component is selected & paste doesn't make sense in that case.
	}

    private void setProxyMenu( JPopupMenu parentMenu, PInputEvent e ) {

	    List<DLComponent> selected = selectModel.getExpandedSelection()

        // Only show if there is a selected component that is proxyable.
        for( DLComponent component : selected ) {
            if( DLProxy.canProxy( component ) ) {
                JMenuItem menuItem = makeMenuItem( CentralCatalogue.getUIS('menu.component.newProxy.label'), this.&makeProxy )
                uiDisplay.setDisplay('addComponent', menuItem )
                parentMenu.add( menuItem )
                break
            }
        }

	    if( ( selected.size() == 1 ) && selected.get(0).hasRole("proxy") ) {
		    JMenuItem menuItem = makeMenuItem( CentralCatalogue.getUIS('menu.component.goToAnchor.label'), this.&locateAnchor )
		    parentMenu.add( menuItem )
	    }
    }

    private void makeProxy(ActionEvent e){
        toolManager.getTool("proxyadder").addProxy( selectModel.getExpandedSelection() )
    }

	private void locateAnchor(ActionEvent e) {
		// Should only get called when exactly one proxy is selected, but be safe anyway.
		List<DLComponent> selectedList = selectModel.getExpandedSelection()

		if( ( selectedList.size() == 1 ) && selectedList.get(0).hasRole("proxy") ) {
			DLProxy proxy = (DLProxy) selectedList.get(0)
			DLComponent anchor = proxy.getAnchor()

			// set active drawings first to get drawings
			selectModel.setActiveDrawing( anchor.getDrawing() )
			selectModel.setSelection( anchor )
			deploymentPanel.scrollToView( anchor )
		}
	}

	public void setMoveToDatasourceMenu(JPopupMenu parentMenu, boolean isMultiple){
		JMenuItem dsItem = makeMenuItem ("Change Data Source", this.&changeDataSource)
		uiDisplay.setDisplay('moveToDataSource', dsItem)
		parentMenu.add(dsItem)
	}

	private void changeDataSource(ActionEvent e){
		def cpd = new ChangeParentsController(selectModel.getExpandedSelection(), ParentCategory.NETWORK, undoBuffer,selectModel)
		cpd.showDialog(parent)
	}

	public void setMoveToGroupMenu(JPopupMenu parentMenu, boolean isMultiple){
        // a child component cannot move individually. should move with parent and other siblings
        if(!isMultiple){
            DLComponent component = selectModel.getSelectedObjects().get(0)
            if(component.isStaticOrDynamicChild())
                return
        }

		JMenuItem dsItem = makeMenuItem ("Change Grouping", this.&changeGrouping)
		uiDisplay.setDisplay('moveToZone', dsItem)
		parentMenu.add(dsItem)
	}

	private void changeGrouping(ActionEvent e){
		def cpd = new ChangeParentsController(selectModel.getExpandedSelection(), ParentCategory.GROUPING, undoBuffer,selectModel)
		cpd.showDialog(parent)
	}

	public void setRemoveFromGroupMenu(JPopupMenu parentMenu){

		// Go ahead and show it if at least one is in a logical group even though the action won't apply to all.
		for( DLComponent c : selectModel.getSelectedObjects() ) {
			if( c.hasParentOfRole( ComponentRole.LOGICAL_GROUP ) ) {
				JMenuItem menuItem = makeMenuItem( CentralCatalogue.getUIS('menu.component.unchildLogicalGroup.label'), this.&unchildLogicalGroup )
				uiDisplay.setDisplay('moveToZone', menuItem )
				parentMenu.add( menuItem )
				break
			}
		}
	}

	private void unchildLogicalGroup(ActionEvent e){
		LogicalGroupController.removeFromGroup( selectModel.getSelectedObjects() )
	}

    public void setMoveToDrawingMenu(JPopupMenu parentMenu){
	    if( componentModel.getComponentsByType('drawing').size() > 1 ) {
		    JMenuItem dsItem = makeMenuItem(CentralCatalogue.getUIS("menu.component.moveToDrawing.label"), this.&moveSelectionToDrawing)
		    uiDisplay.setDisplay('moveToDrawing', dsItem)
		    parentMenu.add(dsItem)
	    }
    }

	// This is a copy of what is also in DeploymentLabWindow. Should refactor so that a common impl is used.
	private void moveSelectionToDrawing(ActionEvent e){
		ProgressManager.doTask {
			log.info(CentralCatalogue.getUIS("progress.drawing.moveDrawing"), "progress")
			try {
				undoBuffer.startOperation()
				def fb = new DrawingBuilder(componentModel,userPreferences, toolManager.getTool("proxyadder"), selectModel)
				NodeMover nm = new NodeMover(undoBuffer, deploymentPanel, selectModel)
				fb.moveSelectionToAnotherDrawing(selectModel.getExpandedSelection(),nm)
			}catch(Exception ex){
				log.error(log.getStackTrace(ex))
				log.error(CentralCatalogue.getUIS("drawing.new.genericerror"), "message")
				undoBuffer.rollbackOperation()
			}finally{
				undoBuffer.finishOperation()
			}
		}
	}

    public void setViewFilterMenu(JPopupMenu parentMenu){
        JMenu viewFilterMenu = new JMenu(CentralCatalogue.getUIS("menu.view.context.lable"))
        viewFilterMenu.add(makeMenuItem(CentralCatalogue.getUIS("menu.view.context.item.only"), {viewOptionDialog.showSelectedTypes()}))
        viewFilterMenu.add(makeMenuItem(CentralCatalogue.getUIS("menu.view.context.item.assoc"), {viewOptionDialog.showSelectedTypes(true)}))
        viewFilterMenu.add(makeMenuItem(CentralCatalogue.getUIS("menu.view.context.item.all"), {viewOptionDialog.toggleAllTypes(true)}))
        uiDisplay.setDisplay('viewOptions', viewFilterMenu)
        parentMenu.add(viewFilterMenu)
    }

	public void setReplaceConfigurationMenu(JPopupMenu parentMenu){
        // child component is not replacable
        if(selectModel.getSelectedObjects().size()==1){
            DLComponent component = selectModel.getSelectedObjects().get(0)
            if(component.isStaticOrDynamicChild())
                return
        }

		JMenuItem configMenu = makeMenuItem("Replace Configuration", {ae -> this.openConfigReplacer(ae)})
		uiDisplay.setDisplay('replaceConfiguration', configMenu)
		parentMenu.add(configMenu)
	}
	
	public void setFindReplaceMenu(JPopupMenu parentMenu){
		JMenuItem findReplaceMenu = makeMenuItem("Find/Replace", {ae -> this.openFindReplacer(ae)})
		uiDisplay.setDisplay('findReplace', findReplaceMenu)
		parentMenu.add(findReplaceMenu)
	}
	
	public void setAlignerMenu(JPopupMenu parentMenu){
		JMenu alignMenu = new JMenu("Align")
		alignMenu.add(makeMenuItem("Align Left", this.&aligner))
		alignMenu.add(makeMenuItem("Align Right", this.&aligner))
		alignMenu.add(makeMenuItem("Align Top", this.&aligner))
		alignMenu.add(makeMenuItem("Align Bottom", this.&aligner))

		parentMenu.add (alignMenu)
	}


	public void setDistributeMenu(JPopupMenu parentMenu){
		JMenu alignMenu = new JMenu("Distribute")
		alignMenu.add(makeMenuItem("Distribute Vertical", this.&distribute))
		alignMenu.add(makeMenuItem("Distribute Horizontal", this.&distribute))
		parentMenu.add (alignMenu)
	}


	public void setOrderMenu(JPopupMenu parentMenu){
		JMenu alignMenu = new JMenu("Order")
		alignMenu.add(makeMenuItem("Bring To Front", this.&moveToFront))
		alignMenu.add(makeMenuItem("Bring Forward", this.&moveForward))
		alignMenu.add(makeMenuItem("Send Backward", this.&moveBackward))
		alignMenu.add(makeMenuItem("Send to Back", this.&moveToBack))

		parentMenu.add (alignMenu)
	}

	/*
	//a sketch for having a paste option in a context menu
	private void setPasteHereMenu(JPopupMenu parentMenu){
		//TransferActionListener actionListener = new TransferActionListener();
		JMenuItem pasteItem = new JMenuItem ("Paste Here")
		pasteItem.setActionCommand((String)TransferHandler.getPasteAction().getValue(Action.NAME));
        pasteItem.addActionListener( CentralCatalogue.getInstance().getActionListener() );
		uiDisplay.setDisplay('clone', pasteItem)
		parentMenu.add(pasteItem)
	}
	*/

	/**
	 * high-level only options: only gets drawn if we're at a high-enough level
	 */
	public void setHighLevelMenu(JPopupMenu parentMenu, PInputEvent e){

			//configure
		JMenuItem configMenuItem = makeMenuItem ("Edit Configuration", this.&editConfiguration)
		parentMenu.add (configMenuItem)
		if ( e ){
			uiDisplay.setDisplay('componentEditor', configMenuItem)
		}

		//separator before the technician-mode items
		if (uiDisplay.isVisible("getInfo")){
			parentMenu.addSeparator();
		}

		JMenuItem specsItem = makeMenuItem ("Component Specs", this.&componentSpecs)
		parentMenu.add (specsItem)
		if ( e ) {
			uiDisplay.setDisplay('getInfo', specsItem)
		}

		JMenuItem propsItem = makeMenuItem ("Component Props", this.&componentProps)
		parentMenu.add (propsItem)
		if ( e ) {
			uiDisplay.setDisplay('getInfo', propsItem)
		}

		JMenuItem xmlItem = makeMenuItem("Component XML", this.&componentXML)
		parentMenu.add(xmlItem)
		if ( e ) {
			uiDisplay.setDisplay('getInfo', xmlItem)
		}

		JMenuItem protoItem = makeMenuItem("Make a Component Proto", this.&componentProto)
		parentMenu.add(protoItem)
		if ( e ) {
			uiDisplay.setDisplay('getInfo', protoItem)
		}

		parentMenu.addSeparator();

		JMenuItem opropsItem = makeMenuItem ("Object Props", this.&objectProps)
		parentMenu.add (opropsItem)
		if ( e ) {
			uiDisplay.setDisplay('getInfo', opropsItem)
		}

		JMenuItem oxmlItem = makeMenuItem("Object XML", this.&objectXML)
		parentMenu.add(oxmlItem)
		if ( e ) {
			uiDisplay.setDisplay('getInfo', oxmlItem)
		}

		parentMenu.addSeparator();

		if ( e ) {
			JMenuItem nItem = makeMenuItem ("Show PNodes", { this.showPNodes(e) } )
			parentMenu.add (nItem)
			uiDisplay.setDisplay('getInfo', nItem)

			JMenuItem transItem = makeMenuItem ("Show Transform", { this.showTransform(e) } )
			parentMenu.add (transItem)
			uiDisplay.setDisplay('getInfo', transItem)
		}

		JMenuItem familyItem = makeMenuItem ("Show Children and Parents", {this.showFamily(e)} )
		parentMenu.add (familyItem)
		if ( e ) {
			uiDisplay.setDisplay('getInfo', familyItem)
		}

		JMenuItem zItem = makeMenuItem ("Show Z-Index", {this.showZIndex()} )
		uiDisplay.setDisplay('getInfo', zItem)
		parentMenu.add (zItem)
	}


	private void showPopupOptionsMenu(PInputEvent e, int selectedCount) {
		//create menu item
		JPopupMenu optionsMenu = new JPopupMenu ("Options")

		if (selectedCount == 1) {
			// See if the node has any node-specific items.
			e.getPickedNode().buildContextMenu(optionsMenu, e)
		}

		this.addCommonOptionsMenuItems(optionsMenu, e, selectedCount)

		optionsMenu.show(true)
		optionsMenu.show((Component) e.getComponent(), (int)e.getCanvasPosition().getX(), (int)e.getCanvasPosition().getY());
	}

	/**
	 * addCommonOptionsMenuItems
	 *
	 * Used to populate an options menu with items that are common to single & multiple selection, as
	 * well as the Groupings tree.
	 *
	 * @param optionsMenu The menu to add the items to
	 * @param e The Input event, will be null if called by the groupings tree.
	 * @param selectedCount The number of placeable items selected.
	 */
	public void addCommonOptionsMenuItems(JPopupMenu optionsMenu, PInputEvent e, int selectedCount) {
		//rotate, associate, and mac assign all need events, so we don't show them here
		//...but, can I interest you in the rest of the menu?

		if (e != null) {
			// rotate,rotatable
			setRotateMenu(optionsMenu, e)

			//associate, only for associate-able component
			setAssociateMenu(optionsMenu, e)

			if (selectedCount == 1) {
				// mac assign, only for components has mac id
				setMacAssignMenu(optionsMenu, e)

				// copy settings
				setCopySettingsMenu(optionsMenu, e)
			}

			optionsMenu.addSeparator()
		}

		setCopyPasteMenu( optionsMenu, e )

		//clone
		//setCloneMenu(optionsMenu, e)

		// proxy
		setProxyMenu( optionsMenu, e )

		//move to another drawing
		setMoveToDrawingMenu(optionsMenu)

		if (selectedCount > 0) {
			// assign to data source
			setMoveToDatasourceMenu(optionsMenu, false)

			//assign to group
			setMoveToGroupMenu(optionsMenu, false)

			//remove from planning group
			setRemoveFromGroupMenu( optionsMenu )
		}

		optionsMenu.addSeparator()

		if (selectedCount == 1) {
			DLComponent component = PopupMenu.getSingleSelection(e, selectModel)
			if (component.hasProperty("manualWSNConfig")) {
				if (setWSNAssignMenu(optionsMenu, false)) {
					optionsMenu.addSeparator()
				}
			}
		} else {
			boolean hasWSNComponents = false
			for (def it : selectModel.getSelectedObjects()) {
				if (it.hasProperty("manualWSNConfig")) {
					hasWSNComponents = true
					break; //no need to keep going
				}
			}

			if (hasWSNComponents) {
				if (setWSNAssignMenu(optionsMenu, true)) {
					optionsMenu.addSeparator()
				}
			}

			// aligner
			setAlignerMenu(optionsMenu)

			//distribute
			setDistributeMenu(optionsMenu)
		}

		//show/hide
		setViewFilterMenu(optionsMenu)

		//order
		setOrderMenu(optionsMenu)

		//add separator between popup-only items and items in both right-click menus
		optionsMenu.addSeparator()

		// replace Configuration
		// child component of row lite can not be replace with another component type
		setReplaceConfigurationMenu(optionsMenu)

		// find/replace
		setFindReplaceMenu(optionsMenu)

		if (selectedCount == 1) {
			setHighLevelMenu(optionsMenu, e)
		}
	}

	public boolean setWSNAssignMenu(JPopupMenu parentMenu, boolean isMultiple) {

		// Don't show these at all if there are no REINS
		if (componentModel.getComponentsByType('network').isEmpty()) {
			return false
		}

		JMenuItem autoAssignMenu = makeMenuItem("Automatically Assign To WSN", { ev -> this.setAutoWSNConfiguration() })
		uiDisplay.setDisplay('autoMode', autoAssignMenu)

		JMenuItem manualAssignMenu = makeMenuItem("Manually Assign To WSN", this.&changeWSNNetwork)
		uiDisplay.setDisplay('manualMode', manualAssignMenu)

		if (isMultiple) {
			parentMenu.add(autoAssignMenu)
			parentMenu.add(manualAssignMenu)
		} else {
			DLComponent component = selectModel.getSelectedObjects().get(0)

			// if node is manual config mode, show "automatically assign to region"
			if ((Boolean) component.getPropertyValue("manualWSNConfig")) {
				parentMenu.add(autoAssignMenu)
			}
			parentMenu.add(manualAssignMenu)
		}

		return true
	}

	private void setAutoWSNConfiguration(){
		CopyOnWriteArrayList<DLComponent> selections = selectModel.getSelectedObjects()

		selections.each { it ->
			if (it.hasProperty("manualWSNConfig")) {
				it.setPropertyValue('manualWSNConfig', false)
			}
		}
		log.info(CentralCatalogue.getUIS("changeParents.WSNToAuto"), "statusbar")
	}

	private void changeWSNNetwork(ActionEvent e) {
		def cpd = new ChangeParentsController(selectModel.getExpandedSelection(), ParentCategory.NETWORK, undoBuffer, selectModel)
		cpd.showDialog(parent)
	}

	/*
	//a sketch for having a context menu on empty space
	private void showEmptySpacePopupMenu(PInputEvent e){
		JPopupMenu fNoNodeOptionMenu = new JPopupMenu ("Options")
		setPasteHereMenu(fNoNodeOptionMenu)
		fNoNodeOptionMenu.show(true)
		fNoNodeOptionMenu.show((Component) e.getComponent(), (int)e.getCanvasPosition().getX(), (int)e.getCanvasPosition().getY());
	}
	*/


    /**
     * get StaticChildrenData from HashMap
     * @param staticChildrenDataMap
     * @param c
     * @return
     */
    private StaticChildrenDistributionData getStaticChildrenData(HashMap<DLComponent,StaticChildrenDistributionData> staticChildrenDataMap, DLComponent c){
        StaticChildrenDistributionData distributionData = staticChildrenDataMap.get(c)
        if(staticChildrenDataMap.get(c)==null){
            distributionData = new StaticChildrenDistributionData(deploymentPanel.getPNodeFor(c))
            staticChildrenDataMap.put(c,distributionData)
        }
       return distributionData;
    }

    /**
     * Align statciChildrenNodes
     * @param staticChildrenDataMap
     * @param option  (l: left, r:right, b:bottom, t:top)
     */
    private void alignStaticChildrenNodes(HashMap<DLComponent,StaticChildrenDistributionData> staticChildrenDataMap, String option){
        for(DLComponent sComponent :staticChildrenDataMap.keySet()){
            log.trace("staticChildNodeDataMap: " +  sComponent.toString())
            StaticChildrenDistributionData distributionData = staticChildrenDataMap.get(sComponent)
            PNode representative = distributionData.getRepresentative(option)
            log.trace("representative" + representative.getComponent().toString())
            Point2D.Double p = distributionData.getSelectedNodePosition(representative)
			//log.trace("point p = $p")

            if(representative instanceof StaticChildNode){
                //offsetByCoord
                if(option.equals("l") || option.equals("r"))
                    ((StaticChildNode)representative).offsetByCoord("x", p)
                else if(option.equals("t") || option.equals("b"))
                    ((StaticChildNode)representative).offsetByCoord("y", p)


            }else if(representative instanceof StaticChildrenNode){
                if(option.equals("l") || option.equals("r"))
                    representative.getComponent().setPropertyValue("x", p.getX())
                else if(option.equals("t") || option.equals("b"))
                    representative.getComponent().setPropertyValue("y", p.getY())
           }
        }
    }


	public void aligner(ActionEvent e) {
		String option = e.getActionCommand()
		def selection = selectModel.getExpandedSelection().findAll{it -> it.hasClass('placeable')}

		if ( selection.size() == 0 ) { return; }

		def comp = selection.first()
		Rectangle2D.Double rect = new Rectangle2D.Double((double)comp.getPropertyValue('x'), (double)comp.getPropertyValue('y'), 0.0, 0.0)
		selection.tail().each{ component ->
			double x = component.getPropertyValue('x')
			double y = component.getPropertyValue('y')
			rect.add(x, y)
		}

		try {
			undoBuffer.startOperation()
			// initialize statciChildrenNode data
            HashMap<DLComponent,StaticChildrenDistributionData> staticChildreNodeDataMap = new HashMap<DLComponent, StaticChildrenDistributionData>();

            if(option.equals("Align Left")) {
                log.trace("align left x:" + rect.x)
                selection.each{ c ->
                    // staticchild should move by changing offset value
                    def node = deploymentPanel.getPNodeFor(c)
                    if(node instanceof StaticChildNode){  // Don't align staticChild Node at this point , just keep new location in hashmap to align later
                        // set selectedNodePosition to get representative later
                        StaticChildrenDistributionData distributionData = getStaticChildrenData(staticChildreNodeDataMap, node.getParentComponent())
                        distributionData.setSelectedNodePosition(node,new Point2D.Double( rect.x, c.getPropertyValue("y")))
                    } else if(node instanceof StaticChildrenNode){
                        StaticChildrenDistributionData distributionData = getStaticChildrenData(staticChildreNodeDataMap,node.getComponent())
                        distributionData.setSelectedNodePosition(node,new Point2D.Double( rect.x, c.getPropertyValue("y")))
                    } else{
                        c.setPropertyValue('x', rect.x)
                    }
				}

                // align staticchild or staticChildrenNode only
                // first get representative node among selection for each rowlite or horizontal row
                // only align representative node  because more than 2 nodes in same staticChildren object is meaningless for alignment
                alignStaticChildrenNodes(staticChildreNodeDataMap, "l")
			} else if(option.equals("Align Right")) {
                double newX = rect.x + rect.width
				log.trace("align right x:" + newX)
                selection.each{ c ->
                   // staticchild should move by changing offset value
                    def node = deploymentPanel.getPNodeFor(c)
                    if(node instanceof StaticChildNode){  // Don't align staticChild Node at this point , just keep new location in hashmap to align later
                        // set selectedNodePosition to get representative later
                        StaticChildrenDistributionData distributionData = getStaticChildrenData(staticChildreNodeDataMap, node.getParentComponent())
                        distributionData.setSelectedNodePosition(node,new Point2D.Double(rect.x + rect.width, c.getPropertyValue("y")))
                    } else if(node instanceof StaticChildrenNode){
                        StaticChildrenDistributionData distributionData = getStaticChildrenData(staticChildreNodeDataMap,node.getComponent())
                        distributionData.setSelectedNodePosition(node,new Point2D.Double( rect.x + rect.width, c.getPropertyValue("y")))
                    } else{
                        c.setPropertyValue('x', rect.x + rect.width)
                    }
				}

                // align staticchild or staticChildrenNode only
                // first get representative node among selection for each rowlite or horizontal row
                // only align representative node  because more than 2 nodes in same staticChildren object is meaningless for alignment
                alignStaticChildrenNodes(staticChildreNodeDataMap, "r")
			} else if(option.equals("Align Top")) {
				log.trace("align top y:" + rect.y)

                selection.each{ c ->
                   // staticchild should move by changing offset value
                    def node = deploymentPanel.getPNodeFor(c)

                    if(node instanceof StaticChildNode){  // Don't align staticChild Node at this point , just keep new location in hashmap to align later
                        // set selectedNodePosition to get representative later
                        StaticChildrenDistributionData distributionData = getStaticChildrenData(staticChildreNodeDataMap, node.getParentComponent())
                        distributionData.setSelectedNodePosition(node,new Point2D.Double(c.getPropertyValue("x"),rect.y))
                    } else if(node instanceof StaticChildrenNode){
                        StaticChildrenDistributionData distributionData = getStaticChildrenData(staticChildreNodeDataMap,node.getComponent())
                        distributionData.setSelectedNodePosition(node,new Point2D.Double(c.getPropertyValue("x"),rect.y))
                    } else{
                        c.setPropertyValue('y', rect.y)
                    }
				}

                // align staticchild or staticChildrenNode only
                // first get representative node among selection for each rowlite or horizontal row
                // only align representative node  because more than 2 nodes in same staticChildren object is meaningless for alignment
                alignStaticChildrenNodes(staticChildreNodeDataMap, "t")

			} else if(option.equals("Align Bottom")) {
                double newY = rect.y + rect.height
                log.trace("align bottom y:" + newY)

				selection.each{ c ->
                   // staticchild should move by changing offset value
                    def node = deploymentPanel.getPNodeFor(c)
                    if(node instanceof StaticChildNode){  // Don't align staticChild Node at this point , just keep new location in hashmap to align later
                        // set selectedNodePosition to get representative later
                        StaticChildrenDistributionData distributionData = getStaticChildrenData(staticChildreNodeDataMap, node.getParentComponent())
                        distributionData.setSelectedNodePosition(node,new Point2D.Double(c.getPropertyValue("x"),rect.y + rect.height))
                    } else if(node instanceof StaticChildrenNode){
                        StaticChildrenDistributionData distributionData = getStaticChildrenData(staticChildreNodeDataMap,node.getComponent())
                        distributionData.setSelectedNodePosition(node,new Point2D.Double(c.getPropertyValue("x"),rect.y + rect.height))
                    } else{
                        c.setPropertyValue('y', rect.y + rect.height)
                    }
				}

                // align staticchild or staticChildrenNode only
                // first get representative node among selection for each rowlite or horizontal row
                // only align representative node  because more than 2 nodes in same staticChildren object is meaningless for alignment
                alignStaticChildrenNodes(staticChildreNodeDataMap, "b")
			}
		} catch (Exception ex) {
			log.error("Error while Aligning Components!", "message")
			log.error(log.getStackTrace(ex))
			undoBuffer.rollbackOperation()
		} finally {
			undoBuffer.finishOperation()
		}
	}


	public void distribute(ActionEvent e) {
		String option = e.getActionCommand()
		if(option.equals("Distribute Vertical")) {
			this.distributeVertical()
		} else {
			this.distributeHorizontal()
		}
	}

	public void distributeHorizontal(){
		List<DLComponent> selection = selectModel.getExpandedSelection().findAll{it -> it.hasRole('placeable') }

        if ( selection.size() == 0 ) { return; }
		try{
		    undoBuffer.startOperation()
			//horizontal
			selection.sort { it.getPropertyValue("x") }
			Double left = (Double) selection.first().getPropertyValue("x")
			Double right = (Double) selection.last().getPropertyValue("x")
			double totalWidth = right - left
			double spacing = totalWidth / (selection.size() - 1)

            // initialize statciChildrenNode data
            HashMap<DLComponent,StaticChildrenDistributionData> staticChildreNodeDataMap = new HashMap<DLComponent, StaticChildrenDistributionData>();
            //selection.findAll {it->it.hasRole("staticchildplaceable")}.each{scn-> staticChildreNodeDataMap.put(scn, new StaticChildrenDistributionData(deploymentPanel.getPNodeFor(scn)))}

			for ( int i = 0; i < selection.size(); i ++ ){
                DLComponent c = selection[i]
                def node = deploymentPanel.getPNodeFor(c)
                if(node instanceof StaticChildNode){
                    //log.trace("staticchildNode distributeHorizontal")
                    DLComponent parentComponent = node.getParentComponent()
                    StaticChildrenDistributionData distributionData = staticChildreNodeDataMap.get(parentComponent)
                    if(staticChildreNodeDataMap.get(parentComponent)==null){
                        distributionData = new StaticChildrenDistributionData(deploymentPanel.getPNodeFor(parentComponent))
                        staticChildreNodeDataMap.put(parentComponent,distributionData)
                    }
                    distributionData.setChildPosition((StaticChildNode)node,new Point2D.Double((left + (spacing * i)), c.getPropertyValue("y")))
                } else{
                    selection[i].setPropertyValue("x", (left + (spacing * i)) )
                }

                //log.trace(c.getPropertyStringValue("name") +   " offset to " +  (left + (spacing * i))) ;
			}

            // distribute staticChildNode
            for(DLComponent sComponent :staticChildreNodeDataMap.keySet()){
                StaticChildrenDistributionData distributionData = staticChildreNodeDataMap.get(sComponent)
                StaticChildrenNode snode = distributionData.getParentNode()
                snode.distributeChildren(distributionData.getChildrenPosition(),"h")
            }

		} catch ( Exception ex ){
			undoBuffer.rollbackOperation()
			log.error("Error while attempting to Distribute: ${ex.getMessage()}", "message" )
			log.error( log.getStackTrace(ex), "default" )
		} finally {
			undoBuffer.finishOperation()
		}
	}

	public void distributeVertical(){
		List<DLComponent> selection = selectModel.getExpandedSelection().findAll{it -> it.hasRole('placeable') }

        if ( selection.size() == 0 ) { return; }

        //log.trace("start distributeVertical:" + selection.size())
		try{
		    undoBuffer.startOperation()
			//all we care about is y
			selection.sort { it.getPropertyValue("y") }
			Double top = (Double) selection.first().getPropertyValue("y")
			Double bottom = (Double) selection.last().getPropertyValue("y")
			double totalHeight = bottom - top
			double spacing = totalHeight / (selection.size() - 1 )

            // initialize statciChildrenNode data
            HashMap<DLComponent,StaticChildrenDistributionData> staticChildreNodeDataMap = new HashMap<DLComponent, StaticChildrenDistributionData>();
            //selection.findAll {it->it.hasRole("staticchildplaceable")}.each{scn-> staticChildreNodeDataMap.put(scn, new StaticChildrenDistributionData(deploymentPanel.getPNodeFor(scn)))}

			for ( int i = 0; i < selection.size(); i ++ ){

                DLComponent c = selection[i]
                //log.trace(c.toString())
                //log.trace("new vertical point: " + c.getPropertyValue("x") + " , "  + (top + (spacing * i)) )
                def node = deploymentPanel.getPNodeFor(c)
                // static child node should move later after parent node moving
                if(node instanceof StaticChildNode){      // staticchild component should move following guidline ( use offset property)
                    DLComponent parentComponent = node.getParentComponent()
                    StaticChildrenDistributionData distributionData = staticChildreNodeDataMap.get(parentComponent)
                    if(staticChildreNodeDataMap.get(parentComponent)==null){
                        distributionData = new StaticChildrenDistributionData(deploymentPanel.getPNodeFor(parentComponent))
                        staticChildreNodeDataMap.put(parentComponent,distributionData)
                    }
                    distributionData.setChildPosition((StaticChildNode)node,new Point2D.Double(c.getPropertyValue("x"),  (top + (spacing * i))))

                    //staticChildSelection.put(((StaticChildNode)node), new Point2D.Double(c.getPropertyValue("x"),  (top + (spacing * i))))
                } else{
                    selection[i].setPropertyValue("y", (top + (spacing * i)) )

                }
                //log.trace("after changing vertical distribution:" + c.getPropertyValue("x") + ", " + c.getPropertyValue("y"))
			}

            // distribute staticChildNode
            for(DLComponent sComponent :staticChildreNodeDataMap.keySet()){
                StaticChildrenDistributionData distributionData = staticChildreNodeDataMap.get(sComponent)
                StaticChildrenNode snode = distributionData.getParentNode()
                snode.distributeChildren(distributionData.getChildrenPosition(), "v")
            }

		} catch ( Exception ex ){
			undoBuffer.rollbackOperation()
			log.error("Error while attempting to Distribute: ${ex.getMessage()}", "message" )
			log.error( log.getStackTrace(ex), "default" )
		} finally {
			undoBuffer.finishOperation()
		}
	}

     private HashMap sortByComparator(HashMap unsortedMap, String key){
		List list = new LinkedList(unsortedMap.entrySet());

		Collections.sort(list, new Comparator(){
			public int compare(Object o1, Object o2){
                if(key.equals("x")){
                    return ((Comparable) ((Map.Entry) (o1)).getValue().getX()).compareTo(((Map.Entry) (o2)).getValue().getX());
                }else{
                    return ((Comparable) ((Map.Entry) (o1)).getValue().getY()).compareTo(((Map.Entry) (o2)).getValue().getY());

                }

			}
		});



		HashMap sortedMap = new LinkedHashMap();

        for(int i=0;i<list.size()/2;i++){
            Map.Entry entry = list.get(i);
            sortedMap.put(entry.getKey(), entry.getValue())

            Map.Entry entry2 = list.get(list.size()-i-1)
            sortedMap.put(entry2.getKey(),entry2.getValue())
        }
        /*
		for(Iterator it = list.iterator(); it.hasNext();){
			Map.Entry entry = (Map.Entry)it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}*/

		return sortedMap;
	}

	public void actionPerformed(ActionEvent e) {
		String option = e.getActionCommand()
		e.getSource().getParent().show(false)
		
		def activeTools = toolManager.getActiveTools()
		saveActiveButton = toolManager.getActiveButton()
		toolManager.setActiveTools(['hover', 'panzoomer', 'popupmenu'])
		saveActiveTools = activeTools

		if(option == "Rotate")
			activeAction = 'rotator'
		if(option == "Rotate2")
			activeAction = 'rotator2'
		else if(option =="Associate")
			//activeAction = 'associator'
			activeAction = 'newassoc'
		else if(option == "Mac Assign")
			activeAction = 'macassigner'
        else if(option == CentralCatalogue.getUIS('copySettings.name'))
            activeAction = 'copysettings'

		log.trace("Setting active modeless tool to '$activeAction'")

		toolManager.getTool(activeAction).activate(false)
		currentIE.setHandled(false)
		toolManager.getTool(activeAction).mouseClicked(currentIE)
		if(currentIE.isHandled())
			deactivate()
	}

	private void editConfiguration(ActionEvent e){
		ComponentEditor.createEditor( selectModel.getExpandedSelection()[0] , parent).show()
	}

	private void moveToFront(ActionEvent e){
		deploymentPanel.moveToFront( selectModel.getExpandedSelection() )
	}

	private void moveToBack(ActionEvent e){
		deploymentPanel.moveToBack( selectModel.getExpandedSelection() )
	}

	private void moveForward(ActionEvent e){
		deploymentPanel.moveForward( selectModel.getExpandedSelection() )
	}

	private void moveBackward(ActionEvent e){
		deploymentPanel.moveBackward( selectModel.getExpandedSelection() )
	}

	private void showZIndex(){
		String message = ""
		selectModel.getExpandedSelection().each{ c->
			message += "${c.getName()} = ${deploymentPanel.getZIndex( deploymentPanel.getPNodeFor(c) )}"
			message += " :: "
			message += "${componentModel.getComponents().indexOf(c)}"
		}
		log.info(message, "statusbar")
	}

	private void componentSpecs(ActionEvent e){
		DLComponent c = selectModel.getExpandedSelection()[0]
		log.debug("showing specs for $c")
		String specs = ""
		specs += "Type: ${c.getType()}\n"
		specs += "Roles: ${c.listRoles()}\n"
		specs += "Grouppath: ${c.getGrouppath()}\n"
		specs += "Is Secondary: ${c.isSecondary()}\n"
		specs += "UID: ${c.getDlid()}\n"
		specs += "Is a Proxy: ${c.hasRole('proxy')}\n"
		specs += "Is Exportable: ${c.isExportable()}\n"
		specs += "\n"
		specs += "Badges:\n"
		c.getBadges().each { k, v ->
			specs += "$k: $v\n"
		}
		specs += "\n"
		specs += "Shape: ${c.getShape()}\n"
		specs += "Name: ${c.getName()}\n"
		specs += "Drawing: ${c.getDrawing()}\n"
		specs += "\n\n"

		String prods = ""
		c.listProducers().each{ pn->
		    def p = c.getProducer(pn)
			prods += "Producer: $pn \n"
			prods += "Type: ${p.getDatatype()} \n"
			prods += "Exclusive: ${p.isExclusive()} \n"
			prods += "Links: "
			p.listConsumerComponents().each{ pair ->
				prods += pair.a.getName() + ", "
			}
			prods += "\n--\n"
		}
		specs += prods + "\n"
		String cons = ""
		c.listConsumers().each{ cn->
		    def con = c.getConsumer(cn)
			cons += "Consumer: $cn \n"
			cons += "Type: ${con.getDatatype()} \n"
			cons += "Collection: ${con.isCollection()} \n"
			cons += "Links: "
			con.listProducerComponents().each{ pair ->
				cons += pair.a.getName() + ", "
			}
			cons += "\n--\n"
		}
		specs += cons

		String conducers = ""
		c.getAllConducers().each{ cd ->
			
			conducers += "Conducer: $cd \n"

			conducers += "=Prosumers: "
            for( Conducer prosumer : cd.getProsumers() ) {
                conducers += "$prosumer, "
            }
            conducers += "\n"
			
			conducers += "==Producer: ${cd.getProducer()} \n"
			conducers += "===Links: "
			cd.getProducer().listConsumerComponents().each{ pair ->
				conducers += pair.a.getName() + ", "
			}
			conducers += "\n"
			conducers += "==Consumer: ${cd.getConsumer()} \n"
			conducers += "===Links: "
			cd.getConsumer().listProducerComponents().each{ pair ->
				conducers += pair.a.getName() + ", "
			}
			conducers += "\n"
			conducers += "\n--\n"
		}
		specs += conducers
		/*
		c.getManagedObjects().each{ o->
		    specs += " \n\n$o \n"
			o.getProperties().each { op ->
				specs += " --- $op \n"
			}
		}
		*/

		specs += "\nManaged Objects: \n"
		c.getManagedObjects().each{ o->
		    specs += "$o == ${o.getName()}\n"
		}

		specs += "\n_objects: \n"
		c.getRoots().each{ o->
		    specs += "$o \n"
		}

		//lets get some x & y coords in here too:

		specs += "\nCoords: \n"
		specs += "Component: (${c.getPropertyStringValue("x")}, ${c.getPropertyStringValue("y")} )\n"
		c.getManagedObjects().each{ o->
			if ( o.hasObjectProperty("x") && o.hasObjectProperty("y") ){
				specs += " (${o.getObjectProperty("x").getValue()}, ${o.getObjectProperty("y").getValue()}) == ${o.getName()}\n"
			}
		}

		log.trace(specs, "message")
	}

	private void componentProps(ActionEvent e){
		DLComponent c = selectModel.getExpandedSelection()[0]
		String specs = ""
		c.listProperties().each{ p ->
			def prop = c.getComponentProperty(p)
			specs += "${prop.getName()} == ${prop.getValue()} \n"
		}
		log.trace(specs, "message")
	}



	private void objectProps(ActionEvent e){
		DLComponent c = selectModel.getExpandedSelection()[0]
		String specs = "Managed Objects: \n"
		c.getManagedObjects().each{ mo ->
			specs += "$mo ${mo.getName()} \n"
		}
		specs += "\n _objects: \n"
		c.getRoots().each{ mo ->
			specs += "$mo ${mo.getName()} \n"
			//mo.getChildren().each{ ch ->
			mo.getSortedChildren().each{ ch ->
				specs += " ---- $ch ${ch.getName()} \n"
			}

		}
		log.trace(specs, "message")
	}


	private void componentXML(ActionEvent e){
		DLComponent c = selectModel.getExpandedSelection()[0]
		String compXML
		def writer = new StringWriter()
		def builder = new groovy.xml.MarkupBuilder(writer)
		c.serialize(builder)
		compXML = writer.toString()
		log.trace(compXML, "message")
	}

	private void componentProto(ActionEvent e){
		DLComponent c = selectModel.getExpandedSelection()[0]
		String compXML
		def writer = new StringWriter()
		def builder = new groovy.xml.MarkupBuilder(writer)
		c.serializeAsPrototype(builder)
		compXML = writer.toString()
		log.trace(compXML, "message")
	}

	private void objectXML(ActionEvent e){
		DLComponent c = selectModel.getExpandedSelection()[0]
		String compXML
		def writer = new StringWriter()
		def builder = new groovy.xml.MarkupBuilder(writer)
		c.getManagedObjects().each{ o->
			def helper = new MarkupBuilderHelper( builder )
			//helper.comment("Object of type ${o.getType()}")
			helper.yield "\n\n"
			o.serialize(builder)
		}
		compXML = writer.toString()
		log.trace(compXML, "message")
	}


	private void showPNodes(PInputEvent e){
		PNode node = e.getPickedNode()
		log.trace( "\n" + printNodeInfo(node,0), "message" )
	}

	String printNodeInfo( PNode node, int level ) {
		def result = ""
		result += "${'    ' * level}* '${node.getName()}'  ${node.getClass().getName()}\n"
		result += "${'    ' * level}  ${node.getListenerList()}\n"
		level++
		node.getChildrenReference().each{ c ->
			result += printNodeInfo( c, level )
		}
		return result
	}


	private void showTransform(PInputEvent e){
		PNode node = e.getPickedNode()
		def result = ""
		result += "picked node is ${node.getClass().getName()} \n"
		result += node.getTransform().toString()
		log.trace( result, "message" )
	}

	private void showFamily(PInputEvent e){
		DLComponent c = selectModel.getExpandedSelection()[0]
		def result = ""
		result += "Parent Components are: \n"
		c.getParentComponents().each{
			result += " -- $it\n"
		}
		result += "\n"
		result += "Children Components are: \n"
		c.getChildComponents().each{
			result += " -- $it \n"
		}
        result += "\n"
        result += "Proxy Components are: \n"
        c.getProxies().each{
            result += " -- (${it.getDrawing().getName()}) $it \n"
        }
		log.trace( result, "message" )
	}

	private void openConfigReplacer(ActionEvent e){
		configurationReplacer.openConfigDialog()
	}
	
	private void openFindReplacer(ActionEvent e){
		FindReplaceController.displayDialog(componentModel,selectModel,undoBuffer)
	}

	void activate(boolean isModal = true) {}

	void deactivate() {
		log.trace("Clearing activeAction")
		if(activeAction != null) {
			toolManager.getTool(activeAction).deactivate()
			activeAction = null
			if(saveActiveTools.size() > 0) {
				log.info("Restoring active tools to $saveActiveTools")
				toolManager.setActiveTools(saveActiveTools)
			}
			saveActiveTools = []
			if(saveActiveButton!=null){
				toolManager.setActiveButton(saveActiveButton)
				saveActiveButton = null
			}
		}
	}

	void mouseDragged(PInputEvent e) {
		if(activeAction != null) {
			toolManager.getTool(activeAction).mouseDragged(e)
			if(e.isHandled())
				deactivate()
		}
	}

	void mouseEntered(PInputEvent e) {
		if(activeAction != null) {
			toolManager.getTool(activeAction).mouseEntered(e)
			if(e.isHandled())
				deactivate()
		}
	}

	void mouseExited(PInputEvent e) {
		if(activeAction != null) {
			toolManager.getTool(activeAction).mouseExited(e)
			if(e.isHandled())
				deactivate()
		}
	}

	void mouseMoved(PInputEvent e) {
		if(activeAction != null) {
			toolManager.getTool(activeAction).mouseMoved(e)
			if(e.isHandled())
				deactivate()
		}
	}

	void mousePressed(PInputEvent e) {
		if(activeAction != null) {
			toolManager.getTool(activeAction).mousePressed(e)
			if(e.isHandled())
				deactivate()
		}
	}

	void mouseReleased(PInputEvent e) {
		if(activeAction != null) {
			toolManager.getTool(activeAction).mouseReleased(e)
			if(e.isHandled())
				deactivate()
		}
	}

	void mouseWheelRotated(PInputEvent e) {
		if(activeAction != null) {
			toolManager.getTool(activeAction).mouseWheelRotated(e)
			if(e.isHandled())
				deactivate()
		}
	}

	void mouseWheelRotatedByBlock(PInputEvent e) {
		if(activeAction != null) {
			toolManager.getTool(activeAction).mouseWheelRotatedByBlock(e)
			if(e.isHandled())
				deactivate()
		}
	}

	void keyPressed(PInputEvent e){
		if(activeAction != null) {
			toolManager.getTool(activeAction).keyPressed(e)
			if(e.isHandled())
				deactivate()
		}
	}

    /**
     * Class for distributing function of staticChildrenNode( horizontal row or rowlite)
     */
    class StaticChildrenDistributionData{
        private StaticChildrenNode parentNode;
        LinkedHashMap<StaticChildNode, Point2D.Double> childrenPosition = new LinkedHashMap<StaticChildNode, Point2D.Double>();
        HashMap<PNode, Point2D.Double> selectedNodesPosition = new HashMap<PNode, Point2D.Double>();

        public StaticChildrenDistributionData(StaticChildrenNode parentNode){
            this.parentNode = parentNode;
            for(int i=0;i<6;i++){
                StaticChildNode childNode = parentNode.getChildNodeByIndex(i);
                DLComponent childComponent = childNode.getComponent();
                Point2D.Double childPoint = new Point2D.Double(childComponent.getPropertyValue("x"), childComponent.getPropertyValue("y"));
                childrenPosition.put(childNode, childPoint);
            }
        }

        public void setChildPosition(StaticChildNode node, Point2D.Double point){
            childrenPosition.put(node,point);
        }

        public StaticChildrenNode getParentNode(){
            return parentNode;
        }

        public LinkedHashMap<StaticChildNode, Point2D.Double> getChildrenPosition(){
            return childrenPosition;
        }

        public void setSelectedNodePosition(PNode node, Point2D.Double point){
          selectedNodesPosition.put(node, point);
        }

        public Point2D.Double getSelectedNodePosition(PNode node){
            return selectedNodesPosition.get(node)
        }

        public PNode getRepresentative(String option){
            //align left
            if(option.equals("l")){
                HashMap sortedMap = sortMap("x")
                log.trace("is same x? " + isAllSameXCoord(sortedMap))
                // A case all rowlite is 0, 180 degree rotation and try select all and align left.
                // should pick parent node as representative
                def parentNodes = sortedMap.keySet().findAll {((DLComponent)it.getComponent()).hasRole("staticchildplaceable")}
                if(isAllSameXCoord(sortedMap) && parentNodes.size()>0) {
                    return parentNodes.toList().get(0)
                }else{
                    List sortedList = sortedMap.keySet().asList();
                    return sortedMap.keySet().asList().get(0)
                }
            }else if(option.equals("r")){ // align right
                HashMap sortedMap = sortMap("x")
                // A case all rowlite is 0, 180 degree rotation and try select all and align right.
                // should pick parent node as representative
                def parentNodes = sortedMap.keySet().findAll {((DLComponent)it.getComponent()).hasRole("staticchildplaceable")}
                if(isAllSameXCoord(sortedMap) && parentNodes.size()>0) {
                    return parentNodes.toList().get(0)
                }else{
                    List sortedList = sortedMap.keySet().asList();
                    return sortedList.get(sortedList.size()-1)
                }
            }else if(option.equals("t")){//align top
                HashMap sortedMap = sortMap("y")
                // A case all rowlite is 90 degree rotation and try select all and align top.
                // should pick parent node as representative
                def parentNodes = sortedMap.keySet().findAll {((DLComponent)it.getComponent()).hasRole("staticchildplaceable")}
                if(isAllSameYCoord(sortedMap) && parentNodes.size()>0) {
                    return parentNodes.toList().get(0)
                }else{
                    List sortedList = sortedMap.keySet().asList();
                    return sortedList.get(0)
                }
            }else if(option.equals("b")){//align bottom
                HashMap sortedMap = sortMap("y")
                // A case all rowlite is 90 degree rotation and try select all and align bottom.
                // should pick parent node as representative
                def parentNodes = sortedMap.keySet().findAll {((DLComponent)it.getComponent()).hasRole("staticchildplaceable")}
                if(isAllSameYCoord(sortedMap) && parentNodes.size()>0) {
                    return parentNodes.toList().get(0)
                }else{
                    List sortedList = sortedMap.keySet().asList();
                    return sortedList.get(sortedList.size()-1)
                }
            }
            return null
        }

        /**
         * determine if all y coordinate is same value or not
         * @param sortedMap
         * @return
         */
        private boolean isAllSameYCoord(HashMap sortedMap){

            def parentNodes = sortedMap.keySet().findAll {((DLComponent)it.getComponent()).hasRole("staticchildplaceable")}

            if(parentNodes.size()>0){
                DLComponent parentComponent = parentNodes.asList().get(0).getComponent()
                double rotation = parentComponent.getPropertyValue("rotation")/90.0

                if((rotation % 2) == 1) {
                    return true
                }else
                    return false
            }else
                return false
        }

        /**
         * determine if all x coordinate is same value or not
         * @param sortedMap
         * @return
         */
        private boolean isAllSameXCoord(HashMap sortedMap){

            def parentNodes = sortedMap.keySet().findAll {((DLComponent)it.getComponent()).hasRole("staticchildplaceable")}

            if(parentNodes.size()>0){
                DLComponent parentComponent = parentNodes.asList().get(0).getComponent()
                double rotation = parentComponent.getPropertyValue("rotation")/90.0

                if((rotation % 2) == 0) {
                    return true
                }else
                    return false
            }else
                return false
        }

        private HashMap sortMap(String key){
            List list = new LinkedList(selectedNodesPosition.entrySet());

            Collections.sort(list, new Comparator(){
                public int compare(Object o1, Object o2){
                    if(key.equals("x")){
                        Double x1 = ((DLComponent)((((Map.Entry)(o1)).getKey())).getComponent()).getPropertyValue("x")
                        Double x2 = ((DLComponent)((((Map.Entry)(o2)).getKey())).getComponent()).getPropertyValue("x")
                        return ((Comparable)x1.compareTo(x2));
                    }else{
                        Double y1 =  ((DLComponent)((((Map.Entry)(o1)).getKey())).getComponent()).getPropertyValue("y")
                        Double y2 =  ((DLComponent)((((Map.Entry)(o2)).getKey())).getComponent()).getPropertyValue("y")
                        return ((Comparable)y1.compareTo(y2));
                    }
                }
            });

            HashMap sortedMap = new LinkedHashMap();

            //log.trace("sorted map info")
            for(Iterator it = list.iterator(); it.hasNext();){
                Map.Entry entry = (Map.Entry)it.next();
                //log.trace(entry.getKey().toString())
                //log.trace(entry.getValue().toString())
                sortedMap.put(entry.getKey(), entry.getValue());
            }

            return sortedMap;
        }

    }
}

