package DeploymentLab.Tools

import DeploymentLab.CentralCatalogue
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.SelectModel
import DeploymentLab.channellogger.Logger
import edu.umd.cs.piccolo.event.PBasicInputEventHandler
import edu.umd.cs.piccolo.event.PInputEvent

/**
 * Augment the Zoom Factor slot in the statusbar by adding the current x & y location when it changes.
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 8/6/12
 * Time: 12:26 PM
 */

class CurrentLocation extends PBasicInputEventHandler {
	private static final Logger log = Logger.getLogger(CurrentLocation.class.getName())
	private static final DIMENSION = "distance"
	private boolean modal
	private SelectModel selectModel
	private DeploymentPanel deploymentPanel

	CurrentLocation(DeploymentPanel dp, SelectModel sm) {
		selectModel = sm
		deploymentPanel = dp
	}


	@Override
	void mouseMoved(PInputEvent event) {
		//Point2D nearestGrid = deploymentPanel.findNearestGridIntersectionTo(event.getPosition())

		//log.trace("Position: x:${event.getPosition().getX().toInteger()}, y:${event.getPosition().getY().toInteger()}; nearest grid point is ${nearestGrid.getX().toInteger()}, ${nearestGrid.getY().toInteger()}", "statusbar")
		//log.trace("Zoom factor: ${numberFormat.format(z)}", 'zoomFactor')
		//log.trace("Position: [x:${event.getPosition().getX().toInteger()}, y:${event.getPosition().getY().toInteger()}]", "mousePosition")
		//deploymentPanel.highlightHere(nearestGrid)

		//with in <-> cm support and adjusted for scale

		def x = event.getPosition().getX() * CentralCatalogue.getDeploymentPanel().getDrawingScale()
		def y = event.getPosition().getY() * CentralCatalogue.getDeploymentPanel().getDrawingScale()
		def systemConverter = CentralCatalogue.INSTANCE.getSystemConverter()
		if (systemConverter != null) {
			def unitConverter = systemConverter.getConverter(DIMENSION)
			x = unitConverter.convert(x)
			y = unitConverter.convert(y)
			//println systemConverter.getTargetSystem().getDimension(DIMENSION).getUnits()
		}
		log.internal("Position: [x:${x.toInteger()}, y:${y.toInteger()}]", "mousePosition")

	}

	void activate(boolean isModal = true) {
		modal = isModal
	}

	void deactivate() {}
}

