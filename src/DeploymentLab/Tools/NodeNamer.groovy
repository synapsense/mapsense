package DeploymentLab.Tools

import java.awt.GridBagConstraints
import java.awt.GridBagLayout

import java.awt.Toolkit

import javax.swing.text.AttributeSet
import javax.swing.text.BadLocationException
import javax.swing.text.DocumentFilter
import javax.swing.text.DocumentFilter.FilterBypass

import javax.swing.BorderFactory
import javax.swing.JDialog
import javax.swing.JTextField
import javax.swing.SpringLayout
import javax.swing.event.DocumentEvent
import javax.swing.event.DocumentListener

import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.event.*

import groovy.swing.SwingBuilder

import DeploymentLab.channellogger.*
import DeploymentLab.SceneGraph.DeploymentNode

import DeploymentLab.SpringUtilities
import DeploymentLab.SelectModel
import java.awt.event.ActionListener
import javax.swing.KeyStroke
import java.awt.event.KeyEvent
import javax.swing.JComponent
import java.awt.FlowLayout
import java.awt.BorderLayout
import java.awt.Insets
import DeploymentLab.CentralCatalogue

class NodeNamer extends PBasicInputEventHandler implements DocumentListener{
	private static final Logger log = Logger.getLogger(NodeNamer.class.getName())

	private SelectModel selectModel

	private boolean modal

	private int curval
	private int increment

	private SwingBuilder builder
	private JDialog dialog

	NodeNamer(SelectModel sm) {
		selectModel = sm
		
		builder = new SwingBuilder()
		GridBagConstraints gbc = new GridBagConstraints()
		gbc.insets = new Insets(5, 5, 5, 5)
		def buttonPanel
		def closeAction

		dialog = builder.dialog(owner: CentralCatalogue.getParentFrame(), layout: new BorderLayout(), resizable: false, title: 'Quick Name settings', alwaysOnTop: true) {
	
			panel( layout: new GridBagLayout(), constraints: BorderLayout.CENTER ){
				gbc.gridx=0; gbc.gridy=0
				label(text: 'Numeric #1', constraints: gbc)
				gbc.gridx=1; gbc.gridy=0
				label(text: 'Numeric #2', constraints: gbc)
				gbc.gridx=2; gbc.gridy=0
				label(text: 'Alpha #1', constraints: gbc)

				gbc.gridx=0; gbc.gridy=1
				panel(constraints: gbc, border: BorderFactory.createEtchedBorder(), layout: new SpringLayout(), id: 'panel1') {
					label 'Nextval:'
					limited(listened(textField(columns: 5, id: 'nextval1', text: '1')), 4, "\\d+")
					label 'Increment:'
					limited(listened(textField(columns: 5, id: 'increment1', text: '1')), 4, "\\d+")
					label 'Width:'
					limited(listened(textField(columns: 5, id: 'width1', text: '1')), 2, "\\d+")
					label 'Padding:'
					limited(listened(textField(columns: 5, id: 'padding1', text: '0')), 3, ".*")
				}
				SpringUtilities.makeCompactGrid(builder.panel1, 4, 2, 6, 6, 6, 6)

				gbc.gridx=1; gbc.gridy=1
				panel(constraints: gbc, border: BorderFactory.createEtchedBorder(), layout: new SpringLayout(), id: 'panel2') {
					label 'Nextval:'
					limited(listened(textField(columns: 5, id: 'nextval2', text: '1')), 4, "\\d+")
					label 'Increment:'
					limited(listened(textField(columns: 5, id: 'increment2', text: '1')), 4, "\\d+")
					label 'Width:'
					limited(listened(textField(columns: 5, id: 'width2', text: '1')), 2, "\\d+")
					label 'Padding:'
					limited(listened(textField(columns: 5, id: 'padding2', text: '0')), 3, ".*")
				}
				SpringUtilities.makeCompactGrid(builder.panel2, 4, 2, 6, 6, 6, 6)

				gbc.gridx=2; gbc.gridy=1
				panel(constraints: gbc, border: BorderFactory.createEtchedBorder(), layout: new SpringLayout(), id: 'panel3') {
					label 'Nextval:'
					limited(listened(textField(columns: 5, id: 'nextval3', text: 'A')), 4, "[a-zA-Z]+")
					label 'Increment:'
					limited(listened(textField(columns: 5, id: 'increment3', text: '1')), 4, "\\d+")
					label 'Width:'
					limited(listened(textField(columns: 5, id: 'width3', text: '1')), 2, "\\d+")
					label 'Padding:'
					limited(listened(textField(columns: 5, id: 'padding3', text: ' ')), 3, ".*")
				}
				SpringUtilities.makeCompactGrid(builder.panel3, 4, 2, 6, 6, 6, 6)

				gbc.gridx=0; gbc.gridy=2; gbc.gridwidth=3; gbc.fill = GridBagConstraints.HORIZONTAL;
				panel(constraints: gbc, border: BorderFactory.createEtchedBorder(), layout: new SpringLayout(), id: 'formatpanel') {
					label 'Format String:'
					listened(textField(columns: 20, id: 'formatstring'))
					label 'Next generated name:'
					textField(columns: 20, id: 'nextname', editable: false)
				}
				SpringUtilities.makeCompactGrid(builder.formatpanel, 2, 2, 6, 6, 6, 6)
			}
			closeAction = action( name: 'Close', mnemonic: KeyEvent.VK_C, closure: {dialog.setVisible(false)} )
			buttonPanel = panel(constraints: BorderLayout.SOUTH, layout: new FlowLayout(FlowLayout.TRAILING)) {
				button(text: 'Reset', mnemonic: KeyEvent.VK_R, actionPerformed: {resetCounters()})
				button(text: 'Close', mnemonic: KeyEvent.VK_C, defaultButton: true, action: closeAction)
			}
		}
		buttonPanel.registerKeyboardAction(closeAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		buttonPanel.registerKeyboardAction(closeAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		dialog.pack()
	}

	String getNumeric1() {
		return calcField(builder.nextval1.text, builder.width1.text, builder.padding1.text)
	}

	String getNumeric2() {
		return calcField(builder.nextval2.text, builder.width2.text, builder.padding2.text)
	}

	String getAlpha1() {
		return calcField(builder.nextval3.text, builder.width3.text, builder.padding3.text)
	}

	String calcField(String strval, String fldWidthStr, String padding) {
		if(strval.size() == 0 || fldWidthStr.size() == 0 || padding.size() == 0)
			return ""
		int fldWidth = Integer.parseInt(fldWidthStr)
		if(strval.size() >= fldWidth)
			return strval

		String padder = padding + padding
		while(padder.size() < fldWidth)
			padder = padder + padder

		return padder.substring(0, fldWidth-strval.size()) + strval
	}

	String getNextName() {
		String format = builder.formatstring.getText()
		format = format.replace('$1', getNumeric1())
		format = format.replace('$2', getNumeric2())
		format = format.replace('$3', getAlpha1())
		return format
	}

	void resetCounters() {
		builder.nextval1.setText("1")
		builder.nextval2.setText("1")
		builder.nextval3.setText("A")
	}

	void incrementCounters() {
		int intval = Integer.parseInt(builder.nextval1.getText())
		int inc = Integer.parseInt(builder.increment1.getText())
		int newval = intval + inc
		builder.nextval1.setText(String.valueOf(newval))

		intval = Integer.parseInt(builder.nextval2.getText())
		inc = Integer.parseInt(builder.increment2.getText())
		newval = intval + inc
		builder.nextval2.setText(String.valueOf(newval))

		char A = 'A'

		String strval = builder.nextval3.getText()
		int carry = Integer.parseInt(builder.increment3.getText())
		StringBuilder result = new StringBuilder()
		(strval.size()-1 .. 0).each{ index ->
			int charval = strval.charAt(index) - A + carry
			result.insert(0, (char)((charval % 26) + A))
			carry = (int)(charval / 26)
		}
		if(carry > 0)
			result.insert(0, (char)(carry + A - 1))

		builder.nextval3.setText(result.toString())
	}

	JTextField listened(JTextField tf) {
		tf.getDocument().addDocumentListener(this)
		return tf
	}

	JTextField limited(JTextField tf, int maxLen, String charRe) {
		tf.getDocument().setDocumentFilter(new LimitingDocFilter(maxLen, charRe))
		return tf
	}

	void setVisible(boolean v) {
		dialog.setVisible(v)
	}

	void mouseClicked(PInputEvent e) {
		super.mouseClicked(e)
		if(!e.isLeftMouseButton())
			return

		PNode node = e.getPickedNode()

		if(! dialog.isVisible()) {
			dialog.setLocationRelativeTo(CentralCatalogue.getParentFrame())
			dialog.setVisible(true)
			return
		}

		if(getNextName() == '') {
			log.error("Please enter a format string first!", 'message')
			return
		}

		if(node instanceof DeploymentNode) {
			node.getComponent().setPropertyValue('name', getNextName())
			selectModel.setSelection([node.getComponent()])
			incrementCounters()
		}
	}

	public void insertUpdate(DocumentEvent ev) {
		builder.nextname.setText(getNextName())
	}

	public void removeUpdate(DocumentEvent ev) {
		builder.nextname.setText(getNextName())
	}

	public void changedUpdate(DocumentEvent ev) {
	}

	void activate(boolean isModal = true) {
		modal = isModal
		dialog.setVisible(true)
	}

	void deactivate() {
		dialog.setVisible(false)
	}
}

class LimitingDocFilter extends DocumentFilter {
	private int maxLen

	private String charRe

	public LimitingDocFilter(int maxLen, String charRe) {
		this.maxLen = maxLen
		this.charRe = charRe
	}

	public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
		if((fb.getDocument().getLength() + text.length() - length <= maxLen) && text.matches(charRe))
			super.replace(fb, offset, length, text, attrs)
		else
			Toolkit.getDefaultToolkit().beep()
	}

	public void insertString(FilterBypass fb, int offset, String text, AttributeSet attrs) throws BadLocationException {
		if(fb.getDocument().getLength() + text.length() <= maxLen && text.matches(charRe))
			super.replace(fb, offset, text, attrs)
		else
			Toolkit.getDefaultToolkit().beep()
	}
}

