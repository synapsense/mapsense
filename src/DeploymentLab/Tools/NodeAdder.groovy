package DeploymentLab.Tools

import DeploymentLab.*
import DeploymentLab.Dialogs.ViewOptionDialog
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.SceneGraph.DeploymentNode
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.SceneGraph.DynamicChildrenNode
import DeploymentLab.SceneGraph.StaticChildrenNode
import DeploymentLab.Tree.ViewOptionTree
import DeploymentLab.channellogger.Logger
import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.event.PInputEvent

import javax.swing.*
import java.awt.geom.Point2D

class NodeAdder extends AbstractNodeAdder {
	private static final Logger log = Logger.getLogger(NodeAdder.class.getName())

	NodeAdder(SelectModel sm, ComponentModel cm, UndoBuffer ub, ViewOptionTree vt, UIDisplay ud, JFrame _parent, ViewOptionDialog vod, Properties uis, DeploymentPanel dp) {
		super(sm, cm, ub, vt, ud, _parent, vod, uis, dp)
    }

	void addNode(PInputEvent e) {
		PNode node = e.getPickedNode()
		if(node instanceof DeploymentNode || node instanceof  StaticChildrenNode || node instanceof DynamicChildrenNode){
			return
		}
		try {
			undoBuffer.startOperation();

			if(isAddable(e)){
				Point2D pt = e.getPosition()
				this.addNode(pt)
			}
		} finally {
			undoBuffer.finishOperation()
			e.setHandled(true)
		}
	}

	void addNode(Point2D pt){
		DLComponent component = null
		try {
			component = componentModel.newComponent(type)

			addToParent( component )

			component.setDrawing(selectModel.getActiveDrawing())
			def p = new Point2D.Double(pt.getX() * selectModel.getActiveDrawing().getScale(),
			                           pt.getY() * selectModel.getActiveDrawing().getScale())
			def adjusted = ((NodeMover)this.getManager().getTool("mover")).getPossiblePosition(p)

			component.setPropertyValue("x", adjusted.x)
			component.setPropertyValue("y", adjusted.y)

        } catch(Exception ex) {
			log.error(log.getStackTrace(ex))
			log.error("Error adding component!\nSee logfile.", 'message')
			undoBuffer.rollbackOperation()
		}

		if ( component != null ){
			//println "setting selection to $component"
			selectModel.setSelection(component)
		}
	}
}

