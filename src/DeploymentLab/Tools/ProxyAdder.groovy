package DeploymentLab.Tools

import edu.umd.cs.piccolo.event.PInputEvent
import edu.umd.cs.piccolo.PNode
import DeploymentLab.SceneGraph.DeploymentNode
import DeploymentLab.channellogger.Logger
import DeploymentLab.SelectModel
import DeploymentLab.UndoBuffer
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLProxy
import DeploymentLab.Image.DLImage
import DeploymentLab.CentralCatalogue
import DeploymentLab.Dialogs.ChooseDrawing.ChooseDrawingController
import edu.umd.cs.piccolo.event.PBasicInputEventHandler

/**
 * Deployment Lab tool used to create proxies for components.
 *
 * @author Ken Scoggins
 * @since Jupiter 2
 */
class ProxyAdder extends PBasicInputEventHandler {
	private static final Logger log = Logger.getLogger(ProxyAdder.class.getName())

    protected SelectModel selectModel
    protected ComponentModel componentModel
    protected UndoBuffer undoBuffer
    protected boolean modal

	public ProxyAdder( SelectModel sm, ComponentModel cm, UndoBuffer ub ) {
        this.selectModel = sm
        this.componentModel = cm
        this.undoBuffer = ub
	}

    void activate( boolean isModal = true ) {
        modal = isModal
    }

    public void deactivate() {}

    /**
     * Handles mouse events this tool is active and a PNode is selected. A new proxy will be created for the selected
     * node if it is a proxyable DLComponent node.
     *
     * @param e  The event to handle.
     */
    public void mousePressed( PInputEvent e ) {
        if( !e.isLeftMouseButton() ) {
            return
        }

		PNode node = e.getPickedNode()

		if( node instanceof DeploymentNode ) {
            def anchor = ((DeploymentNode) node).getComponent()
            addProxy( anchor )
            e.setHandled( true )
        } else {
            log.warn( CentralCatalogue.getUIS("proxy.cannotProxyGeneric"), "statusbar")
        }
	}

    /**
     * Makes a new proxy for the provided component. The user will be prompted to select the target drawings
     * for the new proxy. One proxy will be created in each of the selected drawings. If only one drawing exists,
     * the proxy will be created on the current drawing without prompting the user.
     *
     * @param anchor  The component to proxy.
     */
    public void addProxy( DLComponent anchor ) {
        // Be lazy and just call the multi-select version.
        addProxy( [ anchor ] )
    }

    /**
     * Makes a new proxy for each of the provided components. The user will be prompted to select the target drawings
     * for the new proxies. One proxy for each anchor will be created in each of the selected drawings. If only one
     * drawing exists, the proxy will be created on the current drawing without prompting the user.
     *
     * @param anchors  The list of components to proxy.
     */
    public void addProxy( Collection<DLComponent> anchors ) {
        // Ask for the locations only once, then apply it to all anchors.
        def chosenDrawings = ChooseDrawingController.chooseDrawing( selectModel, componentModel )
        log.trace("Proxy drawings chosen: $chosenDrawings")
        if( !chosenDrawings.isEmpty() ) {
            try {
                // Manage the buffer here instead of where we actually do the create so that all are rolled back together.
                undoBuffer.startOperation();

                def selectThese = []
                for( DLComponent drawing : chosenDrawings ) {
                    for( DLComponent anchor : anchors ) {
                        def proxy = createProxy( anchor, drawing )
                        // We want to select it if it is in the Active Drawing.
                        if( ( proxy != null ) && ( drawing == selectModel.getActiveDrawing() ) ){
                            selectThese += proxy
                        }
                    }
                }

                // Select those on the current drawing.
                if( !selectThese.isEmpty() ) {
                    selectModel.setSelection( selectThese )
                }
            } catch( Exception e ) {
                // The error should have already been handled, but log the stack trace, just in case it is something else.
                log.error( log.getStackTrace(e), "default" )
                undoBuffer.rollbackOperation()
            } finally {
                undoBuffer.finishOperation()
            }

        }
    }

    /**
     * Creates a new proxy from the provided source component. This should not be called directly since it does not
     * perform any Undo Buffer management. The addProxy methods should be used instead.
     *
     * @param anchor      The source component to proxy.
     * @param drawing  The drawing to place the new proxy.
     *
     * @return  The new proxy component.
     */
    public DLComponent createProxy( DLComponent anchor, DLComponent drawing ) {

        if( !DLProxy.canProxy( anchor ) ) {
            log.warn( CentralCatalogue.formatUIS("proxy.cannotProxySpecific", anchor.getDisplaySetting('name') ), "message")
            return
        }

        DLComponent proxy = null

        try {
            log.trace("Creating Proxy: $anchor --> Drawing '${drawing.getName()}'")

            proxy = DLProxy.createProxy( anchor )

			drawing.addChild( proxy )

            // This must be after it is added to the drawing for some initial configuration to work right, like halo placement.
            setLocation( anchor, proxy, drawing )

        } catch( Exception e ) {
            // Log the component specific error, but let the caller handle any further action.
            log.error( CentralCatalogue.formatUIS("proxy.createError", anchor.getName()), "message")
            throw e
        }

        return proxy
	}

    /**
     * Finds a spot to drop the new proxy on the background. Highly complicated calculations and Top Secret proprietary
     * logic are used to determine the best location on the background. Trust me.
     *
     * @param anchor         The source component for the proxy. Its location is used as a reference.
     * @param proxy          The new proxy component to be placed.
     * @param destinationDrawing  The drawing to place the new proxy.
     */
    private void setLocation( DLComponent anchor, DLProxy proxy, DLComponent destinationDrawing ) {

        // todo: PROXY - This will likely need to change. Not sure the best placement so it doesn't get lost. Also
        //               don't want to stack them on top of each other when multiple are selected.

        double newX = anchor.getPropertyValue("x") + (( selectModel.getActiveDrawing() == destinationDrawing ) ? 15 : 0)
        double newY = anchor.getPropertyValue("y") + (( selectModel.getActiveDrawing() == destinationDrawing ) ? 15 : 0)

        DLImage image = destinationDrawing.getPropertyValue('image_data')

        if( image != null ) {
            double scale = destinationDrawing.getPropertyValue('scale')

            // Different drawing, so drop it in the same spot as the anchor, or the nearest edge if the image is smaller.
            double maxX = image.getWidth() * scale
            double maxY = image.getHeight() * scale

            if( newX > maxX ) {
                newX = maxX
            }

            if( newY > maxY ) {
                newY = maxY
            }
        }

        proxy.setPropertyValue('x', newX )
        proxy.setPropertyValue('y', newY )
    }
}
