package DeploymentLab.Tools

import DeploymentLab.Model.ComponentModel
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.SceneGraph.StaticChildrenNode.StaticChildNode
import DeploymentLab.SelectModel
import DeploymentLab.UIDisplay
import DeploymentLab.UndoBuffer
import DeploymentLab.channellogger.Logger
import edu.umd.cs.piccolo.event.PBasicInputEventHandler
import edu.umd.cs.piccolo.event.PInputEvent
import java.awt.Toolkit
import java.awt.event.KeyEvent
import edu.umd.cs.piccolo.util.PDimension

class KeyboardCommands extends PBasicInputEventHandler {
	private static final Logger log = Logger.getLogger(KeyboardCommands.class.getName())
	private static final CAMERA_MOVE = 10

	ToolManager manager

	private ComponentModel componentModel
	private UndoBuffer undoBuffer
	private SelectModel selectModel
	private UIDisplay uiDisplay
	private DeploymentPanel deploymentPanel
	private boolean modal
	def components

	KeyboardCommands(ComponentModel cm, UndoBuffer ub, SelectModel sm, UIDisplay ud, DeploymentPanel dp) {
		componentModel = cm
		undoBuffer = ub
		selectModel = sm
		uiDisplay = ud
		deploymentPanel = dp
	}

	private List keys = []

	void keyPressed(PInputEvent e) {
		super.keyPressed(e)
		//def nodeList = selectModel.getExpandedSelection()
        //log.trace("key pressed" + e.getKeyCode().toString())
		//println "keyboard handler, key pressed was ${e.getKeyCode()}"
		if(e.isControlDown()){
			return
		}
		switch (e.getKeyCode()) {
			case KeyEvent.VK_UP:
				move(MoveCode.UP,)
				keys += e.getKeyCode()
				break;
			case KeyEvent.VK_DOWN:
				move(MoveCode.DOWN)
				keys += e.getKeyCode()
				break;
			case KeyEvent.VK_LEFT:
				move(MoveCode.LEFT)
				keys += e.getKeyCode()
				break;
			case KeyEvent.VK_RIGHT:
				move(MoveCode.RIGHT)
				keys += e.getKeyCode()
				break;

			case KeyEvent.VK_A:
			case KeyEvent.VK_B:
			//case KeyEvent.VK_CONTROL:
			//case KeyEvent.VK_ALT:
				keys += e.getKeyCode()
				break;
            default:
				keys.clear()
				break;
		}

		//log.trace(keys.toString())
		//check list
		if (keys == [KeyEvent.VK_UP, KeyEvent.VK_UP, KeyEvent.VK_DOWN, KeyEvent.VK_DOWN, KeyEvent.VK_LEFT, KeyEvent.VK_RIGHT, KeyEvent.VK_LEFT, KeyEvent.VK_RIGHT, KeyEvent.VK_B, KeyEvent.VK_A]) {
			contraCode()
		} else if (keys.size() > 10 && keys.last() == KeyEvent.VK_A) {
			keys.clear()
		}

	}

	void mousePressed(PInputEvent e) {
		super.mousePressed(e)
		e.getInputManager().setKeyboardFocus(e.getPath())
	}

	void keyReleased(PInputEvent e) {
		super.keyReleased(e)
	}

	void keyTyped(PInputEvent e) {
		super.keyTyped(e)
	}

	void activate(boolean isModal = true) {
		modal = isModal
	}

	void deactivate() {}

	/**
	 * Determines if the current move is a node move or a pan, and forwards to the correct medthod.
	 * @param moveCode the current movement direction
	 * @param nodeList the current selection.
	 */
	void move(MoveCode moveCode) {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		def state = toolkit.getLockingKeyState(KeyEvent.VK_SCROLL_LOCK);
		//if scroll lock is on, move the camera; if off, move the selected nodes
		if (state) {
			moveCamera(moveCode)
		} else {
			moveNodes(moveCode)
		}
	}

	void moveCamera(MoveCode moveCode) {
		def d = new PDimension(0, 0)
		switch (moveCode) {
			case MoveCode.UP:
				//y = -1;
				d.height = CAMERA_MOVE
				break;
			case MoveCode.DOWN:
				//y = 1;
				d.height = -CAMERA_MOVE
				break;
			case MoveCode.LEFT:
				d.width = CAMERA_MOVE
				//x = -1;
				break;
			case MoveCode.RIGHT:
				//x = 1;
				d.width = -CAMERA_MOVE
				break;
		}
		deploymentPanel.pan(d)
	}

	void moveNodes(MoveCode moveCode) {
		def nodeList = selectModel.getExpandedSelection()
		try {
			undoBuffer.startOperation()
			int x = 0
			int y = 0
			switch (moveCode) {
				case MoveCode.UP:
					y = -1;
					break;
				case MoveCode.DOWN:
					y = 1;
					break;
				case MoveCode.LEFT:
					x = -1;
					break;
				case MoveCode.RIGHT:
					x = 1;
					break;
			}
			nodeList.each { n ->
				if (x != 0) {
					if (n.hasRole("staticchild")) {
						log.trace("move staticchild" + moveCode)
						StaticChildNode staticChildNode = (StaticChildNode) deploymentPanel.getPNodeFor(n)
						staticChildNode.offsetByKey(moveCode)
					} else {
						n.setPropertyValue("x", n.getPropertyValue("x") + x)
					}
				}
				if (y != 0) {
					if (n.hasRole("staticchild")) {
						log.trace("move staticchild" + moveCode)
						StaticChildNode staticChildNode = (StaticChildNode) deploymentPanel.getPNodeFor(n)
						staticChildNode.offsetByKey(moveCode)
					} else {
						n.setPropertyValue("y", n.getPropertyValue("y") + y)
					}
				}
			}
		} catch (Exception e) {
			log.error(log.getStackTrace(e));
			undoBuffer.rollbackOperation()
		} finally {
			undoBuffer.finishOperation()
		}
	}

	private void contraCode() {
		log.info("REST 30", "statusbar")
		uiDisplay.setUserLevel(4)
	}
}

