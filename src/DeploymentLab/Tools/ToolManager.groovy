
package DeploymentLab.Tools

import javax.swing.ButtonGroup

import edu.umd.cs.piccolo.event.*

import DeploymentLab.channellogger.*

public class ToolManager extends PBasicInputEventHandler {
	private static final Logger log = Logger.getLogger(ToolManager.class.getName())

	private ButtonGroup buttonGroup

	private tools = [:]
	private activetools = []
	private toolButtons = [:]

	private suspendedTools = null
	private def suspendedButton = null

	protected def defaultTools = []
	protected def defaultButton
	protected Closure defaultClosure
	boolean singleShotMode = false
	Map<String,Boolean> allowedToSingleShot = [:]
	boolean singleShotByPressed = false

	ToolManager(ButtonGroup bg) {
		buttonGroup = bg
	}

	void registerTool(String name, PBasicInputEventHandler tool) {
		//tools[name] = tool
		this.registerTool(name,tool,false,false)
	}

	void registerTool(String name, PBasicInputEventHandler tool, Boolean hasAButton, Boolean allowsSingleShot=false) {
		tools[name] = tool
		toolButtons[name] = hasAButton

		allowedToSingleShot[name] = allowsSingleShot

		if(tool.respondsTo("setManager")){
			log.debug("adding toolmanager to $name")
			tool.setManager(this)
		}
	}

	def getActiveTools() {
		return activetools
	}

	void setActiveTools(def newTools) {
		//if(newTools.indexOf('adder') != -1) // kludge alert!
		//	buttonGroup.clearSelection()
		activetools.each{tools[it].deactivate()}
		activetools = newTools
		activetools.each{tools[it].activate()}

		boolean leaveButtonsAlone = false
		newTools.each{ t ->
			if ( toolButtons[t] == true ){
				leaveButtonsAlone = true
			}
		}
		if ( ! leaveButtonsAlone ) { buttonGroup.clearSelection() }
		//log.trace("Active Tools: $activetools", "statusbar")
	}

	/**
	 * Sets the tools that are to be activated by default after startup or loading a file
	 * @param newTools a List of tools
	 * @param defaultButton the button (if any) to be "pressed" in the toolbar when these tools are active
	 * @param c a closure to run when activating these tools (to handle any other cleanup when switching tools)
	 */
	public void setDefaultTools(def newTools, def newButton, Closure c){
		this.defaultTools = newTools
		this.defaultButton = newButton
		this.defaultClosure = c
	}

	/**
	 * Activates the tools defined by setDefaultTools
	 */
	public void activateDefaults(){
		this.setActiveTools(this.defaultTools)
		defaultButton.setSelected(true)
		defaultClosure.call()
	}

	/**
	 * Checks if a given tool allows single-shot mode
	 * @param toolName the registered name of the tool to check
	 * @return true if the tool supports single-shot mode
	 * todo: the tool itself should report this, not a separate list in the tool manager
	 */
	public boolean canSingleShot(String toolName){
		log.trace("canSingleShot($toolName) == ${allowedToSingleShot[toolName]}")
		return allowedToSingleShot[toolName]
	}

	protected boolean checkSingleShot(String name){
		if (singleShotMode && allowedToSingleShot[name]){
			this.activateDefaults()
			return true
		}
		return false
	}

	def getActiveButton(){
		buttonGroup.getSelection()
	}

	void setActiveButton(def btn){
		buttonGroup.setSelected(btn,true)
	}

    void deactivateAllTools(){
        activetools.each{log.info("deactivate $it");tools[it].deactivate()}
        activetools = []
    }


	public def getTool(String name) {
		return tools[name]
	}

	boolean isToolActive(String name) {
		return (name in activetools)
	}

	public void suspendTools(){
		suspendedButton = buttonGroup.getSelection()
		buttonGroup.clearSelection()

        if(activeTools!=null){
            suspendedTools = new ArrayList(activetools)
            this.deactivateAllTools()
        }
		//suspendedTools.each{log.trace("suspendedTool:" + it)}
	}

	public void resumeTools(){
        //suspendedTools.each{log.trace("resume suspendedTool:" + it)}
		this.setActiveTools(suspendedTools)
		buttonGroup.setSelected(suspendedButton,true)
		suspendedButton = null
		suspendedTools = null
	}

    public def getSuspendedTools(){
        return suspendedTools
    }

	boolean acceptsEvent(PInputEvent e, int type) {
		for(String t: activetools) {
			if(tools[t].acceptsEvent(e, type))
				return true
		}
		return false
	}

	void mouseClicked(PInputEvent e) {
		//log.trace("mouseClicked")
		//if the mousePressed event was a singleShot event, ignore the mouseClicked event
		if (singleShotByPressed){
			e.setHandled(true)
			singleShotByPressed = false
			return
		}
		
		e.getInputManager().setKeyboardFocus(e.getPath())
		for(String t: activetools) {
			//log.trace("$t -> mouseClicked")
			tools[t].mouseClicked(e)
			if(e.isHandled())
				return
		}
		e.setHandled(true)
	}

	void mouseDragged(PInputEvent e) {
		for(String t: activetools) {
			//log.trace("$t -> mouseDragged")
			tools[t].mouseDragged(e)
			if(e.isHandled())
				return
		}
		e.setHandled(true)
	}

	void mouseEntered(PInputEvent e) {
		for(String t: activetools) {
			//log.trace("$t -> mouseEntered")
			tools[t].mouseEntered(e)
			if(e.isHandled())
				return
		}
		e.setHandled(true)
	}

	void mouseExited(PInputEvent e) {
		for(String t: activetools) {
			//log.trace("$t -> mouseExited")
			tools[t].mouseExited(e)
			if(e.isHandled())
				return
		}
		e.setHandled(true)
	}

	void mouseMoved(PInputEvent e) {
		for(String t: activetools) {
			//log.trace("$t -> mouseMoved")
			tools[t].mouseMoved(e)
			if(e.isHandled())
				return
		}
		e.setHandled(true)
	}

	void mousePressed(PInputEvent e) {
		//log.trace("mousePressed")
		for(String t: activetools) {
			//log.trace("$t -> mousePressed")
			tools[t].mousePressed(e)
			if(e.isHandled()){
				if(checkSingleShot(t)){
					singleShotByPressed = true
				}
				return
			}
		}
		e.setHandled(true)
	}

	void mouseReleased(PInputEvent e) {
		for(String t: activetools) {
			//log.trace("$t -> mouseReleased")
			tools[t].mouseReleased(e)
			if(e.isHandled())
				return
		}
		e.setHandled(true)
	}

	void mouseWheelRotated(PInputEvent e) {
		for(String t: activetools) {
			tools[t].mouseWheelRotated(e)
			if(e.isHandled())
				return
		}
		e.setHandled(true)
	}

	void mouseWheelRotatedByBlock(PInputEvent e) {
		for(String t: activetools) {
			tools[t].mouseWheelRotatedByBlock(e)
			if(e.isHandled())
				return
		}
		e.setHandled(true)
	}

	void keyPressed(PInputEvent e){
		e.getInputManager().setKeyboardFocus(e.getPath())
		for(String t: activetools) {
			//println "keypressed to tool $t"
			tools[t].keyPressed(e)
			if(e.isHandled())
				return
		}
		e.setHandled(true)
	}
}

/*
interface DeploymentTool
	activate()
	activate(boolean isModal)
	deactivate
	canSingleShot
	attachToolManager

 */

