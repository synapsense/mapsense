package DeploymentLab.Tools

import java.awt.event.KeyEvent
import java.awt.geom.Point2D
import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.event.*
import edu.umd.cs.piccolo.util.PPickPath
import DeploymentLab.channellogger.*
import DeploymentLab.Model.*
import DeploymentLab.SceneGraph.DeploymentAssociation
import DeploymentLab.SceneGraph.DeploymentNode
import DeploymentLab.SelectModel
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.UndoBuffer
import DeploymentLab.UIDisplay
import DeploymentLab.DisplayProperties
import javax.swing.JFrame
import DeploymentLab.Dialogs.ViewOptionDialog
import DeploymentLab.Dialogs.Association.AssociationController

/**
 * New association tool using the new dialog and controller.
 * Little more than a wrapper around calls to AssociationController.
 * @author Gabriel Helman
 * @since Mars
 * @see AssociationController
 */
class NewAssoc extends PBasicInputEventHandler {
	private static final Logger log = Logger.getLogger(NewAssoc.class.getName())

	private DeploymentNode selected

	private UndoBuffer undoBuffer
	private SelectModel selectModel
	private DeploymentPanel deploymentPanel
	private DisplayProperties displayProperties
	private UIDisplay uiDisplay

	private JFrame parent
	private boolean modal
	private ViewOptionDialog viewOptionDialog

	private List<DLComponent> selectedList = null

	NewAssoc(SelectModel sm, DeploymentPanel dp, UndoBuffer ub, DisplayProperties p, UIDisplay ud, JFrame _parent, ViewOptionDialog vod) {
		undoBuffer = ub
		selectModel = sm
		deploymentPanel = dp
		displayProperties = p
		uiDisplay = ud
		parent = _parent
		viewOptionDialog = vod
		selected = null
	}


	public void mouseClicked(PInputEvent e) {
		super.mouseClicked(e)
		if(modal && !e.isLeftMouseButton()){
			return
		}

		PNode node = e.getPickedNode()


		DLComponent first
		DLComponent second

		if(node instanceof DeploymentAssociation) {
			first = node.getProducer()
			second = node.getConsumer()
			selectModel.setSelection([first, second])
		} else if(node instanceof DeploymentNode) {

			if (!modal &&  selectModel.getExpandedSelection().size() > 1 && selected == null && selectedList == null ){
				//println "!modal 1"
				//we're in the multi-assoc case, just starting
				selectedList = new ArrayList<DLComponent>( selectModel.getExpandedSelection() );
				return;
			}

			if(!modal &&  selectedList != null ){
				//println "!modal 2"
				//we're in the closing half of the multi-case
				multiAssociate(selectedList, node.getComponent())
				selectModel.clearSelection()
				selectedList = null
				e.setHandled(true)
				return
			}


			if(selected == null) {
				selected = (DeploymentNode)node
				first = selected.getComponent()
				selectModel.setSelection([first])
				if(modal){ e.setHandled(true) }
				return
			}

			if(selected == node) {
				selected = null
				selectModel.setSelection([])
				if(modal){
					e.setHandled(true)
				}
				return
			}

			first = selected.getComponent()
			second = node.getComponent()
		} else {
            // Clear and reset.
			selected = null;
			selectModel.clearSelection()
            e.setHandled(true)
			return
		}

		if(first!=null && second!=null){
			try {
				undoBuffer.startOperation()
				def ac = new AssociationController(first,second)
				ac.associate(parent)

			} catch (Exception ex) {
				log.error("Error while performing association!", "message")
				log.error(log.getStackTrace(ex))
				undoBuffer.rollbackOperation()
			} finally {
				undoBuffer.finishOperation()
				selected = null;
				selectModel.clearSelection()
				e.setHandled(true)
			}
		}else{
			log.info("cannot associate null object")
		}
	}


	//todo: solve some focus issues with delete key
	void keyPressed(PInputEvent e) {
		//println "NewAssoc: key pressed! ${e.getKeyCode()} $e"
		super.keyPressed(e)
		Point2D pos = e.getCanvasPosition()
		PPickPath ppath = e.getCamera().pick(pos.x, pos.y, (double)1.0)
		PNode picked = ppath.getPickedNode()
		if(picked instanceof DeploymentAssociation && ( e.getKeyCode() == KeyEvent.VK_DELETE  ||  e.getKeyCode() == KeyEvent.VK_BACK_SPACE )) {
			if(!uiDisplay.isAccessible('associate')){
				log.info(uiDisplay.getUserLevel() + ' cannot delete association' )
				return
			}
			log.info("Deleting all associations between ${picked.getConsumer()} and ${picked.getProducer()}")
			def first = picked.getProducer()
			def second = picked.getConsumer()
			try {
				undoBuffer.startOperation()

				def ac = new AssociationController(first,second)
				ac.clearAllAssociations()

			} catch (Exception ex) {
				log.error("Error while removing associations!", "message")
				log.error(log.getStackTrace(ex))
				undoBuffer.rollbackOperation()
			} finally {
				undoBuffer.finishOperation()
				selected = null;
				selectModel.clearSelection()
				e.setHandled(true)
			}
		} else if( e.getKeyCode() == KeyEvent.VK_ESCAPE ) {
            // Clear and reset.
            selected = null;
            selectModel.clearSelection();
            e.setHandled(true);
        }
	}

	void activate(boolean isModal = true) {
		modal = isModal
		selected = null
		if(modal) {
			selectModel.clearSelection()
		}
	}

	void deactivate() {
		selected = null
		selectModel.clearSelection()
	}

	/**
	 * Associate a group of components with another.
	 * Only if all the items in the batch can be solo-associated will this be performed.
	 * @param batch a List of DLComponents to associate with the target
	 * @param target the DLComponent to associate with
	 */
	protected void multiAssociate(List<DLComponent> batch, DLComponent target){
		//println "multiAssociate($batch, $target)"
		def acs = []
		for( DLComponent c : batch ){
			acs.add( new AssociationController(c, target) )
		}
/*
		boolean canDoIt = true
		List<String> badNames = []
		for( AssociationController ac : acs){
			//println "$ac canSolo is ${ac.canSolo()}"
			if ( ! ac.canSolo().isValid ){
				canDoIt = false
				badNames += ac.getYin().getName()
			}
		}

		if( ! canDoIt ){
			StringBuilder msg = new StringBuilder()
			msg.append(	CentralCatalogue.getUIS("NodeAssociator.badGroup") )
			msg.append("\n\n")
			msg.append(	String.format( CentralCatalogue.getUIS("NodeAssociator.badGroup.listHeading"), target.getName() ) )
			msg.append("\n")
			for ( String s : badNames ){
				msg.append(s)
				msg.append("\n")
			}
			log.info( msg.toString() , "message")
			return
		}
  */
		//and now we're good!
		try{
			undoBuffer.startOperation()
			for( AssociationController ac : acs ){
				ac.associate(parent)
			}
		} catch (Exception ex) {
			log.error("Error while performing association!", "message")
			log.error(log.getStackTrace(ex))
			undoBuffer.rollbackOperation()
		} finally {
			undoBuffer.finishOperation()

		}


	}
}