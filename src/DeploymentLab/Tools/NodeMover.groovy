package DeploymentLab.Tools

import DeploymentLab.CentralCatalogue
import DeploymentLab.Model.ComponentRole
import DeploymentLab.Model.DLComponent
import DeploymentLab.SceneGraph.DeploymentNode
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.SceneGraph.StaticChildrenNode.StaticChildNode
import DeploymentLab.SelectModel
import DeploymentLab.UndoBuffer
import DeploymentLab.channellogger.Logger
import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.event.PDragSequenceEventHandler
import edu.umd.cs.piccolo.event.PInputEvent
import edu.umd.cs.piccolo.nodes.PPath
import java.awt.geom.Line2D
import java.awt.geom.Point2D
import java.awt.geom.Rectangle2D

class NodeMover extends PDragSequenceEventHandler {
	private static final Logger log = Logger.getLogger(NodeMover.class.getName())

	private nodeList = []
	private UndoBuffer undoBuffer
	private Point2D startPt
	private DeploymentPanel deploymentPanel
	private SelectModel selectModel

	def centerMap = [:]

	private boolean modal

	ToolManager manager

	public NodeMover(UndoBuffer ub, DeploymentPanel dp, SelectModel sm) {
		undoBuffer = ub
		deploymentPanel = dp
		selectModel = sm
		this.setMinDragStartDistance(CentralCatalogue.getApp("tools.minDragDistance").toInteger())
	}

	public boolean shouldStartDragInteraction(PInputEvent e) {
		if ( ! super.shouldStartDragInteraction(e) ){
			return false
		}
		if(e.getPickedNode() instanceof DeploymentNode && e.isLeftMouseButton()){
			return true
		}
		return false
	}

	public void startDrag(PInputEvent e) {
		super.startDrag(e)
	}


	public void drag(PInputEvent e) {
		super.drag(e)

		if(startPt == null) {
			undoBuffer.startOperation()
			centerMap.clear()
			if(!selectModel.isSelected(e.getPickedNode().getComponent())) {
				selectModel.setSelection([e.getPickedNode().getComponent()])
			}
			nodeList = selectModel.getExpandedSelection()
			// todo: What's going on here?
			nodeList.each{
				centerMap[it] = new Point2D.Double(it.getPropertyValue('x'), it.getPropertyValue('y'))
				PNode node = deploymentPanel.getPNodeFor(it)
				if( node instanceof DeploymentNode ) {
					node.setDragging( true )
				}
			}
			startPt = CentralCatalogue.getDeploymentPanel().scalePoint(e.getPosition())
		}
		//log.trace("drag")
		moveNodes(startPt,  CentralCatalogue.getDeploymentPanel().scalePoint(e.getPosition()), nodeList, centerMap)
	}

	/**
	 * Moves a selection of nodes from one location to another.
	 * Called from drag(), but also available to other bits of the program that need to move things around programmatically.
	 * @param startingPoint the point to calculate the move from
	 * @param goHere the point to move the nodes to
	 * @param nodeList the list of nodes to move
	 * @param centerMap a map of components : center locations
	 * @see #drag(PInputEvent)
	 */
	public void moveNodes( Point2D startingPoint, Point2D goHere, List<DLComponent> nodeList, Map<DLComponent, Point2D> centerMap ){
		double xDelta = goHere.x - startingPoint.x
		double yDelta = goHere.y - startingPoint.y
		//log.trace("move $startingPoint to $goHere delta of ($xDelta, $yDelta)")
		nodeList.each{ component ->

			if (component.hasClass('placeable')) {

				Point2D p = new Point2D.Double(centerMap[component].x + xDelta, centerMap[component].y + yDelta)

                // staticchild component should move by following parent's guide line
                if(component.hasClass('staticchild')){
                    log.trace("static child moving")
	                p = getPossiblePosition( p )
                    StaticChildNode staticChildNode = (StaticChildNode)deploymentPanel.getPNodeFor(component)
					//.offset() handles setting the correct component property values
                    staticChildNode.offset(p, true)

                } else if( component.hasRole( ComponentRole.ARBITRARY_SHAPE ) ||
                           component.hasRole( ComponentRole.RECTANGLE_SHAPE ) ) {
	                def node = deploymentPanel.getNodeFor( component )
	                p = getPossiblePosition( p, node.getPath() )

	                component.setPropertyValue('x', p.x)
	                component.setPropertyValue('y', p.y)

/* todo: Don't check overlapping shapes right now until we can improve it since usability is really poor.
	                float deltaX = (float) (p.x - (Double)component.getPropertyValue('x'));
	                float deltaY = (float) (p.y - (Double)component.getPropertyValue('y'));

	                ArbitraryShapeNode pnode = deploymentPanel.getPNodeFor(component)
	                ArrayList<Point2D> points = new ArrayList<Point2D>();

	                for(Point2D point:pnode.points){
		                points.add(new Point2D((float)point.getX() + deltaX, (float)point.getY()+ deltaY));
	                }

	                if (validatePoints(points, component)){
		                component.setPropertyValue('x', p.x)
		                component.setPropertyValue('y', p.y)
	                } else {
		                log.error(CentralCatalogue.getUIS("PopupMenu.containedArea.crossedEdges"), "statusbar");
	                }
*/
                } else{
	                p = getPossiblePosition(p)
                    component.setPropertyValue('x', p.x)
                    component.setPropertyValue('y', p.y)
                }

    		}
		}
	}

	private boolean validatePoints(List<Point2D> points, DLComponent dlc){
		def lines = []
		for (int i = 0; i < points.size() - 1; i++) {
			if (i == 0) {
				lines.add(new Line2D.Double(points.get(points.size() - 1), points.get(0)))
			}
			lines.add(new Line2D.Double(points.get(i), points.get(i + 1)))
		}

		boolean result = true
		selectModel.getActiveDrawing().getChildComponents().each {
			if (it.hasClass('arbitraryshaped') && !it.equals(dlc)){
				def polygon = deploymentPanel.getPNodeFor(it)
				for (Line2D l : lines){
					if (polygon.intersects(l)){
						result = false;
						break
					}
				}
			}
		}

		return result
	}
/**
	 * Moves the nodes currently selected in this NodeMover's selectModel from startingPoint to goHere.
	 * @param startingPoint the location to start the move from
	 * @param goHere the location to move 'startingPoint' to
	 */
	public void moveNodes( Point2D startingPoint, Point2D goHere ){
		def localCenterMap = [:]
		def localNodeList = selectModel.getExpandedSelection()
		localNodeList.each{localCenterMap[it] = new Point2D.Double(it.getPropertyValue('x'), it.getPropertyValue('y'))}
		moveNodes(startingPoint, goHere, localNodeList, localCenterMap)
	}

	/**
	 * Moves the nodes currently selected in this NodeMover's selectModel to the center of the view.
	 */
	public void moveNodesToCenter(){
		//find the center of the screen
		Point2D goHere = CentralCatalogue.getDeploymentPanel().scalePoint(deploymentPanel.getViewCenter())

		//get the center of the currently selected nodes
		def localCenterMap = [:]
		def localNodeList = selectModel.getExpandedSelection()
		localNodeList.each{localCenterMap[it] = new Point2D.Double(it.getPropertyValue('x'), it.getPropertyValue('y'))}
		Point2D startingPoint = DeploymentPanel.findCenterPoint( localCenterMap.values().toList() )
		//move
		moveNodes(startingPoint, goHere, localNodeList, localCenterMap)
	}

	public void endDrag(PInputEvent e) {
		super.endDrag(e)
		if(startPt == null)
			return
		//log.trace("endDrag")
		nodeList.each { component ->
			PNode node = deploymentPanel.getPNodeFor(component)
			if( node instanceof DeploymentNode ) {
				node.setDragging( false )
			}

			// todo: What's going on here?!?! is this just to trigger a room member evaluation?
			if (component.getType().equals("room")) {
				component.setPropertyValue("points", component.getPropertyValue("points"))
			}
		}
		undoBuffer.finishOperation()

		startPt = null
	}

	/**
	 * Insures that a moving node will stay on the drawing.
	 *
	 * This now includes "round to nearest integer", and any other tool that moves nodes around should shell out to this
	 * method to make sure that the target point is "legal."
	 *
	 * @param p the point that we want to move a node to
	 * @return the nearest point to p that's still on on the drawing
	 */
	protected Point2D getPossiblePosition(Point2D p, PPath path = null) {
		Point2D possibleP = p.clone()

		double minX = 0.0
		double minY = 0.0
		double maxX = deploymentPanel.getBlankCanvasWidth() * selectModel.getActiveDrawing().getScale()
		double maxY = deploymentPanel.getBlankCanvasHeight() * selectModel.getActiveDrawing().getScale()

		// If desired, take the full shape into consideration, not just the center point.
		if( path != null ) {
			Rectangle2D pbounds = path.getPathReference().getBounds2D()
			double xOffset = pbounds.getWidth() / 2
			double yOffset = pbounds.getHeight() / 2

			minX += xOffset
			maxX -= xOffset

			minY += yOffset
			maxY -= yOffset
		}

		if(p.getX() < minX){
			possibleP.x = minX
		}

		if(p.getY() < minY){
			possibleP.y = minY
		}

		if(p.getX() > maxX){
			possibleP.x = maxX
		}

		if(p.getY() > maxY){
			possibleP.y = maxY
		}

		//todo: this is the hook for "snap to grid"
		//possibleP = deploymentPanel.findNearestGridIntersectionTo(possibleP)

		//round to nearest integer:
		possibleP.x = Math.rint(possibleP.x)
		possibleP.y = Math.rint(possibleP.y)

		return possibleP
	}

	void activate(boolean isModal = true) {
		modal = isModal
	}
	void deactivate() {
		centerMap = [:]
	}
}

