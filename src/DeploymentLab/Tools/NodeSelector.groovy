package DeploymentLab.Tools

import edu.umd.cs.piccolo.*

import edu.umd.cs.piccolo.event.*

import DeploymentLab.SceneGraph.DeploymentNode
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.SelectModel

import DeploymentLab.channellogger.*

class NodeSelector extends PBasicInputEventHandler {
	private static final Logger log = Logger.getLogger(NodeSelector.class.getName())

	private boolean modal

	private SelectModel selectModel
	private DeploymentPanel deploymentPanel

	NodeSelector(DeploymentPanel dp, SelectModel sm) {
		selectModel = sm
		deploymentPanel = dp
	}

	public void mouseClicked(PInputEvent e) {
		super.mouseClicked(e)
		if(!e.isLeftMouseButton())
			return

		PNode node = e.getPickedNode()
		
		if(node instanceof DeploymentNode) {
			if(e.isControlDown()) {
				selectModel.toggleSelect([node.getComponent()])
			} else {
				selectModel.setSelection([node.getComponent()])
	        }
		} else {

            if(! e.isControlDown()){
                selectModel.setSelection([])
			}
		}
	}

	void activate(boolean isModal = true) {
		modal = isModal
	}
	void deactivate() {}
}
