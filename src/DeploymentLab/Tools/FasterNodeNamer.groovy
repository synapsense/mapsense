package DeploymentLab.Tools

import DeploymentLab.CentralCatalogue
import DeploymentLab.Dialogs.DocumentCRFilter
import DeploymentLab.Model.ComponentType
import DeploymentLab.Model.DLComponent
import DeploymentLab.SceneGraph.DeploymentNode
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.SceneGraph.IconNode
import DeploymentLab.UIDisplay
import DeploymentLab.UndoBuffer
import DeploymentLab.channellogger.Logger
import edu.umd.cs.piccolo.PCanvas
import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.event.PBasicInputEventHandler
import edu.umd.cs.piccolo.event.PInputEvent
import edu.umd.cs.piccolo.util.PBounds
import edu.umd.cs.piccolox.event.PStyledTextEventHandler
import edu.umd.cs.piccolox.nodes.PStyledText

import java.awt.Font
import java.awt.event.FocusEvent
import java.awt.event.FocusListener
import java.awt.event.KeyEvent
import java.awt.event.KeyListener
import javax.swing.event.ChangeEvent
import javax.swing.event.ChangeListener
import javax.swing.event.DocumentListener
import javax.swing.text.*

class FasterNodeNamer extends PBasicInputEventHandler {
	private static final Logger log = Logger.getLogger(FasterNodeNamer.class.getName())
	private DeploymentPanel deploymentPanel
	private boolean modal
	private PStyledTextEventHandler nodeNamerHandler
	private PStyledText nameStyledText
	private StyledDocument nameDocument
	private SimpleAttributeSet nameAttr
	private PNode node
	private String oldValue = ''
	ToolManager manager
	private UIDisplay uiDisplay
	private UndoBuffer undoBuffer

	FasterNodeNamer(DeploymentPanel dp, UIDisplay ud, UndoBuffer ub) {
		deploymentPanel = dp
		uiDisplay = ud
		undoBuffer = ub
	}

	private boolean setStyledText() {
		if (node instanceof IconNode) {
			def DLComponent c = node.getComponent()
			oldValue = c.getPropertyValue('name')
			//log.trace("parent fullbound before adding child" + node.getFullBounds().toString())
			PBounds textGroupBound = node.getTextNode().getFullBounds()

			//log.trace("zoom factor" + deploymentPanel.getZoom())
			//log.trace("drawing scale" +1/deploymentPanel.getDrawingScale())

			double zoomFactor = deploymentPanel.getZoom() * 1 / deploymentPanel.getDrawingScale()

			StyleContext context = new StyleContext()
			nameDocument = new DefaultStyledDocument(context)
			nameDocument.setDocumentFilter(new DocumentCRFilter());

			nameAttr = new SimpleAttributeSet()

			StyleConstants.setBold(nameAttr, true)
			StyleConstants.setFontSize(nameAttr, getFontSize(zoomFactor))
			StyleConstants.setFontFamily(nameAttr, Font.SANS_SERIF)
			setText(oldValue)

			nameStyledText = new PStyledText()
			nameStyledText.setDocument(nameDocument)
			nameStyledText.setBounds(textGroupBound.getX(), textGroupBound.getY(), textGroupBound.getWidth(), textGroupBound.getHeight())
			//println "text group global bounds: " + node.getTextNode().getGlobalBounds()
			//println "camera view bounds: ${deploymentPanel.getCameraViewBounds()}"
			//log.trace("font size:" + getFontSize(zoomFactor).toString())
			//log.trace("StyleConstants size:" + StyleConstants.getFontSize(nameAttr).toString())
			//log.trace("zoomFactor:" + zoomFactor)
			node.addChild(nameStyledText)

			//log.trace("nameStyledText fullbounds"+ nameStyledText.getFullBounds().toString())

			//correct for name boxes that will hang off the screen
			//for nodes that will not be entirely visible, compute some new bounds and
			//move the node.
			//Note that this needs to happen *after* the node is childed to it's parent and has text added for two reasons:
			//first) we don't know how big it will be until the text is added
			//second) before it's been childed, Piccolo doesn't know how to convert the node's bounds to the global coordinate space
			//(this all means that on a failure we have to remove it and add it back.)
			if (!deploymentPanel.checkIfInView(nameStyledText)) {
				def newBounds = deploymentPanel.getBoundsInView(nameStyledText)
				//log.trace("new bound:" + newBounds.toString())
				node.removeChild(nameStyledText)
				nameStyledText.setBounds(newBounds)
				node.addChild(nameStyledText)

				//let's try shrinking the font?
				if (!deploymentPanel.checkIfInView(nameStyledText)) {
					//log.trace("attempting to shrink the font")
					node.removeChild(nameStyledText)
					StyleConstants.setFontSize(nameAttr, getFontSize(zoomFactor, 10))
					//log.trace("font size:" + getFontSize(zoomFactor, 10).toString())
					setText(oldValue)
					nameStyledText = new PStyledText()
					nameStyledText.setDocument(nameDocument)
					nameStyledText.setBounds(textGroupBound.x, textGroupBound.y, textGroupBound.width, textGroupBound.height)
					node.addChild(nameStyledText)
					def newnewBounds = deploymentPanel.getBoundsInView(nameStyledText)
					node.removeChild(nameStyledText)
					nameStyledText.setBounds(newnewBounds)
					node.addChild(nameStyledText)

					if (!deploymentPanel.checkIfInView(nameStyledText)) {
						//log.trace("shrink the font DOUBLE DOWN")
						node.removeChild(nameStyledText)
						StyleConstants.setFontSize(nameAttr, getFontSize(zoomFactor, 8))
						setText(oldValue)
						nameStyledText = new PStyledText()
						nameStyledText.setDocument(nameDocument)
						nameStyledText.setBounds(textGroupBound.x, textGroupBound.y, textGroupBound.width, textGroupBound.height)
						node.addChild(nameStyledText)
						def newnewnewBounds = deploymentPanel.getBoundsInView(nameStyledText)
						node.removeChild(nameStyledText)
						nameStyledText.setBounds(newnewnewBounds)
						node.addChild(nameStyledText)

						//check again, and then give up if we have to
						if (!deploymentPanel.checkIfInView(nameStyledText)) {
							log.error(CentralCatalogue.getUIS("FasterNodeNamer.tooBig"), "statusbar")
							closeNodeNamer()
							//node.removeChild(nameStyledText)
							//node.toggleName(true)
							//node = null
							//activePanZoomer()
							return false
						}
					}
				}
			}

			nodeNamerHandler = new NamePStyledTextEventHandler(deploymentPanel.getCanvas(), nameStyledText, this, node, undoBuffer)
			nodeNamerHandler.setDocumentListener(nodeNamerHandler.createDocumentListener())
		}
		return true
	}

	/**
	 * If we add another letter, will the faster namer go off the screen?
	 * If we can't, stop editing and complain.
	 * Called from the caret change listener in the editor.
	 *
	 * @see NamePStyledTextEventHandler#stateChanged
	 *
	 */
	public void checkNextLetter() {
		log.trace("checking for the next letter after this")
		nameStyledText.syncWithDocument()

		String existing = nameStyledText.getDocument().getText(0, nameStyledText.getDocument().getLength())
		//why "M"? - by convention, the capital M is the widest letter in a font; hence the em-dash
		existing += "M"

		//println "longer text is :$existing"
		StyleContext context = new StyleContext()
		def longerDocument = new DefaultStyledDocument(context)
		def attr = new SimpleAttributeSet()
		StyleConstants.setBold(attr, true)
		StyleConstants.setFontSize(attr, getFontSize(deploymentPanel.getZoom()))
		StyleConstants.setFontFamily(attr, Font.SANS_SERIF)
		longerDocument.insertString(0, existing, attr);

		//PBounds textGroupBound = node.getTextNode().getFullBounds()
		//println "textgroupbounds = ${deploymentPanel.printBounds(textGroupBound)}"
		//def existingBounds = nameStyledText.getBounds()
		//println "existing bounds = ${deploymentPanel.printBounds(existingBounds)}"


		def longerText = new PStyledText()
		longerText.setDocument(longerDocument)
		//longerText.setBounds(textGroupBound.x, textGroupBound.y, textGroupBound.width, textGroupBound.height)
		//longerText.setBounds(existingBounds)
		longerText.setX(nameStyledText.getX() + (nameStyledText.getWidth() / 2))
		longerText.setY(nameStyledText.getY())

		node.addChild(longerText)

		//println "real version:"
		//deploymentPanel.checkIfInView(nameStyledText)

		//println "longer version:"
		if (!deploymentPanel.checkIfInView(longerText, false)) {
/*
			log.trace("moving faster namer over!")
			def newBounds = deploymentPanel.getBoundsInView(longerText,true)

			println "nameStyledText x = ${nameStyledText.getX()}"
			println "new x is ${newBounds.getX()}"
			//node.removeChild(nameStyledText)
			nameStyledText.setBounds(newBounds)
			nodeNamerHandler.editedText.setBounds(newBounds)
			//nameStyledText.getBounds().setOrigin(newBounds.getOrigin().getX(), newBounds.getOrigin().getY())
			//def off = nameStyledText.getOffset()
			//nameStyledText.setOffset(newBounds.getOrigin())
			//nameStyledText.setX( nameStyledText.getX() - 10 )
			//nameStyledText.setY(longerText.getY())

			//node.addChild(nameStyledText)
			//nameStyledText.animateToBounds(newBounds.getX(), newBounds.getY(), newBounds.getWidth(), newBounds.getHeight(), 1)

			deploymentPanel.refresh()
*/
			node.removeChild(longerText)
			//log.info("bailing on edit, no more space")
			this.save()
			this.closeNodeNamer()
			log.warn(CentralCatalogue.getUIS("FasterNodeNamer.tooBig"), "statusbar")
		} else {
			node.removeChild(longerText)
		}


	}

	private int getFontSize(double zoomFactor) {
		int fontSize = 15 * zoomFactor
		if (fontSize < 15)
			fontSize = 15
		return fontSize
	}

	private int getFontSize(double zoomFactor, int baseFontSize) {
		int fontSize = baseFontSize * zoomFactor
		if (fontSize < baseFontSize)
			fontSize = baseFontSize
		return fontSize
	}

	private void setText(String value) {
		try {
			nameDocument.remove(0, nameDocument.getLength())
			nameDocument.insertString(0, value, nameAttr);
		} catch (BadLocationException e) {
			log.error(e.getMessage())
		}
	}

	private void restoreText() {
		setText(oldValue)
	}

	private void deactivePanZoomer() {
		manager.suspendTools()
		manager.setActiveTools(["fasternamer"])
		uiDisplay.setDisplay('zoomIn', false, true)
		uiDisplay.setDisplay('zoomOut', false, true)
		uiDisplay.setDisplay('zoom11', false, true)
		uiDisplay.setDisplay('setZoom', false, true)
	}

	private void activePanZoomer() {
		manager.resumeTools()
		uiDisplay.setDisplay('zoomIn')
		uiDisplay.setDisplay('zoomOut')
		uiDisplay.setDisplay('zoom11')
		uiDisplay.setDisplay('setZoom')
	}

	public void openNodeNamer(PInputEvent e) {

		if (node instanceof IconNode) {
			deactivePanZoomer()
			node.toggleName(false)
			if (setStyledText()) {
				nodeNamerHandler.startEditing(e, nameStyledText)
			}

		}
	}

	public void closeNodeNamer() {
		if (node != null && node instanceof IconNode) {
			node.removeChild(nameStyledText)
			node.toggleName(true)
			node = null
			activePanZoomer()
			nodeNamerHandler = null
		}
	}

	public void save() {
		if (nodeNamerHandler != null) {
			if (!nodeNamerHandler.saveExiting())
				cancel()
		}
	}

	public void cancel() {
		if (node != null) {
			restoreText()
			nodeNamerHandler.stopEditing()
		}
	}

	public void mouseClicked(PInputEvent e) {
		super.mouseClicked(e)

		PNode pickedNode = e.getPickedNode()

		if (e.isLeftMouseButton()) {
			if (node != null && node instanceof IconNode) {
				save()
				closeNodeNamer()
			}

			if (pickedNode instanceof IconNode) {
				if (pickedNode.component.type.equals(ComponentType.DRAWING_TEXT)){
					save()
					closeNodeNamer()
					return
				}
				if (!e.isControlDown() && e.getClickCount() == 2) {
					node = pickedNode
					openNodeNamer(e)
				}
			} else {
				save()
				closeNodeNamer()
			}
		}
	}

	public void mousePressed(PInputEvent e) {
		if (e.isRightMouseButton()) {
			save()
			closeNodeNamer()
		}
	}

	void activate(boolean isModal = true) {
		modal = isModal
	}

	void deactivate() {
	}
}

/**
 * Handles editing the text inside the FasterNodeNamer.
 * @see FasterNodeNamer
 */
public class NamePStyledTextEventHandler extends PStyledTextEventHandler implements ChangeListener {
	private static final Logger log = Logger.getLogger(NamePStyledTextEventHandler.class.getName())
	private FasterNodeNamer fasterNamer
	private PStyledText editorText
	private DeploymentNode deploymentNode
	private UndoBuffer undoBuffer

	NamePStyledTextEventHandler(PCanvas canvas, PStyledText text, FasterNodeNamer fn, DeploymentNode dn, UndoBuffer ub) {
		super(canvas)
		editorText = text
		fasterNamer = fn
		deploymentNode = dn
		undoBuffer = ub
		this.editor.addKeyListener(new EditorKeyListener(fasterNamer))
		this.editor.addFocusListener(new EditorFocusListener(this.editor, fasterNamer))
		this.editor.getCaret().addChangeListener(this)
	}

	void startEditing(PInputEvent event, PStyledText text) {
		super.startEditing(event, text)

	}

	void setDocumentListener(DocumentListener dl) {
		this.docListener = dl
	}

	boolean saveExiting() {
		Document doc = editorText.getDocument()
		def DLComponent c = deploymentNode.getComponent()
		//handles the case where "Ctrl-Z" has pulled the component out from under us, and we just need to get out of here without crashing
		if (c == null) { return true }
		String newName = doc.getText(0, doc.getLength())

		if (newName == '' || doc.getLength() == 0) {
			log.error('Name is required.', 'message')
			return false
		} else {
			stopEditing()

			try {
				undoBuffer.startOperation()
				c.setPropertyValue('name', newName)
			} catch (Exception ex) {
				log.error("Error while renaming!", "message")
				log.error(log.getStackTrace(ex))
				undoBuffer.rollbackOperation()
			} finally {
				undoBuffer.finishOperation()
				return true
			}
		}
	}

	/**
	 * Stores the last location of the caret's Dot while the cursor is moving back and forth.
	 */
	Integer lastDot = null;

	/**
	 * Called when the editor's caret changes.  This should be called after the text has been updated, so if the letter
	 * the user just typed blows away and replaces the text, this will happen afterwards.
	 * @see FasterNodeNamer#checkNextLetter
	 * @see javax.swing.text.Caret
	 *
	 * @param e the Caret's ChangeEvent
	 */
	@Override
	void stateChanged(ChangeEvent e) {
		//println "state changed $e"
		//println "src = " + e.getSource().getClass().getName()
		DefaultCaret c = e.getSource() as DefaultCaret
		if (lastDot == null) {
			lastDot = c.getDot()
		}
		//println "$lastDot ${c.getDot()}"
		//check for space if we're moving forward
		if (c.getDot() > lastDot) {
			//println "moving forward"
			fasterNamer.checkNextLetter()
		}
		lastDot = c.getDot()
	}


}

public class EditorKeyListener implements KeyListener {
	private static final Logger log = Logger.getLogger(EditorKeyListener.class.getName())
	private FasterNodeNamer fasterNamer

	EditorKeyListener(FasterNodeNamer fn) {
		fasterNamer = fn
	}

	void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			fasterNamer.cancel()
			fasterNamer.closeNodeNamer()
		} else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			fasterNamer.save()
			fasterNamer.closeNodeNamer()
		} else if (e.getKeyCode() == KeyEvent.VK_Z && e.isControlDown()) {
			fasterNamer.cancel()
			fasterNamer.closeNodeNamer()
		}
	}

	void keyReleased(KeyEvent e) {}

	void keyTyped(KeyEvent e) {}
}

public class EditorFocusListener implements FocusListener {
	private static final Logger log = Logger.getLogger(EditorFocusListener.class.getName())
	private JTextComponent editorText
	private FasterNodeNamer fasterNamer

	EditorFocusListener(JTextComponent text, FasterNodeNamer fn) {
		editorText = text
		fasterNamer = fn
	}

	void focusGained(FocusEvent e) {
		editorText.selectAll()
	}

	void focusLost(FocusEvent e) {
		log.trace("FasterNamer focus lost.", "default")
		fasterNamer.save()
		fasterNamer.closeNodeNamer()
	}
}