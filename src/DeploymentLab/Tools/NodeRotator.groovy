package DeploymentLab.Tools

import DeploymentLab.CentralCatalogue
import DeploymentLab.SceneGraph.RectangleNode
import DeploymentLab.SceneGraph.TextNode

import java.awt.geom.Point2D

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.event.*;


import DeploymentLab.SceneGraph.DeploymentNode
import DeploymentLab.SceneGraph.OrientedDeploymentNode
import DeploymentLab.SelectModel

class NodeRotator extends PBasicInputEventHandler {

	private DeploymentNode deploymentNode

	private boolean modal

	private SelectModel selectModel

	private double nodeX
	private double nodeY

	//now in degrees
	//private gravityPoints = [-180.0, -135.0, -90.0, -45.0, 0.0, 45.0, 90, 135, 180 ]
	private gravityPoints = [0.0, 45.0, 90.0, 135.0, 180.0, 225.0, 270.0, 315.0, 360.0]
	private double gravity = 22.5

	PNode node

	NodeRotator(SelectModel sm) {
		deploymentNode = null
		selectModel = sm
	}

	public void mouseClicked(PInputEvent e) {
		super.mouseClicked(e)
		if (modal && !e.isLeftMouseButton())
			return

		node = e.getPickedNode()

		if (deploymentNode == null) {
			if (!(node instanceof OrientedDeploymentNode) && !(node instanceof RectangleNode) && !(node instanceof TextNode)) {
				return
			}
			deploymentNode = (DeploymentNode) node
			selectModel.setSelection([node.getComponent()])
			nodeX = deploymentNode.getComponent().getPropertyValue('x')
			nodeY = deploymentNode.getComponent().getPropertyValue('y')
			//e.setHandled(true)
		} else { // second click, done rotating
			deploymentNode = null
			selectModel.setSelection([])
			e.setHandled(true)
		}
	}

	public void mouseMoved(PInputEvent e) {
		super.mouseMoved(e)
		node = e.getPickedNode()

		if (deploymentNode != null) {
			Point2D pt = CentralCatalogue.getDeploymentPanel().scalePoint(e.getPosition())
			double r = calcAngle(pt.getX() - nodeX, pt.getY() - nodeY)
			if (!e.isControlDown()) {
				gravityPoints.each { it -> if (it - gravity < r && r < it + gravity) r = it }
			}
			//we never want 360 degrees when it's back to the starting point, so coerce that to zero
			if (r == 360) {
				r = 0
			}
			deploymentNode.getComponent().setPropertyValue('rotation', r)
			//e.setHandled(true)
		}
	}

	// Returns an angle in range -Pi, Pi
	protected double calcAngleRads(double x, double y) {
		if (y == 0.0) {
			if (x >= 0.0)
				return 0.0
			else
				return Math.PI
		} else if (x == 0.0) {
			if (y >= 0.0)
				return Math.PI / 2
			else
				return -Math.PI / 2
		}

		double len = Math.sqrt(x * x + y * y)

		if (y < 0)
			return -Math.acos(x / len)
		else
			return Math.acos(x / len)
	}

	/**
	 * Returns an angle in degrees in range 0,360
	 * @param x
	 * @param y
	 * @return
	 */
	protected double calcAngle(double x, double y) {
		//convert the return value of calcAngleRads, which is in the
		//range of -PI to PI to degrees from 0 to 360.
		//For bonus trouble, we also want to rotate the opposite direction from that method
		def rads = this.calcAngleRads(x, y)
		def degs = Math.toDegrees(-rads)
		if (degs < 0) {
			degs += 360
		}
		if (degs >= 360) {
			degs -= 360
		}
		return degs
	}

	void activate(boolean isModal = true) {
		modal = isModal
	}

	void deactivate() {
		deploymentNode = null
	}
}

