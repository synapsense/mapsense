package DeploymentLab

import java.text.NumberFormat
import java.text.ParsePosition
import DeploymentLab.channellogger.Logger
import DeploymentLab.Model.ComponentProperty
import com.synapsense.util.unitconverter.ConverterException
import com.synapsense.util.unitconverter.UnitConverter
import com.synapsense.util.unitconverter.Dimension
import DeploymentLab.Model.PropertyConverter

/**
 * Centralized location for localization logic and related helper methods. Using this class instead of the language
 * localization allows us to tweak how we handle localization without affecting the code that needs localization.
 *
 * @author Ken Scoggins
 * @since  Jupiter
 */
class Localizer {

    private static final Logger log = Logger.getLogger(Localizer.name)

    // Local constants
    private static final String USE_GROUPING_PROP       = 'localization.numbers.useGroupingCharacter'
    private static final String MIN_FRAC_DIGITS_PROP    = 'localization.numbers.minFractionalDigits'
    private static final String MAX_FRAC_DIGITS_PROP    = 'localization.numbers.maxFractionalDigits'
    private static final int    DEFAULT_MIN_FRAC_DIGITS = 0
    private static final int    DEFAULT_MAX_FRAC_DIGITS = 2

    // Singleton reference to the Localizer class.
    private static final Localizer INSTANCE = new Localizer()

    private Locale currentLocale = Locale.default
    private NumberFormat numberFormat = NumberFormat.numberInstance


    /**
     * Private constructor. Public instances are retreived through {@link #getInstance} and not using the constructor.
     */
    private Localizer() {
        initFormatters()
        log.info( String.format( CentralCatalogue.getUIS('localizer.changeLocale'),
                                 currentLocale, currentLocale.displayLanguage,
                                 currentLocale.displayCountry, currentLocale.displayVariant ) )
    }

    /**
     * Initializes the formatters based on the configured application style.
     */
    private void initFormatters() {

        // Parse boolean never returns an error and defaults to false if the value is anything but the "true" values.
        numberFormat.setGroupingUsed( Boolean.parseBoolean( CentralCatalogue.getApp( USE_GROUPING_PROP ) ) )

        int minFracDig = DEFAULT_MIN_FRAC_DIGITS
        try {
            minFracDig = Integer.parseInt( CentralCatalogue.getApp( MIN_FRAC_DIGITS_PROP ) )
        } catch( NumberFormatException e ) {
            log.error( String.format( CentralCatalogue.getUIS('properties.app.badValueWithDefault'),
                                      MIN_FRAC_DIGITS_PROP, CentralCatalogue.getApp( MIN_FRAC_DIGITS_PROP ),
                                      DEFAULT_MIN_FRAC_DIGITS ) )
        }
        numberFormat.setMinimumFractionDigits( minFracDig )

        int maxFracDig = DEFAULT_MAX_FRAC_DIGITS
        try {
            maxFracDig = Integer.parseInt( CentralCatalogue.getApp( MAX_FRAC_DIGITS_PROP ) )
        } catch( NumberFormatException e ) {
            log.error( String.format( CentralCatalogue.getUIS('properties.app.badValueWithDefault'),
                                      MAX_FRAC_DIGITS_PROP, CentralCatalogue.getApp( MAX_FRAC_DIGITS_PROP ),
                                      DEFAULT_MAX_FRAC_DIGITS ) )
        }
        numberFormat.setMaximumFractionDigits( maxFracDig )
    }

    /**
     * Change the locale used by the <code>Localizer</code> class. The locale returned by {@link Locale#getDefault} is
     * used by default. Therefore, there is no need to call this method explicitly except to use something other than
     * the system default locale or if the system default is changed after the construction of the
     * <code>Localizer</code> instance.
     *
     * Calling this method does not change the system default locale, only the locale used by the <code>Localizer</code>
     * class.
     *
     * @param locale  The new locale to use or null to reset locale to the current system default as returned by
     *                {@link Locale#getDefault}.
     */
     void changeLocale( Locale locale ) {

         if( locale == null ) {
             locale = Locale.default
         }

         if( locale != currentLocale ) {
             log.info( String.format( CentralCatalogue.getUIS('localizer.changeLocale'),
                                      locale, locale.displayLanguage,
                                      locale.displayCountry, locale.displayVariant ) )

             // Get new formatter instances with a locale specified this time.
             numberFormat = NumberFormat.getNumberInstance( locale )

             // Re-initialize the formatters.
             initFormatters()

             currentLocale = locale
         }
     }

    /**
     * Returns the current locale being used.
     *
     * @return  The current Locale.
     */
    Locale getLocale() {
        return currentLocale
    }

    /**
     * Returns the maximum number of digits beyond the decimal place that the localizer will use to format strings.
     *
     * @return  Maximum digits beyond the decimal.
     */
    int getMaximumFractionalDigits() {
        return numberFormat.maximumFractionDigits
    }

    /**
     * Returns true if grouping is used in this format. For example, in the English locale, with grouping on, the number
     * 1234567 might be formatted as "1,234,567". The grouping separator as well as the size of each group is locale
     * dependant and is determined by sub-classes of NumberFormat.
     */
    boolean isGroupingUsed() {
        return numberFormat.isGroupingUsed()
    }

    /**
     * Returns an instance of the Localizer class.
     *
     * @return  A Localizer instance.
     */
    static Localizer getInstance() {
        return INSTANCE
    }

    /**
     * Determines if a component property is one that should use localization. This version should be used over the
     * other isLocalizable methods in this class since it checks other component properties that affect localization
     * and not just the property data type.
     *
     * @param comp  The component to test for localization.
     * @return      True if the component is a type that should be localized, false otherwise.
     */
    static boolean isLocalizable( ComponentProperty comp ) {
        // Comboboxes are not localizable regardless of the underlying value class, which might be an integer.
        if( ( comp != null ) && ( comp.valueChoices == null ) ) {
            return isLocalizable( comp.type )
        }
        return false
    }

    /**
     * Determines if a component property is one that should use localization. This method only checks the class name
     * and should only be used if the full <code>ComponentProperty</code> is not available. The best method to use is
     * {@link Localizer#isLocalizable(ComponentProperty)}.
     *
     * @param className  The class name of the property data type, not a string representaion of ComponentProperty itself.
     *
     * @return  True if the class is a type that should be localized, false otherwise.
     */
    static boolean isLocalizable( String className ) {
        if( className ) {
            return PropertyConverter.isLocalizable( className )
        }
        return false
    }

    /**
     * Determines if a component property is one that should use localization. This method only checks the class name
     * and should only be used if the full <code>ComponentProperty</code> is not available. The best method to use is
     * {@link Localizer#isLocalizable(ComponentProperty)}.
     *
     * @param clazz  The data property class, not the ComponentProperty class itself.
     *
     * @return  True if the class is a type that should be localized, false otherwise.
     */
    static boolean isLocalizable( Class clazz ) {
        if( clazz ) {
            return isLocalizable( clazz.name )
        }
        return false
    }
    
    /**
     * Properly format a number for the application locale.
     *
     * @param value  The number to format.
     * @return       The formatted version of the provided number if the conversion is successful, otherwise the input
     *               argument is returned untouched, including when IllegalArgumentExceptions occur during the format.
     */
    static def format( def value ) {
        def result = value
        try {
            result = Localizer.instance.numberFormat.format( value )
        } catch( IllegalArgumentException e ) {
            // Only time this is expected is when a blank string is used when multiple Number components are selected
            // with different prop values and the occasional unset field that comes through as a null. Only warn for
            // other cases since it may be a sign that something is wrong. Don't propagate the exception, though.
            if( (value != null) && (value != '') ) {
                log.warn("Format failed for '$value' as ${value?.class}")
            }
        }
        return result
    }

    /**
     * Correctly parses a number based on the application locale. The locale-safe parsers do not throw a format
     * exception like the native Number parsers, such as Double.parseDouble(). This method will throw an exception
     * when the input string is not a valid format in order to mimic the other parsers.
     *
     * @param str  A String containing the number representation to be parsed.
     *
     * @return     The number value represented by the string argument.
     *
     * @throws NumberFormatException  If the string does not contain a parsable number.
     */
    static Number parse( String str ) throws NumberFormatException {

        // A little wonkiness to handle some NumberFormat.parse stupidity.
        ParsePosition pp = new ParsePosition(0)
        def val = Localizer.instance.numberFormat.parse( str, pp )

        if( (val == null) || (str.length() != pp.index) ) {
            throw new NumberFormatException( String.format( CentralCatalogue.getUIS('localizer.parseError'), str ) )
        }
        return val
    }

    /**
     * Correctly parses a number based on the application locale and returns it as a native double value. The
     * locale-safe parsers do not throw a format exception like the native Number parsers, such as Double.parseDouble().
     * This method will throw an exception when the input string is not a valid format in order to mimic the other
     * parsers.
     *
     * @param str  A String containing the double representation to be parsed.
     *
     * @return     The double value represented by the string argument.
     *
     * @throws NumberFormatException  If the string does not contain a parsable double.
     */
    static double parseDouble( String str ) throws NumberFormatException {
        return Localizer.parse( str ).doubleValue()
    }

    /**
     * Correctly parses a number based on the application locale and returns it as a native int value. The
     * locale-safe parsers do not throw a format exception like the native Number parsers, such as Integer.parseInt().
     * This method will throw an exception when the input string is not a valid format in order to mimic the other
     * parsers.
     *
     * @param str  A String containing the integer representation to be parsed.
     *
     * @return     The int value represented by the string argument.
     *
     * @throws NumberFormatException  If the string does not contain a parsable integer.
     */
    static int parseInt( String str ) throws NumberFormatException {
        return Localizer.parse( str ).intValue()
    }

    /**
     * Correctly parses a number based on the application locale and returns it as a native float value. The
     * locale-safe parsers do not throw a format exception like the native Number parsers, such as Double.parseDouble().
     * This method will throw an exception when the input string is not a valid format in order to mimic the other
     * parsers.
     *
     * @param str  A String containing the float representation to be parsed.
     *
     * @return     The float value represented by the string argument.
     *
     * @throws NumberFormatException  If the string does not contain a parsable float.
     */
    static float parseFloat( String str ) throws NumberFormatException {
        return Localizer.parse( str ).floatValue()
    }

    /**
     * Correctly parses a number based on the application locale and returns it as a native long value. The
     * locale-safe parsers do not throw a format exception like the native Number parsers, such as Integer.parseInt().
     * This method will throw an exception when the input string is not a valid format in order to mimic the other
     * parsers.
     *
     * @param str  A String containing the long representation to be parsed.
     *
     * @return     The long value represented by the string argument.
     *
     * @throws NumberFormatException  If the string does not contain a parsable long.
     */
    static long parseLong( String str ) throws NumberFormatException {
        return Localizer.parse( str ).longValue()
    }

    /**
     * Convenience method for converting to the target unit system. If no system converter is present, the value is
     * returned without being converted.
     *
     * @param dimension  The unit type of the value to be converted.
     * @param val        The value to convert.
     *
     * @return  The converted value in the target unit system if a system converter exists, otherwise the unmodified {@code val}.
     *
     * @throws ConverterException  if the provided dimension is not found within the unit system used by the converter.
     */
    static double convert( def val, String dimension ) throws ConverterException {
        if( CentralCatalogue.instance.systemConverter != null ) {
            UnitConverter conv = CentralCatalogue.instance.systemConverter.getConverter( dimension )
            if( conv == null ) {
                // getUnit behaves this way. Not sure why getConverter doesn't. So ours will for consistency,
                String msg = String.format( CentralCatalogue.getUIS('localizer.badDimension'), dimension,
                                            CentralCatalogue.instance.systemConverter.targetSystemName )
                throw new ConverterException( msg )
            }
            return conv.convert( val )
        }
        return val
    }

    /**
     * Convenience method for converting from the target unit system back to the base unit system. If no system
     * converter is present, the value is returned without being converted.
     *
     * @param dimension  The unit type of the value to be converted.
     * @param val        The value to convert.
     *
     * @return  The converted value in the base unit system if a system converter exists, otherwise the unmodified {@code val}.
     *
     * @throws ConverterException  if the provided dimension is not found within the unit system used by the converter.
     */
    static double convertFrom( def val, String dimension ) throws ConverterException {
        if( CentralCatalogue.instance.reverseConverter != null ) {
            UnitConverter conv = CentralCatalogue.instance.reverseConverter.getConverter( dimension )
            if( conv == null ) {
                // getUnit behaves this way. Not sure why getConverter doesn't. So ours will for consistency.
                String msg = String.format( CentralCatalogue.getUIS('localizer.badDimension'), dimension,
                                            CentralCatalogue.instance.reverseConverter.targetSystemName )
                throw new ConverterException( msg )
            }
            return conv.convert( val )
        }
        return val
    }

    /**
     * Convenience method for retreiving the name of the units for the provided unit type. The caller can optionally
     * specify that the full name be returned. By default, the abbreviated name is returned. For example, the default
     * for US Imperial temperature would be "F" while the long name would return "Farenheit".
     *
     * @param dimension  The unit type of interest.
     * @param longName   (Optional) Returns the full name if true, otherwise the abbreviated name is returned.
     *
     * @return  The unit name of the provided dimension.
     *
     * @throws ConverterException  if the provided dimension is not found within the unit system used by the converter.
     */
    static String getUnits( String dimension, boolean longName = false ) throws ConverterException {
        Dimension dim   = null
        String    units = ''

        if( CentralCatalogue.instance.systemConverter == null ) {
            dim = CentralCatalogue.instance.defaultUnitSystem.getDimension( dimension )
        } else {
            dim = CentralCatalogue.instance.systemConverter.targetSystem.getDimension( dimension )
        }

        if( longName ) {
            units = dim.longUnits
        } else {
            units = dim.units
        }

        return units
    }
}
