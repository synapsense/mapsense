package DeploymentLab.Import

import DeploymentLab.CentralCatalogue
import DeploymentLab.DLProject
import DeploymentLab.FileFilters
import DeploymentLab.Model.AccessModel
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentProperty
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLObject
import DeploymentLab.Model.Dlid
import DeploymentLab.Model.ObjectModel
import DeploymentLab.ProgressManager
import DeploymentLab.SelectModel
import DeploymentLab.UserPreferences
import DeploymentLab.channellogger.Logger
import com.opencsv.CSVReader

import javax.swing.JFileChooser
import javax.swing.JFrame
import java.awt.Component

/**
 * Created with IntelliJ IDEA.
 * User: vadgusev
 * Date: 08.02.13
 * Time: 12:57
 * To change this template use File | Settings | File Templates.
 */
class DLObjectValueImporter {
    private static final Logger log = Logger.getLogger(DLObjectValueImporter.class.getName())
    private static String filename;

    public static void importComponent(List<String[]> components, ComponentModel componentModel, boolean macOnly = false) {
        for (String[] component : components) {
            if (macOnly && !component[3].contains("MAC ID")) continue
            if (component[3].contains("MAC ID") && component[4].isEmpty()) { // check this because mac id has a string type
                log.info("Skip empty `MAC ID` property value of `" + component[2] + "`")
                continue
            }

            DLComponent dlc = componentModel.getComponentFromDLID(Dlid.getDlid(component[0]))

            ComponentProperty cp = dlc.getComponentProperty(dlc.getPropertyNameFromDisplayName(component[3]))
            String type = cp.getType()
            if (type.equals(Double.class.getName()) || type.equals(Float.class.getName())) {
                component[4] = component[4].replace(",", ".")
            }

            if (!component[4].isEmpty() || type.equals(String.class.getName())) {
                try {
                    dlc.setPropertyValue(dlc.getPropertyNameFromDisplayName(component[3]), component[4])
                } catch (NumberFormatException e) {
                    log.error("Incorrect value format for " + cp.getDisplayName())
                }
            } else {
                log.info("Skip empty non-string `" + component[3] + "` property value of `" + component[2] + "`")
            }
        }

        log.info("Values imported from '$filename'", "statusbar")

    }

    public static void importMacScanList(DLProject openProject, List<String[]> scanList) {
        AccessModel am = openProject.accessModel
        if (am){
	        ProgressManager.doTask {
		        log.info("Importing MAC ID Scan List...", "progress")
		        am.importScanList(scanList)
	        }
        }
    }

    public static void importAccessList(DLProject openProject, SelectModel selectModel, List<String[]> accessList, JFrame parent) {
        AccessModel am = openProject.getAccessModel()
        am.importAccessList(accessList, parent)
    }

    public static List<String[]> importFromCSV(String filename) {
        this.filename = filename
        CSVReader reader
        List<String[]> result = new ArrayList<String[]>()
        try {
            reader = new CSVReader(new FileReader(filename))
            result = reader.readAll()
        } catch (Exception e) {
            log.error(String.format(CentralCatalogue.getUIS("importCSV.readerror"), e.getMessage()), "message")
            log.error(log.getStackTrace(e))
        } finally {
            if (reader) {
                reader.close()
            }
        }

        return result
    }

    public static List<String[]> importTable(UserPreferences userPreferences, Component parent, String title) {
        def importFileChooser = new JFileChooser()
        importFileChooser.addChoosableFileFilter(FileFilters.CSVFile.getFilter())
        importFileChooser.setFileFilter(FileFilters.CSVFile.getFilter())
        importFileChooser.setDialogTitle(title)

        importFileChooser.setCurrentDirectory(new File(userPreferences.getUserPreference(UserPreferences.CSV)))

        int rv = importFileChooser.showOpenDialog(parent)
        if (rv == JFileChooser.APPROVE_OPTION) {
            String selectedPath = importFileChooser.getSelectedFile().getPath()

            File selectedFile = new File(selectedPath)
            userPreferences.setUserPreference(UserPreferences.CSV, selectedFile.getParent())

            return importFromCSV(selectedPath)
        }

    }
}
