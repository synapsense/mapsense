package DeploymentLab.Import

import DeploymentLab.Exceptions.UnknownComponentException
import DeploymentLab.FileIo.BadHeaderRowException
import DeploymentLab.FileIo.Excel.BadSheetNumberException
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentProperty
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.Dlid
import DeploymentLab.channellogger.Logger
import DeploymentLab.FileIo.FlatFileRow
import DeploymentLab.FileIo.Excel.ExcelReader
import DeploymentLab.FileIo.Excel.ExcelSheetReader

import java.awt.Color
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * @author Vadim Gusev
 * @since Europa
 * Date: 11.04.13
 * Time: 11:52
 */
class DLComponentValueImporter {
	private static final Logger log = Logger.getLogger(DLComponentValueImporter.class.getName())
	private static ComponentModel cmodel

	private static final String[] DEFAULT_PROPERTIES = ["DLID", "Drawing Name", "Zone Name", "Network Name", "Component Type"]

	public static void importComponents(ComponentModel cmodel, Map<String, List<String[]>> data) {
		this.cmodel = cmodel
		for (Map.Entry<String, List<String[]>> e : data.entrySet()) {
			log.info("Importing ${e.getKey()}...", "progress")

			String[] header = e.value.get(0)
			if (!isGoodHeader(header)){
				log.warn("Sheet '${e.key}' has a bad header. Skip this sheet")
				continue
			}
			for (int i = 1; i < e.value.size(); i++) {
				String[] row = e.value.get(i);
				parseRow(header, row)
			}
		}
	}

	private static boolean isGoodHeader(String[] header){
		for(int i = 0; i< DEFAULT_PROPERTIES.length ; i++){
			if (!DEFAULT_PROPERTIES[i].equals(header[i])){
				return false
			}
		}
		return true
	}

	private static void parseRow(String[] header, String[] row) {
		String dlid = row[0];
		DLComponent dlc;
		if (dlid == null || dlid.isEmpty()) {
			if (row[4] != null && !row[4].isEmpty()) {
				log.trace("Create new component ${row[4]}")
				dlc = createNewComponent(row)
			}
		} else {
			if (Dlid.isDlid(dlid)){
				Dlid d = Dlid.getDlid(dlid)
				log.trace("Update component $d")
				dlc = cmodel.getComponentFromDLID(d)
			} else {
				log.warn("Incorrect DLID: '$dlid'. Skip the line")
				return
			}
		}

		if (dlc) {
			for (int i = 5; i < header.size(); i++) {
				String propName = dlc.getPropertyNameFromDisplayName(header[i])
				if (!propName){
					log.warn("Incorrect property name. Skip this '${header[i]}' : '${row[i]}'")
					continue
				}
				if (row[i]){
					setPropertyValue(dlc, propName, row[i])
				}
			}
		}
	}

	private static void setPropertyValue(DLComponent dlc, String propName, String value) {
		ComponentProperty cp = dlc.getComponentProperty(propName)

		if (!value.isEmpty() || cp.type.equals(String.class.getName())) {

			def v
			try {
				v = checkPropertyValue(cp, value)
			} catch (Exception e2){
				log.error("Unable to parse property '$propName' : '$value'")
				return
			}

			if (!cp.value.equals(v)) {
				log.info("Different prop values: $dlc.dlid Prop: ${propName} oldVal: ${cp.value.toString()} newVal: ${v.toString()}")
				try {
					dlc.setPropertyValue(propName, v)
				} catch (NumberFormatException e) {
					log.error("Incorrect value format for " + cp.getDisplayName())
				} catch (Exception e1) {
					log.error("Unable to set property value! Prop: $propName Value: ${v.toString()}")
				}
			}
		} else {
			log.info("Skip empty non-string `" + propName + "` property value of `" + dlc.getName() + "`")
		}
	}

	private static def checkPropertyValue(ComponentProperty cp, String value) {
		String type = cp.getType();

		switch (type) {
			case Integer.class.getName():
				Double dValue = Double.valueOf(value);
				if (dValue == Math.round(dValue)) {
					return dValue.intValue();
				}
				throw new NumberFormatException("Cannot convert " + dValue + " to integer");
				break;

			case Long.class.getName():
				Double dValue = Double.valueOf(value);
				if (dValue == Math.round(dValue)) {
					return dValue.longValue();
				}
				throw new NumberFormatException("Cannot convert " + dValue + " to long");
				break;

			case Double.class.getName():
				return Double.valueOf(value);
				break;

			case Color.class.getName():
				return parseColor(value)
				break;

			case Boolean.class.getName():
				return value.equals("true");
				break;
		}

		return value;
	}

	private static Color parseColor(String input) {
		Pattern c = Pattern.compile("java.awt.Color\\[r=([0-9]+),g=([0-9]+),b=([0-9]+)\\]");
		Matcher m = c.matcher(input);

		if (m.matches()) {
			return new Color(Integer.valueOf(m.group(1)),  // r
					Integer.valueOf(m.group(2)),  // g
					Integer.valueOf(m.group(3))); // b
		}

		throw new Exception("Unable to parse color: $input")
	}

	private static DLComponent createNewComponent(String[] row){
		def drawing
		def zone
		def network

		def parentTypes = cmodel.listParentComponentsOfType(row[4])
		def parentRoles = parentTypes.collect{ cmodel.listComponentClasses(it).flatten() }.flatten().unique()

		if('network' in parentRoles) {
			network = cmodel.getComponentsInRole('network').find{
				it.getName().equals(row[3])
			}
			if (!network){
				log.error("Incorrect Network Name: ${row[3]}")
				return null
			}
		}
		if(('zone' in parentRoles) || ('set' in parentRoles) ) {
			zone = cmodel.getComponentsInRole('zone').find{
				it.getName().equals(row[2])
			}
			if (!zone){
				zone = cmodel.getComponentsInRole('set').find{
					it.getName().equals(row[2])
				}
			}
			if (!zone){
				log.error("Incorrect Zone Name: ${row[2]}")
				return null
			}
		}
		if('drawing' in parentRoles) {
			drawing = cmodel.getComponentsInRole('drawing').find{
				it.getName().equals(row[1])
			}
			if (!drawing){
				log.error("Incorrect Drawing Name: ${row[2]}")
				return null
			}
		}

		DLComponent dlc
		try {
			dlc = cmodel.newComponent(row[4])
		}catch (UnknownComponentException e)  {
			log.error("Unknown component type ${row[4]}")
			return null
		}

		drawing?.addChild(dlc)
		zone?.addChild(dlc)
		network?.addChild(dlc)
		return dlc
	}

	public static Map<String, List<String[]>> importFromExcel(String fileName) {
		log.info("Reading Excel File...", "progress")
		Map<String, List<String[]>> result = new HashMap<>()
		ExcelSheetReader sheetReader
		int sheetNum = 1
		while(1){
			try{
				sheetReader = new ExcelSheetReader(fileName, sheetNum)
				break
			} catch (BadHeaderRowException e1){
				log.warn("Unable to get header row at sheet #$sheetNum. Skip the sheet")
				sheetNum++
			} catch (BadSheetNumberException e2){
				break
			}
		}
		if (!sheetReader){
			return result
		}
		while (sheetReader.hasMoreSheets()) {
			List<String[]> sheetResult = new ArrayList<>()
			ExcelReader reader = sheetReader.nextSheet()
			if (!reader) {
				log.warn("Unable to get header row at sheet '${sheetReader.sheetName}'. Skip the sheet")
				continue
			}
			String sheetName = sheetReader.sheetName
			log.trace("Loading $sheetName")
			def headers = reader.headers
			sheetResult.add(headers.toArray(new String[headers.size()]))
			while (reader.hasMoreRows()) {
				String[] row = new String[headers.size()]
				FlatFileRow fileRow = reader.nextRow()
				int i = 0;
				for (String header : headers) {
					row[i] = fileRow.getStringValue(header)
					log.trace("[$i] $header: ${row[i]}")
					i++
				}
				sheetResult.add(row)
				log.trace(row.toString())
			}
			result.put(sheetName, sheetResult)
		}

		return result
	}
}
