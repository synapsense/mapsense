package DeploymentLab.Import

import DeploymentLab.Model.ObjectModel
import DeploymentLab.CentralCatalogue
import DeploymentLab.Model.ComponentModel
import DeploymentLab.DLProject
import DeploymentLab.Tools.ConfigurationReplacer
import DeploymentLab.UndoBuffer
import DeploymentLab.Model.ModelState
import DeploymentLab.Model.DLComponentCloner
import DeploymentLab.Model.DLComponent
import DeploymentLab.ComponentLibrary
import DeploymentLab.SelectModel
import DeploymentLab.channellogger.Logger

/**
 * Holds all the sauce to import a project from another DL file.
 * Moved here from the methods that used to be in DLW
 * @author Gabriel Helman
 * @author Hyeeun Lim
 * @since Jupiter 2
 * Date: 10/1/12
 * Time: 10:37 AM
 */
class ImportProjectController implements Closeable {
	private static final Logger log = Logger.getLogger(ImportProjectController.name)

	private DLProject activeProject
	private ComponentModel componentModel
	private ObjectModel objectModel
	private SelectModel selectModel
	private UndoBuffer undoBuffer
	private def configurationReplacerTree

	private ObjectModel iObjectModel
	private ComponentModel iComponentModel

	public boolean esKeyCollision

	public ImportProjectController( DLProject ap, SelectModel sm, def crt) {
		activeProject = ap
		componentModel = activeProject.getComponentModel()
		objectModel = activeProject.getObjectModel()
		selectModel = sm
		undoBuffer = activeProject.getUndoBuffer()
		configurationReplacerTree = crt
	}

	@Override
	void close() {
		iObjectModel = null
		iComponentModel = null
		esKeyCollision = false
	}
/**
	 * Import project by chosen project file
	 * @param f
	 */
	public void doImportProject(File f, boolean forceUpdate = false) {
		def objectsXml = ComponentLibrary.parseXmlFile( CentralCatalogue.getApp("objects.definitions.main") )
		def modelXml = ComponentLibrary.parseXmlFile('cfg/commoncomponents.xml')

		// create new instances for objectModel and componentModel
		iObjectModel = new ObjectModel(objectsXml)
		CentralCatalogue.getApp("objects.definitions.extra").split(",").each {
			iObjectModel.addTypes( ComponentLibrary.parseXmlFile( it.trim() ) )
		}
		iComponentModel = new ComponentModel(iObjectModel, modelXml)
		iComponentModel.setCLib(ComponentLibrary.INSTANCE)
		//move over the loaded component types
		iComponentModel.getComponentTypes().putAll( componentModel.getComponentTypes() )


		DLProject importedPrj = null
		ConfigurationReplacer iConfigurationReplacer = new ConfigurationReplacer(selectModel, iComponentModel, configurationReplacerTree, undoBuffer, CentralCatalogue.getParentFrame())
		try {
			// include validation of project file. No AOE Controller for upgrades since it there's no DeploymentPanel with these components so it won't work.
			//todo: a whole new undo buffer?  WHAT?
			DLProject realOpenProject = CentralCatalogue.getOpenProject()
			importedPrj = new DLProject(f, new UndoBuffer(iComponentModel,selectModel), iConfigurationReplacer, iComponentModel, iObjectModel)

			if(forceUpdate){
				iConfigurationReplacer.replaceEverything( true, false );
			}

			// haxxerz: The system really needs this to be set for upgrades and loding files, but we want to put it
			// back to the real project afterward. This exposes a bit of a flaw with the OpenProject concept.
			CentralCatalogue.setOpenProject(realOpenProject)

		} catch (Exception e) {
			// cannot load the file
			def message = "The file '${f.getPath()}' could not be imported:\n${e.getMessage()}"
			log.error(message, "message")
			log.error( log.getStackTrace(e) )
			if( importedPrj != null ) {
				importedPrj.close()
			}
			return
		}

		// make sure the project is one we can handle
		if( !canImportProject() ) {
			importedPrj.close()
			return
		}

		log.info(CentralCatalogue.getUIS("progress.drawing.importing"), "progress")

		// check ES key collision
		esKeyCollision = isESKeyCollision(iObjectModel)
		if (esKeyCollision) {
			// clear ES key
			try {
				iObjectModel.clearKeys()
				iComponentModel.resetImages()
			} catch (Exception e) {
				log.error("Clearing Object IDs failed during importing project!\nSee log file for details.", 'message')

				log.error(log.getStackTrace(e))
			}
		}

		//log.trace("info about imported componentModel")
		//iComponentModel.getComponentsByType("drawing").each{it->log.trace(it.toString())}

		// clone components to current model
		// update DL ID
		try {
			componentModel.metamorphosisStarting(ModelState.INITIALIZING)

			DLComponentCloner componentCloner = new DLComponentCloner(iComponentModel, componentModel, iObjectModel, objectModel)
			componentCloner.cloneAll()

			componentModel.metamorphosisFinished(ModelState.INITIALIZING)

			// inform ES keys were cleared
			log.trace("esKeyCollision:" + esKeyCollision)
			if (esKeyCollision) {
				log.trace("key collides")
				log.info(CentralCatalogue.getUIS("importProject.clearESkeys"), "message")
			} else {
				log.trace("import drawing done ")
				log.info(CentralCatalogue.getUIS("importProject.success"), "message")

			}

			//set active datacener
			DLComponent activeDrawing = componentCloner.getActiveDrawing()
			selectModel.setActiveDrawing(activeDrawing)


		} catch (Exception e) {
			log.trace("cannot import project")
			log.error(log.getStackTrace(e))
			log.error(CentralCatalogue.getUIS("importProject.fail"), "message")
		} finally {
			importedPrj.close()
		}

	}

	private boolean isESKeyCollision(ObjectModel iObjectModel) {
		//def objectsMap = [:]
		boolean result = false
		iObjectModel.getObjects().each {o ->
			if (o.getKey() != '') {
				log.trace("check es key collision:" + o.getKey())
				def collision = objectModel.getObjects().findAll {it -> it.getKey().equals(o.getKey())}
				log.trace("collision.size():" + collision.size())
				if (collision.size() > 0) {
					log.trace("collision.size()>0")

					result = true
				}
			}
		}
		return result
	}

	/**
	 * checks deleted objects exist without exporting
	 * @param iObjectModel
	 * @return
	 */
	private boolean canImportProject() {
		// Make sure all the components are valid in the real model. Cloner doesn't handle deprecated components well.
		StringBuilder badTypes = new StringBuilder()
		for( DLComponent c : iComponentModel.getComponents() ) {
			if( !componentModel.isLoadedComponentType( c.getType() ) ) {
				if( badTypes.length() != 0 ) {
					badTypes.append(', ')
				}
				badTypes.append( c.getPropertyStringValue('configuration') )
			}
		}

		if( badTypes.length() > 0 ) {
			log.error(CentralCatalogue.formatUIS("importProject.unsupportedComponents", badTypes.toString()), "message")
			return false
		}

		// check if deleted objects exist without export
		def deletedObjects = iObjectModel.getObjects().findAll {o -> o.getDeleted() && o.getKey() == ""}
		if( deletedObjects.size() > 0 ) {
			log.error(CentralCatalogue.getUIS("importProject.unexportedDeletedObject"), "message")
			return false
		}

		return true
	}

}
