package DeploymentLab

/**
 * Dirt simple stopwatch that returns number of seconds elapsed between instantiation and finish().
 * @author Gabriel Helman
 * Date: Nov 30, 2010
 * Time: 1:45:51 PM
 */
class DLStopwatch {

	private Calendar start
	private Calendar stop
	private BigDecimal delta

	DLStopwatch(){
		start = Calendar.getInstance()
	}

	public BigDecimal finish(){
		stop = Calendar.getInstance()
		delta = (stop.getTimeInMillis() - start.getTimeInMillis()) / 1000
		//log.info("Upgrading time = ${(upgradeFinish.getTimeInMillis() - upgradeStart.getTimeInMillis()) / 1000} seconds" )
		return delta
	}

	public String finishMessage(){
		stop = Calendar.getInstance()
		delta = (stop.getTimeInMillis() - start.getTimeInMillis()) / 1000
		return( "time = ${(stop.getTimeInMillis() - start.getTimeInMillis()) / 1000} seconds" )
	}

	public String finishMessage(String operationName){
		stop = Calendar.getInstance()
		delta = (stop.getTimeInMillis() - start.getTimeInMillis()) / 1000
		return( "$operationName time = ${(stop.getTimeInMillis() - start.getTimeInMillis()) / 1000} seconds" )
	}


}
