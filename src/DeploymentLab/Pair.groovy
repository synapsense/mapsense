
package DeploymentLab;

public class Pair<S, T> {
	public final S a;
	public final T b;

	public Pair(S _a, T _b) {
		a = _a;
		b = _b;
	}

	public boolean equals(Object o) {
		if(! o.getClass().getName().equals(getClass().getName()))
			return false;
		Pair p = (Pair)o;
		return a.equals(p.a) && b.equals(p.b);
	}

	@Override
	public int hashCode() {
		return a.hashCode() + (b.hashCode() >> 1);
	}

	public String toString(){
		return "PAIR A:$a B:$b"
	}
}

