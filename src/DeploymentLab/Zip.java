package DeploymentLab;

import DeploymentLab.channellogger.Logger;

import java.io.*;
import java.util.*;
import java.util.zip.*;


/**
 * Zip file utility class.
 *
 * @author Gabriel Helman
 * @since Mars
 * Date: 3/22/11
 * Time: 11:34 AM
 */
public class Zip {
	static final int BUFFER = 2048;
	private static final Logger log = Logger.getLogger(Zip.class.getName());


	/**
	 * Constructs a zip file based on the contents provided.
	 *
	 * @param destination the File to zip the contents to.
	 * @param filesToSave a List of {@code <Triplet<String, byte[], Integer>}.
	 *                    The three values are the zip entry name, an array of bytes to be stored under that name,
	 *                    and the storage method.  Note that as of Phobos, methods other than ZipEntry.DEFLATED are
	 *                    NOT supported, and this parameter will be removed in a future release.
	 *
	 */
	public static void store(File destination, List<Triplet<String, byte[], Integer>> filesToSave) {
		try {
			FileOutputStream dest = new FileOutputStream(destination);
			CheckedOutputStream checksum = new CheckedOutputStream(dest, new Adler32());
			ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(checksum));
			for (Triplet<String, byte[], Integer> p : filesToSave) {
				ZipEntry entry = new ZipEntry(p.a);
				entry.setMethod(ZipEntry.DEFLATED);
				out.putNextEntry(entry);
				out.write(p.b);
			}
			out.close();
			//System.out.println("checksum:" + checksum.getChecksum().getValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private static byte[] createZip(Map files) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ZipOutputStream zipfile = new ZipOutputStream(bos);
		Iterator i = files.keySet().iterator();
		String fileName = null;
		ZipEntry zipentry = null;
		while (i.hasNext()) {
			fileName = (String) i.next();
			zipentry = new ZipEntry(fileName);
			zipfile.putNextEntry(zipentry);
			zipfile.write((byte[]) files.get(fileName));
		}
		zipfile.close();
		return bos.toByteArray();
	}

	public static void load(File sourceFile) {

		try {
			BufferedOutputStream dest = null;
			BufferedInputStream is = null;
			ZipEntry entry;
			ZipFile zipfile = new ZipFile(sourceFile);

			Enumeration e = zipfile.entries();
			while (e.hasMoreElements()) {
				entry = (ZipEntry) e.nextElement();
				System.out.println("Extracting: " + entry);
				is = new BufferedInputStream(zipfile.getInputStream(entry));
				int count;
				byte data[] = new byte[BUFFER];
				FileOutputStream fos = new
						FileOutputStream(entry.getName());
				dest = new
						BufferedOutputStream(fos, BUFFER);
				while ((count = is.read(data, 0, BUFFER)) != -1) {
					dest.write(data, 0, count);
				}
				dest.flush();
				dest.close();
				is.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * Pull an entry from a zip file.
	 *
	 * @param zipFile   a File representing a Zip Archive somewhere in the file system
	 * @param entryName the name of the entry (or really, file) to pull out of the zip archive
	 * @return an InputStream representing the file inside the zip archive
	 */
	public static InputStream getStreamFromZip(File zipFile, String entryName) {
		ZipEntry entry = new ZipEntry(entryName);
		InputStream zis = null;
		try {
			ZipFile zf = new ZipFile(zipFile);
			zis = zf.getInputStream(entry);
		} catch (ZipException e) {
			log.error("Unable to load " + entryName + " from zip file " + zipFile.getName(), "default");
			log.error(log.getStackTrace(e), "default");
		} catch (IOException e) {
			log.error("Unable to load " + entryName + " from zip file " + zipFile.getName(), "default");
			log.error(log.getStackTrace(e), "default");
		}
		return zis;
	}


	/**
	 * Convert a zip file into a map of the contents.
	 * <p/>
	 * Borrowed from the ES side of the DLIS
	 *
	 * @param config a byte[] holding a zipped file
	 * @return a map of entry name : byte[] of contents
	 */
	public static Map<String, byte[]> parseConfiguration(byte[] config) throws IOException {
		//log.trace("Zip to Map", "default");
		Calendar start = Calendar.getInstance();
		Map<String, byte[]> res = new HashMap<String, byte[]>();
		int numberOfBytesToBeRead = 1024 * 500;
		ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new ByteArrayInputStream(config), numberOfBytesToBeRead));
		ZipEntry zipEntry;
		while ((zipEntry = zis.getNextEntry()) != null) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] buf = new byte[4096];
			int r;
			while ((r = zis.read(buf)) != -1) {
				baos.write(buf, 0, r);
			}
			byte[] buffer = baos.toByteArray();
			log.trace("Unzipped " + zipEntry.getName() + "(" + buffer.length + " bytes)", "default");
			res.put(zipEntry.getName(), buffer);
		}
		Calendar stop = Calendar.getInstance();
		double time = ((stop.getTimeInMillis() - start.getTimeInMillis()) / 1000.0);
		log.trace("unzip time was sec=" + time, "default");
		return res;
	}

}
