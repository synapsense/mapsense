package DeploymentLab.Integration;

import java.util.List;

/**
 * Stores a result from the syntax checker
 * @author Gabriel Helman
 * @since Hermes
 *
 * @see Evaluator
 */
	public class Result {
		private boolean passed;
		private String message;

		public Result(boolean passed, String message) {
			this.passed = passed;
			this.message = message;
		}

		public Result(boolean passed, String message, List<String> errors) {
			this.passed = passed;
			this.message = message;
		}

		public boolean getPassed() {
			return passed;
		}

		public String getMessage() {
			return message;
		}
	}
