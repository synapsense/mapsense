package DeploymentLab.Integration;

import org.antlr.runtime.*;

public class Evaluator {

	public static Result evaluate(String expr) {
		return evaluate(expr, 64);
	}

	public static Result evaluate(String expr, Integer maxMessage) {

		if (expr.trim().length() == 0) {
			return new Result(false, "<html>Empty expression!</html>");
		}

		try {
			CharStream cs = new ANTLRStringStream(expr);
			ErlExprLexer lexer = new ErlExprLexer(cs);
			CommonTokenStream tokens = new CommonTokenStream();
			tokens.setTokenSource(lexer);
			ErlExprParser parser = new ErlExprParser(tokens);
			ErlExprParser.erlexprs_return result = parser.erlexprs();
			return new Result(true, "<html>Syntax OK!</html>");
			//} catch(MismatchedTokenException e) {
			//} catch(MissingTokenException e) {
			//} catch(UnwantedTokenException e) {
		} catch (RecognitionException e) {
			if (e.line <= 0 || e.getUnexpectedType() == Token.EOF ) {
				return new Result(false, "<html>Expression incomplete, ends abruptly.</html>");
			}

			StringBuilder sb = new StringBuilder();
			Token errTok = e.token;
			String inputLine = expr.split("\n\r?")[e.line - 1];
			sb.append("<html>Error on line " + e.line + "<br>");
			sb.append(buildMessage(inputLine.substring(0, errTok.getCharPositionInLine()), inputLine.substring(errTok.getCharPositionInLine(), errTok.getCharPositionInLine() + errTok.getText().length()), inputLine.substring(errTok.getCharPositionInLine() + errTok.getText().length()), maxMessage));
			sb.append("</html>");
			return new Result(false, sb.toString());

		} catch (IllegalArgumentException ex) {
			RecognitionException e = (RecognitionException) ex.getCause();
			StringBuilder sb = new StringBuilder();
			String inputLine = expr.split("\n\r?")[e.line - 1];
			sb.append("<html>Error on line " + e.line + "<br>");
			sb.append(buildMessage(inputLine.substring(0, e.charPositionInLine), inputLine.substring(e.charPositionInLine), "", maxMessage));
			sb.append("</html>");
			return new Result(false, sb.toString());
		}
	}


	private static StringBuilder buildMessage(String first, String bold, String after, Integer maxMessage) {
		StringBuilder sb = new StringBuilder();
		String f = first;
		String b = bold;
		String a = after;
		while (f.length() + b.length() + a.length() > maxMessage) {
			if (a.length() > 0) {
				a = a.substring(0, a.length() - 1);
			} else if (b.length() > 2) {
				b = b.substring(0, b.length() - 1);
			} else if (f.length() > 2) {
				f = f.substring(1);
			}
		}

		sb.append(f);
		sb.append("<b>");
		sb.append(b);
		sb.append("</b>");
		sb.append(a);
		return sb;
	}

}

