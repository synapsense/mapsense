package DeploymentLab.Integration;

import DeploymentLab.channellogger.*;

import java.net.*;

class XMLAuth extends Authenticator {
	private static final Logger log = Logger.getLogger(XMLAuth.class.getName());

	private String username;
	private String pass;

	public XMLAuth(String u, String p) {
		super();
		this.username = u;
		this.pass = p;
	}

	public PasswordAuthentication getPasswordAuthentication() {
		log.debug("Requesting Authentication Scheme is " + getRequestingScheme(), "default");
		// System.out.println( this.username + " " + this.pass );
		return new PasswordAuthentication(username, pass.toCharArray());
	}
}
