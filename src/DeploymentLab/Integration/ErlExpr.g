grammar ErlExpr;

options {
	output = AST;
	ASTLabelType = CommonTree;
}

@lexer::header {
package DeploymentLab.Integration;
}

@header {
package DeploymentLab.Integration;

import java.util.LinkedList;
}

@members {
	protected void mismatch(IntStream input, int ttype, BitSet follow)
		throws RecognitionException
	{
		throw new MismatchedTokenException(ttype, input);
	}

	public Object recoverFromMismatchedSet(IntStream input,
			RecognitionException e,
			BitSet follow)
		throws RecognitionException
	{
		throw e;
	}
	
	protected Object recoverFromMismatchedToken(IntStream input,
			int ttype,
			BitSet follow)
		throws RecognitionException
	{
		throw new MismatchedTokenException(ttype, input);
	}
}

@lexer::members {
	@Override
	public void reportError(RecognitionException e) {
		throw new IllegalArgumentException(e);
	}
}

@rulecatch {
	catch (RecognitionException e) {
		throw e;
	}
}

erlexprs:	erlexpr (',' erlexpr)* EOF
	;

erlexpr :	assignexpr | expr
	;

assignexpr
	:	Var '=' expr
	;

funcall :	Fname '(' arguments ')'
	;

arguments
	:	expr (',' expr)*
	;

expr	:	multexpr (('+' | '-') multexpr)*
	;

multexpr:	modexpr (('*' | '/') modexpr)*
	;

modexpr :	compexpr ('rem' compexpr)*
	;

compexpr
	:	boolexpr (Compop boolexpr)*
	;
	
boolexpr
	:	bitexpr (Boolop bitexpr)*
	;

bitexpr	:	molecule (Bitop molecule)*
	;

molecule:	(unary)? (Var | literal | funcall | number | atom | '(' expr ')')
	;

number 	:	Int | Float | Based
	;

atom	:	'true' | 'false'
	;

unary	:	'bnot' | 'not' | '-'
	;

literal :	StringLiteral
	;


Fname 	:	'read_coil' | 'read_int16' | 'read_int32' | 'read_uint16' | 'read_uint32' | 'read_float32' | 'read_float64'
	|	'write_coil' | 'write_register16' | 'write_register32' | 'write_float32' | 'write_float64'
	|	'read_ai' | 'read_ao' | 'read_av' | 'read_bi' | 'read_bo' | 'read_bv' | 'read_mi'| 'read_mo' | 'read_mv'
	|	'write_ao' | 'write_av' | 'write_bo' | 'write_bv' | 'write_mo' | 'write_mv'
	|	'round' | 'float' | 'trunc' | 'min' | 'max'
	|	'bit' | 'b_to_i' | 'swap_endian16' | 'swap_endian32' | 'swap_endian64'
	|	'i32_to_f' | 'i64_to_f' | 'f_to_i32' | 'f_to_i64'
	;

Boolop	:	'and' | 'or' | 'xor' | 'orelse' | 'andalso'
	;

Compop 	:	'==' | '/=' | '=<' | '<' | '>=' | '>'
	;

Bitop 	:	'band' | 'bor' | 'bxor' | 'bsr' | 'bsl'
	;

Id	:	('a'..'z') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
	;

Var 	:	('A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
	;

Int	:	 '0'..'9'+
	;

Float	:	('0'..'9')+ '.' ('0'..'9')+ Exponent?
	|	'.' ('0'..'9')+ Exponent?
	|	('0'..'9')+ Exponent
	;

Based	:	('2' '#' ('0' | '1')+) | ('16' '#' ('0'..'9'|'A'..'F'|'a'..'f')+)
	;

Ws	:	(' ' | '\t' | '\r' | '\n') {$channel=HIDDEN;}
	;

fragment
Exponent: ('e'|'E') ('+'|'-')? ('0'..'9')+
	;

StringLiteral
    :  '"' ( EscapeSequence | ~('\\'|'"') )* '"'
    ;

fragment
EscapeSequence
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')
    ;

