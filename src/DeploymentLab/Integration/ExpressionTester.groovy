package DeploymentLab.Integration

import DeploymentLab.Export.DebugHelper
import DeploymentLab.FileUtil
import DeploymentLab.channellogger.Logger
import DeploymentLab.CentralCatalogue

/**
 * Tests configurable expressions (Modbus or Bacnet) against a running Shadoww CP.
 * @author Gabriel Helman
 * Date: Nov 11, 2010
 * Time: 1:32:06 PM
 * @since Hermes
 */
class ExpressionTester {

	private static final Logger log = Logger.getLogger(ExpressionTester.class.getName())
	/**
	 * Runs an expression against the actual AC modbus driver and returns the result.
	 * @param controlHost the URI of the CP
	 * @param evalPage the eval service page to call for the expression type;
	 * 		"modbus_eval.yaws" for modbus, "bacnet_eval.yaws" for bacnet.
	 * @param params a map holding key-value pairs to pass to the eval page as query parameters.
	 * Possible params include:
	 *
	 * modbusHost the IP of the modbus gateway
	 * devid the Modbus Device ID
	 * value the value to test write (if needed)
	 * pointName the CP data point this expression will read or write
	 *
	 * @param userName the username to use when authenticating to the eval service
	 * @param pass the password to use when authenticating to the eval service
	 * @return a Map containing the result codes:
	 * "result" : the result code, 'ok' means a pass
	 * "message" : the message returned by the CP about how we ded
	 * "both" : the entire result set formatted together for easy(er) display
	 */
	public static Map<String, String> test(String controlHost, String evalPage, Map<String, String> params, String userName, String pass) {

		StringBuilder sb = new StringBuilder()
		sb.append("${controlHost.trim()}/${evalPage.trim()}?")
		params.each { k, v ->
			sb.append("$k=${URLEncoder.encode(v, "UTF-8")}&")
		}
		String url = new URI(sb.toString()).toURL()

		log.trace("Testing URL $url")

		// Skip the actual call if this is dev and we just want the internal response generator.
		if( DebugHelper.useFakeControlServer ) {
			return getFakeResponse(params, url)
		}

		def reply = getDocumentAsString(url, userName, pass)
		def parser = new XmlParser()
		def xml = parser.parseText(reply)

		return buildResultMap( xml.result.text(), xml.message.text() )
	}

	public static Map<String, String> buildResultMap( String resultStr, String messageStr ) {
		def result = new StringBuilder();

		result.append("Test Result: ")
		result.append( resultStr.toUpperCase() )
		result.append( System.getProperty("line.separator") )
		result.append( messageStr )

        /* Commented this out and added the line above so this doesn't conflict with the text component wrapping.
		String resultMessage = xml.message.text()
		if (resultMessage.length() < 80) {
			result.append(resultMessage)
		} else {
			//drop in some linebreaks
			int len = resultMessage.length()
			String resultPart = resultMessage
			while (len > 0) {
				if (resultPart.length() > 80) {
					result.append(resultPart.substring(0, 80))
					resultPart = resultPart.substring(80)
					len -= 80
					result.append(System.getProperty("line.separator"))
				} else {
					result.append(resultPart)
					len = 0
				}
			}
		}
		*/
		//result.append(System.getProperty("line.separator"));

		Map<String, String> returnCode = [:]
		returnCode["result"] = resultStr
		returnCode["message"] = messageStr
		returnCode["both"] = result.toString().trim()

		return returnCode
	}

	/**
	 * Fetches an arbitrary URL as a String
	 * @param url an addressable URL
	 * @return a String containing the contents of that URL
	 * @throws Exception if something goes haywire
	 */
	public static String getDocumentAsString(String url, String user, String pass) throws Exception {
		URL _url = new URL(url);
		Authenticator.setDefault(new XMLAuth(user, pass));
		URLConnection conn = _url.openConnection();
		conn.setConnectTimeout(CentralCatalogue.getApp("expressionTester.timeout").toInteger())
		log.trace("Content Type for URL '" + url + "' is '" + conn.getContentType() + "' ");
		return (FileUtil.inputStreamToString(conn.getInputStream()));
	}

	/** Sometimes we just wanna get the expressions to pass without setting up a bunch of services to respond. */
	public static Map<String, String> getFakeResponse( Map<String, String> params, String url ) {

		if( params["expr"].contains("write_register32") ) {
			return buildResultMap("ABORT",
			                      "Unknown function 'write_register32' called with arguments [{string,1,\"192.168.201.150\"}, \n" +
			                      "                                                                    {integer,1,20800}, \n" +
			                      "                                                                    {integer,1,1},\n" +
			                      "                                                                    {integer,1,85}]" +
			                      "\nURL:\n$url")
		} else if( params["expr"].contains("read_int32") ){
			return buildResultMap("ABORT",
			                      "Unknown function 'read_int32' called with arguments [{string,1,\"192.168.201.150\"}, \n" +
			                      "                                                              {integer,1,20800}, \n" +
			                      "                                                              {integer,1,1},\n" +
			                      "                                                              {integer,1,85}]" +
			                      "\nURL:\n$url")
		} else if( params["expr"].contains("write_") ){
			return buildResultMap("OK", "Expression Result: \"${params["value"]}\"\nURL:\n$url")
		}

		return buildResultMap("OK", "Expression Result: \"1\"\n" +
		                            "URL:n$url")
	}
}
