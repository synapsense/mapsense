package DeploymentLab;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import DeploymentLab.channellogger.Logger;

public class DeploymentLabVersion {

	public static String getModelVersion(){
		return CentralCatalogue.getApp("model.version");
	}
	
	public static double getModelVersionByDouble(){
		double modelVersion =0.0;
		modelVersion = Double.parseDouble(CentralCatalogue.getApp("model.version"));
		return modelVersion;
	}
	
	public static String getObjectsVersion(){
		return CentralCatalogue.getApp("objects.version");
	}

	public static String getComponentsVersion(){
		return CentralCatalogue.getApp("components.version");
	}
	
	public static String getComponentLibVersion(){
		return CentralCatalogue.getApp("componentlib.version");
	}
	
	public static String getAllowedVersions(){
		return CentralCatalogue.getApp("objects.allowedversions");
	}
}
