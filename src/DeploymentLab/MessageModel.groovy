
package DeploymentLab

import javax.swing.table.DefaultTableModel

import DeploymentLab.channellogger.*
import DeploymentLab.Validator
import DeploymentLab.ValidationResult
import DeploymentLab.Model.*

class MessageModel extends DefaultTableModel {
	private static final Logger log = Logger.getLogger(MessageModel.class.getName())

	final String[] columns = ['Message', 'Objects']
	private data = []
	private Validator validator

	MessageModel() {}

	String getColumnName(int col) {
		return columns[col]
	}

	Class getColumnClass(int col) {
		return String.class
	}

	Object getValueAt(int row, int col) {
		//def validation = data.find{it.index == row}
		def validation = data[row]
		if(validation == null)
			return ''

		if(col==0)
			return validation.getMessage()
		else
			return validation.getObjects()
	}

	int getRowCount() {
		if(data == null)
			return 0
		return data.size()
	}

	int getColumnCount() {
		return columns.size()
	}

	void updateMessage(Validator v) {
		validator = v
		data = []
		def warnings = v.getWarnings()
		def failures = v.getFailures()

		if(failures.size() > 0) {
			failures.inject(''){ s, vr -> data += new Validation(data.size(), vr, 1)}
		}

		if(warnings.size() > 0) {
			warnings.inject(''){ s, vr -> data += new Validation(data.size(), vr, 0)}
		}

		fireTableChanged(null)
	}


	void updateInfoMessage(Validator v) {
		validator = v
		data = []
		def infos = v.getInfos()


		if(infos.size() > 0) {
			infos.inject(''){ s, vr -> data += new Validation(data.size(), vr, 2)}
		}

		fireTableChanged(null)
	}

	Validation getObjectAt(int row) {
		def validation = data.find{it.index == row}
		if(validation == null)
			return null
		return validation
	}
	
	public void sortByColumn(int colIndex, boolean ascending){
		if(colIndex ==0){
			data.sort{it.getMessage()}
		}else{
			data.sort{it.getObjects()}
		}
			
		int i
		
		if(ascending){
			i=0
			data.each{it->it.setIdex(i);i++}
		}else{
			i = data.size()-1
			data.each{it->it.setIdex(i);i--}
			data.reverse(true)
		}
		fireTableChanged(null)
	}
	/**
	 * The pre-export validation results are never editable, so this method is real easy.
	 */
	def boolean isCellEditable(int row, int column) {
		return false
	}

	public ArrayList<String[]> getVisibleData() {
		ArrayList<String[]> result = new ArrayList<String[]>(data.size())
		for (int row = 0; row < this.getRowCount(); row++) {
			String[] line = new String[this.getColumnCount()]
			for (int col = 0; col < this.getColumnCount(); col++) {
				line[col] = this.getValueAt(row, col)
			}
			result.add(line)
		}
		return result
	}

}

public class Validation {
	protected int index
	protected ValidationResult validationResult
	protected int type		// messageType : warning =0, failure =1
	Validation(int idx, ValidationResult vr, int t) {
		index = idx
		validationResult = vr
		type = t
	}

	public ValidationResult getValidationResult(){
		return validationResult
	}
	
	public int getType(){
		return type
	}
	
	public void setIdex(int i){
		index = i
	}
	
	public String getObjects(){
		return validationResult.objects.collect{o ->
			if (o){ o.getName() }else{ "badname" }
		}.join(',')
	}
	
	public String getMessage(){
		return validationResult.message
	}
}
