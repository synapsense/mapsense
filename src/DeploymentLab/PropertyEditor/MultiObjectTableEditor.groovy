package DeploymentLab.PropertyEditor

import DeploymentLab.CentralCatalogue
import DeploymentLab.Localizer
import DeploymentLab.Model.ComponentProperty
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLObject
import com.synapsense.util.unitconverter.SystemConverter
import groovy.swing.SwingBuilder

import java.awt.BorderLayout

import java.awt.Component
import java.awt.Dimension
import java.awt.FlowLayout
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.awt.event.KeyEvent
import javax.swing.table.TableCellEditor
import net.miginfocom.swing.MigLayout
import javax.swing.*

/**
 * Provides an interface for editing a common property across multiple objects in a single component where the value
 * can be different for each object. The example that this was created for is the APC MPDU component, where the
 * component has 72 circuits that can each have a different max power rating.  Prior to this special editor,
 * there would need to be 72 hardcoded component properties and their varbindings in the component property table.
 *
 * The dialog allows the user to enter a default value to avoid the need to fill-in every value when most of them are
 * the same.
 *
 * For consistency with the existing component property to object setting relationship used in MapSense, this editor
 * behaves in a similar manner as the other Lookup/BTU/CFM tables, where the result is an encoded string of key=value
 * pairs that is saved as the value of the component property. The actual object settings are not changed in this class.
 * It is up to the component to provide the varbinding to populate the objects.
 *
 * @author Ken Scoggins
 * @since Ganymede
 */
class MultiObjectTableEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {

	private static final String EDIT = "edit";
	private static final String DEFAULT_KEY = "default";

	private String objectType
	private String labelSetting
	private String keySetting
	private String sortSetting

	private DLComponent parentComponent;
	private ComponentProperty parentProperty

	private JButton button;
	private JDialog dialog;
	private SwingBuilder builder

	private ArrayList<DLObject> sourceObjects
	private ArrayList<JFormattedTextField> fieldList = []
	private JFormattedTextField defaultField

	private int layoutColumns
	private String currentTable
	private String originalTable

	private String rightDimension
	private SystemConverter systemConverter
	private SystemConverter reverseConverter
	private Localizer nf



	/**
	 * Constructor: produces a new Editor connected to a given component.
	 * @param newComponent The component that this property belongs to.  Sets a reference to allow us to edit >1 component property at a time.
	 */
	public MultiObjectTableEditor( DLComponent component, ComponentProperty property ) {

		// TODO: To be generic, we need to pull these from a hidden property or MultiObjectTable property attribute or something.
		this.objectType = "circuit"
		this.keySetting = "num"
		this.labelSetting = "name"
		this.sortSetting = "num"
		this.rightDimension = "dimensionless"
		this.layoutColumns = 2


		parentComponent = component
		parentProperty = property
		systemConverter = CentralCatalogue.INSTANCE.getSystemConverter()
		reverseConverter = CentralCatalogue.INSTANCE.getReverseConverter()
		nf = Localizer.instance

		// Get all the objects and sort them by the given object property.
		sourceObjects = component.getManagedObjectsOfType( objectType ).sort { a,b ->
			a.getObjectSetting( sortSetting ).getValue() <=> b.getObjectSetting( sortSetting ).getValue()
		}

		builder = new SwingBuilder();

		button = new JButton();
		button.setActionCommand(EDIT);
		button.addActionListener(this);
		button.setBorderPainted(false);

		currentTable = property.getValue().toString().trim()
		originalTable = currentTable
	}


	private void initDialog( String tableString ) {
		def okAction
		def cancelButtonAction
		def cvf = new ChartVerifier()
		def unitSystem
		if (systemConverter) {
			unitSystem = systemConverter.getTargetSystem()
		} else {
			unitSystem = CentralCatalogue.INSTANCE.getDefaultUnitSystem()
		}

		String rightUnits
		if (unitSystem.getDimensionsMap().containsKey(rightDimension)) {
			rightUnits = unitSystem.getDimension(rightDimension).getUnits()
		} else {
			rightUnits = CentralCatalogue.INSTANCE.getUnitSystems().getSimpleDimensions().get(rightDimension).getUnits()
		}

		// Dynamically tell MigLayout to add some space between columns, when necessary. (todo: Better way than this?)
		String colConstraint = ""
		if( layoutColumns > 1 ) {
			colConstraint = "[][]"
			for( int i = 1; i < layoutColumns; i++ ) {
				colConstraint += "50[][]"
			}
		}

		okAction = builder.action(name: CentralCatalogue.getUIS("button.ok"), mnemonic: KeyEvent.VK_O, closure: { saveAndExitAction() })
		cancelButtonAction = builder.action(name: CentralCatalogue.getUIS("button.cancel"), mnemonic: KeyEvent.VK_C, closure: { cancelAction() })
		JPanel dlgPanel
		dialog = builder.dialog(owner: CentralCatalogue.getParentFrame(), title: "${parentComponent.getName()} - ${parentProperty.getDisplayName()}",
		                        modal: true, layout: new BorderLayout(), resizable: false) {
			panel(layout: new FlowLayout(FlowLayout.CENTER), constraints: BorderLayout.NORTH) {
				label( text: "Default Value" )
				defaultField = formattedTextField( columns: 10, inputVerifier: cvf )
			}
			scrollPane(constraints: BorderLayout.CENTER, verticalScrollBarPolicy: JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, horizontalScrollBarPolicy: JScrollPane.HORIZONTAL_SCROLLBAR_NEVER) {
				dlgPanel = panel(layout: new MigLayout("fill, wrap ${layoutColumns*2}", colConstraint ) ) {

					/*
					for( int i = 0; i < layoutColumns; i++ ) {
						label(text: "${objectType.charAt(0).toUpperCase()}${objectType.substring(1)}")
						label(text: parentProperty.getDisplayName())
					}
					*/

					for( int i = 0; i < sourceObjects.size(); i++) {
						label( text: sourceObjects[i].getObjectSetting( labelSetting ).getValue().toString(), constraints: "gapright related" )
						fieldList.add( formattedTextField( columns: 10, constraints: "gapleft related", inputVerifier: cvf ) )
					}
				}
			}
			panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.SOUTH) {
				button(text: CentralCatalogue.getUIS("button.ok"), mnemonic: KeyEvent.VK_O, defaultButton: true, action: okAction)
				button(text: CentralCatalogue.getUIS("button.cancel"), mnemonic: KeyEvent.VK_C, defaultButton: false, action: cancelButtonAction)
			}
			dlgPanel.registerKeyboardAction(okAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			dlgPanel.registerKeyboardAction(cancelButtonAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		}

		populateUI( tableString )
		dialog.pack();

		// TODO: Fix This! Something above is stopping the scrollpane from properly sizing based on the screen, so force it.
		Dimension size = dialog.getSize()
		double hmax =  ( CentralCatalogue.getParentFrame().getGraphicsConfiguration().getBounds().getHeight() * 0.75 )
		if( size.getHeight() > hmax ) {
			dialog.setPreferredSize( new Dimension( (int) size.getWidth(), (int) hmax ) )
			dialog.pack();
		}
	}

	/**
	 * Exits the dialog due to the user clicking the OK button.  Flushes the GUI widgets to some variables to be
	 * returned by getCellEditorValue()
	 */
	def saveAndExitAction() {
		currentTable = buildTableString()
		dialog.setVisible( false )
	}

	/**
	 * Exits the dialog due to the user clicking the CANCEL button. Does not save any changes.
	 */
	def cancelAction() {
		currentTable = originalTable
		dialog.setVisible( false )
		dialog.dispose()
		dialog = null
	}

	/**
	 * Runs if the user has clicked the button returned from getTableCellEditorComponent.
	 */
	public void actionPerformed(ActionEvent e) {
		//println "actionPerformed: ${e.getActionCommand()}"
		if (EDIT.equals(e.getActionCommand())) {
			dialog.setLocationRelativeTo( CentralCatalogue.getParentFrame() )
			dialog.setVisible( true )
			fireEditingStopped();
		}
	}

	/**
	 * Save and return the user-entered chart.
	 */
	public Object getCellEditorValue() {
		//println "the value that the chart is going to return is '$currentTableName' == ${currentTable}"
		return currentTable; //return the chart string itself in value form for saving
	}

	/**
	 * Loads the data from the component and returns the editor component (in this case, a button to DISPLAY the component, but still.)
	 */
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int col) {
		//println "getTableCellEditorComponent()"
		//initDialog(value.toString())
		initDialog(currentTable)
		return button;
	}


	protected void populateUI( String tableString ) {

		Map<String, String> table = parseTableString( tableString )

		if( table.containsKey( DEFAULT_KEY ) ) {
			//convert from storage to display units
			String val = table[DEFAULT_KEY]
			if( val.isDouble() ) {
				if( systemConverter != null && rightDimension != "" ) {
					def unitConverter = systemConverter.getConverter( rightDimension )
					defaultField.text = nf.format( unitConverter.convert( Double.parseDouble( val )))
				} else {
					defaultField.text = nf.format( Double.parseDouble( val ) )
				}
			} else {
				defaultField.text = val
			}
		} else {
			defaultField.text = ""
		}

		for( int i = 0; i < sourceObjects.size(); i++ ) {

			String val = table[ sourceObjects[i].getObjectSetting( keySetting ).getValue().toString() ]

			if( val == null ) {
				fieldList[i].text = ""
			} else {
				//convert from storage to display units
				if( val.isDouble() ) {
					if( systemConverter != null && rightDimension != "" ) {
						def unitConverter = systemConverter.getConverter( rightDimension )
						fieldList[i].text = nf.format( unitConverter.convert( Double.parseDouble( val )))
					} else {
						fieldList[i].text = nf.format( Double.parseDouble( val ) )
					}
				} else {
					fieldList[i].text = val
				}
			}
		}
	}


	protected Map<String, String> parseTableString( String tableString ) {

		def table = [:]

		if( !tableString.trim().isEmpty() ) {
			def pairs = tableString.split(';')
			for (String it: pairs) {
				String[] elems = it.split(',')

				if( elems.length == 1 ) {
					table[ elems[0] ] = ""
				} else {
					table[ elems[0] ] = elems[1]
				}
			}
		}

		return table
	}


	/**
	 * Converts the the user entries in the screen widgets to a table string
	 * @return a tablestring matching the values currently in the GUI.
	 */
	protected String buildTableString() {
		String chart = ""
		String valStr

		valStr = defaultField.text.trim()
		try {
			if( systemConverter != null && rightDimension != "" ) {
				def unitConverter = reverseConverter.getConverter( rightDimension )
				valStr = unitConverter.convert( nf.parse( valStr ).doubleValue() )
			} else {
				valStr = nf.parse( valStr )
			}
		} catch( NumberFormatException e ) {
			valStr = ""
		}

		if( !valStr.isEmpty() ) {
			chart += "$DEFAULT_KEY,$valStr;"
		}


		for( int i = 0; i < fieldList.size(); i++ ) {
			valStr = fieldList[i].text.trim()
			try {
				if( systemConverter != null && rightDimension != "" ) {
					def unitConverter = reverseConverter.getConverter( rightDimension )
					valStr = unitConverter.convert( nf.parse( valStr ).doubleValue() )
				} else {
					valStr = nf.parse( valStr )
				}
			} catch( NumberFormatException e ) {
				valStr = ""
			}

			if( !valStr.isEmpty() ) {
				chart += "${sourceObjects[i].getObjectSetting( keySetting ).getValue()},$valStr;"
			}
		}

		//println "buildTableString results in $chart"
		return chart
	}
}
