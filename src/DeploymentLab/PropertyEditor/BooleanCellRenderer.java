
package DeploymentLab.PropertyEditor;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

class BooleanCellRenderer extends JCheckBox implements TableCellRenderer {

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
		if(isSelected) {
			setForeground(table.getSelectionForeground());
			setBackground(table.getSelectionBackground());
		} else {
			setForeground(table.getForeground());
			setBackground(table.getBackground());
		}
		boolean newVal = Boolean.parseBoolean(value.toString());
		//boolean newVal = (boolean)value != null && ((boolean)value).booleanValue();
		setSelected(newVal);
		return this;
	}

	public void validate() {}
	public void invalidate() {}
	public void revalidate() {}

	public void repaint() {}
	public void repaint(long ts) {}
	public void repaint(int x, int y, int width, int height) {}
	public void repaint(long ts, int x, int y, int width, int height) {}

	public void firePropertyChange(String p, boolean o, boolean n) {}
	public void firePropertyChange(String p, char o, char n) {}
	public void firePropertyChange(String p, int o, int n) {}
	public void firePropertyChange(String p, Object o, Object n) {}
}

