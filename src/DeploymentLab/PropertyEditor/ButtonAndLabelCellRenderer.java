package DeploymentLab.PropertyEditor;

import DeploymentLab.Palette.PaletteRenderer;
import javax.swing.*;
import javax.swing.table.TableCellRenderer;

import DeploymentLab.channellogger.Logger;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: ghelman
 * Date: May 5, 2010
 * Time: 5:03:10 PM
 *
 */
public class ButtonAndLabelCellRenderer extends JPanel implements TableCellRenderer {
	private static final Logger log = Logger.getLogger(ButtonAndLabelCellRenderer.class.getName());
	private JButton miniButton;
	private JLabel words;

	/**
	 * Constructs a blank renderer.
	 */
	 public ButtonAndLabelCellRenderer(){
		 this("");
	 }

	/**
	 * Constructs a new cell renderer with a fixed label.  If a label is supplied, the cell renderer will always show that label and nothing else.
	 * @param _text a string label to show in this table cell.
	 */
	public ButtonAndLabelCellRenderer(String _text){
		super( new BorderLayout() );
		String fixedText;
		fixedText = _text;
		words = new JLabel();
		words.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4)); 
		words.setText(fixedText);

		miniButton = new JButton();
		miniButton.setText("...");
		miniButton.setPreferredSize( new Dimension( 20, 16 ) );
		miniButton.setMargin(new Insets(1,1,5,1));

		this.add( words, BorderLayout.CENTER );
		this.add( miniButton, BorderLayout.LINE_END );
		this.setToolTipText( "<html>" + PaletteRenderer.wrap( _text, 75 ) + "</html>" );
	}


	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		//miniButton.setSelected( isSelected );
		//text installed via the constructor cannot be overridden
		if ( words.getText().trim().equals("") ){
			words.setText( value.toString().trim() );
		}
		return (this);
	}
}
