package DeploymentLab.PropertyEditor

import java.awt.BorderLayout
import java.awt.Component
import java.awt.FlowLayout
import java.awt.Dialog.ModalityType
import java.awt.event.ActionEvent
import java.awt.event.ActionListener

import javax.swing.AbstractCellEditor
import javax.swing.JButton
import javax.swing.JDialog
import javax.swing.JFrame
import javax.swing.JTable
import javax.swing.JTextArea
import javax.swing.JToggleButton
import javax.swing.SpringLayout
import javax.swing.table.TableCellEditor

import groovy.swing.SwingBuilder

import DeploymentLab.Model.*
import DeploymentLab.channellogger.*
import DeploymentLab.SpringUtilities
import DeploymentLab.UndoBuffer
import DeploymentLab.Dialogs.DocumentSizeFilter


/**
 * Show readonly long String typed property value or allows user to edit the value
 * User : hlim
 * Date : 03/22/2010
 * 
 */

class MultiLineCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {
	static final int MAX_CHARACTERS = 2000; //sets max length of text area
	private static final Logger log = Logger.getLogger(MultiLineCellEditor.class.getName())
	private ComponentProperty componentProperty
	private DLComponent component
	private SwingBuilder builder
	private JDialog dialog
	private String pvalue
	private JTextArea valueField
	private JToggleButton button
	private static final String EDIT = "edit"
	private boolean isReadOnly
	private UndoBuffer undoBuffer
	private JFrame parent
	private String dialogTitle


	/**
	 * Constructor
	 * creating editor to show and edit long string value
	 */
	 MultiLineCellEditor(DLComponent _c, ComponentProperty _cp, UndoBuffer ub, JFrame _parent){
		component = _c
		componentProperty = _cp
		undoBuffer = ub
		parent = _parent;
		pvalue = componentProperty.getValue()

		//button = new JButton()
		button = new JToggleButton() //now a toggle button so it looks a little better when sitting in the properties table
		button.setActionCommand(EDIT)
		button.addActionListener(this)
		button.setBorderPainted(false)
		button.setFocusPainted(false)
		button.setVisible(true)
		button.setText(pvalue) //install the value of the property in the button so it won't vanish while the editor is open
		button.setSelected(false)


		if( component.getComponentProperty( componentProperty.getName() ) ==null) {
			isReadOnly = true
		} else {
			isReadOnly = false
		}
		 dialogTitle = "${componentProperty.getDisplayName()} (${component.getName()})"
		setUI()
	}
	
	/**
	 * set UI
	 */
	private void setUI(){
		builder = new SwingBuilder()
		
		String cancelText = ""
		if(isReadOnly)
			cancelText = "Close"
		else
			cancelText = "Cancel"

		def topPanel

		valueField = builder.textArea(lineWrap:true, columns: 20, rows: 10, text:pvalue, editable:!isReadOnly, wrapStyleWord: true )
		//set max length of the text area
		def doc = valueField.getDocument()
		doc.setDocumentFilter(new DocumentSizeFilter(MAX_CHARACTERS));

		dialog = builder.dialog(owner:parent, title: dialogTitle, layout: new BorderLayout(), modalityType: ModalityType.APPLICATION_MODAL, resizable:true){
			topPanel = panel(constraints: BorderLayout.CENTER, layout: new SpringLayout()){
				scrollPane() {
					//valueField = textArea(lineWrap:true, columns: 20, rows: 10, text:pvalue, editable:!isReadOnly, wrapStyleWord: true )
					widget(valueField)
				}
			}
			panel(constraints: BorderLayout.SOUTH, layout: new FlowLayout( FlowLayout.TRAILING )){
				button(text:"OK", visible:!isReadOnly, actionPerformed: {save();dialog.setVisible(false)})
				button(text:cancelText, actionPerformed: {dialog.setVisible(false)})
			}
		}
		SpringUtilities.makeCompactGrid(topPanel,
                                1, 1,   //rows, cols
                                5, 5,   //initX, initY
                                5, 5);  //xPad, yPad
		dialog.pack()
	}
	
	void actionPerformed(ActionEvent e) {
		if(EDIT.equals(e.getActionCommand())) {
			dialog.setLocationRelativeTo( parent )
			dialog.show()
			fireEditingStopped()
		} 
	}
	
	public Object getCellEditorValue() {
		return pvalue
	}
	
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		pvalue = value
		return button
	}
	
	public save(){
		try{
			undoBuffer.startOperation()
			String newValue = valueField.text
			component.setPropertyValue(componentProperty.getName(), newValue)
		}catch(IllegalArgumentException e){
			log.error(e.getLocalizedMessage(), 'message')
		}catch(Exception e){
			log.error("Cannot save value", 'message')
		}finally{
			undoBuffer.finishOperation()
		}
		
	}
}