package DeploymentLab.PropertyEditor;

import DeploymentLab.Model.ComponentProperty;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;


/**
 * Comparator used for sorting Component properties. This is primarily to maintain a consistent property order in the
 * property editor. The defined order accounts for properties that are not explicitly listed. The defined order has a
 * top half and a bottom half. If a property is not found in the list, it will be placed between the top and bottom
 * property sets in the same order found in the Component XML definition.
 *
 * This is an enum-style singleton, as per the example in Effective Java, and can be referenced using
 * <code>PropertySorter.INSTANCE</code>.
 *
 * @author Ken Scoggins
 * @since Io
 */
public enum PropertySorter implements Comparator<ComponentProperty> {
	INSTANCE;

	// This could be in a property file and loaded in the class constructor when it starts getting large or we just want
	// to allow more cutomization of the order. For now, it is a pretty small subset since most of the properties are
	// already in order in the XML. As the properties in XML move more toward the DTD model where a feature ENTITY comes
	// with its objects, properties, bindings, etc., as we have prototyped a little with controlpoint objects and the
	// 'direction' property, more properties will need to be listed here.
	private static final List<String> PROPERTY_ORDER = Arrays.asList(
			"name",
			"rotation",
			"depth",
			"width",
			"x",
			"y",
			"", // Placeholder for anything not found.
			"direction",
			"direction1",
			"direction2",
			"powerSource",
			"configuration",
			PropertyModel.DESCRIPTION_PSEUDO_PROPERTY);

	private static final int NOT_FOUND_INDEX = PROPERTY_ORDER.indexOf("");

	private PropertySorter() {}

	@Override
	public int compare( ComponentProperty o1, ComponentProperty o2 ) {
		int i1 = PROPERTY_ORDER.indexOf( o1.getName() );
		int i2 = PROPERTY_ORDER.indexOf( o2.getName() );

		if( i1 < 0 ) {
			i1 = NOT_FOUND_INDEX;
		}

		if( i2 < 0 ) {
			i2 = NOT_FOUND_INDEX;
		}

		return i1 - i2;
	}
}