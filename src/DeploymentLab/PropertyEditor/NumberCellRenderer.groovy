package DeploymentLab.PropertyEditor

import javax.swing.table.DefaultTableCellRenderer
import java.awt.Component
import javax.swing.JTable
import DeploymentLab.Localizer

/**
 * Renders a number in the configured localization format and Locale.
 *
 * @author Ken Scoggins
 * @since  Jupiter
 */
class NumberCellRenderer extends DefaultTableCellRenderer {
    Component getTableCellRendererComponent( JTable table, Object value, boolean isSelected,
                                             boolean hasFocus, int row, int col ) {
        // Format the value and pass it to the parent class.
        value = Localizer.format( value )
        return super.getTableCellRendererComponent( table, value, isSelected, hasFocus, row, col )
    }
}
