package DeploymentLab.PropertyEditor

import javax.swing.table.AbstractTableModel

import DeploymentLab.SelectionChangeEvent
import DeploymentLab.SelectionChangeListener
import DeploymentLab.Model.*
import DeploymentLab.UndoBuffer
import DeploymentLab.channellogger.*

//import com.synapsense.util.unitconverter.ConvertersMap
import com.synapsense.util.unitconverter.UnitSystems
import com.synapsense.util.unitconverter.UnitConverter
import com.synapsense.util.unitconverter.SystemConverter
import DeploymentLab.CentralCatalogue
import DeploymentLab.Localizer

class PropertyModel extends AbstractTableModel implements SelectionChangeListener, ModelChangeListener {
	private static final Logger log = Logger.getLogger(PropertyModel.class.getName())

	/** A reference to the table being used by this table model instance. */
	private PropertyTable tableRef

	final String[] columns = ['Property', 'Value']

	/** selected components */
	private components = []
	/** list of property objects to be edited by the table. */
	private List<ComponentProperty> _properties = []
	/** a list of the display names that go with the property objects in _properties. */
	private _displayNames = []

    /** Indicates that the model is updating a component property. Component listeners will ignore updates while true. */
    private String changingProp = null

	private ComponentModel cmodel
	private UndoBuffer undoBuffer
	private SystemConverter systemConverter
	private UnitConverter unitConverter
	//private ConvertersMap convertersMap
	private UnitSystems convertersMap
	private SystemConverter reverseSystemConverter = null
	
	PropertyModel(ComponentModel cm, UndoBuffer ub, SystemConverter sc) {
		cmodel = cm
		undoBuffer = ub
		systemConverter = sc

		cmodel.addModelChangeListener( this )
	}

	public void setTable( PropertyTable theTable ) {
		this.tableRef = theTable
	}
	
	void setUnitSystem(SystemConverter sc, UnitSystems cm){
		systemConverter = sc
		convertersMap = cm
		if(systemConverter!=null){
			reverseSystemConverter = convertersMap.getSystemConverter(systemConverter.getTargetSystemName(), systemConverter.getBaseSystemName())
		}
		fireTableDataChanged()
	}

	public SystemConverter getSystemConverter(){
		return systemConverter
	}

	public SystemConverter getReverseSystemConverter(){
		return reverseSystemConverter
	}


	public boolean undoOperationInProgress(){
		return ( undoBuffer.operationInProgress() )
	}
	

	/**
	 * Update the table model based on the new selection.
	 */
	void selectionChanged(SelectionChangeEvent e) {
		//save the entry in the cell currently being edited before we refresh the table
        // For unit testing purposes, only do this if we have a tableRef
		if ( tableRef && tableRef.isEditing() ) {
			if ( ! undoBuffer.operationInProgress() ) {
			    tableRef.getCellEditor().stopCellEditing();
			}
		}

		//iterate on separate collections, not .each; don't use "in" or +=, -= for collections
		HashSet<DLComponent> componentsSet = new HashSet<DLComponent>()
		componentsSet.addAll(components)

		HashSet<DLComponent> removedComponents = e.getRemovedSet()
		for( DLComponent c : removedComponents ){
			if ( componentsSet.contains(c) ){
				components.remove(c)
				c.removePropertyChangeListener(this)
			}
		}

		HashSet<DLComponent> addedComponents = e.getAddedSet()
		for( DLComponent c : addedComponents ){
			if ( !componentsSet.contains(c) ){
				components.add(c)
				c.addPropertyChangeListener(this, this.&componentPropertyChanged)
			}
		}

		buildTable()
	}

	private void buildTable() {

		if(components.size() == 0) {
			_properties = []
		} else {
			def allProperties = []
			allProperties = components.first().listSortedPropertyNames().findAll{components.first().getComponentProperty(it).displayed}
			for(DLComponent c: components.tail()) {
				allProperties = allProperties.intersect(c.listSortedPropertyNames().findAll{c.getComponentProperty(it).displayed})
			}
			if(components.size() > 1) {
				//filter out any configurable expressions:
				def toFilter = []
				allProperties.each{ p ->
					ComponentProperty prop = components.first().getComponentProperty(p)
					if ( prop.getType().contains("DeploymentLab.Config") ){
						toFilter += p
					}
				}
				toFilter.each{ allProperties.remove(it) }
			}

			//now, distribute all that out:
			_properties = allProperties.collect{ components.first().getComponentProperty(it) }

			// grab the actual display names for the properties
			// _displayNames = _properties.collect{it.getDisplayName()}
			_displayNames = []
			for( int i = 0; i < _properties.size(); i++ ) {
				def p = _properties.get(i)
				String prefix = "    " * p.displayLevel
				_displayNames.add("$prefix${p.getDisplayName()}")
			}

			//only show the description if we only have one type of comp in play
			def checkTypes = []
			components.each{ checkTypes += it.getType()	}
			checkTypes = checkTypes.unique()
			if ( checkTypes.size == 1 ) {
				_properties += getDescriptionProperty(components.first())
				_displayNames+="Description"
			}

		}
		fireTableStructureChanged()
	}

    @Override
    void activeDrawingChanged(  DLComponent oldDrawing, DLComponent newDrawing ) {}

	public static final String DESCRIPTION_PSEUDO_PROPERTY = "component_description"

//	 forces Display description to one of properites
	public ComponentProperty getDescriptionProperty(DLComponent c){
		String desc = c.getDisplaySetting("description")
		String xml = """
		<property name="$DESCRIPTION_PSEUDO_PROPERTY" display="Description" type="DeploymentLab.MultiLineContents" editable="true" displayed="true">
		""" + desc + "</property>"
		
		XmlSlurper parser = new XmlSlurper()
		def descProperty = parser.parseText(xml)
		return new ComponentProperty(descProperty)
	}

	void componentPropertyChanged( def who, String prop, def oldVal, def newVal ) {
        if( changingProp == prop ) { log.trace("rejecting recursive property change. $who '$prop' '$oldVal'->'$newVal'"); return }
		if( prop == "metamorphosizing" && oldVal == Boolean.TRUE && newVal == Boolean.FALSE ) {
			// Might have different props. One example is a CRAH moving between rooms with different roomType or controlType.
			buildTable()
		}
		fireTableStructureChanged()
	}

	String getColumnName(int col) {
		return columns[col]
	}

	boolean isCellEditable(int row, int col) {
		if(col == 0)
			return false
		
		ComponentProperty cp = _properties[row]
		return cp.isEditable()
	}

	/**
	 * Method used by PropertyTable to determine what cell editor/renderer to use.
	 */
	String getCellClassName(int row, int col) {
		if(col == 0)
			return 'java.lang.String'
		
		return _properties[row].type
	}

	Object[] getCellValues(int row, int col) {
		if(col == 0)
			return null
		
		return _properties[row].getValueChoices()
	}

	Class getColumnClass(int col) {
		if(col == 1)
			return String.class
		return Object.class
	}

	Object getValueAt(int row, int col) {
		if(col == 0)
			return _displayNames[row]
		String pname = _properties[row].getName()
		ComponentProperty cp = _properties[row]
		def pVal = _properties[row].getValue()

		//special case for description pseudo-property
		//TODO: augment the component API to make the description field act like a normal property in here
		if ( cp.getName().equals( DESCRIPTION_PSEUDO_PROPERTY )){
			return pVal
		}
		
		if(components.tail().every{it.getPropertyValue(pname) == pVal}) {
			if(cp.getValueChoices() != null) {
				return cp.choiceForValue(pVal)
			}

			boolean isUnitConvertable = cp.isUnitConvertable()
			if(systemConverter!=null && pVal != null && isUnitConvertable) {
				String dimension = cp.getDimensionName()
				unitConverter = systemConverter.getConverter(dimension)
				if( unitConverter != null ) {
					pVal = unitConverter.convert(pVal)
				} else {
					log.error("Unit converter not found for dimension '$dimension'.")
				}
			}
			return pVal
		}
		return ''
	}

    void setValueAt(Object val, int row, int col) {
        if(col == 0)
            return

	    boolean rebuildTable = false
        try {
            ComponentProperty cp = _properties[row]
            String pname = _properties[row].getName()

            undoBuffer.startOperation()

            //iterate on separate collection, not .each
            List<DLComponent> componentsToChange = new ArrayList<DLComponent>(components)
            changingProp = pname
            for( DLComponent it : componentsToChange ){

                // only if component has this componentProperty, set property value
                if(it.getComponentProperty(pname)!=null){
                    // Fresh copy each iteration since we mangle it.
                    def newVal = val

                    if( cp.getValueChoices() != null ) {
                        newVal = cp.valueForChoice( newVal )
                    }

                    // This might be the numerical value or a string representation, so handle both.
                    if( (( newVal instanceof String ) || ( newVal instanceof GString )) && Localizer.isLocalizable( cp ) ) {
	                    if( !newVal.isEmpty() ) {
		                    newVal = Localizer.parse( newVal )
	                    }
                    }

                    if( (systemConverter != null) && cp.isUnitConvertable() ) {
                        unitConverter = reverseSystemConverter.getConverter( cp.getDimensionName() )

                        newVal = unitConverter.convert( newVal )
                    }

	                // Snatch the current props for comparison after the change.
	                def propsBefore = null
	                if( !rebuildTable ) {
		                propsBefore = new ArrayList<>( it.getAllProperties() )
	                }

	                // Make the change
                    it.setPropertyValue( pname, newVal )

	                // Check the props to detect any Parcel shenanigans that added/removed properties.
	                if( !rebuildTable ) {
		                def propsAfter = it.getAllProperties()
		                if( ( propsBefore.size() != propsAfter.size() ) || !propsBefore.containsAll( propsAfter ) ) {
			                rebuildTable = true
		                }
	                }
                }
            }
        } catch(IllegalArgumentException iae){
	        String err = ( iae.getMessage() == null ? iae.getClass().getName() : iae.getMessage() )
            log.error( String.format( CentralCatalogue.getUIS('componentProperty.setError'), _displayNames[row].trim(), val, err ), 'message')
            log.trace( String.format( CentralCatalogue.getUIS('componentProperty.errorSource'), iae.getClass().name, iae.message ) )
            undoBuffer.rollbackOperation()
        } catch(Exception e) {
	        String err = ( e.getMessage() == null ? e.getClass().getName() : e.getMessage() )
            log.error( String.format( CentralCatalogue.getUIS('componentProperty.setError'), _displayNames[row].trim(), val, err ), 'message')
            log.trace( String.format( CentralCatalogue.getUIS('componentProperty.errorSource'), e.getClass().name, e.message ) )
            log.trace( log.getStackTrace(e) )
            undoBuffer.rollbackOperation()
        } finally {
            // Don't want to let this get stuck on!
            changingProp = null
	        if( rebuildTable ) {
		        buildTable()
	        } else {
		        fireTableCellUpdated(row, col)
	        }
            undoBuffer.finishOperation()
        }
    }

	int getRowCount() {
		return _properties.size()
	}

	int getColumnCount() {
		return columns.size()
	}
	
	DLComponent getComponent() {
		return ( components.first() )
	}

	int getNumberOfComponents() {
		return components.size()
	}
	
	ComponentProperty getProperty(int row){
		return _properties[row]
	}

	@Override
	void componentAdded( DLComponent component ) {}

	@Override
	void componentRemoved( DLComponent component ) {}

	@Override
	void childAdded( DLComponent parent, DLComponent child ) {}

	@Override
	void childRemoved( DLComponent parent, DLComponent child ) {}

	@Override
	void associationAdded( DLComponent producer, String producerId, DLComponent consumer, String consumerId ) {}

	@Override
	void associationRemoved( DLComponent producer, String producerId, DLComponent consumer, String consumerId ) {}

	@Override
	void modelMetamorphosisStarting( MetamorphosisEvent event ) {}

	@Override
	void modelMetamorphosisFinished( MetamorphosisEvent event ) {
		if( event.getMode() == ModelState.UNDOING ) {
			// An undo may cause a parcel change, so rebuild just in case.
			buildTable()
		}
	}
}
