
package DeploymentLab.PropertyEditor;

import java.util.Arrays;

import java.awt.Component;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

class ListCellRenderer<E> extends JComboBox<E> implements TableCellRenderer {

	public ListCellRenderer(E[] items) {
		super(items);
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		if (isSelected) {
			setForeground(table.getSelectionForeground());
			setBackground(table.getSelectionBackground());
		} else {
			setForeground(table.getForeground());
			setBackground(table.getBackground());
		}

		setSelectedItem(value);
		return this;
	}
}
