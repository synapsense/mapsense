package DeploymentLab.PropertyEditor;

import DeploymentLab.CentralCatalogue;
import DeploymentLab.Model.ComponentProperty;
import DeploymentLab.Model.DLComponent;
import DeploymentLab.channellogger.Logger;
import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Based on the ButtonAndLabelCellRenderer, this also displays the current validity status of the expression.
 *
 * @author Gabriel Helman
 * @since Mars
 *        Date: 9/1/11
 *        Time: 9:52 AM
 */
public class ExpressionRenderer extends JPanel implements TableCellRenderer {
	private static final Logger log = Logger.getLogger(ExpressionRenderer.class.getName());
	private JButton miniButton;
	private JLabel words;
	private DLComponent source;
	private ComponentProperty prop;


	public ExpressionRenderer(DLComponent sourceComponent, ComponentProperty sourceProperty) {
		this("");
		this.source = sourceComponent;
		this.prop = sourceProperty;
	}

	/**
	 * Constructs a new cell renderer with a fixed label.  If a label is supplied, the cell renderer will always show that label and nothing else.
	 *
	 * @param _text a string label to show in this table cell.
	 */
	public ExpressionRenderer(String _text) {
		super(new BorderLayout());
		String fixedText;
		fixedText = _text;
		words = new JLabel();
		words.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		words.setText(fixedText);

		miniButton = new JButton();
		miniButton.setText("...");
		miniButton.setPreferredSize(new Dimension(20, 16));
		miniButton.setMargin(new Insets(1, 1, 5, 1));

		this.add(words, BorderLayout.CENTER);
		this.add(miniButton, BorderLayout.LINE_END);
		if( !_text.isEmpty() ) {
			this.setToolTipText(_text);
		}
	}


	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		//text installed via the constructor cannot be overridden
		if (words.getText().trim().equals("")) {
			//go check the valid status
			Boolean fromComponent; // = Boolean.parseBoolean(source.getPropertyValue(prop.getName() + "Valid").toString());
			if (source.getPropertyValue(prop.getName() + "Valid").toString().equalsIgnoreCase("0")) {
				fromComponent = false;
			} else {
				fromComponent = true;
			}
			//log.trace("valid prop name is '" + prop.getName() + "Valid'", "default" );
			//log.trace("in renderer, fromComponent = " + fromComponent.toString(), "default");
			String symbol = CentralCatalogue.getUIS("expressionRenderer.invalidSymbol");
			String result = "";
			String val = value.toString().trim();
			if (fromComponent) {
				result = val;
			} else {
				//result = symbol + " " + value.toString().trim();
				StringBuilder sb = new StringBuilder();
				sb.append("<html><font color='red'><bold>");
				sb.append(symbol);
				sb.append("</bold>");
				sb.append( val );
				sb.append("</font></html>");
				result = sb.toString();
			}
			words.setText(result);
			this.setToolTipText( val.isEmpty() ? null : val );
		}
		return (this);
	}
}
