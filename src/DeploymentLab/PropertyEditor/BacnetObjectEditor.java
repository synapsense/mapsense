package DeploymentLab.PropertyEditor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;
import java.util.Vector;

import javax.swing.AbstractCellEditor;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.CellEditorListener;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.table.TableCellEditor;

import DeploymentLab.Model.ComponentModel;
import DeploymentLab.Model.ComponentProperty;
import DeploymentLab.Model.DLComponent;
import DeploymentLab.SceneGraph.DeploymentPanel;
import DeploymentLab.SceneGraph.DynamicChildrenNode;
import DeploymentLab.SceneGraph.DynamicChildrenNode.DynamicChildNode;
import DeploymentLab.channellogger.Logger;

public class BacnetObjectEditor extends AbstractCellEditor implements TableCellEditor, ActionListener{
	private static final Logger log = Logger.getLogger(BacnetObjectEditor.class.getName());
	private DLComponent component;
	private ComponentProperty cProperty;
	private ComponentModel componentModel;
	private DeploymentPanel deploymentPanel;
	private JButton editorButton = new JButton("..");
	private JDialog dialog = new JDialog();
	private JList objectsList = new JList();
	private JFrame parentFrame;
	private DefaultListModel listModel = new DefaultListModel();
	private Vector propertyModel = new Vector();
	private JComboBox propertyList;
	
	private final String CHILDREN_PROPERTY_NAME="children";
	
	public BacnetObjectEditor(DLComponent component, ComponentProperty cProperty, JFrame frame, ComponentModel cModel, DeploymentPanel deploymentPanel){
		this.component = component;
		this.cProperty = cProperty;
		this.componentModel = cModel;
		this.deploymentPanel = deploymentPanel;
		parentFrame  = frame;
		
		bindData();
		createUI();	
	}
	private void bindData(){
		String strFields = (String)cProperty.getValue();
		
		String[] fileds = strFields.split(",");
		for(String f:fileds){
			listModel.addElement(f);
		}
		
		objectsList.setModel(listModel);
		
		propertyModel.addElement(new PropertyItem(85, "Present Value"));
		propertyModel.addElement(new PropertyItem(81, "Out Of Service"));
		propertyModel.addElement(new PropertyItem(69, "Min Present Value"));
		propertyModel.addElement(new PropertyItem(65, "Max Presnet Value"));
		
		
	}
	
	/**
	 * Create dialog box ui to edit or display database table column
	 * 
	 */
	private void createUI(){
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		
		objectsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane scrollPane = new JScrollPane(objectsList);
		scrollPane.setPreferredSize(new Dimension(200,150));
		mainPanel.add(scrollPane,BorderLayout.CENTER);
		
		JPanel addPanel = new JPanel();
		FlowLayout flowLayout = new FlowLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		addPanel.setLayout(flowLayout);
		
		JComboBox propertyList = new JComboBox(propertyModel);
		propertyList.setRenderer(new PropertyItemRenderer());
		propertyList.setPreferredSize(new Dimension(150,30));
		addPanel.add(propertyList);
		
		JTextField nameField = new JTextField("   ");
		nameField.setPreferredSize(new Dimension(100,30));
		addPanel.add(nameField);
		
		JButton addButton = new JButton("+");
		addPanel.add(addButton);
		addButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				//addField(newField.getText());
			}
		});
		
		JButton deleteButton = new JButton("-");
		addPanel.add(deleteButton);
		deleteButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				//removeField();
			}
			
		});
		
		mainPanel.add(addPanel, BorderLayout.SOUTH);
		dialog.add(mainPanel);
		dialog.setTitle("Add an object to device");
		dialog.pack();
		editorButton.addActionListener(this);
	}
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		dialog.setLocationRelativeTo(parentFrame);
		dialog.setVisible(true);
	}
	@Override
	public Component getTableCellEditorComponent(JTable arg0, Object arg1,
			boolean arg2, int arg3, int arg4) {
		editorButton.setText((String)cProperty.getValue());
		return editorButton;
	}
	
	@Override
	public Object getCellEditorValue() {
		// TODO Auto-generated method stub
		return (String)cProperty.getValue();
	}

	private void addField(String objectName){
		// add child component (db field object)
		DLComponent bacnetObject = componentModel.newComponent("bacnet-object");
		bacnetObject.setPropertyValue("name", objectName);
		
		DynamicChildrenNode parentNode = deploymentPanel.getPNode(component);
		DynamicChildNode childNode = parentNode.addChildComponent(bacnetObject);
		deploymentPanel.addNode(childNode);
		
		// set fields property for datatable component
		String propertiesStr = (String)cProperty.getValue();
		if(propertiesStr.length()>0)
			propertiesStr+="," + objectName;
		else
			propertiesStr+=objectName;
			
		component.setPropertyValue(CHILDREN_PROPERTY_NAME, propertiesStr);
		
		// Handling GUI
		listModel.addElement(objectName);
		propertyList.setSelectedIndex(listModel.getSize());
		
	}
	
	
	class PropertyItem{
		private int id;
		private String name;
		
		public PropertyItem(int id, String name){
			this.id = id;
			this.name = name;
		}
		
		public void setId(int id){
			this.id = id;
		}
		
		public void setName(String name){
			this.name = name;
		}
		
		public int getId(){
			return id;
		}
		
		public String getName(){
			return name;
		}
	}
	
	class PropertyItemRenderer extends BasicComboBoxRenderer{
		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus){
			super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			if(value!=null){
				PropertyItem property = (PropertyItem)value;
				this.setText(property.getName());
			}
			
			return this;
			
		}

	}

	
}
