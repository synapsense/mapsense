
package DeploymentLab.PropertyEditor

import java.awt.Color
import java.awt.Component

import javax.swing.BorderFactory
import javax.swing.JLabel
import javax.swing.JTable
import javax.swing.table.TableCellRenderer

class ColorCellRenderer extends JLabel implements TableCellRenderer {
	
	Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
		setOpaque(true)
		if( value && value instanceof Color ) setBackground((Color)value)
		if(isSelected)
			setBorder(BorderFactory.createMatteBorder(2,5,2,5, table.getSelectionBackground()))
		else
			setBorder(BorderFactory.createMatteBorder(2,5,2,5, table.getBackground()))
		return this
	}

	void validate() {}
	void invalidate() {}
	void revalidate() {}

	void repaint() {}
	void repaint(long ts) {}
	void repaint(int x, int y, int width, int height) {}
	void repaint(long ts, int x, int y, int width, int height) {}

	void firePropertyChange(String p, boolean o, boolean n) {}
	void firePropertyChange(String p, char o, char n) {}
	void firePropertyChange(String p, int o, int n) {}
	void firePropertyChange(String p, Object o, Object n) {}
}

