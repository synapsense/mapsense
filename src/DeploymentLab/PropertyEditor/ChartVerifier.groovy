package DeploymentLab.PropertyEditor

import javax.swing.InputVerifier
import javax.swing.JFormattedTextField
import javax.swing.JComponent
import DeploymentLab.channellogger.*
import DeploymentLab.Localizer

/**
 * Numerical input verifier for table entries.
 * @author Gabriel Helman
 * @since Venus
 * Date: Nov 10, 2009
 * Time: 2:29:25 PM
 *
 * @see LookupTableEditor
 */
class ChartVerifier extends InputVerifier {
	private static final Logger log = Logger.getLogger(ChartVerifier.name)

	public boolean verify(JComponent input) {
		JFormattedTextField tf = (JFormattedTextField) input;
		String entry = tf.getText();
		//return ((entry.isNumber() && entry.toDouble() < 9999999999.999) || entry.trim().equals("")) // and less than 10,000,000,000
		if (entry.trim().size() == 0) {
			return true
		}
		boolean passedParse = true
		Double value = 10000000000
		try {
			value = Localizer.parseDouble( entry )
		} catch (NumberFormatException e) {
			passedParse = false
		}
		return (passedParse && value.toDouble() < 9999999999.999)  // and less than 10,000,000,000
	}

	def boolean shouldYieldFocus(JComponent input) {
		//return super.shouldYieldFocus(input);
		boolean result = true
		if (this.verify(input)) {
			result = true
		} else {
			log.warn("Invalid input to chart, value must be a number.", 'statusbar')
			result = false
		}
		return (result)
	}
}
