
package DeploymentLab.PropertyEditor;

import java.util.EventObject;

import java.awt.Component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JTable;

import javax.swing.table.TableCellEditor;

class BooleanCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {

	private boolean currentValue;
	private JButton button;

	private static final String EDIT = "edit";

	public BooleanCellEditor() {
		button = new JButton();
		button.setActionCommand(EDIT);
		button.addActionListener(this);
		button.setBorderPainted(false);
	}

	public void actionPerformed(ActionEvent e) {
		if(EDIT.equals(e.getActionCommand())) {
			currentValue = !currentValue;
			fireEditingStopped();
		}
	}

	public Object getCellEditorValue() {
		return currentValue;
	}

	public boolean shouldSelectCell(EventObject ev) {
		return false;
	}

	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int col) {
		currentValue = Boolean.parseBoolean(value.toString());
		return button;
	}
}

