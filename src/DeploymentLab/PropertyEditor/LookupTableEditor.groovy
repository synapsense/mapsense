package DeploymentLab.PropertyEditor

import DeploymentLab.CentralCatalogue
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.Pair
import com.synapsense.util.unitconverter.SystemConverter
import groovy.swing.SwingBuilder
import java.awt.BorderLayout
import java.awt.Component
import java.awt.FlowLayout
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.awt.event.KeyEvent
import javax.swing.table.TableCellEditor
import net.miginfocom.swing.MigLayout
import javax.swing.*
import DeploymentLab.Localizer

/**
 *
 * @author Gabriel Helman
 * @since Mars
 * Date: 8/1/11
 * Time: 2:52 PM
 */
class LookupTableEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {

	private static final String EDIT = "edit";
	private String rightLabel
	private String TABLENAME
	private String TABLE
	private String DIALOGTITLE
	private String LEFTLABEL1
	private String LEFTLABEL2
	private String TOPLEFTLABEL
	private String TOPRIGHTLABEL

	private DLComponent parentComponent;

	private JButton button;
	private JDialog dialog;
	/** The parent frame of this dialog box.        */
	private JFrame parent
	private def builder

	private Map<Integer, JFormattedTextField> fieldList = [:]
	private Map<Integer, JFormattedTextField> leftFieldList = [:]

	private ComponentModel cmodel

	private int totalRows
	private String currentTable
	private String originalTable

	private String leftDimension
	private String rightDimension
	private SystemConverter systemConverter
	private SystemConverter reverseConverter
	private Localizer nf

	/**
	 * Constructor: produces a new Editor connected to a given component.
	 * @param newComponent The component that this property belongs to.  Sets a reference to allow us to edit >1 component property at a time.
	 */
	public LookupTableEditor(DLComponent newComponent, JFrame parent,
							 String _rightLabel,
							 String _TABLENAME,
							 String _TABLE,
							 String _DIALOGTITLE,
							 String _LEFTLABEL1,
							 String _LEFTLABEL2,
							 String _topLeftLabel,
							 String _topRightLabel,
							 int rows,
							 String defaultTableString,
							 String _leftDimension,
							 String _rightDimension,
							 SystemConverter _systemConverter,
							 SystemConverter _reverseConverter
	) {
		//println "constructor!"
		//load in the setup info
		rightLabel = _rightLabel
		TABLENAME = _TABLENAME
		TABLE = _TABLE
		DIALOGTITLE = _DIALOGTITLE
		LEFTLABEL1 = _LEFTLABEL1
		LEFTLABEL2 = _LEFTLABEL2
		TOPLEFTLABEL = _topLeftLabel
		TOPRIGHTLABEL = _topRightLabel
		totalRows = rows

		leftDimension = _leftDimension
		rightDimension = _rightDimension
		systemConverter = _systemConverter
		reverseConverter = _reverseConverter
		nf = Localizer.instance

		parentComponent = newComponent;
		builder = new SwingBuilder();

		button = new JButton();
		button.setActionCommand(EDIT);
		button.addActionListener(this);
		button.setBorderPainted(false);

		currentTable = (String) parentComponent.getComponentProperty(TABLE).getValue().toString().trim()
		originalTable = (String) parentComponent.getComponentProperty(TABLE).getValue().toString().trim()

		cmodel = parentComponent.getModel()

		//println "table editor init: tableName is '$currentTableName', table is '$currentTable'"
		if (currentTable.trim().size() == 0) {
			//slug in a default table:
			currentTable = defaultTableString
		}
	}



	private void initDialog(String tableString) {
		//println "initDialog"
		def okAction
		def cancelButtonAction
		def cvf = new ChartVerifier()
		def unitSystem
		if (systemConverter) {
			unitSystem = systemConverter.getTargetSystem()
		} else {
			unitSystem = CentralCatalogue.INSTANCE.getDefaultUnitSystem()
		}
		//println "left dimension is $leftDimension"
		//println "right dimension is $rightDimension"

		String leftUnits
		String rightUnits
		if (unitSystem.getDimensionsMap().containsKey(leftDimension)) {
			leftUnits = unitSystem.getDimension(leftDimension).getUnits()
		} else {
			leftUnits = CentralCatalogue.INSTANCE.getUnitSystems().getSimpleDimensions().get(leftDimension).getUnits()
		}

		if (unitSystem.getDimensionsMap().containsKey(rightDimension)) {
			rightUnits = unitSystem.getDimension(rightDimension).getUnits()
		} else {
			rightUnits = CentralCatalogue.INSTANCE.getUnitSystems().getSimpleDimensions().get(rightDimension).getUnits()
		}
		//println "left units are $leftUnits"
		//println "right units are $rightUnits"

		okAction = builder.action(name: CentralCatalogue.getUIS("button.ok"), mnemonic: KeyEvent.VK_O, closure: { saveAndExitAction() })
		cancelButtonAction = builder.action(name: CentralCatalogue.getUIS("button.cancel"), mnemonic: KeyEvent.VK_C, closure: { cancelAction() })
		JPanel dlgPanel
		dialog = builder.dialog(owner: parent, title: DIALOGTITLE, modal: true, layout: new BorderLayout(), resizable: false) {
			dlgPanel = panel(layout: new MigLayout(), constraints: BorderLayout.CENTER) {}
			panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.SOUTH) {
				button(text: CentralCatalogue.getUIS("button.ok"), mnemonic: KeyEvent.VK_O, defaultButton: true, action: okAction)
				button(text: CentralCatalogue.getUIS("button.cancel"), mnemonic: KeyEvent.VK_C, defaultButton: false, action: cancelButtonAction)
			}
			dlgPanel.registerKeyboardAction(okAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			dlgPanel.registerKeyboardAction(cancelButtonAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		}


		if (TOPLEFTLABEL != null && TOPRIGHTLABEL != null) {
			dlgPanel.add(new JLabel(TOPLEFTLABEL))
			dlgPanel.add(new JLabel(""), "gapright unrelated, gapleft unrelated")
			dlgPanel.add(new JLabel(TOPRIGHTLABEL), "wrap")
		}

		for (int l = 0; l < totalRows; l++) {
			leftFieldList[l] = builder.formattedTextField(columns: 10, inputVerifier: cvf)
			fieldList[l] = builder.formattedTextField(columns: 10, inputVerifier: cvf)
			//dlgPanel.add(new JLabel( String.format(LEFTLABEL1, rightUnits) ), "align right")
			dlgPanel.add(leftFieldList[l], "gapright related")
			dlgPanel.add(new JLabel(String.format(LEFTLABEL2, leftUnits)), "align left")
			dlgPanel.add(fieldList[l], "gapleft related, gapright related")
			dlgPanel.add(new JLabel(String.format(rightLabel, rightUnits)), "align left, wrap")
		}

		populateUI(tableString)
		dialog.pack();
	}

	/**
	 * Exits the dialog due to the user clicking the OK button.  Flushes the GUI widgets to some variables to be
	 * returned by getCellEditorValue()
	 */
	def saveAndExitAction() {
		currentTable = buildTableString()
		dialog.hide()
	}

	/**
	 * Exits the dialog due to the user clicking the CANCEL button. Does not save any changes.
	 */
	def cancelAction() {
		currentTable = originalTable
		dialog.hide()
		dialog.dispose()
		dialog = null
	}

	/**
	 * Runs if the user has clicked the button returned from getTableCellEditorComponent.
	 */
	public void actionPerformed(ActionEvent e) {
		//println "actionPerformed: ${e.getActionCommand()}"
		if (EDIT.equals(e.getActionCommand())) {
			dialog.setLocationRelativeTo(parent)
			dialog.show();
			fireEditingStopped();
		}
	}

	/**
	 * Save and return the user-entered chart.
	 */
	public Object getCellEditorValue() {
		//println "the value that the chart is going to return is '$currentTableName' == ${currentTable}"
		return currentTable; //return the chart string itself in value form for saving
	}

	/**
	 * Loads the data from the component and returns the editor component (in this case, a button to DISPLAY the component, but still.)
	 */
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int col) {
		//println "getTableCellEditorComponent()"
		//initDialog(value.toString())
		initDialog(currentTable)
		return button;
	}


	protected void populateUI(String tableString) {
		//println "populateUI()"
		//populate ui with tableString
		List<Pair<String, String>> pairs = tableStringToPairs(tableString)
		pairs = pairs.sort { Double.parseDouble(it.a) }

		int count = 0
		for (Pair p: pairs) {
			if (count >= totalRows) {
				break
			}
			//convert from storage to display units
			//left side!
			if (p.a.toString().isDouble()) {
				if (systemConverter != null && leftDimension != "") {
					def unitConverter = systemConverter.getConverter(leftDimension)
					leftFieldList[count].text = nf.format(unitConverter.convert(Double.parseDouble(p.a.toString())))
				} else {
					leftFieldList[count].text = nf.format(Double.parseDouble(p.a.toString()))
				}
			} else {
				leftFieldList[count].text = p.a.toString()
			}
			//right side!
			if (p.b.toString().isDouble()) {
				if (systemConverter != null && rightDimension != "") {
					def unitConverter = systemConverter.getConverter(rightDimension)
					fieldList[count].text = nf.format(unitConverter.convert(Double.parseDouble(p.b.toString())))
				} else {
					fieldList[count].text = nf.format(Double.parseDouble(p.b.toString()))
				}
			} else {
				fieldList[count].text = p.b
			}
			//println "count=$count ; p = $p"
			//leftFieldList[count].text = p.a
			//fieldList[count].text = p.b
			count++
		}
	}


	protected List<Pair<String, String>> tableStringToPairs(String tableString) {
		if (tableString.trim().equals("")) {
			return ([])
		}
		def pairs = tableString.split(';')
		List table = new ArrayList()
		for (String it: pairs) {
			String[] elems = it.split(',')

			if (elems.length == 1) {
				table += new Pair(elems[0], "")
			} else {
				table += new Pair(elems[0], elems[1])
			}

		}
		//table.sort{ a, b -> a.a.compareTo(b.a) }
		return (table)
	}

	/**
	 * Converts the the user entries in the screen widgets to a table string
	 * @return a tablestring matching the values currently in the GUI.
	 */
	protected String buildTableString() {
		String chart = ""
		String currX
		String currY
		for (int i = 0; i < totalRows; i++) {
			//currX = leftFieldList[i].text
			//currY = fieldList[i].text

			//left side!
			try {
				if (leftFieldList[i].text.trim().equals("")) {
					currX = leftFieldList[i].text
				} else if (systemConverter != null && leftDimension != "") {
					def unitConverter = reverseConverter.getConverter(leftDimension)
					currX = unitConverter.convert(nf.parse(leftFieldList[i].text).doubleValue())
				} else {
					currX = nf.parse(leftFieldList[i].text).toString()
				}
			} catch (NumberFormatException e) {
				currX = ""
			}

			//right side!
			try {
				if (fieldList[i].text.trim().equals("")) {
					currY = fieldList[i].text
				} else if (systemConverter != null && rightDimension != "") {
					def unitConverter = reverseConverter.getConverter(rightDimension)
					currY = unitConverter.convert(nf.parse(fieldList[i].text).doubleValue())
				} else {
					currY = nf.parse(fieldList[i].text)
				}
			} catch (NumberFormatException e) {
				currY = ""
			}


			if (currX.isNumber() && currY.isNumber()) {
				chart += "$currX,$currY;"
			} else if (currX.isNumber()) {
				chart += "$currX,;"
			}
		}
		//println "buildTableString results in $chart"
		return (chart)
	}

}
