package DeploymentLab.PropertyEditor;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * Renders a "fancy" property in the Properties Editor as a single string (and allows for a pop-open window to be triggered by the custom editor.)
 * Created by IntelliJ IDEA.
 * User: ghelman
 * Date: Nov 10, 2009
 * Time: 2:23:37 PM
 * 
 * Modified by : hlim
 * Data : 03/22/2010
 *
 * And again by: ghelman
 * Date: 4-16-2010
 * 
 * This class extends a basic JLabel, and merely hard-wires the contents of said label.
 */

public class CustomCellRenderer extends JLabel implements TableCellRenderer {
	private String fixedText;

	/**
	 * Constructs a blank renderer.
	 */
	 CustomCellRenderer(){
		 super();
	 }

	/**
	 * Constructs a new cell renderer with a fixed label.  If a label is supplied, the cell renderer will always show that label and nothing else.
	 * @param _text a string label to show in this table cell.
	 */
	CustomCellRenderer(String _text){
		super();
		fixedText = _text;
	}

	/**
	 * Returns the custom renderer for ChartContents and multiline cells.
	 * This used to just be a label that said "[Click for Detail]", but with the addition of multiline cells, it got a little fancier.
	 * Now the label will try to print the string contents of the value being passed to it, unless this label was created with a fixed label,
	 * in which case only that will be shown and the value of the underlying data will be ignored.
	 * @param table The JTable asking for the Renderer.
	 * @param value The value to be rendered.
	 * @param isSelected true if the cell is selected.
	 * @param hasFocus true if the cell has focus.
	 * @param row The row of the renderer to be returned.
	 * @param col The column of the renderer to be returned.
	 * @return The renderer component, which is, in fact, "this" - a very thin extension of a JLabel.
	 */
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
		//this.setText("[Click for Detail]");
		String tempText;
		if ( fixedText == null ){
			 tempText = value.toString().trim();
		} else {
			tempText = fixedText;
		}
		this.setText(tempText);
		return (this);
	}

}
