package DeploymentLab.PropertyEditor;

import java.awt.Component;
import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import DeploymentLab.Localizer;


/**
 * Editor for numbers that accounts for the configured localization format and Locale.
 *
 * @author Ken Scoggins
 * @since Jupiter
 */
public class NumberCellEditor extends DefaultCellEditor {

	public NumberCellEditor(final JTextField comp) {
		super(comp);
		this.setClickCountToStart(1);
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		JTextComponent comp = (JTextComponent) super.getTableCellEditorComponent(table, value, isSelected, row, column);
		//comp.setText( Localizer.format( value ).toString() );
		Object v = Localizer.format(value);
		if (v != null) {
			comp.setText(v.toString());
		} else {
			comp.setText("");
		}
		return comp;
	}
}


