package DeploymentLab.PropertyEditor;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.EventObject;


import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.text.JTextComponent;
import javax.swing.text.PlainDocument;

import DeploymentLab.CentralCatalogue;
import DeploymentLab.Localizer;
import DeploymentLab.UIDisplay;
import DeploymentLab.channellogger.*;
import DeploymentLab.Dialogs.DocumentCRFilter;
import DeploymentLab.Model.ComponentModel;
import DeploymentLab.Model.ComponentProperty;
import DeploymentLab.Model.DLComponent;
import DeploymentLab.SceneGraph.DeploymentPanel;
import DeploymentLab.UndoBuffer;


//TODO: this should no extend the selecting table

/**
 * Represents the property editor on the main screen.
 * The model for this table is in PropertyModel.
 *
 * Some of the select-on-edit functionality was adapted from Rob Camick's RXTable
 * at http://tips4java.wordpress.com/2008/10/20/table-select-all-editor/
 *
 * @see PropertyModel
 */
public class PropertyTable extends JTable {
	private final PropertyModel propertyModel;
	private DeploymentPanel deploymentPanel;
	private UndoBuffer undoBuffer;
	private ComponentModel componentModel;
	/** The parent frame of this dialog box.*/
	private JFrame parent;
	private UIDisplay uiDisplay;
	
	public boolean undoOperationInProgress(){
		return ( propertyModel.undoOperationInProgress() );
	}

	/**
	 * Installs the model, and sets the model's reference to the table.
	 * @param pm the model to use for the table
	 * @param ub the system undoBuffer
	 * @param _parent the parent JFrame of this table
	 */
	public PropertyTable(PropertyModel pm, UndoBuffer ub, JFrame _parent, ComponentModel cModel, DeploymentPanel dp, UIDisplay uid) {
		super(pm);
		deploymentPanel = dp;
		propertyModel = pm;
		componentModel = cModel;
		propertyModel.setTable( this ); //let the model know who we are
		undoBuffer = ub;
		parent = _parent;
		uiDisplay = uid;
		this.setColumnModel( new PropertiesColumnModel( propertyModel.getColumnCount() ) );
		this.putClientProperty("JTable.autoStartsEdit", Boolean.TRUE);
		this.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		//this.setSurrendersFocusOnKeystroke(true);
		this.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
		this.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		this.setSelectAllForEdit(true);
	}

	/**
	 * For cases where the parent frame has not yet been initialzed when this table is constructed, this allows us to
	 * slip in a parent after the fact.
	 * @param _parent a JFrame to use as the parent for any dialogs triggered off as custom cell editors
	 */
	public void setParent( JFrame _parent ){
		parent = _parent;
	}

	/**
	 * Returns an appropriate editor for the cell in question.
	 * @param row The Row being edited
	 * @param col The Column being edited
	 * @return An appropriate TableCellEditor for the current cell's type
	 */
	public TableCellEditor getCellEditor(int row, int col) {
		if(propertyModel.getCellValues(row, col) != null)
			return new ListCellEditor<Object>(propertyModel.getCellValues(row, col));
		
		String cname = propertyModel.getCellClassName(row, col);
		
		//System.out.println("cname:" + cname);
		if(cname.equals("java.awt.Color")) {
			return new ColorCellEditor();
		} else if(cname.equals("java.lang.Boolean")) {
			return new BooleanCellEditor();
		} else if( (col > 0) && Localizer.isLocalizable( propertyModel.getProperty( row ) ) ) {
			// Custom Number editor so they are properly localized. Use the default component so it looks the same.
			Component comp = super.getCellEditor(row,col).getTableCellEditorComponent( this, propertyModel.getValueAt(row,col), true, row, col );
			return new NumberCellEditor( (JTextField) comp );
		} else if(cname.equals("DeploymentLab.ChartContents")) {
			DLComponent c = propertyModel.getComponent();
			return new ChartContentsEditor( c );
		} else if(cname.equals("DeploymentLab.ChartContentsCFM")) {
			DLComponent c = propertyModel.getComponent();

			return new LookupTableEditor( c, parent,
				CentralCatalogue.getUIS("propertyTable.ChartContentsCFM.rightLabel"),
				"cfm_table_name",
				"cfm_table",
				CentralCatalogue.getUIS("propertyTable.ChartContentsCFM.dialogTitle"),
				CentralCatalogue.getUIS("propertyTable.ChartContentsCFM.leftLabel1"),
				CentralCatalogue.getUIS("propertyTable.ChartContentsCFM.leftLabel2"),
				CentralCatalogue.getUIS("propertyTable.ChartContentsCFM.topLeftLabel"),
				CentralCatalogue.getUIS("propertyTable.ChartContentsCFM.topRightLabel"),
				9,
				"20,;30,;40,;50,;60,;70,;80,;90,;100,;",
				"percents",
				"airvolume",
				propertyModel.getSystemConverter(),
				propertyModel.getReverseSystemConverter()
				);

		} else if(cname.equals("DeploymentLab.ChartContentsBTU")) {
			DLComponent c = propertyModel.getComponent();

			return new LookupTableEditor( c, parent,
				CentralCatalogue.getUIS("propertyTable.ChartContentsBTU.rightLabel"),
				"btu_table_name",
				"btu_table",
				CentralCatalogue.getUIS("propertyTable.ChartContentsBTU.dialogTitle"),
				CentralCatalogue.getUIS("propertyTable.ChartContentsBTU.leftLabel1"),
				CentralCatalogue.getUIS("propertyTable.ChartContentsBTU.leftLabel2"),
				CentralCatalogue.getUIS("propertyTable.ChartContentsBTU.topLeftLabel"),
				CentralCatalogue.getUIS("propertyTable.ChartContentsBTU.topRightLabel"),
				9,
				"40,;65,;70,;75,;80,;85,;90,;95,;100,;",
				"temperature",
				"sensiblecooling",
				propertyModel.getSystemConverter(),
				propertyModel.getReverseSystemConverter()
				);

		} else if(cname.equals("DeploymentLab.LookupTable")) {
			DLComponent c = propertyModel.getComponent();

			return new LookupTableEditor( c, parent,
				CentralCatalogue.getUIS("propertyTable.LookupTable.rightLabel"),
				"table_name",
				"table",
				CentralCatalogue.getUIS("propertyTable.LookupTable.dialogTitle"),
				CentralCatalogue.getUIS("propertyTable.LookupTable.leftLabel1"),
				CentralCatalogue.getUIS("propertyTable.LookupTable.leftLabel2"),
				CentralCatalogue.getUIS("propertyTable.LookupTable.topLeftLabel"),
				CentralCatalogue.getUIS("propertyTable.LookupTable.topRightLabel"),
				11,
				"",
				"dimensionless",
				"dimensionless",
				null,
				null
				);

		} else if(cname.equals("DeploymentLab.MultiObjectTable")) {

			return new MultiObjectTableEditor( propertyModel.getComponent(), propertyModel.getProperty( row ) );

		} else if(cname.equals("DeploymentLab.ConfigModbusRead")){
			DLComponent c = propertyModel.getComponent();
			ComponentProperty p = propertyModel.getProperty(row);
			return new ConfigModbusEditor(c, p, parent, true, uiDisplay, undoBuffer);
		} else if(cname.equals("DeploymentLab.ConfigModbusWrite")){
			DLComponent c = propertyModel.getComponent();
			ComponentProperty p = propertyModel.getProperty(row);
			return new ConfigModbusEditor(c, p, parent, false, uiDisplay, undoBuffer);

		} else if(cname.equals("DeploymentLab.ConfigBacnetRead")){
			DLComponent c = propertyModel.getComponent();
			ComponentProperty p = propertyModel.getProperty(row);
			return new ConfigBacnetEditor(c, p, parent, true, uiDisplay, undoBuffer);
		} else if(cname.equals("DeploymentLab.ConfigBacnetWrite")){
			DLComponent c = propertyModel.getComponent();
			ComponentProperty p = propertyModel.getProperty(row);
			return new ConfigBacnetEditor(c, p, parent, false, uiDisplay, undoBuffer);


		} else if(cname.equals("DeploymentLab.MultiLineContents") || cname.equals("DeploymentLab.ValidatedMultiLineContents")){
			return new MultiLineCellEditor(propertyModel.getComponent(), propertyModel.getProperty(row), undoBuffer, parent);
		} else if(cname.equals("DeploymentLab.BacnetObject")){
			DLComponent c = propertyModel.getComponent();
			ComponentProperty p = propertyModel.getProperty(row);
			return new BacnetObjectEditor(c, p, parent, componentModel, deploymentPanel);
			
		} else {
			//now we use our custom subclass rather than the default
			//PropertiesCellEditor pe = new PropertiesCellEditor(this);
			//return( pe );

			//back to the default but with customization
			DefaultCellEditor dce = (DefaultCellEditor)super.getCellEditor(row,col);
			System.out.println(dce.getClass().getName() );
			dce.setClickCountToStart(1);
			return dce;
		}
	}

	/**
	 * Returns an appropriate renderer for the cell in question.
	 * @param row The Row being edited
	 * @param col The Column being edited
	 * @return An appropriate TableCellRenderer for the current cell's type
	 */
	public TableCellRenderer getCellRenderer(int row, int col) {
		/* Disabled because using a ComboBox as a cell renderer looks crappy.  Need to find a way to make it look better.
		if(propertyModel.getCellValues(row, col) != null)
			return new ListCellRenderer(propertyModel.getCellValues(row, col));
		*/
		String cname = propertyModel.getCellClassName(row, col);

		if(cname.equals("java.awt.Color")) {
			return new ColorCellRenderer();
		} else if(cname.equals("java.lang.Boolean")) {
			return new BooleanCellRenderer();
        } else if( (col > 0) && Localizer.isLocalizable( propertyModel.getProperty( row ) ) ) {
			// Custom Number renderer so they are properly localized.
			return new NumberCellRenderer();
		} else if(cname.equals("DeploymentLab.ChartContents")) {
			return new ButtonAndLabelCellRenderer("(Table)");
		} else if(cname.equals("DeploymentLab.LookupTable")) {
			return new ButtonAndLabelCellRenderer("(Table)");
		} else if(cname.equals("DeploymentLab.MultiObjectTable")) {
			return new ButtonAndLabelCellRenderer("(Table)");
		} else if(cname.equals("DeploymentLab.ChartContentsCFM")) {
			return new ButtonAndLabelCellRenderer("(Table)");
		} else if(cname.equals("DeploymentLab.ChartContentsBTU")) {
			return new ButtonAndLabelCellRenderer("(Table)");
		} else if(cname.startsWith("DeploymentLab.Config")){
			//bacnet & modbus get same renderer
			//return new ButtonAndLabelCellRenderer();
			DLComponent c = propertyModel.getComponent();
			ComponentProperty p = propertyModel.getProperty(row);
			return new ExpressionRenderer(c,p);


		} else if(cname.equals("DeploymentLab.MultiLineContents") || cname.equals("DeploymentLab.ValidatedMultiLineContents")){
			ComponentProperty p = propertyModel.getProperty(row);
			return new ButtonAndLabelCellRenderer((String)p.getValue());
		} else if(cname.startsWith("DeploymentLab.") ) {
			//otherwise, just use the value of the underlying data
			//return new CustomCellRenderer();
			return new ButtonAndLabelCellRenderer();
		}else {
			return super.getCellRenderer(row, col);
		}


	}

	/**
	 * To be called from an external source if something stops the user from editing the table, but we can't wait for the focuslost event to fire.
	 */
	public void finishEditing(){
		if ( this.isEditing() ) {
			if ( ! this.undoOperationInProgress() ) {
				this.getCellEditor().stopCellEditing();
			}
		}
	}

	private boolean isSelectAllForMouseEvent = false;
	private boolean isSelectAllForActionEvent = false;
	private boolean isSelectAllForKeyEvent = false;

	/*
	 *  Sets the Select All property for for all event types
	 */
	public void setSelectAllForEdit(boolean isSelectAllForEdit) {
		setSelectAllForMouseEvent( isSelectAllForEdit );
		setSelectAllForActionEvent( isSelectAllForEdit );
		setSelectAllForKeyEvent( isSelectAllForEdit );
	}

	/*
	 *  Set the Select All property when editing is invoked by the mouse
	 */
	public void setSelectAllForMouseEvent(boolean isSelectAllForMouseEvent) {
		this.isSelectAllForMouseEvent = isSelectAllForMouseEvent;
	}

	/*
	 *  Set the Select All property when editing is invoked by the "F2" key
	 */
	public void setSelectAllForActionEvent(boolean isSelectAllForActionEvent) {
		this.isSelectAllForActionEvent = isSelectAllForActionEvent;
	}

	/*
	 *  Set the Select All property when editing is invoked by
	 *  typing directly into the cell
	 */
	public void setSelectAllForKeyEvent(boolean isSelectAllForKeyEvent){
		this.isSelectAllForKeyEvent = isSelectAllForKeyEvent;
	}

	/*
	 *  Override to provide Select All editing functionality
	 */
	public boolean editCellAt(int row, int column, EventObject e) {
		boolean result = super.editCellAt(row, column, e);
		if (isSelectAllForMouseEvent || isSelectAllForActionEvent || isSelectAllForKeyEvent) {
			selectAll(e);
		}
		return result;
	}

	/*
	 * Select the text when editing on a text related cell is started
	 */
	private void selectAll(EventObject e) {
		final Component editor = getEditorComponent();

		if (editor == null || ! (editor instanceof JTextComponent)) { return; }

		if (e == null) {
			//((JTextComponent)editor).selectAll();
			selectContents(((JTextComponent) editor));
			return;
		}

		//  Typing in the cell was used to activate the editor
		if (e instanceof KeyEvent && isSelectAllForKeyEvent) {
			//((JTextComponent)editor).selectAll();
			selectContents(((JTextComponent) editor));
			return;
		}

		//  F2 was used to activate the editor
		if (e instanceof ActionEvent && isSelectAllForActionEvent) {
			//((JTextComponent)editor).selectAll();
			selectContents(((JTextComponent) editor));
			return;
		}

		//  A mouse click was used to activate the editor.
		//  Generally this is a double click and the second mouse click is
		//  passed to the editor which would remove the text selection unless
		//  we use the invokeLater()
		if (e instanceof MouseEvent && isSelectAllForMouseEvent) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					//((JTextComponent)editor).selectAll();
					selectContents(((JTextComponent) editor));
				}
			});
		}
	}

	/**
	 * Select the contents of the editor.  If the value in the editor is a negative number,
	 * select everything except the minus sign.
	 * @param ed
	 */
	private static void selectContents(JTextComponent ed) {
		//System.out.println("SELECT CONTENTS");
		boolean isNumeric = true;
		double d = 0;
		try {
			d = Localizer.parseDouble( ed.getText() );
		} catch (NumberFormatException nfe) {
			isNumeric = false;
		}
		if (isNumeric && d < 0) {
			//only select the bit after the negative sign
			String t = ed.getText();
			int minusSign = t.indexOf("-");
			ed.select(minusSign + 1, t.length());
		} else {
			ed.selectAll();
		}
	}

} //end PropertyTable

	/**
	 * Custom class to handle cell editors that auto-save whenever focus is lost.
	 */
	class PropertiesCellEditor extends DefaultCellEditor implements FocusListener {
		private static final Logger log = Logger.getLogger(PropertiesCellEditor.class.getName());
		private PropertyTable parentTable;
		private Object startValue;

		/**
		 * Constructs a new editor by calling the DefaultCellEditor constructor with a text field and setting the new instance as a focus listener of itself.
		 * @param parent the PropertyTable this editor is going to be bolted to
		 */
		public PropertiesCellEditor( PropertyTable parent ){
			super( new JTextField() );
			this.getComponent().addFocusListener( this );
			//we need to specify the border for the text field to (among other things) override the default nimbus huge&glowy border
			Border editorBorder = BorderFactory.createLineBorder(Color.BLACK);
			((JComponent)this.getComponent()).setBorder(editorBorder);
			this.parentTable = parent;
			PlainDocument doc = (PlainDocument)((JTextField)this.getComponent()).getDocument();
			doc.setDocumentFilter(new DocumentCRFilter());

			this.setClickCountToStart(1);
		}

		 /** Implements the <code>TableCellEditor</code> interface.
		  *  Overridden to store the initial value of the cell before editing begins.
		  */
		@Override
		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			startValue = value;
			return super.getTableCellEditorComponent(table, value, isSelected, row, column);
		}

		/**
		 * Forwards the message from the <code>CellEditor</code> to the <code>delegate</code>.
		 * Compares the original value to the current value, and if they are still the same this is converted to a cancel.
		 * (This has the bonus feature of not filling the undo buffer with non-edits as well.)
		 */
		@Override
		public boolean stopCellEditing() {
			//log.trace("stopped cell editing", "default");
			if (  (startValue == null && delegate.getCellEditorValue() != null ) ||  !startValue.equals( delegate.getCellEditorValue()) ){
				//log.trace("save", "default");
				return super.stopCellEditing();	//To change body of overridden methods use File | Settings | File Templates.
			} else {
				//log.trace("cancel", "default");
				super.cancelCellEditing();
				return true;
			}
		}

		/**
		 * Implemented for the FocusListener interface; not used.
		 * @param evt the Focus Event.
		 */
		public void focusGained(FocusEvent evt) { }

		/**
		 * Called when the actual swing component doing the editing loses focus.  For the properties Table, this means that the editing action is over, and we need to save the data.
		 * @param evt the Focus Event.
		 */
		public void focusLost(FocusEvent evt) {
 			//log.trace("focus lost!", "default");
			if ( parentTable.isEditing() ) {
				if ( ! parentTable.undoOperationInProgress() ) {
					//log.trace("checking to see if '" + startValue.toString() + "' == '" + delegate.getCellEditorValue() + "'", "default" );
//					if (startValue == null){
//						log.trace("startvalue is null", "default");
//					}
//					if (delegate == null){
//						log.trace("delegate is null", "default");
//					}
					if ( startValue == null || ! startValue.equals( delegate.getCellEditorValue()) ){
						//log.trace("save", "default");
						this.stopCellEditing();
					} else {
						//log.trace("cancel", "default");
						this.cancelCellEditing();
					}
				}
			}
		}

		public void selectAll(){
			((JTextField)this.getComponent()).selectAll();
		}
	} //End propertiesCellEditor

/**
 * Custom class to stop Java Swing from auto resetting the table column widths anytime you select a new component. This
 * is really annoying when you drag to resize a column then it resets when you select the next component. It
 * happens when the TableModel fires a table change event that makes it believe the structure has changed. This happens
 * anytime you select a new component. The default implementation for JTable is to ignorantly remove the columns and
 * create new ones with all defaults.
 *
 * This class allows us to keep track of the column sizes so we can override the default when the new one is created.
 */
class PropertiesColumnModel extends DefaultTableColumnModel {
	int[] widths;

	public PropertiesColumnModel( int columnCnt ) {
		super();
		widths = new int[ columnCnt ];
	}

	@Override
	public void removeColumn( TableColumn column ) {
		// Remember the column width before it disappears.
		widths[ column.getModelIndex() ] = column.getWidth();
		super.removeColumn( column );
	}

	@Override
	public void addColumn( TableColumn column ) {
		// If we have a saved size, override the default so the column width stays the same.
		if( widths[ column.getModelIndex() ] > 0 ) {
			column.setPreferredWidth( widths[ column.getModelIndex() ] );
		}
		super.addColumn(column);
	}
}
