package DeploymentLab.PropertyEditor;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: ghelman
 * Date: Apr 28, 2010
 * Time: 10:12:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class ButtonCellRenderer extends JButton implements TableCellRenderer {
	private String fixedText;

	/**
	 * Constructs a blank renderer.
	 */
	 ButtonCellRenderer(){
		 super();
		fixedText = null;
	 }

	/**
	 * Constructs a new cell renderer with a fixed label.  If a label is supplied, the cell renderer will always show that label and nothing else.
	 * @param _text a string label to show in this table cell.
	 */
	ButtonCellRenderer(String _text){
		super();
		fixedText = _text;
	}

	/**
	 * Returns the custom renderer for ChartContents and multiline cells.
	 * This used to just be a label that said "[Click for Detail]", but with the addition of multiline cells, it got a little fancier.
	 * Now the label will try to print the string contents of the value being passed to it, unless this label was created with a fixed label,
	 * in which case only that will be shown and the value of the underlying data will be ignored.
	 * @param table The JTable asking for the Renderer.
	 * @param value The value to be rendered.
	 * @param isSelected true if the cell is selected.
	 * @param hasFocus true if the cell has focus.
	 * @param row The row of the renderer to be returned.
	 * @param col The column of the renderer to be returned.
	 * @return The renderer component, which is, in fact, "this" - a very thin extension of a JLabel.
	 */
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
		//this.setPreferredSize( new Dimension( 50, 10 ) );

		/*
		Dimension size = this.getPreferredSize();
		System.out.println( size );
		size.setSize( size.getWidth(), (size.getHeight() + 4 ) );
		this.setPreferredSize(size);
          */

		//System.out.println( this.getSize() );


		String tempText;
		if ( fixedText == null ){
			 tempText = value.toString().trim();
		} else {
			tempText = fixedText;
		}
		this.setText(tempText);
		return (this);
	}


}
