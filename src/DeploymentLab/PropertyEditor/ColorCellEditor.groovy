
package DeploymentLab.PropertyEditor

import java.awt.Color
import java.awt.Component

import java.awt.event.ActionEvent
import java.awt.event.ActionListener

import javax.swing.AbstractCellEditor
import javax.swing.JButton
import javax.swing.JColorChooser
import javax.swing.JDialog
import javax.swing.JTable

import javax.swing.table.TableCellEditor

class ColorCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {
	
	private Color currentColor
	private JButton button
	private JColorChooser colorChooser
	private JDialog dialog
	
	private static final String EDIT = "edit"
	
	ColorCellEditor() {
		button = new JButton()
		button.setActionCommand(EDIT)
		button.addActionListener(this)
		button.setBorderPainted(false)
		
		colorChooser = new JColorChooser()
		dialog = JColorChooser.createDialog(
					button,
					"Pick a color",
					true,  // modal
					colorChooser,
					this,  // OK button handler
					null  // don't need a cancel button handler
		)
	}
	
	void actionPerformed(ActionEvent e) {
		if(EDIT.equals(e.getActionCommand())) {
			button.setBackground(currentColor)
			colorChooser.setColor(currentColor)
			dialog.setVisible(true)
			
			fireEditingStopped()
		} else {
			currentColor = colorChooser.getColor()
		}
	}
	
	Object getCellEditorValue() {
		return currentColor
	}
	
	Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int col) {
		if( value && value instanceof Color ) currentColor = (Color)value
		return button
	}
}

