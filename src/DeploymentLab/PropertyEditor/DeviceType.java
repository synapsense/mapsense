package DeploymentLab.PropertyEditor;

/**
 * @author Gabriel Helman
 * @since Hermes
 * Date: 9/1/11
 * Time: 1:51 PM
 */
public enum DeviceType {
	CRAC("CRAC"),
	CRAH("CRAH"),
	VFD("VFD"),
    RTUMGR("RTUMGR"),
    RTUDX("RTUDX"),
    RTUCHW("RTUCHW"),
	HEARTBEATMGR("HEARTBEATMANAGER");

    private String prettyName;
	private String shortName;

    DeviceType( String name ) {
        this( name, name );
    }

	DeviceType( String pn, String sn ) {
        prettyName = pn;
		shortName = sn;
	}

	public String toString() {
		return prettyName();
	}

    public String prettyName() {
        return prettyName;
    }

    public String shortName() {
        return shortName;
    }
}

