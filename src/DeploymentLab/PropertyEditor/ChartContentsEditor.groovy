package DeploymentLab.PropertyEditor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JComponent
import javax.swing.JDialog;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import javax.swing.table.TableCellEditor;

import groovy.swing.SwingBuilder

import DeploymentLab.ChartContents;
import DeploymentLab.Model.*
import DeploymentLab.Localizer

/**
 * Allows for editing of an efficiency table via a custom UI that pops out of the main Properties Editor.
 * Created by IntelliJ IDEA.
 * User: ghelman
 * Date: Nov 10, 2009
 * Time: 2:29:25 PM
 *
 * Based on Sun's example ColorCellEditor.
 * @see CustomCellRenderer
 * @see ChartContents
 * @see DLComponent
 */
class ChartContentsEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {
	private DLComponent parentComponent;

	private ComponentProperty topProp;
	private ComponentProperty pfProp;

	private JButton button;
	private JDialog dialog;

	private ChartContents currentChart;

	def builder
	private static final String EDIT = "edit";
	private static String rightLabel = " % Efficiency";
	//private def fieldList = [:]
	private Map<Integer, JTextField> fieldList = [:]
	//private def kwList = [:]
	private def maxLoadField
	private def pfField

	/**
	 * Constructor: produces a new Editor connected to a given component.
	 * @param newComponent The component that this property belongs to.  Sets a reference to allow us to edit >1 component property at a time.
	 */
	public ChartContentsEditor( DLComponent newComponent ) {
		parentComponent = newComponent;
		builder = new SwingBuilder();
		currentChart = new ChartContents();

		button = new JButton();
		button.setActionCommand(EDIT);
		button.addActionListener(this);
		button.setBorderPainted(false);

		def okAction
		def cancelButtonAction

		dialog = builder.dialog( title: "Efficiency", modal: true, layout: new BorderLayout(), resizable: false ) {
			def dlgPanel = panel(layout: new BorderLayout(), constraints: BorderLayout.CENTER) {
				panel(constraints: BorderLayout.LINE_START){ glue() }
				panel(constraints: BorderLayout.CENTER) {
					tableLayout{
						tr {
							td { label "Max Load " }
							td { maxLoadField = textField( columns: 5 ) }
							td { label " kVa" }
						}
						tr {
							td { label "Power Factor" }
							td { pfField = textField( columns: 5 ) }
							td { label "" }
						}
						tr { td { label "10% Load = " } ; td { /*kwList[10] = label ''*/ } ; td { fieldList[10] = textField( columns: 5 ) } ; td { label rightLabel } ; }
						tr { td { label "20% Load = " } ; td { /*kwList[20] = label ''*/ } ; td { fieldList[20] = textField( columns: 5 ) } ; td { label rightLabel } ; }
						tr { td { label "30% Load = " } ; td { /*kwList[30] = label ''*/ } ; td { fieldList[30] = textField( columns: 5 ) } ; td { label rightLabel } ; }
						tr { td { label "40% Load = " } ; td { /*kwList[40] = label ''*/ } ; td { fieldList[40] = textField( columns: 5 ) } ; td { label rightLabel } ; }
						tr { td { label "50% Load = " } ; td { /*kwList[50] = label ''*/ } ; td { fieldList[50] = textField( columns: 5 ) } ; td { label rightLabel } ; }
						tr { td { label "60% Load = " } ; td { /*kwList[60] = label ''*/ } ; td { fieldList[60] = textField( columns: 5 ) } ; td { label rightLabel } ; }
						tr { td { label "70% Load = " } ; td { /*kwList[70] = label ''*/ } ; td { fieldList[70] = textField( columns: 5 ) } ; td { label rightLabel } ; }
						tr { td { label "80% Load = " } ; td { /*kwList[80] = label ''*/ } ; td { fieldList[80] = textField( columns: 5 ) } ; td { label rightLabel } ; }
						tr { td { label "90% Load = " } ; td { /*kwList[90] = label ''*/ } ; td { fieldList[90] = textField( columns: 5 ) } ; td { label rightLabel } ; }
						tr { td { label "100% Load = " } ; td { /*kwList[100] = label ''*/ } ;td { fieldList[100] = textField( columns: 5 ) } ; td { label rightLabel } ; }
					}
				}
				panel(constraints: BorderLayout.LINE_END){ glue() }
				hbox (constraints: BorderLayout.PAGE_END ) {

					okAction = action(name: 'OK', mnemonic: KeyEvent.VK_O, closure: { saveAndExitAction() } )
					cancelButtonAction = action(name: 'Cancel', mnemonic: KeyEvent.VK_C, closure: { cancelAction() } )

					panel(layout: new FlowLayout(FlowLayout.TRAILING) ){
						button ( text: 'OK', mnemonic: KeyEvent.VK_O, defaultButton: true, action: okAction )
						button ( text: 'Cancel', mnemonic: KeyEvent.VK_C, defaultButton: false, action: cancelButtonAction )
					}
				}
			}
			dlgPanel.registerKeyboardAction(okAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			dlgPanel.registerKeyboardAction(cancelButtonAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		}
        dialog.pack();

		//the component property technically being edited at the moment is the table values themselves.  However, from a UI persepective, we have two other fields we need to edit at the same time.
		//So, we grab those values now.
		topProp = parentComponent.getComponentProperty('top')
		maxLoadField.text = Localizer.format( topProp.value )

		pfProp = parentComponent.getComponentProperty('powerFactor')
		pfField.text = Localizer.format( pfProp.value )
	}

	/**
	 * Exits the dialog due to the user clicking the OK button.  Flushes the GUI widgets to the ChartContents object (which shouldn't be needed, but I'm paranoid.
	 */
	def saveAndExitAction() {
		currentChart = new ChartContents( buildTableString(), (double)( Localizer.parseDouble( maxLoadField.text ) * Localizer.parseDouble( pfField.text ) ) );
		dialog.hide()
	}

	/**
	 * Exits the dialog due to the user clicking the CANCEL button. Does not save any changes.
	 */
	def cancelAction() {
		//reset the GUI, just in case
		loadCurrentChartString(currentChart.getPercentageString());
        def loadVal = topProp.getValue() ?: 1
        def pfVal   = pfProp.getValue() ?: 1
		maxLoadField.text = Localizer.format( loadVal ) //load the max load
		pfField.text = Localizer.format( pfVal ) //load the power factor
		currentChart.apex =  (loadVal * pfVal) //but, make sure we give the chart the apex as well
		dialog.hide()
	}

	/**
	 * Runs if the user has clicked the button returned from getTableCellEditorComponent.
	 */
	public void actionPerformed(ActionEvent e) {
		if(EDIT.equals(e.getActionCommand())) {
			//button has been clicked as an edit command
			loadCurrentChartString(currentChart.getPercentageString());
			dialog.show();
			fireEditingStopped();
		} else {
			//button has been clicked for some other command, which implies that the world is broken.
			currentChart = new ChartContents( buildTableString(), (double)(Localizer.parseDouble( maxLoadField.text ) * Localizer.parseDouble( pfField.text )) );
		}
	}

	/**
	 * Save and return the user-entered chart.
	 */
	public Object getCellEditorValue() {
		topProp.setValue( Localizer.parseDouble( maxLoadField.text ) ); //save the maxLoad value
		pfProp.setValue( Localizer.parseDouble( pfField.text ) ); //save the powerFactor value
		//println "the value that the chart is going to return is ${currentChart.getValueString()}"
		return currentChart.getValueString(); //return the chart string itself in value form for saving
	}

	/**
	 * Loads the data from the "data store" into a ChartContents and returns the editor component (in this case, a button to DISPLAY the component, but still.)
	 */
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int col) {
        def loadVal = topProp.getValue() ?: 1
        def pfVal   = pfProp.getValue() ?: 1
		maxLoadField.text = Localizer.format( loadVal ) //load the max load
		pfField.text = Localizer.format( pfVal ) //load the power factor
		currentChart = new ChartContents( value.toString() ); //always stored in "value mode"
        currentChart.apex =  (loadVal * pfVal) //but, make sure we give the chart the apex as well
		return button;
	}

	/**
	* Takes a tablestring and loads it into the screen widgets.
	* @param sTable a tablestring representing the chart to be displayed in the GUI.
	*/
	private void loadCurrentChartString( String sTable ) {
		//println "loading chart into editor: $sTable"
		def table = ChartContents.parseTable( sTable )
		table.each{
			int intX = (int)it.x
			//println "current point in chart load is $it, int x is $intX"
			fieldList[intX].text = Localizer.format( it.y )
		}
	}

	/**
	 * Converts the the user entries in the screen widgets to a table string
	 * @return a tablestring matching the values currently in the GUI.
	 */
	private String buildTableString() {
		String chart = ""
		def currentEff = 0.0
		def currentLoad = 0.0
		//in fieldlist, k is the load%, and k is the textfield component holding the eff%
		fieldList.each{ k, v ->
            try {
				currentLoad = k.toDouble()
				currentEff = Localizer.parseDouble( v.text )
				chart += "$currentLoad,$currentEff;"
			} catch( NumberFormatException e ) {
                // Do nothing. The entry is just skipped.
            }
		}
		//println "buildTableString results in $chart"
		return ( chart )
	}
}