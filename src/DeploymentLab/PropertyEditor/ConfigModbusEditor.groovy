package DeploymentLab.PropertyEditor

import java.awt.BorderLayout
import java.awt.Component
import java.awt.FlowLayout
import java.awt.Dialog.ModalityType
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.awt.event.KeyEvent

import javax.swing.AbstractCellEditor
import javax.swing.BorderFactory
import javax.swing.ButtonGroup

import javax.swing.JButton
import javax.swing.JComboBox
import javax.swing.JComponent
import javax.swing.JDialog
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JOptionPane
import javax.swing.JPanel
import javax.swing.JRadioButton
import javax.swing.JSlider
import javax.swing.JTable
import javax.swing.JTextArea
import javax.swing.JTextField
import javax.swing.KeyStroke
import javax.swing.SpringLayout
import javax.swing.event.ChangeListener
import javax.swing.event.ChangeEvent
import javax.swing.event.DocumentListener
import javax.swing.event.DocumentEvent
import javax.swing.event.UndoableEditListener
import javax.swing.event.UndoableEditEvent
import javax.swing.table.TableCellEditor

import groovy.swing.SwingBuilder

import DeploymentLab.CentralCatalogue
import DeploymentLab.FileUtil
import DeploymentLab.ProjectSettings
import DeploymentLab.SpringUtilities
import DeploymentLab.channellogger.Logger

import DeploymentLab.Integration.Result
import DeploymentLab.Integration.Evaluator
import DeploymentLab.Integration.ExpressionTester
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentProperty
import DeploymentLab.Model.DLComponent
import javax.swing.JEditorPane
import java.awt.Font
import java.awt.Dimension

import javax.swing.JToggleButton

import DeploymentLab.UIDisplay
import net.miginfocom.swing.MigLayout
import DeploymentLab.UndoBuffer
import javax.swing.text.DefaultCaret

/**
 * Configures a configurable modbus expression.
 *
 * Final score: 18 layout manager instances.
 *
 * @author Gabriel Helman
 * Date: Nov 9, 2010
 * Time: 4:48:58 PM
 * @since Hermes
 */
class ConfigModbusEditor extends AbstractCellEditor implements TableCellEditor, ActionListener, DocumentListener, ChangeListener, UndoableEditListener {
	private static final Logger log = Logger.getLogger(ConfigModbusEditor.class.getName())
	private static final String EDIT = "edit";
	private static final String SIGIL = "OWLBEAR"
	private static final Integer MAXMESSAGE = 32;

	private String DIALOGTITLE
	private boolean readMode
	private DeviceType deviceType
	private DLComponent parentComponent;
	private ComponentProperty parentProperty
	private ComponentModel cmodel
	private SwingBuilder builder

	private JButton button;
	private JDialog dialog;
	/** The parent frame of this dialog box. */
	private JFrame parent

	JPanel dlgPanel, buttonPanel, simplePanel, advPanel, internalSimplePanel, internalAdvPanel, buttonAdvPanel, testPanel, outerTestPanel
	JTextArea scriptText
	JRadioButton simpleButton, advButton
	ButtonGroup modeGroup
	JTextField taRegister, taActionMod, taTestVal
	JComboBox cbDataType, cbAction, cbActionBase
	JLabel lResult, lTestVal
	JSlider activeSwitch
	JPanel bigPanel
	JPanel helpPanel
	JEditorPane examplesBox
	JToggleButton exampleButton

    Dimension basePanelSize
    Dimension expandedPanelSize

	private String expression
	/** From the syntax checker */
	private Result currentResult
	private String modbusHost
	private String devid
	private Integer valid = 0
	/** from the webservice expression tester */
	private Boolean currentTestResult = false
	private Boolean optional = true
	private UIDisplay uiDisplay;

	private JLabel lCurrentValidState
	private String originalExpression = ""
	private JTextArea lTestResultDisplay
	private UndoBuffer ub

	/**
	 * Constructor: produces a new Editor connected to a given component.
	 * @param newComponent The component that this property belongs to.  Sets a reference to allow us to edit >1 component property at a time.
	 */
	public ConfigModbusEditor(DLComponent newComponent,
							  ComponentProperty prop,
							  JFrame parent,
							  boolean _READ,
							  UIDisplay uid,
							  UndoBuffer undo
	) {

		//load in the setup info
		readMode = _READ
		uiDisplay = uid
		ub = undo

		parentComponent = newComponent;
		parentProperty = prop
		cmodel = parentComponent.getModel()

		if ( parentComponent.getType().toLowerCase().contains("vfd") ||
		     (prop.getParcel() && prop.getParcel().toLowerCase().contains("vfd")) ) {
			deviceType = DeviceType.VFD
		} else if (parentComponent.getType().toLowerCase().contains("crac")) {
			deviceType = DeviceType.CRAC
        } else if (parentComponent.getType().toLowerCase().contains("rtu_manager")) {
            deviceType = DeviceType.RTUMGR
		} else if (parentComponent.getType().toLowerCase().contains("rtu_dx")) {
            deviceType = DeviceType.RTUDX
		} else if (parentComponent.getType().toLowerCase().contains("rtu_chw")) {
            deviceType = DeviceType.RTUCHW
		} else if (parentComponent.getType().toLowerCase().contains("room")) {
            deviceType = DeviceType.HEARTBEATMGR
		} else {
			deviceType = DeviceType.CRAH
		}

		DIALOGTITLE = "$deviceType: ${parentProperty.getDisplayName()}"
		DIALOGTITLE += (readMode ? " (READ)" : " (WRITE)")

		modbusHost = parentComponent.getPropertyValue( "ip", parentProperty.getParcel() )
		devid = parentComponent.getPropertyValue( "devid", parentProperty.getParcel() )

		builder = new SwingBuilder();

		button = new JButton();
		button.setActionCommand(EDIT);
		button.addActionListener(this);
		button.setBorderPainted(false);

		//def regVF = new RegisterNumberVerifier()
		//def modVF = new ActionModifierVerifier()

		modeGroup = builder.buttonGroup()

		String[] dataTypeChoices = ["Int16", "UInt16", "Int32", "UInt32", "Coil", "Float32", "Float64"];
		String[] actionChoices = ["None", "Bitmask", "Scale", "Offset", "Equal To"];
		String[] baseChoices = ["2", "10", "16"];

		def testAction = builder.action(name: 'Test', mnemonic: KeyEvent.VK_T, closure: { testAction() })
		def okAction = builder.action(name: 'OK', mnemonic: KeyEvent.VK_O, closure: { saveAndExitAction() })
		def cancelButtonAction = builder.action(name: 'Cancel', mnemonic: KeyEvent.VK_C, closure: { cancelAction() })
		def exampleAction = builder.action(name: 'Examples', mnemonic: KeyEvent.VK_E, closure: { showExamples() })

		Dictionary<Integer, JLabel> labels = new Hashtable<Integer, JLabel>()
		labels.put(0, builder.label("Disabled"))
		labels.put(1, builder.label("Active"))

		//activeSwitch = builder.slider( minimum: 0, maximum: 1, orientation: JSlider.HORIZONTAL, labelTable: labels, paintLabels: true, paintTicks: true, snapToTicks: true )
		activeSwitch = new JSlider(JSlider.HORIZONTAL, 0, 1, 1)
		activeSwitch.setLabelTable(labels)
		activeSwitch.setPaintTicks(true)
		activeSwitch.setPaintLabels(true)
		activeSwitch.setSnapToTicks(true)
		activeSwitch.addChangeListener(this)

		examplesBox = new JEditorPane("text/html", "")
		examplesBox.setEditable(false)
		examplesBox.setFont(new Font("SansSerif", Font.PLAIN, 12)) //to be LaF-change immune, always use logical font names
		examplesBox.setPreferredSize(new Dimension(495, 500))
		examplesBox.setText(FileUtil.inputStreamToString(this.class.getClassLoader().getResourceAsStream(CentralCatalogue.getApp("ConfigModbus.examplesFile"))))
		examplesBox.setCaretPosition(0)

		dialog = builder.dialog(owner: parent, title: DIALOGTITLE, modalityType: ModalityType.APPLICATION_MODAL, layout: new BorderLayout(), resizable: false) {

			bigPanel = panel(layout: new SpringLayout()) {

				dlgPanel = panel(layout: new SpringLayout()) {
					panel(layout: new FlowLayout(FlowLayout.LEADING)) {
						widget(activeSwitch)
					}
					simplePanel = panel(layout: new SpringLayout()) {
						simpleButton = radioButton(text: "Simple", selected: true, actionPerformed: {setMode(true)})
						internalSimplePanel = panel(layout: new SpringLayout(), border: BorderFactory.createEtchedBorder()) {
							label "Register"
							taRegister = textField(columns: 10)
							label "Data Type"
							cbDataType = comboBox(new JComboBox(dataTypeChoices), actionPerformed: {constructExpression()})
							label "Action"
							cbAction = comboBox(new JComboBox(actionChoices), actionPerformed: {constructExpression()})
							label "Action Modifier"
							taActionMod = textField(columns: 10)
							label "Action Modifier Base"
							cbActionBase = comboBox(new JComboBox(baseChoices), actionPerformed: {constructExpression()})

						}
					}
					advPanel = panel(layout: new SpringLayout()) {
						advButton = radioButton(text: "Advanced", selected: false, actionPerformed: {setMode(false)})
						internalAdvPanel = panel(layout: new SpringLayout(), border: BorderFactory.createEtchedBorder()) { //, preferredSize: [378,106]  ){
							panel(layout: new BorderLayout()) { //, preferredSize: [363, 80]) {
								scrollPane(constraints: BorderLayout.CENTER) {
									scriptText = textArea(columns: 40, lineWrap: true, rows: 3, wrapStyleWord: true)
								}
							}

                            // The result label dynamically changing causes the layout manager to screw up resizing under certain circumstances. It is rare
                            // and not a big deal so it's not worth the hassle to fix. We're compensating for it in other ways. To see it mess up the width,
                            // open the example pane, enter enough Advanced text to make a scrollbar visible, then go between valid syntax and error syntax
                            // a couple of times. You will see the pane grow. To see it mess up the height, open a version of the editor that does not have
                            // the "Disable/Active" slider, enter invalid Advanced syntax, then click Examples. You'll see the bottom of the window flash as
                            // it messes up the height then we catch and reset it real quick.
							buttonAdvPanel = panel(layout: new SpringLayout()) {
								panel(layout: new FlowLayout(FlowLayout.LEADING)) {
									lResult = label(text: "Syntax: OK", preferredSize: [250, 32])
								}
								panel(layout: new FlowLayout(FlowLayout.TRAILING)) {
									exampleButton = toggleButton(action: exampleAction, selected: false)
								}
							} //end buttonAdvPanel
						}
					}

					outerTestPanel = panel(layout: new SpringLayout()) {
						/*
						testPanel = panel(layout: new SpringLayout(), border: BorderFactory.createEtchedBorder()) {
							lTestVal = label("Test Write Value")
							taTestVal = textField(columns: 10)
						}
						*/
						testPanel = panel(layout: new MigLayout("fillx"), border: BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),"Test Write Value")) {
							taTestVal = textField(columns: 10, constraints: "growx")
						}
					}

					/*
					buttonPanel = panel(layout: new SpringLayout(), constraints: BorderLayout.SOUTH) {
						//panel(layout:new FlowLayout(FlowLayout.LEADING), border: BorderFactory.createTitledBorder("Test Result")){
						panel(layout:new MigLayout(), border: BorderFactory.createTitledBorder("Test Result")){
							lTestResultDisplay = textArea(text:"", rows:3, columns:25, editable:false)
						}
						panel(layout:new FlowLayout(FlowLayout.TRAILING)){ }

						panel(layout: new FlowLayout(FlowLayout.LEADING)) {
							uiDisplay.addUIChangeListener(
								button(text: 'Test', mnemonic: KeyEvent.VK_T, defaultButton: false, action: testAction), 'testExpression')
								lCurrentValidState = label(text: "Valid: ${getCurrentValidState()}")
						}
						panel(layout: new FlowLayout(FlowLayout.TRAILING)) {
							button(text: 'OK', mnemonic: KeyEvent.VK_O, defaultButton: true, action: okAction)
							button(text: 'Cancel', mnemonic: KeyEvent.VK_C, defaultButton: false, action: cancelButtonAction)
						}
					} //end buttonPanel
*/

					buttonPanel = panel(layout: new MigLayout("fillx") ) {
						panel(layout:new MigLayout("fillx"), border: BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),"Test Result"), constraints: "growx, span 2, wrap"){
                            scrollPane(constraints: "growx") {
                                lTestResultDisplay = textArea(text:"", rows:3, editable:false, lineWrap: true, wrapStyleWord: true, constraints: "growx")
                            }
						}

						panel(layout: new FlowLayout(FlowLayout.LEADING), constraints: "") {
							uiDisplay.addUIChangeListener(button(text: 'Test', mnemonic: KeyEvent.VK_T, defaultButton: false, action: testAction), 'testExpression')
							lCurrentValidState = label(text: "Valid: ${getCurrentValidState()}")
						}
						panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: "gapleft unrel, alignx trailing, wrap") {
							button(text: 'OK', mnemonic: KeyEvent.VK_O, defaultButton: true, action: okAction)
							button(text: 'Cancel', mnemonic: KeyEvent.VK_C, defaultButton: false, action: cancelButtonAction)
						}
					} //end buttonPanel


				} //end dlgPanel (the left side)


				/*
			    helpPanel = panel(layout: new BorderLayout()) {
					scrollPane( constraints: BorderLayout.CENTER ){
						widget(examplesBox)
					}
				}
				*/

			} //end bigPanel


			dlgPanel.registerKeyboardAction(okAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			dlgPanel.registerKeyboardAction(cancelButtonAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			dlgPanel.registerKeyboardAction(exampleAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		}

		//define the helpPanel out here and then add it to the layout when the user asks
		helpPanel = builder.panel(layout: new BorderLayout()) {
			scrollPane(constraints: BorderLayout.CENTER) {
				widget(examplesBox)
			}
		}

		//dialog.setPreferredSize(new Dimension(423, 700))

		modeGroup.add(simpleButton)
		modeGroup.add(advButton)

		taRegister.getDocument().addDocumentListener(this)
		taActionMod.getDocument().addDocumentListener(this)
		scriptText.getDocument().addUndoableEditListener(this)

        // Don't autoscroll to the bottom of the Result text area if we set lots of text that needs scrollbars.
        ((DefaultCaret)lTestResultDisplay.getCaret()).setUpdatePolicy(DefaultCaret.NEVER_UPDATE)

		//taRegister.setInputVerifier( regVF )
		//taActionMod.setInputVerifier( modVF )

		int dlgRows
		if (!readMode) {
			testPanel.setVisible(true)
			taTestVal.setVisible(true)
			//lTestVal.setVisible(true)
			dlgRows = 5
		} else {
			testPanel.setVisible(false)
			taTestVal.setVisible(false)
			//lTestVal.setVisible(false)
			dlgRows = 5
		}
/*
		SpringUtilities.makeCompactGrid(testPanel,
				1, 2,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad
*/
		SpringUtilities.makeCompactGrid(outerTestPanel,
				1, 1,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad
/*
		SpringUtilities.makeCompactGrid(buttonPanel,
				2, 2,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad
  */
		SpringUtilities.makeCompactGrid(internalSimplePanel,
				5, 2,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad

		SpringUtilities.makeCompactGrid(simplePanel,
				2, 1,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad

		SpringUtilities.makeCompactGrid(buttonAdvPanel,
				1, 2,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad

		SpringUtilities.makeCompactGrid(internalAdvPanel,
				2, 1,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad

		SpringUtilities.makeCompactGrid(advPanel,
				2, 1,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad

		SpringUtilities.makeCompactGrid(dlgPanel,
				dlgRows, 1,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad

		SpringUtilities.makeCompactGrid(bigPanel,
				///1, 2,   //rows, cols
				1, 1,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad

		dialog.pack();

        // Save the base size that the layout manager came up with so we can reset it later when the layout manager screws it up.
        basePanelSize = bigPanel.getSize()
	}

	/**
	 * Exits the dialog due to the user clicking the OK button.  Flushes the GUI widgets to the ChartContents object.
	 */
	def saveAndExitAction() {
		if (!currentResult.getPassed()) {
			if (JOptionPane.YES_OPTION != JOptionPane.showConfirmDialog(dialog, "Syntax does not pass.  Really save?", "Syntax Check Fails", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE)) {
				return
			}
		}

		ub.startOperation()
		parentComponent.setPropertyValue("${parentProperty.getName()}String", getConfigString())

		valid = -1
		//special case: crah valve control
		if (deviceType == DeviceType.CRAH && (parentProperty.getName().contains("rat") || parentProperty.getName().contains("sat"))) {
			//this property is actually required, even though it has the optional bit set
			if (activeSwitch.getValue() == 0) {
				valid = 2
			}
		}

		if (valid < 0) {
			if (activeSwitch.getValue() == 0) {
				valid = 1
			} else if (activeSwitch.getValue() == 1 && currentResult.getPassed() && currentTestResult) {
				valid = 1
			} else {
				valid = 0
			}
		}
		parentComponent.setPropertyValue("${parentProperty.getName()}Valid", valid)

		expression = scriptText.getText()
		parentComponent.setPropertyValue(parentProperty.getName(), getCellEditorValue())

		ub.finishOperation()
		dialog.hide()
	}


	boolean getCurrentValidState(){
		boolean result = false
		boolean currentDialog = (activeSwitch.getValue() == 1 && currentResult?.getPassed() && currentTestResult)
		boolean fromComponent = parentComponent.getPropertyValue("${parentProperty.getName()}Valid").toString().toBoolean()
		boolean changedFromModel = false
		if(scriptText.getText().trim().equals(originalExpression) && currentTestResult == false){
			changedFromModel = false
		} else {
			changedFromModel = true
		}
		//println "getCurrentValidState() changedFromModel: $changedFromModel fromComponent $fromComponent currentDialog: $currentDialog "
		//println "scriptText = '${scriptText.getText().trim()}' og = '$originalExpression'"
		if (changedFromModel){
			result = currentDialog
		} else {
			result = fromComponent
		}
		return result
	}

	void refreshValidStateLabel(){
		//lCurrentValidState.setText("Valid: ${getCurrentValidState()}")
		if ( getCurrentValidState() ){
			lCurrentValidState.setText(CentralCatalogue.getUIS("expressionEditor.expressionValid"))
		} else {
			lCurrentValidState.setText(CentralCatalogue.getUIS("expressionEditor.expressionInvalid"))
			lTestResultDisplay.text = ""
		}
	}


	/**
	 * Exits the dialog due to the user clicking the CANCEL button. Does not save any changes.
	 */
	def cancelAction() {
		//reload the old expression
		expression = parentComponent.getPropertyValue(parentProperty.getName()).toString()
		dialog.hide()
	}

	/**
	 * Runs if the user has clicked the button returned from getTableCellEditorComponent.
	 */
	public void actionPerformed(ActionEvent e) {

		if (EDIT.equals(e.getActionCommand())) {
			//button has been clicked as an edit command, show the dialog
			dialog.setLocationRelativeTo(parent)
			dialog.show();
			fireEditingStopped();
		} else {
			println "Action command! ${e.getActionCommand()}"
		}

	}

	/**
	 * Returns the value to be saved back into the data model.
	 */
	public Object getCellEditorValue() {
		if (activeSwitch.getValue() == 1) {
			return expression
		}
		return ""
	}

	/**
	 * Loads the data from the "data store" into the dialog and returns the editor component (in this case, a button to DISPLAY the component, but still.)
	 */
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int col) {

		//pull the data out of the component property and populate the table
		//scriptText.setText( parentComponent.getPropertyValue("modbusConfigExpression").toString() )

		//store this in case we want to compare things to it
		originalExpression = parentComponent.getPropertyValue(parentProperty.getName()).toString()

		//default:  0;40001;Int16;None;10;10;true
		def configString = parentComponent.getPropertyValue("${parentProperty.getName()}String").toString()
		def config = configString.split(";")

		if (Integer.parseInt(config[0]) == 2) {
			optional = false
			activeSwitch.setValue(1)
			activeSwitch.setVisible(false)
		} else {
			activeSwitch.setValue(Integer.parseInt(config[0]))
		}


		taRegister.setText(config[1])
		cbDataType.setSelectedItem(config[2])
		cbAction.setSelectedItem(config[3])
		taActionMod.setText(config[4])
		cbActionBase.setSelectedItem(config[5])
		if (Boolean.parseBoolean(config[6])) {
			simpleButton.selected = true
			constructExpression()
			setMode(true)
			expression = scriptText.getText()
		} else {
			advButton.selected = true
			setMode(false)
			expression = parentComponent.getPropertyValue(parentProperty.getName()).toString()
			scriptText.setText(expression)
		}

		//do this last, such that it'll override the simple/advanced radio button
		if (activeSwitch.getValue() == 0) {
			enableEverything(false)
		} else {
			enableEverything(true)
		}
		return button;
	}

	/**
	 * Constructs an expression based on the simple mode choices and installs it in the text area.
	 */
	protected void constructExpression() {
		/*
		String funcHead = ""
		if ( readMode ){
			funcHead = "read"
		} else {
			funcHead = "write"
		}
		String func = "${funcHead}_${cbDataType.getSelectedItem().toString().toLowerCase()}(${taRegister.getText()})"
		*/
		String func = ""
		if (readMode) {
			func = "read_${cbDataType.getSelectedItem().toString().toLowerCase()}(${taRegister.getText()}) $SIGIL"
		} else {
			switch (cbDataType.getSelectedItem().toString().toLowerCase()) {
				case "int16":
				case "uint16":
					func = "write_register16(${taRegister.getText()}, Value $SIGIL)"
					break;
				case "int32":
				case "uint32":
					func = "write_register32(${taRegister.getText()}, Value $SIGIL)"
					break;
				case "coil":
					func = "write_coil(${taRegister.getText()}, Value $SIGIL)"
					break;
				case "float32":
					func = "write_float32(${taRegister.getText()}, Value $SIGIL)"
					break;
				case "float64":
					func = "write_float64(${taRegister.getText()}, Value $SIGIL)"
					break;
			}

		}

		String expr = ""
		switch (cbAction.getSelectedItem().toString().toLowerCase()) {
			case "none":
				expr = func.replace(SIGIL, "")
				break
			case "bitmask":
				if (readMode) {
					expr = func.replace(SIGIL, " bor " + buildActionMod())
				} else {

					expr = func.replace(SIGIL, "")
				}
				break

			case "scale":
				if (readMode) {
					//expr = func + " / " + buildActionMod()
					expr = func.replace(SIGIL, " / " + buildActionMod())
				} else {
					//expr = func + " * " + buildActionMod()
					expr = func.replace(SIGIL, " * " + buildActionMod())
				}
				break

			case "offset":
				if (readMode) {
					//expr = func + " + " + buildActionMod()
					expr = func.replace(SIGIL, " + " + buildActionMod())
				} else {
					//expr = func + " - " + buildActionMod()
					expr = func.replace(SIGIL, " - " + buildActionMod())
				}
				break

			case "equal to":
				if (readMode) {
					//expr = func + " == " + buildActionMod()
					expr = func.replace(SIGIL, " == " + buildActionMod())
				} else {
					expr = func.replace(SIGIL, "")
				}
				break
		}
		scriptText.setText(expr)
		checkExpression(expr)
	}


	protected String buildActionMod() {
		String result = ""
		if (cbActionBase.getSelectedItem().toString().equals("10")) {
			result = taActionMod.getText()
		} else {
			result = cbActionBase.getSelectedItem().toString() + "#" + taActionMod.getText()
		}
		return result
	}

	protected void checkExpression(String expressionToCheck) {
		currentResult = Evaluator.evaluate(expressionToCheck, MAXMESSAGE)
		lResult.setText(currentResult.getMessage())
		if (currentResult.getPassed()) {
			expression = scriptText.getText()
		}

		refreshValidStateLabel()
	}

	void changedUpdate(DocumentEvent e) {
		constructExpression()
	}

	void insertUpdate(DocumentEvent e) {
		constructExpression()
	}

	void removeUpdate(DocumentEvent e) {
		constructExpression()
	}

	/**
	 * Callback for when the contents of the scriptbox changes for any reason
	 * @param e
	 */
	void undoableEditHappened(UndoableEditEvent e) {
		//a text change means we need to re-test
		currentTestResult = false
		checkExpression(scriptText.getText())
	}

	/**
	 * Fired when the user flips the active/disabled slider.
	 * @param e the ChangeEvent from the JSlider
	 */
	void stateChanged(ChangeEvent e) {
		JSlider source = (JSlider) e.getSource();
		if (!source.getValueIsAdjusting()) {
			int mode = (int) source.getValue();
			if (mode == 0) {
				enableEverything(false)
			} else {
				enableEverything(true)
			}
			refreshValidStateLabel()
		}
	}

	private void enableEverything(boolean enable) {
		if (!enable) {
			taRegister.setEnabled(false)
			taActionMod.setEnabled(false)
			cbDataType.setEnabled(false)
			cbAction.setEnabled(false)
			cbActionBase.setEnabled(false)
			scriptText.setEnabled(false)
			simpleButton.setEnabled(false)
			advButton.setEnabled(false)
		} else {
			simpleButton.setEnabled(true)
			advButton.setEnabled(true)
			if (simpleButton.selected) {
				setMode(true)
			} else {
				setMode(false)
			}
		}
	}

	private void setMode(boolean simple) {
		if (simple) {
			//enable top bits, disable bottom
			taRegister.setEnabled(true)
			taActionMod.setEnabled(true)
			cbDataType.setEnabled(true)
			cbAction.setEnabled(true)
			cbActionBase.setEnabled(true)
			scriptText.setEnabled(false)
			constructExpression()
		} else {
			//vice-versa
			taRegister.setEnabled(false)
			taActionMod.setEnabled(false)
			cbDataType.setEnabled(false)
			cbAction.setEnabled(false)
			cbActionBase.setEnabled(false)
			scriptText.setEnabled(true)
		}
	}

	/**
	 * Builds the configuration string representing the state of this manually-configured modus property.
	 *
	 * The config string is a semi-colon delimited string with these fields:
	 *
	 * active;register;datatype;action;mod;base;isSimple
	 *
	 * active: 0, 1, 2
	 * 0 = inactive
	 * 1 = active
	 * 2 = required, cannot be deactivated
	 *
	 * register: the value of the register field of the edit box
	 *
	 * datatype: one of dataTypeChoices: "Int16", "UInt16", "Int32", "UInt32", "Coil", "Float32", "Float64"
	 *
	 * action: one of actionChoices: "None", "Bitmask", "Scale", "Offset", "Equal To"
	 *
	 * mod: the text value of the actionMod field
	 *
	 * base: one of baseChoices: "2", "10", "16"
	 *
	 * isSimple: "true", "false" if the dialog is set to Simple mode
	 *
	 * @return the Configuration String
	 */
	public String getConfigString() {
		String result = ""

		if (optional) {
			result += activeSwitch.value.toString() + ";"
		} else {
			result += "2;"
		}
		result += taRegister.getText() + ";"
		result += cbDataType.getSelectedItem().toString() + ";"
		result += cbAction.getSelectedItem().toString() + ";"
		result += taActionMod.getText() + ";"
		result += cbActionBase.getSelectedItem().toString() + ";"
		result += simpleButton.selected ? "true" : "false"
		return result
	}

	/**
	 * Helper function to split up the config string.
	 *
	 * Config String is: active;register;datatype;action;mod;base;isSimple
	 * @param configString
	 * @return
	 * @see ConfigModbusEditor#getConfigString()
	 */
	public static Map<String, String> parseConfigString(String configString) {
		def config = configString.split(";")
		def result = [:]
		result["active"] = config[0]
		result["register"] = config[1]
		result["datatype"] = config[2]
		result["action"] = config[3]
		result["mod"] = config[4]
		result["base"] = config[5]
		result["isSimple"] = config[6]
		return result
	}

	/**
	 * Toggles the examples sidecar on and off.
	 */
	private void showExamples() {
		if (!exampleButton.selected) {
			//hide
			bigPanel.remove(helpPanel)
			bigPanel.setPreferredSize( basePanelSize )
			SpringUtilities.makeCompactGrid(bigPanel,
					1, 1,   //rows, cols
					6, 6,   //initX, initY
					5, 5);  //xPad, yPad
			dialog.pack()
			bigPanel.repaint()
		} else {
			//show
            // The layout manager usually does a good job the first time, so let it. For some reason, it sucks all times after that, so coax it a bit.
            if( expandedPanelSize ) {
                bigPanel.setPreferredSize( expandedPanelSize )
            }
			bigPanel.add(helpPanel)
			SpringUtilities.makeCompactGrid(bigPanel,
					1, 2,   //rows, cols
					6, 6,   //initX, initY
					5, 5);  //xPad, yPad
			dialog.pack();

            // Grab the size after the first time for use in subsequent attempts.
            if( !expandedPanelSize ) {
                // On random occasion, the new height gets messed up even on this first time! So only take the new width just to be safe.
                expandedPanelSize = new Dimension( (int) bigPanel.size.width, (int) basePanelSize.height )

                // If the height did get wonky that first time, reset it. Should not happen often.
                if( bigPanel.size.height != expandedPanelSize.height ) {
                    bigPanel.setPreferredSize( expandedPanelSize )
                    dialog.pack();
                }
            }
            bigPanel.repaint()
		}
	}

	/**
	 * Runs the expression test against shadoww.
	 * @see DeploymentLab.Integration.ExpressionTester
	 */
	private void testAction() {
		if (activeSwitch.value == 0) {
			log.warn("Cannot test an inactive expression", "message")
			return
		}
		if (!currentResult.getPassed()) {
			log.warn("Expression does not pass syntax check.", "message")
			return
		}

		def controlHost = ProjectSettings.getInstance().acHost.trim()
		if (controlHost.trim() == "") {
			log.warn(CentralCatalogue.getUIS("expressionEditor.noHostName"), "message")
			return
		}
		try {
			def uri = new URI(controlHost);
		} catch (URISyntaxException use) {
			log.warn("Invalid Control Process Test URI, cannot test expression.", "message")
			return
		}
		def testValue = taTestVal.text
		if (!readMode) {
			if (testValue.toString().trim() == "") {
				log.warn("Cannot test a Write Expression without a Test Value.", "message")
				return
			}
		}
        // check device id
        int devidInt = Integer.parseInt(devid)

        if(devidInt<1 || devidInt>247){
            log.warn(CentralCatalogue.formatUIS("validator.controlcomponents.devid", 1, 247 ),"message")
            return
        }

		// ugh... strip the prepended parcel modifier from property names.
		def pointProp
		if( parentProperty.getParcel()?.isEmpty() ) {
			pointProp = parentProperty.getName()
		} else {
			pointProp = parentProperty.getName().substring( parentProperty.getName().indexOf('_') + 1 )
		}

		def pointName = "${deviceType.shortName().toLowerCase()}.${pointProp.toLowerCase()}"
		String testResult
		Map<String, String> resultTuple
		try {
			//resultTuple = ExpressionTester.test(controlHost, modbusHost, devid, testValue, pointName, expression, CentralCatalogue.getApp("expressionTester.username"), CentralCatalogue.getApp("expressionTester.password"))

			def params = [:]
			params["ip"] = modbusHost
			params["expr"] = expression
			params["device"] = devid
			params["value"] = testValue
			params["pointname"] = pointName
			resultTuple = ExpressionTester.test(controlHost, "modbus_eval.yaws", params, CentralCatalogue.getApp("expressionTester.username"), CentralCatalogue.getApp("expressionTester.password"))

			//assumes that "both" is correctly formatted for display by the tester
			testResult = resultTuple["both"]
			currentTestResult = resultTuple["result"].equalsIgnoreCase("ok")
		} catch (java.net.MalformedURLException mue) {
			testResult = "Error with Control Hostname: ${System.getProperty("line.separator")}${mue.getMessage()}"
			log.error(testResult, "message")
			return
		} catch (IOException ioe) {
			//testResult = "Error: ${System.getProperty("line.separator")}${ioe.getMessage()}"
			testResult = ioe.getMessage()
			log.error(testResult, "message")
			return
		} catch (java.lang.IllegalArgumentException iae){
			testResult = iae.getMessage()
			log.error(testResult, "message")
			return
		}
		refreshValidStateLabel()
		//test result to message area, not to a messagebox
		lTestResultDisplay.setText(testResult)
		//log.info(testResult, "message")
	}

}
