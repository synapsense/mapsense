package DeploymentLab.PropertyEditor

import java.awt.BorderLayout
import java.awt.Component
import java.awt.FlowLayout
import java.awt.Dialog.ModalityType
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.awt.event.KeyEvent

import javax.swing.AbstractCellEditor
import javax.swing.BorderFactory
import javax.swing.ButtonGroup

import javax.swing.JButton
import javax.swing.JComboBox
import javax.swing.JComponent
import javax.swing.JDialog
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JOptionPane
import javax.swing.JPanel
import javax.swing.JRadioButton
import javax.swing.JSlider
import javax.swing.JTable
import javax.swing.JTextArea
import javax.swing.JTextField
import javax.swing.KeyStroke
import javax.swing.SpringLayout
import javax.swing.event.ChangeListener
import javax.swing.event.ChangeEvent
import javax.swing.event.DocumentListener
import javax.swing.event.DocumentEvent
import javax.swing.event.UndoableEditListener
import javax.swing.event.UndoableEditEvent
import javax.swing.table.TableCellEditor

import groovy.swing.SwingBuilder

import DeploymentLab.CentralCatalogue
import DeploymentLab.FileUtil
import DeploymentLab.ProjectSettings
import DeploymentLab.SpringUtilities
import DeploymentLab.channellogger.Logger

import DeploymentLab.Integration.Result
import DeploymentLab.Integration.Evaluator
import DeploymentLab.Integration.ExpressionTester
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentProperty
import DeploymentLab.Model.DLComponent
import javax.swing.JEditorPane
import java.awt.Font
import java.awt.Dimension

import javax.swing.JToggleButton

import DeploymentLab.UIDisplay
import net.miginfocom.swing.MigLayout
import DeploymentLab.NamePair
import DeploymentLab.UndoBuffer
import javax.swing.SwingUtilities
import javax.swing.text.DefaultCaret

/**
 *
 * @author Gabriel Helman
 * @since
 * Date: 9/19/11
 * Time: 2:46 PM
 */
class ConfigBacnetEditor extends AbstractCellEditor implements TableCellEditor, ActionListener, DocumentListener, ChangeListener, UndoableEditListener  {
	private static final Logger log = Logger.getLogger(ConfigBacnetEditor.class.getName())
	private static final String EDIT = "edit";
	private static final String SIGIL = "OWLBEAR"
	private static final Integer MAXMESSAGE = 64;

	private String DIALOGTITLE
	private boolean readMode
	private DeviceType deviceType
	private DLComponent parentComponent;
	private ComponentProperty parentProperty
	private ComponentModel cmodel
	private SwingBuilder builder

	private JButton button;
	private JDialog dialog;
	/** The parent frame of this dialog box.  */
	private JFrame parent

	JPanel dlgPanel, buttonPanel, simplePanel, advPanel, internalSimplePanel, internalAdvPanel, buttonAdvPanel, testPanel, outerTestPanel
	JTextArea scriptText
	JRadioButton simpleButton, advButton
	ButtonGroup modeGroup
	JTextField taRegister, taActionMod, taTestVal, taWritePriority
	JComboBox cbDataType, cbPropertyId, cbAction, cbActionBase
	JLabel lResult, lTestVal
	JSlider activeSwitch
	JPanel bigPanel
	JPanel helpPanel
	JEditorPane examplesBox
	JToggleButton exampleButton

    Dimension basePanelSize
    Dimension expandedPanelSize

	private String expression
	/** From the syntax checker  */
	private Result currentResult
	private String bacnetIP
	private String devid
	private String bacnetNet
	private Integer valid = 0
	/** from the webservice expression tester  */
	private Boolean currentTestResult = false
	private Boolean optional = true
	private UIDisplay uiDisplay;

	private JLabel lCurrentValidState
	private String originalExpression = ""
	private JTextArea lTestResultDisplay
	private UndoBuffer ub

	private static NamePair[] dataTypeChoicesRead = [
				new NamePair("ai", "Analog Input"), new NamePair("ao", "Analog Output"), new NamePair("av", "Analog Value"),
				new NamePair("bi", "Binary Input"), new NamePair("bo", "Binary Output"), new NamePair("bv", "Binary Value"),
				new NamePair("mi", "Multi-State Input"), new NamePair("mo", "Multi-State Output"), new NamePair("mv", "Multi-State Value")
		];

	private static NamePair[] dataTypeChoicesWrite = [
				new NamePair("ao", "Analog Output"), new NamePair("av", "Analog Value"),
				new NamePair("bo", "Binary Output"), new NamePair("bv", "Binary Value"),
				new NamePair("mo", "Multi-State Output"), new NamePair("mv", "Multi-State Value")
		];

	private dataTypeChoices

    private static NamePair[] propertyIdChoices = [
            new NamePair("6", "alarm-value (6)"),
            new NamePair("22", "cov-increment (22)"),
            new NamePair("25", "deadband (25)"),
            new NamePair("36", "event-state (36)"),
            new NamePair("40", "feedback-value (40)"),
            new NamePair("45", "high-limit (45)"),
            new NamePair("59", "low-limit (59)"),
            new NamePair("65", "maximum-present-value (65)"),
            new NamePair("66", "minimum-off-time (66)"),
            new NamePair("67", "minimum-on-time (67)"),
            new NamePair("68", "minimum-output (68)"),
            new NamePair("69", "minimum-present-value (69)"),
            new NamePair("75", "object-identifier (75)"),
            new NamePair("77", "object-name (77)"),
            new NamePair("79", "object-type (79)"),
            new NamePair("81", "out-of-service (81)"),
            new NamePair("85", "present-value (85)"),
            new NamePair("103", "reliability (103)"),
            new NamePair("104", "relinquish-default (104)"),
            new NamePair("106", "resolution (106)"),
            new NamePair("111", "status-flags (111)"),
            new NamePair("118", "update-interval (118)"),
        ];

	private static String[] actionChoices = ["None", "Bitmask", "Scale", "Offset", "Equal To"];
	private static String[] baseChoices = ["2", "10", "16"];

	/**
	 * Constructor: produces a new Editor connected to a given component.
	 * @param newComponent The component that this property belongs to.  Sets a reference to allow us to edit >1 component property at a time.
	 */
	public ConfigBacnetEditor(DLComponent newComponent,
							  ComponentProperty prop,
							  JFrame parent,
							  boolean _READ,
							  UIDisplay uid,
							  UndoBuffer undo
	) {

		//load in the setup info
		readMode = _READ
		uiDisplay = uid
		ub = undo

		parentComponent = newComponent;
		parentProperty = prop
		cmodel = parentComponent.getModel()

		if ( parentComponent.getType().toLowerCase().contains("vfd") ||
		     (prop.getParcel() && prop.getParcel().toLowerCase().contains("vfd")) ) {
			deviceType = DeviceType.VFD
		} else if (parentComponent.getType().toLowerCase().contains("crac")) {
			deviceType = DeviceType.CRAC
        } else if (parentComponent.getType().toLowerCase().contains("rtu_manager")) {
            deviceType = DeviceType.RTUMGR
		} else if (parentComponent.getType().toLowerCase().contains("rtu_dx")) {
            deviceType = DeviceType.RTUDX
		} else if (parentComponent.getType().toLowerCase().contains("rtu_chw")) {
            deviceType = DeviceType.RTUCHW
		} else if (parentComponent.getType().toLowerCase().contains("room")) {
            deviceType = DeviceType.HEARTBEATMGR
		} else {
			deviceType = DeviceType.CRAH
		}

		DIALOGTITLE = "$deviceType: ${parentProperty.getDisplayName()}"
		DIALOGTITLE += (readMode ? " (READ)" : " (WRITE)")

		if (readMode){
			dataTypeChoices = dataTypeChoicesRead
		} else {
			dataTypeChoices = dataTypeChoicesWrite
		}

		bacnetIP = parentComponent.getPropertyValue( "ip", parentProperty.getParcel() )
		devid = parentComponent.getPropertyValue( "device", parentProperty.getParcel() )
		bacnetNet = parentComponent.getPropertyValue( "network", parentProperty.getParcel() )

		builder = new SwingBuilder();

		button = new JButton();
		button.setActionCommand(EDIT);
		button.addActionListener(this);
		button.setBorderPainted(false);

		//def regVF = new RegisterNumberVerifier()
		//def modVF = new ActionModifierVerifier()

		modeGroup = builder.buttonGroup()

		//String[] dataTypeChoices = ["Int16", "UInt16", "Int32", "UInt32"];
		//String[] dataTypeChoices = ["Analog Input", "Analog Output", "Analog Value", "Binary", "Multi-State"];



		def testAction = builder.action(name: 'Test', mnemonic: KeyEvent.VK_T, closure: { testAction() })
		def okAction = builder.action(name: 'OK', mnemonic: KeyEvent.VK_O, closure: { saveAndExitAction() })
		def cancelButtonAction = builder.action(name: 'Cancel', mnemonic: KeyEvent.VK_C, closure: { cancelAction() })
		def exampleAction = builder.action(name: 'Examples', mnemonic: KeyEvent.VK_E, closure: { showExamples() })

		Dictionary<Integer, JLabel> labels = new Hashtable<Integer, JLabel>()
		labels.put(0, builder.label("Disabled"))
		labels.put(1, builder.label("Active"))

		//activeSwitch = builder.slider( minimum: 0, maximum: 1, orientation: JSlider.HORIZONTAL, labelTable: labels, paintLabels: true, paintTicks: true, snapToTicks: true )
		activeSwitch = new JSlider(JSlider.HORIZONTAL, 0, 1, 1)
		activeSwitch.setLabelTable(labels)
		activeSwitch.setPaintTicks(true)
		activeSwitch.setPaintLabels(true)
		activeSwitch.setSnapToTicks(true)
		activeSwitch.addChangeListener(this)

		examplesBox = new JEditorPane("text/html", "")
		examplesBox.setEditable(false)
		examplesBox.setFont(new Font("SansSerif", Font.PLAIN, 12)) //to be LaF-change immune, always use logical font names
		examplesBox.setPreferredSize(new Dimension(495, 500))
		examplesBox.setText(FileUtil.inputStreamToString(this.class.getClassLoader().getResourceAsStream(CentralCatalogue.getApp("ConfigModbus.examplesFile"))))
		examplesBox.setCaretPosition(0)

		dialog = builder.dialog(owner: parent, title: DIALOGTITLE, modalityType: ModalityType.APPLICATION_MODAL, layout: new BorderLayout(), resizable: false) {

			bigPanel = panel(layout: new SpringLayout()) {

				dlgPanel = panel(layout: new SpringLayout()) {
					panel(layout: new FlowLayout(FlowLayout.LEADING)) {
						widget(activeSwitch)
					}
					simplePanel = panel(layout: new SpringLayout()) {
						simpleButton = radioButton(text: "Simple", selected: true, actionPerformed: {setMode(true)})
						internalSimplePanel = panel(layout: new SpringLayout(), border: BorderFactory.createEtchedBorder()) {
							label "Object Instance"
							taRegister = textField(columns: 10)
							label "Object Type"
							cbDataType = comboBox(new JComboBox(dataTypeChoices), actionPerformed: {constructExpression()})
							label "Property Id"
                            cbPropertyId = comboBox(new JComboBox(propertyIdChoices), editable:true, actionPerformed: {propertyIdChanged()})
							label "Action"
							cbAction = comboBox(new JComboBox(actionChoices), actionPerformed: {constructExpression()})
							label "Action Modifier"
							taActionMod = textField(columns: 10)
							label "Action Modifier Base"
							cbActionBase = comboBox(new JComboBox(baseChoices), actionPerformed: {constructExpression()})
							//if (!readMode) {
							//	label "Write Priority"
							//	taWritePriority = textField(columns: 10)
							//}
						}
					}
					advPanel = panel(layout: new SpringLayout()) {
						advButton = radioButton(text: "Advanced", selected: false, actionPerformed: {setMode(false)})
						internalAdvPanel = panel(layout: new SpringLayout(), border: BorderFactory.createEtchedBorder()) { //, preferredSize: [378,106]  ){
							panel(layout: new BorderLayout()) { //, preferredSize: [363, 80]) {
								scrollPane(constraints: BorderLayout.CENTER) {
									scriptText = textArea(columns: 40, lineWrap: true, rows: 3, wrapStyleWord: true)
								}
							}

							buttonAdvPanel = panel(layout: new SpringLayout()) {
								panel(layout: new FlowLayout(FlowLayout.LEADING)) {
									lResult = label(text: "Syntax: OK", preferredSize: [300, 32])
								}
								panel(layout: new FlowLayout(FlowLayout.TRAILING)) {
									exampleButton = toggleButton(action: exampleAction, selected: false)
								}
							} //end buttonAdvPanel
						}
					}

					outerTestPanel = panel(layout: new SpringLayout()) {
						testPanel = panel(layout: new MigLayout("fillx"), border: BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Test Write Value")) {
							taTestVal = textField(columns: 10, constraints: "growx")
						}
					}

					buttonPanel = panel(layout: new MigLayout("fillx")) {
						panel(layout: new MigLayout("fillx"), border: BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Test Result"), constraints: "growx, span 2, wrap") {
                            scrollPane(constraints: "growx") {
                                lTestResultDisplay = textArea(text:"", rows:3, editable:false, lineWrap: true, wrapStyleWord: true, constraints: "growx")
                            }
                        }

						panel(layout: new FlowLayout(FlowLayout.LEADING), constraints: "") {
							uiDisplay.addUIChangeListener(button(text: 'Test', mnemonic: KeyEvent.VK_T, defaultButton: false, action: testAction), 'testExpression')
							lCurrentValidState = label(text: "Valid: ${getCurrentValidState()}")
						}
						panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: "gapleft unrel, alignx trailing, wrap") {
							button(text: 'OK', mnemonic: KeyEvent.VK_O, defaultButton: true, action: okAction)
							button(text: 'Cancel', mnemonic: KeyEvent.VK_C, defaultButton: false, action: cancelButtonAction)
						}
					} //end buttonPanel


				} //end dlgPanel (the left side)


				/*
								 helpPanel = panel(layout: new BorderLayout()) {
									 scrollPane( constraints: BorderLayout.CENTER ){
										 widget(examplesBox)
									 }
								 }
								 */

			} //end bigPanel


			dlgPanel.registerKeyboardAction(okAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			dlgPanel.registerKeyboardAction(cancelButtonAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			dlgPanel.registerKeyboardAction(exampleAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		}

		//define the helpPanel out here and then add it to the layout when the user asks
		helpPanel = builder.panel(layout: new BorderLayout()) {
			scrollPane(constraints: BorderLayout.CENTER) {
				widget(examplesBox)
			}
		}
		//dialog.setPreferredSize(new Dimension(473, 828))

		modeGroup.add(simpleButton)
		modeGroup.add(advButton)

		taRegister.getDocument().addDocumentListener(this)
		cbPropertyId.getEditor().getEditorComponent().getDocument().addDocumentListener(this)
		taActionMod.getDocument().addDocumentListener(this)
		scriptText.getDocument().addUndoableEditListener(this)

        // Don't autoscroll to the bottom of the Result text area if we set lots of text that needs scrollbars.
        ((DefaultCaret)lTestResultDisplay.getCaret()).setUpdatePolicy(DefaultCaret.NEVER_UPDATE)

		int dlgRows
		if (!readMode) {
			testPanel.setVisible(true)
			taTestVal.setVisible(true)
			dlgRows = 5
		} else {
			testPanel.setVisible(false)
			taTestVal.setVisible(false)
			dlgRows = 5
		}

		SpringUtilities.makeCompactGrid(outerTestPanel,
				1, 1,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad
/*
		SpringUtilities.makeCompactGrid(buttonPanel,
				2, 2,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad
  */
		int internalSimplePanelRows
		if(readMode){
			internalSimplePanelRows = 6
		} else {
			internalSimplePanelRows = 6
		}

		SpringUtilities.makeCompactGrid(internalSimplePanel,
                                internalSimplePanelRows, 2,   //rows, cols
                                6, 6,   //initX, initY
                                5, 5);  //xPad, yPad

		SpringUtilities.makeCompactGrid(simplePanel,
				2, 1,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad

		SpringUtilities.makeCompactGrid(buttonAdvPanel,
				1, 2,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad

		SpringUtilities.makeCompactGrid(internalAdvPanel,
				2, 1,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad

		SpringUtilities.makeCompactGrid(advPanel,
				2, 1,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad

		SpringUtilities.makeCompactGrid(dlgPanel,
				dlgRows, 1,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad

		SpringUtilities.makeCompactGrid(bigPanel,
				///1, 2,   //rows, cols
				1, 1,   //rows, cols
				6, 6,   //initX, initY
				5, 5);  //xPad, yPad

		dialog.pack();

        // Save the base size that the layout manager came up with so we can reset it later when the layout manager screws it up.
        basePanelSize = bigPanel.getSize()
	}

	/**
	 * Exits the dialog due to the user clicking the OK button.  Flushes the GUI widgets to the ChartContents object.
	 */
	def saveAndExitAction() {
		if (!currentResult.getPassed()) {
			if (JOptionPane.YES_OPTION != JOptionPane.showConfirmDialog(dialog, "Syntax does not pass.  Really save?", "Syntax Check Fails", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE)) {
				return
			}
		}

		ub.startOperation()
		parentComponent.setPropertyValue("${parentProperty.getName()}String", getConfigString())

		valid = -1
		//special case: crah valve control
		if (deviceType == DeviceType.CRAH && (parentProperty.getName().contains("rat") || parentProperty.getName().contains("sat"))) {
			//this property is actually required, even though it has the optional bit set
			if (activeSwitch.getValue() == 0) {
				valid = 2
			}
		}

		if (valid < 0) {
			if (activeSwitch.getValue() == 0) {
				valid = 1
			} else if (activeSwitch.getValue() == 1 && currentResult.getPassed() && currentTestResult) {
				valid = 1
			} else {
				valid = 0
			}
		}
		parentComponent.setPropertyValue("${parentProperty.getName()}Valid", valid)

		expression = scriptText.getText()
		parentComponent.setPropertyValue(parentProperty.getName(), getCellEditorValue())
		ub.finishOperation()
		dialog.hide()
	}


	boolean getCurrentValidState() {
		boolean result = false
		boolean currentDialog = (activeSwitch.getValue() == 1 && currentResult?.getPassed() && currentTestResult)
		boolean fromComponent = parentComponent.getPropertyValue("${parentProperty.getName()}Valid").toString().toBoolean()
		boolean changedFromModel = false
		if (scriptText.getText().trim().equals(originalExpression) && currentTestResult == false) {
			changedFromModel = false
		} else {
			changedFromModel = true
		}
		//println "getCurrentValidState() changedFromModel: $changedFromModel fromComponent $fromComponent currentDialog: $currentDialog "
		//println "scriptText = '${scriptText.getText().trim()}' og = '$originalExpression'"
		if (changedFromModel) {
			result = currentDialog
		} else {
			result = fromComponent
		}
		return result
	}

	void refreshValidStateLabel() {
		//lCurrentValidState.setText("Valid: ${getCurrentValidState()}")
		if (getCurrentValidState()) {
			lCurrentValidState.setText(CentralCatalogue.getUIS("expressionEditor.expressionValid"))
		} else {
			lCurrentValidState.setText(CentralCatalogue.getUIS("expressionEditor.expressionInvalid"))
			lTestResultDisplay.text = ""
		}
	}

	/**
	 * Exits the dialog due to the user clicking the CANCEL button. Does not save any changes.
	 */
	def cancelAction() {
		//reload the old expression
		expression = parentComponent.getPropertyValue(parentProperty.getName()).toString()
		dialog.hide()
	}

	/**
	 * Runs if the user has clicked the button returned from getTableCellEditorComponent.
	 */
	public void actionPerformed(ActionEvent e) {

		if (EDIT.equals(e.getActionCommand())) {
			//button has been clicked as an edit command, show the dialog
			dialog.setLocationRelativeTo(parent)
			dialog.show();
			fireEditingStopped();
		} //else {
			//println "Action command! ${e.getActionCommand()}"
		//}

	}

	/**
	 * Returns the value to be saved back into the data model.
	 */
	public Object getCellEditorValue() {
		if (activeSwitch.getValue() == 1) {
			return expression
		}
		return ""
	}

	/**
	 * Loads the data from the "data store" into the dialog and returns the editor component (in this case, a button to DISPLAY the component, but still.)
	 */
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int col) {

		//pull the data out of the component property and populate the table
		//scriptText.setText( parentComponent.getPropertyValue("modbusConfigExpression").toString() )

		//store this in case we want to compare things to it
		originalExpression = parentComponent.getPropertyValue(parentProperty.getName()).toString()

		//default:  0;40001;Int16;None;10;10;true
		def configString = parentComponent.getPropertyValue("${parentProperty.getName()}String").toString()
		//def config = configString.split(";")

		def config = parseConfigString(configString)

		if (Integer.parseInt(config["active"]) == 2) {
			optional = false
			activeSwitch.setValue(1)
			activeSwitch.setVisible(false)
		} else {
			activeSwitch.setValue(Integer.parseInt(config["active"]))
		}

		taRegister.setText(config["register"])

        // It might be a free-form value not in our known list. So set the raw text & override with the object if found.
        def propObj = config["propertyId"]
        propertyIdChoices.each{ pic ->
            if ( pic.getInternal().equals( config["propertyId"] ) ) {
                propObj = pic
            }
        }
        cbPropertyId.setSelectedItem( propObj )

		dataTypeChoices.each{ dtc ->
			if ( dtc.getDisplay().equals(config["datatype"])){
				cbDataType.setSelectedItem(dtc)
			}
		}

		cbAction.setSelectedItem(config["action"])
		taActionMod.setText(config["mod"])
		cbActionBase.setSelectedItem(config["base"])
		if (Boolean.parseBoolean(config["isSimple"])) {
			simpleButton.selected = true
			constructExpression()
			setMode(true)
			expression = scriptText.getText()
		} else {
			advButton.selected = true
			setMode(false)
			expression = parentComponent.getPropertyValue(parentProperty.getName()).toString()
			scriptText.setText(expression)
		}

		//do this last, such that it'll override the simple/advanced radio button
		if (activeSwitch.getValue() == 0) {
			enableEverything(false)
		} else {
			enableEverything(true)
		}
		return button;
	}

	/**
	 * Constructs an expression based on the simple mode choices and installs it in the text area.
	 */
	protected void constructExpression() {

		String funcName = ""
		String func
		if (readMode) {
			//ex:
			//-spec read_ai(LocalIf :: integer(),  Ip :: string(), Net :: integer(), Dev :: integer(), Obj :: integer(), Id :: integer()) -> Value :: float().
			funcName = "read_${cbDataType.getSelectedItem().getInternal().toString().toLowerCase()}"
			func = "$funcName( ${taRegister.getText()}, ${getComboBoxValue(cbPropertyId)} ) $SIGIL "
		} else {
			//funcName = "write_${cbDataType.getSelectedItem().getInternal().toString().toLowerCase()}(${taRegister.getText()}, Value $SIGIL)"
			funcName = "write_${cbDataType.getSelectedItem().getInternal().toString().toLowerCase()}"
			func = "$funcName( ${taRegister.getText()}, ${getComboBoxValue(cbPropertyId)}, ( Value $SIGIL ) )"
		}

		String expr = ""
		switch (cbAction.getSelectedItem().toString().toLowerCase()) {
			case "none":
				expr = func.replace(SIGIL, "")
				break
			case "bitmask":
				if (readMode) {
					expr = func.replace(SIGIL, " bor " + buildActionMod())
				} else {

					expr = func.replace(SIGIL, "")
				}
				break

			case "scale":
				if (readMode) {
					//expr = func + " / " + buildActionMod()
					expr = func.replace(SIGIL, " / " + buildActionMod())
				} else {
					//expr = func + " * " + buildActionMod()
					expr = func.replace(SIGIL, " * " + buildActionMod())
				}
				break

			case "offset":
				if (readMode) {
					//expr = func + " + " + buildActionMod()
					expr = func.replace(SIGIL, " + " + buildActionMod())
				} else {
					//expr = func + " - " + buildActionMod()
					expr = func.replace(SIGIL, " - " + buildActionMod())
				}
				break

			case "equal to":
				if (readMode) {
					//expr = func + " == " + buildActionMod()
					expr = func.replace(SIGIL, " == " + buildActionMod())
				} else {
					expr = func.replace(SIGIL, "")
				}
				break
		}
		scriptText.setText(expr)
		checkExpression(expr)
	}

    /**
     * Listern to handle completed change events to the Property ID Combo Box. It checks for manually entered text in
     * the Property ID Combo Box and selects the known matching item instead. For entered numbers, it must match the
     * property id value exactly. When non-numeric text is entered, it will select the item if the entered text matches
     * the beginning of an items display string. This allows for partial entries to match.
     *
     * Ideally, this would auto-select as the user is typing if there is a match. However, that takes a lot more effort
     * to implement (custom Document implementations and such) than it is probably worth just for this item.
     */
    protected void propertyIdChanged() {

        // Only do this if it is a manually typed entry, not when an item was selected from the list.
        if( cbPropertyId.getSelectedIndex() == -1 ) {
            String str = cbPropertyId.getSelectedItem().toString()

            if( !str.isEmpty() ) {
                // Check the display and internal value of each available item in the list for a match.
                for( int idx = 0; idx < cbPropertyId.getItemCount(); idx++ ) {
                    NamePair item = (NamePair) cbPropertyId.getItemAt(idx)

                    if( item.getInternal().equals( str ) || item.getDisplay().startsWith( str ) ) {
                        // Need to select it outside of this thread action.
                        SwingUtilities.invokeLater( new Runnable() {
                            int localIdx = idx
                            void run() {
                                cbPropertyId.setSelectedIndex( localIdx )
                            }
                        })

                        // Setting the item will trigger another call. constructExpression will be called then.
                        return
                    }
                }
            }
        }

        // The normal event processing that existed before we injected our special logic.
        constructExpression()
    }

	protected String buildActionMod() {
		String result = ""
		if (cbActionBase.getSelectedItem().toString().equals("10")) {
			result = taActionMod.getText()
		} else {
			result = cbActionBase.getSelectedItem().toString() + "#" + taActionMod.getText()
		}
		return result
	}

	protected void checkExpression(String expressionToCheck) {
		currentResult = Evaluator.evaluate(expressionToCheck, MAXMESSAGE)
		lResult.setText(currentResult.getMessage())
		if (currentResult.getPassed()) {
			expression = scriptText.getText()
		}

		refreshValidStateLabel()
	}

	void changedUpdate(DocumentEvent e) {
		constructExpression()
	}

	void insertUpdate(DocumentEvent e) {
		constructExpression()
	}

	void removeUpdate(DocumentEvent e) {
		constructExpression()
	}

	/**
	 * Callback for when the contents of the scriptbox changes for any reason
	 * @param e
	 */
	void undoableEditHappened(UndoableEditEvent e) {
		//a text change means we need to re-test
		currentTestResult = false
		checkExpression(scriptText.getText())
	}

	/**
	 * Fired when the user flips the active/disabled slider.
	 * @param e the ChangeEvent from the JSlider
	 */
	void stateChanged(ChangeEvent e) {
		JSlider source = (JSlider) e.getSource();
		if (!source.getValueIsAdjusting()) {
			int mode = (int) source.getValue();
			if (mode == 0) {
				enableEverything(false)
			} else {
				enableEverything(true)
			}
			refreshValidStateLabel()
		}
	}

	private void enableEverything(boolean enable) {
		if (!enable) {
			taRegister.setEnabled(false)
			taActionMod.setEnabled(false)
			cbPropertyId.setEnabled(false)
			cbDataType.setEnabled(false)
			cbAction.setEnabled(false)
			cbActionBase.setEnabled(false)
			scriptText.setEnabled(false)
			simpleButton.setEnabled(false)
			advButton.setEnabled(false)
		} else {
			simpleButton.setEnabled(true)
			advButton.setEnabled(true)
			if (simpleButton.selected) {
				setMode(true)
			} else {
				setMode(false)
			}
		}
	}

	private void setMode(boolean simple) {
		if (simple) {
			//enable top bits, disable bottom
			taRegister.setEnabled(true)
			cbPropertyId.setEnabled(true)
			taActionMod.setEnabled(true)
			cbDataType.setEnabled(true)
			cbAction.setEnabled(true)
			cbActionBase.setEnabled(true)
			scriptText.setEnabled(false)
			constructExpression()
		} else {
			//vice-versa
			taRegister.setEnabled(false)
			cbPropertyId.setEnabled(false)
			taActionMod.setEnabled(false)
			cbDataType.setEnabled(false)
			cbAction.setEnabled(false)
			cbActionBase.setEnabled(false)
			scriptText.setEnabled(true)
		}
	}

	/**
	 * Builds the configuration string representing the state of this manually-configured modus property.
	 *
	 * The config string is a semi-colon delimited string with these fields:
	 *
	 * active;register;datatype;action;mod;base;isSimple
	 *
	 * active: 0, 1, 2
	 * 0 = inactive
	 * 1 = active
	 * 2 = required, cannot be deactivated
	 *
	 * register: the value of the register field of the edit box
	 *
	 * datatype: one of dataTypeChoices: "Int16", "UInt16", "Int32", "UInt32"
	 *
	 * action: one of actionChoices: "None", "Bitmask", "Scale", "Offset", "Equal To"
	 *
	 * mod: the text value of the actionMod field
	 *
	 * base: one of baseChoices: "2", "10", "16"
	 *
	 * isSimple: "true", "false" if the dialog is set to Simple mode
	 *
	 * @return the Configuration String
	 */
	public String getConfigString() {
		String result = ""

		if (optional) {
			result += activeSwitch.value.toString() + ";"
		} else {
			result += "2;"
		}
		result += taRegister.getText() + ";"
		result += cbDataType.getSelectedItem().toString() + ";"
		result += cbAction.getSelectedItem().toString() + ";"
		result += taActionMod.getText() + ";"
		result += cbActionBase.getSelectedItem().toString() + ";"
		result += simpleButton.selected ? "true" : "false"
		result += ";"
		result += getComboBoxValue(cbPropertyId) + ";"
		return result
	}

	/**
	 * Helper function to split up the config string.
	 *
	 * Config String is: active;register;datatype;action;mod;base;isSimple
	 * @param configString
	 * @return
	 * @see #getConfigString()
	 */
	public static Map<String, String> parseConfigString(String configString) {
		def config = configString.split(";")
		def result = [:]
		result["active"] = config[0]
		result["register"] = config[1]
		result["datatype"] = config[2]
		result["action"] = config[3]
		result["mod"] = config[4]
		result["base"] = config[5]
		result["isSimple"] = config[6]
		try{
			result["propertyId"] = config[7]
		} catch (ArrayIndexOutOfBoundsException e){
			result["propertyId"] = "0"
		}

		return result
	}

	/**
	 * Toggles the examples sidecar on and off.
	 */
	private void showExamples() {
		if (!exampleButton.selected) {
			//hide
			bigPanel.remove(helpPanel)
            bigPanel.setPreferredSize( basePanelSize )
			SpringUtilities.makeCompactGrid(bigPanel,
					1, 1,   //rows, cols
					6, 6,   //initX, initY
					5, 5);  //xPad, yPad
			dialog.pack()
			bigPanel.repaint()
		} else {
			//show
            // The layout manager usually does a good job the first time, so let it. For some reason, it sucks all times after that, so coax it a bit.
            if( expandedPanelSize ) {
                bigPanel.setPreferredSize( expandedPanelSize )
            }
			bigPanel.add(helpPanel)
			SpringUtilities.makeCompactGrid(bigPanel,
					1, 2,   //rows, cols
					6, 6,   //initX, initY
					5, 5);  //xPad, yPad
			dialog.pack();

            // Grab the size after the first time for use in subsequent attempts.
            if( !expandedPanelSize ) {
                // On random occasion, the new height gets messed up even on this first time! So only take the new width just to be safe.
                expandedPanelSize = new Dimension( (int) bigPanel.size.width, (int) basePanelSize.height )

                // If the height did get wonky that first time, reset it. Should not happen often.
                if( bigPanel.size.height != expandedPanelSize.height ) {
                    bigPanel.setPreferredSize( expandedPanelSize )
                    dialog.pack();
                }
            }
			bigPanel.repaint()
		}
	}

	/**
	 * Runs the expression test against shadoww.
	 * @see DeploymentLab.Integration.ExpressionTester
	 */
	private void testAction() {
		if (activeSwitch.value == 0) {
			log.warn("Cannot test an inactive expression", "message")
			return
		}
		if (!currentResult.getPassed()) {
			log.warn("Expression does not pass syntax check.", "message")
			return
		}

		def controlHost = ProjectSettings.getInstance().acHost.trim()
		if (controlHost.trim() == "") {
			log.warn(CentralCatalogue.getUIS("expressionEditor.noHostName"), "message")
			return
		}
		try {
			def uri = new URI(controlHost);
		} catch (URISyntaxException use) {
			log.warn("Invalid Control Process Test URI, cannot test expression.", "message")
			return
		}
		def testValue = taTestVal.text
		if (!readMode) {
			if (testValue.toString().trim() == "") {
				log.warn("Cannot test a Write Expression without a Test Value.", "message")
				return
			}
		}

        // check device id
        int devidInt = Integer.parseInt(devid)

        if(devidInt<0 || devidInt>4194303){
            log.warn(CentralCatalogue.formatUIS("validator.controlcomponents.devid", 0, 4194303 ),"message")
            return
        }

		// ugh... strip the prepended parcel modifier from property names.
		def pointProp
		if( parentProperty.getParcel()?.isEmpty() ) {
			pointProp = parentProperty.getName()
		} else {
			pointProp = parentProperty.getName().substring( parentProperty.getName().indexOf('_') + 1 )
		}

		def pointName = "${deviceType.toString().toLowerCase()}.${pointProp.toLowerCase()}"
		String testResult
		Map<String, String> resultTuple
		try {
			//resultTuple = ExpressionTester.test(controlHost, bacnetIP, devid, testValue, pointName, expression, CentralCatalogue.getApp("expressionTester.username"), CentralCatalogue.getApp("expressionTester.password"))

			def params = [:]
			params["ip"] = bacnetIP
			params["net"] = bacnetNet
			params["dev"] = devid
			params["expr"] = expression
			params["pointname"] = pointName
			params["value"] = testValue
			resultTuple = ExpressionTester.test(controlHost, "bacnet_eval.yaws", params, CentralCatalogue.getApp("expressionTester.username"), CentralCatalogue.getApp("expressionTester.password"))

			//assumes that "both" is correctly fomatted for display by the tester
			testResult = resultTuple["both"]
			currentTestResult = resultTuple["result"].equalsIgnoreCase("ok")
		} catch (java.net.MalformedURLException mue) {
			testResult = "Error with Control Hostname: ${System.getProperty("line.separator")}${mue.getMessage()}"
			log.error(testResult, "message")
			return
		} catch (IOException ioe) {
			//testResult = "Error: ${System.getProperty("line.separator")}${ioe.getMessage()}"
			testResult = ioe.getMessage()
			log.error(testResult, "message")
			return
		} catch (java.lang.IllegalArgumentException iae){
			testResult = iae.getMessage()
			log.error(testResult, "message")
			return
		}
		refreshValidStateLabel()
		//test result to message area, not to a messagebox
		lTestResultDisplay.text = testResult
		//log.info(testResult, "message")
	}

    /**
     * Helper method for editable ComboBoxes to return the internal NamePair value when an item from the list is
     * selected and the free-form value when the user manually entered a value rather than using the pulldown list.
     *
     * @param cb  The ComboBox to get the value from.
     * @return    The current item value of the ComboBox.
     */
    private String getComboBoxValue( JComboBox cb ) {
        if( cb.getSelectedItem() instanceof NamePair ) {
            return cb.getSelectedItem().getInternal()
        }
        return cb.getEditor().getEditorComponent().getText()
    }
}
