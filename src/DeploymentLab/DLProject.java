package DeploymentLab;

import java.awt.*;
import java.io.Closeable;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.URI;
import java.nio.channels.FileLock;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import javax.imageio.ImageIO;
import javax.xml.transform.stream.StreamSource;

import DeploymentLab.Exceptions.ModelCatastrophe;
import DeploymentLab.Exceptions.ProjectFileCatastrophe;
import DeploymentLab.Export.ImageExporter;
import DeploymentLab.Image.ARGBConverter;
import DeploymentLab.Image.DLImage;
import DeploymentLab.Model.*;
import DeploymentLab.Tools.ConfigurationReplacer;
import DeploymentLab.Upgrade.UpgradeController;
import DeploymentLab.channellogger.Logger;
import com.synapsense.dto.BinaryData;
import groovy.util.XmlSlurper;
import groovy.util.slurpersupport.GPathResult;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.XsltCompiler;
import net.sf.saxon.s9api.XsltExecutable;
import net.sf.saxon.s9api.XsltTransformer;

/**
 * @author Ken Scoggins
 * @since Jupiter
 */
public class DLProject implements Closeable {
    private static final Logger log = Logger.getLogger(DLProject.class.getName());

    private ComponentModel componentModel;
    private ObjectModel objectModel;
    private UndoBuffer undoBuffer;
    private ProjectSettings projectSettings;
	private ConfigurationReplacer configurationReplacer;
    private AccessModel accessModel;

    private Path projectFile;
    private FileLock fileLock;
    private boolean needsASave;
    private String originalFileVersion = "";

    public DLProject(UndoBuffer undoBuffer, ComponentModel componentModel, ObjectModel objectModel,
                     ConfigurationReplacer configurationReplacer, boolean setAsOpenProject ) {
	    this.undoBuffer = undoBuffer;
	    this.componentModel = componentModel;
	    this.objectModel = objectModel;
	    this.projectSettings = ProjectSettings.getInstance();
	    this.configurationReplacer = configurationReplacer;
	    this.accessModel = new AccessModel(this);

        // Optional here because this isn't loading a file and is often used with pre-populated models.
        if( setAsOpenProject ) {
            CentralCatalogue.setOpenProject( this );
        }
    }

    public DLProject(UndoBuffer undoBuffer, ComponentModel componentModel, ObjectModel objectModel,
                     ConfigurationReplacer configurationReplacer, AccessModel accessModel, boolean setAsOpenProject) {
	    this(undoBuffer, componentModel, objectModel, configurationReplacer, setAsOpenProject);
        this.accessModel = accessModel;
    }

    public AccessModel getAccessModel(){
        return accessModel;
    }

	public void setFile(File file){
		this.projectFile = file.toPath();
		if( ! lockProjectFile() ) {
			throw new ProjectFileCatastrophe( CentralCatalogue.getUIS("fileOpen.alreadyOpen") );
		}
	}

	public void setFileButDontLock(File file){
		this.projectFile = file.toPath();
	}

	public DLProject( File file, UndoBuffer undoBuffer, ConfigurationReplacer configurationReplacer,
	                  ComponentModel componentModel, ObjectModel objectModel )
            throws ProjectFileCatastrophe, ModelCatastrophe, FileNotFoundException {
        this( file.toPath(), undoBuffer, configurationReplacer, componentModel, objectModel );
    }

    public DLProject( Path file, UndoBuffer undoBuffer, ConfigurationReplacer configurationReplacer,
                      ComponentModel componentModel, ObjectModel objectModel )
            throws ProjectFileCatastrophe, ModelCatastrophe, FileNotFoundException {

        if( !Files.exists( file ) ) {
            throw new FileNotFoundException( CentralCatalogue.formatUIS("file.totalExistenceFailure", file ) );
        }

        // Eventually, would be nice to refactor everything so that new models are created when you create a new project.
        // Everything would then reference the models for a specific project.
        this.undoBuffer = undoBuffer;
        this.componentModel = componentModel;
        this.objectModel = objectModel;
	    this.configurationReplacer = configurationReplacer;
        this.projectSettings = ProjectSettings.getInstance();
        this.projectFile = file;
	    this.accessModel = new AccessModel(this);

        // Attempt to lock the file to see if another program is using it. Unlock when done so we can open it.
        if( ! lockProjectFile() ) {
            throw new ProjectFileCatastrophe( CentralCatalogue.getUIS("fileOpen.alreadyOpen") );
        }
        unlockProjectFile();

        FileSystem fileSystem = null;
        Path dlXml = null;
        Path accessXml = null;
        List<Path> dlImages = new ArrayList<Path>();

        long fileReadStart = System.currentTimeMillis();

        // See what type of file we have.
        if( FileFilters.ZipProject.getFilter().accept( projectFile.toFile() ) ) {
            // Zipped project, so make it a filesystem and grab paths for the contents.
            try {
                fileSystem = openFileSystem( projectFile, false );
                try ( DirectoryStream<Path> dirs = Files.newDirectoryStream( fileSystem.getRootDirectories().iterator().next() ) ) {
                    for( Path ze : dirs ) {
                        log.debug( "Found in dlz: " + ze );
                        if (ze.toString().toLowerCase().endsWith(".xml")) {
                            if (ze.toString().toLowerCase().endsWith("access_model.xml")) {
                                accessXml = ze;
                            } else {
                                dlXml = ze;
                            }
                        } else if (ze.toString().toLowerCase().endsWith(".png")) {
                            dlImages.add(ze);
                        }
                    }
                }
            } catch( IOException e ) {
                throw new ProjectFileCatastrophe( CentralCatalogue.getUIS("fileOpen.badZipFile"), e );
            } catch( UnsupportedOperationException e ) {
                throw new ProjectFileCatastrophe( CentralCatalogue.getUIS("fileOpen.badZipFile"), e );
            }
        } else {
            // Old style DL, so just point directly to the XML file.
            dlXml = projectFile;

            // It is possible that this is an exploded DLZ. If so, look for image files.
            if( file.toString().toLowerCase().endsWith(".xml") ) {
                try {
                    for( Path p : Files.newDirectoryStream( dlXml.getParent(), FileFilters.BackgroundImage.getDirectoryFilter() ) ) {
                        dlImages.add( p );
                    }
                } catch( IOException e ) {
                    // Only care if it causes loadImages to fail since this is just a courtesy anyway.
                    log.debug( log.getStackTrace( e ) );
                }
            }
        }

        // The project better have an XML file.
        if( dlXml == null ) {
            throw new ProjectFileCatastrophe( CentralCatalogue.getUIS("fileOpen.noFile") );
        }

        // Parse the file.
        GPathResult xml = readXml( dlXml );

        GPathResult access = null;
        if (accessXml != null) {
            access = readXml(accessXml);
        }

        long fileReadFinish = System.currentTimeMillis();

        projectSettings.reset();

        // If necessary, run some upgrade scripts.
        long upgradeStart = System.currentTimeMillis();

        SaxonMessageListener upgradeMessages = new SaxonMessageListener();
        if( needsUpgrading( xml ) ) {
            xml = upgrade( xml, upgradeMessages );
            needsASave = true;
            // openFile = null  OR  forceSaveAs = true  OR  originalFile = null (might be needed for locking?)
        } else {
            needsASave = false;
            // openFile = originalFile
        }
        long upgradeFinish = System.currentTimeMillis();

        // We got a properly versions XML, let's load it into the model now.
        projectSettings.load( xml );

        long start;

        long bonusFileReadFinish;
        long bonusFileReadStart;

        undoBuffer.stopTracking();
        try {
            // Need this or things can bonk during the load/upgrade since openProject is used to get the models.
            CentralCatalogue.setOpenProject( this );

            componentModel.clear();
            componentModel.metamorphosisStarting( ModelState.INITIALIZING );

            // Start timer after the model is cleared.
            start = System.currentTimeMillis();

            log.trace("Loading Object Model...", "progress");
            objectModel.deserialize(xml.getProperty("objects"));
            log.trace("Loading Component Model...", "progress");
            componentModel.deserialize(xml.getProperty("components"), ComponentLibrary.INSTANCE);
	        if (access != null) {
		        log.trace("Loading Access Model...", "progress");
		        accessModel.loadFromXml(access.getProperty("objects"));
	        }

			// Do model upgrades and manipulation.
	        UpgradeController uc = null;
	        if( configurationReplacer == null ) {
		        // This is really only for testing where you may want to load an old project and keep it old.
		        log.warn("Skipping model upgrades.");
	        } else {
		        uc = new UpgradeController( this );
		        uc.performUpgrade( Double.parseDouble( originalFileVersion ) );
	        }

            // Load the scene graph settings after the upgrade or things like order get lost.
            log.trace("Loading Deployment Panel Settings...");
            CentralCatalogue.getDeploymentPanel().deserialize( xml.getProperty("deploymentpanel") );

            // All loaded. Don't need the XML anymore, so let the GC have it if it needs it.
            xml = null;

            log.trace("Loading Background Images...", "progress");

            bonusFileReadStart = System.currentTimeMillis();
            loadImages(dlImages);
            bonusFileReadFinish = System.currentTimeMillis();

            // The Zip File System implementation isn't very good. There'e no reason to keep it open since you have to
            // close it before anything is actually sync'd to disk. *sigh*
            if( fileSystem != null ) {
                fileSystem.close();
            }

            lockProjectFile();

			componentModel.metamorphosisFinished(ModelState.INITIALIZING);

        } catch( ModelCatastrophe mce ){
            //nothing we can or should do in this case, toss up the chain
            CentralCatalogue.setOpenProject( null );
            throw mce;
        } catch( Exception e ) {
            log.error("Failure while loading a project file, resetting application state");
            //but log the exception first, if you please
            log.error( log.getStackTrace(e) );
            CentralCatalogue.setOpenProject(null);

            //actually, we don't want to try and recover.  Just crash on out, man.
            throw new ModelCatastrophe(e);
        } finally {
            undoBuffer.resumeTracking();
        }

        long finish = System.currentTimeMillis();

        log.info("Loading time = " + ((finish - start) / 1000) + " seconds" );
        log.info("Total Loading, Upgrade, and Unloading time = " + ((finish - upgradeStart) / 1000) + " seconds" );
        log.info("Upgrading time = " + ((upgradeFinish - upgradeStart) / 1000) + " seconds" );
        log.info("XML file read time = " + ((fileReadFinish - fileReadStart) / 1000) + " seconds" );
        log.info("Total file reading time = " + (((fileReadFinish - fileReadStart) + (bonusFileReadFinish - bonusFileReadStart))/ 1000) + " seconds" );
        log.info("Total process time = " + ((finish - fileReadStart) / 1000) + " seconds" );

        if ( upgradeMessages.getMessagesAtOnce().length() > 0 ) {
            log.info( upgradeMessages.getMessagesAtOnce(), "message" );
        }
    }

    public ObjectModel getObjectModel() {
        return objectModel;
    }

    public ComponentModel getComponentModel() {
        return componentModel;
    }

    public ProjectSettings getProjectSettings() {
        return projectSettings;
    }

	public UndoBuffer getUndoBuffer(){
		return undoBuffer;
	}

	public ConfigurationReplacer getConfigurationReplacer() {
		return configurationReplacer;
	}

    public Path getFile() {
        return projectFile;
    }

    public String fileVersion() {
        return originalFileVersion;
    }

    public boolean needsASave() {
        return needsASave;
    }

	public void setNeedsASave(boolean n){
		needsASave = n;
	}

    // todo: Temporarily public until everything is migrated.
    private final Object fileLockLock = new Object();
    public boolean lockProjectFile() {
        boolean success = false;
        synchronized( fileLockLock ) {
            if( fileLock != null ) {
                unlockProjectFile();
            }

            try {
                fileLock = new RandomAccessFile( projectFile.toFile(), "rw" ).getChannel().tryLock();
                if( fileLock != null ) {
                    success = true;
                }
            } catch( Exception e ) {
                log.warn("Exception locking file " + projectFile + ": " + log.getStackTrace(e) );
            }
        }
        return success;
    }

    // todo: Temporarily public until everything is migrated.
    public void unlockProjectFile() {
        synchronized( fileLockLock ) {
            try {
                if( fileLock != null ) {
                    if( fileLock.isValid() ) {
                        fileLock.release();
                    }
                    fileLock.channel().close();
                    fileLock = null;
                }
            } catch(Exception e) {
                log.warn("Exception unlocking file: " + log.getStackTrace(e) );
            }
        }
    }

    public void close() {
        unlockProjectFile();
        if (this == CentralCatalogue.getOpenProject()) {
            CentralCatalogue.setOpenProject(null);
        }
        projectSettings.reset();
    }

    public boolean export( File newFile ) {
        return save( true, newFile.toPath() );
    }

    public boolean saveAs( File newFile ) {
        //-- unlock/lock appropriately
        //-- Change our project file

		unlockProjectFile();
		this.setFile(newFile);
		return save(false, null);
    }

    public boolean save() {
        return save( false, null );
    }

	//todo: always save all versions of all the images if they've changed, eliminating the processImages switch

	/**
	 * Save the project to a DLZ file.
	 * @param forExport if true, do special handling for exported models, include creating zone images and scaling down the drawing image
	 * @param fileToUseInstead if non-null, use this Path as the destination, and leave the "main" file alone
	 * @return true/false if the save was successful
	 */
    private boolean save( boolean forExport, Path fileToUseInstead ) {
	    // todo: fileToUseInstead should just be the file to use. save() would pass projectFile rather than this assuming it.
	    //       Should also eliminate the need for calling setFile in ExportController. Should really remove all setFile methods
	    //       since saveAs() should encapsulate setting that variable.

        boolean success = false;
        DLStopwatch watch = new DLStopwatch();

        // Write to a temp file first, then move it if everything is successful. Otherwise, it is possible to corrupt
        // the file if an error occurs. Use '.zip' since the Zip provider has hardcoded assumptions that can hide errors.
        Path tmpFile = Paths.get( StringUtil.replaceExtension( projectFile.toString(), "zip" ) );

        try ( FileSystem fileSystem = openFileSystem( tmpFile, true ) ) {

            if ( forExport ){
                //process images
                ImageExporter ie = new ImageExporter();
                ie.exportToEs(componentModel);
            }

            projectSettings.edit( projectFile.getFileName().toString() );

            // Save the drawing background images first.
            for( DLComponent drawing : componentModel.getComponentsByType("drawing") ) {
                DLObject drawingObj = drawing.getObject( ObjectType.DRAWING );
                BufferedImage deploymentImage = null;

                if ( forExport ){
                    // todo: MULTI-DC - Refactor to use DLZoneImage.

                    Setting imageProp = (Setting) drawingObj.getObjectProperty("image");

                    //the scaled-down version
                    deploymentImage = ARGBConverter.binaryDataToBufferedImage( (BinaryData) imageProp.getValue() );

                    // Flush the image data since we don't need it anymore and don't want to save it or keep it around.
                    imageProp.setValue( null );
                } else {
                    //the full-size bg image
	                if ((Boolean)drawing.getPropertyValue("hasImage")){
		                deploymentImage = ((DLImage) drawing.getPropertyValue("image_data")).getImage();
	                }
                }

                if ( deploymentImage != null ){
                    DLStopwatch timer = new DLStopwatch();
                    Path imgFile = fileSystem.getPath( buildImageFileName(drawing) );
                    try ( OutputStream stream = Files.newOutputStream( imgFile ) ) {
                        ImageIO.write( deploymentImage, "png", stream );
                    } catch( IOException e ) {
                        throw e;
                    }
                    log.trace( timer.finishMessage("Saved bg image " + imgFile.toAbsolutePath() ) );
                }

                // Process the room images.
                if ( forExport ){
                    // todo: Refactor to use DLZoneImage.
                    HashSet<DLObject> currentDrawing = new HashSet<>(1);
                    currentDrawing.add( drawingObj );
                    for( DLObject room : objectModel.getObjectsByTypeInDrawings( ObjectType.ROOM, currentDrawing ) ) {
                        Setting imageProp = (Setting) room.getObjectProperty("image");
                        BufferedImage roomImage = ARGBConverter.binaryDataToBufferedImage( (BinaryData) imageProp.getValue() );
                        if ( roomImage != null ){
                            DLStopwatch timer = new DLStopwatch();
                            Path imgFile = fileSystem.getPath( buildImageFileName( room ) );
                            try ( OutputStream stream = Files.newOutputStream( imgFile ) ) {
                                ImageIO.write( roomImage, "png", stream );
                            } catch( IOException e ) {
                                throw e;
                            }

                            // Flush the image data since we don't need it anymore and don't want to save it or keep it around.
                            imageProp.setValue( null );
                            log.trace( timer.finishMessage("Saved room image " + imgFile.toAbsolutePath() ) );
                        }
                    }
                }
            }

            Path xmlPath = fileSystem.getPath( StringUtil.replaceExtension( projectFile.getFileName().toString(),
                                                                            FileFilters.XMLFile.getFilter().getExtensions()[0] ) );

            // Purge unused orphanages.
            for( DLComponent orphanage : componentModel.getComponentsByType( ComponentType.ORPHANAGE ) ) {
                if( orphanage.getChildComponents().isEmpty() ) {
                    componentModel.remove( orphanage );
                }
            }

            // Now save the models. Must be after the images since we want the zone images data purged after they are saved.
            DLProjectWriter.saveModel( this, xmlPath, !forExport );

            if (accessModel !=null && !accessModel.isEmpty()){
                xmlPath = fileSystem.getPath("access_model.xml");
                DLProjectWriter.saveModel(this, xmlPath, true, true);
            }

            // Close the temp file and overwrite the original with it.
            fileSystem.close();

			if(fileToUseInstead == null){
				unlockProjectFile();
				Files.move( tmpFile, projectFile, StandardCopyOption.REPLACE_EXISTING );
				log.info("File saved to '" + projectFile + "'.", "statusbar");
				lockProjectFile();
				undoBuffer.markSavePoint();
				needsASave = false;
			} else {
				//use the provided file instead
				Files.move( tmpFile, fileToUseInstead, StandardCopyOption.REPLACE_EXISTING );
				log.info("File saved to '" + fileToUseInstead + "'.", "default");
			}

            success = true;
            //log.info("File saved to '" + projectFile + "'.", "statusbar");
        } catch(Exception e) {
            log.error("Exception saving file '" + projectFile + "' :" + log.getStackTrace(e) );
            log.error("File could not be saved!\nTry saving to a new location using File->SaveAs.", "message");
        } finally {
            if( tmpFile != null ) {
                try {
                    Files.deleteIfExists( tmpFile );
                } catch( IOException e ) {
                    log.trace("Exception deleting temp file: " + tmpFile + ":\n" + log.getStackTrace(e) );
                }
            }
        }

        log.trace( watch.finishMessage( "File Save" ) , "default" );

        return success;
    }

    public void unlinkFromSmartZone() {
        projectSettings.setSmartZoneHost("");
        projectSettings.setSmartZonePort("");

        // Purge all smartZoneIds
        for( DLComponent c : componentModel.getComponents() ) {
            c.unsetSmartZoneId();
        }
    }

    private FileSystem openFileSystem( Path file, boolean create ) throws IOException {
		String fileUri = file.toUri().toString();
		URI uri = URI.create("jar:" + fileUri );

        HashMap<String, String> env = new HashMap<String, String>();
        if( create ) {
            env.put( "create", "true" );
        }

        return FileSystems.newFileSystem( uri, env );
    }


    private GPathResult readXml( Path xmlFile ) {
        GPathResult result;

        try ( InputStream is = Files.newInputStream( xmlFile ) ) {

            result = new XmlSlurper().parse( is );

            // The original version ran the file through a UTF8 decoder before parsing. That doesn't seem to be needed
            // anymore. Just wanted to comment that this was removed in case some old file has the problem when upgraded.
        } catch( Exception e ) {
            throw new ProjectFileCatastrophe( CentralCatalogue.getUIS("fileOpen.noFile"), e );
        }

        return result;
    }

    // Once fully migrated, this will be private and done from within.
    private int loadImages( List<Path> imageFiles ) {
        int loadedCnt = 0;

        for( DLComponent drawing : componentModel.getComponentsByType("drawing") ) {
            try {
                // Load the image from the image file.
                Path imageFile = findImageFile( imageFiles, drawing );

                if( imageFile != null ) {
                    DLImage srcImage = new DLImage( imageFile );
	                if ((Boolean)drawing.getPropertyValue("hasImage")){
                        drawing.setPropertyValue( "image_data", srcImage );
                        drawing.setPropertyValue( "width", srcImage.getWidth() );
                        drawing.setPropertyValue( "height", srcImage.getHeight() );
                        loadedCnt++;
                    } else {
                        // Old style where the canvas was a blank image file. Set the canvas to match the old image.
                        drawing.setPropertyValue( "image_data", null );
                        drawing.setPropertyValue( "width", srcImage.getWidth() );
                        drawing.setPropertyValue( "height", srcImage.getHeight() );
                    }
                } else {
                    // May be the old style where the image is in the XML. However, it may have already been converted during an upgrade.
                    ComponentProperty prop = drawing.getComponentProperty("image_data");
                    if( prop.getBase64Image() != null ) {
                        ARGBConverter.loadComponentImage( drawing );
                    }

                    if( prop.getValue() == null && (Boolean)drawing.getPropertyValue("hasImage") ) {
                        // The component thinks it has an image, but we didn't find one. So figure out an acceptable canvas size.
                        log.error(CentralCatalogue.formatUIS("fileOpen.noImage", drawing.getName()), "message");
                        Dimension minSize = CentralCatalogue.getDeploymentPanel().getSmallestAllowedCanvas( drawing );
                        drawing.setPropertyValue( "image_data", null );
                        drawing.setPropertyValue( "hasImage", false );
                        drawing.setPropertyValue( "width", minSize.getWidth() );
                        drawing.setPropertyValue( "height", minSize.getHeight() );
                    } else {
                        loadedCnt++;
                    }
                }
            } catch( Exception e ) {
                log.error( CentralCatalogue.formatUIS("fileOpen.noImage", drawing.getName() ), "message");
                log.error( log.getStackTrace(e) );
                if( (Boolean)drawing.getPropertyValue("hasImage") ) {
                    // Bad image. Can happen if the DLZ or png are corrupt. So figure out an acceptable canvas size.
                    Dimension minSize = CentralCatalogue.getDeploymentPanel().getSmallestAllowedCanvas( drawing );
                    drawing.setPropertyValue( "image_data", null );
                    drawing.setPropertyValue( "hasImage", false );
                    drawing.setPropertyValue( "width", minSize.getWidth() );
                    drawing.setPropertyValue( "height", minSize.getHeight() );
                }
            }
        }

        return loadedCnt;
    }


    /**
     * Builds the standard project image name for the provided object.
     *
     * @param o  The object to base the image name on.
     * @return   A standard project image file name.
     */
    public static String buildImageFileName( DLObject o ) {
        return buildImageFileName( o.getDlid() );
    }


    /**
     * Builds the standard project image name for the provided component.
     *
     * @param c  The component to base the image name on.
     * @return   A standard project image file name.
     */
    public static String buildImageFileName( DLComponent c ) {
        return buildImageFileName( c.getObject( ObjectType.DRAWING ).getDlid() );
    }


    /**
     * Builds the standard project image name with the provided parameters.
     *
     * @param dlid  The image DLID.
     * @return      A standard project image file name.
     */
    public static String buildImageFileName( Dlid dlid ) {
        return dlid.toString().replace(":", "_") + ".png";
    }


    /**
     * Searches the list of image files found in the project file to find the one matching the provided component.
     * This method will attempt to search for the file in the following manner, in this order:
     *     1) By Component's Drawing DLID.
     *     2) If the Component's drawing has been upgraded, by the old DLID.
     *     3) By the project filename (old style)
     *     4) If there's only one, just return it (old style, but the project file name may have been manually changed)
     *
     * @param imageFiles  The available image files to search.
     * @param comp        The component in need of an image file.
     *
     * @return The path to the image file or null if none is found.
     */
    private Path findImageFile( List<Path> imageFiles, DLComponent comp ) {

        // First check the default that should be correct for current projects.
        Path result = findImageFile( imageFiles, buildImageFileName( comp ) );
        if( result == null ) {
            // Not found! Upgrades may change the component DLID, so try using the old one if it exists.
            Dlid oldDlid = projectSettings.findOldDlid( comp.getObject( ObjectType.DRAWING ).getDlid() );
            if( oldDlid != null ) {
                result = findImageFile( imageFiles, buildImageFileName( oldDlid ) );
            }

            if( result == null ) {
                // Still not found! This might be an old style where the image name is based on the project file name.
                // Should only be one since it is preMultiDrawing, but check by name at first to be safe.
                result = findImageFile( imageFiles, FileUtil.changeExtension( projectFile.toFile(), "png").getName() );

                // Well, I guess just return whatever image we have if we only have one image.
                if( imageFiles.size() == 1 && this.componentModel.getComponentsByType("drawing").size()==1 ) {
                    result = imageFiles.get(0);
                }
            }
        }

        return result;
    }

    /**
     * Searches the list of image files found in the project file to find the one matching the provided file name.
     *
     * @param imageFiles     The available image files to search.
     * @param imageFileName  The file name, with no path.
     *
     * @return  The path to the image file or null if none is found.
     */
    private Path findImageFile( List<Path> imageFiles, String imageFileName ) {
        for( Path p : imageFiles ) {
            if( p.endsWith( imageFileName ) ) {
                return p;
            }
        }
        return null;
    }

    private boolean needsUpgrading( GPathResult xml ) {
        originalFileVersion = ((GPathResult) xml.getProperty("@version")).text();
		double xmlVersion;
		try{
			xmlVersion   = Double.parseDouble( originalFileVersion );
		} catch (NumberFormatException nfe) {
			throw new ProjectFileCatastrophe( CentralCatalogue.formatUIS("fileupgrade.unknownVersion", "" ), nfe );
		}
        double modelVersion = DeploymentLabVersion.getModelVersionByDouble();

        if( xmlVersion < modelVersion ) {
            return true;
        }

        if( xmlVersion > modelVersion ) {
            throw new ProjectFileCatastrophe( CentralCatalogue.formatUIS("fileupgrade.unknownVersion", Double.toString( xmlVersion ) ) );
        }

        return false;
    }

    private GPathResult upgrade( GPathResult preXml, SaxonMessageListener messages ) {
        GPathResult upgradedXml;
        try{
            String xmlVersion = ((GPathResult) preXml.getProperty("@version")).text();

            String scripts = CentralCatalogue.getUp( xmlVersion );
            log.trace("Upgrading Project File (Scripts:" + (scripts == null ? "None Specified" : scripts) + ")...", "progress");

            if( scripts == null || scripts.isEmpty() ) {
	            // The new auto-update process was added in 6.1 (model 2.1), so no scripts required for versions since.
	            if( Double.parseDouble( xmlVersion ) < 2.1 ) {
		            throw new ProjectFileCatastrophe( CentralCatalogue.formatUIS("fileupgrade.missingUpgrade", xmlVersion ) );
	            }
                log.trace("No upgrade scripts required, just an update.");
                upgradedXml = preXml;
            } else {
                // Make a copy of the original file.
                File tempFile = File.createTempFile( projectFile.getFileName().toString(), ".dlt" );
                tempFile.deleteOnExit();
                log.info("Upgrading from temp file " + tempFile.getAbsolutePath() );
                FileUtil.copyFile( projectFile.toFile(), tempFile );

                for( String s : scripts.split(",") ) {
                    tempFile = upgradeProjectFile( tempFile, s, messages );
                }

                upgradedXml = readXml( tempFile.toPath() );

                //mark that we just did the upgrade
                projectSettings.setRefreshAllRules( true );
                needsASave = true;
                projectSettings.upgrade( scripts );
            }
        } catch( ProjectFileCatastrophe pfe ) {
	        // Just to stop chaining the same exception to itself.
	        throw pfe;
        } catch( Exception e ) {
            throw new ProjectFileCatastrophe("Cannot upgrade version:" + log.getStackTrace( e ), e );
        }

        return upgradedXml;
    }

    private File upgradeProjectFile( File file, String upgradeName, SaxonMessageListener messages ) throws IOException, SaxonApiException {

        log.info("Upgrading project file format from version " + upgradeName );

        File upgradedFile = File.createTempFile(upgradeName, ".dl");
        upgradedFile.deleteOnExit();
        log.info( "temp file: " + upgradedFile.getPath() );

        ClassLoader cl = this.getClass().getClassLoader();
        CachingErrorListener el = new CachingErrorListener();

        Processor saxonP = new Processor( false );
        XsltCompiler compiler  = saxonP.newXsltCompiler();

        try ( InputStream sourceStream = Files.newInputStream( file.toPath() );
              InputStream scriptStream = cl.getResourceAsStream("cfg/upgradescripts/" + upgradeName + ".xslt" ) ) {

            XsltExecutable saxExe = compiler.compile( new StreamSource( scriptStream ) );

            XsltTransformer t = saxExe.load();
            t.setErrorListener( el );

            t.setSource( new StreamSource( sourceStream ) );

            Serializer ser = new Serializer( upgradedFile );
            t.setDestination( ser );
            t.setMessageListener( messages );

            t.transform();

            /* todo: Investigate. Original version had this. The string was never used, though. Not exactly sure why. I don't
               think errors end-up in the other message listener since that is suppose to only be xsl:message instructions, not errors.

            if( el.hasAnyMessages() ) {
                StringBuilder sb = new StringBuilder();
                if( el.hasWarnings() ) {
                    sb.append("Warnings:\n");
                    for( TransformerException te : el.getWarnings() ) {
                        sb.append( te.getMessageAndLocation() );
                        sb.append("\n");
                    }
                }
                if( el.hasErrors() ) {
                    sb.append("Errors:\n");
                    for( TransformerException te : el.getErrors() ) {
                        sb.append( te.getMessageAndLocation() );
                        sb.append("\n");
                    }
                }
                if( el.hasFatalErrors() ) {
                    sb.append("FatalErrors:\n");
                    for( TransformerException te : el.getFatalErrors() ) {
                        sb.append( te.getMessageAndLocation() );
                        sb.append("\n");
                    }
                }
                //log.info("Conversion produced the following:\n")
            }
            */
        }

        return upgradedFile;
    }
}
