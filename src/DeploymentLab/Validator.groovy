
package DeploymentLab

import DeploymentLab.Model.*
import DeploymentLab.SceneGraph.ArbitraryShapeNode
import DeploymentLab.channellogger.*
import DeploymentLab.Security.MacChecksum
import DeploymentLab.PropertyEditor.ConfigModbusEditor
import com.google.common.net.InetAddresses

import java.awt.Shape
import java.awt.geom.Point2D

import DeploymentLab.Image.DLImage

public class Validator {
	private static final Logger log = Logger.getLogger(Validator.name)

	public static final int RESULT_PASS =  0
	public static final int RESULT_WARN =  1
	public static final int RESULT_FAIL =  2
	public static final int RESULT_ABORT = 3 // Exception running check

    private static final int SENDDELTA_MIN_TEMP_F = 3    // Farenheit
    private static final int JAWA_MAX_DIST_IN     = 120  // inches

    private static final int BACNET_INSTANCE_MIN = 0
    private static final int BACNET_INSTANCE_MAX = 4194303

	private static final Map<Integer,Integer> RADIO_PACKETS = [20:2, 51:5, 52:3]

	private static final Map<String,String> DEFAULT_NAMES_TO_CHECK = [ (ObjectType.WSN_GATEWAY):"Gateway" ]
	private warnResults = []
	private failResults = []

	private infoResults = []

	private ObjectModel omodel
	private ComponentModel cmodel

	private Properties uiStrings
	private ProjectSettings projectSettings

	Validator(ObjectModel om, ComponentModel cm, ProjectSettings ps ) {
		omodel = om
		cmodel = cm
		uiStrings = CentralCatalogue.getInstance().getUIStrings()
		projectSettings = ps
	}

	int validate() {
		try {
			checkDeprecated()
			checkModelIntegrity()
			nameMaxLength()
			wsnUniquesNetworkNames()
			wsnUniquesNodeNames()
			wsnMacIds()
			wsnNetworkVersion()
			wsnMinTempSendDelta()
			wsnNetworkPanId()
			wsnNetworkGateway()
			wsnNetworkSize()
			wsnSensepointRanges()
			validAltitude()
			groupingNames()
			drawingName()
			defaultNames()
			zoneSize()
			samplePeriod()
			requiredAssociations()
			modbusAddress()
			modbusResponseTimeout()
			modbusRetries()
			uniqueModbusDevice()
			uniqueModbusDeviceName()
			modbusNetworkSize()
			constellation2()
			uniquePdiPduNames()
			pdiPduAssociations()
			modbusDevicePullPeriods()
			ionMeters()
			tableFitPoints()
			pueOperations()
			preventHugeNumbers()
			warnHugeDecimals()
			onlyOnePue()
			threePhase()
			limitCoordinates()
			objectsWithinBounds()
			checkCycles()
			SNMPAgent()
			SNMPAgentV1()
			SNMPAgentV2c()
			SNMPAgentV3()
		//	WebServiceHost()
			controlDeviceConfigCheck()
			modbusGateways()
			validateHorizontalRows()
			crahMetrics()
			uniqueRackNames()
			hostName()
			powerComponents()
			tempManualConfigExpressionValid()
			generalManualConfigExpressionValid()
			checkModbusRanges()
			crahsInLI()
			inletsOutlets()
			modbusIntegrator()
			bacnetComponents()
            uniqueBacnetInstances()
			checkOrphans()
			controlRoomChecks()
			//AC2Validations()
			wsnSensorNames()
			chartDupeEntries()
			checkLontalkPlugin()
			checkIPMI()
			optionalRefTemp()
            checkRtuManagerName()
			ackbar()
			checkWSNReassignment()
			checkPlanningGroups()
			checkPhantomAssociations()
			checkOverlappingRooms()
			checkSameRoomAssociations()
			checkComponentObjectsInSameRoom()
		} catch(Exception e) {
			failResults += new ValidationResult("- INTERNAL VALIDATION ERROR -", [])

			log.error("Error occurred while running validation checks!", "message")
			log.error(log.getStackTrace(e))
			return RESULT_ABORT
		}

		log.info("Validation: failures=${failResults.size()} warnings=${warnResults.size()} infos=${infoResults.size()}")

		if(failResults.size() > 0)
			return RESULT_FAIL
		if(warnResults.size() > 0)
			return RESULT_WARN
		return RESULT_PASS
	}

	def getWarnings() {
		return warnResults
	}

	def getFailures() {
		return failResults
	}

	def getInfos() {
		return infoResults
	}
	
	public setFailures(ValidationResult vr){
		failResults += vr
	}

	/**
	 * Installs a failure ValidationResult with the provided message and pointing to the specified component.
	 * @param uiString the uiString key to use to retrieve the message from uistrings.properties
	 * @param c the DLComponent to attach the error to
	 * @see ValidationResult
	 * @see CentralCatalogue#getUIS
	 * @since Mars
	 * @deprecated Use the version that supports formatted strings: {@link #makeFail( DLComponent, String, Object... )}
	 */
	@Deprecated
	private void makeFail(String uiString, DLComponent c){
		this.makeFail(uiString,[c])
	}

	/**
	 * Installs a failure ValidationResult with the provided message and pointing to the specified list of components.
	 * @param uiString the uiString key to use to retrieve the message from uistrings.properties
	 * @param comps a list of DLComponents to attach the error to
	 * @see ValidationResult
	 * @see CentralCatalogue#getUIS
	 * @since Mars
	 * @deprecated Use the version that supports formatted strings: {@link #makeFail( List<DLComponent>, String, Object... )}
	 */
	@Deprecated
	private void makeFail(String uiString, List<DLComponent> comps){
		if( ! CentralCatalogue.getInstance().getUIStrings().containsKey(uiString) ){
			log.error("failed to find error UI key $uiString")
		}
		failResults += new ValidationResult( CentralCatalogue.getUIS( uiString ), comps)
	}

	/**
	 * Installs a failure ValidationResult with the provided message and pointing to the specified component.
	 * @param uiString the uiString key to use to retrieve the message from uistrings.properties
	 * @param c the DLComponent to attach the error to
	 * @see ValidationResult
	 * @see CentralCatalogue#getUIS
	 * @since Mars
	 */
	private void makeFail( DLComponent c, String uiString, Object... uiStringArgs ){
		this.makeFail( [c], uiString, uiStringArgs )
	}

	/**
	 * Installs a failure ValidationResult with the provided message and pointing to the specified list of components.
	 * @param uiString the uiString key to use to retrieve the message from uistrings.properties
	 * @param comps a list of DLComponents to attach the error to
	 * @see ValidationResult
	 * @see CentralCatalogue#getUIS
	 * @since Mars
	 */
	private void makeFail( List<DLComponent> comps, String uiString, Object... uiStringArgs ){
		if( ! CentralCatalogue.getInstance().getUIStrings().containsKey(uiString) ){
			log.error("failed to find error UI key $uiString")
		}
		failResults += new ValidationResult( CentralCatalogue.formatUIS( uiString, uiStringArgs ), comps )
	}

	/**
	 * Installs a warning ValidationResult with the provided message and pointing to the specified component.
	 * @param uiString the uiString key to use to retrieve the message from uistrings.properties
	 * @param c the DLComponent to attach the warning to
	 * @see ValidationResult
	 * @see CentralCatalogue#getUIS
	 * @since Mars
	 * @deprecated Use the version that supports formatted strings: {@link #makeWarn( DLComponent, String, Object... )}
	 */
	@Deprecated
	private void makeWarn(String uiString, DLComponent c){
		this.makeWarn(uiString,[c])
	}

	/**
	 * Installs a warning ValidationResult with the provided message and pointing to the specified list of components.
	 * @param uiString the uiString key to use to retrieve the message from uistrings.properties
	 * @param comps a list of DLComponents to attach the warning to
	 * @see ValidationResult
	 * @see CentralCatalogue#getUIS
	 * @since Mars
	 * @deprecated Use the version that supports formatted strings: {@link #makeWarn( List<DLComponent>, String, Object... )}
	 */
	@Deprecated
	private void makeWarn(String uiString, List<DLComponent> comps){
		if( ! CentralCatalogue.getInstance().getUIStrings().containsKey(uiString) ){
			log.error("failed to find error UI key $uiString")
		}
		warnResults += new ValidationResult( CentralCatalogue.getUIS( uiString ), comps)
	}

	/**
	 * Installs a warning ValidationResult with the provided message and pointing to the specified component.
	 * @param uiString the uiString key to use to retrieve the message from uistrings.properties
	 * @param c the DLComponent to attach the warning to
	 * @see ValidationResult
	 * @see CentralCatalogue#getUIS
	 * @since Mars
	 */
	private void makeWarn( DLComponent c, String uiString, Object... uiStringArgs ){
		this.makeWarn( [c], uiString, uiStringArgs )
	}

	/**
	 * Installs a warning ValidationResult with the provided message and pointing to the specified list of components.
	 * @param uiString the uiString key to use to retrieve the message from uistrings.properties
	 * @param comps a list of DLComponents to attach the warning to
	 * @see ValidationResult
	 * @see CentralCatalogue#getUIS
	 * @since Mars
	 */
	private void makeWarn( List<DLComponent> comps, String uiString, Object... uiStringArgs ){
		if( ! CentralCatalogue.getInstance().getUIStrings().containsKey(uiString) ){
			log.error("failed to find warning UI key $uiString")
		}
		warnResults += new ValidationResult( CentralCatalogue.formatUIS( uiString, uiStringArgs ), comps )
	}

	/**
	 * Installs a info ValidationResult with the provided message and pointing to the specified list of components.
	 * @param uiString the uiString key to use to retrieve the message from uistrings.properties
	 * @param comps a list of DLComponents to attach the info to
	 * @see ValidationResult
	 * @see CentralCatalogue#getUIS
	 * @since Jupiter 2
	 */
	private void makeInfo(String uiString, List<DLComponent> comps){
		if( ! CentralCatalogue.getInstance().getUIStrings().containsKey(uiString) ){
			log.error("failed to find error UI key $uiString")
		}
		infoResults += new ValidationResult( CentralCatalogue.getUIS( uiString ), comps)
	}

	private void checkDeprecated(){
		cmodel.getDeprecatedComponents().each { c ->
			failResults += new ValidationResult( "${c.getName()} is of a type that is no longer supported (${c.getDisplaySetting("name")}).  The project cannot be exported until it has been removed."  ,c)
		}
	}

	/**
	 * Do some basic integrity checks to catch possible bugs that could corrupt the DB.
	 */
	private void checkModelIntegrity(){
		// Make sure we don't have objects exporting when the component is set to not export
		def objsHidden = []
		def objsRogue = []
		cmodel.getComponents().each { c ->
			def objs = ( c.hasRole("proxy") ? c.getRoots() : c.getManagedObjects() )
			objs.each { o ->
				if( !o.isExportable() && c.isExportable() ) {
					objsHidden += c
				} else if( o.isExportable() && !c.isExportable() ) {
					objsRogue += c
				}
			}
		}

		if( objsHidden ) {
			failResults += new ValidationResult("Model Integrity: Exporting Component has Objects that won't export.", objsHidden )
		}

		if( objsRogue ) {
			failResults += new ValidationResult("Model Integrity: Component not exporting has Objects that will export.", objsRogue )
		}
	}

	private void nameMaxLength() {
		cmodel.components.each{ c ->
			if('name' in c.listProperties()) {
				String name = c.getComponentProperty('name').getValue()
				if(name.length() > 100)
					failResults += new ValidationResult("'Name' cannot be longer than 100 characters.", [c])
			}
		}
	}

	private void wsnUniquesNetworkNames() {
		def names = [:]
		omodel.getObjectsByType('wsnnetwork').each{ o ->
			String name = o.getObjectProperty('name').getValue()
			if(! names.containsKey(name)) {
				names[name] = []
			}
			names[name] += o
		}

		names.each{ name, l ->
			def clist = l.collect{it -> cmodel.getOwner(it)}.unique()
			if(l.size() > 1)
				failResults += new ValidationResult("Cannot have more than one network named '$name'.", clist)
		}
	}

	private void groupingNames() {
		def names = [:]

		cmodel.getComponentsInAnyRole( [ ComponentRole.ZONE, ComponentRole.ROOM ] ).each { c ->
			String name = c.getName()

			if(name.length() < 3) {
				failResults += new ValidationResult("${c.getPropertyValue( ComponentProp.CONFIGURATION )} name must be at least 3 characters long.", [c] )
			}

			if(! names.containsKey(name)) {
				names[name] = []
			}
			names[name] += c
		}

		names.each{ name, l ->
			if(l.size() > 1)
				failResults += new ValidationResult("Cannot have more than one grouping named '$name'.", l )
		}
	}

	private void wsnUniquesNodeNames() {
		def names = [:]
		omodel.getObjectsByType('wsnnode').each{ o ->
			String name = o.getObjectProperty('name').getValue()
			if(! names.containsKey(name)) {
				names[name] = []
			}
			names[name] += o
		}

		names.each{ name, l ->
			def clist = l.collect{it -> cmodel.getOwner(it)}.unique()
			if(l.size() > 1)
				failResults += new ValidationResult("Cannot have more than one node named '$name'.", clist)
		}
	}

	private void drawingName() {
		omodel.getObjectsByType( ObjectType.DRAWING ).each{ o ->
			String name = o.getObjectProperty('name').getValue()
			if(name == 'Drawing')
				warnResults += new ValidationResult("Change name 'Drawing' from default", [cmodel.getOwner(o)])
			if(name.trim().length() < 3)
				failResults += new ValidationResult("Drawing name must be at least 3 characters long.", [cmodel.getOwner(o)])
		}
	}

	private void defaultNames() {
		DEFAULT_NAMES_TO_CHECK.each { obj, defaultName ->
			omodel.getObjectsByType( obj ).each{ o ->
				String name = o.getObjectProperty('name').getValue()
				if( name == defaultName ) {
					makeWarn('validator.general.defaultName', [cmodel.getOwner(o)] )
				}
			}
		}
	}

	private void wsnMacIds() {
		//check both gateways and nodes
		def macids = [:]
		//omodel.getObjectsByType('wsnnode').each{ o ->
		for(DLObject o : omodel.getObjectsByType('wsnnode')){
			long macid = o.getObjectProperty('mac').getValue()
			if(macid == null || macid == 0) {
				failResults += new ValidationResult("MAC ID is required.", [cmodel.getOwner(o)])
			} else {
				if(! MacChecksum.isValidMac(macid, o.getObjectSetting("platformId").getValue()))
					warnResults += new ValidationResult("MAC ID '" + Long.toHexString(macid) + "' has a bad checksum.", [cmodel.getOwner(o)])
				if(! macids.containsKey(macid))
					macids[macid] = []
				macids[macid] += o
			}
		}

		// (Bug 9877) Generate a fake MAC for Gateways. We only guarantee uniqueness in this project, but we put some
		// trivial effort into reducing collision chances elsewhere (time based, only 32-bit, not valid, not used)
		int fakeId = (int) System.currentTimeMillis();
		omodel.getObjectsByType('wsngateway').each{ o ->
			long uid = o.getObjectProperty('mac').getValue()

			while(uid == null || uid == 0) {
				if( !MacChecksum.isValidMac( fakeId ) && !macids.containsKey( fakeId ) ) {
					cmodel.getOwner(o).setPropertyValue('mac_id', Integer.toHexString( fakeId ) )
					uid = o.getObjectProperty('mac').getValue()
					log.debug("Generating fake wsngateway mac: " + Long.toHexString( uid ) )
				}
				fakeId++
			}

			if( !macids.containsKey(uid) ) {
				macids[uid] = []
			}
			macids[uid] += o
		}

		//dupe checking across both gateways and nodes
		macids.each{ id, l ->
			def clist = l.collect{it -> cmodel.getOwner(it)}.unique()
			if(l.size() > 1)
				failResults += new ValidationResult("Duplicate MAC ID of '${Long.toHexString(id)}'.", clist)
		}
	}

	private void zoneSize() {

		omodel.getObjectsByType('logicalzone').each{ o ->
			int numids = o.getObjectProperty('objects').getChildIds().size()
			if(numids == 0)
				warnResults += new ValidationResult("Calculation Group is empty.", [cmodel.getOwner(o)])
			/*
			else if(numids > 350)
				failResults += new ValidationResult("$numids exceeds the maximum number of objects (350) per Calculation Group", [cmodel.getOwner(o)])
			else if(numids >= 300)
				warnResults += new ValidationResult("$numids is approaching the maximum number (350) of objects in a Calculation Group", [cmodel.getOwner(o)])
			*/
		}

	}

	private void wsnNetworkVersion() {
		omodel.getObjectsByType('wsnnetwork').each{ o ->
			String version = o.getObjectProperty('version').getValue()
			if( version != 'N29' && version != 'N30')
				failResults += new ValidationResult("Network version must be 'N29' or 'N30'.", [cmodel.getOwner(o)])
		}
	}

	private void samplePeriod() {
		cmodel.components.each{ c ->
			if('sample_interval' in c.listProperties()) {
				int samples = c.getComponentProperty('sample_interval').getValue()

                // ThermaNode DX is special.
                if( c.type == 'crac-thermanode-dx' ) {
                    if( samples < 1 ) {
                        failResults += new ValidationResult( String.format(CentralCatalogue.getUIS('validator.sampleInterval.tooLow'), 1), [c])
                    }
                } else {
                    if(samples < 3)
                        failResults += new ValidationResult( String.format(CentralCatalogue.getUIS('validator.sampleInterval.tooLow'), 3), [c])

                    else if(samples < 5)
                        warnResults += new ValidationResult("Setting Sampling Interval to less than 5 minutes can cause network instabilities.", [c])
                }
            }
		}
	}

	private void wsnMinTempSendDelta() {
		omodel.getObjectsByType('wsnnode').each{ n ->
			def sensors = n.getObjectProperty('sensepoints').getChildren()
			for(DLObject s: sensors) {
				int type = s.getObjectProperty('type').getValue()
				if(! (type in [1, 7, 13, 21, 22, 25, 27, 33])) // All types with data class 200, temperature
					continue
				Integer delta = s.getObjectProperty('smartSendThreshold').getValue()
				if(delta == null || delta == 0.0)
					continue
                if(delta < SENDDELTA_MIN_TEMP_F) {
                    failResults += new ValidationResult( String.format( uiStrings.getProperty('validator.tempSendDelta.tooLow'),
                                                                        Localizer.convert( SENDDELTA_MIN_TEMP_F, 'temperature'),
                                                                        Localizer.getUnits('temperature') ),
                                                         [cmodel.getOwner(n)] )
					break
				}
			}
		}
	}

	private void wsnNetworkPanId() {
		def panids = [:]
		omodel.getObjectsByType('wsnnetwork').each{ o ->
			int panId = o.getObjectProperty('panId').getValue()
			// 0xFFFE = 65534
			if(panId < 1 || panId > 65534)
				failResults += new ValidationResult("PAN ID must be a hex value between 1 and FFFE.", [cmodel.getOwner(o)])
			if(! panids.containsKey(panId))
				panids[panId] = []
			panids[panId] += o
		}

		panids.each{ id, l ->
			def clist = l.collect{it -> cmodel.getOwner(it)}.unique()
			if(l.size() > 1)
				failResults += new ValidationResult("Networks must have unique PAN IDs.", clist)
		}
	}

	private void wsnNetworkGateway() {
		def gatewayAddresses = [:]
		omodel.getObjectsByType('wsngateway').each{ o ->
			String address = o.getObjectProperty('address').getValue()
			if(address == '') {
				failResults += new ValidationResult("Each gateway must have an address.", [cmodel.getOwner(o)])
			} else {
				String protocol = o.getObjectProperty('protocol').getValue()

				if(protocol.equals("TCP_RG2")){
					// Allow the bracketed IPv6 with port format if we are in technician mode by stripping the brackets
					// and port from the string so we can still make sure the address is a proper one.
					if( address.startsWith("[") && UIDisplay.INSTANCE.getUserLevelId() >= 4 ) {
						String[] parts = address.split("]:")
						if( parts.size() == 2 ) {
							address = parts[0].substring(1)
						}
					}

					if( !( InetAddresses.isInetAddress( address ) || address.matches('[a-zA-Z0-9_.-]+') ) ) {
						failResults += new ValidationResult("Badly formatted gateway address '$address'.", [cmodel.getOwner(o)])
					}else{
						if(! gatewayAddresses.containsKey(address))
							gatewayAddresses[address] = []
						gatewayAddresses[address] += o
					}
				}else if(protocol.equals("COM")){
					if(!address.matches('[0-9]+')){
						failResults += new ValidationResult("Badly formatted gateway address '$address'.", [cmodel.getOwner(o)])
					}else{
						if(! gatewayAddresses.containsKey(address))
							gatewayAddresses[address] = []
						gatewayAddresses[address] += o
					}
				}else{
					failResults += new ValidationResult("Badly formatted protocol '$protocol'.", [cmodel.getOwner(o)])
				}
			}
			
			String securityMode = o.getObjectProperty('securityMode').getValue()
			String key = o.getObjectProperty('key').getValue()
			if(!((securityMode.toUpperCase().equals("AES-CFB-PSK") && key.matches('[a-fA-FX0-9]{32}')) || (securityMode.toLowerCase().equals("none") && (key ==null || key.equals(""))))
			){
				failResults += new ValidationResult("Security mode and key doesn't match.", [cmodel.getOwner(o)])
			}
		}

		gatewayAddresses.each{ address, l ->
			def clist = l.collect{it -> cmodel.getOwner(it)}.unique()
			if(l.size() > 1)
				failResults += new ValidationResult("Multiple gateway objects with the same address '$address'.", clist)
		}
	}

	private void wsnNetworkSize() {
		omodel.getObjectsByType('wsnnetwork').each{ o ->
			int numids = o.getObjectProperty('nodes').getChildIds().size()
			if(numids == 0) {
				failResults += new ValidationResult("Network is empty.", [cmodel.getOwner(o)])
			} else if(numids > 2000) {
				// Make it a warning if we are in technician mode, just in case we want to live life on the edge!
				def foo = new ValidationResult("$numids exceeds the maximum number of nodes (2000) per network.", [cmodel.getOwner(o)])
				if( UIDisplay.INSTANCE.getUserLevelId() < 4 ) {
					failResults += foo
				} else {
					warnResults += foo
				}
			} else if(numids >= 1900) {
				warnResults += new ValidationResult("$numids is approaching the maximum number (2000) of nodes in a network.", [cmodel.getOwner(o)])
			}
			int gatewayNumids = o.getObjectProperty('gateways').getChildIds().size()
			
			if(gatewayNumids == 0)
				failResults += new ValidationResult("Gateway is required.", [cmodel.getOwner(o)])

			//check for # gateways vs # nodes
			//we want total packets per hour
			int totalpackets = 0
			o.getObjectProperty("nodes").getChildren().each{ n ->
				int nodepackets = 0
				log.trace("platform id is ${n.getObjectProperty("platformId")}")
				if(RADIO_PACKETS.containsKey(n.getObjectProperty("platformId").getValue())){
					nodepackets += RADIO_PACKETS[n.getObjectProperty("platformId").getValue()]
				}else {
					nodepackets += 1
				}
				//now, packets per hour // min
				double sampleinterval = Double.parseDouble(n.getObjectProperty("period").getValue().replace(" min", ""))
				double reportsPerHour = 60.0 / sampleinterval
				log.trace("$n has base $nodepackets, sampleinterval of $sampleinterval, $reportsPerHour per hour")
				totalpackets += (reportsPerHour * nodepackets)
			}

			log.trace("network $o has $totalpackets packets")

			int packetsPerGW = 0
			if(gatewayNumids > 0){
				packetsPerGW = totalpackets / gatewayNumids
			}

			log.trace("network $o has $packetsPerGW packets per gw")

			double gwNeeded = totalpackets / 2400
			log.trace("network $o should have $gwNeeded gateways")
			int gwInt = Math.ceil(gwNeeded)

			if(packetsPerGW> 2400){
				warnResults += new ValidationResult( String.format(CentralCatalogue.getUIS("validator.wsnNetworkSize.notEnoughGateways"), gwInt), [cmodel.getOwner(o)])
			}
		}
	}

	private void wsnSensepointRanges() {
		for(DLObject node : omodel.getObjectsByType('wsnnode')){
			for(DLObject sensepoint: node.getObjectProperty("sensepoints").getChildren()) {
				Double amin = sensepoint.getObjectProperty("aMin").getValue()
				Double amax = sensepoint.getObjectProperty("aMax").getValue()
				Double rmin = sensepoint.getObjectProperty("rMin").getValue()
				Double rmax = sensepoint.getObjectProperty("rMax").getValue()
				Double dmin = sensepoint.getObjectProperty("min").getValue()
				Double dmax = sensepoint.getObjectProperty("max").getValue()
				if(amin != null && amax != null && rmin != null && rmax != null) {
					if(rmin < amin || amin < dmin || rmax > amax || amax > dmax) {
						failResults += new ValidationResult("Sensor ranges configured incorrectly.  See user guide for configuration guidelines.", [cmodel.getOwner(node)])
						break // only 1 message per node
					}
				}
			}
		}
	}

	private void validAltitude() {
		for( DLObject drawing : omodel.getObjectsByType( ObjectType.DRAWING ) ) {
			Double altitude = drawing.getObjectSetting('altitude').getValue()

			if( altitude == null || altitude <= -2000 ) {
				// No altitude, so make sure we don't have a pressure sensor (type 29)  since it requires altitude in the DM conversion.
				for( DLObject child : drawing.getDescendents() ) {
					if( child.getType() == 'wsnsensor' && child.getObjectSetting('type').getValue() == 29 ) {
						failResults += new ValidationResult("Pressure nodes require a valid Data Center Altitude value.",
						                                    [cmodel.getOwner(drawing)])
						break
					}
				}
			} else if( altitude < -1000 ) {
				// Silly safety net in case the DC is on the Dead Sea since other places in the system do "< -1000"
				// checks for error codes, so an altitude below this would not behave correctly.
				failResults += new ValidationResult("The system does not support an altitude below -1000 ft.",
				                                    [cmodel.getOwner(drawing)])
			}

			// HAXXERZ: DM needs altitude in the network, not DC. Since it is hidden and only needed before export, just
			// slug it in here rather than a special handler to do so live as the DC altitude is changed. I feel so dirty.
			log.debug("Propagating '${drawing.getName()}' altitude '$altitude' to the networks.")
			for( DLObject child : drawing.getChildren() ) {
				if( child.getType() == 'wsnnetwork' ) {
					child.getObjectSetting('altitude').setValue( altitude )
				}
			}
		}
	}

	private void requiredAssociations() {
		for(def component : cmodel.getComponents()){
			// Skip proxies since they are checked only when the anchor is missing the association.
			if( component.hasRole("proxy") ) {
				continue
			}

			for(String consumerId: component.listConsumers()) {
				Consumer consumer = component.getConsumer(consumerId)
				if( consumer.isRequired() ) {
					if( consumer.listProducers().isEmpty() ) {
						// Check our proxies before we fail.
						boolean hasAssoc = false
						for( DLComponent proxy : component.getProxies() ) {
							if( proxy.getConsumer(consumerId).listProducers().size() > 0 ) {
								hasAssoc = true
								break
							}
						}

						if( !hasAssoc ) {
							failResults += new ValidationResult("Missing association '${consumer.getName()}'.", [component])
						}
					}
				}
			}
			//now, required conducers as well
			for(Conducer con : component.getAllConducers()){
				if( con.isRequired() ) {
					if( con.getProsumers().isEmpty() ) {
						// Check our proxies before we fail.
						boolean hasAssoc = false
						for( DLComponent proxy : component.getProxies() ) {
							if( proxy.getConducer( con.getId() ).getProsumers().size() > 0 ) {
								hasAssoc = true
								break
							}
						}

						if( !hasAssoc ) {
							failResults += new ValidationResult("Missing association '${con.getName()}'.", [component])
						}
					}
				}
			}

		}
	}

	private void modbusResponseTimeout() {
		omodel.getObjectsByType('modbusnetwork').each{ device ->
			Integer responseTimeout = device.getObjectProperty('timeout').getValue()
			if (responseTimeout<1 || responseTimeout>300000) {
				failResults += new ValidationResult(uiStrings.getProperty("validator.modbusResponseTimeout"), [cmodel.getOwner(device)])
			}
		}
	}

	private void modbusRetries() {
		omodel.getObjectsByType('modbusnetwork').each{ device ->
			DLComponent component = cmodel.getOwner(device)
			Integer retries = component.getComponentProperty('retries').getValue()
			String displayName = component.getPropertyDisplayName("retries")
			if (retries<0) {
				failResults += new ValidationResult(String.format(CentralCatalogue.getUIS('validator.modbusRetriesInvalid'), displayName), [component])
			}
		}
	}

	private void modbusAddress(){
		Map<String, List<DLComponent>> modbusIPs = [:]
		omodel.getObjectsByType('modbusnetwork').each{ modbus ->
			String port = modbus.getObjectProperty('port').getValue()
			if(port == ''){
				//need to issue the right error based on type
				if ( cmodel.getOwner( modbus ).getType().contains("rtu")  ) {
					failResults += new ValidationResult(uiStrings.getProperty("validator.modbusAddress.noPort.COM"), [cmodel.getOwner(modbus)])
				} else {
					failResults += new ValidationResult(uiStrings.getProperty("validator.modbusAddress.noPort.IP"), [cmodel.getOwner(modbus)])
				}
				
			}else{
				String tcpRegex = 'TCP:[a-zA-Z0-9_.-:]+'
				if (!(port.matches('COM[0-9]+') || port.matches(tcpRegex))) {
					if (cmodel.getOwner(modbus).getType().contains("rtu")) {
						failResults += new ValidationResult(uiStrings.getProperty("validator.modbusAddress.badPort.COM") + " '$port'", [cmodel.getOwner(modbus)])
					} else {
						failResults += new ValidationResult(uiStrings.getProperty("validator.modbusAddress.badPort.IP") + " '$port'", [cmodel.getOwner(modbus)])
					}
				} else if (port.matches(tcpRegex)) {
					def ip = port.split(':', 2)
					validateIPAddress(ip[1], cmodel.getOwner(modbus))
				}

				//also check network IPs for dupes
				MapHelper.addList(modbusIPs, port, cmodel.getOwner(modbus))
			}
		} //end each network

		//and grab the wsn ips for a little side checking
		def wsnGateways = [:]
		omodel.getObjectsByType('wsngateway').each{ o ->
			String address = "TCP:" + o.getObjectProperty('address').getValue()
			MapHelper.addList(wsnGateways, address, cmodel.getOwner(o) )
		}

		modbusIPs.each{ k, v ->
			if ( v.size() > 1 || wsnGateways.containsKey(k) ){

				if ( wsnGateways.containsKey(k) ){
					warnResults += new ValidationResult( String.format( uiStrings.getProperty("validator.dupeIP"), k ) , v + wsnGateways[k])
				} else {
					warnResults += new ValidationResult( String.format( uiStrings.getProperty("validator.dupeIP"), k ) , v)
				}
			}
		}

	}

	/**
	 * Validate the given ip address to check if it is in the valid format. Checks both IPv4 and IPv6 addresses
	 * @param ip
	 * @param component
     */
	private void validateIPAddress(String ip, def component) {
		if( !InetAddresses.isInetAddress( ip.toString().trim() ) ) {
			failResults += new ValidationResult(uiStrings.getProperty("validator.controlcomponents.validIP"), component)
		}
	}

	private void uniqueModbusDevice() {

        def registers = [:] //HashMap<String, List<DLObject>>()

		omodel.getObjectsByType('modbusnetwork').each{ modbus ->
			def ids = [:]
			modbus.getObjectProperty('devices').getChildren().each{ device ->
				if ( device.hasObjectProperty('id') ) { //otherwise, we won't bother to look, since it's not a "real" modbus device

					String id = device.getObjectProperty('id').getValue()
					if(! ids.containsKey(id)) {
						ids[id] = []
					}
					ids[id] += device

					if(id==null || id=='' || !id.isInteger() || Integer.parseInt(id) < 1 || Integer.parseInt(id) > 247 ){
						failResults += new ValidationResult("Modbus ID must be an integer between 1 and 247 inclusive.", [cmodel.getOwner(device)])
					}

                    //warn same net + id + register combo
                    device.getObjectProperty('registers').getChildren().each{ modProp ->
                        if ( modProp.getObjectProperty('id').getValue() > 0 ) {
                            String idAndReg = "${modbus.getObjectProperty('port').getValue()}::${device.getObjectProperty('id').getValue()}::${modProp.getObjectProperty('msw').getValue()}:${modProp.getObjectProperty('id').getValue()}"
                            MapHelper.addList(registers, idAndReg, device)
                        }
                    }
				}
			}
/*
			ids.each{ id, l ->
				def clist = l.collect{it -> cmodel.getOwner(it)}.unique()
				//fail if more than 1 device has the same modbus ID.
				//But, only if that id is *valid* - so we check to make sure it's an integer between 1 and 247.
				//And also that it isn't null.
				if(l.size() > 1 && id && id.isInteger() && (Integer.parseInt(id) >0 && Integer.parseInt(id) <= 247)  )
					failResults += new ValidationResult("Cannot have more than one Modbus Device id '$id'", clist)
			}
*/
		}

		//flatten the list so we only warn on component pairs, not each address+register pair
		List devicesToWarnOn = []
		registers.each{ idAndRegister, devices ->
			if ( devices.size() > 1 ){
				List comps = devices.collect { cmodel.getOwner(it) }
				//warnResults += new ValidationResult("Components using the Modbus ID + register combination.", comps)
				if ( ! devicesToWarnOn.contains( comps ) ){
					devicesToWarnOn.add( comps )
				}
			}
		}
		devicesToWarnOn.each{ l ->
			warnResults += new ValidationResult( CentralCatalogue.getUIS("validator.modbus.dupeCombo") , l)
		}
	}

	private void uniqueModbusDeviceName(){
		def names = [:]
		omodel.getObjectsByType('modbusdevice').each{ device ->
			String name = device.getObjectProperty('name').getValue()
			if(! names.containsKey(name))
				names[name] = []
			names[name] += device
		}

		names.each{ name, devices ->
			if(devices.size() > 1)
				failResults += new ValidationResult("Cannot have more than one Modbus device named '$name'.", devices.collect{cmodel.getOwner(it)})
		}
	}

	private void modbusNetworkSize(){
		omodel.getObjectsByType('modbusnetwork').each{ modbus ->
			//if( modbus.getObjectProperty('devices').getChildren().size() < 1 ){
			if( modbus.getObjectProperty('devices').getChildrenOfType("modbusdevice").size() < 1 ){
				failResults += new ValidationResult("Modbus Network must have a least 1 device.", [cmodel.getOwner(modbus)])
			}
		}
	}

	private void constellation2() {
		omodel.getObjectsByType('wsnnode').each{ node ->
			//if(node.getObjectProperty('platformId').getValue() == 15 || node.getObjectProperty('platformId').getValue() == 16) {
			if( node.getObjectProperty('platformId').getValue() >= 15  ){
				def network = node.getParents().find{it.type == 'wsnnetwork'}
				if(network.getObjectProperty('version').getValue() != 'N29')
					failResults += new ValidationResult("Component only supported in an N29 network.", [cmodel.getOwner(node)])
			}
		}
	}

	private void uniquePdiPduNames() {
		def names = [:]
		omodel.getObjectsByType('pdipdu').each{ pdipdu ->
			String name = pdipdu.getObjectProperty('name').getValue()
			if(! names.containsKey(name)) {
				names[name] = []
			}
			names[name] += pdipdu
		}

		names.each{ name, l ->
			def clist = l.collect{it -> cmodel.getOwner(it)}.unique()
			if(l.size() > 1)
				failResults += new ValidationResult("Cannot have more than one PDI PDU named '$name'.", clist)
		}
	}

	private void pdiPduAssociations() {
		def panels = [:]
		def panelNames = ["panel1", "panel2", "panel3", "panel4"]

		omodel.getObjectsByType('panel').each{ panel ->
			panels[panel] = []
		}

		omodel.getObjectsByType('pdipdu').each{ pdipdu ->
			panelNames.each{ panelName ->
				def panel = pdipdu.getObjectProperty(panelName).getReferrent()
				if(panel != null) {
					panels[panel] += pdipdu
				}
			}
			if(panelNames.every{pdipdu.getObjectProperty(it).getReferrent() == null}) {
				warnResults += new ValidationResult("PDU with no panels.", [cmodel.getOwner(pdipdu)])
			}
		}

		panels.each{ panel, l ->
			if(l.size() > 1)
				failResults += new ValidationResult("Panel bound more than once.", [cmodel.getOwner(panel)])
		}
	}

	private void modbusDevicePullPeriods(){
		omodel.getObjectsByType('modbusdevice').each{o->
			def pullPeriod = cmodel.getOwner(o).getPropertyValue('pullPeriod')
			if( pullPeriod>=16){
				warnResults += new ValidationResult("Setting Sampling Interval to greater than 15 can cause network instabilities.", [cmodel.getOwner(o)])
			}else if(pullPeriod<=2){
				failResults += new ValidationResult("The sampling interval must never be less than 3 minutes.", [cmodel.getOwner(o)])
				
			}
		}
	}
	
	private void ionMeters(){
		def ions = cmodel.getNonProxyComponentsByType('ion6200-wye') + cmodel.getNonProxyComponentsByType('ion6200-delta')
		ions.each{c->
			def max_v = c.getPropertyValue('max_voltage')
			def lower_v_warning = c.getPropertyValue('lower_voltage_warning_threshold')
			def upper_v_warning = c.getPropertyValue('upper_voltage_warning_threshold')
			def lower_v_danger = c.getPropertyValue('lower_voltage_danger_threshold')
			def upper_v_danger = c.getPropertyValue('upper_voltage_danger_threshold')
			def max_c = c.getPropertyValue('max_current')
			def lower_c_warning = c.getPropertyValue('lower_current_warning_threshold')
			def upper_c_warning = c.getPropertyValue('upper_current_warning_threshold')
			def lower_c_danger = c.getPropertyValue('lower_current_danger_threshold')
			def upper_c_danger = c.getPropertyValue('upper_current_danger_threshold')
			
			if(lower_v_warning<0)
				failResults += new ValidationResult("Lower Voltage Warning Threshold must be greater than or equal to 0 V.", c)
			if(upper_v_warning<0)
				failResults += new ValidationResult("Upper Voltage Warning Threshold must be greater than or equal to 0 V.", c)
			if(lower_v_danger<0)
				failResults += new ValidationResult("Lower Voltage Danger Threshold must be greater than or equal to 0 V.", c)
			if(upper_v_danger<0)
				failResults += new ValidationResult("Upper Voltage Danger Threshold must be greater than or equal to 0 V.", c)
			if(max_v<=0)
				failResults += new ValidationResult("The Maximum Voltage configured must be greater than 0 V.", c)

			if(lower_c_warning<0)
				failResults += new ValidationResult("Lower Current Warning Threshold must be greater than or equal to 0 A.", c)
			if(upper_c_warning<0)
				failResults += new ValidationResult("Upper Current Warning Threshold must be greater than or equal to 0 A.", c)
			if(lower_c_danger<0)
				failResults += new ValidationResult("Lower Current Danger Threshold must be greater than or equal to 0 A.", c)
			if(upper_c_danger<0)
				failResults += new ValidationResult("Upper Current Danger Threshold must be greater than or equal to 0 A.", c)
			if(max_c<=0)
				failResults += new ValidationResult("The Maximum Current configured must be greater than 0 A.", c)
			
			if(upper_v_danger>max_v)
				failResults += new ValidationResult("The Upper Voltage Danger Threshold  must be less than or equal to Maximum Voltage.", c)
			
			if(lower_v_danger>=upper_v_danger)
				failResults += new ValidationResult("The Lower Voltage Danger Threshold must be less than the Upper Voltage Danger Threshold.", c)

			if(lower_v_warning< lower_v_danger)
				failResults += new ValidationResult("The Lower Voltage Warning Threshold must be greater than or equal to the Lower Voltage Danger Threshold.", c)
		
			if(upper_v_warning< lower_v_warning)
				failResults += new ValidationResult("The Upper Voltage Warning Threshold must be greater than or equal to the Lower Voltage Warning Threshold.", c)
		
			if(upper_v_warning > upper_v_danger)
				failResults += new ValidationResult("The Upper Voltage Warning Threshold must be less than or equal to the Upper Voltage Danger Threshold.", c)
				
			if(upper_c_danger > max_c)
				failResults += new ValidationResult("The Upper Current Danger Threshold  must be less than or equal to Maximum Current.", c)

			if(lower_c_danger>= upper_c_danger)
				failResults += new ValidationResult("The Lower Current Danger Threshold must be less than the Upper Current Danger Threshold.", c)
			
			if(lower_c_warning < lower_c_danger)
				failResults += new ValidationResult("The Lower Current warning threshold must be greater than or equal to the Lower Current Danger Threshold.", c)
			
			if(upper_c_warning > upper_c_danger)	
				failResults += new ValidationResult("The Upper Current Warning Threshold must be less than or equal to the Upper Current Danger Threshold.", c)
			
			if(lower_c_warning >= upper_c_warning)
				failResults += new ValidationResult("The Lower Current Warning Threshold must be less than the Upper Current Warning Threshold.", c)
		}
	}


	private void tableFitPoints(){
		omodel.getObjectsByType('tablefit').each{ o ->
			String currentTable = o.getObjectProperty('table').getValue()
			Integer points = ChartContents.numberOfPoints(currentTable)
			if ( points < 2 ) {
				DLComponent c = cmodel.getOwner( o )
				String componentName = c.getName()
				failResults += new ValidationResult("The table provided in $componentName must have at least two points.", c)
			}

		}
	}
	
	private void pueOperations(){
		def pueMainComponents = cmodel.getNonProxyComponentsByType('pue_main')
		pueMainComponents.each{c->
			if(c.getPropertyValue('itDesign')==0.0){
				failResults += new ValidationResult(  String.format( CentralCatalogue.getUIS("validator.general.notZero"), c.getComponentProperty('itDesign').getDisplayName() ), c)
			}

			if(c.getPropertyValue('baselinePue')==0.0){
				failResults += new ValidationResult(  String.format( CentralCatalogue.getUIS("validator.general.notZero"), c.getComponentProperty('baselinePue').getDisplayName() ), c)
			}
			
			if(c.getPropertyValue('coolingDesign')==0.0){
				failResults += new ValidationResult(  String.format( CentralCatalogue.getUIS("validator.general.notZero"), c.getComponentProperty('coolingDesign').getDisplayName() ), c)
			}

			if(c.getPropertyValue('emissionFactor')==0.0){
				failResults += new ValidationResult(  String.format( CentralCatalogue.getUIS("validator.general.notZero"), c.getComponentProperty('emissionFactor').getDisplayName() ), c)
			}
		}

		def pueLiteComponents = cmodel.getNonProxyComponentsByType('pue_lite')
		pueLiteComponents.each{c->
			if(c.getPropertyValue('itDesign')==0.0){
				failResults += new ValidationResult(  String.format( CentralCatalogue.getUIS("validator.general.notZero"), c.getComponentProperty('itDesign').getDisplayName() ), c)
			}
			if(c.getPropertyValue('baselinePue')==0.0){
				failResults += new ValidationResult(  String.format( CentralCatalogue.getUIS("validator.general.notZero"), c.getComponentProperty('baselinePue').getDisplayName() ), c)
			}
			if(c.getPropertyValue('emissionFactor')==0.0){
				failResults += new ValidationResult(  String.format( CentralCatalogue.getUIS("validator.general.notZero"), c.getComponentProperty('emissionFactor').getDisplayName() ), c)
			}
		}

	}

	/**
	 * ES saves floating-point values as DOUBLE(15,6).
	 * What that means is that we only get 9 places left of the decimal point or we get an error.
	 * Any field that lets the user type in a floating-point value needs to get checked here.
	 *
	 * This problem doesn't exist for integral fields, as DL and ES are using the same bounds.
	 */
	private void preventHugeNumbers(){
		def MAXSIZE = 999999999.999999

		def userInputs = cmodel.getNonProxyComponentsByType('userinput')
		userInputs.each{ c->
			if( c.getPropertyValue('value') >MAXSIZE ) {
				failResults += new ValidationResult( String.format(uiStrings.getProperty("validator.preventHugeNumbers.general"), c.getPropertyDisplayName('value'), MAXSIZE)  , c)
			}
		}
		def pue = cmodel.getNonProxyComponentsByType('pue_main')
		pue.each{ c ->
		    if( c.getPropertyValue('itDesign') > MAXSIZE ) {
				failResults += new ValidationResult( String.format(uiStrings.getProperty("validator.preventHugeNumbers.general"), c.getPropertyDisplayName('itDesign'), MAXSIZE)  , c)
			}
			if( c.getPropertyValue('baselinePue') > MAXSIZE ) {
				failResults += new ValidationResult( String.format(uiStrings.getProperty("validator.preventHugeNumbers.general"), c.getPropertyDisplayName('baselinePue'), MAXSIZE)  , c)
			}
			if( c.getPropertyValue('coolingDesign') > MAXSIZE ) {
				failResults += new ValidationResult( String.format(uiStrings.getProperty("validator.preventHugeNumbers.general"), c.getPropertyDisplayName('coolingDesign'), MAXSIZE)  , c)
			}
			if( c.getPropertyValue('co2AbatedIndex') > MAXSIZE ) {
				failResults += new ValidationResult( String.format(uiStrings.getProperty("validator.preventHugeNumbers.general"), c.getPropertyDisplayName('co2AbatedIndex'), MAXSIZE)  , c)
			}
		}

		//same applies to all width/depth/length fields
		omodel.getObjects().each{ o ->
			if ( o.hasObjectProperty("width") ){
				if ( o.getObjectProperty("width").getValue() > MAXSIZE ){
					def c = cmodel.getOwner(o)
                    failResults += new ValidationResult( String.format(uiStrings.getProperty("validator.preventHugeNumbers.dimension"),
                                                                       Localizer.convert( MAXSIZE, 'distance')), c)
				}
			}
			if ( o.hasObjectProperty("depth") ){
				if ( o.getObjectProperty("depth").getValue() > MAXSIZE ){
					def c = cmodel.getOwner(o)
                    failResults += new ValidationResult( String.format(uiStrings.getProperty("validator.preventHugeNumbers.dimension"),
                                                                       Localizer.convert( MAXSIZE, 'distance')), c)
				}
			}
		}
		
	}

	/**
	 * As mentioned in the comments for preventHugeNumbers(), ES stores floating point values as DOUBLE(15,6).
	 * That means, among other things, that all double values will be rounded to six places after the decimal point at export-time.
	 * However, it used to be (15,3), and we're still going to check for three places rather than 6. 
	 * While this might be a problem, this is probably generally okay.  So, we warn rather than fail.
	 */
	private void warnHugeDecimals(){
		def userInputs = cmodel.getNonProxyComponentsByType('userinput')
		userInputs.each{ c->
			double val = c.getPropertyValue('value')
			if ( getPlaces(val) > 3 ){
				warnResults += new ValidationResult("User Entered Values with more than three places after the decimal point will be rounded to three places during export.", c)
			}
		}
		def pue = cmodel.getNonProxyComponentsByType('pue_main')
		pue.each{ c ->
			if ( getPlaces(c.getPropertyValue('itDesign')) > 3 ){
				warnResults += new ValidationResult("IT Power Design Limit has more than three places after the decimal point will be rounded to three places during export.", c)
			}
			if ( getPlaces(c.getPropertyValue('baselinePue')) > 3 ){
				warnResults += new ValidationResult("Baseline PUE has more than three places after the decimal point will be rounded to three places during export.", c)
			}
			if ( getPlaces(c.getPropertyValue('coolingDesign')) > 3 ){
				warnResults += new ValidationResult("Cooling Power Design Limit has more than three places after the decimal point will be rounded to three places during export.", c)
			}
			/*
			if ( getPlaces(c.getPropertyValue('co2AbatedIndex')) > 3 ){
				warnResults += new ValidationResult("CO2 Abated Index has more than three places after the decimal point will be rounded to three places during export.", c)
			}
			*/
		}

	}

	/**
	 * Helper function for warnHugeDecimals() to guess the number of places the user entered.
	 */
	private static int getPlaces( double source ){
		int places = 0
		String valS = source.toString().trim()
		if ( valS.contains(".") ) {
			places = valS.substring( valS.indexOf(".") ).length() - 1 //-1 to remove the "."
		}
		return ( places )
	}

	/**
	* Ensures there is only one PUE Reporter Object (contained in any Component type) in the Dl.
	*/
	private void onlyOnePue(){
        for( DLComponent drawing : cmodel.getComponentsInRole('drawing') ) {
            List<DLObject> pues = drawing.getChildObjectsOfType('pue')
            if ( pues.size() > 1 ) {
                def c = pues.collect {cmodel.getOwner(it)}
                failResults += new ValidationResult("A Drawing cannot have more than one PUE Reporter.", c)
            }
        }
	}


	/**
	 * Checks to make sure the user has filled out the 3-phase current to kW component correctly.
	 */
	private void threePhase(){
		def p3 = cmodel.getNonProxyComponentsByType('3PhaseTokW')
		p3.each{ c ->
		    def pf = c.getPropertyValue('pf')
			if ( pf < 0 || pf > 1.0 ) {
				failResults += new ValidationResult("Power Factor must be between 0.0 and 1.0.", c)
			}
			def volts = c.getPropertyValue('volts')
			if ( !volts || volts <= 0.0 ) {
				failResults += new ValidationResult("Volts must be a number greater than 0.0.", c)
			}
		}
	}

	final int coordLimit = 63360  // 1 mile in inches
	private void limitCoordinates() {
		def offenders = []

		omodel.getObjectsByType('wsnnode').each{ node ->
			double x = node.getObjectProperty('x').getValue()
			double y = node.getObjectProperty('y').getValue()
			if(x > coordLimit || y > coordLimit) {
				offenders += cmodel.getOwner(node)
				return // closure return
			}
			node.getObjectProperty('sensepoints').getChildren().each{ sensepoint ->
				x = sensepoint.getObjectProperty('x').getValue()
				y = sensepoint.getObjectProperty('y').getValue()
				if(x > coordLimit || y > coordLimit) {
					offenders += cmodel.getOwner(node)
					return // closure return
				}
			}
		}

		if(offenders.size() > 0)
            failResults += new ValidationResult( String.format( uiStrings.getProperty('validator.limitCoordinates.tooHigh'),
                                                                (int) Localizer.convert( coordLimit, 'distance') ), offenders )
	}

	/**
	 * Check that all "placeable" components are on the drawing.
	 * Also check that all sensor x,y locations are on the drawing.
	 * Specifically, this is intended to check for horizontal row sensors that are hanging off the edge, but
	 * this will also cover any other case where things are near the border (rack too close to the wall, etc.)
	 */
	private void objectsWithinBounds() {
		def offenders = []

        // drawing -> ( BG Width, BG Height )
        Map<DLComponent,Pair<Double,Double>> imageSizes = [:]

        // Calculate the background size for each drawing.
        for( DLComponent drawing : cmodel.getComponentsInRole('drawing') ) {
            DLImage image = (DLImage) drawing.getPropertyValue("image_data")
            if( image != null ) {
                double inchesPerPixel = (Double) drawing.getPropertyValue("scale");
                double imgWidth = image.getWidth() * inchesPerPixel
                double imgHeight = image.getHeight() * inchesPerPixel

                imageSizes.put( drawing, new Pair( imgWidth, imgHeight ) )
            }
        }

		cmodel.getComponentsInRole('placeable').each{ p ->
            Pair imageSize = imageSizes.get( p.getDrawing() )
            if( imageSize != null ) {
                double x = p.getPropertyValue('x')
                double y = p.getPropertyValue('y')
                if( (x > imageSize.a) || (y > imageSize.b) || (x < 0) || (y < 0) ) {
                    offenders += p
                    return // closure return
                }
			}
		}

		if(offenders.size() > 0) {
			failResults += new ValidationResult(uiStrings.getProperty("validator.objectsWithinBounds.components"), offenders)
		}

		//do it again for sensors, but just as a warning:
		def outboardSensors = []
		omodel.getObjectsByType("wsnsensor").each{ o ->
            DLComponent component = cmodel.getManagingComponent(o)  //getOwner(o.getParents().first())
            Pair imageSize = imageSizes.get( component.getDrawing() )

            if( imageSize != null ) {
                Double x = ((DLObject)o).getObjectProperty("x").getValue()
                Double y = ((DLObject)o).getObjectProperty("y").getValue()
                //def sensorname = ((DLObject)o).getObjectProperty("name").getValue()
                //println "checking sensor $sensorname ($x, $y) vs $imgWidth, $imgHeight"
                if(x > imageSize.a || y > imageSize.b || x < 0 || y < 0) {
                    log.trace("${((DLObject)o).getObjectProperty("name").getValue()} sensor bounds fail on $x, $y vs ${imageSize.a}, ${imageSize.b}")
                    outboardSensors += component
                }
            }
		}
		outboardSensors = outboardSensors.unique()
		if(outboardSensors.size() > 0) {
			warnResults += new ValidationResult(uiStrings.getProperty("validator.objectsWithinBounds.sensors"), outboardSensors)
		}

	}

	private void checkCycles() {
		CycleChecker c = new CycleChecker(cmodel);
		c.findCycles().each{ cycle ->
			failResults += new ValidationResult(CentralCatalogue.getUIS("validator.checkCycles.cycleDetected"), cycle)
		}

		c.findInletOutletConnections().each{ io ->
			failResults += new ValidationResult( CentralCatalogue.getUIS("validator.checkCycles.inletOutletConnected") , io)
		}
	}

	/** 
	 * Checks unique SNMP agents name and address property
	 */
	 private void SNMPAgent() {
		def names = [:]
		omodel.getObjectsByType('snmpagent').each{ o ->
			String name = o.getObjectProperty('name').getValue()
			String address = cmodel.getOwner(o).getPropertyValue('address')
			int port = (int) cmodel.getOwner(o).getPropertyValue('port')
			
			if( address=='' || address.toLowerCase() == 'udp:' || address.toLowerCase() == 'tcp:' ){
				failResults += new ValidationResult("IP Address is required.", [cmodel.getOwner(o)])
			} else if( !address.toLowerCase().startsWith('udp:') && !address.toLowerCase().startsWith('tcp:') ){
				failResults += new ValidationResult("Badly formatted IP Address. Must begin with 'UDP:' or 'TCP:' followed by the address.", [cmodel.getOwner(o)])
			}
			
			if(port <1 || port >65535){
				failResults += new ValidationResult("Port must be between 1 and 65535.", [cmodel.getOwner(o)])
			}
			
			if(! names.containsKey(name)) {
				names[name] = []
			}
			names[name] += o
		}
	
		names.each{ name, l ->
			def clist = l.collect{it -> cmodel.getOwner(it)}.unique()
			if(l.size() > 1)
				failResults += new ValidationResult("Cannot have more than one SNMP Agent named '$name'.", clist)
		}
	}
	/**
	* Check SNMP V1 agent
	*/
	private void SNMPAgentV1(){
		cmodel.getComponentsByType('snmp-v1-agent').each{ c ->
			String privateCommunity = c.getPropertyValue('privateCommunity')
			if(privateCommunity==''){
				failResults += new ValidationResult("Private Community is required.", [c])
			}
		}
	}
	
	/**
	* Check SNMP V2c agent
	*/
	private void SNMPAgentV2c(){
		cmodel.getComponentsByType('snmp-v2c-agent').each{ c ->
			String privateCommunity = c.getPropertyValue('privateCommunity')
			String publicCommunity = c.getPropertyValue('publicCommunity')
			if(privateCommunity == ''){
				failResults += new ValidationResult("Private Community is required.", [c])
			}
			
			if(publicCommunity == ''){
				failResults += new ValidationResult("Public Community is required.", [c])
			}
		}
	}
	
	/**
	* Check SNMP V3 agent
	*/
	private void SNMPAgentV3(){
		cmodel.getComponentsByType('snmp-v3-agent').each{ c ->

			if( c.getPropertyValue('securityName') == '' ) {
				failResults += new ValidationResult("Security Name is required.", [c])
			}

			switch( c.getPropertyValue('secLevel') ) {
				case 'authpriv' :
					if( c.getPropertyValue('privPwd') == '' ) {
						failResults += new ValidationResult("Private Password is required.", [c])
					} else {
						String privPwd = c.getPropertyValue('privPwd');
						if (privPwd.length() < 8) {
							failResults += new ValidationResult("Private Password must be at least 8 characters long.", [c])
						}
					}
					// Intentional fall-through

				case 'auth' :
					if( c.getPropertyValue('authPasswd') == '' ) {
						failResults += new ValidationResult("Auth Password is required.", [c])
					} else {
						String authPasswd = c.getPropertyValue('authPasswd');
						if (authPasswd.length() < 8) {
							failResults += new ValidationResult("Auth Password must be at least 8 characters long.", [c])
						}
					}
			}
		}
	}

	private void controlDeviceConfigCheck() {
		def id  = [:] //ip+devid          : [{component,parcel}]
		def sid = [:] //ip+devid+sitelink : [{component,parcel}]
		def nid = [:] //ip+network+devid  : [{component,parcel}]

		for( DLComponent c : cmodel.getNonProxyComponentsInRole( ComponentRole.CONTROL_DEVICE ) ) {
			// Do the checks within the context of each parcel. The null entry will cover non-parcel core properties.
			Set<ComponentParcel> parcels = [null] as Set
			parcels += c.getEnabledParcelsInRole( ComponentRole.CONTROL_DEVICE )

			for( ComponentParcel parcel : parcels ) {
				// Skip ones that don't have the IP,Device properties
				if( !c.hasProperty("ip", parcel ) ) continue

				String ip = c.getPropertyStringValue("ip", parcel).trim()
				if( ip.length() == 0 || ip.equals("0") ) {
					failResults += new ValidationResult(uiStrings.getProperty("validator.controlcomponents.emptyIP"),  ( parcel ? parcel : c ))
				} else if( !InetAddresses.isInetAddress( ip.toString().trim() ) ) {
					failResults += new ValidationResult(uiStrings.getProperty("validator.controlcomponents.validIP"), ( parcel ? parcel : c ))
				}

				Integer devid
				Integer network
				if( c.hasManagedObjectOfType( ObjectType.CONTROLPROTO_BACNET, parcel ) ) {
					devid = c.getPropertyValue('device', parcel)
					if( ( devid < 0 || devid > 4194303 ) ) {
						failResults += new ValidationResult( CentralCatalogue.formatUIS("validator.controlcomponents.devid", 0, 4194303 ), ( parcel ? parcel : c ))
					}

					network = c.getPropertyValue('network', parcel)
					if( ( network < 0 || network > 65535 ) ) {
						failResults += new ValidationResult( CentralCatalogue.formatUIS("validator.controlcomponents.network", 0, 65535 ), ( parcel ? parcel : c ))
					}
				} else if( c.hasManagedObjectOfType( ObjectType.CONTROLPROTO_MODBUS, parcel ) ){
					devid = c.getPropertyValue('devid', parcel)
					if( ( devid < 1 || devid > 247 ) ) {
						failResults += new ValidationResult(CentralCatalogue.formatUIS("validator.controlcomponents.devid", 1, 247 ), ( parcel ? parcel : c ))
					}
				}

				if( c.hasManagedObjectOfType( ObjectType.CONTROLCFG_LIEBERT, parcel ) ) {
					Integer sitelink = c.getPropertyValue('port', parcel)
					if ( sitelink < 1 || sitelink > 12 ) {
						failResults += new ValidationResult(uiStrings.getProperty("validator.controlcomponents.sitelink"), ( parcel ? parcel : c ))
					}

					// For the unique check later.
					MapHelper.addList( sid, "$ip:$devid:$sitelink", new Pair( c, parcel ) )
				} else if( network ) {
					MapHelper.addList( nid, "$ip:$network:$devid", new Pair( c, parcel ) )
				} else {
					MapHelper.addList( id, "$ip:$devid", new Pair( c, parcel ) )
				}
			}
		}

		genUniqueDeviceMessages( id, "validator.controlcomponents.ipDevid" )
		genUniqueDeviceMessages( sid, "validator.controlcomponents.ipDevidPort" )
		genUniqueDeviceMessages( nid, "validator.controlcomponents.ipDevidNetwork" )
	}

	private genUniqueDeviceMessages( Map idMap, String uistring ) {
		idMap.each{ k, v ->
		    if (v.size() > 1 ) {
			    def comps = []
				boolean isWarn = true
				for( Pair c : v ) {
					// Manual Expression devices are warnings. If any are not those (Liebert,ACH, etc.), it is an error.
					if( c.b ) {
						comps += c.b
						if( !c.b.hasRole( ComponentRole.MANUAL_EXPRESSION ) ) {
							isWarn = false
						}
					} else {
						comps += c.a
						if( !c.a.hasRole( ComponentRole.MANUAL_EXPRESSION ) ) {
							isWarn = false
						}
					}
				}
				if( isWarn ) {
					makeWarn( uistring, comps )
				} else {
					makeFail( uistring, comps )
				}
		    }
		}
	}

	private void modbusGateways(){
		def gatewayNames = [:]
		List<DLObject> nets = omodel.getObjectsByType('modbusnetwork')
		List<DLObject> mbgw = omodel.getObjectsByType('virtual_modbus_gateway')
		mbgw.each{ o ->
			def cg = cmodel.getOwner(o)
			def name = cg.getPropertyValue('name')
			MapHelper.addList(gatewayNames, name, cg)
		}

		if( nets.size() > mbgw.size() ){
			def c = nets.collect { cmodel.getOwner(it) }
			infoResults += new ValidationResult(uiStrings.getProperty("validator.modbusGateway.noGateway"), c)
		}
		gatewayNames.each{ name, l ->
			def clist = new HashSet(l).toList()
			if(clist.size() > 1)
				failResults += new ValidationResult(uiStrings.getProperty("validator.modbusGateway.uniquename") + " '$name'", clist)
		}
	}

	private void validateHorizontalRows(){
		def singleRows = cmodel.getNonProxyComponentsByType('single-horizontal-row-thermanode2')
		def dualRows = cmodel.getNonProxyComponentsByType('dual-horizontal-row-thermanode2')
		
		for(def r : singleRows){
			def offsetArrays = []
			offsetArrays.add((double) r.getPropertyValue('c3_offset'))
			offsetArrays.add((double) r.getPropertyValue('c4_offset'))
			offsetArrays.add((double) r.getPropertyValue('c5_offset'))
			offsetArrays.add((double) r.getPropertyValue('c6_offset'))
			offsetArrays.add((double) r.getPropertyValue('c7_offset'))
			offsetArrays.add((double) r.getPropertyValue('c8_offset'))

			def sortedArrays = offsetArrays.clone().sort()
			boolean outOfOrder = false
			for(int i=0;i<6;i++){
				if(offsetArrays.get(i)!=sortedArrays.get(i)){
					outOfOrder = true
				}
			}
			if(outOfOrder){
				makeWarn("validator.horizontalRows.channelsOutOfOrder", r)
			}

            int li_layer = r.getPropertyValue("li_layer")
            def channelChildren = r.getChildComponents()
			for(def child : channelChildren){
				boolean wrongAssociation = false
                //def wrong_association = []
                if(li_layer==CentralCatalogue.LI_LAYER_TOP){       // li layer top, should not have middle or bottom temperature coin
                    def bad = child.getProducerConsumers().findAll{pc-> pc.getType().equals("controlpoint_single_temperature_mid") || pc.getType().equals("controlpoint_single_temperature_bot")}
					if(bad.size() > 0){ wrongAssociation = true }
                }else if(li_layer==CentralCatalogue.LI_LAYER_MIDDLE) {     // li layer middle, should not have top or bottom temperature coin
                    def bad = child.getProducerConsumers().findAll{pc-> pc.getType().equals("controlpoint_single_temperature") || pc.getType().equals("controlpoint_single_temperature_bot")}
					if(bad.size() > 0){ wrongAssociation = true }
                }else if(li_layer==CentralCatalogue.LI_LAYER_BOTTOM){   // li layer bottom, should not have top or middle temperature coin
                    def bad = child.getProducerConsumers().findAll{pc-> pc.getType().equals("controlpoint_single_temperature_mid") || pc.getType().equals("controlpoint_single_temperature")}
					if(bad.size() > 0){ wrongAssociation = true }
                } else if(li_layer==CentralCatalogue.LI_LAYER_SUBFLOOR || li_layer==CentralCatalogue.LI_LAYER_NONE){
					if(child.getProducerConsumers().size()>0){
						wrongAssociation = true
					}
				}

                //if(wrong_association.size() >0)  {
				if(wrongAssociation){
                    Integer channel = (Integer)child.getPropertyValue("channel")
                    //failResults +=new ValidationResult("LI layer doesn't match temperature control input", child)
                    //failResults +=new ValidationResult("LI layer doesn't match temperature control input of channel $channel", r)
					failResults += new ValidationResult( CentralCatalogue.formatUIS("validator.horizontalRows.wrongLI", channel.toString()), r )

                }
            }

		}
		
		for(def r : dualRows){
			def offsetArrays = []
			offsetArrays.add((double) r.getPropertyValue('rack1_offset'))
			offsetArrays.add((double) r.getPropertyValue('rack2_offset'))
			offsetArrays.add((double) r.getPropertyValue('rack3_offset'))
			offsetArrays.add((double) r.getPropertyValue('rack4_offset'))
			offsetArrays.add((double) r.getPropertyValue('rack5_offset'))
			offsetArrays.add((double) r.getPropertyValue('rack6_offset'))
	
			def sortedArrays = offsetArrays.clone().sort()
			boolean outOfOrder = false
			for(int i=0;i<6;i++){
				if(offsetArrays.get(i)!=sortedArrays.get(i)){
					outOfOrder = true
				}
			}
			if(outOfOrder){
				makeWarn("validator.horizontalRows.racksOutOfOrder", r)
			}
		}
	}

	private void crahMetrics(){
		//warn if crahs don't have tables set
		for( DLComponent c : cmodel.getComponents() ) {
			def currentTable = c.getPropertyValue("btu_table")
			if( currentTable != null ) {
				Integer points = ChartContents.numberOfPoints(currentTable)
				if ( points < 2 ) {
					makeWarn( c, "validator.crahMetrics.smallTable.btu" )
				}
			}

			currentTable = c.getPropertyValue("cfm_table")
			if ( currentTable != null ){
				Integer points = ChartContents.numberOfPoints(currentTable)
				if ( points < 2 ) {
					// As of v7.0, this is required if the room is using a raised floor strategy.
					if( c.getRoom()?.getPropertyStringValue( RoomType._PROPERTY ) == RoomType.RAISED_FLOOR ) {
						makeFail( c, "validator.crahMetrics.smallTable.cfmWithCtrl" )
					} else {
						makeWarn( c, "validator.crahMetrics.smallTable.cfm" )
					}
				}
			}
		}
	}
	
	private void uniqueRackNames(){
		def names = [:]
		/*def dualRows = cmodel.getComponentsByType('dual-horizontal-row-thermanode2')
		dualRows.each{c->
			String rack1_name = c.getPropertyValue("rack1_name")
			String rack2_name = c.getPropertyValue("rack2_name")
			String rack3_name = c.getPropertyValue("rack3_name")
			String rack4_name = c.getPropertyValue("rack4_name")
			String rack5_name = c.getPropertyValue("rack5_name")
			String rack6_name = c.getPropertyValue("rack6_name")
			
			if(! names.containsKey(rack1_name)) {
				names[rack1_name] = []
			}

			if(! names.containsKey(rack2_name)) {
				names[rack2_name] = []
			}

			if(! names.containsKey(rack3_name)) {
				names[rack3_name] = []
			}

			if(! names.containsKey(rack4_name)) {
				names[rack4_name] = []
			}
	
			if(! names.containsKey(rack5_name)) {
				names[rack5_name] = []
			}
	
			if(! names.containsKey(rack6_name)) {
				names[rack6_name] = []
			}

			names[rack1_name] += c
			names[rack2_name] += c
			names[rack3_name] += c
			names[rack4_name] += c
			names[rack5_name] += c
			names[rack6_name] += c
		}
		*/
		omodel.getObjectsByType('rack').each{ o ->
			String name = o.getObjectProperty('name').getValue()
			if(! names.containsKey(name)) {
				names[name] = []
			}
			names[name] += o
		}

		names.each{ name, l ->
			def clist = l.collect{it -> cmodel.getOwner(it)}.unique()
			if(l.size() > 1)
				failResults += new ValidationResult("Cannot have more than one rack named '$name'.", clist)
		}
	}

	/**
	 * Validate the ES host name.
	 */
	private void hostName(){
		if ( projectSettings.esHost.trim().size() == 0 ){
			failResults += new ValidationResult(uiStrings.getProperty("validator.hostName.noName"), null)
		}
	}

	private void powerComponents(){

		//jawas must be connected to a SC
		def usedRacks = []
		omodel.getObjectsByType("wsnnode").each{ node ->
			Dockingpoint docking = node.getObjectProperty("centerRack")
			if ( docking ) {
				usedRacks += docking.getReferrent()
			}
			docking = node.getObjectProperty("rightRack")
			if ( docking ) {
				usedRacks += docking.getReferrent()
			}
			docking = node.getObjectProperty("leftRack")
			if ( docking ) {
				usedRacks += docking.getReferrent()
			}
		}
		omodel.getObjectsByType("power_rack").each{ pr ->
			//filter out non-jawas
			if ( cmodel.getOwner(pr).getType() == "j_rack"){
				if ( ! usedRacks.contains(pr) ){
					failResults += new ValidationResult(uiStrings.getProperty("validator.powerComponents.lonelyJawa"), cmodel.getOwner(pr) )
				}
			}

			def currentQuorum = pr.getObjectProperty("quorum").getValue()
			if( (currentQuorum <= 0 || currentQuorum > 100) && currentQuorum != null ){
				failResults += new ValidationResult(uiStrings.getProperty("validator.powerComponents.badQuorum"), cmodel.getOwner(pr) )
			}

		}

		//jawas only connected to one SC
		cmodel.getComponentsByType("j_rack").each{ j->
			def scs = j.getProducerConsumers("jawaRack")
			if ( scs.size() > 1 ){
				failResults += new ValidationResult(uiStrings.getProperty("validator.powerComponents.jawaMultipleSC"), j )
			} else if (scs.size() == 1) {
				//check for jawa more than 10 feet from SC
				def sc = scs.first()
				def jawaLoc = new Point2D.Double( (Double)j.getPropertyValue("x"), (Double)j.getPropertyValue("y") )
				def scLoc = new Point2D.Double( (Double)sc.getPropertyValue("x"), (Double)sc.getPropertyValue("y") )
                def dist = jawaLoc.distance(scLoc)
                if ( dist > JAWA_MAX_DIST_IN ){
                    warnResults += new ValidationResult(String.format(uiStrings.getProperty("validator.powerComponents.jawaTooFar"),
                                                                      Localizer.convert( JAWA_MAX_DIST_IN, 'distance'),
                                                                      Localizer.getUnits('distance') ), j )
				}
			}
		}


		//info: power racks (of any kind) not paired with env rack
		omodel.getObjectsByType("power_rack").each{ pr ->
			DLComponent comp = cmodel.getOwner(pr)
			if ( comp.listConsumers(true).contains("envRack") ){
				def envs =  comp.getConsumerProducers("envRack")
				if ( envs.size() == 0 ){
					def compType = comp.getDisplaySetting("name")
					infoResults += new ValidationResult(String.format(uiStrings.getProperty("validator.powerComponents.powerNoEnv"), compType), comp )
				}
			}
		}

		//error: tuskens need a BCMS link
		cmodel.getComponentsByType("standalone_power_rack").each{ tr ->
			//circuit1a...
			def total = 0
			total += tr.getConsumerProducers("circuit1a").size()
			total += tr.getConsumerProducers("circuit1b").size()
			total += tr.getConsumerProducers("circuit1c").size()
			total += tr.getConsumerProducers("circuit2a").size()
			total += tr.getConsumerProducers("circuit2b").size()
			total += tr.getConsumerProducers("circuit2c").size()
			if ( total == 0 ){
				failResults += new ValidationResult(uiStrings.getProperty("validator.powerComponents.lonelyTusken"), tr )
			}
		}



	}


	private void crahsInLI(){
		List<DLObject> coolingObjs = omodel.getObjectsByType( ObjectType.CRAC_CRAH )
		for( DLObject o : coolingObjs ) {
			DLComponent crah = cmodel.getOwner(o)

			if (crah.getPropertyValue("supplyLayerTop")  && crah.getPropertyValue("returnLayerTop")  ){
				failResults += new ValidationResult( CentralCatalogue.getUIS("validator.crahsInLI.Top"), crah )
			}
			if (crah.getPropertyValue("supplyLayerMiddle")  && crah.getPropertyValue("returnLayerMiddle")  ){
				failResults += new ValidationResult( CentralCatalogue.getUIS("validator.crahsInLI.Middle"), crah )
			}
			if (crah.getPropertyValue("supplyLayerBottom")  && crah.getPropertyValue("returnLayerBottom")  ){
				failResults += new ValidationResult( CentralCatalogue.getUIS("validator.crahsInLI.Bottom"), crah )
			}
			if (crah.getPropertyValue("supplyLayerSubfloor")  && crah.getPropertyValue("returnLayerSubfloor")  ){
				failResults += new ValidationResult( CentralCatalogue.getUIS("validator.crahsInLI.Subfloor"), crah )
			}
		}
	}


	private void generalManualConfigExpressionValid(){

		for( DLComponent c : cmodel.getNonProxyComponentsInRole( ComponentRole.MANUAL_EXPRESSION ) ) {
			def invalidProps = []
			c.listProperties().each{ propName ->
				ComponentProperty prop = c.getComponentProperty(propName)
				if( propName.contains("Valid") ) {
					if( prop.getValue().toString() == "0" ) {
						//it's invalid, go grab the display name
						invalidProps += c.getPropertyDisplayName( propName.replace("Valid", "") )
					}
				}

				//check for 'Value' in a write expression
				if( prop.getType().equalsIgnoreCase("DeploymentLab.ConfigModbusWrite") ||
				    prop.getType().equalsIgnoreCase("DeploymentLab.ConfigBacnetWrite") ) {
					String expr = prop.getValue().toString().toLowerCase()
					if ( !expr.isEmpty() && !( expr ==~ /.*\bvalue\b.*/ ) ) {
						warnResults += new ValidationResult( String.format( CentralCatalogue.getUIS("validator.manualConfigExpressionValid.noValue"), c.getPropertyDisplayName(propName) ) , c)
					}
				}
			}
			if ( invalidProps.size() > 0 ){
				warnResults += new ValidationResult( CentralCatalogue.getUIS("validator.manualConfigExpressionValid.invalid") + " " +  StringUtil.listFormat(invalidProps, "and") , c)
			}
		}
	}


	private void tempManualConfigExpressionValid(){
		List<String> roles = [ ComponentRole.MANUAL_EXPRESSION, ComponentRole.CONTROL_DEVICE, ComponentRole.CONTROLTYPE_TEMP ]

		for( DLComponent c : cmodel.getNonProxyComponentsInRoles( roles ) ) {
			// Do the checks within the context of each parcel. The null entry will cover non-parcel core properties.
			Set<ComponentParcel> parcels = [null] as Set
			parcels += c.getEnabledParcelsInRoles( roles )

			for( ComponentParcel parcel : parcels ) {
				//check to make sure the right optional setpoint read is filled in
				//check valveControl: 1=RAT, 2=SAT
				//make sure it lines up with satRead or ratRead

				DLObject ctrlCrah = c.getManagedObjectsOfType("controlout_crah", parcel).find { it } // empty-safe first()
				String propertyName = "valveControl"
				if (!ctrlCrah){
					ctrlCrah = c.getManagedObjectsOfType("controlout_crac", parcel).find { it }
					propertyName = "compressorControl"
				}

				if ( c.getPropertyStringValue(propertyName, parcel) == "1" ){
					// Check ratRead, but not the valid flag since the general expression checker will catch that.
					String ratRead = ((Dockingpoint)ctrlCrah.getObjectProperty("config")).getReferrent().getObjectProperty("ratRead").getValue().trim()
					Integer ratReadActive = Integer.parseInt( ConfigModbusEditor.parseConfigString(c.getPropertyStringValue("ratReadString", parcel))["active"] )
					if( ratRead.length() == 0 || ratReadActive == 0 ) {
						warnResults += new ValidationResult(CentralCatalogue.getUIS("validator.manualConfigExpressionValid.needsARAT"), ( parcel ? parcel : c ))
					}
				} else if ( c.getPropertyStringValue(propertyName, parcel) == "2" ){
					//check satRead, but not the valid flag since the general expression checker will catch that.
					String satRead = ((Dockingpoint)ctrlCrah.getObjectProperty("config")).getReferrent().getObjectProperty("satRead").getValue().trim()
					Integer satReadActive = Integer.parseInt( ConfigModbusEditor.parseConfigString(c.getPropertyStringValue("satReadString", parcel))["active"] )
					if( satRead.length() == 0 || satReadActive == 0 ) {
						warnResults += new ValidationResult(CentralCatalogue.getUIS("validator.manualConfigExpressionValid.needsASAT"), ( parcel ? parcel : c ))
					}
				}
			}
		}
	}

	private void checkModbusRanges() {
		for( DLObject o : omodel.getObjectsByType( ObjectType.CONTROLPROTO_MODBUS ) ) {
			DLComponent c = cmodel.getOwner( o )
			String parcel = o.getParcel()

			for( String propName : c.listProperties( parcel ) ) {
				if ( propName.contains("String") ) {
					String pval = c.getPropertyStringValue( propName )
					if( !pval ) continue

					def currentConfig = ConfigModbusEditor.parseConfigString( pval )
					if( currentConfig["isSimple"] == "true" ) {
						String propDisplayName = c.getPropertyDisplayName( propName.replace("String", "") )

						// The default min/max works for most types, but check for the occasions that they aren't.
						int regMin = 30001
						int regMax = 49999
						if( currentConfig["datatype"].equalsIgnoreCase('coil') ) {
							regMin = 1
							if ( propName.contains("Read") ) {
								regMax = 19999
							} else {
								regMax = 9999
							}
						}

						if ( currentConfig["register"].isInteger() ){
							def currentReg = Integer.parseInt( currentConfig["register"] )
							if ( currentReg > regMax || currentReg < regMin ) {
								failResults += new ValidationResult( String.format( CentralCatalogue.getUIS("validator.manualConfigExpressionValid.badRegister"), propDisplayName, regMin, regMax  ), ( parcel ? parcel : c ))
							}
						} else {
							failResults += new ValidationResult( String.format( CentralCatalogue.getUIS("validator.manualConfigExpressionValid.badRegister"), propDisplayName, regMin, regMax  ), ( parcel ? parcel : c ))
						}
					}
				}
			}
		}
	}

	/**
	 * Make sure the component configurations are correct for the Room Type.
	 */
	private void controlRoomChecks() {
		final int RF_MIN_DEVICES = 1
		final int RF_MIN_DEVICES_WARN = 4
		final int RF_MIN_COIN_TEMP = 4
		final int RF_MIN_COIN_PRESSURE = 4

		for( DLComponent room : cmodel.getComponentsInRole( ComponentRole.ROOM ) ) {
			// Skip the orphanage. They'll get checked once they're in a room, but some displayed errors are confusing when outside a room.
			if( room.getType() == ComponentType.ORPHANAGE ) continue

			// get some facts
			String roomType = room.getPropertyStringValue( RoomType._PROPERTY )
			String controlType = room.getPropertyStringValue( RoomControlType._PROPERTY )
			boolean crahOnly = ( roomType == RoomType.RAISED_FLOOR )

			List<DLComponent> controllable = []
			List<DLComponent> noPressureDevice = []
			List<DLComponent> noTempDevice = []

			List<DLComponent> tempCoins = []
			List<DLComponent> pressureCoins = []

			for( DLComponent kid : room.getChildComponents() ) {
				if( kid.hasRole( ComponentRole.CONTROLLABLE ) ) {
					// Exclude CRACs if this is a CRAH only control room.
					if( kid.hasManagedObjectOfType( ObjectType.CRAC_CRAH ) ) {
						Dockingpoint cracCondenser = (Dockingpoint) kid.getObject( ObjectType.CRAC_CRAH ).getObjectProperty('condenser1')
						if( crahOnly && cracCondenser?.getReferrent() != null ) {
							continue
						}
					}

					controllable.add( kid )

					if( !kid.hasRole( ComponentRole.CONTROLTYPE_TEMP ) ) {
						noTempDevice.add( kid )
					}

					if( !kid.hasRole( ComponentRole.CONTROLTYPE_PRESSURE ) ) {
						noPressureDevice.add( kid )
					}
				}

				if( kid.hasRole( ComponentRole.CONTROL_INPUT ) ) {
					if( kid.hasRole( ComponentRole.CONTROLTYPE_TEMP ) ) {
						tempCoins.add( kid )
					}

					if( kid.hasRole( ComponentRole.CONTROLTYPE_PRESSURE ) ) {
						pressureCoins.add( kid )
					}
				}
			}

			if( !RoomType._CONTROLLABLE_TYPES.contains( roomType ) || controlType == RoomControlType.NONE ) {
				// Should never happen since we don't even make the options available, but just in case Artisan messes up.
				for( DLComponent c : controllable ) {
					if( !noPressureDevice.contains( c ) && !noTempDevice.contains( c ) ) {
						makeFail( c, "validator.controlroom.controlConfigInUncontrollableRoom" )
					}
				}
			} else if( roomType == RoomType.RAISED_FLOOR ) {
				// We need a minimum number of devices to be valid.
				if( controllable.size() < RF_MIN_DEVICES ) {
					makeFail( room, "validator.controlroom.raisedfloor.tooFewDevices", RF_MIN_DEVICES )
				} else if( controllable.size() < RF_MIN_DEVICES_WARN ) {
					makeWarn( room, "validator.controlroom.raisedfloor.suggestMoreDevices", RF_MIN_DEVICES_WARN )
				}

				if( controlType.contains('pressure') ) {
					// All devices need to have pressure control configured.
					for( DLComponent c : noPressureDevice ) {
						makeFail( c, "validator.controlroom.raisedfloor.noPressureDevice" )
					}

					// There must be a minimum number of pressure COINs in the room if we have pressure control.
					if( pressureCoins.size() < RF_MIN_COIN_PRESSURE ) {
						makeFail( room, "validator.controlroom.raisedfloor.tooFewPressureCoins", pressureCoins.size(), RF_MIN_COIN_PRESSURE )
					}
				}

				if( controlType.contains('temperature') ) {
					// All devices need to have temperature control configured.
					for( DLComponent c : noTempDevice ) {
						makeFail( c, "validator.controlroom.raisedfloor.noTempDevice" )
					}

					// There must be a minimum number of temp COINs in the room if we have temp control.
					if( tempCoins.size() < RF_MIN_COIN_TEMP ) {
						makeFail( room, "validator.controlroom.raisedfloor.tooFewTempCoins", tempCoins.size(), RF_MIN_COIN_TEMP )
					}
				}
			} else if( roomType == RoomType.SLAB ) {
				// Warn if there are no controlled devices in the room.
				if( controllable.isEmpty() || (( noPressureDevice.size() == controllable.size() ) && ( noTempDevice.size() == controllable.size() )) ) {
					makeWarn( room, "validator.controlroom.slab.noDevices" )
				}
			}
		}
	}

	private void inletsOutlets(){
		def names = [:]
		omodel.getObjectsByType("graph_outlet").each{ o ->
			MapHelper.addList(names, o.getObjectProperty("name").getValue(), o )
		}

		names.each{ name, l ->
			def clist = l.collect{it -> cmodel.getOwner(it)}.unique()
			if(l.size() > 1)
				failResults += new ValidationResult("Cannot have more than one Outlet named '$name'.", clist)
		}

	}

	private void modbusIntegrator(){
		omodel.getObjectsByType("modbusproperty").each{ m ->
			if ( m.getObjectProperty("type").getValue().contains("32") ){
				if ( m.getObjectProperty("msw").getValue() == 0 || m.getObjectProperty("id").getValue() == 0 ){
					failResults += new ValidationResult( String.format( CentralCatalogue.getUIS("validator.modbusIntegrator.needBothRegisters"), m.getObjectProperty("name").getValue()  ),  cmodel.getOwner(m) )
				}
			}
		}
	}
	
	private void bacnetComponents(){
		omodel.getObjectsByType("bacnetnetwork").each{ n->
			String destination=  n.getObjectProperty("destination").getValue()
			String name = n.getObjectProperty("name").getValue()
			Integer DNET = n.getObjectProperty("DNET").getValue()
			
			DLComponent component = cmodel.getOwner(n)
			
			Integer ipInterfaceIdx = component.getPropertyValue("ipInterfaceIdx")
			String ipMask = component.getPropertyValue("ipMask")

			String dnetDN = component.getPropertyDisplayName("DNET")
			String ipInterfaceIdxDN = component.getPropertyDisplayName("ipInterfaceIdx")

			if(name==null || name==""){
				failResults += new ValidationResult("Name is required.", component)
			}
			
			if(destination==null || destination==""){
				failResults += new ValidationResult("Destination Network IP is required.", component)
			} else {
				validateIPAddress(destination, component)
			}
			
			if(ipInterfaceIdx==null || ipInterfaceIdx==""){
				failResults += new ValidationResult(String.format(CentralCatalogue.getUIS('validator.bacnet.propertyReq'), ipInterfaceIdxDN), component)
			} else if (ipInterfaceIdx<0) {
				failResults += new ValidationResult(String.format(CentralCatalogue.getUIS('validator.bacnet.badNetworkInterfaceIdx'), ipInterfaceIdxDN), component)
			}
			
			if(ipMask==null || ipMask==""){
				failResults += new ValidationResult("IP Mask is required.", component)
			}
			
			if(DNET==null || DNET==""){
				failResults += new ValidationResult(String.format(CentralCatalogue.getUIS('validator.bacnet.propertyReq'), dnetDN), component)
			} else if (DNET<1 || DNET>65534) {
				failResults += new ValidationResult(String.format(CentralCatalogue.getUIS('validator.bacnet.badNetworkId'), dnetDN), component)
			}
			
			if(n.getChildren().size()==0){
				failResults += new ValidationResult("BACNet network is empty.", component)
			}
		}

		omodel.getObjectsByType("bacnetdevice").each{ d->
			if(d.getChildren().size()==0){
				failResults += new ValidationResult("BACNet device is empty.", cmodel.getOwner(d))
			}
		}
			
	}

    private void uniqueBacnetInstances(){
        def instances = [:]
        omodel.getObjectsByType("bacnetdevice").each{ device ->
            def net  = device.getParents().find(){ it.type == 'bacnetnetwork' }
            def ip   = net.getObjectProperty('destination').value
            def dIns = device.getObjectProperty('instance').value
            if( ( dIns != null ) && ( dIns >= BACNET_INSTANCE_MIN ) && ( dIns <= BACNET_INSTANCE_MAX ) ) {
                // The Device Instance must be unique per IP Network. So use the key "IP::Instance"
                String key = "${ip}::${dIns}"
                MapHelper.addList( instances, key, device )
            } else {
                failResults += new ValidationResult( String.format( CentralCatalogue.getUIS('validator.bacnet.badInstance'), BACNET_INSTANCE_MIN, BACNET_INSTANCE_MAX ), cmodel.getOwner(device) )
            }

            // Now check the BACnet Objects attached to this device.
            device.getChildren().each { obj ->
                def oIns = obj.getObjectProperty('instance').value
				def oPullPeriod = cmodel.getOwner(obj).getPropertyValue('pullPeriod')
				def pullPeriodDN = cmodel.getOwner(obj).getPropertyDisplayName('pullPeriod')
                if( ( oIns != null ) && ( oIns >= BACNET_INSTANCE_MIN ) && ( oIns <= BACNET_INSTANCE_MAX ) ) {
                    // The Object Instance must be unique per Object type. Key: "IP::DeviceInstance::ObjType::ObjInst"
                    // FIXBUG 7671: The Object Instance must be unique per Object type. Key: "IP::DeviceInstance::bacnetproperty id::ObjInst"
                    //log.trace("propertyid:" + cmodel.getOwner(obj).getPropertyStringValue("property").split(":")[0]  )
                    String propertyId = cmodel.getOwner(obj).getPropertyStringValue("property").split(":")[0]
                    String key = "${ip}::${dIns}::${propertyId}::${oIns}"
                    MapHelper.addList( instances, key, obj )
                } else {
                    failResults += new ValidationResult( String.format( CentralCatalogue.getUIS('validator.bacnet.badInstance'), BACNET_INSTANCE_MIN, BACNET_INSTANCE_MAX ), cmodel.getOwner(obj) )
                }
				if (oPullPeriod < 0) {
					failResults += new ValidationResult( String.format( CentralCatalogue.getUIS('validator.bacnet.negativeSI'), pullPeriodDN ), cmodel.getOwner(obj) )
				}
            }
        }

        //flatten the list so we only warn on component pairs, not each address+register pair
        List devicesToWarnOn = []
        instances.each{ key, objs ->
            if( objs.size() > 1 ) {
                List comps = objs.collect { cmodel.getOwner(it) }
                if( ! devicesToWarnOn.contains( comps ) ) {
                    if( objs.get(0).type == 'bacnetdevice' ) {
                        warnResults += new ValidationResult( CentralCatalogue.getUIS("validator.bacnet.dupeDeviceInstance") , comps )
                    } else {
                        warnResults += new ValidationResult( CentralCatalogue.getUIS("validator.bacnet.dupeObjectInstance") , comps )
                    }
                }
            }
        }
    }

	/**
	 * Make sure nothing is in the orphanage.
	 */
	private void checkOrphans() {
		for( DLComponent orphanage : cmodel.getComponentsByType( ComponentType.ORPHANAGE ) ) {
			for( DLComponent child : orphanage.getChildComponents() ) {
				makeFail('validator.rooms.OrphanageNotEmpty', [child] )
			}
		}
	}

	/**
	 * Warn if any sensor name field is longer than 25 characters.
	 */
	private void wsnSensorNames(){
		omodel.getObjectsByType("wsnsensor").each {
			if( it.getName().length() > 25 ){
				makeWarn("validator.wsnSensorNames.tooLong", [cmodel.getManagingComponent(it)])
			}
		}

	}

	/**
	 * Error if any chart contents have the same value listed twice in the left column
	 */
	private void chartDupeEntries(){
		//DeploymentLab.ChartContents*
		cmodel.getComponents().each{ c ->
			c.listProperties().each{ pid ->
				ComponentProperty p = c.getComponentProperty(pid)
				if ( p.getType().contains("DeploymentLab.ChartContents") ){
					//begin checking!

					String tableString = p.getValue()
					def leftSide = ChartContents.getXValues(tableString)
					def xcount = [:]
					leftSide.each{ x -> MapHelper.increment(xcount,x) }

					xcount.each{ k, v ->
						if( v > 1 ){
							failResults += new ValidationResult( String.format( CentralCatalogue.getUIS("validator.chartDupeEntries.dupe"), k, p.getDisplayName() ), c )
						}
					}

				}
			}
		}
	}

	/**
	* Lontalk plugin validation
	*/
	private void checkLontalkPlugin() {
		def lontalkRouters = cmodel.getComponentsByType("lonrouter")
		lontalkRouters.each {router ->
			String name = router.getPropertyStringValue("name")
			String address = router.getPropertyStringValue("address")
			String port = router.getPropertyValue("port")
			String retries = router.getPropertyValue("retries")
			String timeout = router.getPropertyValue("timeout")

			if (name.equals("") || name.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.lontalk.router.name"), router)
			}

			if (address.equals("") || address.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.lontalk.router.address"), router)
			}

			if (port == null) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.lontalk.router.port"), router)
			} else {
				if (!port.isInteger() || Integer.parseInt(port) < 1) {
					failResults += new ValidationResult(CentralCatalogue.getUIS("validator.lontalk.router.invalidport"), router)
				}
			}

			if (retries == null) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.lontalk.router.retries"), router)
			} else {
				if (!retries.isInteger() || Integer.parseInt(retries) < 1) {
					failResults += new ValidationResult(CentralCatalogue.getUIS("validator.lontalk.router.invalidretries"), router)
				}
			}

			if (timeout == null) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.lontalk.router.timeout"), router)
			} else {
				if (!timeout.isInteger() || Integer.parseInt(timeout) < 1) {
					failResults += new ValidationResult(CentralCatalogue.getUIS("validator.lontalk.router.invalidtimeout"), router)
				}
			}

		}

		// check lontalk device
		def lontalkDevices = cmodel.getComponentsByType("lonnode")
		lontalkDevices.each {device ->
			String name = device.getPropertyStringValue("name")
			String subnet = device.getPropertyStringValue("subnet")
			String node = device.getPropertyValue("node")

			if (name.equals("") || name.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.lontalk.device.name"), device)
			}

			if (subnet.equals("") || subnet.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.lontalk.device.subnet"), device)
			}

			if (node == null) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.lontalk.device.node"), device)
			} else {
				if (!node.isInteger() || Integer.parseInt(node) < 1) {
					failResults += new ValidationResult(CentralCatalogue.getUIS("validator.lontalk.device.invalidnode"), device)
				}
			}
		}

		//fast and dirty check for properties
		omodel.getObjectsByType("lonproperty").each{ o ->
			o.getPropertiesByType("setting").each{ p ->
                // check String
				if (p.name.equals("name") || p.name.equals("type") || p.name.equals("fieldname")){
					def val = ((Setting)p).getValue()
					if( val == null || val.toString().equals("") || val.toString().equals("0") ){
						failResults += new ValidationResult("bad value for '${p.name}'".toString(), cmodel.getOwner(o))
					}
				}else if(p.name.equals("nvindex")){
                    int nvindex = ((Integer)((Setting)p).getValue()).intValue()
					if(nvindex<0 || nvindex>255 ){
						failResults += new ValidationResult("bad value for '${p.name}'".toString(), cmodel.getOwner(o))
					}
                }else if(p.name.equals("selector")){
                    int selector = ((Integer)((Setting)p).getValue()).intValue()

					if(selector<0 || selector > 16384 ){
						failResults += new ValidationResult("bad value for '${p.name}'".toString(), cmodel.getOwner(o))
					}
                }
			}
		}
	}
	
	private void checkIPMI(){
	
		def ipmirack=cmodel.getComponentsByType("ipmirack-intake")
		ipmirack.each { rack ->
			String name = rack.getPropertyValue("name")
			String user=rack.getPropertyValue("user")
			
			Integer depth=rack.getPropertyValue("depth")
			Integer width=rack.getPropertyValue("width")
			Integer period=rack.getPropertyValue("period")
					
			String topaddress = rack.getPropertyValue("topaddress")
			String topsensorid=rack.getPropertyValue("topsensorid")
			
			String middleaddress = rack.getPropertyValue("middleaddress")
			String middlesensorid=rack.getPropertyValue("middlesensorid")

			String bottomaddress = rack.getPropertyValue("bottomaddress")
			String bottomsensorid=rack.getPropertyValue("bottomsensorid")

			if (name.equals("") || name.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.name"), rack)
			}
			if (user.equals("") || user.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.user"), rack)
			}
			if (depth<1){
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.depth"), rack)
			}
			if(width<1){
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.width"), rack)
			}
			if (period<1){
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.period"), rack)
			}
			if (topaddress.equals("") || topaddress.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.address"), rack)
			}
			if (topsensorid==null){
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensorid"), rack)
			} else{
				if (Integer.parseInt(topsensorid) < 1) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensoridinvalid"), rack)
				}	
			}
			if (middleaddress.equals("") || middleaddress.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.address"), rack)
			}
			if (middlesensorid == null) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensorid"), rack)
			} else {
				if (!middlesensorid.isInteger() || Integer.parseInt(middlesensorid) < 1) {
					failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensoridinvalid"), rack)
				}
			}
			if (bottomaddress.equals("") || bottomaddress.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.address"), rack)
			}
			if (bottomsensorid == null) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensorid"), rack)
			} else {
				if (!bottomsensorid.isInteger() || Integer.parseInt(bottomsensorid) < 1) {
					failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensoridinvalid"), rack)
				}
			}
		}
		
		def ipmirackinout=cmodel.getComponentsByType("ipmirack-exhaust")
		ipmirackinout.each { server ->
			String name = server.getPropertyStringValue("name")
			String user=server.getPropertyStringValue("user")
			
			Integer depth=server.getPropertyValue("depth")
			Integer width=server.getPropertyValue("width")
			Integer period=server.getPropertyValue("period")
			
			String topaddress = server.getPropertyStringValue("topaddress")
			String topinletsensorid=server.getPropertyValue("topinletsensorid")
			String topoutletsensorid=server.getPropertyValue("topoutletsensorid")
			
			String middleaddress = server.getPropertyStringValue("middleaddress")
			String middleinletsensorid=server.getPropertyValue("middleinletsensorid")
			String middleoutletsensorid=server.getPropertyValue("middleoutletsensorid")
			
			String bottomaddress = server.getPropertyStringValue("bottomaddress")
			String bottominletsensorid=server.getPropertyValue("bottominletsensorid")
			String bottomoutletsensorid=server.getPropertyValue("bottomoutletsensorid")
			
			if (name.equals("") || name.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.name"), server)
			}
			if (user.equals("") || user.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.user"), server)
			}
			if (depth<1){
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.depth"), server)
			}
			if(width<1){
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.width"), server)
			}
			if (period<1){
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.period"), server)
			}
			if (topaddress.equals("") || topaddress.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.address"), server)
			}
			if (topinletsensorid == null) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensorid"), server)
			} else {
				if (!topinletsensorid.isInteger() || Integer.parseInt(topinletsensorid) < 1) {
					failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensoridinvalid"), server)
				}
			}
			if (topoutletsensorid == null) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensorid"), server)
			} else {
				if (!topoutletsensorid.isInteger() || Integer.parseInt(topoutletsensorid) < 1) {
					failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensoridinvalid"), server)
				}
			}
			if (middleaddress.equals("") || middleaddress.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.address"), server)
			}
			if (middleinletsensorid == null) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensorid"), server)
			} else {
				if (!middleinletsensorid.isInteger() || Integer.parseInt(middleinletsensorid) < 1) {
					failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensoridinvalid"), server)
				}
			}
			if (middleoutletsensorid == null) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensorid"), server)
			} else {
				if (!middleoutletsensorid.isInteger() || Integer.parseInt(middleoutletsensorid) < 1) {
					failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensoridinvalid"), server)
				}
			}
			if (bottomaddress.equals("") || bottomaddress.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.address"), server)
			}
			if (bottominletsensorid == null) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensorid"), server)
			} else {
				if (!bottominletsensorid.isInteger() || Integer.parseInt(bottominletsensorid) < 1) {
					failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensoridinvalid"), server)
				}
			}
			if (bottomoutletsensorid == null) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensorid"), server)
			} else {
				if (!bottomoutletsensorid.isInteger() || Integer.parseInt(bottomoutletsensorid) < 1) {
					failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensoridinvalid"), server)
				}
			}	
		}
		def ipmitemp=cmodel.getComponentsByType("ipmi-temperature-sensor")
		ipmitemp.each { ipmi ->
			String name = ipmi.getPropertyValue("name")
			Integer period=ipmi.getPropertyValue("period")

			String user=ipmi.getPropertyValue("user")
			String address=ipmi.getPropertyValue("address")
			String sensorid=ipmi.getPropertyValue("sensorid")
			
			if (name.equals("") || name.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.name"), ipmi)
			}
			if (period < 1) {
					failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.period"), ipmi)
			}
			if (user.equals("") || user.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.user"), ipmi)
			}
			if (address.equals("") || address.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.address"), ipmi)
			}
			if (sensorid == null) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensorid"), ipmi)
			} else {
				if (!sensorid.isInteger() || Integer.parseInt(sensorid) < 1) {
					failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensoridinvalid"), ipmi)
				}
			}
			def layer = ipmi.getPropertyValue('li_layer')
            def bad   = null
            if( layer == CentralCatalogue.LI_LAYER_TOP ) {
                bad = ipmi.getProducerConsumers().findAll{ pc ->
                    pc.getType().equals('controlpoint_single_temperature_mid') || pc.getType().equals('controlpoint_single_temperature_bot')
                }
            } else if( layer == CentralCatalogue.LI_LAYER_MIDDLE ) {
                bad = ipmi.getProducerConsumers().findAll{ pc ->
                    pc.getType().equals('controlpoint_single_temperature') || pc.getType().equals('controlpoint_single_temperature_bot')
                }
            } else if( layer == CentralCatalogue.LI_LAYER_BOTTOM ) {
                bad = ipmi.getProducerConsumers().findAll{ pc ->
                    pc.getType().equals('controlpoint_single_temperature_mid') || pc.getType().equals('controlpoint_single_temperature')
                }
            } else if((layer == CentralCatalogue.LI_LAYER_NONE) ) {
                bad = ipmi.getProducerConsumers().findAll{ pc ->
					pc.getType().contains('controlpoint')
				}
            }

            if( bad?.size() > 0 ) {
                failResults += new ValidationResult( CentralCatalogue.getUIS('validator.ipmi.wrongLI'), ipmi )
            }
			
		}
		
		def ipmitopmonitor=cmodel.getComponentsByType("ipmirack-top-monitoring")
		ipmitopmonitor.each{ ipmi ->
		
		String name = ipmi.getPropertyStringValue("name")
		Integer period=ipmi.getPropertyValue("period")	
		String user=ipmi.getPropertyStringValue("user")
		String address=ipmi.getPropertyStringValue("address")
		String inletsensorid=ipmi.getPropertyValue("inletsensorid")
		String outletsensorid=ipmi.getPropertyValue("outletsensorid")

			
		if (name.equals("") || name.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.name"), ipmi)
			}
		if (user.equals("") || user.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.user"), ipmi)
			}
		if (period<1){
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.period"), ipmi)
		}
		if (address.equals("") || address.length() == 0) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.address"), ipmi)
			}
		if (inletsensorid == null) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensorid"), ipmi)
			} else {
				if (!inletsensorid.isInteger() || Integer.parseInt(inletsensorid) < 1) {
					failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensoridinvalid"), ipmi)
				}
			}
		if (outletsensorid == null) {
				failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensorid"), ipmi)
			} else {
				if (!outletsensorid.isInteger() || Integer.parseInt(outletsensorid) < 1) {
					failResults += new ValidationResult(CentralCatalogue.getUIS("validator.ipmi.rack.sensoridinvalid"), ipmi)
				}
			}
		}
	}

	/**
	 * Check for optional reftemp consumers; if any exist with out a reftemp, add an info talking about it.
	 */
	private void optionalRefTemp(){
		def results = []
		for( DLComponent c : cmodel.getComponentsInRole("placeable") ) {
			for(Consumer rt : c.getConsumerObjectsOfType("reftemp") ){
				if(! rt.isRequired() && !rt.hasProducer()){
					results.add(c)
				}
			}
		}
		if (results.size() > 0){
			this.makeInfo("validator.optionalRefTemp.notUsed", results)
		}
	}

    /**
     * check duplicate rtu manager name
     */
    private void checkRtuManagerName(){
        def names = [:]
		omodel.getObjectsByType('controlalg_rtumgr').each{ o ->
			String name = o.getObjectProperty('name').getValue()
			if(! names.containsKey(name)) {
				names[name] = []
			}
			names[name] += o
		}
		names.each{ name, l ->
			def clist = l.collect{it -> cmodel.getOwner(it)}.unique()
			if(l.size() > 1){
				failResults += new ValidationResult(CentralCatalogue.getUIS('validator.rtumanager.duplicatename')+ " '$name'.", clist)
			}
		}
    }

	private void ackbar(){
		for(def ackbar: omodel.getObjectsByType("wsnactuator")){
			def c = cmodel.getManagingComponent(ackbar)
			def failsafe = ackbar.getObjectSetting("failsafeSetPoint").getValue()
			def minimumSetting = ackbar.getObjectSetting("minimumSetting").getValue()
			def maximumSetting = ackbar.getObjectSetting("maximumSetting").getValue()
			def travelTime = ackbar.getObjectSetting("travelTime").getValue()
			if(failsafe < 0 || failsafe > 100 ){
				makeFail("validator.ackbar.failsafe", c)
			}
			if(minimumSetting < 0 || minimumSetting > 100 ){
				makeFail("validator.ackbar.min", c)
			}
			if(maximumSetting < 0 || maximumSetting > 100 ){
				makeFail("validator.ackbar.max", c)
			}
			if(travelTime <= 0 ){
				makeFail("validator.ackbar.traveltime", c)
			}
		}
	}

	private void checkWSNReassignment(){
		List<DLComponent> toWarn = new ArrayList<DLComponent>()
		omodel.getObjectsByType("wsnnetwork").each {network ->
			for(ChildLink link : network.getPropertiesByType('childlink')){
				link.getRemovedChildIds().each {dlid ->
					// Don't warn if it was deleted anyway.
					if( !omodel.isDeleted( dlid ) ) {
						toWarn.add(cmodel.getOwner(omodel.getObject(dlid)))
					}
				}
			}
		}

		// Don't forget to check for deleted networks.  All those nodes need the warning.
		for( DLObject o : omodel.listDeletedObjects() ) {
			if( o.getType() == ObjectType.WSN_NETWORK ) {
				for(ChildLink link : o.getPropertiesByType('childlink')){
					link.getChildren().each {dlid ->
						// Don't warn if it was deleted anyway.
						if( !omodel.isDeleted( dlid ) ) {
							toWarn.add(cmodel.getOwner(omodel.getObject(dlid)))
						}
					}
				}
			}
		}

		if (!toWarn.isEmpty()){
			makeWarn( toWarn.unique(), "validator.wsnassignment.reassign" )
		}
	}

	private void checkPlanningGroups() {
		for( DLComponent c : cmodel.getComponentsByType( ComponentType.PLANNING_GROUP ) ) {

			// Warn if the group isn't being exported but has components that were previously exported.
			if( !c.getPropertyValue('export') ) {
				List<DLComponent> toWarn = new ArrayList<DLComponent>()

				for( DLComponent child : c.getChildComponents() ) {
					if( child.isExported() ) {
						toWarn.add( child )
					}
				}

				if( !toWarn.isEmpty() ) {
					warnResults += new ValidationResult( CentralCatalogue.formatUIS('validator.planningroup.partiallyExported', c.getName() ), toWarn )
				}
			}
		}
	}

	/**
	 * Make sure that one side of an association isn't a component that will export while the other is set to not export.
	 */
	private void checkPhantomAssociations() {
		for( DLComponent c : cmodel.getNonProxyComponents() ) {
			for( DLComponent other : c.getProducerConsumers() ) {
				// Proxies never export, so make sure to check the ancho, not the proxy.
				if( other.isProxy() ) {
					other = other.getAnchor()
				}

				if( c.isExportable() != other.isExportable() ) {
					makeFail( [c, other], "validator.association.phantomMate" )
				}
			}
		}
	}

	/**
	 * Make sure room boundaries don't overlap.
	 */
	private checkOverlappingRooms() {
		for( DLComponent drawing : cmodel.getComponentsByType( ComponentType.DRAWING ) ) {
			List<DLComponent> rooms = cmodel.getComponentsByType( ComponentRole.ROOM, drawing )

			if( rooms.size() > 1 ) {
				for( int i = 0; i < rooms.size(); i++ ) {
					ArbitraryShapeNode rA = (ArbitraryShapeNode) CentralCatalogue.getDeploymentPanel().getPNodeFor( rooms[i] )
					for( int j = i + 1; j < rooms.size(); j++ ) {
						ArbitraryShapeNode rB = (ArbitraryShapeNode) CentralCatalogue.getDeploymentPanel().getPNodeFor( rooms[j] )

						if( rA.intersects( rB ) ) {
							failResults += new ValidationResult("Room boundaries overlap.", [ rooms[i], rooms[j] ] )
						}
					}
				}
			}
		}
	}

	/**
	 * Make sure associations that don't make sense to be in other rooms, such as ref temp, are in the same room.
	 */
	private checkSameRoomAssociations() {
		def sameRoomAssociation = ["reftemp","exhausttemp"]
		for( DLComponent thisRoom : cmodel.getComponentsInRole( ComponentRole.ROOM ) ) {
			for( DLComponent c : thisRoom.getChildComponents() ) {
				for( Consumer consumer : c.getAllConsumers() ) {
					if( sameRoomAssociation.contains( consumer.getDatatype() ) ) {
						for( Pair<DLComponent,String> other : consumer.listProducerComponents() ) {
							DLComponent otherRoom = other.a.getParentsOfRole( ComponentRole.ROOM ).first()
							if( otherRoom != thisRoom ) {
								failResults += new ValidationResult("Associated components must be in the same Room.", [ c, other.a ] )
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Make sure the sensors and any other x/y locations of a component are all inside the same room.
	 */
	private checkComponentObjectsInSameRoom() {
		for( DLComponent room : cmodel.getComponentsInRoles( [ ComponentRole.ROOM, ComponentRole.PLACEABLE ] ) ) {
			ArbitraryShapeNode roomNode = (ArbitraryShapeNode) CentralCatalogue.getDeploymentPanel().getPNodeFor( room )

			if( roomNode ) {
				Shape roomShape = roomNode.getPath().getPathReference()

				for( DLComponent c : room.getChildComponents() ) {
					// Skip inner rooms since the are handled otherwise and have internal x/y that is used for export only.
					if( c.hasRole( ComponentRole.ROOM ) ) continue

					for( DLObject o : c.getManagedObjects() ) {
						Double x = o.getObjectSetting('x')?.getValue()
						Double y = o.getObjectSetting('y')?.getValue()

						if( ( x != null ) && ( y != null ) ) {
							if( !roomShape.contains( x, y ) ) {
								makeFail( c, "validator.rooms.junkHangingOut" )
								break
							}
						}
					}
				}
			}
		}
	}
}

class ValidationResult {
	String message
	def objects = []
	ValidationResult(String m, def os) {
		message = m
		os.each { objects += new ValidationObject( it ) }
	}

	String toString() {
		StringBuilder str = new StringBuilder( message )
		str.append("  [ ")
		objects.each {
			str.append("'")
			str.append( it.getName() )
			str.append("' ")
		}
		str.append("]")
		return str.toString()
	}
}

class ValidationObject {
	DLComponent component
	String name

	ValidationObject( DLComponent c ) {
		this.component = c
		this.name = c.getName()
	}

	ValidationObject( ComponentParcel p ) {
		this.component = p.getOwner()
		this.name = "${p.getOwner().getName()}::${p.getTraitDisplayName()}"
	}
}
