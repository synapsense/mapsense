
package DeploymentLab;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;

import java.awt.font.GlyphVector;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.swing.JComponent;

public class ProgressPane extends JComponent {
	private static final long serialVersionUID = 1L;

	private String caption;
	private GlyphVector captionGlyph;
	private Rectangle2D captionBounds;

	private BufferedImage background;

	private int pathRadius = 50;
	private int radius = 10;
	private double angle = 0;

	private Color alphaGray  = new Color(0.0f, 0.0f, 0.0f, 0.6f);
	private Color alphaBlue  = new Color(0.0f, 0.0f, 1.0f, 0.5f);

	public ProgressPane() {
		setOpaque(true);
		addMouseListener(new MouseAdapter() {});
		addMouseMotionListener(new MouseMotionAdapter() {});
		addKeyListener(new KeyAdapter() {});
		addComponentListener(new ComponentAdapter() {
			public void componentShown(ComponentEvent evt) {
				requestFocusInWindow();
				setFocusTraversalKeysEnabled(false);
			}
		});

		//setCaption("Thinking...");
		this.setCaption( this.generateMessage() );
	}

	@Override
	public synchronized void paintComponent(Graphics g1) {
		Graphics2D g = (Graphics2D)g1;
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		g.drawImage(background, 0, 0, this);

		int centerX = getWidth()/2;
		int centerY = getHeight()/2;

		double spotX = pathRadius * Math.cos(angle);
		double spotY = pathRadius * Math.sin(angle);

		g.setColor(alphaBlue);
		g.fillOval((int)(centerX + spotX) - radius, (int)(centerY + spotY) - radius, 2*radius, 2*radius);

		if(captionGlyph == null) {
			captionGlyph = g.getFont().createGlyphVector(g.getFontRenderContext(), caption);
			captionBounds = captionGlyph.getVisualBounds();
		}

		float textX = centerX - (float)captionBounds.getWidth() / 2.0f;
		float textY = centerY + (float)captionBounds.getHeight() / 2.0f;
		g.setColor(Color.white);
		g.drawGlyphVector(captionGlyph, textX, textY);
	}

	public String getCaption() {
		return caption;
	}

	public synchronized void setCaption(String c) {
		if( this.usingDefaultMessages() ){
			c = this.generateMessage();
		}

		caption = c;
		captionGlyph = null;
		repaint();
	}

	public double getAngle() {
		return angle;
	}

	public synchronized void setAngle(double a) {
		if(a >= 2*Math.PI){
			a -= 2*Math.PI;
		}
		angle = a;

		if(this.usingDefaultMessages()){
			 if(countFromLastMessage == messageThreshold ){
				this.setCaption( this.generateMessage() );
				countFromLastMessage = 0;
			} else {
				countFromLastMessage++;
			}
		}

		int centerX = getWidth()/2;
		int centerY = getHeight()/2;

		repaint(centerX - pathRadius - radius, centerY - pathRadius - radius, 2*pathRadius + 2*radius, 2*pathRadius + 2*radius);
	}

	public synchronized void setBG(BufferedImage bg) {
		background = bg;
		Graphics2D g = (Graphics2D)background.getGraphics();
		g.setColor(alphaGray);
		g.fillRect(0, 0, background.getWidth(), background.getHeight());
	}

	/**
	 * Throw away the background image when we're done with it.
	 */
	public synchronized void flush(){
		background = null;
	}


	private int countFromLastMessage = 0;
	private static final int messageThreshold = 3;

	private boolean _makeDefaultMessages = false;
	public boolean usingDefaultMessages() {
		return _makeDefaultMessages;
	}

	public void useDefaultMessages(boolean useDefaultMessages) {
		this._makeDefaultMessages = useDefaultMessages;
	}

	private static final List<String> loadingMessages = Arrays.asList(CentralCatalogue.getUIS("loading.messages").split(","));
	private static final Random gygax = new Random();

	public String generateMessage(){
		return loadingMessages.get(gygax.nextInt(loadingMessages.size()) );
	}

}

