package DeploymentLab

import DeploymentLab.Model.*
import DeploymentLab.channellogger.*

/**
 * Manages what nodes are currently selected on the deployment panel.
 */
class SelectModel implements ModelChangeListener {
	private static final Logger log = Logger.getLogger(SelectModel.class.getName())

	/** The currently active drawing component.  */
	private DLComponent _activeDrawing
	/** The currently active Container Component.  */
	private Map<DLComponent, DLComponent> activeZones = new HashMap<>()
	/** The currently active data source.  */
	private Map<DLComponent,DLComponent> activeNetworks = new HashMap<>()

	/** List of all registered SelectionChangeListeners.  */
	private selectionChangeListeners = []

	//TODO: make the current selection a set, not a list (no dupes allowed!)
	/** A list of the currently selected Components. (Despite the name of the variable.)  */
	ArrayList<DLComponent> selectedObjects = []

	/** Indicates if the select model is in the process of currently notifying listeners of model changes.  Updates will be rejected while true.  */
	boolean notifying = false

    boolean cmodelInitializing = false


	/** "Blank" constructor.  Doesn't do much.  */
	SelectModel() {
		//for debugging purposes, turn this on to log all selection changes
		//this.addSelectionChangeListener(new SelectModelWatcher())
	}

	/**
	 * Register a new SelectionChangeListener.
	 * @param l a SelectionChangeListener to add
	 */
	void addSelectionChangeListener(SelectionChangeListener l) {
		selectionChangeListeners += l
	}

	/**
	 * Unregister a new SelectionChangeListener.
	 * @param l a SelectionChangeListener to remove
	 */
	void removeSelectionChangeListener(SelectionChangeListener l) {
		selectionChangeListeners -= l
	}

	/**
	 * Notify any registered selectionChangeListeners that a selection event has taken place.
	 * @param the selectionChangeEvent to notify listeners of.
	 */
	private void notifyListeners(SelectionChangeEvent e) {
		if (e.componentsAdded.size() + e.zonesAdded.size() + e.networksAdded.size() + e.drawingsAdded.size() + e.groupsAdded.size() +
		    e.componentsRemoved.size() + e.zonesRemoved.size() + e.networksRemoved.size() + e.drawingsRemoved.size() + e.groupsRemoved.size() == 0) {
			log.trace("Ignoring empty selection change", "default")
			return
		}
		//log.debug("Notify Start!")
		notifying = true
		selectionChangeListeners.each {it.selectionChanged(e)}
		notifying = false
		//log.debug("Notify End!")
	}

    /**
     * Notify any registered selectionChangeListeners that the Active Drawing has changed.
     *
     * @param oldDrawing  The previously active drawing.
     * @param newDrawing  The currently active drawing.
     */
    private void fireActiveDrawingChanged( DLComponent oldDrawing, DLComponent newDrawing ) {
		log.debug("fireActiveDrawingChanged firing now:")

        // Some wonkiness to minimize the effect of the timing debug. Logging in each iteration added a significant overhead.
        long[] tstamps = new long[ selectionChangeListeners.size() + 1 ]

        notifying = true
        for( int i = 0; i < selectionChangeListeners.size(); i++ ) {
            tstamps[i] = System.currentTimeMillis()
			log.debug("notify ${selectionChangeListeners.get(i).getClass().name}")
            selectionChangeListeners.get(i).activeDrawingChanged( oldDrawing, newDrawing )
        }
        notifying = false

        // TODO: Is there some way to wrap this in a log.isDebug()?
        tstamps[tstamps.length-1] = System.currentTimeMillis()
        for( int i = 0; i < selectionChangeListeners.size(); i++ ) {
            log.debug("fireActiveDrawingChanged " + selectionChangeListeners.get(i).getClass().name + ": " + (tstamps[i+1] - tstamps[i]) + " ms", "default" )
        }
        log.debug( "fireActiveDrawingChanged TOTAL: " + (tstamps[tstamps.length-1] - tstamps[0]) + " ms", "default" )
    }


	private void fireActiveZoneChanged(DLComponent oldZ, DLComponent newZ){
		log.debug("see if anyone responds to activeZoneChanged")
		notifying = true
		for(SelectionChangeListener l : selectionChangeListeners){
			if(l.respondsTo("activeZoneChanged")){
				log.debug("activeZoneChanged found on $l, firing", "default")
				l.activeZoneChanged(oldZ, newZ)
			}
		}
		notifying = false
	}

	private void fireActiveDatasourceChanged(DLComponent oldDS, DLComponent newDS){
		notifying = true
		for(SelectionChangeListener l : selectionChangeListeners){
			if(l.respondsTo("activeDatasourceChanged")){
				log.debug("activeDatasourceChanged found on $l, firing", "default")
				l.activeDatasourceChanged(oldDS, newDS)
			}
		}
		notifying = false
	}


	/**
	 * Getter for the active drawing property.  The system can only interact with components that are in the active data center.
	 * @return the currently active drawing.
	 */
	DLComponent getActiveDrawing() {
		return _activeDrawing
	}

    /**
     * Set the active drawing. The call is ignored, thus no listeners are notified, if the provided drawing is
     * the same as the already active drawing.
     *
     * @param drawing  The new component to make the active drawing.
     */
	void setActiveDrawing(DLComponent drawing) {
		if (notifying) { log.trace("rejecting recursive drawing change "); return }

		// Undo sometimes sets to NULL because of a previous null->drawing notification just to trigger listeners. Ignore those.
		if( drawing == null && _activeDrawing != null && _activeDrawing.getModel().hasActiveState( ModelState.UNDOING ) ) return

        log.trace("setActiveDrawing  old: '${getActiveDrawing()?.name}'  new: '${drawing?.name}'" )
		/*
		if (drawing == null){
			log.trace("got a null drawing from: ")
			def e = new RuntimeException("tester")
			log.trace(log.getStackTrace(e))
		}
		*/

        if( drawing == _activeDrawing ) {
            log.debug("Ignoring setActiveDrawing call since '${drawing?.getName()}' is already the active drawing.")
            return
        }

        if( (drawing != null) && !drawing.hasClass('drawing') ) {
            throw new Exception("Tried to set active drawing to not a drawing!")
        }

        DLComponent oldDrawing = _activeDrawing

        _activeDrawing = drawing

        if( _activeDrawing != null ) {
            ProjectSettings.getInstance().activeDrawing = _activeDrawing.getObject( ObjectType.DRAWING ).getDlid().toString()
        } else {
            ProjectSettings.getInstance().activeDrawing = ""
        }

		//clear the selection before changing the drawing; a selection cannot contain components from >1 drawing
		this.clearSelection()
        fireActiveDrawingChanged( oldDrawing, _activeDrawing )
	}

	/**
	 * Getter for the active zone property.  Zone, in this context, really means Container, so we're talking about zones and sets.  Essentially, containers are components that can hold other components but aren't a drawing or a network.
	 * New components are always added to the active zone at the time of their creation.
	 * @return the Component representing the active zone.
	 */
	DLComponent getActiveZone() {
		return activeZones.get( _activeDrawing )
	}

	/**
	 * Setter for the active zone property.
	 * @see SelectModel#getActiveZone()
	 * @param the Component to make the new active zone.
	 * @throws Exception If the supplied Component is not a container.
	 */
	void setActiveZone(DLComponent g) {
        //log.trace("setActiveZone  old: ${getActiveZone()?.name}  new: ${g?.name}  (Active Drawing: ${getActiveDrawing()?.name})" )

		def oldZ = activeZone

        if( g == null ) {
            if( getActiveDrawing() != null ) {
                // Assume it is for the current active drawing.
                activeZones.put( _activeDrawing, g )
            }
        } else if( !g.isContainer() ) {
            throw new Exception("Tried to set active grouping to not a grouping!")
        } else {
            // Use the components drawing since things sometimes get set out of order.
            activeZones.put( g.getDrawing(), g )
        }

		fireActiveZoneChanged(oldZ, activeZone)
	}

	/**
	 * Getter for the active network property.  "Network", in this context, is a synonym for "data source."
	 * New components (that go in a data source) are always placed in the currently active network.
	 * @return the Component representing the currently active network.
	 */
	DLComponent getActiveNetwork() {
		return activeNetworks.get( _activeDrawing )
	}

	/**
	 * Setter for the active network property.
	 * @see SelectModel#getActiveNetwork()
	 * @param the Component to make the new active network.
	 * @throws Exception If the supplied Component is not a network.
	 */
	void setActiveNetwork(DLComponent n) {
        //log.trace("setActiveNetwork  old: ${getActiveNetwork()?.name}  new: ${n?.name}  (Active Drawing: ${getActiveDrawing()?.name})" )

		def oldDS = activeNetwork

        if( n == null ) {
            if( getActiveDrawing() != null ) {
                // Assume it is for the current active drawing.
                activeNetworks.put( _activeDrawing, n )
            }
        } else if( !n.hasClass('network') ) {
            throw new Exception("Tried to set active network to not a network!")
        } else {
            // Use the components drawing since things sometimes get set out of order.
            activeNetworks.put( n.getDrawing(), n )
        }

		fireActiveDatasourceChanged(oldDS, activeNetwork)
	}

	/**
	 * Set the current selection to the collection passed in.  Afterwards the current selection is set to exactly the collection provided.
	 * Note that no components outside of the active drawing will be selected.  Anything in the list of things to select that are not
	 * on the active drawing will be ignored.  If they are all off the active drawing, this method will have no effect.
	 * @param objects A Collection of Components to set the current selection to.
	 */
	void setSelection(def objects) {
		if (notifying) { log.trace("rejecting recursive update (setSelection) "); return }
		//log.trace("setting selection to $objects")

		//check if the incoming selection is the same as the current selection, if so, skip all this
		LinkedHashSet<DLComponent> incomingSelection = new LinkedHashSet<DLComponent>();
		incomingSelection.addAll(objects)

		LinkedHashSet<DLComponent> currentlySelectedComponents = new LinkedHashSet<DLComponent>(selectedObjects);

		//println "setSelection()"
		//println "incoming $incomingSelection"
		//println "current $currentlySelectedComponents"


		SelectionChangeEvent ev = new SelectionChangeEvent()

		//use Set.contains() rather than "in" for massive speed boost,
		//along with for-each instead of closure to get around the closure making things concurrent and slowing that down
		for (DLComponent i: incomingSelection) {
			if (!currentlySelectedComponents.contains(i) && i.getDrawing().equals(this.getActiveDrawing()) ) {
				ev.add(i)
				selectedObjects.add(i)
			}
		}

		for (DLComponent i: currentlySelectedComponents) {
			if (!incomingSelection.contains(i)) {
				ev.remove(i)
				selectedObjects.remove(i)
			}
		}


		notifyListeners(ev)
	}

	/**
	 * Add the provided collection of Components to the current selection, leaving any currently selected Components selected.
	 * Note that no components outside of the active drawing will be selected.  Anything in the list of things to select that are not
	 * on the active drawing will be ignored.  If they are all off the active drawing, this method will have no effect.
	 * @param objects A Collection of Components to add to the current selection.
	 */
	void addToSelection(java.util.Collection<DLComponent> components) {
		if (notifying) { log.trace("rejecting recursive update"); return }
		SelectionChangeEvent ev = new SelectionChangeEvent()
		components.each {
			if (!(it in selectedObjects) && it.getDrawing().equals(this.getActiveDrawing()) ) {
				ev.add(it)
				selectedObjects += it
			}
		}
		notifyListeners(ev)
	}

	/**
	 * Adds a single component to the selection
	 * @param target a DLComponent to add to the current selection
	 */
	void addToSelection(DLComponent target) {
		this.addToSelection([target]);
	}

	/**
	 * Add the provided collection of Components to the current selection, leaving any currently selected Components selected.
	 * @param objects A Collection of Components to add to the current selection.
	 */
	@Deprecated
	public void select(def objects) {
		this.addToSelection(objects);
	}

	/**
	 * Toggles the selection status of a collection of Components.  Currently selected Components in the collection are unselected, and currently unselected Components are selected.
	 * @param objects A collection of Components to toggle selection on.
	 */
	void toggleSelect(def objects) {
		if (notifying) { log.trace("rejecting recursive update"); return }
		SelectionChangeEvent ev = new SelectionChangeEvent()
		//iterate on separate list to remove wacky closure concurrency thing and wacky closure redirection
		def currentlySelectedComponents = new ArrayList(selectedObjects)
		for (DLComponent c: objects) {
			if (currentlySelectedComponents.contains(c)) {
				ev.remove(c)
				selectedObjects.remove(c)
			} else {
				ev.add(c)
				selectedObjects.add(c)
			}
		}
		currentlySelectedComponents = null //just to be clear

		notifyListeners(ev)
	}

	/**
	 * Removes the specified collection of Components from the current selection.  Components that are currently selected and not in the provided collection are left selected.
	 * @param objects A collection of Components to unselect.
	 */
	void unSelect(def objects) {
		if (notifying) { log.trace("rejecting recursive update"); return }
		SelectionChangeEvent ev = new SelectionChangeEvent()
		objects.each {
			if (it in selectedObjects) {
				ev.remove(it)
				selectedObjects -= it
			}
		}
		notifyListeners(ev)
	}

	public void removeFromSelection(java.util.Collection<DLComponent> components) {
		this.unSelect(components)
	}

	/**
	 * Clears out the current selection.
	 */
	void clearSelection() {
		if(this.selectedObjects.size() >0){
			this.setSelection([])
		}
	}

	/**
	 * Returns true if the given Components is in the current selection.
	 * @param object The Components to test for selection.
	 */
	boolean isSelected(def object) {
		return (object in selectedObjects)
	}

	/**
	 * The Expanded Selection is the list of selected components, but with Container components replaced with the list of components they contain.
	 * So, if the "normal" selection is just two Zone components, the Expanded Selection is the full list of all the Components in those two zones.
	 * @return a List of the Expanded Selection.
	 */
	public ArrayList<DLComponent> getExpandedSelection() {
		def components = []
		selectedObjects.each {
			if (it.hasClass('placeable'))
				components << it
			else if ( it.isContainerOrNetwork() )
				components.addAll(it.getChildComponents())
		}
		return components
	}

	/**
	 * Returns the list of selected components, but does not expand containers.
	 * @return a new arraylist containing the contents of the selected components
	 */
	public ArrayList<DLComponent> getBaseSelection() {
		return new ArrayList<DLComponent>(selectedObjects)
	}

	/**
	 * From the ModelChangeListener interface; called when a Component is added to the data model.
	 * @see ModelChangeListener
	 * @param component the component that has been added.
	 */
	public void componentAdded(DLComponent component) {
        if( cmodelInitializing ) return

		if (component.hasClass('drawing')){
            setActiveDrawing( component )
        }
	}

	/**
	 * From the ModelChangeListener interface; called when a Component is removed from the data model.
	 * @see ModelChangeListener
	 * @param component the component that has been removed.
	 */
	void componentRemoved(DLComponent component) {
        if( cmodelInitializing ) return

		if (isSelected(component))
			unSelect([component])

		if (activeNetwork == component)
            setActiveNetwork( null )
		if (activeZone == component)
            setActiveZone( null )

        // Let the Active Drawing model decide what to set.
		//if (activeDrawing == component)
        //    setActiveDrawing( null )
	}

	/**
	 * From the ModelChangeListener interface; called when a Component is added as a child to another Component.
	 * @see ModelChangeListener
	 * @param parent the component that has had a child added.
	 * @param child the component that has been childed.
	 */
	public void childAdded(DLComponent parent, DLComponent child) {
        // Go ahead and process during init since it is rather benign but will give us the old behavior of having the last one be the selected one.
        // if( cmodelInitializing ) return

        // Do these ones here instead of componentAdded since we want to make sure they have already been assigned to a drawing.
        if( child.isContainer() ) {
            setActiveZone( child )
        } else if( child.hasRole('network') && child.getType() != ComponentType.WSN_NETWORK ) {
	        // Skip for WSN Networks since they are auto assigned now and aren't part of the "Active" framework.
            setActiveNetwork( child )
        }
    }

	/**
	 * From the ModelChangeListener interface; called when a Component is removed from another Components list of children.
	 * Has no effect on the selection model, the method is an empty stub.
	 * @see ModelChangeListener
	 */
	public void childRemoved(DLComponent parent, DLComponent child) {}

	/**
	 * From the ModelChangeListener interface; called when an Association is added to the data model.
	 * Has no effect on the selection model, the method is an empty stub.
	 * @see ModelChangeListener
	 */
	public void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	/**
	 * From the ModelChangeListener interface; called when an Association is added to the data model.
	 * Has no effect on the selection model, the method is an empty stub.
	 * @see ModelChangeListener
	 */
	public void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

    @Override
    public void modelMetamorphosisStarting( MetamorphosisEvent event ) {
        if( event.getMode() == ModelState.INITIALIZING || event.getMode() == ModelState.REPLACING ) {
            cmodelInitializing = true
        }
    }

    @Override
    public void modelMetamorphosisFinished( MetamorphosisEvent event ) {
        if( event.getMode() == ModelState.INITIALIZING || event.getMode() == ModelState.REPLACING ) {
            cmodelInitializing = false

            // Make sure everyone knows the current active selection.
            fireActiveDrawingChanged( null, _activeDrawing )

        } else if( event.getMode() == ModelState.CLEARING ) {
            _activeDrawing = null
            activeZones.clear()
            activeNetworks.clear()
        }
    }
}

/**
 * Represents a change in the selected Components.
 * Essentially, this contains a list of Components that have been added to the selection and a list of those that have been removed.
 */
class SelectionChangeEvent {
	def drawingsAdded = []
	def componentsAdded = []
	def zonesAdded = []
	def networksAdded = []
	def groupsAdded = []

	def drawingsRemoved = []
	def componentsRemoved = []
	def zonesRemoved = []
	def networksRemoved = []
	def groupsRemoved = []

	/**
	 * Adds a Component to the list of components that have been selected.
	 * @param c the Component that has been selected.
	 */
	void add(DLComponent c) {
		if (c.hasClass('zone'))
			zonesAdded << c
		else if (c.hasClass('network'))
			networksAdded << c
		else if (c.hasClass('placeable'))
			componentsAdded << c
		else if (c.hasClass('drawing'))
			drawingsAdded << c
		else if (c.hasRole( ComponentRole.LOGICAL_GROUP ))
			groupsAdded << c
	}
	/**
	 * Adds a Component to the list of components that have been un-selected.
	 * @param c the Component that has been un-selected.
	 */
	void remove(DLComponent c) {
		if (c.hasClass('zone'))
			zonesRemoved << c
		else if (c.hasClass('network'))
			networksRemoved << c
		else if (c.hasClass('placeable'))
			componentsRemoved << c
		else if (c.hasClass('drawing'))
			drawingsRemoved << c
		else if (c.hasRole( ComponentRole.LOGICAL_GROUP ))
			groupsRemoved << c
	}

	/**
	 * Supplies the full list of all components that have been added to the selection.
	 * @return a List of newly selected Components.
	 */
	def getExpandedAddition() {
		return componentsAdded + zonesAdded.collect {it.getChildComponents()}.flatten() + networksAdded.collect {it.getChildComponents()}.flatten() + groupsAdded.collect {it.getChildComponents()}.flatten()
	}
	/**
	 * Supplies the full list of all components that have been removed from the selection.
	 * @return a List of newly un-selected Components.
	 */
	def getExpandedRemoval() {
		return componentsRemoved + zonesRemoved.collect {it.getChildComponents()}.flatten() + networksRemoved.collect {it.getChildComponents()}.flatten() + groupsRemoved.collect {it.getChildComponents()}.flatten()
	}

	/**
	 * Hands out all added components (across all categories) as a single set
	 * @return
	 */
	Set<DLComponent> getAddedSet() {
		HashSet<DLComponent> result = new HashSet<DLComponent>()
		result.addAll(drawingsAdded)
		result.addAll(componentsAdded)
		result.addAll(zonesAdded)
		result.addAll(networksAdded)
		result.addAll(groupsAdded)
		return result
	}

	/**
	 * Hands out all removed components (across all categories) as a single set
	 * @return
	 */
	Set<DLComponent> getRemovedSet() {
		HashSet<DLComponent> result = new HashSet<DLComponent>()
		result.addAll(drawingsRemoved)
		result.addAll(componentsRemoved)
		result.addAll(zonesRemoved)
		result.addAll(networksRemoved)
		result.addAll(groupsRemoved)
		return result
	}


	public String toString() {
		return "SelectionChangeEvent{" +
				"drawingsAdded=" + drawingsAdded +
				", componentsAdded=" + componentsAdded +
				", zonesAdded=" + zonesAdded +
				", networksAdded=" + networksAdded +
				", groupsAdded=" + groupsAdded +
				", drawingsRemoved=" + drawingsRemoved +
				", componentsRemoved=" + componentsRemoved +
				", zonesRemoved=" + zonesRemoved +
				", networksRemoved=" + networksRemoved +
				", groupsRemoved=" + groupsRemoved +
				'}';
	}

}

/**
 * Represents classes that can register to listen for changes to the selection model.
 *
 * There are two "optional"methods: activeZoneChanged and activeDatasourceChanged.
 * These need not be implemented if not used, the SelectModel will find them via the Meta Object Protocol.
 *
 * @see SelectModelWatcher
 */
interface SelectionChangeListener {
	/**
	 * Called when the current selection changes.
	 * @see SelectionChangeEvent
	 * @param e the SelectionChangeEvent representing the selection change.
	 * */
	void selectionChanged(SelectionChangeEvent e)

    /**
     * Called when the active drawing changes.
     *
     * @param activeDrawing The new active drawing
     */
    void activeDrawingChanged( DLComponent oldDrawing, DLComponent newDrawing )
}

/**
 * For debugging purposes, this class will log all selection change events as they happen.
 */
class SelectModelWatcher implements SelectionChangeListener{
	private static final Logger log = Logger.getLogger(SelectModelWatcher.class.getName())
	@Override
	void selectionChanged(SelectionChangeEvent e) {
		log.trace(e.toString())
	}

	@Override
	void activeDrawingChanged(DLComponent oldDrawing, DLComponent newDrawing) {
		log.trace("ADC change: $oldDrawing -> $newDrawing")
	}

	public void activeZoneChanged(DLComponent oldZone, DLComponent newZone) {
		log.trace("AGrC change: $oldZone -> $newZone")
	}

	public void activeDatasourceChanged(DLComponent oldDatasource, DLComponent newDatasource) {
		log.trace("ADSC change: $oldDatasource -> $newDatasource")
	}
}
