package DeploymentLab.Upgrade;

import DeploymentLab.CentralCatalogue
import DeploymentLab.DLProject;
import DeploymentLab.Exceptions.ModelCatastrophe
import DeploymentLab.Image.ARGBConverter
import DeploymentLab.MapHelper
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentProperty;
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLObject
import DeploymentLab.Model.Dlid
import DeploymentLab.Model.Dockingpoint
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Model.RoomControlType
import DeploymentLab.Model.RoomType
import DeploymentLab.Pair
import DeploymentLab.ProjectSettings;
import DeploymentLab.channellogger.Logger;


/**
 * Handles the migration of projects to the v7.0 object model where control objects are members of the environmental
 * component and not standalone components connected through associations. We also want to remove the old strategies
 * since they will be completely replaced by new ones.
 *
 * @author Ken Scoggins
 * @since  Aireo
 */
public class MigrateControlObjects extends UpgradeFacet {
	private static final Logger log = Logger.getLogger(MigrateControlObjects.class.getName());

	// Env Component -> [Control Device Components]
	Map<DLComponent,List<DLComponent>> deviceMap = [:]

	// Control Device Component -> {Device Type, Config Type}
	Map<DLComponent,Pair<String,String>> configMap = [:]

	@Override
	public boolean requirementsCheck( UpgradeController uc, double fromVersion ) {
		return fromVersion < 3.0 && !uc.isManualUpgrade()
	}

	@Override
	public List<String> preUpdate( UpgradeController uc ) {

		List<String> messages = new ArrayList<>();

		// Remove all strategies with wreckless abandon.
		List<DLComponent> strategies = uc.getProject().getComponentModel().getComponentsInRole('controlstrategy');

		log.info("Removing " + strategies.size() + " deprecated control strategy components.");

		for( DLComponent strat : strategies ) {
			uc.getProject().getComponentModel().remove( strat )
		}

		// Remove all devices but save the association to migrate the settings post upgrade since the association will get lost.
		List<DLComponent> devices = uc.getProject().getComponentModel().getComponentsInRole('controlleddevice');

		log.info("Removing " + devices.size() + " deprecated control device components.");

		for( DLComponent dev : devices ) {

			List<DLComponent> envs = dev.getAllAssociatedComponents()

			// Shouldn't happen, but let someone know just in case.
			if( envs.size() > 1 ) {
				log.error("Device $dev is associated with more than one component ($envs). Some data may not upgrade.")
			}

			for( DLComponent env : envs ) {
				if( env.hasManagedObjectOfType('crac') ) {
					MapHelper.addList( deviceMap, env, dev )

					// Snag the device & config types now to save some hassle later.
					DLObject co = getControlObject( dev )

					if( co == null ) {
						messages.add( CentralCatalogue.formatUIS( "fileupgrade.controlMigrate.oldObjectNotFound", dev ) )
					} else {
						configMap[dev] = new Pair<>( co.getType(), co.getObjectProperty('config').getReferrent().getType() )
					}
				}
			}

			uc.getProject().getComponentModel().remove( dev )
		}

		// Remove all Regions of Influence.
		List<DLComponent> reins = uc.getProject().getComponentModel().getComponentsInRole('set');

		log.info("Removing " + reins.size() + " deprecated Regions of Influence.");

		for( DLComponent rein : reins ) {

			log.info( "Removing " + rein.getChildComponents().size() + " components from Region '" + rein.getName() + "'" );

			for( DLComponent child : rein.getChildComponents() ) {
				rein.removeChild( child );

				// An orphaned child would be a bug that needs to be fixed since devices and strategies were already removed.
				// ... unless it is deprecated because nobody wants it.
				if( child.getParentComponents().isEmpty() && !child.isDeprecated() ) {
					throw new ModelCatastrophe( CentralCatalogue.formatUIS( "fileupgrade.reinRemoval.childAbandoned", rein.getName(), child.getName() ) );
				}
			}

			// Make sure we didn't leave any REIN children, which is probably a bug.
			if( !rein.getChildComponents().isEmpty() ) {
				log.error( "Removed REIN '" + rein.getName() + "' still had children: " + rein.getChildComponents() );
				throw new ModelCatastrophe( CentralCatalogue.formatUIS( "fileupgrade.reinRemoval.childHostage", rein.getName(), rein.getChildComponents().size() ) );
			}

			uc.getProject().getComponentModel().remove( rein );
		}

		return messages
	}

	public List<String> postUpdate( UpgradeController uc ) {

		// For readability since they are used a few times.
		ProjectSettings ps = uc.getProject().getProjectSettings()
		ComponentModel  cm = uc.getProject().getComponentModel()

		List<String> messages = new ArrayList<>();

		for( DLComponent cOld : deviceMap.keySet() ) {

			// We need the new component created during the upgrade.
			Dlid newDlid = ps.findNewDlid( cOld.getDlid() )
			DLComponent cNew = cm.getComponentFromDLID( newDlid )

			if( cNew == null ) {
				messages.add( CentralCatalogue.formatUIS( "fileupgrade.controlMigrate.newCompNotFound", cOld ) );
			} else {
				// No rooms yet, so set the inherited props so that the device parcels are active and we have the device props.
				cNew.setPropertyValue( RoomType._PROPERTY, RoomType.SLAB )
				cNew.setPropertyValue( RoomControlType._PROPERTY, RoomControlType.REMOTE )

				for( DLComponent devOld : deviceMap.get( cOld ) ) {

					String msg = null
					Pair<String,String> ctypes = configMap[devOld]

					// Set the device property so Artisan will create the device specific properties, then copy!
					String dtypeValue = getDeviceSetting( ctypes.b )

					if( dtypeValue == null ) {
						messages.add( CentralCatalogue.formatUIS( "fileupgrade.controlMigrate.unknownConfigType", ctypes.b, cOld ) );
						//throw new ModelCatastrophe("Unexpected Device Config '$ctypes.b' in $cOld")
					} else {
						switch( ctypes.a ) {
							case "controlout_crah":
							case "controlout_crac":
								cNew.setPropertyValue( 'controlDeviceTemp', dtypeValue )
								copyProperties( devOld, cNew, 'td_' )
								break

							case "controlout_vfd":
								println "$cNew  ${cNew.listProperties(  )}"
								cNew.setPropertyValue( 'controlDeviceVfd', dtypeValue )
								copyProperties( devOld, cNew, 'vd_' )
								break

							default:
								messages.add( CentralCatalogue.formatUIS( "fileupgrade.controlMigrate.unknownDeviceType", ctypes.a, cOld ) );
								break
						}

						if( msg != null ) {
							messages.add( msg );
						}
					}
				}
			}
		}

		deviceMap.clear()

		return null;
	}

	private String getDeviceSetting( String cfgType ) {
		String setting = null

		if( cfgType.contains( 'liebert' ) ) {
			setting = 'liebert'
		} else if( cfgType.contains( 'ach550' ) ) {
			setting = 'ach550'
		} else if( cfgType.contains( 'modbus' ) ) {
			setting = 'gmodbus'
		} else if( cfgType.contains( 'bacnet' ) ) {
			setting = 'gbacnet'
		}

		return setting
	}

	private DLObject getControlObject( DLComponent c ) {
		for( DLObject o : c.getManagedObjects() ) {
			if( o.getType().startsWith('controlout_') ) {
				return o
			}
		}
		return null
	}

	// This is a partial copy of what is in ConfigurationReplacer. Should really refactor that one so it can be reused directly.
	private void copyProperties( DLComponent oldComp, DLComponent newComp, String prefix ) {

		for( String oldProp : oldComp.listProperties() ) {
			// The new ones have a prefix to avoid name collision since they are all in one component.
			String newProp = prefix + oldProp

			if( newComp.hasProperty( newProp ) ) {
				newComp.setPropertyValue( newProp, oldComp.getPropertyValue( oldProp ) )
			} else {
				log.trace("Missing property '$newProp' in new component $newComp")
			}
		}
	}
}
