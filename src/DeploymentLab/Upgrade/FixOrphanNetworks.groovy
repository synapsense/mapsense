package DeploymentLab.Upgrade

import DeploymentLab.channellogger.Logger
import DeploymentLab.CentralCatalogue

/**
 * Custom upgrade processing to fix an issue caused when installation processes are not followed correctly. In short,
 * Java 7 was not installed prior to running a newly installed version of MapSense that required it. This caused a
 * situation where the network object upgrade fails and the old object remains in the project as an orphan with the same
 * properties as the new object.
 *
 * @see <a href="http://bugzilla.synapsense.int/show_bug.cgi?id=8255">Bug 8255</a>
 *
 * @author Gabriel Helman
 * @since Jupiter 1.1
 * Date: 10/11/12
 * Time: 4:37 PM
 */
class FixOrphanNetworks extends UpgradeFacet {
	private static final Logger log = Logger.getLogger(FixOrphanNetworks.class.getName())

	@Override
	public boolean requirementsCheck( UpgradeController uc, double fromVersion ) {
		// No need to limit by version since it doesn't hurt to run the check all the time.
		return true
	}

	@Override
	public List<String> preUpdate(UpgradeController uc) {
		def messages = []

		def nets = uc.getProject().getComponentModel().getComponentsByType("network")
		for(def n : nets){
			log.trace("look at net $n")
			if(n.getParentComponents().size() == 0 && n.getChildComponents().size() == 0){
				uc.getProject().getComponentModel().remove(n)
				messages.add( CentralCatalogue.getUIS("fileupgrade.orphanedWSN") );
			}
		}

		return messages
	}
}
