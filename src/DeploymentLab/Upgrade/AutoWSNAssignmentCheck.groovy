package DeploymentLab.Upgrade

import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentType
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.WSNController
import DeploymentLab.channellogger.Logger

/**
 * @author Vadim Gusev
 * @since Aireo
 * Date: 22.01.14
 * Time: 16:44
 */
class AutoWSNAssignmentCheck extends UpgradeFacet {
	private static final Logger log = Logger.getLogger(AutoWSNAssignmentCheck.class.getName())

	@Override
	boolean requirementsCheck(UpgradeController uc, double fromVersion) {
		return fromVersion < 3.0 && !uc.isManualUpgrade()
	}

	@Override
	public List<String> postUpdate(UpgradeController uc) {
		ComponentModel cm = uc.getProject().getComponentModel()

		for (DLComponent node : cm.getComponents()) {
			// Skip proxies.
			if( node.hasRole("proxy") ) continue

			if (cm.typeCanBeChild(ComponentType.WSN_NETWORK, node.getType(), true) && !node.getType().equals(ComponentType.WSN_GATEWAY)) {
				DLComponent network = WSNController.findNearestNetworkForComponent(node)
				if (network) {
					def parentNetwork = node.getParentsOfType(ComponentType.WSN_NETWORK)
					if (parentNetwork.isEmpty() || !parentNetwork.first().equals(network)) {
						log.info("Enable manual WSN config for $node")
						node.setPropertyValue("manualWSNConfig", true)
					}
				}
			}
		}

		return null
	}

}
