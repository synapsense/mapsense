package DeploymentLab.Upgrade

import DeploymentLab.CentralCatalogue
import DeploymentLab.Model.DLObject
import DeploymentLab.Model.Setting

/**
 * WSN Network version N29 is the only version currently supported. The normal upgrade process will set the version
 * property to whatever it was in the old project file. This will force the version to N29 and also warn the user, just
 * in case this will require an upgrade to some old hardware.
 *
 * @author Ken Scoggins
 * @since 6.7
 */
class UpgradeOldNetworks extends UpgradeFacet {
	@Override
	public boolean requirementsCheck( UpgradeController uc, double fromVersion ) {
		// Only need to check when upgrading from pre 6.7 (omodel 2.7).
		return fromVersion < 2.7
	}

	@Override
	public List<String> preUpdate( UpgradeController uc ) {
		List<String> messages = []

		for( DLObject o : uc.getProject().getObjectModel().getObjectsByType('wsnnetwork') ) {
			Setting prop = o.getObjectSetting('version')
			if( prop.getValue() != 'N29' ) {
				messages.add( CentralCatalogue.formatUIS("fileupgrade.oldWSN", uc.getProject().getComponentModel().getOwner(o).getName(), prop.getValue() ));
				prop.setValue ('N29')
			}
		}

		return messages
	}
}
