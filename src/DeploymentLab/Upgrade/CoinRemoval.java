package DeploymentLab.Upgrade;

import DeploymentLab.*;
import DeploymentLab.Exceptions.IncompatibleTypeException;
import DeploymentLab.Model.*;
import DeploymentLab.channellogger.Logger;

import java.util.*;


/**
 * Handles migrating projects that have COIN Components to the new model where COIN Components no longer exist and the
 * control object is embedded in all components that are control eligible.
 *
 * @author Ken Scoggins
 * @since  Io
 */
public class CoinRemoval extends UpgradeFacet {
	private static final Logger log = Logger.getLogger(CoinRemoval.class.getName());

	// List of [ COIN, Associated Control Component ] pairs. Done this way since the association is lost during upgrade.
	List<Pair<DLComponent,DLComponent>> originalCoins = new ArrayList<>();

	// Map of original "ObjectDlid::DockingpointName" -> Original Referrent Sensor DLID
	Map<String,Dlid> originalDocks = new HashMap<>();

	// A couple mappings to handle the special cases for dual inlet and Hor Row.
	Map<DLComponent,String> dualInletSide = new HashMap<>();
	Map<DLComponent,String> hRowChildName = new HashMap<>();

	@Override
	public boolean requirementsCheck( UpgradeController uc, double fromVersion ) {
		return ( fromVersion < 2.4 ) && ( DeploymentLabVersion.getModelVersionByDouble() >= 2.4 );
	}

	@Override
	public List<String> preUpdate( UpgradeController uc ) {

		List<DLComponent> coins = uc.getProject().getComponentModel().getComponentsInRole("controlinput");
		log.debug("Migrating " + coins.size() + " COIN Components");

		// Snag all the original COINs and their associated component for later use.
		for( DLComponent coin : coins ) {
			// Slam into a set to get unique entries since there could be multiple attachment.
			Set<DLComponent> comps = new HashSet<>( coin.getConsumerProducers() );

			// Sanity check.
			if( comps.size() != 1 ) {
				log.warn( coin + " is associated with " + comps.size() + " and we expected only 1");
			}

			for( DLComponent c : comps ) {
				originalCoins.add( new Pair<>( coin, c ) );

				// Grab the sensors we are hooked to so we don't have to jump through hoops later when the association is gone.
				DLObject ctrlObj = getControlObject( coin, null, null );
				for( Property p : ctrlObj.getPropertiesByType("dockingpoint") ) {
					Dockingpoint dock = (Dockingpoint) p;
					originalDocks.put( ctrlObj.getDlid() + "::" + p.getName(), dock.getReferrent().getDlid() );
				}

				// A little extra work for some components.
				String producerId = coin.getConsumer( coin.getType().contains("strat") ? "input_top" : "input" ).listProducers().get(0).b;

				if( c.getObjectsOfType("rack").size() == 2 ) {
					// Probably a dual inlet rack so we also need to know what side the coin was associated with.
					if( producerId.startsWith("rackA") ) {
						dualInletSide.put( coin, "rack1");
					} else if( producerId.startsWith("rackB") ) {
						dualInletSide.put( coin, "rack2");
					} else {
						log.error("Component has " + c.getObjectsOfType("rack").size() +
						          " racks that aren't a handled type. COIN: " + coin + "  Associated Comp: " + c );
					}
				} else if( c.getObjectsOfType("rack").size() > 2 ) {
					// Old style horizontal row with embedded children and producers. Need to determine the name of the
					// standalone child in the new version and save it for later.
					if( c.getType().equals("single-horizontal-row-thermanode2") ) {
						// temp# --> c#
						hRowChildName.put( coin, "c" + producerId.charAt(4) );
					} else if( c.getType().equals("dual-horizontal-row-thermanode2")) {
						// rack#topinlet --> rack#
						hRowChildName.put( coin, producerId.substring(0, 5) );
					} else {
						log.error("Component has " + c.getObjectsOfType("rack").size() +
						          " racks that aren't a handled type. COIN: " + coin + "  Associated Comp: " + c );
					}
				}
			}
		}

		return null;
	}

	@Override
	public List<String> postUpdate( UpgradeController uc ) {

		// For readability since they are used a few times.
		ProjectSettings projectSettings = uc.getProject().getProjectSettings();
		ComponentModel  componentModel  = uc.getProject().getComponentModel();
		ObjectModel     objectModel     = uc.getProject().getObjectModel();

		List<String> messages = new ArrayList<>();

		for( Pair<DLComponent,DLComponent> pair : originalCoins ) {
			DLComponent coin = pair.a;
			DLComponent cOld = pair.b;

			// We need the new component created during the upgrade.
			DLComponent cNew = componentModel.getComponentFromDLID( projectSettings.findNewDlid( cOld.getDlid() ) );

			if( cNew == null ) {
				messages.add( CentralCatalogue.formatUIS("fileupgrade.coinRemoval.newCompNotFound", cOld, coin ) );
			} else {
				// Get the old and new control objects.
				DLObject oldCtrlObj = getControlObject( coin, null, null );
				if( oldCtrlObj == null ) {
					messages.add( CentralCatalogue.formatUIS("fileupgrade.coinRemoval.oldObjectNotFound", coin ) );
				} else {
					DLObject newCtrlObj = getControlObject( cNew, oldCtrlObj, coin );

					if( newCtrlObj == null) {
						messages.add( CentralCatalogue.formatUIS("fileupgrade.coinRemoval.newObjectNotFound", cNew, coin ) );
					} else {
						String msg = copyObject(coin, oldCtrlObj, cNew, newCtrlObj, uc.getProject());
						if( msg != null ) {
							messages.add( msg );
						}
					}
				}
			}

			componentModel.remove(coin);

			// If the project had been exported before, the old COIN may be hanging around in an "isDeleted" state. We don't
			// want that since we don't want the next export to try to delete the object that we just re-childed to something else.
			for( DLObject o : objectModel.listDeletedObjects() ) {
				if( o.getType().startsWith("controlpoint_") ) {
					objectModel.prune( o );
				}
			}
		}

		// Make sure we release the cache.
		originalCoins.clear();
		originalDocks.clear();
		dualInletSide.clear();
		hRowChildName.clear();

		return messages;
	}


	private DLObject getControlObject( DLComponent cNew, DLObject oldCtrlObj, DLComponent oldCoin ) {
		List<DLObject> cpObjs;

		if( oldCtrlObj == null ) {
			// Find whatever is there.
			cpObjs = cNew.getObjectsOfType("controlpoint_singlecompare");

			if( cpObjs.isEmpty() ) {
				cpObjs = cNew.getObjectsOfType("controlpoint_strat");
			}
		} else {
			// Get what we're told to get.
			cpObjs = cNew.getObjectsOfType( oldCtrlObj.getType() );
		}

		DLObject ctrlObj = null;
		if( (cpObjs == null) || cpObjs.isEmpty() ) {
			// Might be an old style horizontal row. Try to find it in the correct child component.
			String childName = hRowChildName.get( oldCoin );

			if( childName == null ) {
				// Bad news.
				log.error("No " + (oldCtrlObj == null ? "" : oldCtrlObj.getType() + " ") + "control object found in " + cNew + ". New object not upgraded.");
			} else {
				for( DLComponent kid : cNew.getChildComponents() ) {
					if( kid.getPropertyStringValue("child_id").equals( childName ) ) {
						ctrlObj = getControlObject( kid, oldCtrlObj, oldCoin );
					}
				}
			}
		} else if( cpObjs.size() == 1 ) {
			// Ah, the easy case.
			ctrlObj = cpObjs.iterator().next();
		} else if( cpObjs.size() == 2 ) {
			// Probably a dual inlet. Need to pick the correct side.
			String sideName = dualInletSide.get( oldCoin );

			if( sideName == null ) {
				log.error("New component has multiple objects that can't be matched up. Old COIN: " +
				          oldCoin + "  New Comp: " + cNew + "  New Objects: " + cpObjs );
			} else {
				for( DLObject o : cpObjs ) {
					for( DLObject p : o.getParents() ) {
						if( p.getAsName().equals( sideName ) ) {
							ctrlObj = o;
						}
					}
				}
			}
		}

		return ctrlObj;
	}


	// This is a partial copy of what is in ConfigurationReplacer. Should really refactor that one so it can be reused directly.
	private String copyObject( DLComponent oldComp, DLObject oldObj, DLComponent newComp, DLObject newObj, DLProject project ) {

		String msg = null;

		if( !oldObj.getType().equals(newObj.getType()) ) {
			log.trace("Objects not same type, not updating: " + oldObj + " -> " + newObj );
			return null;
		}

		newObj.setKey(oldObj.getKey());

		for( Property s : oldObj.getPropertiesByType("setting") ) {
			Setting oldProp = (Setting) s;
			if (oldProp.getOldValue() != null && newObj.hasObjectProperty(oldProp.getName())) {
				Setting hotProp = (Setting) newObj.getObjectProperty(oldProp.getName());
				hotProp.setOldValue(oldProp.getOldValue());
			}

			if (oldProp.getOverride() != null && newObj.hasObjectProperty(oldProp.getName())) {
				Setting hotProp = (Setting) newObj.getObjectProperty(oldProp.getName());
				hotProp.setOverride(oldProp.getOverride());
			}

			//special case for sync timestamp
			if (oldProp.getName().equalsIgnoreCase("lastExportTs")) {
				Setting hotProp = (Setting) newObj.getObjectProperty(oldProp.getName());
				hotProp.setValue(oldProp.getValue());
			}
		}

		//same thing, but for oldvalues and overrides on tags
		for( Property oldProp : oldObj.getProperties() ) {
			if (newObj.hasObjectProperty(oldProp.getName())) {
				if (oldProp.hasTags()) {
					Property newProp = newObj.getObjectProperty(oldProp.getName());
					for( CustomTag oldTag : oldProp.getTags() ) {
						if (newProp.hasTag(oldTag.getTagName())) {
							CustomTag newTag = newProp.getTag(oldTag.getTagName());
							newTag.setOldValue(oldTag.getOldValue());
							newTag.setOverride(oldTag.getOverride());
						}
					}
				}
			}
		}

		// We also need to make sure the dockingpoints are linked to the same target as before and not just the default.
		for( Property p : oldObj.getPropertiesByType("dockingpoint") ) {
			Dockingpoint oldDock = (Dockingpoint) p;
			Dockingpoint newDock = (Dockingpoint) newObj.getObjectProperty( oldDock.getName() );

			Dlid newDlid = project.getProjectSettings().findNewDlid( originalDocks.get(oldObj.getDlid() + "::" + p.getName()) );
			DLObject newRef = project.getObjectModel().getObjectFromDlid( newDlid );

			if( newRef == null ) {
				log.error("Referent not found, not updating property: " + oldObj + "::" + oldDock );
				msg = "Unable to migrate control position sensor link. The control position must be reset manually. " + newComp;
			} else {
				try {
					newDock.setReferrent( newRef );
				} catch( IncompatibleTypeException e ) {
					log.error( e.getMessage() + " Not updating property: " + oldObj + "::" + oldDock );
					msg = "Unable to migrate control position sensor link. The control position must be reset manually. " + newComp;
				}
			}
		}

		// New property: Set 'position' based on the old component type. Default is already the top/top-middle.
		String newVal = null;
		switch( oldComp.getType() ) {
			case "controlpoint_single_temperature_mid" :
				newVal = "m";
				break;
			case "controlpoint_single_temperature_bot" :
				newVal = "b";
				break;
			case "control_input_strat_tm" :
				newVal = "tm";
				break;
			case "control_input_strat_mb" :
				newVal = "mb";
				break;
		}

		if( newVal != null ) {
			newObj.getObjectSetting("position").setValue( newVal );
		}

		return msg;
	}
}
