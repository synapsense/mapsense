package DeploymentLab.Upgrade;

import DeploymentLab.Model.DLComponent;
import java.util.List;

/**
 * Defines the many facets of the project file upgrade process. This class is extended in order to insert custom logic
 * into the upgrade process. The concrete class implements the methods necessary to insert the custom logic into the
 * desired phase of the process.
 *
 * @author Ken Scoggins
 * @since Io
 */
public abstract class UpgradeFacet {

	/**
	 * Implemented by concrete classes to notify the controller if this plug-in should be included in the upgrade. This
	 * allows plug-ins to run only under specific conditions, such as upgrades from or two specific versions.
	 *
	 * @param uc           The Upgrade Controller instance containing project references.
	 * @param fromVersion  The version of the project prior to this upgrade running.
	 *
	 * @return  True if this plug-in should be included in the upgrade. False is it should be skipped.
	 */
	abstract boolean requirementsCheck( UpgradeController uc, double fromVersion );

	/**
	 * Called prior to the models being updated to the latest version. The models will be in the state pertaining to
	 * the version provided in {@link #requirementsCheck(UpgradeController, double)}.
	 *
	 * @param uc  The Upgrade Controller instance containing project references.
	 *
	 * @return  Optional log messages to display to the user at the end of the upgrade process. NULL or an empty list
	 *          if there are no messages.
	 */
	public List<String> preUpdate( UpgradeController uc ) {
		return null;
	}

	/**
	 * Called during the project upgrade after each new component has been created but before it has been updated.
	 * This allows for any tweaks necessary before properties are copied and parents are added.
	 *
	 * @param uc  The Upgrade Controller instance containing project references.
	 *
	 * @return  Optional log messages to display to the user at the end of the upgrade process. NULL or an empty list
	 *          if there are no messages.
	 */
	public List<String> preComponentUpdate( UpgradeController uc, DLComponent oldComponent, DLComponent newComponent ) {
		return null;
	}

	/**
	 * Called during the project upgrade after each new component has been fully updated and has replaced the old
	 * component in the model.
	 *
	 * @param uc  The Upgrade Controller instance containing project references.
	 *
	 * @return  Optional log messages to display to the user at the end of the upgrade process. NULL or an empty list
	 *          if there are no messages.
	 */
	public List<String> postComponentUpdate( UpgradeController uc, DLComponent oldComponent, DLComponent newComponent ) {
		return null;
	}

	/**
	 * Called after the models have been updated to the latest version. The models will be in the state pertaining to
	 * the current version as defined by {@link DeploymentLab.DeploymentLabVersion}.
	 *
	 * @param uc  The Upgrade Controller instance containing project references.
	 *
	 * @return  Optional log messages to display to the user at the end of the upgrade process. NULL or an empty list
	 *          if there are no messages.
	 */
	public List<String> postUpdate( UpgradeController uc ) {
		return null;
	}
}
