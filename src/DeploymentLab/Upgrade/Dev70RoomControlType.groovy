package DeploymentLab.Upgrade

import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentRole
import DeploymentLab.Model.ComponentType
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.RoomControlType
import DeploymentLab.Model.RoomType
import DeploymentLab.channellogger.Logger

/**
 * Upgrades projects created before the Room Control Type was created so that their control config is not lost. Since
 * device config is now only available based on the room and control type, all the device settings would be lost if
 * we don't set the control type on the room before the devices are upgraded.
 *
 * This should only be needed for a short time during Aireo/SS7/SZ8 dev to save some people some grief
 *
 * @author Ken Scoggins
 * @since Aireo
 */
public class Dev70RoomControlType extends UpgradeFacet {
    private static final Logger log = Logger.getLogger(Dev70RoomControlType.class.getName());

    private ComponentModel cmodel;
    private boolean doIt = false

    @Override
    public boolean requirementsCheck( UpgradeController uc, double fromVersion ) {
        // Only check once since checks post upgrade will fail.
        if( cmodel == null && uc.isManualUpgrade() ) {
            cmodel = uc.getProject().getComponentModel()
            if( cmodel.getComponentsInRole( ComponentRole.CONTROL_DEVICE ) ) {
                List<DLComponent> rooms = cmodel.getComponentsByType( ComponentType.ROOM )
                if( !rooms.isEmpty() ) {
                    // See if this is a project from before room had controlType.
                    if( !rooms.first().hasProperty("controlType") ) {
                        doIt = true
                    }
                }
            }
        }

        return doIt
    }

    @Override
    public List<String> preComponentUpdate( UpgradeController uc, DLComponent oldComponent, DLComponent newComponent ) {
        if( newComponent.hasRole( ComponentRole.CONTROLLABLE ) ) {
            // When the room was upgraded, all the old room children were migrated to the new room,
            // so this old component should already have the new room as a parent.
            DLComponent newRoom = oldComponent.getRoom()

            // Need to force the inherited prop since it doesn't have the parent hooked up yet. Otherwise, control props won't get copied.
            newComponent.setPropertyValue( RoomType._PROPERTY, newRoom.getPropertyStringValue( RoomType._PROPERTY ) )
            newComponent.setPropertyValue( RoomControlType._PROPERTY, newRoom.getPropertyStringValue( RoomControlType._PROPERTY ) )
        }

        return null
    }


    @Override
    public List<String> postComponentUpdate( UpgradeController uc, DLComponent oldComponent, DLComponent newComponent ) {
        if( newComponent.getType() == ComponentType.ROOM  ) {
            String roomType = newComponent.getPropertyStringValue( RoomType._PROPERTY )

            // Just set to the value that exposes all the devices. Can be changed back by user, but at least this will copy everything.
            if( roomType == RoomType.RAISED_FLOOR ) {
                newComponent.setPropertyValue( RoomControlType._PROPERTY, RoomControlType.PRESSURE_TEMP )
            } else if( roomType == RoomType.SLAB ) {
                newComponent.setPropertyValue( RoomControlType._PROPERTY, RoomControlType.REMOTE )
            }
        }

        return null
    }
}
