package DeploymentLab.Upgrade;

import DeploymentLab.Model.DLObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import DeploymentLab.CentralCatalogue;
import DeploymentLab.Exceptions.ModelCatastrophe;
import DeploymentLab.Model.ComponentModel;
import DeploymentLab.Model.DLComponent;
import DeploymentLab.ProjectSettings;
import DeploymentLab.channellogger.Logger;


/**
 * Handles migrating projects that have Zones. They no longer exist and need to be removed in a safe manner.
 *
 * @author Ken Scoggins
 * @since  Aireo
 */
public class ZoneRemoval extends UpgradeFacet {
	private static final Logger log = Logger.getLogger(ZoneRemoval.class.getName());

	// DC -> Old Zone Kids
	Map<DLComponent,List<DLComponent>> cache = new HashMap<>();

	@Override
	public boolean requirementsCheck( UpgradeController uc, double fromVersion ) {
		return fromVersion < 3.0;
	}

	@Override
	public List<String> preUpdate( UpgradeController uc ) {

		// Need a cache per DC so we can add the zone members to the orphanage after the upgrade.
		for( DLComponent dc : uc.getProject().getComponentModel().getComponentsByType("datacenter") ) {
			cache.put( dc, new ArrayList<DLComponent>() );
		}

		List<DLComponent> zones = uc.getProject().getComponentModel().getComponentsByType("zone");

		log.info("Removing " + zones.size() + " deprecated Zones.");

		for( DLComponent zone : zones ) {

			log.info("Removing " + zone.getChildComponents().size() + " components from Zone '" + zone.getName() + "'");

			List<DLComponent> myCache = cache.get( zone.getDrawing() );

			for( DLComponent child : zone.getChildComponents() ) {

				myCache.add( child );

				zone.removeChild( child );

				// An orphaned child would be a bug that needs to be fixed, unless it is deprecated.
				if( child.getParentComponents().isEmpty() && !child.isDeprecated() ) {
					log.warn( CentralCatalogue.formatUIS( "fileupgrade.zoneRemoval.childAbandoned", zone.getName(), child.getName() ) );
					//throw new ModelCatastrophe( CentralCatalogue.formatUIS( "fileupgrade.zoneRemoval.childAbandoned", zone.getName(), child.getName() ) );
				}
			}

			// Make sure we didn't leave any Zone children, which is probably a bug.
			if( !zone.getChildComponents().isEmpty() ) {
				log.error( "Removed Zone '" + zone.getName() + "' still had children: " + zone.getChildComponents() );
				throw new ModelCatastrophe( CentralCatalogue.formatUIS("fileupgrade.zoneRemoval.childHostage", zone.getName(), zone.getChildComponents().size()) );
			}

			List<DLObject> objs = zone.getManagedObjects();

			uc.getProject().getComponentModel().remove( zone );

			// If the project had been exported before, the old ZONE may be hanging around in an
			// "isDeleted" state. We don't want that. Even if we supported server upgrades to Aireo,
			// it would be a server script and deleting on export wouldn't be needed.
			for( DLObject o : objs ) {
				uc.getProject().getObjectModel().prune( o );
			}
		}

		return null;
	}

	public List<String> postUpdate( UpgradeController uc ) {

		ProjectSettings ps = uc.getProject().getProjectSettings();
		ComponentModel cm = uc.getProject().getComponentModel();

		List<String> messages = new ArrayList<>();

		// Find all the upgraded components and add the zoneless components to the orphanage until Rooms are drawn.
		for( DLComponent dcOld : cache.keySet() ) {
			DLComponent drawing = cm.getComponentFromDLID( ps.findNewDlid( dcOld.getDlid() ) );

			if( drawing == null ) {
				throw new ModelCatastrophe("No upgraded Drawing found for " + dcOld );
			} else {
				DLComponent orphanage = cm.getOrphanage( drawing );

				for( DLComponent cOld : cache.get( dcOld ) ) {
					DLComponent cNew = cm.getComponentFromDLID( ps.findNewDlid( cOld.getDlid() ) );

					if( cNew == null ) {
						// Probably deprecated so there is no upgraded component.
						orphanage.addChild( cOld );

						if( cOld.getParentComponents().isEmpty() ) {
							throw new ModelCatastrophe("Deprecated component couldn't find a new parent during the upgrade " + cOld );
						}
					} else {
						orphanage.addChild( cNew );
					}
				}
			}
		}

		cache.clear();

		return messages;
	}
}
