package DeploymentLab.Upgrade

import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentType
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLObject
import DeploymentLab.Model.ObjectModel

/**
 * @author Vadim Gusev
 * @since Aireo
 * Date: 23.01.14
 * Time: 18:05
 */
class InsertOrphanage extends UpgradeFacet {
	@Override
	boolean requirementsCheck(UpgradeController uc, double fromVersion) {
		return fromVersion < 3.0 && !uc.isManualUpgrade()
	}

	@Override
	public List<String> postUpdate(UpgradeController uc) {
		ComponentModel cm = uc.getProject().getComponentModel()
		ObjectModel om = uc.getProject().getObjectModel()

		DLComponent orphanage = cm.newComponent( ComponentType.ORPHANAGE )
		orphanage.getDrawing().addChild(orphanage)
		orphanage.setPropertyValue('name', "Orphanage Room")

		String points = "0,0;10,0;10,10;0,10";
		orphanage.setPropertyValue("points", points);

		def kids = [] as Set

		for( String childType : cm.listChildObjectTypes( orphanage.getType() ) ) {
			for( DLObject childObject : om.getObjectsByType( childType ) ) {
				kids.add( cm.getManagingComponent( childObject ) )
			}
		}

		for( DLComponent c : kids ) {
			orphanage.addChild( c )
		}

		return null
	}
}
