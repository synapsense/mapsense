package DeploymentLab.Upgrade;

import DeploymentLab.Model.DLComponent;
import java.util.ArrayList;
import java.util.List;

import DeploymentLab.CentralCatalogue;
import DeploymentLab.DLProject;
import DeploymentLab.DeploymentLabVersion;
import DeploymentLab.Model.ModelState;
import DeploymentLab.channellogger.Logger;

/**
 * Manages the framework for project file upgrades. The upgrade framework allows custom logic to be inserted into the
 * upgrade process. To insert custom logic, perform the following steps:
 * <ol>
 * <li>Extend {@link UpgradeFacet}.
 * <li>Implement {@link UpgradeFacet#requirementsCheck(UpgradeController, double)}.
 * <li>Override one or more of the upgrade phase methods to insert the custom logic where desired.
 * <li>Add the new concrete class to the hardcoded plugins list in {@link #UpgradeController(DLProject)}.
 * </ol>
 *
 * @author Ken Scoggins
 * @since Io
 */
public class UpgradeController {
	private static final Logger log = Logger.getLogger(UpgradeController.class.getName());

	private DLProject project;
	private boolean isManualUpgrade = false;
	private List<UpgradeFacet> plugins = new ArrayList<>();
	List<String> pluginMsgs = new ArrayList<>();
	private double startingVersion;

	public UpgradeController( DLProject project, boolean isForcedUpgrade ) {
		this( project );
		this.isManualUpgrade = isForcedUpgrade;
	}

	public UpgradeController(DLProject project) {
		this.project = project;

		if( project.getConfigurationReplacer() != null ) {
			project.getConfigurationReplacer().setUpgradeController( this );
		}

		// No fancy plug-in auto-discovery. Hardcode so we can control the order and repeatability.
		plugins.add( new CoinRemoval() );
		plugins.add( new MigrateControlObjects() );
		plugins.add( new ZoneRemoval() );
		plugins.add( new UpgradeOldNetworks() );
		plugins.add( new FixOrphanNetworks() );
		plugins.add( new AutoWSNAssignmentCheck() );
		plugins.add( new Dev70RoomControlType() );
	}

	public DLProject getProject() {
		return project;
	}

	/**
	 * Flag to let you know if this was a manual upgrade triggered, most likely, during development. Some upgrades may
	 * be one-time only or bad things can happen and therefore should only run during the normal natural automatic upgrade.
	 *
	 * @return True if this has been manually triggered. False if this is a normal, natural automatic upgrade.
	 */
	public boolean isManualUpgrade() {
		return isManualUpgrade;
	}

	/**
	 * Runs through the model upgrade process, calling implemented plug-in methods at the appropriate time.
	 *
	 * @param startingVersion  The model version at the start of the upgrade process.
	 */
	public void performUpgrade( double startingVersion ) {

		this.startingVersion = startingVersion;

		project.getComponentModel().metamorphosisStarting( ModelState.REPLACING );

		// Run all the plug-ins that want to do something before we upgrade the models.
		for( UpgradeFacet plugin : plugins ) {
			if( plugin.requirementsCheck( this, startingVersion ) ) {
				log.trace("Calling preUpdate on " + plugin.getClass().getName() );

				List<String> messages = plugin.preUpdate( this );

				if( messages != null ) {
					pluginMsgs.addAll( messages );
				}
			}
		}

		// NOTE: Another way to do this could be to create more phase methods and split replaceEverything into it's
		// various phases, allowing custom actions to be inserted, for example, between the new component phase and the
		// association phase for a specific component. However, this requires non-trivial refactoring of
		// ConfigurationReplacer that isn't necessary right now.

		// If needed, update the models to the latest component and object versions.
		if( startingVersion < DeploymentLabVersion.getModelVersionByDouble() ){
			log.trace("", "progress");
			project.getProjectSettings().addAnnotation("Automatic Update Config");
			project.getProjectSettings().upgrade("Update: " + startingVersion + " to " + DeploymentLabVersion.getModelVersionByDouble());
			project.getConfigurationReplacer().replaceEverything(true, false);
		}

		// Run all the plug-ins that want to do something after the models are upgraded.
		for( UpgradeFacet plugin : plugins ) {
			if( plugin.requirementsCheck( this, startingVersion ) ) {
				log.trace("Calling postUpdate on " + plugin.getClass().getName() );

				List<String> messages = plugin.postUpdate( this );

				if( messages != null ) {
					pluginMsgs.addAll( messages );
				}
			}
		}

		project.getComponentModel().metamorphosisFinished( ModelState.REPLACING );

		// Display any messages that the plug-ins generated.
		if( !pluginMsgs.isEmpty() ) {
			StringBuilder sb = new StringBuilder( CentralCatalogue.getUIS("fileupgrade.combinedMessagePrefix") );

			for( String msg : pluginMsgs ) {
				sb.append( System.getProperty("line.separator") );
				sb.append( msg );
				sb.append( System.getProperty("line.separator") );
			}

			// The type is a warning since, theoretically, errors will result in an exception that will halt the upgrade.
			log.warn(sb.toString(), "message");
		}
	}

	/**
	 * Run all the plug-ins that want to do something before we update a component.
	 *
	 * @param oldComponent  The component being replaced.
	 * @param newComponent  The new component being added to the model.
	 */
	public void preComponentUpdate( DLComponent oldComponent, DLComponent newComponent ) {
		for( UpgradeFacet plugin : plugins ) {
			if( plugin.requirementsCheck( this, startingVersion ) ) {
				log.trace("Calling preComponentUpdate on " + plugin.getClass().getName() );

				List<String> messages = plugin.preComponentUpdate( this, oldComponent, newComponent );

				if( messages != null ) {
					pluginMsgs.addAll( messages );
				}
			}
		}
	}

	/**
	 * Run all the plug-ins that want to do something to a component after it has been updated.
	 *
	 * @param oldComponent  The component being replaced.
	 * @param newComponent  The new component being added to the model.
	 */
	public void postComponentUpdate( DLComponent oldComponent, DLComponent newComponent ) {
		// Run all the plug-ins that want to do something before we upgrade a component.
		for( UpgradeFacet plugin : plugins ) {
			if( plugin.requirementsCheck( this, startingVersion ) ) {
				log.trace("Calling postComponentUpdate on " + plugin.getClass().getName() );

				List<String> messages = plugin.postComponentUpdate( this, oldComponent, newComponent );

				if( messages != null ) {
					pluginMsgs.addAll( messages );
				}
			}
		}
	}
}
