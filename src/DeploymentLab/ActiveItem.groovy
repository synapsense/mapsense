
package DeploymentLab

import DeploymentLab.Model.ComponentType

import java.awt.Component

import javax.swing.BorderFactory
import javax.swing.DefaultComboBoxModel
import javax.swing.DefaultListCellRenderer
import javax.swing.JLabel
import javax.swing.JList
import javax.swing.ListCellRenderer
import javax.swing.SwingConstants

import DeploymentLab.Image.ComponentIconFactory
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.MetamorphosisEvent
import DeploymentLab.Model.ModelChangeListener
import DeploymentLab.Model.ModelState
import DeploymentLab.channellogger.Logger
import DeploymentLab.Model.ComponentModel
import javax.swing.JComboBox


public class ComponentListRenderer extends DefaultListCellRenderer {
	public ComponentListRenderer() {
		setOpaque(true);
	}

	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus)
		if(value instanceof String)
			setText(value)
		else
			setText(value.getPropertyValue("name"))
		return this
	}
}


public class ActiveWithIconRenderer extends JLabel implements ListCellRenderer {

        public ActiveWithIconRenderer() {
            setOpaque(true);
            setHorizontalAlignment(CENTER);
            setVerticalAlignment(CENTER);
        }

        /*
         * This method finds the image and text corresponding
         * to the selected value and returns the label, set up
         * to display the text and image.
         */
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

            if (isSelected) {
                this.setBackground(list.getSelectionBackground());
                this.setForeground(list.getSelectionForeground());
            } else {
                this.setBackground(list.getBackground());
                this.setForeground(list.getForeground());
            }

			this.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

			if(value instanceof String) {
				this.setText(value)
				this.setIcon(null)
			} else if(value != null) {
				this.setText(value.getPropertyValue("name"))
				this.setIcon( ComponentIconFactory.getComponentIcon(value) )
				this.setHorizontalAlignment( SwingConstants.LEADING )
				this.setHorizontalTextPosition( SwingConstants.TRAILING ) 
			}
            return this;
        }

    }


public class DrawingComboBoxModel extends DefaultComboBoxModel implements SelectionChangeListener, ModelChangeListener {
	private static final Logger log = Logger.getLogger(DrawingComboBoxModel.class.getName())

    private static final String NULL_ELEMENT = CentralCatalogue.getUIS("drawing.selector.noSelectionItem")

    private ComponentModel componentModel
    private SelectModel selectModel

	DrawingComboBoxModel( ComponentModel componentModel, SelectModel selectModel ) {
		super()
        this.componentModel = componentModel
        this.selectModel = selectModel
		addElement( NULL_ELEMENT )
	}

	void drawingPropertyChanged(def who, String prop, oldVal, newVal) {
		if(prop == 'name') {
			boolean isSelected = getSelectedItem().equals(who)
			removeElement(who)
			insertElementAt(who, getSortedIndex(who))

			if(isSelected){
				setSelectedItem(who)
			}
			fireContentsChanged(this, getIndexOf(who), getIndexOf(who))
		}
	}

	void componentAdded(DLComponent component) {
        if( componentModel.hasActiveState( ModelState.INITIALIZING ) ) return

        if(component.hasRole('drawing')) {
            addComponent( component )
        }
    }

    /**
     * Adds the component to this model. Separated out from componentAdded so it could be called during initialization
     * without triggering an activeDrawingChanged event for each added drawing.
     * @param component  The component to add to the model.
     * @param selectItem  True if the item should be set as the selected item in the model.
     */
    private void addComponent( DLComponent component, boolean selectItem = true ) {
        component.addPropertyChangeListener(this, this.&drawingPropertyChanged)
        insertElementAt(component, getSortedIndex(component))
        if( selectItem ) {
            // works in concert with selectModel to make newly added drawings active
            setSelectedItem(component)
        }
	}

	void componentRemoved(DLComponent component) {
        if( componentModel.hasActiveState( ModelState.INITIALIZING ) ) return

		if(component.hasRole('drawing')) {
			component.removePropertyChangeListener(this)

            // If this is the first drawing in the list, Swing will select our NULL item by default. Override that action by
            // selecting the next item before we remove it so Swing doesn't need to change the selection for us.
            int idx = getIndexOf( getSelectedItem() )
            if( (idx - 1) == getIndexOf( NULL_ELEMENT ) ) {
                def nextItem = getElementAt( idx + 1 )
                if( nextItem != null ) {
                    setSelectedItem( nextItem )
                }
            }

			removeElement(component)
		}
	}

	int getSortedIndex(DLComponent component){
		if(this.getSize()==1){
			return 1
		}else{
			for(int i=1;i<this.getSize();i++){
				if(component.getPropertyValue('name') < getElementAt(i).getPropertyValue('name')){
					return i
				}
			}
			return this.getSize()
		}
	}

	void childAdded(DLComponent parent, DLComponent child) {}
	void childRemoved(DLComponent parent, DLComponent child) {}

	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}
	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	@Override
    public void modelMetamorphosisStarting( MetamorphosisEvent event ) {
        if( event.getMode() == ModelState.INITIALIZING ) {
            // We're going to repopulate once initialized, so clear everything.
            for( int i = this.getSize()-1; i > 0; i-- ) {
                DLComponent c = (DLComponent) getElementAt(i)
                c.removePropertyChangeListener(this)
                removeElementAt( i )
            }
        }
    }

	@Override
	public void modelMetamorphosisFinished( MetamorphosisEvent event ) {
        if( event.getMode() == ModelState.INITIALIZING ) {
            // Populate the combo box now that the model is initialized.
            for( DLComponent drawing : componentModel.getComponentsInRole('drawing') ) {
                addComponent( drawing, false )
            }
        } else if( event.getMode() == ModelState.CLEARING ) {
            removeAllElements()
            addElement( NULL_ELEMENT )
        }
    }

    @Override
    void activeDrawingChanged( DLComponent oldDrawing, DLComponent newDrawing ) {
        // Ignore during a model load.
        if( componentModel.hasActiveState( ModelState.INITIALIZING ) ) return

        if( getSelectedItem() != newDrawing ) {
            // Something other than the combo box has changed the active drawing, so we need to make the change.
            if( newDrawing == null ) {
                setSelectedItem( NULL_ELEMENT )
            } else {
                setSelectedItem( newDrawing )
            }
        }
    }

    @Override
    void selectionChanged(SelectionChangeEvent e) {}

}

public class NetworkComboBoxModel extends DefaultComboBoxModel implements ModelChangeListener {
	private static final Logger log = Logger.getLogger(NetworkComboBoxModel.class.getName())

    public static final String NULL_ELEMENT = CentralCatalogue.getUIS("network.selector.noSelectionItem")

    DLComponent myDrawing

	NetworkComboBoxModel( DLComponent drawing ) {
		super()
        myDrawing = drawing
		addElement( NULL_ELEMENT )
	}

	void networkPropertyChanged(def who, String prop, oldVal, newVal) {
		// Skip WSN Networks since they are handled automatically now and aren't part of the Active framework.
		if( who.getType() == ComponentType.WSN_NETWORK ) { return }

		if(prop == 'name') {
			boolean isSelected = getSelectedItem().equals(who)
			removeElement(who)
			insertElementAt(who, getSortedIndex(who))
			
			if(isSelected){
				setSelectedItem(who)
			}
			fireContentsChanged(this, getIndexOf(who), getIndexOf(who))
		}
	}

    void childAdded(DLComponent parent, DLComponent child) {
		// Skip WSN Networks since they are handled automatically now and aren't part of the Active framework.
		if( child.getType() == ComponentType.WSN_NETWORK ) { return }

        if( child.hasRole('network') && (myDrawing == parent.getDrawing()) ) {
			child.addPropertyChangeListener(this, this.&networkPropertyChanged)
			insertElementAt(child, getSortedIndex(child))
			setSelectedItem(child) // works in concert with selectModel to make newly added networks active
		}
	}

    void childRemoved(DLComponent parent, DLComponent child) {
		// Skip WSN Networks since they are handled automatically now and aren't part of the Active framework.
		if( child.getType() == ComponentType.WSN_NETWORK ) { return }

		if( child.hasRole('network') && (myDrawing == parent.getDrawing()) ) {
			child.removePropertyChangeListener(this)
			removeElement(child)
			if(getSelectedItem() == child)
				setSelectedItem( NULL_ELEMENT )
		}
	}

	int getSortedIndex(DLComponent component){
		// NOTE: If you change this, also change the sorting in ScanMACDialog.populateNetworks(). Maybe refactor this for reuse at some point.
		if(this.getSize()==1){
			return 1
		}else{
			for(int i=1;i<this.getSize();i++){
				if(component.getPropertyValue('name') < getElementAt(i).getPropertyValue('name')){
					return i
				}
			}
			return this.getSize()
		}
	}

    void componentAdded(DLComponent component) {}
    void componentRemoved(DLComponent component) {}

	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}
	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	@Override
    public void modelMetamorphosisStarting( MetamorphosisEvent event ) {}

	@Override
    public void modelMetamorphosisFinished( MetamorphosisEvent event ) {}
}

public class ZoneComboBoxModel extends DefaultComboBoxModel implements ModelChangeListener {
    private static final Logger log = Logger.getLogger(ZoneComboBoxModel.class.getName())

    public static final String NULL_ELEMENT = CentralCatalogue.getUIS("group.selector.noSelectionItem")

    DLComponent myDrawing

	ZoneComboBoxModel( DLComponent drawing ) {
		super()
        myDrawing = drawing
		addElement( NULL_ELEMENT )
	}

	void zonePropertyChanged(def who, String prop, oldVal, newVal) {
		if(prop == 'name') {
			boolean isSelected = getSelectedItem().equals(who)
			removeElement(who)
			insertElementAt(who, getSortedIndex(who))
			
			if(isSelected){
				setSelectedItem(who)
			}
			fireContentsChanged(this, getIndexOf(who), getIndexOf(who))
		}
	}

	/**
	 * Checks to see if a newly added component should go in the Groupings combobox.
	 * Note that only Containers (that is, components with roll "zone") should end up here
	 */
    void childAdded(DLComponent parent, DLComponent child) {
		if( child.isContainer() && ( myDrawing == parent.getDrawing() ) ) {
			child.addPropertyChangeListener(this, this.&zonePropertyChanged)
			insertElementAt(child, getSortedIndex(child))
			setSelectedItem(child) // works in concert with selectModel to make newly added zones active
		}
	}

    void childRemoved(DLComponent parent, DLComponent child) {
        if( child.isContainer() && ( myDrawing == parent.getDrawing() ) ) {
			child.removePropertyChangeListener(this)
			removeElement(child)
			if(getSelectedItem() == child)
				setSelectedItem( NULL_ELEMENT )
		}
	}

	int getSortedIndex(DLComponent component){
		if(this.getSize()==1){
			return 1
		}else{
			for(int i=1;i<this.getSize();i++){
				if(component.getPropertyValue('name') < getElementAt(i).getPropertyValue('name')){
					return i
				}
			}
			return this.getSize()
		}
	}

    void componentAdded(DLComponent component) {}
    void componentRemoved(DLComponent component) {}

	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}
	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	@Override
    public void modelMetamorphosisStarting( MetamorphosisEvent event ) {}

	@Override
    public void modelMetamorphosisFinished( MetamorphosisEvent event ) {}
}

/**
 *
 * @author Ken Scoggins
 * @since Jupiter
 */
public class NetworkComboBoxController implements SelectionChangeListener, ModelChangeListener {

    private HashMap<DLComponent,NetworkComboBoxModel> models = new HashMap<>()
    private SelectModel selectModel
    private ComponentModel componentModel
    private JComboBox comboBox

    public NetworkComboBoxController( SelectModel selectModel, ComponentModel componentModel, JComboBox comboBox ) {
        this.selectModel = selectModel
        this.componentModel = componentModel
        this.comboBox = comboBox

        // We need a blank model for when no drawing is selected.
        models.put( null, new NetworkComboBoxModel(null) )
        componentModel.addModelChangeListener( models.get(null) )
    }

    public NetworkComboBoxModel getModel( DLComponent drawing ) {
        return models.get( drawing )
    }

    @Override
    void componentAdded( DLComponent component ) {
        // Create the drawing model on-demand in the activeDrawingChanged instead of here.
    }

    @Override
    void componentRemoved( DLComponent component ) {
        if( component.hasRole('drawing') ) {
            def removedModel = models.remove( component )
            componentModel.removeModelChangeListener( removedModel )
        }
    }

    @Override
    void activeDrawingChanged( DLComponent oldDrawing, DLComponent newDrawing ) {
        // Ignore during a model load.
        if( componentModel.hasActiveState( ModelState.INITIALIZING ) ) return

        // See if we already have this model.
        NetworkComboBoxModel model = models.get( newDrawing )

        if( model == null ) {
            // Need to create a new one and initialize it using the contents of the component model.
            model = new NetworkComboBoxModel( newDrawing )
            models.put( newDrawing, model )
            componentModel.addModelChangeListener( model )

            for( DLComponent net : newDrawing.getChildrenOfRole('network') ) {
	            // Skip WSN Networks since they are handled automatically now and don't need to be manually selected.
	            if( net.getType() != ComponentType.WSN_NETWORK ) {
		            model.childAdded( newDrawing, net )
	            }
            }
        }

        // Make sure we are in sync with the select model.
        if( selectModel.getActiveNetwork() == null ) {
            model.setSelectedItem( NetworkComboBoxModel.NULL_ELEMENT )
        } else {
            model.setSelectedItem( selectModel.getActiveNetwork() )
        }

        comboBox.setModel( model )
    }

	public void activeDatasourceChanged(DLComponent oldDatasource, DLComponent newDatasource) {
        // Ignore during a model load.
        if( componentModel.hasActiveState( ModelState.INITIALIZING ) ) return

        NetworkComboBoxModel model = models.get( selectModel.getActiveDrawing() )

        if( model.getSelectedItem() != newDatasource ) {
            // Something other than the combo box has changed the active drawing, so we need to make the change.
            if( newDatasource == null ) {
                comboBox.setSelectedItem( NetworkComboBoxModel.NULL_ELEMENT )
            } else {
                comboBox.setSelectedItem( newDatasource )
            }
        }
	}

    @Override
    void childAdded(DLComponent parent, DLComponent child) {}

    @Override
    void childRemoved(DLComponent parent, DLComponent child) {}

    @Override
    void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

    @Override
    void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

    @Override
    public void modelMetamorphosisStarting( MetamorphosisEvent event ) {}

    @Override
    public void modelMetamorphosisFinished( MetamorphosisEvent event ) {}

    @Override
    void selectionChanged(SelectionChangeEvent e) {}
}

/**
 *
 * @author Ken Scoggins
 * @since Jupiter
 */
public class ZoneComboBoxController implements SelectionChangeListener, ModelChangeListener {

    private HashMap<DLComponent,ZoneComboBoxModel> models = new HashMap<>()
    private SelectModel selectModel
    private ComponentModel componentModel
    private JComboBox comboBox

    public ZoneComboBoxController( SelectModel selectModel, ComponentModel componentModel, JComboBox comboBox ) {
        this.selectModel = selectModel
        this.componentModel = componentModel
        this.comboBox = comboBox

        // We need a blank model for when no drawing is selected.
        models.put( null, new ZoneComboBoxModel(null) )
        componentModel.addModelChangeListener( models.get(null) )
    }

    public ZoneComboBoxModel getModel( DLComponent drawing ) {
        return models.get( drawing )
    }

    @Override
    void componentAdded( DLComponent component ) {
        // Create the drawing model on-demand in the activeDrawingChanged instead of here.
    }

    @Override
    void componentRemoved( DLComponent component ) {
        if( component.hasRole('drawing') ) {
            def removedModel = models.remove( component )
            componentModel.removeModelChangeListener( removedModel )
        }
    }

    @Override
    void activeDrawingChanged( DLComponent oldDrawing, DLComponent newDrawing ) {
        // Ignore during a model load.
        if( componentModel.hasActiveState( ModelState.INITIALIZING ) ) return

        // See if we already have this model.
        ZoneComboBoxModel model = models.get( newDrawing )

        if( model == null ) {
            // Need to create a new one and initialize it using the contents of the component model.
            model = new ZoneComboBoxModel( newDrawing )
            models.put( newDrawing, model )
            componentModel.addModelChangeListener( model )

            for( DLComponent child : newDrawing.getChildComponents() ) {
                if( child.isContainer() ) {
                    model.childAdded( newDrawing, child )
                }
            }
        }

        // Make sure we are in sync with the select model.
        if( selectModel.getActiveZone() == null ) {
            model.setSelectedItem( ZoneComboBoxModel.NULL_ELEMENT )
        } else {
            model.setSelectedItem( selectModel.getActiveZone() )
        }

        comboBox.setModel( model )
    }

	public void activeZoneChanged(DLComponent oldZone, DLComponent newZone) {
        // Ignore during a model load.
        if( componentModel.hasActiveState( ModelState.INITIALIZING ) ) return

        ZoneComboBoxModel model = models.get( selectModel.getActiveDrawing() )

        if( model.getSelectedItem() != newZone ) {
            // Something other than the combo box has changed the active grouping, so we need to make the change.
            if( newZone == null ) {
                comboBox.setSelectedItem( ZoneComboBoxModel.NULL_ELEMENT )
            } else {
                comboBox.setSelectedItem( newZone )
            }
        }
	}

    @Override
    void childAdded(DLComponent parent, DLComponent child) {}

    @Override
    void childRemoved(DLComponent parent, DLComponent child) {}

    @Override
    void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

    @Override
    void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

    @Override
    public void modelMetamorphosisStarting( MetamorphosisEvent event ) {}

    @Override
    public void modelMetamorphosisFinished( MetamorphosisEvent event ) {}

    @Override
    void selectionChanged(SelectionChangeEvent e) {}
}
