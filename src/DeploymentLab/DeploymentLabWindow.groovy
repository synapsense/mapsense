package DeploymentLab

import DeploymentLab.Controller.InstrumentFloorplanController
import DeploymentLab.Controller.UninstrumentFloorplanController
import DeploymentLab.SmartZone.LinkedProjectListener
import DeploymentLab.SmartZone.MergeLogicController
import DeploymentLab.SmartZone.SyncWithServerListener
import DeploymentLab.Util.SmartZoneResponseAssembler
import DeploymentLab.Controller.GetFloorplanController
import DeploymentLab.Controller.OpenLocationController
import DeploymentLab.Controller.SaveLocationController
import DeploymentLab.Examinations.PowerImagingRuleExamination
import DeploymentLab.Examinations.WSNExamination

import DeploymentLab.Image.ComponentIconFactory
import DeploymentLab.Import.DLComponentValueImporter
import DeploymentLab.Import.DLObjectValueImporter
import DeploymentLab.PropertyEditor.PropertyModel
import DeploymentLab.PropertyEditor.PropertyTable

import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.Security.NetworkKey
import DeploymentLab.Security.PanIDFactory
import DeploymentLab.Transfer.SelectionTransferForge
import DeploymentLab.Transfer.TransferActionListener
import com.panduit.sz.api.ss.assets.Location
import com.panduit.sz.api.ss.assets.LocationLevel
import com.panduit.sz.api.ss.assets.Floorplan
import com.synapsense.dto.BinaryData
import com.synapsense.dto.TOFactory
import com.synapsense.util.unitconverter.SystemConverter
import groovy.swing.SwingBuilder
import groovy.ui.Console

import javax.swing.tree.DefaultMutableTreeNode
import javax.swing.tree.TreePath
import javax.xml.XMLConstants
import java.awt.BorderLayout

import java.awt.Color
import java.awt.Cursor
import java.awt.Desktop
import java.awt.FlowLayout
import java.awt.Frame
import java.awt.GridBagConstraints
import java.awt.GridBagLayout
import java.awt.Graphics2D
import java.awt.Insets
import java.awt.Dialog.ModalityType
import java.awt.geom.AffineTransform
import java.awt.geom.Point2D
import java.awt.image.BufferedImage
import java.text.DecimalFormat
import java.util.zip.ZipEntry
import java.util.zip.ZipFile
import javax.imageio.ImageIO

import javax.swing.filechooser.FileNameExtensionFilter

import org.apache.commons.io.FilenameUtils
import DeploymentLab.Dialogs.*
import DeploymentLab.Export.*
import DeploymentLab.Model.*
import DeploymentLab.Tools.*
import DeploymentLab.Tree.*
import DeploymentLab.channellogger.*
import java.awt.event.*
import javax.swing.*
import DeploymentLab.Dialogs.Stats.DeploymentStatsController
import DeploymentLab.Image.DLImage
import DeploymentLab.Dialogs.ChangeParents.ChangeParentsController
import DeploymentLab.Dialogs.ChangeParents.ParentCategory
import DeploymentLab.Dialogs.About.AboutController
import DeploymentLab.Dialogs.ChooseDrawing.ChooseDrawingController
import java.awt.Component
import java.awt.Container
import DeploymentLab.Examinations.ShapeViewer

import DeploymentLab.Palette.PaletteController
import net.miginfocom.swing.MigLayout
import java.awt.Dimension
import DeploymentLab.Dialogs.FindReplace.FindReplaceController
import DeploymentLab.Transfer.DeploymentPanelTransferHandler

import DeploymentLab.Dialogs.StyledMessage.StyledMessageController
import DeploymentLab.Import.ImportProjectController
import DeploymentLab.Upgrade.UpgradeController



public class DeploymentLabWindow implements WindowListener {
	private static final Logger log = Logger.getLogger(DeploymentLabWindow.class.getName())

	private static final String LINE_SEP = System.getProperty("line.separator")

	private boolean showUI = true

	private JFileChooser dirChooser
	private JFileChooser fileChooser
	private JFileChooser saveFileChooser
	private JFileChooser imageFileChooser
	private JFileChooser openFileChooser
	private JFileChooser importFileChooser

	private JFileChooser simFileChooser
	private JFileChooser clibFileChooser

	private ObjectModel objectModel
	protected ComponentModel componentModel

	private File openFile
	private File currentFileName

	/** The main view & edit pane of the application. */
	protected DeploymentPanel deploymentPanel
	protected SelectModel selectModel
	private SwingBuilder builder
	private DeploymentLab.Tools.PopupMenu popupMenu

	private Properties appProperties
	private Properties uiStrings
	private Properties componentsProperties

	private JFrame deploymentLabFrame

	private JDialog generalConfigDialog

	private ProjectSettings projectSettings = ProjectSettings.getInstance()

	private String defaultUnit = 'Imperial US'
	private ButtonGroup toolButtonGroup
	private ButtonGroup presetButtonGroup

	private int scrollingMessageDialogResult
	private JDialog scrollingMessageDialog
	private JTextArea scrollingMessageText
	private JButton scrollingMessageOkButton

	private JDialog scrollingExportWarningDialog
	private JTextArea scrollingExportWarningText
	private int scrollingWarningDialogResult

	private JDialog licenseTermsDialog
	private JDialog userDocDialog
	private JButton userDocCloseBtn

	private JTextArea bomText

	private JTree structureView
	private JTree networkView

	private StructureTreeKeyboardCommands structureKeyListener
	private NetworkTreeKeyboardCommands networkKeyListener

	private ConfigurationTreeModel configModel
	protected ToolManager toolManager
	private NodeNamer nodeNamer
	private UndoBuffer undoBuffer

	private DeploymentStatsController deploymentStats

	private String applianceHost

	private final Object backgroundTaskLock = new Object()
	private Thread backgroundTask
	private ProgressPane progressPane
	private Timer progressPaneTimer

	private PropertyModel pm
	private SystemConverter systemConverter = CentralCatalogue.instance.systemConverter

	private ViewOptionTree viewOptionConfigTree
	private ViewOptionTree viewOptionHaloTree
	private ViewOptionTreeModel viewOptionConfigTreeModel
	private ViewOptionTreeModel viewOptionHaloTreeModel
	private ConfigurationReplacerTree configurationReplacerTree
	private ConfigurationReplacerTreeModel configurationReplacerTreeModel
	private ConfigurationReplacer configurationReplacer

	private DisplayProperties displayProperties
	private UserPreferences userPreferences

	private JMenu fileMenu
	private JMenu scriptsMenu
	private JMenu componentLibMenu
	private JMenuItem subMenuNoComponentLib
	private componentLibMenuMap = [:]
	private ComponentLibrary componentLibrary

	private UIDisplay uiDisplay
	private JToggleButton environmentalsToggleBtn
	private JToggleButton powerToggleBtn
	private JToggleButton pueToggleBtn
	private JToggleButton controlToggleBtn
	private JToggleButton containmentToggleBtn


	private toggleGroupMap = [:]

	private UserAccess userAccess
	private LoginDialog loginDialog = new LoginDialog(deploymentLabFrame)
	private ServerDialog smartZoneServerDialog = new ServerDialog(deploymentLabFrame)
	private LoginDialog loginDialogWithHost = new LoginDialog(deploymentLabFrame, true)
	private UnStashDialog unStashDialog = new UnStashDialog(deploymentLabFrame, this)
	private ExportComponentDialog exportComponentDialog = new ExportComponentDialog(deploymentLabFrame, this);
	private AccessModelDialog accessModelDialog = new AccessModelDialog(deploymentLabFrame, this)
	private ViewOptionDialog viewOptionDialog
	private CanvasSettingsDialog canvasSettingsDialog = new CanvasSettingsDialog(deploymentLabFrame, this);

	JComboBox drawingComboBox
	JComboBox networkComboBox
	JComboBox zoneComboBox

	// Active selection combo box models. Each drawing has its own set of net & zone options, hence the maps.
	private DrawingComboBoxModel drawingComboBoxModel
	private NetworkComboBoxController netComboBoxCtrl
	private ZoneComboBoxController zoneComboBoxCtrl

	private java.util.Timer tardis
	private java.util.Timer delorean
	private WorkingDir wd
	private DebugHelper debugHelper

//	private Properties smartZoneProperties = new Properties();
	private SmartZoneLoginDialog smartZoneLoginDialog;

	private MergeLogicController mergeController

	PaletteController paletteController
	LogicalGroupController logicalGroupController
	ManualExpressionController manualExpressionController
	RoomController roomController
	WSNController wsnController
	FloorTilesViewController floorTilesViewController
	LinkedProjectListener linkedProjectListener

	private boolean isSyncWithSmartZone
	private boolean syncWithServerCancelled
	private boolean locationFound

	private static final LOC_NOT_FOUND = "Location not found in SmartZone server"
	private static final LOC_UNLINKED = "Location unlinked in SmartZone server"
	private static final SYNC_ABORTED = "Sync with Server aborted"
	private static final SAVE_ABORTED = "Save to SmartZone aborted"

	public DeploymentLabWindow(String applianceHost = null, boolean useStandardOutLogger) {
		this.isSyncWithSmartZone = false
		this.applianceHost = applianceHost
		//uiDisplay = new UIDisplay()
		uiDisplay = UIDisplay.INSTANCE
		uiDisplay.setProperty("isFileLoaded", false)
		uiDisplay.setProperty("isSmartZoneLinked", false)
		uiDisplay.setProperty("isSZAdvancedEditorMode", false)
		uiDisplay.setProperty("isNotSZAdvancedEditorMode", true)

		userAccess = new UserAccess()

		fileChooser = new JFileChooser()
		for (FileFilters ff : FileFilters.values())
			fileChooser.addChoosableFileFilter(ff.getFilter())

		saveFileChooser = new JFileChooser()
		imageFileChooser = new JFileChooser()
		openFileChooser = new JFileChooser()
		openFileChooser.addChoosableFileFilter(FileFilters.DLProject.getFilter())
		importFileChooser = new JFileChooser()
		importFileChooser.addChoosableFileFilter(FileFilters.DLProject.getFilter())

		dirChooser = new JFileChooser()
		dirChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY)

		simFileChooser = new JFileChooser()
		simFileChooser.addChoosableFileFilter(FileFilters.XMLFile.getFilter())

		clibFileChooser = new JFileChooser()

		uiStrings = CentralCatalogue.getInstance().getUIStrings()
		appProperties = CentralCatalogue.getInstance().getAppProperties()
		System.setProperty("application.ui", appProperties.getProperty("application.ui"))
		componentsProperties = CentralCatalogue.getInstance().getComponentsProperties()

		/* HP stuff is not valid right now, but keep this framework in place to maybe reuse in the SZ 8 integration to flag the integrated vs standalone instance?
		if (appProperties.getProperty('application.ui') == 'hp') {
			uiDisplay.setProperty("isNotHP", false)
			uiDisplay.setProperty("isNotSS", true)

		} else {*/
			uiDisplay.setProperty("isNotHP", true)
			uiDisplay.setProperty("isNotSS", false)
		//}

		if (applianceHost == null) {
			uiDisplay.setProperty("isNotAppliance", true)
		} else {
			uiDisplay.setProperty("isNotAppliance", false)
		}
		uiDisplay.setProperty('useBackgroundImage', true)
		uiDisplay.setProperty('useCanvas', false)

		ChannelManager cm = ChannelManager.getInstance()

		wd = new WorkingDir()
		File logFile = wd.getLogFile()

		//int logLevel = Integer.parseInt(appProperties.getProperty("logging.level"))
		//pull log level from system properties if it's there
		//# -1 - No logging whatsoever, 0 - Fatal, 1 - Error, 2 - Warn, 3 - Info, 4 - Debug, 5 - Trace
		int logLevel
		String strLevel = System.getProperty("synapsense.loglevel", appProperties.getProperty("logging.level"))
		if (strLevel.isInteger()) {
			logLevel = Integer.parseInt(strLevel)
		} else {
			switch (strLevel.toLowerCase()) {
				case "fatal":
					logLevel = 0
					break;
				case "error":
					logLevel = 1
					break;
				case "warn":
					logLevel = 2
					break;
				case "info":
					logLevel = 3
					break;
				case "debug":
					logLevel = 4
					break;
				case "trace":
					logLevel = 5
					break;
				default:
					logLevel = 3
					break;
			}
		}

		int rotateSize = Integer.parseInt(appProperties.getProperty("logging.rotatesize"))
		if (logFile != null && logFile != '') {
			if (logFile.exists() && logFile.size() > rotateSize) {
				File logFile1 = new File(wd.getLogPath() + ".1")
				File logFile2 = new File(wd.getLogPath() + ".2")
				if (logFile1.exists()) {
					log.info("Rotating ${logFile1} to ${logFile2}")
					if (logFile2.exists())
						logFile2.delete()
					logFile1.renameTo(logFile2)
				}
				log.info("Rotating ${logFile} to ${logFile1}")
				logFile.renameTo(logFile1)
			}
			FileWriter fw = new FileWriter(logFile, true)
			cm.addChannelListener({ logData ->
				if (logLevel >= logData.level.ordinal()) {
					fw.write(logData.toString()); fw.flush()
				}
			} as ChannelListener)
			if (useStandardOutLogger) {
				//cm.addChannelListener({logData -> if(logLevel >= logData.level.ordinal()) {print logData.toString()} } as ChannelListener)
				cm.addChannelListener({ logData ->
					if (logLevel >= logData.level.ordinal()) {
						BufferedWriter log = new BufferedWriter(new OutputStreamWriter(System.out));
						log.write(logData.toString());
						log.flush();
					}
				} as ChannelListener)
			}

		}

		builder = new SwingBuilder()
	}

	JDialog getGeneralConfigDialog() {
		if (generalConfigDialog != null) {
            setupGeneralOptions()
            return generalConfigDialog
        }

		GridBagConstraints gbc = new GridBagConstraints()

		generalConfigDialog = builder.dialog(owner: deploymentLabFrame, resizable: false, title: 'General Configuration Options', layout: new BorderLayout(), modalityType: ModalityType.APPLICATION_MODAL) {
			def okAction = { updateGeneralOptions(); generalConfigDialog.setVisible(false) }
			def cancelAction = { generalConfigDialog.setVisible(false) }
			def dlgPanel = panel(layout: new GridBagLayout(), constraints: BorderLayout.CENTER) {
				gbc.insets = new Insets(5, 5, 5, 5)

				int gridy = 0;

				gbc.gridx = 0; gbc.gridy = gridy++; gbc.anchor = GridBagConstraints.CENTER; gbc.gridwidth = 1
				panel(layout: new GridBagLayout(), constraints: gbc) {
					gbc.gridx = 0; gbc.gridy = 0; gbc.gridwidth = 1; gbc.anchor = GridBagConstraints.WEST
					label(text: CentralCatalogue.getUIS("generalSetup.serverHost"), constraints: gbc)

					gbc.gridx = 0; gbc.gridy = 1; gbc.gridwidth = 2; gbc.anchor = GridBagConstraints.WEST
					textField(id: 'esHostIP', text: '', columns: 50, constraints: gbc)

					gbc.gridx = 0; gbc.gridy = 2; gbc.gridwidth = 1; gbc.anchor = GridBagConstraints.WEST
					label(text: CentralCatalogue.getUIS("generalSetup.controlHost"), constraints: gbc)

					gbc.gridx = 0; gbc.gridy = 3; gbc.gridwidth = 2; gbc.anchor = GridBagConstraints.WEST
					textField(id: 'acHostIP', text: '', columns: 50, constraints: gbc)


				}

				gbc.gridx = 0; gbc.gridy = gridy++; gbc.anchor = GridBagConstraints.WEST; gbc.gridwidth = 1
				panel(layout: new GridBagLayout(), constraints: gbc) {
					gbc.gridx = 0; gbc.gridy = 0; gbc.gridwidth = 1; gbc.anchor = GridBagConstraints.WEST
					label(text: 'Units:', constraints: gbc)

					unitSystemGroup = buttonGroup(id: 'unitSystemGroup')
					gbc.gridx = 1; gbc.gridy = 0; gbc.gridwidth = 2; gbc.anchor = GridBagConstraints.WEST
					btn1 = radioButton(id: 'unit_SI', actionCommand: 'SI', text: 'Standard International', buttonGroup: unitSystemGroup, constraints: gbc)

					gbc.gridx = 3; gbc.gridy = 0; gbc.gridwidth = 2; gbc.anchor = GridBagConstraints.WEST
					btn2 = radioButton(id: 'unit_server', actionCommand: 'Imperial US', text: 'US Standard', buttonGroup: unitSystemGroup, constraints: gbc, selected: true)
					unitSystemGroup.add(btn1)
					unitSystemGroup.add(btn2)
				}

				gbc.gridx = 0; gbc.gridy = 4; gbc.anchor = GridBagConstraints.CENTER; gbc.gridwidth = 1; gbc.fill = GridBagConstraints.HORIZONTAL;
				panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: gbc) {
					button(text: 'OK', defaultButton: true, actionPerformed: okAction)
					button(text: 'Cancel', actionPerformed: cancelAction)
				}
			}
			dlgPanel.registerKeyboardAction(okAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			dlgPanel.registerKeyboardAction(cancelAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		}
		generalConfigDialog.pack()
		generalConfigDialog.setLocationRelativeTo(deploymentLabFrame)
		return generalConfigDialog
	}

	JDialog getScrollingMessageDialog() {
		if (scrollingMessageDialog != null)
			return scrollingMessageDialog

		GridBagConstraints gbc = new GridBagConstraints()
		scrollingMessageDialog = builder.dialog(owner: deploymentLabFrame, resizable: true, title: "Message", layout: new BorderLayout(), modalityType: ModalityType.APPLICATION_MODAL) {
			def okAction = { scrollingMessageDialog.setVisible(false) }

			scrollPane(constraints: BorderLayout.CENTER, border: BorderFactory.createEtchedBorder(), preferredSize: [600, 300]) {
				scrollingMessageText = textArea(editable: false)
			}
			def buttonPanel = panel(constraints: BorderLayout.SOUTH, layout: new FlowLayout(FlowLayout.TRAILING)) {
				scrollingMessageOkButton = button(text: 'Close', mnemonic: KeyEvent.VK_C, defaultButton: true, actionPerformed: okAction)
			}
			buttonPanel.registerKeyboardAction(okAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			buttonPanel.registerKeyboardAction(okAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)

		}
		scrollingMessageDialog.pack()

		return scrollingMessageDialog
	}

	JDialog getScrollingExportWarningDialog() {
		if (scrollingExportWarningDialog != null)
			return scrollingExportWarningDialog

		GridBagConstraints gbc = new GridBagConstraints()
		scrollingExportWarningDialog = builder.dialog(owner: deploymentLabFrame, resizable: true, title: "Confirm Export", layout: new BorderLayout(), modalityType: ModalityType.APPLICATION_MODAL) {
			def okAction = { scrollingWarningDialogResult = JOptionPane.OK_OPTION; scrollingExportWarningDialog.setVisible(false) }
			def cancelAction = { scrollingWarningDialogResult = JOptionPane.CANCEL_OPTION; scrollingExportWarningDialog.setVisible(false) }
			def windowPanel = scrollPane(constraints: BorderLayout.CENTER, border: BorderFactory.createEtchedBorder(), preferredSize: [600, 300]) {
				scrollingExportWarningText = textArea(editable: false)
			}
			windowPanel.registerKeyboardAction(okAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			windowPanel.registerKeyboardAction(cancelAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)

			def buttonPanel = panel(constraints: BorderLayout.SOUTH, layout: new FlowLayout(FlowLayout.TRAILING)) {
				button(text: 'OK', defaultButton: true, actionPerformed: okAction)
				button(text: 'Cancel', actionPerformed: cancelAction)
			}
			buttonPanel.registerKeyboardAction(okAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			buttonPanel.registerKeyboardAction(cancelAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		}
		scrollingExportWarningDialog.pack()

		return scrollingExportWarningDialog
	}


	public void start(String fileToLoad = null, int level = 1) {
		log.info(appProperties.getProperty('application.shortname') + " version " + appProperties.getProperty('build.version') + " revision " + appProperties.getProperty('build.revision') + " built on " + appProperties.getProperty('build.date') + " starting.")

		//use the initial user level to decide whether to show some menu options at all
		uiDisplay.setUserLevel(level)

		def objectsXml = ComponentLibrary.parseXmlFile(CentralCatalogue.getApp("objects.definitions.main"))
		objectModel = new ObjectModel(objectsXml)
		CentralCatalogue.getApp("objects.definitions.extra").split(",").each {
			objectModel.addTypes(ComponentLibrary.parseXmlFile(it.trim()))
		}

		selectModel = new SelectModel()
		CentralCatalogue.setSelectModel( selectModel )

		def modelXml = ComponentLibrary.parseXmlFile('cfg/commoncomponents.xml')
		componentModel = new ComponentModel(objectModel, modelXml)

		undoBuffer = new UndoBuffer(componentModel, selectModel)
		CentralCatalogue.setUndoBuffer( undoBuffer )

		networkView = new NetworkTree(componentModel, undoBuffer, uiDisplay)
		NetworkTreeController networkViewCtrl = new NetworkTreeController(selectModel, componentModel, networkView)
		componentModel.addModelChangeListener(networkViewCtrl)
		selectModel.addSelectionChangeListener(networkViewCtrl)

		structureView = new StructureTree(componentModel, undoBuffer, uiDisplay)
		StructureTreeController structureViewCtrl = new StructureTreeController(selectModel, componentModel, structureView)
		componentModel.addModelChangeListener(structureViewCtrl)
		selectModel.addSelectionChangeListener(structureViewCtrl)

		def treeModels = []
		def paletteTreeModels = []

		paletteController = new PaletteController(componentModel, selectModel)
		paletteTreeModels.add(paletteController)
		selectModel.addSelectionChangeListener(paletteController)

		// set up object types for option view dialog
		viewOptionConfigTreeModel = new ViewOptionTreeModel(componentModel, CentralCatalogue.getUIS("components.name"), 'configuration')
		viewOptionConfigTree = new ViewOptionTree(viewOptionConfigTreeModel)
		treeModels.add(viewOptionConfigTreeModel)

		viewOptionHaloTreeModel = new ViewOptionTreeModel(componentModel, 'Halo', 'halo')
		viewOptionHaloTree = new ViewOptionTree(viewOptionHaloTreeModel)
		componentModel.addModelChangeListener(selectModel)
		treeModels.add(viewOptionHaloTreeModel)

		configurationReplacerTreeModel = new ConfigurationReplacerTreeModel(componentModel, "Configurations")
		configurationReplacerTree = new ConfigurationReplacerTree(configurationReplacerTreeModel)
		treeModels.add(configurationReplacerTreeModel)

		// after creating treemodel object, initialize component lib
		//componentLibrary = new ComponentLibrary(componentModel, paletteTreeModels, treeModels)
		componentLibrary = ComponentLibrary.INSTANCE
		componentLibrary.init(componentModel, paletteTreeModels, treeModels)
		componentLibrary.loadDefaultComponents()

		// create ConfigurationReplacer
		configurationReplacer = new ConfigurationReplacer(selectModel, componentModel, configurationReplacerTree, undoBuffer, deploymentLabFrame)


		drawingComboBoxModel = new DrawingComboBoxModel(componentModel, selectModel)
		componentModel.addModelChangeListener(drawingComboBoxModel)
		selectModel.addSelectionChangeListener(drawingComboBoxModel)

		toolButtonGroup = new ButtonGroup()

		toolManager = new ToolManager(toolButtonGroup)
		displayProperties = new DisplayProperties(selectModel)
		componentModel.addModelChangeListener(displayProperties)

		paletteController.setToolManager(toolManager)

		deploymentPanel = new DeploymentPanel(selectModel, undoBuffer, componentModel, toolManager, displayProperties)
		CentralCatalogue.setDeploymentPanel(deploymentPanel)

		logicalGroupController = new LogicalGroupController()
		componentModel.addModelChangeListener(logicalGroupController)

		manualExpressionController = new ManualExpressionController()
		componentModel.addModelChangeListener(manualExpressionController)

		roomController = new RoomController(componentModel, selectModel, deploymentPanel)
		componentModel.addModelChangeListener(roomController)

		wsnController = new WSNController(componentModel, selectModel, undoBuffer, deploymentPanel)
		componentModel.addModelChangeListener(wsnController)

		floorTilesViewController = new FloorTilesViewController(deploymentPanel)
		componentModel.addModelChangeListener(floorTilesViewController)

		linkedProjectListener = new LinkedProjectListener()
		componentModel.addModelChangeListener(linkedProjectListener)

		pm = new PropertyModel(componentModel, undoBuffer, systemConverter)
		selectModel.addSelectionChangeListener(pm)
		PropertyTable propertyTable = new PropertyTable(pm, undoBuffer, deploymentLabFrame, componentModel, deploymentPanel, uiDisplay)

		NodeOperator nodeOperator = new NodeOperator(selectModel, undoBuffer, componentModel, deploymentPanel)

		viewOptionDialog = new ViewOptionDialog(viewOptionConfigTree, viewOptionHaloTree, displayProperties, toggleGroupMap, deploymentLabFrame, systemConverter, selectModel, componentModel)

		popupMenu = new DeploymentLab.Tools.PopupMenu(this, undoBuffer, uiDisplay, configurationReplacer, deploymentPanel)
		FasterNodeNamer fasterNamer = new FasterNodeNamer(deploymentPanel, uiDisplay, undoBuffer)

		toolManager.registerTool('selector', new NodeSelector(deploymentPanel, selectModel), true)
		toolManager.registerTool('currentlocation', new CurrentLocation(deploymentPanel, selectModel), true)
		toolManager.registerTool('rectselect', new RectangularSelect(deploymentPanel.getSelectShape(), deploymentPanel, selectModel))
		toolManager.registerTool('panzoomer', new PanZoomer(deploymentPanel))
		def hovermatic = new NodeHover(deploymentPanel, displayProperties)
		componentModel.addModelChangeListener(hovermatic)
		toolManager.registerTool('hover', hovermatic)
		//toolManager.registerTool('hover', new NodeHover(deploymentPanel, displayProperties))
		toolManager.registerTool('mover', new NodeMover(undoBuffer, deploymentPanel, selectModel))
		toolManager.registerTool('adder', new NodeAdder(selectModel, componentModel, undoBuffer, viewOptionConfigTree, uiDisplay, deploymentLabFrame, viewOptionDialog, uiStrings, deploymentPanel), false, true)
		toolManager.registerTool('controlinputadder', new ControlInputAdder(selectModel, componentModel, undoBuffer, viewOptionConfigTree, uiDisplay, deploymentPanel, displayProperties, deploymentLabFrame, viewOptionDialog), false, true)
		toolManager.registerTool('rotator', new NodeRotator(selectModel), true)
		toolManager.registerTool('rotator2', new NodeGroupRotator(selectModel, undoBuffer), true)
		nodeNamer = new NodeNamer(selectModel)
		toolManager.registerTool('namer', nodeNamer, true)
		toolManager.registerTool('macassigner', new MacAssigner(selectModel, deploymentLabFrame), true)
		toolManager.registerTool('keycommands', new KeyboardCommands(componentModel, undoBuffer, selectModel, uiDisplay, deploymentPanel))
		toolManager.registerTool('popupmenu', popupMenu)
		toolManager.registerTool('fasternamer', fasterNamer)
		toolManager.registerTool('dynamicchildadder', new DynamicChildAdder(selectModel, componentModel, undoBuffer, viewOptionConfigTree, uiDisplay, deploymentPanel, displayProperties, deploymentLabFrame, viewOptionDialog, uiStrings), false, true)
		toolManager.registerTool('proxyadder', new ProxyAdder(selectModel, componentModel, undoBuffer), false, true)
		toolManager.registerTool('arbitraryshaper', new ArbitraryShaper(selectModel, componentModel, undoBuffer, viewOptionConfigTree, uiDisplay, deploymentLabFrame, viewOptionDialog, uiStrings, deploymentPanel, toolManager), false, false)
		toolManager.registerTool('staticchildrennodeadder', new StaticChildrenNodeAdder(selectModel, componentModel, undoBuffer, viewOptionConfigTree, uiDisplay, deploymentLabFrame, viewOptionDialog, uiStrings, deploymentPanel), false, true)
		toolManager.registerTool('newassoc', new NewAssoc(selectModel, deploymentPanel, undoBuffer, displayProperties, uiDisplay, deploymentLabFrame, viewOptionDialog), true)
		toolManager.registerTool('copysettings', new CopySettings(selectModel, undoBuffer), true)

		DeploymentTreeSelectionListener selectListener = new DeploymentTreeSelectionListener(selectModel)

		structureKeyListener = new StructureTreeKeyboardCommands(componentModel, undoBuffer, uiDisplay, selectModel, nodeOperator)
		networkKeyListener = new NetworkTreeKeyboardCommands(componentModel, undoBuffer, uiDisplay)

		structureView.addKeyListener(structureKeyListener)
		structureView.addTreeSelectionListener(selectListener)
		networkView.addKeyListener(networkKeyListener)
		networkView.addTreeSelectionListener(selectListener)

		selectModel.addSelectionChangeListener(structureView)
		selectModel.addSelectionChangeListener(networkView)
		selectModel.addSelectionChangeListener(deploymentPanel)
		selectModel.addSelectionChangeListener(displayProperties)

		//sets up the stats dialog controller
		deploymentStats = new DeploymentStatsController(componentModel, deploymentLabFrame)


		progressPane = new ProgressPane()
		ProgressManager.getInstance().init(this)

		progressPaneTimer = new Timer(50, { progressPane.setAngle(progressPane.getAngle() + 0.25) } as ActionListener)
		progressPaneTimer.setRepeats(true)

		userPreferences = new UserPreferences()
/*
		if (fileToLoad == null) {
			// Load a "default" project file
			def defModelXml = ComponentLibrary.parseXmlFile('cfg/defaultmodel.xml')
			objectModel.deserialize(defModelXml.objects)
			componentModel.deserialize(defModelXml.components, componentLibrary)
			undoBuffer.markSavePoint()
		}
*/
		def splitPane1, splitPane2, splitPane3, splitPane4, splitPane5, outerPanel = null
		JPanel leftSidePanel

		TransferActionListener actionListener = new TransferActionListener(deploymentPanel.getCanvas());
		def copyItem, pasteItem //, pasteProxyItem

		debugHelper = new DebugHelper(componentModel, objectModel, projectSettings, selectModel, undoBuffer, deploymentPanel)

		//todo: dlf needs to be initialized BEFORE all that stuff up there that uses it

		deploymentLabFrame = builder.frame(
				id: 'deploymentLabFrame',
				title: appProperties.getProperty('application.shortname') + " " + appProperties.getProperty("build.version"),
				size: [800, 500],
				defaultCloseOperation: JFrame.DO_NOTHING_ON_CLOSE,
				layout: new BorderLayout(),
				glassPane: progressPane,
				iconImage: new ImageIcon(getClass().getResource('/cfg/appicon.png')).getImage()
		) {
			menuBar {
				fileMenu = menu('File', mnemonic: 'F', menuSelected : {updateFileMenu()}) {
					uiDisplay.addUIChangeListener(menuItem(text: 'New project', mnemonic: 'N', accelerator: shortcut('N'), actionPerformed: {fileNew()}),'newFile')
					uiDisplay.addUIChangeListener(menuItem(text: 'Open from disk...', mnemonic: 'O', accelerator: shortcut('O'), actionPerformed: {fileOpen()}),'openFile')
					uiDisplay.addUIChangeListener(menuItem(text: 'Open from SynapSoft...', actionPerformed: {fileOpenFromServer()}),'openFile')
					uiDisplay.addUIChangeListener(menuItem(text: 'Open from SmartZone...', actionPerformed: {fileOpenFromSmartZone()}),'openFile')
					uiDisplay.addUIChangeListener(menuItem(text: 'Close project', mnemonic: 'C', actionPerformed: {fileClose()}),'closeFile')
					separator()
					uiDisplay.addUIChangeListener(menuItem(text: 'Save', mnemonic: 'S', accelerator: shortcut('S'), actionPerformed: { fileSave() }), 'saveFile')
					uiDisplay.addUIChangeListener(menuItem(text: 'Save to SmartZone', mnemonic: 'S', actionPerformed: { fileSaveToSmartZone() }), 'saveToSmartZone')
					uiDisplay.addUIChangeListener(menuItem(text: 'Save As', mnemonic: 'A', actionPerformed: { fileSaveAs() }), 'saveAsFile')
					separator()
					uiDisplay.addUIChangeListener(menuItem(text: 'Load Component Library', mnemonic: 'l', actionPerformed: { openClib() }), 'loadComponentLib')
					componentLibMenu = menu(text: 'Remove Component Library', mnemonic: 'R') {
						subMenuNoComponentLib = menuItem(text: 'No Loaded Component Library', enabled: false)
					}
					uiDisplay.addUIChangeListener(componentLibMenu, 'removeComponentLib')
					separator()
					menuItem(text: 'Exit', mnemonic: 'x', actionPerformed: { fileExit() })
				}
				menu('Edit', mnemonic: 'E') {
					uiDisplay.addUIChangeListener(menuItem(text: 'Undo', mnemonic: 'U', accelerator: shortcut('Z'), actionPerformed: { undoBuffer.undo() }), 'undo')
					uiDisplay.addUIChangeListener(menuItem(text: 'Redo', mnemonic: 'R', accelerator: shortcut('Y'), actionPerformed: { undoBuffer.redo() }), 'redo')
					separator()
					//uiDisplay.addUIChangeListener(menuItem(name: 'copy', text: 'Copy', mnemonic: 'C', accelerator: KeyStroke.getKeyStroke( KeyEvent.VK_C, ActionEvent.CTRL_MASK ), actionPerformed: { nodeOperator.doCopy() }), 'clone')
					//uiDisplay.addUIChangeListener(menuItem(name: 'paste', text: 'Paste', mnemonic: 'P', accelerator: KeyStroke.getKeyStroke( KeyEvent.VK_V, ActionEvent.CTRL_MASK ), actionPerformed: { nodeOperator.doPaste(null) }), 'clone')
					copyItem = menuItem(name: "copy", text: CentralCatalogue.getUIS('menu.component.copy.label'),
							mnemonic: CentralCatalogue.getUIS('menu.component.copy.mnemonic'),
							accelerator: shortcut(CentralCatalogue.getUIS('menu.component.copy.shortcut')))
					pasteItem = menuItem(name: "paste", text: CentralCatalogue.getUIS('menu.component.paste.label'),
							mnemonic: CentralCatalogue.getUIS('menu.component.paste.mnemonic'),
							accelerator: shortcut(CentralCatalogue.getUIS('menu.component.paste.shortcut')))
					/*
					pasteProxyItem = menuItem(name:"pasteProxy", text: CentralCatalogue.getUIS('menu.component.pasteAsProxy.label'),
											  mnemonic: CentralCatalogue.getUIS('menu.component.pasteAsProxy.mnemonic'),
											  accelerator: shortcut(CentralCatalogue.getUIS('menu.component.pasteAsProxy.shortcut')))
					*/
					uiDisplay.addUIChangeListener(menuItem(name: "createProxy", text: CentralCatalogue.getUIS('menu.component.newProxy.label'),
							mnemonic: CentralCatalogue.getUIS('menu.component.newProxy.mnemonic'),
							accelerator: shortcut(CentralCatalogue.getUIS('menu.component.newProxy.shortcut')),
							actionPerformed: { toolManager.getTool('proxyadder')?.addProxy(selectModel.getExpandedSelection()) }), 'addComponent')

					uiDisplay.addUIChangeListener(menuItem(name: "moveToDrawing", text: CentralCatalogue.getUIS('menu.component.moveToDrawing.label'),
							mnemonic: CentralCatalogue.getUIS('menu.component.moveToDrawing.mnemonic'),
							actionPerformed: { moveSelectionToDrawing() }), 'moveToDrawing')

					separator()
					uiDisplay.addUIChangeListener(menuItem(name: 'selectAll', text: 'Select All', mnemonic: 'A', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK), actionPerformed: { selectModel.setSelection(deploymentPanel.listAllVisibleNodes().collect { it.getComponent() }) }), 'select')
					uiDisplay.addUIChangeListener(menuItem(name: 'unselectAll', text: 'Un-Select All', mnemonic: 'U', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK), actionPerformed: { selectModel.setSelection([]) }), 'select')
					separator()
					uiDisplay.addUIChangeListener(menuItem(name: 'delete', text: 'Delete', mnemonic: 'D', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), actionPerformed: { ae -> this.deleteNode(ae) }), 'deleteComponent')
					separator()
					uiDisplay.addUIChangeListener(menuItem(text: 'Find...', mnemonic: 'F', accelerator: shortcut('F'), actionPerformed: { spotlightSearch() }), 'spotlight')
					uiDisplay.addUIChangeListener(menuItem(text: 'Replace...', mnemonic: 'R', accelerator: shortcut('G'), actionPerformed: { FindReplaceController.displayDialog(componentModel, selectModel, undoBuffer) }), 'findReplace')
					separator()
					uiDisplay.addUIChangeListener(menuItem(text: 'Rotate Selection Right', mnemonic: 'R', accelerator: shortcut('W'), actionPerformed: { editRotateSelection(NodeGroupRotator.SMALL_RIGHT) }), 'rotateRight')
					uiDisplay.addUIChangeListener(menuItem(text: 'Rotate Selection Left', mnemonic: 'L', accelerator: shortcut('Q'), actionPerformed: { editRotateSelection(NodeGroupRotator.SMALL_LEFT) }), 'rotateLeft')
					uiDisplay.addUIChangeListener(menuItem(text: 'Rotate Selection 90\u00b0 Right', mnemonic: 'R', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_W, ActionEvent.SHIFT_MASK + ActionEvent.CTRL_MASK), actionPerformed: { editRotateSelection(NodeGroupRotator.BIG_RIGHT) }), 'rotateRight')
					uiDisplay.addUIChangeListener(menuItem(text: 'Rotate Selection 90\u00b0 Left', mnemonic: 'L', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.SHIFT_MASK + ActionEvent.CTRL_MASK), actionPerformed: { editRotateSelection(NodeGroupRotator.BIG_LEFT) }), 'rotateLeft')
					uiDisplay.addUIChangeListener(menuItem(text: 'Rotate Selection around Center...', mnemonic: 'S', actionPerformed: { editRotateSelectionArbitrary() }), 'rotateLeft')
					separator()
					uiDisplay.addUIChangeListener(menuItem(text: 'Rotate Drawing 90\u00b0 Right', mnemonic: 'R', accelerator: shortcut('R'), actionPerformed: { editRotate90(Math.PI / 2.0) }), 'rotateRight')
					uiDisplay.addUIChangeListener(menuItem(text: 'Rotate Drawing 90\u00b0 Left', mnemonic: 'L', accelerator: shortcut('L'), actionPerformed: { editRotate90(-Math.PI / 2.0) }), 'rotateLeft')
					separator()

					uiDisplay.addUIChangeListener(menu(text: 'Align', mnemonic: 'A') {
						uiDisplay.addUIChangeListener(menuItem(name: 'alignR', text: 'Align Left', mnemonic: 'L', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, ActionEvent.CTRL_MASK), actionPerformed: { popupMenu.aligner(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "Align Left")) }), 'align')
						uiDisplay.addUIChangeListener(menuItem(name: 'alignR', text: 'Align Right', mnemonic: 'R', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, ActionEvent.CTRL_MASK), actionPerformed: { popupMenu.aligner(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "Align Right")) }), 'align')
						uiDisplay.addUIChangeListener(menuItem(name: 'alignT', text: 'Align Top', mnemonic: 'T', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_UP, ActionEvent.CTRL_MASK), actionPerformed: { popupMenu.aligner(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "Align Top")) }), 'align')
						uiDisplay.addUIChangeListener(menuItem(name: 'alignB', text: 'Align Bottom', mnemonic: 'B', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, ActionEvent.CTRL_MASK), actionPerformed: { popupMenu.aligner(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "Align Bottom")) }), 'align')
					}, 'align')

					uiDisplay.addUIChangeListener(menu(text: 'Distribute', mnemonic: 'B') {
						uiDisplay.addUIChangeListener(menuItem(name: 'distributeVertical', text: 'Distribute Vertical', mnemonic: 'V', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SLASH, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK), actionPerformed: { popupMenu.distributeVertical() }), 'distribute')
						uiDisplay.addUIChangeListener(menuItem(name: 'distributeHorizontal', text: 'Distribute Horizontal', mnemonic: 'H', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SLASH, ActionEvent.CTRL_MASK), actionPerformed: { popupMenu.distributeHorizontal() }), 'distribute')
					}, 'distribute')

					uiDisplay.addUIChangeListener(menu(text: 'Order', mnemonic: 'O') {
						uiDisplay.addUIChangeListener(menuItem(name: 'moveToFront', text: 'Bring To Front', mnemonic: 'F', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_CLOSE_BRACKET, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK), actionPerformed: { deploymentPanel.moveToFront(selectModel.getExpandedSelection()) }), 'ordering')
						uiDisplay.addUIChangeListener(menuItem(name: 'moveForward', text: 'Bring Forward', mnemonic: 'O', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_CLOSE_BRACKET, ActionEvent.CTRL_MASK), actionPerformed: { deploymentPanel.moveForward(selectModel.getExpandedSelection()) }), 'ordering')
						uiDisplay.addUIChangeListener(menuItem(name: 'moveBackward', text: 'Send Backward', mnemonic: 'A', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_OPEN_BRACKET, ActionEvent.CTRL_MASK), actionPerformed: { deploymentPanel.moveBackward(selectModel.getExpandedSelection()) }), 'ordering')
						uiDisplay.addUIChangeListener(menuItem(name: 'moveToBack', text: 'Send To Back', mnemonic: 'B', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_OPEN_BRACKET, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK), actionPerformed: { deploymentPanel.moveToBack(selectModel.getExpandedSelection()) }), 'ordering')
					}, 'ordering')

					uiDisplay.addUIChangeListener(menuItem(name: 'setPinHover', text: 'Pin Current Hover', mnemonic: 'P', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_COMMA, ActionEvent.CTRL_MASK), actionPerformed: { toolManager.getTool("hover")?.pinHover() }), 'pinHover')
					uiDisplay.addUIChangeListener(menuItem(name: 'unPinHover', text: 'Un-Pin Current Hover', mnemonic: 'U', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_COMMA, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK), actionPerformed: { toolManager.getTool("hover")?.unPinHover() }), 'pinHover')
					uiDisplay.addUIChangeListener(menuItem(name: 'clearPinHover', text: 'Clear Pinned Hover', mnemonic: 'H', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_PERIOD, ActionEvent.CTRL_MASK), actionPerformed: { toolManager.getTool("hover")?.clearPin() }), 'pinHover')
					separator()
					uiDisplay.addUIChangeListener(menuItem(text: 'Clear Undo/Redo Buffer', mnemonic: 'B', actionPerformed: { undoBuffer.clear() }), 'clearUndoRedoBuffer')
					separator()
					uiDisplay.addUIChangeListener(menuItem(text: 'Replace Configuration', mnemonic: 'C', actionPerformed: { configurationReplacer.openConfigDialog() }), 'replaceConfiguration')
					uiDisplay.addUIChangeListener(menuItem(text: 'Reposition Secondary Nodes', mnemonic: 'S', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), actionPerformed: { deploymentPanel.repositionSecondaryNodes() }), 'repositionSecondaryNodes')
					uiDisplay.addUIChangeListener(menuItem(name: 'console', text: 'Activate MapSense Console', mnemonic: 'N', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0), actionPerformed: { activateConsole() }), 'occulo')

					//uiDisplay.addUIChangeListener(menuItem(name: 'copyformove', text: 'Mark for Move', actionPerformed: { nodeOperator.doCopy(selectModel.getExpandedSelection()) }), 'clone')
					//uiDisplay.addUIChangeListener(menuItem(name: 'pasteformove', text: 'Paste for Move', actionPerformed: { nodeOperator.doMove(deploymentPanel.getViewCenter()) }), 'clone')

				}
				menu('Project', mnemonic: 'P') {
					uiDisplay.addUIChangeListener(menuItem(text: 'Change User Level', mnemonic: 'L', accelerator: shortcut('U'), actionPerformed: { showUserLevelDialog() }), 'changeUserLevel')
					uiDisplay.addUIChangeListener(menuItem(name: 'changeAccessCode', text: 'Change User Level Access Code', mnemonic: 'C', enabled: false, actionPerformed: { showAccessCodeDialog() }), 'changeAccessCode')
					separator()
					uiDisplay.addUIChangeListener(menuItem(text: CentralCatalogue.getUIS('menu.drawing.add.label'),
							mnemonic: CentralCatalogue.getUIS('menu.drawing.add.mnemonic'),
							accelerator: shortcut(CentralCatalogue.getUIS('menu.drawing.add.shortcut')),
							actionPerformed: { addDrawing(null) }), 'addDrawing')
					uiDisplay.addUIChangeListener(menuItem(text: CentralCatalogue.getUIS('menu.drawing.delete.label'),
							mnemonic: CentralCatalogue.getUIS('menu.drawing.delete.mnemonic'),
							actionPerformed: { deleteDrawing() }), 'deleteDrawing')
					uiDisplay.addUIChangeListener(menuItem(text: CentralCatalogue.getUIS('menu.drawing.import.label'),
							mnemonic: CentralCatalogue.getUIS('menu.drawing.import.mnemonic'),
							accelerator: shortcut(CentralCatalogue.getUIS('menu.drawing.import.shortcut')),
							actionPerformed: { importProject() }), 'importProject')
					uiDisplay.addUIChangeListener(menuItem(text: CentralCatalogue.getUIS('menu.drawing.addfromSelection.label'),
							mnemonic: CentralCatalogue.getUIS('menu.drawing.addfromSelection.mnemonic'),
							accelerator: shortcut(CentralCatalogue.getUIS('menu.drawing.addfromSelection.shortcut')),
							actionPerformed: { addDrawingFromSelection() }), 'addDrawingFromSelection')
					separator()
					uiDisplay.addUIChangeListener(menuItem(name: 'manageAccessList', text: 'Manage Access List', mnemonic: 'i', actionPerformed: { accessModelDialog.showAccessModelDialog() }), 'manageAccessList')
					uiDisplay.addUIChangeListener(menuItem(name: 'importMacScanList', text: 'Import MAC ID Scan List', mnemonic: 'i', actionPerformed: { importData("scan") }), 'importMacScanList')
					uiDisplay.addUIChangeListener(menuItem(name: 'importComponents', text: 'Import Component Properties', mnemonic: 'i', actionPerformed: { importComponentProperties()}), 'importComponents')
					uiDisplay.addUIChangeListener(menuItem(name: 'scanMacIds', text: 'Scan MAC IDs', mnemonic: 'i', accelerator: shortcut('alt M'), actionPerformed: { scanMAC()}), 'scanMacIds')
					separator()
					uiDisplay.addUIChangeListener(menuItem(name: 'loadBg', text: 'Change Background Image', mnemonic: 'C', enabled: false, actionPerformed: { setBGImage() }), 'loadBg')
					uiDisplay.addUIChangeListener(menuItem(name: 'removeBg', text: 'Remove Background Image', mnemonic: 'R', enabled: false, actionPerformed: { removeBGImage() }), 'removeBg')

					uiDisplay.addUIChangeListener(menuItem(name: 'setBgScale', text: 'Set Background Scale', mnemonic: 'S', actionPerformed: { setScale() }), 'setBgScale')
					uiDisplay.addUIChangeListener(menuItem(name: 'setCanvasSize', text: 'Set Canvas Size', mnemonic: 'S', actionPerformed: { setCanvasSize() }), 'setCanvasSize')
					separator()
					uiDisplay.addUIChangeListener(menuItem(text: 'Project Statistics', mnemonic: 's', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0), actionPerformed: { viewStats() }), 'viewStats')
					uiDisplay.addUIChangeListener(menuItem(text: 'Project Statistics by WSN Network', mnemonic: 's', accelerator: shortcut('F6'), actionPerformed: { viewStatsByWSN() }), 'viewStats')
					uiDisplay.addUIChangeListener(menuItem(name: 'fileHist', text: 'Project History', mnemonic: 'P', actionPerformed: { StyledMessageController.showStyledMessage(projectSettings.historyAsHTML(), "History", 0) }), 'viewStats')
					//uiDisplay.addUIChangeListener(menuItem(name: 'configureDefaults', text: 'Configure Defaults...', mnemonic: 'D', actionPerformed: {this.configureDefaults()}),'configureDefaults')
					uiDisplay.addUIChangeListener(menuItem(name: 'generalConfigOption', text: 'Project Options...', mnemonic: 'O', accelerator: shortcut('alt O'), actionPerformed: { getGeneralConfigDialog().setVisible(true) }), 'generalConfigOption')
					uiDisplay.addUIChangeListener(menuItem(name: 'configureSim', text: 'Configure Simulator...', mnemonic: 'i', actionPerformed: { this.configureSimulator() }), 'configureDefaults')
				}
				menu('Data Sources', mnemonic: 'D') {
					uiDisplay.addUIChangeListener(menuItem(text: 'Add WSN Network', mnemonic: 'N', accelerator: shortcut('alt N'), actionPerformed: { addNetwork("wsn") }), 'addWSN')
					uiDisplay.addUIChangeListener(menuItem(text: 'Add Modbus/TCP Network', mnemonic: 'M', actionPerformed: { addNetwork("modbustcp") }), 'addModbusTCP')
					uiDisplay.addUIChangeListener(menu(text: 'SNMP', mnemonic: 'P') {
						uiDisplay.addUIChangeListener(menuItem(text: 'Add SNMP V1 Agent', mnemonic: '1', actionPerformed: { addNetwork("snmpv1agent") }), 'addSnmpAgent')
						uiDisplay.addUIChangeListener(menuItem(text: 'Add SNMP V2c Agent', mnemonic: '2', actionPerformed: { addNetwork("snmpv2cagent") }), 'addSnmpAgent')
						uiDisplay.addUIChangeListener(menuItem(text: 'Add SNMP V3 Agent', mnemonic: '3', actionPerformed: { addNetwork("snmpv3agent") }), 'addSnmpAgent')
					}, 'addSnmpAgent')

				    uiDisplay.addUIChangeListener(menuItem(text: 'Add BACnet Network', mnemonic: 'B', actionPerformed: { addNetwork("bacnet") }), 'addBacnet')
					//uiDisplay.addUIChangeListener(menuItem(text: 'Add LonTalk Network', mnemonic: 'L', actionPerformed: { addNetwork("lontalk") }), 'addLontalk')
					//uiDisplay.addUIChangeListener(menuItem(text: 'Add IPMI Network', mnemonic: 'I', actionPerformed: { addNetwork("ipmi") }), 'addIPMI')
					separator()
					uiDisplay.addUIChangeListener(menuItem(text: 'Change Data Source of Selection', accelerator: shortcut('alt D'), mnemonic: 'D', actionPerformed: { changeDataSource() }), 'addWSN')
				}
				menu('Groupings', mnemonic: 'G') {
					uiDisplay.addUIChangeListener(menuItem(text: 'Add Planning Group', mnemonic: 'P', accelerator: shortcut('alt P'), actionPerformed: { addGroup('logicalgroup') }), 'addLogicalGroup')
					uiDisplay.addUIChangeListener(menuItem(text: 'Add Calculation Group', accelerator: shortcut('alt C'), mnemonic: 'C', actionPerformed: { addGroup('logicalzone') }), 'addLogicalZone')
					separator()
					uiDisplay.addUIChangeListener(menuItem(text: 'Change Grouping of Selection', accelerator: shortcut('alt G'), mnemonic: 'G', actionPerformed: { changeGrouping() }), 'addZone')
					uiDisplay.addUIChangeListener(menuItem(text: CentralCatalogue.getUIS('menu.component.unchildLogicalGroup.label'),
					                                       actionPerformed: { LogicalGroupController.removeFromGroup( selectModel.getSelectedObjects() ) }), 'moveToZone')
				}
				menu('Export', mnemonic: 'X') {
					uiDisplay.addUIChangeListener(menuItem(text: 'Validate Project', mnemonic: 'v', accelerator: KeyStroke.getKeyStroke('F7'), actionPerformed: { showPreExportValidation() }), 'preExportValidation')
					uiDisplay.addUIChangeListener(menuItem(name: 'exportSchema', text: 'Sync with Server', mnemonic: 's', accelerator: KeyStroke.getKeyStroke('F8'), actionPerformed: { showPreExportValidation(true) }), 'exportSchema')

					uiDisplay.addUIChangeListener(menuItem(name: 'checkSync', text: 'Check File Sync', mnemonic: 's', actionPerformed: { this.checkSyncOnly() }), 'exportSchema')
					separator()
					uiDisplay.addUIChangeListener(menu('Config File', mnemonic: 'F') {
						uiDisplay.addUIChangeListener(menuItem(name: 'exportSimConfig', text: 'Simulator', mnemonic: 's', actionPerformed: { exportSimConfig() }), 'exportSimConfig')
						uiDisplay.addUIChangeListener(menuItem(name: 'exportSimConfigWithPower', text: 'Simulator with Power', mnemonic: 'P', actionPerformed: { exportSimConfigWithPower() }), 'exportSimConfigWithPower')
						//uiDisplay.addUIChangeListener(menuItem(name: 'exportSuperSimConfig', text: 'SuperSim', mnemonic: 'u', actionPerformed: {exportSuperSimConfig()}),'exportSuperSimConfig')
						uiDisplay.addUIChangeListener(menuItem(name: 'exportEnhancedSimConfig', text: 'Enhanced Sim', mnemonic: 'u', actionPerformed: { exportSimConfig(null, true) }), 'exportSimConfig')
						uiDisplay.addUIChangeListener(menuItem(name: 'exportEnhancedSimConfig', text: 'Enhanced Sim with Power', mnemonic: 'u', actionPerformed: { exportSimConfigWithPower(null, true) }), 'exportSimConfig')
						uiDisplay.addUIChangeListener(menuItem(name: 'exportDMConfig', text: 'Device Manager', mnemonic: 'd', actionPerformed: { exportDMConfig() }), 'exportDMConfig')
						uiDisplay.addUIChangeListener(menuItem(name: 'exportNodeNames', text: 'Node Names', mnemonic: 'd', actionPerformed: { exportData("nodename") }), 'exportNodeNames')
						uiDisplay.addUIChangeListener(menuItem(name: 'exportComponentModel', text: 'Component Properties', mnemonic: 'd', actionPerformed: { exportComponentDialog.showExportDialog() }), 'exportComponentModel')
						//uiDisplay.addUIChangeListener(menuItem(name: 'exportObjectModel', text: 'Object Model', mnemonic: 'd', actionPerformed: { exportData("object") }), 'exportObjectModel')
						//uiDisplay.addUIChangeListener(menuItem(name: 'exportPowerAssets', text: 'Power Assets', mnemonic: 'd', actionPerformed: { powerAssetExport() }), 'powerAssetExport')
					}, 'exportConfigFile')
					separator()
					uiDisplay.addUIChangeListener(menuItem(name: 'clearEsIds', text: 'Clear Object IDs', mnemonic: 'C', actionPerformed: { clearEsIds() }), 'clearEsIds')
					separator()
					uiDisplay.addUIChangeListener(menuItem(name: 'unexportSchema', text: 'Remove from SynapSoft', mnemonic: 'r', actionPerformed: { unExportSchema() }), 'unexportSchema')
					uiDisplay.addUIChangeListener(menuItem(name: 'unlinkFromSmartZone', text: 'Remove from SmartZone', mnemonic: 'r', actionPerformed: { unlinkFromSmartZone() }), 'unlinkFromSmartZone')
					uiDisplay.addUIChangeListener(menuItem(name: 'unexportSchemaCascade', text: 'EXPERIMENTAL: Cascade Remove from SynapSoft', mnemonic: 'c', actionPerformed: { unExportSchema(false) }), 'forceExportSchema')
					uiDisplay.addUIChangeListener(menuItem(name: 'fullExportSchema', text: 'Reinitialize Schema', mnemonic: 'i', actionPerformed: { exportSchema(true) }), 'forceExportSchema')
					uiDisplay.addUIChangeListener(menuItem(name: 'fullExportSchemaNoSync', text: 'Reinitialize Schema ignoring Sync', mnemonic: 'f', actionPerformed: { exportSchema(true, false) }), 'forceExportSchema')
					separator()
					uiDisplay.addUIChangeListener(menu('Export Image', mnemonic: 'E') {
						uiDisplay.addUIChangeListener(menuItem(text: 'Deployment Plan', mnemonic: 'D', accelerator: KeyStroke.getKeyStroke('F9'), actionPerformed: { exportDeploymentPlan() }), 'exportDeploymentPlan')
						uiDisplay.addUIChangeListener(menuItem(text: 'Background Image', mnemonic: 'B', actionPerformed: { exportBackgroundImage() }), 'exportBackgroundImage')
						uiDisplay.addUIChangeListener(menuItem(text: 'All Images', mnemonic: 'A', actionPerformed: { exportAllImages() }), 'exportAllImages')
					}, 'exportPng')
					uiDisplay.addUIChangeListener(menuItem(name: 'exportMacId', text: 'Export MAC ID Scan List', mnemonic: 'd', actionPerformed: { exportData("mac") }), 'exportMacId')
					separator()
					uiDisplay.addUIChangeListener(menuItem(name: 'soundBoard', text: 'Open Sound Board', mnemonic: 'S', actionPerformed: { debugHelper.soundBoard() }), 'occulo')
					uiDisplay.addUIChangeListener(menuItem(name: 'prepForExport', text: 'Prep for Export', mnemonic: 'P', actionPerformed: { debugHelper.prepForExport() }), 'autoMacAssign')
					uiDisplay.addUIChangeListener(menuItem(name: 'autoMacAssigner', text: 'Assign Mac IDs ', mnemonic: 'M', actionPerformed: { debugHelper.assignMacIDs() }), 'autoMacAssign')
					uiDisplay.addUIChangeListener(menuItem(name: 'autoMacCleaner', text: 'Clear Mac IDs ', mnemonic: 'C', actionPerformed: { debugHelper.clearMacIDs() }), 'autoMacClean')
					uiDisplay.addUIChangeListener(menuItem(name: 'unlinkSmartZone', text: 'Clear SmartZone IDs', mnemonic: 'U', actionPerformed: { CentralCatalogue.getOpenProject().unlinkFromSmartZone() }), 'autoMacClean')
					uiDisplay.addUIChangeListener(menuItem(name: 'allDirty', text: 'Make All Links Dirty ', mnemonic: 'D', actionPerformed: { objectModel.makeEverythingDirty() }), 'allDirty')
					uiDisplay.addUIChangeListener(menuItem(name: 'resetOldValues', text: 'Set all Old Values to null', mnemonic: 'D', actionPerformed: { objectModel.clearOldValues() }), 'allDirty')
					uiDisplay.addUIChangeListener(menuItem(name: 'noParents', text: 'Show Objects Without Parents ', mnemonic: 'P', actionPerformed: { debugHelper.noParents() }), 'noParents')
					uiDisplay.addUIChangeListener(menuItem(name: 'uniqueNamesForEverybody', text: 'Make Names Unique ', mnemonic: 'M', actionPerformed: { debugHelper.uniqueNamesForEverybody() }), 'autoMacAssign')
					uiDisplay.addUIChangeListener(menuItem(name: 'anonymize', text: 'Anonymize Project', mnemonic: 'A', actionPerformed: { debugHelper.anonymizeProject() }), 'anonymizeProject')
					uiDisplay.addUIChangeListener(menuItem(name: 'resetImages', text: 'Reset Image Tracking', mnemonic: 'I', actionPerformed: { componentModel.resetImages() }), 'clearEsIds')
					uiDisplay.addUIChangeListener(menuItem(name: 'flushChecksum', text: 'Reset Checksum Values', mnemonic: 'F', actionPerformed: { debugHelper.flushChecksum() }), 'autoMacAssign')
					uiDisplay.addUIChangeListener(menuItem(name: 'jmxconsole', text: 'JMX Console', mnemonic: 'J', actionPerformed: { debugHelper.jmxConsole() }, accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_F1, ActionEvent.ALT_MASK)), 'getInfo')
				}
				menu('View', mnemonic: 'V') {
					uiDisplay.addUIChangeListener(menuItem(text: 'Zoom In', mnemonic: 'I', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_ADD, java.awt.event.InputEvent.CTRL_DOWN_MASK), actionPerformed: { viewZoomIn() }), 'zoomIn')
					uiDisplay.addUIChangeListener(menuItem(text: 'Zoom Out', mnemonic: 'O', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_SUBTRACT, java.awt.event.InputEvent.CTRL_DOWN_MASK), actionPerformed: { viewZoomOut() }), 'zoomOut')
					uiDisplay.addUIChangeListener(menuItem(text: 'Zoom 1:1', mnemonic: '1', actionPerformed: { viewZoom11() }), 'zoom11')
					uiDisplay.addUIChangeListener(menuItem(text: 'Zoom to Fit', mnemonic: 'F', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD0, java.awt.event.InputEvent.CTRL_DOWN_MASK), actionPerformed: { viewZoomToFit() }), 'zoomToFit')
					uiDisplay.addUIChangeListener(menuItem(text: 'Set Zoom', mnemonic: 'S', actionPerformed: { viewSetZoom() }), 'setZoom')
					separator()
					uiDisplay.addUIChangeListener(menuItem(text: 'View Options', mnemonic: 'V', actionPerformed: { showViewOption() }), 'viewOptions')
					uiDisplay.addUIChangeListener(menuItem(text: 'Toggle Names', mnemonic: 'N', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_DOWN_MASK), actionPerformed: { viewOptionToggleNames() }), 'toggleName')
					uiDisplay.addUIChangeListener(menuItem(text: 'Toggle Halos', mnemonic: 'H', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_H, java.awt.event.InputEvent.CTRL_DOWN_MASK), actionPerformed: { viewOptionToggleHalos() }), 'toggleHalos')
					uiDisplay.addUIChangeListener(menuItem(text: 'Toggle Associations', mnemonic: 'A', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_J, java.awt.event.InputEvent.CTRL_DOWN_MASK), actionPerformed: { viewOptionToggleAssociations() }), 'toggleAssoications')
					uiDisplay.addUIChangeListener(menuItem(text: 'Toggle Association Detail', mnemonic: 'D', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_DOWN_MASK), actionPerformed: { viewOptionToggleAssociationDetail() }), 'toggleAssoications')
					uiDisplay.addUIChangeListener(menuItem(text: 'Toggle Background Image', mnemonic: 'B', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_DOWN_MASK), actionPerformed: { viewOptionToggleBackground() }), 'toggleBackgroundImage')
					uiDisplay.addUIChangeListener(menuItem(text: 'Toggle Floor Tiles', mnemonic: 'E', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_DOWN_MASK), actionPerformed: { viewOptionToggleFloorTilesView() }), 'toggleFloorTiles')
				}

				scriptsMenu = menu('Scripts', mnemonic: 'S', menuSelected: { updateScriptsMenu() }) {
					uiDisplay.addUIChangeListener(menuItem(text: 'Reload', mnemonic: 'r', actionPerformed: {}), 'occulo')
				}

				menu('Help', mnemonic: 'H') {
					menuItem(text: 'User Guide', mnemonic: 'G', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0), actionPerformed: { openUserDocDialog() })
					menuItem(text: 'Component Encyclop\u00e6dia', mnemonic: 'E', accelerator: KeyStroke.getKeyStroke(KeyEvent.VK_F1, ActionEvent.SHIFT_MASK), actionPerformed: { openComponentEncyclopedia() })
					menuItem(text: 'About', mnemonic: 'A', actionPerformed: { helpAbout() })
					//menuItem(text: 'Shape Viewer', mnemonic: 'S', actionPerformed: {new ShapeViewer().go()})
				}
			}

			toolBar(constraints: BorderLayout.PAGE_START, layout: new BorderLayout(), autoscrolls: true, floatable: false, borderPainted: true, preferredSize: [800, 72]) {
				//BEGIN TOOLBAR BUTTONS
				toolBar(id: 'menuToolbar', constraints: BorderLayout.NORTH, autoscrolls: true, floatable: false, borderPainted: true) {
					uiDisplay.addUIChangeListener(button(name: 'newProject', icon: new ImageIcon(getClass().getResource('/resources/new.png')), toolTipText: 'New Project', actionPerformed: { fileNew() }), 'newFile')
					uiDisplay.addUIChangeListener(button(name: 'openProject...', icon: new ImageIcon(getClass().getResource('/resources/open_project.png')), toolTipText: 'Open Project', actionPerformed: { fileOpen() }), 'openFile')
					uiDisplay.addUIChangeListener(button(name: 'Save', icon: new ImageIcon(getClass().getResource('/resources/save_project.png')), toolTipText: 'Save', actionPerformed: { fileSave() }), 'saveFile')
					label(icon: ComponentIconFactory.getImageIcon('/resources/narrow_separator.png'), preferredSize: [16, 16])

					uiDisplay.addUIChangeListener(button(name: 'LoadComponentLibrary', icon: new ImageIcon(getClass().getResource('/resources/load_comp_lib.png')), toolTipText: 'Load Component Library', actionPerformed: { openClib() }), 'loadComponentLib')
					uiDisplay.addUIChangeListener(button(name: 'ChangeUserLevel', icon: new ImageIcon(getClass().getResource('/resources/edit_userlevel.png')), toolTipText: 'Change User Level', actionPerformed: { showUserLevelDialog() }), 'changeUserLevel')
					label(icon: ComponentIconFactory.getImageIcon('/resources/narrow_separator.png'), preferredSize: [16, 16])

					uiDisplay.addUIChangeListener(button(name: 'setBgScale', icon: new ImageIcon(getClass().getResource('/resources/scale_bg.png')), toolTipText: 'Set Background Scale', actionPerformed: { setScale() }), 'setBgScale')
					uiDisplay.addUIChangeListener(button(name: 'loadBg', icon: new ImageIcon(getClass().getResource('/resources/change_bg_image.png')), toolTipText: 'Reload Background Image ', actionPerformed: { setBGImage() }), 'loadBg')
					uiDisplay.addUIChangeListener(button(name: 'Undo', icon: new ImageIcon(getClass().getResource('/resources/arrow_undo.png')), toolTipText: 'Undo', actionPerformed: { undoBuffer.undo() }), 'undo')
					uiDisplay.addUIChangeListener(button(name: 'Redo', icon: new ImageIcon(getClass().getResource('/resources/arrow_redo.png')), toolTipText: 'Redo', actionPerformed: { undoBuffer.redo() }), 'redo')
					label(icon: ComponentIconFactory.getImageIcon('/resources/narrow_separator.png'), preferredSize: [16, 16])

					uiDisplay.addUIChangeListener(button(name: 'Find', icon: new ImageIcon(getClass().getResource('/resources/find.png')), toolTipText: 'Find', actionPerformed: { spotlightSearch() }), 'spotlight')
					uiDisplay.addUIChangeListener(button(name: 'ZoomIn', icon: new ImageIcon(getClass().getResource('/resources/zoom_in.png')), toolTipText: 'Zoom In', actionPerformed: { viewZoomIn() }), 'zoomIn')
					uiDisplay.addUIChangeListener(button(name: 'Zoom1:1', icon: new ImageIcon(getClass().getResource('/resources/zoom_1-1.png')), toolTipText: 'Zoom 1:1', actionPerformed: { viewZoom11() }), 'zoom11')
					uiDisplay.addUIChangeListener(button(name: 'ZoomOut', icon: new ImageIcon(getClass().getResource('/resources/zoom_out.png')), toolTipText: 'Zoom Out', actionPerformed: { viewZoomOut() }), 'zoomOut')
					uiDisplay.addUIChangeListener(button(name: 'ZoomToFit', icon: new ImageIcon(getClass().getResource('/resources/zoom_fit.png')), toolTipText: 'Zoom to Fit', actionPerformed: { viewZoomToFit() }), 'zoomToFit')
					label(icon: ComponentIconFactory.getImageIcon('/resources/narrow_separator.png'), preferredSize: [16, 16])

					uiDisplay.addUIChangeListener(button(name: 'Pre-exportValidation', icon: new ImageIcon(getClass().getResource('/resources/pre_export_validation.png')), toolTipText: 'Validate Project', actionPerformed: { showPreExportValidation() }), 'preExportValidation')
					uiDisplay.addUIChangeListener(button(name: 'exportSchema', icon: new ImageIcon(getClass().getResource('/resources/export.png')), toolTipText: 'Sync with Server', actionPerformed: { showPreExportValidation(true) }), 'exportSchema')
					uiDisplay.addUIChangeListener(button(name: 'DeploymentPlan', icon: new ImageIcon(getClass().getResource('/resources/export_deploymentplan.png')), toolTipText: 'Export Deployment Plan to PNG', actionPerformed: { exportDeploymentPlan() }), 'exportDeploymentPlan')
					label(icon: ComponentIconFactory.getImageIcon('/resources/narrow_separator.png'), preferredSize: [16, 16])

					uiDisplay.addUIChangeListener(button(name: 'ViewOptions', icon: new ImageIcon(getClass().getResource('/resources/options.png')), toolTipText: 'View Options', actionPerformed: { showViewOption() }), 'viewOptions')
					uiDisplay.addUIChangeListener(button(name: 'ViewStats', icon: new ImageIcon(getClass().getResource('/resources/project_stats.png')), toolTipText: 'View Stats', actionPerformed: { viewStats() }), 'viewStats')
					label(icon: ComponentIconFactory.getImageIcon('/resources/narrow_separator.png'), preferredSize: [16, 16])

					button(name: 'User Guide', icon: ComponentIconFactory.getImageIcon('/resources/help.png'), toolTipText: 'User Guide', actionPerformed: { openUserDocDialog() })


					uiDisplay.addUIChangeListener(button(name: 'replacetheuniverse', icon: ComponentIconFactory.getImageIcon('/resources/burn_16.png'), toolTipText: 'Replace Everything', actionPerformed: {
						def result = JOptionPane.showConfirmDialog(deploymentLabFrame, "Woah, woah, whoah!  This will call Replace Configuration on EVERYTHING.  Among other things, this will remove all historical information from the console.  Do you REALLY want to do this?", "Please do not press this button again.", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, ComponentIconFactory.getImageIcon("/resources/burn_48.png"))
						if (result == JOptionPane.YES_OPTION) {
							doBackgroundTask({
								projectSettings.addAnnotation("ATOMICS!")
								configurationReplacer.replaceEverything(false)
							})
						}
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'updatetheuniverse', icon: ComponentIconFactory.getImageIcon('/resources/burn_16.png'), toolTipText: 'Update Everything', actionPerformed: {
						def result = JOptionPane.showConfirmDialog(deploymentLabFrame, "Woah, woah, whoah!  This will call Update Configuration on EVERYTHING.  Do you REALLY want to do this?", "Please do not press this button again.", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, ComponentIconFactory.getImageIcon("/resources/burn_48.png"))
						if (result == JOptionPane.YES_OPTION) {
							doBackgroundTask({
								projectSettings.addAnnotation("UPDATE - No Plugins!")
								configurationReplacer.replaceEverything(true)
							})
						}
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'updatetheuniversewithplugins', icon: ComponentIconFactory.getImageIcon('/resources/burn_16.png'), toolTipText: 'Update Everything with Upgrade Controller', actionPerformed: {
						def result = JOptionPane.showConfirmDialog(deploymentLabFrame, "Woah, woah, whoah!  This will call Update Configuration on EVERYTHING.  Do you REALLY want to do this?", "Please do not press this button again.", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, ComponentIconFactory.getImageIcon("/resources/burn_48.png"))
						if (result == JOptionPane.YES_OPTION) {
							doBackgroundTask({
								projectSettings.addAnnotation("UPDATE! - With Plugins")
								UpgradeController uc = new UpgradeController( CentralCatalogue.getOpenProject(), true )
								uc.performUpgrade(DeploymentLabVersion.getModelVersionByDouble() - 0.1); // ## HACK! Change to be a selector of possible old versions.
							})
						}
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'memInfo', icon: ComponentIconFactory.getImageIcon('/resources/reports_16.png'), toolTipText: 'memory report', actionPerformed: {
						StringBuilder message = new StringBuilder()
						def df = new DecimalFormat()
						def rt = Runtime.getRuntime()
						message.append("Free Memory ${df.format(rt.freeMemory())} B \n")
						message.append("Used Memory ${df.format(rt.totalMemory() - rt.freeMemory())} B\n")
						message.append("Current Total Memory ${df.format(rt.totalMemory())} B\n")

						message.append("---\n")
						message.append("Max Heap Size ${df.format(rt.maxMemory())} B\n")

						message.append("---\n")
						message.append("Number of Objects: ${objectModel.getObjectCount()} \n")
						message.append("Number of Components: ${componentModel.getComponents().size()} \n")

						List drawingList = componentModel.getComponentsByType('drawing')
						int inMemoryCnt = 0
						for (DLComponent drawing : drawingList) {
							if (drawing.getPropertyValue('image_data')?.isInMemory()) {
								inMemoryCnt++
							}
						}
						message.append("Images in Memory: $inMemoryCnt of ${drawingList.size()}\n")
						//message.append( "---\n")
						message.append("Java Version: ${System.getProperty("java.version")} (${System.getProperty("java.specification.version")}) ${System.getProperty("java.vm.name")} from ${System.getProperty("java.vendor")} \n")
						message.append("Groovy Version: ${ GroovySystem.version }")
						log.info(message.toString(), "message")
					}), 'lightning')


					uiDisplay.addUIChangeListener(button(name: 'rungc', icon: ComponentIconFactory.getImageIcon('/resources/trash_16.png'), toolTipText: 'run gc', actionPerformed: {
						System.gc();
					}), 'lightning')


					uiDisplay.addUIChangeListener(button(name: 'fakesomeracks', icon: ComponentIconFactory.getImageIcon('/resources/wizard_16.png'), toolTipText: 'Fake Power Model', actionPerformed: {
						def usr = JOptionPane.showConfirmDialog(null, "This will randomly generate objects under power_racks for this drawing.", "Please Do Not Press This Button Again.", JOptionPane.YES_NO_OPTION)
						if (usr != JOptionPane.YES_OPTION) {
							return;
						}
						Properties prop = new Properties()
						if (loginDialog.getLoginInfo(prop)) {
							String serverUrl = SynapEnvContext.populateProperties(prop, projectSettings)
							SynapEnvContext ctx = new SynapEnvContext(prop)
							DeploymentLab.Examinations.PowerImagingRuleExamination.setEnv(ctx.getEnv())
							if (ctx.testConnect()) {
								DLObject drawing = selectModel.getActiveDrawing().getObject( ObjectType.DRAWING )
								def results = PowerImagingRuleExamination.createRPDUS(TOFactory.getInstance().loadTO(drawing.key))
								println results
								//call the exporter
								this.exportSimConfig(results)
							} else {
								log.error(ctx.getMessage(), 'message');
								ctx.logout()
							}
							log.info("I've done all the damage I can do.", "message")
						}
					}), 'lightning')


					uiDisplay.addUIChangeListener(button(name: 'showlids', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'show lids', actionPerformed: {
						String message = ""
						objectModel.getObjectsByType("wsnnode").each { node ->
							message += Long.toHexString(node.getObjectProperty("mac").getValue() ?: 0).padLeft(16, "0") + "=>" + Integer.toHexString(node.getObjectProperty("id").getValue() ?: 0) + " : " + node.getObjectProperty("name").getValue() + "\n"
						}
						objectModel.getObjectsByType("wsngateway").each { node ->
							message += Long.toHexString(node.getObjectProperty("mac").getValue() ?: 0).padLeft(16, "0") + "=>" + Integer.toHexString(node.getObjectProperty("id").getValue()) + ": " + node.getObjectProperty("name").getValue() + "\n"
						}
						log.info(message, "message")
					}), 'lightning')

					//BEGIN TESTING BUTTONS
					uiDisplay.addUIChangeListener(button(name: 'Display Undo', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'UNDO STACK', actionPerformed: { undoBuffer.display() }), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'All Model', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'All Model', actionPerformed: {
						def message = ""
						message += "Total Components ${componentModel.getComponents().size()}\n"
						message += "Total Objects ${objectModel.getObjects().size()}\n"
						message += "Listing Components: \n"
						componentModel.getComponents().each { c -> message += "$c \n" }
						message += "Listing Objects:\n"
						objectModel.getObjects().each { o -> message += "$o \n" }
						log.info(message, "message")
					}), 'lightning')


					uiDisplay.addUIChangeListener(button(name: 'showlids', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'show lids', actionPerformed: {
						String message = ""
						objectModel.getObjectsByType("wsnnode").each { node ->
							message += Long.toHexString(node.getObjectProperty("mac").getValue() ?: 0).padLeft(16, "0") + "=>" + Integer.toHexString(node.getObjectProperty("id").getValue() ?: 0) + " : " + node.getObjectProperty("name").getValue() + "\n"
						}
						objectModel.getObjectsByType("wsngateway").each { node ->
							message += Long.toHexString(node.getObjectProperty("mac").getValue() ?: 0).padLeft(16, "0") + "=>" + Integer.toHexString(node.getObjectProperty("id").getValue()) + ": " + node.getObjectProperty("name").getValue() + "\n"
						}
						log.info(message, "message")
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'settings', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'project settings', actionPerformed: {
						log.info(projectSettings.toString(), "message")
					}), 'lightning')


					uiDisplay.addUIChangeListener(button(name: 'deleted', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'deleted', actionPerformed: {
						def message = "Deleted Objects:"
						objectModel.listDeletedObjects().each {
							message += "${it.toString()} [${it.getKey()}] \n"
						}
						log.info(message, "message")
					}), 'lightning')


					uiDisplay.addUIChangeListener(button(name: 'tools', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'tools', actionPerformed: {
						log.info(toolManager.getActiveTools().toString(), "message")
					}), 'lightning')


					uiDisplay.addUIChangeListener(button(name: 'copyxml', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'copyxml', actionPerformed: {
						String message = ""
						message = SelectionTransferForge.copyToXML(selectModel)
						log.info(message, "message")
						SelectionTransferForge.pasteFromXML(message, componentModel, selectModel, undoBuffer, nodeOperator, deploymentPanel)
					}), 'lightning')


					uiDisplay.addUIChangeListener(button(name: 'wsntest', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'wsntest', actionPerformed: {
						componentModel.components.each { c ->
							WSNExamination.checkNodes(c)
						}
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'prunenull', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'prune null', actionPerformed: {
						def objList = []
						objectModel.getObjects().each { o ->
							if (o.getDlid() == null) {
								//objectModel.remove(o)
								objList += o
							}
						}
						objList.each { objectModel.remove(it) }
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'defs', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'regenerate the objects.xml file', actionPerformed: {
						def strwriter = new StringWriter()
						def strbuilder = new groovy.xml.MarkupBuilder(strwriter)
						objectModel.makeDefinitionFile(strbuilder)
						String allObjects = strwriter.toString()
						//println allObjects
						log.info(allObjects, "message")
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'objs', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'make objs half of the DL file', actionPerformed: {
						ImageExporter ie = new ImageExporter()
						ie.exportToEs(componentModel)

						log.info(objectModel.makeObjectsString(), "message")
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'allObjs', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'All Objects & Keys', actionPerformed: {
						StringBuilder message = new StringBuilder()
						message.append("Listing Objects:\n")
						objectModel.getObjects().each { o ->
							message.append("$o Key:${o.getKey()} \n")
						}
						log.info(message.toString(), "message")
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'toggleCtrlTestSvr', icon: ComponentIconFactory.getImageIcon('/resources/components/crah_single_tn_16.png'), toolTipText: 'Use Fake Ctrl Expression Tester', actionPerformed: { ae ->
						debugHelper.toggleFakeCtrlSvrFlag()
						if( debugHelper.useFakeControlServer ) {
							((JButton)ae.getSource()).setToolTipText("Use Configured URI Ctrl Expression Tester")
						} else {
							((JButton)ae.getSource()).setToolTipText("Use Fake Ctrl Expression Tester")
						}
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'containedPolygonPoints', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'Contained Polygon Points', actionPerformed: {
						debugHelper.showContainedPolygon()
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'currselection', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'Current Selection', actionPerformed: {
						//log.info(selectModel.getExpandedSelection().toString().replace(",", "\n"), "message")
						String message = selectModel.getExpandedSelection().toString().replace(",", "\n")
						message += "\npinned:\n"
						message += hovermatic.pinnedHoverNodes.toString().replace(",", "\n")

						log.info(message, "message")
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'unstash', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'UnStash', actionPerformed: {
						Properties prop = new Properties()
						if (loginDialog.getLoginInfo(prop)) {
							def f = new UnStash()
							f.fetch(projectSettings.esHost, prop.getProperty("username"), prop.getProperty("password"))
							log.info("unstashed!", "statusbar")
						}
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'findorphans', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'Find Orphans', actionPerformed: {
						def noComponent = []
						def noParent = []
						objectModel.getObjects().each { o ->
							if( !o.deleted ) {
								if( !componentModel.getManagingComponent(o) ) {
									noComponent += o
								} else {
									if( o.getParents().isEmpty() ) {
										noParent += o
									}
								}
							}
						}
						StringBuilder sb = new StringBuilder()
						sb.append("**** Objects with no managing Component:\n")
						noComponent.each { sb.append("$it \n") }
						sb.append("\n**** Objects with no parent Object:\n")
						noParent.each { sb.append("$it   Owner: ${componentModel.getManagingComponent(it)}\n") }
						log.info(sb.toString(), "message")

					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'showNotExportable', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'Show Not Exportable', actionPerformed: {
						// Find all the objects that won't export.
						List<String> comps = []
						List<String> objsHidden = []
						List<String> objsRogue = []
						componentModel.getComponents().each { c ->
							if( !c.isExportable() ) {
								comps += "  $c"
							}

							// Check objects that don't match the comp. These are probably bugs somewhere.
							def objs = ( c.hasRole("proxy") ? c.getRoots() : c.getManagedObjects() )
							objs.each { o ->
								if( !o.isExportable() && c.isExportable() ) {
									objsHidden += "  $o   OWNER: $c"
								} else if( o.isExportable() && !c.isExportable() ) {
									objsRogue += "  $o   OWNER: $c"
								}
							}
						}

						StringBuilder sb = new StringBuilder()
						if( !comps.isEmpty() ) {
							sb.append("**** Components Not Exporting:\n")
							comps.each { sb.append("$it \n") }
						}
						if( !objsHidden.isEmpty() ) {
							sb.append("\n**** ERROR! Not Exporting Object when Component is:\n")
							objsHidden.each { sb.append("$it \n") }
						}
						if( !objsRogue.isEmpty() ) {
							sb.append("\n**** ERROR! Exporting Object when Component is not:\n")
							objsRogue.each { sb.append("$it \n") }
						}
						if( sb.length() == 0 ) {
							sb.append("Nothing to see here. Everything will export.\n")
						}
						log.info(sb.toString(), "message")
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'fakeKeys', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'fake some es keys', actionPerformed: {
						int keys = 0
						objectModel.getObjects().each { o ->
							if (!o.hasKey()) {
								keys++
								o.setKey("FAKOR:${o.getDlid().toString().substring(5)}")
							}
						}
						log.info("Keys created for $keys objects out of ${objectModel.getObjects().size()}", "message")
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'activeItems', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'Active Items', actionPerformed: {

						String msg = "SelectModel Active Items\n\n"
						msg += "Drawing : ${selectModel.activeDrawing?.name}\n"

						if (selectModel.activeNetwork != null) {
							msg += "Data Source: ${selectModel.activeNetwork.getDrawing().name} -> ${selectModel.activeNetwork.name}\n"
						} else {
							msg += "Data Source: null\n"
						}

						if (selectModel.activeZone) {
							msg += "Grouping   : ${selectModel.activeZone.getDrawing().name} -> ${selectModel.activeZone.name}\n"
						} else {
							msg += "Grouping   : null\n"
						}

						log.info(msg, "message")
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'shapes', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'shapes', actionPerformed: {
						new ShapeViewer().go()
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'randomize', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'randomize', actionPerformed: {
						debugHelper.randomlySynergizeMeAFDrawing()
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'hyperassoc', icon: ComponentIconFactory.getImageIcon('/resources/effects_16.png'), toolTipText: 'hyperassoc', actionPerformed: {
						debugHelper.hyperassoc()
					}), 'lightning')

					uiDisplay.addUIChangeListener(button(name: 'purgesmartzonecfg', icon: ComponentIconFactory.getImageIcon('/resources/company_green_16x16.png'), toolTipText: 'Purge SmartZone Config', actionPerformed: {
						println "Purging stored SmartZone host and user config."
						CentralCatalogue.getSystemSettings().setSmartZoneHost("")
						CentralCatalogue.getSmartZoneConnecionProperties().remove("username")
					}), 'lightning')

					//END TESTING BUTTONS

				} //END TOOLBAR BUTTONS
				toolBar(id: 'toolbar', constraints: BorderLayout.WEST, autoscrolls: true, floatable: false, borderPainted: false, layout: new FlowLayout(FlowLayout.LEADING), margin: new Insets(0, 0, 0, 0)) {
					uiDisplay.addUIChangeListener(toggleButton(id: 'select', buttonGroup: toolButtonGroup, toolTipText: 'Select (Alt+1)',
							mnemonic: KeyEvent.VK_1,
							icon: ComponentIconFactory.getIconByName("select"),
							//actionPerformed: {toolManager.setActiveTools(['hover', 'rotator2', 'rectselect', 'panzoomer', 'selector', 'mover', 'keycommands', 'popupmenu', 'fasternamer', 'currentlocation']); paletteController.clearSelection();  deploymentPanel.focus(); },
							actionPerformed: { toolManager.activateDefaults(); paletteController.clearSelection(); deploymentPanel.focus(); },
							selected: true), 'select')

					uiDisplay.addUIChangeListener(toggleButton(id: 'rotate', buttonGroup: toolButtonGroup, toolTipText: 'Rotate (Alt+2)',
							mnemonic: KeyEvent.VK_2,
							icon: ComponentIconFactory.getIconByName("rotate"),
							actionPerformed: { toolManager.setActiveTools(['hover', 'rotator', 'panzoomer', 'popupmenu', 'fasternamer', 'currentlocation']); paletteController.clearSelection(); deploymentPanel.focus(); }), 'rotate')
/*
					uiDisplay.addUIChangeListener(toggleButton(id: 'rotate2', buttonGroup: toolButtonGroup, toolTipText: 'Group Rotate (Alt+7)',
							mnemonic: KeyEvent.VK_7,
							icon: ComponentIconFactory.getIconByName("group_rotate"),
							actionPerformed: {toolManager.setActiveTools(['hover', 'rotator2', 'panzoomer', 'selector', 'popupmenu', 'fasternamer', 'currentlocation']); paletteController.clearSelection(); deploymentPanel.focus(); }),'rotate')
*/
					uiDisplay.addUIChangeListener(toggleButton(id: 'newassociate', buttonGroup: toolButtonGroup, toolTipText: 'Associate (Alt+3)',
							mnemonic: KeyEvent.VK_3,
							icon: ComponentIconFactory.getIconByName("associate"),
							actionPerformed: { toolManager.setActiveTools(['hover', 'newassoc', 'panzoomer', 'popupmenu', 'fasternamer', 'currentlocation']); paletteController.clearSelection(); deploymentPanel.focus(); }), 'associate')

					uiDisplay.addUIChangeListener(toggleButton(id: 'quickname', buttonGroup: toolButtonGroup, toolTipText: 'Quick Name (Alt+4)',
							mnemonic: KeyEvent.VK_4,
							icon: ComponentIconFactory.getIconByName("quickname"),
							actionPerformed: { toolManager.setActiveTools(['hover', 'namer', 'panzoomer', 'popupmenu', 'fasternamer', 'currentlocation']); paletteController.clearSelection(); }), 'quickName')

					uiDisplay.addUIChangeListener(toggleButton(id: 'macassign', buttonGroup: toolButtonGroup, toolTipText: 'Mac Assign (Alt+5)',
							mnemonic: KeyEvent.VK_5,
							icon: ComponentIconFactory.getIconByName("macassign"),
							actionPerformed: { toolManager.setActiveTools(['hover', 'macassigner', 'panzoomer', 'popupmenu', 'fasternamer', 'currentlocation']); paletteController.clearSelection(); }), 'macassign')

					uiDisplay.addUIChangeListener(toggleButton(id: 'copySettings', buttonGroup: toolButtonGroup, toolTipText: CentralCatalogue.getUIS('copySettings.name') + " (Alt+6)",
							mnemonic: KeyEvent.VK_6,
							icon: ComponentIconFactory.getIconByName("log_in_24"),
							actionPerformed: { toolManager.setActiveTools(['hover', 'copysettings', 'panzoomer', 'popupmenu', 'fasternamer', 'currentlocation']); paletteController.clearSelection(); }), 'copySettings')
					/*
					uiDisplay.addUIChangeListener(toggleButton(id: 'createProxy',   buttonGroup: toolButtonGroup, toolTipText: CentralCatalogue.getUIS('menu.component.newProxy.label') + " (Alt+7)",
						mnemonic: KeyEvent.VK_7,
						icon: ComponentIconFactory.getIconByName("proxy"),
						actionPerformed: {toolManager.setActiveTools(['hover', 'proxyadder', 'panzoomer', 'popupmenu', 'fasternamer', 'currentlocation']); paletteController.clearSelection(); }),'addComponent')
					*/
				}

				toolBar(id: 'toggleGroup', constraints: BorderLayout.CENTER, floatable: false, borderPainted: false, autoscrolls: true, layout: new FlowLayout(FlowLayout.LEADING), margin: new Insets(0, 0, 0, 0)) {
					//label(icon: new ImageIcon(getClass().getResource('/resources/separator2.png')))
					label(text: "Display Filter:")
					environmentalsToggleBtn = toggleButton(id: 'toggleEnvironmentals',
							icon: ComponentIconFactory.getIconByName("environmentals"),
							toolTipText: 'Environmentals (Alt+N)', mnemonic: KeyEvent.VK_N, actionPerformed: { toggleDisplayGroupFilter('Environmentals') }, selected: true)
					uiDisplay.addUIChangeListener(environmentalsToggleBtn, 'toggleEnvironmentals')

					powerToggleBtn = toggleButton(id: 'togglePower',
							icon: ComponentIconFactory.getIconByName("power"),
							toolTipText: 'Power & Energy (Alt+P)',
							mnemonic: KeyEvent.VK_P,
							actionPerformed: { toggleDisplayGroupFilter('Power') },
							selected: true)
					uiDisplay.addUIChangeListener(powerToggleBtn, 'togglePower')

					pueToggleBtn = toggleButton(id: 'togglePue',
							icon: ComponentIconFactory.getIconByName("pue"),
							toolTipText: 'PUE (Alt+U)',
							mnemonic: KeyEvent.VK_U,
							actionPerformed: { toggleDisplayGroupFilter('Calculations') },
							selected: true)
					uiDisplay.addUIChangeListener(pueToggleBtn, 'togglePue')

					controlToggleBtn = toggleButton(id: 'toggleControl',
							icon: ComponentIconFactory.getIconByName("control"),
							toolTipText: 'Active Control (Alt+C)',
							mnemonic: KeyEvent.VK_C,
							actionPerformed: { toggleDisplayGroupFilter('Control') },
							selected: true)
					uiDisplay.addUIChangeListener(controlToggleBtn, 'toggleControl')

					containmentToggleBtn = toggleButton(id: 'toggleContainment',
							icon: ComponentIconFactory.getIconByName('containment'),
							toolTipText: 'Contained Area(Alt+A)',
							mnemonic: KeyEvent.VK_A,
							actionPerformed: { toggleDisplayGroupFilter('Containment') },
							selected: true)
					uiDisplay.addUIChangeListener(containmentToggleBtn, 'toggleContainment')


					setDisplayGroupFilter()
				}

				toolBar(id: "hidesidebars", constraints: BorderLayout.EAST, floatable: false, borderPainted: false, autoscrolls: true, layout: new FlowLayout(FlowLayout.LEADING), margin: new Insets(0, 0, 0, 0)) {

					uiDisplay.addUIChangeListener(
							button(icon: ComponentIconFactory.getIconByName("add"), toolTipText: CentralCatalogue.getUIS('menu.drawing.add.label'), actionPerformed: { addDrawing(null) }
							), "addDrawing")

					label(text: CentralCatalogue.getUIS("drawing.selector.label"))
					drawingComboBox = comboBox(model: drawingComboBoxModel, renderer: new ActiveWithIconRenderer(), actionPerformed: {
						def selected = drawingComboBoxModel.getSelectedItem()
						if (selected instanceof DLComponent) {
							selectModel.setActiveDrawing(selected)
							log.trace(String.format(CentralCatalogue.getUIS("activeItem.set.drawing"), selected.getName()), "statusbar")
						} else {
							selectModel.setActiveDrawing(null)
							log.trace(CentralCatalogue.getUIS("activeItem.none.drawing"), "statusbar")
						}
					})

					uiDisplay.addUIChangeListener(toggleButton(id: 'togglePalette',
							icon: ComponentIconFactory.getIconByName("toggleleft"),
							toolTipText: 'Toggle Palette',
							selected: true,
							actionPerformed: { leftSidePanel.setVisible(!leftSidePanel.isVisible()) }
					), 'toggleSidecars')

					uiDisplay.addUIChangeListener(toggleButton(id: 'togglePalette',
							icon: ComponentIconFactory.getIconByName("toggleright"),
							toolTipText: 'Toggle Properties',
							selected: true,
							actionPerformed: {
								//println "divider loc = ${splitPane3.getDividerLocation()}"
								//println "divider max loc = ${splitPane3.getMaximumDividerLocation()}"
								//println "divider width = ${splitPane3.getDividerSize()}"
								splitPane3.getRightComponent().setMinimumSize(new Dimension());
								if (splitPane3.getDividerLocation() < splitPane3.getMaximumDividerLocation() - splitPane3.getDividerSize()) {
									splitPane3.setDividerLocation(1.0)
								} else {
									splitPane3.resetToPreferredSizes()
								}
							}), 'toggleSidecars')
				}

			}

			outerPanel = panel(layout: new BorderLayout(), constraints: BorderLayout.CENTER) {
				//LEFT SIDE COLUMN
				leftSidePanel = panel(layout: new MigLayout(), constraints: BorderLayout.WEST) {

					label(text: CentralCatalogue.getUIS("network.selector.label"), constraints: "gap rel")
					networkComboBox = comboBox(items: [CentralCatalogue.getUIS("network.selector.noSelectionItem")], renderer: new ActiveWithIconRenderer(), actionPerformed: {
						def selected = netComboBoxCtrl.getModel(selectModel.getActiveDrawing()).getSelectedItem()
						if (selected instanceof DLComponent) {
							selectModel.setActiveNetwork(selected)
							log.trace(String.format(CentralCatalogue.getUIS("activeItem.set.ds"), selectModel.getActiveDrawing().getName(), selected.getName()), "statusbar")
						} else {
							selectModel.setActiveNetwork(null)
							log.trace(CentralCatalogue.getUIS("activeItem.none.ds"), "statusbar")
						}
					}, constraints: "grow x, wrap")

					label(text: CentralCatalogue.getUIS("group.selector.label"), constraints: "gap rel")
					zoneComboBox = comboBox(items: [CentralCatalogue.getUIS("group.selector.noSelectionItem")], renderer: new ActiveWithIconRenderer(), actionPerformed: {
						def selected = zoneComboBoxCtrl.getModel(selectModel.getActiveDrawing()).getSelectedItem()
						if (selected instanceof DLComponent) {
							selectModel.setActiveZone(selected)
							log.trace(String.format(CentralCatalogue.getUIS("activeItem.set.grouping"), selectModel.getActiveDrawing().getName(), selected.getName()), "statusbar")
						} else {
							selectModel.setActiveZone(null)
							log.trace(CentralCatalogue.getUIS("activeItem.none.grouping"), "statusbar")
						}
					}, constraints: "grow x, wrap")

					widget(paletteController.getPalette(), constraints: "span 2, wrap, grow x, grow y")

				}

				splitPane3 = splitPane(oneTouchExpandable: false, resizeWeight: 1, continuousLayout: true, preferredSize: [800, 800], constraints: BorderLayout.CENTER) {
					//CENTER PANEL
					widget(deploymentPanel)

					//RIGHT SIDE COLUMN
					splitPane4 = splitPane(oneTouchExpandable: true, resizeWeight: (float) 0.5, continuousLayout: true, orientation: JSplitPane.VERTICAL_SPLIT) {
						tabbedPane() {
							panel(minimumSize: [200, 250], title: 'Data Sources', layout: new BorderLayout()) {
								toolBar(id: 'datasourcetoolbar', constraints: BorderLayout.EAST, autoscrolls: true, floatable: false, borderPainted: true, orientation: SwingConstants.VERTICAL) {
									uiDisplay.addUIChangeListener(button(name: 'deleteDataSource', toolTipText: 'Remove Data Source', icon: ComponentIconFactory.getIconByName("delete"), actionPerformed: { deleteNetwork() }), 'deleteDataSource')
									separator()
									uiDisplay.addUIChangeListener(button(name: 'addWSN', toolTipText: 'Add WSN Network', icon: ComponentIconFactory.getIconByName("DS_WSN"), actionPerformed: { addNetwork("wsn") }), 'addWSN')
									uiDisplay.addUIChangeListener(button(name: 'addModTcp', toolTipText: 'Add Modbus/TCP Network', icon: ComponentIconFactory.getIconByName("DS_MODBUS"), actionPerformed: { addNetwork("modbustcp") }), 'addModbusTCP')
									uiDisplay.addUIChangeListener(button(name: 'addSNMP1', toolTipText: 'Add SNMP V1 Agent', icon: ComponentIconFactory.getIconByName("DS_SNMP"), actionPerformed: { addNetwork("snmpv1agent") }), 'addSnmpAgent')
									uiDisplay.addUIChangeListener(button(name: 'addSNMP2', toolTipText: 'Add SNMP V2c Agent', icon: ComponentIconFactory.getIconByName("DS_SNMP"), actionPerformed: { addNetwork("snmpv2cagent") }), 'addSnmpAgent')
									uiDisplay.addUIChangeListener(button(name: 'addSNMP3', toolTipText: 'Add SNMP V3 Agent', icon: ComponentIconFactory.getIconByName("DS_SNMP"), actionPerformed: { addNetwork("snmpv3agent") }), 'addSnmpAgent')
									uiDisplay.addUIChangeListener(button(name: 'addBacnet', toolTipText: 'Add BACnet Network', icon: ComponentIconFactory.getIconByName("DS_BACNET"), actionPerformed: { addNetwork("bacnet") }), 'addBacnet')
									//uiDisplay.addUIChangeListener(button(name: 'addLontalk', toolTipText: 'Add LonTalk Network', icon: ComponentIconFactory.getIconByName("DS_LONTALK"), actionPerformed: { addNetwork("lontalk") }), 'addLontalk')
									//uiDisplay.addUIChangeListener(button(name: 'addIPMI', toolTipText: 'Add IPMI Network', icon: ComponentIconFactory.getIconByName("DS_IPMI"), actionPerformed: { addNetwork("ipmi") }), 'addIPMI')
								}
								scrollPane(constraints: BorderLayout.CENTER) {
									widget(networkView)
								}
							}

							panel(preferredSize: [200, 250], title: 'Groupings', layout: new BorderLayout()) {
								toolBar(id: 'structuretoolbar', constraints: BorderLayout.EAST, autoscrolls: true, floatable: false, borderPainted: true, orientation: SwingConstants.VERTICAL) {
									uiDisplay.addUIChangeListener(button(name: 'deleteZone', toolTipText: 'Delete Node', icon: ComponentIconFactory.getIconByName("delete"), actionPerformed: { deleteZone() }), 'deleteZone')
									separator()
									uiDisplay.addUIChangeListener(button(name: 'addLogicalGroup', toolTipText: 'Add Planning Group', icon: ComponentIconFactory.getIconByName("add_planning_group"), actionPerformed: { addGroup('logicalgroup') }), 'addLogicalGroup')
									uiDisplay.addUIChangeListener(button(name: 'AddLogicalZone', icon: ComponentIconFactory.getIconByName("add_logical_group"), toolTipText: 'Add Calculation Group', actionPerformed: { addGroup('logicalzone') }), 'addLogicalZone')
								}
								scrollPane(constraints: BorderLayout.CENTER) {
									widget(structureView)
								}
							}
						}
						scrollPane(preferredSize: [200, 250]) {
							widget(propertyTable)
						}
					}
				}
//				}
			}

			// JSplitPane captures the F6 and F8 accelerator keys by default, this clears them out
//			SwingUtilities.replaceUIInputMap(splitPane1, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT, null)
			//SwingUtilities.replaceUIInputMap(splitPane2, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT, null)
			SwingUtilities.replaceUIInputMap(splitPane3, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT, null)
			SwingUtilities.replaceUIInputMap(splitPane4, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT, null)
			//SwingUtilities.replaceUIInputMap(splitPane5, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT, null)
//			SwingUtilities.replaceUIActionMap(splitPane1, null)
			//SwingUtilities.replaceUIActionMap(splitPane2, null)
			SwingUtilities.replaceUIActionMap(splitPane3, null)
			SwingUtilities.replaceUIActionMap(splitPane4, null)
			//SwingUtilities.replaceUIActionMap(splitPane5, null)

			panel(layout: new BorderLayout(), constraints: BorderLayout.PAGE_END, border: BorderFactory.createEtchedBorder(), preferredSize: [800, 30]) {
				label(id: 'statusbarText', constraints: BorderLayout.CENTER, horizontalAlignment: SwingConstants.CENTER, horizontalTextPosition: SwingConstants.CENTER, text: appProperties.getProperty('application.welcome'), mouseClicked: { builder.statusbarText.setText(""); builder.statusbarText.setIcon(null); })
				panel(constraints: BorderLayout.EAST, layout: new MigLayout()) {
					label(id: 'zoomFactorText', text: '', constraints: "gapright unrel")
					label(id: 'mousePositionText', text: '', constraints: "gapright unrel")
					label(id: 'userLevelText', text: '[' + uiDisplay.getUserLevel() + ']')
				}


			}
		}

		smartZoneLoginDialog = new SmartZoneLoginDialog(deploymentLabFrame, false)
		smartZoneServerDialog = new ServerDialog(deploymentLabFrame)

		CentralCatalogue.setParentFrame(deploymentLabFrame) //going forward, everything that needs a parent frame should grab this

		// Controllers for the Active Item combo boxes that are dependent on the Active Drawing selection.
		netComboBoxCtrl = new NetworkComboBoxController(selectModel, componentModel, networkComboBox)
		componentModel.addModelChangeListener(netComboBoxCtrl)
		selectModel.addSelectionChangeListener(netComboBoxCtrl)

		zoneComboBoxCtrl = new ZoneComboBoxController(selectModel, componentModel, zoneComboBox)
		componentModel.addModelChangeListener(zoneComboBoxCtrl)
		selectModel.addSelectionChangeListener(zoneComboBoxCtrl)

		// Make PageUp & PageDown cycle through the drawings throughout the application. Remember, the list is indexed
		// with higher numbers going down visually, so the logic is flipped from your normal human instincts.
		JComponent cycleDrawingAnchor = deploymentLabFrame.getRootPane();
		KeyStroke prevDrawingKey = KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_UP, 0)
		KeyStroke nextDrawingKey = KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, 0)

		clearExistingKeyBindings(prevDrawingKey)
		clearExistingKeyBindings(nextDrawingKey)

		def prevDrawingAction = builder.action(name: "prevDrawingAction", closure: {
			if (drawingComboBoxModel.getSize() > 1) {
				int idx = drawingComboBox.getSelectedIndex() - 1;
				if (idx < 1) {
					idx = drawingComboBoxModel.getSize() - 1;
				}
				selectModel.setActiveDrawing(drawingComboBoxModel.getElementAt(idx))
			}
		})
		cycleDrawingAnchor.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(prevDrawingKey, "prevDrawing");
		cycleDrawingAnchor.getInputMap(JComponent.WHEN_FOCUSED).put(prevDrawingKey, "prevDrawing");
		cycleDrawingAnchor.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(prevDrawingKey, "prevDrawing");
		cycleDrawingAnchor.getActionMap().put("prevDrawing", prevDrawingAction);

		def nextDrawingAction = builder.action(name: "nextDrawingAction", closure: {
			if (drawingComboBoxModel.getSize() > 1) {
				int idx = drawingComboBox.getSelectedIndex() + 1;
				if (idx >= drawingComboBoxModel.getSize()) {
					idx = 1;
				}
				selectModel.setActiveDrawing(drawingComboBoxModel.getElementAt(idx))
			}
		})
		cycleDrawingAnchor.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(nextDrawingKey, "nextDrawing");
		cycleDrawingAnchor.getInputMap(JComponent.WHEN_FOCUSED).put(nextDrawingKey, "nextDrawing");
		cycleDrawingAnchor.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(nextDrawingKey, "nextDrawing");
		cycleDrawingAnchor.getActionMap().put("nextDrawing", nextDrawingAction);

		//much like the splitpane, jtables capture the F8 key.  However, we don't want to blow away the whole input map, just replace the F8 entry
		//cook up a swing action to handle the F8 command to connect to the input map
		def exportAction = builder.action(name: "exportAction", closure: {
			if (uiDisplay.isAccessible("exportSchema")) {
				propertyTable.finishEditing()
				showPreExportValidation(true)
			}
		})
		propertyTable.getInputMap().put(KeyStroke.getKeyStroke("F8"), "exportSchema");
		propertyTable.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("F8"), "exportSchema");
		propertyTable.getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke("F8"), "exportSchema");
		propertyTable.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F8"), "exportSchema");
		propertyTable.getActionMap().put("exportSchema", exportAction);

		//same deal with the f7 key
		def pevAction = builder.action(name: "pevAction", closure: {
			if (uiDisplay.isAccessible("preExportValidation")) {
				propertyTable.finishEditing()
				showPreExportValidation()
			}
		})
		propertyTable.getInputMap().put(KeyStroke.getKeyStroke("F7"), "pev");
		propertyTable.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("F7"), "pev");
		propertyTable.getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke("F7"), "pev");
		propertyTable.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F7"), "pev");
		propertyTable.getActionMap().put("pev", pevAction);

		propertyTable.setParent(deploymentLabFrame)

		//DebugHelper.showInputAndActionMaps(propertyTable)

		//Pipe up the copy-paste actions.
		//for reasons I don't understand, this doesn't work if done inside the swingbuilder.
		copyItem.setActionCommand((String) TransferHandler.getCopyAction().getValue(Action.NAME));
		copyItem.addActionListener(actionListener);
		pasteItem.setActionCommand((String) TransferHandler.getPasteAction().getValue(Action.NAME));
		pasteItem.addActionListener(actionListener);
		//pasteProxyItem.setActionCommand( DeploymentPanelTransferHandler.getPasteAsProxyAction().getValue(Action.NAME) )
		//pasteProxyItem.addActionListener(actionListener)

		// Add an action to the scene graph to handle this correctly within our framework.
		deploymentPanel.getCanvas().getActionMap().put(DeploymentPanelTransferHandler.getPasteAsProxyAction().getValue(Action.NAME),
				DeploymentPanelTransferHandler.getPasteAsProxyAction())

		uiDisplay.addUIChangeListener(copyItem, 'clone')
		uiDisplay.addUIChangeListener(pasteItem, 'clone')
		// uiDisplay.addUIChangeListener(pasteProxyItem, 'clone')

		//scrub the copy & paste actions out of the JTree's action maps so those don't get in the way
		//configView.getActionMap().getParent().remove((String)TransferHandler.getCopyAction().getValue(Action.NAME))
		//configView.getActionMap().getParent().remove((String)TransferHandler.getPasteAction().getValue(Action.NAME))
		structureView.getActionMap().getParent().remove((String) TransferHandler.getCopyAction().getValue(Action.NAME))
		structureView.getActionMap().getParent().remove((String) TransferHandler.getPasteAction().getValue(Action.NAME))
		networkView.getActionMap().getParent().remove((String) TransferHandler.getCopyAction().getValue(Action.NAME))
		networkView.getActionMap().getParent().remove((String) TransferHandler.getPasteAction().getValue(Action.NAME))

		//set the default tools that to tool manager will return to with a call to activateDefaults
		toolManager.setDefaultTools(['hover', 'rectselect', 'panzoomer', 'selector', 'mover', 'keycommands', 'popupmenu', 'fasternamer', 'currentlocation'], builder.select, { paletteController.clearSelection() })
		//toolManager.activateDefaults()

		uiDisplay.addUIChangeListener(scriptsMenu, 'occulo')

		deploymentLabFrame.addWindowListener(this)
		deploymentLabFrame.pack()

		//maximize by default
		deploymentLabFrame.setExtendedState(deploymentLabFrame.getExtendedState() | Frame.MAXIMIZED_BOTH)

		if (showUI) {
			deploymentLabFrame.show()
		}

		popupMenu.setParent(deploymentLabFrame)

		structureView.addMouseListener(new StructureTreeMouseListener(structureView, deploymentPanel, deploymentLabFrame, popupMenu, selectModel))
		networkView.addMouseListener(new StructureTreeMouseListener(networkView, deploymentPanel, deploymentLabFrame, popupMenu, selectModel))

		File[] userComponentLibs = userPreferences.getComponentLibs()
		for (File file : userComponentLibs) {
			// Remove it from the saved prefs in case the load fails. It will be readded if successful.
			userPreferences.removeComponentLib(file.getName())
			fileLoadCLib(file)
		}
		//configView.expandRow(0)

		ChannelManager cm = ChannelManager.getInstance()

		cm.addChannelListener({ logData -> builder.mousePositionText.setText(logData.message) } as ChannelListener, 'mousePosition')
		cm.addChannelListener({ logData -> builder.zoomFactorText.setText(logData.message) } as ChannelListener, 'zoomFactor')
		cm.addChannelListener({ logData -> builder.userLevelText.setText(logData.message) } as ChannelListener, 'userLevel')
		cm.addChannelListener({ logData ->
			SwingUtilities.invokeLater(new Runnable() {
				void run() {
					String htmlMessage = "<html><b>$logData.message</b></html>"
					builder.statusbarText.setText(htmlMessage)
					switch (logData.level) {
						case LogLevel.INFO:
							builder.statusbarText.setForeground(Color.blue);
							builder.statusbarText.setIcon(ComponentIconFactory.getImageIcon('/resources/about_16.png'))
							def realColor = builder.statusbarText.getForeground();
							if (delorean) {
								delorean.cancel()
							}
							delorean = new java.util.Timer("DeLorean", true)
							builder.statusbarText.setForeground(Color.yellow);
							delorean.schedule({ builder.statusbarText.setForeground(realColor); } as TimerTask, 125)
							break
						case LogLevel.WARN:
						case LogLevel.ERROR:
						case LogLevel.FATAL:
							builder.statusbarText.setForeground(Color.red);
							builder.statusbarText.setIcon(ComponentIconFactory.getImageIcon('/resources/warning_16.png'))
							def realColor = builder.statusbarText.getForeground();
							if (delorean) {
								delorean.cancel()
							}
							delorean = new java.util.Timer("DeLorean", true)
							builder.statusbarText.setForeground(Color.yellow);
							delorean.schedule({ builder.statusbarText.setForeground(realColor); } as TimerTask, 125)
							break
						default:
							builder.statusbarText.setForeground(Color.black);
							builder.statusbarText.setIcon(null)
							break
					}

					if (tardis) {
						tardis.cancel()
					}
					tardis = new java.util.Timer("The Tardis", true)
					tardis.schedule({ builder.statusbarText.setText(""); builder.statusbarText.setIcon(null); } as TimerTask, 30000)

					builder.statusbarText.setVerticalTextPosition(JLabel.CENTER);
					builder.statusbarText.setHorizontalTextPosition(JLabel.TRAILING);
				}
			})
		} as ChannelListener, 'statusbar')

		/*
		deploymentLabFrame.addMouseListener( new MouseAdapter() {
			public void mouseClicked(MouseEvent me) {
				builder.statusbarText.setText("");
				builder.statusbarText.setIcon(null);
			}
		} )
		*/

		/*
		deploymentLabFrame.addMouseListener( {
			builder.statusbarText.setText("");
			builder.statusbarText.setIcon(null);
		} as MouseAdapter )
		*/

		//cm.addChannelListener({logData -> progressPane.setCaption(logData.message)} as ChannelListener, 'progress')
		cm.addChannelListener({ logData ->
			if (logData.message == null || logData.message.size() == 0) {
				progressPane.useDefaultMessages(true)
			} else {
				progressPane.useDefaultMessages(false)
			}
			// Invoke later in case this is being called from a background thread.
			SwingUtilities.invokeLater(new Runnable() {
				void run() {
					progressPane.setCaption(logData.message)
				}
			})
		} as ChannelListener, 'progress')

		//message box logger
		cm.addChannelListener({
			logData ->
				String message = logData.message

				int messageType
				String messageTitle
				switch (logData.level) {
					case LogLevel.INFO:
						messageType = JOptionPane.INFORMATION_MESSAGE
						messageTitle = uiStrings.getProperty("logger.message.info.title") //Information"
						break
					case LogLevel.WARN:
						messageType = JOptionPane.WARNING_MESSAGE
						messageTitle = uiStrings.getProperty("logger.message.warn.title") //"Warning"
						break
					case LogLevel.ERROR:
					case LogLevel.FATAL:
						messageType = JOptionPane.ERROR_MESSAGE
						messageTitle = uiStrings.getProperty("logger.message.error.title") //"Error"
						break
					default:
						messageType = JOptionPane.INFORMATION_MESSAGE
						messageTitle = uiStrings.getProperty("logger.message.info.title") //"Information"
						break
				}

				int len = 0
				message.split('\n').each { l ->
					if (l.length() > len) {
						len = l.length()
					}
				}

				SwingUtilities.invokeLater(new Runnable() {
					void run() {
						if (message.split('\n').size() > 13 || len > 100) {
							scrollingMessageText.text = logData.message
							scrollingMessageText.setCaretPosition(0)
							scrollingMessageDialog.setTitle(messageTitle)
							getScrollingMessageDialog().setLocationRelativeTo(builder.deploymentLabFrame)
							scrollingMessageOkButton.requestFocusInWindow()
							getScrollingMessageDialog().setVisible(true)
						} else {
							JOptionPane.showMessageDialog(builder.deploymentLabFrame, message, messageTitle, messageType)
						}
					}
				})
		} as ChannelListener,
				'message')

		cm.addChannelListener(new StyledMessageLogger(), StyledMessageLogger.LOGGERNAME)

		if (this.applianceHost != null) {
			def runLater = {
				loadFromAppliance()
			}
			SwingUtilities.invokeLater(runLater as Runnable)
		}

		if (fileToLoad != null) {
			doFileOpen(new File(fileToLoad))
		}

		// pre-init all dialogs
		getGeneralConfigDialog()
		getScrollingMessageDialog()
		getScrollingExportWarningDialog()
		undoBuffer.setParent(deploymentLabFrame)

	}

	/**
	 * Removes the given hotkey from being used anywhere in the application.
	 *
	 * @param key The hotkey to remove.
	 */
	public void clearExistingKeyBindings(KeyStroke key) {
		clearExistingKeyBindings(deploymentLabFrame, key)
	}

	/**
	 * Removes the given hotkey from the provided component and, by default, any of its children.
	 *
	 * @param parent Component to start the removal from.
	 * @param key The hotkey to remove.
	 * @param recursive (Optional) If true (the default), remove the hotkey from all descendants of the component.
	 */
	public void clearExistingKeyBindings(Component parent, KeyStroke key, boolean andDescendants = true) {

		def conditions = [(JComponent.WHEN_FOCUSED): 'WHEN_FOCUSED',
				(JComponent.WHEN_IN_FOCUSED_WINDOW): 'WHEN_IN_FOCUSED_WINDOW',
				(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT): 'WHEN_ANCESTOR_OF_FOCUSED_COMPONENT']

		if (parent instanceof JComponent) {
			//String parentStr = "-- ${parent.getClass().getName()}  " + ( parent.getName() ? "'${parent.getName()}'" : "" )
			for (int condition : conditions.keySet()) {
				InputMap inputMap = ((JComponent) parent).getInputMap(condition)
				KeyStroke[] keys = inputMap.allKeys();
				if ((keys != null) && (keys.length > 0)) {
					//String conditionStr = "       ${conditions[condition]}"
					for (KeyStroke k : keys) {
						if (k == key) {
							//if( parentStr ) { println parentStr; parentStr = null }
							//if( conditionStr ) { println conditionStr; conditionStr = null }
							//println "           '" + k.toString().replace("pressed ", "").replace("released ", "") + "'" + " --> ${inputMap.get( k )}"

							inputMap.put(k, "noop")
						}
					}
				}
			}
		}
		if (andDescendants && parent instanceof Container) {
			Component[] children = (parent instanceof JMenu) ? ((JMenu) parent).getMenuComponents() : ((Container) parent).getComponents();
			for (Component child : children) {
				clearExistingKeyBindings(child, key, andDescendants);
			}
		}
	}

	private void moveSelectionToDrawing() {
		ProgressManager.doTask {
			log.info(CentralCatalogue.getUIS("progress.drawing.moveDrawing"), "progress")
			try {
				undoBuffer.startOperation()
				def fb = new DrawingBuilder(componentModel, userPreferences, toolManager.getTool("proxyadder"), selectModel)
				NodeMover nm = new NodeMover(undoBuffer, deploymentPanel, selectModel)
				fb.moveSelectionToAnotherDrawing(selectModel.getExpandedSelection(), nm)
			} catch (Exception ex) {
				log.error(log.getStackTrace(ex))
				log.error(CentralCatalogue.getUIS("drawing.new.genericerror"), "message")
				undoBuffer.rollbackOperation()
			} finally {
				undoBuffer.finishOperation()
			}
		}

	}

	private void addDrawingFromSelection() {
		ProgressManager.doTask {
			log.info(CentralCatalogue.getUIS("progress.drawing.addfromSelection"), "progress")
			try {
				undoBuffer.startOperation()
				def fb = new DrawingBuilder(componentModel, userPreferences, toolManager.getTool("proxyadder"), selectModel)
				fb.makeDrawingFromSelection(selectModel.getExpandedSelection())
			} catch (Exception ex) {
				log.error(log.getStackTrace(ex))
				log.error(CentralCatalogue.getUIS("drawing.new.genericerror"), "message")
				undoBuffer.rollbackOperation()
			} finally {
				undoBuffer.finishOperation()
			}
		}
	}

	private boolean addDrawing(Floorplan fp) {
		try {
			undoBuffer.startOperation()
			def fb = new DrawingBuilder(componentModel, userPreferences, toolManager.getTool("proxyadder"), selectModel)
			return fb.addDrawing(fp)
		} catch (IOException e) {
			// Something went wrong with the background image.
			log.error( e.getMessage(), "message")
			undoBuffer.rollbackOperation()
		} catch (Exception ex) {
			log.error(CentralCatalogue.getUIS("drawing.new.genericerror"), "message")
			log.error(log.getStackTrace(ex))
			undoBuffer.rollbackOperation()
		} finally {
			undoBuffer.finishOperation()
		}

	}


	private void deleteDrawing() {
		if (selectModel.getActiveDrawing() == null) {
			log.error(CentralCatalogue.getUIS("drawing.delete.noActiveDrawing"), "statusbar")
			return
		}

		def result = JOptionPane.showConfirmDialog(deploymentLabFrame, String.format(CentralCatalogue.getUIS("drawing.delete.dialogtext"), selectModel.getActiveDrawing().getName()),
				CentralCatalogue.getUIS("drawing.delete.dialogtitle"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)
		if (result == JOptionPane.YES_OPTION) {
			try {
				undoBuffer.startOperation()
				componentModel.remove(selectModel.getActiveDrawing())

			} catch (Exception ex) {
				log.error("Error while deleting drawing!", "message")
				log.error(log.getStackTrace(ex))
				undoBuffer.rollbackOperation()
			} finally {
				undoBuffer.finishOperation()
			}

		}
	}

	/**
	 * import project
	 *
	 */
	private void importProject() {
		// choose project file
		importFileChooser.setSelectedFile(new File(""))
		importFileChooser.setCurrentDirectory(new File(userPreferences.getUserPreference(UserPreferences.IMPORT)))
		importFileChooser.setFileFilter(FileFilters.ZipProject.getFilter())
		importFileChooser.setDialogTitle(uiStrings.getProperty("fileImport.filechooser.title"))
		int rv = importFileChooser.showOpenDialog(builder.deploymentLabFrame)

		if (rv == JFileChooser.APPROVE_OPTION) {
			String selectedFilePath = importFileChooser.getSelectedFile().getPath()
			File f = new File(selectedFilePath)
			userPreferences.setUserPreference(UserPreferences.IMPORT, importFileChooser.getSelectedFile().getParent())
			if (f.exists()) {
				log.info("Importing Project $f")

				doBackgroundTask({
					log.info(CentralCatalogue.getUIS("progress.drawing.importing"), "progress")
					try {
						def ifc = new ImportProjectController( CentralCatalogue.getOpenProject(), selectModel, configurationReplacerTree)
						ifc.doImportProject(f)
						ifc.close()
					} catch (Exception e) {
						//clean up the undo buffer in case loading the file breaks
						//undoBuffer.clear()
						//undoBuffer.markSavePoint()
						//log.fatal("Fatal error opening file:\n" + e.getMessage(), 'message')
						log.fatal("Fatal error while importing project:\n" + e.getMessage(), 'default')
						log.fatal(log.getStackTrace(e))
						throw e
					}
				})

			} else {
				log.error(uiStrings.getProperty("importProject.noFile"), 'message')
			}
		}
	}


	private void addNetwork(String netType) {

		if (selectModel.getActiveDrawing() == null) {
			JOptionPane.showMessageDialog(builder.deploymentLabFrame, CentralCatalogue.getUIS("network.error.noActiveDrawing"), "Error!", JOptionPane.ERROR_MESSAGE)
			return
		}

		String prompt = uiStrings.getProperty("newnetwork." + netType + ".prompt")
		String dialogTitle = uiStrings.getProperty("newnetwork." + netType + ".dialogtitle")
		String componentName = uiStrings.getProperty("newnetwork." + netType + ".componentname")
		String entryError = uiStrings.getProperty("newnetwork." + netType + ".entryerror")

		while (true) {
			String networkName = JOptionPane.showInputDialog(builder.deploymentLabFrame, prompt, dialogTitle, JOptionPane.PLAIN_MESSAGE)
			if (networkName == null)
				break
			networkName = networkName.trim()
			if (networkName.size() > 0) {
				try {
					undoBuffer.startOperation()
					DLComponent network = componentModel.newComponent(componentName)
					network.setPropertyValue('name', networkName)
					selectModel.getActiveDrawing().addChild(network)
					// needed?  netComboBoxCtrl.getModel(selectModel.getActiveDrawing()).setSelectedItem(network)

					//generate a PAN ID for WSN Networks
					if (componentName.equalsIgnoreCase("network")) {
						network.setPropertyValue('pan_id', PanIDFactory.makePanID(componentModel))

						//also make new network keys
						def p = NetworkKey.makeNetworkKeys()
						network.setPropertyValue('networkPublicKey', p.a)
						network.setPropertyValue('networkPrivateKey', p.b)
					}

				} catch (Exception ex) {
					log.error("Error while creating Data Source!", "message")
					log.error(log.getStackTrace(ex))
					undoBuffer.rollbackOperation()
				} finally {
					undoBuffer.finishOperation()
				}
				break
			} else {
				JOptionPane.showMessageDialog(builder.deploymentLabFrame, entryError)
			}
		}
	}

	private void deleteNetwork() {
		networkKeyListener.doDelete(networkView)
	}

	private void addGroup(String groupType) {

		if (selectModel.getActiveDrawing() == null) {
			JOptionPane.showMessageDialog(builder.deploymentLabFrame, CentralCatalogue.getUIS("group.error.noActiveDrawing"), "Error!", JOptionPane.ERROR_MESSAGE)
			return
		}

		String prompt = uiStrings.getProperty("group." + groupType + ".prompt")
		String dialogTitle = uiStrings.getProperty("group." + groupType + ".dialogtitle")
		String componentName = uiStrings.getProperty("group." + groupType + ".componentname")
		String entryError = uiStrings.getProperty("group." + groupType + ".entryerror")

		while (true) {
			String groupName = JOptionPane.showInputDialog(builder.deploymentLabFrame, prompt, dialogTitle, JOptionPane.PLAIN_MESSAGE)
			if (groupName == null)
				break
			groupName = groupName.trim()
			if (groupName.size() > 0) {
				try {
					undoBuffer.startOperation()
					DLComponent group = componentModel.newComponent(componentName)
					group.setPropertyValue('name', groupName)
					selectModel.getActiveDrawing().addChild(group)
					zoneComboBoxCtrl.getModel(selectModel.getActiveDrawing()).setSelectedItem(group)
				} catch (Exception ex) {
					log.error("Error while creating Grouping!", "message")
					log.error(log.getStackTrace(ex))
					undoBuffer.rollbackOperation()
				} finally {
					undoBuffer.finishOperation()
				}
				break
			} else {
				JOptionPane.showMessageDialog(builder.deploymentLabFrame, entryError)
			}
		}
	}

	private void deleteZone() {
		structureKeyListener.doDelete(structureView)
	}

	private void changeGrouping() {
		def cpd = new ChangeParentsController(selectModel.getExpandedSelection(), ParentCategory.GROUPING, undoBuffer, selectModel)
		cpd.showDialog(deploymentLabFrame)
	}

	private void changeDataSource() {
		def cpd = new ChangeParentsController(selectModel.getExpandedSelection(), ParentCategory.NETWORK, undoBuffer, selectModel)
		cpd.showDialog(deploymentLabFrame)
	}

	private File stripExtension(File f) {
		String path = f.getPath()
		int len = path.length()
		if (len < 4)
			log.warn("File name too small")
		else if (path.substring(len - 4, len).matches("\\.[a-zA-Z]{3}"))
			path = path.substring(0, len - 4)
		else if (path.substring(len - 3, len).matches("\\.[a-zA-Z]{2}"))
			path = path.substring(0, len - 3)

		return new File(path)
	}

	private void loadFromAppliance() {
		URL url = new URL(this.applianceHost + "/fetch")
		HttpURLConnection conn = url.openConnection()
		conn.setRequestMethod("GET")
		conn.setDoInput(true)

		conn.connect()
		int respCode = conn.getResponseCode()

		if (respCode == 200) {
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()))
			StringBuffer xmlText = new StringBuffer()
			String inputLine
			while ((inputLine = br.readLine()) != null)
				xmlText.append(inputLine + '\n')
			br.close()
			def xml = new XmlSlurper().parseText(xmlText.toString())

			projectSettings.load(xml)
			setupGeneralOptions()
			initUI()

			componentModel.clear()
			objectModel.deserialize(xml.objects)
			componentModel.deserialize(xml.components, componentLibrary)

			openFile = null
			undoBuffer.clear()
			log.info("Project loaded from " + this.applianceHost + " with response code " + respCode + ".")
		} else {
			log.info("Project could not be read from " + this.applianceHost + " with response code " + respCode + ".")
			log.error("Could not read project file from appliance!", 'message')
		}
		conn.disconnect()
	}

	private void saveToAppliance() {
		StringWriter stringWriter = new StringWriter()

		try {

			def builder = new groovy.xml.MarkupBuilder(stringWriter)
			builder.model('version': '1.0') {
				projectSettings.save(builder)
				objectModel.serialize(builder)
				componentModel.serialize(builder)
			}

			undoBuffer.markSavePoint()
			saveSuccessful = true
		} catch (Exception e) {
			log.error("File could not be saved!\nTry saving to a new location using File->SaveAs.", 'message')
			saveSuccessful = false
		}

		URL url = new URL(this.applianceHost + "/push")
		HttpURLConnection conn = url.openConnection()
		conn.setRequestMethod("POST")
		conn.setDoOutput(true)

		OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream())
		osw.write("contents=" + URLEncoder.encode(stringWriter.toString()))
		osw.flush()
		osw.close()

		conn.connect()
		int respCode = conn.getResponseCode()
		log.info("Project saved to " + this.applianceHost + " with response code " + respCode + ".")
		conn.disconnect()
	}

	public int checkSaved() {
		// DO NOT RUN THIS METHOD ON THE EDT!
		assert !SwingUtilities.isEventDispatchThread(), "Logic Error: Tried to run checkSaved() from EDT"
		log.trace("checkSaved status: Undo? ${undoBuffer.hasUnsavedChanges()}  Settings? ${projectSettings.needSave()}  Project? ${CentralCatalogue.getOpenProject()?.needsASave()}")
		if (undoBuffer.hasUnsavedChanges() || projectSettings.needSave() || CentralCatalogue.getOpenProject()?.needsASave()) {
			int choice = JOptionPane.showConfirmDialog(builder.deploymentLabFrame, "Your project has unsaved changes.\nSave it?", "Save project?", JOptionPane.YES_NO_CANCEL_OPTION)
			if (choice == JOptionPane.YES_OPTION) {
				if (uiDisplay.getProperty("isSZAdvancedEditorMode")) {
					fileSaveToSmartZone()
				} else {
					fileSave()
				}

				waitForBackgroundTask()
				if (!saveSuccessful)
					return JOptionPane.CANCEL_OPTION
			}
			return choice
		}
		return JOptionPane.YES_OPTION
	}

	private void createNewProject(Floorplan fp) {
		Thread newThread = new Thread({
			int n = checkSaved()
			if (n == JOptionPane.CANCEL_OPTION || n == JOptionPane.CLOSED_OPTION)
				return

			// Close the currently opened file, if there is one.
			if( CentralCatalogue.getOpenProject() != null ) {
				doFileClose()
			}
			if (fp != null) {
				uiDisplay.setProperty("isSZAdvancedEditorMode", true)
				uiDisplay.setProperty("isNotSZAdvancedEditorMode", false)
				isSyncWithSmartZone = true
			}
			doBackgroundTask({
				DLProject dlp = new DLProject(undoBuffer, componentModel, objectModel, configurationReplacer, true)
				dlp.setNeedsASave(false)

				progressPane.setCaption(CentralCatalogue.getUIS("progress.initializing"))

				builder.deploymentLabFrame.title = appProperties.getProperty('application.shortname') + " - New Project"

				projectSettings.create()
				setupGeneralOptions()

				// Be nice and automatically ask for the first drawing.
				if(!addDrawing(fp)) {
					doFileClose()
					return
				}

				initUI()
				userPreferences.setUserPreference(UserPreferences.FILENAME, "New_project.dlz")

				toolManager.activateDefaults()

				undoBuffer.clear()
				undoBuffer.resetSavePoint()
			})
		} as Runnable)
		log.debug("Calling thread.start in fileNew")
		newThread.start()
	}

	public void mergeFloorplan() {
		log.debug("Synchronize file initiated.")
		progressPane.setCaption("Getting Floorplan")

		// Get location id out of project
		for (DLComponent drawing : componentModel.getComponentsByType(ComponentType.DRAWING)) {
			if (drawing.isSmartZoneLinked()) {
				locationFound = false
				int locationId = drawing.getSmartZoneId()
				String locationName = drawing.getPropertyValue('name')

				//Check to continue with pull only if location is still instrumented.
				if (!canPullFromSmartZone(drawing)) {
					return
				}

				// TODO: REFACTOR! This controller interface scaffolding angers me. Should make one call and
				// get a floorplan. Nor should I need to pass around the frame and dialogs.

				// Retrieve the floorplan from the server and clear whatever was loaded and replace with server data.
				def getFloorplanController = new GetFloorplanController(deploymentLabFrame,
						smartZoneLoginDialog, smartZoneServerDialog, locationId);
				int port;
				if (projectSettings.smartZonePort != null) {
					port = Integer.parseInt(projectSettings.smartZonePort);
				}
				getFloorplanController.setURL(projectSettings.smartZoneHost, port);
				// do the network call
				if (getFloorplanController.start()) {

					// as long as the data still exists on the server
					if (!getFloorplanController.getNotFound()) {
						locationFound = true
						// if the user had to change the to a different save it to the project
						if (getFloorplanController.getURLChangedByUser()) {
							projectSettings.smartZoneHost = getFloorplanController.getURLHost();
							projectSettings.smartZonePort = getFloorplanController.getURLPort();
						}

						Floorplan fp = getFloorplanController.getFloorplan();
						if (fp != null) {
							try {
								undoBuffer.startOperation()
								mergeController = new MergeLogicController(drawing, fp, componentModel, objectModel, undoBuffer, deploymentLabFrame)
								mergeController.setSyncWithServerListener(new SyncWithServerListener() {
									@Override
									void synced() {
										isSyncWithSmartZone = true
									}
								})
								mergeController.runMergeLogic()
								mergeController.showDialog()
							} catch(Exception ex) {
								log.error(log.getStackTrace(ex))
								log.error(CentralCatalogue.getUIS("drawing.new.genericerror"), "message")
								undoBuffer.rollbackOperation()
								undoBuffer.finishOperation()
							}
						}
					} else {
						showError(LOC_NOT_FOUND, SYNC_ABORTED)
					}
				//	saveSuccessful = CentralCatalogue.getOpenProject().saveAs(currentFileName);
				} else {
					log.debug("user cancelled synchronization on load while collecting connection parameters.")
				}
			}
		}
	}

	private void showError(String errorMsg, String statusMsg) {
		hideProgressPane()
		log.error(errorMsg, "message")
		log.error(statusMsg, "statusbar");
	}

	public void fileNew() {
		createNewProject(null)
	}

	// TODO: SHOULD BE PRIVATE! Use doBackgroundTask() or ProgressManager, not the progress pane directly.
	public void showProgressPane() {
		BufferedImage bg = new BufferedImage(deploymentLabFrame.getWidth(), deploymentLabFrame.getHeight(), BufferedImage.TYPE_INT_RGB)
		deploymentLabFrame.getRootPane().paint(bg.getGraphics())
		progressPane.setBG(bg)
		progressPane.setVisible(true)
		progressPaneTimer.start()
	}

	public ProgressPane getProgressPane() {
		return progressPane
	}

	public void doBackgroundTask(def task) {
		synchronized (backgroundTaskLock) {
			if (backgroundTask != null && backgroundTask.isAlive()) {
				throw new Exception("Can't start another background task when one is already running!")
			}
			showProgressPane() // run on EDT

			def taskMonitor = {
				Thread taskThread = new Thread(task as Runnable, "DL Inner Background Task")
				taskThread.start()
				taskThread.join()
				hideProgressPane()
			}
			backgroundTask = new Thread(taskMonitor as Runnable, "DL Outer Background Task")
			backgroundTask.start()
		}
	}

	public void waitForBackgroundTask() {
		log.debug("Waiting for background task to complete...")
		Thread task = null
		synchronized (backgroundTaskLock) {
			if (backgroundTask != null && backgroundTask.isAlive())
				task = backgroundTask
		}
		if (task != null)
			task.join()
	}

	public boolean backgroundTaskRunning() {
		boolean running = false
		synchronized (backgroundTaskLock) {
			if (backgroundTask != null && backgroundTask.isAlive())
				running = true
		}
		return running
	}

	public void hideProgressPane() {
		progressPaneTimer.stop()
		progressPane.setVisible(false)
		progressPane.flush()

		/* Don't do this now that we are using DLImages, which use Soft References.
		//and now!
		//compute delay for the worst case; 1 sec per possible heap gb
		def rt = Runtime.getRuntime()
		int delay = (rt.maxMemory() / 1024) / 1024
		//println delay
		def shrike  = new java.util.Timer("Skrike", true)
		//the shrike will trip off and double-tap the GC after all soft references have expired
		shrike.schedule( {
			System.gc()
			System.gc()
		} as TimerTask , delay )
		*/
	}

	public void fileOpen() {
		//leave the openChooser pointing at the last directory we opened a file from, but don't pre-slug a filename
		openFileChooser.setSelectedFile(new File(""))
		openFileChooser.setCurrentDirectory(new File(userPreferences.getUserPreference(UserPreferences.FILE)))

		openFileChooser.addChoosableFileFilter(FileFilters.DLProject.getFilter())
		openFileChooser.setFileFilter(FileFilters.ZipProject.getFilter())
		openFileChooser.setDialogTitle(uiStrings.getProperty("fileOpen.filechooser.title"))
		int rv = openFileChooser.showOpenDialog(builder.deploymentLabFrame)

		if (rv == JFileChooser.APPROVE_OPTION) {
			String selectedFilePath = openFileChooser.getSelectedFile().getPath()
			File f = new File(selectedFilePath)
			userPreferences.setUserPreference(UserPreferences.FILE, openFileChooser.getSelectedFile().getParent())
			if (f.exists()) {
				log.info("Loading file $f")
				doFileOpen(f)
			} else {
				log.error(uiStrings.getProperty("fileOpen.noFile"), 'message')
			}
		}
	}

	public void fileOpenFromServer() {
		Properties connectProperties = new Properties()
		if(loginDialogWithHost.getLoginInfo(connectProperties)){
			doBackgroundTask({
				unStashDialog.fetch(connectProperties)
			})
		}
	}

	private static String nodeName(DefaultMutableTreeNode node) {
		if (node.getUserObject() instanceof Location) {
			Location loc = (Location)node.getUserObject();
			return loc.name
		} else {
			return "root";
		}
	}

	/** Returns the Swing tree node corresponding to the given location or null if the location does not have any FLOOR descendents. */
	static DefaultMutableTreeNode mapLocationToSwingTreeWithPruning(Location location) {
		def node = new DefaultMutableTreeNode(location)
		for (def childLocation : location.getChildren()) {
			def childNode = mapLocationToSwingTreeWithPruning(childLocation)
			if (childNode != null) {
				node.add(childNode)
			}
		}
		return (node.childCount > 0 || location.locationLevel == LocationLevel.FLOOR) ? node : null
	}

	private boolean processLocationTree(DefaultMutableTreeNode rootTreeNode,locations) {

		if (locations == null || locations.size() == 0) {
			return false;
		} else {
			boolean foundFloor = false;
			for (Location location : locations) {
				def node = mapLocationToSwingTreeWithPruning(location)
				if (node != null) {
					rootTreeNode.add(node);
					foundFloor = true;
				}
			}
			return foundFloor;
		}
	}

	private void openFromSmartZone() {
		doBackgroundTask({
			log.info("Getting Location Tree", "progress")
		def openLocationController = new OpenLocationController(deploymentLabFrame,
				smartZoneLoginDialog,smartZoneServerDialog);
		if (openLocationController.start()) {
			List<Location> locations = openLocationController.getLocations();

			DefaultMutableTreeNode defaultMutableTreeNode = new DefaultMutableTreeNode("Locations");
			if (processLocationTree(defaultMutableTreeNode, locations)) {
				LocationJTree locationJTree = new LocationJTree(defaultMutableTreeNode);
				LocationTreeDialog locationTreeDialog = new LocationTreeDialog(locationJTree, deploymentLabFrame);
				locationJTree.registerImportButton(locationTreeDialog.getImportButton());
				locationTreeDialog.getImportButton().addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent ae) {
						Location location = locationTreeDialog.getSelectedLocation();
						def getFloorplanController = new GetFloorplanController(deploymentLabFrame,
								smartZoneLoginDialog, smartZoneServerDialog, location.getId());
						log.info("Getting Floorplan", "progress")
						if (getFloorplanController.start()) {
							if (!getFloorplanController.getNotFound()) {
								Floorplan fp = getFloorplanController.getFloorplan();
								if (fp != null) {
									createNewProject(fp);
								}
							} else {
								log.error("The selected Location was not found.", "message");
							}
						} else {
							log.debug("User cancelled request after a failed authentication or connection.")
						}
					}
				});
				locationJTree.expandRow(0);
				locationJTree.setRootVisible(false);

				// Have the ability to unlink if we are a developer. Yes, as of Story 35810 you can do it from SmartZone,
				// but why login to there just to do this during MapSense dev? Deeply nested quick hack of code follows...
				if( UIDisplay.INSTANCE.getUserLevelId() >= 5 ) {
					MouseListener ml = new MouseAdapter() {
						public void mousePressed(MouseEvent e) {
							// Secret popup when right-click + ALT.
							if( e.getButton() == MouseEvent.BUTTON3 && e.isAltDown() ) {
								TreePath selPath = locationJTree.getPathForLocation( e.getX(), e.getY() )
								DefaultMutableTreeNode node = selPath.getLastPathComponent()
								if( node != null ) {
									Location loc = (Location) node.getUserObject()
									if( loc.getLocationLevel() == LocationLevel.FLOOR ) {
										JPopupMenu menu = new JPopupMenu()
										JMenuItem inst = new JMenuItem("SmartZone Instrument")
										inst.addActionListener{ ev ->
											InstrumentFloorplanController ifc = new InstrumentFloorplanController( deploymentLabFrame,
											                                                                       smartZoneLoginDialog,
											                                                                       smartZoneServerDialog,
											                                                                       loc.getId(),
																												   currentFileName.getAbsolutePath())
											if( ifc.start() ) {
												if( !ifc.wasSuccessful() ) {
													log.error("Location already linked in SmartZone.", "message")
												}
												// Should really have it re-query, but this whole thing needs refactoring to do that.
												node.setUserObject( loc.withWsnInstrumented( true ) )
												locationJTree.repaint()
											} else {
												log.error("Error trying to link project file with SmartZone server", "message")
											}
										}
										JMenuItem uninst = new JMenuItem("SmartZone Uninstrument")
										uninst.addActionListener{ ev ->
											UninstrumentFloorplanController ufc = new UninstrumentFloorplanController( deploymentLabFrame,
											                                                                           smartZoneLoginDialog,
											                                                                           smartZoneServerDialog,
											                                                                           loc.getId() )
											if( ufc.start() ) {
												if( !ufc.wasSuccessful() ) {
													log.error("Location already unlinked in SmartZone.", "message")
												}
												// Should really have it re-query, but this whole thing needs refactoring to do that.
												node.setUserObject( loc.withWsnInstrumented( false ) )
												locationJTree.repaint()
											} else {
												log.error("Error trying to unlink project file with SmartZone server", "message")
											}
										}
										menu.add( uninst )
										menu.add( inst )
										menu.show( e.getComponent(), e.getX(), e.getY() )
									}
								}
							}
						}
					};
					locationJTree.addMouseListener( ml )
				}

				//This is to show the dialog later so that the action listener on the import button is not part of
				//this background task.
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					void run() {
						locationTreeDialog.openLocationDialog();
					}
				})
			} else {
				log.error(CentralCatalogue.getUIS("locations.no_importable"), "message")
			}
		}
	})
	}

	public void fileOpenFromSmartZone() {
		openFromSmartZone();
	}

	private void doFileOpen(File f){

		Thread openThread = new Thread({
			int n = checkSaved()
			if (n == JOptionPane.CANCEL_OPTION || n == JOptionPane.CLOSED_OPTION)
				return

			doBackgroundTask({
				progressPane.setCaption("Loading file...")
				try {
					doLoad(f)
				} catch (Exception e) {
					//clean up the undo buffer in case loading the file breaks
					undoBuffer.clear()
					//log.fatal("Fatal error opening file:\n" + e.getMessage(), 'message')
					log.fatal("Fatal error opening file:\n" + e.getMessage(), 'default')
					log.fatal(log.getStackTrace(e))
					throw e

				}
			})
		} as Runnable, "DL File Open Thread")
		openThread.start()
	}

	private void updateFileMenu() {
		def fileMenuItems = fileMenu.menuComponents
		def recentFileMenuItems = fileMenuItems.findAll { it.getName() != null && it.getName().indexOf('recentFile') > -1 }
		recentFileMenuItems.each { m -> fileMenu.remove(m) }

		if (userPreferences.getRecentFileCount() > 0) {
			fileMenu.add(builder.separator(name: 'recentFileSep'), fileMenu.itemCount - 2)
			def recentFiles = userPreferences.getRecentFiles()
			int i = 1
			recentFiles.each { f ->
				def item = builder.menuItem(text: i + ': ' + f, name: 'recentFile' + i, mnemonic: i.toString().toCharacter(),
						actionPerformed: {
							def theFile = new File(f)
							userPreferences.setUserPreference(UserPreferences.FILE, theFile.getParent())
							doFileOpen(theFile)
							//doFileOpen(new File(f))
						}
				);
				i++;
				fileMenu.insert(item, fileMenu.itemCount - 2)
			}
			def clearItem = builder.menuItem(text: "Clear Recent Files List", name: 'recentFile' + i, mnemonic: KeyEvent.VK_F, actionPerformed: { userPreferences.clearRecentFiles() });
			fileMenu.insert(clearItem, fileMenu.itemCount - 2)
		}
	}

	public void fileSave() {
		if (openFile == null) {
			fileSaveAs()
		} else {
			doBackgroundTask({
				//progressPane.setCaption("Saving file...")
				fileSaveLocal()
			})
		}
	}

	public void fileSaveLocal() {
		log.info("Saving file...", "progress")
		saveSuccessful = CentralCatalogue.getOpenProject().save()
		if (saveSuccessful) {
			userPreferences.setRecentFile( CentralCatalogue.getOpenProject().getFile().getAbsolutePath() )
		}
	}

	public void fileSaveToSmartZone() {
		getSZConnectionProperties()

		doBackgroundTask({
			saveToSmartZone()
		})
	}

	private void getSZConnectionProperties() {
		// see if this is a linked file
		if (uiDisplay.getProperty("isSZAdvancedEditorMode") ) {

			// connection properties would be blank if never saved to disk
			if ("".equals(projectSettings.smartZoneHost)) {
				String host = CentralCatalogue.getSystemSettings().getSmartZoneHost()
				String port = CentralCatalogue.getSystemSettings().getSmartZonePort()
				projectSettings.smartZoneHost = host
				projectSettings.smartZonePort = port
			}
		}
	}

	//On Sync with Server this is called directly as it is not in a separate background task.
	public void saveToSmartZone() {
		saveSuccessful = false
		log.info("Saving to SmartZone Server", "progress")
		List<DLComponent> drawings = componentModel.getComponentsByType(ComponentType.DRAWING)
		for (DLComponent eachDrawing : drawings) {
			if ( canSaveToSmartZone( eachDrawing ) ) {
				Floorplan fp = SmartZoneResponseAssembler.buildFloorplan(componentModel, eachDrawing, currentFileName)
				def saveFloorplanController = new SaveLocationController(deploymentLabFrame, smartZoneLoginDialog,
				                                                         smartZoneServerDialog, fp, eachDrawing)
				saveFloorplanController.setURL(projectSettings.smartZoneHost, Integer.parseInt(projectSettings.smartZonePort))
				if (saveFloorplanController.start()) {
					if (saveFloorplanController.getNotFound()) {
						showError(LOC_NOT_FOUND, SAVE_ABORTED)
					} else {
						saveSuccessful = true
						log.debug("Save to SmartZone Server successful.")
						// if the user had to change the to a different save it to the project
						if (saveFloorplanController.getURLChangedByUser()) {
							log.debug("URL changed by user during Save to SmartZone Server.")
							projectSettings.smartZoneHost = saveFloorplanController.getURLHost();
							projectSettings.smartZonePort = saveFloorplanController.getURLPort();
						//	CentralCatalogue.getOpenProject().saveAs(currentFileName);
						}
					}
				} else {
					log.debug("User cancelled Save to SmartZone Server operation.")
				}
			}
		}
	}

	private boolean canSaveToSmartZone( DLComponent drawing ) {
		// Make sure it didn't get marked as instrumented while we were editing.
		boolean canSave = false
		log.debug("canSaveToSmartZone: isLinked? ${drawing.isSmartZoneLinked()}  isAdvancedEditor? ${uiDisplay.getProperty('isSZAdvancedEditorMode')}")
		if( drawing.isSmartZoneLinked() ) {
			// TODO: REFACTOR! This controller interface scaffolding angers me. I should make one call and get a floorplan.
			OpenLocationController api = new OpenLocationController( deploymentLabFrame, smartZoneLoginDialog, smartZoneServerDialog )
			if( api.start() ) {
				Location loc = api.getLocation( drawing.getSmartZoneId() )
				log.debug("canSaveToSmartZone: isInstrumented? ${loc?.isWsnInstrumented()}")
				if( loc != null ) {
					if( uiDisplay.getProperty("isSZAdvancedEditorMode") ) {
						canSave = !loc.isWsnInstrumented()
						if( !canSave ) {
							log.error("This location is instrumented in SmartZone. Another instance \n"
									+ "may have created an instrumented project after this was opened.",
									"message")
						}
					} else {
						if (loc.isWsnInstrumented()) {
							canSave = true
						} else {
							showError(LOC_UNLINKED, SAVE_ABORTED)
						}
					}
				} else {
					//log.error("Location not found on server.", "message")
					showError(LOC_NOT_FOUND, SAVE_ABORTED)
				}
			} else {
				log.error("Unable to determine if the location is already marked as instrumented.", "message")
			}
		} else {
			// Shouldn't happen since this check happens only for a smartZone linked floorplan (either in Advanced editor mode or instrumented mode).
			log.error("This is not a SmartZone project.", "message")
		}
		log.debug("canSaveToSmartZone? $canSave")

		return canSave
	}

	private boolean canPullFromSmartZone ( DLComponent drawing ) {
		boolean canPull = false
		if( drawing.isSmartZoneLinked() ) {
			OpenLocationController api = new OpenLocationController( deploymentLabFrame, smartZoneLoginDialog, smartZoneServerDialog )
			if( api.start() ) {
				Location loc = api.getLocation( drawing.getSmartZoneId() )
					if( loc != null ) {
							if (loc.isWsnInstrumented()) {
											canPull = true
									} else {
										showError(LOC_UNLINKED, SYNC_ABORTED)
										}

									} else {
								showError(LOC_NOT_FOUND, SYNC_ABORTED)
								}
						} else {
						log.error("Unable to determine if the location is already marked as instrumented.", "message")
						}
			} else {
					log.error("This is not a SmartZone project.", "message")
			}
		return canPull
	}

	private boolean saveSuccessful // kludgy flag, improve design

	private void fileClose() {
		Thread closeThread = new Thread({
			int n = checkSaved()
			if (n == JOptionPane.CANCEL_OPTION || n == JOptionPane.CLOSED_OPTION) {
				return;
			}

			doBackgroundTask({
				this.doFileClose()
			})

		} as Runnable, "DL File Close Thread")
		closeThread.start()
	}

	private void doFileClose() {
		def watch = new DLStopwatch()
		try {
			progressPane.setCaption("Closing file...")
			log.info("Closing file $currentFileName", "default")
			undoBuffer.stopTracking()
			componentModel.clear();
			CentralCatalogue.getOpenProject().close()
			openFile = null
			currentFileName = null

			builder.deploymentLabFrame.title = appProperties.getProperty('application.shortname')
			uiDisplay.setProperty('isFileLoaded', false)

			toolManager.deactivateAllTools()
			uiDisplay.setProperty("isSmartZoneLinked", false)
			uiDisplay.setProperty("isSZAdvancedEditorMode", false)
			uiDisplay.setProperty("isNotSZAdvancedEditorMode", true)
			isSyncWithSmartZone = false
		} finally {
			undoBuffer.resumeTracking()
		}
		log.info(watch.finishMessage("file closing"))
	}

	private void doLoad(File file) {
		if (file == currentFileName) {
			log.error(CentralCatalogue.getUIS("fileOpen.alreadyOpen"), "message")
			return
		}

		if( CentralCatalogue.getOpenProject() ) {
			doFileClose()
		}

		try {
			// This will internally set CentralCatalogue.openProject
			new DLProject( file, undoBuffer, configurationReplacer, componentModel, objectModel )

			setupGeneralOptions()

		} catch (FileNotFoundException e) {
			log.error(e.getMessage(), 'message')
			return
		} catch (Exception e) {
			def message = "The file '${file.getPath()}' could not be opened:\n${e.getMessage()}"
			JOptionPane.showMessageDialog(builder.deploymentLabFrame, message, "Error!", JOptionPane.ERROR_MESSAGE)
			log.error(log.getStackTrace(e))
			return
		}

		// ## temp while migrating.
		currentFileName = file
		if( CentralCatalogue.getOpenProject().needsASave() ) {
			openFile = null
		} else {
			openFile = file
		}
		String version = CentralCatalogue.getOpenProject().fileVersion();

		if (version.equals("1.0") || version.equals("1.1")) {
			undoBuffer.resetSavePoint()
		} else {
			undoBuffer.markSavePoint()
		}
		userPreferences.setRecentFile(file.getAbsolutePath())
		userPreferences.setUserPreference(UserPreferences.FILENAME, currentFileName.getName())

		//return to default tools
		toolManager.activateDefaults()

		try {
			//having been (presumably) successful loading the file, copy the file to a .bak
			FileUtil.backupFile(file, CentralCatalogue.getApp("backup.load"));
		} catch (IOException ioe) {
			log.error(CentralCatalogue.getUIS("backup.io"), "message")
		}

		builder.deploymentLabFrame.title = appProperties.getProperty('application.shortname') + " - " + file.getPath()
		initUI()

		// Initialize the active drawing. (MUST be after initUI!)
		DLComponent activeDrawing = null
		if (!projectSettings.activeDrawing.isEmpty()) {
			DLObject obj = objectModel.getObject(objectModel.getDlid(projectSettings.activeDrawing))
			if (obj) {
				activeDrawing = componentModel.getOwner(obj)
			} else {
				log.warn("Bad ActiveDrawing in project file: '${projectSettings.activeDrawing}'")
			}
		}
		if (activeDrawing == null) {
			// Just grab the last one.
			List<DLComponent> drawings = componentModel.getComponentsByType('drawing')
			if (drawings.size() != 0) {
				activeDrawing = drawings.get(drawings.size() - 1)
			}
		}
		selectModel.setActiveDrawing(activeDrawing)

		if (Double.parseDouble(version) < Double.parseDouble(appProperties.getProperty('model.version'))) {
			log.info("Project file is from an older version of " + appProperties.getProperty('application.shortname') + ".\nPlease save the file with a new name.", 'message')
		}

		def oldComps = componentModel.getDeprecatedComponents()
		if (oldComps.size() > 0) {
			//issue a deprecation report
			String message = uiStrings.getProperty("deprecated.onLoad")
			log.warn(message, "message")

			StringBuilder sb = new StringBuilder("Deprecated Components:")
			oldComps.each{ c->
				sb.append("\n    ")
				sb.append( c )
			}
			log.warn( sb.toString() )
		}

		syncWithSZServer()

		//just in case, right?
		//xml = null
	}

	private void syncWithSZServer() {
		// Offer to sync file if it was linked
		if (projectSettings.smartZoneHost != null && !"".equals(projectSettings.smartZoneHost)) {
			uiDisplay.setProperty("isSmartZoneLinked", true)

			int res = JOptionPane.showConfirmDialog(deploymentLabFrame, "Synchronize file with server?", 'Synchronize with Server', JOptionPane.YES_NO_OPTION)
			if (res == JOptionPane.YES_OPTION) {
				syncWithServerCancelled = false
				mergeFloorplan()
			} else {
				syncWithServerCancelled = true
				log.debug("Synchronize file cancelled on open.")
			}
		}
	}

	public void fileSaveAs() {
		getSZConnectionProperties()

		if (currentFileName != null) {
			saveFileChooser.setSelectedFile(currentFileName)
		} else {
			String fileName = userPreferences.getUserPreference(UserPreferences.FILENAME);
			if (uiDisplay.getProperty("isSZAdvancedEditorMode")) {
				fileName = selectModel.getActiveDrawing().name;
			}
			if (!fileName.endsWith(".dlz")) {
				fileName += ".dlz"
			}
			saveFileChooser.setSelectedFile(new File(fileName))
		}
		saveFileChooser.setCurrentDirectory(new File(userPreferences.getUserPreference(UserPreferences.FILE)))
		saveFileChooser.resetChoosableFileFilters()
		saveFileChooser.setFileFilter(FileFilters.ZipProject.getFilter())

		saveFileChooser.setDialogTitle(uiStrings.getProperty("fileSaveAs.filechooser.title"))
		int rv = saveFileChooser.showSaveDialog(builder.deploymentLabFrame)
		if (rv == JFileChooser.APPROVE_OPTION) {
			File selectedFile = saveFileChooser.getSelectedFile()
			//println selectedFile.getAbsolutePath()
			String ext = FilenameUtils.getExtension(selectedFile.getName())
			if (!ext.equalsIgnoreCase(saveFileChooser.getFileFilter().getExtensions().first())) {
				//only replace if the extension is a known extension
				def knownExtensions = []
				for (FileFilters ff : FileFilters.values()) {
					knownExtensions.addAll(ff.getFilter().getExtensions())
				}
				if (knownExtensions.contains(ext)) {
					selectedFile = new File(FilenameUtils.removeExtension(selectedFile.getAbsolutePath()) + "." + saveFileChooser.getFileFilter().getExtensions().first().toString())
				} else {
					selectedFile = new File(selectedFile.getAbsolutePath() + "." + saveFileChooser.getFileFilter().getExtensions().first().toString())
				}
			}
			String selectedPath = selectedFile.getPath()

			userPreferences.setUserPreference(UserPreferences.FILE, selectedFile.getParent())

			if (selectedFile.exists()) {
				int res = JOptionPane.showConfirmDialog(builder.deploymentLabFrame, "Are you sure you wish to overwrite '" + selectedFile.getPath() + "'?", 'Confirm Overwrite File', JOptionPane.OK_CANCEL_OPTION)
				if (res != JOptionPane.OK_OPTION) {
					log.info("SaveAs canceled.")
					return
				}
			}

			doBackgroundTask({
				progressPane.setCaption("Saving file...")
				openFile = selectedFile
				currentFileName = selectedFile

				// If needed, set the floor as linked in SZ.
				for( DLComponent c : componentModel.getComponentsByType( ComponentType.DRAWING ) ) {
					if( c.isSmartZoneLinked() ) {
						InstrumentFloorplanController ifc = new InstrumentFloorplanController( deploymentLabFrame,
						                                                                       smartZoneLoginDialog,
						                                                                       smartZoneServerDialog,
						                                                                       c.getSmartZoneId(),
						                                                                       currentFileName.getAbsolutePath())
						if( ifc.start() ) {
							saveSuccessful = ifc.wasSuccessful()
							if( !saveSuccessful ) {
								log.error("Location already linked in SmartZone. Aborting Save As.", "message")
								// TODO: If we have multiple flooplans in a file, this could leave some linked.
								return
							}
							uiDisplay.setProperty("isSmartZoneLinked", true)
						} else {
							log.error("Error trying to link project file with SmartZone server. Aborting Save As.", "message")
							saveSuccessful = false
							return
						}
					}
				}

				saveSuccessful = CentralCatalogue.getOpenProject().saveAs(selectedFile)
				if (saveSuccessful) {
					uiDisplay.setProperty("isSZAdvancedEditorMode", false)
					uiDisplay.setProperty("isNotSZAdvancedEditorMode", true)
					userPreferences.setRecentFile(CentralCatalogue.getOpenProject().getFile().getAbsolutePath())
					userPreferences.setUserPreference(UserPreferences.FILENAME, currentFileName.getName())

					builder.deploymentLabFrame.title = appProperties.getProperty('application.shortname') + " - " + selectedPath
					log.info("File saved to '" + selectedPath + "'.", 'statusbar')
				}
			})
		}
	}

	public void saveAndOpenFile(String drawingName, BinaryData blob){
		saveFileChooser.setFileFilter(FileFilters.ZipProject.getFilter())
		saveFileChooser.setDialogTitle(CentralCatalogue.getUIS("fileSaveAs.filechooser.title"))
		saveFileChooser.setSelectedFile(new File(drawingName+"_project.dlz"))
		saveFileChooser.setCurrentDirectory(new File(userPreferences.getUserPreference(UserPreferences.FILE)))

		int rv = saveFileChooser.showSaveDialog(builder.deploymentLabFrame)
		if (rv == JFileChooser.APPROVE_OPTION) {
			String selectedPath = saveFileChooser.getSelectedFile().getPath()

			if (!selectedPath.endsWith(".dlz")) {
				selectedPath = selectedPath + ".dlz"
			}

			File selectedFile = new File(selectedPath)
			userPreferences.setUserPreference(UserPreferences.FILE, selectedFile.getParent())
			if(selectedFile.exists()) {
				int res = JOptionPane.showConfirmDialog(builder.deploymentLabFrame, "Are you sure you wish to overwrite '" + selectedFile.getPath() + "'?", 'Confirm Overwrite File', JOptionPane.OK_CANCEL_OPTION)
				if(res != JOptionPane.OK_OPTION) {
					log.info("SaveAs canceled.")
					return
				}
			}

			selectedFile.setBytes(blob.getValue())
			userPreferences.setRecentFile(selectedFile.getAbsolutePath())
			log.info("File saved to '" + selectedPath + "'.", 'statusbar')

			doFileOpen(selectedFile)
		}
	}

	// open custom component library
	public void openClib() {
		clibFileChooser.resetChoosableFileFilters()
		clibFileChooser.addChoosableFileFilter(FileFilters.ZipLibrary.getFilter())
		clibFileChooser.setFileFilter(FileFilters.ComponentLibrary.getFilter())

		clibFileChooser.setSelectedFile(new File(""))
		clibFileChooser.setCurrentDirectory(new File(userPreferences.getUserPreference(UserPreferences.CLIB)))

		clibFileChooser.setDialogTitle(uiStrings.getProperty("openClib.filechooser.title"))
		int rv = clibFileChooser.showOpenDialog(builder.deploymentLabFrame)
		if (rv == JFileChooser.APPROVE_OPTION) {
			String selectedFilePath = clibFileChooser.getSelectedFile().getPath()

			File file = new File(selectedFilePath)

			userPreferences.setUserPreference(UserPreferences.CLIB, file.getParent())

			//File file = fileChooser.getSelectedFile()
			if (file.exists()) {
				log.info("component library file name:" + file.getName())
				if (userPreferences.isLoadedComponentLib(file)) {
					int res = JOptionPane.showConfirmDialog(builder.deploymentLabFrame, "This Component Library is already loaded. Do you wish to load it again?", 'Confirm Overload Component Library', JOptionPane.OK_CANCEL_OPTION)
					if (res == JOptionPane.OK_OPTION) {
						fileUnloadCLib(file.getName())
						fileLoadCLib(file)
						log.info("Component Library Reloaded")

					} else {
						log.info("Load Component Library canceled.")
					}
				} else {
					fileLoadCLib(file)
					log.info("Component Library Loaded")
				}
			} else {
				log.error("Selected File doesn't exist", 'message')
			}
		}
	}

	// load custom component library file a user chooses
	public void fileLoadCLib(File file) {
		boolean zipLoad = false
		ZipFile zf = null;
		def xml

		FileNameExtensionFilter fil = FileFilters.ZipLibrary.getFilter()
		if (file.getName().endsWith(fil.getExtensions().first())) {
			zipLoad = true
			zf = new ZipFile(file);
		}


		if (zipLoad) {
			for (ZipEntry e : zf.entries()) {
				if (e.getName().equals(StringUtil.replaceExtension(file.getName(), "dcl"))) {
					InputStream zis = zf.getInputStream(e)
					XmlSlurper slurper = new XmlSlurper( false, false, true )
					slurper.setEntityResolver(ComponentLibrary.CATALOG_RESOLVER)
					slurper.setProperty( XMLConstants.ACCESS_EXTERNAL_DTD, "all" )
					xml = slurper.parse(zis)
					zis.close()
				}
			}

		} else {
			xml = ComponentLibrary.parseXmlFile(file)
		}

		if (xml == null) {
			log.error("Could not load CLZ File.", "message")
			return;
		}

		String file_cversion = ''
		file_cversion = xml.@cversion?.text()
		String file_oversion = ''
		file_oversion = xml.@oversion?.text()
		String file_version = ''
		file_version = xml.@version?.text()

		String current_cversion = DeploymentLabVersion.getComponentsVersion() //always 1.0
		String current_oversion = DeploymentLabVersion.getObjectsVersion()
		String current_version = DeploymentLabVersion.getComponentLibVersion() //always 1.0
		List allowedCLibs = CentralCatalogue.getApp("componentLibs.allowedObjectVersions").split(",")

		log.trace("Component library:" + file.getName())
		log.trace("component model version: '$file_cversion'")
		log.trace("object model version: '$file_oversion'")
		log.trace("component library version: '$file_version'")

		//if(!file_cversion.equals(current_cversion) || !file_oversion.equals(current_oversion) || !file_version.equals(current_version) ){
		if (!allowedCLibs.contains(file_oversion)) {
			def filename = file.getName()
			log.error("Incompatible version number ($file_oversion). Cannot load '$filename' component library", 'message')
			return
		} else {
			// load component library
			componentLibrary.loadCustomComponentLibrary(file, xml)
			// add file-> loaded component library menu
			if (componentLibMenu.isMenuComponent(subMenuNoComponentLib)) {
				componentLibMenu.remove(subMenuNoComponentLib)
			}

			SwingBuilder CLibBuilder = new SwingBuilder()
			JMenuItem menuItem
			if (componentLibMenuMap[file.getName()] == null) {
				menuItem = CLibBuilder.menuItem(text: file.getName(), actionPerformed: { fileUnloadCLib(file.getName()) })
				componentLibMenuMap[file.getName()] = menuItem
			} else {
				menuItem = componentLibMenuMap[file.getName()]
			}
			//	componentLibXmlMap[file.getName()] = xml
			componentLibMenu.add(menuItem)

			// add component library file info to user preference
			userPreferences.addComponentLib(file)

		}
	}

	// unload component library
	public void fileUnloadCLib(String fileName) {
		JMenuItem menuItem = componentLibMenuMap[fileName]

		// remove from treeModels (ComponentLibrary)
		//componentLibrary.unloadCustomComponentLibrary(componentLibXmlMap[fileName])
		componentLibrary.unloadCustomComponentLibrary(fileName)

		// remove from file-> component library menu
		componentLibMenu.remove(menuItem)

		if (componentLibMenu.getItemCount() == 0) {
			componentLibMenu.add(subMenuNoComponentLib)
		}

		// delete component library info from user preference
		userPreferences.removeComponentLib(fileName)
	}

	public void fileExit() {
		if (backgroundTaskRunning()) {
			JOptionPane.showMessageDialog(builder.deploymentLabFrame, "Please wait for the current operation to complete.", "Error!", JOptionPane.ERROR_MESSAGE)
			return
		}
		Thread exitTask = new Thread({
			int n = checkSaved()
			if (n == JOptionPane.CANCEL_OPTION || n == JOptionPane.CLOSED_OPTION)
				return
			log.info(appProperties.getProperty('application.shortname') + " exiting.")
			System.exit(0)
		} as Runnable, "DL Exit Thread")
		exitTask.start()
	}

	private SearchDialog viewSearchDialog
	/**
	 * Displays the search dialog (and creates it if necessary.)
	 */
	void spotlightSearch() {
		if (viewSearchDialog == null) {
			viewSearchDialog = new SearchDialog(deploymentLabFrame, componentModel, selectModel, deploymentPanel, userPreferences)
		}
		viewSearchDialog.show()
	}

	private ScanMACDialog scanMACDialog

	void scanMAC() {
		if (scanMACDialog == null) {
			scanMACDialog = new ScanMACDialog(deploymentLabFrame, CentralCatalogue.getOpenProject(), selectModel, userPreferences, drawingComboBoxModel)
		}
		scanMACDialog.show()
	}

	public void editRotateSelectionArbitrary() {
		((NodeGroupRotator) toolManager.getTool("rotator2")).rotateFromDialogBox()
	}

	public void editRotateSelection(Double doThisRotation) {
		((NodeGroupRotator) toolManager.getTool("rotator2")).rotationOperation(doThisRotation, true)
	}

	public void editRotate90(double theta) {
		DrawingRotator rotator = new DrawingRotator(componentModel, selectModel, undoBuffer, deploymentPanel)
		String title
		if (theta > 0) {
			title = uiStrings.getProperty("rotate90.rotateRightTitle")
		} else {
			title = uiStrings.getProperty("rotate90.rotateLeftTitle")
		}
		int choice = JOptionPane.showConfirmDialog(builder.deploymentLabFrame, uiStrings.getProperty("rotate90.message"), title, JOptionPane.YES_NO_OPTION)
		if (choice != JOptionPane.YES_OPTION)
			return
		rotator.editRotate90(theta)
	}

	public void setBGImage() {
		int choice = JOptionPane.showConfirmDialog(builder.deploymentLabFrame, uiStrings.getProperty("setBGImage.message"), uiStrings.getProperty("setBGImage.title"), JOptionPane.YES_NO_OPTION)
		if (choice != JOptionPane.YES_OPTION)
			return


		File imageFile = askForFile(CentralCatalogue.getUIS("setBGImage.filechooser.title"), FileFilters.BackgroundImage.getFilter())

		if (imageFile != null) {
			try {
				DLImage newBg = new DLImage(imageFile)

				if (newBg != null  && validImage(newBg)) {
					selectModel.getActiveDrawing().setPropertyValue('image_data', newBg)
					selectModel.getActiveDrawing().setPropertyValue('width', newBg.getWidth())
					selectModel.getActiveDrawing().setPropertyValue('height', newBg.getHeight())
					selectModel.getActiveDrawing().setPropertyValue('hasImage', true)
					this.viewZoomToFit()
					componentModel.resetImages()
					CentralCatalogue.getOpenProject().setNeedsASave(true)
				}
			} catch( IOException e ) {
				// Something went wrong with the image.
				log.error( e.getMessage(), "message")
			}
		}
	}

	private boolean validImage(DLImage image){
		int width = deploymentPanel.getFullBoundsWidth();
		int height = deploymentPanel.getFullBoundsHeight();

		if (width > image.getWidth()){
			log.error("Image size less than minimum canvas size. Image:[${image.getWidth()}x${image.getHeight()}], Canvas:[${width}x${height}]", "message")
			return false
		}

		if (height > image.getHeight()){
			log.error("Image size less than minimum canvas size. Image:[${image.getWidth()}x${image.getHeight()}], Canvas:[${width}x${height}]", "message")
			return false
		}

		return true
	}

	/**
	 * remove background image ( make the drawing to virtual layer)
	 */
	public void removeBGImage() {
		int choice = JOptionPane.showConfirmDialog(builder.deploymentLabFrame, uiStrings.getProperty("setBGImage.message"), uiStrings.getProperty("setBGImage.title"), JOptionPane.YES_NO_OPTION)
		if (choice != JOptionPane.YES_OPTION)
			return

		selectModel.getActiveDrawing().setPropertyValue('image_data', null)
		selectModel.getActiveDrawing().setPropertyValue('hasImage', false)
		this.viewZoomToFit()
		componentModel.resetImages()
		CentralCatalogue.getOpenProject().setNeedsASave(true)
	}


	public void setScale() {
		deploymentPanel.startScalingOp()
	}

	public void setCanvasSize(){
		canvasSettingsDialog.show()
	}

	public void setupGeneralOptions() {
		builder.esHostIP.text = projectSettings.esHost
		builder.acHostIP.text = projectSettings.acHost
		def btns = builder.unitSystemGroup.getElements()

		if (projectSettings.unitSystem != null && projectSettings.unitSystem != '') {
			btns.each { b ->
				if (b.getActionCommand() == projectSettings.unitSystem) {
					b.setSelected(true)
				}
			}
		}
		updateUnitSystem()
	}

	public void updateGeneralOptions() {
		projectSettings.esHost = builder.esHostIP.text
		projectSettings.acHost = builder.acHostIP.text
		updateUnitSystem()
	}

	public void updateUnitSystem() {
		String newUnit = builder.unitSystemGroup.getSelection().getActionCommand()
		projectSettings.unitSystem = newUnit

		if (newUnit != defaultUnit) {
			//SI
			systemConverter = CentralCatalogue.INSTANCE.getUnitSystems().getSystemConverter(defaultUnit, newUnit)
		} else {
			//American
			systemConverter = null
		}
		CentralCatalogue.instance.setSystemConverter(systemConverter)

		// Some of these can slowly be replaced with references to the central catalog instance.
		pm.setUnitSystem(systemConverter, CentralCatalogue.INSTANCE.getUnitSystems())
		deploymentPanel.setUnitSystem(systemConverter)
		viewOptionDialog.setUnitSystem(systemConverter)
	}

	private void exportSchema(boolean forceFullExport = false, boolean checkSync = true) {
		if (forceFullExport && checkSync) {
			int choice = JOptionPane.showConfirmDialog(builder.deploymentLabFrame, uiStrings.getProperty("exportSchema.force.message"), uiStrings.getProperty("exportSchema.force.title"), JOptionPane.YES_NO_OPTION)
			if (choice != JOptionPane.YES_OPTION) {
				return
			}
		} else if (forceFullExport && !checkSync) {
			//println "this is the bad option"
			int choice = JOptionPane.showConfirmDialog(builder.deploymentLabFrame, CentralCatalogue.getUIS("exportSchema.forceNoSync.message"), CentralCatalogue.getUIS("exportSchema.forceNoSync.title"), JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE)
			if (choice != JOptionPane.YES_OPTION) {
				return
			}
		}

		if (openFile == null) {
			log.error("Please save the deployment before exporting.", 'message')
			return
		}

		//List<DLObject> drawings = ChooseDrawingController.chooseDrawingAsObjects(selectModel,componentModel,true)
//		List<DLComponent> drawingComponents = ChooseDrawingController.chooseDrawing(selectModel,componentModel,true)

		ExportController ec
		Validator v

		//NOTE: the export-one-drawing-at-a-time feature has been pulled, but this commented out stuff is that feature
		//uncomment to get it back
//		if (drawingComponents.size() == componentModel.getComponentsInRole("drawing").size()){

		ec = new ExportController(CentralCatalogue.getOpenProject())
		v = new Validator(objectModel, componentModel, projectSettings)

/*
		} else {
			List<DLObject> drawingObjects = ChooseDrawingController.getDrawingObjects(drawingComponents)
			log.trace("selected drawings: $drawingObjects")
			ObjectModel subObjectModel = objectModel.makeModelSubset(drawingObjects)
			println subObjectModel.getObjects()
			ComponentModel subComponentModel = componentModel.makeModelSubset(subObjectModel)
			println subComponentModel.getComponents()
			//DLProject subProject = new DLProject(undoBuffer,subComponentModel,subObjectModel)
			//subProject.setFile(File.createTempFile(openFile.getName(), ".dlt"))
			//CentralCatalogue.getOpenProject().unlockProjectFile()
			//subProject.setFileButDontLock(openFile)
			ec = new ExportController(CentralCatalogue.getOpenProject(), subComponentModel, subObjectModel)
			v = new Validator(subObjectModel, subComponentModel, projectSettings)
		}
*/

		deploymentLabFrame.setCursor(Cursor.WAIT_CURSOR)

		//Validator v = new Validator(objectModel, componentModel, projectSettings)
		//Validator v = new Validator(subObjectModel, subComponentModel, projectSettings)
		int res = v.validate()
		deploymentLabFrame.setCursor(null)
		if (res == Validator.RESULT_PASS) {
			Properties connectProperties = new Properties()
			if (loginDialog.getLoginInfo(connectProperties)) {
				doBackgroundTask({
					//def ec = new ExportController(CentralCatalogue.getOpenProject())
					//	def ec = new ExportController(subProject)
					ec.loginESDoExportTask(connectProperties, ExportTaskType.EXPORT, forceFullExport, checkSync)
					//subProject.unlockProjectFile()
				})
			}
			return
		}

		if (res == Validator.RESULT_ABORT) {
			log.info("The validation process failed.  This is bad!", 'message')
			return
		}

		def warnings = v.getWarnings()
		def failures = v.getFailures()

		if (warnings.size() > 0 && failures.size() > 0) {
			String failureMessage = failures.inject('') { s, vr ->
				s + vr.message + ": " + vr.objects.collect { o -> o.getComponentProperty('name').getValue() }.join(',') + LINE_SEP
			}
			String warningMessage = warnings.inject('') { s, vr ->
				s + vr.message + ": " + vr.objects.collect { o -> o.getComponentProperty('name').getValue() }.join(',') + LINE_SEP
			}
			String message = "Validation failed with " + failures.size() + " errors and " + warnings.size() + " warnings." + LINE_SEP + "--- Errors ---" + LINE_SEP + failureMessage + "--- Warnings ---" + LINE_SEP + warningMessage
			log.error(message, 'message')
		} else if (failures.size() > 0) {
			String failureMessage = failures.inject('') { s, vr ->
				s + vr.message + ": " + vr.objects.collect { o -> o.getComponentProperty('name').getValue() }.join(',') + LINE_SEP
			}
			String message = "Validation failed with " + failures.size() + " errors." + LINE_SEP + failureMessage
			log.error(message, 'message')
		} else { // warnings only
			String warningMessage = warnings.inject('') { s, vr ->
				s + vr.message + ": " + vr.objects.collect { o -> o.getComponentProperty('name').getValue() }.join(',') + LINE_SEP
			}

			scrollingExportWarningText.text = "Validation produced " + warnings.size() + " warnings." + LINE_SEP + warningMessage + LINE_SEP + "Export anyway?"
			getScrollingExportWarningDialog().setLocationRelativeTo(builder.deploymentLabFrame)
			scrollingWarningDialogResult = JOptionPane.CANCEL_OPTION
			getScrollingExportWarningDialog().setVisible(true)
			if (scrollingWarningDialogResult == JOptionPane.OK_OPTION) {
				Properties connectProperties = new Properties()
				if (loginDialog.getLoginInfo(connectProperties)) {
					doBackgroundTask({
						//def ec = new ExportController(CentralCatalogue.getOpenProject())
						//def ec = new ExportController(subProject)
						ec.loginESDoExportTask(connectProperties, ExportTaskType.EXPORT, forceFullExport, checkSync)
						//subProject.unlockProjectFile()
					})
				}

			}
		}
	}

	private void checkSyncOnly() {
		def ec = new ExportController(CentralCatalogue.getOpenProject())
		//ec methods fires all user-facing messages
		ec.checkSyncOnly()


	}


	private void exportSuperSimConfig() {
		if (fileChooser.getSelectedFile()?.isFile())
			fileChooser.setSelectedFile(stripExtension(fileChooser.getSelectedFile()))
		fileChooser.setFileFilter(FileFilters.LuaFile.getFilter())
		fileChooser.setDialogTitle(uiStrings.getProperty("exportSimConfig.filechooser.title"))
		int rv = fileChooser.showSaveDialog(builder.deploymentLabFrame)
		if (rv == JFileChooser.APPROVE_OPTION) {
			String selectedPath = fileChooser.getSelectedFile().getPath()
			if (!selectedPath.endsWith(".lua"))
				selectedPath = selectedPath + ".lua"

			doBackgroundTask({
				progressPane.setCaption(uiStrings.getProperty("exportSimConfig.progress"))
				try {
					File selectedFile = new File(selectedPath)
					SuperSimConfigExporter.writeConfig(objectModel, selectedFile)
				} catch (IOException ioe) {
					log.error(String.format(uiStrings.getProperty("exportSimConfig.saveerror"), ioe.getMessage()), "message")
					log.error(log.getStackTrace(ioe), "default")
				}
			})

		}
	}

	private void exportSimConfigWithPower(List<SimBundle> otherItems = null, boolean useEnhanced = false) {
		def usr = JOptionPane.showConfirmDialog(null, "This sim config file will include all power items in this drawing as read from the server.", "Please Do Not Press This Button Again.", JOptionPane.YES_NO_OPTION)
		if (usr != JOptionPane.YES_OPTION) {
			return;
		}

		Properties prop = new Properties()
		if (loginDialog.getLoginInfo(prop)) {
			String serverUrl = SynapEnvContext.populateProperties(prop, projectSettings)
			SynapEnvContext ctx = new SynapEnvContext(prop)

			DeploymentLab.Examinations.PowerImagingRuleExamination.setEnv(ctx.getEnv())

			if (ctx.testConnect()) {
				DLObject drawing = objectModel.roots[0]
				def results = PowerImagingRuleExamination.findPowerItems(TOFactory.getInstance().loadTO(drawing.key))

				log.trace(results.toString())

				//call the exporter
				this.exportSimConfig(results, useEnhanced)

			} else {
				log.error(ctx.getMessage(), 'message');
				ctx.logout()
			}
		}
	}

	private void exportSimConfig(List<SimBundle> otherItems = null, boolean useEnhanced = false) {
		//NOTE: the export-one-drawing-at-a-time feature has been pulled, but this commented out stuff is that feature
		//uncomment to get it back... after fixing the broken pieces.
		/*
		def chosenDrawings = ChooseDrawingController.chooseDrawing(selectModel, componentModel)
		log.trace("drawings chosen are $chosenDrawings")
		if (chosenDrawings.size() == 0) {
			return
		}
		*/

		//def f = new File( "simulator_${selectModel.getActiveDrawing().getPropertyStringValue("name")}.xml" )
		def f = new File("simulator_${FileUtil.changeExtension(CentralCatalogue.getOpenProject().getFile().toFile(), "xml").getName()}")
		simFileChooser.setSelectedFile(f)
		simFileChooser.setFileFilter(FileFilters.XMLFile.getFilter())
		simFileChooser.setDialogTitle(uiStrings.getProperty("exportSimConfig.filechooser.title"))

		simFileChooser.setCurrentDirectory(new File(userPreferences.getUserPreference(UserPreferences.SIM)))

		int rv = simFileChooser.showSaveDialog(builder.deploymentLabFrame)

		if (rv == JFileChooser.APPROVE_OPTION) {
			String selectedPath = simFileChooser.getSelectedFile().getPath()
			if (!selectedPath.endsWith(".xml"))
				selectedPath = selectedPath + ".xml"


			doBackgroundTask({
				progressPane.setCaption(uiStrings.getProperty("exportSimConfig.progress"))
				try {
					File selectedFile = new File(selectedPath)
					userPreferences.setUserPreference(UserPreferences.SIM, selectedFile.getParent())

					//def drawingObjects = ChooseDrawingController.getDrawingObjects(chosenDrawings)
					ObjectModel subObjectModel = objectModel  //.makeModelSubset(drawingObjects)
					ComponentModel subComponentModel = componentModel  //.makeModelSubset(subObjectModel)
					if (!useEnhanced) {
						SimConfigExporter.writeConfig(subComponentModel, subObjectModel, selectedFile, otherItems, projectSettings, CentralCatalogue.getOpenProject().accessModel.getObjectModel())
					} else {
						def ese = new EnhancedSimConfigExporter(CentralCatalogue.getOpenProject(), otherItems)
						ese.writeConfig(selectedFile)
					}

					//SimConfigExporter.writeConfig(componentModel, objectModel, selectedFile, otherItems, projectSettings)
				} catch (IOException ioe) {
					log.error(String.format(uiStrings.getProperty("exportSimConfig.saveerror"), ioe.getMessage()), "message")
					log.error(log.getStackTrace(ioe), "default")
				}
			})

		}
	}

	private void exportDMConfig() {
		if (fileChooser.getSelectedFile()?.isFile())
			fileChooser.setSelectedFile(stripExtension(fileChooser.getSelectedFile()))
		fileChooser.setFileFilter(FileFilters.XMLFile.getFilter())
		fileChooser.setDialogTitle(uiStrings.getProperty("exportDMConfig.filechooser.title"))
		int rv = fileChooser.showSaveDialog(builder.deploymentLabFrame)
		if (rv == JFileChooser.APPROVE_OPTION) {
			String selectedPath = fileChooser.getSelectedFile().getPath()
			if (!selectedPath.endsWith(".xml")) {
				selectedPath = selectedPath + ".xml"
			}

			doBackgroundTask({
				progressPane.setCaption(uiStrings.getProperty("exportDMConfig.progress"))
				try {
					File selectedFile = new File(selectedPath)
					DevmanConfigExporter.writeConfig(objectModel, selectedFile)
					log.info("Device Manager configuration saved.", 'message')
				} catch (IOException ioe) {
					log.error(String.format(uiStrings.getProperty("exportDMConfig.saveerror"), ioe.getMessage()), "message")
					log.error(log.getStackTrace(ioe), "default")
				}
			})

		}
	}

	public void clearEsIds() {
		int choice = JOptionPane.showConfirmDialog(builder.deploymentLabFrame, uiStrings.getProperty("clearEsIds.message"), uiStrings.getProperty("clearEsIds.title"), JOptionPane.YES_NO_OPTION)
		if (choice == JOptionPane.YES_OPTION) {
			List<DLObject> drawings = ChooseDrawingController.getDrawingObjects(ChooseDrawingController.chooseDrawing(selectModel, componentModel))
			if (drawings.size() == 0) {
				return
			}

			try {
				objectModel.clearKeysForDrawing(drawings)
				componentModel.resetImages()
				log.info("Object IDs cleared.", 'message')
				projectSettings.addAnnotation("Reset Object IDs for ${drawings}")
			} catch (Exception e) {
				log.error("Clearing Object IDs failed!\nSee log file for details.", 'message')
				log.error(log.getStackTrace(e))
			}
		}
	}


	public void unExportSchema(boolean useRecursive = true) {
		def ec = new ExportController(CentralCatalogue.getOpenProject())
		ec.removeSchemaFromServer(useRecursive)
	}

	public void unlinkFromSmartZone() {
		int choice = JOptionPane.showConfirmDialog( deploymentLabFrame, "This will prevent changes from being synchronized, but will not modify the location in SmartZone.\nDo you wish to continue?", 'Unlink from SmartZone', JOptionPane.YES_NO_OPTION )
		if( choice == JOptionPane.YES_OPTION ) {
			doBackgroundTask({
				for( DLComponent drawing : CentralCatalogue.getOpenProject().getComponentModel().getComponentsByType( ComponentType.DRAWING ) ) {
					if( drawing.isSmartZoneLinked() ) {
						// First try to clear the instrumented flag on the server.
						UninstrumentFloorplanController ufc = new UninstrumentFloorplanController( deploymentLabFrame, smartZoneLoginDialog,
						                                                                           smartZoneServerDialog, drawing.getSmartZoneId() )
						if( ufc.start() ) {
							if( ufc.getNotFound() ) {
								log.error("Location '${drawing.getName()}' not found in SmartZone. Continuing to unlink local project.", "message")
							} else if( !ufc.wasSuccessful() ) {
								log.error("Location '${drawing.getName()}' already unlinked in SmartZone. Continuing to unlink local project.", "message")
							}
						} else {
							log.error("Error attempting to contact SmartZone server. Continuing to unlink local project.", "message")
						}

						// Error or not, we still unlink the file.
						CentralCatalogue.getOpenProject().unlinkFromSmartZone()
						uiDisplay.setProperty("isSmartZoneLinked", false)
						uiDisplay.setProperty("isSZAdvancedEditorMode", false)
						uiDisplay.setProperty("isNotSZAdvancedEditorMode", true)
					}
				}
				fileSaveLocal()
				log.info("Project unlinked from SmartZone.", "statusbar")
			})
		}
	}


	public void exportDeploymentPlan() {
		String path
		try {
			//if(imageFileChooser.getSelectedFile()?.isFile())
			//	imageFileChooser.setSelectedFile(stripExtension(imageFileChooser.getSelectedFile()))

			if (currentFileName != null) {
				imageFileChooser.setSelectedFile(FileUtil.stripExtension(currentFileName))
			} else {
				imageFileChooser.setSelectedFile(new File(userPreferences.getUserPreference(UserPreferences.FILENAME)))
			}

			imageFileChooser.setCurrentDirectory(new File(userPreferences.getUserPreference(UserPreferences.FILE)))
			imageFileChooser.resetChoosableFileFilters()
			imageFileChooser.setFileFilter(FileFilters.PNGFile.getFilter())
			imageFileChooser.setDialogTitle(uiStrings.getProperty("exportDeploymentPlan.filechooser.title"))
			int rv = imageFileChooser.showSaveDialog(builder.deploymentLabFrame)
			if (rv == JFileChooser.APPROVE_OPTION) {
				builder.deploymentLabFrame.setCursor(new Cursor(Cursor.WAIT_CURSOR))

				BufferedImage deploymentImage = deploymentPanel.plotDeployment()

				path = imageFileChooser.getSelectedFile().getPath()
				if (!path.endsWith(".png"))
					path = path + ".png"

				userPreferences.setUserPreference(UserPreferences.FILE, new File(path).getParent())

				if (new File(path).exists()) {
					if (JOptionPane.showConfirmDialog(deploymentLabFrame, "File exists.  Overwrite?", "Confirm File Overwrite", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
						ImageIO.write(deploymentImage, "png", new File(path))
						log.info("Export deployment plan to '$path' complete.", 'statusbar')
					}
				} else {
					ImageIO.write(deploymentImage, "png", new File(path))
					log.info("Export deployment plan to '$path' complete.", 'statusbar')
				}
			}

		} catch (Exception e) {
			log.error("Exception saving file $path :" + log.getStackTrace(e))
			def errors = "Error while saving file '$path'"
			if (e.getMessage()) {
				errors += ": ${e.getMessage()}."
			} else {
				errors += "."
			}
			errors += "\nTry saving with a new name or in a new location."
			log.error(errors, 'message')
		} finally {
			builder.deploymentLabFrame.setCursor(null)
		}

	}

	public void exportBackgroundImage() {
		String path
		try {
			//if(imageFileChooser.getSelectedFile()?.isFile())
			//	imageFileChooser.setSelectedFile(stripExtension(imageFileChooser.getSelectedFile()))

			if (currentFileName != null) {
				imageFileChooser.setSelectedFile(FileUtil.stripExtension(currentFileName))
			} else {
				imageFileChooser.setSelectedFile(new File(userPreferences.getUserPreference(UserPreferences.FILENAME)))
			}

			imageFileChooser.setCurrentDirectory(new File(userPreferences.getUserPreference(UserPreferences.FILE)))
			imageFileChooser.resetChoosableFileFilters()
			imageFileChooser.setFileFilter(FileFilters.PNGFile.getFilter())
			imageFileChooser.setDialogTitle(uiStrings.getProperty("exportBackgroundImage.filechooser.title"))
			int rv = imageFileChooser.showSaveDialog(builder.deploymentLabFrame)
			if (rv == JFileChooser.APPROVE_OPTION) {
				builder.deploymentLabFrame.setCursor(new Cursor(Cursor.WAIT_CURSOR))

				DLImage deploymentImage = selectModel.getActiveDrawing().getPropertyValue('image_data')

				path = imageFileChooser.getSelectedFile().getPath()
				if (!path.endsWith(".png"))
					path = path + ".png"

				userPreferences.setUserPreference(UserPreferences.FILE, new File(path).getParent())

				if (new File(path).exists()) {
					if (JOptionPane.showConfirmDialog(deploymentLabFrame, "File exists.  Overwrite?", "Confirm File Overwrite", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
						ImageIO.write(deploymentImage, "png", new File(path))
						log.info("Export background image to '$path' complete.", 'statusbar')
					}
				} else {
					ImageIO.write(deploymentImage, "png", new File(path))
					log.info("Export background image to '$path' complete.", 'statusbar')
				}
			}

		} catch (Exception e) {
			log.error("Exception saving file $path :" + log.getStackTrace(e))
			def errors = "Error while saving file '$path'"
			if (e.getMessage()) {
				errors += ": ${e.getMessage()}."
			} else {
				errors += "."
			}
			errors += "\nTry saving with a new name or in a new location."
			log.error(errors, 'message')
		} finally {
			builder.deploymentLabFrame.setCursor(null)
		}
	}

	private void exportAllImages() {
		String path
		dirChooser.setCurrentDirectory(new File(userPreferences.getUserPreference(UserPreferences.FILE)))

		int choice = dirChooser.showDialog(deploymentLabFrame, "Choose Directory")
		if (choice == JFileChooser.APPROVE_OPTION) {
			String selectedPath = dirChooser.getSelectedFile().getPath()

			if (!dirChooser.getSelectedFile().exists()) {
				dirChooser.getSelectedFile().mkdirs()
			}

			userPreferences.setUserPreference(UserPreferences.FILE, selectedPath)

			doBackgroundTask({ doExportAllImages(selectedPath) })
		}
	}

	private void doExportAllImages(String selectedPath) {
		log.info("Exporting Images...", 'progress')
		ImageExporter ie = new ImageExporter()
		try {
			ie.exportToLocal(componentModel, selectedPath)
			log.info("Background Images exported to $selectedPath successfully.", 'statusbar')
		} catch (IOException ioe) {
			//if we get an IO exception when saving the image files, let the user know and bail out of the process.
			log.error("IO Error when saving background image: ${ioe.getMessage()}", 'message')
		} catch (Exception e) {
			log.error(log.getStackTrace(e))
			log.error("Error processing images for export!\nUnable to continue.", 'message')
		} finally {
			builder.deploymentLabFrame.setCursor(null)
		}
	}

	public void exportData(String param) {
		def chosenDrawings = ChooseDrawingController.chooseDrawing(selectModel, componentModel)
		if (chosenDrawings.size() == 0) {
			return
		}
		HashSet<DLObject> drawingObjects = new HashSet<DLObject>()
		String fileName = ""
		chosenDrawings.each { f ->
			drawingObjects += f.getObject( ObjectType.DRAWING )
			fileName+=f.name+"_"
		}
		userPreferences.setUserPreference(UserPreferences.CSV_TITLE, CentralCatalogue.getUIS("exportSearch.filechooser.title"))
		switch (param) {
			case "nodename":
				userPreferences.setUserPreference(UserPreferences.CSV_FILENAME, fileName+"Node_Names")
				DLObjectValueExporter.exportTable(userPreferences, DLObjectValueExporter.exportNodeNames(objectModel, drawingObjects), deploymentLabFrame);
				break;
			case "mac":
				userPreferences.setUserPreference(UserPreferences.CSV_FILENAME, fileName+"ScanList")
				DLObjectValueExporter.exportTable(userPreferences, DLObjectValueExporter.exportComponents(componentModel, chosenDrawings, true), deploymentLabFrame);
				break;
			case "object":
				userPreferences.setUserPreference(UserPreferences.CSV_FILENAME, fileName+"Objects")
				DLObjectValueExporter.exportTable(userPreferences, DLObjectValueExporter.exportObjects(objectModel, drawingObjects), deploymentLabFrame);
				break;
		}


	}

	public void importData(String param) {
		switch (param) {
			case "access":
				def result = DLObjectValueImporter.importTable(userPreferences, deploymentLabFrame, CentralCatalogue.getUIS("importCSV.filechooser.title.access"))
				if (result) {
					DLObjectValueImporter.importAccessList(CentralCatalogue.getOpenProject(), selectModel, result, deploymentLabFrame);
				}
				break;
			case "scan":
				def result = DLObjectValueImporter.importTable(userPreferences, deploymentLabFrame, CentralCatalogue.getUIS("importCSV.filechooser.title.scan"))
				if (result) {
					DLObjectValueImporter.importMacScanList(CentralCatalogue.getOpenProject(), result)
				}
				break;
		}
	}

	public void importComponentProperties(){
		def importFileChooser = new JFileChooser()
		importFileChooser.addChoosableFileFilter(FileFilters.ExcelFile.getFilter())
		importFileChooser.setFileFilter(FileFilters.ExcelFile.getFilter())
		importFileChooser.setDialogTitle(CentralCatalogue.getUIS("importExcel.filechooser.title"))

		importFileChooser.setCurrentDirectory(new File(userPreferences.getUserPreference(UserPreferences.EXCEL)))

		int rv = importFileChooser.showOpenDialog(deploymentLabFrame)
		if (rv == JFileChooser.APPROVE_OPTION) {
			String selectedPath = importFileChooser.getSelectedFile().getPath()

			File selectedFile = new File(selectedPath)
			userPreferences.setUserPreference(UserPreferences.EXCEL, selectedFile.getParent())
			doBackgroundTask({
				def result = DLComponentValueImporter.importFromExcel(selectedPath)
				if (result.isEmpty()){
					log.warn("Nothing readed from $selectedPath", "message")
				} else {
					DLComponentValueImporter.importComponents(componentModel, result)
					log.info("Components imported from $selectedPath", "statusbar")
				}
			})
		}
	}

	public void viewZoomIn() {
		deploymentPanel.setZoom(deploymentPanel.getZoom() + 0.1)
	}

	public void viewZoomOut() {
		deploymentPanel.setZoom(deploymentPanel.getZoom() - 0.1)
	}

	public void viewSetZoom() {
		while (true) {
			String sPercent = JOptionPane.showInputDialog(builder.deploymentLabFrame, "Zoom percent (10% - 400%) :", "New Zoom Level", JOptionPane.PLAIN_MESSAGE)
			if (sPercent == null)
				break
			sPercent = sPercent.trim()
			try {
				int iPercent = Integer.parseInt(sPercent)
				if (iPercent >= 10 && iPercent <= 400) {
					deploymentPanel.setZoom(iPercent / 100.0)
					break
				} else {
					log.error("Zoom factor must be between 10 and 400 percent!", 'message')
				}
			} catch (Exception e) {
				log.error("Zoom factor must be between 10 and 400 percent!", 'message')
			}
		}
	}

	public void viewZoom11() {
		deploymentPanel.setZoom(1.0)
	}

	public void viewZoomToFit() {
		deploymentPanel.fitView()
	}

	public void setDisplayGroupFilter() {
		toggleGroupMap["Environmentals"] = environmentalsToggleBtn
		toggleGroupMap["Power"] = powerToggleBtn
		toggleGroupMap["Calculations"] = pueToggleBtn
		toggleGroupMap["Control"] = controlToggleBtn
		toggleGroupMap["Containment"] = containmentToggleBtn
	}


	public void toggleDisplayGroupFilter(String groupType) {
		//let the palette know what to show or not
		paletteController.toggleDisplayGroupFilter(groupType)

		//viewOptionConfigTree.multipleToggleGroup(groupType, toggleGroupMap[groupType].selected)

		//further restructuring to handle component types that are not in the viewOptions panel,
		// since they don't have a grouppath

		//poll all filter buttons for which filter groups to turn on or off
		List<String> turnOff = []
		List<String> turnOn = []
		toggleGroupMap.each { t, button ->
			if (button.selected) {
				turnOn += componentModel.getComponentTypesForFilter(t)
			} else {
				turnOff += componentModel.getComponentTypesForFilter(t)
			}
		}

		//turn off everything associated with a non-selected filter button
		for (String f : turnOff) {
			viewOptionConfigTree.toggleNode(f, false)
		}
		//turn on anything associated with a selected filter button
		//anything associated with more than 1 filter button will now be shown if any of the buttons are on
		for (String f : turnOn) {
			viewOptionConfigTree.toggleNode(f, true)
		}

		//combine those two lists to get the final batch of invisible types
		def objectTypes = []
		for (String off : turnOff) {
			if (!turnOn.contains(off)) {
				objectTypes += off
			}
		}

		//update the displayProperties with the final invisibleObjects list
		displayProperties.setProperty('invisibleObjects', objectTypes)
		displayProperties.firePropertyChanged()

		//unselect anything of those types
		def unselects = []
		for (String t : objectTypes) {
			for (DLComponent c : componentModel.getComponentsByType(t)) {
				unselects += c
			}
		}
		selectModel.removeFromSelection(unselects)
	}

	private void resetDisplayGroupFilter() {
		//toggleGroupMap.each{t,btn->btn.selected = true;viewOptionConfigTree.multipleToggleGroup(t, true)}
		toggleGroupMap.each { t, btn -> btn.selected = true; }
		viewOptionConfigTree.reset()

	}

	private void initUI() {
		uiDisplay.setProperty('isFileLoaded', true)
		resetDisplayGroupFilter()
		//displayProperties.reset()
		viewOptionDialog.initViewOptions()
		this.viewZoomToFit() //fit the entire drawing to the screen
		exportComponentDialog.clearSelection()
	}

	/**
	 * Displays a File Chooser dialog and returns the file selected by the user.
	 *
	 * @param dialogTitle Dialog title to display.
	 * @param filter (Optional) The file filter to use.
	 *
	 * @return The selected file or null if the action is cancelled.
	 */
	private File askForFile(String dialogTitle, FileNameExtensionFilter filter = null) {
		FileUtil.askForFile(userPreferences, dialogTitle, filter)
	}

	/**
	 * Show the about box.
	 */
	public void helpAbout() {
		def ac = new AboutController()
		ac.showDialog(deploymentLabFrame)
	}

	/**
	 * Show the user-level dialog.
	 */
	void showUserLevelDialog() {
		def userLevelDialog = new UserLevelDialog(uiDisplay, userAccess, deploymentLabFrame)
		userLevelDialog.setLocationRelativeTo()
		userLevelDialog.show()
	}

	/**
	 * Show the change access code dialog.
	 */
	void showAccessCodeDialog() {
		def accessCodeDialog = new AccessCodeDialog(userAccess, deploymentLabFrame)
		accessCodeDialog.setLocationRelativeTo()
		accessCodeDialog.show()
	}

	void showViewOption() {
		viewOptionDialog.setLocationRelativeTo()
		viewOptionDialog.show()
	}

	void viewOptionToggleNames() {
		viewOptionDialog.showNames(!viewOptionDialog.namesShown())
	}

	void viewOptionToggleHalos() {
		viewOptionDialog.showHalos(!viewOptionDialog.halosShown())
	}

	void viewOptionToggleAssociations() {
		viewOptionDialog.showAssociations(!viewOptionDialog.associationsShown())
	}

	void viewOptionToggleAssociationDetail() {
		viewOptionDialog.showAssociationDetail(!viewOptionDialog.associationDetailShown())
	}

	void viewOptionToggleBackground() {
		viewOptionDialog.showBackground(!viewOptionDialog.backgroundShown())
	}

	void viewOptionToggleFloorTilesView() {
		viewOptionDialog.showFloorTilesView(!viewOptionDialog.gridViewShown())
	}

	// move to validatorMessageDialog to DeploymentLab.Dialogs package
	private PreExportValidationDialog preExportValidationDialog

	void showPreExportValidation(boolean export = false) {
		if (preExportValidationDialog == null) {
			preExportValidationDialog = new PreExportValidationDialog(deploymentLabFrame, selectModel, componentModel, objectModel, projectSettings, deploymentPanel, userPreferences, this)
		}
		if (!export){
			preExportValidationDialog.show()
		} else {
			preExportValidationDialog.export()
		}
	}

	//displays the stats dialog, now with the dialog itself living inside DeploymentStatsController
	void viewStats() {
		deploymentStats.showDialog()
	}

	void viewStatsByWSN(){
		deploymentStats.showWSNNetworkDialog()
	}

	void configureDefaults() {
		def cdc = new ConfigureDefaultsController(componentModel, objectModel, projectSettings)
		cdc.showDialog(deploymentLabFrame)
	}

	void configureSimulator() {
		def cdc = new ConfigureSimulatorController(componentModel, objectModel, projectSettings)
		cdc.showDialog(deploymentLabFrame)
	}


	void openUserDocDialog() {
		Desktop desktop = Desktop.getDesktop()
		File manual = new File(appProperties.getProperty('application.usermanual'))
		log.info("about to open ${manual.getAbsolutePath()}")
		try {
			desktop.open(manual)
		} catch (IOException e) {
			log.error(log.getStackTrace(e))
			log.error("Cannot launch viewer for user documentation!  Please make sure a PDF viewer is installed.", 'message')
		} catch (Exception ex) {
			log.error(log.getStackTrace(ex))
			log.error("Cannot launch viewer for user documentation!  Please make sure a PDF viewer is installed.", 'message')
		}
	}

	void openComponentEncyclopedia() {
		Desktop desktop = Desktop.getDesktop()
		File manual = new File(appProperties.getProperty('application.encyclopedia'))
		log.info("about to open ${manual.getAbsolutePath()}")
		try {
			desktop.open(manual)
		} catch (java.lang.IllegalArgumentException iae) {
			log.error(log.getStackTrace(iae))
			log.error("Cannot launch viewer for user documentation!  ${iae.getMessage()}", "message")
		} catch (IOException e) {
			log.error(log.getStackTrace(e))
			log.error("Cannot launch viewer for user documentation!  Please make sure a PDF viewer is installed.", 'message')
		} catch (Exception ex) {
			log.error(log.getStackTrace(ex))
			log.error("Cannot launch viewer for user documentation!  Please make sure a PDF viewer is installed.", 'message')
		}
	}

	private void deleteNode(ActionEvent ae) {
		//bail if nothing selected, this SHOULD pass the event on to the Deployment panel
		if (selectModel.getExpandedSelection().size() == 0) {
			return;
		}

		def selection = selectModel.getExpandedSelection()

		if (selection.any { it.hasClass('staticchild') && it.getParentComponents().intersect(selection).size() == 0 }) {
			log.info("Cannot delete child component!", 'message')
			return
		}

		ProgressManager.doTask {
			log.trace("Deleting...", "progress")
			if (!uiDisplay.isAccessible('deleteComponent')) {
				log.info(uiDisplay.getUserLevel() + ' cannot delete component')
				return
			}

			def nodeList = selectModel.getExpandedSelection()
			try {
				undoBuffer.startOperation()
				nodeList.each { componentModel.remove(it) }
			} catch (Exception ex) {
				log.error("Error while deleting!", "message")
				log.error(log.getStackTrace(ex))
				undoBuffer.rollbackOperation()
			} finally {
				undoBuffer.finishOperation()
			}
		}
	}

	private void activateConsole() {
		Console console = new Console()
		console.setVariable("objectModel", objectModel)
		console.setVariable("componentModel", componentModel)
		console.setVariable("projectSettings", projectSettings)
		console.setVariable("selectModel", selectModel)
		console.setVariable("undoBuffer", undoBuffer)
		console.setVariable("displayProperties", displayProperties)
		console.setVariable("deploymentPanel", deploymentPanel)
		console.setVariable("toolManager", toolManager)
		console.setVariable("log", log)
		//add more variables here as needed
		console.run()
	}

	private void updateScriptsMenu() {
		def scriptsMenuItems = scriptsMenu.menuComponents
		scriptsMenuItems.each { m -> scriptsMenu.remove(m) }

		File scriptsDir = new File("scripts")
		scriptsDir.listFiles().each { f ->
			def currentName = f.getName()

            if(!f.isDirectory())
            {
                def item = builder.menuItem(text: currentName, name: currentName, actionPerformed: {
                    execPlugin(currentName)
                });
                scriptsMenu.add(item)
            }
		}
	}

	private void execPlugin(String pluginName) {
		String[] roots = new String[1];
		roots[0] = "scripts"

		GroovyScriptEngine gse = new GroovyScriptEngine(roots);
		Binding binding = new Binding();
		binding.setVariable("objectModel", objectModel)
		binding.setVariable("componentModel", componentModel)
		binding.setVariable("projectSettings", projectSettings)
		binding.setVariable("selectModel", selectModel)
		//binding.setVariable("undoBuffer", undoBuffer)
		binding.setVariable("displayProperties", displayProperties)

		undoBuffer.startOperation()
		try {
			gse.run(pluginName, binding);
		} catch (Exception ex) {
			undoBuffer.rollbackOperation()
			log.error("Error in script: ${ex.getMessage()}", "message")
			log.error(log.getStackTrace(ex), "default")
		} finally {
			undoBuffer.finishOperation()
		}
	}

	void windowClosing(WindowEvent e) {
		fileExit()
	}

	void windowActivated(WindowEvent e) {}

	void windowDeactivated(WindowEvent e) {}

	void windowIconified(WindowEvent e) {}

	void windowDeiconified(WindowEvent e) {}

	void windowOpened(WindowEvent e) {}

	void windowClosed(WindowEvent e) {}
}

