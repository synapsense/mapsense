
package DeploymentLab;

import java.util.*;

import DeploymentLab.Model.*;
import DeploymentLab.channellogger.*;

public class CycleChecker {
	private static final Logger log = Logger.getLogger(CycleChecker.class.getName());
	private static final String DATATYPE = "power"; //at the moment, we only care about the pue numeric type

	private ComponentModel cm;

	private ArrayList<DLComponent> seen;

	public CycleChecker(ComponentModel cm) {
		this.cm = cm;
	}

	public ArrayList< ArrayList<DLComponent> > findCycles() {
		seen = new ArrayList<DLComponent>();
		//get a hashset of all types with a producer and a consumer of the "power" type
		HashSet<String> types = new HashSet<String>();
		for(String t: cm.listComponentTypes()){
			Set<String> producerTypes = new HashSet<String>(cm.listTypeProducerDatatypes(t));
			Set<String> consumerTypes = new HashSet<String>(cm.listTypeConsumerDatatypes(t));
			if(producerTypes.contains(DATATYPE) && consumerTypes.contains(DATATYPE)){
				types.add(t);
			}
			//checks on all types... we don't want that after all
			/*if(producerTypes.size() > 0 && consumerTypes.size() > 0){
				for(String p: producerTypes){
					if(consumerTypes.contains(p)){
						types.add(t);
					}
				}
			}
			*/
		}

		//log.trace("types are: " + types.toString());
		ArrayList< ArrayList<DLComponent> > cycles = new ArrayList< ArrayList<DLComponent> >();
		for(DLComponent c: cm.getComponents()) {
			if(types.contains(c.getType())){
				ArrayList<DLComponent> rv = checkCycle(new ArrayList<DLComponent>(), c);
				if(rv != null)
					cycles.add(rv);
			}
		}
		//log.trace("cycles: " + cycles.toString());
		return cycles;
	}

	private ArrayList<DLComponent> checkCycle(ArrayList<DLComponent> path, DLComponent o) {
		seen.add(o);
		ArrayList<DLComponent> newPath = new ArrayList<DLComponent>(path);
		newPath.add(o);
		for(DLComponent p: getProducers(o)) {
			if(newPath.contains(p)) {
				return newPath;
			}
			if(!seen.contains(p)) {
				ArrayList<DLComponent> rv = checkCycle(newPath, p);
				if(rv != null)
					return rv;
			}
		}
		return null;
	}

	private ArrayList<DLComponent> getProducers(DLComponent c) {
		ArrayList<DLComponent> producers = new ArrayList<DLComponent>();

		//don't want conducer members since those are SUPPOSED to be circular
		for(String name: c.listProducers(false)) {
			Producer p = c.getProducer(name);
			if(p.getDatatype().equals(DATATYPE)){
				// Call getOwner ourselves since we want the true owner, not a proxy.
				for(Pair<DLObject, String> consumerPair: p.listConsumers()) {
					DLComponent consumerObject = cm.getOwner(consumerPair.a);
					if(!producers.contains(consumerObject))
						producers.add(consumerObject);
				}
			}
		}

		return producers;
	}

	public ArrayList< ArrayList<DLComponent> > findInletOutletConnections() {
		ArrayList< ArrayList<DLComponent> > cycles = new ArrayList< ArrayList<DLComponent> >();
		for( DLComponent inlet : cm.getComponentsByType("graph_inlet") ){
			DLComponent outlet = checkForOutlet(inlet);
			if ( outlet != null ){
				ArrayList<DLComponent> result = new ArrayList<DLComponent>();
				result.add(inlet);
				result.add(outlet);
				cycles.add(result);
			}
		}
		return cycles;
	}


	private DLComponent checkForOutlet(DLComponent target){
		for(DLComponent p: getProducers(target)) {
			if ( p.getType().equalsIgnoreCase("graph_outlet") ){
				return p;
			}
			//um, if something is attached to itself, just go ahead and return it, because THE WORLD IS TOTALLY BROKEN
			if(p == target){
				return p;
			}
			DLComponent nextresult = checkForOutlet(p);
			if ( nextresult != null ){ return nextresult; }
		}
		return null;
	}

}
