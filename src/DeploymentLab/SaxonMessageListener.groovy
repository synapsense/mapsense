package DeploymentLab

import net.sf.saxon.s9api.MessageListener
import net.sf.saxon.s9api.XdmNode
import javax.xml.transform.SourceLocator

/**
 * Created by IntelliJ IDEA.
 * User: ghelman
 * Date: 1/5/11
 * Time: 11:53 AM
 * To change this template use File | Settings | File Templates.
 */
class SaxonMessageListener  implements MessageListener{
	private List<String> messages

	public SaxonMessageListener(){
		messages =  new ArrayList<String>();
	}

	@Override
	void message(XdmNode xdmNode, boolean b, SourceLocator sourceLocator) {
		messages += xdmNode.getStringValue()
	}


	public ArrayList<String> getAllMessages(){
		return messages
	}

	public String getMessagesAtOnce(){
		StringBuilder sb = new StringBuilder();
		messages.each{ m ->
			sb.append(m)
			sb.append("\n")
		}
		return sb.toString()
	}

}
