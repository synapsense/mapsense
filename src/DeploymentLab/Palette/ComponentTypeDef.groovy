package DeploymentLab.Palette

import javax.swing.Icon
import DeploymentLab.Image.ComponentIconFactory
import javax.swing.JLabel

/**
 * Stores everything the palette needs to know about a given component type.
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/30/12
 * Time: 12:36 PM
 */
class ComponentTypeDef {
	//all are Properties
	String type;
	Set roles;
	String groupPath;
	String displayName;
	String description;
	String shortDesc;
	Icon icon;
	String filters

	Set<String> groupTags
	Set<String> categoryFilterTags

	/**
	 * The "category" is the big filter button that applies to this component.
	 * In the rare cases where we have more than one of those, we just want the first one.
	 * @return the name of the category of this component typedef
	 */
	public String getCategory(){
		if(filters.contains(",")){
			return filters.substring(0, filters.indexOf(",")).trim()
		}else{
			return filters.trim()
		}
	}

	private List<String> gpe = null
	public List<String> getGroupPathElements(){
		if(!gpe){
			gpe = groupPath.split(";").toList()
		}
		return gpe
	}

	private String searchText
	public String getSearchText(){
		if(!searchText){
			searchText = (this.getDisplayName() + this.getGroupPath()).toLowerCase()
		}
		return searchText
	}

	/**
	 * The Short Description is the text shown in the list renderer.  Normally, this is the first sentence of the full description.
	 * (Or the entire full description if the full description does not have a period in it anywhere.)
	 * If this is not desirable for some reason, this can be overridden via a {@code <shortdescription></shortdescription>} tag in the component display block.
	 *
	 * @return
	 */
	public String getShortDescription(){
		//return this.getDescription().substring(0, this.getDescription().indexOf('.') + 1)
		return this.shortDesc
	}

	ComponentTypeDef(def cXml) {
		groupPath = cXml.@grouppath.text()
		filters = cXml.@filters.text()
		type = cXml.@type.text()
		roles = new HashSet<String>(cXml.@classes.text().split(",").toList())
		displayName = cXml.display.name.text()
		description = cXml.display.description.text()
		shortDesc = cXml.display.shortdescription.text()
		if(!shortDesc || !(shortDesc.size() > 0)){
			//fill in from the main desc
			int period = this.getDescription().indexOf('.')
			if ( period > 0){
				shortDesc = this.getDescription().substring(0, this.getDescription().indexOf('.') + 1)
			} else {
				shortDesc = this.getDescription()
			}
		}

		icon = ComponentIconFactory.getShapeIcon(cXml.display.shape.text(), ComponentIconFactory.MEDIUM)

		groupTags = new HashSet<String>()
		for(def g : groupPath.split(";").toList()){
			groupTags.add(g.trim())

		}
		categoryFilterTags = new HashSet<String>()
		for(def f : filters.split(",").toList()){
			categoryFilterTags.add(f.trim())
		}
	}

	@Override
	public String toString() {
		return "ComponentTypeDef{" +
				"type='" + type + '\'' +
				", roles=" + roles +
				", groupPath='" + groupPath + '\'' +
				", displayName='" + displayName + '\'' +
				", description='" + description + '\'' +
				", icon=" + icon +
				", filters='" + filters + '\'' +
				", groupTags=" + groupTags +
				", categoryFilterTags=" + categoryFilterTags +
				'}';
	}

	boolean equals(o) {
		if (this.is(o)) return true
		if (getClass() != o.class) return false

		ComponentTypeDef that = (ComponentTypeDef) o

		if (description != that.description) return false
		if (displayName != that.displayName) return false
		if (groupPath != that.groupPath) return false
		if (icon != that.icon) return false
		if (roles != that.roles) return false
		if (type != that.type) return false

		return true
	}

	int hashCode() {
		int result
		result = (type != null ? type.hashCode() : 0)
		result = 31 * result + (roles != null ? roles.hashCode() : 0)
		result = 31 * result + (groupPath != null ? groupPath.hashCode() : 0)
		result = 31 * result + (displayName != null ? displayName.hashCode() : 0)
		result = 31 * result + (description != null ? description.hashCode() : 0)
		result = 31 * result + (icon != null ? icon.hashCode() : 0)
		return result
	}
}
