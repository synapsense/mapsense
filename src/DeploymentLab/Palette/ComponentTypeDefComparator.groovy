package DeploymentLab.Palette

import java.text.Collator

/**
 * Sorts componenttypedefs first by general category, and then alpha within that.
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/30/12
 * Time: 12:37 PM
 */
class ComponentTypeDefComparator implements Comparator<ComponentTypeDef>{
	private Collator collator = Collator.getInstance();

	//[Power Imaging, General, Environmentals, New for Jupiter2, Control, Calculations, Power & Energy]
	//private static final HashMap<String,String> magicSortOrder = ["General" : "1", "Environmentals":"2", "Power & Energy":"3", "Calculations":"4", "Control":"5", "Power Imaging":"6"]

	private static final HashMap<String,String> magicSortOrder = ["Environmentals":"1", "Power":"1", "Calculations":"3", "Control":"4", "Containment":"5"]

	private static final HashMap<String,String> magicEnvSortOrder = ["Racks" : "1", "CRAC/CRAH": "2", "Pressure" : "3", "Other" : "4"]

	@Override
	int compare(ComponentTypeDef o1, ComponentTypeDef o2) {
		//return compareString(o1.getDisplayName(), o2.getDisplayName())
		def o1first = o1.getCategory()
		def o2first = o2.getCategory()
		if(magicSortOrder.keySet().contains(o1first) ){
			o1first = magicSortOrder[o1first] + o1first
		}
		if(magicSortOrder.keySet().contains(o2first) ){
			o2first = magicSortOrder[o2first] + o2first
		}

		//int col = compareString(o1.getFirstPath(), o2.getFirstPath())
		int col = compareString(o1first, o2first)
		//if (collator == 0){
		if(col == 0){

			def o1last = o1.getGroupPathElements().last()
			String o1Path
			def o2last = o2.getGroupPathElements().last()
			String o2Path
			if(magicEnvSortOrder.keySet().contains(o1last)){
				o1Path = o1.getGroupPath().replace(o1last, "${magicEnvSortOrder[o1last]}${o1last}")
			} else {
				o1Path = o1.getGroupPath()
			}
			if(magicEnvSortOrder.keySet().contains(o2last)){
				o2Path = o2.getGroupPath().replace(o2last, "${magicEnvSortOrder[o2last]}${o2last}")
			} else {
				o2Path = o2.getGroupPath()
			}

			return compareString(o1Path + o1.getDisplayName(), o2Path + o2.getDisplayName())
		} else {
			return col
		}
	}

	private int compareString(String o1, String o2) {
		return collator.compare(o1, o2);
	}

}
