package DeploymentLab.Palette

import DeploymentLab.channellogger.Logger
import groovy.swing.SwingBuilder
import javax.swing.JDialog
import javax.swing.JFrame
import javax.swing.JButton
import javax.swing.JPanel
import DeploymentLab.Dialogs.ChangeParents.ChangeParentsController
import DeploymentLab.Model.DLComponent
import java.awt.List
import DeploymentLab.Model.DLComponentNameComparator
import javax.swing.ButtonGroup
import java.awt.Dimension
import java.awt.Toolkit
import DeploymentLab.CentralCatalogue
import java.awt.BorderLayout
import java.awt.Dialog
import javax.swing.JScrollPane
import net.miginfocom.swing.MigLayout
import DeploymentLab.Dialogs.ScrollablePanel
import DeploymentLab.Image.ComponentIconFactory
import java.awt.FlowLayout
import javax.swing.KeyStroke
import java.awt.event.KeyEvent
import javax.swing.InputMap
import javax.swing.JComponent
import javax.swing.AbstractAction
import java.awt.event.ActionEvent
import javax.swing.JCheckBox

/**
 *
 * @author Gabriel Helman
 * @since
 * Date: 6/18/12
 * Time: 5:22 PM
 */
class PaletteFilterDialog {
	private static final Logger log = Logger.getLogger(PaletteFilterDialog.class.getName())
	/** Internal SwingBuilder used to construct the dialogDelegate.  */
	protected SwingBuilder builder;
	/** All the actual dialog work is delegated to the JDialog class.  */
	protected JDialog dialogDelegate;
	/** The parent frame of this dialog box. */
	protected JFrame parentFrame;

	private JButton okButton
	private JButton closeButton
	private JButton applyButton
	private JPanel mainPanel

	private PaletteController controller;

	private static final int BORDERINSET = 1

	private ArrayList<JCheckBox> checkBoxList = new ArrayList<JCheckBox>()

	/**
	 * Prepare a dialog.
	 * @param parent the JFrame to make the parent of the dialog.
	 * @param delegate The ChangeParentsController that will be running the show.
	 */
	public PaletteFilterDialog(JFrame parent, PaletteController delegate) {
		this.builder = new SwingBuilder()
		parentFrame = parent;
		controller = delegate;
	}

	/**
	 * Construct the dialog.
	 */
	protected void initDialog() {
		HashMap<String, Boolean> data = controller.getFilterData()
		ArrayList<String> sortedKeys = new ArrayList<String>(data.keySet()).sort()


		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		def maxSize = new Dimension()

		int numberOfColumns = Math.ceil(sortedKeys.size() / 20.0)
		if (numberOfColumns > 5) {
			numberOfColumns = 5
		}

		dialogDelegate = builder.dialog(owner: parentFrame, resizable: false, title: "Filter by Tags", layout: new MigLayout(), modalityType: Dialog.ModalityType.APPLICATION_MODAL) {
			scrollPane(constraints: "wrap", horizontalScrollBarPolicy: JScrollPane.HORIZONTAL_SCROLLBAR_NEVER) {
				mainPanel = panel(layout: new MigLayout("fill"), new ScrollablePanel()) {
					int i = 0;
					for (String tag: sortedKeys) {
						def wrap = ""
						if (i == numberOfColumns - 1) {
							wrap = ", wrap"
						}

						def t = tag
						def c = checkBox(selected: data[t], actionPerformed: {controller.filterCheckboxChecked(t)}, label: t, constraints: "$wrap".toString())
						checkBoxList += c
						i++
						if (i == numberOfColumns) {
							i = 0;
						}
					}
				}
			}
			panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: "grow x, wrap") {
				builder.button(text: 'Select All', defaultButton: true, actionPerformed: { this.filterAll() })
				builder.button(text: 'Clear All', actionPerformed: { this.filterNone() })
				okButton = builder.button(text: 'OK', defaultButton: true, actionPerformed: { this.apply(true) })
			}

		}

		//set the size of the main panel
		//use the default "preferred size" unless the height is more than 1/2 the screen height; then clamp it at that
		def totalHeight = mainPanel.getPreferredSize().getHeight()
		def finalHeight = 0
		if (dim.getHeight() / 2 < totalHeight) {
			finalHeight = dim.getHeight() / 2
		} else {
			finalHeight = totalHeight
		}
		def finalWidth = mainPanel.getPreferredSize().getWidth()
		maxSize.setSize(finalWidth, finalHeight)
		mainPanel.setPreferredScrollableViewportSize(maxSize)

		setEscapeKeyMap();
		this.dialogDelegate.pack();
		this.dialogDelegate.setLocationRelativeTo(this.parentFrame);
	}

	/**
	 * Tell the controller to apply changes.
	 * @param closeAfter If true, the dialog will close after the apply (OK), if false, it will stay open (Apply).
	 */
	protected void apply(boolean closeAfter) {
		controller.applyFilterChanges(closeAfter)
	}

	protected void filterAll(){
		for(JCheckBox c : checkBoxList){
			c.setSelected(true)
		}
		controller.filterAll()
	}

	protected void filterNone(){
		for(JCheckBox c : checkBoxList){
			c.setSelected(false)
		}
		controller.filterNone()
	}

	/**
	 * Show the dialog.
	 */
	public void show() {
		if (this.dialogDelegate == null) {
			initDialog()
		}
		this.dialogDelegate.requestFocusInWindow();
		this.dialogDelegate.show();
	}

	/**
	 * Close the dialog.
	 */
	public void close() {
		this.dialogDelegate.setVisible(false);
		//this.dialogDelegate.dispose();
		//this.dialogDelegate = null;
	}

	/**
	 * Jettison the dialog.
	 */
	public void dispose() {
		this.dialogDelegate.dispose();
		this.dialogDelegate = null;
	}

	protected void setEscapeKeyMap() {
		String CANCEL_ACTION_KEY = "CANCEL_ACTION_KEY";
		int noModifiers = 0;
		KeyStroke escapeKey = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, noModifiers, false);
		InputMap inputMap = this.dialogDelegate.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(escapeKey, CANCEL_ACTION_KEY);
		AbstractAction cancelAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		};
		this.dialogDelegate.getRootPane().getActionMap().put(CANCEL_ACTION_KEY, cancelAction);
	}

}

