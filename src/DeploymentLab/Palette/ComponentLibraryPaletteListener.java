package DeploymentLab.Palette;

/**
 * Called by the ComponentLibrary when a library is loaded or unloaded.
 *
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/16/12
 * Time: 12:27 PM
 */
public interface ComponentLibraryPaletteListener {
	public void loadConfiguration(Object cXml);

	public void unloadConfiguration(Object cXml);

}
