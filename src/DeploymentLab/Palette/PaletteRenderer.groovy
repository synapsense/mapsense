package DeploymentLab.Palette

import javax.swing.JLabel
import javax.swing.JList
import javax.swing.ListCellRenderer
import javax.swing.border.EmptyBorder
import net.miginfocom.swing.MigLayout
import java.awt.*

/**
 *
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 2/3/12
 * Time: 9:38 AM
 * @see PaletteController
 */
class PaletteRenderer implements ListCellRenderer {
	private static final int BORDERINSET = 5

	/**
	 * Wrap for tooltips.
	 */
	private static final int WRAP = 50

	/**
	 * Wrap for main display
	 */
	private static final int WRAPDESC = 39//34 //50

	private PaletteController controller

	public PaletteRenderer(PaletteController pc) {
		controller = pc
	}

	private static Map<ComponentTypeDef,FancyPanel> panelCache = new HashMap<ComponentTypeDef, FancyPanel>()

	@Override
	Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		ComponentTypeDef c = (ComponentTypeDef) value;

		if(!panelCache.containsKey(c)){

			def panel = new FancyPanel(new MigLayout())
			def iconLabel = new JLabel(c.getIcon())
			//String wrappedName = c.getDisplayName()
			/*
			if (wrappedName.length() > WRAPDESC) {
				wrappedName = wrappedName.replace(":", ":<br>")
			}
			if (wrappedName.length() > WRAPDESC) {
				wrappedName = wrappedName.replace("(", "<br>(")
			}
			*/
			String wrappedName = wrap(c.getDisplayName(),WRAPDESC)
			def nameLabel = new JLabel("<html><b>$wrappedName</b></html>")
			//def nameLabel = new JLabel(c.getNameLabelContents())


			String shortDesc = c.getShortDescription()

			shortDesc = wrap(shortDesc, WRAPDESC)

			//cut the short desc off at two lines with an "..."
			def lines = shortDesc.split("<br>").toList()
			def result = new StringBuilder()
			for(int i = 0; i < 2; i++){
				if(!lines[i]){break}

				result.append(lines[i])
				if(i < 1){
					result.append("<br>")
				}
			}
			if(lines.size() > 2){
				result.append("...")
			}
			shortDesc = result.toString()

			def shortLabel = new JLabel("<html>$shortDesc</html>")


			//def shortLabel = new JLabel(c.getShortLabelContents())
			panel.add(iconLabel, "span 1 2")
			panel.add(nameLabel, "wrap, top")
			panel.add(shortLabel, "top")


			StringBuilder desc = new StringBuilder()
			desc.append("<html>")
			desc.append("Filters: <b>")
			desc.append(wrap(c.getFilters(), WRAP))
			desc.append("</b><br>")
			desc.append("Tags: <b>")
			desc.append(wrap(c.getGroupPath().replace(";", ", "), WRAP))
			desc.append("</b><br>")
			desc.append(wrap(c.getDescription(), WRAP))
			desc.append("</html>")

			panel.setToolTipText(desc.toString())


			//panel.setToolTipText(c.getToolTipContents())
			panel.setOpaque(true)
			panel.setBorder(new EmptyBorder(BORDERINSET, BORDERINSET, BORDERINSET, BORDERINSET))
			panel.setFont(list.getFont())
			panel.setBackground(Color.WHITE)

			//panel.setSingleShotMode(controller.isInSingleShot(index))

			//panel.setCellSelected(isSelected)

			/*
			if (isSelected) {
				nameLabel.setForeground(list.getSelectionForeground())
				shortLabel.setForeground(list.getSelectionForeground())
			} else {
				nameLabel.setForeground(list.getForeground())
				shortLabel.setForeground(list.getForeground())
			}
			*/
			//return panel
			panelCache[c] = panel
		}

		def p = panelCache[c]
		p.setSingleShotMode(controller.isInSingleShot(index))
		p.setCellSelected(isSelected)
		return 	p

	}

	/**
	 * Insert hard returns in text to have lines at a maximum number of characters.
	 * @param text Source text to wrap
	 * @param len max number of characters per line
	 * @return source text with hard returns added
	 */
	public static String wrap(String text, int len) {
		text = text.trim();
		if (text.length() < len) return text;
		if (text.substring(0, len).contains("<br>")){
			return text.substring(0, text.indexOf("<br>")).trim() + "<br><br>" + wrap(text.substring(text.indexOf("<br>") + 1), len);
		}
		int place = Math.max( Math.max(text.lastIndexOf(" ", len), text.lastIndexOf("\t", len)),
		                      Math.max(text.lastIndexOf("-", len), text.lastIndexOf(",", len)) );
		return text.substring(0, place).trim() + "<br>" + wrap(text.substring(place), len);
	}

}


