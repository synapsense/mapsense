package DeploymentLab.Palette

import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.SelectModel
import DeploymentLab.SelectionChangeEvent
import DeploymentLab.SelectionChangeListener
import DeploymentLab.Tools.ToolManager
import DeploymentLab.UIDisplay
import DeploymentLab.channellogger.Logger
import com.google.common.annotations.VisibleForTesting
import com.google.gag.annotation.disclaimer.IAmAwesome
import com.google.gag.annotation.remark.Booyah
import groovy.swing.SwingBuilder
import net.miginfocom.swing.MigLayout

import java.awt.Insets
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.event.DocumentEvent
import javax.swing.event.DocumentListener
import javax.swing.event.ListSelectionEvent
import javax.swing.event.ListSelectionListener
import javax.swing.*
import java.awt.Dimension
import java.awt.Toolkit
import DeploymentLab.Image.ComponentIconFactory
import DeploymentLab.CentralCatalogue

/**
 * Controller for the new component palette.
 *
 * Should assume all the responsibility of all the old palette stuff:
 * configuration tree
 * configuration panel, etc
 * palette tree models
 *
 * Configured via events fired from the ComponentLibrary, which has now been formalized into the ComponentLibraryPaletteListener interface.
 *
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 2/3/12
 * Time: 9:38 AM
 *
 * @see DeploymentLab.ComponentLibrary
 * @see ComponentLibraryPaletteListener
 */
@IAmAwesome
class PaletteController extends MouseAdapter implements ListSelectionListener, DocumentListener, ActionListener, SelectionChangeListener, ComponentLibraryPaletteListener {
	private static final Logger log = Logger.getLogger(PaletteController.class.getName())

	//teh model
	protected ComponentModel componentModel
	protected SelectModel selectModel
	protected DefaultListModel listModel
	protected List<ComponentTypeDef> allTypes = new LinkedList<ComponentTypeDef>()
	ToolManager toolManager

	//UI Controls
	protected SwingBuilder builder = new SwingBuilder()
	private JPanel paletteContainer
	private JToggleButton projectFilter
	private JToggleButton activeFilter
	private JTextField searchBox
	private JScrollPane scrollPane
	private PaletteList palette
	private PaletteRenderer render
	private PaletteFilterDialog filterTagsDialog;
	private JButton filterTagsButton
	private JLabel filterStatus
	private JButton clearButton

	//data bindings to UI controls
	protected HashMap<String, Boolean> filterTags = new HashMap<String, Boolean>()
	HashMap<String, Boolean> categoryFilterTagsInUse = new HashMap<String, Boolean>()
	protected boolean onlyInProject = false
	protected boolean onlyInActive = false
	protected String searchText = ""



	PaletteController(ComponentModel cm, SelectModel sm) {
		componentModel = cm
		selectModel = sm

		//main display list
		listModel = new DefaultListModel();
		//palette = new JList(listModel)
		//palette = new JXList(listModel)
		palette = new PaletteList(listModel)

		palette.setComparator(new ComponentTypeDefComparator());
		palette.setAutoCreateRowSorter(true);
		palette.setSortOrder(SortOrder.ASCENDING);

		palette.setSelectionMode(ListSelectionModel.SINGLE_SELECTION)
		palette.setVisibleRowCount(20)
		//palette.getSelectionModel().addListSelectionListener(this)
		palette.addMouseListener(this)
		render = new PaletteRenderer(this)
		palette.setCellRenderer(render)

		//filter tools (buttons & search box)
		//projectFilter = new JToggleButton("P")
		projectFilter = new JToggleButton(ComponentIconFactory.getIconByName("componentsUsed24"))

		projectFilter.setActionCommand("projectFilter")
		projectFilter.addActionListener(this)
		projectFilter.setToolTipText(CentralCatalogue.getUIS("componentPalette.filter.p"))

		//activeFilter = new JToggleButton("A")
		activeFilter = new JToggleButton(ComponentIconFactory.getIconByName("componentsAllowed24"))
		activeFilter.setActionCommand("activeFilter")
		activeFilter.addActionListener(this)
		activeFilter.setToolTipText(CentralCatalogue.getUIS("componentPalette.filter.a"))

		//filterTagsButton = builder.button(label: "T", actionPerformed: {this.showDialog(new JFrame())})
		filterTagsButton = builder.button(icon: ComponentIconFactory.getIconByName("componentsKeyword24"), actionPerformed: {this.showDialog(new JFrame())})
		filterTagsButton.setToolTipText(CentralCatalogue.getUIS("componentPalette.filter.t"))

		searchBox = new JTextField("")
		searchBox.getDocument().addDocumentListener(this)
		searchBox.setColumns(10)
		///Border rounded = new LineBorder(new Color(210,210,210), 1, true);
		//Border empty = new EmptyBorder(0, 3, 0, 0);
		//searchBox.setBorder(rounded);
		//Border border = new CompoundBorder(rounded, empty);
		//searchBox.setMargin(new Insets(0, 10, 0, 0))
		searchBox.setMargin(new Insets(1, 1, 1, 1))

		clearButton = builder.button(text: "x", actionPerformed: {this.clearButtonAction()})
		clearButton.setToolTipText(CentralCatalogue.getUIS("componentPalette.filter.clear"))

		filterStatus = new JLabel()
		filterStatus.setHorizontalAlignment(JLabel.CENTER)

		paletteContainer = new JPanel(new MigLayout("ins 0"))
		//def toolArea = new JPanel(new MigLayout())

//		paletteContainer.add(projectFilter, "gapafter unrel")
//		paletteContainer.add(activeFilter, "gapafter unrel")
//		paletteContainer.add(filterTagsButton, "grow x, wrap")
//		paletteContainer.add(searchBox, "span 3, grow x, wrap")

		paletteContainer.add(projectFilter, "gapafter rel")
		paletteContainer.add(activeFilter, "gapafter rel")
		paletteContainer.add(filterTagsButton, "gapafter rel")
		//paletteContainer.add(searchBox, "grow x, wrap")
		paletteContainer.add(searchBox, "grow")
		paletteContainer.add(clearButton, "wrap")

		scrollPane = new JScrollPane(palette)
		//scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER)
		paletteContainer.add(scrollPane, "grow x, grow y, span 5, wrap")

		paletteContainer.add(filterStatus, "grow, span 5, wrap")

		//paletteContainer.setMaximumSize(palette.getPreferredScrollableViewportSize())
		//paletteContainer.setMinimumSize(palette.getPreferredScrollableViewportSize())

		//UIDisplay.INSTANCE.addUIChangeListener(searchBox, 'addComponent')
		UIDisplay.INSTANCE.addUIChangeListener(projectFilter, 'addComponent')
		UIDisplay.INSTANCE.addUIChangeListener(activeFilter, 'addComponent')
		//UIDisplay.INSTANCE.addUIChangeListener(filterTagsButton, 'addComponent')
		//UIDisplay.INSTANCE.addUIChangeListener(palette, 'addComponent')


		//Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		/*
		def maxSize = new Dimension()
		//set the size of the main panel
		//use the default "preferred size" unless the height is more than 1/2 the screen height; then clamp it at that
		def totalHeight = mainPanel.getPreferredSize().getHeight()
		def finalHeight = 0
		if (dim.getHeight() / 2 < totalHeight) {
			finalHeight = dim.getHeight() / 2
		} else {
			finalHeight = totalHeight
		}
		def finalWidth = mainPanel.getPreferredSize().getWidth()
		maxSize.setSize(finalWidth, finalHeight)
		mainPanel.setPreferredScrollableViewportSize(maxSize)
		*/
		//paletteContainer.setPreferredSize(new Dimension(250,dim.getHeight().toInteger()))

		//paletteContainer.setPreferredSize(new Dimension(paletteContainer.getWidth(),dim.getHeight().toInteger()))

	}

	/**
	 * Returns the UI of the palette, ready for display in a larger swing interface.
	 * @return
	 */
	public JComponent getPalette() {
		return paletteContainer
	}

	@VisibleForTesting
	protected PaletteList getPaletteList() {
		return palette
	}

	public String getCurrentlySelectedType(){
		String result
		if(palette.getSelectedIndex() > 0){
			result = ((ComponentTypeDef)palette.getSelectedValue()).getType()
		} else {
			result = null
		}
		return result
	}

	public void clearSelection() {
		palette.getSelectionModel().clearSelection()
	}

	public void loadConfiguration(def cXml) {
		def c = new ComponentTypeDef(cXml)

		if (!c.getRoles().contains("placeable") || c.getGroupPath().size() == 0) {
			log.debug("skipping ${c.getType()}")
			return
		}

		allTypes.add(c)
		listModel.addElement(c)

		for (def t : c.getGroupTags()) {
			filterTags.put(t, true)
		}

		for (def t : c.getCategoryFilterTags()) {
			categoryFilterTagsInUse.put(t, true)
		}

		this.doSearch()
	}

	public void unloadConfiguration(def cXml) {
		def c = new ComponentTypeDef(cXml)
		if (!c.getRoles().contains("placeable") || c.getGroupPath().size() == 0) {
			log.debug("skipping ${c.getType()}")
			return
		}

		allTypes.remove(c)
		listModel.removeElement(c)

		//leave the tags for the moment?
		this.doSearch()
	}


	private int currentIndex = 0
	private boolean currentlySingleShot = true

	public boolean isInSingleShot(int index) {
		if (index == currentIndex) {
			return currentlySingleShot
		} else {
			return true
		}
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		// Ignore if there isn't an active drawing, typically when there is no project loaded.
		if( selectModel.getActiveDrawing() == null ) return

		JList list = (JList) evt.getSource();
		currentIndex = list.locationToIndex(evt.getPoint());
		ComponentTypeDef c = (ComponentTypeDef) palette.getElementAt(currentIndex)
		currentlySingleShot = this.typeSelected(c, evt.getClickCount())
		list.repaint()
	}

	@Override
	public void mouseClicked(MouseEvent evt) {
		// Ignore if there isn't an active drawing, typically when there is no project loaded.
		if( selectModel.getActiveDrawing() == null ) return

		JList list = (JList) evt.getSource();
		currentIndex = list.locationToIndex(evt.getPoint());
		ComponentTypeDef c = (ComponentTypeDef) palette.getElementAt(currentIndex)
		currentlySingleShot = this.typeSelected(c, evt.getClickCount())
		list.repaint()
	}


	@Override
	void valueChanged(ListSelectionEvent e) {
		//not being used, since we really want to trip off of the mouseClicked event to get the click count
/*
		if (!e.getValueIsAdjusting()) {
			//log.info("source is ${e.getSource()}")
			//log.info("You just clicked ${e.getFirstIndex()} ${e.getLastIndex()}")
			//log.info(e.getSource().toString())

			//ComponentTypeDef c = (ComponentTypeDef) listModel.getElementAt(palette.getSelectionModel().getMinSelectionIndex())
			ComponentTypeDef c = (ComponentTypeDef) palette.getElementAt(palette.getSelectionModel().getMinSelectionIndex())
			//log.info("which is $c")
			this.typeSelected(c)
		}
*/
	}


	public boolean typeSelected(ComponentTypeDef selected, int clickCount = 1) {
		//log.debug("clicks to typeSelected = $clickCount")
/*
		int longest = 1
		int sum = 0
		def lengths = []
		allTypes.each{
			if(it.getDisplayName().size() > longest){
				longest = it.getDisplayName().size()
			}
			sum += it.getDisplayName().size()
			lengths += it.getDisplayName().size()
		}
		println "longest name is $longest"
		int avg = sum / allTypes.size()
		println "avg is $avg"
		println "mode is ${mode(lengths)}"
		println "median is ${median(lengths)}"
*/
		boolean toolSupportsSingleShotMode = false
		if ( selected.roles.contains('inspector') || selected.roles.contains('controlleddevice') || selected.roles.contains('secondary') || selected.roles.contains('controlstrategy')) {
			toolManager.getTool('controlinputadder').type = selected.type
			toolManager.getTool('controlinputadder').roles = new ArrayList<>(selected.roles)
			toolManager.setActiveTools(['hover', 'panzoomer', 'controlinputadder', 'keycommands', 'popupmenu', 'fasternamer', 'currentlocation'])
			toolSupportsSingleShotMode = toolManager.canSingleShot('controlinputadder')

		} else if (selected.roles.contains('dynamicchild')) {
			toolManager.getTool('dynamicchildadder').type = selected.type
			toolManager.setActiveTools(['hover', 'panzoomer', 'dynamicchildadder', 'keycommands', 'popupmenu', 'fasternamer', 'currentlocation'])
			toolSupportsSingleShotMode = toolManager.canSingleShot('controlinputadder')

		} else if (selected.roles.contains('arbitraryshaped')) {
			toolManager.getTool('arbitraryshaper').type = selected.type
			toolManager.setActiveTools(['arbitraryshaper', 'hover', 'panzoomer', 'keycommands', 'popupmenu', 'fasternamer', 'currentlocation'])
			toolSupportsSingleShotMode = toolManager.canSingleShot('dynamicchildadder')

		} else if (selected.roles.contains('staticchildplaceable')) {
			toolManager.getTool('staticchildrennodeadder').type = selected.type
			toolManager.setActiveTools(['staticchildrennodeadder', 'hover', 'panzoomer', 'keycommands', 'popupmenu', 'fasternamer', 'currentlocation'])
			toolSupportsSingleShotMode = toolManager.canSingleShot('staticchildrennodeadder')

		} else {
			toolManager.getTool('adder').type = selected.type
			toolManager.setActiveTools(['hover', 'panzoomer', 'adder', 'keycommands', 'popupmenu', 'fasternamer', 'currentlocation'])
			toolSupportsSingleShotMode = toolManager.canSingleShot('adder')
		}

		boolean singleShotMode
		if (clickCount == 1 && toolSupportsSingleShotMode) {
			singleShotMode = true
		} else {
			singleShotMode = false
		}
		toolManager.setSingleShotMode(singleShotMode)
		//log.info("SingleShot: $singleShotMode", "statusbar")
		return singleShotMode
	}

	@Override
	void insertUpdate(DocumentEvent e) {
		searchText = searchBox.getText().trim()
		this.doSearch()
	}

	@Override
	void removeUpdate(DocumentEvent e) {
		searchText = searchBox.getText().trim()
		this.doSearch()
	}

	@Override
	void changedUpdate(DocumentEvent e) {
		searchText = searchBox.getText().trim()
		this.doSearch()
	}

	/**
	 * Update the listModel based on the current filter criteria.
	 *
	 * There are three, which act to filter the list in this order:
	 *
	 * Limit by only what's in the current project
	 * Limit by what's possible in the current active grouping/DS
	 * Limit by the current view filters
	 * Limit by a selected palette group path
	 * Limit by the search box text
	 *
	 */
	@Booyah
	public void doSearch(DLComponent activeDrawing = null, DLComponent activeZone = null, DLComponent activeNet = null) {
		if (!activeDrawing) {
			activeDrawing = selectModel.getActiveDrawing()
		}
		if (!activeZone) {
			activeZone = selectModel.getActiveZone()
		}
		if (!activeNet) {
			activeNet = selectModel.getActiveNetwork()
		}

		//log.trace("searching: '$searchText', PF=${onlyInProject} AF=${onlyInActive} ")
		//log.trace("Active Drawing: $activeDrawing Active Zone: $activeZone Active Net: $activeNet")

		if (searchText.toLowerCase() == "do a barrel roll") {
			log.trace("Whaddya think this is, Google?", "statusbar")
		}

		Set<String> projectTypes = null
		if (onlyInProject) {
			projectTypes = new HashSet<String>()
			//filter by current project before anything else
			for (DLComponent c : componentModel.getComponents()) {
				projectTypes.add(c.getType())
			}
		}

		List<ComponentTypeDef> results = []
		for (ComponentTypeDef c : allTypes) {
			boolean passing = true
			//filter 1: in the project or not:
			if (projectTypes) {
				if (projectTypes.contains(c.getType())) {
					passing = true
				} else {
					passing = false
				}
			} else {
				passing = true
			}

			//filter 1.5: whats possible in the active group/DS
			if (onlyInActive && passing) {
				boolean matchesZone = false
				boolean matchesNet = false
				boolean hasZone = false
				boolean hasNet = false
				boolean childOfDrawing = false

				if (activeDrawing != null && componentModel.typeCanBeChild(activeDrawing.getType(), c.getType())) {
					childOfDrawing = true
				}

				if (activeZone != null) {
					if (componentModel.listGroupings(c.getType()).size() > 0) {
						hasZone = true
						if (!componentModel.typeCanBeChild(activeZone.getType(), c.getType())) {
							matchesZone = false
						} else {
							matchesZone = true
						}
					} else {
						hasZone = false
					}
				}

				if (activeNet != null) {
					if (componentModel.listDatasources(c.getType()).size() > 0) {
						hasNet = true
						if (!componentModel.typeCanBeChild(activeNet.getType(), c.getType(), true)) {
							matchesNet = false
						} else {
							matchesNet = true
						}
					} else {
						hasNet = false
					}
				}


				//always show drawing child stuff no matter what
				if (childOfDrawing) {
					passing = true
				}

				if (hasNet && !matchesNet) {
					//log.trace("${c.getType()} fails at (hasNet && !matchesNet) ==>  ($hasNet && !$matchesNet)")
					passing = false
				}
/*
				if(!hasZone){
					log.trace("${c.getType()} fails at (!hasZone=> $hasZone)")
					passing = false
				}
*/


				if (hasZone && !matchesZone) {
					//log.trace("${c.getType()} fails at (hasZone && !matchesZone) ==>($hasZone && !$matchesZone)")
					passing = false
				}


				if (hasZone && matchesZone) {
					if (hasNet && !matchesNet) {
						//log.trace("${c.getType()} fails at (hasZone && matchesZone) && (hasNet && !matchesNet ) ==> ($hasZone && !$matchesZone)  && ($hasNet && !$matchesNet )")
						passing = false
					}
				}


			}

			//filter2a: the category filter tags (same as big view filters)
			if (passing) {
				def good = false
				for (def f : c.getCategoryFilterTags()) {
					//log.trace("filtering $c on filter '$f'")

					if (this.getCategoryFilterTagsInUse()[f]) {
						good = true
					}
				}
				if (!good) {
					passing = false
				}
			}

			//filter 2b: the grouppath filter tags
			if (passing) {
				def currentTags = new HashSet<String>()
				currentTags.addAll(c.getGroupPath().split(";"))
				def good = false
				for (def t : currentTags) {
					if (this.getFilterData()[t]) {
						good = true
					}
				}
				if (!good) {
					passing = false
				}
			}

			//filter 3: search text
			if (passing) {
				if (searchText && searchText.size() > 0) {
					//String allText = c.getDisplayName() + " " + c.getDescription() + c.getGroupPath()
					//passing = searchCompare(searchText.toLowerCase(), allText.toLowerCase())
					passing = searchCompare(searchText.toLowerCase(), c.getSearchText())
				}
			}
			if (passing) {
				results.add(c)
			}
		}

		listModel.clear()
		for (def c : results) {
			listModel.addElement(c)
		}

		//log.trace("total passed = ${results.size()}")
		if (results.size() == allTypes.size()) {
			filterStatus.text = "Showing all ${allTypes.size()} Components"
		} else {
			filterStatus.text = "Showing ${results.size()} of ${allTypes.size()} Components"
		}

		//log.trace("${palette.getPreferredScrollableViewportSize()}")
		//log.trace("row count = ${palette.getVisibleRowCount()}")

		//paletteContainer.setMaximumSize(palette.getPreferredScrollableViewportSize())
		//paletteContainer.setMinimumSize(palette.getPreferredScrollableViewportSize())
		//palette.revalidate()
		paletteContainer.revalidate()
		paletteContainer.repaint()
	}


	/**
	 * Handles the search comparison when searching across properties.
	 * Simply, this searches for the presence of searchingFor in searchingIn.
	 * However, rather than doing a simple .contains() call, searchingFor is tokenized on whitespace,
	 * and each token is searched for in searchingIn separately.  A true result is returned if all tokens are present
	 * in searchingIn somewhere, but not necessarily contiguously or in order.
	 * @param searchingFor The search term to look for.  Tokenized on whitespace.
	 * @param searchingIn The string to look inside of.
	 * @return true if searchingIn contains all the tokens in searchingFor
	 */
	private boolean searchCompare( String searchingFor, String searchingIn ){
		boolean result = true //in case things really go haywire, we want true to come out
		int hits = 0
		//now, lets try that with some tokenizing
		def searches = searchingFor.tokenize()
		for ( String t : searches ){
			if ( searchingIn.contains( t ) ){
				hits ++
			}
		}
		//note: we want true for 0==0 here
		if ( hits == searches.size() ){
			result = true
		} else {
			result = false
		}
		return ( result )
	}

	@Override
	void actionPerformed(ActionEvent e) {
		onlyInProject = projectFilter.isSelected()
		onlyInActive = activeFilter.isSelected()
		this.doSearch()
	}

	void clearButtonAction(){
		this.searchBox.setText("")
		this.projectFilter.setSelected(false)
		this.activeFilter.setSelected(false)
		onlyInProject = projectFilter.isSelected()
		onlyInActive = activeFilter.isSelected()
		this.filterAll()
//		this.doSearch()
	}

	public HashMap<String, Boolean> getFilterData() {
		return filterTags
	}

	public void applyFilterChanges(boolean closeAfter) {
		this.doSearch()
		if (closeAfter) {
			this.finishUpFilter()
		}
	}

	public void cancelFilterChanges() {
		this.finishUpFilter()
	}

	public void filterCheckboxChecked(String key) {
		this.getFilterData()[key] = !this.getFilterData()[key]
		this.applyFilterChanges(false)
	}

	private void finishUpFilter() {
		//clean up before we bail out of here.
		if (filterTagsDialog) {
			filterTagsDialog.close()
			filterTagsDialog.dispose()
			filterTagsDialog = null
		}
	}

	protected PaletteFilterDialog getFilterDialog(JFrame parentFrame) {
		if (!filterTagsDialog) {
			this.filterTagsDialog = new PaletteFilterDialog(parentFrame, this)
		}
		return filterTagsDialog
	}

	public showDialog(JFrame parentFrame) {
		this.getFilterDialog(parentFrame).show()
	}

	public void toggleDisplayGroupFilter(String type) {
		this.categoryFilterTagsInUse[type] = !categoryFilterTagsInUse[type]
		this.applyFilterChanges(false)
	}

	public void filterAll() {
		for (def t : filterTags.keySet()) {
			filterTags[t] = true
		}
		this.applyFilterChanges(false)
	}

	public void filterNone() {
		for (def t : filterTags.keySet()) {
			filterTags[t] = false
		}
		this.applyFilterChanges(false)
	}

	@Override
	public void selectionChanged(SelectionChangeEvent e) {}

	@Override
	public void activeDrawingChanged(DLComponent oldDrawing, DLComponent newDrawing) {
		//log.trace("adc changed to $newDrawing")
		if (onlyInActive || onlyInProject) {
			log.trace("Is EDT?: ${SwingUtilities.isEventDispatchThread()}")
			if (!SwingUtilities.isEventDispatchThread()){
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					void run() {
						log.trace("Invoke Later from thread: ${Thread.currentThread()}")
						doSearch(newDrawing, selectModel.getActiveZone(), selectModel.getActiveNetwork())
					}
				})
			} else {
				this.doSearch(newDrawing, selectModel.getActiveZone(), selectModel.getActiveNetwork())
			}
		}
	}

	public void activeZoneChanged(DLComponent oldZone, DLComponent newZone) {
		if (onlyInActive) {
			this.doSearch(selectModel.getActiveDrawing(), newZone, selectModel.getActiveNetwork())
		}
	}

	public void activeDatasourceChanged(DLComponent oldDatasource, DLComponent newDatasource) {
		if (onlyInActive) {
			this.doSearch(selectModel.getActiveDrawing(), selectModel.getActiveZone(), newDatasource)
		}
	}

/*
	public static int mode(List<? extends Number> a) {
		int maxValue, maxCount;

		for (int i = 0; i < a.size(); ++i) {
			int count = 0;
			for (int j = 0; j < a.size(); ++j) {
				if (a[j] == a[i]) ++count;
			}
			if (count > maxCount) {
				maxCount = count;
				maxValue = a[i];
			}
		}

		return maxValue;
	}

	// the array double[] m MUST BE SORTED
	public static double median(List<? extends Number> m) {
		m.sort()
		int middle = m.size()/2;
		if (m.size()%2 == 1) {
			return m[middle];
		} else {
			return (m[middle-1] + m[middle]) / 2.0;
		}
	}
*/

}

