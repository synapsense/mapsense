package DeploymentLab.Palette;

import org.jdesktop.swingx.JXList;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Extends JXList to do tooltips right
 *
 * @author Gabriel Helman
 * @since Date: 7/16/12
 *        Time: 3:33 PM
 */
class PaletteList<E> extends JXList {

	PaletteList(ListModel dataModel) {
		super(dataModel);
	}

	public String getToolTipText(MouseEvent event) {
		if (event != null) {
			//System.out.println("Event is " + event);
			Point p = event.getPoint();
			int index = locationToIndex(p);
			ListCellRenderer<? super E> r = getCellRenderer();
			Rectangle cellBounds;
			//System.out.println("index is " + index );
			//System.out.println("r is " + r.toString() );
			//System.out.println("cellbounds is " + getCellBounds(index, index) );
			//System.out.println("cellbounds contains = " + getCellBounds(index, index).contains(p.x, p.y) );
			if (index != -1 && r != null && (cellBounds = getCellBounds(index, index)) != null && cellBounds.contains(p.x, p.y)) {
				//System.out.println("made it to if");
				ListSelectionModel lsm = getSelectionModel();
				Component rComponent = r.getListCellRendererComponent(
						//this, getModel().getElementAt(index), index,
						this, (E) this.getElementAt(index), index,
						lsm.isSelectedIndex(index),
						(hasFocus() && (lsm.getLeadSelectionIndex() ==
								index)));

				if (rComponent instanceof JComponent) {
					MouseEvent newEvent;
					p.translate(-cellBounds.x, -cellBounds.y);
					newEvent = new MouseEvent(rComponent, event.getID(),
							event.getWhen(),
							event.getModifiers(),
							p.x, p.y,
							event.getXOnScreen(),
							event.getYOnScreen(),
							event.getClickCount(),
							event.isPopupTrigger(),
							MouseEvent.NOBUTTON);

					String tip = ((JComponent) rComponent).getToolTipText(newEvent);
					if (tip != null) {
						return tip;
					}
				}
			}
		}
		//System.out.println("defaulting to super");
		return super.getToolTipText();
	}
}