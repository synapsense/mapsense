package DeploymentLab.Palette

import javax.swing.JPanel
import java.awt.LayoutManager
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.Color
import javax.swing.JList
import java.awt.BasicStroke

/**
 * Handles the round-rect background of each entry in the palette list.
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/30/12
 * Time: 12:38 PM
 */
public class FancyPanel extends JPanel {

	boolean cellSelected
	boolean singleShotMode = true

	FancyPanel(LayoutManager layout) {
		super(layout)
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g
		super.paintComponent(g2)

		Color rrColor
		if (cellSelected) {
			//if (!singleShotMode){
			//	rrColor = Color.RED
			//} else {
			rrColor = new JList().getSelectionBackground()
			//}

		} else {
			//rrColor = new JList().getBackground()
			//rrColor = Color.LIGHT_GRAY
			//rrColor = new Color(0xf5f5f5)
			//rrColor = new Color(0xC0C0C0)
			rrColor = new Color(0xE0E0E0)
			//rrColor = new Color(0xF0F0F0)
		}
		g2.setColor(rrColor)

		//g.setColor(this.getBackground())
		g2.fillRoundRect(2, 2, (this.getWidth() - 4), (this.getHeight() - 4), 10, 10)

		//add a border for selected cells
		if (cellSelected) {
			g2.setStroke(new BasicStroke(2.0f))
			if (singleShotMode) {
				g2.setColor(Color.BLACK)
			} else {
				g2.setColor(Color.RED)
			}

			g2.drawRoundRect(2, 2, (this.getWidth() - 4), (this.getHeight() - 4), 10, 10)
		}

	}
}
