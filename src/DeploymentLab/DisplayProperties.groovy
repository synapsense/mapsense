package DeploymentLab

import java.awt.Color
import DeploymentLab.channellogger.*
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.ModelChangeListener
import DeploymentLab.Model.MetamorphosisEvent
import DeploymentLab.Model.ModelState

public class DisplayProperties implements SelectionChangeListener, ModelChangeListener {
	private static final Logger log = Logger.getLogger(DisplayProperties.class.getName());

	private SelectModel selectModel
    private _properties = [:]
	private propertyChangeListeners = [:]
	private String nameType
	private invisibleObjects = []
	private invisibleHalos = []
	private invisibleAssociations = []

	public DisplayProperties( SelectModel selectModel ){
		this.selectModel = selectModel

		_properties['halo'] = true
		_properties['orientation'] = true
		_properties['backgroundImg'] = true
		_properties['name'] = false
		_properties['associationDetail'] = true
		_properties['invisibleObjects'] = invisibleObjects
		_properties['invisibleHalos'] = invisibleHalos
		_properties['invisibleAssociations'] = invisibleAssociations
		_properties['augmentHover'] = "CLOSE"
		_properties['floorTilesView'] = true
    	setNameType('Name')
 	}
	
	public void reset(){
		_properties['halo'] = true
		_properties['orientation'] = true
		_properties['backgroundImg'] = true
		_properties['name'] = false
		_properties['''associationDetail'''] = true
		_properties['invisibleObjects'] = invisibleObjects = []
		_properties['invisibleHalos'] = invisibleHalos = []
		_properties['invisibleAssociations'] = invisibleAssociations = []
		_properties['augmentHover'] = "CLOSE"
		_properties['floorTilesView'] = true
 		setNameType('Name')
	}
	
	public void setProperty(String pName, Object pValue){
		_properties[pName] = pValue
	}
	
	public setNameType(String _nameType){
		nameType = _nameType
	}
	
	public String getNameType(){
		return nameType
	}
	
	public Object getProperty(String pName){
		return _properties[pName]
	}

	void addPropertyChangeListener(def who, Closure c) {
    	propertyChangeListeners[who] = c
	}

	void removePropertyChangeListener(def who) {
		propertyChangeListeners.remove(who)
	}
	
	public void firePropertyChanged() {
       // log.debug("selectModel active drawing:" + selectModel.getActiveDrawing())
    	propertyChangeListeners.each{ who, listener ->
			try {
    			listener.call(this)
    		} catch(Exception e) {
				log.error("Exception delivering displayproperty change event:")
                log.trace( log.getStackTrace(e) )
    		}
		}
	}

	def String toString() {
		return _properties.toString()
	}

    void selectionChanged(SelectionChangeEvent e){}

    @Override
    void activeDrawingChanged( DLComponent oldDrawing, DLComponent newDrawing ){
	    // todo: Refactor to not need to do this or need isActiveDrawingComponent()
        firePropertyChanged()
    }

    public boolean isActiveDrawingComponent(DLComponent component){
	    return component.getDrawing().equals( selectModel.getActiveDrawing() )
    }

	@Override
	void modelMetamorphosisStarting( MetamorphosisEvent event ) {
		if( event.getMode() == ModelState.INITIALIZING ) {
			reset()
		}
	}

	@Override
	void modelMetamorphosisFinished( MetamorphosisEvent event ) {
		if( event.getMode() == ModelState.CLEARING ) {
			reset()
		}
	}

	@Override
	void componentAdded( DLComponent component ) {}

	@Override
	void componentRemoved( DLComponent component ) {}

	@Override
	void childAdded( DLComponent parent, DLComponent child ) {}

	@Override
	void childRemoved( DLComponent parent, DLComponent child ) {}

	@Override
	void associationAdded( DLComponent producer, String producerId, DLComponent consumer, String consumerId ) {}

	@Override
	void associationRemoved( DLComponent producer, String producerId, DLComponent consumer, String consumerId ) {}
}
