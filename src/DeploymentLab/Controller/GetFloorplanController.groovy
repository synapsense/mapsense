package DeploymentLab.Controller

import DeploymentLab.Dialogs.ServerDialog
import DeploymentLab.Dialogs.SmartZoneLoginDialog
import com.panduit.sz.api.ss.assets.Floorplan

import javax.swing.JFrame

/**
 * Created by CCR on 1/8/2016.
 */
class GetFloorplanController extends SmartZoneBaseController {

    private int locationId;
    private Floorplan floorplan;

    public GetFloorplanController(JFrame parent,
                                  SmartZoneLoginDialog smartZoneLoginDialog,
                                  ServerDialog smartZoneServerDialog,
                                  int locationId) {
        super(parent,smartZoneLoginDialog,smartZoneServerDialog);
        this.locationId = locationId;
    }

    @Override
    void smartZoneCall() {
        floorplan = smartZoneClient.getFloorplan(locationId);
    }

    public Floorplan getFloorplan() {
        return floorplan;
    }
}
