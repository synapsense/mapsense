package DeploymentLab.Controller

import DeploymentLab.CentralCatalogue
import DeploymentLab.Dialogs.ServerDialog
import DeploymentLab.Dialogs.SmartZoneLoginDialog
import DeploymentLab.ProgressManager
import DeploymentLab.SmartZone.SmartZoneClient
import DeploymentLab.channellogger.Logger
import com.panduit.sz.shared.AuthenticationException
import com.panduit.sz.shared.NotFoundException
import com.panduit.sz.shared.SmartZoneNonTransientException
import com.panduit.sz.shared.SmartZoneTransientException

import javax.swing.*

/**
 * This base controller persistently manages connectivity of a SmartZoneClient session prior to making the network
 * calls. Classes that extend this controller implement the abstract method smartZoneCall using smartZoneClient methods.
 *
 * Prior to the network call this class will collect missing host, port, username, or password connection paramaters by
 * examining host and port information stored in the system preferences (on disk) and username and password data stored in
 * memory.
 *
 * If the authentication or connection fails the user will be returned to the appropriate dialog to allow them to
 * change the information or retry the connection.
 *
 * If a previous connection has been made it will be reused unless a different URL (host and port) is set, using the
 * setURL method, prior to calling start. The setURL method is intended to be used to support project level
 * connection parameters. If the setURL method passes in URL data that is different from the system parameters then
 * the project parameter will be used, but not saved back to the system preferences. If the user has to change the URL
 * through the server dialog then the urlChanged property is set, allowing controller extension to detect the change and
 * save the user entered URL data back to the project.
 *
 * The start method returns true if the user does not cancel an attempt to collect connection parameters.
 *
 * SmartZoneClient methods that reference an id can result in a NotFoundException if the id they submitted is not found
 * on the SmartZone server. The status can be checked through the controller's getNotFound method.
 *
 *
 * Created by CCR on 1/7/2016.
 */
abstract class SmartZoneBaseController {
    private SmartZoneLoginDialog smartZoneLoginDialog;
    private ServerDialog smartZoneServerDialog;
    private JFrame parentFrame;
    private boolean operationCancelled = false;
    private boolean notFound = false;
    private String host;
    private int port;
    private boolean urlChangedByUser = false;
    private String hostToUse;
    private int portToUse;
    private boolean reuseSession;

    private static final Logger log = Logger.getLogger(SmartZoneBaseController.class.getName())

    // Single instance of the currently active session that is used by all.
    protected static SmartZoneClient smartZoneClient

    public SmartZoneBaseController(JFrame parentFrame,
                                   SmartZoneLoginDialog smartZoneLoginDialog,
                                   ServerDialog smartZoneServerDialog) {
        this.parentFrame = parentFrame;
        this.smartZoneLoginDialog = smartZoneLoginDialog;
        this.smartZoneServerDialog = smartZoneServerDialog
    }

    /**
     * This method is optional and should only be used if a project level URL is to be used instead of a system level
     * URL.
     *
     * This method should be called prior to starting the network operation.
     * If this URL is different than the system preferences URL, then it will be used to make this connection.
     * If it is the same then the setting it will have no noticeable effect.
     *
     * Setting the URL here will not cause the system preferences to be saved.
     *
     * @param host
     * @param port
     */
    void setURL(String host, int port) {

        if (host == null || port == null) {
            throw new RuntimeException("Null host or port parameter.");
        }

        this.host = host;
        this.port = port;
    }

    /**
     * If the user changed the URL during this controllers network operation then with will return true, indicating that
     * it should be saved back the project file.
     * @return
     */
    boolean getURLChangedByUser() {
        return urlChangedByUser;
    }

    /**
     * This method will repeatedly display dialogs to collect connection parameters prior to making the extended class's
     * network work call.
     *
     * The extending class is responsible for collected any returned result from their network call.
     *
     * @return boolean true if the operation was not cancelled by the user.
     */
    boolean start() {
        boolean done = false;
        while (!done) {
            // if they hit cancel bail out,
            // if they ever get an error then show Retry instead of OK.
            if (getSmartZoneSession()) {
                try {
                    smartZoneCall()
                    done = true;
                } catch (AuthenticationException ae) {
                    // set error msg in login and start over
                    smartZoneLoginDialog.setErrorMsg(CentralCatalogue.getUIS("locations.auth_failure"))
                    smartZoneClient = null
                } catch (SmartZoneTransientException szte) {
                    // set error in host port dialog and start over
                    smartZoneServerDialog.setErrorMsg(CentralCatalogue.getUIS("locations.server_contact_failure"))
                    log.error(log.getStackTrace(szte))
                    smartZoneClient = null
                } catch (NotFoundException notFoundException) {
                    log.error(notFoundException.getMessage())
                    notFound = true;
                    break
                } catch (SmartZoneNonTransientException sznte) {
                    log.error(sznte.getMessage(), "message")
                    log.error(log.getStackTrace(sznte))
                    smartZoneClient = null
                    operationCancelled = true
                    break
                }
            } else {
                done = true;
                operationCancelled = true;
            }
        }
        return !operationCancelled;
    }

    public boolean getNotFound() {
        return notFound;
    }

    // This method should be overridden to both perform the network call and collect any results.
    abstract void smartZoneCall();

    private boolean getSmartZoneSession() {
        // This means it is not linked yet (Advanced Editor mode)
        if(!isHostPortPairNullOrEmpty()) {
            // Has a system settings ever been collected or is a different project setting being used
           if (!isProjectSettingSame()) {
               smartZoneServerDialog.setErrorMsg(CentralCatalogue.getUIS("locations.server_contact_failure"))
               smartZoneClient = null
          } else {
               hostToUse = CentralCatalogue.getSystemSettings().getSmartZoneHost();
               portToUse = CentralCatalogue.getSystemSettings().getSmartZonePort();
           }
        }

        if (smartZoneClient == null) {
            if (collectHostAndPort()) {
                if (collectUserAndPassword()) {
                    smartZoneClient = new SmartZoneClient(  CentralCatalogue.getSystemSettings().getSmartZoneHost(),
                            CentralCatalogue.getSystemSettings().getSmartZonePort(),
                            (String) CentralCatalogue.getSmartZoneConnecionProperties().get("username"),
                            (String) CentralCatalogue.getSmartZoneConnecionProperties().get("password") )
                }
            }
        } else {
            // always create a new client if the project is trying to override the system setting
            // otherwise try to reuse the session
            if (!isHostPortPairNullOrEmpty()) {
                reuseSession = true
                if (collectHostAndPort()) {
                    if (collectUserAndPassword() && !reuseSession) {
                        smartZoneClient = new SmartZoneClient(hostToUse,
                                portToUse,
                                (String) CentralCatalogue.getSmartZoneConnecionProperties().get("username"),
                                (String) CentralCatalogue.getSmartZoneConnecionProperties().get("password"))
                    }
                }
            }
        }

        // if the connection was successful
        if (smartZoneClient != null) {

            // And a project URL was set
            if (host != null) {

                // And the project values were different then the system values,
                // then the user changed while connecting.
                if ("" != smartZoneServerDialog.getHost()) {
                    urlChangedByUser = true; // so it can be saved back to the project
                }
            }
        }
        return smartZoneClient != null
    }

    String getURLHost() {
        if (host != null) {
            if (urlChangedByUser) {
                return CentralCatalogue.getSystemSettings().getSmartZoneHost();
            }
        }
        return host;
    }

    String getURLPort() {
        if (port != 0) {
            if (urlChangedByUser) {
                return CentralCatalogue.getSystemSettings().getSmartZonePort();
            }
        }
        return port;
    }

    private boolean collectUserAndPassword() {
        if (CentralCatalogue.getSmartZoneConnecionProperties().get("username") == null ||
                CentralCatalogue.getSmartZoneConnecionProperties().get("password") == null) {
            hideProgressPane()
            if (smartZoneLoginDialog.getLoginInfo()) {
                showProgressPane()
                reuseSession = false
                return true;
            }
        } else {
            return true;
        }
        return false;
    }

    private boolean collectHostAndPort() {
        String host = CentralCatalogue.getSystemSettings().getSmartZoneHost()
        if (!"".equals(host) && !smartZoneServerDialog.errorState())  {
            return true;
        } else if (!smartZoneServerDialog.isLogged()) {
            hideProgressPane()
            if(smartZoneServerDialog.getOpenFromSmartZoneInfo()) {
                hostToUse = smartZoneServerDialog.getHost();
                portToUse = smartZoneServerDialog.getPort();
                reuseSession = false
                showProgressPane()
                return true;
            }
        } else {
            return true;
        }
        return false;
    }

    private boolean isHostPortPairNullOrEmpty() {
        if(host != null && !"".equals(host) && port != null && !"".equals(port)) {
            return false
        }
        return true
    }

    private boolean isProjectSettingSame() {
        if ("" == CentralCatalogue.getSystemSettings().getSmartZoneHost() ||
                CentralCatalogue.getSystemSettings().getSmartZoneHost() != host ||
                "" == CentralCatalogue.getSystemSettings().getSmartZonePort() ||
                CentralCatalogue.getSystemSettings().getSmartZonePort() != port) {
            return false
        }
        return true
    }

    // Show the progress pane if the background task is running. This is to show the progress pane after the information
    // is collected.
    private static void showProgressPane() {
        if (ProgressManager.isBackgroundTaskRunning()) {
            ProgressManager.showProgressPane()
        }
    }

    // Hide the progress pane if the background task is running. This is to hide the progress pane when the smart zone
    // login dialog or server dialog is launched to collect information.
    private static void hideProgressPane() {
        if (ProgressManager.isBackgroundTaskRunning()) {
            ProgressManager.hideProgressPane()
        }
    }
}