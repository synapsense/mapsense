package DeploymentLab.Controller

import DeploymentLab.Dialogs.ServerDialog
import DeploymentLab.Dialogs.SmartZoneLoginDialog
import com.panduit.sz.api.ss.assets.Floorplan
import javax.swing.JFrame

/**
 * @author Ken Scoggins
 * @since SmartZone 8.0
 */
public class InstrumentFloorplanController extends SmartZoneBaseController {

    private int floorplanId
    private String filePath
    private boolean success = false

    public InstrumentFloorplanController( JFrame parent,
                                          SmartZoneLoginDialog smartZoneLoginDialog,
                                          ServerDialog smartZoneServerDialog,
                                          int floorplanId,
                                          String filePath) {
        super( parent, smartZoneLoginDialog, smartZoneServerDialog )
        this.floorplanId = floorplanId
        this.filePath = filePath
    }

    void smartZoneCall() {
        success = smartZoneClient.instrumentFloorplan( floorplanId, filePath )
    }

    public boolean wasSuccessful() {
        return success
    }
}
