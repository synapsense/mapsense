package DeploymentLab.Controller

import DeploymentLab.Dialogs.ServerDialog
import DeploymentLab.Dialogs.SmartZoneLoginDialog
import com.panduit.sz.api.ss.assets.Location

import javax.swing.JFrame

/**
 * Created by CCR on 1/7/2016.
 */
class OpenLocationController extends SmartZoneBaseController {

    List<Location> locations;

    public OpenLocationController(JFrame parent,
                                  SmartZoneLoginDialog smartZoneLoginDialog,
                                  ServerDialog smartZoneServerDialog) {
        super(parent,smartZoneLoginDialog,smartZoneServerDialog)
    }

    void smartZoneCall() {
        locations = smartZoneClient.getLocationTree();
    }

    List<Location> getLocations() {
        return locations;
    }

    Location getLocation( int locationId ) {
        Location location = null
        for( Location loc : locations ) {
            location = findLocation( loc, locationId )
            if( location != null ) {
                break
            }
        }
        return location
    }

    private Location findLocation( Location location, int locationId ) {
        // See if this is a match.
        if( location.getId() == locationId ) {
            return location
        }

        // Traverse the family tree to find the location.
        for( Location childLoc : location.getChildren() ) {
            Location loc = findLocation( childLoc, locationId )
            if( loc != null ) {
                return loc
            }
        }

        return null
    }
}
