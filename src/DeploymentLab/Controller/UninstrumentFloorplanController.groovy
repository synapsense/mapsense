package DeploymentLab.Controller

import DeploymentLab.Dialogs.ServerDialog
import DeploymentLab.Dialogs.SmartZoneLoginDialog
import javax.swing.JFrame

/**
 * @author Ken Scoggins
 * @since SmartZone 8.0
 */
public class UninstrumentFloorplanController extends SmartZoneBaseController {

    private int floorplanId
    private boolean success = false

    public UninstrumentFloorplanController( JFrame parent,
                                            SmartZoneLoginDialog smartZoneLoginDialog,
                                            ServerDialog smartZoneServerDialog,
                                            int floorplanId ) {
        super( parent, smartZoneLoginDialog, smartZoneServerDialog )
        this.floorplanId = floorplanId
    }

    void smartZoneCall() {
        success = smartZoneClient.uninstrumentFloorplan( floorplanId )
    }

    public boolean wasSuccessful() {
        return success
    }
}
