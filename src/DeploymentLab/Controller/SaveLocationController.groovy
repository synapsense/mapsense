package DeploymentLab.Controller

import DeploymentLab.CentralCatalogue
import DeploymentLab.Dialogs.ServerDialog
import DeploymentLab.Dialogs.SmartZoneLoginDialog
import DeploymentLab.Model.DLComponent
import DeploymentLab.channellogger.Logger
import com.panduit.sz.api.shared.IdNameTuple
import com.panduit.sz.api.ss.assets.Floorplan

import javax.swing.JFrame

/**
 * Workflow logic for saving project info back to the SmartZone server.
 *
 * @author Lakshmi Nidadavolu
 * @since SmartZone 8.0.0
 */
class SaveLocationController extends SmartZoneBaseController {
    private static final Logger log = Logger.getLogger(SaveLocationController.class.getName())

    private Floorplan szFloorplan;
    private DLComponent drawing

    public SaveLocationController(JFrame parent,
                                  SmartZoneLoginDialog smartZoneLoginDialog,
                                  ServerDialog smartZoneServerDialog,
                                  Floorplan fp,
                                  DLComponent drawing) {
        super(parent, smartZoneLoginDialog, smartZoneServerDialog);
        this.szFloorplan = fp;
        this.drawing = drawing
    }

    @Override
    void smartZoneCall() {
        List<IdNameTuple> szItems = smartZoneClient.updateFloorplan(szFloorplan)

        if( szItems != null ) szItems.each { log.trace("New SZ ID ${it.getId()} for '${it.getName()}'") }

        // Now we need to find any newly created items and slug the SmartZone ID back into the component.
        StringBuilder errors = new StringBuilder()
        for( IdNameTuple szItem : szItems ) {
            List<DLComponent> comps = CentralCatalogue.getOpenProject().getComponentModel().getComponentsByName( szItem.getName(), drawing )

            if( comps.isEmpty() ) {
                // Can't find any matches by name.
                errors.append( "\nID " )
                errors.append( szItem.getId() )
                errors.append( " '" )
                errors.append( szItem.getName() )
                errors.append( "': Unexpected new item in response." )
            } else if( comps.size() > 1 ) {
                // If this happens, there is a break in the unique name contraint. See if we can ignore it.
                Set<DLComponent> found = [] as Set
                for( DLComponent c : comps ) {
                    if( c.getSmartZoneId() == szItem.getId() ) {
                        found.add( c )
                    }
                }

                log.debug("Multiple name matches. Id Match: $found. Name Match: $comps")
                if( found.size() != 1 ) {
                    errors.append( "\nID " )
                    errors.append( szItem.getId() )
                    errors.append( " '" )
                    errors.append( szItem.getName() )
                    errors.append( "': Multiple local name matches found." )
                }
            } else {
                // Found what we think is a match.
                DLComponent c = comps.first()
                if( c.canSmartZoneLink() ) {
                    if( c.getSmartZoneId() == szItem.getId() ) {
                        log.debug("Found existing linked component for ID ${szItem.getId()} : $c")
                    } else if( c.isSmartZoneLinked() ) {
                        errors.append( "\nID " )
                        errors.append( szItem.getId() )
                        errors.append( " '" )
                        errors.append( szItem.getName() )
                        errors.append( "'. Local match found, but it already has a different ID " )
                        errors.append( c.getSmartZoneId() )
                        errors.append( "." )
                    } else {
                        // Whew! Looks like we found a new component that we can link.
                        log.debug("Linked new component for ID ${szItem.getId()} : $c")
                        c.setSmartZoneId( szItem.getId() )
                    }
                } else {
                    log.debug("Found component for ID ${szItem.getId()} but there's no SZ Id property: $c")
                }
            }
        }

        if( errors.length() > 0 ) {
            log.error("Errors encountered while resolving new component IDs.\nLocal components not updated with their SmartZone ID:\n" +
                      errors.toString(), "message" )
        }
    }
}
