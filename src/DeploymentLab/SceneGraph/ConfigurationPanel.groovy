
package DeploymentLab.SceneGraph

import java.awt.BasicStroke
import java.awt.Color
import java.awt.Dimension
import java.awt.Font
import java.awt.Point

import javax.swing.JTextArea

import edu.umd.cs.piccolo.event.*
import edu.umd.cs.piccolo.*
import edu.umd.cs.piccolo.nodes.*
import edu.umd.cs.piccolo.util.*

import DeploymentLab.channellogger.*

import DeploymentLab.Model.*
import DeploymentLab.Tools.*

class ConfigurationPanel extends PCanvas {
	private static final Logger log = Logger.getLogger(ConfigurationPanel.class.getName())

	private PNode root

	private ComponentModel componentModel
	private ConfigurationSelector selector
	private ToolManager toolManager

	private def nodePositions = []

	ConfigurationPanel(ToolManager tm, ComponentModel cm, JTextArea configDesc) {
		toolManager = tm
		componentModel = cm

		setPanEventHandler(null)
		//set the background to the LaF default gray for a canvas; also, make sure this color is NOT TRANSPARENT
		this.setBackground(this.getForeground())


		selector = new ConfigurationSelector(tm, configDesc)

		root = new PNode()
		root.setOffset(0,10)

		int xSpacing = 110
		int ySpacing = 35//32
		for(int i=0; i<20; i++) {
			int x = 60 + (i%2)*xSpacing
			int y = 10 + ySpacing * i - ySpacing * (i%2)
			nodePositions << new Point(x, y)
		}

		getLayer().addChild(root)
	}

	void setDisplayed(def configList) {
		selector.unSelect()
		toolManager.getTool('adder').type = null
		root.removeAllChildren()
		int i=0
		for(String cname: configList) {
			Point at = nodePositions[i]
			String displayName = componentModel.getDisplaySetting(cname, 'name')
			String shapeName = componentModel.getDisplaySetting(cname, 'shape')
			String paranthetical = ""
			if (displayName.contains("(")){
				paranthetical = displayName.substring( displayName.indexOf("(") )
				displayName = displayName.substring( 0, displayName.indexOf("(") - 1 )
			}
			if (displayName.length() > 25) {
				//scan out from the middle looking for a space and try to put a \n in there
				int middle = displayName.length() / 2
				for( int j = 0; j < middle; j ++ ){
					if (displayName[middle+j].equals(" ")) {
						displayName = displayName.substring(0,middle+j) + "\n" + displayName.substring(middle+(j+1))
						break
                    } else if( displayName[middle+j].equals("-") || displayName[middle+j].equals(":") ) {
                        // Make sure the separating character is kept.
                        displayName = displayName.substring(0,middle+j+1) + "\n" + displayName.substring(middle+(j+1))
                        break
                    }

					if (displayName[middle-j].equals(" ")) {
						displayName = displayName.substring(0,middle-j) + "\n" + displayName.substring( (middle-j)+1 )
						break
                    } else if( displayName[middle+j].equals("-") || displayName[middle+j].equals(":") ) {
                        // Make sure the separating character is kept.
                        displayName = displayName.substring( 0, middle-j+1 ) + "\n" + displayName.substring( (middle-j)+1 )
                        break
                    }
				}
			}
			if ( paranthetical.length() > 0 ){
				StringBuilder sb = new StringBuilder()
				sb.append(displayName)
				sb.append("\n")
				sb.append(paranthetical)
				displayName = sb.toString()
			}

			String desc = componentModel.getDisplaySetting(cname, 'description')
			def roles = componentModel.listComponentClasses(cname)
			ConfigurationNode newNode = new ConfigurationNode(cname, displayName, ShapeFactory.getShape(shapeName), at.x, at.y, desc, roles)
			newNode.addInputEventListener(selector)
			root.addChild(newNode)
			i++
		}
		PBounds pb = root.getFullBounds()
		setPreferredSize(new Dimension((int)(pb.x + pb.width + 10), (int)(pb.y + pb.height + 10)))
		revalidate()
	}
	
	void reloadConfigList(){
		root.removeAllChildren()
		revalidate()
	}

	void clearSelection(){
		root.getChildrenReference().each{
			it.unSelect()
		}
	}
}

class ConfigurationNode extends PNode {
	private static final Logger log = Logger.getLogger(ConfigurationNode.class.getName())
	
	private PPath backgroundNode
	private PNode shapeNode
	private PText textNode

	String name
	private String displayName
	private String description

	static private Color backgroundColor = Color.white
	static private Color hoverColor = new Color((float)0.3, (float)0.3, (float)0.3, (float)0.2)
	static private Color selectColor = new Color((float)0.5, (float)0.5, (float)0.5, (float)0.5)
	static private Color superSelectColor = new Color((float)1.0, (float)0.0, (float)0.0, (float)0.5)

	private double spacing = 3.0
	private double iconSize = 30.0
	private float  lineSize = 0.04

	def roles

	ConfigurationNode(String n, String dn, PNode s, double x, double y, String desc, def r) {
		name = n
		displayName = dn
		description = desc
		shapeNode = s
		shapeNode.scale(iconSize)
		
		textNode = new PText()
		textNode.setText(displayName)
		textNode.setFont(new Font(Font.SERIF, Font.BOLD, 8))
		textNode.setTextPaint(Color.black)
		textNode.setX(-(textNode.getFullBounds().width / 2.0))
		textNode.setY(shapeNode.getFullBounds().getY()+ shapeNode.getFullBounds().height)

		this.addChild(shapeNode)
		this.addChild(textNode)

		PBounds b = this.getFullBounds()
		b.inset(-1, -1)
		this.setBounds(b)

		backgroundNode = PPath.createRoundRectangle((float)(b.x-spacing), (float)(b.y - spacing), (float)(b.width + 2.0*spacing), (float)(b.height + 2*spacing),6.0f,6.0f)
		backgroundNode.setPaint(backgroundColor)
		backgroundNode.setStroke(new BasicStroke((float)1.0))
		backgroundNode.setStrokePaint(Color.black)

		this.addChild(backgroundNode)
		shapeNode.moveToFront()
		textNode.moveToFront()

		this.translate(x, y + shapeNode.getFullBounds().height / 2)
		this.setChildrenPickable(false)

		roles = r
	}

	void select(boolean singleShot = false) {
		log.trace("select($singleShot)")
		if(singleShot){
			backgroundNode.setPaint(selectColor)
		} else {
			backgroundNode.setPaint(superSelectColor)
		}
	}

	void unSelect() {
		backgroundNode.setPaint(backgroundColor)
	}

	boolean isSelected() {
		return (backgroundNode.getPaint() == selectColor)
	}

	void hover() {
		backgroundNode.setPaint(hoverColor)
	}

	void unHover() {
		backgroundNode.setPaint(backgroundColor)
	}

	void remove(PNode n) {}

	String getDescription() {
		return description
	}
}

class ConfigurationSelector extends PBasicInputEventHandler {
	private static final Logger log = Logger.getLogger(ConfigurationSelector.class.getName())

	ToolManager toolManager

	JTextArea configDescriptionTextArea

	ConfigurationNode hovered = null
	ConfigurationNode selected = null

	ConfigurationSelector(ToolManager tm, JTextArea configDesc) {
		toolManager = tm
		configDescriptionTextArea = configDesc
	}

	void mouseEntered(PInputEvent e) {
		PNode node = e.getPickedNode()

		if(node == hovered || node == selected)
			return
		if(hovered != null)
			hovered.unHover()
		hovered = (ConfigurationNode)node
		hovered.hover()
		e.setHandled(true)
	}

	public void mouseExited(PInputEvent e) {
		PNode node = e.getPickedNode()

		if(node == selected)
			return
		if(hovered != null) {
			hovered.unHover()
			hovered = null
			e.setHandled(true)
		}
	}

	public void mousePressed(PInputEvent e) {
		PNode node = e.getPickedNode()


		if(selected != null)
			selected.unSelect()
		if(hovered != null) {
			hovered.unHover()
			hovered = null
		}
		selected = (ConfigurationNode)node
		configDescriptionTextArea.setText(selected.getDescription())

		boolean toolSupportsSingleShotMode = false
		if ( selected.roles.contains('controlinput') || selected.roles.contains('inspector') || selected.roles.contains('controlleddevice') || selected.roles.contains('secondary') || selected.roles.contains('controlstrategy')  ){
			toolManager.getTool('controlinputadder').type = selected.name
			toolManager.getTool('controlinputadder').roles = selected.roles
			toolManager.setActiveTools(['hover', 'panzoomer', 'controlinputadder', 'keycommands', 'popupmenu', 'fasternamer', 'currentlocation'])
			toolSupportsSingleShotMode = toolManager.canSingleShot('controlinputadder')

		} else if(selected.roles.contains('dynamicchild')){
			toolManager.getTool('dynamicchildadder').type = selected.name
			toolManager.setActiveTools(['hover', 'panzoomer', 'dynamicchildadder', 'keycommands', 'popupmenu', 'fasternamer', 'currentlocation'])
			toolSupportsSingleShotMode = toolManager.canSingleShot('dynamicchildadder')

		} else if(selected.roles.contains('arbitraryshaped')){
			toolManager.getTool('arbitraryshaper').type = selected.name
			toolManager.setActiveTools(['arbitraryshaper','hover', 'panzoomer', 'keycommands', 'popupmenu', 'fasternamer', 'currentlocation'])
			toolSupportsSingleShotMode = toolManager.canSingleShot('arbitraryshaper')

		} else if(selected.roles.contains('staticchildplaceable')) {
            toolManager.getTool('staticchildrennodeadder').type = selected.name
			toolManager.setActiveTools(['staticchildrennodeadder','hover', 'panzoomer', 'keycommands', 'popupmenu', 'fasternamer', 'currentlocation'])
			toolSupportsSingleShotMode = toolManager.canSingleShot('staticchildrennodeadder')

        } else {
			toolManager.getTool('adder').type = selected.name
			toolManager.setActiveTools(['hover', 'panzoomer', 'adder', 'keycommands', 'popupmenu', 'fasternamer', 'currentlocation'])
			toolSupportsSingleShotMode = toolManager.canSingleShot('adder')

		}


		boolean singleShotMode = true
		if ( e.getClickCount() == 1 && toolSupportsSingleShotMode ){
			singleShotMode = true
		//} else if ( e.getClickCount() > 1 ){
		} else {
			singleShotMode = false
		}

		selected.select(singleShotMode)
		toolManager.setSingleShotMode(singleShotMode)


		e.setHandled(true)
	}

	public void unSelect() {
		if(selected != null) {
			selected.unSelect()
			configDescriptionTextArea.setText("")
		}
	}
}

