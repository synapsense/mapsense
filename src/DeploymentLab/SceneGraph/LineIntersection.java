package DeploymentLab.SceneGraph;

import DeploymentLab.channellogger.Logger;

import javax.sound.sampled.Line;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 * Created by IntelliJ IDEA.
 * User: HLim
 * Date: 6/1/11
 * Time: 5:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class LineIntersection {
    private double x1;
    private double x2;
    private double x3;
    private double x4;

    private double y1;
    private double y2;
    private double y3;
    private double y4;

    private Line2D.Double line1;
    private Line2D.Double line2;

    private Point2D interPoint = null;

    private static final Logger log = Logger.getLogger(LineIntersection.class.getName());
    public LineIntersection(Point2D p1, Point2D p2, Point2D p3, Point2D p4){
        x1 = p1.getX();
        x2 = p2.getX();
        x3 = p3.getX();
        x4 = p4.getX();

        y1 = p1.getY();
        y2 = p2.getY();
        y3 = p3.getY();
        y4 = p4.getY();

        line1 = new Line2D.Double(p1,p2);
        line2 = new Line2D.Double(p3,p4);
        calculateIntersection();
    }

    private void calculateIntersection(){
        if(line1.intersectsLine(line2) ){
           // log.info("line1 point :" + x1 + " ,"  + y1);
            //log.info("line1 point :" + x2 + " ,"  + y2);
           // log.info("line2 point :" + x3 + " ,"  + y3);
           // log.info("line2 point :" + x4 + " ,"  + y4);
           // log.info("line intersect?: " + line1.intersectsLine(line2));

            double x = ((x1*y2 -y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4))/((x1-x2)*(y3-y4) -(y1-y2)*(x3-x4));
            double y = ((x1*y2 -y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4))/((x1-x2)*(y3-y4) -(y1-y2)*(x3-x4));

            //log.info("intersection x" + x + " y " + y) ;
            Point2D.Double p = new Point2D.Double(x,y);

            if(x>0 && y>0){
                double d1 = line1.getP1().distance(p);
                double d2 = line1.getP2().distance(p);
                double d3 = line1.getP1().distance(p);
                double d4 = line1.getP2().distance(p);

                //log.info("d1:" + d1);
               // log.info("d2:" + d2);
               // log.info("d3:" + d3);
               // log.info("d4:" + d4);
                if(d1<1 || d2<1 ||d3<1 || d4<1){

                }   else{
                      interPoint  = new Point2D.Float((float)x,(float)y);
                }
            }
       }
    }

    public Point2D getInterPoint(){
        return interPoint;
    }
}
