package DeploymentLab.SceneGraph

import java.awt.BasicStroke
import java.awt.Color
import java.awt.Font
import java.awt.Polygon
import java.awt.Shape
import java.awt.geom.AffineTransform
import java.awt.geom.GeneralPath
import java.awt.geom.Point2D
import java.awt.geom.Rectangle2D

import edu.umd.cs.piccolo.nodes.*
import edu.umd.cs.piccolo.*

import DeploymentLab.Model.*
import java.awt.geom.Area
import DeploymentLab.Pair
import DeploymentLab.DisplayProperties
import DeploymentLab.CentralCatalogue

/**
 * Handles all rules and regulations for drawing association lines.
 */
class DeploymentAssociation extends PNode {

	static protected Font assocFont = new Font(Font.SANS_SERIF, Font.BOLD, 11)
	static protected Color hoverColor

	/** True if this association line is being mouse-overed.*/
	private boolean hovered

	/** Reference to the producer of this association. */
	private DLComponent producer
	/** Reference to the consumer of this association. */
	private DLComponent consumer

	/** Child node to handle the actual line itself. */
	private PPath pathNode
	/** Child node to handle the text description of the association. */
	private PText textNode
	/** Child node to handle the arrowhead at midline. */
	private PPath arrowNode

	private List<Pair<Object, Object>> associations = []
	
	private DisplayProperties displayProperties
	/** Stores the datatype of this association. */
	private String associationType
	//static private Properties associationProperties
	static private associationProperties = CentralCatalogue.getInstance().getAssociationProperties()
	/** Stores the color of this association line (based on the datatype.) */
	private Color associationColor

	/** Stores the vertical line segment, if applicable. */
	private Rectangle2D vertical
	/** Stores the horizontal line segment, if applicable, */
	private Rectangle2D horizontal

	private static UP_ANGLE = Math.PI / 2
	private static RIGHT_ANGLE = Math.PI
	private static DOWN_ANGLE = (Math.PI / 2) * 3
	private static LEFT_ANGLE = 0

	static{
		hoverColor = new Color(Integer.parseInt(CentralCatalogue.getInstance().getAssociationProperties().getProperty("associations.color.hover"), 16)) 
	}


	DeploymentAssociation(DLComponent producer, String producerId, DLComponent consumer, String consumerId, DisplayProperties p) {
		this.producer = producer
		this.consumer = consumer
		
		producer.addPropertyChangeListener(this, this.&componentPropertyChanged)
		consumer.addPropertyChangeListener(this, this.&componentPropertyChanged)

		associationType = producer.getProducer(producerId).getDatatype()
		
		displayProperties = p
		p.addPropertyChangeListener(this, this.&displayPropertyChanged)

		//if we don't have a color code, default to red
		def colorCode = associationProperties.getProperty("associations.color." + associationType) ?: "ff0000"
		associationColor = new Color(Integer.parseInt(colorCode, 16))

		this.setChildrenPickable( false )

		hovered = false

		Shape line = getLine()

		pathNode = buildPathNode( line )
		this.addChild( pathNode )

		arrowNode = buildArrowNode( line )
		this.addChild( arrowNode )
		checkArrow()

		associations += new Pair<Object, Object>(producer.getProducer(producerId), consumer.getConsumer(consumerId))

		textNode = new PText(getAssociationDescription())
		Point2D lineCenter = getMidpoint()
		textNode.centerFullBoundsOnPoint(lineCenter.x, lineCenter.y)
		textNode.setFont(assocFont)
		this.addChild( textNode )

		//textNode.setVisible((boolean)(displayProperties.getProperty('associationDetail')))
		textNode.setVisible(false)

		//start line with right visibility status based on view options
		this.displayPropertyChanged(null)
	}

	/**
	 * Constructs the node holding the line.
	 * @param line A Shape object (from getLine()) that describes the line to be drawn.
	 */
	private PPath buildPathNode( Shape line ) {
		PPath pn
		pn = new PPath()
		pn.setPathTo(line)
		pn.setPaint(associationColor)
		pn.setStroke(new BasicStroke((float)0.5))
		pn.setStrokePaint(associationColor)
		pn.setVisible( true )
		return ( pn )
	}

	/**
	 * Constructs the node holding the arrowhead.
	 * @param line A Shape object (from getLine()) that describes the line that the arrowhead will be drawn on.
	 */
	private PPath buildArrowNode( Shape arrow ) {
		PPath an
		an = new PPath()
		an.setPathTo( this.getArrow( line ) )
		an.setPaint(associationColor)
		an.setStroke(new BasicStroke((float)0.5))
		an.setStrokePaint(associationColor)
		an.setVisible( true )
		return ( an )
	}

	/** Overloads the default implementation to use the child node as intersection delegates.  Also: bonus boolean operations! */
	public boolean intersects(java.awt.geom.Rectangle2D localBounds) {
		return ( pathNode.intersects( localBounds ) || arrowNode.intersects( localBounds ) )

	}

	/** Construct the text name of this association.*/
	private String getAssociationDescription() {
		//println "working on that description!"
		String result = ""
		associations.each{
            //println "${it.a} (${it.a.getClass().getName()}) , ${it.b} (${it.b.getClass().getName()})"
			if ( it.a instanceof Producer ){
				result += "${it.a.getName()} \u2192 ${it.b.getName()} \n"
			} else {
				result += "${it.a.getName()} \u2190 ${it.b.getName()} \n"
			}
		}
		//println "Here 'tis:\n$result"
		return result
	}

	/**
	 * Add an association.
	 * NOTE: the producer and consumer here may be the inverse of the producer, consumer variables that the class was initialized with.
	 * Since this association represents both directions, that is considered OK (for now)
	 */
	public void addAssociation(DLComponent addedProducer, String producerId, DLComponent addedConsumer, String consumerId) {

//println "adding association: $producerId -> $consumerId"
//		println "$producer, $consumer"
//		println "${producer.getProducer(producerId)}, ${ consumer.getConsumer(consumerId) } "
//		println "${producer.getConsumer(consumerId)}, ${ consumer.getProducer(producerId) } "

        if ( producer == addedProducer ) {
			associations.add(new Pair<Object, Object>(addedProducer.getProducer(producerId), addedConsumer.getConsumer(consumerId)))
		} else {
			//println "we're headed the other way!"
            associations.add(new Pair<Object, Object>(addedConsumer.getConsumer(consumerId), addedProducer.getProducer(producerId)))
		}

		//associations.add(new Pair<Object, Object>(producer.getProducer(producerId), consumer.getConsumer(consumerId)))
		textNode.setText( getAssociationDescription() )

		checkArrowShape()

		//Shape line = getLine()
		Point2D lineCenter = getMidpoint()
		textNode.centerFullBoundsOnPoint(lineCenter.x, lineCenter.y)
	}

	/**
	 * Remove an association.
	 * Returns true when the last association has been removed
	 */
	public boolean removeAssociation(DLComponent removedProducer, String producerId, DLComponent removedConsumer, String consumerId) {
		//associations.remove(new Pair<Object, Object>(producer.getProducer(producerId), consumer.getConsumer(consumerId)))

        if ( producer == removedProducer ) {
			associations.remove(new Pair<Object, Object>(removedProducer.getProducer(producerId), removedConsumer.getConsumer(consumerId)))
		} else {
			//println "we're headed the other way!"
			associations.remove(new Pair<Object, Object>(removedConsumer.getConsumer(consumerId), removedProducer.getProducer(producerId)))
		}


		if(associations.size() == 0)
			return true

		textNode.setText(getAssociationDescription())

		checkArrowShape()

		//Shape line = getLine()
		Point2D lineCenter = getMidpoint()
		textNode.centerFullBoundsOnPoint(lineCenter.x, lineCenter.y)

		return false
	}

	DLComponent getProducer() {
		return producer
	}

	DLComponent getConsumer() {
		return consumer
	}

	def listAssociations(){
		return associations
	}
	
	String getAssociationType(){
		return associationType
	}

	/**
	 * Finds the midpoint of this association line.
	 * Determines which line type we've got (straight or with-a-corner) and then works out the correct coordinates.
	 * @return A Point2D holding the (x,y) coordinates of the midpoint
	 */
	private Point2D.Double getMidpoint() {
		Point2D.Double result
		if (vertical == null && horizontal == null){
			result = getMidpoint( this.getLine() )
		} else if ( vertical.getHeight() > horizontal.getWidth() ) {
			result = getMidpoint(vertical)
		} else {
			result = getMidpoint(horizontal)
		}
		return result
	}

	private static Point2D.Double getMidpoint(Shape s) {
		Rectangle2D r = s.getBounds2D()
		return new Point2D.Double(r.x + (r.width/2.0), r.y + (r.height/2.0))
	}

	/** Disconnect this association from its producer and consumer. */
	void disconnect() {
		producer.removePropertyChangeListener(this)
		consumer.removePropertyChangeListener(this)
		producer = null
		consumer = null
		displayProperties.removePropertyChangeListener( this )
	}

	/* Returns true if the line is currently being mouse-overed. */
	boolean isHovered() {
		return hovered
	}

	/**
	 * Adjusts the association line so it looks different on a mouse-over.
	 */
	boolean setHovered(boolean h) {
		hovered = h
		if(hovered) {
			//pathNode.setPaint(Color.blue)
			//pathNode.setStrokePaint(Color.blue)
			pathNode.setPaint(hoverColor)
			pathNode.setStrokePaint(hoverColor)

			/*
			textNode.setTextPaint(Color.blue)
			textNode.setPaint(Color.gray)
			*/
			//new text node colors: white text, dark blue background
			textNode.setTextPaint(Color.white)
			textNode.setPaint( new Color( 0, 0, 0x88 ) )

			//textNode.setTextPaint(Color.white)
			//textNode.setPaint( associationColor )
			
//			arrowNode.setPaint(Color.blue)
//			arrowNode.setStrokePaint(Color.blue)
			arrowNode.setPaint(hoverColor)
			arrowNode.setStrokePaint(hoverColor)

			textNode.setVisible((boolean)(displayProperties.getProperty('associationDetail')))
			//Moving the node itself and the text to the front of their respective siblings gets puts the text on top of any other line (but behind any nodes.)
			this.moveToFront()
			pathNode.moveToFront()
			arrowNode.moveToFront()
			textNode.moveToFront()
		} else {
			pathNode.setPaint(associationColor)
			pathNode.setStrokePaint(associationColor)
			textNode.setTextPaint(Color.black)
			textNode.setPaint(null)
			arrowNode.setPaint(associationColor)
			arrowNode.setStrokePaint(associationColor)
			textNode.setVisible(false)
		}
	}

	void componentPropertyChanged(who, String prop, oldVal, newVal) {
		if(prop == 'x' || prop == 'y') {
			Shape line = getLine()
			pathNode.setPathTo(line)
			arrowNode.setPathTo( this.getArrow( line ) )
			checkArrow()
			Point2D lineCenter = getMidpoint()
			textNode.centerFullBoundsOnPoint(lineCenter.x, lineCenter.y)
			this.resetBounds()
		}
	}

	/**
	 * Based on the length of the line, check to see if the arrow should be displayed.
	 * Empirical testing seems to show that lines less than about 60 units shouldn't have arrows
	 */
	private void checkArrow(){
		int x = producer.getPropertyValue('x') - consumer.getPropertyValue('x')
		int y = producer.getPropertyValue('y') - consumer.getPropertyValue('y')
		double len = Math.sqrt(x*x + y*y)
		if ( len <= 60 ) {
			arrowNode.setVisible(false)
		} else {
			arrowNode.setVisible(true)
		}
	}

	private void checkArrowShape(){
		arrowNode.setPathTo( this.getArrow( this.getLine() ) )
	}

	/**
	 * Computes the rotation angle of the association line.
	 */
	private double getAngle() {
		int x = producer.getPropertyValue('x') - consumer.getPropertyValue('x')
		int y = producer.getPropertyValue('y') - consumer.getPropertyValue('y')

		double len = Math.sqrt(x*x + y*y)
		double angle

		if(y == 0.0) {
			if(x >= 0.0)
				angle = 0.0
			else
				angle = Math.PI
		} else if(x == 0.0) {
			if(y >= 0.0)
				angle = Math.PI/2
			else
				angle = Math.PI * 3/2
		} else {
			if(y < 0)
				angle = - Math.acos(x/len)
			else
				angle = Math.acos(x/len)
		}
		return ( angle )
	}

	/**
	 * Calculates the Shape object for the line itself.
	 * Basic idea: for this object to be interacted with, it has to take up space.
	 * Since lines do not, use a thin rotated rectangle instead.
	 *
	 * This also looks a the distance between the terminal points and draws either the straight line or the version with a corner.
	 */
	Shape getLine() {
		int x = producer.getPropertyValue('x') - consumer.getPropertyValue('x')
		int y = producer.getPropertyValue('y') - consumer.getPropertyValue('y')

		double len = Math.sqrt(x*x + y*y)
		double angle = this.getAngle()

		//Thanks to groovy's dynamic nature, we can *theoretically* have x&y values that are either null or not a number at this point.
		//However, the system should have prevented that, and if we DO end up with an x value of "Q" at this stage, the Java classes below are going to flame out like nobody's business.
		//So - assertions.
		assert ( consumer.getPropertyValue('x') instanceof Number ) : "Non-numeric Association Line X Value"
		assert ( consumer.getPropertyValue('y') instanceof Number ) : "Non-numeric Association Line Y Value"

		//experimentally, 100 seems about the right cut-off point
		if ( len < 100 ) {
			vertical = null
			horizontal = null
		    //the old straight line alg
			Rectangle2D shape = new Rectangle2D.Double(consumer.getPropertyValue('x').toDouble(), consumer.getPropertyValue('y').toDouble(), len, 1.5)
			return ( AffineTransform.getRotateInstance(angle, consumer.getPropertyValue('x'), consumer.getPropertyValue('y')).createTransformedShape(shape) )
		}

		int cX = consumer.getPropertyValue('x')
		int cY = consumer.getPropertyValue('y')
		int pX = producer.getPropertyValue('x')
		int pY = producer.getPropertyValue('y')

		if ( cX > pX ) {
			len = cX - pX
			horizontal = new Rectangle2D.Double( pX.toDouble(),cY.toDouble(), len, 1.5 )
		} else {
			len = (pX - cX) + 1
			horizontal = new Rectangle2D.Double( cX.toDouble(),cY.toDouble(), len, 1.5 )
		}

		if ( cY > pY ){
			len = (cY - pY) + 1
			vertical = new Rectangle2D.Double( pX.toDouble(),pY.toDouble(), 1.5, len )
		} else {
			len = pY - cY
			vertical = new Rectangle2D.Double( pX.toDouble(),cY.toDouble(), 1.5, len )
		}

		Area combo = new Area(vertical);
		combo.add( new Area(horizontal) )

		return ( combo )
	}

	/** Calculates and returns the basic Shape of the mid-line arrow, based on the current line. */
	Shape getArrow( Shape line ) {

		int centerX
		int centerY
		double angle

		if ( horizontal == null && vertical == null ){
			//the original straight-line arrow
			centerX = getMidpoint(line).getX()
			centerY = getMidpoint(line).getY()
			angle = this.getAngle()
		} else {
			//arrow for corner-lines
			def vLen = vertical.getHeight()
			def hLen = horizontal.getWidth()

			int cX = consumer.getPropertyValue('x')
			int cY = consumer.getPropertyValue('y')
			int pX = producer.getPropertyValue('x')
			int pY = producer.getPropertyValue('y')

			if ( vLen > hLen ){
				centerX = getMidpoint(vertical).getX()
				centerY = getMidpoint(vertical).getY()
				if ( cY > pY ){
					angle = DOWN_ANGLE
				} else {
					angle = UP_ANGLE
				}
			} else {
				centerX = getMidpoint(horizontal).getX()
				centerY = getMidpoint(horizontal).getY()

				if ( cX > pX ){
					angle = RIGHT_ANGLE
				} else {
					angle = LEFT_ANGLE
				}

			}

		}

		def aSides = []
		associations.each{ pair ->
			if ( pair.a instanceof Producer){
				aSides += "producer"
			} else {
				aSides += "consumer"
			}
		}
		aSides = aSides.unique()




		Polygon triangle = new Polygon();
//		triangle.addPoint( centerX, centerY )
		//for a basic triangle
		//triangle.addPoint( (centerX+20), (centerY+10) )
		//triangle.addPoint( (centerX+20), (centerY-10) )
		//this makes a fancy arrow, set back by <offset> units
		//offset used to be 10, so it was set back off of the text, but actually centering it looks better
		def offset = -10
		def scale = 1 //(between 1 and 0)
		//adjust the scale of the arrow based on the line's length
		if ( this.getLength() < 100 && this.getLength() > 60 ) {
			scale = (1/40) * (this.getLength() - 60 )
		}

		if ( aSides.size() > 1 ){

			//make a dual-arrow
			triangle.addPoint( (centerX + (int)( 20 * scale )  ), centerY )

			triangle.addPoint( centerX , centerY + (int)( 10 * scale ) )

			triangle.addPoint( (centerX - (int)( 20 * scale )  ), centerY )

			triangle.addPoint( centerX , centerY - (int)( 10 * scale ) )

			
			triangle.addPoint( (centerX + (int)( 20 * scale )  ), centerY )

			Shape arrow = new GeneralPath( triangle )
			return ( AffineTransform.getRotateInstance( angle, centerX, centerY ).createTransformedShape( arrow ) )		
		}

		triangle.addPoint( centerX, centerY )

		triangle.addPoint( (centerX+offset), centerY ) //back
		triangle.addPoint( (centerX+offset+(int)(20*scale)), (centerY+(int)(10*scale)) ) //up
		triangle.addPoint( (centerX+offset+(int)(15*scale)), centerY ) //mid
		triangle.addPoint( (centerX+offset+(int)(20*scale)), (centerY-(int)(10*scale)) ) //down
		triangle.addPoint( (centerX+offset), centerY ) //back again

		Shape arrow = new GeneralPath( triangle )
		//double angle = this.getAngle()
		return ( AffineTransform.getRotateInstance( angle, centerX, centerY ).createTransformedShape( arrow ) )
	}

	double getLength(){
		int x = producer.getPropertyValue('x') - consumer.getPropertyValue('x')
		int y = producer.getPropertyValue('y') - consumer.getPropertyValue('y')
		return ( Math.sqrt(x*x + y*y) )
	}


	void remove(PNode c) {}

	/** Toggles the description on and off.
	 * NOTE: currently, we've changed the association lines so that they only show the text on mouseover, toggle nonwithstanding.  This code and it's friends are left in as stubs for when we change our minds again.
	 */
	void toggleAssociationDescription(boolean isOn){
		//textNode.setVisible(isOn)
	}

	/**
	 *  Event handler to update the display when properties have changed.
	 */
	void displayPropertyChanged(def who){
        boolean isActiveDrawingChild =  displayProperties.isActiveDrawingComponent(producer)

        if(!isActiveDrawingChild){
            pathNode.setVisible(false)
			this.setVisible(false)
            return
        }

		//only toggle the text on and off it the node is hovered at the moment
		if ( hovered ){
			textNode.setVisible((boolean)(displayProperties.getProperty('associationDetail')))
		} else {
			textNode.setVisible(false)
		}

		def unselectedTypes = displayProperties.getProperty('invisibleObjects')
		boolean isSelectedType = (!unselectedTypes.contains(producer.getType()) 
			&& !unselectedTypes.contains(consumer.getType()) 
			&& !unselectedTypes.contains(producer.getPlaceableChildType()) 
			&& !unselectedTypes.contains(consumer.getPlaceableChildType())) 
		boolean isDisplayable = isSelectedType && isActiveDrawingChild

		if(isDisplayable){
			def unselectedAssociationTypes = displayProperties.getProperty('invisibleAssociations')
			def isSelectedAssociationTypes = !unselectedAssociationTypes.contains(associationType)
			pathNode.setVisible(isSelectedAssociationTypes)
			this.setVisible(isSelectedAssociationTypes )

		}else{
			pathNode.setVisible(false)
			this.setVisible(false)
		}
		
		/*
		if(unselectedTypes.contains(producer.getType()) || unselectedTypes.contains(consumer.getType()) ||  unselectedTypes.contains(producer.getPlaceableChildType()) || unselectedTypes.contains(consumer.getPlaceableChildType() ) ){
			pathNode.setVisible(false)
			this.setVisible(false)
		}else{
			def unselectedAssociationTypes = displayProperties.getProperty('invisibleAssociations')
			pathNode.setVisible(!unselectedAssociationTypes.contains(associationType))
			this.setVisible(!unselectedAssociationTypes.contains(associationType))
		}*/
	}

	public String toString() {
		return( "DA: ${producer.getName()} =>= ${consumer.getName()}" )
	}

}

