package DeploymentLab.SceneGraph;

import java.awt.geom.Point2D;
import java.util.Iterator;

/**
 * An iterator that hands out X,Y coords along a spiral out from a central starting point.
 * @see DeploymentLab.SceneGraph.IconNode#adjustOffset()
 */
class SpiralIterator implements Iterator {
	private double startX;
	private double startY;
	private double x;
	private double y;
	private double r;
	private double theta;

	/**
	 * Inits a new Spiral.
	 * @param start The X,Y coord of the center of the spiral.
	 */
	public SpiralIterator( Point2D.Double start  ) {
		startX = start.getX();
		startY = start.getY();
		x = 0;
		y = 0;
		r = 0;
		theta = 0;
	}

	/**
	 * Returns true if the spiral can continue.
	 * @return Basically, this is always true.
	 */
	public boolean hasNext() {
		return (theta < 360);
	}

	/**
	 * Get the next point on the spiral.
	 * @return a Point2D with the X,Y coord of the next point in the spiral.
	 */
	public Point2D.Double next() {
		//compute current polar coords
		//r = Math.sqrt( (x*x) + (y*y) )
		//theta = Math.atan2(y,x)
		//move on by a few degrees
		theta -= 6;
		r = 1 * theta;
		//go back to rect
		x = r * Math.cos(theta);
		y = r * Math.sin(theta);
		return ( new Point2D.Double(x+startX,y+startY) );
	}

	/**
	 * Not supported.
	 */
	public void remove() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String toString() {
		return "SpiralIterator{" +
				"startX=" + startX +
				", startY=" + startY +
				", x=" + x +
				", y=" + y +
				", r=" + r +
				", theta=" + theta +
				'}';
	}
}