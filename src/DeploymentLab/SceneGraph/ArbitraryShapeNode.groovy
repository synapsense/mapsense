package DeploymentLab.SceneGraph

import DeploymentLab.Model.ComponentProp
import DeploymentLab.Model.ComponentType
import DeploymentLab.Model.ModelState
import DeploymentLab.Model.RoomType
import DeploymentLab.Tools.NodeMover
import edu.umd.cs.piccolo.util.PPickPath
import edu.umd.cs.piccolox.handles.PHandle
import edu.umd.cs.piccolox.nodes.PClip;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent
import java.awt.geom.Line2D
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import DeploymentLab.CentralCatalogue;
import DeploymentLab.StaticValidator;
import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.event.PBasicInputEventHandler;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.nodes.PPath;

import DeploymentLab.DisplayProperties;
import DeploymentLab.Model.DLComponent;
import DeploymentLab.channellogger.Logger;
import DeploymentLab.UndoBuffer
import DeploymentLab.StaticValidator.ShapeValidations
import DeploymentLab.Tools.ToolManager
import javax.swing.event.PopupMenuListener
import javax.swing.event.PopupMenuEvent
import DeploymentLab.SelectModel

/**
 *  The class for contained polygon
 *  The user can create polygon by clicking to add a point of polygon
 *  Contained polygon can be edited by deleting point or adding point
 *
 */
public class ArbitraryShapeNode extends DeploymentNode {
	private static final Logger log = Logger.getLogger(ArbitraryShapeNode.class.getName());

	public static final int HIGHLIGHT_WIDTH = 4

	private ArrayList<Point2D.Float> points = new ArrayList<Point2D.Float>();
	private ArrayList<EdgePoint> resizeDots = new ArrayList<EdgePoint>();
	private ArrayList<Line2D> lines
	private PPath polygon = new PPath();
	private PPath selectPolygon = new PPath();
	private float lineWidth = 1;
	private Paint lineColor = Color.BLACK
	private final String ATTRIBUTE_TYPE ="type";
	private final String ATTRIBUTE_TYPE_EDGE="edge";
	private final String ATTRIBUTE_TYPE_POLYGON="polygon";

	private PClip gridClip = new PClip()
	private PNode gridLayer = new PNode();
	private final Color GRID_COLOR = Color.GRAY
	
	private Properties uiStrings;
	private UndoBuffer undoBuffer;
    private ToolManager toolManager;
    private SelectModel selectModel;
    private boolean isOrthoMode = false;    // ortho mode to draw orthogonal line ( shift key down)
    private DeploymentPanel deploymentPanel;

	private HashSet<ArbitraryShapeNode> cachedPolygons;

	private boolean isDragDotOnly = false
	private boolean ignoreXYchange = false

	protected int minPoints = 3;

	/**
	 * construct ArbitaryShapeNode
	 * @param c   : DLComponent for data model
	 * @param p   : property for interacting with view option or display filter
	 */
	public ArbitraryShapeNode(DLComponent c, DisplayProperties p, UndoBuffer undoBuffer,ToolManager toolManager, SelectModel sm, DeploymentPanel deploymentPanel) {
		super( c, p )

		this.undoBuffer = undoBuffer;
        this.toolManager = toolManager;
        this.selectModel = sm ;
        this.deploymentPanel = deploymentPanel;
		uiStrings = CentralCatalogue.getInstance().getUIStrings();

		if( c.hasProperty( ComponentProp.MIN_SHAPE_PTS ) ) {
			minPoints = c.getPropertyValue( ComponentProp.MIN_SHAPE_PTS )
		}

		// helps with debug
		gridLayer.setName("gridLayer")
		gridClip.setName("gridClip")
		polygon.setName("polygon")
		selectPolygon.setName("selectPolygon")

		// construct points, sides and polygon shape
		constructBoundary();

		selectPolygon.setVisible( selected )
	}

	void remove(PNode n) {}

	public void setDragging( boolean d ) {
		super.setDragging( d )

		// Hide the extra stuff during a drag so it doesn't block the view.
		if( isDragging() ) {
			selectPolygon.setVisible( false )
			setResizeDotsVisible( false )
		} else {
			selectPolygon.setVisible( selected )

		}
	}

	public PPath getPath() {
		return polygon
	}

	void buildContextMenu( JPopupMenu parentMenu, PInputEvent e ) {

		ActionListener addListener = new ActionListener(){
			@Override
			public void actionPerformed( ActionEvent ae ) {
				Point2D selectedPoint = CentralCatalogue.getDeploymentPanel().scalePoint( e.getPosition() )
				int index = getNewPointIndex( selectedPoint );
				addPoint( selectedPoint, index );
			}
		};

		JMenuItem addMenu = new JMenuItem( uiStrings.getProperty("PopupMenu.containedArea.addPoint") );
		addMenu.addActionListener( addListener );
		parentMenu.add( addMenu );
	}

	 /**
	 * Construct points, sides and polygon based on points and sides
	 */
	protected void constructBoundary(){
		constructPoints();
		constructShape()
		resetClipBounds()
		polygon.addAttribute(ATTRIBUTE_TYPE, ATTRIBUTE_TYPE_POLYGON);

		// todo: It appears we treat everything as if it has a grid? May need to rework this.
		gridClip.addChild(gridLayer);
		constructGrid();

		this.addChild(gridClip);
		this.addChild(polygon);
		this.addChild( selectPolygon )
		this.addChildren(resizeDots)

		for(EdgePoint dot:resizeDots){
			dot.moveToFront()
		}
		this.setChildrenPickable(true);
		polygon.setPickable(true);
		selectPolygon.setPickable( false )
		gridLayer.setPickable( false )
		gridLayer.setChildrenPickable( false )
	}

	private void constructPoints(){

		//log.trace("construct points");
		String pointsStr = (String) component.getPropertyValue("points");
		int index =0;

		for(String xyStr:pointsStr.split(";")){
            String[] strParts = xyStr.split(",")
            if( strParts.size() < 2 ) {
                log.trace("Skipping incomplete point string: '${xyStr}' from '${pointsStr}'")
                continue
            }
            String xStr = strParts[0];
            String yStr = strParts[1];

			//log.trace("x:" + xStr);
			//log.trace("y:" + yStr);

			float x = Float.valueOf(xStr).floatValue()
			float y = Float.valueOf(yStr).floatValue();

			Point2D.Float point = new Point2D.Float(x, y);
			// compare existing points and dots and update them
			if(index>= points.size()){
				points.add(index, point);
				EdgePoint dot = new EdgePoint(point, index);
				resizeDots.add(index, dot);
				this.addChild(dot);
				dot.moveToFront()

			}else{
				Point2D.Float oldPoint = points.get(index);
				//if different update them
				if(!point.equals(oldPoint)){
					points.remove(oldPoint);
					points.add(index, point);

					EdgePoint dot = new EdgePoint(point, index);
					EdgePoint oldDot = resizeDots.get(index);
					resizeDots.remove(oldDot);
					this.removeChild(oldDot);
					resizeDots.add(index, dot);
					this.addChild(dot);
					dot.moveToFront()
				}
			}
			index++;
		}

		// if remove point
		if(index<=points.size()){
			for(int i=index;i<points.size();i++){
				Point2D.Float oldPoint = points.get(i);
				EdgePoint oldDot = resizeDots.get(i);

				points.remove(oldPoint);
				resizeDots.remove(oldDot);
				this.removeChild(oldDot);
			}
		}
		lines = null
	}

	private void constructGrid(){
		gridLayer.removeAllChildren()

		if (!component.getType().equals(ComponentType.ROOM)) return
		if (!displayProperties.getProperty('floorTilesView')) return
		if( component.getPropertyValue( RoomType._PROPERTY ) != RoomType.RAISED_FLOOR ) return

		if (points.size() > 0){
			float tileSize = component.getPropertyValue('rfTileSize')

			// Determine the bounding rectangle for the shape.
		    float s1 = points.first().x;
			float n1 = points.first().x;
			float e1 = points.first().y;
			float w1 = points.first().y;

			points.each {
				n1 = (it.x < n1 )? it.x : n1
				s1 = (it.x > s1 )? it.x : s1
				e1 = (it.y > e1 )? it.y : e1
				w1 = (it.y < w1 )? it.y : w1
			}

			// Enlarge it so that we don't have blank areas if it is rotated.
			float a = s1 - n1
			float b = e1 - w1
			float newsize = Math.ceil( Math.sqrt( a*a + b*b ) )

			float n = n1 - ( newsize - a ) / 2.0
			float s = s1 + ( newsize - a ) / 2.0
			float w = w1 - ( newsize - b ) / 2.0
			float e = e1 + ( newsize - b ) / 2.0

			// Shift to align with the real room so we still get a natural alignment where a tile starts at 0,0
			n += (n1 - n) % tileSize
			w += (w1 - w) % tileSize

			// See if the user wants to shift from the natural alignment.
			double xoffset = component.getPropertyValue('rfTileOffsetX') % tileSize
			double yoffset = component.getPropertyValue('rfTileOffsetY') % tileSize

			// Build the grid lines!
			def gridLines = []

			for (float x = n + xoffset; x < s; x = x+tileSize){
				def line = PPath.createLine(x,e,x,w)
				line.setStrokePaint(GRID_COLOR)
				gridLines.add(line)
			}

			for (float y = w + yoffset; y < e; y = y+tileSize){
				def line = PPath.createLine(n,y,s,y)
				line.setStrokePaint(GRID_COLOR)
				gridLines.add(line)
			}

			gridLayer.addChildren(gridLines)

			rotateGrid()
		}
	}

	private void rotateGrid() {
		double rot = Math.toRadians( -component.getPropertyValue('rfTileRotation') ) - gridLayer.getRotation()
		gridLayer.rotateAboutPoint( rot, gridLayer.getFullBounds().getCenter2D() )
	}

	public void resetClipBounds(){
		gridClip.reset()
		gridClip.append(polygon.getPathReference(), false)
		component.getChildrenOfType(ComponentType.ROOM).each {
			ArbitraryShapeNode room = (ArbitraryShapeNode)deploymentPanel.getPNodeFor(it)
			if (room)
				gridClip.append(room.polygon.getPathReference(), false)
		}
		gridClip.getPathReference().setWindingRule(Path2D.WIND_EVEN_ODD)
		gridClip.setStroke(null)
	}

	/**
	 * reshape point and polygon boundary
	 */
	protected void reshape(){
		if(points.size()>0){
			constructShape()
			for(int j=0;j<resizeDots.size();j++){
				EdgePoint dot = resizeDots.get(j);
				dot.reshape(points.get(j));
			}
			resetClipBounds()
		}

		// Make sure the x/y center get updated since many functions in the system use it as a reference.
		ignoreXYchange = true
		component.setXY( polygon.getFullBounds().getCenter2D() );
		ignoreXYchange = false
	}

	private void constructShape() {
		if (points.size() > 0) {
			polygon.setPathToPolyline(points.toArray(new Point2D[points.size()]));

			if( component.hasProperty('lineColor') ) {
				lineColor = (Paint) component.getPropertyValue('lineColor')
				polygon.setStrokePaint( lineColor )
			} else {
				lineColor = polygon.getStrokePaint()
			}

			if( component.hasProperty('lineWidth') ) {
				lineWidth = (float) component.getPropertyValue('lineWidth')
				polygon.setStroke(new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER))
			} else {
				lineWidth = polygon.getStroke().getLineWidth()
			}

			if( component.hasProperty('fillColor') ) {
				int alpha = 0
				if( component.hasProperty('fillAlpha') ) {
					alpha = (int) component.getPropertyValue('fillAlpha')
				}

				// Set a null paint if it is transparent since Piccolo won't pick nodes below it if there is any paint,
				// even if you can't see it! This is confusing because it is not obvious that the shape is blocking the node.
				Color color = null
				if( alpha != 0 ) {
					color = (Color) component.getPropertyValue('fillColor')
					color = new Color( color.getRed(), color.getGreen(), color.getBlue(), alpha )
				}
				polygon.setPaint( color )
			}

			if( !component.getType().equals( ComponentType.DRAWING_LINE ) ) {
				polygon.closePath()
			}

			// and the select highlight under it.
			selectPolygon.setPathTo( polygon.getPathReference() )
			selectPolygon.setStrokePaint( DeploymentNode.selectColor )
			selectPolygon.setStroke(new BasicStroke((float)(lineWidth + HIGHLIGHT_WIDTH), BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER))
		}
	}

	/**
	 * PropertyChanged listener
	 * @param who
	 * @param prop
	 * @param oldVal
	 * @param newVal
	 */
	public void propertyChanged(Object who, String prop, Object oldVal, Object newVal) {
		//super.propertyChanged(who, prop, oldVal, newVal);
		// when x, y coordinate changes, should update points and side position, too
		switch( prop ) {
			case "x" :
			case "y" :
				if (isDragDotOnly || ignoreXYchange ) { return }

				// Protects when the x/y get changed elsewhere. Dragging internally already checks this during the drag.
				// No need to check during model changes since this is meant to catch normal changes, not undos or upgrades.
				if( !isDragging() && !component.getModel().isMetamorphosizing() ) {
					float x
					float y
					if(prop.equals("x")){
						x = (float) newVal
						y = (float) component.getPropertyValue("y")
					} else {
						x = (float) component.getPropertyValue("x")
						y = (float) newVal
					}

					Point2D p = ((NodeMover)toolManager.getTool("mover")).getPossiblePosition( new Point2D.Float( x, y ), polygon )
					if( ( Math.rint( x ) != p.x ) || ( Math.rint( y ) != p.y ) ) {
						throw new IllegalArgumentException("Component would be outside the drawing bounds. $who")
					}
				}

				float deltaX = 0.0f;
				float deltaY = 0.0f;

				if(prop.equals("x")){
					deltaX = (float) ((Double)newVal - (Double)oldVal);
				}

				if(prop.equals("y")){
					deltaY = (float) ((Double)newVal - (Double)oldVal);
				}

				updatePoints(deltaX, deltaY);
				break

			case "points" :
				constructPoints();
				constructGrid();
				reshape();
				break

			case "lineColor" :
			case "lineWidth" :
			case "fillColor" :
			case "fillAlpha" :
				reshape()
				break

			case RoomType._PROPERTY :
			case "rfTileSize" :
			case "rfTileOffsetX" :
			case "rfTileOffsetY" :
				constructGrid();
				break

			case "rfTileRotation" :
				rotateGrid();
				break
		}
	}

	/**
	 * DisplayChange event listener
	 * @param who
	 */
	public void displayPropertyChanged(Object who){
     /*
		if contained polygon is included in invisible object list, hide it
		 */

        boolean isDisplayable = false
        if(displayProperties.isActiveDrawingComponent(component)){
            ArrayList<String> unselectedTypes = (ArrayList<String>)displayProperties.getProperty("invisibleObjects");
            boolean isSelectedType = !unselectedTypes.contains(component.getType())
            isDisplayable = isSelectedType
 	    }
        this.setVisible(isDisplayable);
        this.setPickable(isDisplayable);
        this.setChildrenPickable(isDisplayable);
		this.constructGrid()
    }
	/**
	 * update points' location with offsetvalue
	 * @param deltaX
	 * @param deltaY
	 */
	private void updatePoints(float deltaX, float deltaY){
		ArrayList<Point2D.Float> tempPoints = (ArrayList<Point2D.Float>) points.clone();

		for(Point2D.Float p:tempPoints){
			// Keep the point on the whole numbers.
			p.setLocation( Math.rint( p.getX() + deltaX ), Math.rint( p.getY()+ deltaY ) );
		}
		component.setPropertyValue("points", getPointsString(tempPoints));
	}
	/**
	 * update a designated point with new point
	 * @param index
	 * @param newPoint
	 */
	private void updatePoint(int index, Point2D newPoint){
		ArrayList<Point2D.Float> tempPoints = (ArrayList<Point2D.Float>) points.clone();

		// update polygon path point
		Point2D.Float p = tempPoints.get(index);
		p.setLocation(newPoint);
		
		component.setPropertyValue("points", getPointsString(tempPoints));
	}
	
	/**
	 * delete point by index
	 * @param index
	 */
	private void deletePoint(int index){

		ArrayList<Point2D.Float> tempPoints = (ArrayList<Point2D.Float>) points.clone();
		tempPoints.remove(index);

		// Make sure the new shape is still valid.
		ShapeValidations isValidShape = StaticValidator.validateShape( tempPoints, minPoints, component.type )

		if( isValidShape != ShapeValidations.VALID_SHAPE ) {
			String msg
			if( isValidShape == ShapeValidations.LESS_MIN_POINTS ) {
				msg = CentralCatalogue.formatUIS("PopupMenu.containedArea.minNumberPoints", minPoints)
			} else if( isValidShape == ShapeValidations.CROSSED_EDGE ) {
				msg = uiStrings.getProperty("PopupMenu.containedArea.crossedEdges")
			} else {
				msg = uiStrings.getProperty("PopupMenu.containedArea.invalidShape")
			}

			log.error( "Unable to delete point.\n" + msg, "message" );
			return
		}

		// Make sure we don't intersect other polygons.
		ArrayList<Line2D> linesToCheck = convertPointsToLines(tempPoints)
		fetchPolygonCache()
		for (ArbitraryShapeNode pnode : cachedPolygons) {
			for (Line2D l : linesToCheck){
				if (pnode.intersects(l)) {
					log.error("Unable to delete point.\n"+uiStrings.getProperty("PopupMenu.containedArea.intersectingShapes"), "message");
					releasePolygonCache()
					return
				}
			}
		}
		component.setPropertyValue("points", getPointsString(tempPoints));
		releasePolygonCache()
	}
	
	/**
	 * Add a point with new point and index ( can insert new point between two points)
	 * @param point
	 * @param index
	 */
	private void addPoint(Point2D point, int index){
		ArrayList<Point2D> tempPoints = (ArrayList<Point2D>) points.clone();
		tempPoints.add( index, ((NodeMover)toolManager.getTool("mover")).getPossiblePosition( point ) )
		component.setPropertyValue("points", getPointsString(tempPoints));

		// Clear the slection or trying to grab the dot often grabs the whole shape.
		selectModel.clearSelection()
	}

   /**
	 * get the list of points as string value
	 * @return
	 */
	private String getPointsString(){
		String pointsStr = "";
		
		for(int i=0;i<points.size();i++){
			Point2D.Float p = points.get(i);
			
			float x = (float)p.getX();
			float y = (float)p.getY();
			pointsStr+=x + "," + y;
			
			if(i<(points.size()-1))
				pointsStr+=";";
			
		}
		
		return pointsStr;
	}

	private String getPointsString(ArrayList<Point2D> pointsList){
		String pointsStr = "";

		for(int i=0;i<pointsList.size();i++){
			Point2D p = pointsList.get(i);

			float x = (float)p.getX();
			float y = (float)p.getY();
			pointsStr+=x + "," + y;

			if(i<(pointsList.size()-1))
				pointsStr+=";";

		}

		return pointsStr;
	}

	/**
	 * get all points info
	 * @return
	 */
	public ArrayList<Point2D.Float> getPoints(){
		return points;
	}

	/**
	 * Get the polygon as a list of lines.
	 * @return
	 */
	public ArrayList<Line2D> getLines() {
		if( lines == null ) {
			lines = convertPointsToLines( points )
		}
		return lines
	}

	 /**
	 * get point's index by pressed point
	 * @param pressedPoint
	 * @return
	 */
	private int getNewPointIndex(Point2D pressedPoint){
		HashMap<Integer, Line2D.Float> lineMap = new HashMap<Integer, Line2D.Float>();

		for(int i=0;i<(points.size()-1);i++){
			Line2D.Float line = new Line2D.Float(points.get(i), points.get(i+1));
			lineMap.put(i, line);
		}

		if( !component.getType().equals( ComponentType.DRAWING_LINE ) ) {
			Line2D.Float line = new Line2D.Float(points.get(points.size()-1), points.get(0));
			lineMap.put(points.size()-1, line);
		}
		
		HashMap<Integer, Double> unsortedMap = new HashMap<Integer, Double>();
		
		for(int i=0;i<lineMap.size();i++){
			Line2D.Float l = lineMap.get(i);
			double distance =  l.ptLineDist(pressedPoint);
			unsortedMap.put(i, distance);
		}
		
		HashMap<Integer, Double> sortedMap = sortByComparator(unsortedMap);
		
		Object[] entrySet = sortedMap.entrySet().toArray();
		
		//log.info("fist point:" + ((Map.Entry)entrySet[0]).getKey());
		//log.info("second point:" + ((Map.Entry)entrySet[1]).getKey());
		
		int firstPoint = (Integer) ((Map.Entry)entrySet[0]).getKey();
		return firstPoint+1;
		
	}
	
	/**
	 * sort current points to find new point's index
	 * @param unsortedMap
	 * @return
	 */
	private HashMap sortByComparator(HashMap unsortedMap){
		List list = new LinkedList(unsortedMap.entrySet());
		
		Collections.sort(list, new Comparator(){
			public int compare(Object o1, Object o2){
				return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
			}
		});
		
		HashMap sortedMap = new LinkedHashMap();
		for(Iterator it = list.iterator(); it.hasNext();){
			Map.Entry entry = (Map.Entry)it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		
		return sortedMap;
	}

     /**
     * get orthogonal point for neighbor points
     * @param p
     * @return
     */
    private Point2D getOrthoPoint(Point2D p, int index){
	    Point2D newPt = p

	    if( points.size() > 1 ) {
		    Point2D otherA
		    Point2D otherB

		    if( component.getType() == ComponentType.DRAWING_LINE ) {
			    // Open shape, so use the curser point for the ends.
			    otherA = ( index == ( points.size() - 1 ) ) ? p : points.get( index + 1 )
			    otherB = ( index == 0 ) ? p : points.get( index - 1 )
		    } else {
			    // Closed shape, so wrap to pick the previous/next point.
			    otherA = points.get( (index + 1) % points.size() )
			    otherB = ( points.size() == 2 ) ? p : points.get( (index - 1 + points.size()) % points.size() )
		    }

		    Point2D possPt1 = new Point2D.Float( (float) otherA.getX(), (float) otherB.getY() )
			Point2D possPt2 = new Point2D.Float( (float) otherB.getX(), (float) otherA.getY() )

		    if( p.distance( possPt1 ) < p.distance( possPt2 ) ) {
			    newPt = possPt1
		    } else {
			    newPt = possPt2
		    }
	    }

        return newPt
    }
	/**
	 * override disconnect method of deploymentNode
	 */
	void disconnect() {
		log.trace("call disconnect in arbiraryShapeNode");
		component.removePropertyChangeListener(this);
		component.getDrawing().removePropertyChangeListener(this);
		component = null;
		displayProperties.removePropertyChangeListener( this );

	}

	public PNode getShape() {
		return polygon
	}

	/**
	 * override setSelected method
	 * @param s
	 * @return
	 */
	public boolean setSelected(boolean s) {
		super.setSelected( s )
		selectPolygon.setVisible( selected )
		return true
	}

	public void setHovered(boolean h) {
		super.setHovered( h )
		if( !isDragging() ) {
			setResizeDotsVisible( h )
		}
	}

	private void setResizeDotsVisible( boolean v ) {
		float t = ( v ? 1 : 0 )
		for( EdgePoint dot : resizeDots ) {
			dot.setTransparency( t )
		}
	}

	/**
	 * Override the default so that we can pick this node when the underlying polygon is what was selected and not
	 * pick the node when a corner dot is selected since we handle that ourselves.
	 *
	 * @param pickPath
	 * @return
	 */
	protected boolean pick( PPickPath pickPath ) {
		// Check to see if this is over a point and don't pick if it is.
		if( !isDragging() ) {
			for( EdgePoint dot : resizeDots ) {
				if( dot.intersects( pickPath.getPickBounds().getBounds2D() ) ) {
					return false
				}
			}

			// This does the same thing as the standard PPath.intersects except that it will only pick along the actual
			// polygon lines and not the inner fill space. It's essentially a copy and paste of that code with the paint
			// intersection statement removed. Replaces: return polygon.intersects( pickPath.getPickBounds() )
			if( polygon.getBoundsReference().intersects( pickPath.getPickBounds() ) ) {
				return polygon.getStroke().createStrokedShape( polygon.getPathReference() ).intersects( pickPath.getPickBounds() )
			}
		}

		return false
	}

    /**
     * set ortho mode, change deploymentPanel's cursor for ortho mode or normal mode
     * @param isOrtho
     */
    public void setOrthoMode(boolean isOrtho){
        //log.trace("setOrthodmode:" + isOrtho)
        if(isOrtho){

            deploymentPanel.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
            isOrthoMode = true;
           // log.info("Containment Polygon Drawing Ortho Mode", "statusbar");
        }else{
            deploymentPanel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            isOrthoMode = false;
           // log.info("", "statusbar");
        }
    }

	public boolean intersects( Line2D otherLine ) {
		for( Line2D thisLine : getLines() ) {
			if( thisLine.intersectsLine( otherLine ) ) {
				// Intersects returns true if they are directly on top of each other, which we consider ok when we
				// want to cleanly align the rooms on the drawing so that they are directly on top of each other.
				if( !( thisLine.ptLineDist( otherLine.getP1() ) == 0 ||
				       thisLine.ptLineDist( otherLine.getP2() ) == 0 ||
				       otherLine.ptLineDist( thisLine.getP1() ) == 0 ||
				       otherLine.ptLineDist( thisLine.getP2() ) == 0 ) ) {
					return true
				}
			}
		}
		return false
	}

	public boolean intersects( ArbitraryShapeNode otherShape ){
		for( Line2D otherLine : otherShape.getLines() ) {
			if( this.intersects( otherLine ) ) {
				return true
			}
		}
		return false
	}

	private void fetchPolygonCache(){
		cachedPolygons = new HashSet<>()
		log.trace("Fetch polygons to temp cache...")
		selectModel.getActiveDrawing().getChildComponents().each {dlc ->
			if (dlc.getType().equals("room")){
				ArbitraryShapeNode roomNode = deploymentPanel.getPNodeFor(dlc)
				if (roomNode) {
					cachedPolygons.add(roomNode)
				}
			}
		}
		cachedPolygons.remove(this);
		log.trace("${cachedPolygons.size()} polygons fetched.")
	}

	private void releasePolygonCache(){
		if (cachedPolygons){
			cachedPolygons.clear();
			cachedPolygons = null;
			log.trace("Polygon cache cleared.")
		}
	}

	public static ArrayList<Line2D> convertPointsToLines(ArrayList<Point2D> points){
		ArrayList<Line2D.Double> lines = []

		for (int i = 0; i<points.size()-1; i++){
			if (i == 0) {
				lines.add(new Line2D.Double(points.get(points.size() - 1), points.get(0)))

			}
			lines.add(new Line2D.Double(points.get(i), points.get(i + 1)))
		}

		return lines
	}

	/**
	 * The class to handle mouse and keyboard event for points and sides of polygon
	 */
	class ShapeInputEventHandeler extends PBasicInputEventHandler{
        private static final Logger log = Logger.getLogger(ShapeInputEventHandeler.class.getName());

		private PPath selectedDot = null;
		private Point2D.Float selectedPoint = null;
		private boolean isHovering = false

		private JPopupMenu contextMenu = new JPopupMenu();

        /**
		 * construct input evnet handler to designated object (side or point)
		 * @param object
		 */
		public ShapeInputEventHandeler(){
			initContextMenu();
		}

        public void mouseMoved(PInputEvent e){
        //log.info("mouse moved");

            PNode node = e.getPickedNode();
            if(node.getAttribute(ATTRIBUTE_TYPE).equals(ATTRIBUTE_TYPE_EDGE)){
                //log.trace("move mouse inside dot")
                if(e.isShiftDown()){
                    //log.trace("shift is down start ortho mode");
                    setOrthoMode(true);
                } else{
                    setOrthoMode(false);
                }
            }
        }

        public void mouseEntered(PInputEvent e){
            //log.trace("mouseEntered");
            PNode node = e.getPickedNode();
			//log.trace("mouse entered node:" + node.getAttribute(ATTRIBUTE_TYPE).toString())
            // just make selected dot red
            if(node.getAttribute(ATTRIBUTE_TYPE).equals(ATTRIBUTE_TYPE_EDGE)){
	            setHovering( node, true )

	            // Let the node listener get the DELETE key when hovering.
	            e.getInputManager().setKeyboardFocus( node.getInputEventListeners().first() )

                if(e.isShiftDown()){
                //log.trace("shift is down start ortho mode");
                    setOrthoMode(true);
                } else{
                    setOrthoMode(false);
                }
            }
        }

        public void mouseExited(PInputEvent e){
            //log.trace("mouseExited");
            PNode node = e.getPickedNode();
            //log.trace("mouse exited node:" + node.getAttribute(ATTRIBUTE_TYPE).toString())
            if(node.getAttribute(ATTRIBUTE_TYPE).equals(ATTRIBUTE_TYPE_EDGE)){
	            setHovering( node, false )
	            if( !contextMenu.isShowing() ) {
	                releaseSelectedDot()

	                setOrthoMode(false)
                }
            }

        }


		/**
		 * mouseDragged event, move selected point
		 * @param e
		 */
		public void mouseDragged(PInputEvent e){

			super.mouseDragged(e);

			// Ignore all drags if it's not the left mouse.
			if( !e.isLeftMouseButton() ) return

            // Makes the undo go back to the original spot, not just the last micro drag update.
            if( !isDragging() ) {
                setDragging( true )
	            isDragDotOnly = true
                undoBuffer.startOperation()
            }

			PNode node = e.getPickedNode();

			if(node instanceof DeploymentNode){
				//log.info("dragged deploymentnode");
			}else{

				while(node.getAttribute(ATTRIBUTE_TYPE)==null){
					node = node.getParent();
				}

				if(node.getAttribute(ATTRIBUTE_TYPE).equals(ATTRIBUTE_TYPE_EDGE)){
                    // don't move selected dot when context menu is showing
                    if(contextMenu.isShowing()){
                        //log.trace("contextmenu is showing");
                        return;
                    }

                    Integer index =((EdgePoint)node).getIndex();

                    double scale = (Double) CentralCatalogue.getDeploymentPanel().getDrawingScale();

                    Point2D p = e.getPosition();

					p = new Point2D.Float((float)p.getX()*(float)scale,(float)p.getY()*(float)scale);

                    if(isOrthoMode){
                        p = getOrthoPoint(p, index);
                    }

					p = ((NodeMover)toolManager.getTool("mover")).getPossiblePosition( p );

					Line2D l1 = new Line2D.Double(p,points.get((index!=(points.size()-1))?index+1:0)) ;
					Line2D l2 = new Line2D.Double(points.get((index!=0)?index-1:points.size()-1), p) ;

					for (ArbitraryShapeNode pnode : cachedPolygons) {
						if (pnode.intersects(l1) || pnode.intersects(l2)) {
							releaseSelectedDot();
							log.error(uiStrings.getProperty("PopupMenu.containedArea.crossedEdges"), "statusbar");
							e.setHandled(true);
							return;
						}
					}

					//log.info("dragged point[" +index + "]" + p.getX() + ";" + p.getY());

                    // The error log lines were changed from a "message" popup to a "statusbar" error because the popup
                    // caused event issues and mouseReleased isn't called (Piccolo or Swing bug?), which put some things
                    // in a funky state. This change also has a nice effect where your drag is not interrupted yet the
                    // shape will still not change as long as the mouse is in a spot that creates an invalid shape.
					ShapeValidations shapeValidation = validateShape(index,p);
					if(shapeValidation ==ShapeValidations.VALID_SHAPE){
						updatePoint(index, p);
					}else if(shapeValidation == ShapeValidations.LESS_MIN_POINTS){
						releaseSelectedDot();
						log.error( CentralCatalogue.formatUIS("PopupMenu.containedArea.minNumberPoints", minPoints), "statusbar");
                        e.setHandled(true);
                    }else if(shapeValidation == ShapeValidations.CROSSED_EDGE){
                        releaseSelectedDot();
						log.error(uiStrings.getProperty("PopupMenu.containedArea.crossedEdges"), "statusbar");
                        e.setHandled(true);
	    			}else{
                        releaseSelectedDot();
						log.error(uiStrings.getProperty("PopupMenu.containedArea.invalidShape"), "statusbar");
                        e.setHandled(true);
					}
				}
			}
		}
		/**
		 * mousePressed event to select a point and show context menu
		 * @param e
		 */
		public void mousePressed(PInputEvent e){
			//log.trace("mousePressed");
			PNode node = e.getPickedNode();

			Double x = e.getPosition().getX();
			Double y = e.getPosition().getY();

			selectedPoint = new Point2D.Float(x.floatValue(), y.floatValue());
			if(node instanceof DeploymentNode){
				//log.info("pressed deploymentnode");
			}else{

				if(node==null){
					return;
				}

                //log.trace("pressed node:" + node.getAttribute(ATTRIBUTE_TYPE).toString())
				if(node.getAttribute(ATTRIBUTE_TYPE).equals(ATTRIBUTE_TYPE_EDGE)){
					// set selected dot
                    setSelectedDot(node);
                    // hold tools during editing point
                    if(toolManager.getSuspendedTools()==null){
                        toolManager.suspendTools();
                    }

                    if(e.isShiftDown()){

                        setOrthoMode(true);
                    }
					fetchPolygonCache();
					//log.trace("pressed edge with leftbutton index:" + ((EdgePoint)selectedDot).getIndex());
				}
                // right mouse click, open context menu for add a point or delete a point
				if(e.isRightMouseButton()){
					if(node.getAttribute(ATTRIBUTE_TYPE).equals(ATTRIBUTE_TYPE_EDGE)){           // when selecting edge, show add a point
						contextMenu.show((Component) e.getComponent(), (int)e.getCanvasPosition().getX(), (int)e.getCanvasPosition().getY());
						e.setHandled(true);
					}
				}
			}
		}
	   /**
		 * mouseReleased event to unselect selected point
		 * @param e
		 */
        public void mouseReleased(PInputEvent e){
            //log.trace("mouseReleased");

            // Makes the undo go back to the original spot, not just the last micro drag update.
            if( isDragging() ) {
                setDragging( false )
	            isDragDotOnly = false
                setOrthoMode(false)
	            component.setPropertyValue("points", component.getPropertyValue("points"))
	            undoBuffer.finishOperation()
            }

			PNode node = e.getPickedNode();
			//log.trace("mouseReleased node:" + node.getAttribute(ATTRIBUTE_TYPE).toString())
            if(node.getAttribute(ATTRIBUTE_TYPE).equals(ATTRIBUTE_TYPE_EDGE)){
                if(!contextMenu.isShowing()){
                    //log.trace("mouseReleased from edge point")
                    releaseSelectedDot();
                }
           }
		}
		/**
		 * keyPressed event to delete selected point
		 * @param e
		 */
		public void keyPressed(PInputEvent e){
			//log.info("key pressed" + e.getKeyCode());
            //int v = KeyEvent.VK_SHIFT;
            //log.trace(v.toString());

			if(e.getKeyCode()==KeyEvent.VK_DELETE){
				//log.info("delete key pressed");
				if( selectedDot != null && !isDragDotOnly ) {
					if( points.size() <= minPoints ) {
						log.info(CentralCatalogue.formatUIS("PopupMenu.containedArea.minNumberPoints", minPoints), "message");
                        releaseSelectedDot();
						return;
					}

					int index = ((EdgePoint)selectedDot).getIndex();
					deletePoint(index);

					setHovering( selectedDot, false )
					releaseSelectedDot()
				}
			} else if(e.getKeyCode()== KeyEvent.VK_SHIFT){
                //log.trace("shift key pressed");
                setOrthoMode(true)
            }
		}


        public void keyTyped(PInputEvent e){
            //log.trace("keytyed" + e.getKeyChar())
        }

        public void keyReleased(PInputEvent e){
	        if(e.getKeyCode()== KeyEvent.VK_SHIFT){
		        setOrthoMode(false)
	        }
        }

	  /**
		 * initialize context menu
		 * Create a actionlistener to add a point or delete it
		 */
		private void initContextMenu(){
			//ActionListener to delete point
			ActionListener deleteListener = new ActionListener(){

				@Override
				public void actionPerformed(ActionEvent e) {
					log.trace("actionPerformed delete");
					deleteDot();
			}

			};

			JMenuItem deleteMenu = new JMenuItem(uiStrings.getProperty("PopupMenu.containedArea.deletePoint"));
			deleteMenu.addActionListener(deleteListener);
			contextMenu.add(deleteMenu);

            contextMenu.addPopupMenuListener(new PopupMenuListener(){
                public void popupMenuCanceled(PopupMenuEvent e){
                    //log.trace("popupMenuCanceled");
	                // Explicit hide so release dot works. otherwise, it is still "showing" at this point.
	                contextMenu.setVisible( false )
                    releaseSelectedDot();
                }

                void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                    //log.trace("popupmenu visible");
              }

                void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                    //log.trace("popup menu invisible");
                }
            });
		}

        /**
         * Delete selected dot
         */
        private void deleteDot(){

            if(selectedDot!=null){
                if( points.size() <= minPoints ) {
                    log.info(CentralCatalogue.formatUIS("PopupMenu.containedArea.minNumberPoints", minPoints), "message");
                    releaseSelectedDot();
                    return;
                }
                int index = ((EdgePoint) selectedDot).getIndex();

                //log.info("index to delete:" + index);
                deletePoint(index);

	            setHovering( selectedDot, false )
	            releaseSelectedDot()
            }
        }
		/**
		 * verify new point coordinate for valid polygon shape
		 * @param index
		 * @param newPoint
		 * @return
		 */

		private ShapeValidations validateShape(int index, Point2D newPoint){

			ArrayList<Point2D.Float> realPoints = new ArrayList<Point2D.Float>();
			realPoints.addAll(points);
			//log.trace("realPoints size:" + realPoints.size());
			//log.trace("new index:" + index);

			if(index>=realPoints.size()){
				log.trace("invalid index to validate");
				return ShapeValidations.UNKNOWN_INVALID_SHAPE;
			}

			realPoints.set(index, new Point2D.Float((float)newPoint.getX(), (float)newPoint.getY()));

			return StaticValidator.validateShape( realPoints, minPoints, component.type );

		}

		void setHovering( PNode node, boolean hovering ) {
			isHovering = hovering
			if( isHovering ) {
				setSelectedDot( node )
			}
		}

        /**
         * set selectedDot
         * color dot red and set selectedDot instance variable
         * @param node
         */
        private void setSelectedDot(PNode node){
	        // Ignore if the context menu is up because we don't want the selection to change during the operation.
	        if( !contextMenu.isShowing() && !isDragDotOnly ) {
		        node.setTransparency(1)
		        selectedDot = (PPath)node;
	        }
        }

        /**
         * release selectedDot
         */
        private void releaseSelectedDot(){
	        // Ignore if the context menu is up because we don't want the selection to change during the operation.
	        if( !contextMenu.isShowing() && !isDragDotOnly ) {
		        if( !isHovering && selectedDot != null ) {
			        selectedDot.setTransparency(0)
			        selectedDot = null
		        }

		        if( toolManager.getSuspendedTools() != null ) {
			        toolManager.resumeTools();
		        }

		        releasePolygonCache();
	        }
        }
	}
	/**
	 *  The class for side shape of polygon
	 */
   public class EdgePoint extends PPath{
		int index;
		
		public EdgePoint(Point2D.Float point, int index){
			reshape( point )
			this.addAttribute(ATTRIBUTE_TYPE, ATTRIBUTE_TYPE_EDGE);
			this.addInputEventListener(new ShapeInputEventHandeler());
			this.setPaint( PHandle.DEFAULT_COLOR );
			this.setTransparency(0)

			this.index = index;
		}
		
		public void reshape(Point2D.Float point){
			// Make sure it is always bigger than the line.
			float trueDiam = PHandle.DEFAULT_HANDLE_SIZE - 1 + lineWidth
			this.setPathToEllipse(( float)(point.x - trueDiam / 2), (float)(point.y - trueDiam / 2), trueDiam, trueDiam );
		}
		
		public void setIndex(int index){
			this.index = index;
		}
		
		public int getIndex(){
			return index;
		}
	}
}