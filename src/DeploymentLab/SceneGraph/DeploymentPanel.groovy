package DeploymentLab.SceneGraph

import DeploymentLab.Image.ComponentIconFactory
import DeploymentLab.Image.DLImage
import DeploymentLab.SceneGraph.DynamicChildrenNode.DynamicChildNode
import DeploymentLab.SceneGraph.StaticChildrenNode.StaticChildNode
import DeploymentLab.Tools.NodeOperator
import DeploymentLab.Tools.ScaleSetter
import DeploymentLab.Tools.ToolManager
import DeploymentLab.Transfer.DeploymentPanelTransferHandler
import DeploymentLab.channellogger.Logger
import com.synapsense.util.unitconverter.SystemConverter
import edu.umd.cs.piccolo.PCamera
import edu.umd.cs.piccolo.PCanvas
import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.nodes.PImage
import edu.umd.cs.piccolo.nodes.PPath
import edu.umd.cs.piccolo.nodes.PText
import edu.umd.cs.piccolo.util.PBounds
import edu.umd.cs.piccolo.util.PDimension
import edu.umd.cs.piccolo.util.PPaintContext
import edu.umd.cs.piccolox.swing.PScrollPane

import java.awt.event.MouseEvent
import java.awt.event.MouseListener
import java.awt.geom.Line2D
import java.awt.geom.Point2D
import java.awt.image.BufferedImage
import java.beans.PropertyChangeEvent
import java.beans.PropertyChangeListener
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.Collection
import java.util.List

import DeploymentLab.*
import DeploymentLab.Model.*

import java.awt.*
import javax.swing.*

/**
 * Handles the main window of Deployment Lab - you know, with the icons and the dragging and whatnot.
 *
 */
class DeploymentPanel extends PScrollPane implements SelectionChangeListener, ModelChangeListener, MouseListener {

	private static final Logger log = Logger.getLogger(DeploymentPanel.class.getName())

	private SelectModel selectModel
	private UndoBuffer undoBuffer

	private PNode root
	private PNode componentLayer
	private PNode nodeLayer
	private PNode haloLayer
	private PNode associationLayer
	private PPath scaleShape
	private PNode pointLayer
	private PNode betaLayer

	private ScaleSetter scaler
	private double currentScale = 1.0

	private haloMap = [:] //map of component : halonode
	private componentNodeMap = [:]
	private HashMap<Pair<DLComponent, DLComponent>, DeploymentAssociation> associationMap = new HashMap<Pair<DLComponent, DLComponent>, DeploymentAssociation>()

	private boolean antialiasing = true
	private boolean scaling

	private PImage backgroundImage
	private PNode backgroundBlank
	private PCanvas canvas
	private PCamera camera

	private PPath selectShape

	private int backgroundWidth = 0
	private int backgroundHeight = 0

	private int blankCanvasWidth = 1800
	private int blankCanvasHeight = 1200

	private ComponentModel componentModel
	private DisplayProperties displayProperties
	public ToolManager toolManager

	private SystemConverter systemConverter

	// grid layer
	private Line2D gridLine = new Line2D.Double()
	private Stroke gridStroke = new BasicStroke()
	private Color gridColor = Color.black
	private Color girdLineColor = Color.gray
	private double gridSpacing = 10
	private PNode gridLayer

	// Saves the drawing views so that it doesn't reset to "fit" every time you change drawings. the currently visible
	// drawing needs to be saved because oldDrawing isn't always the same as the drawing that is currently visible.
	private DLComponent visibleDrawing = null
	private Map<DLComponent,Pair<PBounds,Double>> drawingViews = [:]


	/**
	 * Indicates if the scene graph should pay attention to model change events.
	 * For the moment, this is always true except for during model clears.
	 */
	//private boolean isLoading

	DeploymentPanel(SelectModel sm, UndoBuffer ub, ComponentModel cm, ToolManager tm, DisplayProperties dp) {
		//some slight shenanigans to make sure that the PScrollPane super() gets called correctly
		super(new PCanvas())
		canvas = (PCanvas) this.getViewport().getView()

		toolManager = tm
		displayProperties = dp
		componentModel = cm
		undoBuffer = ub
		componentModel.addModelChangeListener(this)
		dp.addPropertyChangeListener(this, this.&displayPropertyChanged)

		//canvas = new PCanvas()
		canvas.setDefaultRenderQuality(PPaintContext.HIGH_QUALITY_RENDERING)
		canvas.setInteractingRenderQuality(PPaintContext.HIGH_QUALITY_RENDERING)
		canvas.setPanEventHandler(null)
		canvas.setZoomEventHandler(null)
		//set the background to the LaF default gray for a canvas; also, make sure this color is NOT TRANSPARENT
		canvas.setBackground(this.getBackground())

		//canvas.addInputEventListener(toolManager)

		setWheelScrollingEnabled(false)

		selectModel = sm

		scaling = false

		// set background image
		backgroundImage = new PImage()
		backgroundBlank = new PNode()
		backgroundBlank.setVisible(true)

		//make sure the background also gets the input listener
		//backgroundImage.addInputEventListener(toolManager)
		//backgroundBlank.addInputEventListener(toolManager)


		haloLayer = new PNode()
		nodeLayer = new PNode()
		associationLayer = new PNode()
		pointLayer = new PNode()
		gridLayer = new PNode()
		betaLayer = new PNode()

		selectShape = new PPath()
		selectShape.setPaint(new Color((float) 0.5, (float) 0.5, (float) 0.5, (float) 0.5))
		selectShape.setStrokePaint(Color.black)
		selectShape.setVisible(false)

		componentLayer = new PNode()
		//componentLayer.addChild(selectShape)
		componentLayer.addChild(haloLayer)
		componentLayer.addChild(associationLayer)
		componentLayer.addChild(pointLayer)
		componentLayer.addChild(nodeLayer)

		root = new PNode()
		root.addChild(backgroundBlank)
		root.addChild(backgroundImage)
		root.addChild(betaLayer)
		root.addChild(gridLayer)
		root.addChild(componentLayer)
		root.addChild(selectShape) //use it here to calculate correct component layer bounds
		root.addInputEventListener(toolManager)

		//initGridLayer()
		canvas.getLayer().addChild(root)

		camera = canvas.getCamera()
		camera.addPropertyChangeListener(PCamera.PROPERTY_VIEW_TRANSFORM, new CameraPropertyChangeListener(toolManager))
		camera.setViewScale(1.0d)
		//if the toolmanager is bolted to the camera, then all events eventually end up there,
		//since all pickpaths lead to rome^H^H^H^H^H the camera
		camera.addInputEventListener(toolManager)

		setViewportView(canvas)

		this.addMouseListener(this)

		//add CCP support to action map
		ActionMap map = canvas.getActionMap();
		//map.put(TransferHandler.getCutAction().getValue(Action.NAME), TransferHandler.getCutAction());
		map.put(TransferHandler.getCopyAction().getValue(Action.NAME), TransferHandler.getCopyAction());
		map.put(TransferHandler.getPasteAction().getValue(Action.NAME), TransferHandler.getPasteAction());

		NodeOperator no = new NodeOperator(selectModel, ub, componentModel, this)
		canvas.setTransferHandler(new DeploymentPanelTransferHandler(componentModel, ub, selectModel, no, this))
/*
		 this.getActionMap().allKeys().each{
			println "Action key $it : ${this.getActionMap().get(it)}"

		}
*/
		//clear out the arrow-key entries in the input map so they don't collide with our use for the arrow keys
		def inputMap = this.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
		inputMap.put(KeyStroke.getKeyStroke('pressed UP'), "nothingness")
		inputMap.put(KeyStroke.getKeyStroke('pressed DOWN'), "nothingness")
		inputMap.put(KeyStroke.getKeyStroke('pressed LEFT'), "nothingness")
		inputMap.put(KeyStroke.getKeyStroke('pressed RIGHT'), "nothingness")
/*
		this.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).allKeys().each{
			println "Input map key '$it' : ${this.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).get(it)}"
		}


*/      /*
        this.getHorizontalScrollBar().addAdjustmentListener(new AdjustmentListener(){
             public void adjustmentValueChanged(AdjustmentEvent evt){
                 log.trace("horizontal scroll bar changed")
                 log.trace(getViewportRect().toString())
                 //camera.setViewBounds(getViewportRect())
                 rect = camera.getViewBounds()
                 log.trace("camera view bounds when h scrollbar changed:" + rect.toString())


             }
        });*/
	}

	/**
	 * Save the component display order so we don't lose any reordering done by the user.
	 *
	 * @param builder
	 */
	void serialize( groovy.xml.MarkupBuilder builder ) {
		StringBuilder str = new StringBuilder( nodeLayer.getChildrenCount() * 5 )
		nodeLayer.getChildrenReference().each { node ->
			str.append( node.getComponent().getDlid().toString() )
			str.append(',')
		}

		builder.deploymentpanel() {
			'layer' ( name:'nodeLayer' ) {
				'nodeOrder' ( str.toString() )
			}
		}
	}

	/**
	 * Restore the component display order.
	 *
	 * @param objectsXml
	 */
	void deserialize( def dpXml ) {
		for( Object layerXml: dpXml.layer ) {
			String layer = layerXml.@name.text()
			if( layer == 'nodeLayer' ) {
				// See if we have a stored node order.
				def nodeOrder = layerXml.nodeOrder
				if( nodeOrder ) {
					List<String> savedComponentOrder = Arrays.asList( nodeOrder.text().split(",") )

					// Reorder the nodes based on the stored order.
					for( int i = 0; i < savedComponentOrder.size(); i++ ) {
						DLComponent c = componentModel.getComponentFromDLID( savedComponentOrder[i] )

						if( c == null ) {
							// May have been an upgrade and the id changed on us.
							Dlid dlid = CentralCatalogue.getOpenProject().getProjectSettings().findNewDlid( Dlid.getDlid( savedComponentOrder[i] ) )
							if( dlid != null ) {
								c = componentModel.getComponentFromDLID( dlid )
							}
						}

						if( c == null ) {
							log.trace("Unable to set node order: No component found for '${savedComponentOrder[i]}'")
						} else {
							PNode node = componentNodeMap[c]

							if( node == null ) {
								log.trace("Unable to set node order: No node found for $c")
							} else {
								if( i != nodeLayer.indexOfChild( node ) ) {
									// Need to change the order.
									if( i == 0 ) {
										node.moveToBack()
									} else {
										node.moveInFrontOf( nodeLayer.getChild( i - 1 ) )
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public ToolManager getToolManager() {
		return toolManager
	}

	private Rectangle getViewportRect() {
		this.getViewportBorderBounds()
	}

	public def getHaloMap() {
		return haloMap
	}

	public PNode getComponentLayer() {
		return componentLayer
	}

	public void setGridSpacing(double spacing) {
		this.gridSpacing = spacing
	}

	public double getGridSpacing() {
		return this.gridSpacing
	}

	/**
	 * Returns the smallest valid canvas size that contains all of the components without shifting their X/Y
	 * positions.  This follows similar rules as the export validations and allows arbitrary component icons to hang
	 * over the boundary as long as their internal object position are within the bounds. There is also an absolute
	 * minimum size so that we don't have an unusable canvas.
	 *
	 * @return
	 */
	public Dimension getSmallestAllowedCanvas( DLComponent drawing = selectModel.getActiveDrawing() ) {
		double minX = 200
		double minY = 200
		for( DLComponent c : componentModel.getComponentsInRole( ComponentRole.PLACEABLE, drawing ) ) {
			if( c.hasRole( ComponentRole.ARBITRARY_SHAPE ) || c.hasRole( ComponentRole.RECTANGLE_SHAPE ) ) {
				// Just check the shape points.
				for( Point2D pt : this.getNodeFor( c ).getPoints() ) {
					minX = Math.max( minX, pt.getX() )
					minY = Math.max( minY, pt.getY() )
				}
			} else {
				// Check all the object locations
				for( DLObject o : c.getManagedObjects() ) {
					Double x = o.getObjectSetting('x')?.getValue()
					Double y = o.getObjectSetting('y')?.getValue()

					if( ( x != null ) && ( y != null ) ) {
						minX = Math.max( minX, x )
						minY = Math.max( minY, y )
					}
				}
			}
		}
		return new Dimension( (int) Math.ceil( minX ) / drawing.getScale(),
		                      (int) Math.ceil( minY ) / drawing.getScale() )
	}

	private void revalidateGrid() {
		//log.info("revalidateGrid")
		//log.info("gridColor:" + girdLineColor)
		gridLayer.removeAllChildren()

		for (float x = 0.0; x < blankCanvasWidth; x += gridSpacing) {
			PPath gridLine = PPath.createLine(x, 0.0f, x, blankCanvasHeight)
			gridLine.setStrokePaint(girdLineColor)

			gridLayer.addChild(gridLine)
		}

		for (float y = 0.0; y < blankCanvasHeight; y += gridSpacing) {
			PPath gridLine = PPath.createLine(0.0f, y, blankCanvasWidth, y)
			gridLine.setStrokePaint(girdLineColor)
			gridLayer.addChild(gridLine)
		}

	}

	public Point2D findNearestGridIntersectionTo(Point2D target) {
		Point2D result
		def tx = target.getX()
		int numberOfGridsX = (tx / gridSpacing)
		if (tx % gridSpacing > 0.5) {
			numberOfGridsX++
		}
		def ty = target.getY()
		int numberOfGridsY = (ty / gridSpacing)
		if (ty % gridSpacing > 0.5) {
			numberOfGridsY++
		}
		result = new Point2D.Double(numberOfGridsX * gridSpacing, numberOfGridsY * gridSpacing)
		return result
	}

	PPath highlightBubble

	public void highlightHere(Point2D target) {
		if (highlightBubble && gridLayer.isAncestorOf(highlightBubble)) {
			gridLayer.removeChild(highlightBubble)
		}
		highlightBubble = PPath.createEllipse((float) target.getX() - 5, (float) target.getY() - 5, 10, 10)
		highlightBubble.setPaint(Color.RED)
		gridLayer.addChild(highlightBubble)
	}

/*
	private void initGridLayer(){
		PRoot root = getCanvas().getRoot()
		final PCamera camera = getCanvas().getCamera()
		final PLayer gridLayer = new PLayer(){
			protected void paint (PPaintContext paintContext){

				//log.info("paint gridLayer")
				double bx =(getX() - (getX() % gridSpacing)) -gridSpacing
				double by =(getY() - (getY() % gridSpacing)) -gridSpacing
				double rightBorder = getX() + backgroundWidth + gridSpacing
				double bottomBorder = getY() + backgroundHeight + gridSpacing

				//log.info("getX():" + getX())
				//log.info("getY():" + getY())

				//log.info("bx;" + bx)
				//log.info("by;" + by)
				//log.info("rightBorder;" + rightBorder)
				//log.info("bottomBorder" + bottomBorder)

				Graphics2D g2 = paintContext.getGraphics()
				Rectangle2D clip = paintContext.getLocalClip()

				g2.setStroke(gridStroke)
				g2.setPaint(gridColor)

				for(double x=bx;x< rightBorder;x+=gridSpacing){
					gridLine.setLine(x, by, x, bottomBorder)
					if(clip.intersectsLine(gridLine)){
						g2.draw(gridLine)
					}
				}

				for(double y=by;y< bottomBorder;y+=gridSpacing){
					gridLine.setLine(bx, y, rightBorder, y)
					if(clip.intersectsLine(gridLine)){
						g2.draw(gridLine)
					}
				}

			}
		};

		root.removeChild(camera.getLayer(0))
		camera.removeLayer(0)
		root.addChild(gridLayer)
		camera.addLayer(gridLayer)
		gridLayer.moveToFront()

		camera.addPropertyChangeListener(PNode.PROPERTY_BOUNDS, new PropertyChangeListener(){
			public void propertyChange(PropertyChangeEvent evt){
				gridLayer.setBounds(camera.getViewBounds())
			}
		});

		camera.addPropertyChangeListener(PCamera.PROPERTY_VIEW_TRANSFORM, new PropertyChangeListener(){
			public void propertyChange(PropertyChangeEvent evt){
				gridLayer.setBounds(camera.getViewBounds())
			}
		});

		gridLayer.setBounds(camera.getViewBounds())
	}
*/

	void setUnitSystem(SystemConverter sc) {
		systemConverter = sc
	}

	PPath getSelectShape() {
		return selectShape
	}

	PCanvas getCanvas() {
		return canvas
	}

	void addNode(DeploymentNode dn) {
		dn.addInputEventListener(toolManager)
		componentNodeMap[dn.getComponent()] = dn
		nodeLayer.addChild(dn)
	}

	void addNode(ChildrenNode node) {
		node.addInputEventListener(toolManager)
		componentNodeMap[node.getComponent()] = node
		nodeLayer.addChild(node)
	}

	void addNode(DynamicChildrenNode node) {
		node.addInputEventListener(toolManager)
		componentNodeMap[node.getComponent()] = node
		nodeLayer.addChild(node)

		def childrenNodes = node.getChildrenNodes()
		childrenNodes.each {it -> addNode((DynamicChildNode) it)}

	}

	void addNode(DynamicChildNode node) {
		//log.info("DynamicChildNode added")
		componentNodeMap[node.getComponent()] = node
		nodeLayer.addChild(node)
	}

	void addNode(StaticChildrenNode node) {
		//log.info("StaticChildrenNode added")
		componentNodeMap[node.getComponent()] = node

		// add child nodes to componentNodeMap, too
		def childrenNodes = node.getChildrenNodes()
		childrenNodes.each {componentNodeMap[it.getComponent()] = it}

		nodeLayer.addChild(node)

	}

	void addNode(TextNode dn) {
		componentNodeMap[dn.getComponent()] = dn
		nodeLayer.addChild(dn)
	}

	void addPoint(PPath point) {
		//log.info("add point to deploymentPanel")
		pointLayer.addChild(point)
	}


	void removePoints(ArrayList<PPath> paths, ArrayList<PPath> points) {

		if( paths ) {
			for (PPath path : paths) {
				if (path.isDescendentOf(pointLayer)) {
					//log.info(path.toString() + " is a decendent of pointlayer")
					pointLayer.removeChild(path)
				}
			}
		}

		if( points ) {
			for (PPath point : points) {
				if (point.isDescendentOf(pointLayer)) {
					//log.info(point.toString() + " is a decendent of pointlayer")
					pointLayer.removeChild(point)
				}
			}
		}


	}

	void removePoint(PPath path) {
		if (path && path.isDescendentOf(pointLayer)) {
			pointLayer.removeChild(path)
		}
	}

	DynamicChildrenNode getPNode(DLComponent c) {
		if (!componentNodeMap.containsKey(c)) {
			return null
		} else {
			return (componentNodeMap[c])
		}

	}

	/**
	 * Gets the node that wraps a given component.
	 */
	DeploymentNode getNodeFor(DLComponent c) {
		if (!componentNodeMap.containsKey(c)) {
			return null
		} else {
			return (componentNodeMap[c])
		}
	}


	PNode getPNodeFor(DLComponent c) {
		if (!componentNodeMap.containsKey(c)) {
			return null
		} else {
			return (componentNodeMap[c])
		}
	}

	public void positionAsSecondary(DLComponent host, DLComponent secondary) {
		secondary.setPropertyValue("x", host.getPropertyValue("x"))
		secondary.setPropertyValue("y", host.getPropertyValue("y"))
		def dn = this.getNodeFor(secondary)
		dn.adjustOffset()
	}

	/**
	 * Gets any and all association nodes that the given component is a part of.
	 */
	List<DeploymentAssociation> getAssociationsFor(DLComponent c) {
		return (associationMap.values().findAll { it.getConsumer() == c || it.getProducer() == c }.asList())
	}

	/**
	 * Gets any and all association nodes in which the component is the consumer
	 */
	List<DeploymentAssociation> getConsumersFor(DLComponent c) {
		return (associationMap.values().findAll { it.getConsumer() == c }.asList())
	}

	//TODO: fix all this for the multi-drawing case

	void componentAdded(DLComponent component) {
		/*if (componentModel.hasActiveState( ModelState.INITIALIZING )){
            return
        } */

		//log.trace("componentAdded($component)")
		//if(component.hasClass('drawing') && drawing == null) {
		//the new drawing always wins

		if (component.hasClass('drawing')) {
			//log.trace("drawing componentAdded")
			//drawing = component
			component.addPropertyChangeListener(this, this.&drawingPropertyChanged)
			//componentLayer.setScale(1/drawing.getPropertyValue('scale'))
		}
	}

	void componentRemoved(DLComponent component) {
		//if (componentModel.hasActiveState( ModelState.INITIALIZING )){return}
		//log.trace("componentRemoved($component)")

		if (component.hasRole("drawing")) {
			log.trace("a drawing has been removed")
			component.removePropertyChangeListener(this)
		}

		if (component.hasClass('placeable') && componentNodeMap.containsKey(component)) {
			//log.debug("Removing component $component")

			if (componentNodeMap[component] instanceof DynamicChildNode) {
				DynamicChildNode cn = componentNodeMap[component]
				cn.disconnect()
				nodeLayer.removeChild(cn)
			} else if (componentNodeMap[component] instanceof DeploymentNode) {
				DeploymentNode dn = componentNodeMap[component]
				dn.disconnect()

				if (nodeLayer.indexOfChild(dn) > -1) {
					nodeLayer.removeChild(dn)
				}

			} else if (componentNodeMap[component] instanceof ChildrenNode) {
				ChildrenNode cn = componentNodeMap[component]
				cn.disconnect()
				nodeLayer.removeChild(cn)
			} else if (componentNodeMap[component] instanceof StaticChildrenNode) {
				//log.trace("remove staticChildrenNode")
				component.getChildComponents().each {child ->
					StaticChildNode childNode = componentNodeMap[child]
					if (childNode != null) {
						childNode.disconnect()
					}
					componentNodeMap.remove(child)

				}

				StaticChildrenNode scn = componentNodeMap[component]
				scn.disconnect()
				nodeLayer.removeChild(scn)

			} else {
				componentNodeMap[component].disconnect()
				nodeLayer.removeChild(componentNodeMap[component])

			}
			componentNodeMap.remove(component)

			if (haloMap.containsKey(component)) {
				haloMap[component].disconnect()
				haloLayer.removeChild(haloMap[component])
				haloMap.remove(component)
			}
		}
	}

	void childAdded(DLComponent parent, DLComponent child) {
		//if (componentModel.hasActiveState( ModelState.INITIALIZING )){return}

		// TODO: Will need to change to handle multi-drawing, or start tracking zones that I manage


		if (child.hasRole("placeable") && !componentNodeMap.containsKey(child)) {
			PNode newNode = null

			//log.trace("childAdded($parent, $child)")

			if (child.hasRole('childplaceable')) {
				newNode = new ChildrenNode(child, displayProperties, undoBuffer)
				addNode((ChildrenNode) newNode)
			} else if (child.hasRole('staticchildplaceable')) {
				//log.info("staticchildplaceable")
				newNode = new StaticChildrenNode(child, displayProperties, componentModel, selectModel, undoBuffer)
				addNode((StaticChildrenNode) newNode);
			} else if (child.hasRole('dynamicchildplaceable')) {
				//log.info("dynamicchildplaceable")
				newNode = new DynamicChildrenNode(child, displayProperties)
				addNode((DynamicChildrenNode) newNode);
			} else if (child.hasRole('dynamicchild')) {
				//log.trace("dynamicchild is added in panel")
				DynamicChildrenNode parentNode = getPNode(parent);
				DynamicChildNode childNode = parentNode.addChildComponent(child);
				newNode = childNode
				addNode(childNode)

			} else if (child.hasRole('staticchild')) {
				// Don't do anything in panel, childNodw will be added in custom property editor (DatabaseTableEditor)
				// please don't delete this if condition

			} else if (child.hasRole('arbitraryshaped')) {
				newNode = new ArbitraryShapeNode(child, displayProperties, undoBuffer, toolManager, selectModel, this)
				addNode((ArbitraryShapeNode) newNode)
			} else {
				if (child.hasRole('haloed')) {
					//log.trace("Creating halo for component")
					try {
						double radius = Double.parseDouble(child.getDisplaySetting('haloradius'))
						Color color = new Color(Integer.parseInt(child.getDisplaySetting('halocolor')))
						HaloNode halo = new HaloNode(child, radius, color, displayProperties)
						haloMap[child] = halo
						haloLayer.addChild(halo)
					} catch (Exception e) {
						log.error("Could not draw halo for $child")
						log.error(log.getStackTrace(e))
					}
				}

				if(child.hasRole("text")){
					newNode = new TextNode(child, displayProperties)
					addNode( newNode )
				} else if( child.hasRole("rotatable") ) {
					newNode = new OrientedDeploymentNode(child, displayProperties, undoBuffer)
					addNode((OrientedDeploymentNode) newNode)
				} else if( child.hasRole( ComponentRole.RECTANGLE_SHAPE ) ) {
					newNode = new RectangleNode(child, displayProperties, undoBuffer)
					addNode( newNode )
				} else {
					DeploymentNode dn = new IconNode(child, displayProperties)
					newNode = dn
					addNode(dn)
				}

				if (child.getDrawing() != selectModel.getActiveDrawing()) {
					newNode.setVisible(false)
				}
			}
		}

		// Keep rooms at the bottom.
		if( child.hasRole( ComponentRole.ROOM ) ) {
			def childNode = componentNodeMap.get( child )
			if( parent.hasRole( ComponentRole.ROOM ) ) {
				def parentNode = componentNodeMap.get( parent )
				if( parentNode && childNode ) {
					childNode.moveInFrontOf( parentNode )
				}
			} else if( childNode ) {
				childNode.moveToBack()
			}
		}
	}

	void childRemoved(DLComponent parent, DLComponent child) {}

	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
		//if (componentModel.hasActiveState( ModelState.INITIALIZING )){return}
		//log.trace("associationAdded($producer, $producerId, $consumer, $consumerId)")
		//log.trace("associationAdded $producer.$producerId -> $consumer.$consumerId")
		Pair<DLComponent, DLComponent> pc = new Pair<DLComponent, DLComponent>(producer, consumer)
		Pair<DLComponent, DLComponent> cp = new Pair<DLComponent, DLComponent>(consumer, producer)
		DeploymentAssociation da
		if (associationMap.containsKey(pc)) {
			//log.trace("Updating existing association (producer->consumer)")
			da = associationMap[pc]
			da.addAssociation(producer, producerId, consumer, consumerId)
		} else if (associationMap.containsKey(cp)) {
			//log.trace("Updating existing association (consumer->producer)")
			da = associationMap[cp]
			da.addAssociation(producer, producerId, consumer, consumerId)
		} else {
			//log.trace("Creating new association")
			da = new DeploymentAssociation(producer, producerId, consumer, consumerId, displayProperties)
			associationMap.put(pc, da)
			da.addInputEventListener(toolManager)
			associationLayer.addChild(da)
		}
	}

	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
		//if (componentModel.hasActiveState( ModelState.INITIALIZING )){return}
		//log.trace("associationRemoved($producer, $producerId, $consumer, $consumerId)")
		Pair<DLComponent, DLComponent> pc = new Pair<DLComponent, DLComponent>(producer, consumer)
		Pair<DLComponent, DLComponent> cp = new Pair<DLComponent, DLComponent>(consumer, producer)
		DeploymentAssociation da
		if (associationMap.containsKey(pc)) {
			//log.trace("Removing from existing association")
			da = associationMap[pc]
			if (da.removeAssociation(producer, producerId, consumer, consumerId)) {
				//log.trace("Removing association completely")
				da.disconnect()
				associationLayer.removeChild(da)
				associationMap.remove(pc)
			}
		} else if (associationMap.containsKey(cp)) {
			//log.trace("Removing from existing association")
			da = associationMap[cp]
			if (da.removeAssociation(producer, producerId, consumer, consumerId)) {
				//log.trace("Removing association completely")
				da.disconnect()
				associationLayer.removeChild(da)
				associationMap.remove(cp)
			}
		} else {
			log.warn("Could not find association to remove!")
		}
	}

	boolean isActiveDrawingChild(DLComponent c) {
		if (selectModel.getActiveDrawing() == null)
			return false
		if (c.getDrawing().equals(selectModel.getActiveDrawing()))
			return true
		else
			return false
	}

	void callChildAdded(DLComponent parent) {
		for (DLComponent child : parent.getChildComponents()) {
			//log.trace("child of " + parent.getPropertyStringValue("name") + child.toString())
			if (child.hasRole("placeable")) {
				childAdded(parent, child)
			}

			for (Consumer consumer : child.getAllConsumers()) {
				for (Pair<DLComponent, String> oip : consumer.listProducerComponents()) {
					associationAdded(oip.a, oip.b, child, consumer.getId())
					//fireAssociationAdded(producer, oip.b, c, consumer.getId() )//consumerId)
				}
			}

			//same for conducers
			def conducerCheck = []
			for (Conducer d : child.getAllConducers()) {
				Consumer consumer = d.getConsumer()

				for (Pair<DLComponent, String> oip : consumer.listProducerComponents()) {

					if (!oip in conducerCheck) {

						associationAdded(oip.a, oip.b, child, consumer.getId())
						//fireAssociationAdded(producer, oip.b, c, consumer.getId())
						//conducerCheck += oip

					}

				}
			}

			callChildAdded(child)
		}
	}

	@Override
	public void modelMetamorphosisStarting(MetamorphosisEvent event) {
		if (event.getMode() == ModelState.CLEARING) {
			this.clear()
		}
	}

	@Override
	public void modelMetamorphosisFinished(MetamorphosisEvent event) {
		// scene graph creates all deploymentNode and associationNode during deserializing.
		// And when switching active drawing, all child nodes of active drawing will be displayed
		// so we don't need to do anything here.
		if (event.getMode() == ModelState.CLEARING) {
			drawingViews.clear()
		}
	}

	void drawingPropertyChanged(def who, String pName, def oldVal, def newVal) {
		if (who == selectModel.getActiveDrawing()) {
			if (pName == 'image_data') {
				// call new method to avoid duplicate code
				setDrawingBackground((DLImage) newVal)
				/*log.trace("Installing new background image to scene graph")
				MediaTracker mediaTracker = new MediaTracker(this)
				mediaTracker.addImage(newVal, 0)
				try {
					mediaTracker.waitForID(0)
					if(mediaTracker.isErrorAny()) {
						log.warn("Error loading image")
						return
					}
				} catch(InterruptedException ie) {
					log.error("Error loading image!", 'message')
					log.error(log.getStackTrace(ie))
					return
				}

                // sun.awt has hardcoded BufferedImage needs, so no DLImage here. *sigh*
				backgroundImage.setImage(((DLImage)newVal).getImage())
                log.trace(newVal.toString())
				backgroundWidth = (int)backgroundImage.getWidth()
				backgroundHeight = (int)backgroundImage.getHeight()
				revalidateGrid()
				revalidate()*/

			} else if (pName == 'scale') {
				componentLayer.setScale(1 / newVal)
				currentScale = newVal
				revalidate()
			} else if (pName.equals("color") || pName.equals("alpha")){
				constructBlankCanvas()
			} else if (pName.equals("width")){
				blankCanvasWidth = newVal
				constructBlankCanvas()
			} else if (pName.equals("height")){
				blankCanvasHeight = newVal
				constructBlankCanvas()
			}
		}
	}

	/**
	 * Set drawing background image for general drawing and virtual layer
	 * @param image
	 */
	void setDrawingBackground(DLImage image) {

		if (image == null) {
			//log.error("Error loading background image")
			log.debug("null DLImage for background image; did the user just add a new drawing? or use blank canvas")
			backgroundImage.setImage((Image)null);
			backgroundWidth = 0
			backgroundHeight = 0
			backgroundImage.resetBounds()
			backgroundImage.repaint()

		} else { // general drawing
			MediaTracker mediaTracker = new MediaTracker(this)
			// *sigh* Deep down in sun.awt, it hardcodes the need for BufferedImage... ignoring basic OO concepts. So no DLImage here.
			BufferedImage bufferedImage = image.getImage()
			//log.trace("buffered image info" + bufferedImage.getHeight() + ";" + bufferedImage.getWidth())

			mediaTracker.addImage(bufferedImage, 0)
			try {
				mediaTracker.waitForID(0)
				if (mediaTracker.isErrorAny()) {
					log.warn("Error loading image")
					return
				}
			} catch (InterruptedException ie) {
				log.error("Error loading image!", 'message')
				log.error(log.getStackTrace(ie))
				return
			}

			backgroundImage.setImage(bufferedImage);
			backgroundWidth = (int) backgroundImage.getWidth()
			backgroundHeight = (int) backgroundImage.getHeight()
		}

		if ( CentralCatalogue.isBetaVersion() ) {
			betaLayer.removeAllChildren()
			def betanode = new PText()
			betanode.setText("BETA")
			Font betaFont = new Font(Font.SANS_SERIF, Font.BOLD, 40)
			betanode.setFont(betaFont)
			betanode.setTextPaint(Color.RED)
			betanode.setTransparency(0.5f)
			betaLayer.addChild(betanode)
			betanode.setVisible(true)
			betanode.setPickable(false)
		}
	}

	void displayPropertyChanged(def who) {

		//log.info("displayPropertyChanged: " + displayProperties.getProperty('gridView'))

		boolean isGridView = (boolean) (displayProperties.getProperty('gridView'))

		if (isGridView) {
			double spacing = (double) (displayProperties.getProperty("gridSpacing"))
			String spacingUnit = (String) (displayProperties.getProperty("gridSpacingUnit"))
			Color color = (Color) (displayProperties.getProperty("gridViewColor"))
			double spacingInch

			if (spacingUnit.equals("inch")) {
				spacingInch = spacing
			} else {
				spacingInch = spacing / 2.54
			}

			gridSpacing = spacingInch / currentScale;
			girdLineColor = color
			//log.info("spacing:" + spacing + " spacingUnit;" + spacingUnit + " spacinginch:" + spacingInch)
			//log.info("gridSpacing:" + gridSpacing)
			// log.info("girdLineColor:" + girdLineColor)

		}
		revalidateGrid()
		gridLayer.setVisible(isGridView)


		backgroundImage.setVisible((boolean) (displayProperties.getProperty('backgroundImg')))
//		if (!(boolean) displayProperties.getProperty('backgroundImg')) {
//			//when we need a blank background, construct a new one
//			def whiteboard = new PPath()
//			whiteboard.setPathTo(new Rectangle((int) backgroundImage.getX(), (int) backgroundImage.getY(), (int) backgroundImage.getWidth(), (int) backgroundImage.getHeight()))
//			whiteboard.setPaint(Color.LIGHT_GRAY)
//			whiteboard.setStroke(new BasicStroke((float) 0.5))
//			whiteboard.setStrokePaint(Color.LIGHT_GRAY)
//			backgroundBlank.addChild(whiteboard)
//			backgroundBlank.resetBounds()
//		} else if ((boolean) displayProperties.getProperty('backgroundImg')) {
//			//if the user turns the real background on, throw our constructed one away
//			backgroundBlank.removeAllChildren()
//		}
//		backgroundBlank.setVisible(!(boolean) displayProperties.getProperty('backgroundImg'))

	}

	public getBackgroundImageWidth() {
		return (double) backgroundWidth
	}

	public getBackgroundImageHeight() {
		return (double) backgroundHeight
	}

	public getBlankCanvasWidth() {
		return (double) blankCanvasWidth
	}

	public getBlankCanvasHeight() {
		return (double) blankCanvasHeight
	}

	double getZoom() {
		return camera.getViewScale()
	}

	NumberFormat numberFormat = NumberFormat.getPercentInstance()
	private final static double EPSILON = 0.00001; //this is a decent epsilon factor for IEEE doubles

	/**
	 * Compares two IEEE doubles for equality via an epsilon factor.
	 * @param first the first double to compare
	 * @param second the second double to compare
	 * @return true if first and second differ by less than Epsilon, false if they differ by more than Epsilon
	 */
	private boolean epsilonEquals(double first, double second) {
		if (Math.abs(first - second) > EPSILON) {
			return false
		}
		return true
	}

	/**
	 * Sets the current zoom factor centered on a specified point.
	 *  Zooming with the scroll wheel zooms 5% per "click".
	 If the current zoom factor is less than 5% higher than the minimum of 5%,
	 this would make the zoom seem to get "stuck" at 7% or so.
	 So, a call to adjustZoom() handles that case: incoming zoom commands that are between 5% and 10% are adjusted to 5%,
	 which means that the case of 9% - 5%, which used to be 4% and therefore ignored are now adjusted to 5%,
	 but everything less than that is ignored, so that once you zoom all the way out it still won't continue
	 rescaling the view each wheel click.
	 *
	 * This logic applies to all cases; any specifed zoom factor outside the allowed range is normalized into the 5%-500% range.
	 * This means that any other method attempting to set the zoom level should go through this method to make sure the same logic is applied.
	 *
	 * @param z the new zoom factor (a double between 0.05 and 5.0)
	 * @param pt
	 *
	 * @see DeploymentPanel#adjustZoom(double)
	 * @see DeploymentPanel#scrollToView
	 */
	void setZoom(double z, Point2D pt) {
		double curZoom = camera.getViewScale()

		//log.trace("zoom" + pt.x + "," + pt.y)
		z = this.adjustZoom(z)
		//log.trace("setzoom from point " + camera.getViewBounds().toString())

		if (!epsilonEquals(z, curZoom)) {
			camera.scaleViewAboutPoint(z / curZoom, pt.x, pt.y)
			revalidate()
			log.internal("Zoom factor: ${numberFormat.format(z)}", 'zoomFactor')
		}
	}

	/**
	 * Sets the current zoom factor centered on the camera's current view
	 * @param z the new zoom factor (a double between 0.05 and 5.0)
	 */
	void setZoom(double z) {
		this.setZoom(z, camera.getViewBounds().getCenter2D())
	}

	/**
	 * Checks the supplied zoom factor and makes sure that it's legal, adjusting the current zoom if not.
	 * This method assumes that z is the current zoom factor of the view, so that if the zoom is legal, all it needs to do is log the zoom level.
	 * @param z
	 */
	void checkZoom(double z) {
		if (this.epsilonEquals(this.adjustZoom(z), z)) {
			//we don't need to adjust, just issue the log message
			log.internal("Zoom factor: ${numberFormat.format(z)}", 'zoomFactor')
		} else {
			this.setZoom(z)
		}
	}

	/**
	 * Makes sure that the zoom factor is in the "legal range"
	 * @param z the user supplied zoom factor
	 * @return that zoom factor pulled into the legal range of zooms
	 */
	double adjustZoom(double z) {
		if (z < 0.05) { return 0.05 }
		if (z > 5.0) { return 5.0 }
		return z
	}

	Point2D getViewCenter() {
		return camera.getViewBounds().getCenter2D()
	}

	void pan(PDimension d) {
		//log.info('panning')
		camera.translateView(d.width, d.height)
	}

	/**
	 * Scrolls the camera to center the specified component in view.  No zoom adjustment is performed.
	 * There must be a matching node for the component in the deployment plan or no translation is performed.
	 * @param c the component to center in the panel.
	 */
	void scrollToView(DLComponent c, int speed = 1000) {
		DeploymentNode n = componentNodeMap[c]
		if (n != null) {
			camera.animateViewToCenterBounds(n.getGlobalFullBounds(), false, speed)
		} else {
			log.warn("No node for component!")
		}
	}

	/**
	 * Scrolls the camera to center the specified list of components in view.
	 * The specified selection can be any arbitrary list of components on the deployment plan.
	 * If the list has more than one component, the view is zoomed to fit the selection on the screen along with the pan.
	 * If the list has only one component, no zoom is performed, only the pan.  (Identical behavior to the single component parameter version of this method.)
	 * Note that this method will not "expand" parent components; to scroll to all the contents of a zone, for example, you must hand this method the list of zone children, not the zone component itself.
	 * @param cl The List of DLComponents to center in view.
	 */
	void scrollToView(Collection<DLComponent> cl) {
		PBounds totalBounds = null
		PNode n
		cl.each { c ->
			n = componentNodeMap[c]

			if (n != null) {
				if (totalBounds == null) {
					totalBounds = n.getGlobalFullBounds()
				} else {
					totalBounds.add(n.getGlobalFullBounds())
				}
			}
		}
		if (totalBounds != null && n != null) {
			if (cl.size() == 1) {
				camera.animateViewToCenterBounds(n.getGlobalFullBounds(), false, 1000)
			} else {
				def ani = camera.animateViewToCenterBounds(totalBounds, true, 1000)
				def root = n.getRoot()
				root.waitForActivities()
				double curZoom = camera.getViewScale()
				this.checkZoom(curZoom) //make sure the zoom level works for us
				//log.info("Zoom factor: ${numberFormat.format(curZoom)}", 'zoomFactor')
			}
		} else {
			log.warn("No nodes for components!")
		}
	}

	/**
	 * Centers the camera's view on the center of the background image, and zooms out to view the whole image.
	 * @param animate whether to animate the transition (true) or translate instantly (false)
	 */
	public void fitView(boolean animate) {
		if (animate) {
			camera.animateViewToCenterBounds(backgroundBlank.getGlobalFullBounds(), true, 1000)
			def root = backgroundBlank.getRoot()
			root.waitForActivities()
		} else {
			camera.animateViewToCenterBounds(backgroundBlank.getGlobalFullBounds(), true, 0)
		}
		double curZoom = camera.getViewScale()
		this.checkZoom(curZoom) //make sure the zoom level works for us
		//log.info("Zoom factor: ${numberFormat.format(curZoom)}", 'zoomFactor')
	}

	/**
	 * Overload that performs no animation.
	 */
	public void fitView() {
		this.fitView(false)
	}

	/**
	 * Checks if the given node is fully visible in the current camera view.  PNodes that are "hanging" off the edge of the view
	 * will return false.
	 * @param nodeToCheck a PNode to check - can be any PNode, but should be added to the main deployment plan before checking
	 * @return true if entirely in view.
	 * @see DeploymentPanel#getBoundsInView(PNode, boolean)
	 */
	public boolean checkIfInView(PNode nodeToCheck, boolean doubleTheWidth = true) {
		PBounds boundsToCheck = nodeToCheck.getGlobalFullBounds()
		def boundsW = boundsToCheck.getWidth() / camera.getViewScale()
		if (doubleTheWidth) { boundsW *= 2 }
		def boundsH = boundsToCheck.getHeight() / camera.getViewScale()
		PBounds scaledBounds = new PBounds(boundsToCheck.getX(), boundsToCheck.getY(), boundsW, boundsH)
		//log.trace("scaledBounds:" + scaledBounds.toString())
		return getCameraViewBounds().contains(scaledBounds)
	}

	/**
	 * Generates a slightly better printable string for PBounds over the default .toString() method.
	 * @param b a PBounds object to print out
	 * @return a human-readable string describing the bounds
	 */
	public String printBounds(PBounds b) {
		DecimalFormat df = new DecimalFormat("#.##");
		return "PBounds[x: ${df.format(b.getX())}, y: ${df.format(b.getY())}, farX: ${df.format(b.getX() + b.getWidth())}, farY: ${df.format(b.getY() + b.getHeight())} width: ${df.format(b.getWidth())}, height: ${df.format(b.getHeight())}]"
	}

	/**
	 * Convenience method to get the view bounds of the camera.
	 * @return a PBounds holding the camera's view bounds
	 */
	public PBounds getCameraViewBounds() {
		return camera.getViewBounds()
	}

	public double getDrawingScale() {
		return currentScale
	}

	/**
	 * Takes a node that isn't entirely shown on the screen and computes some new coordinates that that it will be entirely visible.
	 * Note: this assumes that checkIfInView() has already been called on this node,
	 * so this method proceeds as if we already know that this node isn't entirely visible.
	 * @param nodeToCheck any PNode whose location need shifting.  Note that it must be in the main scene graph for this to work.
	 * @return a PBounds with the node's new location.
	 * @see DeploymentPanel#checkIfInView(PNode, boolean)
	 */
	public PBounds getBoundsInView(PNode nodeToCheck, boolean doubleTheWidth = true) {
		PBounds boundsToCheck = nodeToCheck.getGlobalBounds()
		//println "the bounds we're checking are $boundsToCheck"

		//the node's size needs to be adjusted for the current view scale.
		//when a scale happens, the origin point of nodes stays the same, but their width & height are scaled.
		def boundsW = boundsToCheck.getWidth() / camera.getViewScale()
		if (doubleTheWidth) { boundsW *= 2 }
		def boundsH = boundsToCheck.getHeight() / camera.getViewScale()

		//Since we only want to check one axis at a time, we need a "known good" X and Y coordinate to pair with the
		//coordinate to test.
		def safeY = getCameraViewBounds().getY() + 1
		def safeX = getCameraViewBounds().getX() + 1

		//compute the far right and far left points of the node to check
		def farRight = boundsToCheck.getX() + boundsW
		//println "text far right $farRight"
		def farLeft = boundsToCheck.getX()
		//println "text far left $farLeft"

		//check to see if the node is off one side of the screen or the other on the x axis.
		//first, check to see if the far right edge of the node is off the right side of the screen
		if (!getCameraViewBounds().contains(farRight, safeY)) {
			log.warn("FasterNamer box too far to the right; adjusting.", "default")
			//if too far right, adjust the origin of the node so that the x coordinate is the right side of the screen minus the width of the node
			def cameraRight = getCameraViewBounds().getX() + getCameraViewBounds().getWidth()
			//println "camera right $cameraRight"
			def revisedRight = cameraRight - boundsW
			boundsToCheck.setOrigin(revisedRight - 1, boundsToCheck.getY())

		} else if (!getCameraViewBounds().contains(farLeft, safeY)) {
			log.warn("FasterNamer box too far to the left; adjusting.", "default")
			//if too far left, make the x coordinate of the node the left side of the screen
			def cameraLeft = getCameraViewBounds().getX()
			//println "camera left $cameraLeft"
			boundsToCheck.setOrigin(cameraLeft + 1, boundsToCheck.getY())
		}

		//now sort out up and down
		def farDown = boundsToCheck.getY() + boundsH
		//println "text far down $farDown"
		def farUp = boundsToCheck.getY()
		//println "text far up $farUp"

		if (!getCameraViewBounds().contains(safeX, farDown)) {
			log.warn("FasterNamer box too falling off the bottom; adjusting.", "default")

			def cameraDown = getCameraViewBounds().getY() + getCameraViewBounds().getHeight()
			//println "cameraDown $cameraDown"
			def revisedTop = cameraDown - boundsH
			boundsToCheck.setOrigin((boundsToCheck.getX()), revisedTop)

		} else if (!getCameraViewBounds().contains(safeX, farUp)) {
			log.warn("FasterNamer box flying off the top; adjusting.", "default")

			def cameraUp = getCameraViewBounds().getY()
			//println "cameraUp $cameraUp"
			boundsToCheck.setOrigin((boundsToCheck.getX()), cameraUp)
		}

		//println "new bounds in global are $boundsToCheck"
		PBounds revised = (PBounds) nodeToCheck.globalToLocal(boundsToCheck)
		//println "returning revised bounds of ${printBounds(revised)}"
		return revised
	}


	void clear() {
		backgroundImage.setImage((Image)null)
		backgroundHeight = 0
		backgroundWidth = 0

		betaLayer.removeAllChildren()
		backgroundBlank.removeAllChildren()
		blankCanvasHeight = 0
		blankCanvasWidth = 0

		/*
				if(drawing!=null){
					def placeableComponents = drawing.getAllChildComponents().findAll{it->it.hasRole("placeable")}
					placeableComponents.each{it->
						log.trace("placable child of active drawing" + it.toString())
						//childAdded(it.getParentComponents().first(), it)
						if(componentNodeMap[it]!=null)
							componentNodeMap[it].setVisible(false)
					}
				} */

		/*
		for (PNode it : associationLayer.getChildrenIterator()){
			it.disconnect()
		}
		associationLayer.removeAllChildren()
		for (PNode it : nodeLayer.getChildrenIterator()){
			it.disconnect()
		}
		nodeLayer.removeAllChildren()
		pointLayer.removeAllChildren()
		haloLayer.removeAllChildren()
		componentNodeMap.clear()
		haloMap.clear()
		*/
	}

	/**
	 * Begins the Set Background Scaling operation by registering listeners and changing the mode of the UI. This mode
	 * allows an operator to select two points on the background image and specify a distance between those points in
	 * order to calculate a scale.
	 */
	void startScalingOp() {
		if (!scaling) {
			scaling = true

			// Suspend and save the current active tools but reactivate pan and zoom.
			toolManager.suspendTools()
			toolManager.setActiveTools(['panzoomer', 'keycommands'])

			scaleShape = new PPath()
			root.addChild(scaleShape)

			scaler = new ScaleSetter(this, scaleShape)
			root.addInputEventListener(scaler)
			backgroundImage.addInputEventListener(scaler)
			backgroundBlank.addInputEventListener(scaler)
			log.info(CentralCatalogue.getUIS('scaler.start'), 'statusbar')

			//canvas.pushCursor(new Cursor(Cursor.CROSSHAIR_CURSOR))
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			//Load an image for the cursor
			Image image = ComponentIconFactory.getImageIcon("/resources/scalingcursor.png").getImage();
			Point hotSpot = new Point(16, 16);
			Cursor cursor = toolkit.createCustomCursor(image, hotSpot, "rescaling");
			canvas.pushCursor(cursor)
			canvas.requestFocusInWindow()
		}
	}

	BufferedImage plotDeployment() {
		PBounds bounds = root.getFullBounds()
		BufferedImage deploymentImage = new BufferedImage((int) bounds.getWidth(), (int) bounds.getHeight(), BufferedImage.TYPE_INT_ARGB)
		Graphics2D g2 = deploymentImage.createGraphics()
		g2.translate(-bounds.getX(), -bounds.getY())
		PPaintContext pc = new PPaintContext(g2)
		pc.setRenderQuality(pc.HIGH_QUALITY_RENDERING)
		root.fullPaint(pc)
		//TODO: MULTIDC - need to investigate why fullPaint twice and resolve it.
		root.fullPaint(pc)
		return deploymentImage;
	}

	// TODO: In the future, this should be more configurable. Original design was to be similar to the View Options,
	// but it was descoped and now it is just what we decide it to be.  Other than the overall "by type", could also
	// have a "show" property for each component, for lots of flexibility.
	private boolean showOnBackground( DLComponent c ) {

		// Exclude all proxies since they are virtual.
		if( !c.hasRole( ComponentRole.PROXY ) ) {

			// If it has a rotation, it is a physical representation and should be shown.
			if( c.hasProperty( ComponentProp.ROTATION ) ) return true

			// Anything considered a modeling component gets shown.
			if( c.getFilters().contains( ComponentFilter.MODELING ) ) return true

			// Rooms, of course.
			if( c.hasRole( ComponentRole.ROOM ) ) return true
		}

		return false
	}

	BufferedImage generateBackgroundImage( DLComponent drawing ){
		log.debug("Generating a background image for $drawing")

		// Other parts of the system override our visibilty settings based on the active drawing when we try
		// to manage it entirely here for the needs of this method. So instead of having a self-contained method to
		// generate images, we need to change the active drawing, then only adjust visibility for components on the drawing.
		DLComponent activeDrawing = CentralCatalogue.getSelectModel().getActiveDrawing()
		if( drawing != activeDrawing ) {
			CentralCatalogue.getSelectModel().setActiveDrawing( drawing )
		}

		selectModel.clearSelection()
		BufferedImage deploymentImage = new BufferedImage( (int) drawing.getPropertyValue("width"),
		                                                   (int) drawing.getPropertyValue("height"),
		                                                   BufferedImage.TYPE_INT_ARGB )
		Graphics2D g2 = deploymentImage.createGraphics()
		PPaintContext pc = new PPaintContext(g2)
		pc.setRenderQuality(pc.HIGH_QUALITY_RENDERING)

		//draw bounds
		PPath boundsPath = new PPath(new Rectangle(0, 0, deploymentImage.width - 1, deploymentImage.height - 1));
		boundsPath.fullPaint(pc)

		// Set what components should be shown and what shouldn't. Remember the current state so we can set it back.
		def visibleToggles = []
		for( DLComponent c : componentModel.getComponentsInRole( ComponentRole.PLACEABLE, drawing ) ) {
			PNode node = componentNodeMap[c]
			if( node ) {
				boolean showIt = showOnBackground( c )

				if( node.getVisible() != showIt ) {
					visibleToggles.add( node )
					node.setVisible( showIt )
				}
			}
		}

		// Draw the components on the image.
		nodeLayer.setScale( 1 / (double) drawing.getPropertyValue("scale") )
		nodeLayer.fullPaint(pc)
		nodeLayer.setScale(1)

		// Set the visible states back.
		for( PNode node : visibleToggles ) {
			node.setVisible( !node.getVisible() )
		}

		// Set the active drawing back.
		if( drawing != activeDrawing ) {
			CentralCatalogue.getSelectModel().setActiveDrawing( activeDrawing )
		}

		return deploymentImage;
	}

	/**
	 * Completes the Set Background Scaling operation. It will attempt to calculate a scale based on the number of
	 * pixels between the two selected points and a user provided real-world distance between the same points.
	 */
	void finishScalingOp() {
		if (scaling) {
			scaling = false

			Point2D start = scaler.startPoint
			Point2D end = scaler.endPoint

			// Remove the scaling listeners.
			root.removeInputEventListener(scaler)
			backgroundImage.removeInputEventListener(scaler)
			backgroundBlank.removeInputEventListener(scaler)
			scaler = null

			// Calculate the new scale.
			double scale = new ScalingUtility(this, systemConverter)?.computeScale(start, end)

			// If the new scale is valid, save it to the project properties.
			if (scale && (scale > 0)) {
				selectModel.getActiveDrawing().setPropertyValue('scale', scale)
			}

			// Clear the scaling line and get back to the pre-scaling UI state.
			root.removeChild(scaleShape)
			scaleShape = null
			canvas.pushCursor(null)
			toolManager.resumeTools()
		}
	}

	/**
	 * Cancels the Set Background Scaling operation. The UI will be set to its pre-scaling state.
	 */
	void cancelScalingOp() {
		scaling = false
		root.removeInputEventListener(scaler)
		backgroundImage.removeInputEventListener(scaler)
		backgroundBlank.removeInputEventListener(scaler)
		root.removeChild(scaleShape)

		scaler = null
		scaleShape = null
		canvas.pushCursor(null)  // Back to default
		toolManager.resumeTools()
		log.info(CentralCatalogue.getUIS('scaler.canceled'), 'statusbar')
	}

	void moveToFront(List<DLComponent> targets) {
		targets.each { c ->
			PNode pnode = getPNodeFor(c)
			if (pnode == null)
				return

			// rather than a simple moveToFront(), we want to keep rooms below non-rooms if possible.
			if( c.hasRole( ComponentRole.ROOM ) ) {
				PNode p = pnode.getParent()
				for( int i = 0; i < p.getChildrenCount(); i++ ) {
					PNode otherNode = p.getChild(i)
					if( ( pnode != otherNode ) && ( otherNode instanceof DeploymentNode ) && !otherNode.getComponent().hasRole( ComponentRole.ROOM ) ) {
						pnode.moveInBackOf( otherNode )
						return // from closure
					}
				}
			}

			pnode.moveToFront()
		}
	}

	void moveToBack(List<DLComponent> targets) {
		targets.each { c ->
			PNode pnode = getPNodeFor(c)
			if (pnode == null)
				return

			// rather than a simple moveToBack(), we want to stay on top of rooms if possible.
			if( !c.hasRole( ComponentRole.ROOM ) ) {
				PNode p = pnode.getParent()
				for( int i = 0; i < p.getChildrenCount(); i++ ) {
					PNode otherNode = p.getChild(i)
					if( ( pnode != otherNode ) && ( otherNode instanceof DeploymentNode ) && !otherNode.getComponent().hasRole( ComponentRole.ROOM ) ) {
						pnode.moveInBackOf( otherNode )
						return // from closure
					}
				}
			}

			pnode.moveToBack()
		}
	}

	void moveForward(List<DLComponent> targets) {
		this.moveForwardBackward(targets, false)
	}

	void moveBackward(List<DLComponent> targets) {
		this.moveForwardBackward(targets, true)
	}


	void moveForwardBackward(List<DLComponent> targets, boolean moveBackward) {
		for (DLComponent c : targets) {
			PNode node = this.getPNodeFor(c)
			if( node == null ) { continue; }

			PNode p = node.getParent()
			int nodeIdx = p.indexOfChild( node )
			PBounds nodeBounds = node.getShape().getGlobalFullBounds()

			// Find which nodes overlap this one and which of those is nearest in display order.
			int otherIdx = ( moveBackward ? Integer.MIN_VALUE : Integer.MAX_VALUE )
			for( PNode othernode : p.getChildrenReference() ) {
				// Only check nodes that are visible DeploymentNodes that are not Rooms or one of the selected nodes
				// and intersects the selected node's bounds.
				if( othernode instanceof DeploymentNode &&
				    othernode.getVisible() &&
				    !othernode.getComponent().hasRole( ComponentRole.ROOM ) &&
				    !targets.contains( othernode.getComponent() ) &&
					othernode.getShape().getGlobalFullBounds().intersects( nodeBounds ) ) {

					int i = node.getParent().indexOfChild( othernode )
					if( moveBackward && ( i < nodeIdx ) ) {
						if( i > otherIdx ) {
							otherIdx = i
						}
					} else if( i > nodeIdx ) {
						if( i < otherIdx ) {
							otherIdx = i
						}
					}
				}
			}

			if( ( otherIdx < nodeIdx ) && ( otherIdx >= 0 ) ) {
				node.moveInBackOf( p.getChild( otherIdx ) )
			} else if( ( otherIdx > nodeIdx ) && ( otherIdx < p.getChildrenCount() ) ) {
				node.moveInFrontOf( p.getChild( otherIdx ) )
			} else {
				log.trace("Skip ${moveBackward ? "BACKWARD" : "FORWARD"}. No nearby components found.")

				/* No real reason to do this unless we come across odd conditions that this solves. Not doing it
				   prevents being asked to save the file for no good reason since nothing visibly changed.

				// Can't make an intelligent choice, but at least shift relative to the visible nodes, not just +/-1.
				int shift = ( moveBackward ? -1 : 1 )
				while( nodeIdx > 0 && nodeIdx < p.getChildrenCount() ) {
					nodeIdx += shift
					def othernode = p.getChild( nodeIdx )
					if( othernode instanceof DeploymentNode && othernode.getVisible() && !targets.contains( othernode.getComponent() ) ) {
						if( !othernode.getComponent().hasRole( ComponentRole.ROOM ) ) {
							p.removeChild(node)
							p.addChild(nodeIdx, node)
							CentralCatalogue.getOpenProject().setNeedsASave( true )
						}
						break
					}
				}
				*/
			}
		}
	}

	int getZIndex(PNode node) {
		if (node == null) { return 0 }
		def p = node.getParent()
		return p.indexOfChild(node)
	}


	void selectionChanged(SelectionChangeEvent e) {
		//iterate on separate collection and don't use += or -= for collections
		for (DLComponent c : e.getExpandedRemoval()) {
			if (componentNodeMap.containsKey(c)) {
				//log.trace("selectionChanged in panel    " + c.toString())
				componentNodeMap[c].setSelected(false)
			} else {
				log.error("Component not found on the Deployment Panel: $c")
			}
		}

		for (DLComponent c : e.getExpandedAddition()) {
			if (componentNodeMap.containsKey(c)) {
				componentNodeMap[c].setSelected(true)
			} else {
				log.error("Component not found on the Deployment Panel: $c")
			}
		}
	}

	void activeDrawingChanged(DLComponent oldDrawing, DLComponent newDrawing) {
		//log.trace("activeDrawingChanged before")
		if (componentModel.hasActiveState(ModelState.INITIALIZING)) {
			//log.trace("componentModel is initializing")
			return;
		}

		// Save the current view before we switch.
		if( visibleDrawing != null ) {
			drawingViews[visibleDrawing] = new Pair( camera.getViewBounds(), camera.getViewScale() )
		}

		if (newDrawing != null) {
			// 1. change background image
			clear();
			//construct blank canvas
			blankCanvasWidth = (int) newDrawing.getPropertyValue("width")
			blankCanvasHeight = (int) newDrawing.getPropertyValue("height")
			UIDisplay.INSTANCE.setProperty('useBackgroundImage', newDrawing.getPropertyValue("hasImage") )
			UIDisplay.INSTANCE.setProperty('useCanvas', !newDrawing.getPropertyValue("hasImage"))
			constructBlankCanvas()
			//log.trace("new drawing name:" + newDrawing.getPropertyValue("name"))
			setDrawingBackground((DLImage) newDrawing.getPropertyValue("image_data"))
			// set scale
			currentScale = newDrawing.getScale()
			componentLayer.setScale(1 / currentScale)
			visibleDrawing = newDrawing

			// If we have a saved view, set it rather than doing a fitView
			def savedView = drawingViews[ newDrawing ]
			if( savedView == null ) {
				log.trace("FIT VIEW")
				fitView()
			} else {
				log.trace("SET VIEW")
				camera.setViewBounds( savedView.a )
				camera.setViewScale( savedView.b )
			}
		} else {
			//log.trace("active drawing is null")
			backgroundImage.setImage((Image)null);
			backgroundWidth = 0
			backgroundHeight = 0
			backgroundImage.repaint()
			visibleDrawing = null
			blankCanvasWidth = 0
			blankCanvasHeight = 0
			backgroundBlank.removeAllChildren()
		}
		revalidateGrid()
	}

	def listNodesInBox(int x1, int y1, int x2, int y2) {
		Rectangle box = new Rectangle( x1, y1, 0, 0 )
		box.add( x2, y2 )
		return componentNodeMap.collect {it -> it.value}.findAll {
			if( it.getVisible() ) {
				DLComponent c = it.getComponent()
				if( c.hasRole( ComponentRole.ARBITRARY_SHAPE ) || c.hasRole( ComponentRole.RECTANGLE_SHAPE ) ) {
					// Only select if the boundaries are in the box.
					for( Line2D line : it.getLines() ) {
						if( box.intersectsLine( line ) ) {
							return true
						}
					}
				} else {
					return box.contains( c.getPropertyValue('x'), c.getPropertyValue('y') )
				}
			}
			return false
		}
	}

	def listAllVisibleNodes() {
		return componentNodeMap.collect {it -> it.value}.findAll { it.getVisible() }
	}

	void mouseClicked(MouseEvent e) {}

	void mousePressed(MouseEvent e) {}

	void mouseReleased(MouseEvent e) {}

	/**
	 * When mousing into the drawing, automatically make the canvas take focus so piccolo's event handlers take over
	 */
	void mouseEntered(MouseEvent e) {
		this.focus()
	}

	void mouseExited(MouseEvent e) {}


	public void focus() {
		canvas.requestFocusInWindow()
	}

	/**
	 * Helper function to find the geometric center of a group of Points.
	 * @param points a List of Points
	 * @return a Point2D representing the geometric center of a bounding box containing all of those points
	 */
	public static Point2D.Double findCenterPoint(List<Point2D.Double> points) {
		Point first = new Point();
		first.setLocation(points.first().getX(), points.first().getY())
		Rectangle boundingBox = new Rectangle(first);
		for (Point2D p : points) {
			boundingBox.add(p);
		}
		return new Point2D.Double(boundingBox.getCenterX(), boundingBox.getCenterY());
	}

	/**
	 * Scales a point out of the pixel-locations provided by piccolo into the real-world location on the drawing.
	 * The point argument will be modified as a result of this method.
	 *
	 * @param pt a pixel-dimension Point
	 * @return The same point, but scaled to real-world dimensions
	 */
	public Point2D scalePoint( Point2D pt ) {
		pt.x = pt.x * currentScale
		pt.y = pt.y * currentScale
		return pt
	}

	public void setPanelCursor(Cursor cursor) {
		canvas.pushCursor(cursor);
	}


	public void repositionSecondaryNodes() {
		try {
			undoBuffer.startOperation()
			canvas.pushCursor(new Cursor(Cursor.WAIT_CURSOR))
			List<DLComponent> seconds = componentModel.getComponents().findAll { it.isSecondary() && it.getDrawing().equals(selectModel.getActiveDrawing()) }
			for (DLComponent s : seconds) {
				DLComponent host = s.getHostComponent()
				s.setPropertyValue("x", host.getPropertyValue("x"))
				s.setPropertyValue("y", host.getPropertyValue("y"))
				def dn = this.getNodeFor(s)
				dn.adjustOffset()
			}
		} catch (Exception e) {
			undoBuffer.rollbackOperation()
			throw e;
		} finally {
			undoBuffer.finishOperation()
			canvas.pushCursor(null)
		}
	}

	public void constructBlankCanvas(){
		backgroundBlank.removeAllChildren()
		def whiteboard = new PPath()
		whiteboard.setPathTo(new Rectangle(0, 0, blankCanvasWidth, blankCanvasHeight))
		Color backgroundColor = selectModel.getActiveDrawing().getPropertyValue("color")
		int alpha = selectModel.getActiveDrawing().getPropertyValue("alpha")
		whiteboard.setPaint(new Color(backgroundColor.getRed(), backgroundColor.getGreen(), backgroundColor.getBlue(), alpha))
		whiteboard.setStroke(new BasicStroke((float) 0.5))
		whiteboard.setStrokePaint(Color.BLACK)
		backgroundBlank.addChild(whiteboard)
		backgroundBlank.resetBounds()
		revalidateGrid()
	}

	private Rectangle getFullBounds(){
		return componentLayer.getFullBounds().getBounds()
	}

	public int getFullBoundsWidth(){
		int componentBoundWidth = getFullBounds().x + getFullBounds().width
		if (selectModel.activeDrawing.getPropertyValue('hasImage')){
			return (componentBoundWidth > backgroundWidth)?componentBoundWidth:backgroundWidth
		}else{
			return componentBoundWidth;
		}
	}

	public int getFullBoundsHeight(){
		int componentBoundHeight = getFullBounds().y + getFullBounds().height
		if (selectModel.activeDrawing.getPropertyValue('hasImage')){
			return (componentBoundHeight > backgroundHeight)?componentBoundHeight:backgroundHeight
		}else{
			return componentBoundHeight;
		}
	}
}


class CameraPropertyChangeListener implements PropertyChangeListener {
	private static final Logger log = Logger.getLogger(CameraPropertyChangeListener.class.getName())
	private ToolManager toolManager

	CameraPropertyChangeListener(ToolManager tm) {
		toolManager = tm
	}

	void propertyChange(PropertyChangeEvent e) {
		//log.trace("PCE: $e")
		//log.trace("Old: ${e.getOldValue()}")
		//log.trace("New: ${e.getNewValue()}")
		//log.trace("Prop: ${e.getPropertyName()}")
		//def re = new RuntimeException()
		//log.trace(log.getStackTrace(re)
		if (toolManager.getTool('fasternamer') != null)
			toolManager.getTool('fasternamer').cancel()
	}
}
