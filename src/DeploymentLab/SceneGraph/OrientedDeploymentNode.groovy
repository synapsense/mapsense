package DeploymentLab.SceneGraph

import DeploymentLab.DisplayProperties
import DeploymentLab.Model.DLComponent
import DeploymentLab.channellogger.Logger
import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.util.PPickPath
import java.awt.geom.AffineTransform
import DeploymentLab.UndoBuffer


/**
 * Extension of the generic DeploymentNode to allow for rotating and resizing the shape.
 */
class OrientedDeploymentNode extends IconNode {
	private static final Logger log = Logger.getLogger(OrientedDeploymentNode.class.getName())

	private static final double RIGHTING_MIN = Math.toRadians( -270 )
	private static final double RIGHTING_MAX = Math.toRadians( -90 )

	protected double rot = 0
	protected double width
	protected double depth
	private boolean isRighting = true

	protected PNode orientNode

	OrientedDeploymentNode(DLComponent c, DisplayProperties p, UndoBuffer ub) {
		super(c, p)
		//log.trace("init OrientedNode for $c")

		String rotStyle = c.getDisplaySetting('shape-rotation')
		if( rotStyle ) {
			switch( rotStyle ) {
				case 'righting' :
					isRighting = true
					break
				case 'rolling' :
					isRighting = false
					break
				default :
					log.error("Unknown shape rotation style '$rotStyle'. Valid styles are: righting, rolling")
			}
		}

		width = c.getPropertyValue('width') ?: iconSize
		depth = c.getPropertyValue('depth') ?: iconSize

		orientNode = shapeNode

		// It should if it is an Oriented Node, but some don't until upgraded, so be safe so it doesn't blow-up.
		if( c.hasProperty('rotation') ) {
			rot = Math.toRadians(-c.getPropertyValue('rotation'))
		}

		baseX = orientNode.getFullBounds().width
		baseY = orientNode.getFullBounds().height

		//we want the arrow to draw on top of the rest of the node
		//this.addChild(orientNode)

		reshape()
	}

	double baseX
	double baseY

	protected void reshape() {
		if( orientNode ) {
			// We are replacing the existing transform with a new one, so scale based on the original size.
			// Sidenote: Component dimension names are swapped compared to scenegraph and PNode. This is due to the
			// historical MapSense alignment where a rack's cold side points to the right from the user's perspective
			// when the rotation is 0. This makes the depth along the x-axis and width along the y-axis.
			//     scenegraph.x = component.depth = node.width
			//     scenegraph.y = component.width = node.height
			double sx = depth / baseX
			double sy = width / baseY

			// May need to flip it so it's not upside down. We want the top facing left at 90/270 and flip past that.
			if( isRighting ) {
				if( rot >= RIGHTING_MIN && rot < RIGHTING_MAX ) {
					sy = -sy
				}
			}

			AffineTransform tx = AffineTransform.getRotateInstance( rot )
			tx.scale( sx, sy )
			orientNode.setTransform( tx )
			this.setBounds( shapeNode.getFullBounds() )
		}
	}


	void propertyChanged(def who, String prop, oldVal, newVal) {
		//println "orientedNode prop changed $who $prop $oldVal $newVal"
		super.propertyChanged(who, prop, oldVal, newVal)
		if (prop == 'rotation') {
			//rot = newVal
			rot = Math.toRadians(-newVal)
		} else if (prop == 'width') {
			width = newVal
		} else if (prop == 'depth') {
			depth = newVal
		}

		reshape()

		this.resetBackgroundNode()
	}


	void displayPropertyChanged(def who) {
		super.displayPropertyChanged(who)
        boolean isOrientationDisplayable = (boolean) (displayProperties.getProperty('orientation'))
        boolean isActiveDrawingChild =  displayProperties.isActiveDrawingComponent(component)
        boolean isDisplayable = isOrientationDisplayable && isActiveDrawingChild

        //log.debug(component.getPropertyStringValue("name") + "displayPropertiesChanged" +  isActiveDrawingChild.toString())

		orientNode.setVisible(isDisplayable)

	}


	void showArrow() {
		orientNode.setVisible(true)
	}


	void hideArrow() {
		orientNode.setVisible(false)
	}


	/**
	 * Overriding the default PNode implementation because we always want to pick this node before it's children IF the user
	 * has actually clicked on the icon.
	 * @param pickPath the current pick path
	 * @return true if this node has been picked
	 */
	@Override
	protected boolean pick(PPickPath pickPath) {
		if (intersects(pickPath.getPickBounds())) {
			return true;
		}
		return false;
	}
}
