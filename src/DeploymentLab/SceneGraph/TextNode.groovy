package DeploymentLab.SceneGraph

import DeploymentLab.DisplayProperties
import DeploymentLab.Model.DLComponent
import DeploymentLab.channellogger.Logger
import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.nodes.PText
import edu.umd.cs.piccolo.util.PBounds
import edu.umd.cs.piccolo.util.PPickPath

import java.awt.*
import java.awt.geom.RoundRectangle2D

/**
 * Experiment to do text on the drawing
 * @author Gabriel Helman
 * @since Jupiter
 * Date: 1/25/12
 * Time: 3:58 PM
 */
class TextNode extends IconNode {
	private static final Logger log = Logger.getLogger(TextNode.class.getName())
	private static final int TEXT_OFFSET = 15
	protected PText pText;
	protected Integer wrap;
	private String fontName;
	private int fontStyle;
	private int fontSize;
	protected double rot

	protected TextNode(DLComponent c, DisplayProperties p) {
		super(c, p)

		//basic node has been constructed, add our special stuff now
		//pText = new PText(c.getPropertyStringValue("notes"))
		pText = new PText()
		this.addChild(pText)
		pText.setPickable(true)
		pText.setPaint( new Color(1f, 1f, 0.7f, 0.7f) )
		this.setPickable(true)
		this.setChildrenPickable(true)
		//pText.setOffset(TEXT_OFFSET,TEXT_OFFSET)
		//wrap = c.getPropertyValue("wrap")

		//p.addPropertyChangeListener(this, this.&displayPropertyChanged)
		c.addPropertyChangeListener(this, this.&propertyChanged)

		installText(c.getPropertyStringValue("notes"))
		fontName = component.getPropertyValue("fontName")
		fontStyle = component.getPropertyValue("fontStyle")
		fontSize = component.getPropertyValue("fontSize")
		rot = Math.toRadians(-c.getPropertyValue('rotation')) ?: 0
		resetFont()
		rotateNode()

		//configured based on the centerbadge property


		//blow away this node's name node
		this.removeChild(shapeNode)
		this.removeChild(textNode)

		resetBackgroundNode()
	}

	public PNode getShape() {
		return pText
	}

	protected void resetBackgroundNode() {
		backgroundNode.setPathToRectangle((float) 0.0, (float) 0.0, (float) 0.0, (float) 0.0)
		if( pText != null ) {
			PBounds b = pText.getFullBounds()
			b.setRect(b.getX() - HIGHLIGHT_BORDER, b.getY() - HIGHLIGHT_BORDER, b.getWidth() + 2 * HIGHLIGHT_BORDER, b.getHeight() + 2 * HIGHLIGHT_BORDER)
			backgroundNode.setPathTo( new RoundRectangle2D.Float((float) b.x, (float) b.y, (float) (b.width), (float) (b.height), 10.0f , 10.0f) )

			backgroundNode.setStroke(null) //note: this fixes the "leaving trails" refresh problem
		}
	}

	void propertyChanged(def who, String prop, oldVal, newVal) {
		//println "orientedNode prop changed $who $prop $oldVal $newVal"
		super.propertyChanged(who, prop, oldVal, newVal)
		switch (prop){
			case "notes":
				installText(newVal)
				break
//			case "wrap":
//				wrap = newVal
//				installText(component.getPropertyStringValue("notes"))
//				break

			case "fontName":
				fontName = component.getPropertyValue("fontName")
				resetFont()
				break
			case "fontStyle":
				fontStyle = component.getPropertyValue("fontStyle")
				resetFont()
				break
			case "fontSize":
				fontSize = component.getPropertyValue("fontSize")
				resetFont()
				break
			case "rotation":
				rot = Math.toRadians(-newVal)
				rotateNode()
				break
		}

		this.resetBackgroundNode()
	}

	protected void resetFont(){
		pText.setFont( new Font(fontName, fontStyle, fontSize ) )
		this.setBounds(pText.getFullBounds())
	}

	protected void rotateNode(){
		pText.setRotation(rot)
		this.setBounds(pText.getFullBounds())
	}

	protected void installText(String input){
		//pText.setWidth(wrap)
		//pText.setConstrainWidthToTextWidth(false)
		//pText.setConstrainHeightToTextHeight(true)
		pText.setText(input)
		this.setBounds(pText.getFullBounds())
		
/*
		//what we need to do is insert CRs at the wrap interval
		StringBuilder sb = new StringBuilder("")
		int i = -1
		for (Character c : input.toCharArray()){
			i++
			sb.append(c)
			if (i == wrap){
				i = 0;
				sb.append("\n")
			}
			
		}
		pText.setText(sb.toString())
		*/
	}

	/**
	 * Overriding the default PNode implementation because we always want to pick this node before it's children IF the user
	 * has actually clicked on the icon.
	 * @param pickPath the current pick path
	 * @return true if this node has been picked
	 */
	@Override
	protected boolean pick(PPickPath pickPath) {
		if (intersects(pickPath.getPickBounds())) {
			return true;
		}
		return false;
	}
}
