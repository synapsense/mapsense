package DeploymentLab.SceneGraph;

import DeploymentLab.CentralCatalogue;
import edu.umd.cs.piccolo.PNode;
import DeploymentLab.channellogger.*;

import java.util.HashMap;
import java.util.Map;

public enum ShapeFactory {
	INSTANCE;

	private static final Logger log = Logger.getLogger(ShapeFactory.class.getName());
	private Map<String, PNode> shapes;

	private ShapeFactory() {
		shapes = new HashMap<String,PNode>();
	}
	
	public static PNode getShape(String name) {
		if(INSTANCE.shapes.get(name)==null){
			//INSTANCE.shapes.put(name, SvgFactory.parseSvg(name) );
			//log.trace("loading shape " + name, "default");
			PNode result;
			try{
				result = SvgFactory.parseSvg(name);
			}catch(Exception e){
				result = null;
			}
			if (result == null){
				log.trace("loading default shape '" + CentralCatalogue.getApp("shape.default") + "' instead of '" + name + "'", "default");
				result = SvgFactory.parseSvg(CentralCatalogue.getApp("shape.default"));
			}
			INSTANCE.shapes.put(name, result);
		}
		return (PNode)INSTANCE.shapes.get(name).clone();
	}

}