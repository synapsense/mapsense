package DeploymentLab.SceneGraph

import DeploymentLab.DisplayProperties
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.SelectModel
import DeploymentLab.Tools.MoveCode
import DeploymentLab.UndoBuffer
import DeploymentLab.channellogger.Logger
import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.event.PInputEvent
import edu.umd.cs.piccolo.nodes.PPath
import edu.umd.cs.piccolo.nodes.PText
import java.awt.BasicStroke
import java.awt.Color
import java.awt.Font
import java.awt.event.KeyEvent
import java.awt.geom.Line2D
import java.awt.geom.Point2D

/**
 * A class for component which has static number of children
 * The children component will be defined component library.
 * type attribute at children node will define child component type
 * each child node under children node defines detail of child component (name and channel)
 * By Hyeeun Lim
 */
public class StaticChildrenNode extends PNode {
	private static final Logger log = Logger.getLogger(StaticChildrenNode.class.getName());

	private ComponentModel componentModel;
	private OrientedDeploymentNode parentNode;              // parent node is rotatable. so OrientedDeploymentNode
	private PPath connectionNode;                           // line PPath to connect parent and all children
	private Map<String, StaticChildNode> childrenList;      // the map for child component
	private Map<Integer, Point2D> coordList ;               // the map to draw coonetionNode ( a pair of points will draw one connection betwwen parent and child or child and child
	private final String ROLE_ATTRIBUTE_NAME = "role";        // attribute name for role
	private final String OFFSET_SIGN_ATTRIBUTE_NAME = "offsetSign"; // attribute name for offset sign ( -, + => -1,1)
	private final String INDEX_ATTRIBUTE_NAME = "index";            // attribute name of child index (0,1,2,3,4,5,6)
	private final String NAME_ATTRIBUTE_NAME = "name";              // attribute name of child name
    private final String CLASS_ATTRIBUTE_NAME = "class";
    private final String CLASS_ATTRIBUTE_VALUE = "StaticChildrenNode"  ;
    private final float CONNECTION_PATH_THICKNESS = 1.0f;
	private int MAX_CHILD_INDEX;
	private DLComponent component;
	private DisplayProperties displayProperties;
    private SelectModel selectModel;
    private UndoBuffer undoBuffer;
    private boolean isOffsetValidationPass = false;


	public StaticChildrenNode(DLComponent c, DisplayProperties p, ComponentModel componentModel, SelectModel sm, UndoBuffer ub) {
		parentNode = new OrientedDeploymentNode(c, p, ub);
		component = c;
		displayProperties = p;
		this.componentModel = componentModel;
        selectModel = sm ;
        undoBuffer = ub;

		component.addPropertyChangeListener(this, new Closure(this){
			public void call(DLComponent who, String prop, Object oldVal, Object newVal) {
				propertyChanged(who, prop, oldVal, newVal);
			}
		});

        displayProperties.addPropertyChangeListener(this, new Closure(this) {
            public void call(DisplayProperties who){
                displayPropertyChanged(who);
            }
        });

		constructNodes();
	}

	public DLComponent getComponent() {
		return component;
	}

    /**
     * Contstruct parent, childrend and connection nodes
     * parentNode : orientedDeploymentNode
     * children node : StaticChildNode, get list of children from childrenList
     */
	protected void constructNodes(){
		parentNode.addAttribute(CLASS_ATTRIBUTE_NAME, CLASS_ATTRIBUTE_VALUE);
        this.addChild(parentNode);
		childrenList = new HashMap<String, StaticChildNode>();
		coordList = new HashMap<Integer, Point2D>();

		connectionNode = new PPath();
		connectionNode.setStroke(new BasicStroke(CONNECTION_PATH_THICKNESS));
		connectionNode.setStrokePaint(Color.black);
		connectionNode.setPaint(Color.black);
        connectionNode.addAttribute(CLASS_ATTRIBUTE_NAME, CLASS_ATTRIBUTE_VALUE);

		this.addChild(connectionNode);

		Map<String, Map<String, String>> childrenMap = component.getPlaceableChildrenMap();
		String childComponentType = component.getPlaceableChildType();
		MAX_CHILD_INDEX = childrenMap.size()-1;
		int index = 0;

        ArrayList<DLComponent> childComponentList = component.getChildComponents();
		//log.trace("number of childcomponent:" + childComponentList.size());
		for(String childName:childrenMap.keySet() ){
			//log.trace("child for shape" + childName);
			String childDisplay = childrenMap[childName]['display']

			//get each childComponent from componentModel

            final DLComponent childRackComponent = getComponentByProperty(childComponentList, "child_id", childName);

            if(childRackComponent!=null){

            // create childNode as StaticChildNode
                StaticChildNode childNode = new StaticChildNode(childRackComponent, displayProperties, component, undoBuffer);

                double offsetValue = (Double)component.getPropertyValue(childName + "_offset");
                if(offsetValue<0){
                    childNode.addAttribute(OFFSET_SIGN_ATTRIBUTE_NAME, -1.0);
                }else{
                    childNode.addAttribute(OFFSET_SIGN_ATTRIBUTE_NAME, 1.0);
                }

                childNode.addAttribute(NAME_ATTRIBUTE_NAME, childName);
                childNode.addAttribute(INDEX_ATTRIBUTE_NAME, index);
                childNode.addAttribute(ROLE_ATTRIBUTE_NAME, "chidNode");
                childNode.addAttribute(CLASS_ATTRIBUTE_NAME, CLASS_ATTRIBUTE_VALUE);

                PText textNode = new PText();
                textNode.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 1));
                textNode.setTextPaint(Color.black);
                textNode.setText(childDisplay);
                textNode.centerBoundsOnPoint(childNode.getBounds().width/2, childNode.getBounds().height + (double)1.0);
                textNode.scale((double)0.5);
                childNode.addChild(textNode);

                childrenList.put(childName, childNode);
                this.addChild(childNode);

            }
			index++;
		}

		positionChildrenNode();

		this.setChildrenPickable(true);
		//this.setPickable(true);
	}

    /**
     * get component which has a matching property and its value
     * @param cList
     * @param pName
     * @param pValue
     * @return
     */
    private DLComponent getComponentByProperty(ArrayList<DLComponent> cList, String pName, String pValue){
        for(DLComponent c:cList){
            String value = c.getPropertyStringValue(pName);
            if(value.equals(pValue))
                return c;
        }
        return null;
    }

    /**
     * disconnect this object
     */
    void disconnect() {
		parentNode.disconnect();
		component.removePropertyChangeListener(this);
		component = null;
		displayProperties.removePropertyChangeListener( this );
	}

    public PNode getShape() {
        return parentNode
    }

    /**
     * this one will be fired when changing parent component property
     * @param who
     * @param prop
     * @param oldVal
     * @param newVal
     */
	public void propertyChanged(Object who, String prop, Object oldVal, Object newVal) {
        parentNode.propertyChanged( who, prop, oldVal, newVal )

        ArrayList<String> offsetList = new ArrayList<String>();
		for(String childName: childrenList.keySet() ){
			offsetList.add(childName + "_offset");
		}

       if(prop.equals("x") || prop.equals("y") ){
            positionChildrenNode();

        }else if(prop.equals("rotation")){      // when rotating parent component, children components should rotate together
            positionChildrenNode();

        }else if(offsetList.contains(prop)){     // any child moves, needs to update child component position
            String[] tempStrArray = prop.split("_");
            offsetChild(tempStrArray[0], (Double)oldVal);
        }else if(childrenList.keySet().contains(prop)){        // any child name change
            renameAllChildren();
        }

        // when changing x,y coordinate, children component should move together

	}

   public void displayPropertyChanged(Object who){
       // configuration display setting
       boolean isActiveDrawingChild =  displayProperties.isActiveDrawingComponent(component)
       boolean isDisplayable = false

       if(isActiveDrawingChild){
            ArrayList<String> unselectedTypes = (ArrayList<String>)displayProperties.getProperty("invisibleObjects");
            boolean isSelectedType = !unselectedTypes.contains(component.getType())
            isDisplayable = isSelectedType
       }
       this.setVisible(isDisplayable)
        /*
		if(unselectedTypes.contains(component.getType())){
		    this.setVisible(false);
        }else{
            this.setVisible(true);
		}*/
	}

	public StaticChildNode[] getChildrenNodes(){
		return childrenList.values().toArray(new StaticChildNode[childrenList.size()]);
	}

    /**
     * update children node coordinate and connectioNode
     */
	void positionChildrenNode(){
		coordList.clear();
		//log.trace("root coordinate: " + this.getX() + " " + this.getY());
		//log.trace("root coordinate: " + this.getXOffset() + " " + this.getYOffset());


		for(String childName: childrenList.keySet()){
			double offsetValue = (Double)component.getPropertyValue(childName + "_offset");
			//log.trace("offset child:" + childName + ":" +  offsetValue);
			Point2D coord = getCoordinate(offsetValue);

			StaticChildNode childNode = childrenList.get(childName);
			//log.trace(coord.getX() + " , " + coord.getY());
			//childNode.setOffset(coord.getX(), coord.getY());
			DLComponent childComponent = childNode.getComponent();
			//log.trace("positionChildrenNode:");
			//log.trace("x: " +  coord.getX());
			//log.trace("y: " +  coord.getY());

			childComponent.setPropertyValue("x", coord.getX());
			childComponent.setPropertyValue("y", coord.getY());


			//log.trace("child origin position:" + childNode.getGlobalFullBounds().getCenterX() + " , " + childNode.getGlobalFullBounds().getCenterY());
			coordList.put((Integer) childNode.getAttribute(INDEX_ATTRIBUTE_NAME), coord);
		}
		//connectionNode.setPathTo(new Line2D.Float(coordList.first(), coordList.last()))
		//make the connection path a poly line with all the child nodes included
        if(coordList.size()>0)
		    connectionNode.setPathToPolyline( coordList.values().toArray(new Point2D[coordList.size()]) );

	}

    /**
     * rename all child components
     */
	void renameAllChildren(){
		for(String childName: childrenList.keySet()){
			String newName = component.getPropertyValue(childName + "_name").toString();
			PNode childNode = childrenList.get(childName);
			((PText)((PNode)childNode).getChild(1)).setText(newName);
		}
	}

    /**
     * get real point with offset value
     * @param offsetValue
     * @return
     */
	Point2D getCoordinate(double offsetValue){
		double rotation = Math.toRadians( -(Double)component.getPropertyValue("rotation") );
		double x = (Double)component.getPropertyValue("x") - (offsetValue * Math.sin(rotation));
		double y = (Double)component.getPropertyValue("y") + (offsetValue * Math.cos(rotation));
		//double x =  - (offsetValue * Math.sin(rotation));
		//double y =  + (offsetValue * Math.cos(rotation));
		Point2D pt = new Point2D.Double(x, y);
		return pt;
	}


	void setEffects(boolean e) {}

    /**
     * when parentNode is selected or deselected
     * @param s
     * @return
     */
	public boolean setSelected(boolean s) {
		//return selected = s;
		return parentNode.setSelected(s);
	}

    /**
     * move child component with mouse event
     * with mouse event, get offsetvalue
     * and if that is a valid position, move child component
     * @param e
     */
   /* private void offsetChild(PInputEvent e){
       try{
		    undoBuffer.startOperation();
            PNode node = e.getPickedNode();

           // log.trace("selected objects:" + selectModel.getSelectedObjects().size());

            while(node.getAttribute(ROLE_ATTRIBUTE_NAME)==null){
                node = node.getParent();
            }

            double offsetValue = getChildOffsetValue(e.getPosition(), node);

            String childName = (String)node.getAttribute(NAME_ATTRIBUTE_NAME);
            if(!validatePosition(childName, offsetValue)){
                log.trace("Wrong position");
                return;
            }

            component.setPropertyValue(node.getAttribute(NAME_ATTRIBUTE_NAME) + "_offset", offsetValue);

        }catch(Exception ex){
            log.error(log.getStackTrace(ex));
			undoBuffer.rollbackOperation();
	    } finally {
            undoBuffer.finishOperation();
        }

   	}*/


    /*private void offsetChild(Point2D destinationP, StaticChildNode childNode ){
        try{
            undoBuffer.startOperation();

            double offsetValue = getChildOffsetValue(destinationP, childNode);

            String childName = (String)childNode.getAttribute(NAME_ATTRIBUTE_NAME);
            if(!validatePosition(childName, offsetValue)){
                log.trace("Wrong position");
                return;
            }

            component.setPropertyValue(childNode.getAttribute(NAME_ATTRIBUTE_NAME) + "_offset", offsetValue);

        }catch(Exception ex){
            log.error(log.getStackTrace(ex));
            undoBuffer.rollbackOperation();
        } finally {
            undoBuffer.finishOperation();
        }
   }  */


    private void offsetChildByKey(PInputEvent e){
        try{
            undoBuffer.startOperation();
            PNode node = e.getPickedNode();

           // log.trace("selected objects:" + selectModel.getSelectedObjects().size());

            while(node.getAttribute(ROLE_ATTRIBUTE_NAME)==null){
                node = node.getParent();
            }

            int keyCode = e.getKeyCode();
            String childName = (String)node.getAttribute(NAME_ATTRIBUTE_NAME);

            //log.trace("keyCode:" + keyCode);
            //log.trace("child:" + childName);
            Point2D point = new Point2D.Double();
            Point2D pickedPoint = node.getOffset();
            //log.trace("picked Point:" + pickedPoint.getX() + " , " + pickedPoint.getY());
            switch (keyCode){
                case KeyEvent.VK_DOWN:
            //        log.trace("down");
                    point.setLocation(pickedPoint.getX(), pickedPoint.getY() + 1);
                    break;
                case KeyEvent.VK_UP:
            //        log.trace("up");
                    point.setLocation(pickedPoint.getX(), pickedPoint.getY() - 1);
                    break;
                case KeyEvent.VK_LEFT:
            //        log.trace("left");
                    point.setLocation(pickedPoint.getX() - 1, pickedPoint.getY());
                    break;
                case KeyEvent.VK_RIGHT:
             //       log.trace("rigth");
                    point.setLocation(pickedPoint.getX() + 1, pickedPoint.getY());
                    break;
            }
            //log.trace("nwe point:" + point.toString());
            double offsetValue = getChildOffsetValue(point, node);
            //log.trace("offsetValue:" + offsetValue);

            if(!validatePosition(childName, offsetValue)){
                log.trace("Wrong position");
                return;
            }

            component.setPropertyValue(node.getAttribute(NAME_ATTRIBUTE_NAME) + "_offset", offsetValue);

        }catch(Exception ex){
            log.error(log.getStackTrace(ex));
			undoBuffer.rollbackOperation();
        }finally {
            undoBuffer.finishOperation();
        }
    }

    /**
     * some alignment or distribute function will use the offsetChild method
     * @param node
     * @param p
     */
    /*private void offsetChild(PNode node, Point2D p){
        double offsetValue = getChildOffsetValue(p, node);

        String childName = (String)node.getAttribute(NAME_ATTRIBUTE_NAME);
        if(!validatePosition(childName, offsetValue)){
            log.trace("Wrong position");
            return;
        }

        component.setPropertyValue(node.getAttribute(NAME_ATTRIBUTE_NAME) + "_offset", offsetValue);
    } */
    /**
     * offset child component if a new value is valid
     * this is called when offset property is changed
     * @param childName
     * @param oldValue
     */
	private void offsetChild(String childName, double oldValue){
		double offsetValue = (Double)component.getPropertyValue(childName + "_offset");
        //log.trace("isOffsetValidationPass:" + isOffsetValidationPass)
        if(!isOffsetValidationPass){
            if(!validatePosition(childName, offsetValue)){
            //log.trace("Wrong position" + offsetValue);
            component.setPropertyValue(childName + "_offset", oldValue);
            return;
            }
        }

		//log.trace("offset child:" + childName + ":" +  offsetValue);
		Point2D coord = getCoordinate(offsetValue);

		DeploymentNode childNode = childrenList.get(childName);

		//log.trace("parent:" +component.getPropertyValue("x") + " , " + component.getPropertyValue("y"));
		//log.trace("child[" + childName + "]" + coord.getX() + " , " + coord.getY());
		//childNode.setOffset(coord.getX(), coord.getY());

		childNode.getComponent().setPropertyValue("x",  coord.getX());
		childNode.getComponent().setPropertyValue("y",  coord.getY());

		coordList.put((Integer) childNode.getAttribute(INDEX_ATTRIBUTE_NAME), coord);
		connectionNode.setPathToPolyline( coordList.values().toArray(new Point2D[coordList.size()]) );

        isOffsetValidationPass = false;
	}

    /**
     * validate offset value with provided child name and offsetvalue
     * valid: return true
     * invalid: return false
     * @param childName
     * @param offsetValue
     * @return
     */
	public boolean validatePosition(String childName, double offsetValue){
		return true;
		//for the time being, we have removed all location restrictions
/*
		//println "validatePosition $childName, $offsetValue"
        StaticChildNode childNode = childrenList.get(childName);
		//println "$childNode"
        int index = (Integer)childNode.getAttribute(INDEX_ATTRIBUTE_NAME);
        double offsetSign = (Double)childNode.getAttribute(OFFSET_SIGN_ATTRIBUTE_NAME)

        log.trace("index:" + index + " offsetValue:" + offsetValue);
        int prevIndex = index - 1;
        int nextIndex = index + 1;

        if(prevIndex>=0){
            StaticChildNode prevNode = getChildNodeByIndex(prevIndex);
            log.trace("preNode name:" + prevNode.getAttribute(NAME_ATTRIBUTE_NAME) + "_offset");
            double prevOffsetValue =(Double)component.getPropertyValue(prevNode.getAttribute(NAME_ATTRIBUTE_NAME) + "_offset");

            log.trace("prevIndex:" + prevIndex + " offsetValue:" + prevOffsetValue );
            if(prevOffsetValue>(offsetValue-15)){
                log.trace("mismatch with previndex");

                return false;
            }

        }

        if(nextIndex<childrenList.size()){
            StaticChildNode nextNode = getChildNodeByIndex(nextIndex);
            log.trace("nextNode name:" + nextNode.getAttribute(NAME_ATTRIBUTE_NAME) + "_offset");
            double nextOffsetValue =(Double)component.getPropertyValue(nextNode.getAttribute(NAME_ATTRIBUTE_NAME) + "_offset");
            log.trace("nextIndex:" + nextIndex + " offsetValue:" + nextOffsetValue );

            if(nextOffsetValue<(offsetValue+15)){
                log.trace("mismatch with nextindex");
                return false;
            }
        }

        if(offsetSign * offsetValue <0 ){
            log.trace("child cannot cross parent")
            return false;
        }


        isOffsetValidationPass =true;
        return true;
*/
    }
    /**
     * Distribute children node
     * Should check new position is valid or not before setting new offsetValue
     * To check new position, offset values for prev child or next child should be new values.
     * @param newPositions
     */
    public void distributeChildren(LinkedHashMap<StaticChildNode, Point2D.Double> newPositions, String direction){

        ListIterator iterator = newPositions.keySet().toList().listIterator()

        StaticChildNode prevNode = null;
        StaticChildNode nextNode = null;
        while(iterator.hasNext()){
            StaticChildNode currentNode = (StaticChildNode) iterator.next()

            double offsetValue = getChildOffsetValue(newPositions.get(currentNode), currentNode, direction);

            if(iterator.hasNext()){
                nextNode = iterator.next();
                iterator.previous();

            }else{
                nextNode = null;
            }

            double prevOffsetValue = 0.0;
            double nextOffsetValue = 0.0;

            if(prevNode!=null)
                prevOffsetValue = getChildOffsetValue(newPositions.get(prevNode), prevNode, direction);

            if(nextNode!=null)
                nextOffsetValue =  getChildOffsetValue(newPositions.get(nextNode), nextNode,direction);

            //log.trace("distribute:" + currentNode.getAttribute(NAME_ATTRIBUTE_NAME))
            //log.trace("new Point:" + newPositions.get(currentNode).getX() + "," + newPositions.get(currentNode).getY())
            //log.trace("offsetValue:" + offsetValue)
            //log.trace("prevOffset:" + prevOffsetValue)
            //log.trace("nextOffsetL" + nextOffsetValue)
            if(validatePosition(prevOffsetValue, nextOffsetValue, offsetValue)){
                component.setPropertyValue(currentNode.getAttribute(NAME_ATTRIBUTE_NAME) + "_offset", offsetValue);
            }

            //log.trace(currentNode.getComponent().getPropertyValue("x") + "," + currentNode.getComponent().getPropertyValue("y"))
            prevNode = currentNode;
        }
    }

    public void alignChildren(LinkedHashMap<StaticChildNode, Point2D.Double> newPositions, String direction){
        ListIterator iterator = newPositions.keySet().toList().listIterator()

        StaticChildNode prevNode = null;
        StaticChildNode nextNode = null;
        while(iterator.hasNext()){
            StaticChildNode currentNode = (StaticChildNode) iterator.next()

            double offsetValue = getChildOffsetValue(newPositions.get(currentNode), currentNode, direction);

            if(iterator.hasNext()){
                nextNode = iterator.next();
                iterator.previous();

            }else{
                nextNode = null;
            }

            double prevOffsetValue = 0.0;
            double nextOffsetValue = 0.0;

            if(prevNode!=null)
                prevOffsetValue = getChildOffsetValue(newPositions.get(prevNode), prevNode, direction);

            if(nextNode!=null)
                nextOffsetValue =  getChildOffsetValue(newPositions.get(nextNode), nextNode, direction);

            //log.trace("align:" + currentNode.getAttribute(NAME_ATTRIBUTE_NAME))
            //log.trace("new Point:" + newPositions.get(currentNode).getX() + "," + newPositions.get(currentNode).getY())
            //log.trace("offsetValue:" + offsetValue)
            //log.trace("prevOffset:" + prevOffsetValue)
            //log.trace("nextOffsetL" + nextOffsetValue)
            if(validatePosition(prevOffsetValue, nextOffsetValue, offsetValue)){
                component.setPropertyValue(currentNode.getAttribute(NAME_ATTRIBUTE_NAME) + "_offset", offsetValue);
                //log.trace("it's valid")
            }

            //log.trace(currentNode.getComponent().getPropertyValue("x") + "," + currentNode.getComponent().getPropertyValue("y"))
            prevNode = currentNode;
        }
    }

    public boolean validatePosition(double prevOffsetValue, double nextOffsetValue, double offsetValue){
		return true;
		//for the time being, we have removed all location restrictions
		/*
        if(prevOffsetValue>0.0){

            //log.trace("prevNode offset:" + prevOffsetValue );
            if(prevOffsetValue>(offsetValue-15)){
                log.trace("mismatch with previndex");
                return false;
            }
        }

        if(nextOffsetValue>0.0){
            //log.trace("nextNode  offsetValue:" + nextOffsetValue );

            if(nextOffsetValue<(offsetValue+15)){
                //log.trace("mismatch with nextindex");
                return false;
            }
        }

        isOffsetValidationPass = true;
        return true;
        */
    }


    /**
     * get child component node by index
     * @param index
     * @return
     */
    public StaticChildNode getChildNodeByIndex(int index){
        for(StaticChildNode c: childrenList.values()){
            int i = (Integer)c.getAttribute(INDEX_ATTRIBUTE_NAME);
            if(index == i)
                return c;
        }
        return null;
    }

    /**
     * calculate child offset value with mouseEvent and selected node
     * @param p the desired target point - this is where the mouse cursor is during a drag
     * @param node the child node to be moved
     * @return a corrected offset value
     */
	private double getChildOffsetValue(Point2D p, PNode node){
		//log.trace( "getChildOffsetValue( $p, $node )")
		double value = 0.0;

		//log.trace("coordlist is $coordList")

		//Point2D startP = new Point2D.Double(coordList.get(0).getX(), coordList.get(0).getY());
		//log.trace("start x:" + startP.getX() + " y:" + startP.getY());
		//Point2D endP = new Point2D.Double(coordList.get(coordList.size()-1).getX(), coordList.get(coordList.size()-1).getY());
		//log.trace("end x:" + endP.getX() + " y:" + endP.getY());

		//but!  What if all the subracks (and the center) are at the exact same point!
		//trouble, that's what.
		//rather than grab a second point from the coordlist, compute a fake point out about 10 units based on the rotation of the whole thing.
		//we need a line that represents the "spine" of the row.
		//rather than use the locations of the subracks themselves, we'll draw a line from the center node
		//to a point <some number> away at the correct rotation.  This way, bad subrack locations can't screw this up.

		Point2D startP = parentNode.getOffset()
		log.trace("start x:" + startP.getX() + " y:" + startP.getY());

		Point2D endP = getCoordinate(42)
		log.trace("end x:" + endP.getX() + " y:" + endP.getY());\

		Line2D line = new Line2D.Double(startP, endP);

		//log.trace("dragged x:" + p.getX() + " y:" + p.getY());

		Point2D centerP = parentNode.getOffset();
		log.trace("center x:" + centerP.getX() + " y:" + centerP.getY());

		double d1 = line.ptLineDist(p);
		double d2 = centerP.distance(p);

       // log.trace((String)node.getAttribute(NAME_ATTRIBUTE_NAME)) ;
        value = Math.sqrt(d2*d2 - d1*d1)
		//log.trace("d1:" + d1 + " d2:" + d2);
		//subnodes can now cross the center, so we can't just assign a pos/neg based on starting location
        //value*=(Double)node.getAttribute(OFFSET_SIGN_ATTRIBUTE_NAME);

		//which side is the mouse closer to: the positive or negative?
		value *= getOffsetSign(p)
		//log.trace("final offset value:" + value);
		return value;
	}


	/***
	 * Compute the sign of the offset based on which side of the line the mouse cursor (or other destination point) is closest to
	 * @param mouseLocation the current mouse location
	 * @return {@code 1} if the mouse is on the positive side, and {@code -1} if the mouse is on the negative side
	 */
	protected int getOffsetSign(Point2D mouseLocation){
		int sign = 0
		Point2D plus = getCoordinate(1)
		Point2D minus = getCoordinate(-1)
		if (mouseLocation.distance(plus) < mouseLocation.distance(minus)){
			sign = 1
		} else {
			sign = -1
		}
		return sign
	}


    /**
     * Get child offset value for distribution
     * @param p
     * @param node
     * @param direction
     * @return
     */
   private double getChildOffsetValue(Point2D p, PNode node, String direction) {
        // get matching point on the guide line
        double x1 = connectionNode.getX();
        double y1 = connectionNode.getY();

        double x2 = connectionNode.getX() + connectionNode.getWidth() - CONNECTION_PATH_THICKNESS;
        double y2 = connectionNode.getY() + connectionNode.getHeight() -CONNECTION_PATH_THICKNESS;

        double x = 0.0;
        double y = 0.0;


        //log.trace("x1:" + x1 + " y1:" + y1)
        //log.trace("x2:" + x2 + " y2:" + y2)

        if(direction.equals("v")){
            // get x value
            y = p.getY()
            if(y1==y2)
                x = p.getX();
            else
                x = (y-y1)*(x2-x1)/(y2-y1) + x1;
        } else{
            //get y value
            x = p.getX();
            if(x2==x1)
                y = p.getY();
            else
                y = (y2-y1)/(x2-x1)*(x-x1) + y1;
        }

        //log.trace((String)node.getAttribute(NAME_ATTRIBUTE_NAME)) ;
        //log.trace("connectionNode: x: " + connectionNode.getX());
        //log.trace("connectionNode: y: " + connectionNode.getY());
        //log.trace("connectionNode: x: " + connectionNode.getX()+connectionNode.getWidth());
        //log.trace("connectionNode: x: " + connectionNode.getY()+connectionNode.getHeight());
        //log.trace("before calculation new point" + p.getX() + " , " + p.getY());
        //log.trace("after calculation new point" + x + " , " + y);
        Point2D.Double newPoint = new Point2D.Double(x,y);
        Point2D centerP = parentNode.getOffset();
        //log.trace("center x:" + centerP.getX() + " y:" + centerP.getY());

        double value = 0.0;
        value = newPoint.distance(centerP)
	   	//can't just use the sign anymore - need to check which side we're one
        //value = value * (Double)node.getAttribute(OFFSET_SIGN_ATTRIBUTE_NAME);
		value *= getOffsetSign(p)

        //log.trace("value:" + value);
        return value;
   }

    /**
     * A class for child component. this object will child of main StaticChildrenNode.
     */
	public class StaticChildNode extends OrientedDeploymentNode{
		private static final Logger log = Logger.getLogger(StaticChildNode.class.getName())

        private DLComponent parentComponent;

		public StaticChildNode(DLComponent c, DisplayProperties p, DLComponent parentComponent, UndoBuffer ub){
			super( c, p, ub )
		    this.parentComponent = parentComponent;

			component.addPropertyChangeListener(this, new Closure(this){
				public void call(DLComponent who, String prop, Object oldVal, Object newVal) {
					//log.trace("call propertyChanged for" + prop );
					propertyChanged(who, prop, oldVal, newVal);
				}
			});
		}

        public DLComponent getParentComponent(){
            return parentComponent;
        }

        /**
         * this event will be fired when changing child component position or name
         * @param who
         * @param prop
         * @param oldVal
         * @param newVal
         */
		public void propertyChanged(Object who, String prop, Object oldVal, Object newVal) {
			//super.propertyChanged(who, prop, oldVal, newVal);

			if(prop.equals("x") || prop.equals("y")){
				//log.trace("user tries to move childnode");
				this.setOffset((Double)component.getPropertyValue("x"), (Double)component.getPropertyValue("y"));
			} else {
				super.propertyChanged(who, prop, oldVal, newVal);
			}
		}

        /**
         * offset child node
         * @param destinationP the target point the the client wants to move this child node to
         */
        public void offset(Point2D destinationP, boolean isChildOnly ){
			//log.trace("offset($destinationP, $isChildOnly)")
            ArrayList<DLComponent> selections = selectModel.getSelectedObjects();

            if(isChildOnly && selections.contains(parentComponent)){
                log.trace("isChildOnly and parent is selected")
                return;

            }

            double offsetValue = getChildOffsetValue(destinationP, this);

            String childName = (String)this.getAttribute(NAME_ATTRIBUTE_NAME);
            if(!validatePosition(childName, offsetValue)){
                log.trace("Wrong position");
                return;
            }else{
                log.trace("good position")
            }
			//log.trace("set ${this.getAttribute(NAME_ATTRIBUTE_NAME)}_offset to $offsetValue")
            parentComponent.setPropertyValue(this.getAttribute(NAME_ATTRIBUTE_NAME) + "_offset", offsetValue);
        }

        public void offsetByCoord(String key,Point2D.Double p){
			//log.trace("offsetByCoord( $key, $p )")
            double offsetValue;

			//use the same offset calculation
			offsetValue = getChildOffsetValue(p, this);

/*
            if(key.equals("x")){
                offsetValue = getChildOffsetValueByX(p, this);
            }else{
                offsetValue = getChildOffsetValueByY(p, this);
            }
*/
			//log.trace("offsetByCoord gets offset value $offsetValue")

            String childName = (String)this.getAttribute(NAME_ATTRIBUTE_NAME);
            if(!validatePosition(childName, offsetValue)){
                log.trace("Wrong position");
                return;
            }
            parentComponent.setPropertyValue(this.getAttribute(NAME_ATTRIBUTE_NAME) + "_offset", offsetValue);
        }

        /**
         * offset child node by keyboard
         * @param moveCode
         */
        public void offsetByKey(MoveCode moveCode){

            ArrayList<DLComponent> selections = selectModel.getSelectedObjects();

            if(selections.contains(parentComponent))
                return;

            String childName = (String)this.getAttribute(NAME_ATTRIBUTE_NAME);

            //log.trace("keyCode:" + keyCode);
            //log.trace("child:" + childName);
            Point2D point = new Point2D.Double();
            Point2D pickedPoint = this.getOffset();
            //log.trace("picked Point:" + pickedPoint.getX() + " , " + pickedPoint.getY());
            switch (moveCode){
                case MoveCode.DOWN:
            //        log.trace("down");
                    point.setLocation(pickedPoint.getX(), pickedPoint.getY() + 1);
                    break;
                case MoveCode.UP:
            //        log.trace("up");
                    point.setLocation(pickedPoint.getX(), pickedPoint.getY() - 1);
                    break;
                case MoveCode.LEFT:
            //        log.trace("left");
                    point.setLocation(pickedPoint.getX() - 1, pickedPoint.getY());
                    break;
                case MoveCode.RIGHT:
             //       log.trace("rigth");
                    point.setLocation(pickedPoint.getX() + 1, pickedPoint.getY());
                    break;
            }
            //log.trace("nwe point:" + point.toString());
            double offsetValue = getChildOffsetValue(point, this);
            //log.trace("offsetValue:" + offsetValue);

            if(!validatePosition(childName, offsetValue)){
                log.trace("Wrong position");
                return;
            }
            parentComponent.setPropertyValue(this.getAttribute(NAME_ATTRIBUTE_NAME) + "_offset", offsetValue);
        }


	}
}
