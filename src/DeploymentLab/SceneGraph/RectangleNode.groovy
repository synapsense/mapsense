package DeploymentLab.SceneGraph

import DeploymentLab.DisplayProperties
import DeploymentLab.Model.DLComponent
import DeploymentLab.channellogger.Logger
import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.event.PBasicInputEventHandler
import edu.umd.cs.piccolo.nodes.PPath
import edu.umd.cs.piccolo.util.PBounds
import edu.umd.cs.piccolo.util.PPickPath
import edu.umd.cs.piccolox.handles.PBoundsHandle

import java.awt.BasicStroke
import java.awt.Color
import java.awt.geom.Line2D
import java.awt.geom.Point2D
import java.beans.PropertyChangeEvent
import java.beans.PropertyChangeListener
import edu.umd.cs.piccolo.event.PInputEvent
import DeploymentLab.UndoBuffer
import edu.umd.cs.piccolox.util.PBoundsLocator


/**
 * Extension of the generic DeploymentNode that includes a reshapable rectangle, complete with grabable shape points.
 */
class RectangleNode extends DeploymentNode implements PropertyChangeListener {
	private static final Logger log = Logger.getLogger(RectangleNode.class.getName())

	private ArrayList<CustomPBoundsHandle> resizeDots = new ArrayList<CustomPBoundsHandle>();
	protected PPath rectNode = new PPath();
	protected PPath selectPolygon = new PPath();
	protected double width
	protected double depth
	private float lineWidth = 1;

	// Some cached representations used often.
	private ArrayList<Point2D> points
	private ArrayList<Line2D> lines

	RectangleNode(DLComponent c, DisplayProperties p, UndoBuffer ub) {
		super(c, p)
		//log.trace("init RectangleNode for $c")

		width = c.getPropertyValue('width') ?: 0
		depth = c.getPropertyValue('depth') ?: 0

		reshape()
		rectNode.setVisible((boolean) (displayProperties.getProperty('orientation')))

		// add drag handles for the rectangle. Use our custom PBoundHandler so that undo works properly.
		resizeDots.add( new CustomPBoundsHandle( PBoundsLocator.createEastLocator(rectNode), ub ) )
		resizeDots.add( new CustomPBoundsHandle( PBoundsLocator.createWestLocator(rectNode), ub ) )
		resizeDots.add( new CustomPBoundsHandle( PBoundsLocator.createNorthLocator(rectNode), ub ) )
		resizeDots.add( new CustomPBoundsHandle( PBoundsLocator.createSouthLocator(rectNode), ub ) )
		resizeDots.add( new CustomPBoundsHandle( PBoundsLocator.createNorthEastLocator(rectNode), ub ) )
		resizeDots.add( new CustomPBoundsHandle( PBoundsLocator.createNorthWestLocator(rectNode), ub ) )
		resizeDots.add( new CustomPBoundsHandle( PBoundsLocator.createSouthEastLocator(rectNode), ub ) )
		resizeDots.add( new CustomPBoundsHandle( PBoundsLocator.createSouthWestLocator(rectNode), ub ) )

		// Don't child the dots to the rectNode because we want the highlight over the line but under the handles.
		this.addChild(rectNode)
		this.addChild(selectPolygon)
		this.addChildren(resizeDots)

		setResizeDotsVisible( false )
		selectPolygon.setVisible( selected )

		this.setChildrenPickable(true)
		selectPolygon.setPickable( false )

		rectNode.addPropertyChangeListener(this)

		ResizeInputEventHandeler dotListener = new ResizeInputEventHandeler()
		for( CustomPBoundsHandle dot : resizeDots ) {
			dot.addInputEventListener( dotListener )
		}
	}

	public PPath getPath() {
		return rectNode
	}

	/**
	 * Get the Rectangle as a list of corner points.
	 * @return
	 */
	public ArrayList<Point2D> getPoints() {
		if( points == null ) {
			PBounds rect = rectNode.getGlobalBounds()
			points = polylineFromRectangle( rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight() )
		}
		return points
	}

	/**
	 * Get the Rectangle as a list of lines.
	 * @return
	 */
	public ArrayList<Line2D> getLines() {
		if( lines == null ) {
			lines = ArbitraryShapeNode.convertPointsToLines( getPoints() )
		}
		return lines
	}

	protected void reshape() {
		if( rectNode ) {
			int rectX = -(depth / 2)
			int rectY = -(width / 2)
			rectNode.setPathToPolyline( polylineFromRectangle( rectX, rectY, depth.intValue(), width.intValue() ))

			if( component.hasProperty('lineColor') ) {
				rectNode.setStrokePaint((Color) component.getPropertyValue('lineColor'))
			}

			if( component.hasProperty('lineWidth') ) {
				lineWidth = (float) component.getPropertyValue('lineWidth')
				rectNode.setStroke(new BasicStroke(lineWidth, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER))
			} else {
				lineWidth = rectNode.getStroke().getLineWidth()
			}

			if( component.hasProperty('fillColor') ) {
				Color color = (Color) component.getPropertyValue('fillColor')
				if( component.hasProperty('fillAlpha') ) {
					color = new Color( color.getRed(), color.getGreen(), color.getBlue(), (int) component.getPropertyValue('fillAlpha') )
				}
				rectNode.setPaint( color )
			}

			if( component.hasProperty('x') && component.hasProperty('y') ) {
				this.setOffset( component.getPropertyValue('x'), component.getPropertyValue('y') )
			}

			// Make sure the PHandles move to the new bounds aince we aren't childing them to the rectNode itself.
			this.setBounds( rectNode.getBounds() )

			// and the select highlight under it.
			selectPolygon.setPathTo( rectNode.getPathReference() )
			selectPolygon.setStrokePaint( DeploymentNode.selectColor )
			selectPolygon.setStroke(new BasicStroke((float)(lineWidth + ArbitraryShapeNode.HIGHLIGHT_WIDTH), BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER))

			points = lines = null
		}
	}

	public void disconnect() {
		component.removePropertyChangeListener(this)
		component = null
		displayProperties.removePropertyChangeListener(this)
	}

	public PNode getShape() {
		return rectNode
	}

	void remove(PNode n) {}

	/**
	 * Updates the rectangle shape.
	 * To allow the user to "click through" the rectangle, this needs to be a polygon, not an actual rectangle.
	 * For ease of use, this takes the rectangle data and hands out an array of points
	 * @param rectX    x-coordinate of upper-left of rectangle
	 * @param rectY    y-coordinate of upper-left of rectangle
	 * @param rectD    depth of rectangle (added to x)
	 * @param rectW    width of rectangle (added to y)
	 * @return an array of points representing a poly-line version of this rectangle, all ready to be handed to a PPath
	 * @see PPath#setPathToPolyline
	 */
	protected Point2D[] polylineFromRectangle(Double rectX, Double rectY, Double rectD, Double rectW){
		def points = [new Point2D.Double(rectX, rectY), new Point2D.Double(rectX+rectD, rectY),
				      new Point2D.Double(rectX+rectD, rectY+rectW), new Point2D.Double(rectX, rectY+rectW),
				      new Point2D.Double(rectX, rectY)]
		return points.toArray(new Point2D[5])
	}

	void propertyChanged(def who, String prop, oldVal, newVal) {
		if( prop == 'width' ) {
			width = newVal
		} else if( prop == 'depth' ) {
			depth = newVal
		}

		reshape()
	}

	void displayPropertyChanged(def who) {
		super.displayPropertyChanged(who)
        boolean isOrientationDisplayable = (boolean) (displayProperties.getProperty('orientation'))
        boolean isActiveDrawingChild =  displayProperties.isActiveDrawingComponent(component)
        boolean isDisplayable = isOrientationDisplayable && isActiveDrawingChild

        //log.debug(component.getPropertyStringValue("name") + "displayPropertiesChanged" +  isActiveDrawingChild.toString())

		rectNode.setVisible(isDisplayable)
	}

	/**
	 * Override the default so that we can pick this node when the underlying rectangle is what was selected and not
	 * pick the node when a corner dot is selected since we handle that ourselves.
	 * @param pickPath the current pick path
	 * @return true if this node has been picked
	 */
	@Override
	protected boolean pick(PPickPath pickPath) {
		if( !isDragging() ) {
			// Check to see if this is over a point and don't pick if it is.
			for( CustomPBoundsHandle dot : resizeDots ) {
				if( dot.intersects( pickPath.getPickBounds().getBounds2D() ) ) {
					return false
				}
			}

			// This does the same thing as the standard PPath.intersects except that it will only pick along the actual
			// polygon lines and not the inner fill space. It's essentially a copy and paste of that code with the paint
			// intersection statement removed. Replaces: return polygon.intersects( pickPath.getPickBounds() )
			if( rectNode.getBoundsReference().intersects( pickPath.getPickBounds() ) ) {
				return rectNode.getStroke().createStrokedShape( rectNode.getPathReference() ).intersects( pickPath.getPickBounds() )
			}
		}
		return false;
	}

	boolean updating = false

	/**
	 * Called if the PBoundsHandles change the bounds of the node; in this case, we need to update the underlying component.
	 * @param evt
	 */
	@Override
	void propertyChange(PropertyChangeEvent evt) {
		if (updating) { return; }
		//println "EVENT: $evt"
		//println evt.propertyName + " --> " + evt.newValue
		if (evt.propertyName.equals("bounds")) {
			//recompute!
			updating = true
			PBounds newBounds = (PBounds) evt.newValue
			//need to subtract off the width of the border line
			int rectDepth = newBounds.getWidth() - lineWidth  //y-axis
			int rectWidth = newBounds.getHeight() - lineWidth //x-axis
			if (this.getComponent().getPropertyValue("width") != rectWidth) {
				this.getComponent().setPropertyValue("width", rectWidth)
			}
			if (this.getComponent().getPropertyValue("depth") != rectDepth) {
				this.getComponent().setPropertyValue("depth", rectDepth)
			}
			updating = false
		}
	}

	boolean setSelected(boolean s) {
		super.setSelected( s )
		selectPolygon.setVisible( selected )
		return s
	}

	public void setDragging( boolean d ) {
		super.setDragging( d )

		// Hide the extra stuff during a drag so it doesn't block the view.
		if( isDragging() ) {
			selectPolygon.setVisible( false )
			setResizeDotsVisible( false )
		} else {
			selectPolygon.setVisible( selected )
		}
	}

	public void setHovered(boolean h) {
		super.setHovered( h )
		if( !isDragging() ) {
			setResizeDotsVisible( h )
		}
	}

	private void setResizeDotsVisible( boolean v ) {
		float t = ( v ? 1 : 0 )
		for( CustomPBoundsHandle dot : resizeDots ) {
			dot.setTransparency( t )
		}
	}

	class ResizeInputEventHandeler extends PBasicInputEventHandler {

		public void mouseEntered(PInputEvent e){
			e.getPickedNode().setTransparency(1)
		}

		public void mouseExited(PInputEvent e){
			e.getPickedNode().setTransparency(0)
		}
	}
}


/**
 * A custom version of the PBoundsHandler that allows us to act on the handle events. This class has the inherited
 * behavior and only overrides a couple methods to allow us to inject some UndoBuffer management before calling super().
 * If PBoundsHandle had an ability to add a handle events listener, this would not be necessary.
 *
 * Without this custom version, the UndoBuffer replays each micro drag event location rather than snapping back to the
 * original pre-drag location.
 */
class CustomPBoundsHandle extends PBoundsHandle {

    UndoBuffer undoBuffer

    public CustomPBoundsHandle(final PBoundsLocator locator, UndoBuffer ub) {
        super(locator)
        this.undoBuffer = ub
    }

    public void startHandleDrag(final Point2D aLocalPoint, final PInputEvent aEvent) {
        undoBuffer.startOperation()
        super.startHandleDrag( aLocalPoint, aEvent )
    }

    public void endHandleDrag(final Point2D aLocalPoint, final PInputEvent aEvent) {
        super.endHandleDrag( aLocalPoint, aEvent )
        undoBuffer.finishOperation()
    }
}
