package DeploymentLab.SceneGraph;

import DeploymentLab.UndoBuffer;
import groovy.lang.Closure;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.geom.AffineTransform;
import java.awt.geom.Dimension2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.ArrayList; 
import java.util.Map;

import edu.umd.cs.piccolo.*;
import edu.umd.cs.piccolo.event.PBasicInputEventHandler;
import edu.umd.cs.piccolo.event.PDragSequenceEventHandler;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.event.PInputEventListener;
import edu.umd.cs.piccolo.nodes.*;

import DeploymentLab.DisplayProperties;
import DeploymentLab.Model.DLComponent;
import DeploymentLab.channellogger.Logger;

class ChildrenNode extends PNode {
	private static final Logger log = Logger.getLogger(ChildrenNode.class.getName());

	private OrientedDeploymentNode parentNode;
	private PNode childrenNode; 
	private PPath connectionNode; 
	private Map<String, PNode> childrenList;        
	private Map<Integer, Point2D> coordList ;
	private final String ROLE_ATTRIBUTE_NAME = "role";
	private final String OFFSET_SIGN_ATTRIBUTE_NAME = "offsetSign";
	private final String INDEX_ATTRIBUTE_NAME = "index";
	private final String NAME_ATTRIBUTE_NAME = "name";
	
	private DLComponent component;
	private DisplayProperties displayProperties;
	
	public ChildrenNode(DLComponent c, DisplayProperties p, UndoBuffer ub) {
		parentNode = new OrientedDeploymentNode(c, p, ub);
		component = c;
		displayProperties = p;
		
		component.addPropertyChangeListener(this, new Closure(this){
			public void call(DLComponent who, String prop, Object oldVal, Object newVal) {
				log.info("call propertyChanged for" + prop );
				propertyChanged(who, prop, oldVal, newVal);
			}
		});
		
		constructNodes();
	}

	public DLComponent getComponent() {
		return component;
	}
	
	protected void constructNodes(){
		this.addChild(parentNode);
		childrenList = new HashMap<String, PNode>();        
		coordList = new HashMap<Integer, Point2D>();
		childrenNode = new PNode();
		
		connectionNode = new PPath();
		connectionNode.setStroke(new BasicStroke(1.0f));
		connectionNode.setStrokePaint(Color.black);
		connectionNode.setPaint(Color.black);
		/*connectionNode.addInputEventListener(new PBasicInputEventHandler(){
			public void mouseClicked(PInputEvent event){
				log.info("connectionNode mouseClicked");
			}
			
			public void mouseEntered(PInputEvent event){
				log.info(" connectionNode mouseEntered");
					
			}
			
		});*/
		childrenNode.addChild(connectionNode);
		
		int index = 0;
		String[] children = (String[]) component.getPlaceableChildren();
		for(String child:children ){
			//log.info("child for shape" + child);
			PNode childNode = ShapeFactory.getShape(component.getDisplaySetting("childshape"));
			childNode.scale((double)30);

			double offsetValue = (Double)component.getPropertyValue(child + "_offset");
			if(offsetValue<0){
				childNode.addAttribute(OFFSET_SIGN_ATTRIBUTE_NAME, -1.0);
			}else{
				childNode.addAttribute(OFFSET_SIGN_ATTRIBUTE_NAME, 1.0);
			}

			childNode.addAttribute(NAME_ATTRIBUTE_NAME, child);
			childNode.addAttribute(INDEX_ATTRIBUTE_NAME, index);
			childNode.addAttribute(ROLE_ATTRIBUTE_NAME, "chidNode");
			index++;
			
			PText textNode = new PText();
			textNode.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 1));
			textNode.setTextPaint(Color.black);
			textNode.setText(child);
			textNode.centerBoundsOnPoint(childNode.getBounds().width/2, childNode.getBounds().height + (double)1.0);
			textNode.scale((double)0.5);
			childNode.addChild(textNode);
				
			
			childNode.addInputEventListener(new PDragSequenceEventHandler(){
				public void drag(PInputEvent e){
					log.info("drag");
					super.drag(e);
					
					offsetChild(e);
					
				}
				
				public void endDrag(PInputEvent e){
					log.info("endDrag");
					super.endDrag(e);
				}
				
				public void mouseDragged(PInputEvent e){
					super.mouseDragged(e);
				}
				
				public void mousePressed(PInputEvent e){
					super.mousePressed(e);
					log.info("mousePressed");
				}
				
				public void mouseReleased(PInputEvent e){
					super.mouseReleased(e);
					log.info("mouseReleased");
				}
				
				public void startDrag(PInputEvent e){
					super.startDrag(e);
					log.info("startDrag");
				}
				
			});
			childrenList.put(child, childNode);
			childrenNode.addChild(childNode);
		}
		
		this.addChild(childrenNode);
		positionChildrenNode();
		
		this.setChildrenPickable(true);
		//this.setPickable(true);
	}
	
	void disconnect() {
		parentNode.disconnect();
		component.removePropertyChangeListener(this);
		component = null;
		displayProperties.removePropertyChangeListener( this );
	}

	public PNode getShape() {
		return parentNode;
	}

	public void propertyChanged(Object who, String prop, Object oldVal, Object newVal) {
		//super.propertyChanged(who, prop, oldVal, newVal);
		ArrayList<String> offsetList = new ArrayList<String>();
		for(String childName: childrenList.keySet() ){
			offsetList.add(childName + "_offset");
		}
		
		if(prop.equals("x") || prop.equals("y") ){
			childrenNode.setOffset((Double)component.getPropertyValue("x"), (Double)component.getPropertyValue("y"));
			
		}else if(prop.equals("rotation")){
			parentNode.rot = Math.toRadians(-(Double)newVal);
			AffineTransform tx = AffineTransform.getRotateInstance(parentNode.rot);
			tx.scale(parentNode.depth, parentNode.width);
			parentNode.orientNode.setTransform(tx);
			log.info("call positionChilderneNode");
			positionChildrenNode();
		}else if(offsetList.contains(prop)){
			String[] tempStrArray = prop.split("_");
			offsetChild(tempStrArray[0]);
		}else if(childrenList.keySet().contains(prop)){
			renameAllChildren();
		}
	}
	
	public PNode[] getChildrenNodes(){
		return childrenList.values().toArray(new PNode[childrenList.size()]);
	}
	
	void positionChildrenNode(){
		coordList.clear();
		//log.info("root coordinate: " + this.getX() + " " + this.getY());
		//log.info("root coordinate: " + this.getXOffset() + " " + this.getYOffset());
		
		
		for(String childName: childrenList.keySet()){
			double offsetValue = (Double)component.getPropertyValue(childName + "_offset");
			//log.info("offset child:" + childName + ":" +  offsetValue);
			Point2D coord = getCoordinate(offsetValue);
			
			PNode childNode = childrenList.get(childName);
			//log.info(coord.getX() + " , " + coord.getY());
			childNode.setOffset(coord.getX(), coord.getY());
			
			//log.info("child origin position:" + childNode.getGlobalFullBounds().getCenterX() + " , " + childNode.getGlobalFullBounds().getCenterY());
			coordList.put((Integer) childNode.getAttribute(INDEX_ATTRIBUTE_NAME), coord);
		}
		//connectionNode.setPathTo(new Line2D.Float(coordList.first(), coordList.last()))
		//make the connection path a poly line with all the child nodes included
		connectionNode.setPathToPolyline( coordList.values().toArray(new Point2D[coordList.size()]) );
		
	}

	void renameAllChildren(){
		for(String childName: childrenList.keySet()){
			String newName = component.getPropertyValue(childName + "_name").toString();
			PNode childNode = childrenList.get(childName);
			((PText)((PNode)childNode).getChild(1)).setText(newName);
		}
	}

	Point2D getCoordinate(double offsetValue){
		double rotation = Math.toRadians( -(Double)component.getPropertyValue("rotation") );
		//double x = (Double)component.getPropertyValue("x") - (offsetValue * Math.sin(rotation));
		//double y = (Double)component.getPropertyValue("y") + (offsetValue * Math.cos(rotation));
		double x =  - (offsetValue * Math.sin(rotation));
		double y =  + (offsetValue * Math.cos(rotation));
		Point2D pt = new Point2D.Double(x, y);
		return pt;
	}

	
	void setEffects(boolean e) {}
	
	public boolean setSelected(boolean s) {
		//return selected = s;
		return parentNode.setSelected(s);
	}

	private void offsetChild(PInputEvent e){
		PNode node = e.getPickedNode();
		
		while(node.getAttribute(ROLE_ATTRIBUTE_NAME)==null){
			node = node.getParent();
		}

		double offsetValue = getChildOffsetValue(e, node);
		component.setPropertyValue(node.getAttribute(NAME_ATTRIBUTE_NAME) + "_offset", offsetValue);
	}
	
	private void offsetChild(String childName){
		double offsetValue = (Double)component.getPropertyValue(childName + "_offset");
		
		//log.info("offset child:" + childName + ":" +  offsetValue);
		Point2D coord = getCoordinate(offsetValue);
		
		PNode childNode = childrenList.get(childName);
		//log.info(coord.getX() + " , " + coord.getY());
		childNode.setOffset(coord.getX(), coord.getY());
		coordList.put((Integer) childNode.getAttribute(INDEX_ATTRIBUTE_NAME), coord);
		connectionNode.setPathToPolyline( coordList.values().toArray(new Point2D[coordList.size()]) );
	}
	
	private double getChildOffsetValue(PInputEvent e, PNode node){
		double value = 0.0;
		
		Point2D startP = new Point2D.Double(parentNode.getXOffset() + coordList.get(0).getX(), parentNode.getYOffset()+coordList.get(0).getY());
		//log.info("start x:" + startP.getX() + " y:" + startP.getY());
		
		Point2D endP = new Point2D.Double(parentNode.getXOffset()+ coordList.get(coordList.size()-1).getX(), parentNode.getYOffset() + coordList.get(coordList.size()-1).getY()); 
		//log.info("end x:" + endP.getX() + " y:" + endP.getY());
		
		Line2D line = new Line2D.Double(startP, endP);
		
		Point2D p = e.getPosition();
		//log.info("dragged x:" + p.getX() + " y:" + p.getY());
		
		Point2D centerP = parentNode.getOffset();
		//log.info("center x:" + centerP.getX() + " y:" + centerP.getY());
		
		double d1 = line.ptLineDist(p);
		double d2 = centerP.distance(p);
		
		//log.info("d1:" + d1 + " d2:" + d2);
		value = Math.sqrt(d2*d2 - d1*d1) * (Double)node.getAttribute(OFFSET_SIGN_ATTRIBUTE_NAME);
		
		//log.info("value:" + value);
		return value;
	}
}

