package DeploymentLab.SceneGraph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import DeploymentLab.DisplayProperties;
import DeploymentLab.Model.DLComponent;
import DeploymentLab.channellogger.Logger;

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.nodes.PPath;
import edu.umd.cs.piccolo.util.PBounds;
import groovy.lang.Closure
import java.awt.geom.RoundRectangle2D
import DeploymentLab.CentralCatalogue;

public class DynamicChildrenNode extends PNode{
	private static final Logger log = Logger.getLogger(DynamicChildrenNode.class.getName());
	
	private DeploymentNode parentNode;
	private HashMap<DLComponent, DynamicChildNode> childrenMap= new HashMap<DLComponent, DynamicChildNode>();
	
	private DLComponent component;
	private DisplayProperties displayProperties;
	
	private final String CHILDREN_PROPERTY_NAME="children";
	private final String CLASS_ATTRIBUTE_NAME = "class";
    private final String CLASS_ATTRIBUTE_VALUE = "DynamicChildrenNode"  ;

	public DynamicChildrenNode(DLComponent c, DisplayProperties p){
		parentNode = new IconNode(c, p);
		component = c;
		displayProperties = p;
		
		component.addPropertyChangeListener(this, new Closure(this){
			public void call(DLComponent who, String prop, Object oldVal, Object newVal) {
				//log.info("call propertyChanged for" + prop );
				propertyChanged(who, prop, oldVal, newVal);
			}
		});
		
		constructNodes();
	}
	
	private void constructNodes(){
		int i = 0;
		for(DLComponent c:component.getChildComponents()){
			DynamicChildNode childNode = new DynamicChildNode(c, displayProperties, parentNode, this, i);
			childrenMap.put(c, childNode);
			i++;
		}

        this.addAttribute(CLASS_ATTRIBUTE_NAME, CLASS_ATTRIBUTE_VALUE);
		this.addChild(parentNode);
	}
	
	public void propertyChanged(Object who, String prop, Object oldVal, Object newVal) {
		log.trace("dynamicChildrenNode propertyChanged who: " + who);
		if(prop.equals("x") || prop.equals("y") ){
			//childrenGroup.setOffset((Double)component.getPropertyValue("x"), (Double)component.getPropertyValue("y"));
			for(DynamicChildNode n:childrenMap.values()){
				n.propertyChanged(n.getComponent(), "parent_move", "", "");
			}
		}else if(prop.equals("name")){
            for(DynamicChildNode n:childrenMap.values()){
				n.propertyChanged(n.getComponent(), "parent_name", oldVal, newVal);
 			}
        }

	}
	
	public void disconnect() {
		parentNode.disconnect();
		component.removePropertyChangeListener(this);
		component = null;
		displayProperties.removePropertyChangeListener( this );
	}

	public PNode getShape() {
		return parentNode
	}

	public DLComponent getComponent() {
		return component;
	}
	
	void setEffects(boolean e) {}
	
	public boolean setSelected(boolean s) {
		//return selected = s;
		return parentNode.setSelected(s);
	}

	void setHovered(boolean h) {
		parentNode.setHovered(h);
	}
	
	public DynamicChildNode addChildComponent(DLComponent c){
		int numberOfChildren = childrenMap.size();
		//component.addChild(c);
		DynamicChildNode childNode = new DynamicChildNode(c, displayProperties, parentNode, this, numberOfChildren);
		childrenMap.put(c, childNode);
		
		return childNode;
	}
	
	public void removeChildComponent(DLComponent c){
		childrenMap.remove(c);
	}
	
	public Collection<DynamicChildNode> getChildrenNodes(){
		return childrenMap.values();
	}
	
	public class DynamicChildNode extends IconNode{
		private PPath connectionPath;
		private DeploymentNode parentNode;
		private int index ;
		private DLComponent childComponent;
		private DynamicChildrenNode parentShapeNode;
		
		public DynamicChildNode(DLComponent c, DisplayProperties p, DeploymentNode parentNode, DynamicChildrenNode parentShapeNode, int index){
			super(c,p);			
			this.parentNode = parentNode;
			this.index = index;
			this.childComponent = c;
			this.parentShapeNode = parentShapeNode;
			
			component.addPropertyChangeListener(this, new Closure(this){
				public void call(DLComponent who, String prop, Object oldVal, Object newVal) {
					//log.info("call propertyChanged for" + prop );
					propertyChanged(who, prop, oldVal, newVal);
				}
			});
			
			setupNodes();
		}
		
		public DeploymentNode getParentNode(){
			return parentNode;
		}
		
		public void disconnect() {
			if(parentNode.getComponent()?.getPropertyValue(CHILDREN_PROPERTY_NAME)!=null){
				String[] childrenArray = parentNode.getComponent().getPropertyValue(CHILDREN_PROPERTY_NAME).toString().split(",");
				
				String children = "";
				for(String child:childrenArray){
					if(!child.equals(childComponent.getPropertyValue("name"))){
						children+=child+ ",";
					}
				}
				if(children.length()>0)
					children = children.substring(0, children.length()-1);
				
				parentNode.getComponent().setPropertyValue(CHILDREN_PROPERTY_NAME, children);
				
			}
			
			childComponent.removePropertyChangeListener(this);
			displayProperties.removePropertyChangeListener( this );
			parentShapeNode.removeChildComponent(childComponent);
		}

		protected void setupNodes(){
            // get current component coordinate
            // if x = -1 and y = -1, it means new dynamic child component is added to parent without real x,y coordinate
            double x = component.getPropertyValue("x");
            double y = component.getPropertyValue("y");

            // get parent component coordinate
            double parent_x = (Double)parentNode.getComponent().getPropertyValue("x");
            double parent_y = (Double)parentNode.getComponent().getPropertyValue("y");

            // get real coordinate
            // if this dynamic child is just added to parent,
            // x, y coordinate will be calculated based on existing other children component's position
            double child_x, child_y;
            //log.info("child x:" + x + " y:" + y);
            //log.info("parent bound:" + parentBound.toString());
            // if this dynamic child is added to parent without actual x,y coordinate,
            // need to calculate position
            if(x<0 && y<0){
                double max_child_y = parent_y - 50;

                for(DynamicChildNode child:childrenMap.values()){
                    DLComponent c = child.getComponent();
                    double cur_y = (Double)c.getPropertyValue("y");
                    if(cur_y>max_child_y)
                        max_child_y = cur_y;
                }

                child_x = parent_x+ 25;
                child_y = max_child_y + this.getFullBounds().getHeight();

            }else{
                // this case, dynamic child already has actual x,y coordinate
                child_x = x;
                child_y = y;
            }

            // start to draw connection line and set x,y property after calculating actual position

			connectionPath = PPath.createLine(0.0f, 0.0f, (float)(parent_x - child_x), (float)(parent_y - child_y));
			connectionPath.setStroke(new BasicStroke(1.0f));

			def colorCode = CentralCatalogue.getInstance().getAssociationProperties().getProperty("associations.color.dynamicchild") ?: "000000"
			def associationColor = new Color(Integer.parseInt(colorCode, 16))

			connectionPath.setStrokePaint(associationColor);
			connectionPath.setPaint(associationColor);

			this.addChild(connectionPath);
			connectionPath.moveToBack()

			component.setPropertyValue("x", child_x);
			component.setPropertyValue("y", child_y);
		}

		public void propertyChanged(Object who, String prop, Object oldVal, Object newVal) {
			super.propertyChanged(who, prop, oldVal, newVal);
			//log.trace("DynamicChildNode propertyChanged:" + prop);
			if(prop.equals("parent_move") ){
				reconnectPath((Double)component.getPropertyValue("x"),(Double)component.getPropertyValue("y"));
			}else if(prop.equals("x") || prop.equals("y")){
				//log.info("user tries to move childnode");
				reconnectPath((Double)component.getPropertyValue("x"),(Double)component.getPropertyValue("y"));
			}
		}
		
		private void reconnectPath(double child_x, double child_y){
			double parent_x = (Double)parentNode.getComponent().getPropertyValue("x");
			double parent_y = (Double)parentNode.getComponent().getPropertyValue("y");
			
			ArrayList<Point2D> array = new ArrayList<Point2D>();
			array.add(new Point2D.Float(0.0f, 0.0f));
			array.add(new Point2D.Float((float)(parent_x - child_x), (float)(parent_y - child_y)));
			
			connectionPath.setPathToPolyline(array.toArray(new Point2D[array.size()]));
		}
	}
}
