package DeploymentLab.SceneGraph

import DeploymentLab.CentralCatalogue
import DeploymentLab.channellogger.Logger
import com.synapsense.util.unitconverter.SystemConverter
import com.synapsense.util.unitconverter.UnitConverter
import java.awt.Component
import java.awt.geom.Point2D
import javax.swing.JOptionPane
import DeploymentLab.Localizer

/**
 * Provides a convenient set of tools related to computing the scale of an image.
 *
 * @author Ken Scoggins
 * @since  Jupiter
 */
class ScalingUtility {

    private static final Logger log = Logger.getLogger(ScalingUtility.name)

    // Local constants
    protected static final String DISTANCE_PROPERTY_STR   = 'default.scale.distance.feet'
    protected static final double DISTANCE_DEFAULT_INCHES = 24

    private Component     uiParent = null
    private UnitConverter distConv = null

    /**
     * Creates a new instance of ScalingUtility.
     *
     * @param dialogParent  Parent component used to anchor popup dialogs.
     * @param converter     Optional unit converter.
     */
    ScalingUtility( Component dialogParent, SystemConverter converter ) {
        this.uiParent  = dialogParent

        // Get the specific converter we need if there is a system converter in place.
        distConv = ( converter ? converter.getConverter('distance') : null )
    }

    /**
     * Calculates a scale vale based on the number of pixels between two points on an image and a user provided
     * real-world distance between the same points. The number of pixels is calculated based on two image
     * points. If possible, the user will be prompted for a distance value. Otherwise, the system default will be used.
     *
     * @param pointA  The first point selected on the image.
     * @param pointB  The second point selected on the image.
     *
     * @return  The computed scale, in inches per pixel, or 0 if one could not be computed for any reason.
     */
    double computeScale( Point2D pointA, Point2D pointB ) {

        double scale = 0

        if( !pointA || !pointB ) {
            log.error('Internal Error: Received a null point in computeScale')
            return scale
        }

        // Calculate the number of pixels between the selected points. Abort if the distance is 0 pixels.
        double pixels = Math.sqrt((double)(((pointA.getX() - pointB.getX())**2) + ((pointA.getY() - pointB.getY())**2)))

        if( pixels == 0 ) {
            log.error( CentralCatalogue.getUIS('scaler.zeroPixels'), 'message')
            log.error( CentralCatalogue.getUIS('scaler.aborted'), 'statusbar')
        } else {
            // Get the user supplied scale distance.
            double distance = getScaleDistance( getDefaultScaleDistance() )

            if( distance > 0 ) {
                double tmpScale = computeScale( pixels, distance )

                if( tmpScale > 0 ) {
                    // Looks good, so save the value to be returned.
                    scale = tmpScale

                    // A little mess so our final message is in the proper units for the user.
                    String units1  = null
                    String units2  = null
                    double tmpDist = distance

                    if( distConv  ) {
                        units1   = distConv.getTargetSystem().getDimension('distance').getUnits()
                        units2   = units1
                        tmpScale = distConv.convert( scale )
                        tmpDist  = distConv.convert( distance )
                    } else {
                        units1  = CentralCatalogue.getUIS('scaler.feet')
                        units2  = CentralCatalogue.INSTANCE.getDefaultUnitSystem().getDimension('distance').getUnits()
                        tmpDist = ( distance / 12.0 )
                    }

                    log.info( String.format( CentralCatalogue.getUIS('scaler.success'), tmpDist, units1, pixels, tmpScale, units2 ),
                              'statusbar' )
                }
            }
        }

        return scale
    }

    /**
     * Calculates a scale value based on the number of pixels between two points on an image and a user provided
     * real-world distance between the same points.
     *
     * @param numPixels  Number of pixels between two points on an image.
     * @param distance   Real world distance between two points on an image, in inches.
     *
     * @return  The computed scale, in inches per pixel, or 0 if one could not be computed for any reason.
     */
    double computeScale( double numPixels, double distance ) {
        if( ( numPixels > 0 ) && ( distance > 0 ) ) {
            return ( distance / numPixels )
        }
        return 0
    }

    /**
     * Returns the system default distance between points used when setting the background image scale.
     *
     * @return  The default scale distance, in inches.
     */
    double getDefaultScaleDistance() {

        double distance = DISTANCE_DEFAULT_INCHES

        // Get the value from the app properties.
        String propStr = CentralCatalogue.getApp( DISTANCE_PROPERTY_STR )

        if( propStr ) {
            try {
                // The property is in feet and the default internal representation is inches.
                distance = Double.parseDouble( propStr ) * 12.0

            } catch( NumberFormatException e ) {
                log.error( String.format( CentralCatalogue.getUIS('properties.app.badValueWithDefault'),
                                          DISTANCE_PROPERTY_STR, propStr, ( distance / 12.0 ) ) )
            }
        }

        return distance
    }

    /**
     * Displays an input dialog to allow the user to input the distance between two selected points. The dialog will
     * remain until either the cancel button is pressed or a valid value is entered.
     *
     * @param defaultDistance  The default scale distance to display in the user prompt, in inches.
     *
     * @return  The user supplied scale distance, in inches, or 0 if the action was canceled or there was an error.
     */
    double getScaleDistance( double defaultDistance ) {

        double distance = 0

        // Prompt the user for the true scale distance. Prepopulate with the default.
        while( distance <= 0 ) {
            // Convert the default value to the proper display units. Imperial US will show feet, not inches.
            double dispVal
            String dispUnits
            if( distConv ) {
                dispVal   = distConv.convert( defaultDistance )
                dispUnits = distConv.getTargetSystem().getDimension('distance').getLongUnits()
            } else {
                dispVal   = ( defaultDistance / 12.0 )
                dispUnits = CentralCatalogue.getUIS('scaler.feet')
            }

            // Allow uiParent to be null for unit testing of the class.
            String distStr = Localizer.format( dispVal )
            if( uiParent ) {
                distStr = JOptionPane.showInputDialog( uiParent,
                                                       String.format( CentralCatalogue.getUIS('scaler.prompt'), dispUnits ),
                                                       distStr )
            }
            
            if( distStr == null ) {
                // Cancel button, so just bail out.
                log.info( CentralCatalogue.getUIS('scaler.canceled'), 'statusbar' )
                break
            } else {
                try {
                    distance = Localizer.parseDouble( distStr )

                    // Convert the entered value back to inches.
                    if( distConv ) {
                        // Need a reverse converter to get back to inches & there doesn't seem to be a convertFrom().
                        UnitConverter reverseDistConv =
                            CentralCatalogue.INSTANCE.getUnitSystems().getUnitConverter( distConv.getTargetSystem().getName(),
                                                                                         distConv.getBaseSystem().getName(),
                                                                                         distConv.getDimensionName() )
                        distance = reverseDistConv.convert( distance )
                    } else {
                        distance *= 12.0
                    }
                } catch( NumberFormatException e ) {
                    log.error( CentralCatalogue.getUIS('scaler.invalidValue'), 'message' )
                    distance = 0
                }
            }
        }

        return distance
    }
}
