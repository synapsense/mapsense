package DeploymentLab.SceneGraph;

import DeploymentLab.CentralCatalogue;
import DeploymentLab.ComponentLibrary;
import DeploymentLab.Zip;
import DeploymentLab.channellogger.Logger;
import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.nodes.PPath;
import edu.umd.cs.piccolo.nodes.PText;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.*;
import java.util.List;

public class SvgFactory {
	private static final Logger log = Logger.getLogger(SvgFactory.class.getName());
	static ClassLoader cl = SvgFactory.class.getClassLoader();


	@SuppressWarnings("finally")
	static PNode parseSvg(String svgName){
		PNode rootNode = new PNode();
		PPath defaultShape = PPath.createRectangle(0, 0, 1, 1);
		InputStream svgInputStream = null;
		try {
			log.trace("Reading SVG graphic file '" + svgName + "'", "default");
			PNode svgShape = new PNode();
			List<PNode> shapeList = new ArrayList<PNode>();



			ComponentLibrary clib = ComponentLibrary.INSTANCE;
/*			if ( clib.hasOverloadedShape(svgName)){
				File libFile = clib.getOverloadedShapeFile(svgName);
				svgInputStream = Zip.getStreamFromZip(libFile,svgName + ".svg");


			} else {
*/

			svgInputStream = clib.getStreamFromLibraryFile( svgName + ".svg" );

			if(svgInputStream == null) {
				String shapeDirPath =  CentralCatalogue.getApp("shape.path");
				String shapePath = shapeDirPath + svgName + ".svg";
				svgInputStream = cl.getResourceAsStream(shapePath);
			}

			if(svgInputStream == null) {
				String shapeDirPath =  "shapes/";
				String shapePath = shapeDirPath + svgName + ".svg";
				log.info("attempting to get shape from: " + shapePath);
				log.info("Working Directory = " + System.getProperty("user.dir"));
				//svgInputStream = cl.getResourceAsStream(shapePath);
				File svgf = new File(shapePath);
				log.info("full path is " + svgf.getAbsolutePath());
				if(svgf.exists()){
					svgInputStream = new FileInputStream(svgf);
				}

			}

			if(svgInputStream == null) {
				//log.error("SVG image file " + shapePath + " could not be found!", "default");
				log.error("SVG image file " + svgName + " could not be found!", "default");
				return null;
			}

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document dom = db.parse(svgInputStream);
			Element docEle = dom.getDocumentElement();
			docEle.normalize();

			NodeList elementList = docEle.getChildNodes();

			for(int i=0; i<elementList.getLength();i++){
				if(elementList.item(i).getNodeType()==1){
					Element e = (Element) (elementList.item(i));
					String shapeType = ((Element)e).getTagName();

					if(shapeType.equals("g")){
						recursiveGroup((Element)e, shapeList);
					}else if(shapeType.equals("text")){
						drawText((Element)e, shapeList );
					}else{
						drawShape((Element)e, shapeList);
					}
				}
			}
			// add all shapeList into root
			for(int i=0;i < shapeList.size();i++){
				svgShape.addChild(shapeList.get(i));
			}

			// Scale to a normalized size as a starting point.
			AffineTransform af = new AffineTransform();
			double scaleFactor = 1.0 / svgShape.getFullBounds().height;
			af.scale(scaleFactor, scaleFactor);
			af.translate(-svgShape.getFullBounds().getCenterX(), -svgShape.getFullBounds().getCenterY());
			svgShape.setTransform(af);
			rootNode.addChild(svgShape);
			rootNode.setName( svgName );

			if ( svgInputStream != null ){
				svgInputStream.close();
			}

		} catch (ParserConfigurationException pce) {
			log.error(pce.getMessage(), "default");
			rootNode.addChild(defaultShape);
		} catch (SAXException se) {
			log.error(se.getMessage(), "default");
			rootNode.addChild(defaultShape);
		} catch (IOException ioe) {
			log.error(ioe.getMessage(), "default");
			rootNode.addChild(defaultShape);
		}

		return rootNode;
	}

	private static void recursiveGroup(Element el, List<PNode> list) {
		NodeList elementList = el.getChildNodes();

		for(int i=0; i<elementList.getLength();i++){
			if(elementList.item(i).getNodeType()==1){
				Element child = (Element) (elementList.item(i));
				Element childElement = (Element)child;
				PNode shape = null;

				// inherits attributes from parent
				NamedNodeMap groupAtts = el.getAttributes();

				for (int j = 0; j < groupAtts.getLength(); j++) {
					if (!childElement.hasAttribute(groupAtts.item(j).getNodeName())) {
						childElement.setAttribute(groupAtts.item(j).getNodeName(),groupAtts.item(j).getNodeValue());
					}
				}

				if(childElement.getTagName().equals("g")){
					recursiveGroup(childElement, list);
				}else if(childElement.getTagName().equals("text")){
					drawText(childElement, list);
				}else{
					drawShape(childElement, list);
				}
			}
		}
	}

	private static void drawShape(Element el, List<PNode> list) {
		PNode shape = null;
		String shapeType = el.getTagName();

		HashMap<String, String> shapePropertiesMap = new HashMap<String, String>(); // shape's properties map
		Style style = new Style(); // style class
		Point2D[] points = null; // point array for polyline or polygon
		ArrayList<pathDimension> pathDArr = null; // hashmap for path which
													// has information about command and points
		HashMap<String, float[]> transformList = null; // transform matrix
		shapePropertiesMap.put("value", el.getTextContent()); // node value
		NamedNodeMap nodeMap = el.getAttributes(); // attributes map for shape

		// 1. set attributes for shape
		for (int k = 0; k < nodeMap.getLength(); k++) {

			String attrName = nodeMap.item(k).getNodeName();
			String attrValue = nodeMap.item(k).getNodeValue();

			// sometimes style attribute has information about stroke, fill and  color
			// sometimes no style attribute. instead of style, fill, stroke and  color attribute exist
			if (attrName.equals("style")) {
				style.setStyle(el.getAttribute("style"));
			} else if (attrName.equals("fill")) {
				style.setFillColor(attrValue);
			} else if (attrName.equals("stroke")) {
				style.setStrokeColor(attrValue);
			} else if (attrName.equals("points")) { // polyline has points
				String pointsStr = el.getAttribute("points");
				points = getPoints(pointsStr);
			} else if (attrName.equals("d")) { // path has d attribute for path data
				pathDArr = new ArrayList<pathDimension>();
				String dStr = el.getAttribute("d");

				if(!dStr.equals("")){
					StringTokenizer dArr = new StringTokenizer(dStr,
							"mMzZlLhHvVcCsSqQtTaA", true);
					while (dArr.hasMoreTokens()) {
						String cmdStr = dArr.nextToken();
						Point2D.Float[] pt = null;

						// elliptical arc is exceptional case for path parsing
						if (cmdStr.equals("A") || cmdStr.equals("a")) {

							if (dArr.hasMoreTokens()) {
								String ptStr = dArr.nextToken();

								if (el.hasAttribute("sodipodi:type") && el.getAttribute("sodipodi:type").equals("arc")) {
									ptStr += " " + el.getAttribute("sodipodi:cx") + "," + el.getAttribute("sodipodi:cy");
									if(el.hasAttribute("sodipodi:start") && el.hasAttribute("sodipodi:end")){
										ptStr += " " + el.getAttribute("sodipodi:start")+ "," + el.getAttribute("sodipodi:end");
									}
								}
								pt = getArcPoints(ptStr);
							}

						} else {
							if (dArr.hasMoreTokens()) {
								String ptStr = dArr.nextToken();
								pt = getPoints(ptStr);
							}
						}
						pathDArr.add(new pathDimension(cmdStr.charAt(0), pt));
					}
				}
			} else if (attrName.equals("transform")) { // get transform information
				transformList = getTransformList(el);
			} else {
				shapePropertiesMap.put(attrName, attrValue);
			}
		}

		// 2. draw each shape
		if (shapeType.equals("rect")) {
			if (shapePropertiesMap.containsKey("rx")) {
				shape = PPath.createRoundRectangle(Float
						.parseFloat(shapePropertiesMap.get("x")), Float
						.parseFloat(shapePropertiesMap.get("y")), Float
						.parseFloat(shapePropertiesMap.get("width")), Float
						.parseFloat(shapePropertiesMap.get("height")), Float
						.parseFloat(shapePropertiesMap.get("rx")) * 2, Float
						.parseFloat(shapePropertiesMap.get("ry")) * 2);
			} else {
				shape = PPath.createRectangle(Float
						.parseFloat(shapePropertiesMap.get("x")), Float
						.parseFloat(shapePropertiesMap.get("y")), Float
						.parseFloat(shapePropertiesMap.get("width")), Float
						.parseFloat(shapePropertiesMap.get("height")));
			}
		} else if (shapeType.equals("circle")) {
			float x = Float.parseFloat(shapePropertiesMap.get("cx"))
					- Float.parseFloat(shapePropertiesMap.get("r"));
			float y = Float.parseFloat(shapePropertiesMap.get("cy"))
					- Float.parseFloat(shapePropertiesMap.get("r"));

			shape = PPath.createEllipse(x, y, 2 * Float
					.parseFloat(shapePropertiesMap.get("r")), 2 * Float
					.parseFloat(shapePropertiesMap.get("r")));

		} else if (shapeType.equals("ellipse")) {
			float x = Float.parseFloat(shapePropertiesMap.get("cx"))
					- Float.parseFloat(shapePropertiesMap.get("rx"));
			float y = Float.parseFloat(shapePropertiesMap.get("cy"))
					- Float.parseFloat(shapePropertiesMap.get("ry"));

			shape = PPath.createEllipse(x, y, 2 * Float
					.parseFloat(shapePropertiesMap.get("rx")), 2 * Float
					.parseFloat(shapePropertiesMap.get("ry")));
		} else if (shapeType.equals("line")) {
			shape = PPath.createLine(Float.parseFloat(shapePropertiesMap
					.get("x1")),
					Float.parseFloat(shapePropertiesMap.get("y1")), Float
							.parseFloat(shapePropertiesMap.get("x2")), Float
							.parseFloat(shapePropertiesMap.get("y2")));
		} else if (shapeType.equals("polyline")) {
			shape = PPath.createPolyline(points);
		} else if (shapeType.equals("polygon")) {
			shape = PPath.createPolyline(points);
			((PPath) shape).closePath();
		} else if (shapeType.equals("path")) {

			shape = drawPath(pathDArr);

		} else if (shapeType.equals("tspan")) {
			shape = new PText();
			((PText) shape).setText(shapePropertiesMap.get("value"));
			((PText) shape).setX(Double
					.parseDouble(shapePropertiesMap.get("x")));
			((PText) shape).setY(Double
					.parseDouble(shapePropertiesMap.get("y"))- style.getFontSize());

		} else {
			shape = new PPath();
		}

		// 3. apply style
		style.applyStyle(shape);

		// 4. do transform
		if (transformList != null) {
			for (Map.Entry<String, float[]> me : transformList.entrySet()) {
				AffineTransform aft = new AffineTransform(me.getValue());
				shape.setTransform(aft);
			}
		}

		if(!shape.getBounds().isEmpty()){
			list.add(shape);
		}
	}

	private static void drawText(Element el, List<PNode> list) {
		NamedNodeMap txtAtts = el.getAttributes();
		NodeList tspanList = el.getElementsByTagName("tspan");

		for (int j = 0; j < tspanList.getLength(); j++) {
			// inhert all properties to tspan element from text element
			for (int k = 0; k < txtAtts.getLength(); k++) {
				if(!((Element)tspanList.item(j)).hasAttribute(txtAtts.item(k).getNodeName())){
					((Element) tspanList.item(j)).setAttribute(txtAtts.item(k)
							.getNodeName(), txtAtts.item(k).getNodeValue());
				}
			}
			drawShape((Element) tspanList.item(j), list);
		}
	}

	private static PPath drawPath(ArrayList<pathDimension> pathDArr) {
		PPath shape = new PPath();

		for (int j = 0; j < pathDArr.size(); j++) {
			char pathCommand = pathDArr.get(j).getCommand();

			Point2D.Float[] pathPoints = pathDArr.get(j).getPoints();
			Point2D.Float[] prevPoints = null;
			Point2D.Float currentPoint = null;
			Point2D.Float prevCtrPoint = null;
			Point2D.Float currentCtrPoint = null;

			if (j > 0) {
				prevPoints = pathDArr.get(j - 1).getPoints();
			}

			if(pathCommand=='Z' || pathCommand=='z'){
				//	close path
				if(!shape.getFullBounds().isEmpty()){
					shape.closePath();
				}
			}else{
				if( pathPoints!=null && pathPoints.length>0){
					switch (pathCommand) {
					// move to
					case 'M':
					case 'm':
						shape.moveTo((float)pathPoints[0].x, (float)pathPoints[0].y);
						break;
					// line to
					case 'L':
					case 'l':
						shape.lineTo((float)pathPoints[0].x, (float)pathPoints[0].y);
						break;
					// Bezier curve
					case 'C':
						shape.curveTo((float)pathPoints[0].x, (float)pathPoints[0].y,
								(float)pathPoints[1].x, (float)pathPoints[1].y, (float)pathPoints[2].x,
								(float)pathPoints[2].y);
						break;
					case 'c':

						Point2D.Float basePoint = prevPoints[prevPoints.length - 1];

						pathPoints[0].setLocation((float)pathPoints[0].x + (float)basePoint.x,
								(float)pathPoints[0].y + (float)basePoint.y);
						pathPoints[1].setLocation((float)pathPoints[1].x + (float)basePoint.x,
								(float)pathPoints[1].y + (float)basePoint.y);
						pathPoints[2].setLocation((float)pathPoints[2].x + (float)basePoint.x,
								(float)pathPoints[2].y + (float)basePoint.y);

						shape.curveTo((float)pathPoints[0].x, (float)pathPoints[0].y,
								(float)pathPoints[1].x, (float)pathPoints[1].y, (float)pathPoints[2].x,
								(float)pathPoints[2].y);

						break;
					// short hand/smooth curve
					case 'S':

						currentPoint = prevPoints[2];
						prevCtrPoint = prevPoints[1];
						// first control point = the reflection of the second control
						// point on the previous command relative to the current point
						currentCtrPoint = new Point2D.Float();
						currentCtrPoint.x = 2 * currentPoint.x - prevCtrPoint.x;
						currentCtrPoint.y = 2 * currentPoint.y - prevCtrPoint.y;

						shape.curveTo(currentCtrPoint.x, currentCtrPoint.y,
								(float)pathPoints[0].x, (float)pathPoints[0].y, (float)pathPoints[1].x,
								(float)pathPoints[1].y);

						break;
					case 's':
						break;
					// Quadratic Bezier curve
					case 'Q':
						shape.quadTo((float)pathPoints[0].x, (float)pathPoints[0].y, (float)pathPoints[1].x,
								(float)pathPoints[1].y);
							break;
					case 'q':
						break;
					// shorthand/smooth quadratic Bezier curve
					case 'T':
						currentPoint = prevPoints[1];
						prevCtrPoint = prevPoints[0];
						// first control point = the reflection of the second control
						// point on the previous command relative to the current point
						currentCtrPoint = new Point2D.Float();
						currentCtrPoint.x = 2 * currentPoint.x - prevCtrPoint.x;
						currentCtrPoint.y = 2 * currentPoint.y - prevCtrPoint.y;

						shape.quadTo((float)currentCtrPoint.x, (float)currentCtrPoint.y,
								(float)pathPoints[0].x, (float)pathPoints[0].y);
						break;
					case 't':
						break;
					// draw elliptical arc
					case 'A':
						Point2D.Float startPoint = prevPoints[prevPoints.length - 1];

						float x1= startPoint.x,
						x2 = pathPoints[4].x,
						y1 = startPoint.y,
						y2 = pathPoints[4].y,
						fa = pathPoints[2].x,
						fs = pathPoints[3].x,
						rx = pathPoints[0].x,
						ry = pathPoints[0].y;

						// central point of ellipse
						float cx = pathPoints[5].x;
						float cy = pathPoints[5].y;

						float angleSt,
						angleExt;
						// java geom class has different direction for angle from svg coordinate
						if(pathPoints.length>6){
							angleSt = (float) (pathPoints[6].x * 180 / Math.PI) * (-1);
							angleExt = (float) (pathPoints[6].y * 180 / Math.PI - pathPoints[6].x * 180 / Math.PI) * (-1);
						}else{
							angleSt =0.0f;
							angleExt = 360.0f;
						}

						Arc2D.Float arcShape = new Arc2D.Float();
						arcShape.setArc(pathPoints[5].x - rx, pathPoints[5].y - ry,
								2 * rx, 2 * ry, angleSt, angleExt, Arc2D.OPEN);
						shape.setPathTo(arcShape);
						break;
					}
				}
			}
		}
		return shape;
	}

	private static HashMap getTransformList(Element el) {
		HashMap<String, float[]> transformList = new HashMap<String, float[]>();

		String transformStr = el.getAttribute("transform");
		String[] transformGroupArr = transformStr.split(" ");

		for (int i = 0; i < transformGroupArr.length; i++) {

			StringTokenizer transformArr = new StringTokenizer(
					transformGroupArr[i], "()", true);

			while (transformArr.hasMoreTokens()) {
				String cmdStr = transformArr.nextToken();
				String ptStr = "";

				if (transformArr.hasMoreTokens()) {
					ptStr = transformArr.nextToken();
					ptStr = transformArr.nextToken();
					String[] pointArr = ptStr.split(",");
					float[] transformMat = new float[6];

					if (cmdStr.equals("matrix")) {
						transformMat[0] = Float.parseFloat(pointArr[0]);
						transformMat[1] = Float.parseFloat(pointArr[1]);
						transformMat[2] = Float.parseFloat(pointArr[2]);
						transformMat[3] = Float.parseFloat(pointArr[3]);
						transformMat[4] = Float.parseFloat(pointArr[4]);
						transformMat[5] = Float.parseFloat(pointArr[5]);
					} else if (cmdStr.equals("translate")) {
						transformMat[0] = 1.0f;
						transformMat[1] = 0.0f;
						transformMat[2] = 0.0f;
						transformMat[3] = 1.0f;
						transformMat[4] = Float.parseFloat(pointArr[0]);
						transformMat[5] = Float.parseFloat(pointArr[1]);
					} else if (cmdStr.equals("scale")) {
						transformMat[0] = Float.parseFloat(pointArr[0]);
						transformMat[1] = 0.0f;
						transformMat[2] = 0.0f;
						transformMat[3] = Float.parseFloat(pointArr[1]);
						transformMat[4] = 0.0f;
						transformMat[5] = 0.0f;
					} else if (cmdStr.equals("rotate")) {
						float a = (float) (Float.parseFloat(pointArr[0])
								* Math.PI / 180);

						transformMat[0] = (float) Math.cos(a);
						transformMat[1] = (float) Math.sin(a);
						transformMat[2] = -(float) Math.cos(a);
						transformMat[3] = (float) Math.cos(a);
						transformMat[4] = 0.0f;
						transformMat[5] = 0.0f;
					} else if (cmdStr.equals("skewX")) {
						float a = (float) (Float.parseFloat(pointArr[0])
								* Math.PI / 180);

						transformMat[0] = 1.0f;
						transformMat[1] = (float) Math.tan(a);
						transformMat[2] = 0.0f;
						transformMat[3] = 1.0f;
						transformMat[4] = 0.0f;
						transformMat[5] = 0.0f;
					} else if (cmdStr.equals("skewY")) {
						float a = (float) (Float.parseFloat(pointArr[0])
								* Math.PI / 180);

						transformMat[0] = 1.0f;
						transformMat[1] = (float) Math.tan(a);
						transformMat[2] = 0.0f;
						transformMat[3] = 1.0f;
						transformMat[4] = 0.0f;
						transformMat[5] = 0.0f;
					} else {
						transformMat[0] = 1.0f;
						transformMat[1] = 0.0f;
						transformMat[2] = 0.0f;
						transformMat[3] = 1.0f;
						transformMat[4] = 0.0f;
						transformMat[5] = 0.0f;
					}

					transformList.put(cmdStr, transformMat);
				}// if(transformArr.hasMoreTokens()
			}// while(transformArr.hasMoreTokens())
		}// for(int i=0;i<transformGroupArr.length;i++)

		return transformList;
	}

	private static Point2D.Float[] getPoints(String pointsStr) {
		String[] pointGroup = pointsStr.split(" ");
		ArrayList<Point2D.Float> pointArr = new ArrayList<Point2D.Float>();

		for (int j = 0; j < pointGroup.length; j++) {
			if (pointGroup[j].contains(",")) {
				pointArr.add(new Point2D.Float(Float.parseFloat(pointGroup[j]
						.split(",")[0]), Float.parseFloat(pointGroup[j]
						.split(",")[1])));

			}
		}

		Point2D.Float[] points = new Point2D.Float[pointArr.size()];

		for (int j = 0; j < pointArr.size(); j++) {
			points[j] = pointArr.get(j);
		}
		return points;
	}

	private static Point2D.Float[] getArcPoints(String pointsStr) {
		String[] pointGroup = pointsStr.split(" ");
		ArrayList<Point2D.Float> pointArr = new ArrayList<Point2D.Float>();

		for (int j = 0; j < pointGroup.length; j++) {
			if (!pointGroup[j].equals("")) {
				if (pointGroup[j].contains(",")) {
					pointArr.add(new Point2D.Float(Float
							.parseFloat(pointGroup[j].split(",")[0]), Float
							.parseFloat(pointGroup[j].split(",")[1])));
				} else {
					pointArr.add(new Point2D.Float(Float
							.parseFloat(pointGroup[j]), 0.0f));
				}
			}
		}

		Point2D.Float[] points = new Point2D.Float[pointArr.size()];

		for (int j = 0; j < pointArr.size(); j++) {
			points[j] = pointArr.get(j);
		}

		return points;
	}
}

class pathDimension {
	private static final Logger log = Logger.getLogger(DeploymentLab.SceneGraph.pathDimension.class.getName());
	private char command;
	private Point2D.Float[] points;


	pathDimension(char c, Point2D.Float[] p) {
		command = c;
		if (p != null)
			points = (Point2D.Float[]) p.clone();

	}

	public char getCommand() {
		return command;
	}

	public Point2D.Float[] getPoints() {
		return points;
	}

	public void setPoints(Point2D.Float point) {
		points[points.length] = point;
	}
}

class Style {
	private static final Logger log = Logger.getLogger(DeploymentLab.SceneGraph.Style.class.getName());
	private String styleStr = "";
	// style properties
	private Color fillColor = null;
	private Color strokeColor = null;
	private float fillOpacity = 1.0f;
	private float strokeOpacity = 1.0f;
	private float strokeWidth = 0.0f;
	private Font font = null;
	private String fontName;
	private int fontSize;

	Style(){
	}

	Style(String str) {
		setStyle(str);
	}

	public void setStyle(String str) {
		styleStr = str;

		String[] styleArr = str.split(";");

		if (styleArr.length > 0) {

			for (int j = 0; j < styleArr.length; j++) {
				String[] stylePropertyArr = styleArr[j].split(":");

				if (stylePropertyArr.length > 0) {
					String propertyName = stylePropertyArr[0].trim();
					String propertyValue = stylePropertyArr[1].trim();

					// get style properties
					if (propertyName.equals("fill")) {
						fillColor = getRGB(propertyValue);
					} else if (propertyName.equals("stroke")) {
						strokeColor = getRGB(propertyValue);
					} else if (propertyName.equals("stroke-width")) {
						strokeWidth = Float.parseFloat(propertyValue.replace(
								"px", "").replace("pt", ""));
					} else if (propertyName.equals("stroke-opacity")) {
						strokeOpacity = Float.parseFloat(propertyValue.replace(
								"px", ""));
					} else if (propertyName.equals("fill-opacity")) {
						fillOpacity = Float.parseFloat(propertyValue.replace(
								"px", ""));
					} else if (propertyName.equals("font-family")) {
						fontName = propertyValue;
					} else if (propertyName.equals("font-size")) {
						Double fontSize_f = Double.parseDouble(propertyValue.replace("px",	""));
						fontSize = (int) Math.ceil(fontSize_f);
					}

				}
			}
		}else{
			if(str.length()>0 && str.contains(":")){
				for (int j = 0; j < styleArr.length; j++) {
					String[] stylePropertyArr = styleArr[j].split(":");

					if (stylePropertyArr.length > 0) {
						String propertyName = stylePropertyArr[0].trim();
						String propertyValue = stylePropertyArr[1].trim();

						// get style properties
						if (propertyName.equals("fill")) {
							fillColor = getRGB(propertyValue);
						} else if (propertyName.equals("stroke")) {
							strokeColor = getRGB(propertyValue);
						} else if (propertyName.equals("stroke-width")) {
							strokeWidth = Float.parseFloat(propertyValue.replace(
									"px", "").replace("pt", ""));
						} else if (propertyName.equals("stroke-opacity")) {
							strokeOpacity = Float.parseFloat(propertyValue.replace(
									"px", ""));
						} else if (propertyName.equals("fill-opacity")) {
							fillOpacity = Float.parseFloat(propertyValue.replace(
									"px", ""));
						} else if (propertyName.equals("font-family")) {
							fontName = propertyValue;
						} else if (propertyName.equals("font-size")) {
							Double fontSize_f = Double.parseDouble(propertyValue.replace("px",	""));
							fontSize = (int) Math.ceil(fontSize_f);

						}
					}
				}
			}
		}
	}

	public void setFillColor(Color c) {
		fillColor = c;
	}

	public void setStrokeColor(Color c) {
		strokeColor = c;
	}

	public void setFillColor(String c) {
		fillColor = getRGB(c);
	}

	public void setStrokeColor(String c) {
		strokeColor = getRGB(c);
	}

	public void setFillOpacity(float o) {
		fillOpacity = o;
	}

	public void setStrokeOpacity(float o) {
		strokeOpacity = o;
	}

	public void setStrokeWidth(float w) {
		strokeWidth = w;
	}

	public void setFont(Font f) {
		font = f;
	}

	public void setFontName(String fn) {
		fontName = fn;
	}

	public void setFontSize(int fs) {
		fontSize = fs;
	}

	public Color getFillColor() {
		return fillColor;
	}

	public Color getStrokeColor() {
		return strokeColor;
	}

	public float getFillOpacity() {
		return fillOpacity;
	}

	public float getStrokeOpacity() {
		return strokeOpacity;
	}

	public float getStrokeWidth() {
		return strokeWidth;
	}

	public Font getFont() {
		return font;
	}

	public String getFontName() {
		return fontName;
	}

	public int getFontSize() {
		return fontSize;
	}

	private Color getRGB(String rgbStr) {
		Color rgb = null;
		if (rgbStr.contains("rgb(")) {
			rgbStr = rgbStr.replace("rgb", "");
			rgbStr = rgbStr.replace("(", "");
			rgbStr = rgbStr.replace(")", "");

			String[] rgbArr = rgbStr.split(",");
			if (rgbArr.length > 0) {
				rgb = new Color(Integer.parseInt(rgbArr[0]), Integer
						.parseInt(rgbArr[1]), Integer.parseInt(rgbArr[2]));
			}
		} else if (rgbStr.contains("#")) {
			rgb = new Color(Integer.valueOf(rgbStr.substring(1), 16).intValue());
		} else {
			String colorName = rgbStr.toUpperCase();

			if (colorName.toUpperCase().equals("NONE")) {
				return null;
			}

			Field colorField;
			try {
				colorField = Color.class.getDeclaredField(colorName);

				if (colorField != null) {
					rgb = (Color) colorField.get(null);
				}

			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchFieldException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block

				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return rgb;
	}

	public void applyStyle(PNode shape) {
		if (shape.getClass().getSimpleName().equals("PPath")) {
			if (strokeWidth > 0.0f) {
				((PPath) shape).setStroke(new BasicStroke(strokeWidth));
			}

			if (fillColor == null) {
				shape.setPaint(null);
			} else {
				shape.setPaint(new Color(fillColor.getRed(), fillColor
						.getGreen(), fillColor.getBlue(),
						(int) (fillOpacity * 255)));
			}

			if (strokeColor == null) {
				((PPath) shape).setStrokePaint(null);
			} else {
				((PPath) shape).setStrokePaint(new Color(strokeColor.getRed(),
						strokeColor.getGreen(), strokeColor.getBlue(),
						(int) (strokeOpacity * 255)));
			}

		} else {
			font = new Font(fontName, Font.PLAIN, fontSize);
			((PText) shape).setFont(font);
			if(fillColor==null)
				fillColor = Color.BLACK;
			((PText) shape).setTextPaint(new Color(fillColor.getRed(), fillColor.getGreen(), fillColor.getBlue(),(int) (fillOpacity * 255)));
		}
	}
}