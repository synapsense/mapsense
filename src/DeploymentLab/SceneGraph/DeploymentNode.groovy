package DeploymentLab.SceneGraph

import DeploymentLab.CentralCatalogue
import DeploymentLab.DisplayProperties
import DeploymentLab.Model.DLComponent
import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.event.PInputEvent

import javax.swing.JPopupMenu
import java.awt.Color
import java.awt.Font

abstract class DeploymentNode extends PNode {
	static protected Font nodeFont = new Font(Font.SANS_SERIF, Font.BOLD, 12)
	static protected Font proxyFont = new Font(Font.SANS_SERIF, Font.ITALIC, 12)
	protected Font centerBadgeFont = new Font(Font.SANS_SERIF, Font.PLAIN, 12)

	protected boolean selected
	protected boolean hovered
	private boolean isDragging = false

	protected static final Color NORMAL_TEXT_COLOR    = Color.BLACK
	protected static final Color NORMAL_TEXT_BG_COLOR = new Color( 1.0f, 1.0f, 1.0f, 0.25f )

	protected static final Color HOVER_TEXT_COLOR    = Color.WHITE
	protected static final Color HOVER_TEXT_BG_COLOR = new Color( 0.3f, 0.3f, 0.3f, 1.0f )

	static protected Color selectColor = new Color((float) 1.0, (float) 0.0, (float) 0.0, (float) 0.5)
	static protected Color centerBadgeColor = new Color((float) 0.0, (float) 1.0, (float) 0.0, (float) 1.0)

	protected DLComponent component
	protected DisplayProperties displayProperties

	static final int _indent = 2
	static final int HIGHLIGHT_BORDER = 10


	DeploymentNode(DLComponent c, DisplayProperties p) {
		component = c
		c.addPropertyChangeListener(this, this.&propertyChanged)
		displayProperties = p
		p.addPropertyChangeListener(this, this.&displayPropertyChanged)
		selected = false
		hovered = false
	}


	abstract void disconnect()

	/**
	 * Returns the core PNode that is the actual shape, exluding the name and highlight and other misc. parts.
	 * @return
	 */
	abstract PNode getShape()

	DLComponent getComponent() {
		return component
	}

	boolean getSelected() {
		return selected
	}

	boolean setSelected(boolean s) {
		selected = s
	}

	boolean getHovered() {
		return hovered
	}

	void setHovered(boolean h) {
		hovered = h
	}

	boolean isDragging() {
		return isDragging
	}

	void setDragging( boolean d ) {
		isDragging = d
		if( isDragging ) {
			component.metamorphosisStarting()
		} else {
			component.metamorphosisFinished()
		}
	}

	abstract void propertyChanged(def who, String prop, oldVal, newVal)

	void displayPropertyChanged(def who) {

        boolean isActiveDrawingChild =   displayProperties.isActiveDrawingComponent(component)
        if(!this.getVisible() && !isActiveDrawingChild){
            return;
        }

        boolean isDisplayable = false
        if(isActiveDrawingChild){
            def unselectedTypes = displayProperties.getProperty('invisibleObjects')
            boolean isSelectedType = !unselectedTypes.contains(component.getType())
            isDisplayable = isSelectedType && isActiveDrawingChild
        }
        //log.trace("isActiveDrawingChild:" + isActiveDrawingChild.toString())
        //log.trace("isDisplayable:" + isDisplayable.toString())

        this.setVisible(isDisplayable)
        this.setPickable(isDisplayable)
	}

	abstract void remove(PNode n)

	// Override to add node specific items to the context menu.
	void buildContextMenu( JPopupMenu parentMenu, PInputEvent e ) {}

	// Override so we can set needsASave if the order changes since we persist that now.
	public void moveToBack() {
		int beforeIdx = getParent().indexOfChild( this )

		super.moveToBack()

		if( beforeIdx != getParent().indexOfChild( this ) && !CentralCatalogue.getOpenProject().getComponentModel().isMetamorphosizing() ) {
			CentralCatalogue.getOpenProject().setNeedsASave( true )
		}
	}

	// Override so we can set needsASave if the order changes since we persist that now.
	public void moveInBackOf( final PNode sibling ) {
		int beforeIdx = getParent().indexOfChild( this )

		super.moveInBackOf(sibling)

		if( beforeIdx != getParent().indexOfChild( this ) && !CentralCatalogue.getOpenProject().getComponentModel().isMetamorphosizing() ) {
			CentralCatalogue.getOpenProject().setNeedsASave( true )
		}
	}

	// Override so we can set needsASave if the order changes since we persist that now.
	public void moveToFront() {
		int beforeIdx = getParent().indexOfChild( this )

		super.moveToFront()

		if( beforeIdx != getParent().indexOfChild( this ) && !CentralCatalogue.getOpenProject().getComponentModel().isMetamorphosizing() ) {
			CentralCatalogue.getOpenProject().setNeedsASave( true )
		}
	}

	// Override so we can set needsASave if the order changes since we persist that now.
	public void moveInFrontOf( final PNode sibling ) {
		int beforeIdx = getParent().indexOfChild( this )

		super.moveInFrontOf(sibling)

		if( beforeIdx != getParent().indexOfChild( this ) && !CentralCatalogue.getOpenProject().getComponentModel().isMetamorphosizing() ) {
			CentralCatalogue.getOpenProject().setNeedsASave( true )
		}
	}
}
