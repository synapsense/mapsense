package DeploymentLab.SceneGraph

import java.awt.BasicStroke
import java.awt.Color
import java.awt.RadialGradientPaint
import java.awt.geom.Ellipse2D

import edu.umd.cs.piccolo.*
import edu.umd.cs.piccolo.nodes.*

import DeploymentLab.Model.*
import DeploymentLab.channellogger.Logger;
import DeploymentLab.DisplayProperties

class HaloNode extends PPath {

	private static final Logger log = Logger.getLogger(HaloNode.class.getName());
	
	protected DLComponent component

	protected PPath shapeNode

	protected double radius
	protected double sceneScale = 1.0

	protected dist = [(float)0.0, (float)1.0] as float[]
	protected colors
	
	protected DisplayProperties displayProperties
	
	
	HaloNode(DLComponent c, double r, Color color, DisplayProperties p) {
		component = c
		radius = r

		colors = [new Color(color.red, color.green, color.blue, 128), new Color(color.red, color.green, color.blue, 25)] as Color[]

		component.addPropertyChangeListener(this, this.&componentPropertyChanged)
		
		displayProperties = p
		p.addPropertyChangeListener(this, this.&displayPropertyChanged)
		
		boolean isVisible = true
		if(displayProperties.getProperty('invisibleHalos')!=null){
			isVisible = !displayProperties.getProperty('invisibleHalos').contains(component.getType())
		}
		
		log.trace("isVisible:" + isVisible)
		
		this.setStroke(new BasicStroke(0.1f))
		this.setStrokePaint(Color.black)
		this.setOffset(component.getPropertyValue('x'), component.getPropertyValue('y'))
		this.setPathTo(new Ellipse2D.Double((double)-radius, (double)-radius, (double)2*radius, (double)2*radius))
		this.setPaint(new RadialGradientPaint((float)0, (float)0, (float)radius, dist, colors))
		this.setVisible(isVisible)
        this.setPickable(false)
        this.moveToBack()
	}

	void disconnect() {
		component.removePropertyChangeListener(this)
		component = null
		displayProperties.removePropertyChangeListener( this )
	}

	DLComponent getComponent() {
		return component
	}

	void componentPropertyChanged(who, String prop, oldVal, newVal) {
		if(prop == 'x' || prop == 'y') {
			this.setOffset(component.getPropertyValue('x'), component.getPropertyValue('y'))
		}
	}

	void displayPropertyChanged(def who){
        boolean isActiveDrawingChild =  displayProperties.isActiveDrawingComponent(component)

        if(isActiveDrawingChild){
            def unselectedTypes = displayProperties.getProperty('invisibleObjects')
            boolean isSelectedType = !unselectedTypes.contains(component.getType())
            boolean isDisplayable = isSelectedType && isActiveDrawingChild

            if(isDisplayable){
                // if displayable ( is not unselected type and is a child of active drawing
                def unselectedHalos = displayProperties.getProperty('invisibleHalos')
                boolean isVisibleHalos = !unselectedHalos.contains(component.getType())
//                log.trace("isVisibleHalos:" + isVisibleHalos)
                this.setVisible(isVisibleHalos)
//                log.trace("isSelectedType:" + isSelectedType.toString())
//                log.trace("isActiveDrawingChild:" + isActiveDrawingChild.toString())
//                log.trace("isDisplayable:" + isDisplayable.toString())

            }else{
                this.setVisible(false)
                this.moveToBack()
            }

        }else{
            this.setVisible(false)
			this.moveToBack()
        }


		

		/*
		if(unselectedTypes.contains(component.getType())){
			this.setVisible(false)
            this.moveToBack()
		}else{
			def unselectedHalos = displayProperties.getProperty('invisibleHalos')
			this.setVisible(!unselectedHalos.contains(component.getType()))
		}*/
	
	}
	void setEffects(boolean e) {}

	void remove(PNode n) {}
}

