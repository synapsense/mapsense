package DeploymentLab.SceneGraph

import DeploymentLab.DisplayProperties
import DeploymentLab.Model.ComponentProperty
import DeploymentLab.Model.DLComponent
import DeploymentLab.channellogger.Logger
import edu.umd.cs.piccolo.PNode
import edu.umd.cs.piccolo.nodes.PPath
import edu.umd.cs.piccolo.nodes.PText
import edu.umd.cs.piccolo.util.PBounds

import java.awt.*
import java.awt.geom.Point2D
import java.awt.geom.RoundRectangle2D
import java.util.List

class IconNode extends DeploymentNode {
	private static final Logger log = Logger.getLogger(IconNode.class.getName());

	private static final List<String> cornerBadges = ['nwbadge', 'swbadge', 'nebadge', 'sebadge', 'centerbadge']
	private static final Map<String,Float> badgeX = ['nwbadge': -0.5f, 'swbadge': -0.5f, 'nebadge': 0.5f, 'sebadge': 0.5f, 'centerbadge': 0.0f ]
	private static final Map<String,Float> badgeY = ['nwbadge': -0.5f, 'swbadge': 0.5f, 'nebadge': -0.5f, 'sebadge': 0.5f, 'centerbadge': 0.0f ]

	protected PPath backgroundNode
	protected PNode shapeNode
	protected PText textNode

	protected int iconSize = 15

	protected boolean secondary = false
	protected boolean textAllowed = true
	private final int secondaryOffset = 10
	private final int secondarySearch = 15

	static final double badgeSize = 0.5
	protected Map<String,PNode> cornerBadgeNodes = new HashMap<String, PNode>(4)

	protected ComponentProperty centerBadgeProperty = null
	protected PPath centerBadgeGroup
	protected PPath centerBadgeBackground
	protected PText centerBadge
	protected int wrap = -1


	IconNode(DLComponent c, DisplayProperties p) {
		super( c, p )
		//check if this is a "secondary" node.  Logic outsourced to the DLComponent.
		secondary = component.isSecondary()
		//does this node have a centerbadge?
		for(def prop : c.getAllProperties() ){
			if (prop.hasCenterbadge()){
				centerBadgeProperty = prop
				//go grab the other props as well
				if (component.hasProperty("wrap")){
					wrap = component.getPropertyValue("wrap")
					log.debug("wrap set to $wrap")
				}
				if (component.hasProperty("fontSize")){
					centerBadgeFont = new Font(Font.SANS_SERIF, Font.BOLD, component.getPropertyValue("fontSize"))
					log.debug("font size set to $centerBadgeFont")
				}
			}
		}

		// Grab the icon size if it exists, otherwise just keep the default.
		if( c.hasProperty('width') || c.hasProperty('depth') ) {
			double width = c.getPropertyValue('width') ?: 1
			double depth = c.getPropertyValue('depth') ?: 1

			iconSize = Math.max( width, depth )
		}

		constructNodes()
	}

	private final void constructNodes() {
		//	 set background node
		backgroundNode = new PPath()
		backgroundNode.setVisible(false)
		//	TODO: set antialias

		// set shape node
		shapeNode = ShapeFactory.getShape(component.getDisplaySetting('shape'))
		reshape()

		addBadges()

		// set text node
		textNode = new PText()
		textNode.setFont( this.component.hasRole("proxy") ? proxyFont : nodeFont )
		textNode.setTextPaint( NORMAL_TEXT_COLOR )
		textNode.setPaint( NORMAL_TEXT_BG_COLOR )
		if (secondary){
			textNode.setVisible(false)
		}

		if (centerBadgeProperty!= null){

			centerBadgeGroup = new PPath()
			centerBadgeGroup.setVisible(false)
			centerBadgeBackground = new PPath()

			if (component.getComponentProperty("centerbadgecolor") != null){
				centerBadgeBackground.setPaint( component.getComponentProperty("centerbadgecolor").getValue() )
			} else {
				centerBadgeBackground.setPaint(centerBadgeColor)
			}

			centerBadgeGroup.addChild(centerBadgeBackground)
			centerBadge = new PText()
			centerBadgeGroup.addChild(centerBadge)
			centerBadge.setFont(centerBadgeFont)

			updateCenterBadge(centerBadgeProperty)

			// Do like the SVG and scale down to the base size before scaling to the target. Subtracting the indent
			// so the overall size matches since this is a pad around the text.
			centerBadgeGroup.scale( 1.0 / centerBadge.getFullBounds().height )
			centerBadgeGroup.scale( iconSize - _indent );
		}

		this.addChild(backgroundNode)
		this.addChild(textNode)
		if (centerBadgeProperty!= null){
			this.addChild(centerBadgeGroup)
		}

		// todo: Change to be a 'display?' shape attribute since we still need the shape for the component pallet.
		String isIconless = component.getDisplaySetting('iconless')
		if (isIconless && isIconless.equals('true')){
			this.setBounds(textNode.getFullBounds())
			this.setChildrenPickable(true)
		} else {
			this.addChild(shapeNode)
			this.setChildrenPickable(false)
		}

        toggleName((boolean) (displayProperties.getProperty('name')), displayProperties.getNameType())

		Double x = component.getPropertyValue('x')
		Double y = component.getPropertyValue('y')
		if (x && y){
			this.setOffset(x, y)
		}
	}

	protected void reshape() {
		shapeNode.scale(iconSize);
		this.setBounds(shapeNode.getFullBounds())
	}

	protected void addBadges(){
		//add a "badge" to the corner of the node
		for(def b : cornerBadges){
			String shapeName = component.getDisplaySetting(b)
			if ( shapeName != null && shapeName != "" ){
				def currentbadge = ShapeFactory.getShape( shapeName )

				// Only do the work if it's a new badge.
				if( !cornerBadgeNodes.containsKey(b) || (cornerBadgeNodes[b].getName() != currentbadge.getName()) ) {

					// Clear any existing badge.
					if( cornerBadgeNodes.containsKey(b) ) {
						removeBadge( b )
					}

					currentbadge.setPickable(false)
					shapeNode.addChild(currentbadge)
					currentbadge.scale(badgeSize);
					currentbadge.setVisible(true)
					currentbadge.setOffset(badgeX[b], badgeY[b])
					currentbadge.moveToFront()
					cornerBadgeNodes[b] = currentbadge
				}
			} else {
				if (cornerBadgeNodes.containsKey(b)){
					removeBadge( b )
				}
			}
		}
	}

	protected void removeBadge( String b ) {
		cornerBadgeNodes[b].setVisible(false)
		shapeNode.removeChild(cornerBadgeNodes[b])
		cornerBadgeNodes.remove(b)
	}

	/**
	 * Computes the selected highlight shape.  Subclasses should override this if they need the selected shape
	 * to be something different than just a bounding rectangle.
	 */
	protected void resetBackgroundNode() {
		backgroundNode.setPathToRectangle((float) 0.0, (float) 0.0, (float) 0.0, (float) 0.0)
		PBounds b = shapeNode.getFullBounds()
		if( centerBadgeGroup != null ) {
			b.add( centerBadgeGroup.getFullBounds() )
		}
		b.setRect(b.getX() - HIGHLIGHT_BORDER, b.getY() - HIGHLIGHT_BORDER, b.getWidth() + 2 * HIGHLIGHT_BORDER, b.getHeight() + 2 * HIGHLIGHT_BORDER)
		//backgroundNode.setPathToRectangle((float) b.x, (float) b.y, (float) (b.width), (float) (b.height))
		backgroundNode.setPathTo( new RoundRectangle2D.Float((float) b.x, (float) b.y, (float) (b.width), (float) (b.height), 10.0f , 10.0f) )

		backgroundNode.setStroke(null) //note: this fixes the "leaving trails" refresh problem
	}

	public void disconnect() {
		component.removePropertyChangeListener(this)
		component = null
		displayProperties.removePropertyChangeListener(this)
	}

	public PNode getShape() {
		return shapeNode
	}

	boolean setSelected(boolean s) {
		super.setSelected( s )

		if (selected) {
			backgroundNode.setPaint(selectColor)
			backgroundNode.setVisible(true)

			textNode.setVisible(true)
			textNode.setTextPaint( NORMAL_TEXT_COLOR)
			textNode.setPaint( NORMAL_TEXT_BG_COLOR)
		} else {
			backgroundNode.setVisible(false)
			if( secondary || !displayProperties.getProperty('name') ) {
				textNode.setVisible(false)
			}
		}

		//don't actually need to refresh on a selection change
		//this.refresh()
		return s
	}

	void setHovered(boolean h) {
		super.setHovered( h )
		if (h) {
			if (!selected) {
				// If it's already visible, highlight it, otherwise just show it.
				if( displayProperties.getProperty('name') ) {
					textNode.setTextPaint( HOVER_TEXT_COLOR )
					textNode.setPaint( HOVER_TEXT_BG_COLOR )
				} else {
					textNode.setVisible( true )
				}
			}
		} else {
			if (!selected) {
				if( displayProperties.getProperty('name') ) {
					textNode.setTextPaint( NORMAL_TEXT_COLOR )
					textNode.setPaint( NORMAL_TEXT_BG_COLOR )
				} else {
					textNode.setVisible( false )
				}
			}
		}
		//don't actually need to refresh on a hover change
		//this.refresh()
	}

	public int getNumberOfNeighbors(int range) {
		int results = 0
		//now, all within the deployment plan itself:
		PNode nodeLayer = this.getParent()
		Point2D.Double source = new Point2D.Double( this.getComponent().getPropertyValue("x"), this.getComponent().getPropertyValue("y") )
		for(def sib : nodeLayer?.getChildrenReference() ){
			//things to screen for: it's a deploymentNode, on the same drawing, and not me
			if (sib instanceof IconNode){
				if (sib.getComponent() != this.getComponent()){
					if (sib.getComponent().getDrawing() == this.getComponent().getDrawing()){

						//log.debug("for node ${this.getComponent()}, checking neighbor ${sib.getComponent()}")

						if (!displayProperties.getProperty("invisibleObjects").contains(sib.getComponent().getType())) {
							Point2D target = new Point2D.Double( sib.getComponent().getPropertyValue("x"), sib.getComponent().getPropertyValue("y") )
							//log.debug("range to $target is ${target.distance(source)}")
							if (target.distance(source) <= range) {
								results += 1
							}
						}
					}
				}
			}
		}

		/*
		ArrayList<DeploymentNode> intersections = new ArrayList<DeploymentNode>()
		def searchArea = new Rectangle2D.Double(this.getComponent().getPropertyValue("x")-range, this.getComponent().getPropertyValue("y")-range, 2*range, 2*range)
		this.getParent().findIntersectingNodes(searchArea,intersections)
		for(def i : intersections){
			if (i instanceof DeploymentNode && i.getVisible() ){
				results ++
			}
		}
		*/
		//log.debug("getNumberOfNeighbors for ${this.getComponent()} found $results within $range")
		return results
	}

	/**
	 * For "secondary" nodes - by which we really mean "control inputs" - handle adjusting the node's initial x&y location out from the node the COIN is attached to.
	 */
	public void adjustOffset() {
		log.debug("${this.getComponent()} adjust offset")
		//it's new, so we need to see if we need to do some offset work
		//assumes the node has been created and added to the layer already
		if (secondary) {
			//get the x&y of the parent we started at
			def centerX = component.getPropertyValue('x')
			def centerY = component.getPropertyValue('y')
			//try the four corners of the icon
			//log.debug("${this.getComponent()} upper left")
			this.component.setPropertyValue('x', centerX - secondaryOffset)
			this.component.setPropertyValue('y', centerY - secondaryOffset)
			if (this.getNumberOfNeighbors(secondarySearch) < 2) { return }
			//log.debug("${this.getComponent()} upper right")
			this.component.setPropertyValue('x', centerX + secondaryOffset)
			this.component.setPropertyValue('y', centerY - secondaryOffset)
			if (this.getNumberOfNeighbors(secondarySearch) < 2) { return }
			//log.debug("${this.getComponent()} lower left")
			this.component.setPropertyValue('x', centerX + secondaryOffset)
			this.component.setPropertyValue('y', centerY + secondaryOffset)
			if (this.getNumberOfNeighbors(secondarySearch) < 2) { return }
			//log.debug("${this.getComponent()} lower left")
			this.component.setPropertyValue('x', centerX - secondaryOffset)
			this.component.setPropertyValue('y', centerY + secondaryOffset)
			if (this.getNumberOfNeighbors(secondarySearch) < 2) { return }

			//secondary loop: now we just spiral out to infinity
			//log.debug("${this.getComponent()} spiral")
			Point2D.Double nextPoint
			def spiral = new SpiralIterator(new Point2D.Double(centerX, centerY))
			while (spiral.hasNext()) {
				nextPoint = spiral.next()
				//println "trying $nextPoint"
				this.component.setPropertyValue('x', nextPoint.getX().toInteger())
				this.component.setPropertyValue('y', nextPoint.getY().toInteger())
				if (this.getNumberOfNeighbors(secondarySearch) < 2) { return }
			}
		}
	}

	void propertyChanged(def who, String prop, oldVal, newVal) {
		boolean isDisplayName = (boolean) (displayProperties.getProperty('name'))
		def strNameType = displayProperties.getNameType()
		def macIdPropertiesList = []
		component.listMacIdProperties().each {macIdPropertiesList.add(it)}
		macIdPropertiesList.add('mac_id')

		switch (prop) {
			case ['x', 'y']:
				//log.info("change proeprty in DeploymentNode")
				this.setOffset(component.getPropertyValue('x'), component.getPropertyValue('y'))
				break
			case 'name':
				if (isDisplayName) {
					if (strNameType.equals("Name")) {
						showNodeName(newVal)
					}
				}
				break
			case 'color':
				shapeNode.setPaint(newVal)
				break

			case macIdPropertiesList:
				if (isDisplayName) {
					if (strNameType.equals("MacID")) {
						def macIds = component.listMacIdProperties()
						String str_macIds = ""
						macIds.each {it ->
							if (prop.equals(it)) {
								str_macIds += newVal + "\n"
							} else {
								str_macIds += component.getPropertyValue(it) + "\n"
							}
						}
						showNodeName(str_macIds)
					}
				}
				break

			case 'fontSize':
				updateFontSize(newVal)
				break
			case 'wrap':
				updateWrap(newVal)
				break
			case 'centerbadgecolor':
				centerBadgeBackground.setPaint( component.getComponentProperty("centerbadgecolor").getValue() )
				break

			case 'shape':
				if( newVal && ( oldVal != newVal ) ) {
					this.removeAllChildren()
					constructNodes()
					setSelected( getSelected() )
				}
				break

			case 'nwbadge':
			case 'swbadge':
			case 'nebadge':
			case 'sebadge':
			case 'centerbadge':
				addBadges()
				break
		}

		//make sure it isn't a display setting instead
		if ( ((DLComponent)who).hasProperty(prop) ){
			if (((DLComponent)who).getComponentProperty(prop).hasCenterbadge()){
				updateCenterBadge(((DLComponent)who).getComponentProperty(prop))
			}
		}

		toggleName((boolean) (displayProperties.getProperty('name')), displayProperties.getNameType())
	}

	protected void updateCenterBadge(ComponentProperty p){
		if (!p){return}
		def newVal
		if (!p.hasValueChoices()){
			newVal = p.getValue().toString()
		} else {
			newVal = p.choiceForValue(p.getValue())
		}
		updateCenterBadgeText(newVal)
	}

	protected void updateCenterBadgeText(String newVal){

		centerBadge.setFont(centerBadgeFont)
		centerBadge.setText( newVal.toString() )

		if (wrap > 0){
			centerBadge.setConstrainWidthToTextWidth(false)
			centerBadge.setWidth(wrap)
		} else{
			centerBadge.setConstrainWidthToTextWidth(true)
		}
		centerBadge.setConstrainHeightToTextHeight(true)

		log.debug("building center badge with wrap $wrap and font $centerBadgeFont")
		shapeNode.setVisible(false)
		centerBadgeBackground.setPathToRectangle((float) 0.0, (float) 0.0, (float) 0.0, (float) 0.0)
		PBounds b = centerBadge.getBounds()
		b.setRect(b.getX() - _indent, b.getY() - _indent, b.getWidth() + 2 * _indent, b.getHeight() + 2 * _indent)
		centerBadgeBackground.setPathTo( new RoundRectangle2D.Float((float) b.x, (float) b.y, (float) (b.width), (float) (b.height), 10.0f , 10.0f) )
		//centerBadgeBackground.setStroke(null) //note: this fixes the "leaving trails" refresh problem
		def d = centerBadgeGroup.getUnionOfChildrenBounds(null)
		centerBadgeGroup.setOffset( -(d.getSize().getWidth() / 2) + _indent , -(d.getSize().getHeight() / 2) + _indent )
		centerBadgeGroup.setVisible(true)
		//centerBadgeGroup.setPickable(true)
		//centerBadge.setPickable(true)
		//centerBadgeBackground.setPickable(true)
		this.setBounds(centerBadgeGroup.getFullBounds())
		//move the name field to a better position

		resetBackgroundNode()
	}

	protected void updateFontSize(def fontSize){
		centerBadgeFont = new Font(Font.SANS_SERIF, Font.BOLD, fontSize)
		//centerBadge.setFont(centerBadgeFont)
		//updateCenterBadgeText(centerBadgeProperty.getValue())
		updateCenterBadge(centerBadgeProperty)
	}

	protected void updateWrap(def wrapWidth){
		wrap = wrapWidth
		//updateCenterBadgeText(centerBadgeProperty.getValue())
		updateCenterBadge(centerBadgeProperty)
	}

	void displayPropertyChanged(def who) {
		super.displayPropertyChanged( who )
		toggleName((boolean) (displayProperties.getProperty('name')), displayProperties.getNameType())
	}

	void remove(PNode n) {}

	void toggleName(boolean isOn) {
		toggleName(isOn, displayProperties.getNameType())
	}

	void toggleName(boolean isOn, String nameType) {
		if( textAllowed ) {
			String value = ""

			if (nameType == 'Name') {
				value = component.getPropertyValue('name')
			} else if (nameType == 'MacID') {
				def macIds = component.listMacIdProperties()
				value = macIds.collect {"${component.getPropertyValue(it)}"}.join("\n")
			}

			showNodeName(value)

			if( !selected && !hovered ) {
				textNode.setVisible( isOn )
			}
		}
	}

	void showNodeName(String nameValue) {
		textNode.setText(nameValue)
		if (centerBadgeProperty == null){
			textNode.centerBoundsOnPoint(shapeNode.getBounds().width / 2, shapeNode.getBounds().height / 2 )
		} else {
			textNode.centerBoundsOnPoint(centerBadgeGroup.getBounds().width / 2, centerBadgeGroup.getBounds().height / 2 )
		}

		this.addChild(textNode)
        resetBackgroundNode()
	}

	PNode getTextNode() {
		return textNode
	}
}
