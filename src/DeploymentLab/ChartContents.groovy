package DeploymentLab

import java.awt.geom.Point2D;

/**
 * Stores the contents of a chart and provides the values in various formats.
 * Created by IntelliJ IDEA.
 * User: ghelman
 * Date: Nov 10, 2009
 * Time: 2:03:22 PM
 *
 * The "lingua franca" of ChartContents is the "tablestring", which is nothing more than a formatted string:
 * "Xval,Yval;Xval2,yVal2;" etc.
 *
 * In the case of Transformer efficiencies, for example (which is the cause of this class's creation, X is assumed to be Load and Y is assumed to be efficiency, so:
 * "load,eff%;load,eff%;" etc.
 *
 * The chart has two potential formats: value and percentage.  Value format has no special treatment: the input values are assumed to be the values wanted on the chart with no monkey business.
 * Percentage format is where it gets interesting: the values provided for X are treated as being a percentage of some maximum value (the apex).
 * Internally, the chart is always stored in value format, but can be set or returned in either, as ChartContents will use the current apex to convert the x-axis values between value and percentage mode as needed.
 *
 * @see DeploymentLab.PropertyEditor.CustomCellRenderer
 * @see DeploymentLab.PropertyEditor.ChartContentsEditor
 * @see Point2D.Double
 */
public class ChartContents {
	/** apex (or max load) of the table in percentage format  */
	private double apex;
	/** list of points describing the table, always in value format  */
	protected List<Point2D.Double> allPoints;
	/** While Apex defines the amount to adjust the X-axis, yAdjust is the amount to convert the y-Axis by. */
	private BigDecimal yAdjust = 100;

	public double getApex() {
		return apex;
	}

	public void setApex(double apex) {
		this.apex = apex;
	}

	public List<Point2D.Double> getPoints() {
		return allPoints;
	}

	public void setPoints(List<Point2D.Double> freshPointsList) {
		this.allPoints = freshPointsList;
	}

	/**
	 * Returns a tablestring representing the chart in value format.
	 * @return a tablestring in value format.
	 */
	public String getValueString() {
		return (buildChartString(this.allPoints))
	}

	/**
	 * Returns the current chart with x-values as percentages.
	 * (Which is probably how the user typed it.)  This requires conversion from the internally stored values.
	 * @return a tablestring in percentage format.
	 */
	public String getPercentageString() {
		//println "creating the percentage string; apex is $apex"
		String percentageString = ""
		def adjustment = apex / 100
		BigDecimal yVal
		allPoints.each { it ->
			//println "adding $it to percentageString"
			//why the casts, you ask?  Because we got some really weird floating-point precision errors when just using groovy & implicit doubles.
			yVal = ((BigDecimal) it.y) * yAdjust
			//percentageString += "${it.x / adjustment},${it.y * yAdjust};"
			percentageString += "${it.x / adjustment},$yVal;"
		}
		//println "percentage string returned was $percentageString"
		return (percentageString)
	}

	/**
	 * Sets the chart via a percentage string.  The user-entered percentages are converted to values for storage.
	 * @param freshPercentageString a tablestring with x-axis as percentages.
	 * @param freshApex the x-value equivalent to 100%.
	 */
	public void setPercentageString(String freshPercentageString, double freshApex) {
		this.allPoints = resetChart(freshPercentageString, freshApex)
		this.apex = freshApex;
	}

	/**
	 * Constructor for empty chart.
	 */
	public ChartContents() {
		this.allPoints = []
		this.apex = 100;
	}

	/**
	 * Constructor based on a percentage string.
	 * @param freshPercentageString a tablestring with x-axis as percentages.
	 * @param freshApex the x-value equivalent to 100%.
	 */
	public ChartContents(String freshPercentageString, double freshApex) {
		if (freshPercentageString.trim() != "") {
			this.allPoints = resetChart(freshPercentageString, freshApex)
		} else {
			this.allPoints = []
		}
		this.apex = freshApex;
	}

	/**
	 * Constructor based on values.
	 * @param freshValueString a tablestring with x-axis as values.
	 */
	public ChartContents(String freshValueString) {
		//println "wooo!  new instance with values $freshValueString"
		if (freshValueString.trim() != "") {
			this.allPoints = parseTable(freshValueString);
		} else {
			this.allPoints = []
		}
		this.apex = 100;
	}

	/**
	 * Converts a percentageString & apex to a list of value points.
	 * @param freshPercentageString a tablestring with x-axis as percentages.
	 * @param freshApex the x-value equivalent to 100%.
	 * @return a List of Point2D.Doubles matching the provided tablestring.
	 * @see Point2D.Double
	 */
	private List<Point2D.Double> resetChart(String freshPercentageString, double freshApex) {
		def table = parseTable(freshPercentageString);
		double adjustment = freshApex / 100
		table = table.collect { it ->
			return new Point2D.Double((it.x * adjustment), (it.y / yAdjust))
		}
		return (table)
	}

	/**
	 * Converts a tablestring into a list of (sorted) points, with no other adjustment.
	 * @param sTable a tablestring.
	 * @return a List of Point2D.Doubles matching the provided tablestring.
	 * @see Point2D.Double
	 */
	public static List<Point2D.Double> parseTable(String sTable) {
		if (sTable.trim().equals("")) {
			return ([])
		}
		def pairs = sTable.split(';')
		List table = new ArrayList()
		//table = pairs.collect{
		for (String it: pairs) {
			def elems = it.split(',')
			if (elems.length == 2 && elems[0].isDouble() && elems[1].isDouble()) {
				//return ( new Point2D.Double(Double.parseDouble(elems[0]), Double.parseDouble(elems[1])) )
				table += new Point2D.Double(Double.parseDouble(elems[0]), Double.parseDouble(elems[1]))
			}
			//a non-double pair will result in nothing being returned from the closure.
		}
		table.sort { a, b -> a.x.compareTo(b.x) }
		return (table)
	}

	/**
	 * Returns the number of points in a given tablestring.
	 */
	public static Integer numberOfPoints(String sTable) {
		List<Point2D.Double> tableList = parseTable(sTable)
		return (tableList.size())
	}

	/**
	 * Directly converts a list of points into a tablestring, performing no other adjustment.
	 * @param pointsList a List of Point2D.Doubles to be processed.
	 * @return a tablestring matching the list of points.
	 * @see Point2D.Double
	 */
	public static String buildChartString(List<Point2D.Double> pointsList) {
		String chart = ""
		pointsList.each { it ->
			chart += "$it.x,$it.y;"
		}
		return (chart)
	}

	/**
	 * Returns a list of all x-values in the tablestring
	 * @param tableString
	 * @return
	 */
	public static List<Double> getXValues(String tableString) {
		if (tableString.trim().equals("")) {
			return ([])
		}
		def pairs = tableString.split(';')
		List table = new ArrayList()
		//table = pairs.collect{
		for (String it: pairs) {
			def elems = it.split(',')
			table += Double.parseDouble(elems[0])
		}
		table.sort()
		return table
	}


	public static List<Pair<String, String>> tableStringToPairs(String tableString) {
		if (tableString.trim().equals("")) {
			return ([])
		}
		def pairs = tableString.split(';')
		List table = new ArrayList()
		for (String it: pairs) {
			String[] elems = it.split(',')

			if (elems.length == 1) {
				table += new Pair(elems[0], "")
			} else {
				table += new Pair(elems[0], elems[1])
			}

		}
		//table.sort{ a, b -> a.a.compareTo(b.a) }
		return (table)
	}


	boolean equals(o) {
		if (this.is(o)) return true;

		if (!o || getClass() != o.class) return false;

		ChartContents that = (ChartContents) o;

		if (Double.compare(that.apex, apex) != 0) return false;
		if (allPoints ? !allPoints.equals(that.allPoints) : that.allPoints != null) return false;
		if (yAdjust ? !yAdjust.equals(that.yAdjust) : that.yAdjust != null) return false;

		return true;
	}

	int hashCode() {
		int result;

		long temp;
		temp = apex != +0.0d ? Double.doubleToLongBits(apex) : 0L;
		result = (int) (temp ^ (temp >>> 32));
		result = 31 * result + (allPoints ? allPoints.hashCode() : 0);
		result = 31 * result + (yAdjust ? yAdjust.hashCode() : 0);
		return result;
	}
}