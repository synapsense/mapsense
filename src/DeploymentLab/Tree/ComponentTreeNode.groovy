
package DeploymentLab.Tree

import javax.swing.tree.DefaultMutableTreeNode
//import javax.swing.tree.MutableTreeNode

import DeploymentLab.Model.*

class ComponentTreeNode extends DefaultMutableTreeNode implements Comparable {
	DLComponent component

    ComponentTreeNode(DLComponent c, boolean allowChildren) {
		super(c, allowChildren)
        component = c
	}

	String toString() {
        if(component==null)
            return ""
        if(component.hasRole("staticchild")){
            return component.getPropertyValue("parent_name")+"-" + component.getPropertyValue("name")
        } else if( component.hasRole( ComponentRole.TEXT ) ) {
	        // There is no useful name, so display part of the text itself.
	        String val = component.getPropertyValue('notes').toString()
	        if( val.length() > 20 ) {
		        return "${val.substring(0,20)}..."
	        }
	        return val
        }else{
            return getUserObject().getPropertyValue('name')
        }

	}

	public int compareTo(final Object o) {
        return this.toString().compareToIgnoreCase(o.toString());
    }

}