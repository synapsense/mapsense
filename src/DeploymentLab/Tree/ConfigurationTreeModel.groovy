package DeploymentLab.Tree

import javax.swing.tree.DefaultMutableTreeNode
import javax.swing.tree.DefaultTreeModel
import javax.swing.tree.TreePath

import DeploymentLab.channellogger.*
import DeploymentLab.Model.*
import DeploymentLab.CentralCatalogue

class ConfigurationTreeModel extends DefaultTreeModel {
	private static final Logger log = Logger.getLogger(ConfigurationTreeModel.class.getName())

	protected ConfigurationGroup treeRoot
	
	ConfigurationTreeModel() {
		super(new ConfigurationGroup(CentralCatalogue.getUIS("components.palette"), []))
		treeRoot = getRoot()
	}

	def getConfigurationFromPath(TreePath path) {
		if(path == null || path.getPathCount() < 1)
			return null
		return path.getLastPathComponent().getConfigurations()
	}

	void loadConfiguration(def cXml){
		String groupPath = cXml.@grouppath.text()
		String configuration = cXml.@type.text()
		def classes = cXml.@classes.text().split(',').collect{it}
		String displayName = cXml.display.name.text()

		if(!isLoadedConfiguration(groupPath, configuration)){
			loadGroup(treeRoot, groupPath, configuration, classes, displayName)
		}
	}
	
	void loadGroup(ConfigurationGroup parent, String groupPath, String configuration, def classes, String displayName){
		def groupList = groupPath.split(';')
		String groupName = groupList[0]

		ConfigurationGroup groupNode = parent.findSubgroup(groupName)

		if(groupNode == null){
			groupNode = new ConfigurationGroup(groupName, [])
			insertNodeInto(groupNode, parent, parent.getChildCount())
		}
			
		// if last group
		if(groupList.size()==1){
			groupNode.addConfigs(configuration)
			AddCustomConfigurationNode(groupNode, configuration, displayName)
		}else{
			groupList-=groupList[0]
			String newGroupPath = groupList.join(';')
			// recursive
			loadGroup(groupNode, newGroupPath, configuration, classes, displayName)
		}
	}
	
	void unloadConfiguration(def cXml){
		String groupPath = cXml.@grouppath.text()
		String configuration = cXml.@type.text()
		
		unloadGroup(treeRoot, groupPath, configuration)
	}
	
	void unloadGroup(ConfigurationGroup parent, String groupPath, String configuration){
		def groupList = groupPath.split(";")
		String groupName = groupList[0]

		ConfigurationGroup groupNode = parent.findSubgroup(groupName)
		
		// remove configuration
		if(groupNode!=null && groupNode.getConfigurations().contains(configuration)){
			groupNode.removeConfigs(configuration)
		}
		
		//recursive function to delete child group
		if(groupList.size()>1){
			groupList-=groupList[0]
			String newGroupPath = groupList.join(';')
			unloadGroup(groupNode, newGroupPath, configuration)
		}
		//delete current group if there is no child
		if(groupNode!=null && groupNode.getChildCount() + groupNode.getConfigurationCount() ==0){
			removeNodeFromParent(groupNode)
		}
	}
	/**
	 *  Abstract method for other trees using custom TreeNode Type
	 */
	void AddCustomConfigurationNode(ConfigurationGroup groupTreeNode, String configuration, String displayName){
	}

	/**
	 * Traverses the model to find components that should be removed because they are no longer valid or needed in
	 * the specific model.
	 */
	void pruneConfigurations() {
		// default impl does nothing. Subclass should override if they have a need to prune on occasion.
	}

	private boolean isLoadedConfiguration(String groupPath, String configurationName){
		if(groupPath==null || groupPath==""){
			return true
		}
		def groupList = groupPath.split(';')
		
		ConfigurationGroup parent = treeRoot
		
		groupList.each{gname->
			if(parent.findSubgroup(gname)==null){
				return false
			}else{
				parent = parent.findSubgroup(gname)
			}
		}
		
		def childrenConfiguration = parent.getConfigurations()
		if(childrenConfiguration.contains(configurationName)){
			return true
		}else
			return false
		
	}	
}
class ConfigurationGroup extends DefaultMutableTreeNode {
	private static final Logger log = Logger.getLogger(ConfigurationGroup.class.getName())

	def configurations

	ConfigurationGroup(String n, def c) {
		super(n, true)
		configurations = c
	}

	void addConfigs(def c) {
		configurations += c
	}

	void removeConfigs(def c) {
		configurations -= c
	}

	ConfigurationGroup findSubgroup(String name) {
		return children().find{it.getUserObject().equals(name)}
	}

	def getConfigurations() {
		return configurations
	}

	int getConfigurationCount() {
		return configurations.size()
	}
}


