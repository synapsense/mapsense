
package DeploymentLab.Tree

import java.awt.event.KeyEvent
import java.awt.event.KeyListener

import javax.swing.JOptionPane
import javax.swing.JTree

import DeploymentLab.Model.*
import DeploymentLab.channellogger.*
import DeploymentLab.UndoBuffer
import DeploymentLab.UIDisplay
import DeploymentLab.ProgressManager
import DeploymentLab.CentralCatalogue

class NetworkTreeKeyboardCommands implements KeyListener {

	private static final Logger log = Logger.getLogger(NetworkTreeKeyboardCommands.class.getName())

	private ComponentModel componentModel
	private UndoBuffer undoBuffer
	private UIDisplay uiDisplay
	
	NetworkTreeKeyboardCommands(ComponentModel cm, UndoBuffer ub, UIDisplay ud) {
		componentModel = cm
		undoBuffer = ub
		uiDisplay = ud
	}

	void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_DELETE) {
			doDelete(e.getSource())
			e.consume()
		}
	}

	void doDelete(JTree tree) {
		// TODO: change this to use the selection model
		def objs = tree.getSelectionPaths().collect{ p -> p.getLastPathComponent().getUserObject() }

		if(objs.any{it instanceof java.lang.String}) {  // ie, if the user has the "Data Sources" root tree node selected
			log.error("Cannot delete \"Data Sources\".\nPlease unselect it and try again.", 'message')
			return
		}

        if(objs.any{it.hasClass('staticchild')}){
            log.info("Cannot delete child Component!", 'message')
            return
        }

		// check user level 
		if(objs.any{it.hasClass('network')}) {
			if(!uiDisplay.isAccessible('deleteDataSource')){
				log.info(uiDisplay.getUserLevel() + ' cannot delete Data Source!', 'message')
				return
			}
		} else {
			if(!uiDisplay.isAccessible('deleteComponent')){
				log.info(uiDisplay.getUserLevel() + ' cannot delete component!', 'message')
				return
			}
		}
		
		if (tree.getSelectionCount() == 0) {
			JOptionPane.showMessageDialog(null, 'Please select something to delete!')
			return
		}

		if(objs.any{it.hasClass('network')}) {
			int res = JOptionPane.showConfirmDialog( null, CentralCatalogue.getUIS("network.delete.dialogtext"),
                                                     CentralCatalogue.getUIS("network.delete.dialogtitle"),
                                                     JOptionPane.YES_NO_OPTION )
			if(res != JOptionPane.YES_OPTION)
				return
		}


		ProgressManager.doTask {
			log.trace("Deleting...", "progress")
			try {
				undoBuffer.startOperation()
				objs.each{ component ->
					componentModel.remove(component)
				}
			} catch(Exception e) {
				log.error("Exception removing networks: " + log.getStackTrace(e))
				undoBuffer.rollbackOperation()
			} finally {
				undoBuffer.finishOperation()
			}
		}
	}

	void keyReleased(KeyEvent e) {
	}

	void keyTyped(KeyEvent e) {
	}
}

