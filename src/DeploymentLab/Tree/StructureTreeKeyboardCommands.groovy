
package DeploymentLab.Tree

import java.awt.event.KeyEvent
import java.awt.event.KeyListener

import javax.swing.JOptionPane
import javax.swing.JTree

import DeploymentLab.Model.*
import DeploymentLab.channellogger.*
import DeploymentLab.UndoBuffer
import DeploymentLab.UIDisplay
import DeploymentLab.SelectModel
import DeploymentLab.Tools.NodeOperator
import DeploymentLab.ProgressManager
import DeploymentLab.CentralCatalogue

class StructureTreeKeyboardCommands implements KeyListener {

	private static final Logger log = Logger.getLogger(StructureTreeKeyboardCommands.class.getName())

	private ComponentModel componentModel
	private UndoBuffer undoBuffer
	private UIDisplay uiDisplay
	private SelectModel selectModel
	private NodeOperator nodeOperator
	
	StructureTreeKeyboardCommands(ComponentModel cm, UndoBuffer ub, UIDisplay ud, SelectModel sm, NodeOperator no) {
		componentModel = cm
		undoBuffer = ub
		uiDisplay = ud
		selectModel = sm
		nodeOperator = no
	}

	void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_DELETE) {
			doDelete(e.getSource())
			e.consume()
		}

/*
		if(e.getKeyCode() == KeyEvent.VK_C && e.isControlDown()){
			if(uiDisplay.isAccessible('clone')){
				log.info('Copy selected components')
				nodeOperator.doCopy()

			}
		}

		if(e.getKeyCode() == KeyEvent.VK_V && e.isControlDown()){
			if(uiDisplay.isAccessible('clone')){
				log.info('Paste copied components')
				nodeOperator.doPaste(null)
			}
		}
*/
	}
	
	void doDelete(JTree tree) {
		def objs = tree.getSelectionPaths().collect{ p -> p.getLastPathComponent().getUserObject() }
		if(objs.any{it instanceof java.lang.String && it.equals("Root")} ){
			log.info("Cannot delete \"Root\".\nPlease unselect it and try again.", 'message')
			return
			
		}

        if(objs.any{it.hasClass('staticchild')}){
			log.info("Cannot delete child Component!", 'message')
			return
		}

        if(objs.any{it.hasClass('drawing')}){
			log.info("Cannot delete Drawing!", 'message')
			return
		}
		//		 check user level 
		if(objs.any{it.hasRole('zone')}){
			if(!uiDisplay.isAccessible('deleteZone')){
				log.info(uiDisplay.getUserLevel() + ' cannot delete grouping!', 'message')
				return
			}
		} else {
			if(!uiDisplay.isAccessible('deleteComponent')){
				log.info(uiDisplay.getUserLevel() + ' cannot delete component!', 'message')
				return
			}
		}
		
		if (tree.getSelectionCount() == 0) {
			JOptionPane.showMessageDialog(null, 'Please select something to delete!')
			return
		}

		if( objs.any{ it.hasRole( ComponentRole.LOGICAL_GROUP ) } ) {
			int res = JOptionPane.showConfirmDialog( null, CentralCatalogue.getUIS("group.logicalgroup.delete.dialogtext"),
                                                     CentralCatalogue.getUIS("group.logicalgroup.delete.dialogtitle"),
                                                     JOptionPane.YES_NO_OPTION )
			if(res != JOptionPane.YES_OPTION)
				return
		} else if( objs.any{ it.hasRole( ComponentRole.ZONE ) } ) {
            int res = JOptionPane.showConfirmDialog( null, CentralCatalogue.getUIS("group.zone.delete.dialogtext"),
                                                     CentralCatalogue.getUIS("group.zone.delete.dialogtitle"),
                                                     JOptionPane.YES_NO_OPTION )
			if(res != JOptionPane.YES_OPTION)
				return
		}

		ProgressManager.doTask {
			log.trace("Deleting...", "progress")
			try {
				undoBuffer.startOperation()
				objs.each{ component ->
					componentModel.remove(component)
				}
			} catch(Exception e) {
				log.error("Exception removing groupings: " + log.getStackTrace(e))
				undoBuffer.rollbackOperation()
			} finally {
				undoBuffer.finishOperation()
			}

		}
	}

	void keyReleased(KeyEvent e) {
	}

	void keyTyped(KeyEvent e) {
	}
}

