package DeploymentLab.Tree

import DeploymentLab.ComponentEditor.ComponentEditor
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentRole
import DeploymentLab.Model.ComponentType
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.ModelChangeListener
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.Tools.PopupMenu
import DeploymentLab.channellogger.Logger
import edu.umd.cs.piccolo.event.PInputEvent
import groovy.xml.MarkupBuilderHelper
import java.awt.datatransfer.DataFlavor
import java.awt.datatransfer.Transferable
import java.awt.event.ActionListener
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.activation.DataHandler
import javax.swing.tree.DefaultMutableTreeNode
import javax.swing.tree.DefaultTreeModel
import javax.swing.tree.TreeNode
import javax.swing.tree.TreePath
import DeploymentLab.*
import javax.swing.*
import javax.swing.tree.MutableTreeNode
import DeploymentLab.Model.ModelState
import DeploymentLab.Model.MetamorphosisEvent

class StructureTree extends JTree implements SelectionChangeListener {

	protected UIDisplay uiDisplay
	private static final Logger log = Logger.getLogger(StructureTree.class.getName())

	StructureTree(ComponentModel cmodel, UndoBuffer ub, UIDisplay ud) {
		super()
		uiDisplay = ud
		setEditable(false)
		setDragEnabled(true)
		setShowsRootHandles(true)
		setDropMode(DropMode.ON_OR_INSERT)
		setTransferHandler(new StructureTreeTransferHandler(cmodel, ub))
		//addMouseListener(new StructureTreeMouseListener(this, parent))
		this.setCellRenderer(new MapSenseTreeCellRenderer())
	}

	void expandAll(DefaultMutableTreeNode r = null) {
		DefaultMutableTreeNode root
		if (r == null)
			root = getModel().getRoot()
		else
			root = r

		expandPath(new TreePath(root.getPath() as Object[]))
		root.children().each { child -> expandAll(child) }
	}


	void collapseBranch(DefaultMutableTreeNode r = null) {
		DefaultMutableTreeNode root
		if (r == null)
			root = getModel().getRoot()
		else
			root = r
		root.children().each { child -> collapseAll(child) }
		collapsePath(new TreePath(root.getPath() as Object[]))
	}

	//collapse the "groupings" level
	void collapseFirstLevel() {
		DefaultMutableTreeNode root = this.getModel().getRoot()
		//walk to the end
		def current = root
		while (!current.isLeaf()) {
			current = current.getFirstChild()
		}
		def parent = current.getParent().getParent()
		if (parent == null) {
			parent = current.getParent()
		}
		def count = parent.getChildCount()
		for (int i = 0; i < count; i++) {
			parent.getChildAt(i)
			collapsePath(new TreePath(parent.getChildAt(i).getPath() as Object[]))
		}
	}


	void selectionChanged(SelectionChangeEvent e) {
        StructureTreeModel structModel = (StructureTreeModel) getModel()

		removeSelectionPaths((e.componentsRemoved.collect {c -> structModel.getPathToComponent(c)} + e.zonesRemoved.collect {z -> structModel.getPathToZone(z)}).flatten() as TreePath[])
		removeSelectionPaths(e.networksRemoved.collect {n -> n.getChildComponents()}.flatten().collect {c -> structModel.getPathToComponent(c)}.flatten() as TreePath[])
		removeSelectionPaths(e.groupsRemoved.collect {s -> structModel.getPathToGroup(s)} as TreePath[])
        if( e.drawingsRemoved.contains( structModel.getMyDrawing() ) ) {
            removeSelectionPath( new TreePath( structModel.getRoot() ) )
        }

		addSelectionPaths((e.componentsAdded.collect {c -> structModel.getPathToComponent(c)} + e.zonesAdded.collect {z -> structModel.getPathToZone(z)}).flatten() as TreePath[])
		addSelectionPaths(e.networksAdded.collect {n -> n.getChildComponents()}.flatten().collect {c -> structModel.getPathToComponent(c)}.flatten() as TreePath[])
		addSelectionPaths(e.groupsAdded.collect {s -> structModel.getPathToGroup(s)} as TreePath[])
        if( e.drawingsAdded.contains( structModel.getMyDrawing() ) ) {
            addSelectionPath( new TreePath( structModel.getRoot() ) )
        }
	}

    void activeDrawingChanged( DLComponent oldDrawing, DLComponent newDrawing ) {
        // The NetworkTreeController manages this.
    }
}

/**
 * MouseAdapter for both the Structure Tree and the Network Tree.
 * Responsible, primarily, for creating the right-click menu.
 */
class StructureTreeMouseListener extends MouseAdapter {
	private static final Logger log = Logger.getLogger(StructureTreeMouseListener.class.getName())
	JTree tree
	JFrame parent
	DeploymentPanel deploymentPanel
	PopupMenu contextMenu
	SelectModel selectModel

	public StructureTreeMouseListener(JTree t, DeploymentPanel _deploymentPanel, JFrame _parent, PopupMenu pm, SelectModel sm) {
		tree = t
		deploymentPanel = _deploymentPanel
		parent = _parent
		contextMenu = pm
		selectModel = sm
	}

	public void mousePressed(MouseEvent e) {
		checkPopup(e)
	}

	public void mouseReleased(MouseEvent e) {
		checkPopup(e)
	}

	/**
	 * Construct and display the right-click menu if appropriate.
	 */
	private void checkPopup(MouseEvent e) {
		if (e.isPopupTrigger()) {
			TreePath selPath = tree.getPathForLocation(e.getX(), e.getY())
			if (selPath == null)
				return

			def clicked = selPath.getLastPathComponent()
			if (clicked instanceof ComponentTreeNode) {
				DLComponent c = clicked.getUserObject()

				if(! (c in selectModel.getExpandedSelection())) {
					selectModel.setSelection([c])
				}

				JPopupMenu editMenu = new JPopupMenu()

				//Scroll To
				JMenuItem scrollItem = new JMenuItem("Center in View")
				scrollItem.setEnabled(tree.uiDisplay.isAccessible('scrollToCenter'))
				scrollItem.addActionListener(
						{ev ->
							//deploymentPanel.scrollToView(c)
							if (c.isContainer()) {
								deploymentPanel.scrollToView(c.getChildComponents())
							} else if (c.hasRole('drawing')) { // todo: Now that there's no drawing node, do this for the root node?
								deploymentPanel.fitView(true)
							} else if (c.hasRole('network')) {
								deploymentPanel.scrollToView(c.getChildComponents())
							} else {
								deploymentPanel.scrollToView(c)
							}
						} as ActionListener)
				editMenu.add(scrollItem)

				//expand branch
				JMenuItem exbItem = new JMenuItem("Expand All")
				exbItem.setEnabled(tree.uiDisplay.isAccessible('expandTree'))
				exbItem.addActionListener({ev -> tree.expandAll()  } as ActionListener)
				editMenu.add(exbItem)

				//collapse branch
				JMenuItem collapseItem = new JMenuItem("Collapse All")
				collapseItem.setEnabled(tree.uiDisplay.isAccessible('collapseTree'))
				collapseItem.addActionListener({ev -> tree.collapseFirstLevel()  } as ActionListener)
				editMenu.add(collapseItem)

				//separator between the things that only go in this menu and the stuff shared with the context menu
				editMenu.addSeparator()

				contextMenu.addCommonOptionsMenuItems(editMenu, (PInputEvent) null, selectModel.getExpandedSelection().size())

				// Add action listeners as needed
				// The following code looks at menu items that are direct children of the popup menu
				// If need to look at menu items that are in submenus, will need to make this a recursive
				// method
				for (MenuElement child : editMenu.getSubElements()) {
					if (child instanceof JMenuItem) {
						JMenuItem menuItem = (JMenuItem) child
						String menuText = menuItem.getText()
						if (menuText.equals("Edit Configuration")) {
							tree.uiDisplay.setDisplay('componentEditor', menuItem)
						} else if (menuText.equals("Component XML")) {
							tree.uiDisplay.setDisplay('getInfo', menuItem)
						} else if (menuText.equals("Make a Component Proto")) {
							tree.uiDisplay.setDisplay('getInfo', menuItem)
						} else if (menuText.equals("Component Props")) {
							tree.uiDisplay.setDisplay('getInfo', menuItem)
						} else if (menuText.equals("Component Specs")) {
							tree.uiDisplay.setDisplay('getInfo', menuItem)
						} else if (menuText.equals("Object Props")) {
							tree.uiDisplay.setDisplay('getInfo', menuItem)
						} else if (menuText.equals("Object XML")) {
							tree.uiDisplay.setDisplay('getInfo', menuItem)
						} else if (menuText.equals("Show PNodes")) {
							tree.uiDisplay.setDisplay('getInfo', menuItem)
						} else if (menuText.equals("Show Transform")) {
							tree.uiDisplay.setDisplay('getInfo', menuItem)
						} else if (menuText.equals("Show Children and Parents")) {
							tree.uiDisplay.setDisplay('getInfo', menuItem)
						} else if (menuText.equals("Show Z-Index")) {
							tree.uiDisplay.setDisplay('getInfo', menuItem)
						}
					}
				}

				//produce final menu
				editMenu.show(tree, e.getX(), e.getY())
			}
		}
	}

}

class StructureTreeModel extends DefaultTreeModel implements ModelChangeListener {
	private static final Logger log = Logger.getLogger(StructureTreeModel.class.getName())

    public static final String ROOT_NODE_STRING = 'Groupings'

	protected DefaultMutableTreeNode treeRoot

	// For efficient searching, map each type to it's TreeNode
	private roomNodes = [:]
	private zoneNodes = [:]
	private groupNodes = [:]

	//note: with the addition of Groups, components can now be in more than one place in the tree.  So, we need to expand this mapping (definition to generics to self-document)
	protected Map<DLComponent, List<ComponentTreeNode>> componentNodes = [:]

	//protected DLComponent drawing
	private SelectModel sm
    private ComponentModel cm
    private DLComponent myDrawing

	boolean inTheMiddleOfARename = false
	boolean respondingToEvent

	StructureTreeModel(SelectModel _sm, ComponentModel _cm, DLComponent _drawing) {
        // If we have one, make the drawing the root so we don't have an unnecessary tree level and can still access the drawing properties.
        super( (_drawing == null) ? new DefaultMutableTreeNode( ROOT_NODE_STRING, true) : new ComponentTreeNode( _drawing, true ) )
		treeRoot = getRoot()
		setAsksAllowsChildren(true)
		sm = _sm
        cm = _cm
        myDrawing = _drawing
	}

    DLComponent getMyDrawing() {
        return myDrawing
    }

/*	TreePath getPathToComponent(DLComponent component) {
		if(componentNodes.containsKey(component))
			return new TreePath(componentNodes[component].getPath() as Object[])
*/

	List<TreePath> getPathToComponent(DLComponent component) {
		def List<TreePath> paths

		if (componentNodes.containsKey(component)) {
			paths = []
			componentNodes[component].each { n ->
				paths << new TreePath(n.getPath() as Object[])
			}
		} else if( roomNodes.containsKey( component ) ) {
			paths = [new TreePath(roomNodes[component].getPath() as Object[])]
		}
		return paths
	}

	TreePath getPathToZone(DLComponent zone) {
		if (zoneNodes.containsKey(zone))
			return new TreePath(zoneNodes[zone].getPath() as Object[])
		return null
	}

	TreePath getPathToGroup(DLComponent group) {
		if (groupNodes.containsKey(group))
			return new TreePath(groupNodes[group].getPath() as Object[])
		return null
	}

	void componentAdded(DLComponent component) {}

	void componentRemoved(DLComponent component) {}

	void childAdded(DLComponent parent, DLComponent child) {
        //log.trace("zoneNodes:" + zoneNodes.size())
        //zoneNodes.each{c,node->
        //    log.trace("zone $c : $node")
        //}
        //log.trace("groupNodes:" + groupNodes.size())
        //groupNodes.each{c,node->
        //    log.trace("group $c : $node")
        //}

        // Ignore if the model is loading or this is not for our drawing.
        if( cm.hasActiveState( ModelState.INITIALIZING ) || ( myDrawing != parent.getDrawing() ) ) {
            return
        }
		try{
		this.respondingToEvent = true
        //log.trace("childAdded($parent, $child)  [MyDrawing: ${myDrawing?.name}]")

		// Don't show the orphanage unless you are in developer mode.
		if( UIDisplay.INSTANCE.getUserLevelId() < 5 ) {
			if( parent.getType() == ComponentType.ORPHANAGE || child.getType() == ComponentType.ORPHANAGE ) {
				return
			}
		}

        MutableTreeNode parentNode
		// Flatten nested rooms in this view so they are all at the root.
		if( parent.hasRole('drawing') || child.hasRole( ComponentRole.ROOM ) ) {
			parentNode = treeRoot
		} else if ( parent.hasRole( ComponentRole.ROOM ) ) {
			parentNode = roomNodes[parent]
		} else if (parent.hasRole('zone')){
			parentNode = zoneNodes[parent]
		} else if( parent.hasRole( ComponentRole.LOGICAL_GROUP ) ) {
			parentNode = groupNodes[parent]
		}else{
			return
		}

		ComponentTreeNode childNode

		if( child.hasRole( ComponentRole.ROOM ) ) {
			childNode = new ComponentTreeNode( child, true )
			roomNodes[child] = childNode

		} else if (child.hasRole('zone')) {
			childNode = new ComponentTreeNode(child, true)
			zoneNodes[child] = childNode

		} else if( child.hasRole( ComponentRole.LOGICAL_GROUP ) ) {
			childNode = new ComponentTreeNode(child, true)
			groupNodes[child] = childNode

		} else if (child.hasRole('placeable')) {
			childNode = new ComponentTreeNode(child, false)
			if (!componentNodes.containsKey(child)) {
				componentNodes[child] = []
			}
			componentNodes[child] += childNode

		} else {
			log.trace("child node not '${ComponentRole.ROOM}' 'zone' or '${ComponentRole.LOGICAL_GROUP}' or 'placeable'")
			return
		}

        //log.trace("childNode: $childNode , parentNode: $parentNode")
        if(childNode==null || parentNode==null){
            return
        }

		insertNodeInto(childNode, parentNode, findInsertLocation(parentNode, childNode))
		child.addPropertyChangeListener(this, this.&componentPropertyChanged)
		} finally{
			this.respondingToEvent = false
		}
	}

	void childRemoved(DLComponent parent, DLComponent child) {
		//log.trace("childRemoved($parent, $child)")

        // Ignore if this is not for our drawing.
        if( myDrawing != parent.getDrawing() ) {
            return
        }

		this.respondingToEvent = true
		try{
		if( child.hasRole( ComponentRole.ROOM ) ) {
			ComponentTreeNode node = roomNodes[child]
			if( node == null ) {
				log.error("Room tree node lookup failed for child $child with parent $parent")
				return
			}
			removeNodeFromParent( node )
			roomNodes.remove( child )
			child.removePropertyChangeListener( this )

		} else if( parent.hasRole( ComponentRole.DRAWING ) && child.hasRole( ComponentRole.ZONE ) ) {
			ComponentTreeNode node = zoneNodes[child]
			if (node == null) {
				log.error("Zone tree node lookup failed for child $child with parent $parent")
				return
			}
			removeNodeFromParent(node)
			zoneNodes.remove(child)
			child.removePropertyChangeListener(this)

		} else if( parent.hasRole( ComponentRole.DRAWING ) && child.hasRole( ComponentRole.LOGICAL_GROUP ) ) {

			ComponentTreeNode node = groupNodes[child]
			if (node == null) {
				log.error("Group tree node lookup failed for child $child with parent $parent")
				return
			}
			removeNodeFromParent(node)
			groupNodes.remove(child)
			child.removePropertyChangeListener(this)

		} else if( child.hasRole( ComponentRole.PLACEABLE ) && (parent.hasRole( ComponentRole.ROOM ) || parent.hasRole( ComponentRole.LOGICAL_GROUP )) ) {
			// special problems - we need to make sure we only remove it from the group itself, not all places
			def nodesToRemove = []
			componentNodes[child].each { node ->
				if( node == null ) {
					log.error("Component tree node lookup failed for child $child with parent $parent")
					return
				}

				// for each node, check to see if the parent is the group we're talking about
				// if so, remove it from the tree and componentNodes
				if( node.getParent()?.getUserObject() == parent ) {
					removeNodeFromParent( node )
					//componentNodes[child].remove(node)
					nodesToRemove += node
				}

				child.removePropertyChangeListener(this)
			}

			nodesToRemove.each { node -> componentNodes[child].remove(node) }

			// if there are no more nodes for this group component, remove the component
			if( componentNodes[child]?.isEmpty() ) {
				componentNodes.remove(child)
			}
		} else if (child.hasRole('placeable') && (parent.hasRole('zone') || parent.hasRole('drawing'))) {
			//ComponentTreeNode node = componentNodes[child]
			componentNodes[child].each { node ->
				if (node == null) {
					log.error("Component tree node lookup failed for child $child with parent $parent")
					return
				}
				removeNodeFromParent(node)
				componentNodes.remove(child)
				child.removePropertyChangeListener(this)
			}
		}
		} finally{
			this.respondingToEvent = false
		}
	}

	int findInsertLocation(TreeNode parent, TreeNode child) {
		if (parent.getChildCount() == 0) {
			return 0;
		}
		int i;
		for (i = 0; i < parent.getChildCount(); i++) {
			if (parent.getChildAt(i) >= child) {
				return i
			}
		}

		return i;
	}

	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

    @Override
    public void modelMetamorphosisStarting( MetamorphosisEvent event ) {}

    @Override
    public void modelMetamorphosisFinished( MetamorphosisEvent event ) {}


	void componentPropertyChanged(def who, String prop, oldVal, newVal) {
//		println "--tree componentPropertyChanged($who, $prop, $oldVal, $newVal)"
		if (prop == 'name' || prop == 'parent_name' || prop == 'notes') {
			TreeNode node
			TreeNode parent

			if( who.hasRole( ComponentRole.ROOM ) ) {
				// Check rooms before 'placeable' since rooms are also placeable.
				node = roomNodes[who]
				parent = node.getParent()
				removeNodeFromParent(node)
				insertNodeInto(node, parent, findInsertLocation(parent, node))
				nodeStructureChanged(parent)

			} else if (who.hasClass('placeable')) {
				inTheMiddleOfARename = true
				//node = componentNodes[who]

				def sel = sm.getBaseSelection()
//				println "base selection is $sel"
				//componentNodes[who].each { n ->
				for(def n : componentNodes[who]){
//					println "--iterate!"
					parent = n.getParent()
//					println "--remove node from Parent"
					removeNodeFromParent(n)
//					println "--insert node"
					insertNodeInto(n, parent, findInsertLocation(parent, n))
//					println "--nodeStructureChanged()"
					nodeStructureChanged(parent)
//					println "--returning base selection"
				}
				sm.setSelection(sel)
				inTheMiddleOfARename = false

			} else if (who.hasClass('zone')) {
				node = zoneNodes[who]
				parent = node.getParent()
				removeNodeFromParent(node)
				insertNodeInto(node, parent, findInsertLocation(parent, node))
				nodeStructureChanged(parent)

			} else if( who.hasRole( ComponentRole.LOGICAL_GROUP ) ) {
				node = groupNodes[who]
				parent = node.getParent()
				removeNodeFromParent( node )
				insertNodeInto( node, parent, findInsertLocation(parent, node) )
				nodeStructureChanged( parent )

			} else {
                log.trace("Unexpected listener event: componentPropertyChanged($who, $prop, $oldVal, $newVal)")
			}
		}
	}
}

class StructureTreeTransferHandler extends TransferHandler {

	private static final Logger log = Logger.getLogger(StructureTreeTransferHandler.class.getName())

	private DataFlavor flavor

	private ComponentModel componentModel
	private UndoBuffer undoBuffer

	StructureTreeTransferHandler(ComponentModel model, UndoBuffer ub) {
		componentModel = model
		undoBuffer = ub
	}

	int getSourceActions(JComponent c) {
		StructureTree tree = (StructureTree) c
		return TransferHandler.MOVE
	}

	Transferable createTransferable(JComponent c) {
		Transferable xfer = new DataHandler(c.getSelectionPaths().collect {it.getLastPathComponent()}, DataFlavor.javaJVMLocalObjectMimeType)
		flavor = xfer.getTransferDataFlavors()[0]
		return xfer
	}

	void exportDone(JComponent c, Transferable t, int action) {
		c.getModel().nodeStructureChanged(c.getModel().getRoot())
		c.expandAll()
	}

	boolean canImport(TransferHandler.TransferSupport support) {
		if (!support.isDrop())
			return false

		def nodes = support.getTransferable().getTransferData(flavor)

		JTree.DropLocation dropLocation = support.getDropLocation()
		def dropNode = dropLocation.getPath().getLastPathComponent()

		return nodes.every { node ->
			if (dropNode.isNodeAncestor(node))
				return false

            if (dropNode.toString() == StructureTreeModel.ROOT_NODE_STRING)
				return false

			// Not allowed to move items within rooms
			if ("room".equalsIgnoreCase(dropNode.getUserObject().type))
				return false

			return dropNode.getUserObject().canChild(node.getUserObject())
		}
	}

	boolean importData(TransferHandler.TransferSupport support) {
		if (!support.isDrop())
			return false

		try {
			undoBuffer.startOperation()

			def nodes = support.getTransferable().getTransferData(flavor)

			JTree.DropLocation dropLocation = support.getDropLocation()
			def dropNode = dropLocation.getPath().getLastPathComponent()
			def dropObject = dropNode.getUserObject()

			nodes.each { node ->
				def nodeObject = node.getUserObject()

				if (nodeObject.hasClass('placeable') && dropObject.hasClass('zone')) {
					DLComponent oldParent = node.getParent().getUserObject()
					if (oldParent.hasClass('logicalgroup') && dropObject.hasClass('logicalgroup')) {
						oldParent.removeChild(nodeObject)
					}

					dropObject.addChild(nodeObject)
				}
			}

			return true
		} catch (Exception e) {
			log.error("Error dropping tree structure node in a new place:\n" + e.getMessage())
			log.error(log.getStackTrace(e))
			undoBuffer.rollbackOperation()
		} finally {
			undoBuffer.finishOperation()
		}
		return false
	}
}

/**
 *
 * @author Ken Scoggins
 * @since Jupiter
 */
public class StructureTreeController implements SelectionChangeListener, ModelChangeListener {

    private HashMap<DLComponent,StructureTreeModel> models = new HashMap<>()
    SelectModel selectModel
    private ComponentModel componentModel
    private JTree structureTree

    public StructureTreeController( SelectModel selectModel, ComponentModel componentModel, JTree structureTree ) {
        this.selectModel = selectModel
        this.componentModel = componentModel
        this.structureTree = structureTree

        // We need a blank model for when no drawing is selected.
        StructureTreeModel newModel = new StructureTreeModel( selectModel, componentModel, null )
        models.put( null, newModel )
        componentModel.addModelChangeListener( newModel )
        structureTree.setModel( newModel )
    }

    public StructureTreeModel getModel( DLComponent drawing ) {
        return models.get( drawing )
    }

    @Override
    void componentAdded( DLComponent component ) {
        // Create the drawing model on-demand in the activeDrawingChanged instead of here.
    }

    @Override
    void componentRemoved( DLComponent component ) {
        if( component.hasRole('drawing') ) {
            def removedModel = models.remove( component )
            componentModel.removeModelChangeListener( removedModel )
        }
    }

    @Override
    void activeDrawingChanged( DLComponent oldDrawing, DLComponent newDrawing) {
        // Ignore during a model load.
        if( componentModel.hasActiveState( ModelState.INITIALIZING ) ) return

        // See if we already have this model.
        StructureTreeModel model = models.get( newDrawing )

        if( model == null ) {
            // Need to create a new one and initialize it using the contents of the component model.
            model = new StructureTreeModel( selectModel, componentModel, newDrawing )
            models.put( newDrawing, model )
            componentModel.addModelChangeListener( model )

            // structure tree should have all containers and drawing direct child components except network role components
            def structureTreeChildren = newDrawing.getChildComponents().findAll{it->!it.hasRole("network")}
            addDecendents( model, newDrawing, structureTreeChildren )
        }

        structureTree.setModel( model )
    }

    private void addDecendents( StructureTreeModel model, DLComponent parent, List<DLComponent> children ) {
        for( DLComponent child : children ) {
            model.childAdded( parent, child )
            addDecendents( model, child, child.getChildComponents() )
        }
    }

	private void resetAndRebuild(){
		//clear all models
		models.values().each{ componentModel.removeModelChangeListener(it) }
		models = new HashMap<DLComponent, StructureTreeModel>()
		StructureTreeModel newModel = new StructureTreeModel( selectModel, componentModel, null )
		models.put( null, newModel )
	}

    @Override
    void childAdded(DLComponent parent, DLComponent child) {}

    @Override
    void childRemoved(DLComponent parent, DLComponent child) {}

    @Override
    void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

    @Override
    void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

    @Override
    public void modelMetamorphosisStarting( MetamorphosisEvent event ) {}

    @Override
    public void modelMetamorphosisFinished( MetamorphosisEvent event ) {
		if(event.getMode() == ModelState.INITIALIZING){
			resetAndRebuild()
		}

	}

    @Override
    void selectionChanged(SelectionChangeEvent e) {}
}
