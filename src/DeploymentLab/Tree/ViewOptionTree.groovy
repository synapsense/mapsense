package DeploymentLab.Tree

import javax.swing.tree.DefaultMutableTreeNode
import javax.swing.tree.DefaultTreeCellRenderer
import javax.swing.tree.DefaultTreeModel
import javax.swing.tree.TreePath
import javax.swing.JTree
import javax.swing.tree.TreeCellRenderer
import javax.swing.JCheckBox
import java.awt.Component
import java.awt.event.ItemEvent
import java.awt.event.ItemListener
import javax.swing.AbstractCellEditor
import javax.swing.tree.TreeCellEditor
import DeploymentLab.channellogger.*
import javax.swing.event.ChangeEvent
import java.awt.event.MouseEvent
import java.awt.Color
import DeploymentLab.Model.*
import javax.swing.tree.TreeNode

class ViewOptionTree extends JTree{
	private static final Logger log = Logger.getLogger(ViewOptionTree.class.getName())
	private ViewOptionTreeModel viewOptionTreeModel
	private CheckBoxNodeRenderer renderer 
	ViewOptionTree(ViewOptionTreeModel vtm){
		super()
		this.setModel(vtm)
		viewOptionTreeModel = vtm
		renderer = new CheckBoxNodeRenderer();
	    this.setCellRenderer(renderer);
	    this.expandRow(0)
		this.setCellEditor(new CheckBoxNodeEditor(this))
	    this.setEditable(true)
	}
	
	def getSelectedNodes(){
		def selectedNodes = []
		viewOptionTreeModel.configNodes.each{k,v->
			Object userObject = ((DefaultMutableTreeNode)v).getUserObject();
			def isCheckBoxNode = userObject instanceof CheckBoxNode
			CheckBoxNode node = (CheckBoxNode) userObject;
			if(node.isSelected()){
				selectedNodes.add(k)
			}
		}
		return selectedNodes
	}

	/**
	 * Toggles the display filter for a specific component type.  By "toggle", we mean to flip to the opposite display state.
	 * @param typeName the component typename to toggle.
	 */
	void toggleNode( String typeName ){
		//k = typename
		//v is the checkbox
		viewOptionTreeModel.configNodes.each{k,v->
			if ( k.equals(typeName)){
			    Object userObject = ((DefaultMutableTreeNode)v).getUserObject();
				CheckBoxNode node = (CheckBoxNode) userObject;
				node.setSelected( ! node.isSelected() )
			}
		}
	}


	void toggleNode( String typeName, boolean setToThis  ){
		//k = typename
		//v is the checkbox
		viewOptionTreeModel.configNodes.each{k,v->
			if ( k.equals(typeName)){
			    Object userObject = ((DefaultMutableTreeNode)v).getUserObject();
				CheckBoxNode node = (CheckBoxNode) userObject;
				node.setSelected( setToThis )
			}
		}
	}


	def getUnselectedNodes(){
		def unselectedNodes = []
		viewOptionTreeModel.configNodes.each{k,v->
			Object userObject = ((DefaultMutableTreeNode)v).getUserObject();
			def isCheckBoxNode = userObject instanceof CheckBoxNode
			CheckBoxNode node = (CheckBoxNode) userObject;
			
			if(!node.isSelected()){
				unselectedNodes.add(k)
			}
		}
		return unselectedNodes
	}
	
	void toggleNodesSelection(boolean isSelected){
		viewOptionTreeModel.configNodes.each{k,v->
			Object userObject = ((DefaultMutableTreeNode)v).getUserObject();
			def isCheckBoxNode = userObject instanceof CheckBoxNode
			if(isCheckBoxNode){
				CheckBoxNode node = (CheckBoxNode) userObject;
				node.setSelected(isSelected)
				
			}
		}
		viewOptionTreeModel.reload()
		expandAll()
	}
	
	void toggleGroup(String groupName, boolean isSelected){
		expandAll()
		int row = 0
		while (row < this.getRowCount()) {
			String eachPath =  this.getPathForRow(row).toString()
			Object userObject = this.getPathForRow(row).getLastPathComponent().getUserObject()
			def isCheckBoxNode = userObject instanceof CheckBoxNode
			boolean isGroup = false
			if(eachPath.find(groupName)!=null){
				isGroup = eachPath.find(groupName)
			}
			if(isCheckBoxNode){
				CheckBoxNode node = (CheckBoxNode) userObject;
					if(isGroup)
						node.setSelected(isSelected)
					else
						node.setSelected(false)
			}
			row++
		}
	}
	
	void multipleToggleGroup(String groupName, boolean isSelected){
		expandAll()
		int row = 0
		while (row < this.getRowCount()) {
			String eachPath =  this.getPathForRow(row).toString()
			Object userObject = this.getPathForRow(row).getLastPathComponent().getUserObject()
			def isCheckBoxNode = userObject instanceof CheckBoxNode
			boolean isGroup = false
			if(eachPath.find(groupName)!=null){
				isGroup = eachPath.find(", $groupName,")
			}
			if(isCheckBoxNode && isGroup){
				CheckBoxNode node = (CheckBoxNode) userObject;
				node.setSelected(isSelected)
			}
			row++
		}
	}
	
	public void expandAll() {
		int row = 0
		while (row < this.getRowCount()) {
			this.expandRow(row)
			row++
		}
	}


	boolean hasType(String typeName){
		//k = typename
		//v is the checkbox
		boolean result = false
		for( String k : viewOptionTreeModel.configNodes.keySet()  ){
			if ( k.equals(typeName)){
				result = true
			}
		}
		return result
	}

	/**
	 * Reset all nodes to visible
	 */
	public void reset(){
		//k = typename
		//v is the checkbox
		viewOptionTreeModel.configNodes.each{k,v->
			Object userObject = ((DefaultMutableTreeNode)v).getUserObject();
			CheckBoxNode node = (CheckBoxNode) userObject;
			node.setSelected(true)
		}
	}
}

class ViewOptionTreeModel extends ConfigurationTreeModel {
	private static final Logger log = Logger.getLogger(ViewOptionTreeModel.class.getName())
	//protected ConfigurationGroup treeRoot
	private ComponentModel componentModel
	private String modelType
	protected Map<String,DefaultMutableTreeNode> configNodes = [:]
	
	ViewOptionTreeModel(ComponentModel cm, String TreeName, String type) {
		super()
		componentModel = cm
		modelType = type
	}

	void loadGroup(ConfigurationGroup parent, String groupPath, String configuration, def classes, String displayName){
		boolean isHalo = classes.contains('haloed')
		
		if(modelType.equals("configuration") || (modelType.equals("halo") && isHalo)){
			super.loadGroup(parent, groupPath, configuration, classes, displayName)
		}
	}

	void AddCustomConfigurationNode(ConfigurationGroup groupTreeNode, String configuration, String displayName){
		CheckBoxNode configChkNode = new CheckBoxNode(displayName, true)
		DefaultMutableTreeNode configTreeNode = new DefaultMutableTreeNode(configChkNode)
		insertNodeInto(configTreeNode, groupTreeNode, groupTreeNode.getChildCount())
		configNodes[configuration] = configTreeNode
	}

	void pruneConfigurations() {
		configNodes.each{ ctype, node ->
			// Get rid of it if it isn't a valid loaded type and it's not currently in use.
			if( !componentModel.isLoadedComponentType( ctype ) && componentModel.getComponentsByType( ctype ).isEmpty() ) {
				log.debug("Pruning extinct configuration: $ctype")

				// Build the group path, skipping the root and ignoring ourself, the last node.
				String groupPath = ""
				TreeNode[] path = node.getPath()
				for( int i = 1; i < path.length-1; i++ ) {
					groupPath += "${path[i]};"
				}

				if( node.getParent() != null ) {
					removeNodeFromParent( node )
				}
				unloadGroup( treeRoot, groupPath, ctype )
			}
		}
	}


	boolean isLoadedConfiguration(String groupPath, String configurationName){
		super.isLoadedConfiguration(groupPath, configurationName)
	}
}

class CheckBoxNodeRenderer implements TreeCellRenderer {
	private JCheckBox leafRenderer = new JCheckBox();
	private DefaultTreeCellRenderer nonLeafRenderer = new DefaultTreeCellRenderer();
	
	protected JCheckBox getLeafRenderer() {
		return leafRenderer;
	}

	public CheckBoxNodeRenderer() {
		super()
	}

	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		Component returnValue;
		if (leaf) {
			String stringValue = tree.convertValueToText(value, selected, expanded, leaf, row, false)
				leafRenderer.setText(stringValue)
				leafRenderer.setSelected(true)
				leafRenderer.setEnabled(tree.isEnabled())
				leafRenderer.setBackground(Color.WHITE)

				if ((value != null) && (value instanceof DefaultMutableTreeNode)) {
					Object userObject = ((DefaultMutableTreeNode) value).getUserObject();
					if (userObject instanceof CheckBoxNode) {
						CheckBoxNode node = (CheckBoxNode) userObject;
						leafRenderer.setText(node.getText());
						leafRenderer.setSelected(node.isSelected());
					}
				}
			returnValue = leafRenderer;
		} else {
			returnValue = nonLeafRenderer.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
		}
		return returnValue;
	}
}

class CheckBoxNodeEditor extends AbstractCellEditor implements TreeCellEditor {
	CheckBoxNodeRenderer renderer = new CheckBoxNodeRenderer();
	ChangeEvent changeEvent = null;
	JTree tree;

	public CheckBoxNodeEditor(JTree tree) {
		this.tree = tree;
	}

	public Object getCellEditorValue() {
		JCheckBox checkbox = renderer.getLeafRenderer();
		CheckBoxNode checkBoxNode = new CheckBoxNode(checkbox.getText(),checkbox.isSelected());
		return checkBoxNode;
	}

	public boolean isCellEditable(EventObject event) {
		boolean returnValue = false;
		if (event instanceof MouseEvent) {
			MouseEvent mouseEvent = (MouseEvent) event;
			TreePath path = tree.getPathForLocation(mouseEvent.getX(), mouseEvent.getY());
			if (path != null) {
				Object node = path.getLastPathComponent();
				if ((node != null) && (node instanceof DefaultMutableTreeNode)) {
					DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) node;
					Object userObject = treeNode.getUserObject();
					returnValue = ((treeNode.isLeaf()) && (userObject instanceof CheckBoxNode));
				}
			}
		}
		return returnValue;
	}

	public Component getTreeCellEditorComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row) {
		Component editor = renderer.getTreeCellRendererComponent(tree, value, true, expanded, leaf, row, true);

		OptionItemListener optionItemListener = new OptionItemListener(this)
		if (editor instanceof JCheckBox) {
			((JCheckBox) editor).addItemListener(optionItemListener);
		}

		return editor;
	}
}

class OptionItemListener extends AbstractCellEditor implements ItemListener {
	CheckBoxNodeEditor checkBoxNodeEditor;
	OptionItemListener(CheckBoxNodeEditor cne){
		checkBoxNodeEditor = cne;
	}
	public void itemStateChanged(ItemEvent itemEvent) {
		if (checkBoxNodeEditor.stopCellEditing()) {
			fireEditingStopped();
		}
	}
	public Object getCellEditorValue() {
		// TODO Auto-generated method stub
		return null;
	}
}

class CheckBoxNode {
	String text;

	boolean selected;

	public CheckBoxNode(String text, boolean selected) {
		this.text = text;
		this.selected = selected;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean newValue) {
		selected = newValue;
	}

	public String getText() {
		return text;
	}

	public void setText(String newValue) {
		text = newValue;
	}

	public String toString() {
		return getClass().getName() + "[" + text + "/" + selected + "]";
	}
}


