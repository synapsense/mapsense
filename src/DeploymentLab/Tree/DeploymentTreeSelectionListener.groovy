
package DeploymentLab.Tree

import javax.swing.JTree
import javax.swing.event.TreeSelectionEvent
import javax.swing.event.TreeSelectionListener
import javax.swing.tree.TreePath

import DeploymentLab.SelectModel
import DeploymentLab.Model.*

public class DeploymentTreeSelectionListener implements TreeSelectionListener {

	private SelectModel selectModel

	DeploymentTreeSelectionListener(SelectModel sm) {
		selectModel = sm
	}

	void valueChanged(TreeSelectionEvent e) {
/*
		//This way issues deltas, but can cause some REALLY weird behavior
		List<DLComponent> addedSelection = new ArrayList<DLComponent>()
		List<DLComponent> removedSelection = new ArrayList<DLComponent>()
		for ( TreePath path : e.getPaths() ){
			if ( path.getLastPathComponent().getUserObject() instanceof DLComponent ){
				//println "it's a component, let's do THIS ${ path.getLastPathComponent().getUserObject()}"
				if( e.isAddedPath(path)){
					addedSelection.add( path.getLastPathComponent().getUserObject() )
				} else {
					removedSelection.add( path.getLastPathComponent().getUserObject() )
				}
			}
		}
		//selectModel.addToSelection( addedSelection )
		//selectModel.removeFromSelection( removedSelection )
  */
		JTree tree = (JTree)e.getSource()

		if( tree.getModel().inTheMiddleOfARename ){
			return
		}
		
		List<DLComponent> treeSelection = []
		for( TreePath path : tree.getSelectionPaths()){
			if ( path.getLastPathComponent().getUserObject() instanceof DLComponent ){
				treeSelection.add(path.getLastPathComponent().getUserObject())
			}
		}

		//we want to set the selection if the user clicked the tree, but not if we're responding to an external event
		if(!tree.getModel().respondingToEvent){
			//println "setting treeSelection"
			selectModel.setSelection(treeSelection)
		}
	}
}
