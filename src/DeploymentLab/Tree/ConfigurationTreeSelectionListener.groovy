package DeploymentLab.Tree

import javax.swing.event.TreeSelectionEvent
import javax.swing.event.TreeSelectionListener
import javax.swing.tree.TreePath

import DeploymentLab.SceneGraph.ConfigurationPanel

public class ConfigurationTreeSelectionListener implements TreeSelectionListener {

	private ConfigurationPanel configPanel
	private ConfigurationTreeModel configTreeModel

	ConfigurationTreeSelectionListener(ConfigurationPanel panel, ConfigurationTreeModel ctm) {
		configPanel = panel
		configTreeModel = ctm
	}

	void valueChanged(TreeSelectionEvent e) {
		TreePath path = e.getPath()
 		if(path != null)
			configPanel.setDisplayed(configTreeModel.getConfigurationFromPath(path))
	}
}

