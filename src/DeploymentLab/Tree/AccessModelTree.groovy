package DeploymentLab.Tree

import DeploymentLab.Model.AccessModel
import DeploymentLab.Model.DLObject
import DeploymentLab.Model.ObjectType
import DeploymentLab.UIDisplay
import DeploymentLab.channellogger.Logger

import javax.swing.*
import javax.swing.tree.DefaultMutableTreeNode
import javax.swing.tree.DefaultTreeModel
import javax.swing.tree.TreeModel
import javax.swing.tree.TreePath

/**
 * @author Vadim Gusev
 * @since Europa
 * Date: 17.04.13
 * Time: 12:57
 */
class AccessModelTree extends JTree{
	protected UIDisplay uiDisplay
	private static final Logger log = Logger.getLogger(AccessModelTree.class.getName())

	AccessModelTree(def top) {
		super(top)
		setEditable(false)
		setShowsRootHandles(true)
		this.setCellRenderer( new AccessModelTreeCellRenderer() )
		expandAll()
	}

	void expandAll(DefaultMutableTreeNode r = null) {
		DefaultMutableTreeNode root
		if(r == null)
			root = getModel().getRoot()
		else
			root = r

		expandPath(new TreePath(root.getPath() as Object[]))
		root.children().each{ child -> expandAll(child) }
	}

	public void setModel(TreeModel model) {
		super.setModel(model)
		expandAll()
	}

	public List<DLObject> getSelectedNodes(){
		def nodes = []
		def paths = getSelectionPaths()
		for(def path : paths){
			DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) path.getLastPathComponent();
			Object userObject = selectedNode.getUserObject();
			if (userObject){
				if (((DLObject)userObject).getType().equals("wsnnode")){
					nodes.add(userObject);
				}
			}
		}
		return nodes
	}

	public DLObject getSelectedNode(){
		TreePath path = getSelectionPath()
		if (path){
			DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) path.getLastPathComponent();
			Object userObject = selectedNode.getUserObject();
			if (userObject){
				if (((DLObject)userObject).getType().equals("wsnnode")){
					return (DLObject) userObject;
				}
			}
		}
		return null
	}

	public DLObject getSelectedNet(){
		TreePath path = getSelectionPath()
		if (path) {
			DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) path.getLastPathComponent();
			Object userObject = selectedNode.getUserObject();
			if (userObject){
				if (((DLObject)userObject).getType().equals("wsnnetwork")){
					return (DLObject) userObject;
				}
			}
		}
		return null
	}
}

class AccessModelTreeModel extends DefaultTreeModel{
	private static final Logger log = Logger.getLogger(AccessModelTreeModel.class.getName())
	private static final String DEFAULT_ROOT_NODE = "Drawings"

	protected networkNodes = [:]
	protected nodeNodes = [:]
	protected drawingNodes = [:]
	protected DefaultMutableTreeNode treeRoot

	AccessModelTreeModel(AccessModel amodel, DLObject drawingRoot = null) {
		super(new DefaultMutableTreeNode((drawingRoot)?:DEFAULT_ROOT_NODE))
		treeRoot = getRoot()

		if (!drawingRoot){
			amodel.getDrawings().values().each {drawing ->
				log.trace("Add drawing to tree $drawing")
				DefaultMutableTreeNode n = new DefaultMutableTreeNode(drawing)
				treeRoot.add(n)
				drawingNodes.put(drawing.dlid, n)
			}
		}
		amodel.getNetworks().values().each { network ->
			log.trace("Add network to tree $network")
			DefaultMutableTreeNode n = new DefaultMutableTreeNode(network)
			DLObject parent = network.getParents().find {parent ->
				if (parent){
					parent.getType().equals( ObjectType.DRAWING )
				}
			}
			if (parent && drawingNodes.get(parent.dlid)) {
				log.trace("$parent")
				drawingNodes.get(parent.dlid)?.add(n)
			}else{
				treeRoot.add(n)
			}
			networkNodes.put(network.dlid, n)
		}
		amodel.getNodes().values().each {  node ->
			log.trace("Add node to tree $node")
			DefaultMutableTreeNode n = new DefaultMutableTreeNode(node);
			DLObject parent = node.getParents().find {parent ->
				if (parent){
					parent.getType().equals("wsnnetwork")
				}
			}
			if (parent) {
				log.trace("$parent")
				networkNodes.get(parent.dlid)?.add(n)
			}else{
				treeRoot.add(n)
			}
			nodeNodes.put(node.dlid, n)
		}
	}
}
