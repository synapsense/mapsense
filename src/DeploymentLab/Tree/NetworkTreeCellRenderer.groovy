package DeploymentLab.Tree

import DeploymentLab.Model.ComponentProp
import DeploymentLab.Model.DLComponent

import javax.swing.JTree
import java.awt.Component


/**
 * Custom renderer for the Network Tree .
 *
 * @author Ken Scoggins
 * @since Aireo
 */
public class NetworkTreeCellRenderer extends MapSenseTreeCellRenderer {
	//private static final Logger log = Logger.getLogger(NetworkTreeCellRenderer.class.getName())

	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean bSelected, boolean bExpanded, boolean bLeaf, int nRow, boolean bFocus) {
		super.getTreeCellRendererComponent(tree, value, bSelected, bExpanded, bLeaf, nRow, bFocus);

		annotateText( value )

		return this;
	}

	private void annotateText( ComponentTreeNode node ) {
		DLComponent c = node.getComponent()
		if( c.hasProperty( ComponentProp.WSN_OVERRIDE ) ) {
			if( c.getPropertyValue( ComponentProp.WSN_OVERRIDE ) ) {
				// Slap a "(M)" to the name so the user knows this node was manually assigned to the network.
				StringBuilder sb = new StringBuilder()
				sb.append("<html><font color='blue'><bold>(M)</bold></font> ")
				sb.append( node.toString().trim() )
				sb.append("</html>")
				this.setText( sb.toString() )

				// And have a tooltip to explain what the M means.
				this.setToolTipText("<html><font color='blue'><bold>(M)</bold></font> Manually assigned to this network.")

				return
			}
		}

		this.setToolTipText( null )
	}

	private void annotateText( Object obj ) {
		this.setToolTipText( null )
	}
}
