package DeploymentLab.Tree

import javax.swing.ToolTipManager
import javax.swing.tree.DefaultMutableTreeNode

import javax.swing.tree.DefaultTreeModel
import javax.swing.tree.TreePath
import javax.swing.JTree

import javax.swing.tree.TreeNode

import DeploymentLab.SelectModel
import DeploymentLab.SelectionChangeEvent
import DeploymentLab.SelectionChangeListener
import DeploymentLab.Model.*
import DeploymentLab.channellogger.*
import DeploymentLab.UIDisplay
import DeploymentLab.UndoBuffer
import javax.swing.DropMode

class NetworkTree extends JTree implements SelectionChangeListener {
	protected UIDisplay uiDisplay
	private static final Logger log = Logger.getLogger(NetworkTree.class.getName())

	NetworkTree(ComponentModel cmodel, UndoBuffer ub, UIDisplay ud) {
		super()
		uiDisplay = ud
		setEditable(false)
		setShowsRootHandles(true)
		this.setCellRenderer( new NetworkTreeCellRenderer() )
		//support for DnD:
		setDragEnabled(true)
		setDropMode(DropMode.ON_OR_INSERT)
		setTransferHandler(new NetworkTreeTransferHandler(ub))

		// The tree may use tool tips.
		ToolTipManager.sharedInstance().registerComponent( this )
	}

	void expandAll(DefaultMutableTreeNode r = null) {
		DefaultMutableTreeNode root
		if(r == null)
			root = getModel().getRoot()
		else
			root = r

		expandPath(new TreePath(root.getPath() as Object[]))
		root.children().each{ child -> expandAll(child) }
	}


	void collapseBranch(DefaultMutableTreeNode r = null) {
		DefaultMutableTreeNode root
		if(r == null)
			root = getModel().getRoot()
		else
			root = r

		root.children().each{ child -> collapseAll(child) }
		collapsePath(new TreePath(root.getPath() as Object[]))
	}

	//collapse the "networks" level
	void collapseFirstLevel(){
		DefaultMutableTreeNode root = this.getModel().getRoot()
		//walk to the end, find a leaf
		def current = root
		while ( ! current.isLeaf() ){
			current = current.getFirstChild()
		}
		def parent = current.getParent().getParent()
		def count = parent.getChildCount()
		for(int i = 0; i < count; i++ ){
			parent.getChildAt(i)
			collapsePath(new TreePath(parent.getChildAt(i).getPath() as Object[]))
		}
	}


	void selectionChanged(SelectionChangeEvent e) {
        NetworkTreeModel networkModel = (NetworkTreeModel) getModel()

		removeSelectionPaths((e.componentsRemoved.collect{c -> networkModel.getPathToComponent(c)} + e.networksRemoved.collect{n -> networkModel.getPathToNetwork(n)}) as TreePath[])
		removeSelectionPaths(e.zonesRemoved.collect{z -> z.getChildComponents()}.flatten().collect{c -> networkModel.getPathToComponent(c)} as TreePath[])
        if( e.drawingsRemoved.contains( networkModel.getMyDrawing() ) ) {
            removeSelectionPath( new TreePath( networkModel.getRoot() ) )
        }

		addSelectionPaths((e.componentsAdded.collect{c -> networkModel.getPathToComponent(c)} + e.networksAdded.collect{n -> networkModel.getPathToNetwork(n)}) as TreePath[])
		addSelectionPaths(e.zonesAdded.collect{z -> z.getChildComponents()}.flatten().collect{c -> networkModel.getPathToComponent(c)} as TreePath[])
        if( e.drawingsAdded.contains( networkModel.getMyDrawing() ) ) {
            addSelectionPath( new TreePath( networkModel.getRoot() ) )
        }
	}

    void activeDrawingChanged( DLComponent oldDrawing, DLComponent newDrawing ) {
        // The NetworkTreeController manages this.
    }
}

class NetworkTreeModel extends DefaultTreeModel implements ModelChangeListener {

	private static final Logger log = Logger.getLogger(NetworkTreeModel.class.getName())

    public static final String ROOT_NODE_STRING = 'Data Sources'

	protected DefaultMutableTreeNode treeRoot

	// For efficient searching, map each type to it's TreeNode
	protected networkNodes = [:]	//Map network DLComponent --> new ComponentTreeNode
	protected componentNodes = [:]	//Map child DLComponent --> new ComponentTreeNode
	private SelectModel sm
    private ComponentModel cm
    private DLComponent myDrawing

	boolean inTheMiddleOfARename = false
	boolean respondingToEvent = false

	NetworkTreeModel(SelectModel _sm, ComponentModel _cm, DLComponent _drawing) {
        // If we have one, make the drawing the root so we don't have an unnecessary tree level and can still access the drawing properties.
        super( (_drawing == null) ? new DefaultMutableTreeNode( ROOT_NODE_STRING, true) : new ComponentTreeNode( _drawing, true ) )
		treeRoot = getRoot()
		setAsksAllowsChildren(true)
		sm = _sm
        cm = _cm
        myDrawing = _drawing
	}

    DLComponent getMyDrawing() {
        return myDrawing
    }

	DLComponent getNetworkFromPath(TreePath path) {
		if(path == null || path.getPathCount() < 2)
			return null
		return path.getPathComponent(1).getUserObject()
	}

	TreePath getPathToComponent(DLComponent component) {
		if(componentNodes.containsKey(component)) // mostly for Gateway objects
			return new TreePath(componentNodes[component].getPath() as Object[])
		return null
	}

	TreePath getPathToNetwork(DLComponent network) {
		if(networkNodes.containsKey(network))
			return new TreePath(networkNodes[network].getPath() as Object[])
		return null
	}

	DefaultMutableTreeNode getNodeForComponent( DLComponent c ){
		return ( componentNodes[c] )
	}

	void componentAdded(DLComponent network) {}

	void componentRemoved(DLComponent component) {}

	void childAdded(DLComponent parent, DLComponent child) {
        // Ignore if the model is loading or this is not for our drawing.
        if( cm.hasActiveState( ModelState.INITIALIZING ) || ( myDrawing != parent.getDrawing() ) ) {
            return
        }
		this.respondingToEvent = true
        //log.trace("childAdded($parent, $child)  [MyDrawing: ${myDrawing?.name}]")

        if(parent.hasRole('drawing') && child.hasRole('network')) {
			ComponentTreeNode newNode = new ComponentTreeNode(child, true)
			networkNodes[child] = newNode
			child.addPropertyChangeListener(this, this.&networkPropertyChanged)
			insertNodeInto(newNode, treeRoot, findInsertLocation(treeRoot, newNode))
			
		} else if(parent.hasRole('network') && child.hasRole('placeable')) {
			ComponentTreeNode netNode = networkNodes[parent]
			boolean canParent = false
			if (child.hasRole("network")){
				canParent = true
			}
			if(parent.hasRole("placeable")){
				netNode = componentNodes[parent]
			}

			ComponentTreeNode newNode = new ComponentTreeNode(child, canParent)
			componentNodes[child] = newNode
			child.addPropertyChangeListener(this, this.&componentPropertyChanged)
			insertNodeInto(newNode, netNode, findInsertLocation(netNode, newNode))
		}
		this.respondingToEvent = false
	}

	int findInsertLocation(TreeNode parent, TreeNode child) {
		if(parent.getChildCount() == 0)
			return 0;
		int i;
		for(i=0; i<parent.getChildCount(); i++) {
			if(parent.getChildAt(i) >= child) {
				return i
			}
		}
		return i;
	}

	void childRemoved(DLComponent parent, DLComponent child) {
		//log.trace("childRemoved($parent, $child)")

        // Ignore if this is not for our drawing.
        if( myDrawing != parent.getDrawing() ) {
            return
        }
		this.respondingToEvent = true

		if(parent.hasClass('drawing') && child.hasClass('network')) {
			if(networkNodes.containsKey(child)) {
				//log.debug("Removing network")
				def node = networkNodes[child]
				networkNodes.remove(child)
				child.removePropertyChangeListener(this)
				removeNodeFromParent(node)
			}
		} else if(parent.hasClass('network') && child.hasClass('placeable')) {
			if(componentNodes.containsKey(child)) {
				log.debug("Removing child node from network")
				ComponentTreeNode childNode = componentNodes[child]
				componentNodes.remove(child)
				child.removePropertyChangeListener(this)
				removeNodeFromParent(childNode)
			} else {
				log.warn("Got a childRemoved($parent, $child) event, but couldn't find child!")
			}
		}
		this.respondingToEvent = false
	}

	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}
	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

    @Override
    public void modelMetamorphosisStarting( MetamorphosisEvent event ) {}

    @Override
    public void modelMetamorphosisFinished( MetamorphosisEvent event ) {}


	void componentPropertyChanged(def who, String prop, oldVal, newVal) {

		if(prop == 'name') {
			inTheMiddleOfARename = true
			def sel = sm.getBaseSelection()
			TreeNode node = componentNodes[who]
			TreeNode parent = node.getParent()
			removeNodeFromParent(node)
			insertNodeInto(node, parent, findInsertLocation(parent, node))
			nodeStructureChanged(parent)
			sm.setSelection( sel )
			inTheMiddleOfARename = false
		} else if( prop == ComponentProp.WSN_OVERRIDE ) {
			nodeChanged( componentNodes[who] )
		}
	}

	void networkPropertyChanged(def who, String prop, oldVal, newVal) {
		if(prop == 'name') {
			nodeChanged(networkNodes[who])
		}
	}
}

/**
 *
 * @author Ken Scoggins
 * @since Jupiter
 */
public class NetworkTreeController implements SelectionChangeListener, ModelChangeListener {
	private static final Logger log = Logger.getLogger(NetworkTreeController.class.getName())
    private HashMap<DLComponent,NetworkTreeModel> models = new HashMap<>()
    SelectModel selectModel
    private ComponentModel componentModel
    private JTree networkTree

    public NetworkTreeController( SelectModel selectModel, ComponentModel componentModel, JTree networkTree ) {
        this.selectModel = selectModel
        this.componentModel = componentModel
        this.networkTree = networkTree

        // We need a blank model for when no drawing is selected.
        NetworkTreeModel newModel = new NetworkTreeModel( selectModel, componentModel, null )
        models.put( null, newModel )
        componentModel.addModelChangeListener( newModel )
        networkTree.setModel( newModel )
    }

    public NetworkTreeModel getModel( DLComponent drawing ) {
        return models.get( drawing )
    }

    @Override
    void componentAdded( DLComponent component ) {
        // Create the drawing model on-demand in the activeDrawingChanged instead of here.
    }

    @Override
    void componentRemoved( DLComponent component ) {
        if( component.hasRole('drawing') ) {
            def removedModel = models.remove( component )
            componentModel.removeModelChangeListener( removedModel )
        }
    }

    @Override
    void activeDrawingChanged( DLComponent oldDrawing, DLComponent newDrawing ) {
        // Ignore during a model load.
        if( componentModel.hasActiveState( ModelState.INITIALIZING ) ) return

        // See if we already have this model.
        NetworkTreeModel model = models.get( newDrawing )

        if( model == null ) {
            // Need to create a new one and initialize it using the contents of the component model.
			log.debug("Creating new Net Tree model for $newDrawing")
            model = new NetworkTreeModel( selectModel, componentModel, newDrawing )
            models.put( newDrawing, model )
            componentModel.addModelChangeListener( model )

            addDecendents( model, newDrawing, newDrawing.getChildrenOfRole('network') )
        }

        networkTree.setModel( model )
    }

    private void addDecendents( NetworkTreeModel model, DLComponent parent, List<DLComponent> children ) {
        for( DLComponent child : children ) {
            model.childAdded( parent, child )
            addDecendents( model, child, child.getChildComponents() )
        }
    }

	private void resetAndRebuild(){
		//clear all models
		models.values().each{ componentModel.removeModelChangeListener(it) }
		models = new HashMap<DLComponent, NetworkTreeModel>()
		NetworkTreeModel newModel = new NetworkTreeModel( selectModel, componentModel, null )
		models.put( null, newModel )
	}

    @Override
    void childAdded(DLComponent parent, DLComponent child) {}

    @Override
    void childRemoved(DLComponent parent, DLComponent child) {}

    @Override
    void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

    @Override
    void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

    @Override
    public void modelMetamorphosisStarting( MetamorphosisEvent event ) {}

    @Override
    public void modelMetamorphosisFinished( MetamorphosisEvent event ) {
		if(event.getMode() == ModelState.INITIALIZING){
			resetAndRebuild()
		}
	}

    @Override
    void selectionChanged(SelectionChangeEvent e) {}
}
