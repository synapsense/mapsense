package DeploymentLab.Tree

import DeploymentLab.Image.ComponentIconFactory
import DeploymentLab.Model.AccessModel
import DeploymentLab.Model.DLObject

import javax.swing.*
import javax.swing.tree.DefaultTreeCellRenderer
import java.awt.*

/**
 * @author Vadim Gusev
 * @since Europa
 * Date: 17.04.13
 * Time: 13:16
 */
class AccessModelTreeCellRenderer  extends DefaultTreeCellRenderer {

	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean bSelected, boolean bExpanded, boolean bLeaf, int nRow, boolean bFocus) {
		super.getTreeCellRendererComponent(tree, value, bSelected, bExpanded, bLeaf, nRow, bFocus)
		def o = value.getUserObject()
		if (o instanceof String){
			this.setIcon( ComponentIconFactory.getDefaultIcon() )
			this.setText(o)
		}else{
			if (!o.getType().equals("wsnnode")){
				this.setIcon( ComponentIconFactory.getComponentIcon(o.getAsName()) )
				this.setText("${o.getName()}")
			} else {
				if (o.hasKey()){
					if (o.deleted){
						this.setIcon(ComponentIconFactory.getIconByName("deleted_16"))
					}else{
						this.setIcon(ComponentIconFactory.getIconByName("exported_16"))
					}
				}else{
					this.setIcon(ComponentIconFactory.getIconByName("new_16"))
				}
				String platform = o.getObjectSetting("platformName").getValue()
				String platformId = o.getObjectSetting("platformId").getValue()
				String mac = AccessModel.convertLongToMac((long)o.getObjectSetting("mac").getValue())
				this.setText("$mac - $platform ($platformId)")
			}
		}
		return this;
	}
}
