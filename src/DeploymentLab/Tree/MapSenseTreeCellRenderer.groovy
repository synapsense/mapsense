package DeploymentLab.Tree

import java.awt.Component

import javax.swing.JTree
import javax.swing.tree.DefaultTreeCellRenderer

import DeploymentLab.channellogger.Logger
import DeploymentLab.Image.ComponentIconFactory


public class MapSenseTreeCellRenderer extends DefaultTreeCellRenderer {
	private static final Logger log = Logger.getLogger(MapSenseTreeCellRenderer.class.getName())

	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean bSelected, boolean bExpanded, boolean bLeaf, int nRow, boolean bFocus) {
		super.getTreeCellRendererComponent(tree, value, bSelected, bExpanded, bLeaf, nRow, bFocus);
		//for the moment, we don't want to try to get an icon for leaf nodes, since we don't have raster icons for leaves.
		//if ( ! bLeaf ) {
			def o = value.getUserObject()
			this.setIcon( ComponentIconFactory.getComponentIcon(o) )
		//}
		return this;
	}

}