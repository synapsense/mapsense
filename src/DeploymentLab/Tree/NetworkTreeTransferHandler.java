package DeploymentLab.Tree;

import DeploymentLab.CentralCatalogue;
import DeploymentLab.Model.ComponentModel;
import DeploymentLab.Model.DLComponent;
import DeploymentLab.UndoBuffer;
import DeploymentLab.channellogger.Logger;

import javax.activation.DataHandler;
import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


class NetworkTreeTransferHandler extends TransferHandler {

	private static final Logger log = Logger.getLogger(NetworkTreeTransferHandler.class.getName());

	private DataFlavor flavor;

	private UndoBuffer undoBuffer;

	public NetworkTreeTransferHandler(UndoBuffer ub) {
		undoBuffer = ub;
	}

	public int getSourceActions(JComponent c) {
		NetworkTree tree = (NetworkTree)c;
		return TransferHandler.MOVE;
	}

	public Transferable createTransferable(JComponent c) {
		NetworkTree nt =  (NetworkTree)c;
		List<ComponentTreeNode> toCopy = new ArrayList<ComponentTreeNode>();
		for ( TreePath path : nt.getSelectionPaths() ){
			toCopy.add( (ComponentTreeNode) path.getLastPathComponent() );
		}
		Transferable xfer = new DataHandler( toCopy, DataFlavor.javaJVMLocalObjectMimeType );
		flavor = xfer.getTransferDataFlavors()[0];
		return xfer;
	}

	public void exportDone(JComponent c, Transferable t, int action) {
		NetworkTree nt =  (NetworkTree)c;
		NetworkTreeModel ntm = (NetworkTreeModel)nt.getModel();
		ntm.nodeStructureChanged( (TreeNode)ntm.getRoot() );
		nt.expandAll(null);
	}

	@SuppressWarnings("unchecked")
	public boolean canImport(TransferHandler.TransferSupport support) {
		if(!support.isDrop())
			return false;

		boolean result = true;

		try {
			List<ComponentTreeNode> nodes = (List<ComponentTreeNode>) support.getTransferable().getTransferData(flavor);
			JTree.DropLocation dropLocation = (JTree.DropLocation) support.getDropLocation();
			DefaultMutableTreeNode dropNode = (DefaultMutableTreeNode) dropLocation.getPath().getLastPathComponent();
			for ( DefaultMutableTreeNode node : nodes ){
				if(dropNode.isNodeAncestor(node)) {
					return false;
				}
				if ( ! (dropNode.getUserObject() instanceof DLComponent) ){
					return false;
				}
                if (dropNode.toString().equals( NetworkTreeModel.ROOT_NODE_STRING )) {
					return false;
				}
				result = result & ((DLComponent)dropNode.getUserObject()).canChild( (DLComponent)node.getUserObject()  );
			}
		} catch (IOException ioe) {
			log.error(log.getStackTrace(ioe), "default");
		} catch (UnsupportedFlavorException ufe) {
			log.error(log.getStackTrace(ufe), "default");
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public boolean importData(TransferHandler.TransferSupport support) {
		if(!support.isDrop())
			return false;

		//an automatic call to canImport() with this support is not guaranteed before importData is called
		if (! canImport(support)){
			return false;
		}

		try {
			undoBuffer.startOperation();
			List<ComponentTreeNode> nodes = (List<ComponentTreeNode>) support.getTransferable().getTransferData(flavor);

			JTree.DropLocation dropLocation = (JTree.DropLocation) support.getDropLocation();
			DefaultMutableTreeNode dropNode = (DefaultMutableTreeNode) dropLocation.getPath().getLastPathComponent();
			DLComponent dropObject = (DLComponent) dropNode.getUserObject();

			for ( DefaultMutableTreeNode node : nodes ){
				DLComponent nodeObject = (DLComponent) node.getUserObject();
				if (nodeObject.hasRole("placeable") && dropObject.hasRole("network")) {
					DLComponent oldParent = (DLComponent) ( (DefaultMutableTreeNode) node.getParent() ).getUserObject();
					oldParent.removeChild(nodeObject);
					dropObject.addChild(nodeObject);
					if (nodeObject.hasProperty("manualWSNConfig") && !(Boolean)nodeObject.getPropertyValue("manualWSNConfig")){
						nodeObject.setPropertyValue("manualWSNConfig", true);
						log.info(CentralCatalogue.getUIS("changeParents.WSNToManual"), "statusbar");
					}
				}

			}

			return true;
		} catch(Exception e) {
			log.error("Error dropping tree structure node in a new place:\n" + e.getMessage(), "default");
			log.error(log.getStackTrace(e), "default");
			undoBuffer.rollbackOperation();
		} finally {
			undoBuffer.finishOperation();
		}
		return false;
	}
}
