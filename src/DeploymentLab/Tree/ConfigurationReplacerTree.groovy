package DeploymentLab.Tree

import DeploymentLab.channellogger.*
import DeploymentLab.Model.*

import java.awt.Color
import java.awt.Component
import java.awt.Font
import java.awt.event.ItemEvent
import java.awt.event.ItemListener
import javax.swing.event.ChangeEvent
import java.awt.event.MouseEvent
import javax.swing.AbstractCellEditor
import javax.swing.ButtonGroup
import javax.swing.JTree
import javax.swing.JRadioButton
import javax.swing.tree.DefaultMutableTreeNode
import javax.swing.tree.DefaultTreeCellRenderer
import javax.swing.tree.TreePath
import javax.swing.tree.TreeCellEditor
import javax.swing.tree.TreeCellRenderer
import javax.swing.tree.TreeNode

public class ConfigurationReplacerTree extends JTree {
	private static final Logger log = Logger.getLogger(ConfigurationReplacerTree.class.getName())
	private ConfigurationReplacerTreeModel configTreeModel
	private String selectedConfiguration
	
	ConfigurationReplacerTree(ConfigurationReplacerTreeModel treeModel){
		super(treeModel)
		configTreeModel = treeModel
		this.expandRow(0)
		this.setCellEditor(new RadioButtonNodeEditor(this))
		this.setEditable(true)
		this.setCellRenderer(new RadioButtonNodeRederer())
	}
	
	public void setConfiguration(String configuration){
		selectedConfiguration = configuration
	}
	
	public String getConfiguration(){
		return selectedConfiguration
	}
	
}

public class ConfigurationReplacerTreeModel extends ConfigurationTreeModel{
	private static final Logger log = Logger.getLogger(ConfigurationReplacerTreeModel.class.getName());
	private ComponentModel componentModel
	protected Map<String,DefaultMutableTreeNode> configNodes = [:]
	protected  ButtonGroup buttonGroup = new ButtonGroup()
	
	ConfigurationReplacerTreeModel(ComponentModel cm, String TreeName) {
		super()
		componentModel = cm
	}

	void loadGroup(ConfigurationGroup parent, String groupPath, String configuration, def classes, String displayName){
		super.loadGroup(parent, groupPath, configuration, classes, displayName)
	}
	
	void AddCustomConfigurationNode(ConfigurationGroup groupTreeNode, String configuration, String displayName){
		JRadioButton configNode = new JRadioButton(displayName)
		configNode.setName(configuration)
		buttonGroup.add(configNode)
		DefaultMutableTreeNode configTreeNode = new DefaultMutableTreeNode(configNode)
		insertNodeInto(configTreeNode, groupTreeNode, groupTreeNode.getChildCount())
		configNodes[configuration] = configTreeNode
	}

	void pruneConfigurations() {
		configNodes.each{ ctype, node ->
			// Get rid of it if it isn't a valid loaded type.
			if( !componentModel.isLoadedComponentType( ctype ) ) {
				log.debug("Pruning extinct configuration: $ctype")

				// Build the group path, skipping the root and ignoring ourself, the last node.
				String groupPath = ""
				TreeNode[] path = node.getPath()
				for( int i = 1; i < path.length-1; i++ ) {
					groupPath += "${path[i]};"
				}

				if( node.getParent() != null ) {
					removeNodeFromParent( node )
				}
				unloadGroup( treeRoot, groupPath, ctype )
			}
		}
	}

	boolean isLoadedConfiguration(String groupPath, String configurationName){
		super.isLoadedConfiguration(groupPath, configurationName)
	}
	
}

// radiobutton renderer
class RadioButtonNodeRederer implements TreeCellRenderer {
	private JRadioButton leafRenderer = new JRadioButton()
	private DefaultTreeCellRenderer nonLeafRenderer = new DefaultTreeCellRenderer()
	private Font fontValue
	Color selectionBorderColor, selectionForeground, selectionBackground,textForeground, textBackground
	
	RadioButtonNodeRederer() {
		super()
	}
	
	protected JRadioButton getLeafRenderer(){
		return leafRenderer
	}
	
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		Component component = null
		
		if(leaf){
			if ((value != null) && (value instanceof DefaultMutableTreeNode)) {
				Object userObject = ((DefaultMutableTreeNode) value).getUserObject()
				if (userObject instanceof JRadioButton) {
					JRadioButton node = (JRadioButton) userObject
					leafRenderer = node
					leafRenderer.setEnabled(tree.isEnabled())
					leafRenderer.setBackground(Color.WHITE)
					component = leafRenderer
				}else{
					component = nonLeafRenderer.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus)
				}
			}
		}else{
			component = nonLeafRenderer.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus)
		}
		return component
	}
}

// radioButton editor
class RadioButtonNodeEditor extends AbstractCellEditor implements TreeCellEditor {
	RadioButtonNodeRederer renderer = new RadioButtonNodeRederer()
	ChangeEvent changeEvent = null
	JTree tree
	
	RadioButtonNodeEditor(JTree t){
		tree = t
	}
	
	public getCellEditorValue(){
		return renderer.getLeafRenderer()
	}
	
	public boolean isCellEditable(EventObject event){
		boolean isEditable = false
		
		if(event instanceof MouseEvent){
			MouseEvent mouseEvent = (MouseEvent)event
			TreePath path = tree.getPathForLocation(mouseEvent.getX(), mouseEvent.getY())
			
			if(path!=null){
				Object node = path.getLastPathComponent()
				if ((node != null) && (node instanceof DefaultMutableTreeNode)) {
					DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) node
					Object userObject = treeNode.getUserObject()
					isEditable = ((treeNode.isLeaf()) && (userObject instanceof JRadioButton))
				}
			}
		}
		return isEditable
	}
	
	public Component getTreeCellEditorComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row){
		Component editor = renderer.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, true)
		
		if(editor instanceof JRadioButton){
			((JRadioButton)editor).addItemListener(new ButtonSelectionListener(tree, editor))
		}
		return editor
	}
}

class ButtonSelectionListener implements ItemListener{
	private static final Logger log = Logger.getLogger(ButtonSelectionListener.class.getName());
	
	JTree tree
	Component component
	
	ButtonSelectionListener(JTree t, Component c){
		tree = t
		component = c
	}
	public void itemStateChanged(ItemEvent itemEvent){
		tree.fireTreeExpanded(tree.getSelectionPath().getParentPath())
		JRadioButton selectedButton = (JRadioButton)component
		
		if(selectedButton.isSelected() ){
			tree.setConfiguration(selectedButton.getName())
		}
	}
}
