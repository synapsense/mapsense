package DeploymentLab;

/**
 * A three field tuple.  Based on Pair.
 * @author Gabriel Helman
 * Date: 3/25/11
 * Time: 5:40 PM
 * @see Pair
 */
public class Triplet<S, T, U>{

	public final S a;
	public final T b;
	public final U c;

	public Triplet(S _a, T _b, U _c) {
		a = _a;
		b = _b;
		c = _c;
	}

	public boolean equals(Object o) {
		if(! o.getClass().getName().equals(getClass().getName()))
			return false;
		Triplet t = (Triplet)o;
		return a.equals(t.a) && b.equals(t.b) && c.equals(t.c);
	}

	@Override
	public int hashCode() {
		int result = a != null ? a.hashCode() : 0;
		result = 31 * result + (b != null ? b.hashCode() : 0);
		result = 31 * result + (c != null ? c.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Triplet{" +
				"a=" + a +
				", b=" + b +
				", c=" + c +
				'}';
	}
}
