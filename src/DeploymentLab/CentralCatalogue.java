package DeploymentLab;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import DeploymentLab.SceneGraph.DeploymentPanel;
import DeploymentLab.channellogger.Logger;
import DeploymentLab.channellogger.NoLogException;
import com.synapsense.util.unitconverter.ConverterException;
import com.synapsense.util.unitconverter.SystemConverter;
import com.synapsense.util.unitconverter.UnitSystem;
import com.synapsense.util.unitconverter.UnitSystems;

import javax.swing.*;


/**
 * Central location for singleton properties files and program-wide static constants.
 * Use this to access either application.properties or uistrings.properties, rather than passing references around
 * or instantiating multiple Properties objects.
 * <p/>
 * For Mars, updated to a enum-style singleton, as per the example in Effective Java.
 * <p/>
 * Note also that this is initialized before the logger is, so we can't use the logger to report errors in the constructor.
 * Hence, we throw NoLogExceptions, which are caught by Main() and reported to the user before dying.
 * (Because also, if this constructor fails, DL won't work at all.)
 *
 * @author Gabriel Helman
 * @since Hermes
 *        Date: Oct 26, 2010
 *        Time: 12:08:48 PM
 */
public enum CentralCatalogue {
	INSTANCE;

    /** Globally unique identifier for this application instance. */
    public static final UUID APPLICATION_INSTANCE_ID = UUID.randomUUID();

	//we also do world-wide constants in here
	/**
	 * Bitmask value for no LI layer.
	 */
	public static final int LI_LAYER_NONE = 0;
	/**
	 * Bitmask value for the top LI layer.
	 */
	public static final int LI_LAYER_TOP = 0x20000;
	/**
	 * Bitmask value for the middle LI layer.
	 */
	public static final int LI_LAYER_MIDDLE = 0x4000;
	/**
	 * Bitmask value for the bottom LI layer.
	 */
	public static final int LI_LAYER_BOTTOM = 0x800;
	/**
	 * Bitmask value for the subfloor LI layer.
	 */
	public static final int LI_LAYER_SUBFLOOR = 0x100;

	public static final List<String> CONTROL_TYPE_ROLES = Arrays.asList("pressurecontrol", "temperaturecontrol", "airflowcontrol");
	public static final List<String> NETWORK_TYPE_ROLES = Arrays.asList("network");
	public static final List<String> GROUP_TYPE_ROLES = Arrays.asList("zone", "set");

	//Objects that the catalog is in charge of holding:
	private Properties uiStrings;
	private Properties appProperties;
	private Properties upgradeProperties;
	private Properties associationProperties;
	private Properties componentsProperties;
	private Properties componentConversionProperties;
	private Properties objectConversionProperties;
	private Properties updateByChannelProperties;
	private Properties smartZoneConnectionProperties;

	private UnitSystems unitSystems;
	private UnitSystem defaultUnitSystem;

    private SystemConverter systemConverter = null;
    private SystemConverter reverseConverter = null;

	private JFrame globalParentFrame = null;

	private SystemSettings systemSettings = new SystemSettings();


	// TODO: Probably need to rethink OpenProject. If we want there to be multiple DLProjects at a given time, this won't
	// work the way things are implemented right now. Code depends on this being set when loading/upgrading files, but you
	// might want to load a file on the side without it being the active project. ImportProjectController is one case.
	private DLProject openProject;
	private SelectModel selectModel;
	private UndoBuffer undoBuffer;
	private DeploymentPanel deploymentPanel;

	private CentralCatalogue() {
		ClassLoader cl = this.getClass().getClassLoader();
		uiStrings = new Properties();
		appProperties = new Properties();
		upgradeProperties = new Properties();
		associationProperties = new Properties();
		componentsProperties = new Properties();
		componentConversionProperties = new Properties();
		objectConversionProperties = new Properties();
		updateByChannelProperties = new Properties();
		smartZoneConnectionProperties = new Properties();

		try {
			InputStream uiStream = cl.getResourceAsStream("cfg/uistrings.properties");
			uiStrings.load(uiStream);
			uiStream.close();
		} catch (IOException ioe) {
			throw new NoLogException("Cannot Open UIStrings Property File!");
		}

		try {
			InputStream appStream = cl.getResourceAsStream("cfg/application.properties");
			appProperties.load(appStream);
			appStream.close();
		} catch (IOException ioe) {
			throw new NoLogException("Cannot Open Application Property File!");
		}

		try {
			InputStream propStream = cl.getResourceAsStream("cfg/upgrade.properties");
			upgradeProperties.load(propStream);
			propStream.close();
		} catch (IOException ioe) {
			throw new NoLogException("Cannot Open Upgrade Property File!");
		}

		try {
			InputStream assocStream = cl.getResourceAsStream("cfg/associations.properties");
			associationProperties.load(assocStream);
			assocStream.close();
		} catch (IOException ioe) {
			throw new NoLogException("Cannot Open Associations Property File!");
		}

		try {
			InputStream compPropStream = cl.getResourceAsStream("cfg/components.properties");
			componentsProperties.load(compPropStream);
			compPropStream.close();
		} catch (IOException ioe) {
			throw new NoLogException("Cannot Open Components Property File!");
		}

		try {
			InputStream convStream = cl.getResourceAsStream("cfg/componentConversion.properties");
			componentConversionProperties.load(convStream);
			convStream.close();
		} catch (IOException ioe) {
			throw new NoLogException("Cannot Open Component Conversion Property File!");
		}

		try {
			InputStream convStream = cl.getResourceAsStream("cfg/objectConversion.properties");
			objectConversionProperties.load(convStream);
			convStream.close();
		} catch (IOException ioe) {
			throw new NoLogException("Cannot Open Object Conversion Property File!");
		}

		try {
			InputStream convStream = cl.getResourceAsStream("cfg/updateByChannel.properties");
			updateByChannelProperties.load(convStream);
			convStream.close();
		} catch (IOException ioe) {
			throw new NoLogException("Cannot Open Channel Updates Property File!");
		}

		try {
			InputStreamReader systems = new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream("cfg/systems.xml"));
			unitSystems = new UnitSystems();
			unitSystems.loadFromXml(systems);
			defaultUnitSystem = unitSystems.getUnitSystem("Imperial US");
		} catch (Exception e) {
			throw new RuntimeException("Unable to load Unit Systems!", e);
		}

	}

	/**
	 * Get the instance of the Central Catalogue.
	 *
	 * @return the CentralCatalogue instance.
	 */
	public static CentralCatalogue getInstance() {
		return INSTANCE;
	}

	/**
	 * Determines if the instance of the application running right now is a Beta (pre-release) version or a Release
	 * version. This looks at the build revision, which is the most recent git tag for the branch, and takes advantage
	 * of our (hopefully) consistent tag naming scheme to determine if this is a pre-release build.
	 *
	 * @return  True is this is a Beta version.
	 */
	public static boolean isBetaVersion() {
		// We want the fail case to be a non-beta version, so just check for the explicit pattern of development builds
		// where we have "_B#" after the dot version: <Release Name>/#.#.#_B<anything>
		return getApp("build.revision").matches("\\w+/\\d+\\.\\d+\\.\\d+_B.+");
	}

	/**
	 * Gets the Properties object for uistrings.properties
	 *
	 * @return a Properties matching uistrings.properties
	 */
	public Properties getUIStrings() {
		return uiStrings;
	}

	/**
	 * Convenience method to pull a value from uistrings.properties
	 *
	 * @param fieldname the key to retrieve from uistrings.properties
	 * @return the value retrieved from uistrings.properties
	 */
	public static String getUIS(String fieldname) {
		return INSTANCE.getUIStrings().getProperty(fieldname);
	}

	/**
	 * Convenience method to pull a value from uistrings.properties and format it with String.format.
	 * @param fieldname the key to retrieve from uistrings.properties
	 * @param fields variable-arity list of values to format into the string
	 * @return the formatted String
	 * @see String#format(String, Object...)
	 */
	public static String formatUIS(String fieldname, Object ... fields){
		return String.format(INSTANCE.getUIStrings().getProperty(fieldname), fields);
	}

	/**
	 * Gets the Properties object for application.properties
	 *
	 * @return a Properties matching application.properties
	 */
	public Properties getAppProperties() {
		return appProperties;
	}

	/**
	 * Convenience method to pull a value from application.properties
	 *
	 * @param fieldname the key to retrieve from application.properties
	 * @return the value retrieved from application.properties
	 */
	public static String getApp(String fieldname) {
		return INSTANCE.getAppProperties().getProperty(fieldname);
	}


	/**
	 * Gets the Properties object for upgrade.properties
	 *
	 * @return a Properties matching upgrade.properties
	 */
	public Properties getUpgradeProperties() {
		return upgradeProperties;
	}

	/**
	 * Convenience method to pull a value from upgrade.properties
	 *
	 * @param fieldname the key to retrieve from upgrade.properties
	 * @return the value retrieved from application.upgrade
	 */
	public static String getUp(String fieldname) {
		return INSTANCE.getUpgradeProperties().getProperty(fieldname);
	}


	/**
	 * Gets the Properties object for associations.properties
	 *
	 * @return a Properties matching associations.properties
	 */
	public Properties getAssociationProperties() {
		return associationProperties;
	}

    /**
     * Convenience method to pull a value from associations.properties
     *
     * @param fieldname the key to retrieve from associations.properties
     * @return the value retrieved from associations.upgrade
     */
    public static String getAssoc(String fieldname) { // H.R. requires the "oc" in the name.
        return INSTANCE.getAssociationProperties().getProperty(fieldname);
    }

	public static SystemSettings getSystemSettings() {
		return INSTANCE.systemSettings;
	}


	public Properties getComponentsProperties() {
		return componentsProperties;
	}

	public static Properties getSmartZoneConnecionProperties() {
		return INSTANCE.smartZoneConnectionProperties;
	}

	public static Properties getComponentConversionProperties() {
		return INSTANCE.componentConversionProperties;
	}

	public static boolean hasComponentConversion( String componentType ) {
		return getComponentConversionProperties().containsKey( componentType );
	}

	public static String getComponentConversion( String componentType ) {
		return getComponentConversionProperties().getProperty( componentType, componentType );
	}


	public static Properties getObjectConversionProperties() {
		return INSTANCE.objectConversionProperties;
	}

	public static boolean hasObjectTypeConversion( String objectType ) {
		return getObjectConversionProperties().containsKey( objectType );
	}

	public static String getObjectTypeConversion( String objectType ) {
		return getObjectConversionProperties().getProperty( objectType, objectType );
	}

	public static boolean hasObjectNameConversion( String componentType, String objectName ) {
		return getObjectConversionProperties().containsKey( componentType + "." + objectName );
	}

	public static String getObjectNameConversion( String componentType, String objectName ) {
		return getObjectConversionProperties().getProperty( componentType + "." + objectName, objectName );
	}


	public Properties getUpdateByChannelProperties() {
		return updateByChannelProperties;
	}

	public static String getComp(String fieldname) {
		return INSTANCE.getComponentsProperties().getProperty(fieldname);
	}

	public UnitSystems getUnitSystems() {
		return unitSystems;
	}

	public UnitSystem getDefaultUnitSystem() {
		return defaultUnitSystem;
	}

    /**
     * Sets the common unit converter system currently in use throughout the application. A reverse converter is
     * automatically created.
     *
     * @param systemConverter      The new system converter.
     * @throws ConverterException  A reverse converter could not be created.
     */
    public void setSystemConverter( SystemConverter systemConverter ) throws ConverterException {
        this.systemConverter = systemConverter;

        if( systemConverter == null ) {
            this.reverseConverter = null;
        } else {
            this.reverseConverter = unitSystems.getSystemConverter( systemConverter.getTargetSystemName(),
                                                                    systemConverter.getBaseSystemName() );
        }
    }

    /**
     * Get the currently configured system converter.
     *
     * @return  System converter or null if the base system is in use.
     */
    public SystemConverter getSystemConverter() {
        return systemConverter;
    }

    /**
     * Get the reverse converter for the currently configured system converter. This is used to convert back to the
     * application base system from the current system.
     *
     * @return  Reverse system converter or null if the base system is in use.
     */
    public SystemConverter getReverseConverter() {
        return reverseConverter;
    }

	public static void setParentFrame(JFrame f){
		INSTANCE.globalParentFrame = f;
	}

	public static JFrame getParentFrame(){
		return INSTANCE.globalParentFrame;
	}

	public static void setSelectModel( SelectModel m ) {
		INSTANCE.selectModel = m;
	}

	public static SelectModel getSelectModel() {
		return INSTANCE.selectModel;
	}

	public static void setUndoBuffer( UndoBuffer b ) {
		INSTANCE.undoBuffer = b;
	}

	public static UndoBuffer getUndoBuffer() {
		return INSTANCE.undoBuffer;
	}

	public static void setOpenProject( DLProject p ) {
		INSTANCE.openProject = p;
	}

	public static DLProject getOpenProject() {
		return INSTANCE.openProject;
	}

	public static void setDeploymentPanel(DeploymentPanel dp){
		INSTANCE.deploymentPanel = dp;
	}

	public static DeploymentPanel getDeploymentPanel(){
		return INSTANCE.deploymentPanel;
	}
}
