
package DeploymentLab

import DeploymentLab.Palette.PaletteRenderer
import java.awt.Component
import java.awt.Color
import java.awt.Font

import javax.swing.JTable
import javax.swing.table.DefaultTableCellRenderer
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.table.JTableHeader
import javax.swing.table.TableColumnModel

import DeploymentLab.channellogger.*
import DeploymentLab.Model.*

import javax.swing.JComponent

class MessageTable extends JTable {
	private static final Logger log = Logger.getLogger(MessageTable.class.getName())
	private MessageModel messageModel
	private SelectModel selectModel
	private ComponentModel componentModel

	MessageTable(MessageModel mm) {
		super(mm)
		messageModel = mm
		setDefaultRenderer(String.class,new MessageTableCellRenderer())
		JTableHeader header = this.getTableHeader()
		header.addMouseListener(new ColumnMouseListener(this))
	}

	void setModel(SelectModel sm, ComponentModel cm){
		selectModel = sm
		componentModel = cm
	}

	void changeSelection(int rowIndex, int columnIndex, boolean toggle,boolean extend){
		super.changeSelection(rowIndex, columnIndex, toggle, extend)
		def objects = []
		def rows = this.getSelectedRows()
		for( def i : rows ){
			def msg = messageModel.getObjectAt(i)
			msg.validationResult.objects.each {
				objects.add( it.getComponent() )
			}
		}
		//log.trace("in changeSelection(), objects= $objects")
		//if all the components are in the same drawing other than the current drawing,
		//change the active drawing to the first drawing in question
		def drawings = componentModel.computeDrawingsForComponents(objects)
		if (! drawings.contains(selectModel.activeDrawing) ){
			selectModel.setActiveDrawing(drawings.iterator().next())
		}

		if(objects!=null){
			selectModel.setSelection(objects)
		}
	}
}

class MessageTableCellRenderer extends DefaultTableCellRenderer {
	private static final Color redColor = new Color(255, 128, 128)
	private static final Color yellowColor = new Color(255, 255, 128)

	private static final Color blueColor = new Color(30, 144, 255)
	//private static final Color blueColor = new Color(65, 105, 255)

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)  {

		JComponent cell = (JComponent)super.getTableCellRendererComponent(table, value, false, hasFocus, row, column)

		if(isSelected)
			cell.setFont(cell.getFont().deriveFont(Font.BOLD))
/*
		if(table.getModel().getObjectAt(row).type == 0)
			cell.setBackground(yellowColor)
		else
			cell.setBackground(redColor)
*/

		switch ( table.getModel().getObjectAt(row).type ){
			case 0:
				cell.setBackground(yellowColor)
				break
			case 1:
				cell.setBackground(redColor)
				break
			case 2:
				cell.setBackground(blueColor)
				break
		}

		cell.setToolTipText( "<html>" + PaletteRenderer.wrap( value.toString(), 100 ) + "</html>" )

		return cell
	}
}

class ColumnMouseListener extends MouseAdapter{
	private static final Logger log = Logger.getLogger(ColumnMouseListener.class.getName())
	
	private JTable table
	private boolean isAscending = true  
	public ColumnMouseListener(JTable _table){
		table = _table
	}
	
	public void mouseClicked(MouseEvent e){
		TableColumnModel colModel = table.getColumnModel()
		int colModelIndex = colModel.getColumnIndexAtX(e.getX())
		isAscending = !isAscending
		
		MessageModel model =table.getModel()
		model.sortByColumn(colModelIndex, isAscending)
	}
}