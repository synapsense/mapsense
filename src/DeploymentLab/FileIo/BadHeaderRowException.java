package DeploymentLab.FileIo;

public class BadHeaderRowException extends FlatFileReaderException {

	private static final long serialVersionUID = 1L;

	private int headerRow = 0;

	private int totalNumberOfRows = 0;

	public BadHeaderRowException() {
		super("Unable to read headers from an empty file");
	}

	public BadHeaderRowException(int headerRow, int totalNumberOfRows) {
		super("Header row index " + headerRow + " is to large, file has only " + totalNumberOfRows + " rows");
		this.headerRow = headerRow;
		this.totalNumberOfRows = totalNumberOfRows;
	}

	public int getHeaderRow() {
		return headerRow;
	}

	public int getTotalNumberOfRows() {
		return totalNumberOfRows;
	}
}
