package DeploymentLab.FileIo;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFShape;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFTable;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelModelImpoter {

	private XSSFWorkbook workbook;

	private XSSFSheet activeSheet;

	private Iterator<XSSFSheet> sheetIter;

	public ExcelModelImpoter(String fileName) throws Exception {
		workbook = (XSSFWorkbook) WorkbookFactory.create(new File(fileName));
		sheetIter = workbook.iterator();
	}

	public int getActiveDrawing() {
		return workbook.getActiveSheetIndex();
	}

	public boolean nextSheet() {
		if (!sheetIter.hasNext()) {
			return false;
		}
		activeSheet = sheetIter.next();
		return true;
	}

	public byte[] getPicture() {
		for (XSSFShape shape : activeSheet.createDrawingPatriarch().getShapes()) {
			if (shape instanceof XSSFPicture) {
				XSSFPicture p = (XSSFPicture) shape;
				return p.getPictureData().getData();
			}
		}
		return null;
	}

	public List<FlatFileRow> getTable(String name) {
		List<XSSFTable> tables = activeSheet.getTables();
		for (XSSFTable table : tables) {
			if (table.getName().startsWith(name)) {
				return parseTable(table);
			}
		}
		return null;
	}

	private List<FlatFileRow> parseTable(XSSFTable table) {
		CellReference start = table.getStartCellReference();

		HashMap<Integer, String> headersMap = new HashMap<>();

		XSSFRow row = activeSheet.getRow(start.getRow());
		for (int i = start.getCol(); i < start.getCol() + table.getNumerOfMappedColumns(); i++) {
			headersMap.put(i, row.getCell(i).getStringCellValue());
		}

		ArrayList<FlatFileRow> rowList = new ArrayList<>();
		for (int i = start.getRow() + 1; i <= start.getRow() + table.getRowCount(); i++) {
			FlatFileRow fRow = new FlatFileRow(i + 1);
			for (int j = start.getCol(); j < start.getCol() + table.getNumerOfMappedColumns(); j++) {
				Cell cell = activeSheet.getRow(i).getCell(j);
				String value;
				switch (cell.getCellType()) {
				case Cell.CELL_TYPE_NUMERIC:
					value = String.valueOf(cell.getNumericCellValue());
					break;
				default:
					value = cell.getStringCellValue();
				}
				fRow.addValue(headersMap.get(j), value);
			}
			rowList.add(fRow);
		}
		return rowList;
	}
}
