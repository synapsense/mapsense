package DeploymentLab.FileIo;

/**
 * Base simple interface for all possible flat file importers types
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public interface FlatFileReader {

	/**
	 * 
	 * @return
	 */
	boolean hasMoreRows() throws FlatFileReaderException;

	/**
	 * 
	 * @return
	 */
	FlatFileRow nextRow() throws FlatFileReaderException;
}
