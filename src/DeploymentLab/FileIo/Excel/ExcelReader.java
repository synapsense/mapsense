package DeploymentLab.FileIo.Excel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import DeploymentLab.FileIo.BadHeaderRowException;
import DeploymentLab.FileIo.FlatFileReader;
import DeploymentLab.FileIo.FlatFileReaderException;
import DeploymentLab.FileIo.FlatFileRow;

/**
 * Implementation of FlatFileReader for Excel xsl/xlsx files.<br>
 * Note: all indexes in this implementation are 1-based, not 0-based.
 * 
 * @author Dmitry Gridzinskiy
 * @author Oleg Stepanov
 * 
 */
public class ExcelReader implements FlatFileReader {

	public static final int DEFAULT_HEADER_ROW = 1;
	public static final int DEFAULT_SHEET = 1;

	protected Workbook workbook;
	protected HashMap<Integer, String> headersMap;
	protected Iterator<Row> rowIterator;
	private int rowNum;
	private DataFormatter formatter = new DataFormatter();

	public ExcelReader(String fileName) throws FlatFileReaderException, FileNotFoundException, IOException {
		this(fileName, DEFAULT_SHEET, DEFAULT_HEADER_ROW);
	}

	public ExcelReader(InputStream is) throws FlatFileReaderException, IOException {
		this(is, DEFAULT_SHEET, DEFAULT_HEADER_ROW);
	}

	public ExcelReader(String fileName, int sheetIndex, int headerRow) throws FlatFileReaderException, IOException {
		this(new FileInputStream(fileName), sheetIndex, headerRow);
	}

	public ExcelReader(InputStream is, int sheetIndex, int headerRow) throws FlatFileReaderException, IOException {
		try {
			workbook = WorkbookFactory.create(is);
		} catch (InvalidFormatException e) {
			throw new FlatFileReaderException("Unable to read file ", e);
		}
		readHeaders(sheetIndex, headerRow);
	}

	@Override
	public boolean hasMoreRows() {
		return rowIterator.hasNext();
	}

	@Override
	public FlatFileRow nextRow() {
		Row row = rowIterator.next();
		FlatFileRow flatFileRow = new FlatFileRow(++rowNum);

		for (Cell cell : row) {
			String header = headersMap.get(cell.getColumnIndex());
			if (header == null) {
				continue;
			}
			String value;
			switch (cell.getCellType()) {
			case Cell.CELL_TYPE_NUMERIC:
				// getNumericCellValue() returns everything as a double, which breaks some MapSense property converters.
				// Since we want a string anyway, just get it as Excel would have formatted it. WYSIWYG
				value = formatter.formatCellValue( cell );
				break;
			default:
				value = cell.getStringCellValue();
			}
			flatFileRow.addValue(header, value);
		}
		return flatFileRow;
	}

	protected void readHeaders(int sheetIndex, int headerRow) throws FlatFileReaderException, IOException {

		if (workbook.getNumberOfSheets() < sheetIndex) {
			throw new BadSheetNumberException(sheetIndex);
		}

		this.headersMap = new HashMap<>();
		this.rowIterator = workbook.getSheetAt(sheetIndex - 1).iterator();

		rowNum = 0;
		Row row = null;
		while (rowNum < headerRow) {
			if (!rowIterator.hasNext()) {
				throw new BadHeaderRowException(headerRow, rowNum);
			}
			row = rowIterator.next();
			rowNum++;
		}
		if (row == null) {
			throw new BadHeaderRowException(headerRow, rowNum);
		}

		for (Cell cell : row) {
			String header = cell.getStringCellValue();
			if (header != null && !header.isEmpty()) {
				headersMap.put(cell.getColumnIndex(), header.trim());
			}
		}
	}

	public Collection<String> getHeaders(){
		return headersMap.values();
	}
}
