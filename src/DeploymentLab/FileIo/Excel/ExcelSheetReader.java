package DeploymentLab.FileIo.Excel;

import DeploymentLab.FileIo.BadHeaderRowException;
import DeploymentLab.FileIo.FlatFileReaderException;

import java.io.IOException;

/**
 * @author Vadim Gusev
 * @since Europa
 * Date: 08.04.13
 * Time: 14:00
 */
public class ExcelSheetReader extends ExcelReader implements SheetReader {

	private int sheetCount;

	public ExcelSheetReader(String fileName) throws IOException, FlatFileReaderException {
		super(fileName);
		sheetCount = DEFAULT_SHEET;
	}

	public ExcelSheetReader(String fileName, int sheetNum) throws IOException, FlatFileReaderException {
		super(fileName, sheetNum, DEFAULT_HEADER_ROW);
		sheetCount = sheetNum;
	}

	@Override
	public boolean hasMoreSheets() {
		return sheetCount <= workbook.getNumberOfSheets();
	}

	@Override
	public ExcelReader nextSheet() throws FlatFileReaderException, IOException {
		try {
			readHeaders(sheetCount, DEFAULT_HEADER_ROW);
		} catch (BadHeaderRowException e1) {
			sheetCount++;
			return null;
		}
		sheetCount++;
		return this;
	}

	@Override
	public String getSheetName() {
		int currentSheet = sheetCount - 2;
		if (currentSheet >= 0) {
			return workbook.getSheetName(currentSheet);
		}

		return null;
	}
}
