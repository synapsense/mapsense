package DeploymentLab.FileIo.Excel;

import DeploymentLab.FileIo.FlatFileReaderException;

import java.io.IOException;

/**
 * @author Vadim Gusev
 * @since Europa
 * Date: 08.04.13
 * Time: 13:58
 */
public interface SheetReader {

	/**
	 *
	 * @return
	 */
	boolean hasMoreSheets();

	/**
	 *
	 * @return
	 */
	ExcelReader nextSheet() throws FlatFileReaderException, IOException;

	/**
	 *
	 * @return  Name of last retrieved sheet
	 */
	String getSheetName();
}
