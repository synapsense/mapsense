package DeploymentLab.FileIo.Excel;

import DeploymentLab.FileIo.FlatFileReaderException;

public class BadSheetNumberException extends FlatFileReaderException {

	private static final long serialVersionUID = 1L;

	private int sheetNumber;

	public BadSheetNumberException(int sheetNumber) {
		super("Sheet number " + sheetNumber + " doesn't exist in the file ");
		this.sheetNumber = sheetNumber;
	}

	public int getSheetNumber() {
		return sheetNumber;
	}
}
