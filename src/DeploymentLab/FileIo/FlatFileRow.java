package DeploymentLab.FileIo;

import java.util.HashMap;

public class FlatFileRow {

	private HashMap<String, String> values = new HashMap<>();
	private final int rowNum;

	public FlatFileRow(int rowNum) {
		super();
		this.rowNum = rowNum;
	}

	public int getRowNum() {
		return rowNum;
	}

	public void addValue(String header, String value) {
		values.put(header, value);
	}

	public String getStringValue(String header) {
		return values.get(header);
	}

	public Double getDoubleValue(String header) {
		String strValue = values.get(header);
		return strValue == null ? null : Double.valueOf(strValue);
	}

	public Integer getIntegerValue(String header) {
		String strValue = values.get(header);
		if (strValue == null) {
			return null;
		}
		return getRoundedDoubleValue(strValue).intValue();
	}

	public Long getLongValue(String header) {
		String strValue = values.get(header);
		if (strValue == null) {
			return null;
		}
		return getRoundedDoubleValue(strValue).longValue();
	}

	public Long getLongHexValue(String header) {
		String strValue = values.get(header);
		return strValue == null ? null : Long.parseLong(strValue, 16);
	}

	private static Double getRoundedDoubleValue(String strValue) {
		// cause I want to be able to convert double values to integers
		Double dValue = Double.valueOf(strValue);
		if (dValue == Math.round(dValue)) {
			return dValue;
		}
		throw new NumberFormatException("Cannot convert " + dValue + " to integer");
	}

	@Override
	public String toString() {
		return values.toString();
	}
}
