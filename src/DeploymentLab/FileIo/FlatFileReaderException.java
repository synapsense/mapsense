package DeploymentLab.FileIo;

public class FlatFileReaderException extends Exception {

	private static final long serialVersionUID = 3561561015127756784L;

	public FlatFileReaderException(String message, Throwable cause) {
		super(message, cause);
	}

	public FlatFileReaderException(String message) {
		super(message);
	}
}
