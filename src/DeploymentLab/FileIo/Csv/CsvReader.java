package DeploymentLab.FileIo.Csv;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.NoSuchElementException;

import DeploymentLab.FileIo.BadHeaderRowException;
import DeploymentLab.FileIo.FlatFileReader;
import DeploymentLab.FileIo.FlatFileReaderException;
import DeploymentLab.FileIo.FlatFileRow;

public class CsvReader implements FlatFileReader {

	public static final String DEFAULT_DELIMITER = ",";

	private String delimiter;

	private LineNumberReader reader;

	private String currentLine;

	private HashMap<Integer, String> headersMap = new HashMap<>();

	public CsvReader(String fileName) throws BadHeaderRowException, FileNotFoundException, IOException {
		this(fileName, DEFAULT_DELIMITER);
	}

	public CsvReader(InputStream is) throws BadHeaderRowException, IOException {
		this(is, DEFAULT_DELIMITER);
	}

	public CsvReader(Reader reader) throws BadHeaderRowException, IOException {
		this(reader, DEFAULT_DELIMITER);
	}

	public CsvReader(InputStream is, String delimiter) throws BadHeaderRowException, IOException {
		this(new InputStreamReader(is), delimiter);
	}

	public CsvReader(String fileName, String delimiter) throws BadHeaderRowException, FileNotFoundException, IOException {
		this(new FileReader(fileName), delimiter);
	}

	public CsvReader(Reader reader, String delimiter) throws BadHeaderRowException, IOException {
		this.reader = new LineNumberReader(reader);
		this.delimiter = delimiter;
		readHeaders();
	}

	@Override
	public boolean hasMoreRows() throws FlatFileReaderException {
		try {
			readNextLine();
			return this.currentLine == null ? false : true;
		} catch (IOException e) {
			throw new FlatFileReaderException("Unable to read next line", e);
		}
	}

	@Override
	public FlatFileRow nextRow() throws FlatFileReaderException {
		try {
			readNextLine();
			if (this.currentLine == null) {
				throw new NoSuchElementException();
			}
			String[] values = this.currentLine.split(delimiter);

			FlatFileRow row = new FlatFileRow(reader.getLineNumber());
			for (int i = 0; i < values.length; i++) {
				String header = headersMap.get(i);
				if (header == null) {
					continue;
				}
				row.addValue(header, "".equals(values[i]) ? null : values[i].trim());
			}
			this.currentLine = null;
			return row;
		} catch (IOException e) {
			throw new FlatFileReaderException("Unable to read next line", e);
		}
	}

	private void readNextLine() throws IOException {
		try {
			if (this.currentLine != null) {
				return;
			}
			while ("".equals(this.currentLine = this.reader.readLine()))
				;
			if (currentLine == null) {
				this.reader.close();
				return;
			}
		} catch (IOException e) {
			this.reader.close();
			throw e;
		}
	}

	private void readHeaders() throws IOException, BadHeaderRowException {
		try {
			String strHeader;
			while ("".equals(strHeader = this.reader.readLine()))
				;
			if (strHeader == null) {
				reader.close();
				throw new BadHeaderRowException();
			}

			String[] headers = strHeader.split(delimiter);
			for (int i = 0; i < headers.length; i++) {
				headersMap.put(i, headers[i].trim());
			}
		} catch (IOException e) {
			reader.close();
		}
	}
}
