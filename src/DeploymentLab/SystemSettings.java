package DeploymentLab;

import org.jboss.logging.Logger;

import java.util.prefs.Preferences;

public class SystemSettings {
	private static Preferences prefs = Preferences.systemRoot();
	private static final Logger log = Logger.getLogger(SystemSettings.class.getName());
	
	public void setSmartZoneHost(String host) {
		prefs.put("SmartZoneHost", host);
	}

	public String getSmartZoneHost() {
		return prefs.get("SmartZoneHost","");
	}

	public void setSmartZonePort(int port) {
		prefs.putInt("SmartZonePort", port);
	}

	public int getSmartZonePort() {
		return prefs.getInt("SmartZonePort", 8443);
	}

}
