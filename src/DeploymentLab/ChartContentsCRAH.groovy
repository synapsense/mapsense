package DeploymentLab
/**
 * Much like the regular ChartContents, except that this one has a name, is value-only, and has some equivalancy handling.
 */
class ChartContentsCRAH extends ChartContents {
	private String chartName

	public String getChartName(){
		return( chartName )
	}

	public String setChartName( String cn ){
		chartName = cn
	}


	public ChartContentsCRAH(){
		super()
	}

	public ChartContentsCRAH( String freshValueString ){
		super(freshValueString)
	}

	public ChartContentsCRAH( String freshValueString, String cn ){
		super(freshValueString)
		chartName = cn
	}
	
	public String getPercentageString(){
		throw new Exception("ChartContentsCRAH is value only!")
	}

	public String toString() {
		return chartName
	}

	boolean equals(o) {
		if (this.is(o)) return true;
		if (!o || getClass() != o.class) return false;
		ChartContentsCRAH that = (ChartContentsCRAH) o;
		if (chartName ? !chartName.equals(that.chartName) : that.chartName != null) return false;
		return true;
	}

	int hashCode() {
		return (chartName ? chartName.hashCode() : 0);
	}
}