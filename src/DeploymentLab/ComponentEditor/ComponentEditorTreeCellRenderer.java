package DeploymentLab.ComponentEditor;

import java.awt.Component;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import DeploymentLab.Image.ComponentIconFactory;
import DeploymentLab.channellogger.Logger;


public class ComponentEditorTreeCellRenderer extends DefaultTreeCellRenderer {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(ComponentEditorTreeCellRenderer.class.getName());

	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean bSelected, boolean bExpanded, boolean bLeaf, int nRow, boolean bFocus) {
		super.getTreeCellRendererComponent(tree, value, bSelected, bExpanded, bLeaf, nRow, bFocus);
		//for the moment, we don't want to try to get an icon for leaf nodes, since we don't have raster icons for leaves.
		//if ( ! bLeaf ) {
			//Object o = ((DefaultMutableTreeNode)value).getUserObject();
			this.setIcon( ComponentIconFactory.getComponentIcon(value) );
		//}
		return this;
	}

}