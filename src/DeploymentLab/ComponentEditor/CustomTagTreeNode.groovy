
package DeploymentLab.ComponentEditor

import javax.swing.tree.DefaultMutableTreeNode

import DeploymentLab.Model.*

import DeploymentLab.channellogger.*
import DeploymentLab.Image.IconNameProvider

class CustomTagTreeNode extends DefaultMutableTreeNode implements IconNameProvider {
	private static final Logger log = Logger.getLogger(CustomTagTreeNode.class.getName())

	CustomTagTreeNode(CustomTag ct) {
		super(ct, false)
	}

	void setUserObject(Object o) {
		if(o instanceof CustomTag) {
			super.setUserObject(o)
		} else {
			try {
				getUserObject().setValue(o)
			} catch(Exception e) {
				log.warn("Can't set property to '$o'\n" + e, 'message')
			}
		}
	}

	String toString() {
		CustomTag ct = getUserObject()
		//return s.name + ": " + s.getValue() + ";  " + s.printTags()
		//def valuetype = s.getValueType()
		//valuetype = valuetype.substring( valuetype.lastIndexOf('.') + 1 )
		//return "${s.name}: ${s.getValue()} (${valuetype}); ${s.printTags()}  "

		String result =  "${ct.getTagName()}: ${ct.getValue()} (${ct.getTagType()}) [oldVal=${ct.getOldValue()}]"


		if ( ct.getOverride() != null ){
			result += " OVERRIDDEN"
		}

		return result
	}

	String getIconName() {
		return "ce_customtag.png"
	}


}



