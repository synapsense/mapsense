
package DeploymentLab.ComponentEditor

import javax.swing.tree.DefaultMutableTreeNode

import DeploymentLab.channellogger.*
import DeploymentLab.Image.IconNameProvider

class CollectionTreeNode extends DefaultMutableTreeNode implements IconNameProvider  {
	private static final Logger log = Logger.getLogger(CollectionTreeNode.class.getName())

	CollectionTreeNode(DeploymentLab.Model.Collection c) {
		super(c, false)
		this.setAllowsChildren(true)
	}

	void setUserObject(Object o) {
		if(o instanceof DeploymentLab.Model.Collection) {
			super.setUserObject(o)
		} else {
			try {
				getUserObject().setValue(o)
			} catch(Exception e) {
				log.warn("Can't set property to '$o'\n" + e, 'message')
			}
		}
	}

	String toString() {
		DeploymentLab.Model.Collection c = getUserObject()
		return c.name + ": " + c.getItemIds() + " [dirty=${c.dirty}]"
	}

	String getIconName() {
		return "ce_collection.png"
	}
}

