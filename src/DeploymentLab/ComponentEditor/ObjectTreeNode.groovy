
package DeploymentLab.ComponentEditor

import javax.swing.tree.DefaultMutableTreeNode

import DeploymentLab.Model.*
import DeploymentLab.Image.IconNameProvider

class ObjectTreeNode extends DefaultMutableTreeNode implements IconNameProvider {
	ObjectTreeNode(DLObject o) {
		super(o, true)
	}

	String toString() {
		DLObject o = getUserObject()
		try {
			return o.getType() +
			       (o.hasObjectProperty('name') ? " \"" + o.getObjectProperty('name').getValue() + "\"" : "") +
			       " (" + o.getDlid() + ") (" + o.getKey() + ")" +
			       (o.isSmartZoneLinked() ? " [SZID:${o.getSmartZoneId()}]" : "") + (!o.isExportable() ? " (NO EXPORT)" : "")
		} catch(Exception e) {
			return o.getType() + " (" + o.getDlid() + ") (" + o.getKey() + ")" + " ** ERROR **"
		}
	}

	String getIconName() {
		return "ce_object.png"
	}
}

