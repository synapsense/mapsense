
package DeploymentLab.ComponentEditor

import javax.swing.tree.DefaultMutableTreeNode

import DeploymentLab.Model.*

import DeploymentLab.channellogger.*
import DeploymentLab.Image.IconNameProvider

class GroovyRuleTreeNode extends DefaultMutableTreeNode implements IconNameProvider {
	private static final Logger log = Logger.getLogger(GroovyRuleTreeNode.class.getName())

	GroovyRuleTreeNode(GroovyRule r) {
		super(r, false)
		this.setAllowsChildren(true);
	}

	void setUserObject(Object o) {
		if(o instanceof GroovyRule) {
			super.setUserObject(o)
		} else {
			try {
				getUserObject().setValue(o)
			} catch(Exception e) {
				log.warn("Can't set property to '$o'\n" + e, 'message')
			}
		}
	}

	String toString() {
		GroovyRule r = getUserObject()
		return r.name + ": " + r.getRuleFile()
	}

	String getIconName() {
		return "ce_groovyrule.png"
	}
}

