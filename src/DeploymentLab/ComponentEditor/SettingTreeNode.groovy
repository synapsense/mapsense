
package DeploymentLab.ComponentEditor

import javax.swing.tree.DefaultMutableTreeNode

import DeploymentLab.Model.*

import DeploymentLab.channellogger.*
import DeploymentLab.Image.IconNameProvider

class SettingTreeNode extends DefaultMutableTreeNode implements IconNameProvider {
	private static final Logger log = Logger.getLogger(SettingTreeNode.class.getName())

	SettingTreeNode(Setting s) {
		super(s, false)
		this.setAllowsChildren(true)
	}

	void setUserObject(Object o) {
		if(o instanceof Setting) {
			super.setUserObject(o)
		} else {
			try {
				getUserObject().setValue(o)
			} catch(Exception e) {
				log.warn("Can't set property to '$o'\n" + e, 'message')
			}
		}
	}

	String toString() {
		Setting s = getUserObject()
		//return s.name + ": " + s.getValue() + ";  " + s.printTags()
		def valuetype = s.getValueType()
		valuetype = valuetype.substring( valuetype.lastIndexOf('.') + 1 )
		//return "${s.name}: ${s.getValue()} (${valuetype}) [oldValue=${s.getOldValue()}] " //; ${s.printTags()}  "
		String result =  "${s.name}: ${s.getValue()} (${valuetype}) [oldValue=${s.getOldValue()}]  "

		if ( s.getOverride() != null ){
			result += " OVERRIDDEN"
		}

		return result
	}

	String getIconName() {
		return "ce_setting.png"
	}


}

