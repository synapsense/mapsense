package DeploymentLab.ComponentEditor;

import javax.swing.tree.DefaultMutableTreeNode;

import DeploymentLab.Image.IconNameProvider;
import DeploymentLab.Model.*;
import DeploymentLab.channellogger.*;

class RuleTreeNode extends DefaultMutableTreeNode implements IconNameProvider {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(RuleTreeNode.class.getName());

	RuleTreeNode(Rule r) {
		super(r, false);
		this.setAllowsChildren(true);
	}

	public void setUserObject(Object o) {
		if(o instanceof Rule) {
			super.setUserObject(o);

		} else {
			/*
			try {
				this.getUserObject().setValue(o);
			} catch(Exception e) {
			*/
				log.warn("Can't set property to '$o'\n", "message");
			//}

		}
	}

	public String toString() {
		Rule r = (Rule)getUserObject();
		return r.getName() + ": " + r.getRuleClass();
	}

	public String getIconName() {
		return "ce_rule.png";
	}
}

