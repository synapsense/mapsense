
package DeploymentLab.ComponentEditor


import java.awt.BorderLayout
import java.awt.Component
import java.awt.FlowLayout
import java.awt.Dialog.ModalityType

import javax.swing.JDialog
import javax.swing.JFrame
import javax.swing.JTree

import javax.swing.tree.DefaultTreeModel
import javax.swing.tree.DefaultTreeCellEditor
import javax.swing.tree.DefaultTreeCellRenderer
import javax.swing.tree.TreeCellEditor
import javax.swing.tree.TreePath

import groovy.swing.SwingBuilder

import DeploymentLab.Tree.ComponentTreeNode

import DeploymentLab.Model.*

import DeploymentLab.channellogger.*
import javax.swing.tree.DefaultMutableTreeNode
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.JPopupMenu
import javax.swing.JMenuItem
import java.awt.event.ActionListener

class ComponentEditor {
	static JDialog createEditor(DLComponent component, JFrame parent) {
		String componentName = component.getPropertyValue("name")
		
		ComponentEditorTree editorTree = new ComponentEditorTree(component)

		def builder = new SwingBuilder()
		JDialog editor
		editor = builder.dialog(
			owner: parent,
			resizable: true,
			title: "Editing Component '$componentName'",
			layout: new BorderLayout(),
			//modalityType: ModalityType.APPLICATION_MODAL
			modalityType: ModalityType.MODELESS
		) {
			scrollPane(preferredSize: [400, 400], constraints: BorderLayout.CENTER) {
				widget(editorTree)
			}

			panel(constraints: BorderLayout.SOUTH, layout: new FlowLayout(FlowLayout.TRAILING)) {
				button(text: "Close", mnemonic: "C", actionPerformed: {editor.hide()})
			}
		}
		editor.pack()
		return editor
	}
}

class ComponentEditorTree extends JTree {
	ComponentTreeNode root
	private boolean editOldValue = false

	ComponentEditorTree(DLComponent c) {
		root = new ComponentTreeNode(c, true)
		c.roots.each{ o ->
			root.add(buildTree(o))
		}
		setModel(new DefaultTreeModel(root))
		setEditable(true)
		setShowsRootHandles(true)
		setCellRenderer( new ComponentEditorTreeCellRenderer() )

		c.addPropertyChangeListener(this, this.&componentPropertyChanged)
		addMouseListener( new ComponentTreeMouseAdapter(this))
	}

	private ObjectTreeNode buildTree(DLObject o) {
		ObjectTreeNode onode = new ObjectTreeNode(o)
		o.getPropertiesByType('setting').sort().each{ s ->
			SettingTreeNode pnode = new SettingTreeNode(s)
			onode.add(pnode)
			//add tag nodes if there are tags to be had
			if ( s.hasTags() ){
				s.getTags().each{ ct ->
					CustomTagTreeNode tnode = new CustomTagTreeNode(ct)
					pnode.add(tnode)
				}
			}
		}
		o.getPropertiesByType('dockingpoint').sort().each{ d ->
			DockingpointTreeNode pnode = new DockingpointTreeNode(d)
			onode.add(pnode)
			//add tag nodes if there are tags to be had
			if ( d.hasTags() ){
				d.getTags().each{ ct ->
					CustomTagTreeNode tnode = new CustomTagTreeNode(ct)
					pnode.add(tnode)
				}
			}
		}

		o.getPropertiesByType('pier').sort().each{ d ->
			PierTreeNode pnode = new PierTreeNode(d)
			onode.add(pnode)
			//add tag nodes if there are tags to be had
			if ( d.hasTags() ){
				d.getTags().each{ ct ->
					CustomTagTreeNode tnode = new CustomTagTreeNode(ct)
					pnode.add(tnode)
				}
			}
		}

		o.getPropertiesByType('collection').sort().each{ c ->
			CollectionTreeNode pnode = new CollectionTreeNode(c)
			onode.add(pnode)
			//add tag nodes if there are tags to be had
			if ( c.hasTags() ){
				c.getTags().each{ ct ->
					CustomTagTreeNode tnode = new CustomTagTreeNode(ct)
					pnode.add(tnode)
				}
			}
		}
		o.getPropertiesByType('groovyrule').sort().each{ r ->
			GroovyRuleTreeNode pnode = new GroovyRuleTreeNode(r)
			onode.add(pnode)
			//add tag nodes if there are tags to be had
			if ( r.hasTags() ){
				r.getTags().each{ ct ->
					CustomTagTreeNode tnode = new CustomTagTreeNode(ct)
					pnode.add(tnode)
				}
			}
		}
		o.getPropertiesByType('rule').sort().each{ r ->
			RuleTreeNode pnode = new RuleTreeNode(r)
			onode.add(pnode)
			//add tag nodes if there are tags to be had
			if ( r.hasTags() ){
				r.getTags().each{ ct ->
					CustomTagTreeNode tnode = new CustomTagTreeNode(ct)
					pnode.add(tnode)
				}
			}
		}
		o.getPropertiesByType('childlink').sort().each{ childProperty ->
			childProperty.getChildren().sort(true, new DLObjectComparator() ).each{ child ->
				if (!o.dlid.equals(child.dlid)){  // avoid endless search for rooms
					onode.add(buildTree(child))
				}
			}
		}
		return onode
	}

	TreeCellEditor getCellEditor() {
		return new ComponentTreeCellEditor(this, super.getCellRenderer())
	}

	boolean isPathEditable(TreePath p) {
		return (p.getLastPathComponent() instanceof SettingTreeNode || p.getLastPathComponent() instanceof CustomTagTreeNode  )
	}

	void componentPropertyChanged(who, String prop, oldVal, newVal) {
		root = new ComponentTreeNode(who, true)
		who.roots.each{ o ->
			root.add(buildTree(o))
		}
		setModel(new DefaultTreeModel(root))
		this.expandAll()
	}

	void expandAll(DefaultMutableTreeNode r = null) {
		DefaultMutableTreeNode root
		if(r == null)
			root = getModel().getRoot()
		else
			root = r

		expandPath(new TreePath(root.getPath() as Object[]))
		root.children().each{ child -> expandAll(child) }
	}


	public void useOldValue(boolean _old){
		editOldValue = _old
	}

	public boolean isEditingOldValue(){
		return editOldValue
	}

}

class ComponentTreeCellEditor extends DefaultTreeCellEditor {
	private static final Logger log = Logger.getLogger(ComponentTreeCellEditor.class.getName())

	//private Property p
	private MutableModelValue p
	boolean useOldValue = false

	ComponentTreeCellEditor(JTree tree, DefaultTreeCellRenderer renderer) {
		super(tree, renderer)
	}

	Object getCellEditorValue() {
		try {

			if ( useOldValue ){
				p.setOldValue(super.getCellEditorValue())
			} else{
				//p.setValue(super.getCellEditorValue())
				//the CE now deals with the override, not the "real" value
				if ( super.getCellEditorValue() == "NULL" ){
					p.setOverride(null)
				} else {
					p.setOverride(super.getCellEditorValue())
				}

			}


		} catch(Exception e) {
			log.error("Can't set " + p.getName() + " to '" + super.getCellEditorValue() + "' because: " + e, 'message')
		}
		return p
	}

	Component getTreeCellEditorComponent(JTree tree, Object value, boolean isSelected, boolean expanded, boolean leaf, int row) {
		p = value.getUserObject()
		return super.getTreeCellEditorComponent(tree, p.getValue(), isSelected, expanded, leaf, row)

	}
}


class ComponentTreeMouseAdapter extends MouseAdapter{
	private ComponentEditorTree tree

	ComponentTreeMouseAdapter(ComponentEditorTree tree) {
		super()
		this.tree = tree
	}

	@Override
	void mousePressed(MouseEvent e) {
		/*
		if ( e.getClickCount() == 3 && e.getButton() != MouseEvent.BUTTON1 ){
			//println "triple right click!"
			TreePath selPath = tree.getPathForLocation(e.getX(), e.getY())
			if(selPath == null) { return }
			tree.useOldValue(true)
			tree.startEditingAtPath(selPath)
			tree.useOldValue(false)
		}
		*/
		this.checkPopup(e);

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		checkPopup(e)
	}


	/**
	 * Construct and display the right-click menu if appropriate.
	 */
	private void checkPopup(MouseEvent e) {

		//println "checking popup ${e.isPopupTrigger()} "

		if(e.isPopupTrigger()) {
			TreePath selPath = tree.getPathForLocation(e.getX(), e.getY())
			if(selPath == null){ return; }

			def clicked = selPath.getLastPathComponent()


			//println clicked.class.getName()
			//println clicked.getUserObject().class.getName()


			if(clicked instanceof SettingTreeNode || clicked instanceof CustomTagTreeNode) {

				MutableModelValue c = (MutableModelValue) clicked.getUserObject()

				JPopupMenu editMenu = new JPopupMenu()

				JMenuItem clearOverrideItem = new JMenuItem("Clear Override")
				clearOverrideItem.addActionListener({ev -> c.setOverride(null)  } as ActionListener)
				editMenu.add(clearOverrideItem)


				JMenuItem clearOldItem = new JMenuItem("Force Re-Export")
				clearOldItem.addActionListener({ev -> c.setOldValue(null) } as ActionListener)
				editMenu.add(clearOldItem)


				//produce final menu
				editMenu.show(tree, e.getX(), e.getY())
			}
		}
	}


}


