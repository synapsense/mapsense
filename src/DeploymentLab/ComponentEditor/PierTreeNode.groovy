package DeploymentLab.ComponentEditor

import javax.swing.tree.DefaultMutableTreeNode
import DeploymentLab.Image.IconNameProvider
import DeploymentLab.channellogger.Logger
import DeploymentLab.Model.*

/**
 * Models a PropertyTO pier in the edit config tree.
 * @author Gabriel Helman
 * @since Jupiter
 * Date: 1/9/12
 * Time: 10:12 AM
 */
 class PierTreeNode extends DefaultMutableTreeNode implements IconNameProvider {

	 private static final Logger log = Logger.getLogger(PierTreeNode.class.getName())

	 PierTreeNode(Pier d) {
		 super(d, false)
		 this.setAllowsChildren(true)
	 }

	 void setUserObject(Object o) {
		 if(o instanceof Dockingpoint) {
			 super.setUserObject(o)
		 } else {
			 try {
				 getUserObject().setValue(o)
			 } catch(Exception e) {
				 log.warn("Can't set property to '$o'\n" + e, 'message')
			 }
		 }
	 }

	 String toString() {
		 Pier d = (Pier)getUserObject()
		 //return d.name + ": " + d.getReferent()?.getDlid()
		 return "${d.name}: ${d.getReferentObject()?.getDlid()}.${d.getReferentName()} [dirty=${d.dirty}]"
	 }

	 String getIconName() {
		 return "ce_pier.png"
	 }


}
