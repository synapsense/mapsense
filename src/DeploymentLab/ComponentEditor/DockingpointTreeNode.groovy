
package DeploymentLab.ComponentEditor

import javax.swing.tree.DefaultMutableTreeNode

import DeploymentLab.Model.*

import DeploymentLab.channellogger.*
import DeploymentLab.Image.IconNameProvider

class DockingpointTreeNode extends DefaultMutableTreeNode implements IconNameProvider {
	private static final Logger log = Logger.getLogger(DockingpointTreeNode.class.getName())

	DockingpointTreeNode(Dockingpoint d) {
		super(d, false)
		this.setAllowsChildren(true)
	}

	void setUserObject(Object o) {
		if(o instanceof Dockingpoint) {
			super.setUserObject(o)
		} else {
			try {
				getUserObject().setValue(o)
			} catch(Exception e) {
				log.warn("Can't set property to '$o'\n" + e, 'message')
			}
		}
	}

	String toString() {
		Dockingpoint d = getUserObject()
		return d.name + ": " + d.getReferrent()?.getDlid() + " [dirty=${d.dirty}]"
	}

	String getIconName() {
		return "ce_dockingpoint.png"
	}
}

