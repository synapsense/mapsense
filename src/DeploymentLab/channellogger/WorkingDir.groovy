package DeploymentLab.channellogger

import DeploymentLab.CentralCatalogue

/**
 * Class to wrap logic around finding the right place on the current OS to store the log and backup files.
 */
public class WorkingDir {

	private ClassLoader cl
	private Properties appProperties = CentralCatalogue.getInstance().getAppProperties()
	private File logFile

	public WorkingDir(){
		logFile = null
	}

	public String getLogPath(){
		if ( logFile == null ){
			logFile = this.getLogFile()
		}
		return logFile.getAbsolutePath()
	}

	public String getBackupDir(){
		def base = appProperties.getProperty("backup.dir")
		if ( logFile == null ){
			logFile = this.getLogFile()
		}
		def grandparent = logFile.getAbsoluteFile().getParentFile().getParentFile()
		String backupDir = grandparent.getAbsolutePath() + File.separator + base
		return backupDir
	}

	public String getWorkingDir(){
		if ( logFile == null ){
			logFile = this.getLogFile()
		}
		return( logFile.getAbsoluteFile().getParentFile().getParentFile() )
	}

	/**
	 * Finds and returns the correct directory to store log files in
	 */
	public File getLogFile(){
		String normalPath = appProperties.getProperty("logging.logfile")
		//try the normal version first
		logFile = new File(normalPath)
		SecurityManager security = System.getSecurityManager();
		if ( ! logFile.exists()  ) {
			println "no logfile, trying to invoke one..."
			try{
				def success
				success = logFile.getParentFile().mkdirs()
				success = logFile.createNewFile()
				if (!success){
					//println ("Dang, no write access for file!")
					throw ( new java.lang.SecurityException("Logfile is in the Application Directory, but I can't write to it.") )
				}
				println "creation of '$logFile' successful!"
			} catch(java.lang.SecurityException se) {
				logFile = planB()
			} catch( IOException ioe ){
				logFile = planB()
			}

		} else if ( ! logFile.canWrite() || (security!= null && ! security.checkWrite(logFile.getAbsolutePath()) ) ) {
			println "bad permissions, PLAN B"
			logFile = planB()

		} else {
			println "logfile exists!"
			//BUT! see if we can REALLY write to it
			try{
				def writer = new BufferedWriter( new FileWriter(logFile, true) )
				writer.newLine()
			} catch (Exception e) {
				println "Write Failure of ${e.getClass().getName()} : ${e.getMessage()}"
				logFile = planB()
			}
		}
		if ( logFile == null ){
			throw ( new NoLogException("Cannot write to or create the log file!") )
		}
		println "logFile determined to be ${logFile.getAbsolutePath()}"
	 return logFile
	}


	private File planB(){
		File planBFile = null
		String fancyPath = ""
		def progdata = System.getenv("PROGRAMDATA")
		if ( progdata != "" && progdata != null ) {
			fancyPath = progdata + File.separator + appProperties.getProperty("application.shortname") + File.separator + appProperties.getProperty("logging.logfile")
		}
		//we don't have write access to our own directory.  This probably means we're on Windows7 or Vista
		//so, we do the fancy stuff
		//println "burn!  gotta go to plan B!"
		if ( progdata != "" && progdata != null ) {
			//println fancyPath
			planBFile = new File( fancyPath )
			if ( ! planBFile.exists() ) {
				try {
					planBFile.getParentFile().mkdirs()
					planBFile.createNewFile()
				} catch(java.lang.SecurityException se2) {
					//println("DAMN, yo!  Problem!")
					throw ( new NoLogException("Cannot write to or create the log file!") )
				}
			}
			if ( ! planBFile.canWrite() ){
				throw ( new NoLogException("Cannot write to or create the log file!") )
			}
		} else {
			//println("DAMN, yo!  Problem!")
			//this means, probably, that PlanB isn't an option as the env variable isn't there (which means we're not on Win7 after all.)
			throw ( new NoLogException("Cannot write to or create the log file!") )
		}
		return ( planBFile )
	}

}