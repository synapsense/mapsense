package DeploymentLab.channellogger

import DeploymentLab.Dialogs.StyledMessage.StyledMessageController

import javax.swing.JOptionPane
import DeploymentLab.CentralCatalogue

/**
 * Displays a message box with HTML styled text. Intended as a styled alternative to the old plain text message logger.
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/23/12
 * Time: 3:41 PM
 */
class StyledMessageLogger implements ChannelListener {

	public static final LOGGERNAME = "html"

	@Override
	void messageReceived(LogData l) {

		int messageType
		String messageTitle
		switch (l.level) {
			case LogLevel.INFO:
				messageType = JOptionPane.INFORMATION_MESSAGE
				messageTitle = CentralCatalogue.getUIS("logger.message.info.title") //Information"
				break
			case LogLevel.WARN:
				messageType = JOptionPane.WARNING_MESSAGE
				messageTitle = CentralCatalogue.getUIS("logger.message.warn.title") //"Warning"
				break
			case LogLevel.ERROR:
			case LogLevel.FATAL:
				messageType = JOptionPane.ERROR_MESSAGE
				messageTitle = CentralCatalogue.getUIS("logger.message.error.title") //"Error"
				break
			default:
				messageType = JOptionPane.INFORMATION_MESSAGE
				messageTitle = CentralCatalogue.getUIS("logger.message.info.title") //"Information"
				break
		}
		StyledMessageController.showStyledMessage(l.message, messageTitle, messageType)


	}
}
