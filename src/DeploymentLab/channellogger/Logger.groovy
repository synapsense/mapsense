
package DeploymentLab.channellogger

import DeploymentLab.UIDisplay

import java.lang.management.ManagementFactory

class ChannelManager {
	protected static ChannelManager instance

	protected listeners = [:]
	protected superListeners = []

	protected ChannelManager() {}

	static ChannelManager getInstance() {
		if(instance == null)
			instance = new ChannelManager()
		return instance
	}

	void addChannelListener(ChannelListener l, String channel = null) {
		if(channel == null) {
			superListeners += l
		} else {
			if(listeners[channel] == null)
				listeners[channel] = []
			listeners[channel] += l
		}
	}

	void removeChannelListener(ChannelListener l, String channel = null) {
		if(channel == null) {
			superListeners -= l
		} else {
			listeners[channel] -= l
		}
	}

	void fireMessage(LogData d) {
		listeners[d.channel].each{ l ->
			l.messageReceived(d)
		}
		superListeners.each{ l ->
			l.messageReceived(d)
		}
	}
}

class Logger {

	protected String name
	protected ChannelManager channelManager

	protected Logger(String n, ChannelManager cm) {
		name = n
		channelManager = cm
	}

	static Logger getLogger(String name) {
		return new Logger(name, ChannelManager.getInstance())
	}

	protected LogData constructLogData(String msg, String chan, LogLevel lvl) {
		LogData ld = new LogData()
		ld.message = msg
		ld.channel = chan
		ld.clazz = name
		ld.level = lvl
		return ld
	}

	void internal(String message, String channel = 'default') {
		channelManager.fireMessage(constructLogData(message, channel, LogLevel.INTERNAL))
	}

	void trace(String message, String channel = 'default') {
		channelManager.fireMessage(constructLogData(message, channel, LogLevel.TRACE))
	}

	void debug(String message, String channel = 'default') {
		channelManager.fireMessage(constructLogData(message, channel, LogLevel.DEBUG))
	}

	void info(String message, String channel = 'default') {
		channelManager.fireMessage(constructLogData(message, channel, LogLevel.INFO))
	}

	void warn(String message, String channel = 'default') {
		channelManager.fireMessage(constructLogData(message, channel, LogLevel.WARN))
	}

	void error(String message, String channel = 'default') {
		channelManager.fireMessage(constructLogData(message, channel, LogLevel.ERROR))
	}

	void fatal(String message, String channel = 'default') {
		channelManager.fireMessage(constructLogData(message, channel, LogLevel.FATAL))
	}

	String getStackTrace(Throwable t) {
		StringWriter sw = new StringWriter()
		PrintWriter pw = new PrintWriter(sw)
		t.printStackTrace(pw)
		return sw.toString()
	}
}

class LogData {
	private static final String LINE_SEP = System.getProperty("line.separator")

	private static final String vmName = ManagementFactory.getRuntimeMXBean().getName()

	private static final LogStyle LOG_STYLE

	static {
		try {
			LOG_STYLE = LogStyle.valueOf( System.getProperty( "synapsense.logstyle", LogStyle.FULL.toString() ).toUpperCase() )
		} catch( IllegalArgumentException e ) {
			println "ERROR: Invalid Log Style specified: " + System.getProperty("synapsense.logstyle")
			LOG_STYLE = LogStyle.FULL
		}
	}

	LogData() {
		date = new Date()
	}

	String toString() {
		if( LOG_STYLE == LogStyle.SHORT ) {
			// Truncated output when things like VM Name aren't needed, like during development.
			return String.format("%02d/%02d %02d:%02d:%02d [%-5s] <%s> {%-45s} : %s%s",
			                     date.getMonth()+1, date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(),
			                     level.name(), channel, clazz, message, LINE_SEP)
		}

		return String.format("%s %s [%-5s] <%s> {%-45s} : %s%s", vmName, date, level.name(), channel, clazz, message, LINE_SEP)
		//return '' + date + ' [' + level.name() + '] <' + channel + '> {' + clazz + '} : ' + message + LINE_SEP
	}

	Date date
	String clazz
	String message
	String channel
	LogLevel level
}

interface ChannelListener {
	void messageReceived(LogData l)
}

enum LogLevel {
	FATAL,
	ERROR,
	WARN,
	INFO,
	DEBUG,
	TRACE,
	INTERNAL
}

enum LogStyle {
	FULL,
	SHORT
}
