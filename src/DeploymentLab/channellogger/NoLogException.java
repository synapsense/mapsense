package DeploymentLab.channellogger;

/**
 * Created by IntelliJ IDEA.
 * User: ghelman
 * Date: Apr 22, 2010
 * Time: 5:07:27 PM
 * If there is no log file present when the app starts and we have no way to construct one, this gets thrown back to main to tell the user about it.
 * Generally, a NoLogException represents a situation where the application can't start due to a log-related issue.
 */
public class NoLogException extends RuntimeException {
	/**
	 * Builds a stock NoLogException.
	 */
	public NoLogException(){
		super();
	}

	/**
	 * Builds a NoLogException with a custom message.  This message will be shown to the user before the application dies all the way.
	 * @param s A message telling the user some details about why the log is in error.
	 */
	public NoLogException(String s){
		super(s);
	}

}
