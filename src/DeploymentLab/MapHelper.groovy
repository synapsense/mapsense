package DeploymentLab;

public class MapHelper {

	/**
	 * I am tired of re-writing this pattern.  So tired.
	 */

	public static addList(Map host, def key, def value){
		if (host.containsKey(key)){
			host.get(key).add( value )
		} else {
			host.put(key,[value])
		}
	}

	public static removeList(Map host, def key, def value){
		if (host.containsKey(key)){
			host.get(key).remove( value )

			if( host.get(key).isEmpty() ) {
				host.remove( key )
			}
		}
	}

	public static increment(Map host, def key){
		if (host.containsKey(key)){
			host[key] += 1
		} else {
		    host[key] = 1
		}

	}

}