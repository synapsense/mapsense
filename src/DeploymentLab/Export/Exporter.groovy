
package DeploymentLab.Export

import com.synapsense.service.Environment
import com.synapsense.service.RulesEngine;
import com.synapsense.service.DLImportService;

import com.synapsense.dto.CRERule;
import com.synapsense.dto.ObjectType
import com.synapsense.dto.PropertyDescr

import com.synapsense.dto.TO
import com.synapsense.dto.TOFactory

import com.synapsense.dto.ValueTO
import com.synapsense.dto.BinaryData

import com.synapsense.dto.TagDescriptor

import com.synapsense.exception.ServerConfigurationException

import DeploymentLab.Model.*
import DeploymentLab.channellogger.*
import com.synapsense.exception.PropertyNotFoundException
import com.synapsense.dto.Relation
import com.synapsense.dto.DLImportResult
import DeploymentLab.FileUtil
import DeploymentLab.CentralCatalogue
import java.text.DateFormat
import com.synapsense.service.impl.dao.to.BinaryDataImpl

public class Exporter {
	private static final Logger log = Logger.getLogger(Exporter.class.getName())

	private ServerContext ctx
	private Environment env
	private ObjectModel omodel

	private exportErrors = []

	private int exportCounter
	private int exportTotal

	Exporter(ServerContext _ctx) {
		ctx = _ctx
		env = ctx.getEnv()
	}

	def getExportErrors() {
		return exportErrors
	}

	/*
	* @return "true" if the ES copy of this project is in sync or the project does not exist in this instance of ES
	*         "false" only if this project exists in the ES and the timestamps do not match
	*/
	boolean checkSync(ObjectModel omodel, boolean showMessageIfInSync = false) {
		StringBuilder msg = new StringBuilder()
		log.debug("checkSync()  roots = ${omodel.roots}")
		log.info("ESAPI: Looking up ROOT object")

		def roots = env.getObjectsByType('ROOT')
		def syncResults = [:]
		if(roots) {
			for( DLObject syncObj : omodel.getObjectsByType( DeploymentLab.Model.ObjectType.DRAWING ) ) {
				syncResults[syncObj] = null
				msg.append("${syncObj.getName()}: \n")

				if( syncObj.key == null || syncObj.key == '' ) {
					log.info("$syncObj had no key, must be the first export.")
					log.info("Setting export timestamp to initial value")
					log.info("ES is in-sync")
					msg.append(CentralCatalogue.getUIS("checkSync.neverExported"))
					msg.append("\n")

					syncResults[syncObj] = true
				} else {
					if( objectExists( syncObj ) ) {
						TO<?> syncObjTO = loadTO( syncObj.key )
						Long esExportTs = env.getPropertyValue( syncObjTO, 'lastExportTs', Long.class )
						Long exportTs = syncObj.getObjectProperty('lastExportTs').getValue()

						log.info("Checking export timestamp of '$exportTs' against server, '$esExportTs'.")
						if (esExportTs != exportTs) {
							log.error("Sync Error! Export timestamp did not match ES export timestamp!")
							log.error("ES is out-of-sync!")
							def df = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL)

							msg.append(String.format(CentralCatalogue.getUIS("checkSync.notInSync"), df.format(new Date(exportTs)), df.format(new Date(esExportTs))))
							msg.append("\n")
							syncResults[syncObj] = false
						} else {
							log.info("ES is in-sync")
							msg.append(CentralCatalogue.getUIS("checkSync.inSync"))
							msg.append("\n")
							syncResults[syncObj] = true
						}
					} else {
						log.warn("$syncObj had a key, but it couldn't be found in the ES!")
						log.info("Updating export timestamp, clearing ES keys.")
						log.info("ES is in-sync")
						msg.append(CentralCatalogue.getUIS("checkSync.freshServer"))
						msg.append("\n")
						syncResults[syncObj] = true
					}
				}

			}
		} else {
			log.error("ROOT object not found, DB Init script must be run.", "message")
			return false
		}

		boolean result = true
		log.debug("results are $syncResults")
		for(boolean v : syncResults.values()){
			if(!v) {result = false}
		}

		if(!result){
			log.error(msg.toString(), "message")
		}
		if(result && showMessageIfInSync){
			log.info(msg.toString(), "message")
		}

		return result
	}

	/**
	 * Checks that DeploymentLab & ES have the same version and updates the config data object.
	 * @return true if the versions are the same, false if they are not (or if there is no root on the server)
	 */
	public boolean checkVersion(){
		return checkVersion(true)
	}

	/**
	 * Checks that DeploymentLab & ES have the same version.
	 * @param updateConfigData if true, update the configdata object in the database as well
	 * @return true if the versions are the same, false if they are not (or if there is no root on the server)
	 */
	public boolean checkVersion(boolean updateConfigData){
		boolean result = true
		log.info("ESAPI: Looking up ROOT object for version check")
		def roots = env.getObjectsByType('ROOT')
		TO<?> rootTO
		if(roots) {
			rootTO = roots[0]
			def dbVersion = ""
			def dlVersion = CentralCatalogue.getApp('db.version')
			try {
				dbVersion = env.getPropertyValue(rootTO, "version", String.class)
			} catch (PropertyNotFoundException pnfe){
				log.warn("database has no version!", "default")
			}
			if ( !dbVersion.equals( dlVersion ) ) {
				String message
				if ( dbVersion == null || dbVersion.length() > 0 ){
					message = String.format( CentralCatalogue.getUIS("exporter.versionMismatch"), CentralCatalogue.getApp('application.shortname'), dlVersion, dbVersion  )
				} else {
					message = String.format( CentralCatalogue.getUIS("exporter.noVersion"), CentralCatalogue.getApp('application.shortname'), dlVersion  )
				}
				log.error(message, "message")
				result = false
			}

			try {
				env.setPropertyValue(rootTO, 'modelVersion', CentralCatalogue.getApp('objects.version'))
			} catch (PropertyNotFoundException e) {
				log.error("Unable to set 'modelVersion' property! Property does not exist!", "message")
				result = false;
			}

			if(updateConfigData){
				this.updateConfigData(rootTO)
			}
		} else {
			log.error(CentralCatalogue.getUIS("exporter.rootIsMissing"), "message")
			return false
		}
		return result
	}

	/**
	 * Installs or updates a CONFIGURATION_DATA object about mapsense in the database
	 *
	 * @since Mars
	 */
	public void updateConfigData(TO<?> rootTO){
		log.info("ESAPI: getting CONFIGURATION_DATA objects")
		def configs = env.getChildren(rootTO, "CONFIGURATION_DATA")
		TO<?> dlConfig = null;
		for( TO<?> config : configs ){
			log.info("ESAPI: checking name of $config")
			def name = env.getPropertyValue(config, "name", String.class)
			if ( name.equals( CentralCatalogue.getApp("application.shortname") ) ){
				dlConfig = config
			}
		}

		def props = []
		props += new ValueTO("name", CentralCatalogue.getApp("application.shortname"))

		def wd = new WorkingDir()
		def p = new Properties()
		p.setProperty("MapSense_Home", wd.getWorkingDir())
		def writer = new StringWriter()
		p.store(writer, null)
		props += new ValueTO("config", writer.toString())

		if ( dlConfig == null ){
			//create
			log.info("ESAPI: creating new CONFIGURATION_DATA")
			dlConfig = env.createObject("CONFIGURATION_DATA", props.<ValueTO>toArray( new ValueTO[0] ) )
			env.setRelation(rootTO,dlConfig)
		} else {
			//update
			log.info("ESAPI: updating CONFIGURATION_DATA")
			env.setAllPropertiesValues(dlConfig, props.<ValueTO>toArray( new ValueTO[0] ) )
		}
	}


	boolean updateExportTs(ObjectModel omodel, ComponentModel cm) {
		log.debug("updateExportTs()  roots = ${omodel.roots}")
		log.info("ESAPI: Looking up ROOT object")

		def roots = env.getObjectsByType('ROOT')
		if(roots) {
			for( DLObject exportObj : omodel.getObjectsByType( DeploymentLab.Model.ObjectType.DRAWING ) ) {
				if(exportObj.key == null || exportObj.key == '') {
					log.warn("$exportObj had no key, must be the first export.")
					//for sure we want to clear any old values in this case
					this.clearForLaunch(omodel,cm,exportObj)
					def rightNow = new Date()
					log.info("Setting export timestamp to initial value of ${rightNow.getTime()} (${rightNow.toString()})")
					exportObj.getObjectProperty('lastExportTs').setValue(rightNow.getTime())
				} else {
					if(objectExists(exportObj)) {
						TO<?> exportObjTO = loadTO(exportObj.key)
						Long esExportTs = env.getPropertyValue(exportObjTO, 'lastExportTs', Long.class)
						Long exportTs = exportObj.getObjectProperty('lastExportTs').getValue()
						if (esExportTs != exportTs) {
							log.error("Sync Error! Export timestamp did not match ES export timestamp!")
							return false
						}
						exportTs = (new Date()).getTime()
						log.info("Timestamp is a match, updating it to $exportTs")
						exportObj.getObjectProperty('lastExportTs').setValue(exportTs)
						env.setPropertyValue(exportObjTO, 'lastExportTs', exportTs)
						exportObj.getObjectProperty('lastExportTs').resetValue()
					} else {
						log.warn("$exportObj object had a key, but it couldn't be found in the ES!")
						log.info("Updating export timestamp, clearing ES keys.")
						exportObj.getObjectProperty('lastExportTs').setValue((new Date()).getTime())
						this.clearForLaunch(omodel,cm,exportObj)
					}
				}
			}
		} else {
			//mapsense should no longer ever create a ROOT object; also the export will have failed by now
			log.error("No Root Object found on the server!", "default")
			return false
		}

		return true
	}

	void makeDefaultTypes(){
		def defaults = CentralCatalogue.getApp('default.object.types').toString().split(",").toList()
		defaults.each{ t->
			log.trace("Adding type $t", 'progress')
			def tempObj = omodel.createObject(t)
			checkCreateType( tempObj )
		}
	}

	/**
	 * Resets a project to the point where it can be cleanly exported as if for the fist time:
	 * Clears all oldValues, ES Keys, makes links dirty, and resets image change tracking
	 * @param om the Object Model to clear
	 * @param cm the Component Model to clear
	 */
	public void clearForLaunch(ObjectModel om, ComponentModel cm, DLObject drawing){
		log.debug("Clearing all oldValues, ES Keys, and image change tracking for $drawing", "default")
		om.clearKeysForDrawing([drawing]) //also handles making links dirty & clears oldValues
		cm.resetImages()
	}

	/**
	 * Export using the DLImportService in ES.
	 * @param om
	 * @param forceExportLinks
	 * @param forceExportRules
	 * @param forceExportObjects
	 * @return
	 * @since Mars
	 */
	boolean dlisExport(ObjectModel om, Boolean forceExportLinks = false, Boolean forceExportRules = false, Boolean forceExportObjects = false, File sourceFile) {
		omodel = om
		// update ES keys temporarily
		//updateESkeys(omodel)
		//the stuff above this is taken from the old version

		//export the used parts of objects.xml:
		def start = Calendar.getInstance()
		DLImportService importer = ctx.getImporter()
		String allObjects = omodel.makeDefinitionFileString(true)

		log.info("Exporting Types to ES", "default")

		def typeImportResults = []
		try{
			typeImportResults = importer.importObjectTypes(allObjects);
			//def typeImportResults = importer.importObjectTypesViaDOM(allObjects);
		} catch (Exception dlie){
			log.error("Unable to Update Object Type Definitions.  Export Cannot Continue.\n${dlie.getMessage()}", "message")
			log.error( dlie.getMessage(), "default" )
			log.error( log.getStackTrace( dlie ) , "default" )
			return false
		}

//		println typeImportResults
		def finish = Calendar.getInstance()
		log.info("object definition export time = ${(finish.getTimeInMillis() - start.getTimeInMillis()) / 1000} seconds" )
		typeImportResults.each { tr ->
			if ( ! tr.isSuccessful() ){
				log.error("Error exporting Object Type definition for ${tr.getTypeName()}", "default")
				log.error( tr.getException().getMessage(), "default" )
				log.error( log.getStackTrace( tr.getException() ), "default" )
				exportErrors += new ExportError( null, "Error exporting Object Type definition for ${tr.getTypeName()}" )
			} else {
				log.debug("Successfully exported Type definition for '${tr.getTypeName()}' ")
			}
		}

		def instanceStart = Calendar.getInstance()
		//types are done, now the objects:

		//String objectsXML = omodel.makeObjectsString()

		List<DLImportResult> resultz = []
		try{
			//resultz = importer.importObjectInstances(objectsXML).toList()

			def fileBytes = FileUtil.getBytesFromFile(sourceFile)
			Map<String,String> control = [:]
			//control["refreshAllRules"] = forceExportRules.toString()

			//get user name
			control["username"] = System.getProperty("user.name").toString()
			//get host name
			control["hostname"] = java.net.InetAddress.getLocalHost().toString()

			resultz = importer.importObjectInstancesZipped(fileBytes, control).toList()

		} catch (Exception dlie){
			log.error( dlie.getMessage() ?: "", "message" )
			log.error( log.getStackTrace( dlie ) , "default" )
			exportErrors += new ExportError( null, dlie.getMessage() )
		}

		//todo: log the export results in some useful way
		log.debug("Results:", "default")
		for ( DLImportResult r : resultz.sort {it.getDlid()} ){
			log.debug( r.toString(), "default" )
		}

		//slug those TOs back into the object model
		for( DLImportResult dlir : resultz ){
			Dlid dlid = Dlid.getDlid(dlir.getDlid())
			TO<?> to = dlir.getKey();
			if ( dlir.isDeleted() ){
				DLObject d = omodel.getObjectDeleted(dlid);
				omodel.prune(d);
				log.trace("object $d has been pruned.")
				continue;
			}

			DLObject o = omodel.getObject(dlid)
			if ( o == null ){
				log.warn("Recieved a import result for a non-existant object with dlid '$dlid'")
				continue
			}
			if ( to ){
				o.setKey( saveTO(to) )
			}
			//we can assume that objects are atomically transactional; that is, if any properties succeeded, they all did
			for ( Setting prop : o.getPropertiesByType('setting') ){
				prop.resetValue()
				if ( dlir.isTagsSuccessful() ){
					if ( prop.hasTags() ){
						for( CustomTag tag : prop.getTags() ){
							tag.resetValue()
						}
					}
				}
			}
			//same deal with parent-child relations
			if ( dlir.isRelationsSuccessful() ){
				o.getPropertiesByType("childlink").each{ cl ->
					((ChildLink)cl).completeAllAdditionAndRemoval()
				}
			} else {
				//log.error("$o relations were not successful", "default")
				exportErrors += new ExportError( o, "Could not set parent-child relationships for object" )
			}
			//and collections + dockingpoints
			if ( dlir.isLinksSuccessful() ){
				for( DeploymentLab.Model.Collection c : o.getPropertiesByType("collection") ){
					c.clean()
				}
				for( DeploymentLab.Model.Dockingpoint d : o.getPropertiesByType("dockingpoint") ){
					d.clean()
				}
				for (Pier p : o.getPropertiesByType("pier")){
					p.clean()
				}
			} else {
				//log.error("$o links were not successful", "default")
				exportErrors += new ExportError( o, "Could not link object" )
			}
			//rules we just want to be able to warn if they didn't work
			if ( ! dlir.isRulesSuccessful() ){
				log.error("$o rules were not successful")
				//exportErrors += new ExportError( o, "Could not bind rules to object" )
			}

			if ( ! dlir.isSuccessful() ){
				//log.error( "Error on object $o: ${dlir.getE().getMessage()}" )
				//log.error(  log.getStackTrace( dlir.getE() ) )
				log.error("Error on object $o")
			}
			if ( dlir.getErrors().size() > 0 ){
				 dlir.getErrors().each{ err ->
					 exportErrors += new ExportError( o, err.getMessage() )
					 if ( err.getCause() != null ){
						 log.error( "Stacktrace printed from from export result:" )
						 log.error( log.getStackTrace(err.getCause()) )
					 }
				 }
			}
		}

        updateCache()

		def stopTime = Calendar.getInstance()
		log.info("Instance export time = ${(stopTime.getTimeInMillis() - instanceStart.getTimeInMillis()) / 1000} seconds")
		log.info("Combined export time spent on server = ${(stopTime.getTimeInMillis() - start.getTimeInMillis()) / 1000} seconds")
		log.info("Time per Object: ${resultz.isEmpty() ? 'N/A - No Results' : ((stopTime.getTimeInMillis() - instanceStart.getTimeInMillis()) / 1000) / resultz.size()}")

		if(exportErrors.size() > 0) {
			return false
		} else {
			// To support SmartZone merge logic, we want to track old values even for unexportable components. This way,
			// we know what the value was at the last export, even if it wasn't exported, and can make some decisions
			// based on that knowledge. However, don't do that for things that were previously exported (objectExists())
			// but aren't this time because they were in something like a non-exporting planning group. Since those
			// match an actual value on the server, we don't want to mess with those.
			for( DLObject o : omodel.getObjects() ) {
				if( !o.isExportable() && !objectExists( o ) ) {
					if( o.isSmartZoneLinked() ) {
						for( Setting prop : o.getPropertiesByType('setting') ) {
							prop.resetValue()
						}
					}  else {
						// Clear the old value for things that have never been exported to clean up our hack.
						for( Setting prop : o.getPropertiesByType('setting') ) {
							prop.setOldValue( null )
						}
					}
				}
			}

			return true
		}
	}

	/**
	 * Attempts to store a DLZ file as a binary blob in the database.
	 * @param f
	 * @return
	 * @since Mars
	 */
	boolean storeDLZFile(File dlzFile) {
		log.trace("storing DLZ file in database: $dlzFile ")

		def fileBytes = FileUtil.getBytesFromFile(dlzFile)
		BinaryData bd = new BinaryDataImpl(fileBytes)

		DLImportService importer = ctx.getImporter()
		def roots = env.getObjectsByType('ROOT')
		if (!roots) { return false; }
		boolean result = false
		for( DLObject dlzStore : omodel.getObjectsByType( DeploymentLab.Model.ObjectType.DRAWING ) ) {
			if (dlzStore.key == null || dlzStore.key == '') { return false }
			if (!objectExists(dlzStore)) { return false }
			TO<?> dlzStoreTO = loadTO(dlzStore.key)
			try {
				log.trace("ENV.storeDLZFile()")
				result = importer.storeDLZFile(dlzStoreTO, bd)
			} catch (Exception e) {
				log.error("Failed to store DLZ file: ${e.getMessage()}", "default")
				log.error(log.getStackTrace(e))
			}
		}
		return result
	}


	/**
	 * Export using direct ES API calls.
	 * @param om
	 * @param forceExportLinks
	 * @param forceExportRules
	 * @param forceExportObjects
	 * @return
	 * @since 5.0
	 */
	boolean export(ObjectModel om, Boolean forceExportLinks = false, Boolean forceExportRules = false, Boolean forceExportObjects = false) {
		log.trace("forceupdate: links $forceExportLinks rules $forceExportRules objects $forceExportObjects ")

		omodel = om
		// update ES keys temporarily
		//updateESkeys(om)

		if (forceExportLinks){
			omodel.makeEverythingDirty() //this handles all links being refreshed
			log.info("Performing a full export on Links.")
		}

		this.makeDefaultTypes()

		log.trace("Unlinking objects...", 'progress')
		omodel.roots.each{ root ->
			recursiveUnlink(root)
		}

		exportTotal = omodel.getObjectCount()

		log.trace("Exporting object settings...", 'progress')
		omodel.roots.each{ root ->
			exportCounter = 1
			recursiveExport(root, forceExportRules, forceExportObjects)
		}

		TO<?> rootTO = env.getObjectsByType('ROOT')[0]

		exportTotal = 2*exportTotal // Because most objects have 2 access paths, 1 through network, 1 through room
		log.trace("Exporting object relationships...", 'progress')
		for(DLObject localRoot: omodel.roots) {
			exportCounter = 1
			TO<?> localRootTO = loadTO(localRoot.key)
			if(! (localRootTO in env.getChildren((TO<?>)rootTO)))
				env.setRelation(rootTO, localRootTO)
			recursiveLink(localRoot)
		}

		log.trace("Updating Parent-Child relationships...", "progress")
		makeAllRelations()

		log.trace("Performing object deletes...", 'progress')
		omodel.listDeletedObjects().each { o ->
			if(o.key != null && o.key != '')
				if(deleteObject(o))
					omodel.prune(o)
		}

		if(exportErrors.size() > 0)
			return false
		else
			return true
	}

	public void stopES() {
		log.info("Stopping ES services...")

		env.configurationStart()
		log.info("ES stopped")
	}

	public void restartES() {
		log.info("Restarting ES services...")
		try {
			//log.trace("Calling env.configurationComplete()", "default")

			env.configurationComplete()
			log.info("ES restarted")
		} catch(ServerConfigurationException e) {
			log.error("ES restarted with errors: " + log.getStackTrace(e))
			exportErrors += new ExportError(null, "Sever restart failed!  Calculation cycles detected.")
		}
	}

	def seen = []
	private void recursiveExport(DLObject o, Boolean forceExportRules, Boolean forceExportObjects ) {

		if(o in seen)
			return
		seen += o

		log.trace("recursiveExport: $o")

		checkCreateType(o)

		try {
			TO<?> to
			def updates = []
			if(exportCounter % 10 == 0)
				log.trace("Updating properties of object $exportCounter/$exportTotal", 'progress')
			exportCounter ++

			if(objectExists(o)) {
				def tags = []
				o.getPropertiesByType('setting').each{ prop ->
					if(prop.valueChanged() || forceExportObjects ) {
						log.trace("Property ${prop.name} needs update: '${prop.getValue()}'")
						if(prop.getValue() != null) { // TODO: figure out why this if is necessary
							updates << new ValueTO(prop.name, prop.getValue())
						}
					}
					//check for tags and roll up tag objects
					if ( prop.hasTags() ){
						prop.getTags().each{ tag ->
							if ( tag.valueChanged() ){
								tags += tag.makeESTag()
							}
						}
					}


				}
				to = loadTO(o.key)
				if ( updates.size() > 0 ) {
					log.debug("ESAPI: updating $to with $updates")
					env.setAllPropertiesValues(to, (ValueTO[])updates)
				}

				//now load in the tags for this object
				if ( tags.size > 0 ) {
					log.trace("ESAPI: Updating tags on $to with ${tags.collect{ it.getTagName() + ':' + it.getValue() }} ")
					env.setAllTagsValues( to, tags )
				}

				//the contents of rules only change when we kick out a new version,
				//so we only need to push them to the server when a new object is created OR after an upgrade
				//if ( forceExportRules ) {


				//now that the attachCRERule method is a little smarter, there's no reason not to just always call it;
				//this way, we can repair busted rules on a re-export if we need to

					o.getPropertiesByType('groovyrule').each{  grule ->
						//log.trace("updating groovy rule $grule")
						attachCRERule(to, grule, forceExportRules)
					}
				//}

			} else {
				o.makeDirty() //since this hasn't been exported, make sure there aren't any erroneous (not) dirty flags

				def tags = []
				o.getPropertiesByType('setting').each{ prop ->
					if(prop.getValue() != null) { // TODO: figure out why this if is necessary
						updates << new ValueTO(prop.name, prop.getValue())
					}
					//check for tags and roll up tag objects
					if ( prop.hasTags() ){
						prop.getTags().each{ tag ->
							tags += tag.makeESTag()
						}
					}
				}
					
				o.getPropertiesByType('childlink').each{ childlink ->
					childlink.clearOldChildren()
				}
				log.debug("ESAPI: Creating object instance of type '${o.type}' with properties: ${updates}")
				to = env.createObject(o.type.toUpperCase(), (ValueTO[])updates)
				//save key here, in case the tags or rule commands break
				o.key = saveTO(to)

				//attach tags for this object (if any)
				if ( tags.size > 0 ) {
					log.trace("ESAPI: Creating tags on $to with ${tags.collect{ it.getTagName() + ':' + it.getValue() }} ")
					env.setAllTagsValues( to, tags )
				}

				o.getPropertiesByType('rule').each{ prop ->
					log.trace("binding Rule $prop")

					//can't do this check anymore since not all inputs to Rules are dockingpoints.  Also, this should all be screened for somewhere else
//					if(prop.bindings.every{bvar, plink -> o.getObjectProperty(plink).getReferrent() != null})
//						attachCRERule(to, prop, forceExportRules)
//					else
//						log.debug("Skipping binding of rule '${prop.name}' because one of the inputs was null.")


					def hasNulls = false
					prop.bindings.each{bvar, plink ->
						if ( o.getObjectProperty(plink) instanceof Dockingpoint && o.getObjectProperty(plink).getReferrent() == null ){
							hasNulls = true
						}
					}
					if ( ! hasNulls ){
						attachCRERule(to, prop, forceExportRules)
					} else {
						log.debug("Skipping binding of rule '${prop.name}' because one of the inputs was null.")
					}

				}
				o.getPropertiesByType('groovyrule').each{ grule ->
					attachCRERule(to, grule, forceExportRules)
				}
				//o.key = saveTO(to)
				
			}

			o.getPropertiesByType('setting').each{ prop ->
				prop.resetValue()
				if ( prop.hasTags() ){
					prop.getTags().each{ tag ->
						tag.resetValue()
					}
				}

			}

			o.getChildren().each{ child ->
				recursiveExport(child, forceExportRules, forceExportObjects)
			}

			o.getPropertiesByType('collection').each{ collection ->
				collection.items.each{ it ->
					if(!objectExists(it))
						recursiveExport(it, forceExportRules, forceExportObjects)
				}
			}

			o.getPropertiesByType('dockingpoint').each{ docking ->
				//log.trace("Walking docking ${docking.name}")
				DLObject referrent = docking.getReferrent()
				if(referrent != null && !objectExists(referrent))
					recursiveExport(referrent, forceExportRules, forceExportObjects)
			}




		} catch(Exception e) {
			log.error("Export of object $o failed with exception: " + log.getStackTrace(e))
			exportErrors += new ExportError(o, "Couldn't export")
		}
	}

	def unlinkSeen = []
	private void recursiveUnlink(DLObject o) {
		if(o.key == null || o.key == '')
			return

		if(o in unlinkSeen)
			return
		unlinkSeen += o

		TO<?> to = loadTO(o.key)

		try {
			o.getPropertiesByType('childlink').each{ childlink ->
				childlink.getRemovedChildIds().each{ idToRemove ->
					def toRemove = omodel.getObjectDeleted(idToRemove)
					if(toRemove == null || toRemove.key == null) {
						log.warn("Pruning non-existent child $idToRemove")
						childlink.completeRemoval(idToRemove)
					} else {
						log.debug("ESAPI: Removing child link from my TO: '$to' to TO: '" + toRemove.key + "'")
						try {
							env.removeRelation(to, loadTO(toRemove.key))
							childlink.completeRemoval(idToRemove)
						} catch(Exception e) {
							log.error("Removing child link from TO '$to' to TO '${toRemove.key}' failed: " + log.getStackTrace(e))
							exportErrors += new ExportError(o, "Couldn't unlink")
						}
					}
				}
			}
		} catch(Exception e) {
			log.error("Removing children of $o failed with exception: " + log.getStackTrace(e))
			exportErrors += new ExportError(o, "Couldn't unlink")
		}

		o.getChildren().each{ childObject ->
			if(childObject!=null && childObject.key != null && childObject.key != '')
				recursiveUnlink(childObject)
		}

		o.getPropertiesByType('collection').each{ collection ->
			collection.items.each{ it ->
				recursiveUnlink(it)
			}
		}

		o.getPropertiesByType('dockingpoint').each{ docking ->
			DLObject referrent = docking.getReferrent()
			if(referrent != null)
				recursiveUnlink(referrent)
		}
	}

	def linkSeen = []
	private void recursiveLink(DLObject o) {
		if(o.key == null || o.key == '') {
			log.error("Can't link object with no TO!")
			exportErrors += new ExportError(o, "No TO")
			return
		}

		if(o in linkSeen)
			return
		linkSeen += o

		TO<?> to = loadTO(o.key)

		if(exportCounter % 10 == 0)
			log.trace("Linking object $exportCounter/$exportTotal", 'progress')
		exportCounter ++

		try {
			for(ChildLink childlink: o.getPropertiesByType('childlink')) {
				for(Dlid idToAdd: childlink.getAddedChildIds()) {
					log.debug("idToAdd:" + idToAdd)
					def toAdd = omodel.getObject(idToAdd)
					if(toAdd.key == null || toAdd.key == '') {
						log.debug("Not attempting to link to object '${toAdd}' because TO is '${toAdd.key}'!")
					} else {

//if ( ! to.toString().equals( childlink.owner.getKey() ) ){
//	log.error( "DANGER:  TO '$to' != childlink.owner.getKey() '${childlink.owner.getKey()}'" )
//}


//						relations += new Relation(loadTO(childlink.owner.getKey()), loadTO(toAdd.key) )
//						log.debug("Relation(): Adding child link from my TO: '${childlink.owner.getKey()}' to TO: '${toAdd.key}'")

						//should really be this instead:
						//relations += new Relation(to, loadTO(toAdd.key) )
						//updatedChildLinks.add( childlink )

						def esRelation = new Relation(to, loadTO(toAdd.key) )
						updatedRelations += new RelationLot( esRelation, childlink, idToAdd )

						log.debug("Relation(): Adding child link from my TO: '$to' to TO: '${toAdd.key}'")




						/*
						log.debug("ESAPI: Adding child link from my TO: '$to' to TO: '${toAdd.key}'")
						try {
							env.setRelation(to, loadTO(toAdd.key))
							childlink.completeAddition(idToAdd)
						} catch(Exception e) {
							log.error("Adding child link from TO '$to' to TO '${toAdd.key}' failed: " + log.getStackTrace(e))
							exportErrors += new ExportError(o, "Couldn't link")
						}
                        */


					}
				}
			}
		} catch(Exception e) {
			log.error("Adding children with $o failed with exception: " + log.getStackTrace(e))
			exportErrors += new ExportError(o, "Couldn't link")
		}



		try {
			def refs = []
			for(Dockingpoint docking: o.getPropertiesByType('dockingpoint')) {
				if(! docking.isDirty())
					continue
				if(docking.getReferrent() == null)
					refs << new ValueTO(docking.name, null)
				else
					refs << new ValueTO(docking.name, loadTO(docking.getReferrent().key))
			}

			if(refs.size() > 0) {
				log.debug("ESAPI: Updating $to references to ${refs}")
				env.setAllPropertiesValues(to, (ValueTO[])refs)
				for(Dockingpoint docking: o.getPropertiesByType('dockingpoint'))
					docking.clean()
			}
		} catch(Exception e) {
			log.error("Linking object $o failed with exception: " + log.getStackTrace(e))
			exportErrors += new ExportError(o, "Couldn't associate")
		}

		try {
			def collections = []
			for(DeploymentLab.Model.Collection collection: o.getPropertiesByType('collection')) {
				if(collection.isDirty()) {
					def values = collection.items.collect{ it -> loadTO(it.key) } // TODO: add error checking for objects that failed to export and have no key
					collections << new ValueTO(collection.name, values)
				}
			}
			if(collections.size() > 0) {
				log.debug("ESAPI: Updating $to collections to ${collections}")
				env.setAllPropertiesValues(to, (ValueTO[])collections)
				for(DeploymentLab.Model.Collection collection: o.getPropertiesByType('collection'))
					collection.clean()
			}
		} catch(Exception e) {
			log.error("Updating collection in '$o' failed with exception: " + log.getStackTrace(e))
			exportErrors += new ExportError(o, "Couldn't update collection")
		}

		// BEGIN UGLY KLUDGE TO EXPORT NETWORK BEFORE ZONE
		def children = o.getChildren()

		for(DLObject childObject: children) {
			if(childObject==null)
				log.info("null childobject")
			else{
				if(childObject.key != null && childObject.key != '')
					recursiveLink(childObject)
			}
		}

		// END KLUDGE

		for(DeploymentLab.Model.Collection collection: o.getPropertiesByType('collection')) {
			for(DLObject it: collection.getItems()) {
				recursiveLink(it)
			}
		}

		for(Dockingpoint docking: o.getPropertiesByType('dockingpoint')) {
			DLObject referrent = docking.getReferrent()
			if(referrent != null)
				recursiveLink(referrent)
		}
	}


	//private Set<ChildLink> updatedChildLinks = new HashSet<ChildLink>();
	//private List<Relation> relations = []

	private List<RelationLot> updatedRelations = []


	/**
	 * Uses the list of childlinks found in recursiveLink to set all at once
	 */
	private void makeAllRelations(){
		//if ( relations.size() == 0 ){ return; }
		if ( updatedRelations.size() == 0 ){ return; }

/*
		for ( ChildLink cl : updatedChildLinks ) {
			for(Dlid idToAdd: cl.getAddedChildIds()) {
				//log.debug("idToAdd:" + idToAdd)
				def toAdd = omodel.getObject(idToAdd)
				if(toAdd.key == null || toAdd.key == '') {
					log.debug("Not attempting to link to object '${toAdd}' because TO is '${toAdd.key}'!")
				} else {

					relations += new Relation(loadTO(cl.owner.getKey()), loadTO(toAdd.key) )
					log.debug("Relation(): Adding child link from my TO: '${cl.owner.getKey()}' to TO: '${toAdd.key}'")


					try {
						env.setRelation(to, loadTO(toAdd.key))
						childlink.completeAddition(idToAdd)
					} catch(Exception e) {
						log.error("Adding child link from TO '$to' to TO '${toAdd.key}' failed: " + log.getStackTrace(e))
						exportErrors += new ExportError(o, "Couldn't link")
					}


				}
			}
		}
*/

		try{
			log.debug("ESAPI: Setting all Relations", "default")
			def serverRelations = updatedRelations.collect { it.relation }
			env.setRelation( serverRelations )
			//env.setRelation( relations )
		} catch ( Exception ex ){
			log.error("Error Setting Relationships: ${ex.getMessage()}", "default")
			log.error( log.getStackTrace(ex))
			exportErrors += new ExportError(null, "Couldn't create relationships: ${ex.getMessage()}")
			return
		}

		//setRelation(<Collection>) is atomic, so either it all worked or none of it did
		//this means that if we got to here, we can consider all links as having been updated
		for ( RelationLot rl : updatedRelations ){
			rl.childLink.completeAddition( rl.childId )
		}

	}

	//def existingTypes = ['WSNNETWORK', 'WSNNODE', 'WSNSENSOR']
	def existingTypes = []
	private void checkCreateType(DLObject object) {

		if(object == null){
			log.info("checkCreateType: ${object.toString()} is null")
			return
		}
		String typeName = object.type.toUpperCase()

		if(typeName in existingTypes) {
			//log.trace("Type '$typeName' has already been created/updated")
			return
		}

		//log.debug("Creating/Updating object type '$typeName'")

		HashMap<String, PropertyDescr> properties = new HashMap<String, PropertyDescr>()

		object.getPropertiesByType('setting').each{ prop ->
			properties[prop.name] = new PropertyDescr(prop.name, prop.valueType, prop.historic)

			//check for tags!
			if( prop.hasTags() ) {
				//add them to the type definition
				prop.getTags().each{ tag ->
					//log.trace("Attaching tag '${tag.getTagName()}' of type ${tag.getTagType()}")
					def tagDesc = new TagDescriptor( tag.getTagName() , tag.getTagType() )
					properties[prop.name].getTagDescriptors().add( tagDesc )
				}
			}

		}

		object.getPropertiesByType('dockingpoint').each{ prop ->
			properties[prop.name] = new PropertyDescr(prop.name, TO.class.getName())
		}

		object.getPropertiesByType('rule').each{ prop ->
			properties[prop.name] = new PropertyDescr(prop.name, prop.valueType, prop.historic)
		}

		object.getPropertiesByType('groovyrule').each{ prop ->
			properties[prop.name] = new PropertyDescr(prop.name, prop.valueType, prop.historic)
		}

		object.getPropertiesByType('collection').each{ prop ->
			properties[prop.name] = new PropertyDescr(prop.name, TO.class.getName(), false, true)
		}

		try {
			log.debug("ESAPI: Checking for existance of type '$typeName'")
			ObjectType type = env.getObjectType(typeName)
			if(type == null) {
				log.debug("ESAPI: Creating new type '$typeName'")
				type = env.createObjectType(typeName)

				if(properties.size() > 0) {
					type.getPropertyDescriptors().addAll(properties.values())
					env.updateObjectType(type)
				} else {
					log.error("Creating nonsense type '$typeName' with no properties")
				}
			} else {
				log.debug("ESAPI: Updating type '${type.getName()}'")
				Set<PropertyDescr> descrs = type.getPropertyDescriptors()
				descrs.clear()
				descrs.addAll(properties.values())
				env.updateObjectType(type)
			}
			existingTypes += typeName
		} catch(Exception e) {
			log.error("Exception creating/updating type ${typeName}: " + log.getStackTrace(e))
			exportErrors += new ExportError(object, "Couldn't create/update type")
		}
	}

	private boolean deleteObject(DLObject o) {
		try {
			TO<?> to = loadTO(o.key)

			int deleteLevel = 0
			if ( o.hasObjectProperty("name") ){
				Setting n = (Setting) o.getObjectProperty("name")
				if ( n.hasTag("cascadeDelete") ){
					deleteLevel = n.getTag("cascadeDelete").getValue()
				}
			}
			log.debug("ESAPI: Deleting object '$o' with TO '$to' at a cascade level of $deleteLevel")
			env.deleteObject(to, deleteLevel)
			//attached rule objects are now deleted by ES automatically
			toCache.remove(o.key)
			return true
		} catch(Exception e) {
			log.error("Exception deleting object '$o' :" + log.getStackTrace(e))
			exportErrors += new ExportError(o, "Couldn't delete")
			return false
		}
	}


	// Utility functions
	private HashMap<String, TO<?> > toCache = new HashMap<String, TO<?> > ()
	private TO<?> loadTO(String key) {
		if(key == null || key == '')
			throw new NullPointerException("Can't load empty TO!")
		TO<?> to = toCache[key]
		if(to == null) {
			to = TOFactory.getInstance().loadTO(key)
			toCache[key] = to
		}
		return to
	}

	private String saveTO(TO<?> to) {
		if(to == null)
			throw new NullPointerException("Can't save empty TO!")
		String ser = TOFactory.getInstance().saveTO(to)
		toCache[ser] = to
		return ser
	}

	private HashMap<String, Collection<TO<?> > > existsCache = new HashMap<String, Collection<TO<?> > >()  // map typeName to a Collection of TOs

    private void updateCache(){
        log.trace("Update existCache entries")
        for(String typeName : existsCache.keySet()){
            try {
                log.trace("Update $typeName type")
                existsCache[typeName] = env.getObjectsByType(typeName)
            } catch(Exception e) {
                throw new Exception("Unexpected exception '" + e.getCause() + "' trying to update cache values!", e)
            }
        }
    }

	private void rememberObject(DLObject o) {
		if(o.key != null && o.key != '') {
			TO<?> to = loadTO(o.key)
			String typeName = to.getTypeName()
			existsCache[typeName].add(to)
		}
	}
	public boolean objectExists(DLObject o) {
		if(o.key == null || o.key == '') {
			//log.trace("Empty TO key, not bothering to look up.")
			return false
		}

		TO<?> to = loadTO(o.key)
		String typeName = to.getTypeName()
		if(! existsCache.containsKey(typeName)) {
			log.debug("ESAPI: Querying for instances of type '$typeName'")
			try {
				existsCache[typeName] = env.getObjectsByType(typeName)
			} catch(Exception e) {
				throw new Exception("Unexpected exception '" + e.getCause() + "' trying to determine if an object existed!", e)
			}
		}

		return existsCache[typeName].contains(to)
	}


	///New RulesEngine API support for both Java and Groovy-based rules:

	/**
	 * Map of rule type names to their TO.  This is populated at export time to see which rule types have already been created.
	 * The type name is provided from the rule itself via CREMember.getServerTypeName() 
	 */
	private Map<String, TO<?>> ruleTypeIndex = [:]  //[typename : TO]

	/**
	 * Ensures that a given CREMember has a type on the ES.  If the RuleType is not present, a new one is created and the TO returned.
	 * If the type is already present, a cached copy of the TO is returned.
	 * @param ruleProperty the CREMember (either a Rule or GroovyRule) to load on ES
	 * @return the TO of the RuleType object on the server
	 */
	private TO<?> loadCRERuleType(CREMember ruleProperty) {
		try{
			//log.trace("Checking for Rule Type $ruleProperty")
			RulesEngine re = ctx.getCRE()
			if ( ruleTypeIndex.containsKey( ruleProperty.getServerTypeName() ) ){
				return ( ruleTypeIndex[ruleProperty.getServerTypeName()] )
			}
			def ruleTO = re.getRuleTypeId( ruleProperty.getServerTypeName() )

			ruleTypeIndex[ ruleProperty.getServerTypeName() ] = ruleTO
			//return ruleTO

			//if (ruleProperty instanceof Rule){ return ruleTO }

			def rt = ruleProperty.createRuleType()
			if ( ruleTO ){
				log.trace("ESAPI: updating Rule Type $rt with source ${ruleProperty.sourceName}")
				re.updateRuleType( ruleTO, rt )
			} else {
				log.trace("ESAPI: creating Rule Type $rt")
				ruleTO = re.addRuleType(rt)
			}
			ruleTypeIndex[ ruleProperty.getServerTypeName() ] = ruleTO
			return ruleTO
		} catch (java.lang.IllegalStateException ise){
			log.error("Error with CRE: ${ise.getMessage()}", "message")
			log.error( log.getStackTrace(ise), "default" )
		}
	}

	/**
	 * Creates a CRE rule object and links it into the rest of the server's data model.
	 * @param destTO The TO of the server object that contains the destination property of the rule.
	 * @param ruleProperty The rule property itself from the DL object model.
	 * @param updateAllRules If true, the rule instances will be rebuilt (and deleted first, if necessary)
	 */
	void attachCRERule(TO<?> destTO, CREMember ruleProperty, boolean updateAllRules){
		RulesEngine re = ctx.getCRE()
		String ruleName = ruleProperty.getServerInstanceName()

		def typeTO = loadCRERuleType(ruleProperty)

		if ( updateAllRules && ruleProperty.getOwner().hasKey() ){
			//in this case, scrub out the old rule object first
			ruleName = destTO.getID() + ruleProperty.name
			log.trace("ESAPI: Removing old instance of rule $ruleName")
			re.removeRule( ruleName )
		}

//		if( ruleName == null || updateAllRules ){
//		if( ruleName == null ){
			//log.trace("Binding rule (" + ruleProperty.getSourceName() + ") to object '$destTO' property '" + ruleProperty.name + "'")
			ruleName = destTO.getID() + ruleProperty.name
			ruleProperty.setServerInstanceName(ruleName)
    		def ruleTO = re.getRuleId(ruleName)
			if ( ! ruleTO ) {
				log.trace("Binding rule (" + ruleProperty.getSourceName() + ") to object '$destTO' property '" + ruleProperty.name + "'")
				CRERule rule = new CRERule( ruleName, typeTO, destTO, ruleProperty.name  )
				ruleProperty.bindings.each{ boundVar, objectProperty ->
					log.trace("Binding rule variable '$boundVar' to object property '$objectProperty'")
					rule.addSrcProperty( destTO, objectProperty, boundVar )
				}
				log.trace("ESAPI: Adding rule (${ruleProperty.getSourceName()}) to Rules Engine.")
				re.addRule(rule)
			}
//		}
	}

	private String readFile(File f) {
		BufferedReader reader
		if(f.canRead()) {
			reader = new BufferedReader(new FileReader(f))
		} else {
			log.warn("Can't read '" + f.getPath() + "' from filesystem, defaulting to JAR file.")
			try {
				ClassLoader cl = this.getClass().getClassLoader()
				String path = f.getPath().replace('\\', '/')
				InputStream is = cl.getResourceAsStream(path)
				reader = new BufferedReader(new InputStreamReader(is))
			} catch(Exception e) {
				log.error("Error loading source file " + f.getPath() + " : " + log.getStackTrace(e))
				return null
			}
		}
		StringBuffer sb = new StringBuffer()
		String line
		String sep = System.getProperty("line.separator")
		while((line = reader.readLine()) != null) {
			sb.append(line)
			sb.append(sep)
		}
		return sb.toString()
	}

        /*
         *  Get Lookup table for WSN Model Migration. Lookup table is the relationship between old ES object IDs and new ones
        */
       private HashMap<String, String> migrationMap = new HashMap<String, String>()
       private boolean generateWsnModelMigrationMap(TO<?> root){
           BinaryData binaryDataMap = env.getPropertyValue(root, "wsnUpgrade", BinaryData.class)
		   if(binaryDataMap == null){
			   return false
		   }
           String strResult = new String(binaryDataMap.getValue())
           String[] stringMap = strResult.split("\n")

           for(String migratedResult: stringMap){
        	   String[] idArray = migratedResult.split(",")
               
               if(idArray.length==2){
                   String oldId = idArray[0].trim()
                   String newId = idArray[1].trim()
                   migrationMap.put(oldId, newId)
               }
           }
           
           //put gateway es key manually
           omodel.getObjectsByType("wsngateway").each{o->
        	   String esKey = o.key
        	   String[] keyArray = esKey.split(":")
        	   
        	   if(keyArray.length==2 && keyArray[0].toLowerCase().equals("gateway")){
        		   String newEsKey = "WSNGATEWAY:" + keyArray[1]
        		   migrationMap.put(esKey, newEsKey)
        	   }
           }

		   return true
       }
       /*
        * Save new ES key id by old ES key
       */
       private void saveMigratedKey(DLObject o){
    	   if(o.key == null || o.key == '')
    		   return
   			
    	   String oldKey = o.key
    	   String newKey = ""
           if(migrationMap.containsKey(oldKey)){
                newKey = migrationMap.get(oldKey)
                o.key = newKey
                log.info("Migrates oldKey:" + oldKey + " to newKey " + newKey)
           }
       }
       
       /*
        * update all ES key for wsn model migration
        */
        public void updateESkeys(ObjectModel om){
        	omodel = om
        	TO<?> rootTO = env.getObjectsByType('ROOT')[0]

			//check for the wsnupgrade property
			try {
				def descr = env.getPropertyDescriptor('ROOT', 'wsnUpgrade')
			} catch (PropertyNotFoundException pnfe){
				log.info("No wsnUpgrade field found on the server, not doing the WSN Key Migration", "default")
				return
			}


        	def mapExists = generateWsnModelMigrationMap(rootTO)  // generate lookup table for WSN Model migration
			if(mapExists){
				omodel.getObjects().each{o ->
					saveMigratedKey(o)
				}
				omodel.listDeletedObjects().each{d ->
					saveMigratedKey(d)
				}
			} else {
				log.info("Migration Map was empty, not doing the WSN Key Migration", "default")
			}
        }

    public boolean exportAccessModel(AccessModel accessModel) {
       log.info("Exporting Access Model to ES", "progress")
        omodel = accessModel.objectModel

        //export the used parts of objects.xml:
        def start = Calendar.getInstance()
        DLImportService importer = ctx.getImporter()
        String allObjects = omodel.makeDefinitionFileString(true)

        //log.trace(allObjects)

        def typeImportResults = []
        try {
            typeImportResults = importer.importObjectTypes(allObjects);
            //def typeImportResults = importer.importObjectTypesViaDOM(allObjects);
        } catch (Exception dlie) {
            log.error("Unable to Update Object Type Definitions.  Export Cannot Continue.\n${dlie.getMessage()}", "message")
            log.error(dlie.getMessage(), "default")
            log.error(log.getStackTrace(dlie), "default")
            return false
        }

        def instanceStart = Calendar.getInstance()
        //types are done, now the objects:

        String objectsXML = omodel.makeObjectsString()
        //log.trace(objectsXML)

        List<DLImportResult> resultz = []
        try {
            resultz = importer.importObjectInstances(objectsXML).toList()
        } catch (Exception dlie) {
            log.error(dlie.getMessage() ?: "", "message")
            log.error(log.getStackTrace(dlie), "default")
            return false
        }

        log.debug("Results:", "default")
        for ( DLImportResult r : resultz.sort {it.getDlid()} ){
            log.debug( r.toString(), "default" )
        }

        //slug those TOs back into the object model
        for( DLImportResult dlir : resultz ){
            Dlid dlid = Dlid.getDlid(dlir.getDlid())
            TO<?> to = dlir.getKey();
            if ( dlir.isDeleted() ){
                DLObject d = omodel.getObjectDeleted(dlid);
                omodel.prune(d);
                accessModel.deleteObject(dlid)
                log.trace("object $d has been pruned.")
                continue;
            }

            DLObject o = omodel.getObject(dlid)
            if ( o == null ){
                log.warn("Recieved a import result for a non-existant object with dlid '$dlid'")
                continue
            }
            if ( to ){
                o.setKey( saveTO(to) )
            }
        }

        updateCache()

        log.info("Exporting AccessModel done!")
        return true
    }

	public void updateModelVersion(ObjectModel omodel){
		def version = CentralCatalogue.getApp("model.version")

		for(DLObject o : omodel.getObjectsByType( DeploymentLab.Model.ObjectType.DRAWING )){
			o.getObjectSetting("modelVersion").setValue(version)
		}
	}

}

class ExportError {
	DLObject object
	String message

	ExportError(DLObject o, String m) {
		object = o
		message = m
	}
}

