package DeploymentLab.Export

import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.channellogger.Logger

/**
 * @author Vadim Gusev
 * @since Europa
 * Date: 10.04.13
 * Time: 10:59
 */
class DLComponentValueExporter {
	private static final Logger log = Logger.getLogger(DLComponentValueExporter.class.getName());

	private static final String[] DEFAULT_PROPERTIES = ["DLID", "Drawing Name", "Zone Name", "Network Name", "Component Type"]

	public static Map<String, List<String[]>> exportComponents(ComponentModel cmodel, Set<String> cTypes){
		log.trace("Exporting components... $cTypes")
		Map<String, List<String[]>> result = new HashMap<>();

		for (String type : cTypes){
			log.trace("Collect components of type '$type'")
			List<String[]> typeResult = new ArrayList<>();

			String displayTypeName = cmodel.getTypeDisplayName(type);
			if (displayTypeName == null || displayTypeName.isEmpty()){
				log.warn("Unable to export component type $type. Type not found!")
				continue;
			}
			displayTypeName = displayTypeName.replaceAll("[/:]", " ");
			if (displayTypeName.length() > 31){
				displayTypeName = displayTypeName.replaceAll("[ a-z]", "");
			}

			Map<String, String> displayProp = cmodel.getTypeDisplayPropNames(type);
			List<String> headers = new ArrayList<>();
			headers.addAll(DEFAULT_PROPERTIES);
			headers.addAll(displayProp.values());
			typeResult.add(headers.toArray(new String[headers.size()]));
			log.trace("Type properties: ${displayProp.values()}")

			for(DLComponent c : getComponents(cmodel, type)){
				List<String> row = new ArrayList<>();
				row.add(c.getDlid().toString());
				row.add((String)c.getDrawing().getPropertyValue("name"))
				def zone = c.getParentsOfRole("zone")
				zone.addAll(c.getParentsOfRole("set"))
				(zone.isEmpty())?row.add(null):row.add((String)zone.get(0).getPropertyValue("name"))
				def network = c.getParentsOfRole("network")
				(network.isEmpty())?row.add(null):row.add((String)network.get(0).getPropertyValue("name"))
				row.add(type)
				for (String prop : displayProp.keySet()){
					row.add(c.getPropertyValue(prop).toString())
				}
				typeResult.add(row.toArray(new String[row.size()]))
			}
			result.put(displayTypeName, typeResult)
		}

		return result;
	}

	private static List<DLComponent> getComponents(ComponentModel componentModel, String type) {
		List<DLComponent> components  = componentModel.getComponentsByType( type );

		components.sort { a, b ->
				a.getName() <=> b.getName()
		}

		return components
	}
}
