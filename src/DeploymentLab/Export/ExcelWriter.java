package DeploymentLab.Export;

import DeploymentLab.channellogger.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Vadim Gusev
 * @since Europa
 * Date: 08.04.13
 * Time: 16:00
 */
public class ExcelWriter {
	private static final Logger log = Logger.getLogger(ExcelWriter.class.getName());
	public static final String DEFAULT_SHEET_NAME = "Sheet1";

	public static  void write (String path, List<String[]> value) throws IOException, InvalidFormatException {
		write(path, DEFAULT_SHEET_NAME, value);
	}

	public static  void write (String path, String sheetName, List<String[]> value) throws IOException, InvalidFormatException {
		Map<String, String[]> headers = new HashMap<>() ;
		Map<String, List<String[]>> values = new HashMap<>();
		values.put(sheetName, value);
		write(path, new String[]{sheetName}, headers, values);
	}

	public static  void write (String path, String sheetName, String[] header, List<String[]> value) throws IOException, InvalidFormatException {
		Map<String, String[]> headers = new HashMap<>() ;
		Map<String, List<String[]>> values = new HashMap<>();
		headers.put(sheetName, header);
		values.put(sheetName, value);
		write(path, new String[]{sheetName}, headers, values);
	}

	public static void write (String path, Map<String, List<String[]>> values) throws IOException, InvalidFormatException{
		write(path, values.keySet().toArray(new String[values.size()]), new HashMap<String, String[]>(), values);
	}

	public static void write (String path, String[] sheetNames, Map<String, String[]> headers, Map<String, List<String[]>> values) throws IOException, InvalidFormatException {
		Workbook wb = new XSSFWorkbook();

		for(String sheetName : sheetNames){
			Sheet sheet = wb.createSheet(sheetName);
			int rownum = 0;

			String[] titles = headers.get(sheetName);
			if (titles != null && titles.length!=0){
				Row headerRow = sheet.createRow(rownum);
				for (int i = 0; i < titles.length; i++) {
					Cell cell = headerRow.createCell(i);
					cell.setCellValue(titles[i]);
				}
				rownum++;
			}

			Row row;
			Cell cell;
			List<String[]> data = values.get(sheetName);
			if (data != null && !data.isEmpty()){
				for (int i = 0; i < data.size(); i++, rownum++) {
					row = sheet.createRow(rownum);
					String[] rowVal = data.get(i);
					if(rowVal == null) continue;

					for (int j = 0; j < rowVal.length; j++) {
						cell = row.createCell(j);
						cell.setCellValue(rowVal[j]);
					}
				}
				// bug 9119: first 6-7 columns became hidden on some PCs
				// looks like their width sets to 0
				/*
				for(int i = 0; i<data.get(0).length; i++){
					sheet.autoSizeColumn(i);
				}
				*/
			}
		}

		FileOutputStream out = new FileOutputStream(path);
		wb.write(out);
		out.close();
	}
}
