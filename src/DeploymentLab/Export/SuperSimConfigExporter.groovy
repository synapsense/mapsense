
package DeploymentLab.Export

import DeploymentLab.Model.*
import groovy.xml.MarkupBuilderHelper
import javax.xml.transform.stream.StreamSource
import javax.xml.XMLConstants
import javax.xml.validation.SchemaFactory
import DeploymentLab.channellogger.Logger

import groovy.util.BuilderSupport;
import groovy.util.IndentPrinter;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.Map;



public class SuperSimConfigExporter {
	private static final Logger log = Logger.getLogger(SuperSimConfigExporter.class.getName())
	private static int idlast = 0;
	public static void writeConfig(ObjectModel omodel, File configFile) {

		configFile.withWriter { writer ->
			def pw = new IndentPrinter(new PrintWriter(writer));
			
			
			//def builder = new groovy.xml.MarkupBuilder(writer)
			//install XML declaration:
			//def helper = new MarkupBuilderHelper(builder)
			//helper.xmlDeclaration('version': 1.0, encoding: "UTF-8")

			pw.print("super_sim_config = { ")
			pw.println()
			pw.incrementIndent()
			pw.printIndent()
			pw.println("plugins = { ")
			pw.incrementIndent()
			pw.printIndent()
			
			//all plugins
			def otherPlugins = ["wsn", "modbus", "XML", "WS", "SNMP"]
			otherPlugins.each{ plugin ->
				def pluginNets = omodel.getObjectsByTag("name", "pluginId", plugin)
				if ( pluginNets.size > 0 ){
					pw.println(plugin + " = {")
					pw.incrementIndent()
					pw.printIndent()
			
					pluginNets.each{ datasource ->
						makeObj(pw, datasource)
					}
					pw.println(" }, ")
					pw.decrementIndent()
					pw.printIndent()
				}
			}
			pw.println()
			pw.println("},")
			pw.decrementIndent()
			pw.printIndent()
			pw.println()
			pw.println("}")
			pw.decrementIndent()
			pw.printIndent()
		}

	}

	/**
	 * Builds the xml for a single object in the standalone dm config file, and then recurses into that object's children.
	 * @param builder the XMLBuilder to generate the xml with
	 * @param o the DLObject to generate XML for
	 */
	protected static void makeObj(def pw, DLObject o){
		String actualId = o.getKey()
				
		if (actualId == null || actualId == "") {
			actualId = "OBJECT:" + idlast
		}
		String idn = actualId.replace(':', '_')
		
		idlast++;
		
		pw.println(idn + " = { ");
		pw.incrementIndent()
		pw.printIndent()
		
		pw.println("objId = \"" + actualId + "\",");
		pw.printIndent()
		pw.println("objType = \"" + o.getType() + "\",");
		pw.printIndent() // ByType("setting")
		o.getProperties().each { p->
			
			try {
				pw.println(p.getName() + " = [[" + p.getValue() + "]],");
				pw.printIndent()
			} catch (Exception e) {
				log.info("Can't export: " + p.getType())
				log.info(p.toString())
			}
			if ( p.hasTags() ){
				pw.println("_tags = { ")
				pw.incrementIndent()
				pw.printIndent()
				p.getTags().each{ t ->
					pw.println(t.getTagName() + " = [[" + t.getValue() + "]],");
					pw.printIndent()
				}
				pw.println("},")
				pw.decrementIndent()
				pw.printIndent()
			}
						
		}
		pw.println("children = {")
		pw.incrementIndent()
		pw.printIndent()
		o.getChildren().each{ ch ->
			makeObj(pw, ch)
		}
		pw.println("},")
		pw.decrementIndent()
		pw.printIndent()
		pw.println("},")
		pw.decrementIndent()
		pw.printIndent()
		
	}
}