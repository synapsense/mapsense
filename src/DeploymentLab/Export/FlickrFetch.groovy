package DeploymentLab.Export

import java.awt.image.BufferedImage
import javax.imageio.ImageIO

public class FlickrFetch {

	static BufferedImage image

	public static boolean fetchImage(String photo_id) {
		def xml = new URL("http://api.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=0b06823d977ac005e31812e35f0560b7&photo_id="+photo_id).openStream().getText()

		def rsp = new XmlParser().parseText(xml)

		def img_priority = ["Original", "Large"]

		String img_url = null
		for (imgp in img_priority) {
			def sizes = rsp.sizes.size.findAll { it.'@label'.equals(imgp) }
			if (sizes.size() > 0) {
				if (sizes.'@height'[0].toInteger() > 1000 && sizes.'@width'[0].toInteger() > 1000 && sizes.'@height'[0].toInteger() < 2000 && sizes.'@width'[0].toInteger() < 2000) {
					img_url = sizes.'@source'[0]
					break
				}
			}
		}

		if (img_url != null) {
			println("Downloading " + img_url)
			def fetch_data = new URL(img_url).openStream().getBytes()

			image = ImageIO.read(new ByteArrayInputStream(fetch_data))

			def fos = new FileOutputStream("newbg.jpg")
			fos.write(fetch_data)
			return true;
		} else {
			println("No Original or Large images")
			return false;
		}

	}

	public static boolean getImageUrl() {
		String xml = new URL("http://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=0b06823d977ac005e31812e35f0560b7").openStream().getText()
		def rsp = new XmlParser().parseText(xml)
		for (photo in rsp.photos.photo) {
			def photo_id = photo.'@id'
			println("Considering " + photo_id)
			if (fetchImage(photo_id) == true)
				return true;
		}
		return false;
	}

	public static void main(String[] args) {
		getImageUrl()
	}

}
