package DeploymentLab.Export;

import java.awt.*;

import java.awt.image.BufferedImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.zip.CRC32;

import javax.imageio.ImageIO;

import DeploymentLab.CentralCatalogue;
import DeploymentLab.Image.DLImage;
import DeploymentLab.Model.*;
import DeploymentLab.SceneGraph.DeploymentPanel;
import DeploymentLab.channellogger.*;
import edu.umd.cs.piccolo.util.PBounds;

public class ImageExporter {
	private static final Logger log = Logger.getLogger(ImageExporter.class.getName());

	final int maxLaszloWidth = 2879;
	final int maxLaszloHeight = 2879;

	private DeploymentPanel dp;

	//ArrayList<File> oldBackgroundImages = new ArrayList<File>();

	public ImageExporter() {
		this.dp = CentralCatalogue.getDeploymentPanel();
	}

	/**
	 * Exports background images for a given drawing.
	 * @param cmodel The ComponentModel that represents the drawing
	 * @return Success or failure of the export
	 * @throws java.io.IOException If something goes wrong with saving the files.
	 */
	public boolean exportToEs(ComponentModel cmodel) throws java.io.IOException {
		return(this.export(cmodel, false, ""));
	}

	public boolean exportToLocal(ComponentModel cmodel, String imagePath ) throws java.io.IOException{
		return(this.export(cmodel, true, imagePath));
	}

	/**
	 * Exports background images for a given drawing.
	 * @param cmodel The ComponentModel that represents the drawing
	 * @param isFileSystem Whether to create new file and save to local disk or save to ES model
	 * @param imagePath  The ComponentModel that represents the drawing
	 * @return Success or failure of the export
	 * @throws java.io.IOException If something goes wrong with saving the files.
	 */
	public boolean export(ComponentModel cmodel, boolean isFileSystem, String imagePath) throws java.io.IOException {

		for(DLComponent c : cmodel.getComponentsByType("drawing")) {
			exportDrawingImage(c, isFileSystem, imagePath);
		}

		return true;
	}

/*
	private void betaWatermark(Graphics2D destGraphics, int destWidth, int destHeight){
		AlphaComposite alpha = AlphaComposite.getInstance(AlphaComposite.SRC_OVER,0.5f);
		destGraphics.setComposite(alpha);
		destGraphics.setColor(Color.RED);
		destGraphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		destGraphics.setFont(new Font("Arial", Font.BOLD, 30));
		String watermark = "BETA VERSION";
		FontMetrics fontMetrics = destGraphics.getFontMetrics();
		Rectangle2D rect = fontMetrics.getStringBounds(watermark, destGraphics);

		int centerX = (destWidth - (int) rect.getWidth()) / 2;
		int centerY = (destHeight - (int) rect.getHeight()) / 2;
		destGraphics.drawString(watermark, centerX, centerY);
	}
*/

	private void exportDrawingImage(DLComponent drawingComponent, boolean isFileSystem, String imagePath) throws java.io.IOException {
		log.trace("Preparing '" + drawingComponent.getName() + "' Images for Export...", "progress");

		DLObject drawingObj = drawingComponent.getObject( ObjectType.DRAWING );
		String drawingName = (String) ((Setting)drawingObj.getObjectProperty("name")).getValue();
		DLImage srcImage = null;
		if ((Boolean)drawingComponent.getPropertyValue("hasImage")){
			srcImage = (DLImage) drawingComponent.getPropertyValue("image_data");
		} else {
			srcImage = new DLImage( dp.generateBackgroundImage( drawingComponent ) );
		}
		double inchesPerPixel = (Double) drawingComponent.getPropertyValue("scale");

		for(DLComponent c : CentralCatalogue.getOpenProject().getComponentModel().getComponentsInDrawing( drawingComponent ) ) {
			if( c.hasRole( ComponentRole.ROOM ) && c.hasRole( ComponentRole.PLACEABLE ) ) {
                // todo: MULTIDC - Refactor to use DLImage/DLZoneImage
				exportRoom( c, srcImage.getImage(), inchesPerPixel, isFileSystem, imagePath );
			}
		}

		log.trace("Preparing Drawing Image...", "progress");

		//log.debug("Writing drawing: " + drawingName, "default");

		double exportScaleFactor = 1.0;
		double srcWidth = srcImage.getWidth();
		double srcHeight = srcImage.getHeight();
		if(srcWidth > maxLaszloWidth)
			exportScaleFactor = maxLaszloWidth / srcWidth;
		if(srcHeight * exportScaleFactor > maxLaszloHeight)
			exportScaleFactor = maxLaszloHeight / srcHeight;

		int destWidth = (int) (srcWidth * exportScaleFactor);
		int destHeight = (int) (srcHeight * exportScaleFactor);

		Image scaledImage = srcImage.getScaledInstance(destWidth, destHeight, Image.SCALE_SMOOTH);
		BufferedImage destImage = new BufferedImage(destWidth, destHeight, BufferedImage.TYPE_INT_ARGB);

		Graphics2D destGraphics = destImage.createGraphics();
		destGraphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
		destGraphics.drawImage(scaledImage, 0, 0, null);

		/*
		if( CentralCatalogue.isBetaVersion() ){
			betaWatermark(destGraphics,destWidth,destHeight);
		}
		*/

		destGraphics.dispose();

		byte[] imageBytes = getByteData( destImage );

		// Too many potential deltas to rely on image dimensions anymore. So throw in a CRC, but keep the dimensions as an added safety for collisions.
		CRC32 checksum = new CRC32();
		checksum.update( imageBytes, 0, imageBytes.length );

		StringBuilder rectSpec = new StringBuilder();
		rectSpec.append("[");
		rectSpec.append("w:");
		rectSpec.append( destWidth );
		rectSpec.append("h:");
		rectSpec.append( destHeight );
		rectSpec.append("s:");
		rectSpec.append(inchesPerPixel);
		rectSpec.append("]");
		rectSpec.append( checksum.getValue() );
		String rectSpecS = rectSpec.toString();
		log.trace("drawing rectspec computed to " + rectSpecS, "default");

		if( rectSpecS.equalsIgnoreCase( drawingComponent.getPropertyStringValue("rect_spec") ) ) {
			log.trace("drawing " + drawingComponent.getName() + " Rect Spec matches, not updating.", "default");
			((Setting)drawingObj.getObjectProperty("image")).setValue( null );
		} else {
			((Setting)drawingObj.getObjectProperty("image")).setValue( imageBytes );
			((Setting)drawingObj.getObjectProperty("width")).setValue(srcWidth * inchesPerPixel);
			((Setting)drawingObj.getObjectProperty("height")).setValue(srcHeight * inchesPerPixel);

			drawingComponent.setPropertyValue("rect_spec", rectSpecS);
		}

		// create a real file if exporting to local disk
		if(isFileSystem){
			createImageFile(imagePath, drawingName, destImage);
		}
	}

	private void exportRoom(DLComponent roomComponent, BufferedImage drawingImage, double inchesPerPixel, boolean isFileSystem, String imagePath) throws java.io.IOException {
		DLObject roomObject = roomComponent.getObject( ObjectType.ROOM );

		String roomName = (String) ((Setting)roomObject.getObjectProperty("name")).getValue();
		log.trace( "Preparing Room image for " + roomName + "...", "progress" );

		int liPixels = (Integer) roomComponent.getPropertyValue("li_border_pixels");
		int borderPixels = (Integer) roomComponent.getPropertyValue("border_pixels");

		// This rectangle is the smallest rect that contains all objects in the group
		Rectangle footprint = scaleRect( roomFootprint( roomComponent ), inchesPerPixel);

		log.debug( "Room footprint: " + footprint, "default" );

		// This adds some padding that remains transparent
		Rectangle liRect = new Rectangle(footprint);
		liRect.grow(liPixels, liPixels);

		// The rectangle that will be cut out from the background image
		Rectangle cutRect = new Rectangle(liRect);
		cutRect.grow(borderPixels, borderPixels);

		log.debug( "Room cut rect: " + cutRect, "default" );

		Rectangle imageRect = new Rectangle(drawingImage.getWidth(), drawingImage.getHeight());

		// Limit each rect to the background image's borders
		liRect = liRect.intersection(imageRect);
		cutRect = cutRect.intersection(imageRect);

		int imgWidth, imgHeight;

		if(cutRect.width < 0) {
			cutRect.width = - cutRect.width;
			imgWidth = 0;
		} else {
			imgWidth = cutRect.width;
		}

		if(cutRect.height < 0) {
			cutRect.height = - cutRect.height;
			imgHeight = 0;
		} else {
			imgHeight = cutRect.height;
		}

		((Setting)roomObject.getObjectProperty("x")).setValue(cutRect.x * inchesPerPixel);
		((Setting)roomObject.getObjectProperty("y")).setValue(cutRect.y * inchesPerPixel);
		((Setting)roomObject.getObjectProperty("width")).setValue(imgWidth * inchesPerPixel);
		((Setting)roomObject.getObjectProperty("height")).setValue(imgHeight * inchesPerPixel);

		double exportScaleFactor = 1.0;
		if(imgWidth > maxLaszloWidth)
			exportScaleFactor = (double) maxLaszloWidth / (double) imgWidth;
		if(imgHeight * exportScaleFactor > maxLaszloHeight)
			exportScaleFactor = (double) maxLaszloHeight / (double) imgHeight;

		int scaledImgWidth = (int) (imgWidth * exportScaleFactor);
		int scaledImgHeight = (int) (imgHeight * exportScaleFactor);

		BufferedImage liImage = new BufferedImage(scaledImgWidth, scaledImgHeight, BufferedImage.TYPE_INT_ARGB);

		Graphics2D liCanvas = liImage.createGraphics();
		liCanvas.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		liCanvas.scale(exportScaleFactor, exportScaleFactor);
		liCanvas.setColor(Color.white);
		liCanvas.fillRect(0, 0, (int) (liRect.x - cutRect.x), imgHeight); // Fill the left side border
		liCanvas.fillRect(0, 0, imgWidth, (int)(liRect.y - cutRect.y)); // Fill the top side border
		liCanvas.fillRect((int) (liRect.x + liRect.width - cutRect.x), 0, (int) ((cutRect.x + cutRect.width) - (liRect.x + liRect.width)), imgHeight); // Fill the right side border
		liCanvas.fillRect(0, (int)((liRect.y + liRect.height) - cutRect.y), imgWidth, (int)((cutRect.y + cutRect.height)-(liRect.y + liRect.height))); // Bottom side border
		liCanvas.drawImage(drawingImage.getSubimage((int) cutRect.x, (int) cutRect.y, imgWidth, imgHeight), 0, 0, null);

		/*
		if( CentralCatalogue.isBetaVersion() ){
			betaWatermark(liCanvas,imgWidth,imgHeight);
		}
		*/
		liCanvas.dispose();

		byte[] imageBytes = getByteData( liImage );

		// Too many potential deltas to rely on image dimensions anymore. So throw in a CRC, but keep the dimensions as an added safety for collisions.
		CRC32 checksum = new CRC32();
		checksum.update( imageBytes, 0, imageBytes.length );

		StringBuilder rectSpec = new StringBuilder();
		rectSpec.append("[");
		rectSpec.append("x:");
		rectSpec.append( ((Setting) roomObject.getObjectProperty("x")).getValue() );
		rectSpec.append("y:");
		rectSpec.append( ((Setting) roomObject.getObjectProperty("y")).getValue() );
		rectSpec.append("w:");
		rectSpec.append( ((Setting) roomObject.getObjectProperty("width")).getValue() );
		rectSpec.append("h:");
		rectSpec.append( ((Setting) roomObject.getObjectProperty("height")).getValue() );
		rectSpec.append("s:");
		rectSpec.append(inchesPerPixel);
		rectSpec.append("]");
		rectSpec.append( checksum.getValue() );
		String rectSpecS = rectSpec.toString();

		log.trace( "Room " + roomComponent.getName() + " rectspec computed to " + rectSpecS, "default" );

		if( rectSpecS.equalsIgnoreCase( roomComponent.getPropertyStringValue("rect_spec") ) ) {
			log.trace("Room " + roomComponent.getName() + " Rect Spec matches, not updating.", "default");
			((Setting)roomObject.getObjectProperty("image")).setValue( null );
		} else {
			((Setting)roomObject.getObjectProperty("image")).setValue( imageBytes );

			roomComponent.setPropertyValue( "rect_spec", rectSpecS );
		}

		// create a real file if exporting to local disk
		if(isFileSystem){
			createImageFile(imagePath, roomName, liImage);
		}
		// Useful for quick debugging of the generated image. Just check your local user temp directory for the image file.
		// else {
		//	createImageFile(System.getProperty("java.io.tmpdir"), "IMAGETEST_" + roomName, liImage);
		//}
	}

	/**
	 * return Room footprint in inches
	 * @param room the DLComponent holding the Room to compute the rectangle for
	 * @return a Rectangle holding the bounding box of the Room's background image
	 */
	private Rectangle roomFootprint( DLComponent room ) {
		// Include the room itself.
		Rectangle footprint = componentFootprint( room );

		// TODO: This shouldn't be necessary but needs to be here until we stop components from overlapping room bounds.
		for(DLComponent child: room.getChildComponents()) {
			if(child.hasClass("placeable")) {
				footprint.add(componentFootprint(child));
			}
		}

		return footprint;
	}

	/**
	 * Converts a double to an int by always rounding up;
	 * that is, taking the ceiling and then casting to int
	 * @param input
	 * @return
	 */
	private int roundDouble(double input){
		Double c = Math.ceil(input);
		return c.intValue();
	}

	/**
	 * Return component footprint in inches
	 * @param placeable
	 * @return
	 */
	private Rectangle componentFootprint(DLComponent placeable) {
		int x = roundDouble( ((Double)placeable.getPropertyValue("x")) );
		int y = roundDouble( ((Double)placeable.getPropertyValue("y")) );
		int sz = 1;
		if(placeable.listProperties().contains("width") && placeable.listProperties().contains("depth")){
			sz = (int)Math.max((Double)placeable.getPropertyValue("width"), (Double)placeable.getPropertyValue("depth"));
		} else if( placeable.listProperties().contains("points") ) {
			PBounds bounds = dp.getPNodeFor( placeable ).getFullBounds();
			sz = (int)Math.max( bounds.getWidth(), bounds.getHeight() );
		}

		// Because of rotation, width and depth do not correspond to the X or Y axes.  This is a simple hack, a bit less precise, but close enough.
		Rectangle footprint = new Rectangle((int)(x - (sz/2)), (int)(y - (sz/2)), 0, 0);
		footprint.add((int)(x + (sz/2)), (int)(y + (sz/2)));

		for(DLObject o: placeable.getRoots()) {
			if(o.getType().equals("wsnnode")) {
				// node x, y type float to int
				x = roundDouble(((Double)((Setting)o.getObjectProperty("x")).getValue()));
				y = roundDouble(((Double)((Setting)o.getObjectProperty("y")).getValue()));
				footprint.add(x, y);
				for(DLObject sp: ((ChildLink)o.getObjectProperty("sensepoints")).getChildren()) {
					//sensor x,y type double
					if(((Setting)sp.getObjectProperty("x")).getValue()!=null && ((Setting)sp.getObjectProperty("y")).getValue()!=null){
						x = roundDouble(((Double) ((Setting)sp.getObjectProperty("x")).getValue()));
						y = roundDouble(((Double) ((Setting)sp.getObjectProperty("y")).getValue()));
						footprint.add(x, y);
					}
				}
			}
		}
		//a much more optimized way to make sure we round out in all directions
		footprint.grow(1,1);
		return footprint;
	}

	private Rectangle scaleRect(Rectangle rect, double scale) {
		rect.x = (int) (rect.x / scale);
		rect.y = (int) (rect.y / scale);
		rect.width = (int) (rect.width / scale);
		rect.height = (int) (rect.height / scale);
		return rect;
	}

	/**
	 * Deletes any image files created by this instance of the image exporter.
	 */
	/*public void cleanupOldImages() {
		for(File imageFile: oldBackgroundImages) {
			if(imageFile.exists()) {
				if(imageFile.delete())
					log.info("Old image '" + imageFile + "' deleted.");
				else
					log.warn("Old image '" + imageFile + "' could not be deleted!");
			}
		}
	}*/

	public static byte[] getByteData(BufferedImage image) {
		byte[] data = {};
		try{
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			ImageIO.write(image, "png", output);
			data = output.toByteArray();
			output.reset(); // empty out buffer
		} catch (IOException e) {
			log.error("Cannot get byte array for image", "default");
		}finally{
			return data;
		}
    }

	private boolean createImageFile(String imagePath, String imageName, BufferedImage image) throws java.io.IOException {
		File imageFile = new File( new File(imagePath), (imageName + ".png")  );
		//log.trace("writing image " + imageFile.getAbsolutePath());
		boolean writeResult = ImageIO.write(image, "png", imageFile);
		return writeResult;
	}
}
