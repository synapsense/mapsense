package DeploymentLab.Export;

import com.synapsense.dto.Relation;
import DeploymentLab.Model.*;

/**
 * Container to hold what we need to track a parent-child relation.
 *
 * Java, I love you!  Thanks for not having a tuple data structure!
 * @author Gabriel Helman
 * @since Earth
 * Date: 4/6/11
 * Time: 4:51 PM
 */
public class RelationLot {

	private Relation relation;
	private ChildLink childLink;
	private Dlid childId;

	public RelationLot() {
	}

	public RelationLot(Relation relation, ChildLink childLink, Dlid childId) {
		this.relation = relation;
		this.childLink = childLink;
		this.childId = childId;
	}

	public Relation getRelation() {
		return relation;
	}

	public void setRelation(Relation relation) {
		this.relation = relation;
	}

	public ChildLink getChildLink() {
		return childLink;
	}

	public void setChildLink(ChildLink childLink) {
		this.childLink = childLink;
	}

	public Dlid getChildId() {
		return childId;
	}

	public void setChildId(Dlid childId) {
		this.childId = childId;
	}
}
