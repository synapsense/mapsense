package DeploymentLab.Export

import DeploymentLab.CentralCatalogue
import DeploymentLab.FileFilters
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLObject
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Model.Property
import DeploymentLab.Model.Setting
import DeploymentLab.UserPreferences
import DeploymentLab.channellogger.Logger
import com.opencsv.CSVWriter
import java.awt.Component
import javax.swing.JFileChooser

/**
 * Various exporters to get object model data out into something readable
 * @author Gabriel Helman
 * @since Jupiter
 * Date: 2/17/12
 * Time: 10:01 AM
 */
class DLObjectValueExporter {
    private static final Logger log = Logger.getLogger(DLObjectValueExporter.class.getName())

    public static List<String[]> exportComponents(ComponentModel componentModel, List<DLComponent> drawings = null, boolean macScanList = false) {
        List<String> properties;
        List<String[]> result = new ArrayList<String[]>()
	    if (macScanList){
		    result.add(['DLID', 'Component', 'Name', 'Property', 'MAC Address'].toArray(new String[5]))   //header row for mac scan list
	    }
        for (DLComponent dlc : getComponents(componentModel, drawings)) {
            if (macScanList) {
                if (dlc.getManagedObjectsOfType("wsnnode").isEmpty()) continue          // we need only wsnnodes
                properties = dlc.listMacIdProperties()
            } else {
                properties = dlc.listProperties()
            }
            for (String pName : properties) {
                if (!dlc.getComponentProperty(pName).isDisplayed()) continue
	            if (macScanList){
		            // skip components with mac id
		            String mac = dlc.getPropertyValue(pName)
		            if (mac != null && !mac.equals("0")) continue
	            }
                def s = new String[5]
                s[0] = dlc.getDlid().toString()
                s[1] = dlc.getDisplaySetting('name')
                s[2] = dlc.getName()
                s[3] = dlc.getPropertyDisplayName(pName)
                s[4] = (macScanList) ? "" : checkPropertyValue(dlc, pName)
                result.add(s)
            }
        }

        return result
    }

    private static List<DLComponent> getComponents(ComponentModel componentModel, List<DLComponent> drawings) {
        List<DLComponent> components = new ArrayList<DLComponent>()
        if (drawings != null) {
            for (DLComponent drawing : drawings) {
                components.addAll(componentModel.getComponentsInDrawing(drawing))
            }
        } else {
            components = componentModel.getComponents()
        }
        components.sort { a, b ->
            if (a.getType() == b.getType()) {
                a.getName() <=> b.getName()
            } else {
                a.getType() <=> b.getType()
            }
        }
        return components
    }

    // Some properties should be converted, replaced or skip before export
    private static String checkPropertyValue(DLComponent dlc, String pName) {
        if (dlc.getPropertyDisplayName(pName).contains("MAC ID")) {
            if (dlc.getPropertyValue(pName).equals("0")) {
                return ""
            }
        } else if (dlc.getComponentProperty(pName).getType().equals(java.awt.Color.class.getName())) {
            return Integer.toString(((java.awt.Color) dlc.getPropertyValue(pName)).getRGB())
        }

        return dlc.getPropertyValue(pName).toString();
    }

    public static List<String[]> exportObjects(ObjectModel objectModel, Set<DLObject> drawings = null) {
        List<String[]> result = new ArrayList<String[]>()
        for (String typeName : objectModel.getTypes().sort()) {
            List<DLObject> objects
            if (drawings != null) {
                objects = objectModel.getObjectsByTypeInDrawings(typeName, drawings)
            } else {
                objects = objectModel.getObjectsByType(typeName)
            }
            objects.sort { it.getName() }
            for (DLObject dlo : objects) {
                for (Property p : dlo.getProperties()) {
                    if (p.getType().equals("setting")) {
                        def s = new String[5]
                        s[0] = dlo.getDlid().toString()
                        s[1] = typeName
                        s[2] = dlo.getName()
                        s[3] = p.getName()
                        def val = ((Setting) p).getValue()
                        s[4] = (val) ? val.toString() : "null"
                        result.add(s)
                    }
                }
            }
        }

        return result
    }

    /**
     * Generates a list of all node names in CSV format
     * @param objectModel
     * @return
     */
    public static List<String[]> exportNodeNames(ObjectModel objectModel, Set<DLObject> drawings = null) {
        List<String> names = new LinkedList<String>()

        ArrayList<DLObject> nodes
        if (drawings != null) {
            nodes = objectModel.getObjectsByTypeInDrawings("wsnnode", drawings)
        } else {
            nodes = objectModel.getObjectsByType("wsnnode")
        }
        for (DLObject o : nodes) {
            names.add(o.getObjectSetting("name").getValue().replace("Node ", ""))
        }
        names = names.sort()
        List<String[]> result = new LinkedList<String[]>()
        for (String n : names) {
            def current = new String[1]
            current[0] = n
            result.add(current)
        }
        return result
    }

    /**
     * Exports a set of values to a CSV file with the name specified.
     * @param values a List of String arrays, with each array being a line in the resulting CSV file
     * @param filename the full path of the file to be filled with CSVs
     */
    public static void exportToCSV(List<String[]> values, String filename) {
        //println values
        CSVWriter writer
        try {
            writer = new CSVWriter(new FileWriter(filename))
            writer.writeAll(values)
        } catch (Exception e) {
            log.error(String.format(CentralCatalogue.getUIS("exportSearch.saveerror"), e.getMessage()), "message")
            log.error(log.getStackTrace(e))
        } finally {
            if (writer) {
                writer.close()
            }
        }
        log.info("Values exported to '$filename'", "statusbar")
    }

    /**
     * Export a table of values to a CSV file.  Opens a fileChooser to allow the user to select where to save the file to.
     * @param userPreferences the main userPreferences so we can get at the filechooser memory
     * @param values a List of String arrays, with each array being a line in the resulting CSV file
     * @param parent the parent Swing component to hang the dialog on
     */
    public static void exportTable(UserPreferences userPreferences, List<String[]> values, Component parent) {
        def exportFileChooser = new JFileChooser()
        exportFileChooser.addChoosableFileFilter(FileFilters.CSVFile.getFilter())

        //def f = new File( "simulator_${selectModel.getActiveDrawing().getPropertyStringValue("name")}" )
        exportFileChooser.setSelectedFile( new File(userPreferences.getUserPreference(UserPreferences.CSV_FILENAME)))
        exportFileChooser.setFileFilter(FileFilters.CSVFile.getFilter())
        exportFileChooser.setDialogTitle(userPreferences.getUserPreference(UserPreferences.CSV_TITLE))

        exportFileChooser.setCurrentDirectory(new File(userPreferences.getUserPreference(UserPreferences.CSV)))

        int rv = exportFileChooser.showSaveDialog(parent)
        if (rv == JFileChooser.APPROVE_OPTION) {
            String selectedPath = exportFileChooser.getSelectedFile().getPath()

            if (!selectedPath.endsWith(".csv")) {
                selectedPath = selectedPath + ".csv"
            }

            File selectedFile = new File(selectedPath)
            userPreferences.setUserPreference(UserPreferences.CSV, selectedFile.getParent())

            DLObjectValueExporter.exportToCSV(values, selectedPath)
        }

    }

}
