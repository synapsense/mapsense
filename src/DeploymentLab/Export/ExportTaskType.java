package DeploymentLab.Export;

/**
 * THis used to be a String, but really, it's an enum.
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 8/6/12
 * Time: 5:25 PM
 *
 * @see ExportController#loginESDoExportTask(java.util.Properties, ExportTaskType)
 */
public enum ExportTaskType {
	EXPORT,
	UNDOEXPORT,
	UNDOEXPORTCASCADE,
    ACCESS
}
