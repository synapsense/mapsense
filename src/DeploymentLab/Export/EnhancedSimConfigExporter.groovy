package DeploymentLab.Export

import DeploymentLab.channellogger.Logger
import DeploymentLab.Model.*
import DeploymentLab.ProjectSettings
import DeploymentLab.DLProject
import groovy.xml.MarkupBuilderHelper
import groovy.xml.MarkupBuilder

import java.nio.file.StandardCopyOption
import com.synapsense.dto.TO
import javax.swing.JOptionPane

/**
 * SimConfig generator for the fancy new javascript-bsaed simulator
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 10/24/12
 * Time: 3:43 PM
 */
class EnhancedSimConfigExporter {
	private static final Logger log = Logger.getLogger(EnhancedSimConfigExporter.class.getName())

	DLProject project
	ComponentModel componentModel
	ObjectModel objectModel
	ProjectSettings projectSettings

	List<SimBundle> otherItems

	List<SimEntry> entries

	private static String SUPPLY_TARGET = "68.0"
	private static String RACK_HEAT_FACTOR = "13.0"
	private static final String defaulttime = "300000"

	EnhancedSimConfigExporter(DLProject projectToSimulate, List<SimBundle> otherItems = null) {
		this.project = projectToSimulate
		this.componentModel = project.componentModel
		this.objectModel = project.objectModel
		this.projectSettings = project.projectSettings
		this.otherItems = otherItems
	}

	private long delayStep = 10
	private double delay = 0
	private long getDelay() {
		delay += delayStep
		return (long)delay
	}

	public void generateValues(){
		entries = []

		boolean thereIsTempControl = false
		Map<DLObject,DLObject> controlsensors = new HashMap<DLObject,DLObject>()

		def crahAndCracs = []
		crahAndCracs.addAll(objectModel.getObjectsByType("controlout_crah"))
		crahAndCracs.addAll(objectModel.getObjectsByType("controlout_crac"))

		crahAndCracs.each{ crah ->
			thereIsTempControl = true
			String sensorname = crah.getObjectProperty('resource').getValue()
			def component = componentModel.getManagingComponent(crah)
			//'simulator:sensor'('id': crah.getKey(), 'expression': '72' , 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'setpoint', componentName: cmodel.getManagingComponent(crah).getName() , sensorName: crah.getObjectProperty('name').getValue())
			//'simulator:sensor'('id': crah.getKey(), 'expression': '42' , 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'capacity', componentName: cmodel.getManagingComponent(crah).getName() , sensorName: crah.getObjectProperty('name').getValue())
			//'simulator:sensor'('id': crah.getKey(), 'expression': getExpression(1, 'return'), 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'deviceRat', componentName: cmodel.getManagingComponent(crah).getName() , sensorName: crah.getObjectProperty('name').getValue())
			//'simulator:sensor'('id': crah.getKey(), 'expression': getExpression(1, 'supply'), 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'deviceSat', componentName: cmodel.getManagingComponent(crah).getName() , sensorName: crah.getObjectProperty('name').getValue())
			//find the env cooler return temp
			def returnsensor = ((Dockingpoint)crah.getObjectProperty("returnT")).getReferrent()
			def value = keyToSimFormat(returnsensor, "lastValue")

			def supplySensor = ((Dockingpoint)crah.getObjectProperty("supplyT")).getReferrent()
			controlsensors[supplySensor] = crah

			if(SUPPLY_TARGET.size() > 0){
				entries += new SimEntry(crah,SUPPLY_TARGET.toString(),getDelay(), defaulttime, component.getName() , sensorname, "setpoint") //written by AC
			}


			def d = getDelay()
			entries += new SimEntry(crah,"42.0",d, defaulttime, component.getName() , sensorname, "capacity")
			entries += new SimEntry(crah, value ,d, defaulttime, component.getName() , sensorname, "deviceRat")
			entries += new SimEntry(crah, keyToSimFormat(crah, "setpoint") ,d, defaulttime, component.getName() , sensorname, "deviceSat")
		}

		objectModel.getObjectsByType("controlout_vfd").each{ vfd ->
			String sensorname = vfd.getObjectProperty('resource').getValue()
			def component = componentModel.getManagingComponent(vfd)
			//'simulator:sensor'('id': vfd.getKey(), 'expression': 100, 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'setpoint', componentName: cmodel.getManagingComponent(vfd).getName() , sensorName: vfd.getObjectProperty('name').getValue())
			//'simulator:sensor'('id': vfd.getKey(), 'expression': '5 + Math.sin(time/1000000)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'kw', componentName: cmodel.getManagingComponent(vfd).getName() , sensorName: vfd.getObjectProperty('name').getValue())
			def d = getDelay()
			entries += new SimEntry(vfd,"100.0", d, defaulttime, component.getName() , sensorname, "setpoint") //written by AC
			entries += new SimEntry(vfd,"5 + Math.sin(time/1000000)", d, defaulttime, component.getName() , sensorname, "kw")
		}



		//objectModel.getObjectsByType("wsnsensor").each{ sensor ->
		for(DLObject node : objectModel.getObjectsByType("wsnnode") ){
			//compute delay and rollover per node, not per sensor
			int reportingInterval = Integer.parseInt(node.getObjectSetting("period").getValue().toString().replace("min", "").trim())
			String time = (reportingInterval * 60 * 1000 ).toString()
			def d = getDelay()


			for(DLObject sensor : node.getChildren().findAll {it.type == "wsnsensor"}){
			def component = componentModel.getManagingComponent(sensor)
			int type = sensor.getObjectProperty('type').getValue()
			String sensorname = sensor.getObjectProperty('name').getValue().toLowerCase()

			println "processing ${sensor} : ${sensor.getKey()} : $type : $sensorname : ${SimConfigExporter.getExpression(type,sensorname)}"

			String value = SimConfigExporter.checkValue(type, sensorname, sensor.getDlid().toString(), projectSettings)
			value = "function simulate(){ return ( $value ); }"


			if(sensorname.contains("humid")){
				println "leave humid alone"

			} else if (sensorname.contains("battery") ){
				value = "3.378"

				//3.378
				//value = "function simulate() {"
				//value += " return( current - 0.001 )"
				//value += " }"
				//value = functionWrapper("current - 0.001", "3.378")

			} else if(component.getManagedObjectsOfType("crac").size() > 0 && sensorname.toLowerCase().contains("return temp")){
				DLComponent room = component.getParentsOfType("room").first()
				List<DLComponent> racks = []
				room.getChildComponents().each{ k ->
					if(k.getManagedObjectsOfType("rack").size() > 0){
						racks += k
					}
				}

				List<DLObject> exhausts = []
				racks.each{ r ->
					r.getManagedObjectsOfType("wsnsensor").each{ s ->
						println "checking sensor: ${s.getName()} ${s.getObjectSetting("dataclass")}"
						if((s.getName().contains("exhaust") || s.getName().contains("hot")) && (s.getObjectSetting("dataclass").getValue() == 200)){
							exhausts += s
						}
					}
				}

				/*
				value = "( "
				exhausts.each{ e ->
					value += keyToSimFormat(e,"lastValue") + " + "
				}
				value += " 0 ) / ${exhausts.size()}"
				*/


				/*
				value = "function simulate() {"
				value += "var rackHots = ["
				value += exhausts.collect{ keyToSimFormat(it,"lastValue") }.join(" , ")
				value += "];  "
				value += " return Number(coolerTemp(rackHots,3) );"
				value += " }"
				*/


				value = "var rackHots = ["
				value += exhausts.collect{ keyToSimFormat(it,"lastValue") }.join(" , ")
				value += "];  "
				value += " result = Number(coolerTemp(rackHots,3) );"

				value = functionWrapper(value, " (${SUPPLY_TARGET.toString()} + $RACK_HEAT_FACTOR + 5) ")

				/*
				value += "var big = 0;  "
				//value += "for(var n in arr){ if(n>big){big=n;}  "
				value += "for(var n in arr){ big = Math.max(big,n) }"

				value += " return 2.0 + n;"
				value +="  }"
				*/

				//println "updating ${sensor} : ${sensor.getKey()} : $type : $sensorname : $value"

			} else if(component.getManagedObjectsOfType("crac").size() > 0 && sensorname.contains("supply temp")){

				if(!thereIsTempControl){
					value = SUPPLY_TARGET
				} else {
					if (controlsensors.containsKey(sensor)){
						value = keyToSimFormat(controlsensors[sensor], "setpoint" )
					}
				}


			} else if(component.getManagedObjectsOfType("rack").size() > 0 && sensorname.contains("subfloor")){
				//slightly warmer than the supply of the room's cooler
				def supplies = findInRoom(component, "crac", ["supply"], [200])
				if(supplies){
					/*
					value = "function simulate() {"
					value += "var supplies = ["
					value += supplies.collect{ keyToSimFormat(it,"lastValue") }.join(" , ")
					value += "];  "
					value += " return Number(arrAvg(supplies) + roll(1,4) );"
					value += " }"
					*/

					value = "var supplies = ["
					value += supplies.collect{ keyToSimFormat(it,"lastValue") }.join(" , ")
					value += "];  "
					value += " result = Number(arrAvg(supplies) + roll(1,4) );"

					value = functionWrapper(value, SUPPLY_TARGET.toString())

				}


			}else if(component.getManagedObjectsOfType("rack").size() > 0 && (sensorname.contains("cold") || sensorname.contains("intake") )){
				//find my subfloor
				//heat it up based on level
				DLObject sf = component.getManagedObjectsOfType("wsnsensor").find {it.getName().contains("subfloor")}
				if(sf == null){
					def r = component.getManagedObjectsOfType("rack").first()
					sf = ((Dockingpoint)r.getObjectProperty("ref")).getReferrent()
				}



					def factor = 3
					if(sensorname.contains("top")){
						factor = 3
					} else if(sensorname.contains("mid")){
						factor = 2
					} else if(sensorname.contains("bot")){
						factor = 1
					}

				if (sf != null){

					value = "function simulate() {"
					value += " return( ${keyToSimFormat(sf,"lastValue")} + roll($factor,4) )"
					value += " }"

				} else {
					//STILL null?  Use the supply setpoint
					value = "function simulate() {"
					value += " return( $SUPPLY_TARGET + roll($factor,4) )"
					value += " }"

				}

			}else if(component.getManagedObjectsOfType("rack").size() > 0 && (sensorname.contains("hot") || sensorname.contains("chimney") || sensorname.contains("exhaust") )){
				//heat up racks
				//find the matching intake

				def factor = 0
				def name
				if(sensorname.contains("top")){
					factor = 3
					name = "top"
				} else if(sensorname.contains("mid")){
					factor = 2
					name = "mid"
				} else if(sensorname.contains("bot")){
					factor = 1
					name = "bot"
				} else if(sensorname.contains("chim")){
					factor = 4
					name = "top"
				}

				DLObject sf = component.getManagedObjectsOfType("wsnsensor").find {it.getName().contains("cold $name")}

				if (sf != null){
					value = "function simulate() {"
					value += " return( ${keyToSimFormat(sf,"lastValue")} + roll($factor,4) + $RACK_HEAT_FACTOR )"
					value += " }"
				} else {
					value = "function simulate() {"
					value += " return( $SUPPLY_TARGET + roll($factor,4) + $RACK_HEAT_FACTOR )"
					value += " }"
				}

			} else if (component.getManagedObjectsOfType("rack").size() == 0 && (sensorname.toLowerCase().contains("exhaust") || sensorname.toLowerCase().contains("node temp") )) {
				value = SUPPLY_TARGET.toFloat() + RACK_HEAT_FACTOR.toFloat()

			} else if (component.getManagedObjectsOfType("rack").size() == 0 && component.getType().equals("reftemp-thermanode") && sensorname.contains("temp") ) {
				value = SUPPLY_TARGET.toFloat() + 1.0

			} else if (component.getType().contains("pressure")){

				//.035 == target
				//range: .02 --> .08
				//would be nice to fake this based on how hard we think the cooler fans are working

				value = "function simulate() {"
				value += " if (current == null || current < 0.0 || current > .09 ) { return .02 + (roll(1,6) * 0.01); } else { return  current + roll(1,10)*.001; }"
				value += " }"
			} else if (component.getType().equals("control-verticalstring-li-thermanode")){

				def supplies = findInRoom(component, "crac", ["supply"], [200])
				if (supplies){
					def supply = supplies.first()

					def factor = 0
					if(sensorname.contains("top")){
						factor = 3
					} else if(sensorname.contains("mid")){
						factor = 2
					} else if(sensorname.contains("bot")){
						factor = 1
					}

					value = "function simulate() {"
					value += " return( ${keyToSimFormat(supply,"lastValue")} + roll($factor,4) )"
					value += " }"
				}
			}

			//String time = SimConfigExporter.checkTime("300000", sensor.getDlid().toString(), projectSettings)
			//String time = SimConfigExporter.checkTime("5000", sensor.getDlid().toString(), projectSettings)

			//compute time based on the node's configured reporting interval
			//def node = sensor.getParents().find {it.getType() == "wsnnode"}
//			int reportingInterval = Integer.parseInt(node.getObjectSetting("period").getValue().toString().replace("min", "").trim())
//			String time = (reportingInterval * 60 * 1000 ).toString()
//			def d = getDelay()

			entries += new SimEntry(sensor, value, d, time, component.getName() , sensorname)
		}
		}

		/*
		objectModel.getObjectsByType('modbusproperty').each{ modbusproperty ->
			def component = componentModel.getManagingComponent(modbusproperty)
			//int type = modbusproperty.getObjectProperty('type').getValue()
			String sensorname = modbusproperty.getObjectProperty('name').getValue()
			String value
			if(sensorname.toLowerCase().contains("factor")){
				value = ".98"
			} else if (sensorname.toLowerCase().contains("current channel") && component.getType().contains("bcms"))  {
				value = "(20 * (roll(1,6) /10 ))"
			} else if (sensorname.toLowerCase().contains("power channel") && component.getType().contains("bcms"))  {

			} else {

				value = SimConfigExporter.getModbusExpression()
			}


			String time = SimConfigExporter.checkTime("300000", modbusproperty.getDlid().toString(), projectSettings)
			entries += new SimEntry(modbusproperty,value,getDelay(), time, component.getName() , sensorname)
		}
		*/

		def modprops = new HashSet<DLObject>()

		componentModel.getComponentsByType("modbus-squared-bcms-custom").each{ bcms ->
			String time ="300000"

			def powercircuits = []

			bcms.getManagedObjectsOfType("circuit").each{ circuit ->
				//power,powerfactor,current

				def current = ((Dockingpoint)circuit.getObjectProperty("current")).getReferrent()
				String sensorname = current.getObjectProperty('name').getValue()
				def value = "(2 + roll(3,6))"
				entries += new SimEntry(current,value,getDelay(), time, bcms.getName() , sensorname)

				def powerFactor = ((Dockingpoint)circuit.getObjectProperty("powerFactor")).getReferrent()
				sensorname = powerFactor.getObjectProperty('name').getValue()
				value = ".98"
				entries += new SimEntry(powerFactor,value,getDelay(), time, bcms.getName() , sensorname)

				//w = v * a * pf
				// *1000 = kW
				def power = ((Dockingpoint)circuit.getObjectProperty("power")).getReferrent()
				sensorname = power.getObjectProperty('name').getValue()
				value = "((${keyToSimFormat(current,'lastValue')} * 120 * ${keyToSimFormat(powerFactor,'lastValue')}) / 1000)"
				entries += new SimEntry(power,value,getDelay(), time, bcms.getName() , sensorname)

				int circuitNumber = Integer.parseInt(sensorname.toLowerCase().replace("power channel ", ""))
				powercircuits[circuitNumber] = power

				modprops.add(current)
				modprops.add(powerFactor)
				modprops.add(power)
			}

			//voltages== 120 +- 1.5
			bcms.getManagedObjectsOfType("modbusproperty").each{ mp ->
				String sensorname = mp.getObjectProperty('name').getValue()
				def value = ""
				if(sensorname.toLowerCase().toLowerCase().contains("factor")){
					value = ".98"
				} else if (sensorname.toLowerCase().toLowerCase().contains("voltage"))  {
					value = "120.0"
				} else if (sensorname.toLowerCase().contains("phase") && sensorname.toLowerCase().contains("current")){
					value = "(2 + roll(3,6))"
				} else if (sensorname.toLowerCase().contains("phase a real power")){
					//phase power should be sum of all circuits
					value = "("
					value += powercircuits[1..14].collect{ keyToSimFormat(it,"lastValue") }.join(" + ")
					value += ")"

				} else if (sensorname.toLowerCase().contains("phase b real power")){
					//phase power should be sum of all circuits
					value = "("
					value += powercircuits[15..28].collect{ keyToSimFormat(it,"lastValue") }.join(" + ")
					value += ")"

				} else if (sensorname.toLowerCase().contains("phase c real power")){
					//phase power should be sum of all circuits
					value = "("
					value += powercircuits[29..42].collect{ keyToSimFormat(it,"lastValue") }.join(" + ")
					value += ")"
				}

				if(value.length() > 0){
					modprops.add(mp)
					entries += new SimEntry(mp,value,getDelay(), time, bcms.getName() , sensorname)
				}

			}
		}



		objectModel.getObjectsByType('wsnactuator').each{ sensor ->
			def component = componentModel.getManagingComponent(sensor)
			String sensorname = sensor.getObjectProperty('name').getValue()
			String value = "actuatorPosition(100.0, 0.0, 0.0, time)"

			entries += new SimEntry(sensor,value,getDelay(), defaulttime, component.getName() , sensorname)
		}

		objectModel.getObjectsByType('modbusproperty').each{ modbusproperty ->
			if(! modprops.contains(modbusproperty)) {
				def component = componentModel.getManagingComponent(modbusproperty)
				String sensorname = modbusproperty.getObjectProperty('name').getValue()
				String value = SimConfigExporter.getModbusExpression()
				entries += new SimEntry(modbusproperty,value,getDelay(), defaulttime, component.getName() , sensorname)
			}
		}




		if ( otherItems ){
			otherItems.each{ bundle ->

				//String time = "0 3/15 * * * ?"
				String time = "300000"

				switch ( bundle.getTypeName() ){
					case "phase":
						bundle.getIds().eachWithIndex { toO, i ->
							//builder.'simulator:sensor'('id': to, 'expression': 208.0, 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'voltage', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName )
							String to = toO.toString()
							entries += new SimEntry(to, '120.0', getDelay(), time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'voltage')
						}
						break;

					case "phase_delta":
						bundle.getIds().eachWithIndex { toO, i ->
							//builder.'simulator:sensor'('id': to, 'expression': 208.0, 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'voltage', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName )
							String to = toO.toString()
							entries += new SimEntry(to, '208.0', getDelay(), time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'voltage')
						}
						break;

					case "phase_duo":
						bundle.getIds().eachWithIndex{ toO, i ->
							String to = toO.toString()
							def d = getDelay()
							//builder.'simulator:sensor'('id': to, 'expression': 208.0, 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'voltage', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
							//builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt((0.6 + Math.sin(time/100000))^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'avgCurrent', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
							//builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt((0.8 + Math.sin(time/100000))^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'maxCurrent', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
							//builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt( ((0.6 + Math.sin(time/100000)) * 208) ^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'demandPower', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)

							entries += new SimEntry(to, '120.0', d, time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'voltage')
							//entries += new SimEntry(to, 'Math.sqrt((0.6 + Math.sin(time/100000))^2)', getDelay(), time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'avgCurrent')
							//entries += new SimEntry(to, 'Math.sqrt((0.8 + Math.sin(time/100000))^2)', getDelay(), time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'maxCurrent')
							//entries += new SimEntry(to, 'Math.sqrt( ((0.6 + Math.sin(time/100000)) * 208) ^2)', SimConfigExporter.getDelay(), time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'demandPower')

							//def power = "function simulate() {"
							//power += " return( Math.abs( ${keyToSimFormat(to,'avgCurrent')} * 208.0 )  )"
							//power += " }"
							//entries += new SimEntry(to, power, getDelay(), time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'demandPower')
							entries += new SimEntry(to, '1.5', d, time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'avgCurrent')
							entries += new SimEntry(to, '1.6', d, time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'maxCurrent')
							//entries += new SimEntry(to, '0.312', d, time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'demandPower')
						}
						break;

					case "phase_duo_delta":
						bundle.getIds().eachWithIndex{ toO, i ->
							String to = toO.toString()
							def d = getDelay()
							//builder.'simulator:sensor'('id': to, 'expression': 208.0, 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'voltage', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
							//builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt((0.6 + Math.sin(time/100000))^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'avgCurrent', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
							//builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt((0.8 + Math.sin(time/100000))^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'maxCurrent', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
							//builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt( ((0.6 + Math.sin(time/100000)) * 208) ^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'demandPower', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)

							entries += new SimEntry(to, '208.0', d, time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'voltage')
							//entries += new SimEntry(to, 'Math.sqrt((0.6 + Math.sin(time/100000))^2)', getDelay(), time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'avgCurrent')
							//entries += new SimEntry(to, 'Math.sqrt((0.8 + Math.sin(time/100000))^2)', getDelay(), time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'maxCurrent')
							//entries += new SimEntry(to, 'Math.sqrt( ((0.6 + Math.sin(time/100000)) * 208) ^2)', SimConfigExporter.getDelay(), time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'demandPower')

							//def power = "function simulate() {"
							//power += " return( Math.abs( ${keyToSimFormat(to,'avgCurrent')} * 208.0 )  )"
							//power += " }"
							//entries += new SimEntry(to, power, getDelay(), time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'demandPower')
							entries += new SimEntry(to, '1.5', d, time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'deltaAvgCurrent')
							entries += new SimEntry(to, '1.6', d, time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'deltaMaxCurrent')
							//entries += new SimEntry(to, '0.312', d, time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'demandPower')
						}
						break;


					case "plug":
						bundle.getIds().eachWithIndex{ toO, i ->
							def d = getDelay()
							//builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt((0.6 + Math.sin(time/100000))^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'avgCurrent', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
							//builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt((0.8 + Math.sin(time/100000))^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'maxCurrent', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
							//builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt((((0.6 + Math.sin(time/100000)) * 208) / 1000)^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'demandPower', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
							String to = toO.toString()
							//entries += new SimEntry(to, 'Math.sqrt((0.6 + Math.sin(time/100000))^2)', getDelay(), time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'avgCurrent')
						//	entries += new SimEntry(to, 'Math.sqrt((0.8 + Math.sin(time/100000))^2)', getDelay(), time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'maxCurrent')
						//	entries += new SimEntry(to, 'Math.sqrt((((0.6 + Math.sin(time/100000)) * 208) / 1000)^2)', SimConfigExporter.getDelay(), time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'demandPower')

							entries += new SimEntry(to, '1.5', d, time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'avgCurrent')
							entries += new SimEntry(to, '1.6', d, time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'maxCurrent')
							entries += new SimEntry(to, '0.312', d, time, bundle.getAtoms()[i].componentName , bundle.getAtoms()[i].sensorName, 'demandPower')

						}
						break;
				}
			}
		}


	}


	public def findInRoom(DLComponent component, String typename, List<String> names, List<Integer> dataclasses){

		DLComponent room = component.getParentsOfType("room").first()
		List<DLComponent> racks = []
		room.getChildComponents().each{ k ->
			if(k.getManagedObjectsOfType(typename).size() > 0){
				racks += k
			}
		}

		List<DLObject> results = []
		racks.each{ r ->
			r.getManagedObjectsOfType("wsnsensor").each{ s ->
				println "checking sensor: ${s.getName()} ${s.getObjectSetting("dataclass")}"
				//if((s.getName().contains("exhaust") || s.getName().contains("hot")) && (s.getObjectSetting("dataclass").getValue() == 200)){
				//	results += s
				//}

				boolean hasName = false
				names.each{ if(s.getName().contains(it) ){hasName = true} }

				boolean hasDataclass = false
				dataclasses.each{ if (s.getObjectSetting("dataclass").getValue() == it){hasDataclass = true} }

				if(hasName && hasDataclass){
					results += s
				}
			}
		}

		return results
	}


	public void writeConfig(File configFile) {
		//ask the user for things!
		def setpoint = JOptionPane.showInputDialog("Cooler Supply Setpoint?", "68.0")
		def heatfactor = JOptionPane.showInputDialog("Rack Heat Factor?", "13.0")
		SUPPLY_TARGET = setpoint
		RACK_HEAT_FACTOR = heatfactor

		this.generateValues()

		delay = 250
		def spcount = entries.size()
		if (spcount) {
			delayStep = 100000.0 / spcount
		} else {
			delayStep = 100000.0
		}


		configFile.withWriter("UTF-8"){ writer ->
			def builder = new groovy.xml.MarkupBuilder(writer)
			def helper = new MarkupBuilderHelper(builder)
			helper.xmlDeclaration('version': 1.0, encoding: "UTF-8")

			builder.'simulator:simulator'('xmlns:simulator': "http://www.synapsense.com/plugin/simulator/generated", 'xmlns:xsi': "http://www.w3.org/2001/XMLSchema-instance", 'xsi:schemaLocation': "http://www.synapsense.com/devicemapper/simulator simulator.xsd ") {
				entries.sort{it.getKey()}.each{ it.writeToBuilder(builder) }
			}
		}

		//also put js library file
		File dir = configFile.getParentFile()
		File targetLibFile = new File(dir, "mapsense_sim.js")

		//get the library out of the jar
		InputStream filestream = getClass().getResourceAsStream("/resources/javascript/mapsense_sim.js");
		java.nio.file.Files.copy(filestream,targetLibFile.toPath(), StandardCopyOption.REPLACE_EXISTING)

		log.info("Done and done!", "message")

	}

	public String keyToSimFormat(DLObject o, String propName){
		//return o.getKey().replace(":", "_") + "_$propName"
		this.keyToSimFormat(o.getKey(),propName)
	}

	public String keyToSimFormat(String to, String propName){
		//return to.replace(":", "_") + "_$propName"
		def key = to.replace(":", "_") + "_$propName"
		return key

		//def script = "( ($key == null) ? $key : 55.0 )"
		//return script
		//return " properties.get('$key') "
	}

	public String functionWrapper(String value, String fallback){
		return "function simulate() { $value if(isNaN(result)){ return $fallback; }else {return result;} }"

	}

}


public class SimEntry{
	String serverTO
	DLObject sensor
	String value
	Long firstDelay
	String sleepDelay
	String componentName
	String sensorName
	String propertyName

	SimEntry() {}

	SimEntry(DLObject sensor, String value, Long firstDelay, String sleepDelay, String componentName, String sensorName) {
		this.sensor = sensor
		this.value = value
		this.firstDelay = firstDelay
		this.sleepDelay = sleepDelay
		this.componentName = componentName
		this.sensorName = sensorName
	}

	SimEntry(TO key, String value, Long firstDelay, String sleepDelay, String componentName, String sensorName, String propertyName) {
		this(key.toString(),value,firstDelay,sleepDelay,componentName,sensorName,propertyName)
	}

	SimEntry(DLObject sensor, String value, Long firstDelay, String sleepDelay, String componentName, String sensorName, String propertyName) {
		this(sensor.getKey(),value,firstDelay,sleepDelay,componentName,sensorName,propertyName)
	}

	SimEntry(String key, String value, Long firstDelay, String sleepDelay, String componentName, String sensorName, String propertyName) {
		this.serverTO = key
		this.value = value
		this.firstDelay = firstDelay
		this.sleepDelay = sleepDelay
		this.componentName = componentName
		this.sensorName = sensorName
		this.propertyName = propertyName
	}

	String getKey(){
		if(serverTO!= null){
			return serverTO
		} else {
			return sensor.getKey().toString()
		}
	}

	void writeToBuilder(MarkupBuilder builder){

		if(!propertyName){
			builder.'simulator:sensor'('id': this.getKey(), 'expression': value, 'firstDelay': firstDelay, 'sleepDelay': sleepDelay, componentName: componentName , sensorName: sensorName)
		} else {
			builder.'simulator:sensor'('id': this.getKey(), 'expression': value, 'firstDelay': firstDelay, 'sleepDelay': sleepDelay, componentName: componentName , sensorName: sensorName, property: propertyName)
		}
	}


}
