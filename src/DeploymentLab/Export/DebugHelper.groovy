package DeploymentLab.Export

import DeploymentLab.AutoMacAssigner
import DeploymentLab.CentralCatalogue
import DeploymentLab.Export.SoundBoard.SoundBoardGUI
import DeploymentLab.Image.DLImage
import DeploymentLab.MapHelper
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentProp
import DeploymentLab.Model.ComponentRole
import DeploymentLab.Model.ComponentType
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Model.RoomType
import DeploymentLab.ProgressManager
import DeploymentLab.ProjectSettings
import DeploymentLab.SceneGraph.ArbitraryShapeNode
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.SelectModel
import DeploymentLab.UndoBuffer
import DeploymentLab.channellogger.Logger
import java.awt.Point
import javax.swing.JComponent
import javax.swing.JOptionPane

/**
 * Stores debugging tools useful in development and testing but no so much in the field.
 * These all used to be knocking around in the main DeploymentLabWindow class,
 * but now there here to make things a little cleaner.
 * @author Gabriel Helman
 * @since Jupiter
 * Date: 3/26/12
 * Time: 2:28 PM
 */
class DebugHelper {
    private static final Logger log = Logger.getLogger(DebugHelper.class.getName())
    ComponentModel componentModel
    ObjectModel objectModel
    ProjectSettings projectSettings
    SelectModel selectModel
    UndoBuffer undoBuffer
    DeploymentPanel deploymentPanel


    public DebugHelper(ComponentModel cm, ObjectModel om, ProjectSettings ps, SelectModel sm, UndoBuffer ub, DeploymentPanel dp) {
        componentModel = cm
        objectModel = om
        projectSettings = ps
        selectModel = sm
        undoBuffer = ub
        deploymentPanel = dp
    }

    /**
     * Get a project into shape to be exported to a test server; resets all macids and ip addresses.
     * Does the following:
     * <ul>
     *     <li>reset es and ac server ips</li>
     *     <li>give everything a unique macid</li>
     *     <li>set all network panids</li>
     *     <li>set all gateway ip addresses</li>
     *     <li>set all modbus ip addresses</li>
     *     <li>set all snmp addresses</li>
     * </ul>
     *
     * todo: other integration, control
     *
     */
    public void prepForExport() {
        try {
            undoBuffer.startOperation()
            this.uniqueNamesForEverybody()
            this.clearMacIDs()
            this.assignMacIDs()
            Integer p = 0xAAAA
            componentModel.getComponentsByType("network").each { n ->
                n.setPropertyValue("pan_id", Integer.toHexString(p))
                p++
            }
            int addr = 0;
            componentModel.getComponentsByType("remote-gateway").each { rg ->
                rg.setPropertyValue("address", "10.0.0.$addr")
                addr++
            }
            componentModel.getComponentsByType("modbus-tcp-network").each { mod ->
                mod.setPropertyValue("port", "TCP:127.0.0.$addr")
                addr++
            }
            objectModel.getObjectsByType("snmpagent").each { agent ->
                componentModel.getOwner(agent).setPropertyValue("address", "UDP:127.0.0.$addr")
                addr++
            }

            componentModel.getComponentsInRole("controlleddevice").each { c ->
                c.listProperties().each { prop ->
                    if (prop.endsWith("_ip")) {
                        c.setPropertyValue(prop, "10.0.0.$addr")
                        addr++
                    } else if (prop.endsWith("_device") || prop.endsWith("_devid") || prop.endsWith("_port")) {
                        c.setPropertyValue(prop, '1')
                    }
                }
            }

	        componentModel.getComponentsInRole( ComponentRole.CONTROLLABLE ).each { c ->
		        // CFM Table is required in raised floor rooms.
	            if( c.getRoom().getPropertyValue( RoomType._PROPERTY ) == RoomType.RAISED_FLOOR ) {
		            if( c.hasProperty( ComponentProp.AIR_VOLUME_TBL ) ) {
			            c.setPropertyValue( ComponentProp.AIR_VOLUME_TBL, "20,100;30,;40,;50,;60,;70,;80,;90,;100,1000" )
		            }
	            }
	        }

	        // Pressure requires an altitude for the DC.
	        componentModel.getComponentsByType( ComponentType.DRAWING ).each { drawing ->
		        if( !componentModel.getComponentsByType( ComponentType.PRESSURE, drawing ).isEmpty() ) {
			        drawing.setPropertyValue( ComponentProp.ALTITUDE, 1000 )
		        }
	        }

            projectSettings.addAnnotation("Prepped for Export!")
            log.info("Ready for export!", "statusbar")
        } catch (Exception e) {
            log.error("IT SUPER BROKY!", "message")
            log.error(log.getStackTrace(e))
            undoBuffer.rollbackOperation()
        } finally {
            undoBuffer.finishOperation()
        }
    }

    SoundBoardGUI sb = null
    public void soundBoard()
    {
        log.info("Welcome to Sound Board!", "statusbar")

        if(sb == null || !sb.isOn)
        {
            sb = new SoundBoardGUI()
        }
    }

	/**
	 * Assigns fake macids to anything without a macid.
	 */
	public void assignMacIDs() {
		ProgressManager.doTask({
			log.info("Assigning Mac ID...", "progress")
			try {
				AutoMacAssigner autoMacAssigner = new AutoMacAssigner(objectModel, componentModel)
				autoMacAssigner.assignMacID()
				log.info('Assigned All Mac IDs successfully')
			} catch (Exception e) {
				log.error(e.getMessage(), 'message')
				log.error(log.getStackTrace(e))
			}
		})
	}

	/**
	 * Clears all macids.
	 */
	public void clearMacIDs() {
		ProgressManager.doTask({
			log.info("Clearing Mac ID...", "progress")
			try {
				AutoMacAssigner autoMacAssigner = new AutoMacAssigner(objectModel, componentModel)
				autoMacAssigner.clearMacID()
				log.info('Cleared All Mac IDs successfully')
			} catch (Exception e) {
				log.error(e.getMessage(), 'message')
				log.error(log.getStackTrace(e))
			}
		})
	}

	/**
	 * Fakes a unique name for everything in the project.
	 */
	public void uniqueNamesForEverybody() {
		ProgressManager.doTask({
			log.info("Reticulating Splines...", "progress")
			try {
				def names = [:]
				def refs = [:]
				componentModel.getComponents().each { c ->
					// Skip it if the name is hidden anyway
					if( c.getComponentProperty('name').displayed ) {
						MapHelper.increment(names, c.getName())
						MapHelper.addList(refs, c.getName(), c)
					}
				}
				names.each { name, numberOfThem ->
					if (numberOfThem > 1) {
						int i = 1
						refs[name].each { comps ->
							//int counter = ((DLComponent) comps).getManagedObjects().first().getDlid().hashCode()
							//((DLComponent) comps).setPropertyValue("name", (comps.getPropertyValue("name") + " " + counter.toString()))

							def newName = comps.getPropertyValue("name") + " $i"
							while(componentModel.getComponents().find {it.getName() == newName} ){
								i++
								newName = comps.getPropertyValue("name") + " $i"
							}

							((DLComponent) comps).setPropertyValue("name", newName)
							i++
						}
					}
				}
			} catch (Exception e) {
				log.error(e.getMessage(), 'message')
				log.error(log.getStackTrace(e))
			}
		})
	}

	/**
	 * Find everything without a parent.
	 */
	public void noParents() {
		//scan for objects w/o parents
		String noParents = ""
		objectModel.getObjects().each { o ->
			if (o.getParents().size() <= 0 && !o.getDeleted()) {
				noParents += "No parents: $o \n"
			}
		}
		log.info(noParents, "message")
	}

	/**
	 * Anonymize a Project; that is, remove all customer-specific information so it can be used locally for testing.
	 * Wipe app server ip
	 * Wipe control test ip
	 * Wipe all env. gateway ips
	 * Wipe all modbus network ips
	 */
	public void anonymizeProject() {
		try {
			undoBuffer.startOperation()
			log.info("Anonymize Project Go!")
			projectSettings.esHost = "localhost"
			projectSettings.acHost = ""
			componentModel.getComponentsByType("remote-gateway").eachWithIndex { gw, i ->
				println "looking at gw $gw"
				gw.setPropertyValue("address", "127.0.0.$i")
			}
			componentModel.getComponentsByType("modbus-tcp-network").each { mod ->
				mod.setPropertyValue("port", "TCP:127.0.0.1")
			}
			objectModel.getObjectsByType("snmpagent").each { agent ->
				componentModel.getOwner(agent).setPropertyValue("address", "UDP:127.0.0.1")
			}
			projectSettings.addAnnotation("I am not a number, I am a free man!")
		} catch (Exception ex) {
			log.error("Error in wipe!", "default")
			log.error(log.getStackTrace(ex), "default")
			undoBuffer.rollbackOperation()
		} finally {
			undoBuffer.finishOperation()
		}
	}

	/**
	 * Reset the WSNNODE checksum fields.
	 */
	public void flushChecksum() {
		//deploymentLabFrame.setCursor(Cursor.WAIT_CURSOR)
		undoBuffer.startOperation()
		objectModel.flushField("wsnnode", "checksum", -1) //set to all Fs
		undoBuffer.finishOperation()
		log.info(CentralCatalogue.getUIS("debug.resetChecksums"), "statusbar")
		//deploymentLabFrame.setCursor(null)
	}

	/**
	 * Set up a super-basic control setup based on the current coolers.
	 */
	public void toggleFakeCtrlSvrFlag() {
		useFakeControlServer = !useFakeControlServer
		if( useFakeControlServer ) {
			log.info("Using fake Control Server for expression testing.", "statusbar")
		} else {
			log.info("Using configured Control Server URI for expression testing.", "statusbar")
		}
	}
	public static boolean useFakeControlServer = false

	public void showContainedPolygon() {
		StringBuilder message = new StringBuilder()
		message.append("Listing Points of contained polygon:\n")
		DLComponent containedComponent = selectModel.getSelectedObjects().get(0)
		ArbitraryShapeNode node = deploymentPanel.getPNodeFor(containedComponent)
		def points = node.getPoints()
		int index = 0
		points.each { p ->
			message.append("$index:${p.toString()} \n")
			index++;
		}
		log.info(message.toString(), "message")
	}


	public void jmxConsole() {
		def comps = selectModel.getExpandedSelection()
		if (comps.size() > 0) {
			JMXConsole.JMXForComponents(projectSettings, comps)
		} else {
			log.info("Nothing selected", "message")
		}
	}


	public void clearLogicalIds() {
		int choice = JOptionPane.showConfirmDialog(CentralCatalogue.getParentFrame(), CentralCatalogue.getUIS("clearLogicalIds.message"), CentralCatalogue.getUIS("clearLogicalIds.title"), JOptionPane.YES_NO_OPTION)
		if (choice == JOptionPane.YES_OPTION) {
			try {
				objectModel.clearLogicalIDs()
				log.info("Logical IDs cleared.", 'message')
				projectSettings.addAnnotation("Reset Logical IDs")
			} catch (Exception e) {
				log.error("Clearing Logical IDs failed!\nSee log file for details.", 'message')
				log.error(log.getStackTrace(e))
			}
		}
	}

	public void hyperassoc(){
		if(selectModel.getExpandedSelection().size() != 2){
			log.error("need two!", "message")
			return
		}
		undoBuffer.startOperation()
		try{

		def one = selectModel.getExpandedSelection()[0]
		def two = selectModel.getExpandedSelection()[1]

/*
		def ac = new AssociationController(one, two)
		//ac.executeBundles()
		def results = ac.calculateAssociations(one,two,true)
		if(results.size() == 0){
			//go other way
			results = ac.calculateAssociations(two,one,true)
		}
		results.each { it.make = true }
		ac.executeBundles(results)
*/
		DLComponent con
		DLComponent pro
		if( one.listConsumers(false).size() > 0 ){
			//one is consumer
			con = one
			pro = two
		} else {
			//two is consumer
			con = two
			pro = one
		}

		con.listConsumers(false).each{ consumerName ->
			def consumer = con.getConsumer(consumerName)
			def producer = pro.getProducerObjectsOfType(consumer.getDatatype()).first()

			con.associate(consumerName, pro, producer.getId())

		}
		}catch(Exception e){
			undoBuffer.rollbackOperation()
			throw e
		} finally {
			undoBuffer.finishOperation()
		}


	}


	private static final List<String> names = []


	public int roll(int numDice, int diceSides) {
		def gygax = new Random()
		int result = 0
		for (int i = 0; i < numDice; i++) {
			result += gygax.nextInt(diceSides) + 1
		}

		return result
	}

	public void randomlySynergizeMeAFDrawing() {
		//ProgressManager.doTask({
			log.info("Reticulating Splines...", "progress")
			this.makeThatDrawing()
		//})
	}

	public void makeThatDrawing() {
		try {

			undoBuffer.startOperation()
			//def drawing = selectModel.getActiveDrawing()

			DLComponent drawing = componentModel.newComponent( ComponentType.DRAWING )

			def d = new Date()
			def time = "${d.getMonth()}-${d.getDay()} ${d.getHours()}:${d.getMinutes()}"

			drawing.setPropertyValue('name', "Drawing ${time}")


			if( FlickrFetch.getImageUrl() ){
				println "I got an image!"
				drawing.setPropertyValue("image_data", new DLImage(FlickrFetch.image))
				println "that image is in place!"
			}

			DLImage bgimage = drawing.getPropertyValue("image_data")

			//add net and room
			//add gateway
			//pick 1d6 placeable types
			//place 3d6 of each type
			def room = componentModel.newComponent("room")
			room.setPropertyValue("points", "0,0;${bgimage.getWidth()},0;${bgimage.getWidth()},${bgimage.getHeight()};0,${bgimage.getHeight()}");
			drawing.addChild(room)

			def wsn = componentModel.newComponent("network")
			def gateway = componentModel.newComponent("remote-gateway")
			drawing.addChild(wsn)
			wsn.addChild(gateway)

			gateway.setPropertyValue("x", bgimage.width / 2)
			gateway.setPropertyValue("y", bgimage.height / 2)

			def gygax = new Random()
			int numberOfTypes = roll(1, 6)
			def typesToUse = []
			def possibleTypes = []

			componentModel.listTypesWithRole("placeable").each { componentType ->
				def hasRequireAssoc = false
				def goesHere = true
				def other = true

				componentModel.componentTypes[componentType].consumer.each { a ->
					if (Boolean.parseBoolean(a.@required.text())) {
						hasRequireAssoc = true
					}
				}
				componentModel.componentTypes[componentType].producer.each { a ->
					if (Boolean.parseBoolean(a.@required.text())) {
						hasRequireAssoc = true
					}
				}

				goesHere = componentModel.typeCanBeChild(room.getType(), componentType) && componentModel.typeCanBeChild(wsn.getType(), componentType)

				def roles = componentModel.listComponentRoles(componentType)
				if (roles.contains("staticchildplaceable") || roles.contains("staticchild") || roles.contains("dynamicchild") || roles.contains("dynamicchildplacable")) {
					other = false
				}

				if (!hasRequireAssoc && goesHere && other) {
					possibleTypes += componentType
				}
			}

			for (int i = 0; i < numberOfTypes; i++) {
				typesToUse += possibleTypes[gygax.nextInt(possibleTypes.size())]
			}

			for (String t : typesToUse) {
				int typecount = roll(3, 6)
				//this will be the anchor location of the group
				int x = gygax.nextInt(bgimage.width)
				int y = gygax.nextInt(bgimage.height)

				for (int i = 0; i < typecount; i++) {
					//def rack = componentModel.newComponent("rack-control-rearexhaust-sf-thermanode2")
					def rack = componentModel.newComponent(t)

					// controller will add?  room.addChild(rack)
					// controller will add?  wsn.addChild(rack)
					rack.setPropertyValue("name", rack.getPropertyStringValue("name") + " ${i + 1}")
					//DLImage bgimage = drawing.getPropertyValue("image_data")
					rack.setPropertyValue("x", gygax.nextInt(bgimage.width))
					rack.setPropertyValue("y", gygax.nextInt(bgimage.height))
					//def loc = generateLocation(x, y, i)



					//rack.setPropertyValue("x", loc.x)
					//rack.setPropertyValue("y", loc.y)
				}
			}
		} catch (Exception e) {
			undoBuffer.rollbackOperation()
			log.error("trouble!")
			log.error(log.getStackTrace(e))
		} finally {
			undoBuffer.finishOperation()
		}

		this.prepForExport()
	}

	static final int offset = 60

	public Point generateLocation(int x, int y, int order) {
		def result = new Point()
		/*
		if (order % 3 == 0) {
			result.x = x + 3 * offset
		} else {
			result.x = x + (order % 3) * offset
		}
		result.y = Math.ceil(order / 3) * offset
		*/

		result.x = x + (roll(2,12) * offset)
		result.y = y + (roll(2,12) * offset)

		return result
	}

	/**
	 * Displays the curent input and action maps of a given JComponent so we can see what swing nonsense is messing us up.
	 * @param target any JComponent
	 */
	public static void showInputAndActionMaps(JComponent target){
		def msg = ""
		msg += "WHEN_ANCESTOR_OF_FOCUSED_COMPONENT \n"
		def map = target.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
		map.allKeys().each{
			msg += "$it == ${map.get(it)} \n"
		}
		msg += "WHEN_FOCUSED \n"
		map = target.getInputMap(JComponent.WHEN_FOCUSED)
		map.allKeys().each{
			msg += "$it == ${map.get(it)} \n"
		}
		msg += "WHEN_IN_FOCUSED_WINDOW \n"
		map = target.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
		map.allKeys().each{
			msg += "$it == ${map.get(it)} \n"
		}



		log.info(msg, "message")
	}

}
