
package DeploymentLab.Export

import DeploymentLab.Model.*
import groovy.xml.MarkupBuilderHelper
import javax.xml.transform.stream.StreamSource
import javax.xml.XMLConstants
import javax.xml.validation.SchemaFactory
import DeploymentLab.channellogger.Logger

public class DevmanConfigExporter {
	private static final Logger log = Logger.getLogger(DevmanConfigExporter.class.getName())

	public static void writeConfig(ObjectModel omodel, File configFile) {
		int idcounter = 1
		Map fakeIds = [:]
		//first, hash up some fake ids;
		omodel.getObjects().each { o ->
			fakeIds[ o.getDlid() ] = o.getType().toUpperCase() + ":" + idcounter++	
		}

		configFile.withWriter { writer ->
			def builder = new groovy.xml.MarkupBuilder(writer)
			//install XML declaration:
			def helper = new MarkupBuilderHelper(builder)
			helper.xmlDeclaration('version': 1.0, encoding: "UTF-8")

			builder.DMConfig(
					'xmlns': "http://www.synapsense.com/devicemapper/DMConfig", 
					'xmlns:xsi' : "http://www.w3.org/2001/XMLSchema-instance",
					"xsi:schemaLocation" : "http://www.synapsense.com/devicemapper/DMConfig DMConfig.xsd"){
				//all plugins
				def otherPlugins = ["wsn", "modbus", "XML", "WS", "SNMP","ipmi"]
				otherPlugins.each{ plugin ->
					def pluginNets = omodel.getObjectsByTag("name", "pluginId", plugin)
					if ( pluginNets.size > 0 ){
						builder.plugin( name: plugin ){
							pluginNets.each{ datasource ->
								makeObj(builder, datasource, fakeIds)
							}
						}
					}
				}
			} //end DMConfig
		}

		//validate against xsd
		try{
			def factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
			File xsdFile = new File("cfg/dmPluginConfig.xsd" )
			def schema = factory.newSchema(xsdFile)
			def validator = schema.newValidator()
			validator.validate(new StreamSource( configFile )  )
		} catch ( Exception ex){
			def message = "Error while validating configuration file! \n"
			message += "'${ex.getMessage()}'\n"
			message += "Configuration File will be saved but it may have errors." 
			log.error(message, "message")
		}
	}

	/**
	 * Builds the xml for a single object in the standalone dm config file, and then recurses into that object's children.
	 * @param builder the XMLBuilder to generate the xml with
	 * @param o the DLObject to generate XML for
	 * @param fakeIds a Map of faked To-style unique IDs to use for the objects in the form of [Dlid :  TO]
	 */
	protected static void makeObj(def builder, DLObject o, Map fakeIds){
		builder.obj(id: fakeIds[o.getDlid()]) {
			o.getPropertiesByType("setting").each { p->
				builder.prop( name: p.getName(), value: p.getValue() ){
					if ( p.hasTags() ){
						p.getTags().each{ t ->
							builder.tag( name: t.getTagName(), value: t.getValue())
						}


					}
				}
			}
			o.getChildren().each{ ch ->
				makeObj(builder, ch, fakeIds)
			}
		}
	}
}