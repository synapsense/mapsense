package DeploymentLab.Export

import DeploymentLab.CentralCatalogue
import DeploymentLab.DLProject
import DeploymentLab.Dialogs.LoginDialog
import DeploymentLab.FileFilters
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLObject
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Model.Setting
import DeploymentLab.ProgressManager
import DeploymentLab.channellogger.Logger
import DeploymentLab.channellogger.WorkingDir
import java.nio.channels.FileChannel
import javax.swing.JOptionPane
import javax.swing.filechooser.FileNameExtensionFilter

/**
 * An attempt to wrap the export logic that used to be in the DLW into it's own class.
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/24/12
 * Time: 10:59 AM
 */
class ExportController {
	private static final Logger log = Logger.getLogger(ExportController.class.getName())
	private static final String LINE_SEP = System.getProperty("line.separator")

	private DLProject project
	private ComponentModel componentModelToExport
	private ObjectModel objectModelToExport


	public ExportController(DLProject projectToExport, ComponentModel subComponentModel = null, ObjectModel subObjectModel = null){
		this.project = projectToExport

		if(subComponentModel && subObjectModel){
			this.componentModelToExport = subComponentModel
			this.objectModelToExport = subObjectModel
		} else {
			this.componentModelToExport = this.project.componentModel
			this.objectModelToExport = this.project.objectModel
		}


	}


	public void loginESDoExportTask(Properties prop, ExportTaskType taskType, boolean forceFullExport = false, boolean checkSync = true){
		String serverUrl = SynapEnvContext.populateProperties(prop,project.projectSettings)
		log.trace("Establishing server context to $serverUrl")
		SynapEnvContext ctx = new SynapEnvContext(prop)

		if(ctx.testConnect()) {
			switch (taskType){
				case ExportTaskType.EXPORT:
					this.doExportSchema(ctx,forceFullExport, checkSync)
					break
				case ExportTaskType.UNDOEXPORT:
					this.doUnExportSchema(ctx, true)
					break
				case ExportTaskType.UNDOEXPORTCASCADE:
					this.doUnExportSchema(ctx, false)
					break
                case ExportTaskType.ACCESS:
                    this.exportAccessList(ctx)
                    break
				default:
					assert false, "IT BROKYYYYYY!!!!"
					break
			}

		}else{
			log.error(ctx.getMessage(),'message');
			//ctx.logout()
		}
	}


	public void removeSchemaFromServer(boolean useRecursive = true) {
		if(project.getFile() == null) {
			log.error( CentralCatalogue.getUIS("unExportSchema.notExported"), 'message')
			return
		}

		int choice = JOptionPane.showConfirmDialog(CentralCatalogue.getParentFrame(), CentralCatalogue.getUIS("unExportSchema.message"), CentralCatalogue.getUIS("unExportSchema.title"), JOptionPane.YES_NO_OPTION)
		if(choice == JOptionPane.NO_OPTION){
			return
		}

		Properties connectProperties = new Properties()
		if(new LoginDialog(CentralCatalogue.getParentFrame()).getLoginInfo(connectProperties)){

			ExportTaskType unexportMode
			if ( useRecursive ){
				unexportMode = ExportTaskType.UNDOEXPORT
			} else {
				unexportMode = ExportTaskType.UNDOEXPORTCASCADE
			}

			ProgressManager.doTask{
				this.loginESDoExportTask(connectProperties,unexportMode)
			}
		}
	}


	protected void doUnExportSchema(ServerContext ctx, boolean useRecursive) {
		log.info(CentralCatalogue.getUIS("progress.saving"), 'progress')

		//doSave(openFile)
		//undoBuffer.clear()
		project.save()
		project.undoBuffer.clear()

		try {
			log.info(CentralCatalogue.getUIS("progress.analyzing"), 'progress')
			UnExporter unexporter = new UnExporter(ctx)

			log.info(CentralCatalogue.getUIS("progress.checkSync"), 'progress')
			if(!unexporter.checkSync(this.objectModelToExport)) {
				//message handled by checksync()
				return
			}
			log.info(CentralCatalogue.getUIS("progress.removing"), 'progress')

			boolean result

			if ( useRecursive ){
				result = unexporter.export(this.objectModelToExport)
			} else {
				result = unexporter.fastUnExport(this.objectModelToExport)
			}

			if(!result){
				throw new RuntimeException("failcode from unexporter")
			}

			log.info(CentralCatalogue.getUIS("progress.saving"), 'progress')
			//doSave(openFile)
			project.save()
			log.info(CentralCatalogue.getUIS("unExportSchema.success"), 'message')

		} catch (com.synapsense.exception.ServerConfigurationException sce){
			log.error(CentralCatalogue.getUIS("exportSchema.alreadyExporting"), "message")
			return
		} catch (java.lang.IllegalStateException ise){
			log.error(CentralCatalogue.getUIS("exportSchema.alreadyExporting"), "message")
			return
		} catch(Exception e) {
			log.info(CentralCatalogue.getUIS("progress.saving"), 'progress')
			//doSave(openFile)
			project.save()
			log.error(log.getStackTrace(e))
			log.error(CentralCatalogue.getUIS("unExportSchema.fatal"), 'message')
		}
		//ctx.logout()
	}

	/**
	 * Exports the project to the server.  There must be a valid logged-in server context by this point, and
	 * the assumption is that export validation has already passed.
	 * @param ctx
	 * @param forceFullExport
	 * @param checkSync
	 */
	protected void doExportSchema(ServerContext ctx, boolean forceFullExport = false, boolean checkSync = true) {
		def wd = new WorkingDir()
		boolean saveSuccessful

		FileNameExtensionFilter fil = FileFilters.ZipProject.getFilter()
		if (! project.getFile().toFile().getName().contains( fil.getExtensions().first() )){
			log.error("Project must be saved in the DLZ format before exporting.", "message")
			return
		}

		def exportStart = Calendar.getInstance()
//		if(undoBuffer.getUndoState() != undoSavePoint || projectSettings.needSave()) {
//			log.info("Saving Project...", 'progress')
//			doSave(openFile,true)
//		}

		project.undoBuffer.clear()

		Exporter exporter = new Exporter(ctx)
		if (checkSync) {
			log.info("Checking for project sync...", 'progress')
			if(!exporter.checkSync(this.objectModelToExport)) {
				//error message handled by checkSync()
				return
			}
		}

		if(!exporter.checkVersion()) { return }

		//log.trace("Preparing Images for Export...", 'progress')
/*
//now do the image export as part of the save
		ImageExporter ie = new ImageExporter()
		try {
			ie.exportToEs(componentModel)
		} catch (IOException ioe) {
			//if we get an IO exception when saving the image files, let the user know and bail out of the process.
			log.error("IO Error when saving background image: ${ioe.getMessage()}", 'message')
			return
		} catch(Exception e) {
			log.error(log.getStackTrace(e))
			log.error("Error processing images for export!\nUnable to continue.", 'message')
			return
		}
*/
		boolean exportSuccess = false
		boolean backupSuccess = false

		//try{
		try {
			log.info("Stopping ES Services...", 'progress')
			exporter.stopES()
			//Note: in the cases where stopES throws an exception, we do NOT want to call configComplete
		} catch (com.synapsense.exception.ServerConfigurationException sce){
			log.error(CentralCatalogue.getUIS("exportSchema.alreadyExporting"), "message")
			return
		} catch (java.lang.IllegalStateException ise){
			log.error(CentralCatalogue.getUIS("exportSchema.alreadyExporting"), "message")
			return
		} catch (Exception e) {
			log.error("Unable to stop ES Services for Export. Unable to continue.  \nES may not be running. See log for details.", 'message')
			log.error(log.getStackTrace(e))
			return
		}
		try{
			try{
				exporter.updateExportTs(this.objectModelToExport,this.componentModelToExport )
				exporter.updateModelVersion(this.objectModelToExport)

                if (project.accessModel && !project.accessModel.isEmpty()){
                    exporter.exportAccessModel(project.accessModel)
                }else{
                    log.info("Skipping Access Model export")
                }

				if(project.projectSettings.refreshAllObjects || forceFullExport ){
					log.info("Clearing old values to force export objects")
					this.objectModelToExport.clearOldValues()
					this.componentModelToExport .resetImages()
				} else {
					// We still may need to purge some old values to deal with the hack mentioned in the exporter where
					// we create oldvalue for SmartZone objects that aren't exported. If they are now being exported,
					// then we need to clear the old value or the property won't get exported unless it has changed.
					for( DLObject o : project.objectModel.getObjects() ) {
						if( o.isExportable() && o.isSmartZoneLinked() && !o.hasKey() ) {
							for( Setting prop : o.getPropertiesByType('setting') ) {
								prop.setOldValue( null )
							}
						}
					}
				}

				if (project.projectSettings.refreshAllLinks || forceFullExport ){
					this.objectModelToExport.makeEverythingDirty() //this handles all links being refreshed
					log.info("Performing a full export on Links.")
				}

				//perform any needed wsn migration before saving the file for upload
				log.info("Migrating Keys...", 'progress')
				exporter.updateESkeys(this.objectModelToExport)

				//since we're always saving now, and the saved file is what we're sending, we need to do this after the exportTs is updated
				log.info("Saving Project...", 'progress')
				//def fileWithProcessedImages = doSave(openFile,true)
				//def fileWithProcessedImages = project.save(true)

				File fileWithProcessedImages = File.createTempFile(project.getFile().toFile().getName(), ".dlt")
				//project.save(true,fileWithProcessedImages)

				//build a sub-project here to do the save with
				DLProject subProject = new DLProject(project.undoBuffer,componentModelToExport,objectModelToExport, project.configurationReplacer, false)
				subProject.setFileButDontLock(fileWithProcessedImages)  // todo: refactor DLProject so this isn't needed.
				if( subProject.export(fileWithProcessedImages) ) {
					log.info("processed file is $fileWithProcessedImages")
					log.debug("File Prepared for Upload: ${fileWithProcessedImages.getAbsolutePath()}", "default")
					log.info("Exporting Project...", 'progress')

					exportSuccess = exporter.dlisExport(this.objectModelToExport, (project.projectSettings.refreshAllLinks || forceFullExport), (project.projectSettings.refreshAllRules || forceFullExport), (project.projectSettings.refreshAllObjects || forceFullExport), fileWithProcessedImages)
				} else {
					log.error("Failed to create temporary project file. Unable to perform the export.", 'message')
				}
			} catch(Exception e) {
				log.error(log.getStackTrace(e))
				log.error("Fatal error saving schema. For more details see log file.", 'message')
			}

			log.info("Saving Project...", 'progress')
			if ( exportSuccess ) {
				project.projectSettings.refreshAllLinks = false
				project.projectSettings.refreshAllRules = false
				project.projectSettings.refreshAllObjects = false
				if ( forceFullExport ){
					project.projectSettings.addAnnotation("Forced full export")
				}
				project.projectSettings.exported()
			}
			//saveSuccessful = true
			//doSave(openFile)
			saveSuccessful = project.save()

			if (saveSuccessful){
				project.unlockProjectFile()
				exporter.storeDLZFile(project.getFile().toFile())
				project.lockProjectFile()
			}

			File backupFile
			FileChannel saved
			FileChannel backup
			try{
				log.info("Backing up project...", 'progress')
				//File backupDir = new File(appProperties.getProperty("backup.dir"))
				File backupDir = new File( wd.getBackupDir() )
				backupDir.mkdirs()
				String extension = exportSuccess ? "success" : "failed"
				backupFile = File.createTempFile(project.getFile().toFile().getName(), ".$extension", backupDir)
				log.info("Saving to backup file '$backupFile'")
				project.unlockProjectFile()
				saved = new FileInputStream(project.getFile().toFile()).getChannel()
				backup = new FileOutputStream(backupFile).getChannel()
				saved.transferTo(0, saved.size(), backup)
				saved.close()
				backup.close()
				backupSuccess = true
			} catch (IOException ioe) {
				//log.error("Unable to save backup file!", "message")
				saved.close()
				backup.close()
				if ( backupFile != null && backupFile.exists() ){
					log.error("deleting bad backupfile; = " + backupFile.delete(), "default")
				}
			} finally {
				project.lockProjectFile()
			}

		} finally {
			log.info("Restarting ES Services...", 'progress')
			exporter.restartES()
		}

		def exportFinish = Calendar.getInstance()
		log.info("Total Process Export Time = ${(exportFinish.getTimeInMillis() - exportStart.getTimeInMillis()) / 1000} seconds" )

		if (! backupSuccess ){
			log.warn("Unable to save backup file!", "message")
		}

		if ( exportSuccess && saveSuccessful ) {
			//ie.cleanupOldImages()
			log.info("Schema saved successfully.", 'message')

		} else if ( exportSuccess && !saveSuccessful ) {
			log.info("Schema exported successfully, but project file could not be saved.", 'message')
			//file save error should already be in error log, and the user already got a fail message box

		} else if ( !exportSuccess && saveSuccessful ) {
			log.warn("Export completed with errors.  Old background images will remain.")
			def errors = exporter.getExportErrors()
			if( !errors.isEmpty() ) {
				def errorMessage = errors.inject(''){str, err -> str += (err.message + ' : ' + err.object + LINE_SEP)}
				log.error("Export Errors:\n" + errorMessage, 'message')
			}
		} else { //both failed
			log.warn("Export completed with errors and the project file could not be saved.  Old background images will remain.")
			def errors = exporter.getExportErrors()
			if( !errors.isEmpty() ) {
				def errorMessage = errors.inject(''){str, err -> str += (err.message + ' : ' + err.object + LINE_SEP)}
				log.error("Export Errors:\n" + errorMessage, 'message')
			}
		}
		//ctx.logout()
	}

	public void checkSyncOnly(){
		//login overhead
		Properties prop = new Properties()
		if(new LoginDialog(CentralCatalogue.getParentFrame()).getLoginInfo(prop)){
			ProgressManager.doTask{
				log.trace("Checking Sync...", "progress")
				String serverUrl = SynapEnvContext.populateProperties(prop,project.projectSettings)
				SynapEnvContext ctx = new SynapEnvContext(prop)
				if(ctx.testConnect()) {
					Exporter exporter = new Exporter(ctx)
					if(! exporter.checkVersion(false) ){
						//no message needed; checkVersion already kicks out a message
						return
					}
					//messages handled by checkSync()
					exporter.checkSync(this.objectModelToExport,true)
				}else{
					log.error(ctx.getMessage(),'message');
				}
			}
		}
	}

    public void exportAccessList(ServerContext ctx) {
        log.info("Start access list export")
        Exporter exporter = new Exporter(ctx)

	    if(!exporter.checkSync(this.objectModelToExport)) { return }
	    if(!exporter.checkVersion()) { return }

        try {
            log.info("Stopping ES Services...", 'progress')
            exporter.stopES()
            //Note: in the cases where stopES throws an exception, we do NOT want to call configComplete
        } catch (com.synapsense.exception.ServerConfigurationException sce) {
            log.error(CentralCatalogue.getUIS("exportSchema.alreadyExporting"), "message")
            return
        } catch (java.lang.IllegalStateException ise) {
            log.error(CentralCatalogue.getUIS("exportSchema.alreadyExporting"), "message")
            return
        } catch (Exception e) {
            log.error("Unable to stop ES Services for Export. Unable to continue.  \nES may not be running. See log for details.", 'message')
            log.error(log.getStackTrace(e))
            return
        }

        try {
	        exporter.updateExportTs(project.objectModel, project.componentModel)
	        exporter.updateModelVersion(project.objectModel)
            exporter.exportAccessModel(project.accessModel)
        } finally {
            log.info("Restarting ES Services...", 'progress')
            exporter.restartES()
        }

        log.info("Sync origin object model", "progress")
	    project.accessModel.syncKeys()
        log.info("Access List successfully exported.", "message")
        project.save()
    }

}