
package DeploymentLab.Export

import com.synapsense.service.Environment

import com.synapsense.dto.TO
import com.synapsense.dto.TOFactory

import com.synapsense.exception.ObjectNotFoundException

import DeploymentLab.Model.*
import DeploymentLab.channellogger.*
import java.text.DateFormat
import DeploymentLab.CentralCatalogue
import com.synapsense.service.DLImportService
import com.synapsense.exception.DLImportException

public class UnExporter {
	private static final Logger log = Logger.getLogger(UnExporter.class.getName())

	private ServerContext ctx
	private Environment env
	private ObjectModel omodel

	private int exportCounter
	private int exportTotal
	private Properties appProperties
	private Properties uiStrings

	//delegate as much as possible to the exporter version of things
	private Exporter exporter

	UnExporter(ServerContext _ctx) {
		ctx = _ctx
		env = ctx.getEnv()
		appProperties = CentralCatalogue.INSTANCE.getAppProperties()
		uiStrings = CentralCatalogue.INSTANCE.getUIStrings()

		exporter = new Exporter(_ctx)
	}

	boolean fastUnExport(ObjectModel om){
		omodel = om
		log.info("Stopping ES services...", 'progress')
		env.configurationStart()
		log.info("ES stopped", 'progress')

		log.info("Cascade Deleting...", 'progress')
		for(DLObject o : omodel.roots){
			def to = TOFactory.getInstance().loadTO(o.key)
			env.deleteObject(to, -1)
		}

        // todo: Hey Gabe! Fixed Bug 7425 (see this.recursiveExport()) so it works for the 'real' unexport. Doesn't work
        // for this one and I don't know anything about the server-side export stuff. Since this is an experimental feature,
        // I'll let you take care of it or at least wait for you to guide me through the server-side code baseline.

		log.info("Restarting ES services...", 'progress')
		env.configurationComplete()
		log.info("ES restarted", 'progress')

		log.info("Clearing ObjectModel keys", 'progress')
		omodel.clearKeys()

		return true
	}


	boolean export(ObjectModel om) {
		omodel = om

		log.info("Stopping ES services...", 'progress')
		env.configurationStart()
		log.info("ES stopped", 'progress')

		log.info("Looking up ROOT object")

		def roots = env.getObjectsByType('ROOT')
		TO<?> rootTO
		if(!roots) {
			log.error("No ROOT object on this server, nothing to remove.", 'message')
			return false
		}

		/*
		log.info("Removing Objects from Server...", "progress")
		def deletedTOs = []
		def objs = new ArrayList<DLObject>(omodel.getObjects())
		objs.addAll(omodel.listDeletedObjects())
		for(def o : objs){
			if(o.key != null && o.key != ''){
				deletedTOs += loadTO(o.getKey())
			}
		}
		DLImportService importer = ctx.getImporter()
		def result = true
		try{
			importer.removeObjects(deletedTOs)
			omodel.clearKeys()
			//any deleted object without keys will be pruned at save time, so we don't need to prune them here
		} catch (DLImportException dlie){
			log.error("failure! $dlie")
			result = false
		} catch (Exception e){
			log.error("worse failure! $e")
			result = false
		} finally{
			log.info("Restarting ES services...", 'progress')
			env.configurationComplete()
		}
		return result
		*/


		log.info("Deleting pending objects...", 'progress')
		omodel.listDeletedObjects().each { o ->
			if(o.key != null && o.key != '')
				if(deleteObject(o))
					omodel.prune(o)
		}

		exportTotal = omodel.getObjectCount()
		omodel.roots.each{ root ->
			exportCounter = 1
			recursiveExport(root)
		}

		log.info("Restarting ES services...", 'progress')
		env.configurationComplete()
		log.info("ES restarted", 'progress')
		
		log.info("Clearing ObjectModel keys", 'progress')
		omodel.clearKeys()

		return true
	}

	def seen = []
	private void recursiveExport(DLObject o) {

		if(o in seen) {
			log.warn("Preventing infinite recursion!")
			return
		}

		seen += o
		o.getChildren().each{ child ->
			recursiveExport(child)
		}

        // getChildren doesn't return non-childlink properties that are references to objects, leaving them behind on the server.
        for( Dockingpoint dock : o.getPropertiesByType('dockingpoint') ) {
            if( dock && dock.getReferrent() ) {
                recursiveExport( dock.getReferrent() )
            }
        }

		try {
			TO<?> to
			def updates = []
			log.info("Deleting object $exportCounter/$exportTotal", 'progress')
			exportCounter ++
			if(objectExists(o)) {
				deleteObject(o)
			}
		} catch(Exception e) {
			log.error("Delete of object $o failed with exception: " + log.getStackTrace(e))
		}
	}

	private boolean deleteObject(DLObject o) {
		try {
			TO<?> to = loadTO(o.key)
			log.debug("Deleting object '$o' with TO '$to'")
			env.deleteObject(to, -1)

			//attached rules now deleted automatically by ES

			o.getPropertiesByType('dockingpoint').each{ p ->
				p.makeDirty()
			}
			o.getPropertiesByType('collection').each{ p ->
				p.makeDirty()
			}

			toCache.remove(o.key)
			return true
		} catch(Exception e) {
			log.error("Exception deleting object '$o' :" + log.getStackTrace(e))
			return false
		}
	}


	// Utility functions
	private toCache = [:]
	private TO<?> loadTO(String key) {
		if(key == null || key == '')
			throw new NullPointerException("Can't load empty TO!")
		TO<?> to = toCache[key]
		if(to == null) {
			to = TOFactory.getInstance().loadTO(key)
			toCache[key] = to
		}
		return to
	}

	private String saveTO(TO<?> to) {
		if(to == null)
			throw new NullPointerException("Can't save empty TO!")
		String ser = TOFactory.getInstance().saveTO(to)
		toCache[ser] = to
		return ser
	}

	private boolean objectExists(DLObject o) {
		//shell out to the better-maintained exporter version
		return exporter.objectExists(o)
	}

    /**
     * Check for sync with the server. This version differs from the Exporter version in its point of view as to what
     * "in sync" means. The Exporter version is only concerned with timestamps being in sync and returns true for all
     * other cases, therefore allowing an export in all cases except where the timestamp differs.
     * <p>
     * This version is more true to the name and returns true only if the project exists on the server and the timestamp is
     * the same. It returns false in cases where the project has not been exported or does not exist on the server for some
     * other reason. Therefore, an UnExport is not necessary in those cases since there is nothing to UnExport.
     *
     * @param omodel The object model to check against the server
	 * @return true if the project exists on the server and has a matching timestamp.
     */
    boolean checkSync(ObjectModel omodel) {
        log.debug("checkSync(): omodel roots = ${omodel.roots}")
        log.info("ESAPI: Looking up ROOT object")

        def roots = env.getObjectsByType('ROOT')
        TO<?> rootTO
        if(roots) {
            // TODO: Handle multi-dc.
            rootTO = roots[0]

            DLObject syncObj = omodel.getObjectsByType( ObjectType.DRAWING ).first()
            if(syncObj.key == null || syncObj.key == '') {
                log.warn("$syncObj object had no key, must have never been exported.")
                log.error(CentralCatalogue.getUIS("unExportSchema.notExported"), "message")
                return false
            } else {
                if(objectExists(syncObj)) {
                    TO<?> syncObjTO = loadTO(syncObj.key)
                    long esExportTs = env.getPropertyValue(syncObjTO, 'lastExportTs', Long.class)
                    long exportTs = syncObj.getObjectProperty('lastExportTs').getValue()
                    log.info("Checking project export timestamp of '$exportTs' (${new Date(exportTs).toString()}) against server, '$esExportTs' (${new Date(esExportTs).toString()}).")
                    if (esExportTs != exportTs) {
                        log.error("Sync Error! Project export timestamp did not match ES export timestamp!")
                        log.error("ES is out-of-sync!")
                        def df = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL)
                        log.error(String.format(CentralCatalogue.getUIS("checkSync.notInSync"), df.format(new Date(exportTs)), df.format(new Date(esExportTs))), "message")
                        return false
                    }
                    log.info("ES is in-sync")
                    return true
                } else {
                    log.warn("$syncObj object had a key, but it couldn't be found in the ES!")
                    log.error(CentralCatalogue.getUIS("exporter.syncObjIsMissing"), "message")
                    return false
                }
            }
        } else {
            log.info("ROOT object not found, must be fresh install.")
            log.error(CentralCatalogue.getUIS("exporter.syncObjIsMissing"), "message")
            return false
        }
    }
}

