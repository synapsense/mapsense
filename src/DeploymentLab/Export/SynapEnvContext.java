package DeploymentLab.Export;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import DeploymentLab.CentralCatalogue;
import DeploymentLab.ProjectSettings;
import com.synapsense.dto.TO;
import com.synapsense.service.*;

import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

import DeploymentLab.channellogger.*;

/**
 * Convenience class which realizes access to Environment API.
 */
public class SynapEnvContext implements ServerContext {
	private Properties connectProperties;

	private Environment env;
	private RulesEngine cre;
	private DLImportService dlImport;
	private static InitialContext ctx = null;
	private static final Logger log = Logger.getLogger(SynapEnvContext.class.getName());
	private String msg = "";

	public static String populateProperties(Properties prop, ProjectSettings projectSettings){
		return populateProperties(prop, projectSettings.getEsHost());
	}

	public static String populateProperties(Properties prop, String host){
		String serverUrl = "remote://" + host + ":4447";
		prop.setProperty(Context.INITIAL_CONTEXT_FACTORY,"org.jnp.interfaces.NamingContextFactory");
		prop.setProperty(Context.URL_PKG_PREFIXES,"org.jboss.naming:org.jnp.interfaces");
		prop.setProperty(Context.PROVIDER_URL, serverUrl);
		prop.setProperty("SynapEnvRemote", "SynapServer/Environment/remote");
		return serverUrl;
	}


	public SynapEnvContext(Properties connectProps) {
		connectProperties = connectProps;
	}

	public boolean testConnect() {
		log.info("Authenticating...", "progress");
		String username = connectProperties.getProperty("username");

		if (init()) {
			UserManagementService userManagementService = null;

			try {
				userManagementService = ServerLoginModule.getProxy(ctx,
						"es-ear/es-core/UserManagementService!com.synapsense.service.UserManagementService",
						UserManagementService.class);

				if (userManagementService == null) {
					//msg = "Unable to contact Application Server at '" + connectProperties.get(Context.PROVIDER_URL) + "'!";
					msg = String.format( CentralCatalogue.getUIS("synapEnvContext.unableToContact"),  connectProperties.get(Context.PROVIDER_URL) );
					log.error("Got a null UserManagementService", "default");
					return false;
				}

				TO<?> u = userManagementService.getUser(username);

				if (u == null) {
					msg = "Authentication failed!";
					log.info("User [" + username + "] failed", "default");
					return false;
				} else {
					msg = "Authentication successful!";
					log.info("User [" + username + "] successful", "default");
					return true;
				}

			} catch (Exception e) {
				msg = "Authentication failed!";
				log.info("User [" + username + "] failed :" + e.getLocalizedMessage(), "default");
				return false;
			}

		} else {
			//msg = "Unable to contact Application Server at '" + connectProperties.get(Context.PROVIDER_URL) + "'!";
			msg = String.format( CentralCatalogue.getUIS("synapEnvContext.unableToContact"),  connectProperties.get(Context.PROVIDER_URL) );
			return false;
		}

	}

	// Delay loading until a proxy is absolutely needed
	private boolean init() {
		final String INITIAL_CONTEXT_FACTORY = "org.jboss.naming.remote.client.InitialContextFactory";
		
		String PROVIDER_URL = connectProperties.getProperty(Context.PROVIDER_URL);
		String username = connectProperties.getProperty("username");
		String password = connectProperties.getProperty("password");
		connectProperties.setProperty(Context.INITIAL_CONTEXT_FACTORY,INITIAL_CONTEXT_FACTORY);
		connectProperties.setProperty(Context.PROVIDER_URL, PROVIDER_URL);
		connectProperties.setProperty("jboss.naming.client.ejb.context", "true");
		connectProperties.setProperty("jboss.naming.client.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");
		connectProperties.setProperty(Context.SECURITY_PRINCIPAL, username);
		connectProperties.setProperty(Context.SECURITY_CREDENTIALS, password);
		connectProperties.setProperty("SynapEnvRemote", "SynapServer/Environment/remote");

		logout();//force logout for previous credentials
		
		try {
			ctx = new InitialContext(connectProperties);
			env = ServerLoginModule.getProxy(ctx,"es-ear/es-core/Environment!com.synapsense.service.Environment",Environment.class);
			cre = ServerLoginModule.getProxy(ctx,"es-ear/es-core/RulesEngineService!com.synapsense.service.RulesEngine",RulesEngine.class);

			dlImport = ServerLoginModule.getProxy(ctx,"es-ear/es-core/DLImportService!com.synapsense.service.DLImportService", DLImportService.class);

			//ServerLoginModule.login(username, password);
		}
		catch (Exception e) {
			log.error("init() error:" + log.getStackTrace(e), "default");
			return false;
		}
		return true;
	}

	public void logout() {
		//don't bother logging out if we didn't actually log IN
		if (ctx == null) { return; }
		try {
			ctx.close();
			ctx = null;
			env = null;
			cre = null;
			dlImport = null;
		} catch(Exception e) {
			log.error("Error logging out of the server.  What, the thing won't let us give up privileges???\n" + log.getStackTrace(e), "default");
		}
	}

	public Environment getEnv() {
		if(env == null)
			init();
		return env;
	}

	/**
	 * Gets a proxy to the new Rules Engine API for interfacing with that part of ES. 
	 * @return a proxy that implements com.synapsense.service.RulesEngine.
	 */
	public RulesEngine getCRE() {
		if(cre == null)
			init();
		return cre;
	}


	public DLImportService getImporter(){
		if(dlImport == null)
			init();
		return dlImport;

	}
	public String getMessage(){
		return msg;
	}
}

