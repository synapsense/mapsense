package DeploymentLab.Export;

import com.synapsense.service.DLImportService;
import com.synapsense.service.Environment;
import com.synapsense.service.RulesEngine;

/**
 * A marker interface we can use to swap out a fake context provider for the purposes of writing tests.
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/24/12
 * Time: 11:30 AM
 */
public interface ServerContext {

	public Environment getEnv();

	public DLImportService getImporter();

	public RulesEngine getCRE();

	public boolean testConnect();

}
