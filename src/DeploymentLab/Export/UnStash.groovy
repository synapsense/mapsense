package DeploymentLab.Export

import DeploymentLab.Model.ObjectType
import com.synapsense.service.Environment
import javax.naming.Context
import com.synapsense.dto.BinaryData
import com.synapsense.dto.TO
import groovyjarjarcommonscli.Options
import groovyjarjarcommonscli.CommandLineParser
import groovyjarjarcommonscli.PosixParser
import groovyjarjarcommonscli.CommandLine
import groovyjarjarcommonscli.HelpFormatter
import DeploymentLab.ProjectSettings

/**
 * Designed to pull any and all DLZ files stored in the database as binary slugs.
 * @author Gabriel Helman
 * @since Mars
 * Date: 11/11/11
 * Time: 10:42 AM
 *
 * Use: java -cp .\deploymentlab.jar DeploymentLab.Export.UnStash -s<hostname of server> -u<user> -p<password>
 *
 */
class UnStash {
	SynapEnvContext ctx
	private Environment env

	public static void main(String[] args) {

		String host
		String username
		String password

		try {
			Options options = new Options();
			options.addOption("s", true, "hostname of the Environment Server")
			options.addOption("u", true, "username")
			options.addOption("p", true, "password")
			options.addOption("h", false, "shows help")

			CommandLineParser parser = new PosixParser();
			CommandLine cmd = parser.parse(options, args);

			if (cmd.hasOption("h")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("UnStash", options);
				return
			}

			if (!cmd.hasOption("s") || !cmd.hasOption("u") || !cmd.hasOption("p")) {
				println "missing option"
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("UnStash", options);
				return
			}

			host = cmd.getOptionValue("s")
			username = cmd.getOptionValue("u")
			password = cmd.getOptionValue("p")

		} catch (Exception e) {
			println "bad command or filename"
		}

		def f = new UnStash()
		f.fetch(host, username, password)
	}

	public UnStash() {}

	/**
	 * Logs into ES, finds all objects that store DLZ blobs, stores the contents of their dlzFile fields.
	 * @param host
	 * @param username
	 * @param password
	 * @return
	 */
	public fetch(String host, String username, String password) {
		println "username = $username"
		println "pwd = $password"
		try {
			Properties prop = new Properties()
			prop.put('username', username)
			prop.put('password', password)
			String serverUrl = SynapEnvContext.populateProperties(prop,host)
			println "attempting to connect to $serverUrl"
			ctx = new SynapEnvContext(prop)
			if (ctx.testConnect()) {
				env = ctx.getEnv()
				def dlzStores = env.getObjectsByType( ObjectType.DRAWING.toUpperCase() )
				for (TO<?> dlzStore: dlzStores) {
					def name = env.getPropertyValue(dlzStore, "name", java.lang.String)
					println "loading DLZ for '$name'"
					def blob = env.getPropertyValue(dlzStore, "dlzFile", BinaryData)
					if (blob == null) {
						println "no DLZ file!"
						continue
					}
					//save blob to disk right here
					def filename = "${name}_${dlzStore.getID()}.dlz"
					def f = new File(filename)
					f.setBytes(blob.getValue())
					println "file saved as '$filename'"
				}
			} else {
				println "Cannot connect!"
			}
		} catch (Exception e) {
			println "Exception while attempting do do something:"
			println "${e.class.getName()} : ${e.getMessage()}"
			e.printStackTrace()
		}
	}


}
