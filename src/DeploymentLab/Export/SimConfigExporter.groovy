
package DeploymentLab.Export

import DeploymentLab.Model.*
import DeploymentLab.channellogger.*
import groovy.xml.MarkupBuilderHelper
import DeploymentLab.ProjectSettings

/**
 * Generates a sim config file for the "wsnlike" simulator.
 */
public class SimConfigExporter {
	private static final Logger log = Logger.getLogger(SimConfigExporter.class.getName())

	//sleepdelay is the sampling interval


	/**
	 *
	 * <code>
			<xs:complexType name="sensor">
				<xs:attribute name="id" type="xs:string" use="required"/>
				<xs:attribute name="firstDelay" type="xs:int" use="required"/>
				<xs:attribute name="sleepDelay" type="xs:int" use="required"/>
				<xs:attribute name="expression" type="xs:string" use="required"/>
				<xs:attribute name="property" type="xs:string" use="optional"/>
				<xs:attribute name="type" type="xs:string" use="optional"/>
				<xs:attribute name="componentName" type="xs:string" use="optional"/>
				<xs:attribute name="sensorName" type="xs:string" use="optional"/>
			</xs:complexType>
	 * </code>
	 *
	 * @param cmodel
	 * @param omodel
	 * @param configFile
	 * @param otherItems
	 * @param ps
	 */
	public static void writeConfig(ComponentModel cmodel, ObjectModel omodel, File configFile, List<SimBundle> otherItems = null, ProjectSettings ps, ObjectModel amodel = null ) {
		boolean exportOModel = checkConfig(omodel)
		boolean exportAModel = false
		if (amodel){
			if (amodel.getObjectsByType('wsnsensor').size() != 0){
				exportAModel = checkConfig(amodel)
			}
		}

		if (!exportOModel && !exportAModel){
			log.info("Please Export Schema or Access List before Exporting Sim Config", 'message')
			return
		}
		int spcount = omodel.getObjectsByType('wsnsensor').size() + omodel.getObjectsByType('modbusproperty').size() + omodel.getObjectsByType('soapmethod').size()
		spcount += (exportAModel)?amodel.getObjectsByType('wsnsensor').size():0

		if (otherItems) {
			int otherCount = 0
			otherItems.each { oi ->
				otherCount += oi.getIds().size()
			}
			spcount += otherCount
		}

		delay = 250 //5
		//need to check for projects with no sensors at all to account for PUE-type projects.
		if (spcount) {
			//delayStep = 300000.0 / spcount
			delayStep = 100000.0 / spcount
		} else {
			//delayStep = 300000.0
			delayStep = 100000.0
		}

		configFile.withWriter("UTF-8") { writer ->
			def builder = new groovy.xml.MarkupBuilder(writer)
			def helper = new MarkupBuilderHelper(builder)
			helper.xmlDeclaration('version': 1.0, encoding: "UTF-8")

			builder.'simulator:simulator'('xmlns:simulator': "http://www.synapsense.com/plugin/simulator/generated", 'xmlns:xsi': "http://www.w3.org/2001/XMLSchema-instance", 'xsi:schemaLocation': "http://www.synapsense.com/devicemapper/simulator simulator.xsd ") {
				if (exportAModel) {
					amodel.getObjectsByType('wsnsensor').each { sensor ->
						int type = sensor.getObjectProperty('type').getValue()
						String sensorname = sensor.getObjectProperty('name').getValue()
						def itr = sensor.parents.iterator()
						String componentName = ""
						if (itr.hasNext()){
							componentName = itr.next().getObjectProperty('name').getValue();
						}

						println "processing ${sensor} : ${sensor.getKey()} : $type : $sensorname : ${getExpression(type, sensorname)}"

						String value = checkValue(type, sensorname, sensor.getDlid().toString(), ps)
						String time = checkTime("300000", sensor.getDlid().toString(), ps)

						'simulator:sensor'('id': sensor.getKey(), 'expression': value, 'firstDelay': getDelay(), 'sleepDelay': time, componentName: componentName, sensorName: sensorname)
					}
				}
				if (exportOModel){
					omodel.getObjectsByType('wsnsensor').each{ sensor ->
						int type = sensor.getObjectProperty('type').getValue()
						String sensorname = sensor.getObjectProperty('name').getValue()

						println "processing ${sensor} : ${sensor.getKey()} : $type : $sensorname : ${getExpression(type,sensorname)}"

						String value = checkValue(type, sensorname, sensor.getDlid().toString(), ps)
						String time = checkTime("300000", sensor.getDlid().toString(), ps)

						'simulator:sensor'('id': sensor.getKey(), 'expression': value, 'firstDelay': getDelay(), 'sleepDelay': time, componentName: cmodel.getManagingComponent(sensor).getName() , sensorName: sensorname)
					}

					omodel.getObjectsByType('ipmisensor').each{ ipmisensor ->
						'simulator:sensor'('id': ipmisensor.getKey(), 'expression': getIPMIExpression(ipmisensor.getObjectProperty('z').getValue(),ipmisensor.getObjectProperty('name').getValue()), 'firstDelay': getDelay(),  'sleepDelay': 300000, componentName: cmodel.getManagingComponent(ipmisensor).getName() , sensorName: ipmisensor.getObjectProperty('name').getValue())
					}
					omodel.getObjectsByType('modbusproperty').each{ modbusproperty ->
						'simulator:sensor'('id': modbusproperty.getKey(), 'expression': getModbusExpression(), 'firstDelay': getDelay(), 'sleepDelay': 300000, componentName: cmodel.getManagingComponent(modbusproperty).getName() , sensorName: modbusproperty.getObjectProperty('name').getValue())
					}

					omodel.getObjectsByType('snmpobject').each{ snmpobject ->
						'simulator:sensor'('id': snmpobject.getKey(), 'expression': getSNMPExpression(), 'firstDelay': getDelay(), 'sleepDelay': 3000000, 'type' : 'string', componentName: cmodel.getManagingComponent(snmpobject).getName() , sensorName: snmpobject.getObjectProperty('name').getValue())
					}

					omodel.getObjectsByType("controlout_crah").each{ crah ->
						'simulator:sensor'('id': crah.getKey(), 'expression': '72' , 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'setpoint', componentName: cmodel.getManagingComponent(crah).getName() , sensorName: crah.getObjectProperty('resource').getValue())
						'simulator:sensor'('id': crah.getKey(), 'expression': '42' , 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'capacity', componentName: cmodel.getManagingComponent(crah).getName() , sensorName: crah.getObjectProperty('resource').getValue())
						'simulator:sensor'('id': crah.getKey(), 'expression': getExpression(1, 'return'), 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'deviceRat', componentName: cmodel.getManagingComponent(crah).getName() , sensorName: crah.getObjectProperty('resource').getValue())
						'simulator:sensor'('id': crah.getKey(), 'expression': getExpression(1, 'supply'), 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'deviceSat', componentName: cmodel.getManagingComponent(crah).getName() , sensorName: crah.getObjectProperty('resource').getValue())
					}
					omodel.getObjectsByType("controlout_vfd").each{ vfd ->
						'simulator:sensor'('id': vfd.getKey(), 'expression': 100, 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'setpoint', componentName: cmodel.getManagingComponent(vfd).getName() , sensorName: vfd.getObjectProperty('resource').getValue())
						'simulator:sensor'('id': vfd.getKey(), 'expression': '5 + Math.sin(time/1000000)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'kw', componentName: cmodel.getManagingComponent(vfd).getName() , sensorName: vfd.getObjectProperty('resource').getValue())
					}


					if ( otherItems ){
						otherItems.each{ bundle ->
							switch ( bundle.getTypeName() ){
								case "phase":
									bundle.getIds().eachWithIndex { to, i ->
										builder.'simulator:sensor'('id': to, 'expression': 120.0, 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'voltage', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName )
									}
									break;

								case "phase_delta":
									bundle.getIds().eachWithIndex { to, i ->
										builder.'simulator:sensor'('id': to, 'expression': 208.0, 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'voltage', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName )
									}
									break;

								case "phase_duo":

									bundle.getIds().eachWithIndex{ to, i ->
										builder.'simulator:sensor'('id': to, 'expression': 120.0, 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'voltage', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
										builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt((0.6 + Math.sin(time/100000))^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'avgCurrent', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
										builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt((0.8 + Math.sin(time/100000))^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'maxCurrent', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
										//builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt( ((0.6 + Math.sin(time/100000)) * 208) ^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'demandPower', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
									}
									break;

								case "phase_duo_delta":

									bundle.getIds().eachWithIndex{ to, i ->
										builder.'simulator:sensor'('id': to, 'expression': 208.0, 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'voltage', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
										builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt((0.6 + Math.sin(time/100000))^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'deltaAvgCurrent', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
										builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt((0.8 + Math.sin(time/100000))^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'deltaMaxCurrent', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
										//builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt( ((0.6 + Math.sin(time/100000)) * 208) ^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'demandPower', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
									}
									break;


								case "plug":
									bundle.getIds().eachWithIndex{ to, i ->
										builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt((0.6 + Math.sin(time/100000))^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'avgCurrent', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
										builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt((0.8 + Math.sin(time/100000))^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'maxCurrent', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
										builder.'simulator:sensor'('id': to, 'expression': 'Math.sqrt((((0.6 + Math.sin(time/100000)) * 208) / 1000)^2)', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'demandPower', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
									}
									break;
/*
								case "rpdu":
									bundle.getIds().eachWithIndex{ to, i ->
										builder.'simulator:sensor'('id': to, 'expression': '1', 'firstDelay': getDelay(),  'sleepDelay': 300000, 'property' : 'status', 'type' : 'integer', componentName: bundle.getAtoms()[i].componentName , sensorName: bundle.getAtoms()[i].sensorName)
									}
*/
							}
						}
					}
				}
			}
		}
		log.info("Simulator configuration saved.", 'message')
	}
	
	public static boolean checkConfig(ObjectModel omodel){
		if(omodel.getObjectsByType('wsnsensor').findAll{it.getKey().equals("")}.size()>0)
			return false
		
		if(omodel.getObjectsByType('modbusproperty').findAll{it.getKey().equals("")}.size()>0)
			return false
		
//		if(omodel.getObjectsByType('soapmethod').findAll{it.getKey().equals("")}.size()>0)
//			return false
			
		return true
	}
	
	private static long delayStep //= 5
	private static double delay //= 5
	private static long getDelay() {
		delay += delayStep
		return (long)delay
	}

	private static def typeMap = [
			//internal temp:
		1 : '55.240088574 + Math.sin(time/1000000)',
			//internal rH:
		2 : '71.7450564398 + Math.sin(time/1000000)',
			//battery:
		5 : '3.3788881858 + Math.sin(time/1000000)',
			//door sensor
		9: '0.0',
			//leak detector
		12 :'1.0000000000',
		20: '10.9526315789 + Math.sin(time/1000000)',
		21: '55.6261651904 + Math.sin(time/1000000)',
		22: '55.7261651904 + Math.sin(time/1000000)',
		23: '114.9898621528 + Math.sin(time/1000000)',
		24: '0.0513996756 + Math.sin(time/1000000)',
		25: '55.8261651904 + Math.sin(time/1000000)',
		26: '101.9898621528 + Math.sin(time/1000000)',
			//temperature from thermistor:
		27: '55.5261651904 + Math.sin(time/1000000)',
		28: '43.3376480098 + Math.sin(time/1000000)',
			//subfloor pressure (pressure2):
		//29: '0.0513996756 + Math.sin(time/1000000)',
		29: '0.0513996756 + (Math.sin(time/1000000) * 0.015 )',
			//current transducer:
		30: '101.9898621528 + Math.sin(time/1000000)',
			//flow
		31: '43.3376480098 + Math.sin(time/1000000)',
			//CHWE Energy
		32: '43.3376480098 + Math.sin(time/1000000)',
			//CHWE Temperature
		33: '55.5261651904 + Math.sin(time/1000000)',

		//temperature from thermistor:
		36: '54 + Math.sin(time/1000000)',

			100000: '1.0000000000'
	]

	private static def nameMap = [
		/*
		"cold" : '72.5 + Math.sin(time/1000000)',    //+/- 1 degree
		"hot" : '80.5 + Math.sin(time/1000000)',
		"side" : '77.5 + Math.sin(time/1000000)',
		"supply" : '64.5 + Math.sin(time/1000000)',
		"return" : '90 + Math.sin(time/1000000)'
		*/
//		"cold" : '70.75 + ( Math.sin(time/1000000) * 6.25 )',
//		"hot" : '81.3 + ( Math.sin(time/1000000) * 4.3 )',
//		"cold" : '60.75 + (( Math.sin(time/1000000) * 6.25 ) * 2)',
//		"hot" : '98.6 + (( Math.sin(time/1000000) * 4.3 ) * 2)',
		"cold" : '69.3 + (( Math.sin(time/1000000) * 6.25 ) * 2)',
		"hot" : '74.7 + (( Math.sin(time/1000000) * 4.3 ) * 2)',
		//"side" : '76.4 + ( Math.sin(time/1000000) * 4 )',
		"side" : '72.0 + ( Math.sin(time/1000000) * 4 )',
		"supply" : '68.0 + (Math.sin(time/500000) * 5 )', //+/- 5 degrees
		"return" : '85 + (Math.sin(time/500000) * 5)'
	]

	private static String getExpression(int type, String name = "") {
		def gygax = new Random()
		def result

		if ( name.contains("cold") || name.contains("intake") ){
			result = nameMap["cold"]
		}else if ( name.contains("hot") || name.contains("exhaust") ){
			result = nameMap["hot"]
		} else if ( name.contains( "side" ) ){
			result = nameMap["side"]
		} else if ( name.contains( "supply" ) ){
			result = nameMap["supply"]
		} else if ( name.contains( "return" ) ){
			return nameMap["return"]
		} else if ( type == 2 ){
			//humidity recommended range is 40-55

			result = "${38 + (gygax.nextInt(11)+1) + (gygax.nextInt(11)+1) } + Math.sin(time/1000000)"
		} else {
			if(typeMap.containsKey(type)){
				result = typeMap[type]
			} else{
				log.warn("Type '$type' not found in expression map.  Returning expression for type '27' (Thermanode Thermistor).")
				result = typeMap[27]
			}
		}

		//rack temperature recommended range is 64-80 degrees f
		//adjust based on layer (+/- 2 degrees per layer from top)
		if(name.contains("mid")){
			result += " - 2"
		} else if (name.contains("bot")){
			result += " - 4"
		} else if (name.contains("sub")){
			result += " - 6"
		} else if (name.contains("chi")){
			result += " + 2"
		}
		return result;
	}

	private static String getIPMIExpression(Double z, String name) {
		def result
		if (z==131072.0){
			if (name.equals("intaketemp")|| name.equals("tempsensor")){
				result='61+Math.sin(time/1000000)'
			}else{
				result='66+Math.sin(time/1000000)'
			}
		}else if (z==16384.0){
			if (name.equals("intaketemp")|| name.equals("tempsensor")){
				result='60.5+Math.sin(time/1000000)'
			}else{
				result='65.5+Math.sin(time/1000000)'
			}
		}else if (z==2048.0){
			if (name.equals("intaketemp")|| name.equals("tempsensor")){
				result='60+Math.sin(time/1000000)'
			}else{
				result='65+Math.sin(time/1000000)'
			}
		} else{
			result='60+Math.sin(time/1000000)'
		}
		return result;
	}
	
	public static String getModbusExpression() {
		return '143.3 + Math.sin(time/1000000)'
	}

	private static String getSNMPExpression() {
		return '1138.0 + Math.sin(time/1000000)'
	}

	public static String checkValue(int type, String name = "", String dlid, ProjectSettings ps){
		//check for the key's location in the configured sim
		String value = null
		ps.configuredSimulator.each{ m ->
			println "comparing '${m["dlid"]}' to '${dlid}' and '${m["value"]}'"
			if ( m["dlid"].equalsIgnoreCase(dlid) && m["value"].trim().size() > 0 ){
				value = m["value"]
			}
		}
		if (value == null){
			value = getExpression(type,name)
		}
		return value
	}

	public static String checkTime(String passthrough, String dlid, ProjectSettings ps){
		//check for the key's location in the configured sim
		String value = null
		ps.configuredSimulator.each{ m ->
			println "comparing '${m["dlid"]}' to '${dlid}' and '${m["value"]}'"
			if ( m["dlid"].equalsIgnoreCase(dlid) && m["time"].trim().size() > 0 ){
				value = m["time"]
			}
		}
		if (value == null){
			value = passthrough
		}
		return value
	}

}


public class SimBundle<T> {
	private String typeName
	private List<T> ids
	private List<SimBundleAtom> atoms

	public SimBundle( String tn, List<T> i ){
		typeName = tn;
		ids = i
		atoms = []
	}

	public String getTypeName(){
		return typeName
	}

	public List<T> getIds(){
		return ids
	}

	public void add( T item ){
		ids.add(item)
	}

	public void addAtom( SimBundleAtom atom ){
		atoms.add(atom)
	}

	public List<SimBundleAtom> getAtoms(){
		return atoms
	}


	public String toString ( ) {
	return "SimBundle{" +
	"typeName='" + typeName + '\'' +
	", ids=" + ids +
	'}' ;
	}
}

public class SimBundleAtom {
	public String componentName
	public String sensorName

	SimBundleAtom(String componentName, String sensorName) {
		this.componentName = componentName
		this.sensorName = sensorName
	}



}