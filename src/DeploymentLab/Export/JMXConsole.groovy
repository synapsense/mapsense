package DeploymentLab.Export

import DeploymentLab.ProjectSettings
import DeploymentLab.channellogger.Logger
import com.synapsense.dto.TO
import com.synapsense.dto.TOFactory
import com.synapsense.service.impl.dao.to.PropertyTO

import javax.management.MBeanServerConnection
import javax.management.ObjectName
import javax.management.remote.JMXConnector
import javax.management.remote.JMXConnectorFactory
import javax.management.remote.JMXServiceURL

import DeploymentLab.Model.*

/**
 * Connects to the JMX MBean of the cache service and examines the values of the objects in the cache/
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/23/12
 * Time: 9:57 AM
 */
class JMXConsole {
	private static final Logger log = Logger.getLogger(JMXConsole.class.getName());

	public static void JMXForComponent(ProjectSettings projectSettings, DLComponent c) {
		String result = "<html>" + JMXValuesForComponent(projectSettings, c) + "</html>"
		log.info(result, "html")
	}


	public static void JMXForComponents(ProjectSettings projectSettings, List<DLComponent> comps) {
		StringBuilder result = new StringBuilder()
		result.append("<html>")
		for (DLComponent c : comps) {
			result.append(JMXValuesForComponent(projectSettings, c))
		}
		result.append("</html>")
		log.info(result.toString(), "html")
	}


	public static String JMXValuesForComponent(ProjectSettings projectSettings, DLComponent c) {

		//JMXServiceURL url = new JMXServiceURL("service:jmx:remoting-jmx://localhost:9999");
		JMXServiceURL url = new JMXServiceURL("service:jmx:remoting-jmx://${projectSettings.esHost}:9999");
		JMXConnector jmxc = JMXConnectorFactory.connect(url, null);
		MBeanServerConnection mbsc = jmxc.getMBeanServerConnection();

		//Set<ObjectName> names = new TreeSet<ObjectName>(mbsc.queryNames(null, null));
		//def sb = new StringBuilder()
		//for (ObjectName name : names) {
		//sb.append("ObjectName = $name \n")
		//}
		//log.info(sb.toString(), "message");

		def cache = new ObjectName("com.synapsense:type=CacheSvc")

		StringBuilder sb = new StringBuilder()

		//def result = mbsc.invoke(cache, "queryPropInfo", ["WSNGATEWAY", 70, "name", "java.lang.String"].toArray(), ["java.lang.String", "java.lang.Integer", "java.lang.String", "java.lang.String"].toArray(new String[4]))

		sb.append("<h1>")
		sb.append(c.getName())
		sb.append("</h1>")

		for (DLObject o : c.getManagedObjects()) {
			if (!o.hasKey()) {
				sb.append(o.toString())
				sb.append(" Never Exported <br>")
				continue
			}
			sb.append("<table>")
			sb.append("<tr>")
			sb.append("<td>")
			sb.append("<b> ${o.getKey()} </b>")
			sb.append("</td>")
			sb.append("</tr>")
			TO to = TOFactory.instance.loadTO(o.getKey())
			//build a list of things to go get
			def props = []
			for (Property p : o.getProperties()) {
				if (p.getType() == "childlink") {
					continue
				}
				props.add(p)
			}
			props.sort {it.getName()}
			for (Property p : props) {
				String valuetype = "ERROR!!!"
				if (p instanceof Setting) {
					valuetype = p.getValueType()
				} else if (p instanceof Dockingpoint || p instanceof DeploymentLab.Model.Collection) {
					valuetype = TO.class.getName().toString()
				} else if (p instanceof Pier) {
					valuetype = PropertyTO.class.getName().toString()
				} else if (p instanceof Rule || p instanceof GroovyRule) {
					valuetype = Double.class.getName().toString()
				}
				def result = mbsc.invoke(cache, "queryPropInfo", [to.typeName, to.ID, p.getName(), valuetype].toArray(), ["java.lang.String", "java.lang.Integer", "java.lang.String", "java.lang.String"].toArray(new String[4]))
				sb.append("<tr>")
				sb.append("<td>")
				sb.append(p.getName())
				sb.append("</td>")
				sb.append("<td>")
				sb.append(p.getType())
				sb.append("</td>")
				sb.append("<td>")
				sb.append(valuetype)
				sb.append("</td>")
				sb.append("<td>")
				if (result.equals("null") || result == null) {
					result = "<i>$result</i>"
				}
				sb.append(result)
				sb.append("</td>")
				sb.append("</tr>")
			}
			sb.append("</table>")
			sb.append("<hr>")
		}
		jmxc.close();
		return sb.toString()
	}


}
