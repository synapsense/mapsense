package DeploymentLab.Export.SoundBoard;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

public class JSpinnerFilter extends DocumentFilter
{
    SpinnerNumberModel _spm;
    public JSpinnerFilter(SpinnerNumberModel spm)
    {
        _spm = spm;
    }

    //Nakes sure new String only adds digit
    @Override
    public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
        if (stringContainsOnlyDigits(string)) {
            super.insertString(fb, offset, string, attr);
        }
    }

    //Remove any character
    @Override
    public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {
        super.remove(fb, offset, length);
    }

    //Make sure characters are valid
    @Override
    public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
        if (stringContainsOnlyDigits(text))
        {
            super.replace(fb, offset, length, text, attrs);
        }
    }

    //Makes sure only valid characters are contained in the string
    private boolean stringContainsOnlyDigits(String text)
    {
        int numDecimals = 0;

        for (int i = 0; i < text.length(); i++)
        {
            if (text.charAt(i) == '.')
            {
                numDecimals++;

                if(numDecimals >= 2)
                {
                    return false; //Two decimals in a number???
                }
            }

            if (text.charAt(i) != '.' && text.charAt(i) != '-' &&!Character.isDigit(text.charAt(i)))
            {
                return false;
            }
        }


        return true;
    }
}

class JSpinnerMaker
{
    private enum timeType
    {
        delay(0), on(1), off(2);

        private int value;

        private timeType(int value)
        {
            this.value = value;
        }
    }

    public static List<Component> generateSpinners(MasterPanel.Controller controller, final ComponentHandler componentHandler, final boolean isMasterMode, final MasterPanel masterPanel)
    {
        List<Component> spinners = new ArrayList<>();

        SpinnerNumberModel mean = new SpinnerNumberModel();
        SpinnerNumberModel stdev = new SpinnerNumberModel();
        SpinnerNumberModel trend = new SpinnerNumberModel();
        SpinnerNumberModel threshold = new SpinnerNumberModel();

        //Makes spinnerNumbers for the different spinners for different controllers
        switch(controller)
        {
            case TEMPERATURE:
                setSpinnerModel(SoundBoardConstants.tempMeanStartVal,SoundBoardConstants.tempMeanMinVal, SoundBoardConstants.tempMeanMaxVal, SoundBoardConstants.tempMeanStepVal, mean);
                setSpinnerModel(SoundBoardConstants.tempStDevStartVal,SoundBoardConstants.tempStDevMinVal, SoundBoardConstants.tempStDevMaxVal, SoundBoardConstants.tempStDevStepVal, stdev);
                setSpinnerModel(SoundBoardConstants.tempTrendStartVal,SoundBoardConstants.tempTrendMinVal, SoundBoardConstants.tempTrendMaxVal, SoundBoardConstants.tempTrendStepVal, trend);
                setSpinnerModel(SoundBoardConstants.tempThreshStartVal,SoundBoardConstants.tempThreshMinVal, SoundBoardConstants.tempThreshMaxVal, SoundBoardConstants.tempThreshStepVal, threshold);
                componentHandler.setMeanMax(SoundBoardConstants.tempMeanMaxVal);
                componentHandler.setMeanMin(SoundBoardConstants.tempMeanMinVal);
                break;
            case PRESSURE:
                setSpinnerModel(SoundBoardConstants.presMeanStartVal, SoundBoardConstants.presMeanMinVal, SoundBoardConstants.presMeanMaxVal, SoundBoardConstants.presMeanStepVal, mean);
                setSpinnerModel(SoundBoardConstants.presStDevStartVal, SoundBoardConstants.presStDevMinVal, SoundBoardConstants.presStDevMaxVal, SoundBoardConstants.presStDevStepVal, stdev);
                setSpinnerModel(SoundBoardConstants.presTrendStartVal, SoundBoardConstants.presTrendMinVal, SoundBoardConstants.presTrendMaxVal, SoundBoardConstants.presTrendStepVal, trend);
                setSpinnerModel(SoundBoardConstants.presThreshStartVal,SoundBoardConstants.presThreshMinVal, SoundBoardConstants.presThreshMaxVal, SoundBoardConstants.presThreshStepVal, threshold);
                componentHandler.setMeanMax(SoundBoardConstants.presMeanMaxVal);
                componentHandler.setMeanMin(SoundBoardConstants.presMeanMinVal);
                break;
            case SUPPLY:
                setSpinnerModel(SoundBoardConstants.supMeanStartVal, SoundBoardConstants.supMeanMinVal, SoundBoardConstants.supMeanMaxVal, SoundBoardConstants.supMeanStepVal, mean);
                setSpinnerModel(SoundBoardConstants.supStDevStartVal, SoundBoardConstants.supStDevMinVal, SoundBoardConstants.supStDevMaxVal, SoundBoardConstants.supStDevStepVal, stdev);
                setSpinnerModel(SoundBoardConstants.supTrendStartVal, SoundBoardConstants.supTrendMinVal, SoundBoardConstants.supTrendMaxVal, SoundBoardConstants.supTrendStepVal, trend);
                setSpinnerModel(SoundBoardConstants.tempThreshStartVal,SoundBoardConstants.tempThreshMinVal, SoundBoardConstants.tempThreshMaxVal, SoundBoardConstants.tempThreshStepVal, threshold);
                componentHandler.setMeanMax(SoundBoardConstants.supMeanMaxVal);
                componentHandler.setMeanMin(SoundBoardConstants.supMeanMinVal);
                break;
            case RETURN:
                setSpinnerModel(SoundBoardConstants.retMeanStartVal, SoundBoardConstants.retMeanMinVal, SoundBoardConstants.retMeanMaxVal, SoundBoardConstants.retMeanStepVal, mean);
                setSpinnerModel(SoundBoardConstants.retStDevStartVal, SoundBoardConstants.retStDevMinVal, SoundBoardConstants.retStDevMaxVal, SoundBoardConstants.retStDevStepVal, stdev);
                setSpinnerModel(SoundBoardConstants.retTrendStartVal, SoundBoardConstants.retTrendMinVal, SoundBoardConstants.retTrendMaxVal, SoundBoardConstants.retTrendStepVal, trend);
                setSpinnerModel(SoundBoardConstants.tempThreshStartVal,SoundBoardConstants.tempThreshMinVal, SoundBoardConstants.tempThreshMaxVal, SoundBoardConstants.tempThreshStepVal, threshold);
                componentHandler.setMeanMax(SoundBoardConstants.retMeanMaxVal);
                componentHandler.setMeanMin(SoundBoardConstants.retMeanMinVal);
                break;
        }

        //Set current values for component handler
        componentHandler.setMeanValue(Double.parseDouble(mean.getValue().toString()));
        componentHandler.setStandardDeviation(Double.parseDouble(stdev.getValue().toString()));
        componentHandler.setTrend(Double.parseDouble(trend.getValue().toString()));
        componentHandler.setThreshold(Double.parseDouble(threshold.getValue().toString()));

        //Make all of the spinners

        final JSpinner meanSpinner = new JSpinner();
        meanSpinner.setModel(mean);
        JSpinnerMaker.makeDigitsOnlySpinnerUsingDocumentFilter(meanSpinner, mean, controller, true);
        meanSpinner.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                double num = Double.parseDouble(JSpinnerMaker.getText(meanSpinner));

                if(num != componentHandler.getMeanValue())
                {
                    componentHandler.setMeanValue(num);
                }

                if(isMasterMode)
                {
                    if (!masterPanel.isMasterOn())
                    {
                        masterPanel.updateSelection();
                        masterPanel.turnMasterOn();
                    }
                }

                componentHandler.start();
            }
        });

        final JSpinner stdevSpinner = new JSpinner(stdev);
        JSpinnerMaker.makeDigitsOnlySpinnerUsingDocumentFilter(stdevSpinner, stdev, controller, true);
        stdevSpinner.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                double num = Double.parseDouble(JSpinnerMaker.getText(stdevSpinner));

                if(num != componentHandler.getStandardDeviation())
                {
                    componentHandler.setStandardDeviation(num);
                }

                if(isMasterMode)
                {
                    if (!masterPanel.isMasterOn())
                    {
                        masterPanel.updateSelection();
                        masterPanel.turnMasterOn();
                    }
                }

                componentHandler.start();
            }
        });

        final JSpinner trendSpinner = new JSpinner(trend);
        JSpinnerMaker.makeDigitsOnlySpinnerUsingDocumentFilter(trendSpinner, trend, controller, true);
        trendSpinner.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                double num = Double.parseDouble(JSpinnerMaker.getText(trendSpinner));

                if(num != componentHandler.getTrend())
                {
                    componentHandler.setTrend(num);
                }

                if(isMasterMode)
                {
                    if (!masterPanel.isMasterOn())
                    {
                        masterPanel.updateSelection();
                        masterPanel.turnMasterOn();
                    }
                }

                componentHandler.start();
            }
        });

        final SpinnerNumberModel sendDelay = new SpinnerNumberModel(SoundBoardConstants.sendTimeStartVal,SoundBoardConstants.sendTimeMinVal,SoundBoardConstants.sendTimeMaxVal,SoundBoardConstants.sendTimeStepVal);
        final JSpinner sendSpinner = new JSpinner(sendDelay);
        JSpinnerMaker.setupTimeSpinner(sendSpinner,sendDelay, timeType.delay, componentHandler, isMasterMode,masterPanel);
        componentHandler.setSendDelay(SoundBoardConstants.sendTimeStartVal);

        final JSpinner threshSpinner = new JSpinner(threshold);
        JSpinnerMaker.makeDigitsOnlySpinnerUsingDocumentFilter(threshSpinner, threshold, controller, true);
        threshSpinner.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                double num = Double.parseDouble(JSpinnerMaker.getText(threshSpinner));

                if(num != componentHandler.getThreshold())
                {
                    componentHandler.setThreshold(num);
                }

                if(isMasterMode)
                {
                    if (!masterPanel.isMasterOn())
                    {
                        masterPanel.updateSelection();
                        masterPanel.turnMasterOn();
                    }
                }

                componentHandler.start();
            }
        });

        final SpinnerNumberModel onTime = new SpinnerNumberModel(SoundBoardConstants.onTimeStartVal,SoundBoardConstants.onTimeMinVal,SoundBoardConstants.onTimeMaxVal,SoundBoardConstants.onTimeStepVal);
        final JSpinner onSpinner = new JSpinner(onTime);
        JSpinnerMaker.setupTimeSpinner(onSpinner,onTime, timeType.on, componentHandler, isMasterMode,masterPanel);
        componentHandler.setAutoOnTime(SoundBoardConstants.onTimeStartVal);

        final SpinnerNumberModel offTime = new SpinnerNumberModel(SoundBoardConstants.offTimeStartVal,SoundBoardConstants.offTimeMinVal,SoundBoardConstants.offTimeMaxVal,SoundBoardConstants.offTimeStepVal);
        final JSpinner offSpinner = new JSpinner(offTime);
        JSpinnerMaker.setupTimeSpinner(offSpinner,offTime, timeType.off, componentHandler, isMasterMode,masterPanel);
        componentHandler.setAutoOffTime(SoundBoardConstants.offTimeStartVal);

        componentHandler.setSpinner(meanSpinner);

        //Add spinners to list and ship them out
        spinners.add(meanSpinner);
        spinners.add(stdevSpinner);
        spinners.add(trendSpinner);
        spinners.add(sendSpinner);
        spinners.add(threshSpinner);
        spinners.add(onSpinner);
        spinners.add(offSpinner);

        return spinners;
    }

    //Used for non master channels
    public static List<Component> generateSpinners(MasterPanel.Controller controller, final ComponentHandler componentHandler)
    {
        return generateSpinners(controller,componentHandler,false, null);
    }

    //Used for setting spinner Model and save space and repeating lines
    private static void setSpinnerModel(int value, int min, int max, int step, SpinnerNumberModel snm)
    {
        snm.setValue(value);
        snm.setMinimum(min);
        snm.setMaximum(max);
        snm.setStepSize(step);
    }

    //Same as above just for double
    private static void setSpinnerModel(double value, double min, double max, double step, SpinnerNumberModel snm)
    {
        snm.setValue(value);
        snm.setMinimum(min);
        snm.setMaximum(max);
        snm.setStepSize(step);
    }

    public static void makeDigitsOnlySpinnerUsingDocumentFilter(final JSpinner spinner, final SpinnerNumberModel spm, MasterPanel.Controller cont, boolean includeFocus)
    {
        JSpinner.NumberEditor jsEditor = (JSpinner.NumberEditor) spinner.getEditor();
        final Document jsDoc = jsEditor.getTextField().getDocument();

        //Apply Document Filter on spinners
        if (jsDoc instanceof PlainDocument)
        {
            AbstractDocument doc = new PlainDocument() {

                private static final long serialVersionUID = 1L;

                @Override
                public void setDocumentFilter(DocumentFilter filter) {
                    if (filter instanceof JSpinnerFilter) {
                        super.setDocumentFilter(filter);
                    }
                }
            };
            doc.setDocumentFilter(new JSpinnerFilter(spm));
            jsEditor.getTextField().setDocument(doc);
        }

        //Make decimals look nice
        if(cont != null && cont.equals(MasterPanel.Controller.PRESSURE))
        {
            JSpinner.NumberEditor mpEditor = (JSpinner.NumberEditor) spinner.getEditor();
            DecimalFormat format = mpEditor.getFormat();
            format.setMinimumFractionDigits(3);
        }

        //Make text field look ok
        final JTextField tf = ((JSpinner.DefaultEditor) spinner.getEditor()).getTextField();
        tf.getCaret().setBlinkRate(0);
        tf.setText("" + spm.getValue());
        tf.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED , Color.BLACK, Color.gray));

        if(includeFocus)
        {
            //Make sure valid number is entered through various methods of losing focus
            tf.addFocusListener(new FocusListener()
            {
                @Override
                public void focusGained(FocusEvent e)
                {

                }

                @Override
                public void focusLost(FocusEvent e)
                {
                    double num = Double.parseDouble(tf.getText());

                    if (num < Double.parseDouble(spm.getMinimum().toString()))
                    {
                        num = Double.parseDouble(spm.getMinimum().toString());
                        tf.setText("" + num);
                    }

                    if (num > Double.parseDouble(spm.getMaximum().toString()))
                    {
                        num = Double.parseDouble(spm.getMaximum().toString());
                        tf.setText("" + num);
                    }

                    if (spinner.getModel().getValue() instanceof Integer)
                    {
                        spm.setValue((int) num);
                    }
                    else
                    {
                        spm.setValue(num);
                    }
                }
            });

            tf.addKeyListener(new KeyListener()
            {
                @Override
                public void keyTyped(KeyEvent e)
                {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    {
                        double num = Double.parseDouble(tf.getText());

                        if (num < Double.parseDouble(spm.getMinimum().toString()))
                        {
                            num = Double.parseDouble(spm.getMinimum().toString());
                            tf.setText("" + num);
                        }

                        if (num > Double.parseDouble(spm.getMaximum().toString()))
                        {
                            num = Double.parseDouble(spm.getMaximum().toString());
                            tf.setText("" + num);
                        }

                        if (spinner.getModel().getValue() instanceof Integer)
                        {
                            spm.setValue((int) num);
                        }
                        else
                        {
                            spm.setValue(num);
                        }
                    }
                }

                @Override
                public void keyPressed(KeyEvent e)
                {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    {
                        double num = Double.parseDouble(tf.getText());

                        if (num < Double.parseDouble(spm.getMinimum().toString()))
                        {
                            num = Double.parseDouble(spm.getMinimum().toString());
                            tf.setText("" + num);
                        }

                        if (num > Double.parseDouble(spm.getMaximum().toString()))
                        {
                            num = Double.parseDouble(spm.getMaximum().toString());
                            tf.setText("" + num);
                        }

                        if (spinner.getModel().getValue() instanceof Integer)
                        {
                            spm.setValue((int) num);
                        }
                        else
                        {
                            spm.setValue(num);
                        }
                    }
                }

                @Override
                public void keyReleased(KeyEvent e)
                {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    {
                        double num = Double.parseDouble(tf.getText());

                        if (num < Double.parseDouble(spm.getMinimum().toString()))
                        {
                            num = Double.parseDouble(spm.getMinimum().toString());
                            tf.setText("" + num);
                        }

                        if (num > Double.parseDouble(spm.getMaximum().toString()))
                        {
                            num = Double.parseDouble(spm.getMaximum().toString());
                            tf.setText("" + num);
                        }

                        if (spinner.getModel().getValue() instanceof Integer)
                        {
                            spm.setValue((int) num);
                        }
                        else
                        {
                            spm.setValue(num);
                        }
                    }
                }
            });
        }
    }


    public static void setupTimeSpinner(final JSpinner spinner, final SpinnerNumberModel spm, final timeType time, final ComponentHandler componentHandler, final boolean isMasterMode, final MasterPanel masterPanel)
    {
        JSpinnerMaker.makeDigitsOnlySpinnerUsingDocumentFilter(spinner, spm, null, false);
        ((JSpinner.DefaultEditor) spinner.getEditor()).getTextField().setEditable(false);

        int milli = Integer.parseInt(spinner.getModel().getValue().toString());
        setText(spinner, "5.00",false);
        spinner.getModel().setValue(milli);

        //Time spinners display minutes and seconds while their model has milliseconds ;)
        Action nextAction = new AbstractAction()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                int milli = Integer.parseInt((spinner.getModel().getValue()).toString());

                //make sure spinner stays within bounds
                if(milli <= Integer.parseInt(spm.getMinimum().toString()))
                {
                    milli = Integer.parseInt(spinner.getModel().getValue().toString());
                }
                else if(milli >= Integer.parseInt(spm.getMaximum().toString()))
                {
                    milli = Integer.parseInt(spinner.getModel().getPreviousValue().toString());
                }

                setText(spinner, "" + milli,false);
            }
        };

        Action prevAction = new AbstractAction()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                int milli = Integer.parseInt((spinner.getModel().getValue()).toString());

                //Make sure it stays within bounds
                if(milli <= Integer.parseInt(spm.getMinimum().toString()))
                {
                    milli = Integer.parseInt(spinner.getModel().getNextValue().toString());
                }
                else if(milli >= Integer.parseInt(spm.getMaximum().toString()))
                {
                    milli = Integer.parseInt(spinner.getModel().getValue().toString());
                }

                setText(spinner, "" + milli,false);
            }
        };

        //Set button listeners
        for (Component child : spinner.getComponents())
        {
            if ("Spinner.nextButton".equals(child.getName()))
            {
                ((JButton) child).addActionListener(nextAction);
            }
            if ("Spinner.previousButton".equals(child.getName()))
            {
                ((JButton) child).addActionListener(prevAction);
            }
        }

        spinner.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                int milli = Integer.parseInt(spinner.getModel().getValue().toString());

                //calculate time in minutes and seconds
                int minute = milli / 60000;
                int seconds = (milli % 60000) / 1000;
                String sec = (seconds < 10) ? "0" + seconds : "" + seconds;
                setText(spinner,minute + "." + sec,false);
                spinner.getModel().setValue(milli);

                //Set values in component handler
                switch (time)
                {
                    case delay:
                        componentHandler.setSendDelay(milli);

                        if(isMasterMode)
                        {
                            if (!masterPanel.isMasterOn())
                            {
                                masterPanel.updateSelection();
                                masterPanel.turnMasterOn();
                            }
                        }
                        break;
                    case on:
                        componentHandler.setAutoOnTime(milli);

                        if(isMasterMode)
                        {
                            if (!masterPanel.isMasterOn())
                            {
                                masterPanel.updateSelection();
                                masterPanel.turnMasterOn();
                            }
                        }

                        componentHandler.start();
                        break;
                    case off:
                        componentHandler.setAutoOffTime(milli);

                        if(isMasterMode)
                        {
                            if (!masterPanel.isMasterOn())
                            {
                                masterPanel.updateSelection();
                                masterPanel.turnMasterOn();
                            }
                        }

                        componentHandler.start();
                        break;
                }
            }
        });
    }

    //Set text of JSpinner
    public static void setText(JSpinner spinner, String text, boolean forceFire)
    {
        JTextField tf = ((JSpinner.DefaultEditor) spinner.getEditor()).getTextField();
        tf.setText(text);

        if(forceFire)
        {
            //If force fire it forces an update with the listener
            if(text.contains("."))
            {
                spinner.getModel().setValue(Double.parseDouble(text));
            }
            else
            {
                spinner.getModel().setValue(Integer.parseInt(text));
            }

            for (ChangeListener changeListener : spinner.getChangeListeners())
            {
                changeListener.stateChanged(new ChangeEvent(spinner));
            }
        }
    }

    //Get text of spinner
    public static String getText(JSpinner spinner)
    {
        return spinner.getValue().toString();
    }

}


