package DeploymentLab.Export.SoundBoard;

import DeploymentLab.CentralCatalogue;
import DeploymentLab.Model.DLComponent;
import DeploymentLab.Model.DLObject;
import DeploymentLab.SelectModel;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;

public class Channel extends JPanel implements selectionChangeDispatcher
{
    JButton selectbtn;
    JButton viewSelectbtn;
    JSpinner contSpinner;
    OnOffButton OnOffbtn;
    JSpinner stdSpinner;
    JSpinner trendSpinner;
    JSpinner sendSpinner;
    JSpinner threshSpinner;
    JCheckBox sendCheckBox;
    JSpinner onSpinner;
    JSpinner offSpinner;
    JCheckBox autoOnOffCheckBox;

    boolean autoOnOff = false;

    JLabel meanlbl;
    JLabel stdevlbl;
    JLabel trendlbl;
    JLabel timelbl;
    JLabel threshlbl;
    JLabel onlbl;
    JLabel offlbl;

    JComboBox modeSelection;

    Color backColor;
    Color valColor;
    Color btnColor;

    SelectModel selectModel;

    MasterPanel.Controller controller;
    ComponentHandler componentHandler;

    ArrayList<selectionChangeListener> listeners;

    ArrayList<DLComponent> storedObjects;

    ArrayList<DLComponent> addedObjects;
    ArrayList<DLComponent> removedObjects;

    public Channel(String channelName, final MasterPanel.Controller cont, MasterPanel masterPanel, Color backColor, Color valColor, Color btnColor, final boolean isFirst)
    {
        super(new MigLayout("fillx", "[right]rel[grow,fill]", "[]10[]")); //Set panel type

        //Initialize variables
        selectModel = CentralCatalogue.getSelectModel();
        componentHandler = new ComponentHandler(cont, channelName);

        controller = cont;
        listeners = new ArrayList<>();

        storedObjects = new ArrayList<>();

        addedObjects = new ArrayList<>();
        removedObjects = new ArrayList<>();

        this.backColor = backColor;
        this.valColor = valColor;
        this.btnColor = btnColor;

        String[] modes = {"Value", "Send", "Auto"};

        modeSelection = new JComboBox<>(modes);

        //Get Spinners
        List<Component> spinners = JSpinnerMaker.generateSpinners(controller,componentHandler);

        contSpinner = (JSpinner)spinners.get(0);
        stdSpinner = (JSpinner)spinners.get(1);
        trendSpinner = (JSpinner)spinners.get(2);

        sendSpinner = (JSpinner)spinners.get(3);
        sendSpinner.setPreferredSize(new Dimension(30,20));

        threshSpinner = (JSpinner)spinners.get(4);
        sendSpinner.setPreferredSize(new Dimension(30,20));

        onSpinner = (JSpinner)spinners.get(5);
        onSpinner.setPreferredSize(new Dimension(30,20));
        onSpinner.setEnabled(false);

        offSpinner = (JSpinner)spinners.get(6);
        offSpinner.setPreferredSize(new Dimension(30,20));
        offSpinner.setEnabled(false);

        sendCheckBox = new JCheckBox("Send");
        sendCheckBox.setForeground(Color.WHITE);
        sendCheckBox.setHorizontalAlignment(SwingConstants.CENTER);
        sendCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                componentHandler.setAllowSend(sendCheckBox.isSelected());
            }
        });

        autoOnOffCheckBox = new JCheckBox("Auto");
        autoOnOffCheckBox.setForeground(Color.WHITE);
        autoOnOffCheckBox.setHorizontalAlignment(SwingConstants.CENTER);

        autoOnOffCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if(autoOnOffCheckBox.isSelected())
                {
                    autoOnOff = true;
                    onSpinner.setEnabled(true);
                    offSpinner.setEnabled(true);
                    componentHandler.setAuto(true);

                    componentHandler.start();
                }
                else
                {
                    autoOnOff = false;
                    onSpinner.setEnabled(false);
                    offSpinner.setEnabled(false);
                    componentHandler.setAuto(false);

                    componentHandler.start();
                }
            }
        });

        //Define all labels
        meanlbl = new JLabel("Mean");
        meanlbl.setForeground(Color.WHITE);
        meanlbl.setPreferredSize(new Dimension(50,10));
        meanlbl.setHorizontalAlignment(SwingConstants.RIGHT);

        stdevlbl = new JLabel("Std Dev");
        stdevlbl.setForeground(Color.WHITE);
        stdevlbl.setPreferredSize(new Dimension(50,10));
        stdevlbl.setHorizontalAlignment(SwingConstants.RIGHT);

        trendlbl = new JLabel("Trend");
        trendlbl.setForeground(Color.WHITE);
        trendlbl.setPreferredSize(new Dimension(50,10));
        trendlbl.setHorizontalAlignment(SwingConstants.RIGHT);

        timelbl = new JLabel("Time");
        timelbl.setForeground(Color.WHITE);
        timelbl.setPreferredSize(new Dimension(50,10));
        timelbl.setHorizontalAlignment(SwingConstants.RIGHT);

        threshlbl = new JLabel("Thresh");
        threshlbl.setForeground(Color.WHITE);
        threshlbl.setPreferredSize(new Dimension(50,10));
        threshlbl.setHorizontalAlignment(SwingConstants.RIGHT);

        onlbl = new JLabel("On");
        onlbl.setForeground(Color.WHITE);
        onlbl.setPreferredSize(new Dimension(50,10));
        onlbl.setHorizontalAlignment(SwingConstants.RIGHT);

        offlbl = new JLabel("Off");
        offlbl.setForeground(Color.WHITE);
        offlbl.setPreferredSize(new Dimension(50,10));
        offlbl.setHorizontalAlignment(SwingConstants.RIGHT);

        //Make select button for channel
        selectbtn = new JButton("Select");

        selectbtn.setBackground(this.btnColor);
        selectbtn.setForeground(Color.WHITE);
        selectbtn.setPreferredSize(new Dimension(50,10));

        selectbtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                select();
            }
        });

        //Make the view button
        viewSelectbtn = new JButton("View");

        viewSelectbtn.setBackground(this.btnColor);
        viewSelectbtn.setForeground(Color.WHITE);
        viewSelectbtn.setPreferredSize(new Dimension(50,10));

        viewSelectbtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                viewSelection();
            }
        });

        //Create on off button and define what happens when on and off
        OnOffbtn = new OnOffButton(this.backColor);
        OnOffbtn.addStateChangeListener(new StateChangeListener() {
            @Override
            public void disablePanel(StateChangeEvent event)
            {
                //Turn everything off, store and clear list
                componentHandler.stop();
                selectbtn.setEnabled(false);
                viewSelectbtn.setEnabled(false);
                JSpinnerMaker.setText(contSpinner, "",false);
                contSpinner.setEnabled(false);
                JSpinnerMaker.setText(stdSpinner, "",false);
                stdSpinner.setEnabled(false);
                JSpinnerMaker.setText(trendSpinner, "",false);
                trendSpinner.setEnabled(false);
                modeSelection.setEnabled(false);
                sendSpinner.setEnabled(false);
                componentHandler.setAllowSend(false);
                threshSpinner.setEnabled(false);

                sendCheckBox.setSelected(false);
                componentHandler.setAllowSend(sendCheckBox.isSelected());

                sendCheckBox.setEnabled(false);
                autoOnOffCheckBox.setEnabled(false);
                onSpinner.setEnabled(false);
                offSpinner.setEnabled(false);

                storedObjects.clear();
                storedObjects.addAll(componentHandler.getComponents());

                //Should clear entire list and force an event fire for REST to hear
                setComponents(new ArrayList<DLComponent>());
            }

            @Override
            public void activatePanel(StateChangeEvent event)
            {
                //Turn on everything
                selectbtn.setEnabled(true);
                viewSelectbtn.setEnabled(true);
                JSpinnerMaker.setText(contSpinner, "" + componentHandler.getMeanValue(),false);
                contSpinner.setEnabled(true);
                JSpinnerMaker.setText(stdSpinner, "" + componentHandler.getStandardDeviation(),false);
                stdSpinner.setEnabled(true);
                JSpinnerMaker.setText(trendSpinner, "" + componentHandler.getTrend(),false);
                trendSpinner.setEnabled(true);
                modeSelection.setEnabled(true);
                sendSpinner.setEnabled(true);
                threshSpinner.setEnabled(true);
                sendCheckBox.setEnabled(true);
                componentHandler.setAllowSend(sendCheckBox.isSelected());
                autoOnOffCheckBox.setEnabled(true);

                if(autoOnOff)
                {
                    onSpinner.setEnabled(true);
                    offSpinner.setEnabled(true);
                }

                //Add stored objects
                setComponents(storedObjects);
                storedObjects.clear();

                componentHandler.start();
            }
        });

        //Makes sure everything starts off
        OnOffbtn.forceFire();

        //ALl channels must listen to its master when it demands
        masterPanel.addStateChangedListener(new stateChangedListener() {
            @Override
            public void bowToMaster(stateChangedEvent event)
            {
                OnOffbtn.setState(OnOffButton.State.OFF);
            }
        });


        //To include labels or not too...
        if(isFirst)
        {
            super.setPreferredSize(new Dimension(SoundBoardConstants.channelWidth + 55,SoundBoardConstants.channelHeight));
            super.add(OnOffbtn, "align center,gapx 50, width 65::, height 30::, wrap");
            super.add(selectbtn, "align center,gapx 50, width 65::, height 30::, wrap");
            super.add(viewSelectbtn, "align center,gapx 50, width 65::, height 30::, wrap");
            super.add(sendCheckBox, "align center,gapx 50, width 65::, height 20::, wrap");
            super.add(modeSelection, "align center, gapx 50,width 65::, height 20::, wrap");
            super.add(meanlbl, "split 2,width 50::, height 20:: ");
            super.add(contSpinner, "width 65::, height 20::, wrap");
            super.add(stdevlbl, "split 2, width 50::, height 20::");
            super.add(stdSpinner, "width 65::, height 20::, wrap");
            super.add(trendlbl, "split 2, width 50::, height 20::");
            super.add(trendSpinner, "width 65::, height 20::, wrap");
            super.setPreferredSize(new Dimension(SoundBoardConstants.channelWidth + 55,SoundBoardConstants.channelHeight));
        }
        else
        {
            super.setPreferredSize(new Dimension(SoundBoardConstants.channelWidth,SoundBoardConstants.channelHeight));
            super.add(OnOffbtn, "align center, width 65::, height 30::, wrap");
            super.add(selectbtn, "align center, width 65::, height 30::, wrap");
            super.add(viewSelectbtn, "align center, width 65::, height 30::, wrap");
            super.add(sendCheckBox, "align center, width 65::, height 20::, wrap");
            super.add(modeSelection, "align center, width 65::, height 20::, wrap");
            super.add(contSpinner, "align center, width 65::, height 20::, wrap");
            super.add(stdSpinner, "align center, width 65::, height 20::, wrap");
            super.add(trendSpinner, "align center, width 65::, height 20::, wrap");
            super.setPreferredSize(new Dimension(SoundBoardConstants.channelWidth ,SoundBoardConstants.channelHeight));
        }

        modeSelection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                modeSelection.setPreferredSize(new Dimension(65,20));
                updateMenu(isFirst);
            }
        });

        super.setBackground(this.backColor);
    }

    public void updateMenu(boolean isFirst)
    {
        String mode = (String)modeSelection.getSelectedItem();

        //Changes what is displayed on Sound Board given what mode is selected
        if(mode.equals("Value"))
        {
            if(isFirst)
            {
                super.remove(timelbl);
                super.remove(sendSpinner);
                super.remove(threshlbl);
                super.remove(threshSpinner);

                super.remove(autoOnOffCheckBox);
                super.remove(onlbl);
                super.remove(onSpinner);
                super.remove(offlbl);
                super.remove(offSpinner);

                super.add(meanlbl, "split 2,width 50::, height 20::");
                super.add(contSpinner, "width 65::, height 20::, wrap");
                super.add(stdevlbl, "split 2, width 50::, height 20::");
                super.add(stdSpinner, "width 65::, height 20::, wrap");
                super.add(trendlbl, "split 2, width 50::, height 20::");
                super.add(trendSpinner, "width 65::, height 20::, wrap");

                super.repaint();
            }
            else
            {
                super.remove(sendSpinner);
                super.remove(threshSpinner);

                super.remove(autoOnOffCheckBox);
                super.remove(onSpinner);
                super.remove(offSpinner);

                super.add(contSpinner, "align center, width 65::, height 20::, wrap");
                super.add(stdSpinner, "align center, width 65::, height 20::, wrap");
                super.add(trendSpinner, "align center, width 65::, height 20::, wrap");

                super.repaint();
            }
        }
        else if(mode.equals("Send"))
        {
            if(isFirst)
            {
                super.remove(meanlbl);
                super.remove(contSpinner);
                super.remove(stdevlbl);
                super.remove(stdSpinner);
                super.remove(trendlbl);
                super.remove(trendSpinner);

                super.remove(autoOnOffCheckBox);
                super.remove(onlbl);
                super.remove(onSpinner);
                super.remove(offlbl);
                super.remove(offSpinner);

                super.add(timelbl, "split 2,width 50::, height 20:: ");
                super.add(sendSpinner, "width 65::, height 20::, wrap");
                super.add(threshlbl, "split 2, width 50::, height 20::");
                super.add(threshSpinner, "width 65::, height 20::, wrap");

                super.repaint();
            }
            else
            {
                super.remove(contSpinner);
                super.remove(stdSpinner);
                super.remove(trendSpinner);

                super.remove(autoOnOffCheckBox);
                super.remove(onSpinner);
                super.remove(offSpinner);

                super.add(sendSpinner, "align center, width 65::, height 20::, wrap");
                super.add(threshSpinner, "align center, width 65::, height 20::, wrap");

                super.repaint();
            }
        }
        else if(mode.equals("Auto"))
        {
            if(isFirst)
            {
                super.remove(meanlbl);
                super.remove(contSpinner);
                super.remove(stdevlbl);
                super.remove(stdSpinner);
                super.remove(trendlbl);
                super.remove(trendSpinner);

                super.remove(timelbl);
                super.remove(sendSpinner);
                super.remove(threshlbl);
                super.remove(threshSpinner);

                super.add(autoOnOffCheckBox, "align center,gapx 50, width 65::, height 20::, wrap");
                super.add(onlbl, "split 2, width 50::, height 20::");
                super.add(onSpinner, "width 65::, height 20::, wrap");
                super.add(offlbl, "split 2, width 50::, height 20::");
                super.add(offSpinner, "width 65::, height 20::, wrap");

                super.repaint();
            }
            else
            {
                super.remove(contSpinner);
                super.remove(stdSpinner);
                super.remove(trendSpinner);

                super.remove(sendSpinner);
                super.remove(threshSpinner);

                super.add(autoOnOffCheckBox, "align center, width 65::, height 20::, wrap");
                super.add(onSpinner, "align center, width 65::, height 20::, wrap");
                super.add(offSpinner, "align center, width 65::, height 20::, wrap");

                super.repaint();
            }
        }
    }

    public List<String> settings()
    {
        List<String> settings = new ArrayList<>();

        //Add all channel settings to a list
        settings.add(componentHandler.getChannelName());
        settings.add("Channel On:" + isOn());
        settings.add("Mean:" + contSpinner.getModel().getValue());
        settings.add("Standard Deviation:" + stdSpinner.getModel().getValue());
        settings.add("Trend:" + trendSpinner.getModel().getValue());
        settings.add("Send Delay:" + sendSpinner.getModel().getValue());
        settings.add("Threshold:" + threshSpinner.getModel().getValue());
        settings.add("Auto:" + autoOnOffCheckBox.isSelected());
        settings.add("Auto On Time:" + onSpinner.getModel().getValue());
        settings.add("Auto Off Time:" + offSpinner.getModel().getValue());
        settings.add("Ids");

        if(!isOn())
        {
            //if channel is off, put objects in component handler
            setComponents(storedObjects);
        }

        //Get all component ids and add them to save file list
        List<String> ids = componentHandler.getIds();

        for(String id : ids)
        {
            settings.add(id);
        }

        if(!isOn())
        {
            //If channel is off make sure to undo all changes made
            setComponents(new ArrayList<DLComponent>());
        }

        return settings;
    }

    public void loadSettings(List<String> settings)
    {
        //Make sure channel isn't sending any data
        sendCheckBox.setSelected(false);

        for(ActionListener actionListener : sendCheckBox.getActionListeners())
        {
            //Essentially causes the listener to fire immediately instead of going into a queue
            actionListener.actionPerformed(new ActionEvent(sendCheckBox,1,"force"));
        }

        for(StateChangeListener stateChangeListener : OnOffbtn.getStateChangeListener())
        {
            //Turn panel off and fire!
            stateChangeListener.disablePanel(new StateChangeEvent(OnOffbtn));
            OnOffbtn.setWithoutEvent(OnOffButton.State.OFF);
        }

        //Set all spinners to proper values
        JSpinnerMaker.setText(contSpinner,settings.get(2).split(":")[1],true);
        JSpinnerMaker.setText(stdSpinner,settings.get(3).split(":")[1],true);
        JSpinnerMaker.setText(trendSpinner,settings.get(4).split(":")[1],true);
        JSpinnerMaker.setText(sendSpinner,settings.get(5).split(":")[1],true);
        JSpinnerMaker.setText(threshSpinner,settings.get(6).split(":")[1],true);

        //Set auto
        autoOnOffCheckBox.setSelected(Boolean.parseBoolean(settings.get(7).split(":")[1]));

        for(ActionListener actionListener : autoOnOffCheckBox.getActionListeners())
        {
            //fire event
            actionListener.actionPerformed(new ActionEvent(autoOnOffCheckBox,1,"force"));
        }

        //Enable/disable auto time spinners depending on auto checkbox
        if(autoOnOffCheckBox.isSelected())
        {
            onSpinner.setEnabled(true);
            offSpinner.setEnabled(true);
        }
        else
        {
            onSpinner.setEnabled(false);
            offSpinner.setEnabled(false);
        }

        //Set value
        JSpinnerMaker.setText(onSpinner,settings.get(8).split(":")[1],true);
        JSpinnerMaker.setText(offSpinner,settings.get(9).split(":")[1],true);

        //Clear old data
        storedObjects.clear();

        setComponents(new ArrayList<DLComponent>());

        //Add new ids
        List<String> ids = new ArrayList<>();
        for(int i = 11; i < settings.size(); i++)
        {
            ids.add(settings.get(i));
        }

        if(ids.size() != 0)
        {
            componentHandler.addOnjectsById(ids);
        }

        //Turn everything off, forces all settings to be updated everywhere properly
        for(StateChangeListener stateChangeListener : OnOffbtn.getStateChangeListener())
        {
            stateChangeListener.disablePanel(new StateChangeEvent(OnOffbtn));
            OnOffbtn.setWithoutEvent(OnOffButton.State.OFF);
        }

        if(Boolean.parseBoolean(settings.get(1).split(":")[1]))
        {
            //if its supposed to be off, turn it back on
            for(StateChangeListener stateChangeListener : OnOffbtn.getStateChangeListener())
            {
                stateChangeListener.activatePanel(new StateChangeEvent(OnOffbtn));
                OnOffbtn.setWithoutEvent(OnOffButton.State.ON);
            }
        }
    }

    public boolean isOn()
    {
        return OnOffbtn.getState() == OnOffButton.State.ON;
    }

    public MasterPanel.Controller getController()
    {
        return controller;
    }

    //Gets all nodes of a certain type in a region
    private void select()
    {
        switch (controller)
        {
            case TEMPERATURE:
                setComponents(findAllByRoleInSelection("temperaturecontrol"));
                break;
            case PRESSURE:
                setComponents(findAllByRoleInSelection("pressurecontrol"));
                break;
            case SUPPLY:
                //Skip down to return
            case RETURN:
                List<DLComponent> newObjects = new ArrayList<>();
                List<DLComponent> allObjects = selectModel.getSelectedObjects();

                for(DLComponent component : allObjects)
                {
                    ArrayList<DLObject> objects = component.getManagedObjects();

                    for(DLObject object : objects)
                    {
                        if(object.getType().equals("crac"))
                        {
                            newObjects.add(component);
                        }
                    }
                }
                setComponents(newObjects);
                break;
        }
    }

    //view objects in channel
    private void viewSelection()
    {
        selectModel.setSelection(componentHandler.getComponents());
    }

    private List<DLComponent> findAllByRoleInSelection(String role)
    {
        List<DLComponent> allInSelection = selectModel.getExpandedSelection();
        List<DLComponent> objs = new ArrayList<>();

        //does a search by role
        for (DLComponent comp : allInSelection)
        {
            if(comp.listRoles().contains(role))
            {
                objs.add(comp);
            }
        }

        return objs;
    }

    public void addListeners(List<Channel> channels)
    {
        for(final Channel channel : channels)
        {
            //Make sure each Channel listens to each other
            channel.addselectionChangeListener(new selectionChangeListener() {
                @Override
                public void objectsClaimed(selectionChangeEvent event)
                {
                    //Check to see if other channel grabs one of its nodes
                    List<DLComponent> objs = channel.getAddedObjs();

                    for (DLComponent obj : objs)
                    {
                        if(componentHandler.getComponents().contains(obj))
                        {
                            componentHandler.removeObject(obj);
                        }
                    }

                    componentHandler.start();
                }

                @Override
                public void objectsReleased(selectionChangeEvent event)
                {
                    //DO NOTHING ONLY USED FOR THE REST
                }
            });
        }
    }

    public ComponentHandler getComponentHandler()
    {
        return componentHandler;
    }

    public OnOffButton getOnOffbtn()
    {
        return OnOffbtn;
    }

    @Override
    public void addselectionChangeListener(selectionChangeListener listener)
    {
        listeners.add(listener);
    }

    @Override
    public List<DLComponent> getObjs()
    {
        return componentHandler.getComponents();
    }

    @Override
    public List<DLComponent> getAddedObjs()
    {
        return addedObjects;
    }

    @Override
    public List<DLComponent> getRemovedObjs()
    {
        return removedObjects;
    }

    @Override
    public void setComponents(List<DLComponent> componentIds)
    {
        LinkedHashSet<DLComponent> incomingSelection = new LinkedHashSet<>();
        incomingSelection.addAll(componentIds);

        LinkedHashSet<DLComponent> currentlySelectedComponents = new LinkedHashSet<>(componentHandler.getComponents());

        boolean changes = false;

        addedObjects.clear();
        removedObjects.clear();

        //Add only new components to list
        for (DLComponent i: incomingSelection)
        {
            if (!currentlySelectedComponents.contains(i) && i.getDrawing().equals(selectModel.getActiveDrawing()) )
            {
                componentHandler.addObject(i);
                addedObjects.add(i);
                changes = true;
            }
        }

        //Remove objects that aren't in new selection
        for (DLComponent i: currentlySelectedComponents)
        {
            if (!incomingSelection.contains(i))
            {
                componentHandler.removeObject(i);
                removedObjects.add(i);
                changes = true;
            }
        }

        if(changes)
        {
            //All channels should advertise any changes made
            dispatchEvent();
        }

        if(!componentHandler.isRunning() && OnOffbtn.getState().equals(OnOffButton.State.ON))
        {
            componentHandler.start();
        }
    }

    private void dispatchEvent()
    {
        final selectionChangeEvent event = new selectionChangeEvent(this);

        for (selectionChangeListener l : listeners)
        {
            dispatchRunnableOnEventQueue(l, event);
        }
    }


    private void dispatchRunnableOnEventQueue(final selectionChangeListener listener, final selectionChangeEvent event)
    {
        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                if(addedObjects.size() != 0)
                {
                    listener.objectsClaimed(event);
                }

                if(removedObjects.size() != 0)
                {
                    listener.objectsReleased(event);
                }
            }
        });
    }
}

interface selectionChangeDispatcher
{
    public void addselectionChangeListener(selectionChangeListener listener);
    public void setComponents(List<DLComponent> componentIds);
    public List<DLComponent> getObjs();
    public List<DLComponent> getAddedObjs();
    public List<DLComponent> getRemovedObjs();
}

interface selectionChangeListener extends EventListener
{
    public void objectsClaimed(selectionChangeEvent event);
    public void objectsReleased(selectionChangeEvent event);
}

class selectionChangeEvent extends EventObject
{

    private final selectionChangeDispatcher dispatcher;

    public selectionChangeEvent(selectionChangeDispatcher dispatcher)
    {
        super(dispatcher);
        this.dispatcher = dispatcher;
    }

    // type safe way to get source (as opposed to getSource of EventObject
    public selectionChangeDispatcher getDispatcher()
    {
        return dispatcher;
    }
}
