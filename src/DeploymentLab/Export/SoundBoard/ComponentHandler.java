package DeploymentLab.Export.SoundBoard;


import DeploymentLab.CentralCatalogue;
import DeploymentLab.Model.*;

import javax.swing.*;
import java.awt.*;
import java.util.*;

import java.util.List;
import java.util.concurrent.*;

public class ComponentHandler implements channelStateChangeDispatcher
{
    private volatile List<DLComponent> components;
    private volatile List<String> ids;

    private volatile MasterPanel.Controller controller;
    public controlRunnable control;
    private Thread controlThread;
    private ArrayList<channelStateChangeListener> listeners;

    private volatile double currentMeanVal;
    private volatile double standardDeviation;
    private volatile double trend;
    private volatile double threshold;

    private boolean auto = false;
    private long autoOnTime = 0;
    private long autoOffTime = 0;
    private long autoStartTime = 0;
    private boolean inAutoOn = false;

    private String channelName;

    private volatile boolean isBWThreadOn = false;
    private boolean allowSend = false;

    private long sendDelay = 0;
    private volatile long startTime = 0;
    private double meanMax = 0;
    private double meanMin = 0;

    private JSpinner spinner;

    private Random rand;

    private ScheduledExecutorService scheduledThreadDelay;
    private ScheduledFuture<?> futureTask;

    private ScheduledExecutorService scheduledThreadAuto;
    private ScheduledFuture<?> futureAutoTask;

    //Class to handle generating and sending data to active control manager
    public ComponentHandler(MasterPanel.Controller cont, String name)
    {
        //Initialize everything
        controller = cont;
        rand = new Random();
        channelName = name;
        scheduledThreadDelay = Executors.newScheduledThreadPool(2);
        scheduledThreadAuto = Executors.newScheduledThreadPool(2);

        components = new ArrayList<>();
        ids =  new ArrayList<>();
        listeners = new ArrayList<>();


        //Create a new Runnable to be executed when componenthandler thread is turned on
        control = new controlRunnable();
        controlThread = new Thread(control);
    }

    private class controlRunnable implements Runnable
    {
        public List<Double> distribution;
        public volatile List<Node> buffer;
        long delayTime;
        public volatile int numDone = 0;
        public volatile boolean updateCycle = false;
        public volatile boolean stayOn = true;

        @Override
        public void run()
        {
            //These will stay constant during the run
            long currentStart = Math.round(System.nanoTime() / 1000000.0); //Convert to milliseconds
            long storedSend = sendDelay;
            double startMean = currentMeanVal;
            distribution = valueDistribution();

            //Create initial queue
            buffer = getFutureSends(sendDelay, true, currentStart,0,currentMeanVal, startMean, 0, distribution);

            //Keep track of everything
            int numNodesComplete = 0;
            int numCycleComplete = 0;

            Node previousNode = null;

            //Get start time and send it out to graph and turn graph on
            startTime = System.nanoTime();
            dispatchEvent(true);

            while(stayOn)
            {
                try
                {
                    long delta = 0;

                    Node currentNode = buffer.get(0);

                    if(previousNode != null)
                    {
                        //Get delta to know wait time
                        delta = currentNode.getTime() - previousNode.getTime();
                    }

                    //Wait for delta
                    Thread.sleep(delta);

                    //Send data to ACM
                    List<String> data = new ArrayList<>();
                    data.add(currentNode.getNodeId());
                    data.add("" + currentNode.getValue());
                    ActiveControlManager.getInstance().send(data);

                    if(currentNode.getType().equals(nodeType.normal))
                    {
                        //Keep track of main nodes
                        numNodesComplete++;
                    }

                    if(numNodesComplete == ids.size())
                    {
                        //The last node should update start time and the mean value if trend is on
                        startTime = System.nanoTime();

                        if(trend != 0)
                        {
                            double val = currentMeanVal + trend;

                            if(val <= meanMax && val >= meanMin)
                            {
                                currentMeanVal += trend;
                                JSpinnerMaker.setText(spinner, "" + currentMeanVal, false);
                            }
                        }

                        numNodesComplete = 0;
                    }

                    //set up for next iteration
                    previousNode = currentNode;
                    buffer.remove(0);

                    if(buffer.size() == 0)
                    {
                        //If buffer is out new cycle -- This synced with cycles thus works
                        numCycleComplete++;
                        buffer.addAll(getFutureSends(sendDelay * (numCycleComplete + 1), false, currentStart , numCycleComplete, currentMeanVal, startMean, 0, distribution));

                       // System.out.println();
                        //for(Node node: buffer)
                        //{
                          //  System.out.println(node.getNodeId() + " type:" + node.getType()+ " value:" + node.getValue() + " mean:" +  node.getMean() + " Original:" + distribution.get(ids.indexOf(node.getNodeId())));
                        //}

                        startTime = System.nanoTime();
                    }
                }
                catch (InterruptedException ex)
                {
                    if(!updateCycle)
                    {
                        break;
                    }
                }
                finally
                {
                    if(updateCycle)
                    {
                        //If live cycle time update
                        double factor = ((double) sendDelay) / ((double)storedSend);

                        for(int i = 0; i < buffer.size(); i++)
                        {
                            long delta = buffer.get(i).getTime() - currentStart - (storedSend * buffer.get(i).getCycle());

                            //scale time for all by factor to reflect new delay time
                            buffer.get(i).setTime(currentStart + (sendDelay * buffer.get(i).getCycle()) + Math.round(delta * factor));
                        }
                        storedSend = sendDelay;

                        //update values with new cycle time
                        delayTime = sendDelay / ids.size();
                        startTime = System.nanoTime() - ((numDone * delayTime) * 1000000);

                        previousNode = null;
                        updateCycle = false;
                    }
                }
            }
        }
    }

    public void setAllowSend(boolean allow)
    {
        //Cannot start sending without allow send being on
        allowSend = allow;

        if(allowSend)
        {
            start();
        }
        else
        {
            stop();
        }
    }

    public boolean getAllowSend()
    {
        return allowSend;
    }

    public String getChannelName()
    {
        return channelName;
    }

    public void setMeanMax(double value)
    {
        meanMax = value;
    }

    public double getMeanMax()
    {
        return meanMax;
    }

    public void setMeanMin(double value)
    {
        meanMin = value;
    }

    public double getMeanMin()
    {
        return meanMin;
    }

    public long getStartTime()
    {
        return  startTime;
    }

    public void setSpinner(JSpinner spinner)
    {
        this.spinner = spinner;
    }

    public void setSendDelay(long delayTime)
    {
        //Update cycle time
        sendDelay = delayTime;

        if(PlaybackChannel.getInstance(null).allowREC() && allowSend && isBWThreadOn)
        {
            //Auto record flag, if in recording
            if (PlaybackChannel.getInstance(null).getMessages().get(PlaybackChannel.getInstance(null).getMessages().size() - 1).contains("user changed cycle time to:"))
            {
                PlaybackChannel.getInstance(null).removeMessage(PlaybackChannel.getInstance(null).getMessages().size() - 1);
            }

            PlaybackChannel.getInstance(null).addMessage(channelName + " user changed cycle time to: " + delayTime);
        }

        if(controlThread != null)
        {
            if(controlThread.isAlive())
            {
                //If control thread is on then schedule a timer to update send delay

                if(futureTask != null)
                {
                    //Cancel old task
                    futureTask.cancel(false);
                }

                //Waits a second for any new input before running
                TimerTask task = new TimerTask() {
                    @Override
                    public void run()
                    {
                        //Update cycle time live
                        control.updateCycle = true;
                        controlThread.interrupt();
                        dispatchEvent();
                    }
                };

                futureTask = scheduledThreadDelay.schedule(task,1000,TimeUnit.MILLISECONDS);
            }
        }
    }

    public long getSendDelay()
    {
        return sendDelay;
    }

    public void setIds(List<String> ids)
    {
        this.ids = ids;
    }

    //Adds a node to the list of nodes and gets sensor id
    public void addObject(DLComponent component)
    {
        //Make sure to stop component handler thread if it is running before adding object
        //Keeps things thread safe
        if(isRunning())
        {
            this.stop();
        }

        //Add object
        components.add(component);

        //Find WSNSensor id of object necessary for Active Control
        if(controller.equals(MasterPanel.Controller.TEMPERATURE) || controller.equals(MasterPanel.Controller.PRESSURE))
        {
            ArrayList<DLObject> objects = component.getManagedObjectsOfType("controlpoint_singlecompare");

            for (DLObject obj : objects)
            {
                switch (controller)
                {
                    case TEMPERATURE:
                        if (obj.getObjectSetting("resource").getValue().equals("temperature"))
                        {
                            Dockingpoint dp = (Dockingpoint) obj.getObjectProperty("sensor");
                            ids.add(dp.getReferrent().getKey());
                        }
                        break;
                    case PRESSURE:
                        if (obj.getObjectSetting("resource").getValue().equals("pressure"))
                        {
                            Dockingpoint dp = (Dockingpoint) obj.getObjectProperty("sensor");
                            ids.add(dp.getReferrent().getKey());
                        }
                        break;
                }
            }
        }
        else if(controller.equals(MasterPanel.Controller.SUPPLY) || controller.equals(MasterPanel.Controller.RETURN))
        {
            ArrayList<DLObject> objects = component.getManagedObjectsOfType("crac");
            for (DLObject obj : objects)
            {
                switch (controller)
                {
                    case SUPPLY:
                        Dockingpoint dp = (Dockingpoint) obj.getObjectProperty("supplyT");
                        ids.add(dp.getReferrent().getKey());
                        break;
                    case RETURN:
                        Dockingpoint dp2 = (Dockingpoint) obj.getObjectProperty("returnT");
                        ids.add(dp2.getReferrent().getKey());
                        break;
                }
            }

        }
    }

    public void addOnjectsById(List<String> idsToAdd)
    {
        //Make sure to stop component handler thread if it is running before adding object
        //Keeps things thread safe
        if(isRunning())
        {
            this.stop();
        }

        List<DLComponent> newObjects = new ArrayList<>();
        ComponentModel componentModel = CentralCatalogue.getOpenProject().getComponentModel();

        //Get all objects of controller type
        switch(controller)
        {
            case TEMPERATURE:
                newObjects.addAll(componentModel.getComponentsInRole("temperaturecontrol"));
                break;
            case PRESSURE:
                newObjects.addAll(componentModel.getComponentsInRole("pressurecontrol"));
                break;
            case SUPPLY:
                //Skip down to RETURN
            case RETURN:
                List<DLComponent> allObjects = componentModel.getComponents();

                //Find all components that have the object type 'crac' guaranteeing that it will have a supplyT and returnT property
                for(DLComponent component : allObjects)
                {
                    ArrayList<DLObject> objects = component.getManagedObjects();

                    for(DLObject object : objects)
                    {
                        if(object.getType().equals("crac"))
                        {
                            newObjects.add(component);
                        }
                    }
                }
                break;
        }

        for(DLComponent component : newObjects)
        {
            if(controller.equals(MasterPanel.Controller.TEMPERATURE) || controller.equals(MasterPanel.Controller.PRESSURE))
            {
                ArrayList<DLObject> objects = component.getManagedObjectsOfType("controlpoint_singlecompare");

                for (DLObject obj : objects)
                {
                    switch (controller)
                    {
                        case TEMPERATURE:
                            if (obj.getObjectSetting("resource").getValue().equals("temperature"))
                            {
                                Dockingpoint dp = (Dockingpoint) obj.getObjectProperty("sensor");

                                if(idsToAdd.contains(dp.getReferrent().getKey()))
                                {
                                    //if component is in id list then add it
                                    ids.add(dp.getReferrent().getKey());
                                    components.add(component);
                                }
                            }
                            break;
                        case PRESSURE:
                            if (obj.getObjectSetting("resource").getValue().equals("pressure"))
                            {
                                Dockingpoint dp = (Dockingpoint) obj.getObjectProperty("sensor");
                                if(idsToAdd.contains(dp.getReferrent().getKey()))
                                {
                                    //if component is in id list then add it
                                    ids.add(dp.getReferrent().getKey());
                                    components.add(component);
                                }
                            }
                            break;
                    }
                }
            }
            else if(controller.equals(MasterPanel.Controller.SUPPLY) || controller.equals(MasterPanel.Controller.RETURN))
            {
                ArrayList<DLObject> objects = component.getManagedObjectsOfType("crac");
                for (DLObject obj : objects)
                {
                    switch (controller)
                    {
                        case SUPPLY:
                            Dockingpoint dp = (Dockingpoint) obj.getObjectProperty("supplyT");
                            if(idsToAdd.contains(dp.getReferrent().getKey()))
                            {
                                //if component is in id list then add it
                                ids.add(dp.getReferrent().getKey());
                                components.add(component);
                            }
                            break;
                        case RETURN:
                            Dockingpoint dp2 = (Dockingpoint) obj.getObjectProperty("returnT");
                            if(idsToAdd.contains(dp2.getReferrent().getKey()))
                            {
                                //if component is in id list then add it
                                ids.add(dp2.getReferrent().getKey());
                                components.add(component);
                            }
                            break;
                    }
                }

            }
        }

    }

    //Removes Object and sensor id from lists
    public void removeObject(DLComponent component)
    {
        //First ensure thread is closed before changing list
        if(isRunning())
        {
            this.stop();
        }

        //Remove component
        components.remove(component);

        //Find WSNSensor id to remove
        ArrayList<DLObject> objects = component.getManagedObjectsOfType("controlpoint_singlecompare");

        for (DLObject obj : objects)
        {
            switch(controller)
            {
                case TEMPERATURE:
                    if (obj.getObjectSetting("resource").getValue().equals("temperature"))
                    {
                        Dockingpoint dp = (Dockingpoint)obj.getObjectProperty("sensor");
                        ids.remove(dp.getReferrent().getKey());
                    }
                    break;
                case PRESSURE:
                    if (obj.getObjectSetting("resource").getValue().equals("pressure"))
                    {
                        Dockingpoint dp = (Dockingpoint)obj.getObjectProperty("sensor");
                        ids.remove(dp.getReferrent().getKey());
                    }
                    break;
            }
        }
    }

    public List<DLComponent> getComponents()
    {
        return components;
    }

    public List<String> getIds()
    {
        return ids;
    }

    //Functions to set mean, standard deviation and trend, Turns off the the component handler to keep things thread safe
    public void setMeanValue(double value)
    {
        if(isRunning())
        {
            setState(false);
        }

        currentMeanVal = value;

        //Auto record change
        if(PlaybackChannel.getInstance(null).allowREC() && allowSend && isBWThreadOn)
        {
            if (PlaybackChannel.getInstance(null).getMessages().get(PlaybackChannel.getInstance(null).getMessages().size() - 1).contains("user changed mean value to:"))
            {
                PlaybackChannel.getInstance(null).removeMessage(PlaybackChannel.getInstance(null).getMessages().size() - 1);
            }

            PlaybackChannel.getInstance(null).addMessage(channelName + " user changed mean value to: " + currentMeanVal);
        }
    }

    public double getThreshold()
    {
        return threshold;
    }

    public void setStandardDeviation(double value)
    {
        if(isRunning())
        {
            setState(false);
        }

        standardDeviation = value;

        //Auto record change
        if(PlaybackChannel.getInstance(null).allowREC() && allowSend && isBWThreadOn)
        {
            if (PlaybackChannel.getInstance(null).getMessages().get(PlaybackChannel.getInstance(null).getMessages().size() - 1).contains("user changed standard deviation to:"))
            {
                PlaybackChannel.getInstance(null).removeMessage(PlaybackChannel.getInstance(null).getMessages().size() - 1);
            }

            PlaybackChannel.getInstance(null).addMessage(channelName + " user changed standard deviation to: " + standardDeviation);
        }
    }

    public void setTrend(double value)
    {
        if(isRunning())
        {
            setState(false);
        }

        trend = value;

        //Auto record change
        if(PlaybackChannel.getInstance(null).allowREC() && allowSend && isBWThreadOn)
        {
            if (PlaybackChannel.getInstance(null).getMessages().get(PlaybackChannel.getInstance(null).getMessages().size() - 1).contains("user changed trend to:"))
            {
                PlaybackChannel.getInstance(null).removeMessage(PlaybackChannel.getInstance(null).getMessages().size() - 1);
            }

            PlaybackChannel.getInstance(null).addMessage(channelName + " user changed trend to: " + trend);
        }
    }

    public void setThreshold(double value)
    {
        if(isRunning())
        {
            setState(false);
        }

        threshold = value;

        //Auto record change
        if(PlaybackChannel.getInstance(null).allowREC() && allowSend && isBWThreadOn)
        {
            if (PlaybackChannel.getInstance(null).getMessages().get(PlaybackChannel.getInstance(null).getMessages().size() - 1).contains("user changed threshold to:"))
            {
                PlaybackChannel.getInstance(null).removeMessage(PlaybackChannel.getInstance(null).getMessages().size() - 1);
            }

            PlaybackChannel.getInstance(null).addMessage(channelName + " user changed threshold to: " + threshold);
        }
    }

    public void setAuto(boolean turnOn)
    {
        if(isRunning())
        {
            setState(false);
        }

        auto = turnOn;

        //Auto record change
        if(PlaybackChannel.getInstance(null).allowREC() && allowSend && isBWThreadOn)
        {
            if (PlaybackChannel.getInstance(null).getMessages().get(PlaybackChannel.getInstance(null).getMessages().size() - 1).contains("user changed auto mode to:"))
            {
                PlaybackChannel.getInstance(null).removeMessage(PlaybackChannel.getInstance(null).getMessages().size() - 1);
            }

            PlaybackChannel.getInstance(null).addMessage(channelName + " user changed auto mode to: " + auto);
        }
    }

    public void setAutoOnTime(long time)
    {
        if(isRunning())
        {
            setState(false);
        }

        autoOnTime = time;

        //Auto record change
        if(PlaybackChannel.getInstance(null).allowREC() && allowSend && isBWThreadOn)
        {
            if (PlaybackChannel.getInstance(null).getMessages().get(PlaybackChannel.getInstance(null).getMessages().size() - 1).contains("user changed auto on time to:"))
            {
                PlaybackChannel.getInstance(null).removeMessage(PlaybackChannel.getInstance(null).getMessages().size() - 1);
            }

            PlaybackChannel.getInstance(null).addMessage(channelName + " user changed auto on time to: " + time);
        }
    }

    public void setAutoOffTime(long time)
    {
        if(isRunning())
        {
            setState(false);
        }

        autoOffTime = time;

        //Auto record change
        if(PlaybackChannel.getInstance(null).allowREC() && allowSend && isBWThreadOn)
        {
            if (PlaybackChannel.getInstance(null).getMessages().get(PlaybackChannel.getInstance(null).getMessages().size() - 1).contains("user changed auto off time to:"))
            {
                PlaybackChannel.getInstance(null).removeMessage(PlaybackChannel.getInstance(null).getMessages().size() - 1);
            }

            PlaybackChannel.getInstance(null).addMessage(channelName + " user changed auto off time to: " + time);
        }
    }

    public double getMeanValue()
    {
        return currentMeanVal;
    }

    public double getStandardDeviation()
    {
        return standardDeviation;
    }

    public double getTrend()
    {
        return trend;
    }

    public boolean getAuto()
    {
        return auto;
    }

    public long getAutoOnTime()
    {
        return  autoOnTime;
    }

    public long getAutoOffTime()
    {
        return autoOffTime;
    }

    public long getAutoStartTime()
    {
        return autoStartTime;
    }

    public boolean getInAutoOn()
    {
        return inAutoOn;
    }

    public void start()
    {
        setState(true);
    }

    public void stop()
    {
        setState(false);
    }

    public  boolean isRunning()
    {
        return isBWThreadOn;
    }

    //Generates a list of random normally distributed values
    public List<Double> valueDistribution()
    {
        List<Double> values = new ArrayList<>();

        int num = 0;
        while(num < ids.size())
        {
            //uses mean and standard deviation find list of numbers with numbers of mean and standard deviation
            values.add((rand.nextGaussian() * standardDeviation) + currentMeanVal);
            num++;
        }

        return values;
    }

    @Override
    public void addChannelStateChangeListener(channelStateChangeListener listener)
    {
        listeners.add(listener);
    }

    @Override
    public void setState(boolean turnOn)
    {
        if(turnOn && allowSend)
        {
            //First makes sure the thread is off and there are some nodes in the channel
            if(ids.size() != 0 && !isBWThreadOn)
            {
                //Turn thread on and inform any listeners (GraphPanel) thread is on
                control.stayOn = true;
                controlThread = new Thread(control);
                controlThread.start();

//                long currentStart = Math.round(System.nanoTime() / 1000000.0); //Convert to milliseconds

                //combineBuffer(combineBuffer(getFutureSends(sendDelay, true, currentStart, 0, currentMeanVal), getFutureSends(sendDelay * 2, false, currentStart, 1, currentMeanVal + trend)), getFutureSends(sendDelay * 3, false, currentStart, 2, currentMeanVal + (2 * trend)));

                PlaybackChannel.getInstance(null).addMessage(channelName + " has been turned on");

                if(futureAutoTask != null)
                {
                    //Could lead to a really interesting situation
                    //Turning on and off at the same time
                    futureAutoTask.cancel(true);
                }

                if(auto)
                {
                    //If in auto mode schedule to turn off
                    autoStartTime = System.nanoTime();
                    inAutoOn = true;

                    TimerTask task = new TimerTask() {
                        @Override
                        public void run()
                        {
                            PlaybackChannel.getInstance(null).addMessage(channelName + " auto stop");
                            stop();
                        }
                    };

                    futureAutoTask = scheduledThreadAuto.schedule(task, autoOnTime,TimeUnit.MILLISECONDS);
                }

                isBWThreadOn = true;

            }
        }
        else
        {
            //Only can turn thread off if thread is on...
            if(isBWThreadOn)
            {
                //End thread and tell any listeners
                isBWThreadOn = false;
                control.stayOn = false;
                controlThread.interrupt();

                PlaybackChannel.getInstance(null).addMessage(channelName + " has been turned off");

                if(futureAutoTask != null)
                {
                    futureAutoTask.cancel(true);
                }

                if(auto && allowSend)
                {
                    //if in auto schedule to turn on
                    autoStartTime = System.nanoTime();
                    inAutoOn = false;

                    TimerTask task = new TimerTask() {
                        @Override
                        public void run()
                        {
                            PlaybackChannel.getInstance(null).addMessage(channelName + " auto start");
                            start();
                        }
                    };

                    futureAutoTask = scheduledThreadAuto.schedule(task, autoOffTime,TimeUnit.MILLISECONDS);
                }

                dispatchEvent(false);
            }
        }
    }

    public List<Node> combineBuffer(List<Node> original, List<Node> addition)
    {
        //Combines two node lists by time order
        original.addAll(addition);
        Collections.sort(original);

        return original;
    }

    public List<Node> getFutureSends(long howLong, boolean isStart, long currentStart, int startCycle, double currentMean, double startMean, int startNode, List<Double> distribution)
    {
        List<Node> tempBuffer = new ArrayList<>();

        if(!isStart)
        {
            //If not first cycle then make sure to get any smart send that spills into next cycle
            startCycle--;
            currentMean -= trend;
        }

        //currentStart is an absolute that is used more so as a relative time that just stays constant for the entire life of a control thread
        long endTime = currentStart + howLong;

        double numCyclesThatWillBeCompleted = ((double)howLong) / ((double)sendDelay);

        int roundNumCyclesCompleted = (int)numCyclesThatWillBeCompleted;

        String num = Double.toString(numCyclesThatWillBeCompleted);

        //Finds first 4 digits after decimal and determines if it is greater
        if(Integer.parseInt(num.split("\\.")[1].substring(0, Math.min(num.split("\\.")[1].length(), 4))) > 0)
        {
            //If there is a significant amount of cycle left to complete after last full cycle then make sure it completes last portion of final cycle
            roundNumCyclesCompleted++;
        }

        //Get delay time
        double delayTime = (sendDelay / ids.size());

        //SensorNum is its position in the list and is different than id
        for(int sensorNum = startNode; sensorNum < ids.size(); sensorNum++)
        {
            //Each Sensor goes through all cycles
            for(int cycle = startCycle; cycle < roundNumCyclesCompleted; cycle++)
            {
                //Calculate main normal send time
                long nodeTime = currentStart + (Math.round(delayTime * sensorNum)) + (sendDelay * cycle);

                //Make sure nodeTime is within Time range
                if(nodeTime > endTime)
                {
                    break;
                }

                //Set Node Mean
                double nodeMean = currentMean;

                List<Node> smartSendNodes = new ArrayList<>();

                if(trend != 0)
                {
                    //If there is a trend update node mean to reflect trend
                    nodeMean = currentMean + trend * (cycle-startCycle);

                    //Check to make sure everything stays within range
                    if (nodeMean <= meanMax && nodeMean >= meanMin)
                    {
                        if(threshold < Math.abs(trend))
                        {
                            //Should smart send
                            double changerPerMilli = Math.abs(trend) / ((double) sendDelay);
                            double smartSendTime = threshold / changerPerMilli;

                            //Handle putting smart send in queue
                            for (int i = 0; i < (int) (Math.abs(trend) / threshold); i++)
                            {
                                int multFactor = i + 1;

                                if (Math.round(smartSendTime * multFactor) == sendDelay)
                                {
                                    //Don't send at same time as cycle
                                    break;
                                }

                                //Smart send new value with new mean adjusting for difference
                                double smartMean = nodeMean + ((multFactor * threshold) * (trend / Math.abs(trend)));
                                double value = distribution.get(sensorNum) + (smartMean - startMean);
                                long smartTime = nodeTime + Math.round(smartSendTime * multFactor);


                                if(smartTime <= endTime && smartMean <= meanMax && smartMean >= meanMin)
                                {
                                    //If smart send is a valid send then add it to queue
                                    Node smartNode = new Node(nodeType.smartSend, ids.get(sensorNum), cycle, smartTime, smartMean, value);
                                    smartSendNodes.add(smartNode);
                                }
                                else
                                {
                                    break;
                                }

                            }
                        }
                    }
                    else
                    {
                        //if Node mean is above or below max/min no trend or smart send just leave flat
                        nodeMean = currentMean;
                    }
                }

                //Make main send for cycle
                double value = distribution.get(sensorNum) + (nodeMean - startMean);
                Node mainNode = new Node(nodeType.normal,ids.get(sensorNum),cycle,nodeTime,nodeMean, value);

                if(isStart)
                {
                    //If first time then add every thing
                    tempBuffer.add(mainNode);
                    tempBuffer.addAll(smartSendNodes);
                }
                else
                {
                    if(cycle == startCycle)
                    {
                        //If start cycle is pushed back then only grab smart sends after that cycle time so that the new start will include old smart send
                        long endCycle = currentStart + (startCycle + 1) * sendDelay;

                        for(Node sn : smartSendNodes)
                        {
                            if(sn.getTime() > endCycle)
                            {
                                tempBuffer.add(sn);
                            }
                        }
                    }
                    else
                    {
                        //add new data
                        tempBuffer.add(mainNode);
                        tempBuffer.addAll(smartSendNodes);
                    }
                }
            }
        }

        //Sort list by time
        Collections.sort(tempBuffer);

        return tempBuffer;
    }

    private void dispatchEvent(boolean isOn)
    {
        final channelStateChangeEvent event = new channelStateChangeEvent(this);

        for (channelStateChangeListener l : listeners)
        {
            //send out message to all listeners
            dispatchRunnableOnEventQueue(l, event, isOn);
        }
    }

    private void dispatchEvent()
    {
        final channelStateChangeEvent event = new channelStateChangeEvent(this);

        for (channelStateChangeListener l : listeners)
        {
            //send out message to all listeners
            dispatchRunnableOnEventQueue(l, event);
        }
    }

    private void dispatchRunnableOnEventQueue(final channelStateChangeListener listener, final channelStateChangeEvent event, final boolean isOn)
    {
        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                if(isOn)
                {
                    //if thread is now on tell listener to run channelON
                    listener.channelOn(event);
                }
                else
                {
                    //if thread is now off tell listener to run channelOFF
                    listener.channelOff(event);
                }
            }
        });
    }

    private void dispatchRunnableOnEventQueue(final channelStateChangeListener listener, final channelStateChangeEvent event)
    {
        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                listener.delayChange(event);
            }
        });
    }

    public enum nodeType {normal, smartSend}

    public static class Node implements Comparable<Node>
    {
        private int cycle;
        private long time;
        private String nodeId;
        private double mean;
        private double value;
        private nodeType type;

        public Node(nodeType type, String nodeId, int cycle, long time, double mean, double value)
        {
            this.type = type;
            this.cycle = cycle;
            this.nodeId = nodeId;
            this.time = time;
            this.mean = mean;
            this.value = value;
        }

        public Node(Node node)
        {
            //Allows for user to copy Node by value rather than by reference
            this(node.getType(),node.getNodeId(),node.getCycle(),node.getTime(),node.getMean(),node.getValue());
        }

        public void setTime(long time)
        {
            this.time = time;
        }

        public int getCycle()
        {
            return  cycle;
        }

        public long getTime()
        {
            return  time;
        }

        public nodeType getType()
        {
            return this.type;
        }

        public String getNodeId()
        {
            return this.nodeId;
        }

        public double getMean()
        {
            return this.mean;
        }

        public double getValue()
        {
            return this.value;
        }

        @Override
        public boolean equals(Object other)
        {
            //Used for contains
            boolean result = false;
            if (other instanceof Node)
            {
                result = ((Node) other).getCycle() == this.getCycle() && ((Node) other).getNodeId().equals(this.getNodeId()) && ((Node) other).getMean() == this.getMean() && ((Node) other).getType().equals(this.getType());
            }
            return result;
        }

        @Override
        public int compareTo(Node object)
        {
            //Used for sorting algorithm
            int result = 0;

            if(this.getTime() > object.getTime())
            {
                result = 1;
            }
            else if(this.getTime() < object.getTime())
            {
                result = -1;
            }

            return result;
        }
    }

}


//Interface for how channel state change dispatcher should look like
interface channelStateChangeDispatcher
{
    public void addChannelStateChangeListener(channelStateChangeListener listener);
    public void setState(boolean turnOn);
}

//For the listener to define what happens when event is fired
interface channelStateChangeListener extends EventListener
{
    public void channelOn(channelStateChangeEvent event);
    public void channelOff(channelStateChangeEvent event);
    public void delayChange(channelStateChangeEvent event);
}

//Generic event class to be used
class channelStateChangeEvent extends EventObject
{

    private final channelStateChangeDispatcher dispatcher;

    public channelStateChangeEvent(channelStateChangeDispatcher dispatcher)
    {
        super(dispatcher);
        this.dispatcher = dispatcher;
    }

    // type safe way to get source (as opposed to getSource of EventObject
    public channelStateChangeDispatcher getDispatcher()
    {
        return dispatcher;
    }
}

