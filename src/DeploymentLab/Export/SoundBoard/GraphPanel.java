package DeploymentLab.Export.SoundBoard;

import DeploymentLab.Export.SuperSimConfigExporter;

import javax.swing.*;
import java.awt.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;

public class GraphPanel extends JPanel
{
    //Paddings used for graph
    int padding = 25;
    int topPadding = 5;

    MasterPanel.Controller controller;
    int numberYDivisions = 0;

    private Color gridColor = new Color(200, 200, 200, 200);
    private Color valColor;
    private List<ComponentHandler> componentHandlers;
    private List<ComponentHandler> activeHandlers;
    List<Color> colors;
    Random rand = new Random();

    private boolean cycle = false;
    private boolean prevAuto = false;

    //Strokes used to draw on graph
    private static final Stroke GRAPH_STROKE = new BasicStroke(1.5f);
    static float[] dash1 = { 2f, 0f, 2f };
    private static final Stroke DOTTED_GRAPH_STROKE = new BasicStroke(.75f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 1f, dash1, 2f);
    private static final int GRAPH_POINT_WIDTH = 6;

    ScheduledExecutorService repaintSchedule;
    ScheduledFuture<?> futureTask;

    public GraphPanel(MasterPanel.Controller cont, Color backColor, Color valColor, List<ComponentHandler> components)
    {
        controller = cont;

        repaintSchedule = Executors.newScheduledThreadPool(1);

        //Set background color and initialize all variables
        super.setBackground(backColor);
        this.valColor = valColor;
        componentHandlers = components;
        colors = new ArrayList<>();
        activeHandlers = new ArrayList<>();

        //Graph listens to Component Handler to get graph data
        for(final ComponentHandler component : componentHandlers)
        {
            int r = rand.nextInt((255 - 150) + 1) + 150;

            int g = rand.nextInt((255 - 150) + 1) + 150;
            if(r >= 220)
            {
                g = rand.nextInt((220 - 150) + 1) + 150;
            }

            int b = rand.nextInt((255 - 150) + 1) + 150;
            if(r >= 220 || g >= 220)
            {
                b = rand.nextInt((220 - 150) + 1) + 150;
            }

            //Each channel gets its own unique random color
            Color color = new Color(r, g, b);
            colors.add(color);

            component.addChannelStateChangeListener(new channelStateChangeListener() {
                @Override
                public void channelOn(channelStateChangeEvent event)
                {
                    //Add channel to active Channel list
                    if(!activeHandlers.contains(component))
                    {
                        activeHandlers.add(component);
                    }

                    //Cancels all future tasks
                    if(futureTask != null)
                    {
                        if(!futureTask.isDone())
                        {
                            futureTask.cancel(true);
                            futureTask = null;
                        }
                    }

                    cycle = true;
                    repaint();
                }

                @Override
                public void channelOff(channelStateChangeEvent event)
                {
                    //Remove Channel from active list

                    if(!component.getAuto() || !component.getAllowSend())
                    {
                        //If it is off because of auto time then dont remove
                        activeHandlers.remove(component);
                    }

                    if(futureTask != null)
                    {
                        if(!futureTask.isDone())
                        {
                            futureTask.cancel(true);
                            futureTask = null;
                        }
                    }

                    cycle = true;
                    repaint();
                }

                @Override
                public  void delayChange(channelStateChangeEvent event)
                {
                    //Update for cycle time change
                    if(futureTask != null)
                    {
                        if(!futureTask.isDone())
                        {
                            futureTask.cancel(true);
                            futureTask = null;
                        }
                    }

                    cycle = true;
                    repaint();
                }
            });
        }
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        if(cycle)
        {
            //Only schedule new timers if Sound Board wants it minimizing effect of repaints called by Java Swing
            if (futureTask != null)
            {
                futureTask.cancel(true);
            }

            futureTask = null;
            cycle = false;

            if(activeHandlers.size() != 0)
            {
                //At the end reschedule another repaint in 1 minute
                futureTask = repaintSchedule.schedule(new TimerTask() {
                    @Override
                    public void run()
                    {
                        cycle = true;
                        repaint();
                    }
                }, SoundBoardConstants.graphUpdateDelay, TimeUnit.MILLISECONDS);
            }
        }

        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        FontMetrics metrics = g2.getFontMetrics();

        //How many ticks for y axis
        switch (controller)
        {
            case TEMPERATURE:
                numberYDivisions = 5;
                break;
            case PRESSURE:
                numberYDivisions = 4;
                break;
            case SUPPLY:
                numberYDivisions = 5;
                break;
            case RETURN:
                numberYDivisions = 5;
                break;
        }

        //Generate a scale so objects go from graph relative location to panel location
        double xScale = ((double)super.getWidth() - (3* padding)) / (SoundBoardConstants.numMillisInGraph);
        double yScale = ((double)super.getHeight() - (2* padding) - topPadding) / (getMax() - getMin());

        //Create Graph space and borders to make it look 3D
        g2.setColor(valColor);
        g2.fill3DRect(padding * 2, topPadding, super.getWidth() - (3 * padding), super.getHeight() - 2 * padding, false);
        g2.setColor(gridColor);
        g2.drawLine(getWidth() - padding, super.getHeight() - 2 * padding + 3, getWidth() - padding, topPadding);
        g2.setColor(Color.WHITE);
        g2.drawLine(2 * padding, super.getHeight() - 2 * padding + 3, getWidth() - padding, super.getHeight() - 2 * padding + 3);

        //Draws lines for the y axis
        for (int i = 0; i < numberYDivisions + 1; i++) {
            int x = 2 * padding;
            int y = super.getHeight() - ((i * (super.getHeight() - padding * 2 - topPadding)) / numberYDivisions + 2 * padding);

            if(i != 0 && i != numberYDivisions)
            {
                g2.setColor(gridColor);
                g2.drawLine(2 * padding + 1, y, super.getWidth() - padding, y);
            }

            g2.setColor(Color.WHITE);
            String yLabel = ((int) ((getMin() + (getMax() - getMin()) * ((i * 1.0) / numberYDivisions)) * 100)) / 100.0 + "";
            int labelWidth = metrics.stringWidth(yLabel);
            g2.drawString(yLabel, x - labelWidth - 5, y + (metrics.getHeight() / 2) - 3);
        }


        int maxY = -1;
        //Draw lines for the x axis
        for (int i = 0; i < SoundBoardConstants.numMillisInGraph + 1; i += 300000)
        {
            int x = i * (getWidth() - padding * 3) / ((int)SoundBoardConstants.numMillisInGraph) + padding * 2;
            int y = getHeight() - padding * 2;

            if(i != SoundBoardConstants.numMillisInGraph && i != 0 )
            {
                g2.setColor(Color.GRAY);
                g2.drawLine(x, super.getHeight() - padding * 2 + 1, x, topPadding);
            }

            g2.setColor(Color.WHITE);
            String xLabel = (i/60000) + "";

            int labelWidth = metrics.stringWidth(xLabel);
            g2.drawString(xLabel, x - labelWidth / 2, y + metrics.getHeight() + 3);

            if((y + metrics.getHeight() + 3 + metrics.getHeight()) > maxY)
            {
                maxY = y + metrics.getHeight() + 3 + metrics.getHeight();
            }
        }

        int xNamePos = padding * 2;
        //Graph time ;)

        for(ComponentHandler components : activeHandlers)
        {
            long sendDelay = components.getSendDelay();
            long currentTime = System.nanoTime();
            long timeElapsed = ((currentTime - components.getStartTime()) / 1000000) % sendDelay;
            long timeLeftMilli = sendDelay - timeElapsed;

            //Add channel name to legend at bottom of graph
            int nameWidth = metrics.stringWidth(components.getChannelName());
            g2.setColor(colors.get(componentHandlers.indexOf(components)));
            g2.drawString(components.getChannelName(), xNamePos, maxY + 5);
            xNamePos += nameWidth + 10;

            //Decide what sort of graph to draw
            if(!components.getAuto())
            {
                normalGraph(g2, components,sendDelay, timeElapsed,timeLeftMilli,xScale,yScale);
            }
            else
            {
                autoGraph(g2, currentTime, components, sendDelay, timeElapsed,timeLeftMilli,xScale,yScale);
            }
        }
    }

    public void normalGraph(Graphics2D g2, ComponentHandler components, long sendDelay, long timeElapsed, long timeLeftMilli, double xScale, double yScale)
    {
        //Get and graph points for mean line
        List<Point> graphPoints = getPoints(components,0,sendDelay,timeElapsed,timeLeftMilli,1800000,xScale,yScale, components.getMeanValue(), components.getMeanMax(), components.getMeanMin());
        g2.setStroke(GRAPH_STROKE);

        for (int i = 0; i < graphPoints.size() - 1; i++)
        {
            int x1 = graphPoints.get(i).x;
            int y1 = graphPoints.get(i).y;
            int x2 = graphPoints.get(i + 1).x;
            int y2 = graphPoints.get(i + 1).y;

            g2.drawLine(x1, y1, x2, y2);

            if(i != 0)
            {
                int x = graphPoints.get(i ).x - GRAPH_POINT_WIDTH / 2;
                int y = graphPoints.get(i ).y - GRAPH_POINT_WIDTH / 2;
                int ovalW = GRAPH_POINT_WIDTH;
                int ovalH = GRAPH_POINT_WIDTH;
                g2.fillOval(x, y, ovalW, ovalH);
            }
        }

        List<Point> smartSendPoints = findSmartSendPoints(components, 0,sendDelay,timeElapsed,timeLeftMilli,1800000,xScale,yScale, components.getMeanValue(), components.getMeanMax(), components.getMeanMin());
        g2.setStroke(GRAPH_STROKE);

        for (int i = 0; i < smartSendPoints.size(); i++)
        {
            int x = smartSendPoints.get(i).x;
            int y1 = smartSendPoints.get(i).y + 3;
            int y2 = smartSendPoints.get(i).y - 3;
            g2.drawLine(x, y1, x, y2);
        }

        //Draw lines for standard deviation
        g2.setStroke(DOTTED_GRAPH_STROKE);
        List<Point> graphPointsStdAbove = getPoints(components,0,sendDelay,timeElapsed,timeLeftMilli,1800000,xScale,yScale, components.getMeanValue() + components.getStandardDeviation(), components.getMeanMax() + components.getStandardDeviation(), components.getMeanMin() + components.getStandardDeviation());

        for (int i = 0; i < graphPointsStdAbove.size() - 1; i++)
        {
            int x1 = graphPointsStdAbove.get(i).x;
            int y1 = graphPointsStdAbove.get(i).y;
            int x2 = graphPointsStdAbove.get(i + 1).x;
            int y2 = graphPointsStdAbove.get(i + 1).y;
            g2.drawLine(x1, y1, x2, y2);
        }

        List<Point> graphPointsStdBelow = getPoints(components,0,sendDelay,timeElapsed,timeLeftMilli,1800000,xScale,yScale, components.getMeanValue() - components.getStandardDeviation(), components.getMeanMax() - components.getStandardDeviation(), components.getMeanMin() - components.getStandardDeviation());

        for (int i = 0; i < graphPointsStdBelow.size() - 1; i++)
        {
            int x1 = graphPointsStdBelow.get(i).x;
            int y1 = graphPointsStdBelow.get(i).y;
            int x2 = graphPointsStdBelow.get(i + 1).x;
            int y2 = graphPointsStdBelow.get(i + 1).y;
            g2.drawLine(x1, y1, x2, y2);
        }
    }

    public void autoGraph(Graphics2D g2, long currentTime, ComponentHandler components,  long sendDelay, long timeElapsed, long timeLeftMilli, double xScale, double yScale)
    {
        boolean isOn = components.getInAutoOn();
        double meanValue = components.getMeanValue();

        //How far into auto time
        long timeAdjust = (currentTime - components.getAutoStartTime()) / 1000000;

        if(prevAuto != isOn)
        {
            //Just in case runs before update but timeAdjust goes to large
            prevAuto = isOn;

            if(isOn)
            {
                if(timeAdjust > components.getAutoOffTime())
                {
                    timeAdjust -= components.getAutoOffTime();
                }
            }
            else
            {
                if(timeAdjust > components.getAutoOnTime())
                {
                    timeAdjust -= components.getAutoOnTime();
                }
            }
        }

        for(long time = 0; time < 1800000; time = isOn ? (time + components.getAutoOffTime()) : (time + components.getAutoOnTime()) )
        {
            //increment time on graph depending if its in on or off part of cycle
            if(isOn)
            {
                //get end time of period
                long endTime = time + components.getAutoOnTime();

                if(time == 0)
                {
                    //Adjust end time for first time
                    endTime -= timeAdjust;
                }

                if(endTime > 1800000)
                {
                    //if end time is to long, don't go all the way
                    long sub = endTime - 1800000;
                    endTime -= sub;
                }

                //Time to next cycle
                long nextCycle = 0;

                if(time == 0)
                {
                    //is time left milli
                    nextCycle = time + timeLeftMilli;
                }
                else
                {
                    //go one cycle
                    nextCycle = time + sendDelay;
                }

                if(nextCycle > endTime)
                {
                    //if next cycle is after end time then bring it to first cycle
                    nextCycle = time;
                }

                //Get and graph points for mean line
                List<Point> graphPoints = getPoints(components,time,sendDelay,timeElapsed,nextCycle,endTime,xScale,yScale, meanValue, components.getMeanMax(), components.getMeanMin());
                g2.setStroke(GRAPH_STROKE);

                for (int i = 0; i < graphPoints.size() - 1; i++)
                {
                    int x1 = graphPoints.get(i).x;
                    int y1 = graphPoints.get(i).y;
                    int x2 = graphPoints.get(i + 1).x;
                    int y2 = graphPoints.get(i + 1).y;

                    g2.drawLine(x1, y1, x2, y2);

                    if(i != 0)
                    {
                        int x = graphPoints.get(i ).x - GRAPH_POINT_WIDTH / 2;
                        int y = graphPoints.get(i ).y - GRAPH_POINT_WIDTH / 2;
                        int ovalW = GRAPH_POINT_WIDTH;
                        int ovalH = GRAPH_POINT_WIDTH;
                        g2.fillOval(x, y, ovalW, ovalH);
                    }
                }

                List<Point> smartSendPoints = findSmartSendPoints(components,time,sendDelay,timeElapsed,nextCycle,endTime,xScale,yScale, meanValue, components.getMeanMax(), components.getMeanMin());
                g2.setStroke(GRAPH_STROKE);

                for (int i = 0; i < smartSendPoints.size(); i++)
                {
                    int x = smartSendPoints.get(i).x;
                    int y1 = smartSendPoints.get(i).y + 3;
                    int y2 = smartSendPoints.get(i).y - 3;
                    g2.drawLine(x, y1, x, y2);
                }

                //Draw lines for standard deviation
                g2.setStroke(DOTTED_GRAPH_STROKE);
                List<Point> graphPointsStdAbove = getPoints(components,time,sendDelay,timeElapsed,nextCycle,endTime,xScale,yScale, meanValue + components.getStandardDeviation(), components.getMeanMax() + components.getStandardDeviation(), components.getMeanMin() + components.getStandardDeviation());

                for (int i = 0; i < graphPointsStdAbove.size() - 1; i++)
                {
                    int x1 = graphPointsStdAbove.get(i).x;
                    int y1 = graphPointsStdAbove.get(i).y;
                    int x2 = graphPointsStdAbove.get(i + 1).x;
                    int y2 = graphPointsStdAbove.get(i + 1).y;
                    g2.drawLine(x1, y1, x2, y2);
                }

                List<Point> graphPointsStdBelow = getPoints(components,time,sendDelay,timeElapsed,nextCycle,endTime,xScale,yScale, meanValue - components.getStandardDeviation(), components.getMeanMax() - components.getStandardDeviation(), components.getMeanMin() - components.getStandardDeviation());

                for (int i = 0; i < graphPointsStdBelow.size() - 1; i++)
                {
                    int x1 = graphPointsStdBelow.get(i).x;
                    int y1 = graphPointsStdBelow.get(i).y;
                    int x2 = graphPointsStdBelow.get(i + 1).x;
                    int y2 = graphPointsStdBelow.get(i + 1).y;
                    g2.drawLine(x1, y1, x2, y2);
                }

                int numCyclesCompleted = 0;

                if(time == 0)
                {
                    //calculate number of cycles completed
                    numCyclesCompleted = (int)(timeAdjust / sendDelay);
                }

                double storedValue = meanValue;

                if(sendDelay <= components.getAutoOnTime())
                {
                    //Adjust the mean by the number of cycles completed
                    for(int i = 1; i <= (int)(components.getAutoOnTime() / sendDelay) - numCyclesCompleted; i++)
                    {
                        double newValue = storedValue + (components.getTrend() * i);

                        if(newValue <= components.getMeanMax() && newValue >= components.getMeanMin())
                        {
                            meanValue = newValue;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }

            //Switch state
            isOn = !isOn;
            timeElapsed = 0;

            if(time == 0)
            {
                //make time adjust
                time -= timeAdjust;
            }

        }
    }

    //Finds max value for given controller
    private double getMax()
    {
        double max = 0;

        switch (controller)
        {
            case TEMPERATURE:
                max = SoundBoardConstants.tempMeanMaxVal + SoundBoardConstants.tempStDevMaxVal + 5;
                break;
            case PRESSURE:
                max = SoundBoardConstants.presMeanMaxVal + SoundBoardConstants.presStDevMaxVal + .04;
                break;
            case SUPPLY:
                max = SoundBoardConstants.supMeanMaxVal + SoundBoardConstants.supStDevMaxVal + 5;
                break;
            case RETURN:
                max = SoundBoardConstants.retMeanMaxVal + SoundBoardConstants.retStDevMaxVal + 5;
                break;
        }

        return max;
    }

    //Find min value for given controller
    private double getMin()
    {
        double max = 0;

        switch (controller)
        {
            case TEMPERATURE:
                max = SoundBoardConstants.tempMeanMinVal - SoundBoardConstants.tempStDevMaxVal - 5;
                break;
            case PRESSURE:
                max = SoundBoardConstants.presMeanMinVal - SoundBoardConstants.presStDevMaxVal - .04;
                break;
            case SUPPLY:
                max = SoundBoardConstants.supMeanMinVal - SoundBoardConstants.supStDevMaxVal - 5;
                break;
            case RETURN:
                max = SoundBoardConstants.retMeanMinVal - SoundBoardConstants.retStDevMaxVal - 5;
                break;
        }

        return max;
    }

    private List<Point> getPoints(ComponentHandler componentHandler, long startTimeOnGraph, long sendDelay,long timeElapsed,long timeLeftMilli, long howLong, double xScale, double yScale, double value, double max, double min)
    {
        List<Point> points = new ArrayList<>();

        double rate = (componentHandler.getTrend() / sendDelay);

        //Original points
        int x0 = (int) (startTimeOnGraph * xScale + 2 * padding );
        int y0 = (int) ((getMax() - ((timeElapsed * rate) + value)) * yScale + topPadding);

        points.add(new Point(x0,y0));

        //Used for tracking trend
        double z = 1.0;
        double oldValue = y0;
        boolean lastCorrection = false;

        for(long i = timeLeftMilli; i <= howLong; i += sendDelay)
        {
            if(timeLeftMilli != startTimeOnGraph)
            {
                //Find new Trend value
                double trendAdjust = ((((i-timeLeftMilli) % sendDelay) * rate) + (value + (z * componentHandler.getTrend())));

                if(trendAdjust > 1)
                {
                    trendAdjust = Math.round(trendAdjust);
                }
                else
                {
                    BigDecimal bd = new BigDecimal(trendAdjust);
                    bd = bd.setScale(3, RoundingMode.HALF_UP);
                    trendAdjust = bd.doubleValue();
                }

                if(trendAdjust <= max && trendAdjust >= min)
                {
                    if(lastCorrection)
                    {
                        double trendFullAdjust =  (value + ((z +1) * componentHandler.getTrend()));

                        if(trendFullAdjust > max || trendFullAdjust < min)
                        {
                            //If not then keep line flat
                            trendAdjust = oldValue;
                        }
                        else
                        {
                            //Make sure trend is within bounds
                            trendAdjust = ((getMax() - trendAdjust) * yScale + topPadding);
                            oldValue = trendAdjust;
                        }
                    }
                    else
                    {
                        //Make sure trend is within bounds
                        trendAdjust = ((getMax() - trendAdjust) * yScale + topPadding);
                        oldValue = trendAdjust;
                    }
                }
                else
                {
                    //If not then keep line flat
                    trendAdjust = oldValue;
                }

                //Calculate new x and y points
                int x1 = (int) ((i) * xScale + 2 * padding);
                int y1 = (int) trendAdjust;
                points.add(new Point(x1,y1));

                z++;
            }

            long next = i + sendDelay;

            //Make sure it always goes to end
            if(i != howLong && next > howLong)
            {
                long sub = i - (howLong - sendDelay);
                i -= sub;
                z--;
                lastCorrection = true;
            }

            startTimeOnGraph = -1;
        }

        return points;
    }

    private List<Point> findSmartSendPoints(ComponentHandler componentHandler, long startTimeOnGraph, long sendDelay,long timeElapsed,long timeLeftMilli, long howLong, double xScale, double yScale, double value, double max, double min)
    {
        List<Point> points = new ArrayList<>();

        if(componentHandler.getThreshold() < Math.abs(componentHandler.getTrend()))
        {
            //only get smart send if threshold is less than trend
            double rate = (componentHandler.getTrend() / sendDelay);

            //Calculate smart send times
            double changerPerMilli = Math.abs(componentHandler.getTrend()) / componentHandler.getSendDelay();
            double smartSendTime = componentHandler.getThreshold() / changerPerMilli;

            //follow trend and on first cycle
            double z = 1.0;
            for(int i = 0; i < (int)(Math.abs(componentHandler.getTrend())/componentHandler.getThreshold()); i++)
            {
                int multFactor = i + 1;

                if(Math.round(smartSendTime * multFactor) == sendDelay)
                {
                    break;
                }

                double timeLeft = startTimeOnGraph + (smartSendTime * multFactor) - timeElapsed;

                if (timeLeft >= 0 && (timeLeft - 10) <= howLong)
                {
                    //Make sure time is within how much time is left

                    double trendAdjust =  (value + (z * componentHandler.getTrend()));

                    if(trendAdjust > 1)
                    {
                        trendAdjust = Math.round(trendAdjust);
                    }
                    else
                    {
                        BigDecimal bd = new BigDecimal(trendAdjust);
                        bd = bd.setScale(3, RoundingMode.HALF_UP);
                        trendAdjust = bd.doubleValue();
                    }


                    if(trendAdjust > max || (trendAdjust ) < min)
                    {
                        //Smart send only happens if mean is increasing so this checks to make sure mean is growing
                        break;
                    }

                    //Get smard send value
                    double val = value + (componentHandler.getTrend() * (z -1)) + componentHandler.getThreshold() * multFactor * (componentHandler.getTrend() / Math.abs(componentHandler.getTrend()));
                    val = ((getMax() - val) * yScale + topPadding);

                    int x = (int) (timeLeft * xScale + 2 * padding);
                    int y = (int) Math.round(val);

                    points.add(new Point(x, y));
                }
            }

            //Now for rest of cycles
            z++;
            for(long i = timeLeftMilli; i <= howLong; i += sendDelay)
            {
                //ALREADY DID FIRST CYCLE...
                if(i != startTimeOnGraph)
                {
                    for (int j = 0; j < (int) (Math.abs(componentHandler.getTrend()) / componentHandler.getThreshold()); j++)
                    {
                        int multFactor = j + 1;

                        if (Math.round(smartSendTime * multFactor) == sendDelay)
                        {
                            break;
                        }

                        double time = (smartSendTime * multFactor) + i;

                        double trendAdjust = ((((i - timeLeftMilli) % sendDelay) * rate) + (value + (z * componentHandler.getTrend())));

                        if (trendAdjust > 2)
                        {
                            trendAdjust = Math.round(trendAdjust);
                        }

                        if (trendAdjust > max || (trendAdjust) < min)
                        {
                            //Check to make sure valid
                            break;
                        }

                        double val = value + (componentHandler.getTrend() * (z - 1)) + componentHandler.getThreshold() * multFactor * (componentHandler.getTrend() / Math.abs(componentHandler.getTrend()));

                        val = ((getMax() - val) * yScale + topPadding);


                        if ((time - 10) <= howLong)
                        {
                            //Make sure time is within limits
                            int x = (int) (time * xScale + 2 * padding);
                            int y = (int) Math.round(val);

                            points.add(new Point(x, y));
                        }
                    }
                }

                long next = i + sendDelay;

                //Make sure it always goes to end
                if(i != howLong && next > howLong)
                {
                    long sub = i - (howLong - sendDelay);
                    i -= sub;
                }

                z++;
            }
        }
        return points;
    }
}
