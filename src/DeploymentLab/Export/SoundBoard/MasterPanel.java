package DeploymentLab.Export.SoundBoard;

import DeploymentLab.CentralCatalogue;
import DeploymentLab.Model.ComponentModel;
import DeploymentLab.Model.DLComponent;
import DeploymentLab.Model.DLObject;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;

public class MasterPanel extends JPanel implements stateChangedDispatcher
{
    //Used to define what type of controller the panel is
    public enum Controller
    {
        TEMPERATURE(0), PRESSURE(1), SUPPLY(2), RETURN(3);

        private int value;

        private Controller(int value)
        {
            this.value = value;
        }
    }

    JSpinner masterSpinner;
    Controller controller;
    ComponentModel componentModel;
    ComponentHandler componentHandler;
    List<stateChangedListener> listeners;
    JSpinner stdSpinner;
    JSpinner trendSpinner;
    JSpinner sendSpinner;
    JSpinner threshSpinner;
    JCheckBox sendCheckBox;
    JSpinner onSpinner;
    JSpinner offSpinner;
    JCheckBox autoOnOffCheckBox;

    boolean autoOnOff = false;

    JLabel meanlbl;
    JLabel stdevlbl;
    JLabel trendlbl;
    JLabel timelbl;
    JLabel threshlbl;
    JLabel onlbl;
    JLabel offlbl;

    JComboBox modeSelection;

    boolean isMasterMode;

    public MasterPanel(String channelName, Controller cont, Color backColor, Color valColor)
    {
        super(new MigLayout("fillx", "[right]rel[grow,fill]", "[]10[]")); //Sets panel to miglayout

        //initialze components
        listeners = new ArrayList<>();
        componentModel = CentralCatalogue.getOpenProject().getComponentModel();
        this.controller = cont;
        isMasterMode = true; //Starts In Master Mode
        componentHandler = new ComponentHandler(controller, channelName);

        //Gets all objects of given type specified by controller
        updateSelection();

        String[] modes = {"Value", "Send", "Auto"};

        modeSelection = new JComboBox<>(modes);

        //Create components of form
        JLabel lblMasterPres = new JLabel(getLabel());
        lblMasterPres.setForeground(Color.WHITE);

        meanlbl = new JLabel("MV");
        meanlbl.setForeground(Color.WHITE);
        meanlbl.setHorizontalAlignment(SwingConstants.RIGHT);
        meanlbl.setPreferredSize(new Dimension(20,10));

        stdevlbl = new JLabel("St. Dev.");
        stdevlbl.setForeground(Color.WHITE);
        stdevlbl.setHorizontalAlignment(SwingConstants.RIGHT);
        stdevlbl.setPreferredSize(new Dimension(50,10));

        trendlbl = new JLabel("TV");
        trendlbl.setForeground(Color.WHITE);
        trendlbl.setPreferredSize(new Dimension(20,10));
        trendlbl.setHorizontalAlignment(SwingConstants.RIGHT);

        timelbl = new JLabel("TI");
        timelbl.setForeground(Color.WHITE);
        timelbl.setPreferredSize(new Dimension(20,10));
        timelbl.setHorizontalAlignment(SwingConstants.RIGHT);

        threshlbl = new JLabel("TH");
        threshlbl.setForeground(Color.WHITE);
        threshlbl.setPreferredSize(new Dimension(50,10));
        threshlbl.setHorizontalAlignment(SwingConstants.RIGHT);

        onlbl = new JLabel("ON");
        onlbl.setForeground(Color.WHITE);
        onlbl.setPreferredSize(new Dimension(20,10));
        onlbl.setHorizontalAlignment(SwingConstants.RIGHT);

        offlbl = new JLabel("OFF");
        offlbl.setForeground(Color.WHITE);
        offlbl.setPreferredSize(new Dimension(20,10));
        offlbl.setHorizontalAlignment(SwingConstants.RIGHT);

        List<Component> spinners = JSpinnerMaker.generateSpinners(controller,componentHandler, true, this);

        masterSpinner = (JSpinner)spinners.get(0);
        stdSpinner = (JSpinner)spinners.get(1);
        trendSpinner = (JSpinner)spinners.get(2);

        sendSpinner = (JSpinner)spinners.get(3);
        sendSpinner.setPreferredSize(new Dimension(30,20));

        threshSpinner = (JSpinner)spinners.get(4);
        sendSpinner.setPreferredSize(new Dimension(30,20));

        onSpinner = (JSpinner)spinners.get(5);
        onSpinner.setPreferredSize(new Dimension(30,20));
        onSpinner.setEnabled(false);

        offSpinner = (JSpinner)spinners.get(6);
        offSpinner.setPreferredSize(new Dimension(30,20));
        offSpinner.setEnabled(false);

        sendCheckBox = new JCheckBox("Send");
        sendCheckBox.setForeground(Color.WHITE);
        sendCheckBox.setHorizontalAlignment(SwingConstants.CENTER);
        sendCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                //Start sending data for master panel
                if (!isMasterOn())
                {
                    turnMasterOn();
                }

                componentHandler.setAllowSend(sendCheckBox.isSelected());
            }
        });

        autoOnOffCheckBox = new JCheckBox("Auto");
        autoOnOffCheckBox.setForeground(Color.WHITE);
        autoOnOffCheckBox.setHorizontalAlignment(SwingConstants.CENTER);

        autoOnOffCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                //Turn auto mode on and off and change is time spinners are on or off
                if (autoOnOffCheckBox.isSelected())
                {
                    if (!isMasterOn())
                    {
                        turnMasterOn();
                    }

                    autoOnOff = true;
                    onSpinner.setEnabled(true);
                    offSpinner.setEnabled(true);
                    componentHandler.setAuto(true);

                    componentHandler.start();
                }
                else
                {
                    if (!isMasterOn())
                    {
                        turnMasterOn();
                    }

                    autoOnOff = false;
                    onSpinner.setEnabled(false);
                    offSpinner.setEnabled(false);
                    componentHandler.setAuto(false);

                    componentHandler.start();
                }
            }
        });

        //Make panel pretty
        super.setBackground(backColor);

        super.add(lblMasterPres, "split 3, align left");
        super.add(sendCheckBox, "width 65::");
        super.add(modeSelection, "width 65::, wrap");


        //Add components!
        if(controller.value == 0 || controller.value == 1)
        {
            super.setPreferredSize(new Dimension(SoundBoardConstants.channelWidth * 4 + 55,80));
            super.add(meanlbl, "split 6, gapx 35, width 20::");
            super.add(masterSpinner, "width 60::");
            super.add(stdevlbl, "width 50::");
            super.add(stdSpinner, "width 60::");
            super.add(trendlbl, "width 20::");
            super.add(trendSpinner, "width 60::");
            super.setPreferredSize(new Dimension(SoundBoardConstants.channelWidth * 4 + 55,80));

        }
        else if(controller.value == 2 || controller.value == 3)
        {
            super.setPreferredSize(new Dimension(SoundBoardConstants.channelWidth * 3,80));
            super.add(meanlbl, "split 6,  width 20::");
            super.add(masterSpinner, "width 10::");
            super.add(stdevlbl, "width 50::");
            super.add(stdSpinner, "width 5::");
            super.add(trendlbl, "width 20::");
            super.add(trendSpinner, "width 5::");
            super.setPreferredSize(new Dimension(SoundBoardConstants.channelWidth * 3,80));
        }

        modeSelection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (!isMasterOn())
                {
                    turnMasterOn();
                }

                modeSelection.setPreferredSize(new Dimension(65,20));
                updateMenu();
            }
        });
    }

    public void updateMenu()
    {
        //Change what UI displays depending on mode
        String mode = (String)modeSelection.getSelectedItem();

        if(mode.equals("Value"))
        {
            super.remove(timelbl);
            super.remove(sendSpinner);
            super.remove(threshlbl);
            super.remove(threshSpinner);

            super.remove(autoOnOffCheckBox);
            super.remove(onlbl);
            super.remove(onSpinner);
            super.remove(offlbl);
            super.remove(offSpinner);

            if(controller.value == 0 || controller.value == 1)
            {
                super.add(meanlbl, "split 6, gapx 35, width 20::");
                super.add(masterSpinner, "width 60::");
                super.add(stdevlbl, "width 50::");
                super.add(stdSpinner, "width 60::");
                super.add(trendlbl, "width 20::");
                super.add(trendSpinner, "width 60::");

                super.repaint();
            }
            else
            {
                super.add(meanlbl, "split 6,  width 20::");
                super.add(masterSpinner, "width 10::");
                super.add(stdevlbl, "width 50::");
                super.add(stdSpinner, "width 5::");
                super.add(trendlbl, "width 20::");
                super.add(trendSpinner, "width 5::");

                super.repaint();
            }
        }
        else if(mode.equals("Send"))
        {
            super.remove(meanlbl);
            super.remove(masterSpinner);
            super.remove(stdevlbl);
            super.remove(stdSpinner);
            super.remove(trendlbl);
            super.remove(trendSpinner);

            super.remove(autoOnOffCheckBox);
            super.remove(onlbl);
            super.remove(onSpinner);
            super.remove(offlbl);
            super.remove(offSpinner);

            if(controller.value == 0 || controller.value == 1)
            {
                super.add(timelbl, "split 4, gapx 35, width 20::");
                super.add(sendSpinner, "width 60::");
                super.add(threshlbl, "width 20::");
                super.add(threshSpinner, "width 60::");

                super.repaint();
            }
            else
            {
                super.add(timelbl, "split 4, width 20::");
                super.add(sendSpinner, "width 60::");
                super.add(threshlbl, "width 20::");
                super.add(threshSpinner, "width 60::");

                super.repaint();
            }
        }
        else if(mode.equals("Auto"))
        {
            super.remove(meanlbl);
            super.remove(masterSpinner);
            super.remove(stdevlbl);
            super.remove(stdSpinner);
            super.remove(trendlbl);
            super.remove(trendSpinner);

            super.remove(timelbl);
            super.remove(sendSpinner);
            super.remove(threshlbl);
            super.remove(threshSpinner);

            if(controller.value == 0 || controller.value == 1)
            {
                super.add(autoOnOffCheckBox, "split 5, gapx 50, width 30::");
                super.add(onlbl, "width 20::");
                super.add(onSpinner, "width 60::");
                super.add(offlbl, "width 30::");
                super.add(offSpinner, "width 60::");

                super.repaint();
            }
            else
            {
                super.add(autoOnOffCheckBox, "split 5,  width 20::");
                super.add(onlbl, "width 20::");
                super.add(onSpinner, "width 60::");
                super.add(offlbl, "width 30::");
                super.add(offSpinner, "width 60::");

                super.repaint();
            }
        }
    }

    public ComponentHandler getComponentHandler()
    {
        return componentHandler;
    }

    public  boolean isMasterOn()
    {
        return isMasterMode;
    }

    public void addListeners(List<Channel> channels)
    {
        //Master is a fair and just ruler and listens to its sub channels
        for(final Channel channel : channels)
        {
            channel.getOnOffbtn().addStateChangeListener(new StateChangeListener()
            {
                @Override
                public void disablePanel(StateChangeEvent event)
                {
                    //DO NOTHING
                }

                @Override
                public void activatePanel(StateChangeEvent event)
                {
                    //Master goes quite when servants want to talk
                    isMasterMode = false;
                    componentHandler.stop();
                    sendCheckBox.setSelected(false);
                    componentHandler.setAllowSend(sendCheckBox.isSelected());
                }
            });
        }
    }

    private String getLabel()
    {   String lbl = "";
        //Get text for panel :P
        switch(controller)
        {
            case TEMPERATURE:
                lbl = "Temperature Master:";
                break;
            case PRESSURE:
                lbl = "Pressure Master:";
                break;
            case SUPPLY:
                lbl = "Supply";
                break;
            case RETURN:
                lbl = "Return";
                break;
        }

        return lbl;
    }

    public void updateSelection()
    {
        List<DLComponent> newObjects = new ArrayList<>();

        //Get all objects of controller type
        switch(controller)
        {
            case TEMPERATURE:
                newObjects.addAll(componentModel.getComponentsInRole("temperaturecontrol"));
                break;
            case PRESSURE:
                newObjects.addAll(componentModel.getComponentsInRole("pressurecontrol"));
                break;
            case SUPPLY:
                //Skip down to RETURN
            case RETURN:
                List<DLComponent> allObjects = componentModel.getComponents();

                //Find all components that have the object type 'crac' guaranteeing that it will have a supplyT and returnT property
                for(DLComponent component : allObjects)
                {
                    ArrayList<DLObject> objects = component.getManagedObjects();

                    for(DLObject object : objects)
                    {
                        if(object.getType().equals("crac"))
                        {
                            newObjects.add(component);
                        }
                    }
                }
                break;
        }

        LinkedHashSet<DLComponent> incomingSelection = new LinkedHashSet<>();
        incomingSelection.addAll(newObjects);

        LinkedHashSet<DLComponent> currentlySelectedComponents = new LinkedHashSet<>(componentHandler.getComponents());

        //Add all new components to current list
        for (DLComponent i: incomingSelection)
        {
            if (!currentlySelectedComponents.contains(i)) //Master should take any node from any drawing
            {
                componentHandler.addObject(i);
            }
        }

        //Remove all old components that no longer exist
        for (DLComponent i: currentlySelectedComponents)
        {
            if (!incomingSelection.contains(i))
            {
                componentHandler.removeObject(i);
            }
        }
    }

    public List<String> settings()
    {
        List<String> settings = new ArrayList<>();

        //Save settings for master panel
        settings.add(componentHandler.getChannelName());
        settings.add("Mean:" + masterSpinner.getModel().getValue());
        settings.add("Standard Deviation:" + stdSpinner.getModel().getValue());
        settings.add("Trend:" + trendSpinner.getModel().getValue());
        settings.add("Send Delay:" + sendSpinner.getModel().getValue());
        settings.add("Threshold:" + threshSpinner.getModel().getValue());
        settings.add("Auto:" + autoOnOffCheckBox.isSelected());
        settings.add("Auto On Time:" + onSpinner.getModel().getValue());
        settings.add("Auto Off Time:" + offSpinner.getModel().getValue());

        return settings;
    }

    public void loadSettings(List<String> settings)
    {
        sendCheckBox.setSelected(false);

        //Turn sending mode off
        for(ActionListener actionListener : sendCheckBox.getActionListeners())
        {
            actionListener.actionPerformed(new ActionEvent(sendCheckBox,1,"force"));
        }

        //Load settings
        JSpinnerMaker.setText(masterSpinner,settings.get(1).split(":")[1],true);
        JSpinnerMaker.setText(stdSpinner,settings.get(2).split(":")[1],true);
        JSpinnerMaker.setText(trendSpinner,settings.get(3).split(":")[1],true);
        JSpinnerMaker.setText(sendSpinner,settings.get(4).split(":")[1],true);
        JSpinnerMaker.setText(threshSpinner,settings.get(5).split(":")[1],true);
        autoOnOffCheckBox.setSelected(Boolean.parseBoolean(settings.get(6).split(":")[1]));

        //update check box
        for(ActionListener actionListener : autoOnOffCheckBox.getActionListeners())
        {
            actionListener.actionPerformed(new ActionEvent(autoOnOffCheckBox,1,"force"));
        }

        //enable/disable spinners
        if(autoOnOffCheckBox.isSelected())
        {
            onSpinner.setEnabled(true);
            offSpinner.setEnabled(true);
        }
        else
        {
            onSpinner.setEnabled(false);
            offSpinner.setEnabled(false);
        }

        //Set values
        JSpinnerMaker.setText(onSpinner,settings.get(7).split(":")[1],true);
        JSpinnerMaker.setText(offSpinner,settings.get(8).split(":")[1],true);
    }

    @Override
    public void addStateChangedListener(stateChangedListener listener)
    {
        listeners.add(listener);
    }

    @Override
    public void turnMasterOn()
    {
        //Master wants to speak! Everyone listen
        isMasterMode = true;
        dispatchEvent();
    }

    private void dispatchEvent()
    {
        //Send out event to each listener
        final stateChangedEvent event = new stateChangedEvent(this);

        for (stateChangedListener l : listeners)
        {
            dispatchRunnableOnEventQueue(l, event);
        }
    }


    private void dispatchRunnableOnEventQueue(final stateChangedListener listener, final stateChangedEvent event)
    {
        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                listener.bowToMaster(event);
            }
        });
    }
}

//Necessary set up for creating a class that can fire an event to listeners
interface stateChangedDispatcher
{
    public void addStateChangedListener(stateChangedListener listener);
    public void turnMasterOn();
}

interface stateChangedListener extends EventListener
{
    public void bowToMaster(stateChangedEvent event);
}

class stateChangedEvent extends EventObject
{

    private final stateChangedDispatcher dispatcher;

    public stateChangedEvent(stateChangedDispatcher dispatcher)
    {
        super(dispatcher);
        this.dispatcher = dispatcher;
    }

    // type safe way to get source (as opposed to getSource of EventObject
    public stateChangedDispatcher getDispatcher()
    {
        return dispatcher;
    }
}
