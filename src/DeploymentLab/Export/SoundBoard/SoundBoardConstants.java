package DeploymentLab.Export.SoundBoard;

public final class SoundBoardConstants
{
    public static final int numTempChannels = 3;
    public static final int numPresChannels = 3;
    public static final int numSupChannels = 1;
    public static final int numRetChannels = 1;

    public static final int tempMeanStartVal = 80;
    public static final int tempMeanMinVal = 65;
    public static final int tempMeanMaxVal = 90;
    public static final int tempMeanStepVal = 1;

    public static final int tempStDevStartVal = 5;
    public static final int tempStDevMinVal = 1;
    public static final int tempStDevMaxVal = 10;
    public static final int tempStDevStepVal = 1;

    public static final int tempTrendStartVal = 0;
    public static final int tempTrendMinVal = -5;
    public static final int tempTrendMaxVal = 5;
    public static final int tempTrendStepVal = 1;

    public static final double presMeanStartVal = .055;
    public static final double presMeanMinVal = 0.0;
    public static final double presMeanMaxVal = .1;
    public static final double presMeanStepVal = .005;

    public static final double presStDevStartVal = .005;
    public static final double presStDevMinVal = .001;
    public static final double presStDevMaxVal = .01;
    public static final double presStDevStepVal = .001;

    public static final double presTrendStartVal = 0;
    public static final double presTrendMinVal = -.01;
    public static final double presTrendMaxVal = .01;
    public static final double presTrendStepVal = .001;

    public static final int supMeanStartVal = 65;
    public static final int supMeanMinVal = 65; //supply max and mins should be same as returns!!!!
    public static final int supMeanMaxVal = 90;
    public static final int supMeanStepVal = 1;

    public static final int supStDevStartVal = 5;
    public static final int supStDevMinVal = 1;
    public static final int supStDevMaxVal = 10;
    public static final int supStDevStepVal = 1;

    public static final int supTrendStartVal = 0;
    public static final int supTrendMinVal = -5;
    public static final int supTrendMaxVal = 5;
    public static final int supTrendStepVal = 1;

    public static final int retMeanStartVal = 85;
    public static final int retMeanMinVal = 65;
    public static final int retMeanMaxVal = 90;
    public static final int retMeanStepVal = 1;

    public static final int retStDevStartVal = 5;
    public static final int retStDevMinVal = 1;
    public static final int retStDevMaxVal = 10;
    public static final int retStDevStepVal = 1;

    public static final int retTrendStartVal = 0;
    public static final int retTrendMinVal = -5;
    public static final int retTrendMaxVal = 5;
    public static final int retTrendStepVal = 1;

    public static final int tempThreshStartVal = 2;
    public static final int tempThreshMinVal = 1;
    public static final int tempThreshMaxVal = tempTrendMaxVal;
    public static final int tempThreshStepVal = 1;

    public static final double presThreshStartVal = .005;
    public static final double presThreshMinVal = .001;
    public static final double presThreshMaxVal = presTrendMaxVal;
    public static final double presThreshStepVal = .001;

    public static final int sendTimeStartVal = 300000;
    public static final int sendTimeMinVal = 30000;
    public static final int sendTimeMaxVal = 600000;
    public static final int sendTimeStepVal = 15000;

    public static final int onTimeStartVal = 300000;
    public static final int onTimeMinVal = 30000;
    public static final int onTimeMaxVal = 1200000;
    public static final int onTimeStepVal = 15000;

    public static final int offTimeStartVal = 300000;
    public static final int offTimeMinVal = 30000;
    public static final int offTimeMaxVal = 1200000;
    public static final int offTimeStepVal = 15000;

    public static final int acPortNum = 9382;
    public static final int sizeQueue = 2000;

    public static final int channelWidth = 85;
    public static final int channelHeight = 280;

    public static final long numMillisInGraph = 1800000;
    public static final long graphUpdateDelay = 60000;
}
