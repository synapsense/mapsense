package DeploymentLab.Export.SoundBoard;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.EventObject;
import java.util.List;

public class RECbutton extends JButton implements RECStateChangeDispatcher
{
    //Used to easily get state of recording button
    public enum RECstate
    {
        OFF(0), REC(1);

        private int value;

        private RECstate(int value)
        {
            this.value = value;
        }
    }

    private RECstate _state = RECstate.OFF; //State defaults to off

    private List<RECStateChangeListener> _listeners;

    ImageIcon _redIcon;
    ImageIcon _blackIcon;

    public RECbutton(Color backColor)
    {
        super("REC");

        _listeners = new ArrayList<>();

        //Listen to button change
        super.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if(_state.equals(RECstate.OFF))
                {
                    setState(RECstate.REC);
                }
                else
                {
                    setState(RECstate.OFF);
                }
            }
        });

        //set icon color changes
        URL redicon = getClass().getResource("/resources/redlight.png");
        URL blackicon = getClass().getResource("/resources/blacklight.png");

        _redIcon = new ImageIcon(redicon);
        _blackIcon = new ImageIcon(blackicon);

        //Build button
        super.setForeground(Color.WHITE);
        super.setBackground(Color.DARK_GRAY);
        super.setIcon(_blackIcon);
        super.setFocusPainted(false);
        super.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,  backColor, Color.DARK_GRAY));
        super.setPreferredSize(new Dimension(55,20));
    }

    @Override
    public void addStateChangeListener(RECStateChangeListener listener)
    {
        _listeners.add(listener);
    }

    @Override
    public RECstate getState()
    {
        return _state;
    }

    @Override
    public void setState(RECstate state)
    {
        if (!_state.equals(state))
        {
            //Only fire event when state changes
            _state = state;

            switch(_state.value)
            {
                case 0:
                    super.setIcon(_blackIcon);
                    break;
                case 1:
                    super.setIcon(_redIcon);
                    break;
            }

            dispatchEvent();
        }
    }

    private void dispatchEvent()
    {
        final RECStateChangeEvent event = new RECStateChangeEvent(this);

        for (RECStateChangeListener l : _listeners)
        {
            dispatchRunnableOnEventQueue(l, event);
        }
    }


    private void dispatchRunnableOnEventQueue(final RECStateChangeListener listener, final RECStateChangeEvent event)
    {
        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                //Controls for user what will happen
                switch(_state.value)
                {
                    case 0:
                        listener.disableREC(event);
                        break;
                    case 1:
                        listener.activateREC(event);
                        break;
                }
            }
        });
    }
}

interface RECStateChangeDispatcher
{
    public void addStateChangeListener(RECStateChangeListener listener);
    public RECbutton.RECstate getState();
    public void setState(RECbutton.RECstate state);
}

interface RECStateChangeListener extends EventListener
{
    public void disableREC(RECStateChangeEvent event);
    public void activateREC(RECStateChangeEvent event);
}

class RECStateChangeEvent extends EventObject
{

    private final RECStateChangeDispatcher dispatcher;

    public RECStateChangeEvent(RECStateChangeDispatcher dispatcher)
    {
        super(dispatcher);
        this.dispatcher = dispatcher;
    }

    // type safe way to get source (as opposed to getSource of EventObject
    public RECStateChangeDispatcher getDispatcher()
    {
        return dispatcher;
    }
}
