package DeploymentLab.Export.SoundBoard

import net.miginfocom.swing.MigLayout

import javax.swing.ImageIcon
import javax.swing.JFileChooser
import javax.swing.JFrame
import javax.swing.JMenu
import javax.swing.JMenuBar
import javax.swing.JMenuItem
import javax.swing.JOptionPane
import javax.swing.JPanel
import javax.swing.event.MenuEvent
import javax.swing.event.MenuListener
import javax.swing.filechooser.FileNameExtensionFilter
import java.awt.Color
import java.awt.Dimension
import java.awt.GridBagConstraints
import java.awt.GridBagLayout
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.awt.event.MouseEvent
import java.awt.event.MouseMotionListener
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent

class SoundBoardGUI
{
    private JFrame frame

    public boolean isOn = true

    private JMenuBar menuBar
    private JMenu menu
    private JMenuItem saveAction
    private JMenuItem loadAction

    private Color backColor
    private Color valColor
    private Color btnColor

    List<Channel> controlPanels
    List<MasterPanel> masterPanels
    List<RestChannel> restPanels

    PlaybackChannel playbackChannel

    GraphPanel tempGraph
    GraphPanel presGraph
    GraphPanel supRestGraph

    public SoundBoardGUI()
    {
        //Starts up the Active Control Manager and the Active Control Listener
        ActiveControlManager.getInstance()

        menuBar = new JMenuBar()
        menu = new JMenu("Settings")
        menuBar.add(menu)

        saveAction = new JMenuItem("Save")
        loadAction = new JMenuItem("Load")

        menu.add(saveAction)
        menu.add(loadAction)

        //A way to easily store the panels used in this GUI
        masterPanels = new ArrayList<MasterPanel>()
        controlPanels = new ArrayList<Channel>()
        restPanels = new ArrayList<>()

        //Defining the color scheme to be used in the Sound Board
        float[] hsbback = Color.RGBtoHSB(84, 80, 88, null)
        float[] hsbval = Color.RGBtoHSB(73, 58, 90, null)
        float[] hsbbtn = Color.RGBtoHSB(57,79,77,null)

        valColor = Color.getHSBColor(hsbval[0],hsbval[1],hsbval[2])
        backColor = Color.getHSBColor(hsbback[0],hsbback[1],hsbback[2])
        btnColor = Color.getHSBColor(hsbbtn[0],hsbbtn[1],hsbbtn[2])

        //Set up recording system
        playbackChannel = PlaybackChannel.getInstance(backColor);

        //Create the two Master Panels (temperature and pressure)
        masterPanels.add(new MasterPanel("masterTemp", MasterPanel.Controller.TEMPERATURE,backColor,valColor));
        masterPanels.add(new MasterPanel("masterPres", MasterPanel.Controller.PRESSURE, backColor,valColor));
        masterPanels.add(new MasterPanel("masterSup", MasterPanel.Controller.SUPPLY, backColor,valColor));
        masterPanels.add(new MasterPanel("masterRet", MasterPanel.Controller.RETURN, backColor,valColor));

        //These two lists are need for the graphs to be able to get the data necessary to plot
        List<ComponentHandler> tempHandlers = new ArrayList<>();
        List<ComponentHandler> presHandlers = new ArrayList<>();
        List<ComponentHandler> supRetHandlers = new ArrayList<>();

        for(int i = 0; i < (SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + SoundBoardConstants.numSupChannels + SoundBoardConstants.numRetChannels); i++)
        {
            //Add temperature and pressure channels, the numbers specified in constants class
            if (i< SoundBoardConstants.numTempChannels)
            {
                if(i != 0)
                {
                    controlPanels.add(new Channel("temp" + Integer.toString(i+ 1),MasterPanel.Controller.TEMPERATURE, masterPanels[0], backColor, valColor, btnColor, false))
                    tempHandlers.add(controlPanels[i].getComponentHandler())
                }
                else
                {
                    controlPanels.add(new Channel("temp" + Integer.toString(i+ 1), MasterPanel.Controller.TEMPERATURE, masterPanels[0], backColor, valColor, btnColor, true))
                    tempHandlers.add(controlPanels[i].getComponentHandler())
                }
            }
            else if( i < SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels)
            {
                if(i != SoundBoardConstants.numTempChannels)
                {
                    controlPanels.add(new Channel("pres" + Integer.toString(i - SoundBoardConstants.numTempChannels  + 1), MasterPanel.Controller.PRESSURE, masterPanels[1],backColor,valColor,btnColor,false))
                    presHandlers.add(controlPanels[i].getComponentHandler())
                }
                else
                {
                    controlPanels.add(new Channel("pres" + Integer.toString(i - SoundBoardConstants.numTempChannels  + 1), MasterPanel.Controller.PRESSURE, masterPanels[1],backColor,valColor,btnColor,true))
                    presHandlers.add(controlPanels[i].getComponentHandler())
                }
            }
            else if(SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + SoundBoardConstants.numSupChannels)
            {
                if(i != SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels)
                {
                    controlPanels.add(new Channel("sup" + Integer.toString(i - SoundBoardConstants.numTempChannels  - SoundBoardConstants.numPresChannels + 1), MasterPanel.Controller.SUPPLY, masterPanels[1],backColor,valColor,btnColor,false))
                    supRetHandlers.add(controlPanels[i].getComponentHandler())
                }
                else
                {
                    controlPanels.add(new Channel("sup" + Integer.toString(i - SoundBoardConstants.numTempChannels - SoundBoardConstants.numPresChannels + 1), MasterPanel.Controller.SUPPLY, masterPanels[1],backColor,valColor,btnColor,true))
                    supRetHandlers.add(controlPanels[i].getComponentHandler())
                }
            }
            else
            {
                controlPanels.add(new Channel("ret" + Integer.toString(i - SoundBoardConstants.numTempChannels - SoundBoardConstants.numPresChannels - SoundBoardConstants.numSupChannels + 1), MasterPanel.Controller.RETURN, masterPanels[1],backColor,valColor,btnColor,false))
                supRetHandlers.add(controlPanels[i].getComponentHandler())
            }
        }

        //Create the rest panels
        restPanels.add(new RestChannel("tempRest", MasterPanel.Controller.TEMPERATURE, masterPanels[0], controlPanels.subList(0,SoundBoardConstants.numTempChannels), backColor,valColor,btnColor))
        restPanels.add(new RestChannel("presRest", MasterPanel.Controller.PRESSURE, masterPanels[1], controlPanels.subList(SoundBoardConstants.numTempChannels,(SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels)), backColor,valColor,btnColor))
        restPanels.add(new RestChannel("supRest", MasterPanel.Controller.SUPPLY, masterPanels[2], controlPanels.subList((SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels),(SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + SoundBoardConstants.numSupChannels)), backColor,valColor,btnColor))
        restPanels.add(new RestChannel("retRest", MasterPanel.Controller.RETURN, masterPanels[3], controlPanels.subList((SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + SoundBoardConstants.numSupChannels),(SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + SoundBoardConstants.numSupChannels + SoundBoardConstants.numRetChannels)), backColor,valColor,btnColor))

        masterPanels[0].addListeners(controlPanels.subList(0,SoundBoardConstants.numTempChannels));
        masterPanels[1].addListeners(controlPanels.subList(SoundBoardConstants.numTempChannels,(SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels)));
        masterPanels[2].addListeners(controlPanels.subList((SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels),(SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + SoundBoardConstants.numSupChannels)));
        masterPanels[3].addListeners(controlPanels.subList((SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + SoundBoardConstants.numSupChannels),(SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + SoundBoardConstants.numSupChannels + SoundBoardConstants.numRetChannels)));

        tempHandlers.add(restPanels[0].getComponentHandler())
        tempHandlers.add(masterPanels[0].getComponentHandler())

        presHandlers.add(restPanels[1].getComponentHandler())
        presHandlers.add(masterPanels[1].getComponentHandler())

        supRetHandlers.add(restPanels[2].getComponentHandler())
        supRetHandlers.add(restPanels[3].getComponentHandler())
        supRetHandlers.add(masterPanels[2].getComponentHandler())
        supRetHandlers.add(masterPanels[3].getComponentHandler())

        //Make sure each channel of similar controllers listens to each other
        for(Channel panel : controlPanels)
        {
            List<Channel> panelsToWatch = new ArrayList<>();

            for(Channel panel2 : controlPanels)
            {
                if(!panel.equals(panel2))
                {
                    if(panel.getController().equals(panel2.getController()))
                    {
                        panelsToWatch.add(panel2);
                    }
                }
            }

            panel.addListeners(panelsToWatch);
        }

        //Create Graph Panels assigning them componetHandlers to listen to
        tempGraph = new GraphPanel(MasterPanel.Controller.TEMPERATURE, backColor, valColor, tempHandlers);
        tempGraph.setPreferredSize(new Dimension(250, 300))
        presGraph = new GraphPanel(MasterPanel.Controller.PRESSURE, backColor, valColor, presHandlers);
        tempGraph.setPreferredSize(new Dimension(250, 300))
        supRestGraph = new GraphPanel(MasterPanel.Controller.RETURN, backColor, valColor, supRetHandlers);
        supRestGraph.setPreferredSize(new Dimension(250, 300))

        frame = new JFrame("Sound Board")

        frame.setLayout(new GridBagLayout())

        GridBagConstraints c = new GridBagConstraints()

        //Add all panels to the Sound Board
        c.gridx = 0
        c.gridy = 0
        c.gridwidth = SoundBoardConstants.numTempChannels + 1
        c.fill = GridBagConstraints.BOTH
        frame.add(masterPanels[0],c)

        c.gridx = SoundBoardConstants.numTempChannels + 1
        c.gridy = 0
        c.gridwidth = SoundBoardConstants.numPresChannels + 1
        frame.add(masterPanels[1],c)

        c.gridx = SoundBoardConstants.numTempChannels + 1 + SoundBoardConstants.numPresChannels + 1
        c.gridy = 0
        c.gridwidth = 2
        frame.add(masterPanels[2],c)

        c.gridx = SoundBoardConstants.numTempChannels + 1 + SoundBoardConstants.numPresChannels + 3
        c.gridy = 0
        c.gridwidth = 2
        frame.add(masterPanels[3],c)

        //Set these right now for all channels
        c.gridy = 1
        c.gridwidth = 1

        int panelIndex = 0;
        for(int x = 0; x < SoundBoardConstants.numTempChannels; x += 1)
        {
            c.gridx = x
            frame.add(controlPanels[panelIndex],c)
            panelIndex++
        }

        c.gridx = SoundBoardConstants.numTempChannels
        frame.add(restPanels[0],c)
       // c.gridheight = SoundBoardConstants.channelHeight

        for(int x = SoundBoardConstants.numTempChannels + 1; x < (SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + 1); x += 1)
        {
            c.gridx = x


            frame.add(controlPanels[panelIndex],c)
            panelIndex++
        }

        c.gridx = (SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + 1)
        frame.add(restPanels[1],c)

        for(int x = SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + 2; x < (SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + SoundBoardConstants.numSupChannels + 2); x += 1)
        {
            c.gridx = x

            frame.add(controlPanels[panelIndex],c)
            panelIndex++
        }

        c.gridx = (SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + SoundBoardConstants.numSupChannels + 2)
        frame.add(restPanels[2],c)

        for(int x = SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + SoundBoardConstants.numSupChannels + 3; x < (SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + SoundBoardConstants.numSupChannels + SoundBoardConstants.numRetChannels + 3); x += 1)
        {
            c.gridx = x

            frame.add(controlPanels[panelIndex],c)
            panelIndex++
        }

        c.gridx = (SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + SoundBoardConstants.numSupChannels + SoundBoardConstants.numRetChannels + 3)
        frame.add(restPanels[3],c)

        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = SoundBoardConstants.numTempChannels + 1;
        frame.add(tempGraph, c);

        c.gridx = SoundBoardConstants.numTempChannels + 1;
        c.gridy = 2;
        c.gridwidth = SoundBoardConstants.numPresChannels + 1;
        frame.add(presGraph, c);

        c.gridx = SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + 2;
        c.gridy = 2;
        c.gridwidth =  SoundBoardConstants.numSupChannels + SoundBoardConstants.numRetChannels + 2;
        frame.add(supRestGraph, c);

        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = SoundBoardConstants.numTempChannels + SoundBoardConstants.numPresChannels + SoundBoardConstants.numSupChannels + SoundBoardConstants.numRetChannels + 4;
        frame.add(playbackChannel, c)


        //Make the JFrame look pretty :P
        //Dimension dim = Toolkit.getDefaultToolkit().getScreenSize()
        //frame.setLocation((int)dim.width/2-frame.getSize().width/2, (int)dim.height/2-frame.getSize().height/2)

        //Set Frame Properties
        URL iconURL = getClass().getResource("/resources/soundBoardIcon.png")
        frame.setIconImage(new ImageIcon(iconURL).getImage())
        frame.setBackground(backColor)

        frame.addWindowListener(new WindowAdapter() {
            @Override
            void windowClosing(WindowEvent e)
            {
                //Shut down and close everything
                ActiveControlManager.getInstance().shutDown()

                for(MasterPanel masterPanel : masterPanels)
                {
                    masterPanel.getComponentHandler().stop()
                }

                for(Channel channel : controlPanels)
                {
                    channel.getComponentHandler().stop()
                }

                for(RestChannel restChannel : restPanels)
                {
                    restChannel.getComponentHandler().stop()
                }

                if(tempGraph.futureTask != null)
                {
                    tempGraph.futureTask.cancel(true)
                }

                if(presGraph.futureTask != null)
                {
                    presGraph.futureTask.cancel(true)
                }

                if(supRestGraph.futureTask != null)
                {
                    supRestGraph.futureTask.cancel(true)
                }

                PlaybackChannel.getInstance(null).shutdown()

                isOn = false
                super.windowClosing(e)
            }
        })

        frame.setJMenuBar(menuBar)

        saveAction.addActionListener(new ActionListener() {
            @Override
            void actionPerformed(ActionEvent e)
            {
                JFileChooser chooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter("*.txt", "txt");
                chooser.setFileFilter(filter);
                int returnVal = chooser.showOpenDialog(null);

                if (returnVal == JFileChooser.APPROVE_OPTION)
                {
                    String filePath = chooser.getSelectedFile().getAbsolutePath();
                    if(!filePath.contains(".txt"))
                    {
                        filePath += ".txt";
                    }

                    //Only turn on rec mode if file is valid type
                    FileWriter file = new FileWriter(filePath);
                    BufferedWriter writer = new BufferedWriter(file);

                    //Go through and get settings from each channel and write it out
                    for(MasterPanel masterPanel : masterPanels)
                    {
                        List<String> settings = masterPanel.settings();

                        for(String setting : settings)
                        {
                            writer.write(setting);
                            writer.newLine();
                            writer.flush();
                        }

                        writer.write("-------");
                        writer.newLine();
                        writer.flush();
                    }

                    for(Channel channel : controlPanels)
                    {
                        List<String> settings = channel.settings();

                        for(String setting : settings)
                        {
                            writer.write(setting);
                            writer.newLine();
                            writer.flush();
                        }

                        writer.write("-------");
                        writer.newLine();
                        writer.flush();
                    }

                    for(RestChannel restChannel : restPanels)
                    {
                        List<String> settings = restChannel.settings();

                        for(String setting : settings)
                        {
                            writer.write(setting);
                            writer.newLine();
                            writer.flush();
                        }

                        writer.write("-------");
                        writer.newLine();
                        writer.flush();
                    }

                    writer.close();
                    file.close();
                }
                else
                {

                }
            }
        })

        loadAction.addActionListener(new ActionListener() {
            @Override
            void actionPerformed(ActionEvent e)
            {
                int result = JOptionPane.showConfirmDialog( null, "All current settings will be lost","alert", JOptionPane.OK_CANCEL_OPTION);

                if(result == 0)
                {
                    JFileChooser chooser = new JFileChooser();
                    FileNameExtensionFilter filter = new FileNameExtensionFilter("*.txt", "txt");
                    chooser.setFileFilter(filter);
                    int returnVal = chooser.showOpenDialog(null);

                    if (returnVal == JFileChooser.APPROVE_OPTION)
                    {
                        FileReader file = new FileReader(chooser.getSelectedFile().getAbsolutePath());
                        BufferedReader reader = new BufferedReader(file);

                        String line;
                        List<String> settings = new ArrayList<>();

                        //Load all the data from the save file
                        while ((line = reader.readLine()) != null)
                        {
                            if(!line.equals("-------"))
                            {
                                //build settings list
                                settings.add(line);
                            }
                            else
                            {
                                //use settings list to build channels
                                for(MasterPanel masterPanel : masterPanels)
                                {
                                    if(settings.get(0).equals(masterPanel.getComponentHandler().getChannelName()))
                                    {
                                        masterPanel.loadSettings(settings)
                                        break;
                                    }
                                }

                                for(Channel channel : controlPanels)
                                {
                                    if(settings.get(0).equals(channel.getComponentHandler().getChannelName()))
                                    {
                                        channel.loadSettings(settings)
                                        break;
                                    }
                                }

                                for(RestChannel restChannel : restPanels)
                                {
                                    if(settings.get(0).equals(restChannel.getComponentHandler().getChannelName()))
                                    {
                                        restChannel.loadSettings(settings)
                                        break;
                                    }
                                }

                                settings.clear()
                            }
                        }

                        reader.close();
                    }
                }
            }
        })

        frame.pack()
        frame.setResizable(false)
        frame.setLocationRelativeTo(null);
        frame.setVisible(true)

        //Start Master Panels
        masterPanels[0].getComponentHandler().start();
        masterPanels[1].getComponentHandler().start();
        masterPanels[2].getComponentHandler().start();
        masterPanels[3].getComponentHandler().start();
    }
}
