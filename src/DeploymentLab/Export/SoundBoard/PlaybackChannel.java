package DeploymentLab.Export.SoundBoard;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

public class PlaybackChannel extends JPanel
{
    private static PlaybackChannel singletonPlaybackChannel = null;

    private RECbutton recbtn;
    private JButton clearbtn;
    private JButton savebtn;
    private JButton flagbtn;

    java.util.List<String> messages;

    private PlaybackChannel(Color backColor)
    {
        //Set layout
        super(new MigLayout("fillx", "[right]rel[grow,fill]", "[]10[]")); //Set panel type

        //Initialize buffer
        messages = new ArrayList<>();

        clearbtn = new JButton("Clear");
        clearbtn.setForeground(Color.WHITE);
        clearbtn.setBackground(Color.DARK_GRAY);
        clearbtn.setFocusPainted(false);
        clearbtn.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED, backColor, Color.DARK_GRAY));
        clearbtn.setPreferredSize(new Dimension(55, 20));
        clearbtn.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                //Warn user and then clear buffer
                int result = JOptionPane.showConfirmDialog( null, "All unsaved data will be lost","alert", JOptionPane.OK_CANCEL_OPTION);

                if(result == 0)
                {
                    clearMessages();
                }
            }
        });

        savebtn = new JButton("Save");
        savebtn.setForeground(Color.WHITE);
        savebtn.setBackground(Color.DARK_GRAY);
        savebtn.setFocusPainted(false);
        savebtn.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED, backColor, Color.DARK_GRAY));
        savebtn.setPreferredSize(new Dimension(55, 20));
        savebtn.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                JFileChooser chooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter("*.txt", "txt");
                chooser.setFileFilter(filter);
                int returnVal = chooser.showOpenDialog(null);

                //Save buffer to text file
                if (returnVal == JFileChooser.APPROVE_OPTION)
                {
                    try
                    {
                        String filePath = chooser.getSelectedFile().getAbsolutePath();
                        if(!filePath.contains(".txt"))
                        {
                            filePath += ".txt";
                        }

                        //Only turn on rec mode if file is valid type
                        FileWriter file = new FileWriter(filePath);
                        BufferedWriter writer = new BufferedWriter(file);

                        List<String> messagesToWrite = getMessages();

                        for(String message : messagesToWrite)
                        {
                            //write lines to text
                            writer.write(message);
                            writer.newLine();
                            writer.flush();
                        }

                        file.flush();
                        writer.close();
                        file.close();
                    }
                    catch (IOException ex)
                    {
                        ex.printStackTrace();
                    }
                }
            }
        });

        flagbtn = new JButton("Flag");
        flagbtn.setForeground(Color.WHITE);
        flagbtn.setBackground(Color.DARK_GRAY);
        flagbtn.setFocusPainted(false);
        flagbtn.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED, backColor, Color.DARK_GRAY));
        flagbtn.setPreferredSize(new Dimension(55, 20));
        flagbtn.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                String s = (String)JOptionPane.showInputDialog(null,"Flag Message:\n","Add Flag",JOptionPane.PLAIN_MESSAGE,null,null,"flag");

                if(s != null && s.length() > 0)
                {
                    //Let user add custom flag message to recording
                    addMessage(s);
                }
            }
        });

        //Setup record button
        recbtn = new RECbutton(backColor);
        recbtn.addStateChangeListener(new RECStateChangeListener() {
            @Override
            public void disableREC(RECStateChangeEvent event)
            {
                flagbtn.setEnabled(false);
            }

            @Override
            public void activateREC(RECStateChangeEvent event)
            {
                flagbtn.setEnabled(true);
            }
        });
        recbtn.setState(RECbutton.RECstate.REC);

        //add buttons
        super.setBackground(backColor);
        super.setPreferredSize(new Dimension(SoundBoardConstants.channelWidth * 12, 30));
        super.add(recbtn, "split 4, align right");
        super.add(savebtn);
        super.add(clearbtn);
        super.add(flagbtn);
        super.setPreferredSize(new Dimension(SoundBoardConstants.channelWidth * 12, 30));
    }

    //make sure there is only one recorder
    public static PlaybackChannel getInstance(Color backColor)
    {
        if(singletonPlaybackChannel == null)
        {
            singletonPlaybackChannel = new PlaybackChannel(backColor);
        }

        return singletonPlaybackChannel;
    }

    public RECbutton getRECButton()
    {
        return recbtn;
    }

    public synchronized void shutdown()
    {
        clearMessages();
        singletonPlaybackChannel = null;
    }

    public synchronized void clearMessages()
    {
        messages.clear();
    }

    public synchronized void addDataMessage(String message)
    {
        //Create a data message and add to buffer
        String timestamp = new SimpleDateFormat("HH:mm:ss.SSS").format(new Date());

        long time = (System.nanoTime());

        long delta = 0;

        List<String> tempMessage = getMessages();

        for(int i = tempMessage.size() - 1; i >= 0; i--)
        {
            if(tempMessage.get(i).contains("data;"))
            {
                //find previous time to find delta to get a relative time rather than absolute time
                long oldTime = Long.parseLong(tempMessage.get(i).split("data;")[1].split("delta;")[0].replace(" ",""));

                delta = (time - oldTime) / 1000000;
                break;
            }
        }

        //Add data message
        messages.add(timestamp + " data;" + (time) + " delta;" + delta + " " + message);
    }

    public synchronized void removeMessage(int index)
    {
        try
        {
            //Remove message
            messages.remove(index);
        }
        catch(IndexOutOfBoundsException ex)
        {
            ex.printStackTrace();
        }
    }

    public synchronized void addMessage(String message)
    {
        //Add reqular flag/message to buffer
        String timestamp = new SimpleDateFormat("HH:mm:ss.SSS").format(new Date());
        messages.add(timestamp + " " + message);
    }

    public synchronized List<String> getMessages()
    {
        return new ArrayList<>(messages);
    }

    public synchronized boolean allowREC()
    {
        //Return if record is allowed
        return recbtn.getState().equals(RECbutton.RECstate.REC) && ActiveControlManager.getInstance().getClients().size() != 0;
    }
}
