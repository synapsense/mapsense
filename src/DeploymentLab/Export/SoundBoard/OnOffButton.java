package DeploymentLab.Export.SoundBoard;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.EventObject;
import java.util.List;

public class OnOffButton extends JButton implements StateChangeDispatcher
{
    //A way to easily keep track whether button is on or off
    public enum State
    {
        ON(1), OFF(0);

        private int value;

        private State(int value)
        {
            this.value = value;
        }
    }

    private State _state = State.ON; //State will be set to off in Constructor, but causes a state change to be fired

    private List<StateChangeListener> _listeners;

    ImageIcon _redIcon;
    ImageIcon _greenIcon;

    public OnOffButton(Color backColor)
    {
        super("On/Off");

        _listeners = new ArrayList<>();

        //Listen to the button
        super.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if(_state.equals(State.OFF))
                {
                    setState(State.ON);
                }
                else
                {
                    setState(State.OFF);
                }
            }
        });

        //set icons so they can change on and off
        URL redicon = getClass().getResource("/resources/redlight.png");
        URL greenicon = getClass().getResource("/resources/greenlight.png");

        _redIcon = new ImageIcon(redicon);
        _greenIcon = new ImageIcon(greenicon);

        //Build button
        super.setForeground(Color.WHITE);
        super.setBackground(Color.DARK_GRAY);
        super.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,  backColor, Color.DARK_GRAY));
        super.setFocusPainted(false);
        super.setPreferredSize(new Dimension(55,10));

        //Forces an event fire
        setState(State.OFF);
    }

    @Override
    public void addStateChangeListener(StateChangeListener listener)
    {
        _listeners.add(listener);
    }

    public List<StateChangeListener> getStateChangeListener()
    {
        return _listeners;
    }

    @Override
    public State getState()
    {
        return _state;
    }

    @Override
    public void setState(State state)
    {
        //If the state changes, change icon and fire an event
        if (!_state.equals(state))
        {
            _state = state;

            switch(_state.value)
            {
                case 0:
                    super.setIcon(_redIcon);
                    break;
                case 1:
                    super.setIcon(_greenIcon);
                    break;
            }

            dispatchEvent();
        }
    }

    public void setWithoutEvent(State state)
    {
        //set values state without firing an event
        if (!_state.equals(state))
        {
            _state = state;

            switch(_state.value)
            {
                case 0:
                    super.setIcon(_redIcon);
                    break;
                case 1:
                    super.setIcon(_greenIcon);
                    break;
            }
        }
    }

    protected void forceFire()
    {
        dispatchEvent();
    }

    private void dispatchEvent()
    {
        final StateChangeEvent event = new StateChangeEvent(this);

        for (StateChangeListener l : _listeners)
        {
            dispatchRunnableOnEventQueue(l, event);
        }
    }


    private void dispatchRunnableOnEventQueue(final StateChangeListener listener, final StateChangeEvent event)
    {
        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                //Useful for user to control what happens during a state change
                switch(_state.value)
                {
                    case 0:
                        listener.disablePanel(event);
                        break;
                    case 1:
                        listener.activatePanel(event);
                        break;
                }
            }
        });
    }

}

interface StateChangeDispatcher
{
    public void addStateChangeListener(StateChangeListener listener);
    public OnOffButton.State getState();
    public void setState(OnOffButton.State state);
}

interface StateChangeListener extends EventListener
{
    public void disablePanel(StateChangeEvent event);
    public void activatePanel(StateChangeEvent event);
}

class StateChangeEvent extends EventObject
{

    private final StateChangeDispatcher dispatcher;

    public StateChangeEvent(StateChangeDispatcher dispatcher)
    {
        super(dispatcher);
        this.dispatcher = dispatcher;
    }

    // type safe way to get source (as opposed to getSource of EventObject
    public StateChangeDispatcher getDispatcher()
    {
        return dispatcher;
    }
}