package DeploymentLab.Export.SoundBoard;

import org.hornetq.utils.json.JSONException;
import org.hornetq.utils.json.JSONObject;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ActiveControlManager extends Thread
{
    private static ActiveControlManager singletonActiveControlManager;

    private static List<String> SHUTDOWN_REQ = new ArrayList<>();
    private volatile boolean shuttingDown, acManagerTerminated;

    private List<Socket> clients;
    private BlockingQueue<List<String>> queue = new ArrayBlockingQueue<List<String>>(SoundBoardConstants.sizeQueue);

    private ActiveControlManager()
    {
        super();
        //This entire class is its own separate thread from the rest of the program
        SHUTDOWN_REQ.add("shutdown");
        clients = new ArrayList<>();

        //Start the listener to accept clients
        ActiveControlListener.getInstance();
        start();
    }

    //Make sure only one instance of Active Control Manager exists
    public static ActiveControlManager getInstance() {
        if (singletonActiveControlManager == null) {
            singletonActiveControlManager = new ActiveControlManager();
        }

        return singletonActiveControlManager;
    }

    public void run()
    {
        try
        {
            List<String> item;
            //Waits for items to be added to queue and then process the data
            while (!(item = queue.take()).equals(SHUTDOWN_REQ) && !Thread.currentThread().isInterrupted())
            {
                JSONObject json = new JSONObject();
                try
                {
                    //Message to be sent to Active Control
                    json.put("type", "WSN_SENSOR_DATA");
                    json.put("to", item.get(0));
                    json.put("DATA", Double.parseDouble(item.get(1)));
                    json.put("TIMESTAMP", System.currentTimeMillis());

                    List<Socket> clientsToMessage = this.getClients();

                    boolean firstTime = true;

                    //Send message to every client connected to port number
                    for(Socket client : clientsToMessage)
                    {
                        try
                        {
                            //Write stream
                            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
                            out.write(json.toString());
                            out.write("\r\n");
                            out.flush();

                            //Ensures data only gets written to file once in case there are multiple clients
                            if (PlaybackChannel.getInstance(null).getRECButton().getState() == RECbutton.RECstate.REC && firstTime)
                        {
                            //Write recording data if in record mode
                            PlaybackChannel.getInstance(null).addDataMessage(item.get(0) + "><" + item.get(1));
                            firstTime = false;
                        }

                        }
                        catch (IOException ex)
                        {
                            //If client throws error, remove client (assume client was disconnected)
                            this.removeClient(client);
                        }
                    }

                    //System.out.println(json.toString());
                }
                catch (JSONException ex)
                {
                    ex.printStackTrace();
                }
            }
        }
        catch (InterruptedException iex)
        {

        }
        finally
        {
            acManagerTerminated = true;
        }
    }

    public void send(List<String> data)
    {
        if (shuttingDown || acManagerTerminated)
        {
            return;
        }

        try
        {
            //puts the data on the queue
            queue.put(data);
        }
        catch (InterruptedException iex)
        {
            try
            {
                Thread.currentThread().interrupt();
                throw new RuntimeException("Unexpected interruption");
            }
            catch (RuntimeException ex)
            {
                ex.printStackTrace();
            }
        }
    }

    public synchronized void shutDown() throws InterruptedException
    {
        //Clean up everything and shutdown socket connections
        shuttingDown = true;
        queue.clear();

        ActiveControlListener.getInstance().shutdownListener();

        for(Socket client : clients)
        {
            try
            {
                client.close();
            }
            catch(IOException ex)
            {
                //Doesn't need any log because im removing all connections anyways
            }
        }

        clients.clear();
        interrupt();

        //Discard listener
        singletonActiveControlManager = null;
    }


    //A way to control clients being added, removed, and obtained safely in a thread
    public synchronized void addClient(Socket client)
    {
        this.clients.add(client);
    }

    public synchronized List<Socket> getClients()
    {
        return new ArrayList<>(clients);
    }

    public synchronized void removeClient(Socket client)
    {
        this.clients.remove(client);
    }
}

class ActiveControlListener extends Thread
{
    private static ActiveControlListener singletonActiveControlListener;

    private boolean stayAlive = true;

    ServerSocket serverSocket;

    private ActiveControlListener()
    {
        try
        {
            //A thread designed to listen for clients that want to connect
            serverSocket = new ServerSocket(SoundBoardConstants.acPortNum);
            start();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

    public static ActiveControlListener getInstance()
    {
        if(singletonActiveControlListener == null)
        {
            singletonActiveControlListener = new ActiveControlListener();
        }

        return singletonActiveControlListener;
    }

    public void run()
    {
        while(stayAlive &&  !Thread.currentThread().isInterrupted() && !serverSocket.isClosed())
        {
            try
            {
                //Continually allowing active control to connect to port
                if(!serverSocket.isClosed())
                {
                    try
                    {
                        Socket socket = serverSocket.accept();
                        ActiveControlManager.getInstance().addClient(socket);
                    }
                    catch (SocketException ex)
                    {
                        //During shutdown it will throw an error... That is expected
                        ex.printStackTrace();
                    }
                }
            }
            catch(IOException ex)
            {
                ex.printStackTrace();
            }
        }
    }

    public synchronized void shutdownListener()
    {
        //Clean up and shutdown
        stayAlive = false;

        interrupt();

        try
        {
            serverSocket.close();
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }

        //Discard listener
        singletonActiveControlListener = null;
    }
}
