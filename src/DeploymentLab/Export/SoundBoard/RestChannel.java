package DeploymentLab.Export.SoundBoard;

import DeploymentLab.CentralCatalogue;
import DeploymentLab.Model.ComponentModel;
import DeploymentLab.Model.DLComponent;
import DeploymentLab.Model.DLObject;
import DeploymentLab.SelectModel;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

public class RestChannel extends JPanel
{
    JLabel restlbl;
    JButton viewRestbtn;
    JSpinner contSpinner;
    OnOffButton onOffbtn;
    JSpinner stdSpinner;
    JSpinner trendSpinner;
    JSpinner sendSpinner;
    JSpinner threshSpinner;
    JCheckBox sendCheckBox;
    JSpinner onSpinner;
    JSpinner offSpinner;
    JCheckBox autoOnOffCheckBox;

    JComboBox modeSelection;

    boolean autoOnOff = false;

    MasterPanel.Controller controller;
    SelectModel selectModel;
    ComponentModel componentModel;
    ComponentHandler componentHandler;

    List<Channel> channels;

    public RestChannel(String channelName, MasterPanel.Controller cont, final MasterPanel masterPanel, final List<Channel> channels, Color backColor, Color valColor, Color btnColor)
    {
        //Initiliaze everything
        super(new MigLayout("fillx", "[right]rel[grow,fill]", "[]10[]"));

        controller = cont;
        componentHandler = new ComponentHandler(controller, channelName);
        selectModel = CentralCatalogue.getSelectModel();
        componentModel = CentralCatalogue.getOpenProject().getComponentModel();
        this.channels = channels;

        String[] modes = {"Value", "Send", "Auto"};

        modeSelection = new JComboBox<>(modes);

        //Create the spinners
        List<Component> spinners = JSpinnerMaker.generateSpinners(controller,componentHandler);

        contSpinner = (JSpinner)spinners.get(0);
        stdSpinner = (JSpinner)spinners.get(1);
        trendSpinner = (JSpinner)spinners.get(2);

        sendSpinner = (JSpinner)spinners.get(3);
        sendSpinner.setPreferredSize(new Dimension(30,20));

        threshSpinner = (JSpinner)spinners.get(4);
        sendSpinner.setPreferredSize(new Dimension(30,20));

        onSpinner = (JSpinner)spinners.get(5);
        onSpinner.setPreferredSize(new Dimension(30,20));
        onSpinner.setEnabled(false);

        offSpinner = (JSpinner)spinners.get(6);
        offSpinner.setPreferredSize(new Dimension(30,20));
        offSpinner.setEnabled(false);

        sendCheckBox = new JCheckBox("Send");
        sendCheckBox.setForeground(Color.WHITE);
        sendCheckBox.setHorizontalAlignment(SwingConstants.CENTER);
        sendCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                componentHandler.setAllowSend(sendCheckBox.isSelected());
            }
        });

        autoOnOffCheckBox = new JCheckBox("Auto");
        autoOnOffCheckBox.setForeground(Color.WHITE);
        autoOnOffCheckBox.setHorizontalAlignment(SwingConstants.CENTER);

        autoOnOffCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if(autoOnOffCheckBox.isSelected())
                {
                    autoOnOff = true;
                    onSpinner.setEnabled(true);
                    offSpinner.setEnabled(true);
                    componentHandler.setAuto(true);

                    componentHandler.start();
                }
                else
                {
                    autoOnOff = false;
                    onSpinner.setEnabled(false);
                    offSpinner.setEnabled(false);
                    componentHandler.setAuto(false);

                    componentHandler.start();
                }
            }
        });

        //Rest channel doesn't have select button
        restlbl = new JLabel("Rest");
        restlbl.setForeground(Color.WHITE);
        restlbl.setHorizontalAlignment(SwingConstants.CENTER);

        //View what is selected
        viewRestbtn = new JButton("View");

        viewRestbtn.setBackground(btnColor);
        viewRestbtn.setForeground(Color.WHITE);
        viewRestbtn.setPreferredSize(new Dimension(50,10));

        viewRestbtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                viewSelection();
            }
        });

        //Create on off Button
        onOffbtn = new OnOffButton(backColor);
        onOffbtn.addStateChangeListener(new StateChangeListener() {
            @Override
            public void disablePanel(StateChangeEvent event)
            {
                //Turn off everything and release all nodes
                componentHandler.stop();
                viewRestbtn.setEnabled(false);
                JSpinnerMaker.setText(contSpinner, "",false);
                contSpinner.setEnabled(false);
                JSpinnerMaker.setText(stdSpinner, "",false);
                stdSpinner.setEnabled(false);
                JSpinnerMaker.setText(trendSpinner, "",false);
                trendSpinner.setEnabled(false);
                modeSelection.setEnabled(false);
                sendSpinner.setEnabled(false);
                threshSpinner.setEnabled(false);

                sendCheckBox.setSelected(false);
                componentHandler.setAllowSend(sendCheckBox.isSelected());

                sendCheckBox.setEnabled(false);
                autoOnOffCheckBox.setEnabled(false);
                onSpinner.setEnabled(false);
                offSpinner.setEnabled(false);

                releaseFromRest(componentHandler.getComponents());
            }

            @Override
            public void activatePanel(StateChangeEvent event)
            {
                //only turn rest on if master mode is off...
                boolean turnON = !masterPanel.isMasterOn();

                if(turnON)
                {
                    //Turn everything on and get all unclaimed nodes
                    viewRestbtn.setEnabled(true);
                    JSpinnerMaker.setText(contSpinner, "" + componentHandler.getMeanValue(),false);
                    contSpinner.setEnabled(true);
                    JSpinnerMaker.setText(stdSpinner, "" + componentHandler.getStandardDeviation(),false);
                    stdSpinner.setEnabled(true);
                    JSpinnerMaker.setText(trendSpinner, "" + componentHandler.getTrend(),false);
                    trendSpinner.setEnabled(true);
                    modeSelection.setEnabled(true);
                    sendSpinner.setEnabled(true);
                    threshSpinner.setEnabled(true);
                    sendCheckBox.setEnabled(true);
                    autoOnOffCheckBox.setEnabled(true);

                    if(autoOnOff)
                    {
                        onSpinner.setEnabled(true);
                        offSpinner.setEnabled(true);
                    }

                    populateRest();
                    componentHandler.start();
                }
                else
                {
                    //Still in master mode so dont turn rest on
                    onOffbtn.setState(OnOffButton.State.OFF);
                }
            }
        });

        //Listen to master
        masterPanel.addStateChangedListener(new stateChangedListener() {
            @Override
            public void bowToMaster(stateChangedEvent event)
            {
                onOffbtn.setState(OnOffButton.State.OFF);
            }
        });

        //Listen to other channels
        for(final Channel channel : this.channels)
        {
            channel.addselectionChangeListener(new selectionChangeListener() {
                @Override
                public void objectsClaimed(selectionChangeEvent event)
                {
                    //Release nodes to other channels
                    if(onOffbtn.getState().equals(OnOffButton.State.ON))
                    {
                        releaseFromRest(channel.getAddedObjs());

                        if (channel.getRemovedObjs().size() == 0)
                        {
                            componentHandler.start();
                        }
                    }
                }

                @Override
                public void objectsReleased(selectionChangeEvent event)
                {
                    //Claim unwanted nodes
                    if(onOffbtn.getState().equals(OnOffButton.State.ON))
                    {
                        addToRest(channel.getRemovedObjs());
                        componentHandler.start();
                    }
                }
            });
        }

        //Force things to start off
        onOffbtn.forceFire();

        //Add everything to panel
        super.setPreferredSize(new Dimension(SoundBoardConstants.channelWidth,SoundBoardConstants.channelHeight));
        super.add(onOffbtn, "align center, width 65::, height 30::, wrap");
        super.add(restlbl, "align center, width 65::, height 30::, wrap");
        super.add(viewRestbtn, " align center, width 65::, height 30::, wrap");
        super.add(sendCheckBox, "align center, width 65::, height 20::, wrap");
        super.add(modeSelection, "align center, width 65::, height 20::, wrap");
        super.add(contSpinner, "align center, width 65::, height 20::, wrap");
        super.add(stdSpinner, "align center, width 65::, height 20::, wrap");
        super.add(trendSpinner, "align center, width 65::, height 20::, wrap");

        modeSelection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                modeSelection.setPreferredSize(new Dimension(65,20));
                updateMenu();
            }
        });

        super.setBackground(backColor);
    }

    public void updateMenu()
    {
        //Update UI when mode changes
        String mode = (String)modeSelection.getSelectedItem();

        if(mode.equals("Value"))
        {
            super.remove(sendSpinner);
            super.remove(threshSpinner);

            super.remove(autoOnOffCheckBox);
            super.remove(onSpinner);
            super.remove(offSpinner);

            super.add(contSpinner, "align center, width 65::, height 20::, wrap");
            super.add(stdSpinner, "align center, width 65::, height 20::, wrap");
            super.add(trendSpinner, "align center, width 65::, height 20::, wrap");

            super.repaint();
        }
        else if(mode.equals("Send"))
        {
            super.remove(contSpinner);
            super.remove(stdSpinner);
            super.remove(trendSpinner);

            super.remove(autoOnOffCheckBox);
            super.remove(onSpinner);
            super.remove(offSpinner);

            super.add(sendSpinner, "align center, width 65::, height 20::, wrap");
            super.add(threshSpinner, "align center, width 65::, height 20::, wrap");

            super.repaint();
        }
        else if(mode.equals("Auto"))
        {
            super.remove(contSpinner);
            super.remove(stdSpinner);
            super.remove(trendSpinner);

            super.remove(sendSpinner);
            super.remove(threshSpinner);

            super.add(autoOnOffCheckBox, "align center, width 65::, height 20::, wrap");
            super.add(onSpinner, "align center, width 65::, height 20::, wrap");
            super.add(offSpinner, "align center, width 65::, height 20::, wrap");

            super.repaint();
        }
    }

    public List<String> settings()
    {
        List<String> settings = new ArrayList<>();

        //Save Settings
        settings.add(componentHandler.getChannelName());
        settings.add("Channel On:" + isOn());
        settings.add("Mean:" + contSpinner.getModel().getValue());
        settings.add("Standard Deviation:" + stdSpinner.getModel().getValue());
        settings.add("Trend:" + trendSpinner.getModel().getValue());
        settings.add("Send Delay:" + sendSpinner.getModel().getValue());
        settings.add("Threshold:" + threshSpinner.getModel().getValue());
        settings.add("Auto:" + autoOnOffCheckBox.isSelected());
        settings.add("Auto On Time:" + onSpinner.getModel().getValue());
        settings.add("Auto Off Time:" + offSpinner.getModel().getValue());

        return settings;
    }

    public void loadSettings(List<String> settings)
    {
        sendCheckBox.setSelected(false);

        //Start by turning everything on
        for(ActionListener actionListener : sendCheckBox.getActionListeners())
        {
            actionListener.actionPerformed(new ActionEvent(sendCheckBox,1,"force"));
        }

        for(StateChangeListener stateChangeListener : onOffbtn.getStateChangeListener())
        {
            stateChangeListener.disablePanel(new StateChangeEvent(onOffbtn));
            onOffbtn.setWithoutEvent(OnOffButton.State.OFF);
        }

        //Load settings
        JSpinnerMaker.setText(contSpinner,settings.get(2).split(":")[1],true);
        JSpinnerMaker.setText(stdSpinner,settings.get(3).split(":")[1],true);
        JSpinnerMaker.setText(trendSpinner,settings.get(4).split(":")[1],true);
        JSpinnerMaker.setText(sendSpinner,settings.get(5).split(":")[1],true);
        JSpinnerMaker.setText(threshSpinner,settings.get(6).split(":")[1],true);
        autoOnOffCheckBox.setSelected(Boolean.parseBoolean(settings.get(7).split(":")[1]));

        for(ActionListener actionListener : autoOnOffCheckBox.getActionListeners())
        {
            actionListener.actionPerformed(new ActionEvent(autoOnOffCheckBox,1,"force"));
        }

        //Fix settings
        if(autoOnOffCheckBox.isSelected())
        {
            onSpinner.setEnabled(true);
            offSpinner.setEnabled(true);
        }
        else
        {
            onSpinner.setEnabled(false);
            offSpinner.setEnabled(false);
        }

        JSpinnerMaker.setText(onSpinner,settings.get(8).split(":")[1],true);
        JSpinnerMaker.setText(offSpinner,settings.get(9).split(":")[1],true);


        //If channel is supposed to be on, turn on
        if(Boolean.parseBoolean(settings.get(1).split(":")[1]))
        {
            for(StateChangeListener stateChangeListener : onOffbtn.getStateChangeListener())
            {
                stateChangeListener.activatePanel(new StateChangeEvent(onOffbtn));
                onOffbtn.setWithoutEvent(OnOffButton.State.ON);
            }
        }
    }

    public boolean isOn()
    {
        return onOffbtn.getState() == OnOffButton.State.ON;
    }

    public ComponentHandler getComponentHandler()
    {
        return componentHandler;
    }

    private void populateRest()
    {
        switch(controller)
        {
            case TEMPERATURE:
                addToRest(componentModel.getComponentsInRole("temperaturecontrol"));
                break;
            case PRESSURE:
                addToRest(componentModel.getComponentsInRole("pressurecontrol"));
                break;
            case SUPPLY:
                //Skip down
            case RETURN:
                //Find all objects with the object crac to guarantee it has supplyT and returnT
                List<DLComponent> newObjects = new ArrayList<>();
                List<DLComponent> allObjects = componentModel.getComponents();

                for(DLComponent component : allObjects)
                {
                    ArrayList<DLObject> objects = component.getManagedObjects();

                    for(DLObject object : objects)
                    {
                        if(object.getType().equals("crac"))
                        {
                            newObjects.add(component);
                        }
                    }
                }

                //Add objects
                addToRest(newObjects);
                break;
        }

        for(Channel channel : channels)
        {
            if(channel.isOn())
            {
                releaseFromRest(channel.getObjs());
            }
        }
    }

    private void addToRest(List<DLComponent> objs)
    {
        LinkedHashSet<DLComponent> incomingSelection = new LinkedHashSet<>();
        incomingSelection.addAll(objs);

        LinkedHashSet<DLComponent> currentlySelectedComponents = new LinkedHashSet<>(componentHandler.getComponents());

        //Add all new objects to list
        for (DLComponent i: incomingSelection)
        {
            if (!currentlySelectedComponents.contains(i) && i.getDrawing().equals(selectModel.getActiveDrawing()) )
            {
                componentHandler.addObject(i);
            }
        }
    }

    private void releaseFromRest(List<DLComponent> objs)
    {
        LinkedHashSet<DLComponent> incomingSelection = new LinkedHashSet<>();
        incomingSelection.addAll(objs);

        LinkedHashSet<DLComponent> currentlySelectedComponents = new LinkedHashSet<>(componentHandler.getComponents());

        //Remove said objs from list
        for (DLComponent i: incomingSelection)
        {
            if (currentlySelectedComponents.contains(i))
            {
                componentHandler.removeObject(i);
            }
        }
    }

    private void viewSelection()
    {
        selectModel.setSelection(componentHandler.getComponents());
    }
}
