
package DeploymentLab.Exceptions

public class DuplicateObjectException extends Exception {
	private static final long serialVersionUID = 1L;
	public DuplicateObjectException(String _msg) {
		super(_msg)
	}
}

public class IncompatibleTypeException extends Exception {
	private static final long serialVersionUID = 1L;
	public IncompatibleTypeException(String _msg) {
		super(_msg)
        }
}

public class DeserializationException extends Exception {
	private static final long serialVersionUID = 1L;
	public DeserializationException(String _msg) {
		super(_msg)
        }
}

public class UnknownPropertyTypeException extends Exception {
	private static final long serialVersionUID = 1L;
	public UnknownPropertyTypeException(String _msg) {
		super(_msg)
        }
}

public class ConversionNotSupportedException extends Exception {
	private static final long serialVersionUID = 1L;
	ConversionNotSupportedException(def from, String toType) {
		super("Cannot convert from '" + from.class.getName() + "' to '$toType'!")
	}
}

public class OperationCancelledException extends Exception {
	private static final long serialVersionUID = 1L;
	public OperationCancelledException(String _msg) {
		super(_msg)
        }
}
