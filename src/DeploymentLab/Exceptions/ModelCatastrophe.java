package DeploymentLab.Exceptions;

/**
 * Thrown when the component or object models enters a state that is unrecoverable and the app should just die.
 *
 * A ModelCatastrophe should propagate all the way out to main where it terminates the app.
 * This exception should never be caught; if there is something you can do in a catch block,
 * then it is not a Model Catastrophe.
 *
 * Because of this, ModelCatastrophe is an Error, even though this *kind of* violates the guidelines for
 * Exception vs. RuntimeException vs. Error.
 *
 * @author Gabriel Helman
 * @since Mars
 * Date: 9/21/11
 * Time: 10:04 AM
 */
public class ModelCatastrophe extends Error {
	private static final long serialVersionUID = 1L;

	public ModelCatastrophe() {
		super();
	}

	public ModelCatastrophe(String message) {
		super(message);
	}

	public ModelCatastrophe(String message, Throwable cause) {
		super(message, cause);
	}

	public ModelCatastrophe(Throwable cause) {
		super(cause);
	}
}
