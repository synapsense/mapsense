package DeploymentLab.Exceptions;

/**
 * Thrown when something is fundamentally wrong with th eproject file and it can't be opened or saved.
 *
 * @author Ken Scoggins
 * @since Jupiter
 */
public class ProjectFileCatastrophe extends RuntimeException {
	private static final long serialVersionUID = 1L;

    public ProjectFileCatastrophe() {
        super();
    }

    public ProjectFileCatastrophe(String message) {
        super(message);
    }

    public ProjectFileCatastrophe(String message, Throwable cause) {
        super(message, cause);
    }

    public ProjectFileCatastrophe(Throwable cause) {
        super(cause);
    }
}
