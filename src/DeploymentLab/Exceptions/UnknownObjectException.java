package DeploymentLab.Exceptions;

/**
 * Represents a case where MapSense has run into a object type it knows nothing about.
 * This is a fatal error, and needs to crash the program about as hard as it can.
 * @author Gabriel Helman
 * @since Mars
 * Date: 12/30/11
 * Time: 2:29 PM
 */
public class UnknownObjectException extends ModelCatastrophe{
	private static final long serialVersionUID = 1L;

	public UnknownObjectException() {
		super();
	}

	public UnknownObjectException(String message) {
		super(message);
	}

	public UnknownObjectException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnknownObjectException(Throwable cause) {
		super(cause);
	}
}
