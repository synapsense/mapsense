package DeploymentLab.Exceptions;

/**
 * Represents a case where MapSense has run into a component type it knows nothing about.
 * @author Gabriel Helman
 * @since Earth
 * Date: 2/3/11
 * Time: 10:55 AM
 */
public class UnknownComponentException extends Exception {
	private static final long serialVersionUID = 1L;
	public UnknownComponentException() {
	}

	public UnknownComponentException(String message) {
		super(message);
	}

	public UnknownComponentException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnknownComponentException(Throwable cause) {
		super(cause);
	}
}
