
package DeploymentLab

import DeploymentLab.Model.*
import DeploymentLab.channellogger.*
import com.synapsense.hardware_id.EUI64
import com.synapsense.hardware_id.MACID

public class AutoMacAssigner{
	private static final Logger log = Logger.getLogger(AutoMacAssigner.class.getName())
	private ComponentModel cmodel
	private ObjectModel omodel
	
	AutoMacAssigner(ObjectModel _omodel, ComponentModel _cmodel){
		omodel = _omodel
		cmodel = _cmodel
	}
	
	public void assignMacID(){
		def existingMacidList = []

		List<DLObject> macObjects = []
		macObjects += omodel.getObjectsByType('wsnnode')
		macObjects += omodel.getObjectsByType('wsngateway')

		macObjects.each{ o ->
			def c = cmodel.getOwner(o)
			def macIdPropertyList = c.listMacIdProperties()
			
			macIdPropertyList.each{it-> 
				def macid = c.getPropertyValue(it);
				if(macid!='' && macid!='0'){
					existingMacidList+=macid
				}
			}
		}
		
		//macidList.each{log.info('existing mac id:' + it)}

		Set<Integer> EUI64Platforms = new HashSet<Integer>();
		for( String s : CentralCatalogue.getComp("nodes.eui64Platforms").split(",") ) {
			EUI64Platforms.add( Integer.parseInt( s ) );
		}

		Random rand = new Random( System.currentTimeMillis() )

		macObjects.each{ o ->
			def c = cmodel.getOwner(o)
			def macIdPropertyList = c.listMacIdProperties()
			
			macIdPropertyList.each{it->
				def macid = c.getPropertyValue(it);
				if(macid == '' || macid == '0'){
					String newmacid = null
					while( !newmacid || existingMacidList.contains( newmacid )){
						// EUI64 must have the high 4-bits empty.
						int unique = ( rand.nextInt() & 0x0FFFFFFF )
						int platformId = 0
						if( o.hasObjectProperty('platformId') ) {
							platformId = o.getObjectProperty('platformId').getValue()
						}

						if( EUI64Platforms.contains( platformId ) ) {
							newmacid = EUI64.generateWith( platformId, unique ).toString().replace('-','')
						} else {
							newmacid = MACID.generateWith( platformId, unique ).toString().replace('-','')
						}
					}
					c.setPropertyValue( it, newmacid )
				}
			}
		}
	}
	
	public void clearMacID(){
		def macObjects = []
		macObjects += omodel.getObjectsByType('wsnnode')
		macObjects += omodel.getObjectsByType('wsngateway')
		macObjects.each{ o ->
			def c = cmodel.getOwner(o)
			def macIdPropertyList = c.listMacIdProperties()
			
			macIdPropertyList.each{it->
				c.setPropertyValue(it, '0')
			}
		}
	}
}