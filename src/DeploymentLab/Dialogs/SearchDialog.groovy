package DeploymentLab.Dialogs

import DeploymentLab.Export.DLObjectValueExporter
import DeploymentLab.Image.ComponentIconFactory
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.ModelChangeListener
import DeploymentLab.Model.PropertyConverter
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.channellogger.Logger
import groovy.swing.SwingBuilder

import javax.swing.table.TableModel
import java.awt.BorderLayout
import java.awt.Cursor
import java.awt.FlowLayout
import javax.swing.table.AbstractTableModel
import net.miginfocom.swing.MigLayout
import DeploymentLab.*
import java.awt.event.*
import javax.swing.*
import javax.swing.event.*
import DeploymentLab.Model.ComponentProperty
import DeploymentLab.Model.MetamorphosisEvent
import DeploymentLab.Model.ModelState
import groovy.beans.Bindable

/**
 * Displays the Spotlight-style search a dialog box.
 *
 * @see DeploymentLab.Model.ComponentModel#spotlight
 */
class SearchDialog implements ListSelectionListener, DocumentListener, ActionListener, ModelChangeListener, TableModelListener {
	/** Internal SwingBuilder used to construct the dialogDelegate. */
	private SwingBuilder builder
	/** All the actual dialog work is delegated to the JDialog class. */
	private def dialogDelegate

	/** The close button gets its own class-wide field to allow us to set the focus there when we need to. */
	private JButton closeButton
	/** The button that does the component locate. */
	private JButton locateButton
	//private JButton goButton

	/** The parent frame of this dialog box.*/
	private JFrame parent
	/** Stores a reference to the current component model for use in searching. */
	private ComponentModel cm

	private SelectModel sm
	private DeploymentPanel dp
	private UserPreferences userPreferences

	private SelectingTable resultsTable
	private SearchTableModel tm
	private DLComponent selectedComponent
	private JTextField searchbox
	private JComboBox fieldBox
	private static final Logger log = Logger.getLogger(SearchDialog.class.getName())
	List<Map<String, Object>> searchResults = []		 // component: <a DLComponent>, value: <a string>
	/** The label listing the number of components in the current selection.*/
	private JLabel searchDomainNumber
	/** The domain of components to search across.  If empty, searches on the entire project model.*/
	private List<DLComponent> selection

	JToggleButton editButton

	/** The combobox controlling whether the search is on the selection, the active drawing or *everything. */
	private JComboBox comboScope
	/** Bound property that stays updated from the scope bomcobox in the UI */
	@Bindable SearchScope scope

	/**
	 */
	public SearchDialog(JFrame _parent, ComponentModel _cm, SelectModel _sm, DeploymentPanel _dp, UserPreferences _up) {
		this.builder = new SwingBuilder()
		this.parent = _parent
		this.cm = _cm
		this.sm = _sm
		this.dp = _dp
		this.userPreferences = _up
		this.cm.addModelChangeListener(this)
		dialogDelegate = this.getDialog()
	}

	/**
	 * Constructs the delegate and returns the JDialog ready for display.
	 */
	private def getDialog() {
		tm = new SearchTableModel(searchResults, true)
		tm.addTableModelListener(this)
		def searchAction = builder.action(name: 'search', closure: {
			doSearch()
		}
		)
		JPanel topPanel, middlePanel
		searchbox = builder.textField(columns: 20, action: searchAction)
		searchbox.getDocument().addDocumentListener(this)

		NamePair blank = new NamePair()
		fieldBox = builder.comboBox(items: [blank] + cm.getPropertyNames(sm.getExpandedSelection()))
		fieldBox.addActionListener(this)

		def closeAction = builder.action(name: 'Close', closure: { this.exit() })
		def outerPanel

		def comboModel = new DefaultComboBoxModel<SearchScope>(SearchScope.getValuesAsVector())

		dialogDelegate = builder.frame(resizable: true, title: 'Find', layout: new BorderLayout(), iconImage: ComponentIconFactory.getAppIcon().getImage()) {
			panel(layout: new BorderLayout()) {
				topPanel = panel(layout: new MigLayout("fillx"), constraints: BorderLayout.NORTH) {
					label(text: 'Search for ')
					widget(searchbox, constraints: "wrap, growx")
					label(text: 'Search in Property Name')
					widget(fieldBox, constraints: "wrap, growx")

					label(text: "Scope ", constraints: "gapright rel")
					comboScope = comboBox(editable: false, model: comboModel, constraints: "growx, wrap", actionPerformed: {this.scopeChanged()})

					searchDomainNumber = builder.label('Searching across all components')
					editButton = builder.toggleButton(icon: ComponentIconFactory.getIconByName("edit_16"), selected: false, actionPerformed: {tm.setEditState(editButton.selected)}, constraints: "trailing wrap")

				}

				middlePanel = panel(layout: new SpringLayout(), constraints: BorderLayout.CENTER) {
					scrollPane() {
						//resultsTable = table(model: tm)
						resultsTable = (SelectingTable) table(new SelectingTable(tm))
					}
				}
				panel(layout: new BorderLayout(), constraints: BorderLayout.SOUTH) {
					panel(layout: new FlowLayout(FlowLayout.LEADING), constraints: BorderLayout.WEST) {
						//editButton = builder.toggleButton(text: "Toggle Edit", selected: false, actionPerformed: {tm.setEditState(editButton.selected)} )
						builder.button(text: "Export to CSV", actionPerformed: {this.exportTable()})
					}
					panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.EAST) {
						locateButton = builder.button(text: 'Locate in Plan', defaultButton: true, actionPerformed: { this.locate(false) })
						//goButton  = builder.button( text: 'Locate in Plan and Close', actionPerformed: { this.locate(true) } )
						closeButton = builder.button(text: 'Close', action: closeAction)
					}
				}
			}

			bind(source: comboScope, sourceProperty: "selectedItem", target: this, targetProperty: "scope")
		}
		SpringUtilities.makeCompactGrid(middlePanel,
				1, 1,   //rows, cols
				5, 5,   //initX, initY
				5, 5);  //xPad, yPad
		resultsTable.setAutoCreateRowSorter(true);
		((DefaultRowSorter) resultsTable.getRowSorter()).setSortsOnUpdates(true)

		//hotrod the input & action maps
		def im = resultsTable.getInputMap()
		def am = resultsTable.getActionMap()
		am.put("nextCell", new TabAction(resultsTable, true))
		im.put(KeyStroke.getKeyStroke("TAB"), "nextCell")
		am.put("lastCell", new TabAction(resultsTable, false))
		im.put(KeyStroke.getKeyStroke("shift TAB"), "lastCell")
		am.put("nextCellEnter", new TabAction(resultsTable, true))
		im.put(KeyStroke.getKeyStroke("ENTER"), "nextCellEnter")

		//def listSelectionModel = resultsTable.getSelectionModel();
		//listSelectionModel.addListSelectionListener(this);
		//resultsTable.addMouseListener(this);
		resultsTable.setSelectAllForEdit(true);


		topPanel.registerKeyboardAction(closeAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		dialogDelegate.pack()
		//dialogDelegate.setAlwaysOnTop( true );
		return dialogDelegate
	}

	protected void scopeChanged(){
		this.populateFieldBox()
		this.doSearch()
	}

	public void locate(boolean closeWindow) {
		int currentRow
		DLComponent currentComponent
		List<DLComponent> selected = []

        // verify all selected components in same drawing or not
        ArrayList<DLComponent> drawingList = new ArrayList<DLComponent>()

		for (int viewRow: resultsTable.getSelectedRows()) {
			currentRow = resultsTable.convertRowIndexToModel(viewRow)
			currentComponent = tm.getComponent(currentRow)
			selected += currentComponent
            DLComponent drawing = currentComponent.getDrawing()
            if(!drawingList.contains(drawing))
                drawingList.add(drawing)
		}

		if(selected.size() == 0){
			return
		}

        if(drawingList.size()>1){
            log.warn("cannot place components in multi-drawings ","message")
            return
        }
        // set active drawing first to get drawing
        sm.setActiveDrawing(drawingList.get(0))
        sm.setSelection(selected)
        dp.scrollToView(sm.getExpandedSelection())
		if (closeWindow) { this.exit() }
	}

	/**
	 * ListSelectionListener method.  Originally this handled closing and selecting a component when something was clicked in the results pane; left in as a stub.
	 */
	public void valueChanged(ListSelectionEvent lse) {
		/*
				if (lse.getValueIsAdjusting()) { return }
				 selectedComponent = tm.getComponent( resultsTable.getSelectedRow() )
				 //println selectedComponent
				 dialogDelegate.setVisible(false)
				 */
	}

	// DocumentListener methods
	public void insertUpdate(DocumentEvent ev) {
		this.doSearch();
	}

	public void removeUpdate(DocumentEvent ev) {
		this.doSearch();
	}

	public void changedUpdate(DocumentEvent ev) {}

	/**
	 * Fired when something happens in the fieldBox.
	 */
	public void actionPerformed(ActionEvent e) {
		if (fieldBox.getSelectedItem()) {
			this.doSearch();
		}
	}

	public void doSearch() {
		//searchResults = cm.spotlight( searchbox.text, fieldBox.getSelectedItem().getInternal() )
		//we used to pass in the internal name of the property so that we could do some fast hashtable retrieves; but,
		// to handle properties with different internal names and the same display name, we're just using the display name now
		//def s = new DLStopwatch();
		parent.setCursor(new Cursor(Cursor.WAIT_CURSOR))
		switch (scope) {
			case SearchScope.All:
				searchResults = cm.spotlight(searchbox.text, fieldBox.getSelectedItem()?.getDisplay(), false, [])
				break
			case SearchScope.Active:
				searchResults = cm.spotlight(searchbox.text, fieldBox.getSelectedItem()?.getDisplay(), false, cm.getComponentsInDrawing(sm.getActiveDrawing()) )
				break
			case SearchScope.Selection:
				searchResults = cm.spotlight(searchbox.text, fieldBox.getSelectedItem()?.getDisplay(), false, selection)
				break
		}
		//the default sort order is now alphabetical by component name
		//todo: sort this while building the list of results, not after (see TreeSet, etc)
		searchResults.sort { it["component"].getName() }

		boolean showFieldName = true
		if (fieldBox.getSelectedItem()?.getInternal()) {
			showFieldName = false
		}
		if (selection && selection.size() > 0) {
			searchDomainNumber.setText("${selection.size()} components in current selection")
		} else {
			searchDomainNumber.setText("All components in current selection")
		}
		tm.setData(searchResults, showFieldName)
		tm.fireTableStructureChanged()
		parent.setCursor(null)
		//println s.finishMessage()
	}
	
	private void populateFieldBox(){
		NamePair blank = new NamePair()
		def last = fieldBox.getSelectedItem()
		fieldBox.removeAllItems()
		def newList //= [blank] + cm.getPropertyNames(sm.getExpandedSelection()) //.sort()
		switch (scope) {
			case SearchScope.All:
				newList = [blank] + cm.getPropertyNames([])
				break
			case SearchScope.Active:
				newList = [blank] + cm.getPropertyNames( cm.getComponentsInDrawing(sm.getActiveDrawing()) )
				break
			case SearchScope.Selection:
				newList = [blank] + cm.getPropertyNames(selection)
				break
		}
		for (NamePair n: newList) {
			fieldBox.addItem(n)
		}
		if (fieldBox.getSelectedItem() != last) {
			fieldBox.setSelectedItem(last)
		}
	}
	
	/**
	 Delegates to jDialog.setLocationRelativeTo().
	 */
	public void setLocationRelativeTo() {
		dialogDelegate.setLocationRelativeTo(parent)
	}

	/**
	 Delegates to jDialog.show().  Also includes a call to setFocusInWindow, since we'll always want to do that when we show the dialog box as well.
	 */
	public void show() {
		//def sh = new DLStopwatch();
		parent.setCursor(new Cursor(Cursor.WAIT_CURSOR))
		//to support searching on a selection, we grab the current expanded selection and pass it in
		//we only do this once so that the search dialog won't alter the search domain
		selection = sm.getExpandedSelection()

		if(selection.size() > 0){
			comboScope.setSelectedItem(SearchScope.Selection)
		} else {
			comboScope.setSelectedItem(SearchScope.Active)
		}

		//this.populateFieldBox()
		//doSearch()
		this.setLocationRelativeTo()
		this.requestFocusInWindow()
		parent.setCursor(null)
		editButton.selected = false;
		tm.setEditState(editButton.selected)
		dialogDelegate.setVisible(true);
		//println "ShowTime= " + sh.finishMessage()
	}

	public void exit() {
		dialogDelegate.setVisible(false)
	}

	/**
	 Delegates to jDialog.requestFocusInWindow(), however this does not put the dialog in focus - it puts the search box in focus.
	 */
	public void requestFocusInWindow() {
		searchbox.requestFocusInWindow()
	}

	public void exportTable() {
		userPreferences.setUserPreference(UserPreferences.CSV_FILENAME, userPreferences.getUserPreference(UserPreferences.FILENAME).replace(".dlz", "")+"_Search")
		userPreferences.setUserPreference(UserPreferences.CSV_TITLE, CentralCatalogue.getUIS("exportSearch.filechooser.title"))
		DLObjectValueExporter.exportTable(userPreferences, tm.getVisibleData(), dialogDelegate)
	}

	@Override
	void componentAdded(DLComponent component) {}

	@Override
	void componentRemoved(DLComponent component) {}

	@Override
	void childAdded(DLComponent parent, DLComponent child) {}

	@Override
	void childRemoved(DLComponent parent, DLComponent child) {}

	@Override
	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	@Override
	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	@Override
    public void modelMetamorphosisStarting( MetamorphosisEvent event ) {
        if( event.getMode() == ModelState.CLEARING ) {
            //do a small amount of cleanup to make sure we're not leaking search results
            fieldBox.removeAllItems()
            selection = null
            searchResults = null
            this.exit()
        }
	}

    @Override
    public void modelMetamorphosisFinished( MetamorphosisEvent event ) {
		if( event.getMode() == ModelState.INITIALIZING ) {
			this.populateFieldBox()
		}
	}

	@Override
	void tableChanged(TableModelEvent e) {
//		if(e.getColumn() == 2){
//			log.debug("repeat search due to data change! $e")
//			this.doSearch()
//		}
	}
}

/**
 * Provides the data model for the search results table.
 * Note that the table itself is a stock JTable.
 * @see DeploymentLab.Model.ComponentModel#spotlight
 */
class SearchTableModel extends AbstractTableModel {
	/** A List of search results.  Each result is Map<String,Object> with three keys: 'component', 'fieldname', and 'value'. **/
	private static final Logger log = Logger.getLogger(SearchTableModel.name)
	List<Map<String, Object>> data
	final String[] columns = ['Drawing','Component Name', 'Component Property', 'Value']
	final String[] smallColumns = ['Drawing','Component Name', 'Value']
	private showFieldName

	/** Can the data currently be edited at all? */
	boolean editState

	public SearchTableModel(List<Map<String, Object>> dataSource, boolean sfn) {
		this.setData(dataSource)
		this.showFieldName = sfn
	}

	public setData(List<Map<String, Object>> dataSource, boolean sfn) {
		this.data = dataSource
		this.fireTableStructureChanged()
		this.showFieldName = sfn
	}

	public DLComponent getComponent(int row) {
		if (row < data.size()) {
			return (data[row]['component'])
		} else {
			return null
		}
	}

    public ComponentProperty getProperty(int row) {
        DLComponent c = getComponent(row)
        return c.getComponentProperty( c.getPropertyNameFromDisplayName( (String) data[row]['fieldname'] ) )
    }

	@Override
	String getColumnName(int col) {
		if (showFieldName) {
			return columns[col]
		} else {
			return smallColumns[col]
		}
	}

	@Override
	Object getValueAt(int row, int col) {
		if (showFieldName) {
			switch (col) {
                case 0: return (data[row]['component'].getDrawing().getFullName()); break;
				//case 0: return (data[row]['component'].getName()); break;
				case 1: return (data[row]['component'].getFullName()); break;
				case 2: return (data[row]['fieldname']); break;
                case 3:
                    // If possible, use the actual property value so conversion and localization is correct.
                    ComponentProperty prop = getProperty(row)
                    if( prop ) {
                        if( prop.isUnitConvertable() && prop.value != null ) {
                            return Localizer.convert( prop.value, prop.dimensionName )
                        } else {
                            return prop.value
                        }
                    }
                    return data[row]['value']
                    break
			}
		} else {
			switch (col) {
                case 0: return (data[row]['component'].getDrawing().getFullName()); break;
				case 1: return (data[row]['component'].getName()); break;
                case 2:
                    // If possible, use the actual property value so conversion and localization is correct.
                    ComponentProperty prop = getProperty(row)
                    if( prop ) {
                        if( prop.isUnitConvertable() && prop.value != null ) {
                            return Localizer.convert( prop.value, prop.dimensionName )
                        } else {
                            return prop.value
                        }
                    }
                    return data[row]['value']
                    break
			}
		}
		return null
	}

	@Override
	int getRowCount() {
		return data.size()
	}

	@Override
	int getColumnCount() {
		if (showFieldName) {
			return columns.size()
		} else {
			return smallColumns.size()
		}
	}

	@Override
	Class<?> getColumnClass(int columnIndex) {
		if (showFieldName) {
			switch (columnIndex) {
                case 0: return String.class; break;
				case 1: return String.class; break;
				case 2: return String.class; break;
				case 3: return Object.class; break;
			}
		} else {
			switch (columnIndex) {
                case 0: return String.class; break;
				case 1: return String.class; break;
				case 2: return Object.class; break;
			}
		}
		return super.getColumnClass(columnIndex)
	}

	@Override
	boolean isCellEditable(int rowIndex, int columnIndex) {
		if (!editState) { return false}

		if (columnIndex == 0) {
			return false
		}

		boolean result = false
		DLComponent target = (DLComponent) data[rowIndex]['component']
		//we probably need to find a faster way to do this lookup, but fot the moment this works
		String internalName = target.getPropertyNameFromDisplayName(((String) data[rowIndex]['fieldname']))
		if (!internalName) {
			//one of the "fake" properties
			return false
		}
		def prop = target.getComponentProperty(internalName)
		if (!prop.isEditable()) {
			return false
		}
		//now we need to check types: the "fancy" types (valuechoices, tables, etc) can't be edited here either
		if (prop.getValueChoices() != null) {
			return false
		}
		if (PropertyConverter.isComplex(prop.getType())) {
			return false
		}

		//the last column is always the editable one, regardless of mode
		if (columnIndex == this.getColumnCount() - 1){
			result = true
		}
		return result
	}

	@Override
	void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		//only the value column is editable, whichever that one is
		//data[rowIndex]['value']
		DLComponent target = (DLComponent) data[rowIndex]['component']
        ComponentProperty prop = getProperty(rowIndex)
		String internalName = target.getPropertyNameFromDisplayName(((String) data[rowIndex]['fieldname']))
		try {
            // If needed, parse the string in a Locale friendly manner.
            if( (( aValue instanceof String ) || ( aValue instanceof GString )) && Localizer.isLocalizable( prop ) ) {
                aValue = Localizer.parse( aValue )
            }

            // If necessary, convert the units back to our internal units.
            if( prop.isUnitConvertable() && aValue != null ) {
                aValue = Localizer.convertFrom( aValue, prop.dimensionName )
            }
			target.setPropertyValue(internalName, aValue)
			data[rowIndex]['value'] = aValue
			//this.fireTableDataChanged()
			this.fireTableCellUpdated(rowIndex, columnIndex)
		} catch (Exception e) {
			String err = ( e.getMessage() == null ? e.getClass().getName() : e.getMessage() )
			log.error(String.format(CentralCatalogue.getUIS('componentProperty.setError'), data[rowIndex]['fieldname'], aValue, err), 'message')
			log.trace(String.format(CentralCatalogue.getUIS('componentProperty.errorSource'), e.getClass().name, e.message))
		}
	}

	public ArrayList<String[]> getVisibleData() {
		ArrayList<String[]> result = new ArrayList<String[]>(data.size())
		for (int row = 0; row < this.getRowCount(); row++) {
			String[] line = new String[this.getColumnCount()]
			for (int col = 0; col < this.getColumnCount(); col++) {
				line[col] = this.getValueAt(row, col)
			}
			result.add(line)
		}
		return result
	}
}


public class TabAction extends AbstractAction {
	private static final Logger log = Logger.getLogger(TabAction.class.getName())
	SelectingTable table;
	TableModel tm;
	boolean forward

	public TabAction(SelectingTable t, boolean forward) {
		this.table = t;
		this.tm = table.getModel()
		this.forward = forward
	}

	@Override
	void actionPerformed(ActionEvent e) {
		if (table.isEditing()) {
			if( !table.getCellEditor().stopCellEditing() ) {
				// Stop rejected, probably due to field validation, so don't advance the selection.
				return
			}
		}

		int originalRow = table.getSelectedRow();
		//int originalColumn = table.getSelectedColumn();
		int row
		if (forward) {
			row = originalRow + 1
			if (row == tm.getRowCount()) {
				row = 0;
			}
		} else {
			row = originalRow - 1
			if (row < 0) {
				row = tm.getRowCount() - 1;
			}
		}
		int col = tm.getColumnCount() - 1
		log.trace("Changing table selection to $row, $col")
		table.changeSelection(row, col, false, false)
	}
}