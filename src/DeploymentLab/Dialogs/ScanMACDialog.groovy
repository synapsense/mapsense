package DeploymentLab.Dialogs

import DeploymentLab.*
import DeploymentLab.Export.DLObjectValueExporter
import DeploymentLab.Image.ComponentIconFactory
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLObject
import DeploymentLab.Security.MacChecksum
import DeploymentLab.channellogger.Logger
import groovy.beans.Bindable
import groovy.swing.SwingBuilder
import net.miginfocom.swing.MigLayout

import javax.swing.*
import javax.swing.table.AbstractTableModel
import java.awt.*
import java.util.List
import javax.swing.table.DefaultTableCellRenderer
import DeploymentLab.Model.ComponentBinding
import DeploymentLab.Model.Setting

/**
 * @author Vadim Gusev
 * @since Europa
 * Date: 26.07.13
 * Time: 8:06
 */
class ScanMACDialog {

	private static final Logger log = Logger.getLogger(ScanMACDialog.getClass().getName())

	private static final String ALL_NETWORKS_ELEMENT = "Show All"

	// We need a copy or Nimbus appears to ignore us using this color since this is actually a ColorUIResource.
	private static Color COLOR_BAD_CHECKSUM = new Color( UIManager.getColor("nimbusAlertYellow").getRGB() )

	// Nimbus Red is a little too dark, so go a shade lighter. This makes a copy, so no need for us to do so.
	private static Color COLOR_PLATFORM_MISMATCH = UIManager.getColor("nimbusRed").brighter()

	private SwingBuilder builder
	private def dialogDelegate

	private JFrame parent
	private DLProject project
	private SelectModel sm
	private UserPreferences userPreferences

	DrawingComboBoxModel drawingModel
	DefaultComboBoxModel networkModel
	@Bindable def selectedDrawing
	@Bindable def selectedNetwork
	@Bindable boolean hideValidEntries

	private SelectingTable table
	private List data = []

	private Map<Long,List<DLObject>> macids = [:]

	private JButton okBtn
	private JComboBox drawingCombo
	private JComboBox networkCombo
	private JCheckBox blanksOnlyCheckbox


	public ScanMACDialog(JFrame _parent, DLProject _project, SelectModel _sm, UserPreferences _up, DrawingComboBoxModel _drawingModel) {
		this.builder = new SwingBuilder()
		this.parent = _parent
		this.project = _project
		this.sm = _sm
		this.userPreferences = _up
		this.drawingModel = _drawingModel
		dialogDelegate = this.getDialog()

		/*
		// Code snippit that can be used to view all the possible UIManager keys.
		for( Map.Entry<String, Object> entry : UIManager.getLookAndFeelDefaults().entrySet() ) {
			if( entry.getValue().getClass().getName().contains('Color') ) {
				println "-- ${entry.getKey()} :: ${entry.getValue()}"
			}
		}
		*/
	}

	private def getDialog() {
		def validateAction = builder.action(name: 'Validate', closure: { this.validate() })
		def exportAction = builder.action(name: 'Export Names', closure: {
			userPreferences.setUserPreference(UserPreferences.CSV_FILENAME, userPreferences.getUserPreference(UserPreferences.FILENAME).replace(".dlz", "")+"_Labels")
			userPreferences.setUserPreference(UserPreferences.CSV_TITLE, "Export Names")
			DLObjectValueExporter.exportTable(userPreferences, this.generateMacLables(), dialogDelegate) })
		def closeAction = builder.action(name: 'OK', closure: { this.exit() })

		// Can't use the existing network model or controller since we want to only show WSN Networks. May need to do
		// something similar to that controller (or refactor for reuse) if performance becomes an issue with this simple model.
		networkModel = new DefaultComboBoxModel<DLComponent>()

		JPanel topPanel, middlePanel
		dialogDelegate = builder.dialog( owner: parent,
		                                 resizable: true,
		                                 title: 'Scan MAC IDs',
		                                 layout: new BorderLayout(),
		                                 modalityType: Dialog.ModalityType.APPLICATION_MODAL,
		                                 iconImage: ComponentIconFactory.getAppIcon().getImage(),
		                                 preferredSize: new Dimension(600,600)) {
			panel(layout: new BorderLayout()) {
				topPanel = panel(layout: new MigLayout("fillx"), constraints: BorderLayout.NORTH) {

					label(text: "Drawing", constraints: "gapright rel")
					drawingCombo = comboBox(editable: false, model: drawingModel, renderer: new ActiveWithIconRenderer(),
					                          constraints: "growx, wrap", actionPerformed: { populateNetworks(); this.refreshTable()})

					label(text: "Network", constraints: "gapright rel")
					networkCombo = comboBox(editable: false, model: networkModel, renderer: new ActiveWithIconRenderer(),
					                        constraints: "growx, wrap", actionPerformed: {this.refreshTable()})

					label(text: "Hide Valid MAC IDs", constraints: "gapright rel")
					blanksOnlyCheckbox = checkBox(text:'', selected:false, actionPerformed:  { this.refreshTable() })
				}
				middlePanel = panel(layout: new MigLayout("flowy,fill"), constraints: BorderLayout.CENTER ) {
					scrollPane( constraints: "grow, push" ) {
						table = table(new SelectingTable(new MacTableModel(data)))
						table.setSelectAllForEdit(true)
						table.setCascadeEdit(true)
						table.setSelectionMode( ListSelectionModel.SINGLE_SELECTION )
					}
					panel(layout: new FlowLayout(FlowLayout.CENTER,10,0), constraints: "grow x" ) {
						label(text: "Error Legend:")
						label(text: "Bad Checksum", opaque: true, background: COLOR_BAD_CHECKSUM,
						      border: BorderFactory.createEmptyBorder(5,5,5,5))
						JLabel l = label(text: "Platform Mismatch", opaque: true, background: COLOR_PLATFORM_MISMATCH,
						      border: BorderFactory.createEmptyBorder(5,5,5,5))
						label(text: "Duplicate", opaque: true,
						      border: BorderFactory.createEmptyBorder(5,5,5,5)).setFont( l.getFont().deriveFont( Font.BOLD ) )
					}
				}
				panel(layout: new BorderLayout(), constraints: BorderLayout.SOUTH) {
					panel(layout: new FlowLayout(FlowLayout.LEADING), constraints: BorderLayout.WEST) {
						builder.button(text: "Export Names", action: exportAction)
					}
					panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.EAST) {
						builder.button(text: 'Validate', action: validateAction)
						okBtn = builder.button(text: 'OK', action: closeAction)
					}
				}
			}
			bind(source: drawingCombo, sourceProperty: "selectedItem", target: this, targetProperty: "selectedDrawing")
			bind(source: networkCombo, sourceProperty: "selectedItem", target: this, targetProperty: "selectedNetwork")
			bind(source: blanksOnlyCheckbox, sourceProperty: "selected", target: this, targetProperty: "hideValidEntries")
		}

		def im = table.getInputMap()
		def am = table.getActionMap()
		am.put("nextCell", new TabAction(table, true))
		im.put(KeyStroke.getKeyStroke("TAB"), "nextCell")
		am.put("lastCell", new TabAction(table, false))
		im.put(KeyStroke.getKeyStroke("shift TAB"), "lastCell")
		am.put("nextCellEnter", new TabAction(table, true))
		im.put(KeyStroke.getKeyStroke("ENTER"), "nextCellEnter")

		dialogDelegate.pack()

		// Lazy way to get the default component so that the formatting is the same.
		Component comp = table.getCellEditor(1,MacTableModel.COLIDX_MACID).getTableCellEditorComponent( table, "", false, 1, MacTableModel.COLIDX_MACID )
		table.getColumnModel().getColumn( MacTableModel.COLIDX_MACID ).setCellEditor( new ValidatorCellEditor( (JTextField) comp ) )
		table.getColumnModel().getColumn( MacTableModel.COLIDX_MACID ).setCellRenderer( new ValidatorCellRenderer() )

		return dialogDelegate
	}

	public void setLocationRelativeTo() {
		dialogDelegate.setLocationRelativeTo(parent)
	}

	public void show() {
		cacheMacIds()
		populateNetworks()
		refreshTable()
		this.setLocationRelativeTo()
		this.requestFocusInWindow()
		dialogDelegate.setVisible(true)
	}

	public void exit() {
		dialogDelegate.setVisible(false)
	}

	public void requestFocusInWindow() {
		table.requestFocusInWindow()
	}

	// TODO: Would be nice if this and the cache were the same rather than both separately pulling from the model.
	private void populateData(){
		data.clear()

		if( !(selectedDrawing instanceof DLComponent) ) {
			blanksOnlyCheckbox.setText("")
			return
		}

		def components
		if( selectedNetwork instanceof DLComponent ) {
			// Only show components in the selected network.
			components = selectedNetwork.getChildComponents()
		} else {
			// Show all components on the selected drawing.
			components = project.getComponentModel().getComponentsInDrawing( selectedDrawing )
		}

		int skippedEntryCnt = 0
		for (DLComponent dlc : components)  {
			for (String pName : dlc.listMacIdProperties()) {
				if (!dlc.getComponentProperty(pName).isDisplayed()) continue

				String mac = dlc.getPropertyValue(pName)
				if (mac == null || mac.equals("0")) {
					mac = ""
				}

				// In typical MapSense fashion, we have to jump through a couple hoops to find the actual object that
				// the component property is bound to so we can get some object specific info.
				String platformName = ""
				int platformId = 0
				DLObject targetObj = null
				for( ComponentBinding binding : dlc.getBindingsForProp( pName ) ) {
					for( Setting setting : binding.getTargetSettings() ) {
						DLObject boundObject = setting.getOwner()

						// NOTE: We are assuming MACs are bound to a single object. There's a JUnit test to verify this.
						if( boundObject.getType().startsWith('wsnnode') ) {
							platformName = boundObject.getObjectProperty('platformName').getValue()
							platformId = boundObject.getObjectProperty('platformId').getValue()
							targetObj = boundObject
							break
						} else if( boundObject.getType().startsWith('wsngateway') ) {
							// Old device doesn't have the platform identifiers. Use the component type name instead.
							platformName = dlc.getDisplaySetting('name')
							platformId = 0
							targetObj = boundObject
							break
						}
					}
				}

				def validCode = null

				if( !mac.isEmpty() ) {
					long val = Long.parseLong( mac, 16 )
					validCode = MacChecksum.validateMac( val, platformId )

					if( hideValidEntries && (validCode == MacChecksum.ValidationCode.VALID) && (macids[val].size() == 1) ) {
						// Skip this entry since it already has a valid value.
						skippedEntryCnt++
						continue
					}
				}

				def s = [:]

				s['Component Name'] = dlc.getName()
				s['Property Name'] = dlc.getPropertyDisplayName(pName)
				s['MAC ID Value'] = mac
				s['component'] = dlc
				s['Platform Name'] = platformName
				s['platformId'] = platformId
				s['validCode'] = validCode
				s['targetObject'] = targetObj

				data.add(s)
			}
		}

		if( skippedEntryCnt > 0 ) {
			blanksOnlyCheckbox.setText("($skippedEntryCnt of ${data.size()+skippedEntryCnt} hidden)")
		} else {
			blanksOnlyCheckbox.setText("")
		}

		data.sort {a, b ->
			if (a['Platform Name'] == b['Platform Name'] && a['Component Name'] == b['Component Name']){
				a['Property Name'] <=> b['Property Name']
			} else if (a['Platform Name'] == b['Platform Name']) {
				a['Component Name'] <=> b['Component Name']
			} else {
				a['Platform Name'] <=> b['Platform Name']
			}

//			a['Component Name'] <=> b['Component Name'] ||
//			a['Property Name'] <=> b['Property Name']
		}
	}

	private void validate(){
		def noMacCnt = [:]
		def badChecksumCnt = [:]
		def mismatchCnt = [:]
		def dupMacCnt = [:]

		// To be safe, refresh our cache so we are checking the actual objects.
		cacheMacIds()

		for( Map.Entry<Long,List<DLObject>> entry : macids.entrySet() ) {
			for( DLObject o : entry.value ) {
				if( entry.key == 0 ) {
					// No mac assigned.
					MapHelper.increment( noMacCnt, project.getComponentModel().getOwner(o).getDrawing() )
				} else {
					def pid = o.getObjectSetting("platformId")
					def result = MacChecksum.validateMac( entry.key, ( pid ? (int) pid.getValue() : 0 ) )

					if( result == MacChecksum.ValidationCode.BAD_CHECKSUM ) {
						MapHelper.increment( badChecksumCnt, project.getComponentModel().getOwner(o).getDrawing() )
					} else if( result == MacChecksum.ValidationCode.PLATFORM_MISMATCH ) {
						MapHelper.increment( mismatchCnt, project.getComponentModel().getOwner(o).getDrawing() )
					}

					// Also error if there's more than one object using this mac.
					if( entry.value.size() > 1 ) {
						MapHelper.increment( dupMacCnt, project.getComponentModel().getOwner(o).getDrawing() )
					}
				}
			}
		}

		// Piece together the message to display.
		String msg = ""
		int msgLevel = JOptionPane.WARNING_MESSAGE

		if( !noMacCnt.isEmpty() ) {
			msg += "The following Drawings contain empty MAC IDs:<br>${getMapString(noMacCnt)}"
			msgLevel = JOptionPane.ERROR_MESSAGE
		}

		if( !dupMacCnt.isEmpty() ) {
			msg += "The following Drawings contain duplicate MAC IDs:<br>${getMapString(dupMacCnt)}"
			msgLevel = JOptionPane.ERROR_MESSAGE
		}

		if( !mismatchCnt.isEmpty() ) {
			msg += "<p>The following Drawings contain MAC IDs that do not match the Platform:<br>${getMapString(mismatchCnt)}"
			msgLevel = JOptionPane.ERROR_MESSAGE
		}

		if( !badChecksumCnt.isEmpty() ) {
			msg += "<p>The following Drawings contain MAC IDs with bad checksums:<br>${getMapString(badChecksumCnt)}"
			// level will be the default WARNING if this is all we have.
		}

		if( msg.isEmpty() ) {
			msg = "Complete. All MAC IDs have been successfully verified."
			msgLevel = JOptionPane.INFORMATION_MESSAGE
		}

		JOptionPane.showMessageDialog( dialogDelegate, "<html>$msg</html>", "MAC ID Validation", msgLevel )

		if( hideValidEntries ) {
			refreshTable()
		}
	}

	private String getMapString( Map map ) {
		// Java appears to ignore the style to get rid of bullets and <pre> used a different font, so use good ol' spaces.
		// String str = "<ul style='list-style: none;'>"
		String str = ""
		for( int i = 1; i < drawingModel.getSize(); i++ ) {
			DLComponent drawing = drawingModel.getElementAt( i )
			if( map.containsKey( drawing ) ) {
				str += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${drawing.getName()} (${map.get(drawing)})<br>"
			}
		}
		return str
	}

	private List<String[]> generateMacLables(Set<DLObject> drawings = null){
		String currentPlatform = null
		List<String[]> result = new LinkedList<String[]>()

		for(Map map : data){
			if (!map['Platform Name'].equals(currentPlatform)){
				currentPlatform = map['Platform Name']
				String[]  s = new String[1]
				s[0] = "******* "+currentPlatform+" *******"
				result.add(s)
			}

			String[]  s = new String[2]
			s[0] = map['Component Name']
			String pn = map['Property Name'].replace("MAC ID", "")
			if (!pn.isEmpty()) s[0]+=" $pn"
			s[1] = map['MAC ID Value']
			result.add(s)
		}

		return result
	}

	private void cacheMacIds() {
		macids.clear()
		def wsnObjects = project.getObjectModel().getObjectsByType('wsngateway') + project.getObjectModel().getObjectsByType('wsnnode')

		for( DLObject o : wsnObjects ) {
			MapHelper.addList( macids, o.getObjectProperty('mac').getValue(), o )
		}
	}

	private void refreshTable(){
		// May need to do something smarter if performance becomes an issue. Right now, just do a hard refresh any time a filter changes.
		populateData()

		// setData replaces the entire model. @#$! JTable internally wipes things out and we seem to lose the renderer we previously set.
		// This hack houldn't be needed if we ever refactor to do something like NetworkComboBoxController.
		def cr = table.getColumnModel().getColumn( MacTableModel.COLIDX_MACID ).getCellRenderer()
		def ce = table.getColumnModel().getColumn( MacTableModel.COLIDX_MACID ).getCellEditor()

		table.getModel().setData(data)

		table.getColumnModel().getColumn( MacTableModel.COLIDX_MACID ).setCellRenderer( cr )
		table.getColumnModel().getColumn( MacTableModel.COLIDX_MACID ).setCellEditor( ce )

		this.setDefaultSelection()
	}

	private void populateNetworks() {
		// May need to do something smarter like NetworkComboBoxController if performance becomes an issue.
		networkModel.removeAllElements()
		networkModel.addElement( ALL_NETWORKS_ELEMENT )

		if( selectedDrawing instanceof DLComponent ) {
			for( DLComponent net : project.getComponentModel().getComponentsByType('network', (DLComponent) selectedDrawing) ) {
				int idx = networkModel.getSize()
				for( int i = 1; i < networkModel.getSize(); i++ ) {
					if( net.getPropertyValue('name') < networkModel.getElementAt(i).getPropertyValue('name') ) {
						idx = i
						break
					}
				}
				networkModel.insertElementAt( net, idx )
			}
		}
	}

	private void setDefaultSelection(){
		this.requestFocusInWindow()
		table.changeSelection(0, 3, false, false)
	}

	class MacTableModel extends AbstractTableModel{

		private static final Logger log = Logger.getLogger(MacTableModel.getClass().getName())

		public static final int COLIDX_MACID = 3

		List<Map<String, Object>> data
		final String[] columns = ['Platform Name','Component Name', 'Property Name', 'MAC ID Value']

		public MacTableModel(List<Map<String, Object>> dataSource) {
			this.setData(dataSource)
		}

		public setData(List<Map<String, Object>> dataSource) {
			this.data = dataSource
			this.fireTableStructureChanged()
		}

		@Override
		int getRowCount() {
			return data.size()
		}

		@Override
		int getColumnCount() {
			return columns.size()
		}

		@Override
		String getColumnName(int col) {
			return columns[col]
		}

		@Override
		Object getValueAt(int row, int col) {
			def r = data[row]
			return r == null ? "" : r[columns[col]]
		}

		@Override
		boolean isCellEditable(int rowIndex, int columnIndex) {
			if (columnIndex == 3) return true
			return false
		}

		@Override
		void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			DLComponent target = (DLComponent) data[rowIndex]['component']
			log.trace(((String) data[rowIndex][columns[2]]))
			String internalName = target.getPropertyNameFromDisplayName(((String) data[rowIndex][columns[2]]))
			log.trace(internalName)

			try {
				String origVal = data[rowIndex][columns[3]]
				if( origVal.equals( aValue ) ) {
					log.trace("Value not changed! value=$aValue")
					return
				}

				long mac = 0;
				if( aValue.toString().isEmpty() || aValue.toString().equals("0") ) {
					target.setPropertyValue(internalName, 0)
					data[rowIndex][columns[3]] = ""
					data[rowIndex]['validCode'] = null
				} else {
					mac = Long.parseLong( aValue.toString(), 16 )
					target.setPropertyValue(internalName, aValue)
					data[rowIndex][columns[3]] = aValue
					data[rowIndex]['validCode'] = MacChecksum.validateMac( mac, (int) data[rowIndex]['platformId'] )
				}

				// Update the cache.
				long oldMac = origVal.isEmpty() ? 0 : Long.parseLong( origVal, 16 )
				MapHelper.removeList( macids, oldMac, data[rowIndex]['targetObject'] )
				MapHelper.addList( macids, mac, data[rowIndex]['targetObject'] )

				this.fireTableCellUpdated(rowIndex, columnIndex)
			} catch (Exception e) {
				String err = ( e.getMessage() == null ? e.getClass().getName() : e.getMessage() )
				JOptionPane.showMessageDialog(dialogDelegate, String.format(CentralCatalogue.getUIS('componentProperty.setError'), "MAC ID Value", aValue, err), "Error", JOptionPane.ERROR_MESSAGE)
				log.trace(String.format(CentralCatalogue.getUIS('componentProperty.errorSource'), e.getClass().name, e.message))
			}
		}
	}

	class ValidatorCellEditor extends DefaultCellEditor {

		long originalVal;

		public ValidatorCellEditor( JTextField comp ) {
			super(comp);
		}

		@Override
		public Component getTableCellEditorComponent( JTable table, Object value, boolean isSelected, int row, int col ) {
			// Intercepted just to keep track of the pre-edit value.
			this.originalVal = value.toString().isEmpty() ? 0 : Long.parseLong( value.toString(), 16 )
			return super.getTableCellEditorComponent( table, value, isSelected, row, col )
		}

		@Override
		public boolean stopCellEditing() {
			JTextField comp = (JTextField) editorComponent
			String val = comp.getText()

			if( !val.isEmpty() ) {
				try {
					long mac = Long.parseLong( val, 16 )
					if( mac != originalVal ) {
						if( macids.containsKey( mac ) ) {
							JOptionPane.showMessageDialog( dialogDelegate, "Duplicate MAC ID Value", "Error", JOptionPane.ERROR_MESSAGE )
							comp.selectAll()
							return false
						}
					}
				} catch( NumberFormatException e ) {
					JOptionPane.showMessageDialog( dialogDelegate, "Invalid MAC ID Value", "Error", JOptionPane.ERROR_MESSAGE )
					comp.selectAll()
					return false
				}
			}

			return super.stopCellEditing()
		}
	}

	class ValidatorCellRenderer extends DefaultTableCellRenderer {

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {

			// Mostly use the default renderer.
			JLabel l = (JLabel) super.getTableCellRendererComponent( table, value, isSelected, hasFocus, row, col )

			// But change the background color if it is an invalid value.
			if( !isSelected ) {
				if( data[row]['validCode'] == MacChecksum.ValidationCode.BAD_CHECKSUM ) {
					l.setBackground( COLOR_BAD_CHECKSUM )
				} else if( data[row]['validCode'] == MacChecksum.ValidationCode.PLATFORM_MISMATCH ) {
					l.setBackground( COLOR_PLATFORM_MISMATCH )
				} else {
					// This is necessary because DefaultTableCellRenderer.getTableCellRendererComponent() has stupid code
					// that will set the default background to whatever the last setBackground was. So once we change it
					// to an error color, it will always be that color. Setting to null makes it use the correct default color.
					l.setBackground( null )
				}
			}

			// Make it bold if it is a duplicate.
			if( !value.toString().isEmpty() ) {
				long mac = Long.parseLong( value.toString(), 16 )
				def objs =  macids[mac]

				if( objs && objs.size() > 1 ) {
					l.setFont( l.getFont().deriveFont( Font.BOLD ) );
				}
			}

			return l
		}
	}
}
