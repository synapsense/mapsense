package DeploymentLab.Dialogs.Stats

import DeploymentLab.Image.ComponentIconFactory
import groovy.swing.SwingBuilder
import java.awt.BorderLayout
import java.awt.Dialog
import java.awt.FlowLayout
import java.awt.Font
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.awt.event.KeyEvent
import javax.swing.*

/**
 Displays the Project Stats in a dialog box.
 Wraps a JDialog as a delegate such that we can pretend a DeploymentStatsDialog is a JDialog in certain limited cases.
 We *could* extend the JDialog class, but that seemed to open a bigger can of worms than we wanted at the time.

 This should only ever be manipulated from the DeploymentStatsController class.

 @see DeploymentLab.Dialogs.Stats.DeploymentStatsController
 */
class DeploymentStatsDialog {
	/** Internal Swingbuilder used to construct the dialogDelegate. */
	protected SwingBuilder builder
	/** All the actual dialog work is delegated to the JDialog class. */
	private JDialog dialogDelegate
	/** The close button gets its own class-wide field to allow us to set the focus there when we need to. */
	private JButton closeButton
	/** The parent jframe to display relative to. */
	private JFrame parent
	/** Editor Pane that holds the stats text */
	private JEditorPane statsDialogText

	/** The constructor takes a single argument, the parent frame of this dialog.
	 */
	public DeploymentStatsDialog(JFrame _parent) {
		builder = new SwingBuilder()
		parent = _parent
	}

	/** Constructs the delegate and returns the JDialog ready for display.
	 */
	private void initDialog() {
		dialogDelegate = builder.dialog(owner: parent, resizable: true, title: 'Project Stats', layout: new BorderLayout(),
		                                iconImage: ComponentIconFactory.defaultIcon.getImage(),
		                                modalityType: Dialog.ModalityType.APPLICATION_MODAL ) {
			def exitAction = {dialogDelegate.setVisible(false)}
			def dlgPanel = panel(layout: new BorderLayout(), constraints: BorderLayout.CENTER) {
				//lifted from the old stats panel code
				scrollPane(preferredSize: [250, 300], constraints: BorderLayout.CENTER) {
					statsDialogText = editorPane(contentType: "text/html", editable: false, font: new Font("SansSerif", Font.PLAIN, 12))
				}
				panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.PAGE_END) {
					closeButton = button(text: 'Close', mnemonic: KeyEvent.VK_C, defaultButton: true, actionPerformed: { exitAction() })
				}
			}
			dlgPanel.registerKeyboardAction(exitAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			statsDialogText.registerKeyboardAction(exitAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		}
		setEscapeKeyMap();
		dialogDelegate.pack()
	}

	/**
	 * Installs the given text (assumed to be HTML) into the dialog, replacing what was there before.  Assumed to be called from DeploymentStatsController.
	 * @param t an HTML string of stats.
	 * @see DeploymentStatsController#showDialog
	 */
	public void setText(String t) {
		if (this.dialogDelegate == null) {
			initDialog();
		}
		statsDialogText.setText(t);
	}

	public void setTitle(String t){
		if (this.dialogDelegate == null) {
			initDialog();
		}
		dialogDelegate.setTitle(t);
	}
	/**
	 * Displays the dialog, initializing the dialog if needed.  Assumed to be called from DeploymentStatsController.
	 * @see DeploymentStatsController#showDialog
	 */
	public void show() {
		if (this.dialogDelegate == null) {
			initDialog();
		}
		this.dialogDelegate.requestFocusInWindow();
		this.dialogDelegate.setLocationRelativeTo(parent);
		this.dialogDelegate.show();
	}


	private void close() {
		this.dialogDelegate.setVisible(false);
		this.dispose();
	}

	public void dispose() {
		this.dialogDelegate.dispose();
		this.dialogDelegate = null;
	}

	private void setEscapeKeyMap() {
		String CANCEL_ACTION_KEY = "CANCEL_ACTION_KEY";
		int noModifiers = 0;
		KeyStroke escapeKey = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, noModifiers, false);
		InputMap inputMap = this.dialogDelegate.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(escapeKey, CANCEL_ACTION_KEY);
		AbstractAction cancelAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		};
		this.dialogDelegate.getRootPane().getActionMap().put(CANCEL_ACTION_KEY, cancelAction);
	}
}