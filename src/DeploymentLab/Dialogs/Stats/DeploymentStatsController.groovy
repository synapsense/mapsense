package DeploymentLab.Dialogs.Stats

import DeploymentLab.MapHelper
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentRole
import DeploymentLab.Model.ComponentType
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLObject
import DeploymentLab.channellogger.Logger
import javax.swing.JFrame
import com.google.common.annotations.VisibleForTesting
import DeploymentLab.Model.ModelChangeListener
import DeploymentLab.Model.MetamorphosisEvent
import DeploymentLab.Model.ModelState

/**
 * Acts as the controller for the DeploymentStatsDialog.
 *
 * Historically, this used to be a model change listener, but no longer; it now recomputes the whole stats on request.
 *
 * @see DeploymentStatsDialog
 */
class DeploymentStatsController implements ModelChangeListener {
	private static final Logger log = Logger.getLogger(DeploymentStatsController.class.getName())
	/**
	 * Map of [Zone component : [Type Display Name : # of things]
	 */
	private Map<DLComponent,Map<String,Integer>> drawings = [:]
	private Map<DLComponent,Map<String,Integer>> rooms = [:]
	private Map<DLComponent,Map<String,Integer>> zones = [:]
	private Map<DLComponent,Map<String,Integer>> wsnnetworks = [:]
	private Map<String, Set<DLComponent>> wsnByDrawing = [:]
	private Map<String,Integer> gateways = [:]
	private Map<String,Integer> totals = [:]
	private ComponentModel cmodel
	private DeploymentStatsDialog sd;
	private int wsnNodes

	/**
	 * Basic Constructor; we hand the ComponentModel and parent frame in now since those should never change.
	 * @param cm The ComponentModel that the stats will be based on
	 * @param parent The JFrame that will act as the parent to the dialog.
	 */
	DeploymentStatsController(ComponentModel cm, JFrame parent) {
		this.cmodel = cm;
		this.sd = new DeploymentStatsDialog(parent)
		this.cmodel.addModelChangeListener(this)
	}

	public void showDialog() {
		populate()
		sd.setTitle("Project Statistics")
		sd.setText(this.updateText())
		sd.show()
	}

	public void showWSNNetworkDialog(){
		populateWsn()
		sd.setTitle("Project Statistics by WSN Network")
		sd.setText(this.updateWSNNetworkText())
		sd.show()
	}

	/**
	 *  Constructs an HTML string holding the project statistics.
	 * @return An HTML representation of the stats.
	 */
	@VisibleForTesting
	protected String updateText() {
		StringBuilder sb = new StringBuilder()
		sb.append("<html>\n<h2>Project Statistics</h2>\n")
		sb.append("<h3>Gateways</h3>\n")
		sb.append(mapToHtmlTable(gateways))

		sb.append("<h3>Drawings</h3>\n")

		for(DLComponent d : drawings.keySet().sort{it.getName().toLowerCase()}){
			sb.append("&nbsp; &nbsp; <b><u>${d.getName()}</u></b>\n")
			sb.append(mapToHtmlTable(drawings[d]))
		}

		sb.append("<h3>Rooms</h3>\n")

		StringBuilder orphansb = new StringBuilder()

		for(DLComponent r : rooms.keySet().sort{it.getDrawing().getName().toLowerCase() + it.getName().toLowerCase()}){
			if( r.getType() == ComponentType.ORPHANAGE ) {
				if( !r.getChildComponents().isEmpty() ) {
					orphansb.append("&nbsp; &nbsp; <b><u>${r.getDrawing().getName()}</u></b>\n")
					orphansb.append(mapToHtmlTable(rooms[r]))
				}
			} else {
				sb.append("&nbsp; &nbsp; <b><u>${r.getDrawing().getName()} : ${r.getName()}</u></b>\n")
				sb.append(mapToHtmlTable(rooms[r]))
			}
		}

		if( orphansb.length() > 0 ) {
			sb.append("<h3>Not In A Room</h3>\n")
			sb.append( orphansb )
		}

		sb.append("<h3>Groupings</h3>\n")

		for(DLComponent z : zones.keySet().sort{it.getDrawing().getName().toLowerCase() + it.getName().toLowerCase()}){
			sb.append("&nbsp; &nbsp; <b><u>${z.getDrawing().getName()} : ${z.getName()}</u></b>\n")
			sb.append(mapToHtmlTable(zones[z]))
		}

		sb.append("<h3>Totals</h3>\n")
		sb.append(mapToHtmlTable(totals))

        sb.append("<table width='100%'>\n")
		sb.append("<tr><td>Total WSN Nodes</td><td align='right'>")
		sb.append(wsnNodes)
		sb.append("</td></tr>\n")
		sb.append("</table>\n")
		sb.append("</html>\n")
		return sb.toString()
	}

	protected String updateWSNNetworkText(){
		StringBuilder sb = new StringBuilder()
		sb.append("<html>\n<h2>Project Statistics by WSN Network</h2>\n")
		for(Map.Entry entry : wsnByDrawing){
			sb.append("<b>${entry.getKey()}</b>\n")
			sb.append("<table width='100%'>\n")
			sb.append("<tr><td>WSN Networks</td><td align='right'>")
			sb.append(entry.value.size())
			sb.append("</td></tr>\n")
			sb.append("</table>\n")
			sb.append("<br>\n")
			for (DLComponent n : entry.value){
				sb.append("<b><u>WSN Network : ${n.getName()}</u></b>\n")
				sb.append(mapToHtmlTable(wsnnetworks[n]))
				sb.append("<br>\n")
			}
			sb.append("<br>\n")
		}
		sb.append("</html>\n")
		return sb.toString()
	}

    protected String mapToHtmlTable(Map<String,Integer> map){
        StringBuilder sb = new StringBuilder();
        def keys
        keys = map.keySet().toList().sort()
        sb.append("<table width='100%'>\n")
	    //Put gateway first
	    String g = "Gateway"
	    if (keys.contains(g)){
		    keys.remove(g)
		    sb.append("<tr><td>WSN Gateways</td><td align='right'>${map[g]}</td></tr>\n")
	    }
        for (String k : keys) {
            sb.append("<tr><td>${k}</td><td align='right'>${map[k]}</td></tr>\n")
        }
        sb.append("</table>\n")
        return sb.toString();
    }

	/**
	 * Collect the information we care about from the model.
	 * These used to be filled in by modelchangelisteners, but now it's only done on demand.
	 */
	@VisibleForTesting
	protected void populate() {
		rooms = [:]
		zones = [:]
		gateways = [:]
		totals = [:]
		wsnNodes = 0
		drawings = [:]

		//read all containers first so we make sure the maps are initialized
		for (def c: cmodel.getComponentsInRole( ComponentRole.DRAWING )) {
			drawings[c] = [:]
		}

		for (def c: cmodel.getComponentsInRole( ComponentRole.ROOM )) {
			rooms[c] = [:]
		}

		for (def c: cmodel.getComponentsInRole("zone")) {
			zones[c] = [:]
		}

		// Don't include items that are just for modeling purposes or the containers.
		Set<String> ignoreList = [] as Set
		ignoreList += cmodel.getComponentTypesForFilter('Modeling')
		ignoreList += cmodel.listTypesWithRole( ComponentRole.ZONE )
		ignoreList += cmodel.listTypesWithRole( ComponentRole.ROOM )
		ignoreList += cmodel.listTypesWithRole( ComponentRole.DRAWING )

		for (DLComponent c: cmodel.getComponents()) {
			if( ignoreList.contains( c.getType() ) ) {
				continue
			}

			String ctName = c.getDisplaySetting('name')
			if (c.hasRole('gateway')) {
				MapHelper.increment(gateways, ctName)
			}

			// Don't count static child of staticchildren component
			if (c.hasRole('placeable') && !c.hasRole('staticchild')) {
				//check for a room parent
				MapHelper.increment(drawings[c.getDrawing()], ctName)

				//check for a room parent
				def parentRooms = c.getParentsOfRole( ComponentRole.ROOM )
				if (parentRooms.size() > 1) {
					log.warn("Component $c in more than one Room! $parentRooms")
				} else if (parentRooms.size() == 1) {
					MapHelper.increment(rooms[parentRooms.first()], ctName)
				}

				//check for a zone parent
				def parentZones = c.getParentsOfRole('zone')
				if (parentZones.size() > 1) {
					log.warn("Component $c in more than one Zone! $parentZones")
				} else if (parentZones.size() == 1) {
					MapHelper.increment(zones[parentZones.first()], ctName)
				}
			}
			// Don't count static child of staticchildren component
			if (!c.hasRole('staticchild')) {
				//println "about to increment '$ctName' based on $c"
				//println "but from the CM, it's '${cmodel.getDisplaySetting(c.getType(), 'name')}'"
				MapHelper.increment(totals, ctName)
			}
			def nodes = c.getObjectsOfType("wsnnode")
			wsnNodes += nodes.size()
		}
	}

	protected void populateWsn(){
		wsnByDrawing = [:]
		wsnnetworks = [:]
		for (def c: cmodel.getComponentsByType( ComponentType.WSN_NETWORK )) {
			String drawingName = c.getDrawing().getName()
			if (!wsnByDrawing[drawingName]){
				wsnByDrawing[drawingName] = []
			}
			wsnByDrawing[drawingName].add(c)
			wsnnetworks[c] = [:]
		}

		for (DLComponent c : cmodel.getComponents()) {
			String ctName = c.getDisplaySetting('name')

			//check for a wsnnetwork parents
			def parentsNetwork = c.getParentsOfType('network')
			if (parentsNetwork.size() > 1) {
				log.warn("Component $c in more than one Network! $parentsNetwork")
			} else if (parentsNetwork.size() == 1) {
				if (ctName.equals("Gateway")) {
					MapHelper.increment(wsnnetworks[parentsNetwork.first()], ctName)
				}
				for (DLObject dlo : c.getObjectsOfType("wsnnode")) {
					String pName = dlo.getObjectProperty("platformName").getValue().toString()
					MapHelper.increment(wsnnetworks[parentsNetwork.first()], pName)
				}
			}
		}
	}

	//ModelChangeListener : make this a listener again so we can flush on a model clear

	@Override
	void componentAdded(DLComponent component) {}

	@Override
	void componentRemoved(DLComponent component) {}

	@Override
	void childAdded(DLComponent parent, DLComponent child) {}

	@Override
	void childRemoved(DLComponent parent, DLComponent child) {}

	@Override
	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	@Override
	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {}

	@Override
	void modelMetamorphosisStarting(MetamorphosisEvent event) {}

	@Override
	void modelMetamorphosisFinished(MetamorphosisEvent event) {
		if( event.getMode() == ModelState.CLEARING ) {
			//drop all data : since these have references to the actual componnets, this can be a huge memory leak
			drawings = [:]
			rooms = [:]
			zones = [:]
			wsnnetworks = [:]
			wsnByDrawing = [:]
			gateways = [:]
			totals = [:]
			wsnNodes = 0
		}
	}
}