package DeploymentLab.Dialogs

import DeploymentLab.UserAccess
import DeploymentLab.channellogger.*
import DeploymentLab.UIDisplay
import DeploymentLab.SpringUtilities

import java.awt.BorderLayout
import java.awt.FlowLayout
import java.awt.GridBagConstraints
import java.awt.Dialog.ModalityType
import java.awt.event.ActionListener
import java.awt.event.KeyEvent

import javax.swing.JComponent
import javax.swing.JButton
import javax.swing.JComboBox
import javax.swing.JDialog
import javax.swing.JFrame
import javax.swing.JPasswordField
import javax.swing.KeyStroke
import javax.swing.SpringLayout

import groovy.swing.SwingBuilder

/**
* Dialog to change the current user level.
*/
class UserLevelDialog {
	private static final Logger log = Logger.getLogger(UserLevelDialog.class.getName())
	protected SwingBuilder builder
	private JDialog dialogDelegate
	private JButton okButton
	private JComboBox userLevelComboBox
	private JPasswordField accessCodeTxt
	private UIDisplay uiDisplay
	private UserAccess userAccess
	private JFrame parent
	
	public UserLevelDialog(UIDisplay _uiDisplay, UserAccess _userAccess, JFrame _parent) {
		builder = new SwingBuilder()
		uiDisplay = _uiDisplay
		userAccess = _userAccess
		parent = _parent
		dialogDelegate = this.getDialog()
	}

	private JDialog getDialog() {
		GridBagConstraints gbc = new GridBagConstraints()
		def dlgPanel
		dialogDelegate = builder.dialog(owner: parent, resizable: false, title: 'Change User Level', layout: new BorderLayout(), modalityType: ModalityType.APPLICATION_MODAL) {
		def okAction = {accessUserlevel()}
		def cancelAction = {dialogDelegate.setVisible(false)}
		dlgPanel = panel(layout: new SpringLayout(), constraints: BorderLayout.NORTH) {
				label(text: "Access Level: ")
				userLevelComboBox = comboBox(items:["Advanced","Expert"], selectedIndex:0, actionPerformed: { setPasswordEnabled() })
				label(text: "Access Code: ")
				accessCodeTxt = passwordField(text: "", columns: 8)
			}
			panel(layout: new FlowLayout(FlowLayout.TRAILING) ) {
				okButton = button(text: 'OK', defaultButton: true, actionPerformed: okAction)
				button(text: 'Cancel', actionPerformed: cancelAction)

			}
			dlgPanel.registerKeyboardAction(okAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			dlgPanel.registerKeyboardAction(cancelAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		}
		SpringUtilities.makeCompactGrid(dlgPanel,
                                2, 2,   //rows, cols
                                6, 6,   //initX, initY
                                5, 5);  //xPad, yPad
		dialogDelegate.pack()
		setPasswordEnabled()
		return dialogDelegate
	}

	private setPasswordEnabled() {
		accessCodeTxt.setEnabled( getSelectedUserLevel() > uiDisplay.getUserLevelId() )
	}

	public void setLocationRelativeTo() {
		dialogDelegate.setLocationRelativeTo(parent)
	}

	public void show() {
		accessCodeTxt.text = ""
		this.requestFocusInWindow()
		dialogDelegate.show()
	}

	public void requestFocusInWindow() {
		accessCodeTxt.requestFocusInWindow()
	}
	
	void accessUserlevel() {
		int selectedUserLevel = getSelectedUserLevel()

		// Only require the password when increasing the access level.
		if( ( selectedUserLevel <= uiDisplay.getUserLevelId() ) || userAccess.isMatchedAccessCode(selectedUserLevel, accessCodeTxt.text)){
			uiDisplay.setUserLevel(selectedUserLevel)
			dialogDelegate.hide()		
		}else{
			log.error("Incorrect access code.",'message')
			accessCodeTxt.text = ""
			accessCodeTxt.requestFocusInWindow()
		}
	}

	// Good gawd... if we ever expand this menu to have more options, make it so this magic calculation isn't necessary.
	private int getSelectedUserLevel() {
		userLevelComboBox.selectedIndex + 2
	}
}
