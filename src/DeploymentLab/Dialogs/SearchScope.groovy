package DeploymentLab.Dialogs

import DeploymentLab.CentralCatalogue

/**
 * Selects which of the three search scopes (entire file, active drawing, current selection) a given search will operate on.
 * @author Gabriel Helman
 * @since 
 * Date: 8/14/12
 * Time: 12:02 PM
 * @see SearchDialog
 * @see DeploymentLab.Dialogs.FindReplace.FindReplaceController
 */
public enum SearchScope {
	All("all"), Active("active"), Selection("selection");
	private final String display;

	private SearchScope(String s) {
		display = s;
	}

	@Override
	public String toString() {
		return CentralCatalogue.getUIS("searchScope.$display");
	}

	/**
	 * For easier use as the values in a combobox, this builds a Vector holding the values of this emum.
	 * @return a Vector<SearchScope> containing the contents returned from {@code SearchScope.values()}
	 */
	public static Vector<SearchScope> getValuesAsVector() {
		def result = new Vector<SearchScope>()
		result.addAll(SearchScope.values())
		return result
	}

}
