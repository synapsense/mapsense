package DeploymentLab.Dialogs

import DeploymentLab.*
import DeploymentLab.Export.DLObjectValueExporter
import DeploymentLab.Export.ExportController
import DeploymentLab.Export.ExportTaskType
import DeploymentLab.Image.ComponentIconFactory
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.ObjectModel
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.SmartZone.MergeLogicController
import DeploymentLab.SmartZone.MergeProcessCompleteListener
import DeploymentLab.channellogger.Logger
import com.google.common.collect.HashMultimap
import com.google.common.collect.Multimap
import groovy.swing.SwingBuilder

import javax.swing.*
import java.awt.*
import java.awt.event.ActionListener
import java.awt.event.KeyEvent

class PreExportValidationDialog {
	private static final Logger log = Logger.getLogger(PreExportValidationDialog.class.getName())
	protected SwingBuilder builder
	private JDialog dialogDelegate
	private JButton closeButton
	private JButton exportButton
	private JFrame parent
	private SelectModel selectModel
	private ComponentModel componentModel
	private ObjectModel objectModel
	private ProjectSettings projectSettings
	private DeploymentPanel dp
	private UserPreferences userPreferences
	private def dlw

	private MessageTable validatorMessageTable
	private MessageModel validatorMessageTableModel = new MessageModel()
	private JLabel warningLabel
	private JLabel errorLabel

	private MessageTable validatorInfoMessageTable
	private MessageModel validatorInfoMessageTableModel = new MessageModel()
	private JLabel infoLabel
	private JTabbedPane tabz

	private Validator validator
	private int validationRes
	private boolean mergeLogicComplete = false

	public PreExportValidationDialog(JFrame _parent, SelectModel _selectModel, ComponentModel _componentModel, ObjectModel _objectModel, ProjectSettings _ps, DeploymentPanel _dp, UserPreferences _up, def _dlw) {
		builder = new SwingBuilder()
		parent = _parent
		selectModel = _selectModel
		componentModel = _componentModel
		objectModel = _objectModel
		projectSettings = _ps
		dp = _dp
		userPreferences = _up
		dlw = _dlw

		validatorMessageTable = new MessageTable(validatorMessageTableModel)
		validatorMessageTable.setModel(selectModel, componentModel)

		validatorInfoMessageTable = new MessageTable(validatorInfoMessageTableModel)
		validatorInfoMessageTable.setModel(selectModel, componentModel)

		dialogDelegate = this.getDialog()
	}

	private JDialog getDialog() {
		dialogDelegate = builder.dialog(owner: parent, resizable: true, title: 'Project Validation', layout: new BorderLayout()) {
			def closeAction = {dialogDelegate.setVisible(false)}
			def refreshAction = {update()}
			def exportAction = {exportAction()}

			def clickedEvent = { e->
				if (SwingUtilities.isRightMouseButton(e)){
					def popupMenu = builder.popupMenu()
					popupMenu.add(DeploymentLab.Tools.PopupMenu.makeMenuItem('Export To CSV', {ae -> exportTable()}))
					popupMenu.add(DeploymentLab.Tools.PopupMenu.makeMenuItem('Locate in Plan', {ae -> locate()}))
					popupMenu.show(e.getComponent(), e.getX(), e.getY())
				}
			}

			panel(constraints: BorderLayout.NORTH, layout: new FlowLayout(FlowLayout.LEADING)) {
				errorLabel = builder.label('Total Failures: ')
				warningLabel = builder.label('Total Warnings: ')
				infoLabel = builder.label('Informational Messages: ')
			}

			tabz = builder.tabbedPane() {
				panel(title: "Warnings / Errors", layout: new BorderLayout()) {
					def messageScrollPane = scrollPane(constraints: BorderLayout.CENTER, border: BorderFactory.createEtchedBorder(), preferredSize: [600, 300], mouseClicked: clickedEvent) {
						widget(validatorMessageTable, mouseClicked: clickedEvent)
					}
				}
				panel(title: "Information", layout: new BorderLayout()) {
					scrollPane(constraints: BorderLayout.CENTER, border: BorderFactory.createEtchedBorder(), preferredSize: [600, 300]) {
						widget(validatorInfoMessageTable)
					}
				}
			}

			def messagePanel = panel(constraints: BorderLayout.SOUTH, layout: new FlowLayout(FlowLayout.TRAILING)) {
				exportButton = button(text: "Sync with Server", defaultButton: true, actionPerformed: exportAction)
				button(text: 'Locate in Plan', actionPerformed: { this.locate() })
				button(text: 'Refresh', actionPerformed: refreshAction)
				closeButton = button(text: 'Close', mnemonic: KeyEvent.VK_C, actionPerformed: closeAction)
			}

			messagePanel.registerKeyboardAction(exportAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			messagePanel.registerKeyboardAction(closeAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		}

		//Nimbus puts the icon on the right side of the text on a tab with no space inbetween.
		//So, we replace the default tab with a label of our own devising
		JLabel lbl = new JLabel(CentralCatalogue.getUIS("preExportValidationDialog.tab.error"));
		Icon icon = ComponentIconFactory.getWarningIcon()
		lbl.setIcon(icon);
		lbl.setIconTextGap(5);
		lbl.setHorizontalTextPosition(SwingConstants.RIGHT);
		tabz.setTabComponentAt(0, lbl);

		lbl = new JLabel(CentralCatalogue.getUIS("preExportValidationDialog.tab.info"));
		icon = ComponentIconFactory.getInfoIcon()
		lbl.setIcon(icon);
		lbl.setIconTextGap(5);
		lbl.setHorizontalTextPosition(SwingConstants.RIGHT);
		tabz.setTabComponentAt(1, lbl);


		dialogDelegate.pack()
		return dialogDelegate
	}

	public Validator doPreExportValidation() {
		validator = new Validator(objectModel, componentModel, projectSettings)
		validationRes = validator.validate()
		return validator
	}

	private update() {
		parent.setCursor(Cursor.WAIT_CURSOR)
		Validator v = doPreExportValidation()
		def warnings = v.getWarnings()
		def failures = v.getFailures()
		def infos = v.getInfos()
		errorLabel.setText(CentralCatalogue.getUIS("preExportValidationDialog.TotalErrors") + " " + failures.size())
		warningLabel.setText(CentralCatalogue.getUIS("preExportValidationDialog.TotalWarnings") + " " + warnings.size())
		validatorMessageTableModel.updateMessage(v)
		validatorInfoMessageTableModel.updateInfoMessage(v)
		infoLabel.setText(CentralCatalogue.getUIS("preExportValidationDialog.TotalInfos") + " " + infos.size())

		parent.setCursor(null)

		if (!dialogDelegate.isVisible()) {
			if (warnings.size() + failures.size() == 0) {
				//make the info tab on top
				tabz.setSelectedIndex(1)
			} else {
				tabz.setSelectedIndex(0)
			}
			this.requestFocusInWindow()
			dialogDelegate.setLocationRelativeTo(parent)
			dialogDelegate.setVisible(true)
		}
	}

	public void show() {
		this.update()
	}

	public void requestFocusInWindow() {
		exportButton.requestFocusInWindow()
	}

	public void locate() {
		//the table itself handles the selection of the nodes on the plan, we just need to scroll there
		ArrayList<DLComponent> selected = selectModel.getExpandedSelection()
		def rows
		if (tabz.getSelectedIndex() == 0) {
			rows = validatorMessageTable.getSelectedRowCount()
		} else {
			rows = validatorInfoMessageTable.getSelectedRowCount()
		}

        if( selected.size() == 0 ) {
            // Might be containers that are selected, so at least get them so we can change to the drawing if needed.
            selected = selectModel.getBaseSelection()
        }

		if (selected.size() > 0) {
            // We can only show one drawing at a time, so create a mapping of the selected items per drawing.
            Multimap<DLComponent,DLComponent> selectedPerDrawing = HashMultimap.create()
            for( DLComponent c : selected ) {
                selectedPerDrawing.put( c.getDrawing(), c )
            }

            Collection<DLComponent> showComponents = []

            // If we have some on the active drawing, use those. Otherwise, pick the one with the most.
            if( selectedPerDrawing.containsKey( selectModel.getActiveDrawing() ) ) {
                showComponents = selectedPerDrawing.get( selectModel.getActiveDrawing() )
            } else {
                DLComponent showDrawing = null
                for( DLComponent drawing : selectedPerDrawing.keySet() ) {
                    Set<DLComponent> components = selectedPerDrawing.get( drawing )
                    if( components.size() > showComponents.size() ) {
                        showDrawing = drawing
                        showComponents = components
                    }
                }
                // Change to the new drawing.
                selectModel.setActiveDrawing( showDrawing )
            }

            dp.scrollToView( showComponents )

		} else if (rows == 0) {
			//this message should show only if the user didn't select something
			log.info(CentralCatalogue.getUIS("preExportValidationDialog.NoMessage"), 'statusbar')
		}
	}

	public void exportTable() {
		userPreferences.setUserPreference(UserPreferences.CSV_TITLE, CentralCatalogue.getUIS("exportSearch.filechooser.title"))
		DLObjectValueExporter.exportTable(userPreferences, validatorMessageTableModel.getVisibleData(), dialogDelegate)
	}

	private void exportAction(){

		if (dlw.openFile == null) {
			log.error("Please save the deployment before exporting.", 'message')
			return
		}

		ExportController ec = new ExportController(CentralCatalogue.getOpenProject())

		if (validationRes == Validator.RESULT_PASS) {
			dialogDelegate.setVisible(false)
			syncWithServer(ec)
			return
		}

		if (validationRes == Validator.RESULT_ABORT) {
			log.info("The validation process failed.  This is bad!", 'message')
			return
		}

		def warnings = validator.getWarnings()
		def failures = validator.getFailures()

		if (warnings.size() > 0 && failures.size() > 0) {
			log.error("Validation failed with " + failures.size() + " errors and " + warnings.size() + " warnings.", 'message')
		} else if (failures.size() > 0) {
			log.error("Validation failed with " + failures.size() + " errors.", 'message')
		} else { // warnings only
			int res = JOptionPane.showConfirmDialog(parent,"Validation produced " + warnings.size() + " warnings. Export anyway?","Confirm Export",0, JOptionPane.QUESTION_MESSAGE)
			if (res == JOptionPane.OK_OPTION) {
				dialogDelegate.setVisible(false)
				syncWithServer(ec)
			}
		}
	}

	private void syncWithServer(ExportController ec) {
		Properties connectProperties = new Properties()
		if (projectSettings.smartZoneHost != null && !"".equals(projectSettings.smartZoneHost)) {
			ProgressManager.doTask {
				if (!mergeLogicComplete) {
					//Check to show the optional pull dialog
					if (dlw.isSyncWithSmartZone) {
						dlw.syncWithSZServer()
						if (dlw.syncWithServerCancelled) {
							//On cancelling the pull, setting the conflicts as resolved to continue with the flow
							MergeLogicController.conflictsResolved = true
						}
					} else {
						dlw.mergeFloorplan()
					}
				}
				if (dlw.locationFound) {
					if (dlw.mergeController) {
						dlw.mergeController.setProcessCompleteListener(new MergeProcessCompleteListener() {
							@Override
							public void isComplete() {
								mergeLogicComplete = true
								export()
							}
						})
					}
				}
				if (MergeLogicController.isConflictsResolved()) {
					saveToServer(ec)
					resetAfterSave()
				}
			}

		} else {
			if (dlw.loginDialog.getLoginInfo(connectProperties)) {
				ProgressManager.doTask {
					ec.loginESDoExportTask(connectProperties, ExportTaskType.EXPORT, false, true)
				}
			}
		}
	}

	private void resetAfterSave() {
		mergeLogicComplete = false
		MergeLogicController.conflictsResolved = false
	}

	private void saveToServer(ExportController ec) {
		dlw.fileSaveLocal()
		Properties connectProperties = new Properties()
		dlw.saveToSmartZone()
		if (dlw.saveSuccessful) {
			dlw.hideProgressPane()
			if (dlw.loginDialog.getLoginInfo(connectProperties)) {
				dlw.showProgressPane()
				ec.loginESDoExportTask(connectProperties, ExportTaskType.EXPORT, false, true)
			}
		}
	}

	public void export(){
		this.show()
		this.exportAction()
	}

}