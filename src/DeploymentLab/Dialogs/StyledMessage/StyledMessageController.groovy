package DeploymentLab.Dialogs.StyledMessage

import DeploymentLab.channellogger.Logger
import com.google.common.annotations.VisibleForTesting

/**
 * Displays a dialog box holding text styled via HTML.  Generally assumed to be used via the StyledMessageLogger.
 * In the future, this could be expanded to use other style options than HTML.
 * Note also this is *swing* html, which means html 3.whatever from the dark ages.
 *
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/23/12
 * Time: 10:25 AM
 * @see DeploymentLab.channellogger.StyledMessageLogger
 */
class StyledMessageController {
	private static final Logger log = Logger.getLogger(StyledMessageController.class.getName());
	private StyledMessageDialog dialog;
	private String contents
	private String messageTitle
	private int messageType

	/**
	 * One stop shop for getting a styled message.
	 * @param contents The contents, optionally formatted in HTML, that will be displayed
	 * @param messageTitle The dialog title to use
	 * @param messageType an integer designating the kind of message this is; primarily used to determine the icon from the pluggable Look and Feel: ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE, QUESTION_MESSAGE, or PLAIN_MESSAGE (as in JOptionPane)
	 * @see javax.swing.JOptionPane#ERROR_MESSAGE
	 * @see javax.swing.JOptionPane#INFORMATION_MESSAGE
	 * @see javax.swing.JOptionPane#WARNING_MESSAGE
	 * @see javax.swing.JOptionPane#QUESTION_MESSAGE
	 * @see javax.swing.JOptionPane#PLAIN_MESSAGE
	 */
	public static showStyledMessage(String contents, String messageTitle, int messageType){
		new StyledMessageController(contents,messageTitle,messageType).showDialog()
	}


	protected StyledMessageController(String contents, String messageTitle, int messageType) {
		this.contents = contents
		this.messageTitle = messageTitle
		this.messageType = messageType
	}

	protected showDialog() {
		this.dialog = new StyledMessageDialog(this);
		this.dialog.show();
	}

	protected void ok() {
		this.finishUp()
	}

	protected void finishUp() {
		//clean up before we bail out of here.
		if (dialog != null) {
			dialog.close();
			dialog.dispose();
			dialog = null;
		}
	}

	protected String getContents() {
		return contents
	}

	protected String getMessageTitle() {
		return messageTitle
	}


	protected int getMessageType() {
		return messageType
	}


	@VisibleForTesting
	protected StyledMessageDialog getDialog(){
		if(dialog == null){
			this.dialog = new StyledMessageDialog(this);
		}
		return this.dialog;
	}
}
