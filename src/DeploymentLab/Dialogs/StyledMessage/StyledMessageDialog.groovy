package DeploymentLab.Dialogs.StyledMessage

import DeploymentLab.CentralCatalogue
import DeploymentLab.channellogger.Logger
import com.google.common.annotations.VisibleForTesting
import groovy.swing.SwingBuilder

import java.awt.event.ActionEvent
import java.awt.event.KeyEvent
import javax.swing.text.html.HTMLEditorKit
import java.awt.*
import javax.swing.*

/**
 *
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/23/12
 * Time: 10:25 AM
 */
class StyledMessageDialog {
	private static final Logger log = Logger.getLogger(StyledMessageDialog.class.getName())
	/** Internal SwingBuilder used to construct the dialogDelegate.  */
	protected SwingBuilder builder;
	/** All the actual dialog work is delegated to the JDialog class.  */
	protected JDialog dialogDelegate;

	private JButton okButton

	private JTextPane scrollingMessageText

	private StyledMessageController controller;

	/**
	 * Prepare a dialog.
	 * @param delegate The StyledMessageController that will be running the show.
	 */
	protected StyledMessageDialog(StyledMessageController delegate) {
		this.builder = new SwingBuilder()
		controller = delegate;
	}

	/**
	 * Construct the dialog.
	 */
	protected void initDialog() {
		dialogDelegate = builder.dialog(name: "styledMessage", owner: CentralCatalogue.getParentFrame(), resizable: true, title: controller.getMessageTitle(), layout: new BorderLayout(), modalityType: Dialog.ModalityType.APPLICATION_MODAL) {
			scrollPane(constraints: BorderLayout.CENTER, horizontalScrollBarPolicy: JScrollPane.HORIZONTAL_SCROLLBAR_NEVER ) {
				scrollingMessageText = textPane(editable: false, editorKit: new HTMLEditorKit())
			}
			panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.SOUTH) {
				okButton = builder.button(name: "okButton", text: 'OK', defaultButton: true, actionPerformed: { this.ok() })
			}
		}

		scrollingMessageText.setText(controller.getContents())

		//set the size of the main panel
		//use the default "preferred size" unless the height is more than 1/2 the screen height; then clamp it at that
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		def maxSize = new Dimension()
		def totalHeight = dialogDelegate.getPreferredSize().getHeight()
		def finalHeight
		if (dim.getHeight() / 2 < totalHeight) {
			finalHeight = dim.getHeight() / 2
		} else {
			finalHeight = totalHeight
		}
		def finalWidth = dialogDelegate.getPreferredSize().getWidth()
		maxSize.setSize(finalWidth, finalHeight)
		dialogDelegate.setPreferredSize(maxSize)

		setEscapeKeyMap();
		this.dialogDelegate.pack();
		this.dialogDelegate.setLocationRelativeTo(CentralCatalogue.getParentFrame());
	}

	/**
	 * Tell the controller "ok"
	 */
	protected void ok() {
		controller.ok()
	}

	/**
	 * Show the dialog.
	 */
	protected void show() {
		if (this.dialogDelegate == null) {
			initDialog()
		}
		this.dialogDelegate.requestFocusInWindow();
		this.dialogDelegate.show();
	}

	@VisibleForTesting
	protected JDialog getDialog() {
		if (this.dialogDelegate == null) {
			initDialog()
		}
		return dialogDelegate
	}

	/**
	 * Close the dialog.
	 */
	protected void close() {
		this.dialogDelegate.setVisible(false);
		//this.dialogDelegate.dispose();
		//this.dialogDelegate = null;
	}

	/**
	 * Jettison the dialog.
	 */
	protected void dispose() {
		this.dialogDelegate.dispose();
		this.dialogDelegate = null;
	}

	private void setEscapeKeyMap() {
		String CANCEL_ACTION_KEY = "CANCEL_ACTION_KEY";
		int noModifiers = 0;
		KeyStroke escapeKey = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, noModifiers, false);
		InputMap inputMap = this.dialogDelegate.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(escapeKey, CANCEL_ACTION_KEY);
		AbstractAction cancelAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		};
		this.dialogDelegate.getRootPane().getActionMap().put(CANCEL_ACTION_KEY, cancelAction);
	}

}
