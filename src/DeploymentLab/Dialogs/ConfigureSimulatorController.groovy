package DeploymentLab.Dialogs

import DeploymentLab.CentralCatalogue
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.ObjectModel
import DeploymentLab.ProjectSettings
import DeploymentLab.Quadruple
import DeploymentLab.channellogger.Logger
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import javax.swing.JFrame
import javax.swing.event.TableModelEvent
import javax.swing.event.TableModelListener
import javax.swing.table.DefaultTableModel

/**
 * Controller for the new simulator control board
 * @author Gabriel Helman
 * @since Mars
 * Date: 8/31/11
 * Time: 11:18 AM
 */
class ConfigureSimulatorController implements ActionListener, TableModelListener {
	private static final Logger log = Logger.getLogger(ConfigureSimulatorController.class.getName())
	private ConfigureSimulatorDialog dialog;
	private ComponentModel cm;
	private ObjectModel om;
	private ProjectSettings ps;
	private static ComponentModel fakeModel
	private Boolean saveChanges


	public ConfigureSimulatorController(ComponentModel cmodel, ObjectModel omodel, ProjectSettings settings) {
		this.cm = cmodel
		this.om = omodel
		this.ps = settings
	}

	public void applyChanges(boolean c) {
		saveChanges = c
	}

	public showDialog(JFrame parentFrame) {
		ConfigureSimulatorTableModel tm = buildTableModel()
		tm.addTableModelListener(this)
		saveChanges = false
		this.dialog = new ConfigureSimulatorDialog(parentFrame, this, tm)
		this.dialog.show()
		if (saveChanges) {
			ps.configuredSimulator = []
			tm.getDataAsList().each {
				if( it["value"].trim().size() > 0 ){
					ps.configuredSimulator.add(it)
				}
			}
		}
		//clean up before we bail out of here.
		if (dialog) {
			dialog.dispose()
			dialog = null
		}
	}

	private ConfigureSimulatorTableModel buildTableModel() {
		def columns = ["Component Name", "sensor name", "value", "time", "dlid"]
		List<Map<String, String>> data = []
		//find the wsnsensors in the current model
		om.getObjectsByType("wsnsensor").each { o ->
			Map<String, String> currentMap = [:]
			//println "sensor $o"
			//println "owner ${cm.getManagingComponent(o)}"
			currentMap["Component Name"] = cm.getManagingComponent(o).getName()
			currentMap["sensor name"] = o.getObjectProperty("name").getValue()
			currentMap["dlid"] = o.getDlid().toString()
			currentMap["value"] = ""
			currentMap["time"] = ""
			ps.configuredSimulator.each { m ->
				//println "comparing '${m["dlid"]}' to '${currentMap["dlid"]}'"
				if (m["dlid"].equalsIgnoreCase(currentMap["dlid"])) {
					//println "storing ${m["value"]}"
					currentMap["value"] = m["value"]
					currentMap["time"] = m["time"]
				}
			}
			data.add(currentMap)
		}
		def tm = new ConfigureSimulatorTableModel(data, columns, [2, 3]);
		return tm;
	}

	/**
	 * Handles callbacks from the dialog
	 * @param e
	 */
	@Override
	void actionPerformed(ActionEvent e) {

	}


	@Override
	void tableChanged(TableModelEvent e) {
		//listen for changed data on the table
		println e
		println "col: ${e.getColumn()}  frow: ${e.getFirstRow()}  lrow: ${e.getLastRow()}  type: ${e.getType()}"
	}
}


public class ConfigureSimulatorTableModel extends DefaultTableModel {
	private List<Integer> editableColumns
	private Object[][] data //[row][column]

	def ConfigureSimulatorTableModel(Object[][] data, Object[] columnNames, List<Integer> editableColumns) {
		super(data, columnNames)
		this.editableColumns = editableColumns
	}

	def ConfigureSimulatorTableModel(List<Map<String, String>> mapdata, List columnNamesList, List<Integer> editableColumns) {
		Object[] columnNames = columnNamesList.toArray()
		//turn that map into a 2d array
		data = new Object[mapdata.size()][columnNames.size()];
		mapdata.eachWithIndex { m, i ->
			for (int c = 0; c < columnNames.length; c++) {
				data[i][c] = m[columnNames[c]]
			}

		}
		//this is what super() calls
		setDataVector(data, columnNames);
		this.editableColumns = editableColumns
	}

	@Override
	boolean isCellEditable(int row, int column) {
		if (editableColumns.contains(column)) {
			return true
		} else {
			return false
		}
	}

	public Map<String, String> getMapFromRow(int row) {
		Map<String, String> result = new HashMap<String, String>()
		for (int i = 0; i < this.getColumnCount(); i++) {
			result[this.getColumnName(i)] = this.getValueAt(row, i)
		}
		return result
	}

	public Object[][] getData() {
		return data
	}

	public List<Map<String, String>> getDataAsList() {
		List<Map<String, String>> result = []
		for (int row = 0; row < this.getRowCount(); row++) {
			result.add(this.getMapFromRow(row))
		}
		return result
	}


}

