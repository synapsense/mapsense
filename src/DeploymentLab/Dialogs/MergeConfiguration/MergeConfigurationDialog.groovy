package DeploymentLab.Dialogs.MergeConfiguration

import DeploymentLab.SmartZone.MergeLogicController
import DeploymentLab.channellogger.Logger
import groovy.swing.SwingBuilder;
import javax.swing.*;
import javax.swing.border.TitledBorder
import javax.swing.table.JTableHeader
import javax.swing.table.TableCellRenderer
import javax.swing.table.TableColumn
import java.awt.*
import java.awt.event.ActionListener
import java.awt.event.ItemEvent
import java.awt.event.ItemListener
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent

/**
 * Created by LNI on 2/18/2016.
 */

public class MergeConfigurationDialog {
    protected SwingBuilder builder
    private JDialog mergeDialog
    private JButton cancelButton
    private JButton continueButton
    private JFrame parent
    private JPanel panel = new JPanel();
    private JTable tableLeft
    private JTable tableRight
    private MergeModel modelLeft
    private MergeModel modelRight

    private RadioHeaderRenderer radioButtonHeaderLeft = new RadioHeaderRenderer("Select all SmartZone versions")
    private RadioHeaderRenderer radioButtonHeaderRight = new RadioHeaderRenderer("Select all Local versions")

    private static final int SELECTION_COL_WIDTH = 25
    private static final Logger log = Logger.getLogger(MergeLogicController.class.getName())

    public MergeConfigurationDialog(JFrame _parent, MergeModel modelRemote, MergeModel modelLocal) {
        this.builder = new SwingBuilder()
        this.parent = _parent

        createTable(modelRemote)
        createTable(modelLocal)

        mergeDialog = this.getDialog()
    }

    private JDialog getDialog() {
        mergeDialog = builder.dialog(owner: parent, resizable: true, title: 'Reconcile Configuration Changes', layout:
                new BorderLayout()) {
            LayoutManager layout = new BoxLayout(panel, BoxLayout.X_AXIS);
            panel.setLayout(layout)
            panel.setBorder(BorderFactory.createEtchedBorder())

            panel(constraints: BorderLayout.SOUTH, layout: new FlowLayout(FlowLayout.TRAILING)) {
                continueButton = button(text: "Continue", defaultButton: true)
                cancelButton = button(text: "Cancel", defaultButton: true)
            }
        }
        mergeDialog.pack()
        return mergeDialog
    }

    public void show() {
        JScrollPane scrollerLeft = new JScrollPane(tableLeft)
        JScrollPane scrollerRight = new JScrollPane(tableRight)

        scrollerLeft.setBorder(BorderFactory.createTitledBorder (BorderFactory.createEmptyBorder (),
                "SmartZone Server",
                TitledBorder.CENTER,
                TitledBorder.TOP))
        scrollerRight.setBorder(BorderFactory.createTitledBorder (BorderFactory.createEmptyBorder (),
                "Local",
                TitledBorder.CENTER,
                TitledBorder.TOP))
        scrollerRight.getVerticalScrollBar().setModel(
                scrollerLeft.getVerticalScrollBar().getModel());
        scrollerRight.setVerticalScrollBar(
                scrollerLeft.getVerticalScrollBar());

        panel.add(scrollerLeft);
        panel.add(scrollerRight);

        mergeDialog.getContentPane().add(panel, BorderLayout.CENTER)
        mergeDialog.setMinimumSize(new Dimension(500, 300))
        mergeDialog.setSize(950, 400)
        mergeDialog.setLocationRelativeTo(parent)
        mergeDialog.setVisible(true)
    }

    public boolean hasUnSelectedRows() {
        int rowsSelected = getSelectedRowsLeft().size() + getSelectedRowsRight().size()
        if (rowsSelected != tableRight.getRowCount()) {
            return true
        }
        return false
    }

    public JDialog getMergeDialog() {
        return mergeDialog
    }

    public JButton getContinueButton() {
        return continueButton
    }

    public JButton getCancelButton() {
        return cancelButton
    }

    public void close() {
        mergeDialog.dispose()
    }

    private void createTable(MergeModel model) {
        JTable table = new JTable(model)
        int radioButtonColNum

        table.getTableHeader().setReorderingAllowed(false)
        table.setRowHeight(20)

        if (model.isLeftSide()) {
            tableLeft = table
            radioButtonColNum = model.getColumnCount() - 1
            modelLeft = model
            tableLeft.getColumnModel().getColumn(radioButtonColNum).setHeaderRenderer(radioButtonHeaderLeft)
            addRowSelectionListenerLeft()
            addSelectionHeaderListenerLeft()
        } else {
            tableRight = table
            radioButtonColNum = 0
            modelRight = model
            tableRight.getColumnModel().getColumn(radioButtonColNum).setHeaderRenderer(radioButtonHeaderRight)
            addRowSelectionListenerRight()
            addSelectionHeaderListenerRight()
        }

        TableColumn radioButtonCol = table.getColumnModel().getColumn(radioButtonColNum);
        radioButtonCol.setMaxWidth(SELECTION_COL_WIDTH);
        radioButtonCol.setMinWidth(SELECTION_COL_WIDTH)

        table.getColumnModel().getColumn(radioButtonColNum).setCellRenderer(new RadioButtonRenderer());
        table.getColumnModel().getColumn(radioButtonColNum).setCellEditor(new RadioButtonEditor(new JCheckBox()));
    }

    private void addRowSelectionListenerLeft() {
        // This is the key listener to navigate between tables.
        tableLeft.addKeyListener(new KeyAdapter() {
            @Override
            void keyPressed(KeyEvent e) {
                int keyCode = e.getKeyCode();

                if((keyCode==KeyEvent.VK_RIGHT)) {
                    int row = tableLeft.getSelectedRow()
                    tableRight.setRowSelectionInterval(row, row)
                    tableRight.requestFocus()
                }

                if (keyCode==KeyEvent.VK_SPACE) {
                    tableLeftListenerChanges()
                }
            }
        })

        // This is the selection listener for the radio button selection on the table
        tableLeft.addMouseListener(new MouseAdapter() {
            @Override
            void mousePressed(MouseEvent e) {
                tableLeftListenerChanges()
            }
        });
    }

    private void tableLeftListenerChanges() {
        int row = tableLeft.getSelectedRow()
        int column = tableLeft.getSelectedColumn()
        if(row != -1) {
            MergeConflictData rowData = modelLeft.getTableDataList().get(row)
            MergeLogicController.updateDLComponent(rowData.getLocalComponent(), true)
            modelRight.getTableDataList().get(row).getTableProperties().set(0, false)
            tableLeft.setValueAt(true, row, column)
            tableRight.clearSelection()
            radioButtonHeaderRight.setSelected(false)
            if (allRowsSelected(modelLeft, modelLeft.getColumnCount() - 1)) {
                radioButtonHeaderLeft.setSelected(true)
            }
            refresh()
        }
    }

    private void addRowSelectionListenerRight() {
        // This is the key listener to navigate between tables.
        tableRight.addKeyListener(new KeyAdapter() {
            @Override
            void keyPressed(KeyEvent e) {
                int keyCode = e.getKeyCode();
                if((keyCode==KeyEvent.VK_LEFT)) {
                    int row = tableRight.getSelectedRow()
                    tableLeft.setRowSelectionInterval(row, row)
                    tableLeft.requestFocus()
                }

                if (keyCode==KeyEvent.VK_SPACE) {
                    tableRightListenerChanges()
                }
            }
        })

        // This is the selection listener for the radio button selection on the table
        tableRight.addMouseListener(new MouseAdapter() {
            @Override
            void mousePressed(MouseEvent e) {
                tableRightListenerChanges()
            }
        })
    }

    private void tableRightListenerChanges() {
        int row = tableRight.getSelectedRow()
        int column = tableRight.getSelectedColumn()
        if (row != -1) {
            MergeConflictData rowData = modelRight.getTableDataList().get(row)
            MergeLogicController.updateDLComponent(rowData.getLocalComponent(), false)
            modelLeft.getTableDataList().get(row).getTableProperties().set(modelLeft.getColumnCount() - 1, false)
            tableRight.setValueAt(true, row, column)
            tableLeft.clearSelection()
            radioButtonHeaderLeft.setSelected(false)
            if (allRowsSelected(modelRight, 0)) {
                radioButtonHeaderRight.setSelected(true)
            }
            refresh()
        }
    }

    private void addSelectionHeaderListenerLeft() {
        JTableHeader header = tableLeft.getTableHeader()
        header.addMouseListener(new MouseAdapter() {
            @Override
            void mousePressed(MouseEvent e) {
                int col = header.columnAtPoint(e.getPoint());
                 if (col == tableLeft.getColumnCount() - 1) {
                     for (int i = 0; i < tableLeft.getRowCount(); i++) {
                         MergeConflictData rowData = modelLeft.getTableDataList().get(i)
                         MergeLogicController.updateDLComponent(rowData.getLocalComponent(), true)
                         modelRight.getTableDataList().get(i).getTableProperties().set(0, false)
                         modelLeft.getTableDataList().get(i).getTableProperties().set(modelLeft.getColumnCount() - 1, true)
                     }
                     radioButtonHeaderRight.setSelected(false)
                     radioButtonHeaderLeft.setSelected(true)
                }
                refresh()
            }
        })
    }

    private void addSelectionHeaderListenerRight() {
        JTableHeader header = tableRight.getTableHeader()
        header.addMouseListener(new MouseAdapter() {
            @Override
            void mousePressed(MouseEvent e) {
                int col = header.columnAtPoint(e.getPoint());
                if (col == 0) {
                    for (int i = 0; i < tableRight.getRowCount(); i++) {
                        MergeConflictData rowData = modelRight.getTableDataList().get(i)
                        MergeLogicController.updateDLComponent(rowData.getLocalComponent(), false)
                        modelRight.getTableDataList().get(i).getTableProperties().set(0, true)
                        modelLeft.getTableDataList().get(i).getTableProperties().set(modelLeft.getColumnCount() - 1, false)
                    }
                    radioButtonHeaderRight.setSelected(true)
                    radioButtonHeaderLeft.setSelected(false)
                }
                refresh()
            }
        })
    }

    private void refresh() {
        parent.getContentPane().repaint()
        mergeDialog.getContentPane().repaint()
    }

    private static boolean allRowsSelected(MergeModel model, int selectionCol){
        boolean allSelected = true
        for (MergeConflictData eachRow : model.getTableDataList()) {
            if (!eachRow.getTableProperties().get(selectionCol)){
                allSelected = false
                break
            }
        }
        return allSelected
    }

    private ArrayList getSelectedRowsLeft() {
        def selectedRows = new ArrayList<MergeConflictData>()
        for (MergeConflictData eachRow : modelLeft.getTableDataList()) {
            if (eachRow.getTableProperties().get(tableLeft.getColumnCount()-1)) {
                selectedRows.add(eachRow)
            }
        }
        return selectedRows
    }

    private ArrayList getSelectedRowsRight() {
        def selectedRows = new ArrayList<MergeConflictData>()
        for (MergeConflictData eachRow : modelRight.getTableDataList()) {
            if (eachRow.getTableProperties().get(0)) {
                selectedRows.add(eachRow)
            }
        }
        return selectedRows
    }

/** Renderer class for JRadioButton **/
    private class RadioButtonRenderer extends JRadioButton implements TableCellRenderer {
        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus, int row, int column) {

            if (value == null)
                return null;
            setHorizontalAlignment(JRadioButton.CENTER)
            setSelected((Boolean) value);
            return this;
        }
    }

/** Editor class for JRadioButton **/
    private class RadioButtonEditor extends DefaultCellEditor implements ItemListener {
        public JRadioButton btn = new JRadioButton();

        public RadioButtonEditor(JCheckBox checkBox) {
            super(checkBox);
        }

        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
            if (value == null)
                return null;

            btn.addItemListener(this);

            if (((Boolean) value).booleanValue())
                btn.setSelected(true);
            else
                btn.setSelected(false);
            return btn;
        }

        public Object getCellEditorValue() {
            if(btn.isSelected())
                return new Boolean(true);
            else
                return new Boolean(false);
        }

        public void itemStateChanged(ItemEvent e) {
            super.fireEditingStopped();
        }
    }

    class RadioHeaderRenderer extends JRadioButton implements TableCellRenderer{
        String toolTip
        public RadioHeaderRenderer(String toolTip) {
            this.toolTip = toolTip
        }
        public Component getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {
            setHorizontalAlignment(JRadioButton.CENTER)
            setToolTipText(this.toolTip)
            getModel().setPressed(true);
            getModel().setArmed(true);
            return this;
        }
    }
}