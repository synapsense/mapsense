package DeploymentLab.Dialogs.MergeConfiguration

import DeploymentLab.Model.DLComponent
import javax.swing.table.AbstractTableModel

/**
 * Created by LNI on 2/23/2016.
 */

/**
 * This class represents the table model for merging configurations from two different systems.
 * isLeftSide boolean value decides which table is presented on the left side of the other. This model assumes that the
 * radio button selection column is the last column for the table on the left and is the first column for the table on
 * the right.
 */
class MergeModel extends AbstractTableModel{
    private boolean isLeftSide
    private def tableDataList = new ArrayList<MergeConflictData>()
    private String[] columnNamesList = []

    public MergeModel(boolean isRemoteSide) {
        this.isLeftSide = isRemoteSide
    }

    boolean isLeftSide() {
        return this.isLeftSide
    }

    def getTableDataList() {
        return this.tableDataList
    }

    void setTableDataList(tableDataList) {
        this.tableDataList = tableDataList
    }

    void setColumnNamesList(columnNamesList) {
        this.columnNamesList = columnNamesList
    }

    @Override
    public String getColumnName(int column) {
        return columnNamesList[column];
    }

    @Override
    public int getRowCount() {
        return tableDataList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNamesList.size();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (isLeftSide()) {
            return ((columnIndex == getColumnCount()-1) ? Boolean.class : String.class);
        } else {
            return ((columnIndex == 0) ? Boolean.class : String.class);
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (isLeftSide()) {
            return ((columnIndex == getColumnCount()-1) ? true : false);
        } else {
            return ((columnIndex == 0) ? true : false);
        }
    }

/** When the row on one table is clicked the radio button on that table is set true and the one on the other table is
 * set to false and vice-versa. This is to toggle between the selection of a row in both tables.
 **/
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        MergeConflictData source = (MergeConflictData) tableDataList.get(rowIndex);
        if (isLeftSide) {
            source.getTableProperties().set(getColumnCount()-1, true)
        } else {
            source.getTableProperties().set(0, true)
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        MergeConflictData source = tableDataList.get(rowIndex);
        Object cellVal = source.getTableProperties().get(columnIndex)
        if (source.getConflictColumns().contains(columnIndex)) {
            return "<html><u>" +
                    cellVal + "</u></html>"
        }
        return cellVal
    }
}

/** This class represents the data for each row. It also holds the information of the conflicting columns in each row.**/

class MergeConflictData {
    private DLComponent localComponent
    private def tableProperties = new ArrayList()
    private def conflictColumns = new ArrayList<Integer>()

    public MergeConflictData (DLComponent localComponent, def tableProperties, def conflictColumns) {
        this.localComponent = localComponent
        this.tableProperties = tableProperties
        this.conflictColumns = conflictColumns
    }

    DLComponent getLocalComponent() {
        return localComponent
    }

    def getTableProperties() {
        return tableProperties
    }

    def getConflictColumns() {
        return conflictColumns
    }
}

