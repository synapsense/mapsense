package DeploymentLab.Dialogs

import DeploymentLab.channellogger.*;
import DeploymentLab.Model.*
import javax.swing.JFrame
import DeploymentLab.ProjectSettings
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import DeploymentLab.Triplet
import DeploymentLab.CentralCatalogue
import DeploymentLab.Quadruple
import javax.swing.table.TableModel
import javax.swing.table.DefaultTableModel
import javax.swing.event.TableModelListener
import javax.swing.event.TableModelEvent
import DeploymentLab.ComponentLibrary

/**
 * Runs the configure defaults widget
 *
 * @author Gabriel Helman
 * @since Mars
 * Date: 6/28/11
 * Time: 1:51 PM
 */
class ConfigureDefaultsController implements ActionListener, TableModelListener {
	private static final Logger log = Logger.getLogger(ConfigureDefaultsController.class.getName())

	private ConfigureDefaultsDialog dialog;

	private ComponentModel cm;
	private ObjectModel om;
	private ProjectSettings ps;

	private static ComponentModel fakeModel

	private Set<Integer> changedLines;

	private Boolean saveChanges

	/**
	 * The list of defaults.  The four fields are:
	 * Component Type Name
	 * Object asName
	 * Object Setting name
	 * Default Value
	 *
	 *
	 */
	private static List<Quadruple<String, String, String, Object>> defaultables = null;


	public ConfigureDefaultsController(ComponentModel cmodel, ObjectModel omodel, ProjectSettings settings){
		this.cm = cmodel
		this.om = omodel
		this.ps = settings
	}


	public void applyChanges(boolean c){
		saveChanges = c
	}


	public showDialog(JFrame parentFrame){
		//build a list of default-able things
		//unpack the list of overrides from projectSettings
		//show the combined list

		buildDefaultableList()
		//load any already-defaulted values from the project settings
		ps.configuredDefaults.each{ cd ->
			//println "trying to load from pSettings: $cd"
			//find the entry in defaultables & replace it
			defaultables.each{ d ->
				if( d.equalsFirstThree(cd) ){
					//println "found my friend: $d"
					d.d = cd.d
				}
			}
		}

		ConfigureDefaultsTableModel tm = buildTableModel()
		tm.addTableModelListener(this)
		changedLines = new HashSet<Integer>()

		saveChanges = false

		this.dialog = new ConfigureDefaultsDialog(parentFrame, this, tm)
		this.dialog.show()


		//at this point, the tm should have our edits

		if (saveChanges){

			//println "changed lines are: $changedLines"
			//and stash them back into the projectSettings
			def changedQuads = []
			changedLines.each { changed ->
				changedQuads += tm.getQuadrupleFromRow(changed)
			}
			def revisedConfiguredDefaults = []
			ps.configuredDefaults.each{ cd ->
				def stillValid = true
				changedQuads.each{ cq ->
					if( cq.equalsFirstThree(cd) ){
						stillValid = false
					}
				}
				if(stillValid){
					revisedConfiguredDefaults += cd
				}
			}
			revisedConfiguredDefaults.addAll(changedQuads)
			ps.configuredDefaults = revisedConfiguredDefaults
		}

		//clean up before we bail out of here.
		if ( dialog ){
			dialog.dispose()
			dialog = null
		}
	}



	private void buildAFakeComponentModel(){
		if ( fakeModel != null ){
			return
		}
		def modelXml = ComponentLibrary.parseXmlFile('cfg/commoncomponents.xml')
		fakeModel = new ComponentModel(om, modelXml)

		loadDefaultComponents()

	}

	private void buildDefaultableList(){
		if ( defaultables != null ){
			return
		}
		defaultables = []
		buildAFakeComponentModel()
		//Set<String> componentTypes = fakeModel.listComponentTypes()
		List<String> componentTypes = CentralCatalogue.getComp("configureDefaults.components").split(",")

		for ( String t : componentTypes.sort() ){
			if ( ! fakeModel.listComponentRoles(t).contains("placeable") ){
				continue
			}
			//println "testing $t"
			DLComponent c = fakeModel.newComponent(t)
			def currentResults = c.getObjectSettingsWithoutVarbindings()
			//println "$t== $currentResults"
			defaultables.addAll(currentResults)
		}
	}


	public void loadDefaultComponents(){
		def dirPath = CentralCatalogue.getComp('components.path')
		//String[] componentLibraries = CentralCatalogue.getComp('componentLibraries.list').split(",")
		String[] componentLibraries = CentralCatalogue.getComp('configureDefaults.libraries').split(",")
		for(String componentLibrary: componentLibraries){
			String filePath = dirPath + componentLibrary
			def componentLibraryXml = ComponentLibrary.parseXmlFile( filePath )
			componentLibraryXml.component.each{componentXml->
				fakeModel.loadComponent(componentXml)
			}
		}
	}


	private ConfigureDefaultsTableModel buildTableModel(){
		def columns = ["Component Type", "Object", "Setting", "Value"]
		Object[][] data = new Object[defaultables.size()][4];

		//convert defaultables to a 2D array
		for( int i = 0; i < defaultables.size(); i++ ){
			data[i] = defaultables[i].toArray()
		}

		def tm = new ConfigureDefaultsTableModel(data, columns.toArray());
		return tm;
	}


	/**
	 * Handles callbacks from the dialog
	 * @param e
	 */
	@Override
	void actionPerformed(ActionEvent e) {


	}


	@Override
	void tableChanged(TableModelEvent e) {
		//listen for changed data on the table
		println e
		println "col: ${e.getColumn()}  frow: ${e.getFirstRow()}  lrow: ${e.getLastRow()}  type: ${e.getType()}"

		changedLines.add(e.getFirstRow())

	}



}


public class ConfigureDefaultsTableModel extends DefaultTableModel{
	def ConfigureDefaultsTableModel(Object[][] data, Object[] columnNames) {
		super(data, columnNames)
	}

	@Override
	boolean isCellEditable(int row, int column) {
		//return super.isCellEditable(row, column)	//To change body of overridden methods use File | Settings | File Templates.
		if( column == 3 ){
			return true
		} else {
			return false
		}
	}

	public Quadruple getQuadrupleFromRow(int row){
		return new Quadruple(this.getValueAt(row,0), this.getValueAt(row,1), this.getValueAt(row,2), this.getValueAt(row,3))
	}

}
