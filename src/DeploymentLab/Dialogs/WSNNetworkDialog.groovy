package DeploymentLab.Dialogs

import DeploymentLab.CentralCatalogue
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.Security.NetworkKey
import DeploymentLab.Security.PanIDFactory
import DeploymentLab.SelectModel
import DeploymentLab.SpringUtilities
import DeploymentLab.channellogger.Logger
import groovy.swing.SwingBuilder

import javax.swing.*
import javax.swing.event.DocumentEvent
import javax.swing.event.DocumentListener
import java.awt.*
import java.awt.event.ActionEvent
import java.awt.event.ActionListener

/**
 * @author Vadim Gusev
 * @since Aireo
 * Date: 17.12.13
 * Time: 14:32
 */
class WSNNetworkDialog {

	private static final Logger log = Logger.getLogger(WSNNetworkDialog.class.getName())

	protected SwingBuilder sb
	private JDialog dialogDelegate
	private JComboBox cb
	private JTextField tf
	private JLabel nameIndicator

	private WSNNetworkController controller
	private boolean allowCreate

	public WSNNetworkDialog(WSNNetworkController controller, boolean allowCreate = false) {
		this.controller = controller
		this.allowCreate = allowCreate
	}

	public void initDialog( String forComp = null ) {
		sb = new SwingBuilder()
		dialogDelegate = sb.dialog(owner: CentralCatalogue.parentFrame, title: 'Select WSN Network', minimumSize: new Dimension(280, (allowCreate)?180:110), layout: new BorderLayout(), modalityType: Dialog.ModalityType.APPLICATION_MODAL, locationRelativeTo: null) {
			def okAction = {
				if (allowCreate){
					if (cb.getSelectedIndex() == 0){
						String name = tf.getText()
						if (! name ||  name.isEmpty()){
							nameIndicator.setText("Name field is empty")
							return
						}
						if (controller.networkExist( name)){
							nameIndicator.setText("Network '$name' already exists")
							return
						}
						controller.createNetwork(name)
						controller.setSelectedNetwork(name)
					}
				}
				if (!allowCreate || cb.getSelectedIndex() != 0){
					controller.setSelectedNetwork(cb.getSelectedItem().toString())
				}
				dialogDelegate.setVisible(false)
			}
			def cancelAction = { dialogDelegate.setVisible(false) }

			int rows = 1

			def mainPanel = panel(layout: new SpringLayout(), constraints: BorderLayout.NORTH) {

				emptyBorder(5, 5, 5, 5)
				if( forComp ) {
					rows++
					label(text: "Component Name: ")
					label(text: "$forComp")
				}
				label(text: "WSN Network:")
				cb = comboBox(id: 'comboBox' )
				cb.setModel(controller.comboBoxModel)
				((JTextField)cb.getEditor().getEditorComponent()).setColumns(10)
				cb.addActionListener(controller)
				if (allowCreate){
					rows++
					label(text: "Network Name:")
					tf = textField(columns: 10)
					tf.getDocument().addDocumentListener(new DocumentListener() {
						public void changedUpdate(DocumentEvent e) {
							clearIndicator();
						}
						public void removeUpdate(DocumentEvent e) {
							clearIndicator();
						}
						public void insertUpdate(DocumentEvent e) {
							clearIndicator();
						}
					});
				}
			}
			panel(layout: new FlowLayout(FlowLayout.CENTER), constraints: BorderLayout.CENTER) {
				nameIndicator = label(text:"          ", foreground: Color.red)
			}
			panel(layout: new FlowLayout(FlowLayout.RIGHT), constraints: BorderLayout.SOUTH) {
				button(text: 'OK', actionPerformed: okAction)
				button(text: 'Cancel', actionPerformed: cancelAction)
			}

			SpringUtilities.makeCompactGrid(mainPanel,
					rows, 2,
					6, 6,
					5, 5)
		}
		dialogDelegate.setLocationRelativeTo(CentralCatalogue.parentFrame)
	}

	public void clearIndicator() {
		nameIndicator.setText("    ")
	}

	public void show() {
		if (this.dialogDelegate == null) {
			initDialog()
		}
		this.dialogDelegate.requestFocusInWindow();
		this.dialogDelegate.show()
	}

	public void requestFocusInWindow() {
		cb.requestFocusInWindow()
	}

	public void setNameFieldEnabled(boolean b){
		if (tf) tf.setEnabled(b)
	}
}

class WSNNetworkController implements ActionListener{

	private static final Logger log = Logger.getLogger(WSNNetworkController.class.getName())

	private static final WSN_NETWORK_TYPE = "network"
	private static final WSN_GATEWAY_TYPE = "remote-gateway"
	private static final String NEW = "<new>"

	private ComponentModel cmodel
	private SelectModel smodel

	private WSNNetworkDialog dialog
	private static DLComponent selectedNetwork
	private DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>()
	private boolean allowCreate

	public static void showDialog(ComponentModel cm, SelectModel sm, String forComp, boolean allowCreate = false){
		new WSNNetworkController(cm, sm).showDialog( forComp, allowCreate )
	}

	public static void showDialog(ComponentModel cm, SelectModel sm, boolean allowCreate = false){
		new WSNNetworkController(cm, sm).showDialog(allowCreate)
	}

	public static DLComponent getSelectedNetwork(){
		return selectedNetwork
	}

	public WSNNetworkController(ComponentModel cm, SelectModel sm){
		this.cmodel = cm
		this.smodel = sm
	}

	public void showDialog(boolean allowCreate = false){
		showDialog( null, allowCreate )
	}

	public void showDialog(String forComp, boolean allowCreate = false){
	 	dialog = new WSNNetworkDialog(this, allowCreate)
		dialog.initDialog( forComp )
		this.allowCreate = allowCreate
		refillComboBox(allowCreate)
		selectedNetwork = null
		dialog.show()
	}

	public void refillComboBox(boolean allowCreate){
		model.removeAllElements()
		if (allowCreate){
			model.addElement(NEW)
		}
		cmodel.getComponentsByType(WSN_NETWORK_TYPE, smodel.activeDrawing).each {
			model.addElement(it.name.toString())
		}
	}

	public DefaultComboBoxModel<String> getComboBoxModel(){
		return model
	}

	public void setSelectedNetwork(String name){
		selectedNetwork = cmodel.getComponentsByType(WSN_NETWORK_TYPE, smodel.activeDrawing).find { it.name.equals(name) }
	}

	public boolean networkExist(String name){
		return (cmodel.getComponentsByType(WSN_NETWORK_TYPE, smodel.activeDrawing).find { it.name.equals(name) })?true:false
	}

	public DLComponent createNetwork(String name){
		try {
			DLComponent network = cmodel.newComponent('network')
			network.setPropertyValue('name', name)
			smodel.getActiveDrawing().addChild(network)

			//generate a PAN ID for WSN Networks
			network.setPropertyValue('pan_id', PanIDFactory.makePanID(cmodel))

			//also make new network keys
			def p = NetworkKey.makeNetworkKeys()
			network.setPropertyValue('networkPublicKey', p.a)
			network.setPropertyValue('networkPrivateKey', p.b)

			return network
		} catch (Exception ex) {
			log.error("Error while creating Data Source!", "message")
			log.error(log.getStackTrace(ex))
		}

		return null
	}

	@Override
	void actionPerformed(ActionEvent e) {
		dialog.clearIndicator()
		JComboBox cb = (JComboBox)e.getSource()
		if (allowCreate){
			if (cb.getSelectedIndex() == 0){
				dialog.setNameFieldEnabled(true)
			}else{
				dialog.setNameFieldEnabled(false)
			}
		}
	}
}
