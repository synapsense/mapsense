package DeploymentLab.Dialogs.ChangeParents;

import java.util.Collections;
import java.util.HashSet;

/**
 * Enum of possible parent categories so that ChangeParentsDialog and ChangeParentsController can have the right behavior.
 *
 * @author Gabriel Helman
 * @see ChangeParentsController
 * @see ChangeParentsDialog
 * @since Jupiter 2
 *        Date: 6/13/12
 *        Time: 10:00 AM
 */
public enum ParentCategory {
	NETWORK("network", "datasource", false),
	GROUPING("zone,logicalgroup", "grouping", false);

	private final HashSet<String> baseRoles;
	private final String nameKey;
	private final Boolean allowsMoreThanOneParent;

	ParentCategory(final String roles, final String key, final Boolean allowsMoreThanOneParent) {
		this.baseRoles = new HashSet<String>();
		Collections.addAll(baseRoles, roles.split(","));
		this.nameKey = key;
		this.allowsMoreThanOneParent = allowsMoreThanOneParent;
	}

	public HashSet<String> getRoles() {
		return baseRoles;
	}

	public String  getNameKey(){
		return nameKey;
	}

	public Boolean getAllowsMoreThanOneParent(){
		return allowsMoreThanOneParent;
	}
	
	@Override
	public String toString() {
		return "ParentCategory{" +
				"baseRoles=" + baseRoles +
				", nameKey='" + nameKey + '\'' +
				", allowsMoreThanOneParent='" + allowsMoreThanOneParent + '\'' +
				'}';
	}
}
