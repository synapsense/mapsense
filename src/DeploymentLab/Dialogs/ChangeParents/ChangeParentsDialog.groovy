package DeploymentLab.Dialogs.ChangeParents

import DeploymentLab.CentralCatalogue
import DeploymentLab.Dialogs.ScrollablePanel
import DeploymentLab.Image.ComponentIconFactory
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLComponentNameComparator
import DeploymentLab.channellogger.Logger
import com.google.gag.annotation.disclaimer.AhaMoment
import groovy.swing.SwingBuilder
import java.awt.event.ActionEvent
import java.awt.event.KeyEvent
import java.util.List
import net.miginfocom.swing.MigLayout
import java.awt.*
import javax.swing.*

/**
 * The UI side of the Change Parents function.  This just holds display code; all the work is in the controller.
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 6/12/12
 * Time: 5:54 PM
 * @see ChangeParentsController
 */
@AhaMoment
class ChangeParentsDialog {
	private static final Logger log = Logger.getLogger(ChangeParentsDialog.class.getName())
	/** Internal SwingBuilder used to construct the dialogDelegate.  */
	protected SwingBuilder builder;
	/** All the actual dialog work is delegated to the JDialog class.  */
	protected JDialog dialogDelegate;
	/** The parent frame of this dialog box. */
	protected JFrame parentFrame;

	private JButton okButton
	private JButton closeButton
	private JButton applyButton
	private JPanel mainPanel

	private ChangeParentsController controller;

	private static final int BORDERINSET = 1

	/**
	 * Prepare a dialog.
	 * @param parent the JFrame to make the parent of the dialog.
	 * @param delegate The ChangeParentsController that will be running the show.
	 */
	public ChangeParentsDialog(JFrame parent, ChangeParentsController delegate) {
		this.builder = new SwingBuilder()
		parentFrame = parent;
		controller = delegate;
	}

	/**
	 * Construct the dialog.
	 */
	protected void initDialog() {
		HashMap<DLComponent, Boolean> data = controller.getData()
		List<DLComponent> sortedKeys = new ArrayList<DLComponent>(data.keySet())
		sortedKeys.sort(new DLComponentNameComparator())
		ButtonGroup group = new ButtonGroup()

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		def maxSize = new Dimension()

		int numberOfColumns = Math.ceil(sortedKeys.size() / 20.0)
		if (numberOfColumns > 5) {
			numberOfColumns = 5
		}

		dialogDelegate = builder.dialog(owner: parentFrame, resizable: false, title: CentralCatalogue.getUIS("changeParents.dialog.title.${controller.getParentCategory().getNameKey()}"), layout: new BorderLayout(), modalityType: Dialog.ModalityType.APPLICATION_MODAL) {
			scrollPane(constraints: BorderLayout.CENTER, horizontalScrollBarPolicy: JScrollPane.HORIZONTAL_SCROLLBAR_NEVER) {
				mainPanel = panel(layout: new MigLayout("fill"), new ScrollablePanel()) {
					int i = 0;
					for (DLComponent pp: sortedKeys) {
						//BorderFactory.createEmptyBorder(BORDERINSET,BORDERINSET,BORDERINSET,BORDERINSET)
						//def border = BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1)
						//def border = BorderFactory.createMatteBorder(1,0,1,0,Color.LIGHT_GRAY)
						//panel(layout: new MigLayout("ins 5, gap 0, fill"), constraints: "wrap, growx, gap 0", border: border ){

						def currentParent = pp
						if (controller.allowsMoreThanOneParent()) {
							checkBox(selected: data[currentParent], actionPerformed: {controller.checkboxChecked(currentParent)})
						} else {
							radioButton(selected: data[currentParent], actionPerformed: {controller.radioButtonChecked(currentParent)}, buttonGroup: group)
						}
						def wrap = ""
						if (i == numberOfColumns - 1) {
							wrap = ", wrap"
						}
						label(icon: ComponentIconFactory.getComponentIcon(currentParent), text: currentParent.getName(), constraints: "gapleft rel, gapright unrel, growx $wrap".toString())
						//}

						i++;
						if (i == numberOfColumns) {
							i = 0;
						}
					}
				}
			}
			panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.SOUTH) {
				okButton = builder.button(text: 'OK', defaultButton: true, actionPerformed: { this.apply(true) })
				closeButton = builder.button(text: 'Cancel', actionPerformed: { this.close() })
				//builder.button(text: 'Apply', actionPerformed: { this.apply(false) })
			}
		}

		//set the size of the main panel
		//use the default "preferred size" unless the height is more than 1/2 the screen height; then clamp it at that
		def totalHeight = mainPanel.getPreferredSize().getHeight()
		def finalHeight = 0
		if (dim.getHeight() / 2 < totalHeight) {
			finalHeight = dim.getHeight() / 2
		} else {
			finalHeight = totalHeight
		}
		def finalWidth = mainPanel.getPreferredSize().getWidth()
		maxSize.setSize(finalWidth, finalHeight)
		mainPanel.setPreferredScrollableViewportSize(maxSize)

		setEscapeKeyMap();
		this.dialogDelegate.pack();
		this.dialogDelegate.setLocationRelativeTo(this.parentFrame);
	}

	/**
	 * Tell the controller to apply changes.
	 * @param closeAfter If true, the dialog will close after the apply (OK), if false, it will stay open (Apply).
	 */
	private void apply(boolean closeAfter) {
		controller.applyChanges(closeAfter)
	}

	/**
	 * Show the dialog.
	 */
	public void show() {
		if (this.dialogDelegate == null) {
			initDialog()
		}
		this.dialogDelegate.requestFocusInWindow();
		this.dialogDelegate.show();
	}

	/**
	 * Close the dialog.
	 */
	public void close() {
		this.dialogDelegate.setVisible(false);
		//this.dialogDelegate.dispose();
		//this.dialogDelegate = null;
	}

	/**
	 * Jettison the dialog.
	 */
	public void dispose() {
		this.dialogDelegate.dispose();
		this.dialogDelegate = null;
	}

	private void setEscapeKeyMap() {
		String CANCEL_ACTION_KEY = "CANCEL_ACTION_KEY";
		int noModifiers = 0;
		KeyStroke escapeKey = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, noModifiers, false);
		InputMap inputMap = this.dialogDelegate.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(escapeKey, CANCEL_ACTION_KEY);
		AbstractAction cancelAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		};
		this.dialogDelegate.getRootPane().getActionMap().put(CANCEL_ACTION_KEY, cancelAction);
	}

}
