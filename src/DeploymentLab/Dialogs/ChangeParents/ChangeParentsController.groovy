package DeploymentLab.Dialogs.ChangeParents

import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.UndoBuffer
import DeploymentLab.channellogger.Logger
import com.google.gag.enumeration.Where

import javax.swing.JFrame
import com.google.gag.annotation.disclaimer.AhaMoment
import DeploymentLab.CentralCatalogue
import DeploymentLab.SelectModel
import DeploymentLab.Exceptions.ModelCatastrophe
import com.google.common.annotations.VisibleForTesting

/**
 * Handles changing parents of one or more components.
 * Responsible for calculating all possible parents, getting user feedback on the choices, and then applying any changes.
 * Requires no outside dependencies other than the selection of components whose parents are to be changed (and the undo buffer).
 *
 * Prior to Jupiter 2, the earlier iteration of this code all lived in the PopupMenu.
 * This class shares no code with that implementation.
 *
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 6/12/12
 * Time: 5:54 PM
 */
@AhaMoment(Where.TOILET)
class ChangeParentsController {
	private static final Logger log = Logger.getLogger(ChangeParentsController.class.getName())
	private ChangeParentsDialog dialog;
	private ComponentModel cmodel;
	private List<DLComponent> travellers
	private ParentCategory parentCategory
	private UndoBuffer undoBuffer
	private HashMap<DLComponent, Boolean> _potentialParents
	private SelectModel selectModel

	public ChangeParentsController(List<DLComponent> components, ParentCategory selectionCategory, UndoBuffer ub, SelectModel sm) {
		this.travellers = components
		this.parentCategory = selectionCategory
		this.undoBuffer = ub
		this.selectModel = sm
		if (travellers.size() > 0) {
			this.cmodel = travellers.first().getModel()
		}
	}

	public boolean isValid() {
		//log.debug("I know about ${travellers.size()} things")
		if (this.travellers.size() == 0) {
			log.warn(CentralCatalogue.getUIS("changeParents.nothingSelected"), "message")
			return false
		} else if (this.getData().size() == 0) {
			log.warn(CentralCatalogue.getUIS("changeParents.notpossible.${parentCategory.getNameKey()}"), "message")
			return false
		} else {
			return true
		}
	}

	public showDialog(JFrame parentFrame) {
		if (this.isValid()) {
			this.dialog = new ChangeParentsDialog(parentFrame, this)
			this.dialog.show()
		}
	}

	private void finishUp() {
		//clean up before we bail out of here.
		if (dialog) {
			dialog.close()
			dialog.dispose()
			dialog = null
		}
	}

	private void changeChild(DLComponent parent, DLComponent child, boolean makeAParent) {
		//log.debug("changeChild $parent $child $makeAParent")
		if (makeAParent) {
			if (!child.isChildComponentOf(parent)) {
				parent.addChild(child)
			}
		} else {
			if (child.isChildComponentOf(parent)) {
				parent.removeChild(child)
			}
		}
	}

	public void applyChanges(boolean closeAfter) {
		try {
			undoBuffer.startOperation()
			boolean showManualWSNMessage = false

			//all removes
			for (DLComponent pp: this.getData().keySet()) {
				if (!this.getData()[pp]) {
					for (def c: travellers) {
						this.changeChild(pp, c, false)
					}
				}
			}
			//then all adds
			for (DLComponent pp: this.getData().keySet()) {
				if (this.getData()[pp]) {
					for (def c: travellers) {
						if( c.hasProperty("manualWSNConfig") && (pp.getType() == 'network') ) {
							c.setPropertyValue('manualWSNConfig', true)
							showManualWSNMessage = true
						}
						this.changeChild(pp, c, true)
					}
				}
			}
			if (closeAfter) {
				this.finishUp()
			}

			if (showManualWSNMessage){
				log.info(CentralCatalogue.getUIS("changeParents.WSNToManual"), "statusbar")
			}

			//last check: make sure everything has a parent
			for (def t: travellers) {
				if( t.getParentComponents().isEmpty() ) {
					throw new ModelCatastrophe("Change Parents resulted in no parents for $t")
				}
			}
		} catch (Exception e) {
			undoBuffer.rollbackOperation()
			throw e
		} finally {
			undoBuffer.finishOperation()
		}
	}

	public void cancelChanges() {
		this.finishUp()
	}

	public void checkboxChecked(DLComponent c) {
		//log.debug("checkboxChecked $c")
		this.getData()[c] = !this.getData()[c]
	}

	public void radioButtonChecked(DLComponent c) {
		//log.debug("radioButtonChecked $c :: true")
		for (DLComponent pp: this.getData().keySet()) {
			this.getData()[pp] = false
		}
		this.getData()[c] = true
	}

	public HashMap<DLComponent, Boolean> getData() {
		if (_potentialParents == null) {
			ComponentModel cm = travellers.first().getModel()
			Set<DLComponent> byRole = cm.getComponentsInAnyRole(parentCategory.getRoles())
			Set<DLComponent> parents = new HashSet<DLComponent>(byRole.size())
			for (DLComponent p: byRole) {
				def valid = true
				for (def t: travellers) {
					if (!p.canChild(t) || p.getDrawing() != t.getDrawing()) {
						valid = false
					}
				}
				if (valid) {
					parents.add(p)
				}
			}

			_potentialParents = new HashMap<DLComponent, Boolean>(parents.size())
			for (DLComponent pp: parents) {
				_potentialParents[pp] = this.isParentOfAll(pp, travellers)
			}
		}
		return _potentialParents
	}


	public boolean allowsMoreThanOneParent() {
		return parentCategory.allowsMoreThanOneParent
	}


	private boolean isParentOfAll(DLComponent parent, List<DLComponent> children) {
		def result = true
		for (def c: children) {
			if (!c.isChildComponentOf(parent)) {
				result = false
			}
		}
		return result
	}


	public ParentCategory getParentCategory() {
		return parentCategory
	}

	@VisibleForTesting
	protected ChangeParentsDialog getDialog(JFrame parentFrame){
		if(this.dialog==null){
			this.dialog = new ChangeParentsDialog(parentFrame, this)
		}
		return this.dialog;
	}
}
