package DeploymentLab.Dialogs

import DeploymentLab.CentralCatalogue
import DeploymentLab.DLProject
import DeploymentLab.Export.ExportController
import DeploymentLab.Export.ExportTaskType
import DeploymentLab.Image.ComponentIconFactory
import DeploymentLab.Model.AccessModel
import DeploymentLab.ProgressManager
import DeploymentLab.SpringUtilities
import DeploymentLab.Tree.AccessModelTree
import DeploymentLab.Tree.AccessModelTreeModel
import DeploymentLab.channellogger.Logger
import groovy.swing.SwingBuilder

import javax.swing.*
import javax.swing.event.DocumentEvent
import javax.swing.event.DocumentListener
import java.awt.*
import java.awt.event.ActionListener
import java.awt.event.KeyEvent
import DeploymentLab.Model.PlatformType

/**
 * @author Vadim Gusev
 * @since Europa
 * Date: 16.04.13
 * Time: 11:45
 */
class AccessModelDialog {
	private static final Logger log = Logger.getLogger(AccessModelDialog.class.getName())
	protected SwingBuilder builder
	private JDialog dialogDelegate
	private JButton exportButton
	private JFrame parent
	private def dlw
	private DLProject openProject
	private AccessModel amodel
	private AccessModelTree tree;
	private AccessModelTreeModel treeModel

	public AccessModelDialog(JFrame _parent, def dlw){
		builder = new SwingBuilder()
		parent = _parent
		this.dlw = dlw
	}

	def closeAction = {
		if (amodel.isDirty()){
			int res = JOptionPane.showConfirmDialog(parent, "An export is required to add or delete nodes from the Environment Server.\nAre you sure you want to exit without exporting?", 'Confirm Exit', JOptionPane.OK_CANCEL_OPTION)
			if(res != JOptionPane.OK_OPTION) {
				return
			}
			openProject.setNeedsASave(true)
		}
		dialogDelegate.setVisible(false);
		dialogDelegate.dispose();
	}

	private JDialog getDialog() {
		dialogDelegate = builder.dialog(
				owner:parent,
				title: "Access List",
				layout: new BorderLayout(),
				modalityType: Dialog.ModalityType.APPLICATION_MODAL,
				defaultCloseOperation: JFrame.DO_NOTHING_ON_CLOSE ,
				windowClosing: closeAction,
				iconImage: ComponentIconFactory.defaultIcon.getImage()) {
			def exportAction = {
				if (!amodel || amodel.isEmpty()) {
					log.warn("Access Model is empty. Import Access List before export.", "message")
					return
				}
				dialogDelegate.setVisible(false)
				if (amodel.isDirty()){
					openProject.setNeedsASave(true)
				}
				ExportController ec = new ExportController(openProject)
				Properties connectProperties = new Properties()
				if (dlw.loginDialog.getLoginInfo(connectProperties)) {
					ProgressManager.doTask{
						ec.loginESDoExportTask(connectProperties, ExportTaskType.ACCESS)
						dialogDelegate.dispose()
					}
				}else{
					dialogDelegate.setVisible(true)
				}
			}
			def importAction = {
				dialogDelegate.setVisible(false)
				dlw.importData("access")
				updateTree()
				dialogDelegate.setVisible(true)
			}

			def addAction = {
				addNode()
				updateTree()
			}
			def deleteAction = {
				def userObjects = tree.getSelectedNodes()
				for (def userObject : userObjects){
					if (userObject.deleted){
						log.trace("Try to undelete : $userObject")
						amodel.undeleteNode(userObject.dlid)
					}else{
						log.trace("Try to delete : $userObject")
						amodel.deleteNode(userObject.dlid)
					}
				}
				updateTree()
			}
			def deleteAllAction = {
				int res = JOptionPane.showConfirmDialog(parent, "Are you sure you want to delete all nodes?", 'Confirm Delete All Nodes', JOptionPane.OK_CANCEL_OPTION)
				if(res != JOptionPane.OK_OPTION) {
					return
				}
				amodel.deleteAllNodes()
				updateTree()
			}
			def dlgPanel = panel(layout: new BorderLayout()){
				panel(layout: new FlowLayout(FlowLayout.LEADING), constraints:BorderLayout.NORTH) {
					label(text: "Nodes in Access List")
				}
				scrollPane(layout:new ScrollPaneLayout(), preferredSize:[350, 400], constraints:BorderLayout.CENTER){
					amodel.updateAccessModel()
					if (amodel.getDrawings().size() == 1){
						treeModel = new AccessModelTreeModel(amodel, amodel.getDrawings().values().first())
					} else {
						treeModel = new AccessModelTreeModel(amodel)
					}
					tree = new AccessModelTree(treeModel);
					widget(tree)
				}
				panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints:BorderLayout.SOUTH) {
					button(text: 'Import', actionPerformed: importAction)
					button(text: 'Add', actionPerformed: addAction)
					button(text: 'Delete', actionPerformed: deleteAction)
					button(text: 'Delete All', actionPerformed: deleteAllAction)
				}
			}
			panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.SOUTH) {
				exportButton = button(text: 'Export Access List', defaultButton: true, actionPerformed: exportAction)
				button(text: 'Close', actionPerformed: closeAction)
			}
			dlgPanel.registerKeyboardAction(exportAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			dlgPanel.registerKeyboardAction(closeAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		}

		dialogDelegate.pack()
		return dialogDelegate
	}

	private void updateTree(){
		if (amodel.getDrawings().size() == 1){
			treeModel = new AccessModelTreeModel(amodel, amodel.getDrawings().values().first())
		} else {
			treeModel = new AccessModelTreeModel(amodel)
		}
		tree.setModel(treeModel)
		tree.repaint()
	}

	private void requestFocusInWindow() {
		exportButton.requestFocusInWindow()
	}

	private void setLocationRelativeTo() {
		dialogDelegate.setLocationRelativeTo(parent)
	}

	private void show(){
		dialogDelegate.show()
	}

	public void showAccessModelDialog(){
		this.openProject = CentralCatalogue.getOpenProject()
		this.amodel = CentralCatalogue.getOpenProject().accessModel
		dialogDelegate = this.getDialog()
		setLocationRelativeTo()
		requestFocusInWindow()
		show()
	}

	private void addNode(){
		def addDialog = null
		JComboBox typeBox
		JTextField macBox
		JComboBox netBox
		JPanel mainPanel
		JLabel macIndicator
		addDialog = builder.dialog(owner: parent, title: 'Add Node', resizable: false, layout: new BorderLayout(), modalityType: Dialog.ModalityType.APPLICATION_MODAL, locationRelativeTo: parent) {
			def okAction = {
				String macId = macBox.getText()
				if (!macId || macId.isEmpty()){
					macIndicator.setText("MAC ID is empty")
					return
				}
				try {
					Long.parseLong(macId, 16)
				} catch (NumberFormatException e){
					macIndicator.setText("MAC ID is incorrect")
					return
				}
				if (amodel.isExist(macId)){
					macIndicator.setText("MAC ID already exist")
					return
				}

				String network = netBox.getSelectedItem()
				PlatformType type = typeBox.getSelectedItem()
				amodel.addNode(type, macId, network)
				addDialog.setVisible(false);
				addDialog.dispose() }
			def cancelAction = { addDialog.setVisible(false); addDialog.dispose() }

			mainPanel = panel(layout: new SpringLayout(), constraints: BorderLayout.NORTH) {
				emptyBorder(5, 5, 5, 5)
				label(text: "Platform:")
				typeBox = comboBox(id: 'comboBox', items: PlatformType.values(), maximumRowCount: 10)
				label(text: "MAC ID:")
				macBox = textField(columns: 10)
				macBox.getDocument().addDocumentListener(new DocumentListener() {
					public void changedUpdate(DocumentEvent e) {
						clearIndicator();
					}
					public void removeUpdate(DocumentEvent e) {
						clearIndicator();
					}
					public void insertUpdate(DocumentEvent e) {
						clearIndicator();
					}
					public void clearIndicator() {
						macIndicator.setText("    ")
					}
				});
				label(text: "WSN Network:")
				netBox = comboBox(id: 'comboBox', items: amodel.getNetworks().collect { it.value.name }.toArray())
				def net = tree.getSelectedNet()
				if (net){
					netBox.setSelectedItem(net.name)
				}
			}
			panel(layout: new FlowLayout(FlowLayout.CENTER), constraints: BorderLayout.CENTER) {
				macIndicator = label(text:"          ", foreground: Color.red)
			}
			panel(layout: new FlowLayout(FlowLayout.RIGHT), constraints: BorderLayout.SOUTH) {
				button(text: 'OK', actionPerformed: okAction)
				button(text: 'Cancel', actionPerformed: cancelAction)
			}

			SpringUtilities.makeCompactGrid(mainPanel,
					3, 2,
					6, 6,
					5, 5)
		}

		addDialog.pack()
		addDialog.show()
	}
}

