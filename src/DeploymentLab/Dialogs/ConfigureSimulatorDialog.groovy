package DeploymentLab.Dialogs

import DeploymentLab.channellogger.Logger
import groovy.swing.SwingBuilder
import java.awt.BorderLayout
import java.awt.Dialog.ModalityType
import java.awt.FlowLayout
import java.awt.event.ActionEvent
import java.awt.event.KeyEvent
import javax.swing.table.TableModel
import javax.swing.table.TableRowSorter
import javax.swing.*

/**
 * The new simulator control board
 * @author Gabriel Helman
 * @since Mars
 * Date: 8/31/11
 * Time: 11:18 AM
 */
class ConfigureSimulatorDialog {
	private static final Logger log = Logger.getLogger(ConfigureSimulatorDialog.class.getName())
	/** Internal SwingBuilder used to construct the dialogDelegate.  */
	protected SwingBuilder builder;
	/** All the actual dialog work is delegated to the JDialog class.  */
	protected JDialog dialogDelegate;
	/** The parent frame of this dialog box. */
	protected JFrame parentFrame;

	JButton okButton
	JButton closeButton
	JButton applyButton
	JPanel mainPanel
	JTable mainTable

	private TableModel tm

	private ConfigureSimulatorController controller;


	public ConfigureSimulatorDialog(JFrame parent, ConfigureSimulatorController delegate, TableModel model) {
		this.builder = new SwingBuilder()
		parentFrame = parent;
		controller = delegate;
		tm = model;
	}


	protected void initDialog() {

		dialogDelegate = builder.dialog(owner: parentFrame, resizable: true, title: 'Configure Simulator', layout: new BorderLayout(), modalityType: ModalityType.APPLICATION_MODAL) {
			scrollPane(constraints: BorderLayout.CENTER, horizontalScrollBarPolicy: JScrollPane.HORIZONTAL_SCROLLBAR_NEVER) {
				mainTable = table(model: tm)
			}
			panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.SOUTH) {
				okButton = builder.button(text: 'OK', defaultButton: true, actionPerformed: { this.apply() })
				closeButton = builder.button(text: 'Close', actionPerformed: { this.close() })
			}
		}
		mainTable.setRowSorter(new TableRowSorter(tm));
		setEscapeKeyMap();
		this.dialogDelegate.pack();
		this.dialogDelegate.setLocationRelativeTo(this.parentFrame);

	}

	public void apply() {
		controller.applyChanges(true)
		this.close()
	}

	public void show() {
		if (this.dialogDelegate == null) {
			initDialog()
		}
		this.dialogDelegate.requestFocusInWindow();
		this.dialogDelegate.show();
	}

	private void close() {
		this.dialogDelegate.setVisible(false);
		//this.dialogDelegate.dispose();
		//this.dialogDelegate = null;
	}

	public void dispose() {
		this.dialogDelegate.dispose();
		this.dialogDelegate = null;
	}

	private void setEscapeKeyMap() {
		String CANCEL_ACTION_KEY = "CANCEL_ACTION_KEY";
		int noModifiers = 0;
		KeyStroke escapeKey = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, noModifiers, false);
		InputMap inputMap = this.dialogDelegate.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(escapeKey, CANCEL_ACTION_KEY);
		AbstractAction cancelAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		};
		this.dialogDelegate.getRootPane().getActionMap().put(CANCEL_ACTION_KEY, cancelAction);
	}

}