package DeploymentLab.Dialogs

import DeploymentLab.CentralCatalogue
import DeploymentLab.DisplayProperties
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.SelectModel
import DeploymentLab.SpringUtilities
import DeploymentLab.Tree.ViewOptionTree
import DeploymentLab.channellogger.Logger
import com.synapsense.util.unitconverter.SystemConverter
import com.synapsense.util.unitconverter.UnitConverter
import groovy.swing.SwingBuilder

import javax.swing.*
import javax.swing.border.TitledBorder
import java.awt.*
import java.awt.Dialog.ModalityType
import java.awt.event.ActionListener
import java.awt.event.KeyEvent
import java.util.List

/**
 * Effectively, both the view and the controller for the display properties structure.  Primarily, this serves as the UI
 * to set those properties, but also includes a handful of methods to allow external code to set display properties
 * (menu items, or hotkeys, say). These provide single api for handling display settings, as well as keeping the UI dialog up to date.
 */
class ViewOptionDialog {
	private static final Logger log = Logger.getLogger(ViewOptionDialog.class.getName())
	
	protected SwingBuilder builder
	private JDialog dialogDelegate
	private ViewOptionTree configTree
	private ViewOptionTree haloTree
	private JCheckBox viewOptionBgImageChkBox
	private JCheckBox viewOptionNameChkBox
	private JCheckBox viewOptionOrientationChkBox
	private JCheckBox viewOptionAssociationChkBox
	private JCheckBox viewOptionAssociationDetailChkBox
	private List<JRadioButton> viewOptionAugmentHoverChoices = []
	private JRadioButton viewOptionNameRdBtn
	private JRadioButton viewOptionMacidRdBtn
	private JButton viewOptionApplyBtn
	private JButton viewOptionOKBtn
	private JFrame parent

	private DisplayProperties displayProperties
	private Properties associationProperties

    private SystemConverter systemConverter
	
	def associationCheckBoxes = []
	def displayGroupBtnMap = [:]
	/**
	 * Flag that is set to true if the settings have been altered via an outside entity, rather than the actual options dialog.
	 */
	boolean fromOutside = false
	private SelectModel sm
	private ComponentModel cm
	

	public ViewOptionDialog(ViewOptionTree _configTree, ViewOptionTree _haloTree, DisplayProperties _displayProperties, Object _displayGroupBtnMap, JFrame _parent, SystemConverter systemConverter, SelectModel _sm, ComponentModel _cm) {
		builder = new SwingBuilder()
		parent = _parent
		configTree = _configTree
		haloTree = _haloTree
		displayProperties =  _displayProperties
		displayGroupBtnMap = _displayGroupBtnMap
		associationProperties = CentralCatalogue.getInstance().getAssociationProperties()
		dialogDelegate = this.getDialog()
        this.systemConverter = systemConverter
		sm = _sm
		cm = _cm

	}

	public void setUnitSystem(SystemConverter sc) {
		updateViewOption()
	}

	private JDialog getDialog() {

		def associationTypes = associationProperties.getProperty("associations.types").split(',')
		GridBagConstraints gbc = new GridBagConstraints()

		JPanel associationTypePanel;
		JPanel generalCheckboxes;
		dialogDelegate = builder.dialog(owner:parent, resizable: false, title: 'View Options', layout: new BorderLayout(), modalityType: ModalityType.APPLICATION_MODAL) {
		def okAction = {updateViewOption();dialogDelegate.setVisible(false)}
		def applyAction = {updateViewOption()}
		def cancelAction = {dialogDelegate.setVisible(false)}

		int idx =0;

		def dlgPanel = panel(layout: new BorderLayout(), constraints: BorderLayout.CENTER) {
			tabbedPane(constraints: BorderLayout.CENTER){
				gbc.insets = new Insets(5, 5, 5, 5)
				gbc.gridx=0; gbc.gridy=0; gbc.anchor=GridBagConstraints.NORTH; gbc.gridwidth=1


				panel(layout: new GridBagLayout(),  title: 'General'){
					gbc.gridx=0; gbc.gridy=0; gbc.anchor=GridBagConstraints.NORTH; gbc.gridwidth=1; gbc.fill = GridBagConstraints.VERTICAL;
					TitledBorder tb = titledBorder(title:'General')
					generalCheckboxes = panel(border:tb,constraints: gbc, layout: new SpringLayout(),preferredSize:new Dimension(350,200)  ) {
						viewOptionOrientationChkBox = checkBox(text:'Orientation Arrows', selected:true)
						viewOptionBgImageChkBox = checkBox(text:'Background Image', selected:true)
						separator()
						hoverGroup = buttonGroup()
						viewOptionAugmentHoverChoices += radioButton(text:"Don't Highlight Associations on Hover", buttonGroup: hoverGroup, actionCommand: "NONE" )
						viewOptionAugmentHoverChoices += radioButton(text:'Highlight Immediate Associations on Hover', buttonGroup: hoverGroup, actionCommand: "CLOSE", selected:true )
						viewOptionAugmentHoverChoices += radioButton(text:'Highlight Subgraph on Hover', buttonGroup: hoverGroup, actionCommand: "SUBGRAPH" )
					}

					gbc.gridx=0; gbc.gridy=1; gbc.anchor=GridBagConstraints.NORTH; gbc.gridwidth=1
					tb = titledBorder(title:'Object Name')
					panel(border:tb,constraints: gbc,preferredSize:new Dimension(350,80)){
						borderLayout(hgap:2 )
						viewOptionNameChkBox = checkBox(text:'Name on/off', constraints:BorderLayout.NORTH, selected:displayProperties.getProperty('name') /*,actionPerformed:{toggleObjectNameDisplay()},*/)
						nameGroup = buttonGroup()

						viewOptionNameRdBtn = radioButton(text:'Name', enabled:true, selected:displayProperties.getNameType().equals('Name'), constraints:BorderLayout.WEST,  buttonGroup:nameGroup)
						viewOptionMacidRdBtn = radioButton(text:'MacID', enabled:true, selected:displayProperties.getNameType().equals('MacID'), constraints:BorderLayout.CENTER,  buttonGroup:nameGroup)
					}
				}

				panel(layout: new BorderLayout(), title: CentralCatalogue.getUIS("components.name")){
					panel(layout: new FlowLayout(FlowLayout.LEADING), constraints:BorderLayout.NORTH) {
						label(text: "Show/Hide Component Types")
					}
					scrollPane(layout:new ScrollPaneLayout(), preferredSize:[280, 200], constraints:BorderLayout.CENTER){
						widget(configTree)
					}
					panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints:BorderLayout.SOUTH) {
						button(text: 'Select All', actionPerformed: {toggleViewOptionConfigSelection(true)})
						button(text: 'Unselect All', actionPerformed: {toggleViewOptionConfigSelection(false)})
					}
				}

				panel(layout: new BorderLayout(), title: 'Halos'){
					panel(layout: new FlowLayout(FlowLayout.LEADING), constraints:BorderLayout.NORTH) {
						label(text: "Show/Hide Halos", constraints:BorderLayout.NORTH)
					}
					scrollPane(layout:new ScrollPaneLayout(), constraints:BorderLayout.CENTER, preferredSize:[280, 200]){
						widget(haloTree)
					}
					panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints:BorderLayout.SOUTH) {
						button(text: 'Select All', actionPerformed: {toggleViewOptionHaloSelection(true)})
						button(text: 'Unselect All', actionPerformed: {toggleViewOptionHaloSelection(false)})
					}
				}


				panel(layout: new BorderLayout(), title: 'Associations'){
					panel( layout: new FlowLayout(FlowLayout.LEADING), constraints: BorderLayout.NORTH ){
						viewOptionAssociationDetailChkBox = checkBox(text:'Display Association Detail ', enabled:true, selected:true)
					}
					tb = titledBorder(title:'Types')
					associationTypePanel = panel(border:tb, layout: new SpringLayout(), constraints: BorderLayout.SOUTH ){}
				}

			}
			gbc.gridx=0; gbc.gridy=4; gbc.anchor=GridBagConstraints.CENTER
			panel( layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.SOUTH ){
				viewOptionOKBtn = button(text: 'OK', defaultButton: true, actionPerformed: okAction)
				closeButton = button(text: 'Close', actionPerformed: cancelAction)
				viewOptionApplyBtn = button(text: 'Apply', mnemonic: KeyEvent.VK_A , actionPerformed: applyAction)
				}

			}
			dlgPanel.registerKeyboardAction(cancelAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		}

		int idx =0;
		associationTypes.each{it->
				def lbl = new JLabel("   ")
				lbl.setOpaque(true)
				lbl.setBackground( new Color( Integer.parseInt(associationProperties.getProperty("associations.color."+it), 16) ) )
				lbl.setToolTipText("RGB value: ${associationProperties.getProperty("associations.color."+it)}")
				associationTypePanel.add( lbl )
			
				def descr = associationProperties.getProperty("associations.descr.viewoption."+it)
				def chkBox = builder.checkBox(text:descr, enabled:true, selected:true, name:it);
				chkBox.setToolTipText(descr);
				associationTypePanel.add(chkBox)//,idx);

				idx++;
				associationCheckBoxes.add(chkBox)
		}



		SpringUtilities.makeCompactGrid(associationTypePanel,
                                idx, 2,   //rows, cols
                                6, 6,   //initX, initY
                                5, 5);  //xPad, yPad
		SpringUtilities.makeCompactGrid(generalCheckboxes,
                                6, 1,   //rows, cols
                                6, 6,   //initX, initY
                                5, 5);  //xPad, yPad


		dialogDelegate.pack()
		return dialogDelegate
	}
	
	public void setLocationRelativeTo() {
		dialogDelegate.setLocationRelativeTo(parent)
	}

	public void show() {

		fromOutside = false
		this.requestFocusInWindow()
		dialogDelegate.show()
	}

	public void requestFocusInWindow() {
		viewOptionOKBtn.requestFocusInWindow()
	}
	
	
	private void toggleObjectNameDisplay(){
		if(viewOptionNameChkBox.selected){
			viewOptionNameRdBtn.enabled = true
			viewOptionMacidRdBtn.enabled = true
		}else{
			viewOptionNameRdBtn.enabled = false
			viewOptionMacidRdBtn.enabled = false
		}
	}

	private void toggleDiplayAssociation(){
		if(viewOptionAssociationChkBox.selected){
			viewOptionAssociationDetailChkBox.enabled = true
		}else{
			viewOptionAssociationDetailChkBox.enabled = false
		}
	}
	
	private void displayPower(){
		configTree.toggleGroup('Power & Energy', true)
	}

	private void displayEnvironmentals(){
		configTree.toggleGroup('Environmentals', true)
	}

	private void updateViewOption(){
		String objName = ''

		//if(viewOptionNameChkBox.selected){    Always set this now that we always show names on hover.
			if(viewOptionNameRdBtn.selected){
				objName = viewOptionNameRdBtn.text
			}
			if(viewOptionMacidRdBtn.selected){
				objName = viewOptionMacidRdBtn.text
			}
		//}

		// get selected Object types
		def objectTypes = []
		objectTypes = configTree.getUnselectedNodes()

		//okay! if one of those types is a "staticchildplaceable", we need to also add its staticchildren
		//<children type="horizontal-row-rack-child">
		List<String> adds = []
		for(String c : objectTypes){
			if (cm.listComponentRoles(c).contains("staticchildplaceable")){
				def child = cm.listStaticChildTypeForType(c)
				//println "static child type for $c is $child"
				if (!objectTypes.contains(child)){
					adds.add(child)
				}
			}
		}
		objectTypes.addAll(adds)

		displayProperties.setNameType(objName)
		displayProperties.setProperty('orientation', viewOptionOrientationChkBox.selected)
		displayProperties.setProperty('backgroundImg', viewOptionBgImageChkBox.selected)
        displayProperties.setProperty('name', viewOptionNameChkBox.selected)
		displayProperties.setProperty('associationDetail', viewOptionAssociationDetailChkBox.selected)
		displayProperties.setProperty('invisibleObjects', objectTypes)
		viewOptionAugmentHoverChoices.each {
			if (it.selected){
				displayProperties.setProperty('augmentHover', it.actionCommand  )
			}
		}

		def haloTypes = []
		haloTypes = haloTree.getUnselectedNodes()
		displayProperties.setProperty('invisibleHalos', haloTypes)

		def unselectedAssociationLines = []
		associationCheckBoxes.each{if(!((JCheckBox)it).selected){unselectedAssociationLines.add(it.name)}}
		displayProperties.setProperty('invisibleAssociations', unselectedAssociationLines)

		//unselect anything of those types
		def unselects = []
		for ( String t : objectTypes ){
			for( DLComponent c : cm.getComponentsByType(t) ){
				unselects += c
			}
		}
		sm.removeFromSelection(unselects)

		displayProperties.firePropertyChanged()
	}

	private void toggleViewOptionConfigSelection(boolean isSelected){
		configTree.toggleNodesSelection(isSelected)
	}

	private void toggleViewOptionHaloSelection(boolean isSelected){
		haloTree.toggleNodesSelection(isSelected)

	}

	/**
	 * Sets whether node names are shown on the deployment plan.
	 */
	public void showNames( boolean show ){
		fromOutside = true
		displayProperties.setProperty('name', show )
		viewOptionNameChkBox.selected = show
		displayProperties.firePropertyChanged()
	}

	/**
	 * Returns whether node names are currently shown on the deployment plan.
	 */
	public boolean namesShown(){
		displayProperties.getProperty('name')
	}

	/**
	 * Sets whether node halos are shown on the deployment plan.
	 */
	public void showHalos( boolean show ){
		fromOutside = true
		this.toggleViewOptionHaloSelection(show)
		def haloTypes = []
		haloTypes = haloTree.getUnselectedNodes()
		displayProperties.setProperty('invisibleHalos', haloTypes)
		displayProperties.firePropertyChanged()
	}

	/**
	 * Returns whether node halos are currently shown on the deployment plan.
	 */
	public boolean halosShown(){
		if ( haloTree.getSelectedNodes().size() != 0 ) {
			return true
		} else {
			return false
		}
	}

	/**
	 * Sets whether node associations are shown on the deployment plan.
	 */
	public void showAssociations( boolean show ) {
		fromOutside = true
		associationCheckBoxes.each{ it.selected = show }
		def unselectedAssociationLines = []
		associationCheckBoxes.each{if(!((JCheckBox)it).selected){unselectedAssociationLines.add(it.name)}}
		displayProperties.setProperty('invisibleAssociations', unselectedAssociationLines)
		displayProperties.firePropertyChanged()
	}

	/**
	 * Returns whether node associations are currently shown on the deployment plan.
	 */
	public boolean associationsShown(){
		boolean result = false
		associationCheckBoxes.each{
			if ( ((JCheckBox)it).selected ) {
				result = true
			}
		}
		return ( result )
	}


	/**
	 * Sets whether the association details are shown on when an association is mouse-overed.
	 */
	public void showAssociationDetail( boolean show ) {
		fromOutside = true
		viewOptionAssociationDetailChkBox.selected = show
		displayProperties.setProperty('associationDetail', show)
		displayProperties.firePropertyChanged()
	}

	/**
	 * Returns whether association details are shown on when an association is mouse-overed.
	 */
	public boolean associationDetailShown(){
		return ( displayProperties.getProperty('associationDetail') )
	}


	/**
	 * Sets whether the background image is shown on the deployment plan. 
	 */
	public void showBackground( boolean show ){
		fromOutside = true
		displayProperties.setProperty('backgroundImg', show )
		viewOptionBgImageChkBox.selected = show
		displayProperties.firePropertyChanged()
	}

	/**
	 * Returns whether the background image is currently shown on the deployment plan.
	 */
	public boolean backgroundShown() {
		return ( displayProperties.getProperty('backgroundImg') )
	}

	public void showFloorTilesView(boolean show){
		//log.info("show value:" + show)
		displayProperties.setProperty('floorTilesView', show )
		displayProperties.firePropertyChanged()

	}
	
	public boolean gridViewShown() {
		log.info("gridView in viewoption:" + displayProperties.getProperty('gridView'))
		return ( displayProperties.getProperty('floorTilesView') )
	}


	/**
	 * Toggles the display filter of a specific component type and updates the display.
	 * @param typeName the component type to toggle
	 */
	public void toggleComponentType(String typeName){
		fromOutside = true
		configTree.toggleNode(typeName)
		updateViewOption()
	}

	/**
	 * Toggles the display filter of a specific association type and updates the display.
	 * @param typeName the association type to toggle
	 */
	public void toggleAssociationType(String typeName){
		fromOutside = true
		associationCheckBoxes.each{ cb ->
			if( cb.name.equals( typeName ) ) {
				cb.setSelected( ! cb.isSelected() )
			}
		}
		updateViewOption()
	}

    /**
     * Initialize all view option when creating new project or opening project
     */

    public void initViewOptions(){
        // setup "General" option
        viewOptionOrientationChkBox.selected = true
		viewOptionBgImageChkBox.selected = true

        viewOptionAugmentHoverChoices.each {
            if (it.actionCommand.equals("CLOSE")){
                it.selected = true
            }
        }

        viewOptionNameChkBox.selected = displayProperties.getProperty('name')
        viewOptionNameRdBtn.selected = displayProperties.getNameType().equals('Name')
	    viewOptionMacidRdBtn.selected = displayProperties.getNameType().equals('MacID')

        // setup "Configuration" option
        toggleViewOptionConfigSelection(true)

        // setup "Halo" option
        //toggleViewOptionHaloSelection(true)
		this.showHalos(false) //turn halos off by default

        // setup "Association"
        associationCheckBoxes.each{it.selected=true}

        updateViewOption()
    }

    public void showSelectedTypes(boolean withAssoc = false){
        fromOutside = true
        configTree.toggleNodesSelection(false)
        for(DLComponent dlc : sm.getBaseSelection()){
            this.configTree.toggleNode(dlc.getType(), true)
            if (withAssoc){
                for(DLComponent assocDlc : dlc.getAllAssociatedComponents()){
                    this.configTree.toggleNode(assocDlc.getType(), true)
                }
            }
        }
        updateViewOption()
    }

    public void toggleAllTypes(boolean isSelected){
        fromOutside = true
        configTree.toggleNodesSelection(isSelected)
        updateViewOption()
    }
}