package DeploymentLab.Dialogs.FindReplace

import DeploymentLab.CentralCatalogue
import DeploymentLab.Dialogs.SearchScope
import DeploymentLab.NamePair
import DeploymentLab.channellogger.Logger
import groovy.swing.SwingBuilder
import net.miginfocom.swing.MigLayout

import java.awt.Dialog
import java.awt.FlowLayout
import java.awt.event.ActionEvent
import java.awt.event.KeyEvent
import javax.swing.*

/**
 *
 * @author Gabriel Helman
 * @since
 * Date: 8/14/12
 * Time: 12:02 PM
 */
class FindReplaceDialog {
	private static final Logger log = Logger.getLogger(FindReplaceDialog.class.getName())

	/** All the actual dialog work is delegated to the JDialog class.  */
	protected JDialog dialogDelegate;

	protected FindReplaceController controller

	protected JTextField findTxtField
	protected JTextField replaceTxtField
	protected JComboBox comboScope

	protected JComboBox<NamePair> fieldBox

	/**
	 * Prepare a dialog.
	 * @param parent the JFrame to make the parent of the dialog.
	 * @param delegate The ChangeParentsController that will be running the show.
	 */
	public FindReplaceDialog(FindReplaceController delegate) {
		controller = delegate;
	}

	/**
	 * Construct the dialog.
	 */
	protected void initDialog() {
		def builder = new SwingBuilder()
		def topPanel
		def comboModel = new DefaultComboBoxModel<SearchScope>(SearchScope.getValuesAsVector())

		dialogDelegate = builder.dialog(owner: CentralCatalogue.getParentFrame(), layout: new MigLayout(), resizable: false, title: CentralCatalogue.getUIS("findReplace.dialog.title"), modalityType: Dialog.ModalityType.APPLICATION_MODAL) {
			label(text: ' Find ', constraints: "gapright rel")
			findTxtField = textField(text: '', columns: 10, constraints: "grow x, wrap")
			label(text: ' Replace with ', constraints: "gapright rel")
			replaceTxtField = textField(text: '', columns: 10, constraints: "grow x, wrap")

			label(text: 'Search in Property Name')
			fieldBox = builder.comboBox(constraints: "wrap, growx")

			label(text: "Scope ", constraints: "gapright rel")
			comboScope = comboBox(editable: false, model: comboModel, constraints: "grow x, wrap", actionPerformed: {this.populateFieldBox()})

			panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: "span 2, grow x") {
				button(text: 'OK', defaultButton: true, actionPerformed: {controller.apply()})
				button(text: 'Cancel', actionPerformed: {controller.close()})
			}

			//bindings!
			bind(source: findTxtField, sourceProperty: "text", target: controller, targetProperty: "findStr")
			bind(source: replaceTxtField, sourceProperty: "text", target: controller, targetProperty: "replaceStr")
			bind(source: comboScope, sourceProperty: "selectedItem", target: controller, targetProperty: "scope")
			bind(source: fieldBox, sourceProperty: "selectedItem", target: controller, targetProperty: "selectedField")
		}
		this.populateFieldBox()
		fieldBox.setSelectedItem(new NamePair("name", "Name"))
		setEscapeKeyMap()
		dialogDelegate.pack()
		this.dialogDelegate.setLocationRelativeTo(CentralCatalogue.getParentFrame())
	}


	protected void populateFieldBox() {
		def fields = controller.getFieldsInScope()
		def last = fieldBox.getSelectedItem()
		fieldBox.removeAllItems()
		for (NamePair n : fields) {
			fieldBox.addItem(n)
		}
		if (fieldBox.getSelectedItem() != last) {
			fieldBox.setSelectedItem(last)
		}
	}

	/**
	 * Show the dialog.
	 */
	public void show() {
		if (this.dialogDelegate == null) {
			initDialog()
		}
		this.dialogDelegate.requestFocusInWindow();
		this.dialogDelegate.show();
	}

	/**
	 * Close the dialog.
	 */
	public void close() {
		this.dialogDelegate.setVisible(false);
	}

	/**
	 * Jettison the dialog.
	 */
	public void dispose() {
		this.dialogDelegate.dispose();
		this.dialogDelegate = null;
	}

	private void setEscapeKeyMap() {
		String CANCEL_ACTION_KEY = "CANCEL_ACTION_KEY";
		int noModifiers = 0;
		KeyStroke escapeKey = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, noModifiers, false);
		InputMap inputMap = this.dialogDelegate.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(escapeKey, CANCEL_ACTION_KEY);
		AbstractAction cancelAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		};
		this.dialogDelegate.getRootPane().getActionMap().put(CANCEL_ACTION_KEY, cancelAction);
	}

}
