package DeploymentLab.Dialogs.FindReplace

import DeploymentLab.Dialogs.SearchScope
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.PropertyConverter
import DeploymentLab.channellogger.Logger
import com.google.common.annotations.VisibleForTesting
import groovy.beans.Bindable

import java.util.regex.Pattern

import DeploymentLab.*

class FindReplaceController {
	private static final Logger log = Logger.getLogger(FindReplaceController.class.getName())
	private SelectModel selectModel
	private ComponentModel componentModel
	private UndoBuffer undoBuffer
	FindReplaceDialog dialog

	//Bindable properties whip us up propertychangelistener support "for free"
	@Bindable String findStr
	@Bindable String replaceStr
	@Bindable SearchScope scope
	@Bindable NamePair selectedField

	/**
	 * Static factory methods are my new jam.
	 * @param componentModel The ComponentModel to do a find/replace inside of
	 * @param selectModel The SelectModel for that ComponentModel
	 * @param ub The UndoBuffer for that ComponentModel
	 */
	public static void displayDialog(ComponentModel componentModel, SelectModel selectModel, UndoBuffer ub) {
		new FindReplaceController(componentModel, selectModel, ub).showDialog()
	}

	FindReplaceController(ComponentModel _componentModel, SelectModel _selectModel, UndoBuffer ub) {
		selectModel = _selectModel
		componentModel = _componentModel
		undoBuffer = ub
	}

	public showDialog() {
		this.dialog = new FindReplaceDialog(this)
		dialog.initDialog()

		if (selectModel.getExpandedSelection().size() > 0) {
			dialog.comboScope.setSelectedItem(SearchScope.Selection)
		} else {
			dialog.comboScope.setSelectedItem(SearchScope.Active)
		}
		this.dialog.show()
	}

	protected void apply() {
		// Only close the dialog if the operation was successful and there were no errors.
		if( this.getInputsReplace() ) {
			this.finishUp()
		}
	}

	protected void close() {
		this.finishUp()
	}

	private void finishUp() {
		//clean up before we bail out of here.
		if (dialog) {
			dialog.close()
			dialog.dispose()
			dialog = null
		}
	}

	protected boolean getInputsReplace() {
		log.trace("replace ${findStr.toString()} with ${replaceStr.toString()}")
		log.trace("scope is $scope")
		log.trace("selectedField is $selectedField")

		def watch = new DLStopwatch()
		if (findStr == null || findStr == "") {
			log.warn(CentralCatalogue.getUIS("findReplace.noSearchText"), "message")
			return false
		}

		if(selectedField == null || selectedField == ""){
			log.warn(CentralCatalogue.getUIS("findReplace.noSearchText"), "message")
			return false
		}

		List components
		switch (scope) {
			case SearchScope.All:
				components = componentModel.getComponents()
				break
			case SearchScope.Active:
				components = componentModel.getComponentsInDrawing(selectModel.getActiveDrawing())
				break
			case SearchScope.Selection:
				if (selectModel.getExpandedSelection().size() == 0) {
					log.warn(CentralCatalogue.getUIS("findReplace.noSelection"), "message")
					return false
				}
				components = selectModel.getExpandedSelection()
				break
			default:
				components = []
				break
		}

		try {
			undoBuffer.startOperation()
			for (DLComponent c : components) {
				replace(c, findStr, replaceStr, selectedField.getInternal())
			}
		} catch (Exception e) {
			log.info(log.getStackTrace(e))
			undoBuffer.rollbackOperation()
		} finally {
			undoBuffer.finishOperation()
		}
		log.trace(watch.finishMessage())
		return true
	}


	protected void replace(DLComponent c, String from, String to, String fieldName) {
		//use regex patterns to get locale-independent unicode-aware case insensitivity
		Pattern p1 = Pattern.compile(".*${from}.*".toString(), Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
		Pattern p2 = Pattern.compile(from, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);

		if (c.hasProperty(fieldName)) {
			def prop = c.getComponentProperty(fieldName)
			if (prop.getValueChoices() == null && !PropertyConverter.isComplex(prop.getType())) {
				String value = c.getPropertyStringValue(fieldName)
				if (p1.matcher(value).matches()) {
					String replacedName = p2.matcher(value).replaceAll(to)
					c.setPropertyValue(fieldName, replacedName)
				}
			}
		}
	}

	public Vector<NamePair> getFieldsInScope() {
		def newList
		switch (scope) {
			case SearchScope.All:
				newList = componentModel.getPropertyNames([], true)
				break
			case SearchScope.Active:
				newList = componentModel.getPropertyNames(componentModel.getComponentsInDrawing(selectModel.getActiveDrawing()), true)
				break
			case SearchScope.Selection:
				newList = componentModel.getPropertyNames(selectModel.getExpandedSelection(), true)
				break
		}
		def v = new Vector<NamePair>()
		if(newList){
			v.addAll(newList)
		}
		return v
	}

	@VisibleForTesting
	protected FindReplaceDialog getDialog() {
		if (this.dialog == null) {
			this.dialog = new FindReplaceDialog(this)
		}
		return this.dialog;
	}
}


