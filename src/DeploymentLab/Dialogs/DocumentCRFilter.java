package DeploymentLab.Dialogs;

import javax.swing.text.*;

/**
 * Filters newlines and other "bad" whitespace from a document.
 */
public class DocumentCRFilter extends DocumentFilter {
    boolean DEBUG = false;

    public DocumentCRFilter() {}

    public void insertString(FilterBypass fb, int offs, String str, AttributeSet a) throws BadLocationException {
        if (DEBUG) {
            System.out.println("in DocumentCRFilter's insertString method");
        }
		String filtered = str.replaceAll( "[\\t\\n\\x0B\\f\\r]", "");
		super.insertString(fb, offs, filtered, a);
    }

    public void replace(FilterBypass fb, int offs, int length, String str, AttributeSet a) throws BadLocationException {
        if (DEBUG) {
            System.out.println("in DocumentCRFilter's replace method");
        }
		String filtered = str.replaceAll( "[\\t\\n\\x0B\\f\\r]", "");
		super.replace(fb, offs, length, filtered, a);
    }

}
