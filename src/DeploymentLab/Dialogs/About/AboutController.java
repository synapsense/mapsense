package DeploymentLab.Dialogs.About;

import com.google.common.annotations.VisibleForTesting;

import javax.swing.JFrame;

/**
 * Handles showing the About Box and License Terms.
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 6/22/12
 * Time: 5:07 PM
 * @see AboutDialog
 * @see LicenseDialog
 */
public class AboutController {
	protected AboutDialog dialog;
	protected LicenseDialog licenseDialog;

	public AboutController() {}

	/**
	 * Show the About Box.
	 * @param parentFrame the parent JFrame of the dialog.
	 */
	public void showDialog(JFrame parentFrame) {
		//this.dialog = new AboutDialog(parentFrame, this);
		//this.dialog.show();
		this.getAboutDialog(parentFrame).show();
	}

	/**
	 * Show the License Terms.
	 * @param parentFrame the parent JFrame of the dialog.
	 */
	public void showLicenseDialog(JFrame parentFrame){
		//this.licenseDialog = new LicenseDialog(parentFrame, this);
		//this.licenseDialog.show();
		this.getLicenseDialog(parentFrame).show();
	}

	@VisibleForTesting
	protected AboutDialog getAboutDialog(JFrame parentFrame){
		if(this.dialog==null){
			this.dialog = new AboutDialog(parentFrame, this);
		}
		return this.dialog;
	}

	@VisibleForTesting
	protected LicenseDialog getLicenseDialog(JFrame parentFrame){
		if(this.licenseDialog==null){
			this.licenseDialog = new LicenseDialog(parentFrame, this);
		}
		return this.licenseDialog;
	}
}
