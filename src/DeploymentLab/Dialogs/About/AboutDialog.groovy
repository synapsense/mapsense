package DeploymentLab.Dialogs.About

import DeploymentLab.CentralCatalogue
import DeploymentLab.channellogger.Logger
import groovy.swing.SwingBuilder
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dialog
import java.awt.FlowLayout
import java.awt.event.ActionEvent
import java.awt.event.KeyEvent
import net.miginfocom.swing.MigLayout
import javax.swing.*
import java.awt.event.MouseEvent
import java.awt.event.MouseListener

/**
 * Shows the about box.
 * @author Gabriel Helman
 * @since
 * Date: 6/22/12
 * Time: 5:05 PM
 */
class AboutDialog implements MouseListener {
	private static final Logger log = Logger.getLogger(AboutDialog.class.getName())
	/** Internal SwingBuilder used to construct the dialogDelegate.  */
	protected SwingBuilder builder;
	/** All the actual dialog work is delegated to the JDialog class.  */
	protected JDialog dialogDelegate;
	/** The parent frame of this dialog box. */
	protected JFrame parentFrame;

	private AboutController controller;

	/**
	 * Prepare a dialog.
	 * @param parent the JFrame to make the parent of the dialog.
	 * @param delegate The AboutController that will be running the show.
	 */
	public AboutDialog(JFrame parent, AboutController delegate) {
		this.builder = new SwingBuilder()
		parentFrame = parent;
		controller = delegate;
	}

	/**
	 * Construct the dialog.
	 */
	protected void initDialog() {
		String appName = CentralCatalogue.getApp('application.shortname')
		JLabel namelabel
		dialogDelegate = builder.dialog(owner: parentFrame, layout: new BorderLayout(), title: "About $appName", resizable: false, modalityType: Dialog.ModalityType.APPLICATION_MODAL) {
			panel(constraints: BorderLayout.CENTER, layout: new MigLayout("insets 20px")) {
				namelabel = label(text: CentralCatalogue.getApp('application.name'), constraints: "span 2, wrap")
				label 'Version:'
				label(CentralCatalogue.getApp("build.version"), constraints: "align leading, wrap")
				label 'Build Number:'
				label(CentralCatalogue.getApp("build.number"), constraints: "align leading, wrap")
				label 'Build Revision:'
				label(CentralCatalogue.getApp("build.revision"), constraints: "align leading, wrap")
				label 'Build Date:'
				label(CentralCatalogue.getApp("build.date"), constraints: "align leading, wrap")

				//label("Java Version: ${System.getProperty("java.version")}", constraints: "span 2, wrap" )
				//label("Java VM: ${System.getProperty("java.vm.name")}", constraints: "span 2, wrap" )
				//label("Java Vendor: ${System.getProperty("java.vendor")}", constraints: "span 2, wrap" )
				//label("Groovy Version: ${GroovySystem.version}", constraints: "span 2, wrap")

				label(constraints: "span 2, wrap", text: 'All Rights Reserved.')
				button(text: 'View Software License Terms', constraints: "span 2, wrap", border: BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLUE), UI: new javax.swing.plaf.basic.BasicToggleButtonUI(), background: Color.WHITE, foreground: Color.BLUE, actionPerformed: {
					controller.showLicenseDialog(parentFrame)
				})
			}
			panel(constraints: BorderLayout.SOUTH, layout: new FlowLayout(FlowLayout.TRAILING)) {
				button(text: CentralCatalogue.getUIS("button.ok"), defaultButton: true, actionPerformed: {this.close()})
			}
		}
		namelabel.addMouseListener(this)
		setEscapeKeyMap();
		this.dialogDelegate.pack();
		this.dialogDelegate.setLocationRelativeTo(this.parentFrame);
	}

	/**
	 * Show the dialog.
	 */
	public void show() {
		if (this.dialogDelegate == null) {
			initDialog()
		}
		this.dialogDelegate.requestFocusInWindow();
		this.dialogDelegate.show();
	}

	/**
	 * Close the dialog.
	 */
	public void close() {
		this.dialogDelegate.setVisible(false);
		//this.dialogDelegate.dispose();
		//this.dialogDelegate = null;
	}

	/**
	 * Jettison the dialog.
	 */
	public void dispose() {
		this.dialogDelegate.dispose();
		this.dialogDelegate = null;
	}

	private void setEscapeKeyMap() {
		String CANCEL_ACTION_KEY = "CANCEL_ACTION_KEY";
		int noModifiers = 0;
		KeyStroke escapeKey = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, noModifiers, false);
		InputMap inputMap = this.dialogDelegate.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(escapeKey, CANCEL_ACTION_KEY);
		AbstractAction cancelAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		};
		this.dialogDelegate.getRootPane().getActionMap().put(CANCEL_ACTION_KEY, cancelAction);
	}

	@Override
	void mouseClicked(MouseEvent e) {
		String msg = "TEAM MAPSENSE IS: \n"
		msg += "Garret Smith \n"
		msg += "Hyeeun Lim \n"
		msg += "Gabriel Helman \n"
		msg += "Ken Scoggins \n"

		log.info(msg, "message")
	}

	@Override
	void mousePressed(MouseEvent e) {}

	@Override
	void mouseReleased(MouseEvent e) {}

	@Override
	void mouseEntered(MouseEvent e) {}

	@Override
	void mouseExited(MouseEvent e) {}
}
