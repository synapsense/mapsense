package DeploymentLab.Dialogs.About

import DeploymentLab.channellogger.Logger
import groovy.swing.SwingBuilder
import java.awt.BorderLayout
import java.awt.Dialog
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.awt.event.KeyEvent
import javax.swing.*
import java.awt.FlowLayout
import DeploymentLab.CentralCatalogue

/**
 * Shows the software license terms.
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 6/22/12
 * Time: 5:42 PM
 */
class LicenseDialog {
	private static final Logger log = Logger.getLogger(LicenseDialog.class.getName())
	/** Internal SwingBuilder used to construct the dialogDelegate.  */
	protected SwingBuilder builder;
	/** All the actual dialog work is delegated to the JDialog class.  */
	protected JDialog dialogDelegate;
	/** The parent frame of this dialog box. */
	protected JFrame parentFrame;


	private AboutController controller;

	/**
	 * Prepare a dialog.
	 * @param parent the JFrame to make the parent of the dialog.
	 * @param delegate The AboutController that will be running the show.
	 */
	public LicenseDialog(JFrame parent, AboutController delegate) {
		this.builder = new SwingBuilder()
		parentFrame = parent;
		controller = delegate;
	}

	/**
	 * Construct the dialog.
	 */
	protected void initDialog() {

		dialogDelegate = builder.dialog(owner: parentFrame, resizable: true, title: 'License Terms', layout: new BorderLayout(), modalityType: Dialog.ModalityType.APPLICATION_MODAL) {
			def closeAction = {this.close()}
			def dlgPanel = panel(layout: new BorderLayout(), constraints: BorderLayout.CENTER) {
				scrollPane(constraints: BorderLayout.CENTER, border: BorderFactory.createEtchedBorder(), preferredSize: [600, 400]) {
					editorPane(editable: false, page: getClass().getResource("/resources/license.html"))
				}
				panel(constraints: BorderLayout.SOUTH, layout: new FlowLayout(FlowLayout.TRAILING)) {
					button(text: CentralCatalogue.getUIS("button.ok"), actionPerformed: closeAction)
				}
			}
			dlgPanel.registerKeyboardAction(closeAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			dlgPanel.registerKeyboardAction(closeAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		}
		setEscapeKeyMap();
		this.dialogDelegate.pack();
		this.dialogDelegate.setLocationRelativeTo(this.parentFrame);
	}

	/**
	 * Show the dialog.
	 */
	public void show() {
		if (this.dialogDelegate == null) {
			initDialog()
		}
		this.dialogDelegate.requestFocusInWindow();
		this.dialogDelegate.show();
	}

	/**
	 * Close the dialog.
	 */
	public void close() {
		this.dialogDelegate.setVisible(false);
		//this.dialogDelegate.dispose();
		//this.dialogDelegate = null;
	}

	/**
	 * Jettison the dialog.
	 */
	public void dispose() {
		this.dialogDelegate.dispose();
		this.dialogDelegate = null;
	}

	private void setEscapeKeyMap() {
		String CANCEL_ACTION_KEY = "CANCEL_ACTION_KEY";
		int noModifiers = 0;
		KeyStroke escapeKey = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, noModifiers, false);
		InputMap inputMap = this.dialogDelegate.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(escapeKey, CANCEL_ACTION_KEY);
		AbstractAction cancelAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		};
		this.dialogDelegate.getRootPane().getActionMap().put(CANCEL_ACTION_KEY, cancelAction);
	}

}
