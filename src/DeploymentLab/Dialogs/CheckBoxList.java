package DeploymentLab.Dialogs;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;

public class CheckBoxList extends JList
{
	protected static Border noFocusBorder =
			new EmptyBorder(1, 1, 1, 1);

	public CheckBoxList()
	{
		setCellRenderer(new CellRenderer());

		addMouseListener(new MouseAdapter()
		{
			public void mousePressed(MouseEvent e)
			{
				int index = locationToIndex(e.getPoint());

				if (index != -1) {
					JCheckBox checkbox = (JCheckBox)
							getModel().getElementAt(index);
					checkbox.setSelected(
							!checkbox.isSelected());
					repaint();
				}
			}
		}
		);

		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	protected class CellRenderer implements ListCellRenderer
	{
		public Component getListCellRendererComponent(
				JList list, Object value, int index,
				boolean isSelected, boolean cellHasFocus)
		{
			JCheckBox checkbox = (JCheckBox) value;
//			checkbox.setBackground(isSelected ?
//					Color.black : getBackground());
//			checkbox.setForeground(isSelected ?
//					Color.DARK_GRAY : getForeground());
			checkbox.setEnabled(isEnabled());
			checkbox.setFont(getFont());
			checkbox.setFocusPainted(false);
//			checkbox.setBorderPainted(true);
//			checkbox.setBorder(isSelected ?
//					UIManager.getBorder(
//							"List.focusCellHighlightBorder") : noFocusBorder);
			return checkbox;
		}
	}
}