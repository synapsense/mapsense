package DeploymentLab.Dialogs.ChooseDrawing

import DeploymentLab.CentralCatalogue
import DeploymentLab.Dialogs.ScrollablePanel
import DeploymentLab.Image.ComponentIconFactory
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLComponentNameComparator
import DeploymentLab.channellogger.Logger
import groovy.swing.SwingBuilder
import net.miginfocom.swing.MigLayout

import java.awt.event.ActionEvent
import java.awt.event.KeyEvent
import java.awt.*
import javax.swing.*
import com.google.common.annotations.VisibleForTesting

/**
 * Asks the user to choose a drawing.  Uses basically the same layout as the change parents dialog.
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/5/12
 * Time: 12:54 PM
 * @see DeploymentLab.Dialogs.ChangeParents.ChangeParentsDialog
 */
class ChooseDrawingDialog {
	private static final Logger log = Logger.getLogger(ChooseDrawingDialog.class.getName())
	/** All the actual dialog work is delegated to the JDialog class.  */
	protected JDialog dialogDelegate;
	/** The parent frame of this dialog box. */
	protected JFrame parentFrame;

	private JButton okButton
	private JButton closeButton

	private JPanel mainPanel

	private ChooseDrawingController controller;

	private ArrayList<JCheckBox> checkBoxList = new ArrayList<JCheckBox>()

	/**
	 * Prepare a dialog.
	 * @param parent the JFrame to make the parent of the dialog.
	 * @param delegate The ChangeParentsController that will be running the show.
	 */
	public ChooseDrawingDialog(JFrame parent, ChooseDrawingController delegate) {
		parentFrame = parent;
		controller = delegate;
	}

	/**
	 * Construct the dialog.
	 */
	protected void initDialog() {
		SwingBuilder builder = new SwingBuilder()
		HashMap<DLComponent, Boolean> data = controller.getData()
		ArrayList<DLComponent> sortedKeys = new ArrayList<DLComponent>(data.keySet())
		sortedKeys.sort(new DLComponentNameComparator())
		ButtonGroup group = new ButtonGroup()

		//Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		//def maxSize = new Dimension()

		int numberOfColumns = Math.ceil(sortedKeys.size() / 20.0)
		if (numberOfColumns > 5) {
			numberOfColumns = 5
		}

		dialogDelegate = builder.dialog(name: "chooseDrawingDialog", owner: parentFrame, resizable: false, title: CentralCatalogue.getUIS("chooseDrawing.dialog.title"), layout: new MigLayout(), modalityType: Dialog.ModalityType.APPLICATION_MODAL) {
			scrollPane(constraints: "grow, wrap, span 4", horizontalScrollBarPolicy: JScrollPane.HORIZONTAL_SCROLLBAR_NEVER) {
				mainPanel = panel(layout: new MigLayout("fill"), new ScrollablePanel()) {
					int i = 0;
					for (DLComponent pp : sortedKeys) {

						def currentParent = pp
						//def chk = checkBox(selected: data[currentParent], actionPerformed: {controller.checkboxChecked(currentParent)})
						def chk
						if (controller.allowsMoreThanOneDrawing()) {
							chk = checkBox(selected: data[currentParent], actionPerformed: {controller.checkboxChecked(currentParent)})
						} else {
							chk = radioButton(selected: data[currentParent], actionPerformed: {controller.checkboxChecked(currentParent)}, buttonGroup: group)
						}
						checkBoxList += chk

						def wrap = ""
						if (i == numberOfColumns - 1) {
							wrap = ", wrap"
						}
						label(icon: ComponentIconFactory.getComponentIcon(currentParent), text: currentParent.getName(), constraints: "gapleft rel, gapright unrel, growx $wrap".toString())

						i++;
						if (i == numberOfColumns) {
							i = 0;
						}
					}
				}
			}
			//panel(layout: new MigLayout(), constraints: "growx, wrap, align right") {
				//panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: "growx, wrap, align right, gapleft push") {
			if (controller.allowsMoreThanOneDrawing()) {
					builder.button(text: 'All', defaultButton: true, actionPerformed: { this.selectAll() })
					builder.button(text: 'None', actionPerformed: { this.selectNone() })
			}
				//}
				//panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: "growx, wrap, align right, gapleft push") {
					okButton = builder.button(name: "okButton", text: 'OK', defaultButton: true, actionPerformed: { controller.ok() })
					closeButton = builder.button(name: "cancelButton", text: 'Cancel', actionPerformed: { controller.cancel() })
				//}
			//}
		}
/*
		//set the size of the main panel
		//use the default "preferred size" unless the height is more than 1/2 the screen height; then clamp it at that
		def totalHeight = mainPanel.getPreferredSize().getHeight()
		def finalHeight = 0
		if (dim.getHeight() / 2 < totalHeight) {
			finalHeight = dim.getHeight() / 2
		} else {
			finalHeight = totalHeight
		}
		def finalWidth = mainPanel.getPreferredSize().getWidth()
		maxSize.setSize(finalWidth, finalHeight)
		mainPanel.setPreferredScrollableViewportSize(maxSize)
*/
		setEscapeKeyMap();
		this.dialogDelegate.pack();
		this.dialogDelegate.setLocationRelativeTo(this.parentFrame);
	}


	protected void selectAll(){
		for(JCheckBox c : checkBoxList){
			c.setSelected(true)
		}
		controller.selectAll()
	}

	protected void selectNone(){
		for(JCheckBox c : checkBoxList){
			c.setSelected(false)
		}
		controller.selectNone()
	}

	/**
	 * Show the dialog.
	 */
	public void show() {
		if (this.dialogDelegate == null) {
			initDialog()
		}
		this.dialogDelegate.requestFocusInWindow();
		this.dialogDelegate.show();
	}

	@VisibleForTesting
	protected JDialog getDialog(){
		if (this.dialogDelegate == null) {
			initDialog()
		}
		return dialogDelegate
	}

	/**
	 * Close the dialog.
	 */
	public void close() {
		this.dialogDelegate.setVisible(false);
		//this.dialogDelegate.dispose();
		//this.dialogDelegate = null;
	}

	/**
	 * Jettison the dialog.
	 */
	public void dispose() {
		this.dialogDelegate.dispose();
		this.dialogDelegate = null;
	}

	private void setEscapeKeyMap() {
		String CANCEL_ACTION_KEY = "CANCEL_ACTION_KEY";
		int noModifiers = 0;
		KeyStroke escapeKey = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, noModifiers, false);
		InputMap inputMap = this.dialogDelegate.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(escapeKey, CANCEL_ACTION_KEY);
		AbstractAction cancelAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		};
		this.dialogDelegate.getRootPane().getActionMap().put(CANCEL_ACTION_KEY, cancelAction);
	}

}
