package DeploymentLab.Dialogs.ChooseDrawing;

import DeploymentLab.CentralCatalogue;
import DeploymentLab.Exceptions.ModelCatastrophe;
import DeploymentLab.Model.ComponentModel;
import DeploymentLab.Model.DLComponent;
import DeploymentLab.Model.DLComponentNameComparator;
import DeploymentLab.Model.DLObject;
import DeploymentLab.Model.ObjectType;
import DeploymentLab.SelectModel;
import DeploymentLab.channellogger.Logger;
import com.google.common.annotations.VisibleForTesting;

import javax.swing.*;
import java.util.*;

/**
 * Generic dialog and controller to allow the user to select one or more drawings.  Generally, only the static
 * helper methods should be called from code elsewhere in the program.
 *
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/5/12
 * Time: 12:51 PM
 */
public class ChooseDrawingController {

	/**
	 * Let the user choose a single drawing
	 * @param sm the SelectModel to choose from
	 * @param cm the ComponentModel to choose from
	 * @return a List with 0 or 1 drawings.  This is a little silly to have as a list, but kept that way for consistancy.
	 */
	public static List<DLComponent> chooseOneDrawing(SelectModel sm, ComponentModel cm) {
		return ChooseDrawingController.chooseDrawing(sm, cm, false, false);
	}

	public static List<DLComponent> chooseDrawing(SelectModel sm, ComponentModel cm) {
		return ChooseDrawingController.chooseDrawing(sm, cm, false, true);
	}

	public static List<DLComponent> chooseDrawing(SelectModel sm, ComponentModel cm, boolean defaultAll) {
		return ChooseDrawingController.chooseDrawing(sm, cm, defaultAll, true);
	}

	public static List<DLComponent> chooseDrawing(SelectModel sm, ComponentModel cm, boolean defaultAll, boolean allowMoreThanOne) {
		ChooseDrawingController cfc = new ChooseDrawingController(sm, cm);
		cfc.setAllowsMoreThanOneDrawing(allowMoreThanOne);

		//for the default all case, just flip all the boxes to true before we do anything else
		if(defaultAll){
			for(DLComponent c : cfc.getData().keySet()){
				cfc.getData().put(c,true);
			}
		}

		cfc.checkAndShow(CentralCatalogue.getParentFrame());

		return cfc.generateResults();
	}

	public static List<DLObject> chooseDrawingAsObjects(SelectModel sm, ComponentModel cm, boolean defaultAll){
		return getDrawingObjects(chooseDrawing(sm, cm, defaultAll, true));
	}

	private static final Logger log = Logger.getLogger(ChooseDrawingController.class.getName());
	private ChooseDrawingDialog dialog;

	private SelectModel selectModel;
	private ComponentModel componentModel;
	private HashMap<DLComponent, Boolean> _drawings;
	private boolean decidedToDoIt;

	private boolean allowTheUserToSelectMoreThanOneDrawing = true;

	protected ChooseDrawingController(SelectModel sm, ComponentModel cm) {
		this.selectModel = sm;
		this.componentModel = cm;
		this.decidedToDoIt = false;
	}


	protected void showDialog(JFrame parentFrame) {
		this.dialog = new ChooseDrawingDialog(parentFrame, this);
		this.dialog.show();
	}

	protected void finishUp() {
		//clean up before we bail out of here.
		if (dialog != null) {
			dialog.close();
			dialog.dispose();
			dialog = null;
		}
	}

	/**
	 * @return a map of drawings : currently selected
	 */
	protected HashMap<DLComponent, Boolean> getData() {
		if (_drawings == null) {
			_drawings = new HashMap<DLComponent, Boolean>();
			for (DLComponent c : componentModel.getComponentsInRole("drawing")) {
				_drawings.put(c, false);
			}
			if(selectModel.getActiveDrawing() != null){
				if(!_drawings.containsKey(selectModel.getActiveDrawing())){
					throw new ModelCatastrophe("The Active Drawing is not in the Component Model!");
				}
				_drawings.put(selectModel.getActiveDrawing(), true);
			}
		}
		return _drawings;
	}

	protected void checkboxChecked(DLComponent c) {
		//log.debug("checkboxChecked $c")
		if(allowTheUserToSelectMoreThanOneDrawing){
			this.getData().put(c, !this.getData().get(c));
		} else {
			//this.getData().put(c, true);
			for( DLComponent key: this.getData().keySet()){
				if(key == c){
					this.getData().put(key,true);
				} else{
					this.getData().put(key,false);
				}
			}

		}
	}

	protected void ok() {
		decidedToDoIt = true;
		this.finishUp();
	}

	protected void cancel() {
		decidedToDoIt = false;
		this.finishUp();
	}


	public void selectAll() {
		for(DLComponent k : this.getData().keySet()){
			this.getData().put(k, true);
		}
	}

	public void selectNone() {
		for(DLComponent k : this.getData().keySet()){
			this.getData().put(k, false);
		}
	}

	protected void checkAndShow(JFrame f){
		if(this.getData().size() <= 1){
			//if there is only one drawing, just return that without showing the dialog
			decidedToDoIt = true;
		} else {
			this.showDialog(f);
		}
	}

	protected List<DLComponent> generateResults(){
		if (!this.decidedToDoIt) {
			return new ArrayList<DLComponent>();
		}

		ArrayList<DLComponent> result = new ArrayList<DLComponent>(this.getData().size());
		for (DLComponent c : this.getData().keySet()) {
			if (this.getData().get(c)) {
				result.add(c);
			}
		}
		Collections.sort(result, new DLComponentNameComparator());
		return result;
	}

	@VisibleForTesting
	protected ChooseDrawingDialog getDialog(){
		if(dialog == null){
			this.dialog = new ChooseDrawingDialog(null, this);
		}
		return this.dialog;
	}

	public static List<DLObject> getDrawingObjects(List<DLComponent> drawingComponents){
		List<DLObject> result = new ArrayList<DLObject>(drawingComponents.size());
		for(DLComponent c : drawingComponents){
			if(!c.hasRole("drawing")){
				throw new UnsupportedOperationException("Just drawings, please!");
			}
			result.add(c.getRoot( ObjectType.DRAWING ));
		}
		return result;
	}

	public void setAllowsMoreThanOneDrawing(boolean allowed){
		allowTheUserToSelectMoreThanOneDrawing = allowed;
	}

	public boolean allowsMoreThanOneDrawing() {
		return allowTheUserToSelectMoreThanOneDrawing;
	}

}
