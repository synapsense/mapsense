package DeploymentLab.Dialogs

import DeploymentLab.CentralCatalogue
import DeploymentLab.Model.DLComponent
import DeploymentLab.SelectModel
import DeploymentLab.UndoBuffer
import DeploymentLab.channellogger.Logger
import groovy.swing.SwingBuilder
import net.miginfocom.swing.MigLayout

import javax.swing.*
import javax.swing.event.DocumentEvent
import javax.swing.event.DocumentListener
import java.awt.*

/**
 * @author Vadim Gusev
 * @since Aireo
 */
class CanvasSettingsDialog {

	private static final Logger log = Logger.getLogger(CanvasSettingsDialog.class.getName())
	protected SwingBuilder builder
	private JDialog dialogDelegate
	private JButton okButton
	private JFrame parent
	private def dlw

	private JTextField widthField
	private JTextField heightField
	private JCheckBox shrinkBox

	private JLabel borderLabel
	private JSpinner borderSpinner

	private JLabel errorLabel

	private int width
	private int height

	public CanvasSettingsDialog(JFrame parent, def dlw) {
		builder = new SwingBuilder()
		this.parent = parent
		this.dlw = dlw
		this.dialogDelegate = this.getDialog();
	}

	private JDialog getDialog() {
		dialogDelegate = builder.dialog(owner: parent, resizable: false, title: 'Set Canvas Size', layout: new BorderLayout(), modalityType: Dialog.ModalityType.APPLICATION_MODAL) {
			def okAction = {(!validateAndSet())?:dialogDelegate.setVisible(false)}
			def cancelAction = {dialogDelegate.setVisible(false)}
			def shrinkAction = {
				borderSpinner.enabled = shrinkBox.selected
				widthField.editable = !shrinkBox.selected
				heightField.editable = !shrinkBox.selected
				if (shrinkBox.selected){
					width = CentralCatalogue.getDeploymentPanel().getSmallestAllowedCanvas().getWidth()
					height = CentralCatalogue.getDeploymentPanel().getSmallestAllowedCanvas().getHeight()
					widthField.setText((width + (int)borderSpinner.getValue()).toString())
					heightField.setText((height  + (int)borderSpinner.getValue()).toString())
				}
			}
			def borderChanged = {
				widthField.setText((width + (int)borderSpinner.getValue()).toString())
				heightField.setText((height  + (int)borderSpinner.getValue()).toString())
			}

			panel(layout: new BorderLayout(), constraints: BorderLayout.CENTER){
				panel(layout: new MigLayout("center"), constraints: BorderLayout.CENTER){
					label(text: "Width", )
					widthField = textField(columns: 4)
					separator(orientation: SwingConstants.VERTICAL, constraints: "span 1 2, growy")
					shrinkBox = checkBox(text: "Best Fit", constraints: "span, wrap", actionPerformed: shrinkAction)
					label(text: "Height",)
					heightField = textField(columns: 4)
					borderLabel = label(text: "Border", constraints: "split 2")
					borderSpinner = spinner(model: spinnerNumberModel(minimum: 0, stepSize: 1, stateChanged: borderChanged), enabled: false, constraints: "wrap")
					borderSpinner.getEditor().getTextField().setColumns(4)
				}
				panel(layout: new FlowLayout(FlowLayout.CENTER),  constraints: BorderLayout.SOUTH ) {
					errorLabel = label(text: "", foreground: Color.red)
				}
			}

			DocumentListener listener = new DocumentListener() {
				public void changedUpdate(DocumentEvent e) {
					clearIndicator();
				}
				public void removeUpdate(DocumentEvent e) {
					clearIndicator();
				}
				public void insertUpdate(DocumentEvent e) {
					clearIndicator();
				}
				public void clearIndicator() {
					errorLabel.setText("")
				}
			}

			widthField.getDocument().addDocumentListener(listener)
			heightField.getDocument().addDocumentListener(listener)

			panel(layout: new FlowLayout(FlowLayout.TRAILING),  constraints: BorderLayout.SOUTH ) {
				okButton = button(text: 'OK', defaultButton: true, actionPerformed: okAction)
				button(text: 'Cancel', actionPerformed: cancelAction)
			}

		}

		dialogDelegate.pack()
		return dialogDelegate
	}

	public void setLocationRelativeTo() {
		dialogDelegate.setLocationRelativeTo(parent)
	}

	public void show() {
		reloadData()
		this.setLocationRelativeTo()
		this.requestFocusInWindow()
		dialogDelegate.show()
	}

	public void requestFocusInWindow() {
		okButton.requestFocusInWindow()
	}

	public void reloadData(){
		SelectModel sm = dlw.selectModel
		DLComponent drawing = sm.activeDrawing
		width = ((Double)(drawing.getPropertyValue("width"))).intValue()
		height = ((Double)(drawing.getPropertyValue("height"))).intValue()
		widthField.setText(width.toString())
		heightField.setText(height.toString())

		shrinkBox.setSelected(false)
		borderSpinner.setValue(0)
		borderSpinner.setEnabled(false)
		widthField.setEditable(true)
		heightField.setEditable(true)
	}

	private boolean validateAndSet(){
		int width
		int height
		try{
			width = Integer.parseInt(widthField.getText())
		}catch (NumberFormatException e){
			errorLabel.setText("Incorrect width value. Must be int.")
			return false;
		}

		try{
			height = Integer.parseInt(heightField.getText())
		}catch (NumberFormatException e){
			errorLabel.setText("Incorrect height value. Must be int.")
			return false;
		}

		int bWidth = CentralCatalogue.getDeploymentPanel().getSmallestAllowedCanvas().getWidth()
		int bHeight = CentralCatalogue.getDeploymentPanel().getSmallestAllowedCanvas().getHeight()
		log.debug("Full bounds: $bWidth x $bHeight")
		if (bWidth > width){
			errorLabel.setText("Width less than min bound [$bWidth].")
			return false;
		}
		if (bHeight > height){
			errorLabel.setText("Height less than min bound [$bHeight].")
			return false;
		}

		SelectModel sm = dlw.selectModel
		UndoBuffer ub = dlw.undoBuffer
		DLComponent drawing = sm.activeDrawing

		try {
			ub.startOperation()
			drawing.setPropertyValue('width', width)
			drawing.setPropertyValue('height', height)
			dlw.deploymentPanel.fitView()
		}finally {
			ub.finishOperation()
		}

		return true
	}
}
