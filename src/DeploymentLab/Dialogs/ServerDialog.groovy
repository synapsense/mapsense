package DeploymentLab.Dialogs

import DeploymentLab.CentralCatalogue
import DeploymentLab.channellogger.*
import DeploymentLab.SpringUtilities
import com.google.common.net.InetAddresses

import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dialog.ModalityType
import java.awt.event.ActionListener
import java.awt.event.KeyEvent
import java.awt.FlowLayout
import java.awt.Toolkit

import javax.swing.JComponent
import javax.swing.JButton
import javax.swing.JDialog
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JTextField
import javax.swing.KeyStroke
import javax.swing.SpringLayout

import groovy.swing.SwingBuilder

/**
 * Dialog to get SmartZone server URL and port .
 */
class ServerDialog {
    private static final Logger log = Logger.getLogger(ServerDialog.class.getName())
    protected SwingBuilder builder
    private JDialog dialogDelegate
    private JButton okButton
    private JTextField serverField
    private JTextField portField
    private boolean isLogged = false
    private JLabel capsLockIndicator
    private JFrame parent
    private JLabel errorMsgLabel


    public ServerDialog(JFrame _parent) {
        builder = new SwingBuilder()
        parent = _parent
        dialogDelegate = this.getDialog()
    }

    public boolean isLogged() {
        return isLogged;
    }

    public String getHost() {
        return serverField.text;
    }

    public int getPort() {
        return Integer.parseInt(portField.text);
    }

    private boolean validateServer(String serverName) {
        boolean status = true;
        if (serverName == null || serverName.isEmpty() ||
            !(InetAddresses.isInetAddress( serverName ) || serverName.matches('[^.][a-zA-Z0-9_.-]+'))) {
            log.error(CentralCatalogue.getUIS("validator.ip.invalidHost"), "message")
            this.serverField.requestFocusInWindow()
            this.serverField.selectAll()
            status = false;
        }
        return status;
    }

    private boolean validatePort(String serverPort) {
        boolean status = true;
        int serverPortInt
        try {
            serverPortInt = Integer.parseInt(serverPort);
            if ( ( serverPortInt < 1 ) || ( serverPortInt > 65535 ) ) {
                log.error(CentralCatalogue.getUIS("validator.ip.invalidPort"), "message")
                this.portField.requestFocusInWindow()
                this.portField.selectAll()
                status = false
            }
        } catch (NumberFormatException nfe) {
            log.error(CentralCatalogue.getUIS("validator.ip.invalidPort"), "message")
            this.portField.requestFocusInWindow()
            this.portField.selectAll()
            status = false
        }
        return status;
    }

    private JDialog getDialog() {
        dialogDelegate = builder.dialog(owner:parent, resizable: false, title: "SmartZone Server Login", layout: new BorderLayout(), modalityType: ModalityType.APPLICATION_MODAL) {
            def okAction = {
                if (validateServer(serverField.getText().trim()) &&
                    validatePort(portField.getText().trim())) {
                    isLogged = true;
                    dialogDelegate.setVisible(false)
                }
            }
            def cancelAction = {
                isLogged = false
                dialogDelegate.setVisible(false)
            }
            def capsLockAction = {setCapsLockIndicator()}
            def dlgPanel = panel(layout: new SpringLayout(), constraints: BorderLayout.NORTH) {
                label(text: "")
                errorMsgLabel = label(id: 'errorMsg', text: '                                   ', foreground: Color.red)

                label(text: "SmartZone Server:")
                serverField = textField(id: 'hostName', columns: 18)

                label(text: "Port:")
                portField = textField(id: 'serverPort', text: '8443', columns: 18)
            }
            panel(layout: new FlowLayout(FlowLayout.CENTER), constraints: BorderLayout.CENTER) {
                capsLockIndicator = label(text:"          ", foreground: Color.red)
            }
            panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.SOUTH) {
                okButton = button(text: 'OK', defaultButton: true, actionPerformed: okAction)
                button(text: 'Cancel', actionPerformed: cancelAction)
            }
            SpringUtilities.makeCompactGrid(dlgPanel,
                    3, 2,   //rows, cols
                    6, 6,   //initX, initY
                    5, 5);  //xPad, yPad
            dlgPanel.registerKeyboardAction(okAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
            dlgPanel.registerKeyboardAction(cancelAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
            dlgPanel.registerKeyboardAction(capsLockAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_CAPS_LOCK, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)

        }
        dialogDelegate.pack()
        return dialogDelegate
    }

    public boolean errorState() {
        return (errorMsgLabel.getText().trim() != "")
    }

    private void setCapsLockIndicator(){
        try {
            if(Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK)){
                capsLockIndicator.setText("Caps Lock: on")
            }else{
                capsLockIndicator.setText("  ") //we'd just as well show nothing when the caps lock is off
            }
        } catch(Exception e) { // some environments (BSD) don't support the getLockingKeyState method.
            capsLockIndicator.setText("")
        }
    }

    public void setErrorMsg(String errorMsg) {
        isLogged = false
        errorMsgLabel.setText(errorMsg)
        okButton.setText("Retry")
    }

    public void setLocationRelativeTo() {
        dialogDelegate.setLocationRelativeTo(parent)
    }

    public void show(){
        serverField.setText( CentralCatalogue.getSystemSettings().getSmartZoneHost() )
        portField.setText("${CentralCatalogue.getSystemSettings().getSmartZonePort()}")
        this.requestFocusInWindow()
        setCapsLockIndicator()
        dialogDelegate.show()
    }

    public void requestFocusInWindow() {
        this.serverField.requestFocusInWindow()
    }

    public boolean getOpenFromSmartZoneInfo() {
        setLocationRelativeTo()
        requestFocusInWindow()
        show()

        if(!isLogged) {
            return false
        } else {
            CentralCatalogue.getSystemSettings().setSmartZoneHost( serverField.text.trim() )
            CentralCatalogue.getSystemSettings().setSmartZonePort( Integer.parseInt(portField.text.trim()) )
            CentralCatalogue.getSmartZoneConnecionProperties().remove("username")
            CentralCatalogue.getSmartZoneConnecionProperties().remove("password")
            return true
        }
    }
}
