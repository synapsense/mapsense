package DeploymentLab.Dialogs

import DeploymentLab.CentralCatalogue
import DeploymentLab.Export.DLComponentValueExporter
import DeploymentLab.Export.ExcelWriter
import DeploymentLab.FileFilters
import DeploymentLab.Image.ComponentIconFactory
import DeploymentLab.Model.ComponentModel
import DeploymentLab.UserPreferences
import DeploymentLab.channellogger.Logger
import groovy.swing.SwingBuilder

import javax.swing.JButton
import javax.swing.JCheckBox
import javax.swing.JComponent
import javax.swing.JDialog
import javax.swing.JFileChooser
import javax.swing.JFrame
import javax.swing.JList
import javax.swing.JOptionPane
import javax.swing.KeyStroke
import javax.swing.ListModel
import javax.swing.ScrollPaneLayout
import java.awt.BorderLayout
import java.awt.Dialog
import java.awt.FlowLayout
import java.awt.event.ActionListener
import java.awt.event.ItemEvent
import java.awt.event.ItemListener
import java.awt.event.KeyEvent

/**
 * @author Vadim Gusev
 * @since Europa
 * Date: 09.04.13
 * Time: 16:14
 */
class ExportComponentDialog implements ItemListener {
	private static final Logger log = Logger.getLogger(ExportComponentDialog.class.getName())
	protected SwingBuilder builder
	private JDialog dialogDelegate
	private JButton openButton
	private JFrame parent
	private JList list
	private def dlw;
	private Map<String, String> types;
	private Map<String, Boolean> selectionMap = new HashMap<String, Boolean>()


	public ExportComponentDialog(JFrame _parent, def dlw){
		builder = new SwingBuilder()
		parent = _parent
		this.dlw = dlw
	}

	private JDialog getDialog() {
		dialogDelegate = builder.dialog(
				owner:parent,
				resizable: false,
				title: "Export Component Properties",
				layout: new BorderLayout(),
				modalityType: Dialog.ModalityType.APPLICATION_MODAL,
				iconImage: ComponentIconFactory.defaultIcon.getImage()) {
			def exportAction = {
				Set<String> cTypes = new TreeSet<>();
				ListModel listModel = list.getModel();
				for(int i = 0; i<listModel.size; i++){
					def chb = ((JCheckBox)listModel.getElementAt(i))
					log.trace(chb.getText()+" - "+ types.get(chb.getText()))
					if (chb.isSelected()){
						cTypes.add(types.get(chb.getText()))
					}
				}
				if (cTypes.isEmpty()){
					log.info("Select Component Types To Export!", "message")
					return;
				}
				dialogDelegate.setVisible(false);
				export(cTypes); }
			def cancelAction = {dialogDelegate.setVisible(false); dialogDelegate.dispose();}
			def dlgPanel = panel(layout: new BorderLayout()){
				panel(layout: new FlowLayout(FlowLayout.LEADING), constraints:BorderLayout.NORTH) {
					label(text: "Choose Components To Export")
				}
				scrollPane(layout:new ScrollPaneLayout(), preferredSize:[300, 400], constraints:BorderLayout.CENTER){
					list = new CheckBoxList();
					JCheckBox[] newList = new JCheckBox[types.size()]
					int i = 0;
					for(String typeName : types.keySet()){
						newList[i] = new JCheckBox(typeName)
						newList[i].setSelected(selectionMap.get(typeName))
						newList[i].addItemListener(this)
						i++
					}
					list.setListData(newList)
					widget(list)
				}
				panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints:BorderLayout.SOUTH) {
					button(text: 'Select All', actionPerformed: {selectAll(true)})
					button(text: 'Unselect All', actionPerformed: {selectAll(false)})
				}
			}
			panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.SOUTH) {
				openButton = button(text: 'Export', defaultButton: true, actionPerformed: exportAction)
				button(text: 'Cancel', actionPerformed: cancelAction)
			}

			dlgPanel.registerKeyboardAction(exportAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			dlgPanel.registerKeyboardAction(cancelAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		}
		dialogDelegate.pack()
		return dialogDelegate
	}

	private void selectAll(boolean select){
		ListModel listModel = list.getModel();
		for(int i = 0; i<listModel.size; i++){
			def chb = ((JCheckBox)listModel.getElementAt(i))
			chb.setSelected(select)
		}
		list.repaint();
	}

	private void requestFocusInWindow() {
		openButton.requestFocusInWindow()
	}

	private void setLocationRelativeTo() {
		dialogDelegate.setLocationRelativeTo(parent)
	}

	private void show(){
		dialogDelegate.show()
	}

	public void showExportDialog(){
		types = new TreeMap<>()
		ComponentModel cmodel = dlw.componentModel
		for (String type : cmodel.getComponentTypes().keySet()) {
			 if (!cmodel.getComponentsByType(type).isEmpty()){
				 String displayType = cmodel.getTypeDisplayName(type)
				 types.put(displayType, type)
				 if (!selectionMap.containsKey(displayType)){
					 selectionMap.put(displayType, Boolean.TRUE)
				 }
			 }
		}
		selectionMap.keySet().retainAll(types.keySet())
		dialogDelegate = this.getDialog()
		setLocationRelativeTo()
		requestFocusInWindow()
		show()
	}

	private void export(Set<String> cTypes){
		def exportFileChooser = new JFileChooser()
		exportFileChooser.addChoosableFileFilter(FileFilters.ExcelFile.getFilter())
		exportFileChooser.setFileFilter(FileFilters.ExcelFile.getFilter())
		exportFileChooser.setDialogTitle(CentralCatalogue.getUIS("exportComponentProperties.filechooser.title"))
		exportFileChooser.setCurrentDirectory(new File(dlw.userPreferences.getUserPreference(UserPreferences.EXCEL)))

		int rv = exportFileChooser.showSaveDialog(dialogDelegate)
		if (rv == JFileChooser.APPROVE_OPTION) {
			String selectedPath = exportFileChooser.getSelectedFile().getPath()

			if (!selectedPath.endsWith(".xlsx")) {
				selectedPath = selectedPath + ".xlsx"
			}

			File selectedFile = new File(selectedPath)
			dlw.userPreferences.setUserPreference(UserPreferences.EXCEL, selectedFile.getParent())
			if(selectedFile.exists()) {
				int res = JOptionPane.showConfirmDialog(parent, "Are you sure you wish to overwrite '" + selectedFile.getPath() + "'?", 'Confirm Overwrite File', JOptionPane.OK_CANCEL_OPTION)
				if(res != JOptionPane.OK_OPTION) {
					log.info("SaveAs canceled.")
					dialogDelegate.setVisible(true);
					return
				}
			}

			dialogDelegate.dispose()

			dlw.doBackgroundTask({
				log.info("Exporting Components...", "progress")
				def values = DLComponentValueExporter.exportComponents(dlw.componentModel, cTypes);
				log.info("Saving File...", "progress")
				try{
					ExcelWriter.write(selectedPath, values)
				}catch (FileNotFoundException e){
					JOptionPane.showMessageDialog(parent, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE)
					log.error("File not saved!", "statusbar")
					return;
				}
				log.info("File saved to $selectedPath", "statusbar")
			})
		}else {
			dialogDelegate.setVisible(true);
		}

	}

	@Override
	void itemStateChanged(ItemEvent e) {
		JCheckBox cb = (JCheckBox)e.item
		log.trace("Set selection: ${cb.getText()} to ${cb.isSelected()}")
		selectionMap.put(cb.getText(), cb.isSelected())
	}

	public void clearSelection(){
		selectionMap.clear()
	}
}
