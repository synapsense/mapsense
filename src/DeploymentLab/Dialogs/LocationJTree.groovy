package DeploymentLab.Dialogs

import DeploymentLab.Image.ComponentIconFactory
import com.panduit.sz.api.ss.assets.LocationLevel

import javax.swing.*
import javax.swing.event.TreeSelectionEvent
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer
import javax.swing.tree.TreeSelectionModel

import com.panduit.sz.api.ss.assets.Location

import java.awt.Component

class CustomTreeCellRenderer extends DefaultTreeCellRenderer{

    private ImageIcon companyGreenII = ComponentIconFactory.getImageIcon("/resources/company_green_16x16.png");
    private ImageIcon countryGreenII = ComponentIconFactory.getImageIcon("/resources/country_green_16x16.png");
    private ImageIcon stateGreenII = ComponentIconFactory.getImageIcon("/resources/state_green_16x16.png");
    private ImageIcon cityGreenII = ComponentIconFactory.getImageIcon("/resources/city_green_16x16.png");
    private ImageIcon buildingGreenII = ComponentIconFactory.getImageIcon("/resources/building_green_16x16.png");
    private ImageIcon streetGreenII = ComponentIconFactory.getImageIcon("/resources/street_green_16x16.png");
    private ImageIcon floorGreenII = ComponentIconFactory.getImageIcon("/resources/floor_green_16x16.png");
    private ImageIcon roomGreenII = ComponentIconFactory.getImageIcon("/resources/room_green_16x16.png");
    private ImageIcon otherGreenII = ComponentIconFactory.getImageIcon("/resources/other_green_16x16.png");

    public Component getTreeCellRendererComponent(JTree tree,
                                                  Object value, boolean selected, boolean expanded,
                                                  boolean leaf, int row, boolean hasFocus)
    {
        super.getTreeCellRendererComponent(tree, value,
                selected, expanded, leaf, row, hasFocus);

        DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode)value;
        if (defaultMutableTreeNode.getUserObject() instanceof String) {
            return this;
        } else {
            if (defaultMutableTreeNode.getUserObject() instanceof Location) {
                JLabel label = (JLabel) this;
                Location loc = (Location) defaultMutableTreeNode.getUserObject();
                if (loc.getLocationLevel() == LocationLevel.COUNTRY) {
                    setIcon(countryGreenII);
                } else if (loc.getLocationLevel() == LocationLevel.STATE) {
                    setIcon(stateGreenII);
                } else if (loc.getLocationLevel() == LocationLevel.COMPANY) {
                    setIcon(companyGreenII);
                } else if (loc.getLocationLevel() == LocationLevel.CITY) {
                    setIcon(cityGreenII);
                } else if (loc.getLocationLevel() == LocationLevel.BUILDING) {
                    setIcon(buildingGreenII);
                } else if (loc.getLocationLevel() == LocationLevel.STREET) {
                    setIcon(streetGreenII);
                } else if (loc.getLocationLevel() == LocationLevel.ROOM) {
                    setIcon(roomGreenII);
                } else if (loc.getLocationLevel() == LocationLevel.FLOOR) {
                    setIcon(floorGreenII);
                } else {
                    setIcon(otherGreenII);
                }

                if( loc.wsnInstrumented ) {
                    setEnabled( false )
                    label.setText( "<html>${loc.getName()} <i>(instrumented)</i><html>")
                } else {
                    label.setText( loc.getName() )
                }
            }
        }
        return this;
    }
}

class LocationJTree extends JTree {

    public LocationJTree(DefaultMutableTreeNode defaultMutableTreeNode) {
        super(defaultMutableTreeNode);
        CustomTreeCellRenderer customTreeCellRenderer = new CustomTreeCellRenderer();
        this.setCellRenderer(customTreeCellRenderer);
        this.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    }

    public void registerImportButton(JButton button) {
        this.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            void valueChanged(TreeSelectionEvent e) {
                DefaultMutableTreeNode dmtn = e.getPath().getLastPathComponent();
                if (dmtn != null) {
                    Location location = (Location)dmtn.getUserObject();
                    if (location.getLocationLevel() == LocationLevel.FLOOR && !location.isWsnInstrumented()) {
                        button.setEnabled(true);
                    } else {
                        button.setEnabled(false);
                    }
                }
            }
        })
    }
}
