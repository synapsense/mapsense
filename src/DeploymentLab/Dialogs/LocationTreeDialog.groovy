package DeploymentLab.Dialogs

import DeploymentLab.channellogger.Logger
import com.panduit.sz.api.ss.assets.Location
import groovy.swing.SwingBuilder

import javax.swing.JButton
import javax.swing.JDialog
import javax.swing.JFrame
import javax.swing.ScrollPaneLayout
import javax.swing.tree.DefaultMutableTreeNode
import java.awt.BorderLayout
import java.awt.Dialog
import java.awt.FlowLayout


/**
 * Created by ccr on 12/14/2015.
 */
class LocationTreeDialog {
    private static final Logger log = Logger.getLogger(LocationTreeDialog.class.getName())
    private SwingBuilder builder
    private JDialog dialog
    private LocationJTree locationTree
    private JFrame parent
    private Location selectedLocation;
    private JButton importButton;

    public LocationTreeDialog(
            LocationJTree tree,
            JFrame _parent) {
        locationTree = tree
        parent = _parent
        builder = new SwingBuilder()
        getConfigDialog()
    }

    public Location getSelectedLocation() {
        if (locationTree.getSelectionModel().getSelectionCount() == 1) {
            DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode)locationTree.getSelectionModel().getSelectionPath().getLastPathComponent();
            selectedLocation = (Location)defaultMutableTreeNode.getUserObject();
        }
        return selectedLocation;
    }

    public JButton getImportButton() {
        return importButton;
    }

    /**
     * Create dialog box to choose new configuration
     */
    public void getConfigDialog() {

        dialog = builder.dialog(
                owner: parent,
                layout: new BorderLayout(),
                resizable: true,
                title: 'Import Location',
                modalityType: Dialog.ModalityType.APPLICATION_MODAL) {

            panel(  layout: new BorderLayout(),
                    constraints: BorderLayout.CENTER) {
                label(text: "Select Location", constraints: BorderLayout.NORTH)
                scrollPane(layout: new ScrollPaneLayout(), constraints: BorderLayout.CENTER, preferredSize: [400, 300]) {
                    widget(locationTree)
                }
            }

            panel(  layout: new FlowLayout(FlowLayout.TRAILING),
                    constraints: BorderLayout.SOUTH) {
                importButton = button(text: 'Import Location', enabled:false,
                        actionPerformed: {
                            dialog.setVisible(false)
                        }
                )
                button(text: 'Cancel', actionPerformed: {dialog.setVisible(false)})
            }
        }
        dialog.pack()
    }

    public void openLocationDialog() {
        dialog.setLocationRelativeTo(parent)
        dialog.show()
    }
}
