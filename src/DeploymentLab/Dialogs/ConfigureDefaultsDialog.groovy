package DeploymentLab.Dialogs

import java.awt.BorderLayout
import java.awt.FlowLayout
import javax.swing.JScrollPane
import java.awt.Dialog.ModalityType
import javax.swing.KeyStroke
import java.awt.event.KeyEvent
import javax.swing.InputMap
import javax.swing.JComponent
import javax.swing.AbstractAction
import java.awt.event.ActionEvent
import groovy.swing.SwingBuilder
import javax.swing.JFrame
import javax.swing.JButton
import javax.swing.JDialog
import DeploymentLab.channellogger.*
import javax.swing.table.TableModel
import javax.swing.JTable
import javax.swing.table.TableRowSorter
import javax.swing.table.TableCellEditor
import DeploymentLab.Localizer
import javax.swing.DefaultCellEditor
import javax.swing.table.TableCellRenderer
import java.awt.Component
import DeploymentLab.PropertyEditor.NumberCellEditor
import javax.swing.JTextField
import DeploymentLab.PropertyEditor.NumberCellRenderer
import javax.swing.ListSelectionModel
import javax.swing.SwingUtilities;

/**
 * 
 * @author Gabriel Helman
 * @since Mars
 * Date: 6/28/11
 * Time: 1:52 PM
 */
 class ConfigureDefaultsDialog {


	private static final Logger log = Logger.getLogger(ConfigureDefaultsDialog.class.getName())
	/** Internal SwingBuilder used to construct the dialogDelegate. */
	protected SwingBuilder builder;
	/** All the actual dialog work is delegated to the JDialog class. */
	protected JDialog dialogDelegate;
	/** The parent frame of this dialog box.*/
	protected JFrame parentFrame;

	JButton okButton
	JButton closeButton

	private TableModel tm

	private ConfigureDefaultsController controller;


	public ConfigureDefaultsDialog(JFrame parent, ConfigureDefaultsController delegate, TableModel model) {
		this.builder = new SwingBuilder()
		parentFrame = parent;
		controller = delegate;
		tm = model;
	}


	protected void initDialog(){

		dialogDelegate = builder.dialog(owner:parentFrame, resizable: true, title: 'Configure Defaults', layout: new BorderLayout(), modalityType: ModalityType.APPLICATION_MODAL) {
			scrollPane(constraints: BorderLayout.CENTER, horizontalScrollBarPolicy: JScrollPane.HORIZONTAL_SCROLLBAR_NEVER){
				widget( new ConfigureDefaultsTable( tm ) )
			}
			panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.SOUTH) {
				okButton = builder.button( text: 'OK', defaultButton: true, actionPerformed: { this.apply() } )
				closeButton = builder.button( text: 'Close', actionPerformed: { this.close() } )
			}
		}

		setEscapeKeyMap();
        this.dialogDelegate.pack();
        this.dialogDelegate.setLocationRelativeTo(this.parentFrame);

	}



	 public void apply(){
		 controller.applyChanges(true)
		 this.close()
	 }


 	public void show() {
		if (this.dialogDelegate == null) {
			initDialog()
		}
		this.dialogDelegate.requestFocusInWindow();
		this.dialogDelegate.show();
    }


    private void close() {
        this.dialogDelegate.setVisible(false);
		//this.dialogDelegate.dispose();
		//this.dialogDelegate = null;
    }


	public void dispose(){
		this.dialogDelegate.dispose();
		this.dialogDelegate = null;
	}


	private void setEscapeKeyMap(){
		String CANCEL_ACTION_KEY = "CANCEL_ACTION_KEY";
		int noModifiers = 0;
		KeyStroke escapeKey = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, noModifiers, false);
		InputMap inputMap = this.dialogDelegate.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(escapeKey, CANCEL_ACTION_KEY);
		AbstractAction cancelAction = new AbstractAction(){
			public void actionPerformed(ActionEvent e){
				close();
			}
		};
		this.dialogDelegate.getRootPane().getActionMap().put(CANCEL_ACTION_KEY, cancelAction);
	}
}

/**
 * Custom table used to display the configurable defaults in the Configure Defaults dialog.
 */
class ConfigureDefaultsTable extends JTable {

    private static final VAL_COL = 3

    ConfigureDefaultsTable( TableModel model ) {
        super( model )
        setRowSorter( new TableRowSorter( model ) )
        putClientProperty( 'JTable.autoStartsEdit', Boolean.TRUE )
        putClientProperty( 'terminateEditOnFocusLost', Boolean.TRUE )
        this.setSelectionMode( ListSelectionModel.SINGLE_SELECTION )
    }

    TableCellRenderer getCellRenderer(int row, int col) {
        TableCellRenderer renderer = null

        if( (col == VAL_COL) && Localizer.isLocalizable( model.getValueAt( row, col ).class ) ) {
            // Custom renderer for numbers so they are properly localized.
            renderer = new NumberCellRenderer()
        } else {
            renderer = super.getCellRenderer(row, col)
        }

        return renderer
    }

    TableCellEditor getCellEditor(int row, int col) {
        DefaultCellEditor editor = (DefaultCellEditor) super.getCellEditor( row, col )

        if( (col == VAL_COL) && Localizer.isLocalizable( model.getValueAt( row, col ).class ) ) {
            // Custom editor for numbers. Use the default component so they behave the same.
            Component comp = editor.getTableCellEditorComponent( this, model.getValueAt(row,col), true, row, col )
            editor =  new NumberCellEditor( (JTextField) comp )
        } else {
            // Custom behavior for the default.
            editor.setClickCountToStart(1)
        }

        return editor
    }

    // Override to provide Select All editing functionality
    boolean editCellAt( int row, int column, EventObject e ) {
        boolean result = super.editCellAt( row, column, e )

        //  If this event is a mouse click, our single-click editing might allow a second
        // click to clear the selection unless we use the invokeLater() to do the select.
        SwingUtilities.invokeLater( new Runnable() {
            void run() {
                ((JTextField) getEditorComponent()).selectAll()
            }
        })

        return result
    }
}
