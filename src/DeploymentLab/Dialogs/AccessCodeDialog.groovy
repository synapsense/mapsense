package DeploymentLab.Dialogs

import DeploymentLab.channellogger.*
import DeploymentLab.UserAccess

import java.awt.BorderLayout
import java.awt.Color
import java.awt.Component
import java.awt.Dimension
import java.awt.Dialog.ModalityType
import java.awt.event.ActionListener
import java.awt.event.KeyEvent
import java.awt.GridBagConstraints
import java.awt.GridBagLayout
import java.awt.GridLayout
import java.awt.Insets

import javax.swing.JComponent
import javax.swing.JButton
import javax.swing.JComboBox
import javax.swing.JDialog
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.JPasswordField
import javax.swing.KeyStroke

import groovy.swing.SwingBuilder
import java.awt.FlowLayout
import javax.swing.SpringLayout
import DeploymentLab.SpringUtilities

/**
* Dialog to change the user access codes.
*/
class AccessCodeDialog {
	private static final Logger log = Logger.getLogger(AccessCodeDialog.class.getName())
	protected SwingBuilder builder
	private JDialog dialogDelegate
	private JButton okButton
	private JComboBox userLevelComboBox
	private JPasswordField currentAccessCodeTxt
	private JPasswordField newAccessCodeTxt
	private UserAccess userAccess
	private JFrame parent
	
	public AccessCodeDialog(UserAccess _userAccess, JFrame _parent) {
		builder = new SwingBuilder()
		userAccess = _userAccess
		parent = _parent
		dialogDelegate = this.getDialog()
	}

	private JDialog getDialog() {
		GridBagConstraints gbc = new GridBagConstraints()

		def innerPanel

		dialogDelegate = builder.dialog(owner:parent, resizable: false, title: 'Change Access Code', layout: new BorderLayout(), modalityType: ModalityType.APPLICATION_MODAL) {
			def okAction = {changeAccessCode()}
			def cancelAction = {dialogDelegate.setVisible(false)}

			def dlgPanel = panel(layout: new BorderLayout() ) {
				innerPanel = panel(layout: new SpringLayout(), constraints: BorderLayout.NORTH ) {
					label(text: "Access Level:" )
					userLevelComboBox = comboBox(items:["Advanced","Expert"] )
					label(text: "Current Access Code: " )
					currentAccessCodeTxt = passwordField(text: "", columns: 8 )
					label(text: "New Access Code: " )
					newAccessCodeTxt = passwordField(text: "", columns: 8 )
				}
				panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.SOUTH ) {
					okButton = button(text: 'OK', defaultButton: true, actionPerformed: okAction)
					button(text: 'Cancel', actionPerformed: cancelAction)

				}

			}
			dlgPanel.registerKeyboardAction(okAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			dlgPanel.registerKeyboardAction(cancelAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		}

		SpringUtilities.makeCompactGrid(innerPanel,
                                3, 2,   //rows, cols
                                6, 6,   //initX, initY
                                5, 5);  //xPad, yPad
		dialogDelegate.pack()
		return dialogDelegate
	}
	
	public void setLocationRelativeTo() {
		dialogDelegate.setLocationRelativeTo(parent)
	}

	public void show() {
		currentAccessCodeTxt.text = ""
		newAccessCodeTxt.text = ""
		this.requestFocusInWindow()
		dialogDelegate.show()
	}

	public void requestFocusInWindow() {
		currentAccessCodeTxt.requestFocusInWindow()
	}
	
	void changeAccessCode() {
		int selectedUserLevel = userLevelComboBox.selectedIndex + 2
		
		if(userAccess.isMatchedAccessCode(selectedUserLevel, currentAccessCodeTxt.text)){
			userAccess.setAccessCode(selectedUserLevel, newAccessCodeTxt.text)
			dialogDelegate.hide()	
			log.info("Changed Access Code successfully.",'message')
		}else{
			log.error("Current Access Code is incorrect.",'message')
			currentAccessCodeTxt.text = ""
			newAccessCodeTxt.text = ""
			currentAccessCodeTxt.requestFocusInWindow()
		}
	}
}