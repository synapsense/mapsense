package DeploymentLab.Dialogs

import DeploymentLab.Export.SynapEnvContext
import DeploymentLab.Model.ObjectType
import DeploymentLab.channellogger.Logger
import com.synapsense.dto.BinaryData
import com.synapsense.dto.TO
import com.synapsense.exception.EnvException
import com.synapsense.service.Environment
import groovy.swing.SwingBuilder

import javax.swing.JButton
import javax.swing.JComponent
import javax.swing.JDialog
import javax.swing.JFrame
import javax.swing.JList
import javax.swing.KeyStroke
import javax.swing.ListSelectionModel
import java.awt.BorderLayout
import java.awt.Dialog
import java.awt.Dimension
import java.awt.FlowLayout
import java.awt.event.ActionListener
import java.awt.event.KeyEvent

/**
 * @author Vadim Gusev
 * @since Europa
 * Date: 04.04.13
 * Time: 11:28
 */
class UnStashDialog {
	private static final Logger log = Logger.getLogger(UnStashDialog.class.getName())
	protected SwingBuilder builder
	private JDialog dialogDelegate
	private JButton openButton
	private JFrame parent
	private JList list

	private SynapEnvContext ctx
	private Environment env
	def dlw

	public UnStashDialog(JFrame _parent, def dlw) {
		builder = new SwingBuilder()
		parent = _parent
		this.dlw = dlw
	}

	private JDialog getDialog(Map<String, BinaryData> projects) {
		dialogDelegate = builder.dialog(owner:parent, resizable: false, title: "Open Project From Server", layout: new BorderLayout(), modalityType: Dialog.ModalityType.APPLICATION_MODAL) {
			def openAction = { dialogDelegate.setVisible(false); dialogDelegate.dispose(); dlw.saveAndOpenFile(list.getSelectedValue(), projects.get(list.getSelectedValue()));}
			def cancelAction = {dialogDelegate.setVisible(false); dialogDelegate.dispose();}
			def dlgPanel = panel(layout: new FlowLayout(FlowLayout.CENTER), constraints: BorderLayout.NORTH) {
				scrollPane(preferredSize: new Dimension(250, 250)){
					list = list(projects.keySet().toArray())
					list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION)
					list.setSelectedIndex(0)
				}
			}
			panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.SOUTH) {
				openButton = button(text: 'Save & Open', defaultButton: true, actionPerformed: openAction)
				button(text: 'Cancel', actionPerformed: cancelAction)
			}

			dlgPanel.registerKeyboardAction(openAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			dlgPanel.registerKeyboardAction(cancelAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
		}
		dialogDelegate.pack()
		return dialogDelegate
	}

	private void requestFocusInWindow() {
		openButton.requestFocusInWindow()
	}

	private void setLocationRelativeTo() {
		dialogDelegate.setLocationRelativeTo(parent)
	}

	private void show(){
		dialogDelegate.show()
	}

	public void fetch(Properties connectProperties) {
		String serverUrl = SynapEnvContext.populateProperties(connectProperties, connectProperties.get('host').toString())
		log.trace("Attempting to connect to $serverUrl")
		ctx = new SynapEnvContext(connectProperties)
		Map<String, BinaryData> projectMap = new HashMap<String, BinaryData>()
		if (ctx.testConnect()) {
			env = ctx.getEnv()
			def projects = env.getObjectsByType( ObjectType.DRAWING )
			for (TO<?> project : projects) {
				def name = env.getPropertyValue(project, "name", java.lang.String)
				log.trace("Fetching DLZ for project '$name'", "progress")
				def blob
				try{
					blob = env.getPropertyValue(project, "dlzFile", BinaryData)
				}catch (EnvException e){
					// in older versions type has no 'dlzFile' property
					log.warn("Project has no 'dlzFile' property")
				}
				if (blob != null) {
					projectMap.put(name, blob);
					log.trace("Stashed project found for '$name'")
				} else {
					log.trace("No stashed project for '$name'")
				}
			}
		} else {
			log.error("Unable to connect to $serverUrl!", "message")
			return;
		}

		if (projectMap.isEmpty()) {
			log.info("No project files were found on the server.", "message")
			return
		}

		dialogDelegate = this.getDialog(new TreeMap<String, BinaryData>(projectMap))
		setLocationRelativeTo()
		requestFocusInWindow()
		show()
	}
}
