package DeploymentLab.Dialogs.Association;

import DeploymentLab.Dialogs.Association.AssociationBundle;
import com.google.gag.annotation.remark.ThisWouldBeOneLineIn;

import javax.swing.*;

/**
 * Yay!  Thanks for not being there, tuples!
 * And thanks for being so inefficient, hashmaps!
 *
 * @author Gabriel Helman
 * @since Mars
 *        Date: 4/20/11
 *        Time: 11:30 AM
 */
@ThisWouldBeOneLineIn(language = "Python", toWit = "t = bundle, toggleButton, comboSelection" )
public class AssociationDialogRegistry {
	/**
	 * The bundle that backs this line of the dialog.  At start, this has everything except a consumer or conducer on the "right side" component. *
	 */
	private AssociationBundle bundle;
	/**
	 * The checkbox that goes with this potential association.  (In the future we may use checkboxes and radio buttons interchangeably.) *
	 */
	private JToggleButton button;
	/**
	 * The combobox that stores the potential consumers the user can choose for this association. *
	 */
	private JComboBox selection;


	public AssociationDialogRegistry(AssociationBundle bundle, JToggleButton button, JComboBox selection) {
		this.bundle = bundle;
		this.button = button;
		this.selection = selection;
	}

	public AssociationBundle getBundle() {
		return bundle;
	}

	public void setBundle(AssociationBundle bundle) {
		this.bundle = bundle;
	}

	public JToggleButton getButton() {
		return button;
	}

	public void setButton(JToggleButton button) {
		this.button = button;
	}

	public JComboBox getSelection() {
		return selection;
	}

	public void setSelection(JComboBox selection) {
		this.selection = selection;
	}

	@Override
	public String toString() {
		return "AssociationDialogRegistry{" +
				"bundle=" + bundle +
				//", button=" + button +
				", button=" + button.isSelected() +
				//", selection=" + selection +
				", selection=" + selection.getSelectedItem() +
				'}';
	}
}
