package DeploymentLab.Dialogs.Association

import DeploymentLab.CentralCatalogue
import DeploymentLab.Image.ComponentIconFactory
import DeploymentLab.Model.Conducer
import DeploymentLab.Model.Consumer
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.Producer
import DeploymentLab.channellogger.Logger
import com.google.common.annotations.VisibleForTesting
import groovy.swing.SwingBuilder
import java.awt.Dialog.ModalityType
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.awt.event.KeyEvent
import java.util.List
import net.miginfocom.swing.MigLayout
import java.awt.*
import javax.swing.*

import DeploymentLab.Dialogs.ScrollablePanel

/**
 * Rewritten Association Dialog
 * @author Gabriel Helman
 * @since Mars
 * Date: 4/5/11
 * Time: 12:03 PM
 */
class AssociationDialog implements ActionListener {
	private static final Logger log = Logger.getLogger(AssociationDialog.class.getName())
	/** Internal SwingBuilder used to construct the dialogDelegate. */
	protected SwingBuilder builder;
	/** All the actual dialog work is delegated to the JDialog class. */
	protected JDialog dialogDelegate;
	/** The parent frame of this dialog box.*/
	protected JFrame parentFrame;

	JButton okButton
	JButton closeButton
	JButton applyButton
	JPanel mainPanel
	JScrollPane mainScrollPane
	JPanel conducerPanel
	JPanel outerConducerPanel
	//the upper panels handle both the producers and consumers
	JPanel outerUpperPanel
	JPanel targetPanel

	private AssociationController controller;
	protected List<AssociationDialogRegistry> registry;
	private boolean firstPass
	private List<AssociationBundle> currentSelections = [];

	//these two lists are here as testing hooks
	@VisibleForTesting
	protected List<JButton> plusButtons = []
	@VisibleForTesting
	protected List<JComboBox> selectionCombos = []

	private static final String EXTRA_SPACE = "XX"
	static ImageIcon forwardIcon
	//static ImageIcon backwardIcon
	static ImageIcon bothIcon

	static {
		//forwardIcon = ComponentIconFactory.getImageIcon("/resources/forward.png");
		forwardIcon = ComponentIconFactory.getImageIcon("/resources/play_16_gray.png");
		//backwardIcon = ComponentIconFactory.getImageIcon("/resources/back.png");
		bothIcon = ComponentIconFactory.getImageIcon("/resources/both.png");
	}


	public AssociationDialog(JFrame parent, AssociationController delegate) {
		this.builder = new SwingBuilder()
		parentFrame = parent;
		controller = delegate;
		registry = new ArrayList<AssociationDialogRegistry>();
	}


	protected void initDialog() {
		firstPass = true

		dialogDelegate = builder.dialog(owner: parentFrame, resizable: true, title: 'Associations', layout: new BorderLayout(), modalityType: ModalityType.APPLICATION_MODAL, iconImage: ComponentIconFactory.defaultIcon.getImage()) {
			mainScrollPane = scrollPane(constraints: BorderLayout.CENTER, horizontalScrollBarPolicy: JScrollPane.HORIZONTAL_SCROLLBAR_NEVER) {
				//mainPanel = panel(constraints: BorderLayout.CENTER, layout: new MigLayout()){
				mainPanel = panel(layout: new MigLayout(), new ScrollablePanel()) {
					//dynamism!
					//first: yin's producers, yang's consumers
					//second: yin's consumers, yang's producers
					//third: yin's conducers, yang's conducers
				}
			}
			panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.SOUTH) {
				okButton = builder.button(text: 'OK', defaultButton: true, actionPerformed: { this.apply(true) })
				closeButton = builder.button(text: 'Close', actionPerformed: { this.close() })
				//applyButton = builder.button( text: 'Apply', defaultButton: false, actionPerformed: { this.apply(false) } )
			}
		}

		//set max size based on screen size
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		def maxSize = new Dimension()


		if (controller.hasYinToYang() || controller.hasYangToYin()) {
			//println "starting to init layout"
			outerUpperPanel = new JPanel()
			outerUpperPanel.setLayout(new MigLayout())
			mainPanel.add(outerUpperPanel, "wrap")
			targetPanel = new JPanel()
			targetPanel.setLayout(new MigLayout())
			outerUpperPanel.add(targetPanel, "wrap")

			if (controller.hasYinToYang()) {
				//println "gonna make p->"
				addRows(controller.getYin(), controller.getYang())
			}

			if (controller.hasYangToYin()) {
				//println "gonna make ->c"
				addRows(controller.getYang(), controller.getYin())
			}
		}


		if (controller.hasConducers()) {
			outerConducerPanel = new JPanel()
			outerConducerPanel.setLayout(new MigLayout())
			mainPanel.add(outerConducerPanel, "wrap")
			outerConducerPanel.add(generateConducerPanel(), "wrap")
		}

		//set the size of the main panel
		//use the default "preferred size" unless the height is more than 1/2 the screen height; then clamp it at that
		def totalHeight = mainPanel.getPreferredSize().getHeight()
		def finalHeight = 0
		if (dim.getHeight() / 2 < totalHeight) {
			finalHeight = dim.getHeight() / 2
		} else {
			finalHeight = totalHeight
		}
		def finalWidth = mainPanel.getPreferredSize().getWidth()
		maxSize.setSize(finalWidth, finalHeight)
		mainPanel.setPreferredScrollableViewportSize(maxSize)

		firstPass = false

		setEscapeKeyMap();
		this.dialogDelegate.pack();
		this.dialogDelegate.setLocationRelativeTo(this.parentFrame);
	}


	private JPanel generateConducerPanel() {
		conducerPanel = new JPanel()
		List<AssociationBundle> conducers = controller.calculateConducers(true)

		if (conducers.size() > 0) {
			conducerPanel.setLayout(new MigLayout())

			conducerPanel.add(new JLabel(ComponentIconFactory.getComponentIcon(controller.getYin(), ComponentIconFactory.MEDIUM)))
			conducerPanel.add(new JLabel("${controller.getYin().getName()}"))
			conducerPanel.add(new JLabel(bothIcon), "gapleft unrel, gapright unrel")

			conducerPanel.add(new JLabel(ComponentIconFactory.getComponentIcon(controller.getYang(), ComponentIconFactory.MEDIUM)))
			conducerPanel.add(new JLabel("${controller.getYang().getName()}"), "span 3, wrap")

			for (int i = 0; i < conducers.size(); i++) {
				AssociationBundle bundle = conducers[i]

				Conducer alpha = bundle.alpha

				//build the alpha label and surroundings

				String assocType = alpha.getDatatype()
				def lbl = new JLabel("   ")
				lbl.setOpaque(true)
				lbl.setBackground(new Color(Integer.parseInt(CentralCatalogue.getInstance().getAssociationProperties().getProperty("associations.color." + assocType), 16)))
				def descr = CentralCatalogue.getInstance().getAssociationProperties().getProperty("associations.descr.viewoption." + assocType)
				lbl.setToolTipText(descr)
				//conducerPanel.add( lbl )

				conducerPanel.add(new JLabel(""))

				conducerPanel.add(new JLabel(alpha.toString()))
				//conducerPanel.add( new JLabel(bothIcon) )

				//fill in the omega side
				JComboBox combo = new JComboBox(bundle.consumerComponent.getConducersThatMatch(alpha).toArray())
				//find width for box:
				String proto = ""
				bundle.consumerComponent.getConducersThatMatch(alpha).each {
					if (it.getName().length() > proto.length()) {
						proto = it.getName()
					}
				}
				proto += EXTRA_SPACE
				combo.setPrototypeDisplayValue(proto)

				JCheckBox chk = new JCheckBox()
				//chk.addActionListener(this)
				//chk.setActionCommand("checkbox_conducer")
				//chk.setActionCommand("displaycheckbox")
				chk.setEnabled(false)

				if (controller.associationExists(bundle)) {
					//println "assoc $bundle exists!"
					chk.setSelected(true)
				}


				JCheckBox makeMe = new JCheckBox()
				makeMe.addActionListener(this)
				makeMe.setActionCommand("checkbox_conducer")


				if (firstPass && bundle.isDefault()) {
					makeMe.setSelected(true)
				}

				if (bundle.omega != null) {
					makeMe.setSelected(true)
					combo.setSelectedItem(bundle.omega)
				}

				conducerPanel.add(chk, "gapleft unrel, gapright unrel")

				conducerPanel.add(new JLabel(""))

				if ((i + 1 == conducers.size()) || (conducers[i + 1].alpha.getId() != bundle.alpha.getId())) {
					//add the button
					conducerPanel.add(combo)
					conducerPanel.add(makeMe)
					//build the plus button
					def btn = new JButton("+")
					btn.setActionCommand("${alpha.getId()}")
					btn.addActionListener(controller)
					btn.setToolTipText("Add a line for ${alpha.getName()}")
					conducerPanel.add(btn, "wrap")
					plusButtons.add(btn)
				} else {
					conducerPanel.add(combo, "span 1")
					conducerPanel.add(makeMe, "span 2, wrap")
				}
				selectionCombos.add(combo)
				registry.add(new AssociationDialogRegistry(bundle, makeMe, combo))
			}
		}
		return conducerPanel
	}


	private void addRows(DLComponent producerComponent, DLComponent consumerComponent) {
		//println "addRows($producerComponent,$consumerComponent"
		List<AssociationBundle> bundles = controller.calculateAssociations(producerComponent, consumerComponent, true)
		//println "bundles returned to add rows: $bundles"
		if (bundles.size() == 0) { return; }


		targetPanel.add(new JLabel(ComponentIconFactory.getComponentIcon(producerComponent, ComponentIconFactory.MEDIUM)), "span 1")
		targetPanel.add(new JLabel(producerComponent.getName()), "span 1")
		//targetPanel.add( new JLabel(producerComponent.getName(), ComponentIconFactory.getComponentIcon(producerComponent, ComponentIconFactory.MEDIUM), SwingConstants.LEADING), "span 1" )
		targetPanel.add(new JLabel(forwardIcon), "gapleft unrel, gapright unrel")
		//targetPanel.add( new JLabel(consumerComponent.getName(),ComponentIconFactory.getComponentIcon(consumerComponent, ComponentIconFactory.MEDIUM), SwingConstants.LEADING), "span 2, wrap" )
		targetPanel.add(new JLabel(ComponentIconFactory.getComponentIcon(consumerComponent, ComponentIconFactory.MEDIUM)))
		targetPanel.add(new JLabel(consumerComponent.getName()), "span 3, wrap")

		for (int i = 0; i < bundles.size(); i++) {
			AssociationBundle bundle = bundles[i]
			//println "producer side $i: now with bundle $bundle"
			Producer prod = bundle.getProducer()
			//build the producer label and surroundings
			String assocType = prod.getDatatype()
			def lbl = new JLabel("   ")
			lbl.setOpaque(true)
			lbl.setBackground(new Color(Integer.parseInt(CentralCatalogue.getInstance().getAssociationProperties().getProperty("associations.color." + assocType), 16)))
			def descr = CentralCatalogue.getInstance().getAssociationProperties().getProperty("associations.descr.viewoption." + assocType)
			lbl.setToolTipText(descr)
			//targetPanel.add( lbl )
			targetPanel.add(new JLabel(""))
			targetPanel.add(new JLabel(prod.toString()))
			//targetPanel.add( new JLabel(forwardIcon) )

			//fill in the consumer side
			JComboBox combo = new JComboBox(bundle.getPossibleConsumers().toArray())
			//find width for box:
			String proto = controller.getLongestConsumerName(assocType)
			proto += EXTRA_SPACE
			combo.setPrototypeDisplayValue(proto)

/*
				//bonus points!  Make sure this isn't already selected!
				boolean dupe = false
				for( def r : registry ){
					if ( r.getBundle().equalsExceptForMake(bundle) && r.getButton().selected == true ){
						//println "dupe bundle: $bundle"
						dupe = true
					}
				}

				if( firstPass && bundle.isDefault() ){
					if ( ! dupe ){
						chk.setSelected(true)
					}
				}
*/

			if (bundle.consumerName != null) {
//					if ( ! dupe ){
//						chk.setSelected(true)
//					}
				combo.setSelectedItem(bundle.getConsumer())
			}

			JCheckBox makeMe = new JCheckBox()
			makeMe.addActionListener(this)
			makeMe.setActionCommand("checkbox")

			//pre-check the makeme box only for the top and only for the top row
			//AND ONLY IF THERE ARE NO CURRENT ASSOCIATIONS
			makeMe.selected = false
			if (bundle.make) {
				makeMe.selected = true
			} else if (controller.currentConnections() == 0 && firstPass) {
				if (bundle.producer.defaultProducer) {
					makeMe.selected = true
				} else if (producerComponent == controller.getYin() && i == 0 && !producerComponent.hasDefaultProducer())
					makeMe.selected = true
			}

			//use image to indicate the connection, not a checkbox
			JLabel connectionLabel
			if (controller.associationExists(bundle)) {
				connectionLabel = new JLabel(forwardIcon)
			} else {
				connectionLabel = new JLabel("")
			}
			targetPanel.add(connectionLabel, "gapleft unrel, gapright unrel")
			targetPanel.add(new JLabel(""))
			if (((i + 1 == bundles.size()) || (bundles[i + 1].producerName != bundle.producerName)) && controller.allowMoreThanOneConnection(bundle)) {
				//add the button
				targetPanel.add(combo)
				targetPanel.add(makeMe)
				//build the plus button
				def btn = new JButton("+")
				//btn.setActionCommand("${bundle.producerComponent.getType()}#${bundle.producerName}")
				btn.setActionCommand(bundle.getActionCommandName())
				//btn.setActionCommand( "${bundle.producerName}" )
				btn.addActionListener(controller)
				btn.setToolTipText("Add a line for ${bundle.getProducer().getName()}")
				targetPanel.add(btn, "wrap")
				plusButtons.add(btn)
			} else {
				targetPanel.add(combo, "span 1")
				targetPanel.add(makeMe, "span 2, wrap")
			}
			selectionCombos.add(combo)
			registry.add(new AssociationDialogRegistry(bundle, makeMe, combo))
		}

		if (producerComponent == controller.getYin() && controller.hasYangToYin()) {
			JSeparator sep = new JSeparator(JSeparator.HORIZONTAL)
			targetPanel.add(sep, "growx, gaptop unrel, gapbottom unrel, span 7, wrap")
		}

	}


	public void regenerate() {
		//println "REGENERATE"
		//clear out the registry because we're about to rebuild it
		registry = new ArrayList<AssociationDialogRegistry>();
		plusButtons = []
		selectionCombos = []
		if (controller.hasYinToYang() || controller.hasYangToYin()) {
			outerUpperPanel.remove(targetPanel)
			targetPanel = new JPanel()
			targetPanel.setLayout(new MigLayout())
			outerUpperPanel.add(targetPanel, "wrap")
			if (controller.hasYinToYang()) {
				//println "gonna make p->"
				//println "${controller.getYin()} -- > ${controller.getYang()}"
				addRows(controller.getYin(), controller.getYang())
			}
			if (controller.hasYangToYin()) {
				//println "gonna make ->c"
				//println "${controller.getYang()} -- > ${controller.getYin()}"
				addRows(controller.getYang(), controller.getYin())
			}
			targetPanel.revalidate()
			targetPanel.repaint()
		}
		if (controller.hasConducers()) {
			outerConducerPanel.remove(conducerPanel)
			conducerPanel = this.generateConducerPanel()
			outerConducerPanel.add(conducerPanel)
			conducerPanel.revalidate()
			conducerPanel.repaint()
		}
		//recalculate height
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		def maxSize = new Dimension()
		def totalHeight = mainPanel.getPreferredSize().getHeight()
		def finalHeight = 0
		if (dim.getHeight() / 2 < totalHeight) {
			finalHeight = dim.getHeight() / 2
		} else {
			finalHeight = totalHeight
		}
		def finalWidth = mainPanel.getPreferredSize().getWidth()
		maxSize.setSize(finalWidth, finalHeight)
		mainPanel.setPreferredScrollableViewportSize(maxSize)
		dialogDelegate.pack()
	}


	public List<AssociationBundle> getBundles() {
		List<AssociationBundle> results = []
		//println "getBundles()"
		//scrape the registry for selected buttons
		for (AssociationDialogRegistry r: registry) {
			//println r
/*
			if ( r.button.isSelected() ){
				if ( ! r.bundle.isConducer ){
					r.bundle.consumerName = ((Consumer)r.selection.getSelectedItem()).getId()
				} else {
					r.bundle.omega = ((Conducer)r.selection.getSelectedItem())
				}
				r.bundle.make = true
				results += r.bundle
				//println r.bundle
/*
			} else {
				//box is unselected
				if ( r.bundle.consumerName != null ) {
					//it's a live assoc, detach it!
					results += r.bundle
					r.bundle.make = false
				}
*/
//			}

			if (!r.bundle.isConducer()) {
				r.bundle.consumerName = ((Consumer) r.selection.getSelectedItem()).getId()
			} else {
				r.bundle.omega = ((Conducer) r.selection.getSelectedItem())
			}
			if (r.button.isSelected()) {
				r.bundle.make = true
				r.bundle.isValid = true
			} else {
				r.bundle.make = false
				r.bundle.isValid = false
			}
			results += r.bundle
		}
		return results
	}

	public List<AssociationDialogRegistry> getAssociationDialogRegistry() {
		return registry
	}

	public void show() {
		if (this.dialogDelegate == null) {
			initDialog()
		}
		this.dialogDelegate.requestFocusInWindow();
		this.dialogDelegate.show();
		//println "main panel: " +  mainPanel.getSize()
		//println "dialog as a whole:" + dialogDelegate.getSize()
	}


	private void close() {
		this.dialogDelegate.setVisible(false);
		//this.dialogDelegate.dispose();
		//this.dialogDelegate = null;
	}

	public void dispose() {
		this.dialogDelegate.dispose();
		this.dialogDelegate = null;
	}

	private void setEscapeKeyMap() {
		String CANCEL_ACTION_KEY = "CANCEL_ACTION_KEY";
		int noModifiers = 0;
		KeyStroke escapeKey = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, noModifiers, false);
		InputMap inputMap = this.dialogDelegate.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(escapeKey, CANCEL_ACTION_KEY);
		AbstractAction cancelAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		};
		this.dialogDelegate.getRootPane().getActionMap().put(CANCEL_ACTION_KEY, cancelAction);
	}

	private void apply(boolean closeDialogAfter) {
		//println "dialog about to applyChanges()"
		def howDidWeDo = controller.applyChanges(this.getBundles())
		if (!howDidWeDo) {
			log.warn(CentralCatalogue.getUIS("NodeAssociator.twoWay"), "message")
			return
		}

		if (closeDialogAfter) {
			this.close()
		}
	}


	public boolean inCurrentSelection(AssociationBundle bundle, String consumerName) {
		AssociationBundle bundleToSearchFor = new AssociationBundle(bundle)
		bundleToSearchFor.consumerName = consumerName
		return (currentSelections.contains(bundleToSearchFor))
	}

	@Override
	void actionPerformed(ActionEvent e) {
		//should be sent by checkboxes
		//if (! e.getActionCommand().equalsIgnoreCase("checkbox")){ return; }

		//store the current selections so we can re-click them if we have to
		currentSelections = this.getBundles();

		AssociationDialogRegistry reg
		//go find our registry entry
		for (AssociationDialogRegistry r: registry) {
			if (r.button == e.getSource()) {
				reg = r;
				break;
			}
		}

		if (e.getActionCommand().equalsIgnoreCase("checkbox")) {
			//println "checkbox action for $reg"
			Consumer currentConsumer = ((Consumer) reg.getSelection().getSelectedItem())
			//println "consumer in question is $currentConsumer"
			if (!currentConsumer.isCollection()) {
				//do something clever:
				//like... clear any other checkboxes with that consumer
				for (AssociationDialogRegistry r: registry) {
					if (r.getSelection().getSelectedItem() == currentConsumer && r.button != e.getSource()) {
						r.getButton().setSelected(false)
					}
				}
			}

			//the other case: if this line is an exact dupe of producer -> consumer, wipe out the dupes
			Producer currentProducer = reg.getBundle().getProducer()
			for (AssociationDialogRegistry r: registry) {
				if (r.getSelection().getSelectedItem() == currentConsumer && r.button != e.getSource() && r.getBundle().getProducer() == currentProducer) {
					r.getButton().setSelected(false)
				}
			}


		} else if (e.getActionCommand().equalsIgnoreCase("checkbox_conducer")) {
			//println "checkbox action for $reg"
			Conducer currentConducer = ((Conducer) reg.getSelection().getSelectedItem())
			//println "consumer in question is $currentConsumer"
			//do something clever:
			//like... clear any other checkboxes with that consumer
			for (AssociationDialogRegistry r: registry) {
				if (r.getSelection().getSelectedItem() == currentConducer && r.button != e.getSource()) {
					r.getButton().setSelected(false)
				}
			}

			//the other case: if this line is an exact dupe of alpha -> omega, wipe out the dupes
            Conducer currentAlpha = reg.getBundle().alpha
            for( AssociationDialogRegistry r : registry ){
                if ( r.getSelection().getSelectedItem() == currentConducer && r.button != e.getSource() && r.getBundle().alpha == currentAlpha ){
                    r.getButton().setSelected(false)
                }
            }

            // Clear any mirror images of this line.
            Conducer currentOmega = reg.getBundle().omega
            for( AssociationDialogRegistry r : registry ){
                if( ( r.getSelection().getSelectedItem() == currentAlpha ) && ( r.button != e.getSource() ) &&
                    ( r.getBundle().alpha == currentOmega ) && ( r.getBundle().omega == currentAlpha ) ) {
                    r.getButton().setSelected(false)
                }
            }
		}

	}

}
