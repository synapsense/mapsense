package DeploymentLab.Dialogs.Association

import DeploymentLab.CentralCatalogue
import DeploymentLab.Image.ComponentIconFactory
import DeploymentLab.MapHelper
import DeploymentLab.Model.Conducer
import DeploymentLab.Model.Consumer
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.Producer
import DeploymentLab.channellogger.Logger
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import javax.swing.ImageIcon
import javax.swing.JFrame
import java.util.List

/**
 * Controls and manages the component association process
 * @author Gabriel Helman
 * @since Mars
 * Date: 3/14/11
 * Time: 4:23 PM
 */
class AssociationController implements ActionListener {
	private static final Logger log = Logger.getLogger(AssociationController.class.getName())
	private DLComponent yin;
	private DLComponent yang;

	protected AssociationDialog dialog

	private boolean currentStatus = false;
	List<AssociationBundle> fromDialog = []

	AssociationController(DLComponent l, DLComponent r) {
		yin = l;
		yang = r;
	}

	public DLComponent getYin() {
		return yin;
	}

	public DLComponent getYang() {
		return yang;
	}

	/**
	 * Convenience method to get the icon from the icon factory for the yin component.
	 * @return an ImageIcon with this component's icon.
	 */
	public ImageIcon getYinIcon() {
		ComponentIconFactory.getComponentIcon(yin)
	}

	/**
	 * Convenience method to get the icon from the icon factory for the yang component.
	 * @return an ImageIcon with this component's icon.
	 */
	public ImageIcon getYangIcon() {
		ComponentIconFactory.getComponentIcon(yang)
	}

	/**
	 * Check if any association is possible
	 * @return
	 */
	public boolean canAssociate() {
		if (
				(yin.listProducers().size() == 0 && yang.listConsumers() == 0) &&
						(yin.listConsumers().size() == 0 && yang.listProducers() == 0) &&
						(yin.listConducers().size() == 0 && yang.listConducers().size() == 0)
		) {
			return false
		} else {
			boolean sameType = false
			//there are connections, so let's dig in a little
			for (Producer p: yin.getAllProducers()) {
				for (Consumer c: yang.getAllConsumers()) {
					if (c.getDatatype().equalsIgnoreCase(p.getDatatype())) {
						sameType = true
						break;
					}
				}
			}
			if (!sameType) {
				for (Consumer c: yin.getAllConsumers()) {
					for (Producer p: yang.getAllProducers()) {
						if (c.getDatatype().equalsIgnoreCase(p.getDatatype())) {
							sameType = true
							break;
						}
					}
				}
			}
			return sameType
			//return true;
		}
	}

	/**
	 * Check if there is only one possible association; if so, return an AssociationBundle with the info for that association
	 * @return an AssociationBundle about the solo association
	 */
	public AssociationBundle canSolo() {
		int leftChoices = 0
		int rightChoices = 0
		int bothChoices = 0
		AssociationBundle resultingBundle = new AssociationBundle();
		AssociationBundle leftBundle = new AssociationBundle();
		AssociationBundle rightBundle = new AssociationBundle();
		AssociationBundle bothBundle = new AssociationBundle();

		for (String pid: yin.listProducers(false)) {
			Producer p = yin.getProducer(pid)

			for (String cid: yang.listConsumers(false)) {
				Consumer c = yang.getConsumer(cid)
				if (c.canAssociate(p)) {
					leftChoices++

					leftBundle.producerComponent = yin
					leftBundle.producerName = pid
					leftBundle.consumerComponent = yang
					leftBundle.consumerName = cid


				}
			}
		}

		for (String pid: yang.listProducers(false)) {
			Producer p = yang.getProducer(pid)

			for (String cid: yin.listConsumers(false)) {
				Consumer c = yin.getConsumer(cid)
				if (c.canAssociate(p)) {
					rightChoices++

					rightBundle.producerComponent = yang
					rightBundle.producerName = pid
					rightBundle.consumerComponent = yin
					rightBundle.consumerName = cid

				}
			}
		}

		for (String pid: yang.listConducers()) {
			Conducer p = yang.getConducer(pid)

			for (String cid: yin.listConducers()) {
				Conducer c = yin.getConducer(cid)
				if (c.canProsume(p)) {
					bothChoices++

					bothBundle.producerComponent = yang
					bothBundle.alpha = p
					bothBundle.consumerComponent = yin
					bothBundle.omega = c
					//bothBundle.isConducer = true

				}
			}
		}

		//println "canSolo: $leftChoices $rightChoices $bothChoices"
		//println leftBundle
		//println rightBundle
		//println bothBundle

		if (leftChoices + rightChoices + bothChoices == 1) {
			if (leftChoices == 1) {
				resultingBundle = leftBundle
			}
			if (rightChoices == 1) {
				resultingBundle = rightBundle
			}
			if (bothChoices == 1) {
				resultingBundle = bothBundle
			}
			resultingBundle.isValid = true
			resultingBundle.make = true
		}

		//println "canSolo reports $resultingBundle"

		return resultingBundle
	}

	/**
	 * Returns true if all the associations in the collection exist
	 * @param bigQuestion
	 * @return
	 */
	public boolean associationsExist(List<AssociationBundle> bigQuestion) {
		boolean fullResult = true
		for (AssociationBundle ab: bigQuestion) {
			if (!associationExists(ab)) {
				fullResult = false
			}
		}
		return fullResult
	}

	public boolean associationExists(AssociationBundle question) {
		boolean result = false
		if (!question.isConducer()) {
			def con = question.consumerComponent.getConsumer(question.consumerName)
			if (con == null) {
				return false
			}
			if (con.hasProducer(question.producerComponent, question.producerName)) {
				result = true
			}
		} else {
			result = question.alpha.isProsuming(question.omega)
		}
		return result
	}

	/**
	 * Begin an association process between the yin and yang components for this controller.
	 * If there is only one possible association, make it; otherwise, show the dialog
	 * @param parentFrame the frame to use as the parent for the dialog
	 * @return true if (any) association was successful
	 */
	public boolean associate(JFrame parentFrame) {
		log.trace("Starting Association Process")
		//println "defaults? ${this.hasDefaults()}"
		//println "yin to yang:"
		//println calculateAssociations(yin, yang, true)
		//println "yang to yin:"
		//println calculateAssociations(yang, yin, true)
		boolean result = false

		if (!this.canAssociate()) {
			log.warn(CentralCatalogue.getUIS("NodeAssociator.cannotAssociate"), "statusbar")
			return false
		}


		def bundle = canSolo()
		boolean alreadyExists = false
		if (bundle.isValid) {
			alreadyExists = associationExists(bundle)
		}

		if (bundle.isValid && !alreadyExists) {
			//make the solo connection
			executeBundle(bundle)
			result = true
		} else {
			if (alreadyExists) {
				log.info("Solo Association already in place, opening dialog", "default")
			}

			//check for one more case: we have > 1 possible assoc, BUT they're all default
			//in this case, we want to act like a solo
			if (this.hasDefaults()) {
				if (this.getYinToYang().size() == this.getDefaults(yin, yang).size()) {
					if (!associationsExist(this.getDefaults(yin, yang))) {
						this.executeBundles(this.getDefaults(yin, yang))
						currentStatus = true
						return currentStatus
					}
				} else if (this.getYangToYin().size() == this.getDefaults(yang, yin).size()) {
					if (!associationsExist(this.getDefaults(yang, yin))) {
						this.executeBundles(this.getDefaults(yang, yin))
						currentStatus = true
						return currentStatus
					}
				}
			}

			//if (parentFrame != null){
			//println "SHOW DIALOG"
			dialog = new AssociationDialog(parentFrame, this)
			dialog.show()

			//result is based on what the user clicks on the dialog
			result = currentStatus
			//} else {
			//	result = false
			//}
		}

		//clean up before we bail out of here.
		if (dialog) {
			dialog.dispose()
			dialog = null
		}
		return result
	}

	/**
	 * Builds the specified association, if possible
	 * @param producer
	 * @param producerId
	 * @param consumer
	 * @param consumerId
	 * @return
	 * @since Jupiter
	 */
	public boolean associate(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
		def bundle = new AssociationBundle(producer, producerId, consumer, consumerId, true)
		if (this.bundleIsValid(bundle)) {
			bundle.make = true
			this.executeBundle(bundle)
			return true
		} else {
			return false
		}
	}

	/**
	 * Checks a given bundle to see if it is a possible association
	 * @param bundle an AssociationBundle to check
	 * @return true if the bundle can be made
	 * @since Jupiter
	 */
	public boolean bundleIsValid(AssociationBundle bundle) {
		boolean result = true
		if (!this.canAssociate()) {
			result = false
		}
		if (bundle.producerComponent == bundle.consumerComponent) {
			result = false
		}
		if (bundle.producerComponent != this.yin && bundle.producerComponent != this.yang) {
			result = false
		}
		if (bundle.consumerComponent != this.yin && bundle.consumerComponent != this.yang) {
			result = false
		}
		if (bundle.producer.datatype != bundle.consumer.datatype) {
			result = false
		}
		return result
	}

	/**
	 * Associates two components, assuming that the consumerComponent has only a single consumer to the specified Producer
	 * @param producerComponent
	 * @param producer
	 * @param consumerComponent
	 * @return
	 */
	public boolean associateToProducer(DLComponent producerComponent, Producer producer, DLComponent consumerComponent) {
		if (!this.canAssociate()) {
			//println "associateToProducer() ! canAssoc"
			return false
		}

		String consumerId
		List<String> consumers = consumerComponent.listConsumers(false)
		if (consumers.size() > 1) {
			//println "associateToProducer() too many consumers"
			return false
		}
		consumerId = consumers.first()

		def ab = new AssociationBundle(producerComponent, producer.getId(), consumerComponent, consumerId, true)
		ab.make = true

		if (!this.bundleIsPossible(ab)) {
			//println "associateToProducer() not possible"
			return false
		}
		this.executeBundle(ab)
		return true
	}

	/**
	 * Checks if the association represented by a given bundle is legal, based on datatype and membership in conducers.
	 * @param bundle
	 * @return
	 */
	public boolean bundleIsPossible(AssociationBundle bundle) {
		Producer p = bundle.getProducer()
		Consumer c = bundle.getConsumer()
		return ((c.getDatatype().equalsIgnoreCase(p.getDatatype())) && (p.hasConducer() == c.hasConducer()))
	}

	/**
	 * Mainly for testing - set up the dialog for this controller but don't display it.
	 */
	public void initDialog() {
		dialog = new AssociationDialog(null, this)
		dialog.initDialog()
	}

	public void executeBundles(List<AssociationBundle> bundles) {
		//println "executeBundles($bundles)"
		bundles.each { executeBundle(it); }
	}


	public void executeBundle(AssociationBundle bundle) {
		//log.debug("Execute Bundle: $bundle")
		if (bundle.make) {
			if (!bundle.isConducer()) {
				bundle.consumerComponent.associate(bundle.consumerName, bundle.producerComponent, bundle.producerName)
			} else {
				bundle.getAlphaComponent().associate(bundle.alpha, bundle.getOmegaComponent(), bundle.omega)
			}
		} else {
			//println "remove bundle $bundle"
			if (!bundle.isConducer()) {
				//println "unassociate ${bundle.consumerComponent} . ${bundle.consumerName}, ${bundle.producerComponent}, ${bundle.producerName}"
				bundle.consumerComponent.unassociate(bundle.consumerName, bundle.producerComponent, bundle.producerName)
			} else {
				bundle.getAlphaComponent().unassociate(bundle.alpha, bundle.getOmegaComponent(), bundle.omega)
			}
		}

	}

	/**
	 * Callback for the dialog to send the user-selected associations back to the controller
	 * @param resultingBundles
	 */
	public boolean applyChanges(List<AssociationBundle> incomingBundles) {
		//log.debug("Applying changes from Dialog: $bundles")
		//println "applyChanges() is go: $bundles"

		//scrub out any non-valid bundles
		List<AssociationBundle> bundles = []
		for (AssociationBundle ab: incomingBundles) {
			if (ab.isValid) { bundles.add(ab) }
		}

		//check for two-way
		if (hasTwoWay(bundles)) {
			return false;
		}

		clearAllAssociations()

		for (AssociationBundle ab: bundles) {
			//since we blew away everything first, we can just add the live ones, and ignore make=false
			if (ab.make) {
				//println "do this bundle $ab"
				executeBundle(ab)
				currentStatus = true
			}
		}
		return true;
	}

	/**
	 * Detach any and all associations between yin and yang.
	 */
	public void clearAllAssociations() {
		log.trace("removing all associations between $yin and $yang")
		List current = []
		current.addAll(calculateAssociations(yin, yang, false))
		current.addAll(calculateAssociations(yang, yin, false))
		current.addAll(calculateConducers(false))
		current.each { it.make = false }
		executeBundles(current)
	}

	/**
	 * Generate a list of bundles representing associations between <code>producerComponent</code> and <code>consumerComponent</code>.
	 * Since the logic is the same for yin->yang or yang->yin, we genericify this.
	 * This also might be helpful outside the AssociationController, so we might want to make this static.
	 * @param producerComponent
	 * @param consumerComponent
	 * @param includeBlanks if true, the result set also includes half-full bundles for any unassociated producers, or choices checked in the dialog
	 * @return a List of bundles
	 */
	public List<AssociationBundle> calculateAssociations(DLComponent producerComponent, DLComponent consumerComponent, boolean includeBlanks) {
		//println "calculateAssociations($producerComponent, $consumerComponent, $includeBlanks)"
		if (fromDialog == null) {
			//println "calculate is getting a bundles from the dialog"
			fromDialog = dialog.getBundles()
		}
		//println "in calculateAssociations, dialog list has ${fromDialog.size()} entries, is $fromDialog"
		//log.trace("in calculateAssociations, dialog list has ${fromDialog.size()} entries:")
		//fromDialog.each{
		//	log.trace(">> $it")
		//}

		List<AssociationBundle> bundles = []
		for (String p: producerComponent.listProducers(false)) {
			Producer prod = producerComponent.getProducer(p)
			//don't bother looking if we can't connect
			if (consumerComponent.consumersOfType(prod.getDatatype()).size() == 0) {
				//println "I just continued"
				continue
			}
			int countForThisProducer = 0

			//add any currently active associations
			prod.listConsumerComponents().each { pair ->
				if (pair.a == consumerComponent) {
					def currentBundle = new AssociationBundle()
					currentBundle.producerComponent = producerComponent
					currentBundle.producerName = p
					currentBundle.consumerComponent = pair.a
					currentBundle.consumerName = pair.b
					currentBundle.isValid = true
					currentBundle.make = true
					//println "adding $currentBundle"
					bundles += currentBundle
					countForThisProducer++
				}
			}

			if (includeBlanks) {

				//include any choices clicked in the dialog already (in case of a regeneration)
				fromDialog.each { fd ->
					//if ( fd.producerName.equals(p) && !bundles.contains(fd) ){
					if (fd.producerName.equals(p) && fd.producerComponent.getType().equals(producerComponent.getType())) {

						//screen out anything we're going to get from somewhere else (it's active, or it's a default)
						//if ( ! this.associationExists(fd) && ! fd.isDefault() && fd.make == true ){
						if (!this.associationExists(fd) && !fd.isDefault()) {
							bundles += fd
							countForThisProducer++
						}

					}
				}

			}

			//two ways to get blank lines: no connections for this producer OR the user has clicked the plus button
			//of course, blankLines must be true and there must be a way to connect with this producer
			if (includeBlanks && consumerComponent.consumersOfType(prod.getDatatype()).size() > 0) {

				//if there are any default connections off this producer that aren't already clicked, put them here
				def defaultBundles = this.getProducerDefaults(producerComponent, p, consumerComponent)
				defaultBundles.each { db ->
					//if ( ! bundles.contains(db) ){
					if (!AssociationBundle.containsExceptForMake(bundles, db)) {
						//println "adding a default"
						bundles += db
						//set make to true for defaults
						db.make = true
						countForThisProducer++
					}
				}

				//blank line to add new associations if none already exist
				//if ( prod.listConsumerComponents().size() == 0 && defaultBundles.size() == 0 && countForThisProducer == 0 ){
				if (countForThisProducer == 0) {
					//println "Adding blank line"
					bundles += new AssociationBundle(producerComponent, p, consumerComponent)
				}

				//any extra blank lines requested via the plus button
				String blankLineKey = "${producerComponent.getType()}#$p"
				//String blankLineKey = "$p"
				//println "in calculateAssociations, blankLines = $blankLines, key is '$blankLineKey'"
				if (blankLines.containsKey(blankLineKey)) {

					def blanksWeAlreadyHave = 0
					for (AssociationBundle b: bundles) {
						if (b.getProducer() == prod && b.make == false) {
							blanksWeAlreadyHave++
						}
					}

					def numberOfBlankLinesToAdd = blankLines[blankLineKey] - blanksWeAlreadyHave
/*
					//println "found the key"
					//for(int i = 0; i < blankLines[blankLineKey]; i++){
					for (int i = 0; i < numberOfBlankLinesToAdd; i++) {
						//println "adding plus-button line for $blankLineKey"
						bundles += new AssociationBundle(producerComponent, p, consumerComponent)
					}
*/
					if (numberOfBlankLinesToAdd > 0){
						//just add one, actually. (?)
						bundles += new AssociationBundle(producerComponent, p, consumerComponent)
					}
				}

			}

		}
		return bundles.sort()
	}

	/**
	 * True if there is more than one valid consumer - that is, should we show the "+" button.
	 * @param producerComponent
	 * @param consumerComponent
	 * @return
	 */
	public boolean hasMoreThanOneConsumer(DLComponent producerComponent, DLComponent consumerComponent) {
		//return true
		def bundles = this.calculateAssociations(producerComponent, consumerComponent, true)
		boolean moreThanOne = false
		for (AssociationBundle b: bundles) {
			if (b.possibleConsumers.size() > 1) {
				moreThanOne = true
			}
		}
		return moreThanOne
	}

	public boolean allowMoreThanOneConnection(DLComponent producerComponent, String producerId, DLComponent consumerComponent) {
		//there are two reasons not to show the button:
		// there is only one consumer on the consumerComponent, or the producer is exclusive
		//consumer != collection is handled by the checkboxes
		boolean isExclusive = producerComponent.getProducer(producerId).isExclusive()
		boolean isSoloConsumer = !this.hasMoreThanOneConsumer(producerComponent, consumerComponent)  //(consumerComponent.listConsumers(false).size() == 0)

		if (isExclusive || isSoloConsumer) {
			return false
		} else {
			return true
		}
	}

	public boolean allowMoreThanOneConnection(AssociationBundle bundle) {
		return (this.allowMoreThanOneConnection(bundle.producerComponent, bundle.producer.getId(), bundle.consumerComponent))
	}

	//todo: add caching of these guys

	public List<AssociationBundle> getYinToYang() {
		return this.calculateAssociations(yin, yang, true)
	}

	public List<AssociationBundle> getYangToYin() {
		return this.calculateAssociations(yang, yin, true)
	}

	public boolean hasYinToYang() {
		return (this.calculateAssociations(yin, yang, true).size() > 0)
	}

	public boolean hasYangToYin() {
		//println "hasYangToYin()"
		return (this.calculateAssociations(yang, yin, true).size() > 0)
	}


	public List<AssociationBundle> calculateConducers(boolean includeBlanks) {
		List<AssociationBundle> bundles = []

		//before we go any further, see if conducers are worth looking at all
		if (yin.listConducers().size() == 0 || yang.listConducers().size() == 0) {
			return []
		}

		//work out conducers
		yin.listConducers().each { c ->
			Conducer alpha = yin.getConducer(c)
            for( Conducer prosumer : alpha.getProsumers() ) {
                if ( prosumer.getOwner() == yang ) {
                    bundles += new AssociationBundle(yin, alpha, yang, prosumer)
                }
            }

			//if ( includeBlanks && ! alpha.hasProsumer() && alpha.canProsume(yang) ){
			if (includeBlanks && alpha.canProsume(yang)) {

				if (!alpha.isProsuming(yang)) {
					bundles += new AssociationBundle(yin, alpha, yang)
				}


				if (blankLines.containsKey(alpha.getId())) {
					for (int i = 0; i < blankLines[alpha.getId()]; i++) {
						bundles += new AssociationBundle(yin, alpha, yang)
					}
				}

			}
		}

		return bundles.sort()
	}

	public boolean hasConducers() {
		return (this.calculateConducers(true).size() > 0)
	}

	/**
	 * Checks to see if a list of bundles has connections going both ways
	 * @param bundles
	 * @return
	 */
	public boolean hasTwoWay(List<AssociationBundle> bundles) {
		if (bundles.size() == 0) { return false; }
		boolean result = false
		DLComponent producerComponent = bundles[0].producerComponent
		bundles.each { b ->
			if (b.producerComponent != producerComponent)
				result = true
		}
		return result
	}
/*
	public boolean hasDefaults(List<AssociationBundle> bundles){
		println "defaults? $bundles"
		if ( bundles.size() == 0 ){ return false; }
		boolean result = false;
		bundles.each{ b ->
			if ( b.isDefault() ){
				result = true
			}
		}
		return result;
	}
*/

	public boolean hasDefaults(DLComponent producerComponent, DLComponent consumerComponent) {
		boolean results = false
		producerComponent.getAllProducers().each { p ->
			def pName = p.getName()
			consumerComponent.getAllConsumers().each { c ->
				if (c.getName().equals(pName)) {
					results = true
				}
			}
		}
		return results
	}

	public boolean hasDefaultConducers(DLComponent alphaComponent, DLComponent omegaComponent) {
		boolean results = false
		alphaComponent.getAllConducers().each { p ->
			def pName = p.getName()
			omegaComponent.getAllConducers().each { c ->
				if (c.getName().equals(pName)) {
					results = true
				}
			}
		}
		return results
	}

	public boolean hasDefaults() {
		return (this.hasDefaults(yin, yang) || this.hasDefaults(yang, yin) || this.hasDefaultConducers(yin, yang))
	}

	public List<AssociationBundle> getDefaults(DLComponent producerComponent, DLComponent consumerComponent) {
		def results = []
		producerComponent.getAllProducers().each { p ->
			def pName = p.getName()
			consumerComponent.getAllConsumers().each { c ->
				if (c.getName().equals(pName)) {
					//results += new AssociationBundle(producerComponent, p.getId(), consumerComponent, c.getId(), true)
					def b = new AssociationBundle(producerComponent, p.getId(), consumerComponent, c.getId(), true)
					b.make = true
					results += b
				}
			}
		}
		return results
	}

	public List<AssociationBundle> getProducerDefaults(DLComponent producerComponent, String producerId, DLComponent consumerComponent) {
		def results = []
		Producer p = producerComponent.getProducer(producerId)
		def pName = p.getName()
		consumerComponent.getAllConsumers().each { c ->
			if (c.getName().equals(pName) && c.getDatatype().equals(p.getDatatype())) {
				results += new AssociationBundle(producerComponent, p.getId(), consumerComponent, c.getId(), true)
			}
		}
		return results
	}

	public List<AssociationBundle> getDefaultAlphaConducers(DLComponent alphaComponent, Conducer alpha, DLComponent omegaComponent) {
		//todo: fill this in
		return []
	}

	public List<AssociationBundle> getYinToYangDefaults() {
		return getDefaults(yin, yang)
	}

	public List<AssociationBundle> getYangToYinDefaults() {
		return getDefaults(yang, yin)
	}


	public int totalConnections() {
		return this.getYinToYang().size() + this.getYangToYin().size() + this.calculateConducers(true).size()
	}

	/**
	 * Find the total number of active associations between the two components in this controller
	 * @return the total number of associations
	 * @since Jupiter
	 */
	public int currentConnections() {
		return this.calculateAssociations(yin, yang, false).size() + this.calculateAssociations(yang, yin, false).size() + this.calculateConducers(false).size()
	}

	/**
	 * Finds the longest consumer name between both components of the given association type.
	 * Used for things like sizing controls in the dialog.
	 * @param assocType
	 * @return
	 */
	public String getLongestConsumerName(String assocType) {
		String proto = ""
		yin.getConsumerObjectsOfType(assocType).each {
			if (it.getName().length() > proto.length()) {
				proto = it.getName()
			}
		}
		yang.getConsumerObjectsOfType(assocType).each {
			if (it.getName().length() > proto.length()) {
				proto = it.getName()
			}
		}
		return proto;
	}

	/**
	 * Calculate the name for a secondary-class component.
	 * The basic format is "$HostName - $DefaultSecondaryName".
	 * However, if there is more than one producer on the host of the datatype in use, the format becomes
	 * "$HostName:ProducerDisplayName - $DefaultSecondaryName".
	 * If there is no producer->consumer relationship between the host and the secondary component,
	 * the name is just "$DefaultSecondaryName".
	 *
	 * This method doesn't check to make sure that the component indicated is officially a secondary component.
	 *
	 * @param host the DLComponent acting as host
	 * @param secondary the DLComponent that is secondary
	 * @return the calculated name
	 */
	public String calculateSecondaryName(DLComponent host, DLComponent secondary) {
		//assume (for the moment) we're yin->yang
		//base format is "$yinName - $yangName"
		//unless there are >1 producer of that type, in which case we do "$yinName : $producerName - $yangName"
		String result = ""
		String baseName = secondary.getModel().getTypeDefaultValue(secondary.getType(), "name")

		if (host == null) { return baseName; }

		String hostName = host.getName()
		if (host.hasRole("staticchild")) {
			//go find the static child host for this subrack and prepend it's name to hostName
			def superComp = host.getParentComponents().find { it.hasRole("staticchildplaceable") }
			hostName = "${superComp.getName()} - $hostName"
		}

		//first question: is this H->S, or H<-S?
		def secToHostConnections = this.calculateAssociations(secondary, host, false)
		if (secToHostConnections.size() > 0) {
			//we're an "other way" secondary, so we don't use the intermediate name
			return "$hostName - $baseName"
		}

		//okay, having made it this far, we're in a "normal" host->sec situation
		def connections = this.calculateAssociations(host, secondary, false)
		if (connections.size() == 0) {
			//something may be really wrong, but we'll just quietly hand out...
			return baseName

		}

		int possibleConnections = host.getProducerObjectsOfType(connections.first().getProducer().getDatatype()).size()
		def firstConnection = connections.first()
		String datatype = firstConnection.getProducer().getDatatype()
		def producers = host.getProducerObjectsOfType(datatype)

		if (producers.size() == 1 || connections.size() == possibleConnections) {
			result = "$hostName - $baseName"
			//} else if ( producers.size() > 1 ){
		} else {
			//now we want to include the producer name
			String producerDisplayName = firstConnection.getProducer().getName()
			//println producerDisplayName
			result = "$hostName: $producerDisplayName - $baseName"
//		} else {
//			result = baseName
		}
		return result
	}

	/**
	 * Stores the number of additional blank lines requested via the dialog box (with the plus button) in the form
	 * producerName : numberOfBlanks
	 */
	Map<String, Integer> blankLines = [:]

	/**
	 * Called by the "+" buttons in the dialog.
	 * @param e ActionEvent from the dialog's widgets
	 */
	@Override
	void actionPerformed(ActionEvent e) {
		log.trace("getting action command from button to add a blank line to ${e.getActionCommand()}")
		MapHelper.increment(blankLines, e.getActionCommand())
		//before we call regenerate, get a list of all the selected options in the dialog so we can make sure to repopulate those into the dialog
		fromDialog = dialog.getBundles()
		//log.trace( "blanklines are: $blankLines" )
		//log.trace("dialog bundles:")
		//log.trace(fromDialog.toString())
		dialog.regenerate()
	}


}