package DeploymentLab.Dialogs.Association;

import DeploymentLab.Model.Conducer;
import DeploymentLab.Model.Consumer;
import DeploymentLab.Model.DLComponent;
import DeploymentLab.Model.Producer;

import java.util.List;

/**
 * Stores all information needed to represent an association between two components.
 *
 * @author Gabriel Helman
 * @since Mars
 */
public class AssociationBundle implements Comparable<AssociationBundle> {
	DLComponent producerComponent;
	String producerName;
	Conducer alpha;
	DLComponent consumerComponent;
	String consumerName;
	Conducer omega;
	//todo: replace isValid with some actual logic
	boolean isValid;

	/**
	 * When handed to the executeBundle() method (or similar, make is a flag indicating if the association should be removed or created
	 */
	boolean make;

	public AssociationBundle() {
		isValid = false;
		make = false;
	}

	public AssociationBundle(DLComponent producer, String producerId, DLComponent consumer) {
		isValid = false;
		make = false;
		producerComponent = producer;
		producerName = producerId;
		consumerComponent = consumer;
		consumerName = null;
	}

	public AssociationBundle(DLComponent producer, String producerId, DLComponent consumer, String consumerId, boolean isValid) {
		this.isValid = isValid;
		make = false;
		producerComponent = producer;
		producerName = producerId;
		consumerComponent = consumer;
		consumerName = consumerId;
	}

	public AssociationBundle(DLComponent alphaComponent, Conducer alpha, DLComponent omegaComponent, Conducer omega) {
		this.producerComponent = alphaComponent;
		this.alpha = alpha;
		this.consumerComponent = omegaComponent;
		this.omega = omega;
		this.producerName = null;
		this.consumerName = null;
		this.isValid = true;
	}

	public AssociationBundle(DLComponent alphaComponent, Conducer alpha, DLComponent omegaComponent) {
		this.producerComponent = alphaComponent;
		this.alpha = alpha;
		this.consumerComponent = omegaComponent;
		this.omega = null;
		this.producerName = null;
		this.consumerName = null;
		this.isValid = false;
	}

	/**
	 * Copy constructor.
	 *
	 * @param source An AssociationBundle to copy
	 */
	public AssociationBundle(AssociationBundle source) {
		this.producerComponent = source.producerComponent;
		this.producerName = source.producerName;
		this.alpha = source.alpha;
		this.consumerComponent = source.consumerComponent;
		this.consumerName = source.consumerName;
		this.omega = source.omega;
		this.isValid = source.isValid;
		this.make = source.make;
	}

	public DLComponent getProducerComponent() {
		return producerComponent;
	}

	public DLComponent getConsumerComponent() {
		return consumerComponent;
	}

	public String getProducerName() {
		return producerName;
	}

	public String getConsumerName() {
		return consumerName;
	}

	public Producer getProducer() {
		return this.producerComponent.getProducer(this.producerName);
	}

	public Consumer getConsumer() {
		return this.consumerComponent.getConsumer(this.consumerName);
	}

	public Conducer getAlpha(){
		return alpha;
	}
	public Conducer getOmega(){
		return omega;
	}

	public DLComponent getAlphaComponent() {
		return producerComponent;
	}

	public DLComponent getOmegaComponent() {
		return consumerComponent;
	}

	/**
	 * Returns true if this bundle represents a Conducer.
	 * Originally, this was just a instance variable that got set manually,
	 * but now just checks to see if there are conducers or not (so now it can't be out of sync.)
	 *
	 * @return True if a conducer
	 * @since Jupiter
	 */
	public boolean isConducer() {
		return (this.alpha != null || this.omega != null);
	}

	public boolean consumerIsCollection() {
		if (this.isConducer()) {
			return false;
		} else {
			return this.getConsumer().isCollection();
		}

	}

	public boolean producerIsSolo() {
		//for the future, this would be nice to flag
		return false;
	}

	/**
	 * Returns a list of all valid Consumers on the consumer component that can consume the producer in this bundle
	 *
	 * @return
	 */
	public List<Consumer> getPossibleConsumers() {
		return consumerComponent.getConsumerObjectsOfType(this.getProducer().getDatatype());
	}

	/**
	 * Cache the isDefault logic, since we really only need to run those compares once.
	 */
	private Boolean isDefaultCache = null;

	/**
	 * Checks to see if this association should be on by default between these two components.
	 * Currently, this is designated by both connections having the same name.
	 *
	 * @return true if this is a default association
	 */
	public boolean isDefault() {
		if (isDefaultCache != null) {
			return isDefaultCache;
		}
		if (this.getConsumer() == null || this.getProducer() == null) {
			isDefaultCache = false;
			return false;
		}
		if (this.isConducer()) {
			if (alpha.getName().equals(omega.getName())) {
				isDefaultCache = true;
				return true;
			}
		} else {
			if (this.getProducer().getName().equals(this.getConsumer().getName())) {
				isDefaultCache = true;
				return true;
			}
		}
		isDefaultCache = false;
		return false;
	}

	@Override
	public String toString() {
		return "AssociationBundle{" +
				"producerComponent=" + producerComponent +
				", producerName='" + producerName + '\'' +
				", alpha=" + alpha +
				", consumerComponent=" + consumerComponent +
				", consumerName='" + consumerName + '\'' +
				", omega=" + omega +
				", isConducer=" + this.isConducer() +
				", isValid=" + isValid +
				", make=" + make +
				'}';
	}

	@Override
	public int compareTo(AssociationBundle o) {
		if (!this.isConducer()) {
			return this.getProducer().getName().compareTo(o.getProducer().getName());
		} else {
			return this.alpha.compareTo(o.alpha);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		AssociationBundle that = (AssociationBundle) o;

		if (isValid != that.isValid) return false;
		if (make != that.make) return false;
		if (alpha != null ? !alpha.equals(that.alpha) : that.alpha != null) return false;
		if (consumerComponent != null ? !consumerComponent.equals(that.consumerComponent) : that.consumerComponent != null)
			return false;
		if (consumerName != null ? !consumerName.equals(that.consumerName) : that.consumerName != null) return false;
		if (omega != null ? !omega.equals(that.omega) : that.omega != null) return false;
		if (producerComponent != null ? !producerComponent.equals(that.producerComponent) : that.producerComponent != null)
			return false;
		if (producerName != null ? !producerName.equals(that.producerName) : that.producerName != null) return false;

		return true;
	}


	public boolean equalsExceptForMake(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		AssociationBundle that = (AssociationBundle) o;

		if (isValid != that.isValid) return false;
		if (alpha != null ? !alpha.equals(that.alpha) : that.alpha != null) return false;
		if (consumerComponent != null ? !consumerComponent.equals(that.consumerComponent) : that.consumerComponent != null)
			return false;
		if (consumerName != null ? !consumerName.equals(that.consumerName) : that.consumerName != null) return false;
		if (omega != null ? !omega.equals(that.omega) : that.omega != null) return false;
		if (producerComponent != null ? !producerComponent.equals(that.producerComponent) : that.producerComponent != null)
			return false;
		if (producerName != null ? !producerName.equals(that.producerName) : that.producerName != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = producerComponent != null ? producerComponent.hashCode() : 0;
		result = 31 * result + (producerName != null ? producerName.hashCode() : 0);
		result = 31 * result + (alpha != null ? alpha.hashCode() : 0);
		result = 31 * result + (consumerComponent != null ? consumerComponent.hashCode() : 0);
		result = 31 * result + (consumerName != null ? consumerName.hashCode() : 0);
		result = 31 * result + (omega != null ? omega.hashCode() : 0);
		result = 31 * result + (isValid ? 1 : 0);
		result = 31 * result + (make ? 1 : 0);
		return result;
	}


	/**
	 * Checks to see if a collection contains a given bundle using the equalsExceptForMake method
	 *
	 * @param bundles a collection of bundles to check
	 * @param target  the bundle to look for in bundles
	 * @return true if <code>target</code> is present in <code>bundles</code>
	 */
	public static boolean containsExceptForMake(java.util.Collection<AssociationBundle> bundles, AssociationBundle target) {
		boolean result = false;
		for (AssociationBundle b : bundles) {
			if (b.equalsExceptForMake(target)) {
				result = true;
			}
		}
		return result;
	}

	public String getActionCommandName(){
		//"${bundle.producerComponent.getType()}#${bundle.producerName}"
		return this.producerComponent.getType() + "#" + this.producerName;
	}
}