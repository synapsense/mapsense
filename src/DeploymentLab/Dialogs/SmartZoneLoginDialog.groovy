package DeploymentLab.Dialogs

import DeploymentLab.CentralCatalogue
import DeploymentLab.channellogger.*
import DeploymentLab.SpringUtilities

import javax.swing.JOptionPane
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dialog.ModalityType
import java.awt.event.ActionListener
import java.awt.event.KeyEvent
import java.awt.FlowLayout
import java.awt.Toolkit

import javax.swing.JComponent
import javax.swing.JButton
import javax.swing.JDialog
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JPasswordField
import javax.swing.JTextField
import javax.swing.KeyStroke
import javax.swing.SpringLayout

import groovy.swing.SwingBuilder

/**
 * Dialog to get credentials to export the deployment to the server.
 */
class SmartZoneLoginDialog {
	private static final Logger log = Logger.getLogger(LoginDialog.class.getName())
	protected SwingBuilder builder
	private JDialog dialogDelegate
	private JButton okButton
	private JTextField logNameField
	private JPasswordField loginPassField
	private boolean isLogged = false
	private JLabel capsLockIndicator
	private Toolkit toolkit
	private JFrame parent
	private JTextField logHostField
	private JLabel errorMsgLabel
	private JLabel errorLabel

	public SmartZoneLoginDialog(JFrame _parent, boolean withHost = false) {
		builder = new SwingBuilder()
		parent = _parent
		dialogDelegate = this.getDialog(withHost)
	}

	private boolean validateUser(String user) {
		boolean status = true;
		if (user == null ||
			user.trim().isEmpty()) {
			JOptionPane.showMessageDialog(parent,
					"User Name cannot be empty",
					"Error",
					JOptionPane.ERROR_MESSAGE);
			logNameField.requestFocusInWindow()
			status = false;
		}
		return status;
	}

	private boolean validatePassword(String password) {
		boolean status = true;
		if (password == null || password.trim().isEmpty()) {
			JOptionPane.showMessageDialog(parent,
					"Password cannot be empty.\n",
					"Error",
					JOptionPane.ERROR_MESSAGE);
			this.loginPassField.requestFocusInWindow()
			status = false
		}
		return status;
	}

	private JDialog getDialog(boolean withHost = false) {
		dialogDelegate = builder.dialog(owner:parent, resizable: false, title: "SmartZone Server login", layout: new BorderLayout(), modalityType: ModalityType.APPLICATION_MODAL) {
			def okAction = {
				if (validateUser(logNameField.getText()) &&
					validatePassword(loginPassField.getText())) {
					isLogged = true;
					dialogDelegate.setVisible(false)
				}
				errorMsgLabel.setText("")
			}
			def cancelAction = {
				isLogged = false;
				dialogDelegate.setVisible(false)
				errorMsgLabel.setText("")
			}
			def capsLockAction = {setCapsLockIndicator()}
			def dlgPanel = panel(layout: new SpringLayout(), constraints: BorderLayout.NORTH) {
				label(text: "")
				errorMsgLabel = label(id: 'errorMsg', text: '      ', foreground: Color.red)
				label(text: "User Name:")
				logNameField = textField(id: 'loginName', text: 'admin', columns: 10)
				label(text: "Password:")
				loginPassField = passwordField(id: 'loginPass', text: '', columns: 10)
			}
			panel(layout: new FlowLayout(FlowLayout.CENTER), constraints: BorderLayout.CENTER) {
				capsLockIndicator = label(text:"          ", foreground: Color.red)
			}
			panel(layout: new FlowLayout(FlowLayout.TRAILING), constraints: BorderLayout.SOUTH) {
				okButton = button(text: 'OK', defaultButton: true, actionPerformed: okAction)
				button(text: 'Cancel', actionPerformed: cancelAction)
			}
			SpringUtilities.makeCompactGrid(dlgPanel,
                                3, 2,   //rows, cols
                                6, 6,   //initX, initY
                                5, 5);  //xPad, yPad			
			dlgPanel.registerKeyboardAction(okAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			dlgPanel.registerKeyboardAction(cancelAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
			dlgPanel.registerKeyboardAction(capsLockAction as ActionListener, KeyStroke.getKeyStroke(KeyEvent.VK_CAPS_LOCK, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
	
		}
		dialogDelegate.pack()
//		errorMsgLabel.setVisible(false)
		return dialogDelegate
	}
	
	private void setCapsLockIndicator(){
		try {
			if(Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK)){
				capsLockIndicator.setText("Caps Lock: on")
			}else{
				//capsLockIndicator.setText("Caps Lock: off")
				capsLockIndicator.setText("  ") //we'd just as well show nothing when the caps lock is off
			}
		} catch(Exception e) { // some environments (BSD) don't support the getLockingKeyState method.
			capsLockIndicator.setText("")
		}
	}
	
	public void setLocationRelativeTo() {
		dialogDelegate.setLocationRelativeTo(parent)
	}

	public void show(){
		this.requestFocusInWindow()
		setCapsLockIndicator()
		dialogDelegate.show()
	}

	public void setErrorMsg(String errorMsg) {
		isLogged = false
		errorMsgLabel.setText(errorMsg)
		CentralCatalogue.getSmartZoneConnecionProperties().remove("username")
		CentralCatalogue.getSmartZoneConnecionProperties().remove("password")
		okButton.setText("Retry")
	}

	public void requestFocusInWindow() {
		loginPassField.requestFocusInWindow()
	}
	
	public boolean getLoginInfo() {
        isLogged = false
		loginPassField.setText('')
		setLocationRelativeTo()
		requestFocusInWindow()
		show()

		if (!isLogged) {
			log.info('Cancelling export')
			return false
		} else{
			CentralCatalogue.getSmartZoneConnecionProperties().put('username', logNameField.text)
			CentralCatalogue.getSmartZoneConnecionProperties().put('password', new String(loginPassField.getPassword()))
			return true
		}
	}
}
