package DeploymentLab;

import DeploymentLab.channellogger.*;

import java.io.File;
import java.util.ArrayList;
import java.util.prefs.*;

import org.jboss.logging.Logger;

public class UserPreferences {
	private static Preferences prefs = Preferences.userRoot();
	private static final Logger log = Logger.getLogger(UserPreferences.class.getName());
	
	private static String RECENT_FILES = "recentFiles";
	private static String RECENT_FILES_COUNT = "recentFilesCount";
	private static int MAX_NUM_RECENT_FILES = 5;
	
	private static String COMLIB_FILES = "comlibFiles_";


	/*
	Store the last path for all choosers:
    */
	//public static String DIR = "dirChooser"; //dont use this, the dir chooser is just for images
	public static String FILE = "fileChooser";   //supersedes both IMAGE and SAVE; now functions basically as the CWD
	//public static String SAVE = "saveFileChooser";  //really, the save-as path
	//public static String IMAGE = "imageFileChooser"; //used 5 times (inc new projects)

	//public static String OPEN = "openFileChooser"; //now unused, the same as the SAVE path
	public static String SIM = "simFileChooser";   //store dir (file name is generated)
	public static String CLIB = "clibFileChooser"; //store dir
	public static String CSV = "csvFileChooser";  //export target for CSV files, etc
	public static String CSV_FILENAME = "csvChooserFileName";
	public static String CSV_TITLE = "csvChooserTitle";
	public static String EXCEL = "excelFileChooser";

	public static String IMPORT = "importChooser";

	public static String FILENAME = "fileChooserFileName";

	public void setUserPreference(String name, String value){
		prefs.put(name, value);
	}
	
	public String getUserPreference(String name){
		if(prefs.get(name, null)==null){
			return "";
		}else{
			return prefs.get(name, null);
		}
	}

	public String getUserPreference(String name, String def){
		return prefs.get( name, def );
	}
	
	/*	
	 * Implementation about recent file
	 * */
	 
	public ArrayList<String> getRecentFiles(){
		ArrayList<String> fileList = new ArrayList<String>();
		
		for(int i=MAX_NUM_RECENT_FILES;i>0;i--){
			String file = prefs.get(RECENT_FILES + i, null);
			if(file != null)
				fileList.add(file);
		}
		
		return fileList;
	}
	
	public void setRecentFile(String filePath){
		int cur_num_recent_files = prefs.getInt(RECENT_FILES_COUNT, 0);
		
		// if new file path exists in recent files
		if(cur_num_recent_files>0){
			for(int j=1;j<=cur_num_recent_files;j++){
				String existingFile = prefs.get(RECENT_FILES+j,null);
				if(filePath.equals(existingFile)){
					if(j == MAX_NUM_RECENT_FILES){
						return;
					}else{
						int k;
						for(k=j;k<cur_num_recent_files;k++){
							prefs.put(RECENT_FILES + k, prefs.get(RECENT_FILES+(k+1),null));
						}
						prefs.put(RECENT_FILES+cur_num_recent_files, filePath);
						return;
					}
				}
			}
		}

		cur_num_recent_files++;
		if(cur_num_recent_files > MAX_NUM_RECENT_FILES){
			for(int i=2;i<=MAX_NUM_RECENT_FILES;i++){
				String value = prefs.get(RECENT_FILES+i, null);
				prefs.put(RECENT_FILES + (i-1), value);
			}
			cur_num_recent_files = MAX_NUM_RECENT_FILES;
		}
		prefs.put(RECENT_FILES + cur_num_recent_files, filePath);
		prefs.put(RECENT_FILES_COUNT,Integer.toString(cur_num_recent_files));
	}
	
	public int getRecentFileCount(){
		return prefs.getInt(RECENT_FILES_COUNT, 0);
	}
	
	public void clearRecentFiles(){
		int cur_num_recent_files = prefs.getInt(RECENT_FILES_COUNT, 0);
		
		if(cur_num_recent_files>0){
			for(int j=1;j<=cur_num_recent_files;j++){
				prefs.remove(RECENT_FILES+j);
			}
		}
		
		prefs.put(RECENT_FILES_COUNT,"0");

	}
	
	/*
	 * component library 
	 */
	
	public void addComponentLib(File file){
		String filename = file.getName();
		String filePath = file.getPath();
		prefs.put(COMLIB_FILES + filename, filePath);
	}
	
	public boolean isLoadedComponentLib(File file){
		try {
			for(String key : prefs.keys()){
				if(key.startsWith(COMLIB_FILES)){
					String loadedFileName = key.replace(COMLIB_FILES, "");
					if(loadedFileName.equals(file.getName())){
						return true;
					}
				}
			}
		} catch (BackingStoreException e) {
			return false;
		}
		
		return false;
	}
	
	public ArrayList<File> getComponentLibs(){
		ArrayList<File> fileList = new ArrayList<File>();
		try {
			for(String key : prefs.keys()){
				if(key.startsWith(COMLIB_FILES)){
					String filePath = prefs.get(key, null);
					if(filePath!=null){
						File file = new File(filePath);
						if(file.exists()){
							fileList.add(file);
						}else{
							prefs.remove(key);
						}
					}
				}
			}
		} catch (BackingStoreException e) {
			log.error("Cannot get saved componentLibrary files:" );
		}
		return fileList;
	}
	
	public void removeComponentLib(String filename){
		prefs.remove(COMLIB_FILES + filename);
	}
	
	public void clearAllComponentLib(){
		try {
			for(String key : prefs.keys()){
				if(key.startsWith(COMLIB_FILES)){
					prefs.remove(key);
				}
			}
		} catch (BackingStoreException e) {
			
		
		}
	}
}
