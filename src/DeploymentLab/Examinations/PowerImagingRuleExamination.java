package DeploymentLab.Examinations;

import DeploymentLab.Export.SimBundle;
import DeploymentLab.Export.SimBundleAtom;
import com.synapsense.dto.TO;
import com.synapsense.service.Environment;

import java.util.*;

//import com.synapsense.rulesengine.core.script.metric.PowerRackMetrics;

/**
 * Everything you could ever want to test with the PI stuff.
 * <p/>
 * Used mainly for the magic wand button.
 */
public class PowerImagingRuleExamination {

	private static List<Slot> createServers(int height, int spaceUsed, int maxUblock) {
		List<Slot> servers = new ArrayList<Slot>();
		int maxServerSize = 5;
		Integer freeSpace = height - spaceUsed;
		Random random = new Random();

		List<Slot> slots = createSlots(freeSpace, maxUblock, height);
		System.out.println(slots);
		for (int i = -1; i < slots.size(); i++) {
			int start = i == -1 ? 1 : slots.get(i).getLoc() + slots.get(i).getSize();
			int stop = i == slots.size() - 1 ? height + 1 : slots.get(i + 1).getLoc();

			int toFill = stop - start;
			while (toFill > 0) {
				int serverSize = Math.min(toFill, 1 + random.nextInt(maxServerSize));
				servers.add(new Slot(start, serverSize));
				start += serverSize;
				toFill -= serverSize;
			}
		}
		System.out.println(servers);
		return servers;
	}

	private static List<Slot> createSlots(int freeSpace, int maxUBlock, int height) {
		Random rand = new Random();

		List<Slot> slots = new ArrayList<Slot>();
		slots.add(new Slot(1 + rand.nextInt(height - maxUBlock), maxUBlock));

		freeSpace -= maxUBlock;
		while (freeSpace > 0) {
			int block = Math.min(1 + rand.nextInt(maxUBlock), freeSpace);
			int loc = 1 + rand.nextInt(height - block);

			if (!intersects(loc, block, slots)) {
				freeSpace -= block;
				slots.add(new Slot(loc, block));
			}
		}
		Collections.sort(slots);
		return slots;
	}

	private static boolean intersects(int loc, int size, List<Slot> slots) {
		for (Slot sl : slots) {
			if (sl.intersects(loc, size)) {
				return true;
			}
		}
		return false;
	}

	private static class Slot implements Comparable<Slot> {
		private int loc;
		private int size;

		public Slot(int loc, int size) {
			super();
			this.loc = loc;
			this.size = size;
		}

		public int getLoc() {
			return loc;
		}

		public int getSize() {
			return size;
		}

		public boolean intersects(int loc, int size) {
			if (this.loc >= loc && loc + size + 1 > this.loc) {
				return true;
			}
			if (this.loc <= loc && this.loc + this.size + 1 > loc) {
				return true;
			}
			return false;
		}

		@Override
		public int compareTo(Slot other) {
			if (this.loc > other.loc) {
				return 1;
			}
			if (other.loc > this.loc) {
				return -1;
			}
			return 0;
		}

		@Override
		public String toString() {
			return new StringBuilder().append(loc).append(":").append(size).toString();
		}
	}

	private static void deleteAllObjects(TO<?> parent, String... types) throws Exception {
		Environment env = getEnv();
		for (String type : types) {

			Collection<TO<?>> objects;
			if (parent == null) {
				objects = env.getObjectsByType(type);
			} else {
				objects = env.getRelatedObjects(parent, type, true);
			}
			for (TO<?> object : objects) {
				env.deleteObject(object);
			}
		}
	}

	/**
	 * GABE: THIS IS THE ONE!!
	 * <p/>
	 * -Signed, Gabe
	 *
	 * @param drawing
	 * @return
	 * @throws Exception
	 */
	public static List<SimBundle> createRPDUS(TO<?> drawing) throws Exception {
		Random random = new Random();
		Environment env = getEnv();

		List<SimBundle> results = new ArrayList<SimBundle>();
		SimBundle<TO<?>> phaseBundle = new SimBundle<TO<?>>("phase", new ArrayList<TO<?>>());
		SimBundle<TO<?>> phaseDeltaBundle = new SimBundle<TO<?>>("phase_delta", new ArrayList<TO<?>>());
		SimBundle<TO<?>> duoPhaseBundle = new SimBundle<TO<?>>("phase_duo", new ArrayList<TO<?>>());
		SimBundle<TO<?>> duoPhaseDeltaBundle = new SimBundle<TO<?>>("phase_duo_delta", new ArrayList<TO<?>>());
		SimBundle<TO<?>> plugBundle = new SimBundle<TO<?>>("plug", new ArrayList<TO<?>>());
		SimBundle<TO<?>> rpduBundle = new SimBundle<TO<?>>("rpdu", new ArrayList<TO<?>>());


		results.add(phaseBundle);
		results.add(phaseDeltaBundle);
		results.add(plugBundle);
		results.add(duoPhaseBundle);
		results.add(duoPhaseDeltaBundle);
		results.add(rpduBundle);

		try {

			env.configurationStart();

			//Collection<TO<?>> drawings = env.getObjects( ObjectType.DRAWING, new ValueTO[] { new ValueTO("name", "ATT Allen") });

//            for (TO<?> drawing : drawings) {

			String[] labels = new String[]{"A", "B", "C"};

			// deleteAllObjects(drawing, "RPDU", "PHASE", "PLUG");

			Collection<TO<?>> powerRacks = env.getRelatedObjects(drawing, "POWER_RACK", true);
			for (TO<?> rack : powerRacks) {

				if (!env.getRelatedObjects(rack, "RPDU", true).isEmpty()) {
					continue;
				}

				System.out.println("Configuring rack " + env.getPropertyValue(rack, "name", String.class));

				env.setPropertyValue(rack, "status", 1);

				String platform = env.getPropertyValue(rack, "platform", String.class);

				if (platform.equalsIgnoreCase("SC")) {

					Integer uHeight = new Integer(30 + random.nextInt(21));
					env.setPropertyValue(rack, "height", uHeight);
					Integer uSpaceused = uHeight - 10 + random.nextInt(10);
					env.setPropertyValue(rack, "uSpaceUsed", uSpaceused);
					if (uHeight - uSpaceused == 1) {
						env.setPropertyValue(rack, "maxUBlock", 1);
					} else {
						env.setPropertyValue(rack, "maxUBlock", 1 + random.nextInt(uHeight - uSpaceused - 1));
					}

					env.setPropertyValue(rack, "instrumentation", 1);

					Integer spaceUsed = env.getPropertyValue(rack, "uSpaceUsed", Integer.class);
					Integer maxUblock = env.getPropertyValue(rack, "maxUBlock", Integer.class);
					Integer height = env.getPropertyValue(rack, "height", Integer.class);
					List<Slot> createdServers = createServers(height, spaceUsed, maxUblock);
					for (Slot server : createdServers) {
						TO<?> to = env.createObject("SERVER");
						env.setRelation(rack, to);
						env.setPropertyValue(to, "uLocation", server.getLoc());
						env.setPropertyValue(to, "uHeight", server.getSize());
						env.setPropertyValue(to, "name", "Server" + server.getLoc() + "_" + server.getSize());
					}

				}

				List<TO<?>> allPhases = new ArrayList<TO<?>>();
				String rackName = env.getPropertyValue(rack, "name", String.class);
				Double nominalVoltage = env.getPropertyValue(rack, "nominalVoltage", Double.class);
				for (int i = 0; i < 2; i++) {
					TO<?> rpdu = env.createObject("RPDU");
//                        env.setPropertyValue(rpdu, "name", "RPDU_" + i + "_" + rackName );
					byte[] bytes = rackName.getBytes("ASCII");
					env.setPropertyValue(rpdu, "name", "RPDU_" + i + "_" + new String(bytes));
					env.setPropertyValue(rpdu, "rId", i);
					boolean isDelta = false;
					if (nominalVoltage == 110 || nominalVoltage == 120){
						env.setPropertyValue(rpdu, "type", 0);
					} else {
						isDelta = true;
						env.setPropertyValue(rpdu, "type", 1);
					}
					env.setPropertyValue(rpdu, "status", 1);
					rpduBundle.add(rpdu);
					env.setRelation(rack, rpdu);

					for (String phaseLb : labels) {
						TO<?> phaseTo = env.createObject("PHASE");
						env.setRelation(rpdu, phaseTo);
						env.setPropertyValue(phaseTo, "name", phaseLb);
						allPhases.add(phaseTo);

						if (platform.equalsIgnoreCase("SC")) {
							if( isDelta ) {
								phaseDeltaBundle.add(phaseTo);
							} else {
								phaseBundle.add(phaseTo);
							}
						} else {
							if( isDelta ) {
								duoPhaseDeltaBundle.add(phaseTo);
							} else {
								duoPhaseBundle.add(phaseTo);
							}
						}
					}
				}

				if (platform.equalsIgnoreCase("SC")) {

					Collection<TO<?>> servers = env.getChildren(rack, "SERVER");
					for (TO<?> server : servers) {
						for (int i = 0; i < 2; i++) {
							TO<?> plug = env.createObject("PLUG");
							env.setPropertyValue(plug, "pId", ((Integer) plug.getID()).longValue());
							env.setRelation(server, plug);

							int phaseNum = random.nextInt(allPhases.size());
							env.setRelation(allPhases.get(phaseNum), plug);

							plugBundle.add(plug);
						}
					}

				}
				/*
				   else if (platform.equalsIgnoreCase("DUO")){

					   for( TO<?> ph : allPhases ){
						   //each phase gets three plugs
						   for( int i = 0; i < 3; i++ ){
							   TO<?> plug = env.createObject("PLUG");
							   env.setPropertyValue(plug, "pId", ((Integer) plug.getID()).longValue());
							   env.setRelation(ph, plug);
							   plugBundle.add( plug );
						   }
					   }
				   }
				   */

			}
//            }
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			env.configurationComplete();
		}

		//return results;
		return (findPowerItems(drawing));
	}


	public static List<SimBundle> findPowerItems(TO<?> drawing) throws Exception {
		Environment env = getEnv();
		List<SimBundle> results = new ArrayList<SimBundle>();
		SimBundle<TO<?>> phaseBundle = new SimBundle<TO<?>>("phase", new ArrayList<TO<?>>());
		SimBundle<TO<?>> phaseDeltaBundle = new SimBundle<TO<?>>("phase_delta", new ArrayList<TO<?>>());
		SimBundle<TO<?>> duoPhaseBundle = new SimBundle<TO<?>>("phase_duo", new ArrayList<TO<?>>());
		SimBundle<TO<?>> duoPhaseDeltaBundle = new SimBundle<TO<?>>("phase_duo_delta", new ArrayList<TO<?>>());
		SimBundle<TO<?>> plugBundle = new SimBundle<TO<?>>("plug", new ArrayList<TO<?>>());
		SimBundle<TO<?>> rpduBundle = new SimBundle<TO<?>>("rpdu", new ArrayList<TO<?>>());
		results.add(phaseBundle);
		results.add(phaseDeltaBundle);
		results.add(plugBundle);
		results.add(duoPhaseBundle);
		results.add(duoPhaseDeltaBundle);
		results.add(rpduBundle);


		Collection<TO<?>> powerRacks = env.getRelatedObjects(drawing, "POWER_RACK", true);
		for (TO<?> rack : powerRacks) {
			String rackName = env.getPropertyValue(rack, "name", String.class);
			String platform = env.getPropertyValue(rack, "platform", String.class);

			Collection<TO<?>> rpdus = env.getRelatedObjects(rack, "RPDU", true);
			for (TO<?> r : rpdus) {
				rpduBundle.addAtom(new SimBundleAtom(rackName, env.getPropertyValue(r, "rId", Integer.class).toString()));
				rpduBundle.add(r);
				boolean isDelta = ( env.getPropertyValue( r, "type", Integer.class ) == 1 );
				Collection<TO<?>> phases = env.getRelatedObjects(r, "PHASE", true);
				for (TO<?> phaseTo : phases) {
					//FINALLY!  We have some phases
					SimBundleAtom atom = new SimBundleAtom(rackName, env.getPropertyValue(phaseTo, "name", String.class));
					if (platform.equalsIgnoreCase("SC")) {
						if( isDelta ) {
							phaseDeltaBundle.addAtom(atom);
							phaseDeltaBundle.add(phaseTo);
						} else {
							phaseBundle.addAtom(atom);
							phaseBundle.add(phaseTo);
						}
						Collection<TO<?>> plugs = env.getRelatedObjects(phaseTo, "PLUG", true);
						for (TO<?> plg : plugs) {
							plugBundle.addAtom(new SimBundleAtom(rackName, env.getPropertyValue(plg, "pId", Long.class).toString()));
							plugBundle.add(plg);
						}

					} else if (platform.equalsIgnoreCase("DUO")) {
						if( isDelta ) {
							duoPhaseDeltaBundle.addAtom(atom);
							duoPhaseDeltaBundle.add(phaseTo);
						} else {
							duoPhaseBundle.addAtom(atom);
							duoPhaseBundle.add(phaseTo);
						}
					}
				}
			}
		}

		return results;
	}


	private static Environment env;

	private static Environment getEnv() {
		return env;
	}


	/**
	 * Call this before anything else.
	 *
	 * @param newEnv
	 */
	public static void setEnv(Environment newEnv) {
		env = newEnv;
	}
}
