package DeploymentLab.Examinations

import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLObject

/**
 * Provides the core functionality of several unit tests which can also be run on the current object model directly.
 * @author Gabriel Helman
 * @since Juptier 2
 * Date: 7/5/12
 * Time: 10:25 AM
 */
class ComponentExaminations {

	@Examination
	public static void checkProducerPoint( DLComponent item ){
		//println "checkProducerPoint"
		String t = item.getType()
		//println "Testing $t"

		if ( ! item.hasManagedObjectOfType("producerpoint") ){
			return
		}

		def points = item.getManagedObjectsOfType("producerpoint")
		points.each{ p ->
			assert p.getObjectProperty("name").getValue() != null;
			assert p.getObjectProperty("status").getValue() == 1
			String fieldname = p.getObjectProperty("fieldname").getValue()
			DLObject parent = p.getParents().first()
			assert parent.hasObjectProperty(fieldname)
		}
	}

	/**
	 * Checks that all variables exist and can work
	 * @param c the component to check
	 */
	@Examination
	public static checkVarbindings(DLComponent c) {
		//println "checkVarbindings"
		c._varBindings.each{ vb ->
			vb.listVariables().each{ varname ->
				assert c.hasProperty(varname)
			}
		}
	}

	@Examination
	public static checkNameAndConfig(DLComponent c){
		//println "checkNameAndConfig"
		def defaultName = c.getModel().getTypeDefaultValue(c.getType(), "name")

		if(!defaultName.equals(c.getPropertyStringValue("configuration"))){
			println "$c has inconsistant default name and configuration properties: '$defaultName' vs '${c.getPropertyStringValue("configuration")}'"
		}

		//assert defaultName.equals(c.getPropertyStringValue("configuration"))
	}


	@Examination
	public static lookForSensorNames(DLComponent item){
		String t = item.getType()
		//println "Examine $t"
		def roots = item.getRoots()
		def managed = item.getManagedObjects()

		for ( DLObject r : roots) {
			if ( r.getType().equals('wsnnode')){
				List<DLObject> kids = r.getChildren().findAll {it.getType().equals("wsnsensor")}
				List names = []
				kids.each{ names.add(it.getName()) }

				List<String> namesAndChannel = []
				kids.each{ namesAndChannel.add( "${it.getObjectProperty("channel").getValue()}-${it.getName()}" ) }

				Set uniqueNames = new HashSet(names)
				if ( uniqueNames.size() != names.size() ){
					println "$t has non-unique sensor names for node ${r.getName()}"
				} //else {
//					println "$t names are good"
//				}

				Set uniqueNamesAndChannel = new HashSet(namesAndChannel)
				if ( uniqueNamesAndChannel.size() != namesAndChannel.size() ){
					println "$t has non-unique sensor name and channel combination for node ${r.getName()}"
				}
				assert uniqueNamesAndChannel.size() == namesAndChannel.size()
			}
		}
	}


	@Examination
	public static lookForModbusNames(DLComponent item){
		String t = item.getType()
		def roots = item.getRoots()
		def managed = item.getManagedObjects()

		for ( DLObject r : roots) {
			if ( r.getType().equals('modbusdevice')){
				List kids = r.getChildren()
				//List names = kids.collect { it.getName() }
				//Set uniqueNames = names.unique()
				List names = []
				kids.each{ names.add(it.getName()) }
				Set uniqueNames = new HashSet(names)
				if ( uniqueNames.size() != names.size() ){
					println "$t has non-unique modbusproperty names for node ${r.getName()}"
				} //else {
//					println "$t names are good"
//				}
				assert uniqueNames.size() == names.size()
			}
		}
	}

}
