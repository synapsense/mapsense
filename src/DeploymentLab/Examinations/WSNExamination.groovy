package DeploymentLab.Examinations

import DeploymentLab.Model.*

/**
 * Provides the core functionality of the wsn model unit tests which can also be run on the current object model directly.
 * User: ghelman
 * Date: 2/22/11
 * Time: 5:18 PM
 * @see test.WSNObjectTest
 * @see test.ModbusObjectTest
 */
class WSNExamination {

	static List<Integer> wallPoweredPlatformIds = [16,51,52,80]
	static Map<Integer,String> platformMap = [ 10: "Fusion", 11:"ThermaNode", 12:"Pressure Node", 13:"Pressure Node 2", 14:"Constellation", 15:"Constellation 2", 16: "Enthalpy Node", 17:"ThermaNode 2", 32:"Subfloor Node", 51:"P³ SmartPlug", 52:"SmartLink", 80:"Damper Controller", 20:"ThermaNode DX", 82:"ThermaNode EZ", 92:"ThermaNode EZ-H" ]
	static List<String> acceptableNames = [ "Node RACKNAME", "Exhaust RACKNAME", "Intake RACKNAME", "RACKNAME Supply", "RACKNAME Return", "RACKNAME" ]

	//special cases
	static List<String> wallPoweredConst2 = [ "leakdetector-constellation2", "ct-constellation2", "outdoor_temp_humidity" ]
	static List<Integer> smartSendPlatforms = [11, 17, 51, 52]
	static List<Integer> smartSend2Platforms = [32,82,92]
	static List<String> smartSendExceptions = ["37li-thermanode"]

	//static List<String> allowedDeltas = ["dualflow-enthalpy-constellation2", "singleflow-enthalpy-constellation2"]
	static List<Integer> allowedDeltaSensors = [31,33]

	static List<String> nullProps = ["desc","nodeNwkVersion","nodeOSVersion","nodeAppVersion","program","route","checksum","identify","reset","wakeup"]
	static List<Integer> hasCenterRackPlatform = [51,52]
	static List<Integer> sensorsWithRanges = [1,2,5,27,36,43,44]
	static List<String> typesToIgnore = ["test-tnode2", "rack-interior-fusion", "crah-dual-fusion"] //, "generic-constellation2"]
	static List<Integer> scaledSensors = [9, 12,20, 28,30,31,33,34,35,37,100000]
	static List<Integer> neverScaledChannel = [0,1,2]
	static List<Integer> scaledSensorsOnConst = [2,27]

	static List<Integer> dontScaleMeBro = [32]

	static List<String> noSmartSendSensorNames = ["subfloor", "side", "supply", "return", "end cap"]
	static List<String> stateChangeCountComponents = ["energy-constellation2-displayed", "energy-constellation2", "generic-constellation2"]

	static List<String> noRHLayerThermaNodes = ["pipe-temp-thermanode", "reftemp-thermanode", "standalone-thermanode"]

	static List<Integer> platformsWithHumiditySensors = [11,17,20,92]
	static List<String> thermanodeNoHumidityRange = ["37li-thermanode", "control-verticalstring-li-thermanode", "crah-dual-probe-thermanode", "crah-dual-thermanode", "crah-single-plenumrated-thermanode", "crah-thermanode", "pipe-temp-thermanode", "reftemp-thermanode", "standalone-thermanode" ]

    static List<Integer> platformsWithTwoBatteries = [32,82,92]

	public static void checkNodes( DLComponent rack ){

		String t = rack.getType()
		//println "testing $t"

		if ( typesToIgnore.contains(t) ){
			return
		}

			//having set all that, begin checking the nodes:
			List<DLObject> nodes = rack.getObjectsOfType("wsnnode")

			for ( DLObject n : nodes ){

				//name
				//node names can be fancy
/*
				if ( ! acceptableNames.contains( n.getObjectProperty("name").getValue() ) ){
					println "$t -- node name is '${ n.getObjectProperty("name").getValue()}'"
				}
*/
				assert n.getObjectProperty("name").getValue() != "" && n.getObjectProperty("name").getValue() != null



				//location
				assert n.getObjectProperty("location").getValue() == ""

				//desc
				//in null list

/*
				//mac and logical id check
				assert n.getObjectProperty("mac").getValue() == Long.parseLong("01acc6eb130000d1", 16)

				//assert n.getObjectProperty("id").getValue() == Long.parseLong("c618", 16)
				if ( n.getObjectProperty("id").getValue() != Long.parseLong("c618", 16) ){
					if ( n.getObjectProperty("id").getValue() == null ){
						println "$t -- NULL LID"
					} else {
						println "$t -- BAD LID ${ Long.toHexString( n.getObjectProperty("id").getValue()) }"
					}
				}
*/

				//platformId
				//platformName
				int platformId = n.getObjectProperty("platformId").getValue()
				assert platformId >= 0
				if( platformId == 51 ) {
					// Effing P3 SmartPlug UTF character breaks sometimes, so treat it special cuz I'm tired of fixing it. It goes away in v7.0 anyway.
					assert n.getObjectProperty("platformName").getValue().startsWith('P')
					assert n.getObjectProperty("platformName").getValue().endsWith(' SmartPlug')
				} else {
					if ( n.getObjectProperty("platformName").getValue() != platformMap[ platformId ] ){
						println "$t -- id ${n.getObjectProperty("platformId").getValue()}, platform name is '${n.getObjectProperty("platformName").getValue()}'"
					}
					assert n.getObjectProperty("platformName").getValue() == platformMap[ platformId ]
				}

				//x
				//y
				//TODO: better way to check node & sensor locations
				assert n.getObjectProperty("x").getValue() != null
				assert n.getObjectProperty("y").getValue() != null

				//period & sample_interval
				if ( rack.hasProperty("sample_interval") ){

					if ( n.getObjectProperty("period").getValue() == null ){
						println "$t period is null"
					}

					assert n.getObjectProperty("period") != null
					assert n.getObjectProperty("period").getValue().matches( "\\d+ min" )

					/*
					if ( rack.getComponentProperty("sample_interval").editable ){
						assert n.getObjectProperty("period").getValue().matches( "\\d+ min" )
					} else {
						assert n.getObjectProperty("period").getValue() == null || n.getObjectProperty("period").getValue().matches( "\\d+ min" )
					}
					*/

				} else {
					assert n.getObjectProperty("period").getValue() == null
				}


				//status
				assert n.getObjectProperty("status").getValue() == 0
				//assert n.getObjectProperty("period").getValue().equals( "5 min" )

				//nodeNwkVersion
				//nodeOSVersion
				//nodeAppVersion
				//program
				//route
				//properties that should be null
				for ( String npn : nullProps ){
					//println npn
					assert n.getObjectProperty(npn).getValue() == null || n.getObjectProperty(npn).getValue() == ""
				}

				//batteryOperated
				//batteryCapacity
				//batteryStatus
				if ( wallPoweredPlatformIds.contains( n.getObjectProperty("platformId").getValue() ) ||  (n.getObjectProperty("platformId").getValue() == 15 && wallPoweredConst2.contains(t)) ) {
					//if wall-powered

					//we're going to have a bunch of fails here, so I want a report, not asserts:
					if ( n.getObjectProperty("batteryOperated").getValue() != 0 ){
						println "$t -- batteryOperated is ${n.getObjectProperty("batteryOperated").getValue()}, should be 0"
					}
					/*
					//we don't care about capacity for wall-powered nodes
					if ( n.getObjectProperty("batteryCapacity").getValue() != 5600.0 ){
						println "$t -- batteryCapacity is ${n.getObjectProperty("batteryCapacity").getValue()}, should be 5600.00"
					}
					*/
					assert n.getObjectProperty("batteryStatus").getValue() == 0

				} else {
					//if battery powered
					assert n.getObjectProperty("batteryOperated").getValue() == 1

                    if( platformsWithTwoBatteries.contains( n.getObjectProperty("platformId").getValue() ) ) {
                        assert n.getObjectProperty("batteryCapacity").getValue() == 2400.0
                        assert n.getObjectProperty("batteryStatus").getValue() == 0
                    } else {
                        assert n.getObjectProperty("batteryCapacity").getValue() == 5600.0
                        assert n.getObjectProperty("batteryStatus").getValue() == 0
                    }
				}


	            //check the sensors
				Map<Integer,Integer> channels = [:]
				for ( DLObject sensor : n.getObjectProperty("sensepoints").getChildren().sort{ it.getObjectProperty("channel").getValue() } ){
					assert sensor.getType() == "wsnsensor"

					assert sensor.getObjectProperty("name").getValue().length() > 0
					assert sensor.getObjectProperty("desc").getValue() == null ||  sensor.getObjectProperty("desc").getValue() == ""
					assert sensor.getObjectProperty("status").getValue() == 1

					int sensorType = sensor.getObjectProperty("type").getValue()
					assert sensorType > 0

					int channel = sensor.getObjectProperty("channel").getValue()
					assert channel >= 0
					if (! channels[channel] ){
					    channels[channel] = 1
					} else {
						channels[channel] ++
					}

					channels.each{ k, v ->
						assert v == 1
					}

					//delta
					Integer delta = sensor.getObjectProperty("delta").getValue()

					//if ( ! allowedDeltas.contains(t) && delta != null ) {
					if ( ! allowedDeltaSensors.contains(sensorType) && delta != null ) {
						println "$t sensor channel $channel has delta when it shouldn't."
					}

					if ( allowedDeltaSensors.contains(sensorType) && delta == null ) {
						println "$t sensor channel $channel has null delta when it should have something."
					}

					//subsamples
					if ( (sensorType == 27 || sensorType == 36 || sensorType == 44) && smartSendPlatforms.contains(platformId)  && !smartSendExceptions.contains(t) ){


						if (sensor.getObjectProperty("subSamples").getValue() != 30){
							println "$t sensor channel $channel has subSamples ${sensor.getObjectProperty("subSamples").getValue()}, should be 30"
						}
						assert sensor.getObjectProperty("subSamples").getValue() == 30
                    } else if( (sensorType == 36 || sensorType == 44 || sensorType == 29) && smartSend2Platforms.contains(platformId) ) {
                        if (sensor.getObjectProperty("subSamples").getValue() != 15){
                            println "$t sensor channel $channel has subSamples ${sensor.getObjectProperty("subSamples").getValue()}, should be 15"
                        }
						assert sensor.getObjectProperty("subSamples").getValue() == 15
					} else {

						if (sensor.getObjectProperty("subSamples").getValue() != null){
							println "$t sensor channel $channel has subSamples ${sensor.getObjectProperty("subSamples").getValue()}, should be null"
						}
						assert sensor.getObjectProperty("subSamples").getValue() == null
					}

					//scaleMin
					//scaleMax

					if ( n.getObjectProperty("platformId").getValue() != 15 && n.getObjectProperty("platformId").getValue() != 16 && channel < 3 ){
						assert sensor.getObjectProperty("scaleMin").getValue() == null
						assert sensor.getObjectProperty("scaleMax").getValue() == null
					}


					if ( neverScaledChannel.contains(channel) ){
						assert sensor.getObjectProperty("scaleMin").getValue() == null
						assert sensor.getObjectProperty("scaleMax").getValue() == null

					//} else if ( n.getObjectProperty("platformId").getValue() != 15 && n.getObjectProperty("platformId").getValue() != 16 && ! scaledSensors.contains(sensorType) ){
					} else if ( n.getObjectProperty("platformId").getValue() == 15 && scaledSensorsOnConst.contains(sensorType) ){

						assert sensor.getObjectProperty("scaleMin").getValue() != null //?

						assert sensor.getObjectProperty("scaleMax").getValue() != null


					} else if ( ! scaledSensors.contains(sensorType)  ){
						//assert sensor.getObjectProperty("scaleMin").getValue() == null
						//assert sensor.getObjectProperty("scaleMax").getValue() == null

						if ( sensor.getObjectProperty("scaleMin").getValue() != null ){
							println "$t sensor channel $channel (type $sensorType) has scaleMin of ${sensor.getObjectProperty("scaleMin").getValue()}, should be null"
						}
						if ( sensor.getObjectProperty("scaleMax").getValue() != null ){
							println "$t sensor channel $channel (type $sensorType) has scaleMax of ${sensor.getObjectProperty("scaleMax").getValue()}, should be null"
						}


					} else {
						//assert sensor.getObjectProperty("scaleMin").getValue() != null
						//assert sensor.getObjectProperty("scaleMax").getValue() != null

						if ( sensor.getObjectProperty("scaleMin").getValue() == null ){
							println "$t sensor channel $channel has scaleMin of ${sensor.getObjectProperty("scaleMin").getValue()}, should be a value"
						}
						if ( sensor.getObjectProperty("scaleMax").getValue() == null ){
							println "$t sensor channel $channel has scaleMax of ${sensor.getObjectProperty("scaleMax").getValue()}, should be a value"
						}

					}


					//lastValue
					assert sensor.getObjectProperty("lastValue").getValue() == -5000
					//if ( sensor.getObjectProperty("lastValue").getValue() != -5000 ){
					//	println "$t sensor channel $channel has a lastValue of ${ sensor.getObjectProperty("lastValue").getValue() }"
					//}


					//min
					//max
/*
					if ( sensor.getObjectProperty("scaleMin").getValue() != null ){
						assert sensor.getObjectProperty("min").getValue() <= sensor.getObjectProperty("scaleMin").getValue()
					}
					if ( sensor.getObjectProperty("scaleMax").getValue() != null ){
						assert sensor.getObjectProperty("max").getValue() >= sensor.getObjectProperty("scaleMax").getValue()
					}
*/


					//just ashrae scales:
					//rMin
					//rMax
					//aMin
					//aMax

					switch (sensorType){
						case 1:
							//internal temp
							//assert sensor.getObjectProperty("aMin").getValue() == 59.0
							//assert sensor.getObjectProperty("aMax").getValue() == 90.0
							//assert sensor.getObjectProperty("rMin").getValue() == 64.4
							//assert sensor.getObjectProperty("rMax").getValue() == 80.6

							/*

							if (sensor.getObjectProperty("aMin").getValue() != 59.0){
								println "$t sensor channel $channel has aMin ${sensor.getObjectProperty("aMin").getValue()}, should be 59.0"
							}
							if (sensor.getObjectProperty("aMax").getValue() != 90.0){
								println "$t sensor channel $channel has aMax ${sensor.getObjectProperty("aMax").getValue()}, should be 90.0"
							}
							if (sensor.getObjectProperty("rMin").getValue() != 64.4){
								println "$t sensor channel $channel has rMin ${sensor.getObjectProperty("rMin").getValue()}, should be 64.4"
							}
							if (sensor.getObjectProperty("rMax").getValue() != 80.6){
								println "$t sensor channel $channel has rMax ${sensor.getObjectProperty("rMax").getValue()}, should be 80.6"
							}
							*/
							/*
							if ( sensor.getObjectProperty("aMin").getValue() != null ){
								println "$t sensor channel $channel of type $sensorType has aMin ${sensor.getObjectProperty("aMin").getValue()}"
							}
							*/

							//still an open question



							break
						case 2:
							//internal rH

							if ( ! platformsWithHumiditySensors.contains( platformId ) || thermanodeNoHumidityRange.contains(t) ){
								assert sensor.getObjectProperty("aMin").getValue() == null
								assert sensor.getObjectProperty("aMax").getValue() == null
								assert sensor.getObjectProperty("rMin").getValue() == null
								assert sensor.getObjectProperty("rMax").getValue() == null
							} else {

							//assert sensor.getObjectProperty("aMin").getValue() == 20.0
							//assert sensor.getObjectProperty("aMax").getValue() == 80.0
							//assert sensor.getObjectProperty("rMin").getValue() == 40.0
							//assert sensor.getObjectProperty("rMax").getValue() == 55.0



								if (sensor.getObjectProperty("aMin").getValue() != 20.0){
									println "RH:  $t sensor channel $channel of type $sensorType has aMin ${sensor.getObjectProperty("aMin").getValue()}, should be 20.0"
								}
								if (sensor.getObjectProperty("aMax").getValue() != 80.0){
									println "RH:  $t sensor channel $channel of type $sensorType has aMax ${sensor.getObjectProperty("aMax").getValue()}, should be 80.0"
								}
								if (sensor.getObjectProperty("rMin").getValue() != 40.0){
									println "RH:  $t sensor channel $channel of type $sensorType has rMin ${sensor.getObjectProperty("rMin").getValue()}, should be 40.0"
								}
								if (sensor.getObjectProperty("rMax").getValue() != 55.0){
									println "RH:  $t sensor channel $channel of type $sensorType has rMax ${sensor.getObjectProperty("rMax").getValue()}, should be 55.0"
								}

							}

							break

						case 5:
						case 43:
							//battery
						/*
							if (sensor.getObjectProperty("aMin").getValue() != 2.5){
								println "$t sensor channel $channel has aMin ${sensor.getObjectProperty("aMin").getValue()}, should be 2.5"
							}
							if (sensor.getObjectProperty("aMax").getValue() != 3.7){
								println "$t sensor channel $channel has aMax ${sensor.getObjectProperty("aMax").getValue()}, should be 3.7"
							}
							if (sensor.getObjectProperty("rMin").getValue() != 2.7){
								println "$t sensor channel $channel has rMin ${sensor.getObjectProperty("rMin").getValue()}, should be 2.7"
							}
							if (sensor.getObjectProperty("rMax").getValue() != 3.7){
								println "$t sensor channel $channel has rMax ${sensor.getObjectProperty("rMax").getValue()}, should be 3.7"
							}
							break
                        */
						//actually, we don't care about the battery
						break;

						case 27:
						case 36:
						case 44:
							// intake / cold side thermistors

						if ( t.equalsIgnoreCase("dual-horizontal-row-thermanode2") && n.getName().contains("Exhaust") ){
							assert sensor.getObjectProperty("aMin").getValue() == null
							assert sensor.getObjectProperty("aMax").getValue() == null
							assert sensor.getObjectProperty("rMin").getValue() == null
							assert sensor.getObjectProperty("rMax").getValue() == null

						} else if (sensor.getObjectProperty("name").getValue().contains("cold") || sensor.getObjectProperty("name").getValue().contains("channel") || sensor.getObjectProperty("name").getValue().contains("intake") ){

								if (sensor.getObjectProperty("aMin").getValue() != 59.0){
									println "$t sensor channel $channel of type $sensorType has aMin ${sensor.getObjectProperty("aMin").getValue()}, should be 59.0"
								}
								if (sensor.getObjectProperty("aMax").getValue() != 90.0){
									println "$t sensor channel $channel of type $sensorType has aMax ${sensor.getObjectProperty("aMax").getValue()}, should be 90.0"
								}
								if (sensor.getObjectProperty("rMin").getValue() != 64.4){
									println "$t sensor channel $channel of type $sensorType has rMin ${sensor.getObjectProperty("rMin").getValue()}, should be 64.4"
								}
								if (sensor.getObjectProperty("rMax").getValue() != 80.6){
									println "$t sensor channel $channel of type $sensorType has rMax ${sensor.getObjectProperty("rMax").getValue()}, should be 80.6"
								}

							assert sensor.getObjectProperty("aMin").getValue() == 59.0
							assert sensor.getObjectProperty("aMax").getValue() == 90.0
							assert sensor.getObjectProperty("rMin").getValue() == 64.4
							assert sensor.getObjectProperty("rMax").getValue() == 80.6


						} else {

								if (sensor.getObjectProperty("aMin").getValue() != null){
									println "$t sensor channel $channel of type $sensorType has aMin ${sensor.getObjectProperty("aMin").getValue()}, should be null; name is ${sensor.getObjectProperty("name").getValue()}"
								}
								if (sensor.getObjectProperty("aMax").getValue() != null){
									println "$t sensor channel $channel of type $sensorType has aMax ${sensor.getObjectProperty("aMax").getValue()}, should be null; name is ${sensor.getObjectProperty("name").getValue()}"
								}
								if (sensor.getObjectProperty("rMin").getValue() != null){
									println "$t sensor channel $channel of type $sensorType has rMin ${sensor.getObjectProperty("rMin").getValue()}, should be null; name is ${sensor.getObjectProperty("name").getValue()}"
								}
								if (sensor.getObjectProperty("rMax").getValue() != null){
									println "$t sensor channel $channel of type $sensorType has rMax ${sensor.getObjectProperty("rMax").getValue()}, should be null; name is ${sensor.getObjectProperty("name").getValue()}"
								}

								assert sensor.getObjectProperty("aMin").getValue() == null
								assert sensor.getObjectProperty("aMax").getValue() == null
								assert sensor.getObjectProperty("rMin").getValue() == null
								assert sensor.getObjectProperty("rMax").getValue() == null

							}
							break

						default:
							//nothing else has ranges
							assert sensor.getObjectProperty("aMin").getValue() == null
							assert sensor.getObjectProperty("aMax").getValue() == null
							assert sensor.getObjectProperty("rMin").getValue() == null
							assert sensor.getObjectProperty("rMax").getValue() == null
					}





					//x
					assert sensor.getObjectProperty("x").getValue() != null
					//y
					assert sensor.getObjectProperty("y").getValue() != null
					//z
					assert sensor.getObjectProperty("z").getValue() != null
					if ( sensorType == 5 || sensorType == 43 ){
						assert sensor.getObjectProperty("z").getValue() == 0
					} else if ( channel == 1 && sensorType == 2 && (platformId == 11 || platformId == 17) && ! t.contains("crah") && ! noRHLayerThermaNodes.contains(t) ){
						//assert sensor.getObjectProperty("z").getValue() == 131072
						if ( sensor.getObjectProperty("z").getValue() != 131072 ){
							println "$t sensor channel $channel of type $sensorType has z-layer of ${sensor.getObjectProperty("z").getValue()} (should be 131072)"
						}

					}


					//dataclass
					Integer dataclass =  sensor.getObjectProperty("dataclass").getValue()
					assert dataclass != null

					if (sensorType == 1 || sensorType == 27 || sensorType == 36 || sensorType == 33 || sensorType == 39 || sensorType == 44){
					    assert dataclass == 200
						//if (dataclass != 200){
						//	println "$t sensor channel $channel dataclass should be 200, is $dataclass"
						//}
					} else if ( sensorType == 2 ){
						//assert dataclass == 201
						if ( dataclass != 201 ){
							println "$t sensor channel $channel of type $sensorType has dataclass of $dataclass (should be 201)"
						}

					} else if ( sensorType == 24 ){
						assert dataclass == 202

					} else if ( sensorType == 5 || sensorType == 43 ){
						assert dataclass == 210

					} else if ( sensorType == 31 ){
						assert dataclass == 209

					} else if ( sensorType == 32 || sensorType == 20 ){
						assert dataclass == 207

					} else if ( sensorType == 30 ){
						assert dataclass == 206

					} else if ( sensorType == 9 ){
						assert dataclass == 211

					} else if ( sensorType == 12 ){
						assert dataclass == 213

					} else if ( sensorType == 29 ){
						assert dataclass == 202

					} else if ( sensorType == 34 ){
						assert dataclass == 999

					} else if ( sensorType == 35 ){
						assert dataclass == 211 || dataclass == 999

					} else if (sensorType == 40){
						assert dataclass == 200

					} else if ( sensorType == 100000 ){
						assert dataclass == 214

					} else {
						println "$t sensor channel $channel has spooky dataclass of $dataclass on st $sensorType"
					}


					//enabled
					if ( t.equals("generic-constellation2") && channel > 2 ){
						assert sensor.getObjectProperty("enabled").getValue() == 0

					} else if ( !t.contains("constellation") && channel == 10 ) {
						//is a door sensor on not a const2
						assert sensor.getObjectProperty("enabled").getValue() == 0

					} else {
						assert sensor.getObjectProperty("enabled").getValue() == 1
					}


					//smartSendThreshold
					//smart send goes on thermanode thermistors that are not in the subfloor or chimney
					if ( (sensorType == 27 || sensorType == 36 || sensorType == 44) && !smartSendExceptions.contains(t) && smartSendPlatforms.contains(platformId) && ! sensor.getObjectProperty("name").getValue().contains("subfloor") && ! sensor.getObjectProperty("name").getValue().contains("supply") && ! sensor.getObjectProperty("name").getValue().contains("return") && ! sensor.getObjectProperty("name").getValue().contains("side") && ! sensor.getObjectProperty("name").getValue().contains("end cap") ){
						//assert sensor.getObjectProperty("smartSendThreshold").getValue() > 0

						if (sensor.getObjectProperty("smartSendThreshold").getValue() == null  || sensor.getObjectProperty("smartSendThreshold").getValue()  <= 0){
							println "$t sensor channel $channel has smartSendThreshold ${sensor.getObjectProperty("smartSendThreshold").getValue()}, should be >0"
						}

						if (sensor.getObjectProperty("smartSendThreshold").getValue() > 10 ){
							println "$t sensor channel $channel has smartSendThreshold ${sensor.getObjectProperty("smartSendThreshold").getValue()}, that's much too big"
						}


					} else {
						//assert (sensor.getObjectProperty("smartSendThreshold").getValue() == null || sensor.getObjectProperty("smartSendThreshold").getValue() == 0 )

						if (sensor.getObjectProperty("smartSendThreshold").getValue() != null  && sensor.getObjectProperty("smartSendThreshold").getValue()  > 0){
							println "$t sensor channel $channel has smartSendThreshold ${sensor.getObjectProperty("smartSendThreshold").getValue()}, should be null or 0"
						}


					}



					//mode
					String mode = sensor.getObjectProperty("mode").getValue()
					if ( platformId == 15) {
						if (channel == 6 || channel == 8) {

							//if((delta / 100) & 0x7fff == 0) {
/*
							if ( delta >= 0 ){
								//assert sensor.getObjectProperty("mode").getValue() == "edge_detect"
								if ( mode != "edge_detect" ){
									println "$t sensor channel $channel mode should be 'edge_detect', is $mode"
								}
							} else {
								assert mode == "state_change_count"
							}
*/

							if ( stateChangeCountComponents.contains(t) ){
								assert mode == "state_change_count"
							} else {
								assert mode == "edge_detect"
							}


						} else {
							//assert mode == "averaging"

							if (mode != "averaging" ){
								println "$t sensor channel $channel mode should be 'averaging', is ${mode}"
							}
						}
                    } else if( platformId == 20 ) {
                        // ThermaNode DX is special.
                        switch( channel ) {
                            case [ 6, 60, 8, 80 ] :
                                assert mode == "minmax"
                                break
                            default:
                                assert mode == "instant"
                        }
                    } else {
						assert mode == "instant"
						/*
						if (mode != "instant" ){
							println "$t sensor channel $channel mode should be 'instant', is ${mode}"
						}
						*/
					}



					//interval
					if (( mode == "state_change_count" ) || ( mode == "minmax" )) {
						assert sensor.getObjectProperty("interval").getValue() != 0
					} else {
						if ( sensor.getObjectProperty("interval").getValue() == null ){
							//println "$t channel $channel has null interval"
						}
						assert  sensor.getObjectProperty("interval").getValue() == 0 ||  sensor.getObjectProperty("interval").getValue() == null
					}

				}


				//checksum
				///checked for null above
				//leftRack
				assert n.getObjectProperty("leftRack").getReferrent() == null

				//centerRack
				if ( hasCenterRackPlatform.contains(platformId) ){
					assert n.getObjectProperty("centerRack").getReferrent() != null
				} else {
					assert n.getObjectProperty("centerRack").getReferrent() == null
				}

				//rightRack
				assert n.getObjectProperty("rightRack").getReferrent() == null

				//identify
				//reset
				//wakeup
				///checked for null above

			}

	}


	public static void checkModbus( DLComponent mod ){
		String t = mod.getType()
		//println "Testing $t"

		def devices = mod.getManagedObjectsOfType("modbusdevice")
		devices.each{ md ->
			assert md.getObjectProperty("maxpacketlen").getValue() > 0
			assert md.getObjectProperty("name").getValue() != null
		}


		def modbusProperties = mod.getManagedObjectsOfType("modbusproperty")
		modbusProperties.each{ mp ->
			Double pollPeriod = mp.getObjectProperty("lastValue").getTag("poll").getValue()
			//assert pollPeriod == 300000
			if ( pollPeriod != 300000 ){
				println "$t poll should be 300000, is $pollPeriod"
			}
			assert mp.getObjectProperty("id") != null
			assert mp.getObjectProperty("scale") != null
			assert mp.getObjectProperty("type") != null
		}
	}






}
