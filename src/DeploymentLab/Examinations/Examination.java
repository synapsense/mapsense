package DeploymentLab.Examinations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Tags an "Examination", which is really just a unit test method that we want to be able to run via the main app to do "live" sanity checking.
 * The Unit Test grabs all Examinations and runs them.
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/11/12
 * Time: 11:53 AM
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Examination {
}
