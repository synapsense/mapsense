package DeploymentLab.Examinations

import DeploymentLab.Model.*
import DeploymentLab.Image.ComponentIconFactory
import DeploymentLab.CentralCatalogue

/**
 * Provides the core functionality of several unit tests which can also be run on the current object model directly.
 * User: ghelman
 * Date: 4/12/11
 * Time: 9:56 AM
 */
class ObjectExamination {

	public static lookForChildren(DLComponent item){
		String t = item.getType()
		def roots = item.getRoots()
		def managed = item.getManagedObjects()
		for ( DLObject r : roots) {
			if ( r.getType().equals('wsnnode')){
				return
			}
		}
		if ( roots != managed ){
			def kids = []
			managed.each{ m ->
				if ( ! roots.contains(m) ){
					kids += m
				}
			}
			boolean pp = false
			kids.each{ k ->
				if ( k.getType().equals("producerpoint") ){
					pp = true
				}
			}
			if ( pp ){
				println "$t has object children: ${kids.size()} ---> has ProducerPoint"
			} else {
				println "$t has object children: ${kids.size()} "
			}
		}
	}



}
