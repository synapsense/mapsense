package DeploymentLab.Examinations

import edu.umd.cs.piccolo.PCanvas
import DeploymentLab.SceneGraph.ShapeFactory
import DeploymentLab.SceneGraph.ConfigurationNode

import javax.swing.JFileChooser
import java.awt.Point
import java.awt.BorderLayout
import javax.swing.JFrame
import edu.umd.cs.piccolox.swing.PScrollPane
import edu.umd.cs.piccolo.PNode
import java.awt.Dimension

/**
 *
 * @author Gabriel Helman
 * @since
 * Date: 7/24/12
 * Time: 6:15 PM
 */
class ShapeViewer {

	public void go() {
		def scroll = new PScrollPane(new PCanvas())
		def canvas = (PCanvas)scroll.getViewport().getView()

		List<File> shapes = []

		File shapesDir //= new File("shapes")
		JFileChooser j = new JFileChooser();
		j.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		Integer opt = j.showSaveDialog(null);
		if (opt == JFileChooser.APPROVE_OPTION){
			shapesDir = j.getSelectedFile()
		}else{
			return
		}
		shapesDir.listFiles().each{ f ->
			if(f.getAbsolutePath().endsWith("svg")){
				shapes += f
			}
		}

		//def root = canvas.getRoot()
		def root = new PNode()
		//initGridLayer()
		canvas.getLayer().addChild(root)


		def nodePositions = []
		int xSpacing = 110
		int ySpacing = 35//32
		for(int i=0; i<shapesDir.listFiles().size(); i++) {
			int x = 60 + (i%4)*xSpacing
			int y = 10 + ySpacing * i - ySpacing * (i%4)
			nodePositions << new Point(x, y)
		}

		int i = 0
		for(def shapeFile : shapes){
			//if(i > 10){
			//	break
			//}

			Point at = nodePositions[i]

			def shapeName = shapeFile.getName().substring(0,shapeFile.getName().lastIndexOf("."))
			def n = ShapeFactory.getShape(shapeName)
			def node = new ConfigurationNode(shapeName, shapeName, n, at.x, at.y, "", "")
			root.addChild(node)
			i++
		}



		JFrame frame = new JFrame("Shape Viewer");
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(scroll, BorderLayout.CENTER);

		frame.setPreferredSize(new Dimension(300,300))

		frame.pack();
		frame.setVisible(true);
	}

}



