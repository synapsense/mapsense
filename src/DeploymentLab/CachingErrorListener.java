
package DeploymentLab;

import java.util.ArrayList;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.TransformerException;

public class CachingErrorListener implements ErrorListener {
	private ArrayList<TransformerException> warnings;
	private ArrayList<TransformerException> errors;
	private ArrayList<TransformerException> fatalErrors;

	public CachingErrorListener() {
		warnings = new ArrayList<TransformerException>();
		errors = new ArrayList<TransformerException>();
		fatalErrors = new ArrayList<TransformerException>();
	}

	public void warning(TransformerException e) { warnings.add(e); }
	public void error(TransformerException e) { errors.add(e); }
	public void fatalError(TransformerException e) { fatalErrors.add(e); }

	public boolean hasWarnings() { return warnings.size() > 0; }
	public boolean hasErrors() { return errors.size() > 0; }
	public boolean hasFatalErrors() { return fatalErrors.size() > 0; }

	public boolean hasAnyMessages() {
		return hasWarnings() || hasErrors() || hasFatalErrors();
	}

	public ArrayList<TransformerException> getWarnings() { return warnings; }
	public ArrayList<TransformerException> getErrors() { return errors; }
	public ArrayList<TransformerException> getFatalErrors() { return fatalErrors; }
}

