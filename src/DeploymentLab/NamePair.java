package DeploymentLab;

/**
 * Represents the name of something, where there is both a display name and an internal name.
 * This originally came into existence to handle the "field name" selection box in the spotlight search dialog box.
 * Equals, compareTo, and toString are all overloaded and based on the Display Name field, so it will do things like sort and display correctly in pull-down boxes.
 * However, you can still pull the internal name out for internal-type processing. 
 */
public class NamePair implements Comparable<NamePair> {
	private String internal;
	private String display;

	/**
	 * Creates a blank NamePair.  Fields are set to an empty string, not null.
	 */
	public NamePair() {
		this("","");
	}

	/**
	 * Creates a new NamePair with the specified values.
	 * @param _internal the internal name for the new NamePair.
	 * @param _display the display name for the new NamePair.
	 */
	public NamePair( String _internal, String _display ){
		this.internal = _internal;
		this.display = _display;
	}

	public String getInternal() {
		return internal;
	}

	public void setInternal(String internal) {
		this.internal = internal;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	/**
	 * Override of the toString method - just returns the display name.
	 * @return the display name.
	 */
	@Override
	public String toString() {
		return ( display );
	}

	/**
	 * Override of the equals method, based on the display name only.
	 * @param o the object to compare for equality.
	 * @return true if the object being compared to is the same as the display name.
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		NamePair namePair = (NamePair) o;

		if (display != null ? !display.equals(namePair.display) : namePair.display != null) return false;

		return true;
	}

	/**
	 * Override of hashCode based on display name only.
	 * @return the hashCode
	 */
	@Override
	public int hashCode() {
		return display != null ? display.hashCode() : 0;
	}

	/**
	 * Comparison for sorting based on display name only.
	 * @param o another NamePair to compare to.
	 * @return Which namePair is "higher".  The usual for compareTo.
	 */
	@Override
	public int compareTo(NamePair o) {
		return ( this.display.compareTo(o.getDisplay() ) );
	}
}