package DeploymentLab;

import groovy.lang.Closure;


/**
 * Provides a way to hand a task off to the background thread (and trip off the progress pane)
 * Updates to the progress pane message should be handled via the "progress" logger channel from within the task.
 * To get a progress bar on the progress pane, use setMaxProgress() inside the task to set the max to something greater than zero.
 * Increment this from within the task via either bump() or setProgress().  
  */
public class ProgressManager {

	protected static ProgressManager instance;

	protected static DeploymentLabWindow dlw;
	protected static ProgressPane progressPane;

	private ProgressManager() {}

   /**
    * SingletonHolder is loaded on the first execution of Singleton.getInstance()
    * or the first access to SingletonHolder.INSTANCE, not before.
    */
	private static class SingletonHolder {
		public static final ProgressManager INSTANCE = new ProgressManager();
	}

	/**
	 * Gets the instance of ProgressManager, constructing it if needed.
 	 * @return ProgressManager
	 */
	public static ProgressManager getInstance() {
		return SingletonHolder.INSTANCE;
	}

	/**
	 * Connect the ProgressManager to the progressPane itself. Must be called before any other uses.
	 * @param _dlw a reference to the main DeploymentLabWindow
	 */
	public void init(DeploymentLabWindow _dlw){
		dlw = _dlw;
		progressPane = dlw.getProgressPane();
	}

	/**
	 * Executes a given closure as a background task while leaving the EDT to spin the wait ball.
	 * @param task a Groovy closure to execute.
	 */
	public void doBackgroundTask(Closure task){
		if(dlw.backgroundTaskRunning()){
			dlw.waitForBackgroundTask();
		}

		dlw.doBackgroundTask(task);
	}

	public void doSetProgress(Double progressValue){
		//progressPane.setProgress(progressValue);
	}

	public void doSetMaxProgress(Double progressValue){
		//progressPane.setMaxProgress(progressValue);
	}

	public void doBump(){
		//progressPane.bump();
	}

	public void doBump(boolean showNumbers){
		//progressPane.bump(showNumbers);
	}

	////static convenience methods:

	/**
	 * Static convenience method to execute a given closure as a background task while leaving the EDT to spin the wait ball.
	 * @param task a Groovy closure to execute.
	 */
	public static void doTask(Closure task){
		getInstance().doBackgroundTask(task);
	}

	/**
	 * Static convenience method to increment the progress bar by one increment.
	 */
	public static void bump(){
		//getInstance().doBump();
	}

	/**
	 * Static convenience method to increment the progress bar by one increment.
	 * @param showNumbers if True, show the current numerical status of the progressbar below the progress message.
	 */
	public static void bump(boolean showNumbers){
		//getInstance().doBump(showNumbers);
	}


	/**
	 * Static convenience method to set the progress bar's current increment.
	 * @param progressValue an integer value to set the progress bar's progress to.  Should be more than zero and less than the max progress.
	 */
	public static void setProgress(Double progressValue){
		//getInstance().doSetProgress(progressValue);
	}

	/**
	 * Static convenience method to set the max number of progress increments in the progress bar.
	 * @param progressValue The max value of the progress bar.
	 * A value < 1 means that the progressbar will not be displayed when the progress pane is.
	 */
	public static void setMaxProgress(Double progressValue){
		//getInstance().doSetMaxProgress(progressValue);
	}

	/**
	 * Static convenience method to show the progress pane.
	 */
	public static void showProgressPane() {
		getInstance().dlw.showProgressPane();
	}

	/**
	 * Static convenience method to hide the progress pane.
	 */
	public static void hideProgressPane() {
		getInstance().dlw.hideProgressPane();
	}

	/**
	 * Static convenience to check if the background task is running.
	 * @return true if background task is running else false.
     */
	public static boolean isBackgroundTaskRunning() {
		return getInstance().dlw.backgroundTaskRunning();
	}
}
