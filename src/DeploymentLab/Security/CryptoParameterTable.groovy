package DeploymentLab.Security

import java.security.spec.AlgorithmParameterSpec
import java.security.spec.ECFieldFp
import java.security.spec.ECParameterSpec
import java.security.spec.ECPoint
import java.security.spec.EllipticCurve

import org.apache.commons.codec.DecoderException
import org.apache.commons.codec.binary.Hex

/**
 * Provides a lookup table of cryptographic algorithm specifications.
 *
 * @author Ken Scoggins
 * @since  Jupiter
 */
public class CryptoParameterTable {

    /**
     * NOOP. Just a placeholder to keep this class a static implementation.
     */
    private CryptoParameterTable() { }

    /**
     * Return a specification of cryptographic parameters representing the passed in named algorithm.
     *
     * @param name  The name of the cryptographic algorithm.
     *
     * @return  The parameter specification for the named algorithm.
     *
     * @throws IllegalArgumentException  If the provided algorithm name is not found in the table.
     */
    public static AlgorithmParameterSpec getByName( String name ) {
        try {
            // Uh, yeah. Groovy dynamic method calls.
            return this."$name"()
        } catch( MissingMethodException e ) {
            throw new IllegalArgumentException("Unknown cryptographic algorithm name: " + name )
        }
    }


    /**
     * Helper method for converting a hex string to a BigInteger.
     *
     * @param hex  The string representing a hex value.
     *
     * @return  The decoded value.
     */
    protected static BigInteger fromHex( String hex ) {
        try {
            return new BigInteger( 1, Hex.decodeHex( hex.toCharArray() ) )
        } catch( DecoderException e ) {
            throw new IllegalArgumentException( e )
        }
    }


    /**
     * Elliptic Curve Cryptography (ECC) : secp256k1
     *
     * Borrowed from the Bouncy Castle Crypto API implementation org.bouncycastle.asn1.sec.SECNamedCurves
     *
     * @return  The parameter specification for this algorithm.
     */
    private static ECParameterSpec secp256k1() {
        // p = 2^256 - 2^32 - 2^9 - 2^8 - 2^7 - 2^6 - 2^4 - 1
        BigInteger p = fromHex("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F")
        BigInteger a = BigInteger.valueOf(0)
        BigInteger b = BigInteger.valueOf(7)
        BigInteger n = fromHex("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141")
        int        h = 1

        EllipticCurve curve = new EllipticCurve( new ECFieldFp( p ), a, b )

        ECPoint G = new ECPoint( fromHex("79BE667EF9DCBBAC55A06295CE870B07029BFCDB2DCE28D959F2815B16F81798"),
                                 fromHex("483ADA7726A3C4655DA4FBFC0E1108A8FD17B448A68554199C47D08FFB10D4B8") )

        return new ECParameterSpec( curve, G, n, h )
    }
}
