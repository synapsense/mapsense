package DeploymentLab.Security

import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.MessageDigest
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec
import DeploymentLab.Pair
import com.synapsense.dto.BinaryData
import com.synapsense.service.impl.dao.to.BinaryDataImpl
import java.security.SecureRandom
import java.security.spec.AlgorithmParameterSpec

/**
 * Contains methods useful for creating WSN Network public/private key pairs used for network data encryption.
 * 
 * @author Gabriel Helman
 * @since Jupitor
 * Date: 2/8/12
 * Time: 3:52 PM
 */
class NetworkKey {

    public static int KEY_SIZE_BITS  = 256
    public static int KEY_SIZE_BYTES = KEY_SIZE_BITS / 8

	/**
	 * Generate an ECC-256 keypair using the Java7 libraries.
	 * @return a KeyPair holding a 256-bit ECC public-private pair
	 */
	public static KeyPair generateKey(){
        AlgorithmParameterSpec spec = CryptoParameterTable.getByName("secp256k1");
		def gen = KeyPairGenerator.getInstance("EC")
		gen.initialize( spec, new SecureRandom() )
		return gen.generateKeyPair()
	}


	/**
	 * Generates a public/private network keypair for WSN networks.
	 *
	 * Following Yann's spec, the private key is obfuscated as follows:
	 * 256-bit ECC private key, append to that the SHA-256 hash of that key,
	 * then encipher that whole series using AES-128 with a key of all zeroes.
	 *
	 * This needs to be stored in the WSNNetwork object when one is created, but NEVER changed.
	 *
	 * By the time this returns, the two keys are just byte arrays ready to be stored in the model
	 * without further monkey-business.
	 *
	 * Keys go in:
	 * WSNNETWORK.networkPublicKey
	 * WSNNETWORK.networkPrivateKey
	 *
	 * @return a Pair<BinaryData,BinaryData>; a = public key, b = enciphered private key
	 */
	public static Pair<BinaryData,BinaryData> makeNetworkKeys(){

		def kp = DeploymentLab.Security.NetworkKey.generateKey()

        //
        // PUBLIC KEY - Combine the X & Y into a single byte array.
        //
		def xa = toFixedByteArray( kp.getPublic().getW().getAffineX() )
		def ya = toFixedByteArray( kp.getPublic().getW().getAffineY() )

		byte[] allPublic = Arrays.copyOf(xa, xa.length + ya.length);
		System.arraycopy(ya, 0, allPublic, xa.length, ya.length);

        //
        // PRIVATE KEY - Obfuscate with Yann-256
        //
		def pKey = toFixedByteArray( kp.getPrivate().getS() )
		MessageDigest md = MessageDigest.getInstance("SHA-256")
		md.update(pKey)
		def privateHash = md.digest()

		byte[] allPrivate = Arrays.copyOf(pKey, pKey.length + privateHash.length);
		System.arraycopy(privateHash, 0, allPrivate, pKey.length, privateHash.length);

		// Generate the secret key specs.
		byte[] raw = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");

		// Instantiate the cipher
		Cipher cipher = Cipher.getInstance("AES/ECB/NOPADDING");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
		byte[] encrypted = cipher.doFinal(allPrivate);
		
		//println "encry is ${encrypted.length}"

		//and the final results!
		def a = new BinaryDataImpl(allPublic) //public
		def b = new BinaryDataImpl(encrypted) //private
		return new Pair<BinaryData,BinaryData>(a,b)
	}

    public static byte[] toFixedByteArray( BigInteger bigInt, int desiredLength = KEY_SIZE_BYTES ) {

        byte[] orig = bigInt.toByteArray()

        // Nothing to do if it is already the desired length.
        if( orig.length == desiredLength ) {
            return orig
        }

        byte[] result = new byte[desiredLength]

        if( orig.length > desiredLength ) {
            // Need to chop the MSBs.
            System.arraycopy( orig, (orig.length - desiredLength), result, 0, desiredLength )
        } else {
            // Need to pad the MSBs.
            System.arraycopy( orig, 0, result, (desiredLength - orig.length), orig.length )
        }

        return result
    }
}
