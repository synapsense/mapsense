package DeploymentLab.Security;

import java.util.List;
import java.util.Random;

import DeploymentLab.Model.ComponentModel;
import DeploymentLab.Model.DLComponent;
import com.google.common.base.Strings;


/**
 * Generates PAN IDs for WSN components.  All PAN IDs are valid, they can optionally be checked for uniqueness in the current project.
 * @author Gabriel Helman
 * @since Mars
 * Date: 5/23/11
 * Time: 10:45 AM
 */
public class PanIDFactory {

	static Random random;

	private PanIDFactory(){}


	/**
	 * Constructs a new PAN ID in the form of a string holding a hex-representation of the 16-bit value.
	 * PAN IDs are between 1 and 0xfffe.
	 * PAN IDs are computed as pseudo-random number in that range.
	 *
	 * @return a String holding the hex representation of the PAN ID, for example: "DEAD" or "BEEF"
	 */
	private static String makePanID(){
		if ( random == null ){
			random = new Random();
		}

		long result = 0;
		//since nextInt may hand out 0, we make the top of the range one less than we want and add one.
		result = random.nextInt(0xfffd) + 1;
		//return Long.toHexString(result);
		String res = Long.toHexString(result);
		if(res.length() < 4){
			res = Strings.padStart(res,4,'0');
		}
		return res;
	}

	/**
	 * Constructs a new PAN ID in the form of a string holding a hex-representation of the 16-bit value and insures uniqueness.
	 * PAN IDs are between 1 and 0xfffe.
	 * PAN IDs are computed as pseudo-random number in that range.
	 * The value is checked against all other PAN IDs in the provided componentModel to insure uniqueness before returning.
	 *
	 * @param cm the ComponentModel to check for uniqueness across
	 * @return a String holding the PAN ID, for example: "DEAD" or "BEEF"
	 */
	public static String makePanID(ComponentModel cm){
		String result = makePanID();
		while ( ! isUniquePanID(cm,result) ){
			result = makePanID();
		}
		return result;
	}


	/**
	 * Checks a PAN ID for uniqueness.
	 * @param cm the ComponentModel to check across
	 * @param panid the PAN ID to check, in the form of a string holding the hex representation of the id
	 * @return true if the PAN ID is unique
	 */
	public static boolean isUniquePanID(ComponentModel cm, String panid){
		List<DLComponent> nets = cm.getComponentsByType("network");
		for(DLComponent n : nets){
			if ( n.hasProperty("pan_id") ){
				if ( n.getPropertyStringValue("pan_id").equalsIgnoreCase(panid) ){
					return false;
				}
			}
		}
		return true;
	}

}
