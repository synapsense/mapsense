package DeploymentLab.Security;

import DeploymentLab.Model.*
import DeploymentLab.channellogger.Logger

/**
 * Class to wrap all the logic around generating a logical ID for a given DLComponent based on its MAC ID.
 * 
 */
public class LogicalId {
	private static final Logger log = Logger.getLogger(LogicalId.class.getName())
	private ComponentModel cmodel
	private ObjectModel omodel
	static CRC16 hash = new CRC16()

	public LogicalId(ComponentModel _cm, ObjectModel _om){
		cmodel = _cm
		omodel = _om
	}


	/**
	 * Generate a unique logical ID based on a given mac id.
	 *
	 * All Nodes with a MAC ID MUST have a LID.
	 *
	 * The Node MAC ID is converted to a string containing the value in hex.
	 * The value is left-padded with "0"s to a length of 16 hex characters.
	 * The CRC16 is then computed for that string.
	 * The resulting 2-byte value is then checked to make sure that it is unique across the deployment plan,
	 * and not greater than or equal to F000 or equal to 0.
	 * In the case of an invalid logical id, the mac id is appended to the end of the string representation again
	 * and the CRC16 recalculated.  This is repeated until a valid number is reached.
	 *
	 * Special cases:
	 * MAC IDs of 0 result in a LID of 0.
	 * Two nodes with the same MAC ID don't count as a collision; they will receive the same LID.
	 *
	 * @param mac The Mac ID to hash based on.
	 * @return the logical id computed from the given mac id
	 */
	public Integer generateLogicalId( String mac, DLComponent source ){
		//a slight shortcut - a mac id of 0 is never valid, and really just means a new component has been created
		//we can skip all this work, because who cares?
		Long longmac = Long.parseLong(mac, 16)
		if ( longmac == 0 ){
			//log.trace("Not generating a logical ID for a MAC ID of 0.")
			return 0
		}

		//log.trace("Generating a Logical Id for $source based on $mac ") //in network $wsn")

		//println "making a logical based on $mac"
		Integer logical = 0
		String macToUse = ""
		boolean valid = false
		String treatedMac = mac.toLowerCase()
		if ( treatedMac.length() < 16  ){
			//pad out to sixteen chars
			treatedMac = treatedMac.padLeft(16, "0")
		}

		while (! valid){
			macToUse += treatedMac
			//generate the CRC 16
			hash.reset()
			hash.update(macToUse)
			logical = hash.getValue()
//			println "generation: current hash is ${Integer.toHexString(logical)} for $macToUse"
			if (logical >= 0xF000){
            	//print "TOO HIGH!"
            	continue
			} else if (logical == 0){
				continue
			} else if ( this.logicalIdIsADupe(logical, source, LidType.NODE, longmac) ) {
				//log.trace("Collision on $macToUse with $logical")
    	        continue
			} else {
				valid = true
			}
		}
		return logical
	}


	public Integer generateGatewayLogicalId( String mac, DLComponent source ){
		//a slight shortcut - a mac id of 0 is never valid, and really just means a new component has been created
		//we can skip all this work, because who cares?
		Long longmac = Long.parseLong(mac, 16)
		if ( longmac == 0 ){
			//log.trace("Not generating a logical ID for a MAC ID of 0.")
			return 0
		}
		//log.trace("Generating a Logical Id for gateway $source based on $mac in network $wsn")

		Integer logical = 0
		String macToUse = "" //+ panid
		boolean valid = false
		def treatedMac = mac.toLowerCase()
		if ( treatedMac.length() < 16  ){
			//pad out to sixteen chars
			treatedMac = treatedMac.padLeft(16, "0")
//			println "treating mac to $treatedMac"
		}

		while (! valid){
			macToUse += treatedMac
			//generate the CRC 16
			def hash = new CRC16()
			hash.update(macToUse)
			logical = hash.getValue()
			logical = logical | 0xF000
//			println "generation: current hash is ${Integer.toHexString(logical)} for $macToUse"
			if (logical < 0xF000){
            	//print "TOO HIGH!"
            	continue
			} else if (logical == 0xFFFF){
				continue
			} else if ( this.logicalIdIsADupe(logical, source, LidType.GATEWAY, longmac) ) {
				//log.trace("Collision on $macToUse with $logical")
    	        continue
			} else {
				valid = true
			}
		}
		return logical
	}

	/**
	 * Checks the Component Model to see if the logical id is a dupe.
	 *
	 * We only need to do dupe-checking per network.
	 *
	 * @param logical The Logical Id to check for uniqueness.
	 * @return true if the given logical id is not unique.
	 */
	private boolean logicalIdIsADupe(int logical, DLComponent source, LidType type, Long mac){
		//log.trace("Checking for dupe LogicalId on ${Long.toHexString(logical)} for $source")
		boolean result = false
		for ( DLObject o : omodel.getObjectsByType(type.typeName()) ) {
			//we don't want to know if this is the same as OUR logical id, or look at other networks
			DLComponent owner = cmodel.getOwner(o)
			if ( !owner || (owner && owner != source) ){
				if ( ((Integer)o.getObjectProperty("id").getValue()) == logical ){
					//if the LIDS are the same, check to see if the MACs are
					if ( ((Long)o.getObjectProperty("mac").getValue()) != mac ){
						result = true
					}
				}
			}
		}
		return ( result )
	}

	/**
	 * Assign node-type lids to nodes.
	 * @param nodes a List of DLObjects to assign LIDs to
	 */
	public void assignNodeLids( List<DLObject> nodes ){
		for (DLObject node : nodes){
//			println "assigning lid to node $node"\
			if ( node.getObjectProperty("mac").getValue() != null ){
				def currentLid = this.generateLogicalId( Long.toHexString( node.getObjectProperty("mac").getValue() ), cmodel.getOwner(node) )
				node.getObjectProperty("id").setValue( currentLid )
			}
		}
	}

	/**
	 * Assign gateway-type lids to gateways.
	 * @param nodes a List of DLObjects to assign LIDs to
	 */
	public void assignGatewayLids( List<DLObject> gateways ){
		for (DLObject gateway : gateways){
			if ( gateway.getObjectProperty("mac").getValue() != null ){
				def currentLid = this.generateGatewayLogicalId( Long.toHexString( gateway.getObjectProperty("mac").getValue() ), cmodel.getOwner(gateway) )
				gateway.getObjectProperty("id").setValue( currentLid )
			}
		}
	}

}