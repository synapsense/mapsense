package DeploymentLab.Security;

import DeploymentLab.CentralCatalogue;
import DeploymentLab.Model.PlatformType;
import com.google.common.base.Strings;
import com.synapsense.hardware_id.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Shells out to the hardware_id package to validate various kinds of macid checksums.
 * 
 * Formerly, this class did the work itself.
 * 
 * @author Gabriel Helman
 * @see com.synapsense.hardware_id.NodeIdentifierFactory
*/
public class MacChecksum {


	// List of platforms that have the platform id embedded in the MAC address.
	public static List<Integer> EMBEDDED_PLATFORMS = Arrays.asList( PlatformType.THERMANODE_EZ.getId(),
	                                                                PlatformType.THERMANODE_EZH.getId() );

	public static enum ValidationCode { VALID, BAD_CHECKSUM, PLATFORM_MISMATCH }

	/**
	 * Performs advanced MAC validation beyond just checksums and returns the specific error if there is one.
	 *
	 * @param mac         The MAC Address to check.
	 * @param platformId  The Platform Id for the device. Can be zero for devices with no Platform Id.
	 *
	 * @return  The result of the validation checks.
	 */
	public static ValidationCode validateMac( long mac, int platformId ) {
		ValidationCode result = ValidationCode.VALID;

		if( mac > 0 ) {
			if( platformId > 0 ) {
				if( !MacChecksum.isValidMac( mac, platformId ) ) {
					result = ValidationCode.BAD_CHECKSUM;
				}
			} else {
				if( !MacChecksum.isValidMac( mac ) ) {
					result = ValidationCode.BAD_CHECKSUM;
				}
			}

			if( ( result == ValidationCode.VALID ) && EMBEDDED_PLATFORMS.contains( platformId ) ) {
				// For the newer platforms, the 4th byte has the platform id embedded. Make sure they match.
				if( ( (mac >> 32) & 0xFF ) != platformId ) {
					result  = ValidationCode.PLATFORM_MISMATCH;
				}
			}
		}

		return result;
	}

	static public boolean isValidMac(long mac) {
		boolean result = true;
		//convert to 0-padded hexstring
		String rep = Long.toHexString(mac);
		//pad!
		rep = Strings.padStart(rep,16,'0');
		try {
			NodeIdentifierFactory.fromString(rep);
		} catch (MalformedIdentifier e){
			result = false;
		}
		return result;
	}

	private static Set<Integer> EUI64Platforms = null;

	static public boolean isValidMac(long mac, int platformId) {
		boolean result = true;

		if(EUI64Platforms == null){
			EUI64Platforms = new HashSet<Integer>();
			for(String s : CentralCatalogue.getComp("nodes.eui64Platforms").split(",")){
				EUI64Platforms.add(Integer.parseInt(s));
			}
		}
		//convert to 0-padded hexstring
		String rep = Long.toHexString(mac);
		//pad!
		rep = Strings.padStart(rep,16,'0');

		NodeIdentifier ni = null;

		try {
			ni = NodeIdentifierFactory.fromString(rep);
		} catch (MalformedIdentifier e){
			result = false;
		}

		if(result){
			if(EUI64Platforms.contains(platformId)){
				if(ni instanceof EUI64){
					result = true;
				} else {
					result = false;
				}
			}else{
				if(ni instanceof MACID){
					result = true;
				} else {
					result = false;
				}
			}
		}
		return result;
	}


}
