package DeploymentLab.Security;

/**
 * Enum to represent the kinds of Logical Ids that can be generated.
 * Currently, just WSN Node and WSN Gateway, since those two classes of devices use different Logical Id spaces.
 * This is represented as an Enum since comparing two enums can be faster than comparing two strings.
 * @author Gabriel Helman
 */
public enum LidType {

	NODE ("wsnnode"),
	GATEWAY ("wsngateway");

	private final String typeName;

	LidType(String tn){ typeName = tn; }

	public String typeName()   { return typeName; }
}
