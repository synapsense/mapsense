package DeploymentLab;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import java.util.Vector;

import DeploymentLab.PropertyEditor.NumberCellEditor;
import DeploymentLab.PropertyEditor.NumberCellRenderer;

/**
 * A JTable with the extra functionality that it can auto-select the contents of a cell when editing starts.
 * Further, the selection will be "excel-style", in that the negative sign at the start of a number will NOT
 * be selected.
 *
 * Based on the behavior added to the PropertyTable over the last several releases.
 *
 * Some of the select-on-edit functionality was adapted from Rob Camick's RXTable
 * at http://tips4java.wordpress.com/2008/10/20/table-select-all-editor/
 *
 * @author Gabriel Helman
 * @since Jupiter
 * Date: 2/16/12
 * Time: 5:36 PM
 * @see DeploymentLab.PropertyEditor.PropertyTable
 *
 */
public class SelectingTable extends JTable{

	SelectingTable() {
		this.putClientProperty("JTable.autoStartsEdit", Boolean.TRUE);
		this.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
	}

	SelectingTable(TableModel dm) {
		super(dm);
		this.putClientProperty("JTable.autoStartsEdit", Boolean.TRUE);
		this.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
	}

	SelectingTable(TableModel dm, TableColumnModel cm) {
		super(dm, cm);
		this.putClientProperty("JTable.autoStartsEdit", Boolean.TRUE);
		this.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
	}

	SelectingTable(TableModel dm, TableColumnModel cm, ListSelectionModel sm) {
		super(dm, cm, sm);
		this.putClientProperty("JTable.autoStartsEdit", Boolean.TRUE);
		this.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
	}

	SelectingTable(int numRows, int numColumns) {
		super(numRows, numColumns);
		this.putClientProperty("JTable.autoStartsEdit", Boolean.TRUE);
		this.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
	}

	SelectingTable(Vector rowData, Vector columnNames) {
		super(rowData, columnNames);
		this.putClientProperty("JTable.autoStartsEdit", Boolean.TRUE);
		this.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
	}

	SelectingTable(Object[][] rowData, Object[] columnNames) {
		super(rowData, columnNames);
		this.putClientProperty("JTable.autoStartsEdit", Boolean.TRUE);
		this.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
	}


	private boolean isSelectAllForMouseEvent = false;
	private boolean isSelectAllForActionEvent = false;
	private boolean isSelectAllForKeyEvent = false;
	private boolean isCascadeEdit = false;

	public void setCascadeEdit(boolean isCascadeEdit){
		this.isCascadeEdit = isCascadeEdit;
	}

	/*
	 *  Sets the Select All property for for all event types
	 */
	public void setSelectAllForEdit(boolean isSelectAllForEdit) {
		setSelectAllForMouseEvent( isSelectAllForEdit );
		setSelectAllForActionEvent( isSelectAllForEdit );
		setSelectAllForKeyEvent( isSelectAllForEdit );
	}

	/*
	 *  Set the Select All property when editing is invoked by the mouse
	 */
	public void setSelectAllForMouseEvent(boolean isSelectAllForMouseEvent) {
		this.isSelectAllForMouseEvent = isSelectAllForMouseEvent;
	}

	/*
	 *  Set the Select All property when editing is invoked by the "F2" key
	 */
	public void setSelectAllForActionEvent(boolean isSelectAllForActionEvent) {
		this.isSelectAllForActionEvent = isSelectAllForActionEvent;
	}

	/*
	 *  Set the Select All property when editing is invoked by
	 *  typing directly into the cell
	 */
	public void setSelectAllForKeyEvent(boolean isSelectAllForKeyEvent){
		this.isSelectAllForKeyEvent = isSelectAllForKeyEvent;
	}

	/*
	 *  Override to provide Select All editing functionality
	 */
	public boolean editCellAt(int row, int column, EventObject e) {
		boolean result = super.editCellAt(row, column, e);
		if (isSelectAllForMouseEvent || isSelectAllForActionEvent || isSelectAllForKeyEvent) {
			selectAll(e);
		}
		return result;
	}

	/*
	 * Select the text when editing on a text related cell is started
	 */
	private void selectAll(EventObject e) {
		final Component editor = getEditorComponent();

		if (editor == null || ! (editor instanceof JTextComponent)) { return; }

		if (isCascadeEdit){
			((JTextComponent) editor).addKeyListener(new KeyListener() {
				@Override
				public void keyTyped(KeyEvent e) {
				}

				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						changeSelection(getEditingRow()+1, getEditingColumn(), false, false);
					}
				}

				@Override
				public void keyReleased(KeyEvent e) {
				}
			});
		}

		if (e == null) {
			//((JTextComponent)editor).selectAll();
			selectContents(((JTextComponent) editor));
			return;
		}

		//  Typing in the cell was used to activate the editor
		if (e instanceof KeyEvent && isSelectAllForKeyEvent) {
			//((JTextComponent)editor).selectAll();
			selectContents(((JTextComponent) editor));
			return;
		}

		//  F2 was used to activate the editor
		if (e instanceof ActionEvent && isSelectAllForActionEvent) {
			//((JTextComponent)editor).selectAll();
			selectContents(((JTextComponent) editor));
			return;
		}

		//  A mouse click was used to activate the editor.
		//  Generally this is a double click and the second mouse click is
		//  passed to the editor which would remove the text selection unless
		//  we use the invokeLater()
		if (e instanceof MouseEvent && isSelectAllForMouseEvent) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					//((JTextComponent)editor).selectAll();
					selectContents(((JTextComponent) editor));
				}
			});
		}
	}

	/**
	 * Select the contents of the editor.  If the value in the editor is a negative number,
	 * select everything except the minus sign.
	 * @param ed the text component about to be edited
	 */
	private static void selectContents(JTextComponent ed) {
		//System.out.println("SELECT CONTENTS");
		boolean isNumeric = true;
		double d = 0;
		try {
			d = Localizer.parseDouble( ed.getText() );
		} catch (NumberFormatException nfe) {
			isNumeric = false;
		}
		if (isNumeric && d < 0) {
			//only select the bit after the negative sign
			String t = ed.getText();
			int minusSign = t.indexOf("-");
			ed.select(minusSign + 1, t.length());
		} else {
			ed.selectAll();
		}
	}

    @Override
    public TableCellRenderer getCellRenderer(int row, int col) {
	    Object val = getModel().getValueAt( row, col );

        if( val != null && Localizer.isLocalizable( val.getClass() ) ) {
            // Custom renderer for numbers.
            return new NumberCellRenderer();
        }
        return super.getCellRenderer(row, col);
    }

    @Override
    public TableCellEditor getCellEditor(int row, int col) {
        DefaultCellEditor editor = (DefaultCellEditor) super.getCellEditor( row, col );
        Object val = getModel().getValueAt( row, col );

        if( val != null && Localizer.isLocalizable( val.getClass() ) ) {
            // Custom editor for numbers. Use the default component so they behave the same.
            Component comp = editor.getTableCellEditorComponent( this, val, true, row, col );
            editor = new NumberCellEditor( (JTextField) comp );
        } else {
            // Custom behavior for the default.
            editor.setClickCountToStart(1);
        }

        return editor;
    }
}
