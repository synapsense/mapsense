package com.synapsense.hardware_id;

public class MalformedIdentifier extends Exception {

	private static final long serialVersionUID = -5262931879073908872L;
	public MalformedIdentifier(String msg) {
		super(msg);
	}
	
}
