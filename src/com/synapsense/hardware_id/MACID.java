package com.synapsense.hardware_id;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class MACID extends BaseNodeIdentifier implements NodeIdentifier {

	
	public MACID(long id) throws MalformedIdentifier {
		identifier = id;
		
		byte[] bid = new byte[8];

		for (int j = 7; j >= 0; j--) {
			bid[j] = (byte)(id & 0xff);
			id = id >> 8;
		}
		int crc_check = CRC8ONEWIRE.crc8(bid);

		if (crc_check != 0) {
			throw new MalformedIdentifier("CRC does not match " + Integer.toHexString(crc_check));
		}
		
		if (bid[0] != 01) {
			throw new MalformedIdentifier("Not SynapSense prefix");
		}
	}

	public static MACID generateWith(int typecode, int unique) throws MalformedIdentifier {
		ByteBuffer bb = ByteBuffer.allocate(8);

		// Only the 01 first byte is really required. The next two are just to be more like EUI64.
		bb.put( (byte)0x01 );
		bb.put( (byte)0x00 );
		bb.put( (byte)typecode );
		bb.putInt( unique );

		// Calculate the CRC with only the first 7 bytes then add the CRC to the original byte buffer.
		byte crc = (byte) CRC8ONEWIRE.crc8( Arrays.copyOfRange( bb.array(), 0, 7 ) );
		bb.put( crc );

		return new MACID( bb.getLong(0) );
	}

	public static void main(String[] args) throws Exception {
		String str = "01-d3-6b-23-13-00-00-97";
		System.out.println("Input " + str);
		NodeIdentifier r = NodeIdentifierFactory.fromString(str);
		System.out.println(r.toString());
		
		
	}

}
