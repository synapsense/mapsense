package com.synapsense.hardware_id;

import java.nio.ByteBuffer;
import java.security.SecureRandom;

public class EUI64 extends BaseNodeIdentifier implements NodeIdentifier {

	
	public EUI64(long id) throws MalformedIdentifier {
		identifier = id;
		ByteBuffer bb = ByteBuffer.allocate(8);
		bb.putLong(identifier);
		int crc_check = CRC4.compute(bb.array(), 16);
		if (crc_check != 0) {
			throw new MalformedIdentifier("CRC does not match");
		}
		bb.rewind();
		if (bb.get(0) != 00 && bb.get(1) != 0x33 && bb.get(2) != 0x6c) {
			throw new MalformedIdentifier("Not SynapSense prefix");
		}
	}

	
	public static EUI64 generateWith(int typecode, int unique) throws MalformedIdentifier {
		ByteBuffer bb = ByteBuffer.allocate(8);
		bb.put((byte)0x00);
		bb.put((byte)0x33);
		bb.put((byte)0x6c);
		bb.put((byte)typecode);
		if ((unique & 0xF0000000) != 0) {
			throw new MalformedIdentifier("Invalid unique - the high 4 bits must not be set");
		}
		unique = unique << 4;
		bb.mark();
		bb.putInt(unique);
		int crc = CRC4.compute(bb.array(), 15);
		bb.reset();
		unique = unique | crc;
		bb.putInt(unique);
		long eui = bb.getLong(0);
		try {
			return new EUI64(eui);
		} catch (MalformedIdentifier e) {
			return null;
		}
		
	}
	
	public static void main(String[] args) throws Exception {
		for (int i = 0; i < 100; i++) {
			String str = generateWith(new SecureRandom().nextInt(100), new java.security.SecureRandom().nextInt(100000)).toString();
			//System.out.println("Generated " + str);
			EUI64 r = (EUI64)NodeIdentifierFactory.fromString(str);
			System.out.println(r.toString());
		}
	}

}
