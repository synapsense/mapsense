package com.synapsense.hardware_id;

public class NodeIdentifierFactory {

	/**
	 * Return a NodeIdentifier based upon an input string.
	 * @TODO: Try/catch building seems wonky, how else to detect?
	 * @param data Input string containing data, with or without "-" seperators.
	 * @return NodeIdentifier with a validated string
	 * @throws MalformedIdentifier
	 */
	public static NodeIdentifier fromString(String data)
			throws MalformedIdentifier {

		String desep = data.replace("-", ""); // Remove seperators
		if (desep.length() != 16) // 16 required length
			throw new MalformedIdentifier("Invalid length");

		long parsed = 0;

		try {
			parsed = Long.parseLong(desep, 16);
		} catch (NumberFormatException e) {
			throw new MalformedIdentifier(e.toString());
		}
		
		NodeIdentifier idclass = null;
		
		try {
			idclass = new EUI64(parsed);
		} catch (Exception e) {
			// We are ignoring this one to fall through
		}
		
		// We will let this exception fall through
		
		if (idclass == null) {
			idclass = new MACID(parsed);
		}
		
		return idclass;
	}

}
