package com.synapsense.hardware_id;

import java.nio.ByteBuffer;
import java.util.Formatter;
import java.util.Locale;

abstract class BaseNodeIdentifier {
	protected long identifier;
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		Formatter formatter = new Formatter(sb, Locale.US);
		ByteBuffer bb = ByteBuffer.allocate(8);
		bb.putLong(identifier);
		byte[] array = bb.array();
		for (int b = 0; b < 8; b++) {
			if (b < 7)
				formatter.format("%02X-", array[b]);
			else
				formatter.format("%02X", array[b]);
		}
		return sb.toString();
	}

	public long getLong() {
		return identifier;
	}
	
	
	
}
