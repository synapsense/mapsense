package com.synapsense.hardware_id;

public class CRC4 {

	private static final int POLY = 0x3	;
	
	public static int compute(byte [] main, int nibblelimit) {
		int val = 0x0;
		
		for (byte a : main) {
			for (int nibble = 0; nibble <= 1; nibble++) {
				nibblelimit--;
				if (nibblelimit < 0) {
					break;
				}
				val ^= ((a >> 4) & 0xf);
				a = (byte)(a << 4);
				
				for (int bit = 4; bit > 0; --bit) {
					if ((val & 0x8) != 0) {
						val = ((val << 1) ^ POLY) & 0xf;
					} else {
						val = (val << 1) & 0xf;
					}
				}	
			}
		}
		return (val) & 0xf;
	}
	
	
	public static void main(String [] main) {
		byte[] message = {(byte)0xab, (byte)0xab};
		System.out.println("CRC is " + Integer.toHexString(compute(message, 4)));
	}
}
