<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
	name="Row Lite"
	version="&componentlib.version;"
	cversion="&componentlib.cversion;"
	oversion="&componentlib.oversion;"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
	>

	<component type="dual-horizontal-row-thermanode2" classes="placeable,rotatable,staticchildplaceable" grouppath='Environmentals;Wireless;Racks' filters="Environmentals">
		<display>
			<description>Two arrays of sensors, each with a ThermaNode, used to monitor the horizontal temperature distribution at various points along the top of a row of racks.  The two arrays are placed one on the front and back of the racks. Each ThermaNode has 6 external thermistors displaced from the center where the ThermaNode is located.  A pair of sensors from each ThermaNode is represented as a Rack.  This component goes in a Room and requires a WSN Data Source.  This configuration is qualified for SynapSense Active Control™.</description>
			<name>RowLite</name>
			<shape>dual_node</shape>
			<childshape>rack-rearexhaust-toponly</childshape>
		</display>

		<object type="wsnnode" as="intake_node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity"><value>5600.0</value></property>
			<property name="batteryOperated"><value>1</value></property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status"><value>0</value></property>
			<property name="platformId"><value>11</value></property>
			<property name="platformName"><value>ThermaNode</value></property>
			<property name="sensepoints">
				<object type="wsnsensor" as="intake_internal_temp">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>internal temp</value></property>
					<property name="channel"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="aMin"><value>59.0</value></property>
					<property name="aMax"><value>90.0</value></property>
					<property name="rMin"><value>64.4</value></property>
					<property name="rMax"><value>80.6</value></property>
					<property name="type"><value>1</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>humidity</value></property>
					<property name="channel"><value>1</value></property>
					<property name="min"><value>10.0</value></property>
					<property name="max"><value>90.0</value></property>
					<property name="aMin"><value>20.0</value></property>
					<property name="aMax"><value>80.0</value></property>
					<property name="rMin"><value>40.0</value></property>
					<property name="rMax"><value>55.0</value></property>
					<property name="type"><value>2</value></property>
					<property name="dataclass"><value>201</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>131072</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>battery</value></property>
					<property name="channel"><value>2</value></property>
					<property name="min"><value>1.8</value></property>
					<property name="max"><value>3.7</value></property>
					<property name="aMin"><value>2.5</value></property>
					<property name="aMax"><value>3.7</value></property>
					<property name="rMin"><value>2.7</value></property>
					<property name="rMax"><value>3.7</value></property>
					<property name="type"><value>5</value></property>
					<property name="dataclass"><value>210</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				
			</property>
		</object>

		<object type="wsnnode" as="exhaust_node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity"><value>5600.0</value></property>
			<property name="batteryOperated"><value>1</value></property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status"><value>0</value></property>
			<property name="platformId"><value>11</value></property>
			<property name="platformName"><value>ThermaNode</value></property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>internal temp</value></property>
					<property name="channel"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="aMin"><value>59.0</value></property>
					<property name="aMax"><value>90.0</value></property>
					<property name="rMin"><value>64.4</value></property>
					<property name="rMax"><value>80.6</value></property>
					<property name="type"><value>1</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>humidity</value></property>
					<property name="channel"><value>1</value></property>
					<property name="min"><value>10.0</value></property>
					<property name="max"><value>90.0</value></property>
					<property name="aMin"><value>20.0</value></property>
					<property name="aMax"><value>80.0</value></property>
					<property name="rMin"><value>40.0</value></property>
					<property name="rMax"><value>55.0</value></property>
					<property name="type"><value>2</value></property>
					<property name="dataclass"><value>201</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>131072</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>battery</value></property>
					<property name="channel"><value>2</value></property>
					<property name="min"><value>1.8</value></property>
					<property name="max"><value>3.7</value></property>
					<property name="aMin"><value>2.5</value></property>
					<property name="aMax"><value>3.7</value></property>
					<property name="rMin"><value>2.7</value></property>
					<property name="rMax"><value>3.7</value></property>
					<property name="type"><value>5</value></property>
					<property name="dataclass"><value>210</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				
			</property>
		</object>
		<macids value="intake_mac_id,exhaust_mac_id" />
		
		<!-- several parameters for staticChildrenNode-->
		
		<children type="horizontal-row-rack-child">
			<child name="rack1" channel="5" display="CH5-Rack1"/>
			<child name="rack2" channel="4" display="CH4-Rack2"/>
			<child name="rack3" channel="3" display="CH3-Rack3"/>
			<child name="rack4" channel="6" display="CH6-Rack4"/>
			<child name="rack5" channel="7" display="CH7-Rack5"/>
			<child name="rack6" channel="8" display="CH8-Rack6"/>
		</children>

		<synchronizedproperties>
			<property parent="min_allow_t" child="min_allow_t"/>
			<property parent="max_allow_t" child="max_allow_t"/>
			<property parent="min_recommend_t" child="min_recommend_t"/>
			<property parent="max_recommend_t" child="max_recommend_t"/>
			<property parent="cold_delta" child="cold_delta"/>
			<property parent="hot_delta" child="hot_delta"/>
			<property parent="rotation" child="rotation"/>
			<property parent="depth" child="depth"/>
			<property parent="width" child="width"/>
			<property parent="name" child="parent_name"/>
		</synchronizedproperties>
		
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">RowLite</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true">0</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="intake_mac_id" display="Intake MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
		<property name="exhaust_mac_id" display="Exhaust MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true">42.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true">24.0</property>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		<property name="rack1_offset" display="Channel 5 (Rack 1) Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">-216.0</property>
		<property name="rack2_offset" display="Channel 4 (Rack 2) Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">-144.0</property>
		<property name="rack3_offset" display="Channel 3 (Rack 3) Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">-72.0</property>
		<property name="rack4_offset" display="Channel 6 (Rack 4) Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">72.0</property>
		<property name="rack5_offset" display="Channel 7 (Rack 5) Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">144.0</property>
		<property name="rack6_offset" display="Channel 8 (Rack 6) Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">216.0</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="cold_delta" display="Intake Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">5</property>
		<property name="hot_delta" display="Exhaust Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">10</property>

		<property name="powerSource" display="Power Source" type="java.lang.Integer" editable="true" displayed="true"
		valueChoices="Battery Powered:1,Wall Powered:0">1</property>

		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">RowLite</property>

		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

		<property name="rack1_name" display="Rack1 Name" type="java.lang.String" editable="true" displayed="false"></property>
		<property name="rack2_name" display="Rack2 Name" type="java.lang.String" editable="true" displayed="false"></property>
		<property name="rack3_name" display="Rack3 Name" type="java.lang.String" editable="true" displayed="false"></property>
		<property name="rack4_name" display="Rack4 Name" type="java.lang.String" editable="true" displayed="false"></property>
		<property name="rack5_name" display="Rack5 Name" type="java.lang.String" editable="true" displayed="false"></property>
		<property name="rack6_name" display="Rack6 Name" type="java.lang.String" editable="true" displayed="false"></property>

		<!-- intake node and internal sensor positioning -->
		<varbinding vars="x,depth,rotation">
			<property>$intake_node/properties[name="x"]</property>
			<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$intake_node/properties[name="y"]</property>
			<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		
		<!-- exhaust node and internal sensors positioning -->
		<varbinding vars="x,depth,rotation">
			<property>$exhaust_node/properties[name="x"]</property>
			<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<value>x -(depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$exhaust_node/properties[name="y"]</property>
			<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		
		<varbinding vars="name">
			<property>$intake_node/properties[name="name"]</property>
			<value>'Node ' + name + ' Intake'</value>
		</varbinding>
		<varbinding vars="name">
			<property>$exhaust_node/properties[name="name"]</property>
			<value>'Node ' + name + ' Exhaust'</value>
		</varbinding>

		<varbinding vars="intake_mac_id">
			<property>$intake_node/properties[name="mac"]</property>
			<value>Long.parseLong(intake_mac_id,16)</value>
		</varbinding>
		<varbinding vars="exhaust_mac_id">
			<property>$exhaust_node/properties[name="mac"]</property>
			<value>Long.parseLong(exhaust_mac_id,16)</value>
		</varbinding>

		<varbinding vars="intake_mac_id">
			<property>$intake_node/properties[name="id"]</property>
			<value>lid.generateLogicalId(intake_mac_id , self)</value>
		</varbinding>
		<varbinding vars="exhaust_mac_id">
			<property>$exhaust_node/properties[name="id"]</property>
			<value>lid.generateLogicalId(exhaust_mac_id , self)</value>
		</varbinding>

		<varbinding vars="sample_interval">
			<property>$intake_node/properties[name="period"]</property>
			<property>$exhaust_node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>

		<varbinding vars="location">
			<property>$intake_node/properties[name="location"]</property>
			<property>$exhaust_node/properties[name="location"]</property>
			<value>location</value>
		</varbinding>

		<varbinding vars="powerSource">
			<property>$intake_node/properties[name="batteryOperated"]</property>
			<property>$exhaust_node/properties[name="batteryOperated"]</property>
			<value>powerSource</value>
		</varbinding>

	</component>
	
	<component type="horizontal-row-rack-child" classes="placeable,staticchild,controlinput,temperaturecontrol" filters="Environmentals">
		<display>
			<description>Two arrays of sensors, each with a ThermaNode, used to monitor the horizontal temperature distribution at various points along the top of a row of racks.  The two arrays are placed one on the front and back of the racks. Each ThermaNode has 6 external thermistors displaced from the center where the ThermaNode is located.  A pair of sensors from each ThermaNode is represented as a Rack.  This component goes in a Room and requires a WSN Data Source.  This configuration is qualified for SynapSense Active Control™.</description>
			<name>RowLite</name>
			<shape>rack-rearexhaust-toponly</shape>
		</display>
		
		<object type="wsnsensor" as="intake_sensor">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>rack intake temp</value></property>
			<property name="channel"><value>0</value></property>
			<property name="min"><value>40.0</value></property>
			<property name="max"><value>100.0</value></property>
			<property name="type"><value>27</value></property>
			<property name="dataclass"><value>200</value></property>
			<property name="enabled"><value>1</value></property>
			<property name="mode"><value>instant</value></property>
			<property name="interval"><value>0</value></property>
			<property name="status"><value>1</value></property>
			<property name="lastValue"><value>-5000.0</value></property>
			<property name="z"><value>131072</value></property>
			<property name="subSamples"><value>30</value></property>
		</object>			

		<object type="wsnsensor" as="exhaust_sensor">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>rack exhaust temp</value></property>
			<property name="channel"><value>0</value></property>
			<property name="min"><value>40.0</value></property>
			<property name="max"><value>100.0</value></property>
			<property name="type"><value>27</value></property>
			<property name="dataclass"><value>200</value></property>
			<property name="enabled"><value>1</value></property>
			<property name="mode"><value>instant</value></property>
			<property name="interval"><value>0</value></property>
			<property name="status"><value>1</value></property>
			<property name="lastValue"><value>-5000.0</value></property>
			<property name="z"><value>131072</value></property>
			<property name="subSamples"><value>30</value></property>
		</object>

        <docking from='$rack/properties[name="cTop"]' to='$intake_sensor' />
		<docking from='$rack/properties[name="hTop"]' to='$exhaust_sensor' />
		<docking to="$rack/properties[name='coldDp']" from="$rack/properties[name='displaypoints']/children[1]/properties[name='lastValue']"/>

		<!-- Must be after the cTop, etc., dockings or the references to them will be null. -->
		&object.env.rack.single; <!-- $rack -->

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="x" display="x" type="java.lang.Double" editable="false" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="false" displayed="true" dimension="distance">0</property>
		<property name="channel" display="Channel" type="java.lang.Integer" editable="false" displayed="true">0</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>
		
		<property name="child_id" display="Child Id" type="java.lang.String" editable="false" displayed="false"></property>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="false" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="false" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="false" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="false" dimension="temperature">80.6</property>
		<property name="cold_delta" display="Intake Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="false" dimension="temperatureDelta">5</property>
		<property name="hot_delta" display="Exhaust Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="false" dimension="temperatureDelta">10</property>
		<property name="rotation" display="rotation" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="false">42.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="false">24.0</property>
		<property name="parent_name" display="Parent Name" type="java.lang.String" editable="false" displayed="false"></property>
	
		<varbinding vars="parent_name,name">
			<property>$rack/properties[name="name"]</property>
			<value>parent_name + '-' + name</value>
		</varbinding>

		<varbinding vars="x">
			<property>$rack/properties[name="x"]</property>
            <property name="property"><value>$rack/properties[name='displaypoints']/children[1]/properties[name='x']</value></property>
			<value>x</value>
		</varbinding>

		<varbinding vars="y">
			<property>$rack/properties[name="y"]</property>
            <property name="property"><value>$rack/properties[name='displaypoints']/children[1]/properties[name='y']</value></property>
			<value>y</value>
		</varbinding>

		<varbinding vars="width">
			<property>$rack/properties[name="width"]</property>
			<value>width</value>
		</varbinding>

		<varbinding vars="depth">
			<property>$rack/properties[name="depth"]</property>
			<value>depth</value>
		</varbinding>

		<varbinding vars="rotation">
			<property>$rack/properties[name="rotation"]</property>
			<value>rotation</value>
		</varbinding>

		<varbinding vars="x,rotation,depth">
			<property>$intake_sensor/properties[name="x"]</property>
			<value>x  + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>

		<varbinding vars="y,rotation,depth">
			<property>$intake_sensor/properties[name="y"]</property>
			<value>y +  (depth/2 * Math.sin(Math.toRadians(-rotation))) </value>
		</varbinding>
		
		<varbinding vars="x,rotation,depth">
			<property>$exhaust_sensor/properties[name="x"]</property>
			<value>x  - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,rotation,depth">
			<property>$exhaust_sensor/properties[name="y"]</property>
			<value>y  - (depth/2 * Math.sin(Math.toRadians(-rotation))) </value>
		</varbinding>
		

		<varbinding vars="channel">
			<property>$intake_sensor/properties[name="channel"]</property>
			<property>$exhaust_sensor/properties[name="channel"]</property>
			<value>channel</value>
		</varbinding>
		
		<varbinding vars="min_allow_t">
			<property>$intake_sensor/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>

		<varbinding vars="max_allow_t">
			<property>$intake_sensor/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		
		<varbinding vars="min_recommend_t">
			<property>$intake_sensor/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		
		<varbinding vars="max_recommend_t">
			<property>$intake_sensor/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		
		<varbinding vars="cold_delta">
			<property>$intake_sensor/properties[name="smartSendThreshold"]</property>
			<value>(int)(cold_delta)</value>
		</varbinding>
		
		<varbinding vars="hot_delta">
			<property>$exhaust_sensor/properties[name="smartSendThreshold"]</property>
			<value>(int)(hot_delta)</value>
		</varbinding>
		
	</component>

</componentlib>