<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
	name="Legacy 5.1 objects"
	version="&componentlib.version;"
	cversion="&componentlib.cversion;"
	oversion="&componentlib.oversion;"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
	>

		
	<component type="rack-interior-nsf-rt-thermanode" classes="placeable,rotatable" grouppath="Environmentals;Legacy 5.1" filters="Environmentals">
		<display>
			<description>Rack monitoring assembly consisting of a ThermaNode equipped with 5 external sensors.  Sense points are located at cabinet cold top (node), cold middle, cold bottom, hot top, hot middle and hot bottom.</description>
			<name>Interior</name>
			<shape>triangle_orange</shape>
		</display>
		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity">
				<value>5600.0</value>
			</property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status">
				<value>0</value>
			</property>
			<property name="platformId">
				<value>11</value>
			</property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold top temp</value>
					</property>
					<property name="channel">
						<value>0</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>1</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>humidity</value>
					</property>
					<property name="channel">
						<value>1</value>
					</property>
					<property name="min">
						<value>10.0</value>
					</property>
					<property name="max">
						<value>90.0</value>
					</property>
					<property name="aMin">
						<value>20.0</value>
					</property>
					<property name="aMax">
						<value>80.0</value>
					</property>
					<property name="rMin">
						<value>40.0</value>
					</property>
					<property name="rMax">
						<value>55.0</value>
					</property>
					<property name="type">
						<value>2</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>201</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>battery</value>
					</property>
					<property name="channel">
						<value>2</value>
					</property>
					<property name="min">
						<value>1.8</value>
					</property>
					<property name="max">
						<value>3.7</value>
					</property>
					<property name="aMin">
						<value>2.5</value>
					</property>
					<property name="aMax">
						<value>3.7</value>
					</property>
					<property name="rMin">
						<value>2.7</value>
					</property>
					<property name="rMax">
						<value>3.7</value>
					</property>
					<property name="type">
						<value>5</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>210</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold middle temp</value>
					</property>
					<property name="channel">
						<value>3</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>16384</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold bottom temp</value>
					</property>
					<property name="channel">
						<value>4</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>2048</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>hot top temp</value>
					</property>
					<property name="channel">
						<value>5</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>hot middle temp</value>
					</property>
					<property name="channel">
						<value>6</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>16384</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>hot bottom temp</value>
					</property>
					<property name="channel">
						<value>7</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>2048</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
			</property>
			<property name="batteryOperated">
				<value>1</value>
			</property>
			<property name="platformName">
				<value>ThermaNode</value>
			</property>
		</object>
		<object type="rack" as="rack">
			<dlid>DLID:-1</dlid>
			<property name="status">
				<value>1</value>
			</property>
		</object>
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true" dimension="">Rack Thermanode Interior</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true" dimension="">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">30.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">30.0</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true" dimension="">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true" dimension="">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true" dimension=""/>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		<property name="min_allow_h" display="Min Allowed Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">20.0</property>
		<property name="max_allow_h" display="Max Allowed Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">80.0</property>
		<property name="min_recommend_h" display="Min Recommended Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">40.0</property>
		<property name="max_recommend_h" display="Max Recommended Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">55.0</property>
		<property name="cold_delta" display="Cold Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">5</property>
		<property name="hot_delta" display="Hot Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">10</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true" dimension="">Thermanode Interior with no sub-floor sensor</property>

		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

		<consumer id="reftemp" datatype="reftemp" name="Rack Ref Temp" property="$rack/properties[name=&quot;ref&quot;]" required="true"/>
		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="x"]</property>
			<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="y"]</property>
			<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
			<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="name">
			<property>$rack/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="rotation">
			<property>$rack/properties[name="rotation"]</property>
			<value>rotation</value>
		</varbinding>
		<varbinding vars="width">
			<property>$rack/properties[name="width"]</property>
			<value>width</value>
		</varbinding>
		<varbinding vars="depth">
			<property>$rack/properties[name="depth"]</property>
			<value>depth</value>
		</varbinding>
		<varbinding vars="x">
			<property>$node/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<property>$rack/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$node/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<property>$rack/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<property>$rack/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		<varbinding vars="min_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		<varbinding vars="min_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMin"]</property>
			<value>min_allow_h</value>
		</varbinding>
		<varbinding vars="max_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMax"]</property>
			<value>max_allow_h</value>
		</varbinding>
		<varbinding vars="min_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMin"]</property>
			<value>min_recommend_h</value>
		</varbinding>
		<varbinding vars="max_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMax"]</property>
			<value>max_recommend_h</value>
		</varbinding>

		<varbinding vars="cold_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(cold_delta)</value>
		</varbinding>
		<varbinding vars="hot_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(hot_delta)</value>
		</varbinding>
		<docking from="$rack/properties[name=&quot;cTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;0&quot;]]"/>
		<docking from="$rack/properties[name=&quot;rh&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;1&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;3&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;4&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;5&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;6&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;7&quot;]]"/>
		<macids value="mac_id"/>
	</component>


	<component type="rack-interior-nsf-nrt-thermanode" classes="placeable,rotatable" grouppath="Environmentals;Legacy 5.1" filters="Environmentals">
		<display>
			<description>Rack monitoring assembly consisting of a ThermaNode equipped with 5 external sensors.  Sense points are located at cabinet cold top (node), cold middle, cold bottom, hot top, hot middle and hot bottom.</description>
			<name>Interior</name>
			<shape>triangle_red</shape>
		</display>
		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity">
				<value>5600.0</value>
			</property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status">
				<value>0</value>
			</property>
			<property name="platformId">
				<value>11</value>
			</property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold top temp</value>
					</property>
					<property name="channel">
						<value>0</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>1</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>humidity</value>
					</property>
					<property name="channel">
						<value>1</value>
					</property>
					<property name="min">
						<value>10.0</value>
					</property>
					<property name="max">
						<value>90.0</value>
					</property>
					<property name="aMin">
						<value>20.0</value>
					</property>
					<property name="aMax">
						<value>80.0</value>
					</property>
					<property name="rMin">
						<value>40.0</value>
					</property>
					<property name="rMax">
						<value>55.0</value>
					</property>
					<property name="type">
						<value>2</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>201</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>battery</value>
					</property>
					<property name="channel">
						<value>2</value>
					</property>
					<property name="min">
						<value>1.8</value>
					</property>
					<property name="max">
						<value>3.7</value>
					</property>
					<property name="aMin">
						<value>2.5</value>
					</property>
					<property name="aMax">
						<value>3.7</value>
					</property>
					<property name="rMin">
						<value>2.7</value>
					</property>
					<property name="rMax">
						<value>3.7</value>
					</property>
					<property name="type">
						<value>5</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>210</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold middle temp</value>
					</property>
					<property name="channel">
						<value>3</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>16384</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold bottom temp</value>
					</property>
					<property name="channel">
						<value>4</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>2048</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>hot top temp</value>
					</property>
					<property name="channel">
						<value>5</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>hot middle temp</value>
					</property>
					<property name="channel">
						<value>6</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>16384</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>hot bottom temp</value>
					</property>
					<property name="channel">
						<value>7</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>2048</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
			</property>
			<property name="batteryOperated">
				<value>1</value>
			</property>
			<property name="platformName">
				<value>ThermaNode</value>
			</property>
		</object>
		<object type="rack" as="rack">
			<dlid>DLID:-1</dlid>
			<property name="status">
				<value>1</value>
			</property>
		</object>
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true" dimension="">Rack Thermanode Interior</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true" dimension="">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">30.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true " dimension="distance">30.0</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true" dimension="">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true" dimension="">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true" dimension=""/>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		<property name="min_allow_h" display="Min Allowed Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">20.0</property>
		<property name="max_allow_h" display="Max Allowed Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">80.0</property>
		<property name="min_recommend_h" display="Min Recommended Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">40.0</property>
		<property name="max_recommend_h" display="Max Recommended Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">55.0</property>
		<property name="cold_delta" display="Cold Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">5</property>
		<property name="hot_delta" display="Hot Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">10</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true" dimension="">Thermanode with 5 Thermistors</property>
		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="x"]</property>
			<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="y"]</property>
			<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
			<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="name">
			<property>$rack/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="rotation">
			<property>$rack/properties[name="rotation"]</property>
			<value>rotation</value>
		</varbinding>
		<varbinding vars="width">
			<property>$rack/properties[name="width"]</property>
			<value>width</value>
		</varbinding>
		<varbinding vars="depth">
			<property>$rack/properties[name="depth"]</property>
			<value>depth</value>
		</varbinding>
		<varbinding vars="x">
			<property>$node/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<property>$rack/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$node/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<property>$rack/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<property>$rack/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		<varbinding vars="min_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		<varbinding vars="min_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMin"]</property>
			<value>min_allow_h</value>
		</varbinding>
		<varbinding vars="max_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMax"]</property>
			<value>max_allow_h</value>
		</varbinding>
		<varbinding vars="min_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMin"]</property>
			<value>min_recommend_h</value>
		</varbinding>
		<varbinding vars="max_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMax"]</property>
			<value>max_recommend_h</value>
		</varbinding>

		<varbinding vars="cold_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(cold_delta)</value>
		</varbinding>
		<varbinding vars="hot_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(hot_delta)</value>
		</varbinding>
		<docking from="$rack/properties[name=&quot;cTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;0&quot;]]"/>
		<docking from="$rack/properties[name=&quot;rh&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;1&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;3&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;4&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;5&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;6&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;7&quot;]]"/>
		<macids value="mac_id"/>
	</component>


	<component type="rack-interior-sf-thermanode" classes="placeable,rotatable" grouppath="Environmentals;Legacy 5.1" filters="Environmentals">
		<display>
			<description>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors.  Sense points are located at cabinet cold top (node), cold middle, cold bottom, hot top, hot middle, hot bottom, subfloor.</description>
			<name>Interior w/ Plenum Temp</name>
			<shape>triangle_sf</shape>
		</display>
		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity">
				<value>5600.0</value>
			</property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status">
				<value>0</value>
			</property>
			<property name="platformId">
				<value>11</value>
			</property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold top temp</value>
					</property>
					<property name="channel">
						<value>0</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>1</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>humidity</value>
					</property>
					<property name="channel">
						<value>1</value>
					</property>
					<property name="min">
						<value>10.0</value>
					</property>
					<property name="max">
						<value>90.0</value>
					</property>
					<property name="aMin">
						<value>20.0</value>
					</property>
					<property name="aMax">
						<value>80.0</value>
					</property>
					<property name="rMin">
						<value>40.0</value>
					</property>
					<property name="rMax">
						<value>55.0</value>
					</property>
					<property name="type">
						<value>2</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>201</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>battery</value>
					</property>
					<property name="channel">
						<value>2</value>
					</property>
					<property name="min">
						<value>1.8</value>
					</property>
					<property name="max">
						<value>3.7</value>
					</property>
					<property name="aMin">
						<value>2.5</value>
					</property>
					<property name="aMax">
						<value>3.7</value>
					</property>
					<property name="rMin">
						<value>2.7</value>
					</property>
					<property name="rMax">
						<value>3.7</value>
					</property>
					<property name="type">
						<value>5</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>210</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold middle temp</value>
					</property>
					<property name="channel">
						<value>3</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>16384</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold bottom temp</value>
					</property>
					<property name="channel">
						<value>4</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>2048</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>hot top temp</value>
					</property>
					<property name="channel">
						<value>5</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>hot middle temp</value>
					</property>
					<property name="channel">
						<value>6</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>16384</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>hot bottom temp</value>
					</property>
					<property name="channel">
						<value>7</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>2048</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>subfloor temp</value>
					</property>
					<property name="channel">
						<value>8</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>256</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
			</property>
			<property name="batteryOperated">
				<value>1</value>
			</property>
			<property name="platformName">
				<value>ThermaNode</value>
			</property>
		</object>
		<object type="rack" as="rack">
			<dlid>DLID:-1</dlid>
			<property name="status">
				<value>1</value>
			</property>
		</object>
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true" dimension="">Interior</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true" dimension="">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">30.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">30.0</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true" dimension="">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true" dimension="">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true" dimension=""/>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		<property name="min_allow_h" display="Min Allowed Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">20.0</property>
		<property name="max_allow_h" display="Max Allowed Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">80.0</property>
		<property name="min_recommend_h" display="Min Recommended Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">40.0</property>
		<property name="max_recommend_h" display="Max Recommended Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">55.0</property>
		<property name="cold_delta" display="Cold Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">5</property>
		<property name="hot_delta" display="Hot Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">10</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true" dimension="">Thermanode Interior with supply plenum sensor</property>

		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

		<producer id="reftemp" datatype="reftemp" name="Reference Temperature" object="$rack/properties[name=&quot;ref&quot;]/referrent"/>
		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="x"]</property>
			<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="y"]</property>
			<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="y"]</property>
			<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="name">
			<property>$rack/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="rotation">
			<property>$rack/properties[name="rotation"]</property>
			<value>rotation</value>
		</varbinding>
		<varbinding vars="width">
			<property>$rack/properties[name="width"]</property>
			<value>width</value>
		</varbinding>
		<varbinding vars="depth">
			<property>$rack/properties[name="depth"]</property>
			<value>depth</value>
		</varbinding>
		<varbinding vars="x">
			<property>$node/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<property>$rack/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$node/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<property>$rack/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<property>$rack/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		<varbinding vars="min_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		<varbinding vars="min_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMin"]</property>
			<value>min_allow_h</value>
		</varbinding>
		<varbinding vars="max_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMax"]</property>
			<value>max_allow_h</value>
		</varbinding>
		<varbinding vars="min_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMin"]</property>
			<value>min_recommend_h</value>
		</varbinding>
		<varbinding vars="max_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMax"]</property>
			<value>max_recommend_h</value>
		</varbinding>

		<varbinding vars="cold_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(cold_delta)</value>
		</varbinding>
		<varbinding vars="hot_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(hot_delta)</value>
		</varbinding>
		<docking from="$rack/properties[name=&quot;cTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;0&quot;]]"/>
		<docking from="$rack/properties[name=&quot;rh&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;1&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;3&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;4&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;5&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;6&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;7&quot;]]"/>
		<docking from="$rack/properties[name=&quot;ref&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;8&quot;]]"/>
		<macids value="mac_id"/>
	</component>
	
	
	<component type="rack-lhs-endcap-rt-thermanode" classes="placeable,rotatable" grouppath="Environmentals;Legacy 5.1" filters="Environmentals">
		<display>
			<description>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors located on a left end of an aisle (facing the cold aisle).  Sense points are located at cabinet cold top (node), cold middle, cold bottom, cabinet end top, cabinet end mid/bottom, hot top and hot mid/bottom.</description>
			<name>LHS endcap</name>
			<shape>triangle_l_orange</shape>
		</display>
		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity">
				<value>5600.0</value>
			</property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status">
				<value>0</value>
			</property>
			<property name="platformId">
				<value>11</value>
			</property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold top temp</value>
					</property>
					<property name="channel">
						<value>0</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>1</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>humidity</value>
					</property>
					<property name="channel">
						<value>1</value>
					</property>
					<property name="min">
						<value>10.0</value>
					</property>
					<property name="max">
						<value>90.0</value>
					</property>
					<property name="aMin">
						<value>20.0</value>
					</property>
					<property name="aMax">
						<value>80.0</value>
					</property>
					<property name="rMin">
						<value>40.0</value>
					</property>
					<property name="rMax">
						<value>55.0</value>
					</property>
					<property name="type">
						<value>2</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>201</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>battery</value>
					</property>
					<property name="channel">
						<value>2</value>
					</property>
					<property name="min">
						<value>1.8</value>
					</property>
					<property name="max">
						<value>3.7</value>
					</property>
					<property name="aMin">
						<value>2.5</value>
					</property>
					<property name="aMax">
						<value>3.7</value>
					</property>
					<property name="rMin">
						<value>2.7</value>
					</property>
					<property name="rMax">
						<value>3.7</value>
					</property>
					<property name="type">
						<value>5</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>210</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold middle temp</value>
					</property>
					<property name="channel">
						<value>3</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>16384</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold bottom temp</value>
					</property>
					<property name="channel">
						<value>4</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>2048</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>hot top temp</value>
					</property>
					<property name="channel">
						<value>5</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>hot mid/bottom temp</value>
					</property>
					<property name="channel">
						<value>6</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>18432</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>end cap top temp</value>
					</property>
					<property name="channel">
						<value>7</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>end cap mid/bottom temp</value>
					</property>
					<property name="channel">
						<value>8</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>18432</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
			</property>
			<property name="batteryOperated">
				<value>1</value>
			</property>
			<property name="platformName">
				<value>ThermaNode</value>
			</property>
		</object>
		<object type="rack" as="rack">
			<dlid>DLID:-1</dlid>
			<property name="status">
				<value>1</value>
			</property>
		</object>
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true" dimension="">LHS Endcap</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true" dimension="">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">30.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">30.0</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true" dimension="">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true" dimension="">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true" dimension=""/>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		<property name="min_allow_h" display="Min Allowed Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">20.0</property>
		<property name="max_allow_h" display="Max Allowed Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">80.0</property>
		<property name="min_recommend_h" display="Min Recommended Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">40.0</property>
		<property name="max_recommend_h" display="Max Recommended Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">55.0</property>
		<property name="cold_delta" display="Cold Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">5</property>
		<property name="hot_delta" display="Hot Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">10</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true" dimension="">Thermanode endcap for LHS</property>

		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

		<consumer id="reftemp" datatype="reftemp" name="Rack Ref Temp" property="$rack/properties[name=&quot;ref&quot;]" required="true"/>
		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="x"]</property>
			<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="y"]</property>
			<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,width,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="x"]</property>
			<value>x - (width/2 * Math.cos(rotation+Math.PI/2))</value>
		</varbinding>
		<varbinding vars="y,width,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="y"]</property>
			<value>y - (width/2 * Math.sin(rotation+Math.PI/2))</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
			<property>$node/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
			<property>$node/properties[name="y"]</property>
			<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x">
			<property>$rack/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$rack/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="name">
			<property>$rack/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="rotation">
			<property>$rack/properties[name="rotation"]</property>
			<value>rotation</value>
		</varbinding>
		<varbinding vars="width">
			<property>$rack/properties[name="width"]</property>
			<value>width</value>
		</varbinding>
		<varbinding vars="depth">
			<property>$rack/properties[name="depth"]</property>
			<value>depth</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<property>$rack/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		<varbinding vars="min_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		<varbinding vars="min_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMin"]</property>
			<value>min_allow_h</value>
		</varbinding>
		<varbinding vars="max_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMax"]</property>
			<value>max_allow_h</value>
		</varbinding>
		<varbinding vars="min_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMin"]</property>
			<value>min_recommend_h</value>
		</varbinding>
		<varbinding vars="max_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMax"]</property>
			<value>max_recommend_h</value>
		</varbinding>

		<varbinding vars="cold_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(cold_delta)</value>
		</varbinding>
		<varbinding vars="hot_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(hot_delta)</value>
		</varbinding>
		<docking from="$rack/properties[name=&quot;cTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;0&quot;]]"/>
		<docking from="$rack/properties[name=&quot;rh&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;1&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;3&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;4&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;5&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;6&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;6&quot;]]"/>
		<macids value="mac_id"/>
	</component>
	
	
	<component type="rack-lhs-endcap-nrt-thermanode" classes="placeable,rotatable" grouppath="Environmentals;Legacy 5.1" filters="Environmentals">
		<display>
			<description>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors located on a left end of an aisle (facing the cold aisle).  Sense points are located at cabinet cold top (node), cold middle, cold bottom, cabinet end top, cabinet end mid/bottom, hot top and hot mid/bottom.</description>
			<name>LHS Endcap</name>
			<shape>triangle_l_red</shape>
		</display>
		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity">
				<value>5600.0</value>
			</property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status">
				<value>0</value>
			</property>
			<property name="platformId">
				<value>11</value>
			</property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold top temp</value>
					</property>
					<property name="channel">
						<value>0</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>1</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>humidity</value>
					</property>
					<property name="channel">
						<value>1</value>
					</property>
					<property name="min">
						<value>10.0</value>
					</property>
					<property name="max">
						<value>90.0</value>
					</property>
					<property name="aMin">
						<value>20.0</value>
					</property>
					<property name="aMax">
						<value>80.0</value>
					</property>
					<property name="rMin">
						<value>40.0</value>
					</property>
					<property name="rMax">
						<value>55.0</value>
					</property>
					<property name="type">
						<value>2</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>201</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>battery</value>
					</property>
					<property name="channel">
						<value>2</value>
					</property>
					<property name="min">
						<value>1.8</value>
					</property>
					<property name="max">
						<value>3.7</value>
					</property>
					<property name="aMin">
						<value>2.5</value>
					</property>
					<property name="aMax">
						<value>3.7</value>
					</property>
					<property name="rMin">
						<value>2.7</value>
					</property>
					<property name="rMax">
						<value>3.7</value>
					</property>
					<property name="type">
						<value>5</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>210</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold middle temp</value>
					</property>
					<property name="channel">
						<value>3</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>16384</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold bottom temp</value>
					</property>
					<property name="channel">
						<value>4</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>2048</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>hot top temp</value>
					</property>
					<property name="channel">
						<value>5</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>hot mid/bottom temp</value>
					</property>
					<property name="channel">
						<value>6</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>18432</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>end cap top temp</value>
					</property>
					<property name="channel">
						<value>7</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>end cap mid/bottom temp</value>
					</property>
					<property name="channel">
						<value>8</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>18432</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
			</property>
			<property name="batteryOperated">
				<value>1</value>
			</property>
			<property name="platformName">
				<value>ThermaNode</value>
			</property>
		</object>
		<object type="rack" as="rack">
			<dlid>DLID:-1</dlid>
			<property name="status">
				<value>1</value>
			</property>
		</object>
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true" dimension="">LHS Endcap</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true" dimension="">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">30.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">30.0</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true" dimension="">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true" dimension="">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true" dimension=""/>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		<property name="min_allow_h" display="Min Allowed Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">20.0</property>
		<property name="max_allow_h" display="Max Allowed Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">80.0</property>
		<property name="min_recommend_h" display="Min Recommended Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">40.0</property>
		<property name="max_recommend_h" display="Max Recommended Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">55.0</property>
		<property name="cold_delta" display="Cold Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">5</property>
		<property name="hot_delta" display="Hot Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">10</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true" dimension="">Thermanode endcap for LHS</property>
		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="x"]</property>
			<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="y"]</property>
			<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,width,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="x"]</property>
			<value>x - (width/2 * Math.cos(rotation+Math.PI/2))</value>
		</varbinding>
		<varbinding vars="y,width,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="y"]</property>
			<value>y - (width/2 * Math.sin(rotation+Math.PI/2))</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
			<property>$node/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
			<property>$node/properties[name="y"]</property>
			<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x">
			<property>$rack/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$rack/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="name">
			<property>$rack/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="rotation">
			<property>$rack/properties[name="rotation"]</property>
			<value>rotation</value>
		</varbinding>
		<varbinding vars="width">
			<property>$rack/properties[name="width"]</property>
			<value>width</value>
		</varbinding>
		<varbinding vars="depth">
			<property>$rack/properties[name="depth"]</property>
			<value>depth</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<property>$rack/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		<varbinding vars="min_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		<varbinding vars="min_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMin"]</property>
			<value>min_allow_h</value>
		</varbinding>
		<varbinding vars="max_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMax"]</property>
			<value>max_allow_h</value>
		</varbinding>
		<varbinding vars="min_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMin"]</property>
			<value>min_recommend_h</value>
		</varbinding>
		<varbinding vars="max_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMax"]</property>
			<value>max_recommend_h</value>
		</varbinding>

		<varbinding vars="cold_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(cold_delta)</value>
		</varbinding>
		<varbinding vars="hot_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(hot_delta)</value>
		</varbinding>
		<docking from="$rack/properties[name=&quot;cTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;0&quot;]]"/>
		<docking from="$rack/properties[name=&quot;rh&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;1&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;3&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;4&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;5&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;6&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;6&quot;]]"/>
		<macids value="mac_id"/>
	</component>
	
	
	<component type="rack-rhs-endcap-rt-thermanode" classes="placeable,rotatable" grouppath="Environmentals;Legacy 5.1" filters="Environmentals">
		<display>
			<description>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors located on a right end of an aisle (facing the cold aisle).  Sense points are located at cabinet cold top (node), cold middle, cold bottom, cabinet end top, cabinet end mid/bottom, hot top and hot mid/bottom.</description>
			<name>RHS endcap</name>
			<shape>triangle_r_orange</shape>
		</display>
		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity">
				<value>5600.0</value>
			</property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status">
				<value>0</value>
			</property>
			<property name="platformId">
				<value>11</value>
			</property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold top temp</value>
					</property>
					<property name="channel">
						<value>0</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>1</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>humidity</value>
					</property>
					<property name="channel">
						<value>1</value>
					</property>
					<property name="min">
						<value>10.0</value>
					</property>
					<property name="max">
						<value>90.0</value>
					</property>
					<property name="aMin">
						<value>20.0</value>
					</property>
					<property name="aMax">
						<value>80.0</value>
					</property>
					<property name="rMin">
						<value>40.0</value>
					</property>
					<property name="rMax">
						<value>55.0</value>
					</property>
					<property name="type">
						<value>2</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>201</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>battery</value>
					</property>
					<property name="channel">
						<value>2</value>
					</property>
					<property name="min">
						<value>1.8</value>
					</property>
					<property name="max">
						<value>3.7</value>
					</property>
					<property name="aMin">
						<value>2.5</value>
					</property>
					<property name="aMax">
						<value>3.7</value>
					</property>
					<property name="rMin">
						<value>2.7</value>
					</property>
					<property name="rMax">
						<value>3.7</value>
					</property>
					<property name="type">
						<value>5</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>210</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold middle temp</value>
					</property>
					<property name="channel">
						<value>3</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>16384</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold bottom temp</value>
					</property>
					<property name="channel">
						<value>4</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>2048</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>hot top temp</value>
					</property>
					<property name="channel">
						<value>5</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>hot mid/bottom temp</value>
					</property>
					<property name="channel">
						<value>6</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>18432</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>end cap top temp</value>
					</property>
					<property name="channel">
						<value>7</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>end cap bottom temp</value>
					</property>
					<property name="channel">
						<value>8</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>18432</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
			</property>
			<property name="batteryOperated">
				<value>1</value>
			</property>
			<property name="platformName">
				<value>ThermaNode</value>
			</property>
		</object>
		<object type="rack" as="rack">
			<dlid>DLID:-1</dlid>
			<property name="status">
				<value>1</value>
			</property>
		</object>
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true" dimension="">RHS Endcap</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true" dimension="">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">30.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">30.0</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true" dimension="">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true" dimension="">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true" dimension=""/>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		<property name="min_allow_h" display="Min Allowed Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">20.0</property>
		<property name="max_allow_h" display="Max Allowed Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">80.0</property>
		<property name="min_recommend_h" display="Min Recommended Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">40.0</property>
		<property name="max_recommend_h" display="Max Recommended Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">55.0</property>
		<property name="cold_delta" display="Cold Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">5</property>
		<property name="hot_delta" display="Hot Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">10</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true" dimension="">Thermanode endcap for RHS</property>

		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

		<consumer id="reftemp" datatype="reftemp" name="Rack Ref Temp" property="$rack/properties[name=&quot;ref&quot;]" required="true"/>
		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="x"]</property>
			<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="y"]</property>
			<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,width,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="x"]</property>
			<value>x + (width/2 * Math.cos(rotation+Math.PI/2))</value>
		</varbinding>
		<varbinding vars="y,width,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="y"]</property>
			<value>y + (width/2 * Math.sin(rotation+Math.PI/2))</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
			<property>$node/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
			<property>$node/properties[name="y"]</property>
			<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x">
			<property>$rack/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$rack/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="name">
			<property>$rack/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="rotation">
			<property>$rack/properties[name="rotation"]</property>
			<value>rotation</value>
		</varbinding>
		<varbinding vars="width">
			<property>$rack/properties[name="width"]</property>
			<value>width</value>
		</varbinding>
		<varbinding vars="depth">
			<property>$rack/properties[name="depth"]</property>
			<value>depth</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<property>$rack/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		<varbinding vars="min_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		<varbinding vars="min_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMin"]</property>
			<value>min_allow_h</value>
		</varbinding>
		<varbinding vars="max_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMax"]</property>
			<value>max_allow_h</value>
		</varbinding>
		<varbinding vars="min_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMin"]</property>
			<value>min_recommend_h</value>
		</varbinding>
		<varbinding vars="max_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMax"]</property>
			<value>max_recommend_h</value>
		</varbinding>

		<varbinding vars="cold_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(cold_delta)</value>
		</varbinding>
		<varbinding vars="hot_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(hot_delta)</value>
		</varbinding>
		<docking from="$rack/properties[name=&quot;cTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;0&quot;]]"/>
		<docking from="$rack/properties[name=&quot;rh&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;1&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;3&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;4&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;5&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;6&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;6&quot;]]"/>
		<macids value="mac_id"/>
	</component>
	
	
	<component type="rack-rhs-endcap-nrt-thermanode" classes="placeable,rotatable" grouppath="Environmentals;Legacy 5.1" filters="Environmentals">
		<display>
			<description>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors located on a right end of an aisle (facing the cold aisle).  Sense points are located at cabinet cold top (node), cold middle, cold bottom, cabinet end top, cabinet end mid/bottom, hot top and hot mid/bottom.</description>
			<name>RHS Endcap</name>
			<shape>triangle_r_red</shape>
		</display>
		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity">
				<value>5600.0</value>
			</property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status">
				<value>0</value>
			</property>
			<property name="platformId">
				<value>11</value>
			</property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold top temp</value>
					</property>
					<property name="channel">
						<value>0</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>1</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>humidity</value>
					</property>
					<property name="channel">
						<value>1</value>
					</property>
					<property name="min">
						<value>10.0</value>
					</property>
					<property name="max">
						<value>90.0</value>
					</property>
					<property name="aMin">
						<value>20.0</value>
					</property>
					<property name="aMax">
						<value>80.0</value>
					</property>
					<property name="rMin">
						<value>40.0</value>
					</property>
					<property name="rMax">
						<value>55.0</value>
					</property>
					<property name="type">
						<value>2</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>201</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>battery</value>
					</property>
					<property name="channel">
						<value>2</value>
					</property>
					<property name="min">
						<value>1.8</value>
					</property>
					<property name="max">
						<value>3.7</value>
					</property>
					<property name="aMin">
						<value>2.5</value>
					</property>
					<property name="aMax">
						<value>3.7</value>
					</property>
					<property name="rMin">
						<value>2.7</value>
					</property>
					<property name="rMax">
						<value>3.7</value>
					</property>
					<property name="type">
						<value>5</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>210</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold middle temp</value>
					</property>
					<property name="channel">
						<value>3</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>16384</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>cold bottom temp</value>
					</property>
					<property name="channel">
						<value>4</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>2048</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>hot top temp</value>
					</property>
					<property name="channel">
						<value>5</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>hot mid/bottom temp</value>
					</property>
					<property name="channel">
						<value>6</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>18432</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>end cap top temp</value>
					</property>
					<property name="channel">
						<value>7</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>131072</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>end cap bottom temp</value>
					</property>
					<property name="channel">
						<value>8</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>18432</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
			</property>
			<property name="batteryOperated">
				<value>1</value>
			</property>
			<property name="platformName">
				<value>ThermaNode</value>
			</property>
		</object>
		<object type="rack" as="rack">
			<dlid>DLID:-1</dlid>
			<property name="status">
				<value>1</value>
			</property>
		</object>
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true" dimension="">RHS Endcap</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true" dimension="">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">30.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">30.0</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true" dimension="">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true" dimension="">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true" dimension=""/>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		<property name="min_allow_h" display="Min Allowed Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">20.0</property>
		<property name="max_allow_h" display="Max Allowed Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">80.0</property>
		<property name="min_recommend_h" display="Min Recommended Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">40.0</property>
		<property name="max_recommend_h" display="Max Recommended Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">55.0</property>
		<property name="cold_delta" display="Cold Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">5</property>
		<property name="hot_delta" display="Hot Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">10</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true" dimension="">Thermanode endcap for RHS</property>
		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="x"]</property>
			<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="y"]</property>
			<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,width,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="x"]</property>
			<value>x + (width/2 * Math.cos(rotation+Math.PI/2))</value>
		</varbinding>
		<varbinding vars="y,width,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="y"]</property>
			<value>y + (width/2 * Math.sin(rotation+Math.PI/2))</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
			<property>$node/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
			<property>$node/properties[name="y"]</property>
			<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x">
			<property>$rack/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$rack/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="name">
			<property>$rack/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="rotation">
			<property>$rack/properties[name="rotation"]</property>
			<value>rotation</value>
		</varbinding>
		<varbinding vars="width">
			<property>$rack/properties[name="width"]</property>
			<value>width</value>
		</varbinding>
		<varbinding vars="depth">
			<property>$rack/properties[name="depth"]</property>
			<value>depth</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<property>$rack/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		<varbinding vars="min_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		<varbinding vars="min_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMin"]</property>
			<value>min_allow_h</value>
		</varbinding>
		<varbinding vars="max_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMax"]</property>
			<value>max_allow_h</value>
		</varbinding>
		<varbinding vars="min_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMin"]</property>
			<value>min_recommend_h</value>
		</varbinding>
		<varbinding vars="max_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMax"]</property>
			<value>max_recommend_h</value>
		</varbinding>

		<varbinding vars="cold_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(cold_delta)</value>
		</varbinding>
		<varbinding vars="hot_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(hot_delta)</value>
		</varbinding>
		<docking from="$rack/properties[name=&quot;cTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;0&quot;]]"/>
		<docking from="$rack/properties[name=&quot;rh&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;1&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;3&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;4&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;5&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;6&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;6&quot;]]"/>
		<macids value="mac_id"/>
	</component>


	<component type="37li-thermanode" classes="placeable" grouppath="Environmentals;Legacy 5.1" filters="Environmentals">
	      <display>
		    <description>A ThermaNode and external sensor assembly that consists of a 3 ft and a 7 ft. thermistor. Sense points will appear in LI images on Top, Middle and Bottom layers.</description>
		    <name>3-7 LI Thermanode</name>
		    <shape>node-li</shape>
	      </display>
	      <object type="wsnnode" as="node">
		    <dlid>DLID:-1</dlid>
		    <property name="batteryCapacity">
			  <value>5600.0</value>
		    </property>
		    <property name="batteryStatus"><value>0</value></property>
		    <property name="status">
			  <value>0</value>
		    </property>
		    <property name="platformId">
			  <value>11</value>
		    </property>
		    <property name="sensepoints">
			  <object type="wsnsensor">
				<dlid>DLID:-1</dlid>
				<property name="name">
				      <value>node temp</value>
				</property>
				<property name="channel">
				      <value>0</value>
				</property>
				<property name="min">
				      <value>40.0</value>
				</property>
				<property name="max">
				      <value>100.0</value>
				</property>
				<property name="type">
				      <value>1</value>
				</property>
				<property name="z">
				      <value>131072</value>
				</property>
				<property name="status">
				      <value>1</value>
				</property>
				<property name="lastValue">
				      <value>-5000.0</value>
				</property>
				<property name="dataclass">
				      <value>200</value>
				</property>
				<property name="enabled">
				      <value>1</value>
				</property>
				<property name="mode">
				      <value>instant</value>
				</property>
				<property name="interval">
				      <value>0</value>
				</property>
				<property name="smartSendThreshold">
				      <value/>
				</property>
			  </object>
			  <object type="wsnsensor">
				<dlid>DLID:-1</dlid>
				<property name="name">
				      <value>humidity</value>
				</property>
				<property name="channel">
				      <value>1</value>
				</property>
				<property name="min">
				      <value>10.0</value>
				</property>
				<property name="max">
				      <value>90.0</value>
				</property>
				<property name="type">
				      <value>2</value>
				</property>
				<property name="z">
				      <value>131072</value>
				</property>
				<property name="status">
				      <value>1</value>
				</property>
				<property name="lastValue">
				      <value>-5000.0</value>
				</property>
				<property name="dataclass">
				      <value>201</value>
				</property>
				<property name="enabled">
				      <value>1</value>
				</property>
				<property name="mode">
				      <value>instant</value>
				</property>
				<property name="interval">
				      <value>0</value>
				</property>
				<property name="smartSendThreshold">
				      <value/>
				</property>
			  </object>
			  <object type="wsnsensor">
				<dlid>DLID:-1</dlid>
				<property name="name">
				      <value>battery</value>
				</property>
				<property name="channel">
				      <value>2</value>
				</property>
				<property name="min">
				      <value>1.8</value>
				</property>
				<property name="max">
				      <value>3.7</value>
				</property>
				<property name="aMin">
				      <value>2.5</value>
				</property>
				<property name="aMax">
				      <value>3.7</value>
				</property>
				<property name="rMin">
				      <value>2.7</value>
				</property>
				<property name="rMax">
				      <value>3.7</value>
				</property>
				<property name="type">
				      <value>5</value>
				</property>
				<property name="z">
				      <value>0</value>
				</property>
				<property name="status">
				      <value>1</value>
				</property>
				<property name="lastValue">
				      <value>-5000.0</value>
				</property>
				<property name="dataclass">
				      <value>210</value>
				</property>
				<property name="enabled">
				      <value>1</value>
				</property>
				<property name="mode">
				      <value>instant</value>
				</property>
				<property name="interval">
				      <value>0</value>
				</property>
				<property name="smartSendThreshold">
				      <value/>
				</property>
			  </object>
			  <object type="wsnsensor">
				<dlid>DLID:-1</dlid>
				<property name="name">
				      <value>middle temp</value>
				</property>
				<property name="channel">
				      <value>3</value>
				</property>
				<property name="min">
				      <value>40.0</value>
				</property>
				<property name="max">
				      <value>100.0</value>
				</property>
				<property name="type">
				      <value>27</value>
				</property>
				<property name="z">
				      <value>16384</value>
				</property>
				<property name="status">
				      <value>1</value>
				</property>
				<property name="lastValue">
				      <value>-5000.0</value>
				</property>
				<property name="dataclass">
				      <value>200</value>
				</property>
				<property name="enabled">
				      <value>1</value>
				</property>
				<property name="mode">
				      <value>instant</value>
				</property>
				<property name="interval">
				      <value>0</value>
				</property>
				<property name="smartSendThreshold">
				      <value/>
				</property>
			  </object>
			  <object type="wsnsensor">
				<dlid>DLID:-1</dlid>
				<property name="name">
				      <value>bottom temp</value>
				</property>
				<property name="channel">
				      <value>4</value>
				</property>
				<property name="min">
				      <value>40.0</value>
				</property>
				<property name="max">
				      <value>100.0</value>
				</property>
				<property name="type">
				      <value>27</value>
				</property>
				<property name="z">
				      <value>2048</value>
				</property>
				<property name="status">
				      <value>1</value>
				</property>
				<property name="lastValue">
				      <value>-5000.0</value>
				</property>
				<property name="dataclass">
				      <value>200</value>
				</property>
				<property name="enabled">
				      <value>1</value>
				</property>
				<property name="mode">
				      <value>instant</value>
				</property>
				<property name="interval">
				      <value>0</value>
				</property>
				<property name="smartSendThreshold">
				      <value/>
				</property>
			  </object>
		    </property>
		    <property name="batteryOperated">
			  <value>1</value>
		    </property>
		    <property name="platformName">
			  <value>ThermaNode</value>
		    </property>
	      </object>
	      <property name="name" display="Name" type="java.lang.String" editable="true" displayed="true" dimension="">3-7 Thermanode</property>
	      <property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
	      <property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
	      <property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true" dimension="">0</property>
	      <property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true" dimension="">5</property>
	      <property name="location" display="Location" type="java.lang.String" editable="true" displayed="true" dimension=""/>
	      <property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true" dimension="">3-7 LI Thermanode</property>
		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>
	      <varbinding vars="name">
		    <property>$node/properties[name="name"]</property>
		    <value>'Node ' + name</value>
	      </varbinding>
	      <varbinding vars="x">
		    <property>$node/properties[name="x"]</property>
		    <property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
		    <property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
		    <property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
		    <property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
		    <property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
		    <value>x</value>
	      </varbinding>
	      <varbinding vars="y">
		    <property>$node/properties[name="y"]</property>
		    <property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
		    <property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
		    <property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
		    <property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
		    <property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
		    <value>y</value>
	      </varbinding>
	      <varbinding vars="mac_id">
		    <property>$node/properties[name="mac"]</property>
		    <value>Long.parseLong(mac_id,16)</value>
	      </varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
	      <varbinding vars="sample_interval">
		    <property>$node/properties[name="period"]</property>
		    <value>sample_interval + ' min'</value>
	      </varbinding>
	      <varbinding vars="location">
		    <property>$node/properties[name="location"]</property>
		    <value>location</value>
	      </varbinding>
	      <macids value="mac_id"/>
	</component>

</componentlib>

