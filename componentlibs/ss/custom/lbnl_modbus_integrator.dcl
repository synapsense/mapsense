<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
	name="Stanford PUE Metering Modbus integration object"
	version="&componentlib.version;"
	cversion="&componentlib.cversion;"
	oversion="&componentlib.oversion;"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
	>

	<component type="lbnl-modbus-integrator" classes="placeable"  grouppath="Customer Specific" filters="Calculations">
		<display>
			<description>PUE Metering Modbus integration object</description>
			<name>PUE Metering Modbus Integrator</name>
			<shape>star</shape>
		</display>

		<object type="modbusdevice" as="device">
			<dlid>DLID:-1</dlid>
			<property name="maxpacketlen"><value>42</value></property>
			<property name="registers">
				
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Blank2</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40002</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Blank4</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40004</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Blank6</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40006</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk1-PDU1-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40008</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk1-PDU1-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40010</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk1-PDU2-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40012</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk1-PDU2-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40014</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk1-PDU3-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40016</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk1-PDU3-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40018</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk2-PDU1-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40020</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk2-PDU1-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40022</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk2-PDU2-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40024</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk2-PDU2-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40026</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk2-PDU3-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40028</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk2-PDU3-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40030</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk3-PDU1-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40032</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk3-PDU1-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40034</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk3-PDU2-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40036</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk3-PDU2-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40038</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk3-PDU3-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40040</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk3-PDU3-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40042</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Blank1</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40001</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Blank3</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40003</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Blank5</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40005</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk4-PDU1-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40007</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk4-PDU1-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40009</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk4-PDU2-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40011</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk4-PDU2-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40013</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk4-PDU3-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40015</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk4-PDU3-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40017</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk5-PDU1-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40019</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk5-PDU1-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40021</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk5-PDU2-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40023</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk5-PDU2-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40025</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk5-PDU3-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40027</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk5-PDU3-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40029</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk6-PDU1-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40031</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk6-PDU1-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40033</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk6-PDU2-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40035</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk6-PDU2-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40037</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk6-PDU3-PhA-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40039</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Rk6-PDU3-PhB-Amps</value></property>
					<property name="scale"><value>100</value></property>
					<property name="id"><value>40041</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>

			</property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Modbus Integrator</property>
		<property name="devid" display="ModBus ID" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="pullPeriod" display="Sampling Interval (minutes)" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Modbus Integrator</property>

		<producer id="modbusproducer1" datatype="power" name="Blank2" object='$device/properties[name="registers"]/children[properties[name="name"][value="Blank2"]]' />
		<producer id="modbusproducer2" datatype="power" name="Blank4" object='$device/properties[name="registers"]/children[properties[name="name"][value="Blank4"]]' />
		<producer id="modbusproducer3" datatype="power" name="Blank6" object='$device/properties[name="registers"]/children[properties[name="name"][value="Blank6"]]' />
		<producer id="modbusproducer4" datatype="power" name="Rk1-PDU1-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk1-PDU1-PhA-Amps"]]' />
		<producer id="modbusproducer5" datatype="power" name="Rk1-PDU1-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk1-PDU1-PhB-Amps"]]' />
		<producer id="modbusproducer6" datatype="power" name="Rk1-PDU2-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk1-PDU2-PhA-Amps"]]' />
		<producer id="modbusproducer7" datatype="power" name="Rk1-PDU2-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk1-PDU2-PhB-Amps"]]' />
		<producer id="modbusproducer8" datatype="power" name="Rk1-PDU3-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk1-PDU3-PhA-Amps"]]' />
		<producer id="modbusproducer9" datatype="power" name="Rk1-PDU3-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk1-PDU3-PhB-Amps"]]' />
		<producer id="modbusproducer10" datatype="power" name="Rk2-PDU1-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk2-PDU1-PhA-Amps"]]' />
		<producer id="modbusproducer11" datatype="power" name="Rk2-PDU1-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk2-PDU1-PhB-Amps"]]' />
		<producer id="modbusproducer12" datatype="power" name="Rk2-PDU2-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk2-PDU2-PhA-Amps"]]' />
		<producer id="modbusproducer13" datatype="power" name="Rk2-PDU2-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk2-PDU2-PhB-Amps"]]' />
		<producer id="modbusproducer14" datatype="power" name="Rk2-PDU3-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk2-PDU3-PhA-Amps"]]' />
		<producer id="modbusproducer15" datatype="power" name="Rk2-PDU3-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk2-PDU3-PhB-Amps"]]' />
		<producer id="modbusproducer16" datatype="power" name="Rk3-PDU1-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk3-PDU1-PhA-Amps"]]' />
		<producer id="modbusproducer17" datatype="power" name="Rk3-PDU1-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk3-PDU1-PhB-Amps"]]' />
		<producer id="modbusproducer18" datatype="power" name="Rk3-PDU2-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk3-PDU2-PhA-Amps"]]' />
		<producer id="modbusproducer19" datatype="power" name="Rk3-PDU2-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk3-PDU2-PhB-Amps"]]' />
		<producer id="modbusproducer20" datatype="power" name="Rk3-PDU3-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk3-PDU3-PhA-Amps"]]' />
		<producer id="modbusproducer21" datatype="power" name="Rk3-PDU3-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk3-PDU3-PhB-Amps"]]' />
		<producer id="modbusproducer22" datatype="power" name="Blank1" object='$device/properties[name="registers"]/children[properties[name="name"][value="Blank1"]]' />
		<producer id="modbusproducer23" datatype="power" name="Blank3" object='$device/properties[name="registers"]/children[properties[name="name"][value="Blank3"]]' />
		<producer id="modbusproducer24" datatype="power" name="Blank5" object='$device/properties[name="registers"]/children[properties[name="name"][value="Blank5"]]' />
		<producer id="modbusproducer25" datatype="power" name="Rk4-PDU1-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk4-PDU1-PhA-Amps"]]' />
		<producer id="modbusproducer26" datatype="power" name="Rk4-PDU1-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk4-PDU1-PhB-Amps"]]' />
		<producer id="modbusproducer27" datatype="power" name="Rk4-PDU2-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk4-PDU2-PhA-Amps"]]' />
		<producer id="modbusproducer28" datatype="power" name="Rk4-PDU2-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk4-PDU2-PhB-Amps"]]' />
		<producer id="modbusproducer29" datatype="power" name="Rk4-PDU3-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk4-PDU3-PhA-Amps"]]' />
		<producer id="modbusproducer30" datatype="power" name="Rk4-PDU3-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk4-PDU3-PhB-Amps"]]' />
		<producer id="modbusproducer31" datatype="power" name="Rk5-PDU1-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk5-PDU1-PhA-Amps"]]' />
		<producer id="modbusproducer32" datatype="power" name="Rk5-PDU1-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk5-PDU1-PhB-Amps"]]' />
		<producer id="modbusproducer33" datatype="power" name="Rk5-PDU2-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk5-PDU2-PhA-Amps"]]' />
		<producer id="modbusproducer34" datatype="power" name="Rk5-PDU2-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk5-PDU2-PhB-Amps"]]' />
		<producer id="modbusproducer35" datatype="power" name="Rk5-PDU3-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk5-PDU3-PhA-Amps"]]' />
		<producer id="modbusproducer36" datatype="power" name="Rk5-PDU3-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk5-PDU3-PhB-Amps"]]' />
		<producer id="modbusproducer37" datatype="power" name="Rk6-PDU1-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk6-PDU1-PhA-Amps"]]' />
		<producer id="modbusproducer38" datatype="power" name="Rk6-PDU1-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk6-PDU1-PhB-Amps"]]' />
		<producer id="modbusproducer39" datatype="power" name="Rk6-PDU2-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk6-PDU2-PhA-Amps"]]' />
		<producer id="modbusproducer40" datatype="power" name="Rk6-PDU2-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk6-PDU2-PhB-Amps"]]' />
		<producer id="modbusproducer41" datatype="power" name="Rk6-PDU3-PhA-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk6-PDU3-PhA-Amps"]]' />
		<producer id="modbusproducer42" datatype="power" name="Rk6-PDU3-PhB-Amps" object='$device/properties[name="registers"]/children[properties[name="name"][value="Rk6-PDU3-PhB-Amps"]]' />

		<varbinding vars="name">
			<property>$device/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="devid">
			<property>$device/properties[name="id"]</property>
			<value>devid</value>
		</varbinding>
		<varbinding vars="pullPeriod">
			<property>$device/properties[name="registers"]/children/properties[name="lastValue"]/tags[tagName='poll']</property>
			<value>pullPeriod*60000</value>
		</varbinding>
	</component>
</componentlib>
