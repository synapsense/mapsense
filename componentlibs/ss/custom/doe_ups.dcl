<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
	name="DOE UPS Modbus integration object"
	version="&componentlib.version;"
	cversion="&componentlib.cversion;"
	oversion="&componentlib.oversion;"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
	>

	<component type="doe-ups-modbus-integrator" classes="placeable" grouppath="Customer Specific" filters="Calculations">
		<display>
			<description>DOE UPS Modbus integration object</description>
			<name>DOE UPS Modbus Integrator</name>
			<shape>star</shape>
		</display>

		<object type="modbusdevice" as="device">
			<dlid>DLID:-1</dlid>
			<property name="maxpacketlen"><value>42</value></property>
			<property name="registers">
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>output kw</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>30022</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>input kw</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>30023</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
			</property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">DOE UPS Modbus Integrator</property>
		<property name="devid" display="ModBus ID" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="pullPeriod" display="Sampling Interval (minutes)" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">DOE UPS Modbus Integrator</property>

		<producer id="outkw" datatype="power" name="Output kW" object='$device/properties[name="registers"]/children[properties[name="name"][value="output kw"]]' />
		<producer id="inkw" datatype="power" name="Input kW" object='$device/properties[name="registers"]/children[properties[name="name"][value="input kw"]]' />

		<varbinding vars="name">
			<property>$device/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="devid">
			<property>$device/properties[name="id"]</property>
			<value>devid</value>
		</varbinding>
		<varbinding vars="pullPeriod">
			<!--
			<property>$device/properties[name="registers"]/children/properties[name="period"]</property>
			<value>pullPeriod*60.0</value>
			-->
			<property>$device/properties[name="registers"]/children/properties[name="lastValue"]/tags[tagName='poll']</property>
			<value>pullPeriod * 60000</value>
		</varbinding>
	</component>
</componentlib>

