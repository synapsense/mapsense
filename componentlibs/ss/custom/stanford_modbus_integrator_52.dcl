<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
	name="Stanford PUE Metering Modbus integration object"
	version="&componentlib.version;"
	cversion="&componentlib.cversion;"
	oversion="&componentlib.oversion;"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
	>

	<component type="stanford-pue-metering-modbus-integrator" classes="placeable"  grouppath="Customer Specific" filters="Calculations">
		<display>
			<description>PUE Metering Modbus integration object</description>
			<name>PUE Metering Modbus Integrator</name>
			<shape>star</shape>
		</display>

		<object type="modbusdevice" as="device">
			<dlid>DLID:-1</dlid>
			<property name="maxpacketlen"><value>42</value></property>
			<property name="registers">
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Utility Service 1</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40001</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Utility Service 2</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40002</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Utility Service 3</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40002</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-R1 Input PF</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40077</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>				
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-R1 Input Phase A</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40060</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-R1 Input Phase B</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40061</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-R1 Input Phase C</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40062</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-R1 Output</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40004</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-R2 Input PF</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40077</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-R2 Input Phase A</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40063</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-R2 Input Phase B</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40064</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-R2 Input Phase C</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40065</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-R2 Output</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40005</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-1 Output</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40006</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-2 Input PF</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40076</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-2 Input Phase A</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40069</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-2 Input Phase B</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40070</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-2 Input Phase C</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40071</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-2 Output</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40007</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-3 Input PF</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40075</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-3 Input Phase A</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40072</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-3 Input Phase B</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40073</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-3 Input Phase C</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40074</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>UPS-3 Output</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40008</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>FDC-A (PDU-1R 2R)</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40055</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>FDC-B (PDU-1R 2R)</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40056</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>FDC-C (PDU-1R 2R)</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40057</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>FDC-D (PDU-1R 2R)</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40058</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>FDC-D25 (PDU-15 16)</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40050</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>FDC-H25 (PDU-15 16)</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40051</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>FDC-Q25 (PDU-15 16)</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40052</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>FDC-T24 (PDU-15 16)</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40053</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>FDC-T8 (PDU-15 16)</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40054</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>FDC-E (PDU-1R 2R)</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40059</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>PDU-05 Output</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40009</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>PDU-06 Output</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40010</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>PDU-07 Output</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40011</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>PDU-08 Output</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40012</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>PDU-09 Output</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40013</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>PDU-10 Output</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40014</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>PDU-11 Output</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40015</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>PDU-12 Output</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40016</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>PDU-13 Output</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40017</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>PDU-14 Output</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>40018</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>AHU-4 SF Fan Energy</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40045</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>AHU-4 RF Fan Energy</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40046</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>SF-2A Fan Energy</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40020</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>SF-2B Fan Energy</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40021</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>SF-2C Fan Energy</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40022</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>SF-S1 Fan Energy</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40023</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>EF-1 Fan Energy</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40024</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>SF-1 Fan Energy</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40025</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>SF-2 Fan Energy</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40026</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>RF-1 Fan Energy</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40027</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>RF-2 Fan Energy</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40028</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>SF-3 Fan Energy</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40029</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>RF-3 Fan Energy</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40030</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Main Bldg CHW Flow</value></property>
					<property name="scale"><value>100.0</value></property>
					<property name="id"><value>40032</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Main Bldg CHWS Temp</value></property>
					<property name="scale"><value>100.0</value></property>
					<property name="id"><value>40033</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Main Bldg CHWR Temp</value></property>
					<property name="scale"><value>100.0</value></property>
					<property name="id"><value>40034</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Annex Bldg CHW Flow</value></property>
					<property name="scale"><value>100.0</value></property>
					<property name="id"><value>40036</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Annex Bldg CHWS Temp</value></property>
					<property name="scale"><value>100.0</value></property>
					<property name="id"><value>40037</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Annex Bldg CHWR Temp</value></property>
					<property name="scale"><value>100.0</value></property>
					<property name="id"><value>40038</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>DC CRAH CHW Flow</value></property>
					<property name="scale"><value>100.0</value></property>
					<property name="id"><value>40040</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>DC CRAH CHWS Temp</value></property>
					<property name="scale"><value>100.0</value></property>
					<property name="id"><value>40041</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>DC CRAH CHWR Temp</value></property>
					<property name="scale"><value>100.0</value></property>
					<property name="id"><value>40042</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>	
			</property>
		</object>			
		
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Modbus Integrator</property>
		<property name="devid" display="ModBus ID" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="pullPeriod" display="Sampling Interval (minutes)" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Modbus Integrator</property>

		<producer id="utilityservice1" datatype="power" name="Utility Service 1 (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="Utility Service 1"]]' />
		<producer id="utilityservice2" datatype="power" name="Utility Service 2 (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="Utility Service 2"]]' />
		<producer id="utilityservice3" datatype="power" name="Utility Service 3 (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="Utility Service 3"]]' />
		<producer id="ups_r1inputpf" datatype="power" name="UPS-R1 Input PF (kW/kVA)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-R1 Input PF"]]' />
		<producer id="ups_r1inputphasea" datatype="power" name="UPS-R1 Input Phase A (A)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-R1 Input Phase A"]]' />
		<producer id="ups_r1inputphaseb" datatype="power" name="UPS-R1 Input Phase B (A)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-R1 Input Phase B"]]' />
		<producer id="ups_r1inputphasec" datatype="power" name="UPS-R1 Input Phase C (A)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-R1 Input Phase C"]]' />
		<producer id="ups_r1output" datatype="power" name="UPS-R1 Output (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-R1 Output"]]' />
		<producer id="ups_r2inputpf" datatype="power" name="UPS-R2 Input PF (kW/kVA)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-R2 Input PF"]]' />
		<producer id="ups_r2inputphasea" datatype="power" name="UPS-R2 Input Phase A (A)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-R2 Input Phase A"]]' />
		<producer id="ups_r2inputphaseb" datatype="power" name="UPS-R2 Input Phase B (A)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-R2 Input Phase B"]]' />
		<producer id="ups_r2inputphasec" datatype="power" name="UPS-R2 Input Phase C (A)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-R2 Input Phase C"]]' />
		<producer id="ups_r2output" datatype="power" name="UPS-R2 Output (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-R2 Output"]]' />
		<producer id="ups_1output" datatype="power" name="UPS-1 Output (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-1 Output"]]' />
		<producer id="ups_2inputpf" datatype="power" name="UPS-2 Input PF (kW/kVA)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-2 Input PF"]]' />
		<producer id="ups_2inputphasea" datatype="power" name="UPS-2 Input Phase A (A)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-2 Input Phase A"]]' />
		<producer id="ups_2inputphaseb" datatype="power" name="UPS-2 Input Phase B (A)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-2 Input Phase B"]]' />
		<producer id="ups_2inputphasec" datatype="power" name="UPS-2 Input Phase C (A)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-2 Input Phase C"]]' />
		<producer id="ups_2output" datatype="power" name="UPS-2 Output (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-2 Output"]]' />
		<producer id="ups_3inputpf" datatype="power" name="UPS-3 Input PF (kW/kVA)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-3 Input PF"]]' />
		<producer id="ups_3inputphasea" datatype="power" name="UPS-3 Input Phase A (A)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-3 Input Phase A"]]' />
		<producer id="ups_3inputphaseb" datatype="power" name="UPS-3 Input Phase B (A)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-3 Input Phase B"]]' />
		<producer id="ups_3inputphasec" datatype="power" name="UPS-3 Input Phase C (A)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-3 Input Phase C"]]' />
		<producer id="ups_3output" datatype="power" name="UPS-3 Output (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="UPS-3 Output"]]' />
		<producer id="fdc_a_pdu_1r2r" datatype="power" name="FDC-A (PDU-1R 2R) (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="FDC-A (PDU-1R 2R)"]]' />
		<producer id="fdc_b-pdu_1r2r" datatype="power" name="FDC-B (PDU-1R 2R) (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="FDC-B (PDU-1R 2R)"]]' />
		<producer id="fdc_c_pdu_1r2r" datatype="power" name="FDC-C (PDU-1R 2R) (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="FDC-C (PDU-1R 2R)"]]' />
		<producer id="fdc_d_pdu_1r2r" datatype="power" name="FDC-D (PDU-1R 2R) (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="FDC-D (PDU-1R 2R)"]]' />
		<producer id="fdc_d25_pdu_1516" datatype="power" name="FDC-D25 (PDU-15 16) (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="FDC-D25 (PDU-15 16)"]]' />
		<producer id="fdc_h25_pdu_1516" datatype="power" name="FDC-H25 (PDU-15 16) (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="FDC-H25 (PDU-15 16)"]]' />
		<producer id="fdc_q25_pdu_1516" datatype="power" name="FDC-Q25 (PDU-15 16) (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="FDC-Q25 (PDU-15 16)"]]' />
		<producer id="fdc_t24_pdu_1516" datatype="power" name="FDC-T24 (PDU-15 16) (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="FDC-T24 (PDU-15 16)"]]' />
		<producer id="fdc_t8_pdu_1516" datatype="power" name="FDC-T8 (PDU-15 16) (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="FDC-T8 (PDU-15 16)"]]' />
		<producer id="fdc_e_pdu_1516" datatype="power" name="FDC-E  (PDU-1R 2R) (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="FDC-E (PDU-1R 2R)"]]' />
		<producer id="pdu_05output" datatype="power" name="PDU-05 Output (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="PDU-05 Output"]]' />
		<producer id="pdu_06output" datatype="power" name="PDU-06 Output (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="PDU-06 Output"]]' />
		<producer id="pdu_07output" datatype="power" name="PDU-07 Output (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="PDU-07 Output"]]' />
		<producer id="pdu_08output" datatype="power" name="PDU-08 Output (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="PDU-08 Output"]]' />
		<producer id="pdu_09output" datatype="power" name="PDU-09 Output (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="PDU-09 Output"]]' />
		<producer id="pdu_10output" datatype="power" name="PDU-10 Output (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="PDU-10 Output"]]' />
		<producer id="pdu_11output" datatype="power" name="PDU-11 Output (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="PDU-11 Output"]]' />
		<producer id="pdu_12output" datatype="power" name="PDU-12 Output (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="PDU-12 Output"]]' />
		<producer id="pdu_13output" datatype="power" name="PDU-13 Output (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="PDU-13 Output"]]' />
		<producer id="pdu_14output" datatype="power" name="PDU-14 Output (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="PDU-14 Output"]]' />
		<producer id="ahu_4sffanenergy" datatype="power" name="AHU-4 SF Fan Energy (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="AHU-4 SF Fan Energy"]]' />
		<producer id="ahu_4rffanenergy" datatype="power" name="AHU-4 RF Fan Energy (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="AHU-4 RF Fan Energy"]]' />
		<producer id="sf_2afanenergy" datatype="power" name="SF-2A Fan Energy (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="SF-2A Fan Energy"]]' />
		<producer id="sf_2bfanenergy" datatype="power" name="SF-2B Fan Energy (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="SF-2B Fan Energy"]]' />
		<producer id="sf_2cfanenergy" datatype="power" name="SF-2C Fan Energy (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="SF-2C Fan Energy"]]' />
		<producer id="sf_s1fanenergy" datatype="power" name="SF-S1 Fan Energy (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="SF-S1 Fan Energy"]]' />
		<producer id="ef_1fanenergy" datatype="power" name="EF-1 Fan Energy (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="EF-1 Fan Energy"]]' />
		<producer id="sf_1fanenergy" datatype="power" name="SF-1 Fan Energy (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="SF-1 Fan Energy"]]' />
		<producer id="sf_2fanenergy" datatype="power" name="SF-2 Fan Energy (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="SF-2 Fan Energy"]]' />
		<producer id="rf-1fanenergy" datatype="power" name="RF-1 Fan Energy (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="RF-1 Fan Energy"]]' />
		<producer id="rf_2fanenergy" datatype="power" name="RF-2 Fan Energy (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="RF-2 Fan Energy"]]' />
		<producer id="sf_3fanenergy" datatype="power" name="SF-3 Fan Energy (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="SF-3 Fan Energy"]]' />
		<producer id="rf_3fanenergy" datatype="power" name="RF-3 Fan Energy (kW)" object='$device/properties[name="registers"]/children[properties[name="name"][value="RF-3 Fan Energy"]]' />
		<producer id="mainbldgchwflow" datatype="power" name="Main Bldg CHW Flow (GPM)" object='$device/properties[name="registers"]/children[properties[name="name"][value="Main Bldg CHW Flow"]]' />
		<producer id="mainbldgchwstemp" datatype="power" name="Main Bldg CHWS Temp (degF)" object='$device/properties[name="registers"]/children[properties[name="name"][value="Main Bldg CHWS Temp"]]' />
		<producer id="mainbldgchwrtemp" datatype="power" name="Main Bldg CHWR Temp (degF)" object='$device/properties[name="registers"]/children[properties[name="name"][value="Main Bldg CHWR Temp"]]' />
		<producer id="annexbldgchwflow" datatype="power" name="Annex Bldg CHW Flow (GPM)" object='$device/properties[name="registers"]/children[properties[name="name"][value="Annex Bldg CHW Flow"]]' />
		<producer id="annexbldgchwstemp" datatype="power" name="Annex Bldg CHWS Temp (degF)" object='$device/properties[name="registers"]/children[properties[name="name"][value="Annex Bldg CHWS Temp"]]' />
		<producer id="annexbldgchwrtemp" datatype="power" name="Annex Bldg CHWR Temp (degF)" object='$device/properties[name="registers"]/children[properties[name="name"][value="Annex Bldg CHWR Temp"]]' />
		<producer id="dccrahchwflow" datatype="power" name="DC CRAH CHW Flow (GPM)" object='$device/properties[name="registers"]/children[properties[name="name"][value="DC CRAH CHW Flow"]]' />
		<producer id="dccrahchwstemp" datatype="power" name="DC CRAH CHWS Temp (degF)" object='$device/properties[name="registers"]/children[properties[name="name"][value="DC CRAH CHWS Temp"]]' />
		<producer id="dccrahchwrtemp" datatype="power" name="DC CRAH CHWR Temp (degF)" object='$device/properties[name="registers"]/children[properties[name="name"][value="DC CRAH CHWR Temp"]]' />

		<varbinding vars="name">
			<property>$device/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="devid">
			<property>$device/properties[name="id"]</property>
			<value>devid</value>
		</varbinding>
		<varbinding vars="pullPeriod">
			<property>$device/properties[name="registers"]/children/properties[name="lastValue"]/tags[tagName='poll']</property>
			<value>pullPeriod*60000</value>
		</varbinding>
	</component>
</componentlib>
