<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib [
	<!ENTITY % componentlibs SYSTEM "file:///componentlib.dtd">
	%componentlibs;
	<!ENTITY % control_parcels SYSTEM "file:///control_parcels.dtd">
	%control_parcels;
]>
<componentlib
		name="h-wing crah"
		version="&componentlib.version;"
		cversion="&componentlib.cversion;"
		oversion="&componentlib.oversion;"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
		>

	<component type='h-wing-dual-crah' classes='placeable,rotatable,controllable' grouppath="Environmentals;Wireless;CRAC/CRAH" filters="Environmentals">
		<display>
			<description>A CRAH instrumented with two ThermaNode EZ-H sensors.  One ThermaNode EZ-H sensor is in the supply, and the other in the return.  This Component goes in a Room and a WSN Data Source.</description>
			<name>ThermaNode EZ-H Dual CRAH</name>
			<shape>hwing_crah</shape>
		</display>

		<object as='rNode' type='wsnnode'>
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity"><value>2400.0</value></property>
			<property name="batteryOperated"><value>1</value></property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status"><value>0</value></property>
			<property name="platformId"><value>92</value></property>
			<property name="platformName"><value>ThermaNode EZ-H</value></property>
			<property name="sensepoints">

				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>node temp</value></property>
					<property name="channel"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="aMin"><value>59.0</value></property>
					<property name="aMax"><value>90.0</value></property>
					<property name="rMin"><value>64.4</value></property>
					<property name="rMax"><value>80.6</value></property>
					<property name="type"><value>1</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="smartSendThreshold"><value/></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>humidity</value></property>
					<property name="channel"><value>1</value></property>
					<property name="min"><value>10.0</value></property>
					<property name="max"><value>90.0</value></property>
					<property name="aMin"><value>20.0</value></property>
					<property name="aMax"><value>80.0</value></property>
					<property name="rMin"><value>40.0</value></property>
					<property name="rMax"><value>55.0</value></property>
					<property name="type"><value>2</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="dataclass"><value>201</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="smartSendThreshold"><value/></property>
				</object>

				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>battery</value></property>
					<property name="channel"><value>2</value></property>
					<property name="min"><value>1.8</value></property>
					<property name="max"><value>3.7</value></property>
					<property name="aMin"><value>2.5</value></property>
					<property name="aMax"><value>3.7</value></property>
					<property name="rMin"><value>2.7</value></property>
					<property name="rMax"><value>3.7</value></property>
					<property name="type"><value>5</value></property>
					<property name="dataclass"><value>210</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>return temp</value></property>
					<property name="channel"><value>3</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>36</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>15</value></property>
					<property name="smartSendCriticalTemp"><value>80.6</value></property>
					<property name="smartSendDeltaTempLow"><value>5</value></property>
					<property name="smartSendDeltaTempHigh"><value>5</value></property>
				</object>
			</property>
		</object>

		<object as='sNode' type='wsnnode'>
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity"><value>2400.0</value></property>
			<property name="batteryOperated"><value>1</value></property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status"><value>0</value></property>
			<property name="platformId"><value>92</value></property>
			<property name="platformName"><value>ThermaNode EZ-H</value></property>
			<property name="sensepoints">

				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>node temp</value></property>
					<property name="channel"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="aMin"><value>59.0</value></property>
					<property name="aMax"><value>90.0</value></property>
					<property name="rMin"><value>64.4</value></property>
					<property name="rMax"><value>80.6</value></property>
					<property name="type"><value>1</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="smartSendThreshold"><value/></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>humidity</value></property>
					<property name="channel"><value>1</value></property>
					<property name="min"><value>10.0</value></property>
					<property name="max"><value>90.0</value></property>
					<property name="aMin"><value>20.0</value></property>
					<property name="aMax"><value>80.0</value></property>
					<property name="rMin"><value>40.0</value></property>
					<property name="rMax"><value>55.0</value></property>
					<property name="type"><value>2</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="dataclass"><value>201</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="smartSendThreshold"><value/></property>
				</object>

				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>battery</value></property>
					<property name="channel"><value>2</value></property>
					<property name="min"><value>1.8</value></property>
					<property name="max"><value>3.7</value></property>
					<property name="aMin"><value>2.5</value></property>
					<property name="aMax"><value>3.7</value></property>
					<property name="rMin"><value>2.7</value></property>
					<property name="rMax"><value>3.7</value></property>
					<property name="type"><value>5</value></property>
					<property name="dataclass"><value>210</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>supply temp</value></property>
					<property name="channel"><value>3</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>36</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>15</value></property>
					<property name="smartSendCriticalTemp"><value>80.6</value></property>
					<property name="smartSendDeltaTempLow"><value>5</value></property>
					<property name="smartSendDeltaTempHigh"><value>5</value></property>
				</object>
			</property>
		</object>

	<object type="crac" as="crac">
		<dlid>DLID:-1</dlid>
		<property name="status"><value>1</value></property>

		<property name="producerpoints">
			<object type="producerpoint">
				<dlid>DLID:-1</dlid>
				<property name="name"><value>tonsDesign</value></property>
				<property name="fieldname"><value>tonsDesign</value></property>
				<property name="status"><value>1</value></property>
			</object>
			<object type="producerpoint">
				<dlid>DLID:-1</dlid>
				<property name="name"><value>cfm</value></property>
				<property name="fieldname"><value>cfm</value></property>
				<property name="status"><value>1</value></property>
			</object>
			<object type="producerpoint">
				<dlid>DLID:-1</dlid>
				<property name="name"><value>tonsActual</value></property>
				<property name="fieldname"><value>tonsActual</value></property>
				<property name="status"><value>1</value></property>
			</object>
			<object type="producerpoint">
				<dlid>DLID:-1</dlid>
				<property name="name"><value>coolingEfficiency</value></property>
				<property name="fieldname"><value>coolingEfficiency</value></property>
				<property name="status"><value>1</value></property>
			</object>
		</property>
	</object>

	<docking from='$crac/properties[name="supplyT"]' to='$sNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]' />
	<docking from='$crac/properties[name="returnT"]' to='$rNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]' />
	<docking from='$crac/properties[name="supplyRh"]' to='$sNode/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]' />
	<docking from='$crac/properties[name="returnRh"]' to='$rNode/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]' />


		<!-- this is an object exposed for binding by other components -->
	<producer id="reftemp" datatype="reftemp" name="Reference Temperature" object='$crac/properties[name="supplyT"]/referrent' />

	<producer id="tonsDesign" datatype="power" name="Design Tons" object='$crac/properties[name="producerpoints"]/children[properties[name="name"][value="tonsDesign"]]' />
	<producer id="cfm" datatype="power" name="CFM" object='$crac/properties[name="producerpoints"]/children[properties[name="name"][value="cfm"]]' />
	<producer id="tonsActual" datatype="power" name="Actual Tons" object='$crac/properties[name="producerpoints"]/children[properties[name="name"][value="tonsActual"]]' />
	<producer id="coolingEfficiency" datatype="power" name="Cooling Efficiency" object='$crac/properties[name="producerpoints"]/children[properties[name="name"][value="coolingEfficiency"]]' />

	<macids value="rmac_id,smac_id" />

	<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">ThermaNode EZ-H Dual CRAH</property>
	<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true" dimension="">0</property>
	<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">72.0</property>
	<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">36.0</property>
	<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
	<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
	<property name="rmac_id" display="Return MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
	<property name="smac_id" display="Supply MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
	<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
	<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>

	<property name="criticalTemp" display="Critical Send Temperature" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
	<property name="deltaTempLow" display="Low Temperature Send Threshold" type="java.lang.Double" editable="true" displayed="true" dimension="temperatureDelta">5</property>
	<property name="deltaTempHigh" display="High Temperature Send Threshold" type="java.lang.Double" editable="true" displayed="true" dimension="temperatureDelta">5</property>

	<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">ThermaNode EZ-H Dual CRAH</property>
	<property name="btu_table" display="Sensible Cooling Table" type="DeploymentLab.ChartContentsBTU" editable="true" displayed="true">40,;65,;70,;75,;80,;85,;90,;95,;100,;</property>
	<property name="cfm_table" display="Air Volume Table" type="DeploymentLab.ChartContentsCFM" editable="true" displayed="true">20,;30,;40,;50,;60,;70,;80,;90,;100,;</property>

	<!-- Needed for the control config -->
	<inheritedproperty name="roomType" inheritedrole="room" displayed="false"/>
	<inheritedproperty name="controlType" inheritedrole="room" displayed="false"/>

	<property name="supplyLayerTop" display="Display Supply in LI Top" type="java.lang.Boolean" editable="true" displayed="true">false</property>
	<property name="supplyLayerMiddle" display="Display Supply in LI Middle" type="java.lang.Boolean" editable="true" displayed="true">false</property>
	<property name="supplyLayerBottom" display="Display Supply in LI Bottom" type="java.lang.Boolean" editable="true" displayed="true">false</property>
	<property name="supplyLayerSubfloor" display="Display Supply in LI Subfloor" type="java.lang.Boolean" editable="true" displayed="true">false</property>
	<property name="returnLayerTop" display="Display Return in LI Top" type="java.lang.Boolean" editable="true" displayed="true">false</property>
	<property name="returnLayerMiddle" display="Display Return in LI Middle" type="java.lang.Boolean" editable="true" displayed="true">false</property>
	<property name="returnLayerBottom" display="Display Return in LI Bottom" type="java.lang.Boolean" editable="true" displayed="true">false</property>
	<property name="returnLayerSubfloor" display="Display Return in LI Subfloor" type="java.lang.Boolean" editable="true" displayed="true">false</property>

	<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

	<varbinding vars="supplyLayerTop,supplyLayerMiddle,supplyLayerBottom,supplyLayerSubfloor">
		<property>$sNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]</property>
		<value>
			top = 0;
			if (supplyLayerTop){ top = 0x20000; }
			middle = 0;
			if (supplyLayerMiddle){ middle = 0x4000; }
			bottom = 0;
			if (supplyLayerBottom){ bottom = 0x800; }
			sub = 0;
			if (supplyLayerSubfloor){ sub = 0x100; }
			total = top | middle | bottom | sub;
			total
		</value>
	</varbinding>
	<varbinding vars="returnLayerTop,returnLayerMiddle,returnLayerBottom,returnLayerSubfloor">
		<property>$rNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]</property>
		<value>
			top = 0;
			if (returnLayerTop){ top = 0x20000; }
			middle = 0;
			if (returnLayerMiddle){ middle = 0x4000; }
			bottom = 0;
			if (returnLayerBottom){ bottom = 0x800; }
			sub = 0;
			if (returnLayerSubfloor){ sub = 0x100; }
			total = top | middle | bottom | sub;
			total
		</value>
	</varbinding>

	<varbinding vars="cfm_table">
		<property>$crac/properties[name="cfm_table"]</property>
		<value>cfm_table</value>
	</varbinding>
	<varbinding vars="btu_table">
		<property>$crac/properties[name="btu_table"]</property>
		<value>btu_table</value>
	</varbinding>

	<varbinding vars="x">
		<property>$crac/properties[name="x"]</property>
		<value>x</value>
	</varbinding>
	<varbinding vars="x,depth,rotation">
		<property>$rNode/properties[name="x"]</property>
		<property>$rNode/properties[name="sensepoints"]/children/properties[name="x"]</property>
		<value>x - (depth/4 * Math.cos(Math.toRadians(-rotation)))</value>
	</varbinding>
	<varbinding vars="x,depth,rotation">
		<property>$sNode/properties[name="x"]</property>
		<property>$sNode/properties[name="sensepoints"]/children/properties[name="x"]</property>
		<value>x + (depth/4 * Math.cos(Math.toRadians(-rotation)))</value>
	</varbinding>
	<varbinding vars="y">
		<property>$crac/properties[name="y"]</property>
		<value>y</value>
	</varbinding>
	<varbinding vars="y,depth,rotation">
		<property>$rNode/properties[name="y"]</property>
		<property>$rNode/properties[name="sensepoints"]/children/properties[name="y"]</property>
		<value>y - (depth/4 * Math.sin(Math.toRadians(-rotation)))</value>
	</varbinding>
	<varbinding vars="y,depth,rotation">
		<property>$sNode/properties[name="y"]</property>
		<property>$sNode/properties[name="sensepoints"]/children/properties[name="y"]</property>
		<value>y + (depth/4 * Math.sin(Math.toRadians(-rotation)))</value>
	</varbinding>
	<varbinding vars="name">
		<property>$rNode/properties[name="name"]</property>
		<value>'Node ' + name + ' Return'</value>
	</varbinding>
	<varbinding vars="name">
		<property>$sNode/properties[name="name"]</property>
		<value>'Node ' + name + ' Supply'</value>
	</varbinding>
	<varbinding vars="name">
		<property>$crac/properties[name="name"]</property>
		<value>name</value>
	</varbinding>
	<varbinding vars="rotation">
		<property>$crac/properties[name="rotation"]</property>
		<value>rotation</value>
	</varbinding>
	<varbinding vars="width">
		<property>$crac/properties[name="width"]</property>
		<value>width</value>
	</varbinding>
	<varbinding vars="depth">
		<property>$crac/properties[name="depth"]</property>
		<value>depth</value>
	</varbinding>
	<varbinding vars="rmac_id">
		<property>$rNode/properties[name="mac"]</property>
		<value>Long.parseLong(rmac_id,16)</value>
	</varbinding>
	<varbinding vars="smac_id">
		<property>$sNode/properties[name="mac"]</property>
		<value>Long.parseLong(smac_id,16)</value>
	</varbinding>

	<varbinding vars="rmac_id">
		<property>$rNode/properties[name="id"]</property>
		<value>lid.generateLogicalId(rmac_id , self)</value>
	</varbinding>
	<varbinding vars="smac_id">
		<property>$sNode/properties[name="id"]</property>
		<value>lid.generateLogicalId(smac_id , self)</value>
	</varbinding>


	<varbinding vars="sample_interval">
		<property>$sNode/properties[name="period"]</property>
		<property>$rNode/properties[name="period"]</property>
		<value>sample_interval + ' min'</value>
	</varbinding>
	<varbinding vars="location">
		<property>$sNode/properties[name="location"]</property>
		<property>$rNode/properties[name="location"]</property>
		<property>$crac/properties[name="location"]</property>
		<value>location</value>
	</varbinding>

	<!-- Optional Control Parcels -->
	&parcel.control.temperature_devices.crah;
	&parcel.control.vfd_devices.crah;

</component>

</componentlib>
