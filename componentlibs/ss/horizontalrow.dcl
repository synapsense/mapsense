<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
	name="horizontal row objects"
	version="&componentlib.version;"
	cversion="&componentlib.cversion;"
	oversion="&componentlib.oversion;"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
	>

	<component type="single-horizontal-row-thermanode2" classes="placeable,rotatable,staticchildplaceable" grouppath="Environmentals;Wireless;Other" filters="Environmentals">
		<display>
			<description>Array of sensors used to monitor the horizontal temperature distribution at various points in a row of racks at the top of each rack.  There are 6 external thermistors, each displaced from the center where the ThermaNode is located.  This component goes in a Room and requires a WSN Data Source. This configuration is qualified for SynapSense Active Control™.</description>
			<name>Horizontal Row</name>
			<shape>node</shape>
			<childshape>horizontal_row_sensor</childshape>
		</display>

		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity"><value>5600.0</value></property>
			<property name="batteryOperated"><value>1</value></property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status"><value>0</value></property>
			<property name="platformId"><value>11</value></property>
			<property name="platformName"><value>ThermaNode</value></property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>internal temp</value></property>
					<property name="channel"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="aMin"><value>59.0</value></property>
					<property name="aMax"><value>90.0</value></property>
					<property name="rMin"><value>64.4</value></property>
					<property name="rMax"><value>80.6</value></property>
					<property name="type"><value>1</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>humidity</value></property>
					<property name="channel"><value>1</value></property>
					<property name="min"><value>10.0</value></property>
					<property name="max"><value>90.0</value></property>
					<property name="aMin"><value>20.0</value></property>
					<property name="aMax"><value>80.0</value></property>
					<property name="rMin"><value>40.0</value></property>
					<property name="rMax"><value>55.0</value></property>
					<property name="type"><value>2</value></property>
					<property name="dataclass"><value>201</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>battery</value></property>
					<property name="channel"><value>2</value></property>
					<property name="min"><value>1.8</value></property>
					<property name="max"><value>3.7</value></property>
					<property name="aMin"><value>2.5</value></property>
					<property name="aMax"><value>3.7</value></property>
					<property name="rMin"><value>2.7</value></property>
					<property name="rMax"><value>3.7</value></property>
					<property name="type"><value>5</value></property>
					<property name="dataclass"><value>210</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				
			</property>
		</object>

		<macids value="mac_id" />
		
		<!-- several parameters for staticChildrenNode-->
		<children type="horizontal-row-sensor-child">
			<child name="c3" channel="5" display="CH5"/>
			<child name="c4" channel="4" display="CH4"/>
			<child name="c5" channel="3" display="CH3"/>
			<child name="c6" channel="6" display="CH6"/>
			<child name="c7" channel="7" display="CH7"/>
			<child name="c8" channel="8" display="CH8"/>
		</children>

		<synchronizedproperties>
			<property parent="min_allow_t" child="min_allow_t"/>
			<property parent="max_allow_t" child="max_allow_t"/>
			<property parent="min_recommend_t" child="min_recommend_t"/>
			<property parent="max_recommend_t" child="max_recommend_t"/>
			<property parent="rotation" child="rotation"/>
			<property parent="li_layer" child="li_layer"/>
			<property parent="smartsend_delta" child="smartsend_delta"/>
			<property parent="name" child="parent_name"/>
		</synchronizedproperties>
		
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Horizontal Row</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true">0</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="false" displayed="false">20.5</property>
		<property name="width" display="Width" type="java.lang.Double" editable="false" displayed="false">15.0</property>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		<property name="c3_offset" display="Channel 5 Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">-216.0</property>
		<property name="c4_offset" display="Channel 4 Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">-144.0</property>
		<property name="c5_offset" display="Channel 3 Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">-72.0</property>
		<property name="c6_offset" display="Channel 6 Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">72.0</property>
		<property name="c7_offset" display="Channel 7 Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">144.0</property>
		<property name="c8_offset" display="Channel 8 Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">216.0</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Horizontal Row</property>
		<property name="li_layer" display="LI Layer" type="java.lang.Integer" editable="true" displayed="true" valueChoices="None:0,Top:131072,Middle:16384,Bottom:2048,Subfloor:256">131072</property>

		<property name="powerSource" display="Power Source" type="java.lang.Integer" editable="true" displayed="true"
		valueChoices="Battery Powered:1,Wall Powered:0">1</property>

		<property name="smartsend_delta" display="Temperature Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">5</property>

		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

	
		<varbinding vars="li_layer">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="z"]</property>
			<value>li_layer</value>
		</varbinding>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>

		<varbinding vars="x">
			<property>$node/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$node/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<value>location</value>
		</varbinding>

		<varbinding vars="powerSource">
			<property>$node/properties[name="batteryOperated"]</property>
			<value>powerSource</value>
		</varbinding>

	</component>
	
	<component type="horizontal-row-sensor-child" classes="placeable,staticchild,controlinput,temperaturecontrol" filters="Environmentals">
		<display>
			<description>Array of sensors used to monitor the horizontal temperature distribution at various points in a row of racks at the top of each rack.  There are 6 external thermistors, each displaced from the center where the ThermaNode is located.  This component goes in a Room and requires a WSN Data Source. This configuration is qualified for SynapSense Active Control™.</description>
			<name>Temp Sensor</name>
			<shape>horizontal_row_sensor</shape>
		</display>
		
		<object type="wsnsensor" as="sensor">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>intake temp</value></property>
			<property name="channel"><value>0</value></property>
			<property name="min"><value>40.0</value></property>
			<property name="max"><value>100.0</value></property>
			<property name="type"><value>27</value></property>
			<property name="dataclass"><value>200</value></property>
			<property name="enabled"><value>1</value></property>
			<property name="mode"><value>instant</value></property>
			<property name="interval"><value>0</value></property>
			<property name="status"><value>1</value></property>
			<property name="lastValue"><value>-5000.0</value></property>
			<property name="z"><value>131072</value></property>
			<property name="subSamples"><value>30</value></property>
		</object>

		<docking from='$logicalobject/properties[name="sensor"]' to='$sensor' />

		<!-- Must be after the sensor docking or the references to it will be null. -->
		&object.env.generictemperature; <!-- $logicalobject -->

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="x" display="x" type="java.lang.Double" editable="false" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="false" displayed="true" dimension="distance">0</property>
		<property name="channel" display="Channel" type="java.lang.Integer" editable="false" displayed="true">0</property>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="false" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="false" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="false" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="false" dimension="temperature">80.6</property>
		<property name="li_layer" display="LI Layer" type="java.lang.Integer" editable="true" displayed="false" >131072</property>
		<property name="smartsend_delta" display="Temperature Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="false" dimension="temperatureDelta">5</property>
		<property name="child_id" display="Child id" type="java.lang.String" editable="false" displayed="false"></property>
		<property name="parent_name" display="Parent Name" type="java.lang.String" editable="false" displayed="false"></property>

		<!-- Not really resizable & rotatable but staticchild is an OrientedNode, so these are needed so the node logic doesn't explode. -->
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="false" displayed="false">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="false" displayed="false">15.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="false" displayed="false">15.0</property>

		<varbinding vars="parent_name,name">
			<property>$logicalobject/properties[name="name"]</property>
			<value>parent_name + " - " + name</value>
		</varbinding>

		<varbinding vars="x">
			<property>$sensor/properties[name="x"]</property>
			<property>$logicalobject/properties[name="x"]</property>
			<value>x</value>
		</varbinding>	

		<varbinding vars="y">
			<property>$sensor/properties[name="y"]</property>
			<property>$logicalobject/properties[name="y"]</property>
			<value>y</value>
		</varbinding>	

		<varbinding vars="channel">
			<property>$sensor/properties[name="channel"]</property>
			<value>channel</value>
		</varbinding>
		
		<varbinding vars="min_allow_t">
			<property>$sensor/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>

		<varbinding vars="max_allow_t">
			<property>$sensor/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		
		<varbinding vars="min_recommend_t">
			<property>$sensor/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		
		<varbinding vars="max_recommend_t">
			<property>$sensor/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>

		<varbinding vars="li_layer">
			<property>$sensor/properties[name="z"]</property>
			<value>li_layer</value>
		</varbinding>

		<varbinding vars="smartsend_delta">
			<property>$sensor/properties[name="smartSendThreshold"]</property>
			<value>(int)(smartsend_delta)</value>
		</varbinding>

	</component>
	
</componentlib>