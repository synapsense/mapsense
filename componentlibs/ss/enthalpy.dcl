<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
		name="constellation 2 enthalpy meters"
		version="&componentlib.version;"
		cversion="&componentlib.cversion;"
		oversion="&componentlib.oversion;"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
		>


	<component type="dualflow-enthalpy-constellation2" classes="placeable" grouppath="Power &amp; Energy;Wireless" filters="Power">
		<display>
			<description>A Chilled-water energy node, 2 external temp sensors and 2 external flow sensors for measuring energy carried by chilled water.  This Component goes in a Room and requires a WSN Data Source.</description>
			<name>Dual Flow CHW Energy</name>
			<shape>dual_flow</shape>
		</display>

		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity"><value>5600.0</value></property>
			<property name="batteryOperated"><value>0</value></property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status"><value>0</value></property>
			<property name="platformId"><value>16</value></property>
			<property name="platformName"><value>Enthalpy Node</value></property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>battery</value></property>
					<property name="channel"><value>2</value></property>
					<property name="min"><value>1.8</value></property>
					<property name="max"><value>3.7</value></property>
					<property name="aMin"><value>2.5</value></property>
					<property name="aMax"><value>3.7</value></property>
					<property name="rMin"><value>2.7</value></property>
					<property name="rMax"><value>3.7</value></property>
					<property name="type"><value>5</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="dataclass"><value>210</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="smartSendThreshold"><value>0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>supply temp</value></property>
					<property name="channel"><value>3</value></property>
					<property name="min"><value>32.0</value></property>
					<property name="max"><value>90.0</value></property>
					<property name="type"><value>33</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="scaleMin"><value>32.0</value></property>
					<property name="scaleMax"><value>86.0</value></property> <!-- 30 degrees C = 86 F -->
					<property name="delta"><value>3279800</value></property> <!-- 30 degrees C, (30 + 32768)*100 -->
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="smartSendThreshold"><value>0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>return temp</value></property>
					<property name="channel"><value>4</value></property>
					<property name="min"><value>32.0</value></property>
					<property name="max"><value>90.0</value></property>
					<property name="type"><value>33</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="scaleMin"><value>32.0</value></property>
					<property name="scaleMax"><value>86.0</value></property> <!-- 30 degrees C = 86 F -->
					<property name="delta"><value>3279800</value></property> <!-- 30 degrees C, (30 + 32768)*100 -->
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="smartSendThreshold"><value>0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>supply flow</value></property>
					<property name="channel"><value>5</value></property>
					<property name="type"><value>31</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="min"><value>0.0</value></property>
					<property name="max"><value>500.0</value></property>
					<property name="scaleMin"><value>0.0</value></property>
					<property name="scaleMax"><value>475.5</value></property> <!-- 30 L/Sec = ~475.5 Gallons/Min -->
					<property name="delta"><value>3279800</value></property> <!-- 30 L/sec, (30 + 32768)*100 -->
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="dataclass"><value>209</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="smartSendThreshold"><value>0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>return flow</value></property>
					<property name="channel"><value>6</value></property>
					<property name="type"><value>31</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="min"><value>0.0</value></property>
					<property name="max"><value>500.0</value></property>
					<property name="scaleMin"><value>0.0</value></property>
					<property name="scaleMax"><value>475.5</value></property> <!-- 30 L/Sec = ~475.5 Gallons/Min -->
					<property name="delta"><value>3279800</value></property> <!-- 30 L/sec, (30 + 32768)*100 -->
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="dataclass"><value>209</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="smartSendThreshold"><value>0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>energy</value></property>
					<property name="channel"><value>7</value></property>
					<property name="min"><value>0.0</value></property>
					<property name="max"><value>10.0</value></property>
					<property name="type"><value>32</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="dataclass"><value>207</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="smartSendThreshold"><value>0</value></property>
				</object>
			</property>
		</object>

		<macids value="mac_id" />

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Dual Flow CHW Energy</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Dual Flow CHW Energy</property>
		<property name="maxflow" display="Max Flow" type="java.lang.Integer" editable="true" displayed="true"
				  valueChoices="10 L/s; 159 gpm:10,30 L/s; 476 gpm:30,60 L/s; 951 gpm:60,100 L/s; 1585 gpm:100,300 L/s; 4755 gpm:300">30
		</property>
		<property name="maxtemp" display="Max Temperature" type="java.lang.Integer" editable="true" displayed="true"
				  valueChoices="30 C (86 F):30,50 C (122 F):50,80 C (176 F):80">50
		</property>

		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

		<producer id="energy" datatype="power" name="Enthalpy (kWh per Sampling Interval)" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]' />

		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="x">
			<property>$node/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$node/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		<varbinding vars="maxflow"> <!-- flow: delta -->
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="delta"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="delta"]</property>
			<value>100 * (32768 + maxflow )</value>
		</varbinding>
		<varbinding vars="maxflow"> <!-- flow: scaleMax -->
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="scaleMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="scaleMax"]</property>
			<value>maxflow * 15.85032</value>
		</varbinding>
		<varbinding vars="maxflow"> <!-- displayMax -->
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="max"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="max"]</property>
			<value>(maxflow * 15.85032) + 25</value>
		</varbinding>
		<varbinding vars="maxflow,sample_interval"> <!-- energy display max -->
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="max"]</property>
			<value>((maxflow * 63) * 0.0002778 * (sample_interval * 60) )</value>
		</varbinding>

		<varbinding vars="maxtemp"> <!-- temp: delta -->
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="delta"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="delta"]</property>
			<value>(maxtemp + 32768) * 100</value>
		</varbinding>
		<varbinding vars="maxtemp">  <!-- temp: scaleMax -->
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="scaleMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="scaleMax"]</property>
			<value>(maxtemp * 1.8) + 32</value>
		</varbinding>
		<varbinding vars="maxtemp"> <!-- temp: displayMax -->
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="max"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="max"]</property>
			<value>((maxtemp * 1.8) + 32) + 10</value>
		</varbinding>

	</component>

	<component type="singleflow-enthalpy-constellation2" classes="placeable" grouppath="Power &amp; Energy;Wireless" filters="Power">
		<display>
			<description>A Chilled-water energy node, 2 external temp sensors and 1 external flow sensor for measuring energy moved by a CRAH.  This Component goes in a Room and requires a WSN Data Source.</description>
			<name>Single Flow CHW Energy</name>
			<shape>single_flow</shape>
		</display>

		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity"><value>5600.0</value></property>
			<property name="batteryOperated"><value>0</value></property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status"><value>0</value></property>
			<property name="platformId"><value>16</value></property>
			<property name="platformName"><value>Enthalpy Node</value></property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>battery</value></property>
					<property name="channel"><value>2</value></property>
					<property name="min"><value>1.8</value></property>
					<property name="max"><value>3.7</value></property>
					<property name="aMin"><value>2.5</value></property>
					<property name="aMax"><value>3.7</value></property>
					<property name="rMin"><value>2.7</value></property>
					<property name="rMax"><value>3.7</value></property>
					<property name="type"><value>5</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="dataclass"><value>210</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="smartSendThreshold"><value>0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>supply temp</value></property>
					<property name="channel"><value>3</value></property>
					<property name="min"><value>32.0</value></property>
					<property name="max"><value>90.0</value></property>
					<property name="type"><value>33</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="scaleMin"><value>32.0</value></property>
					<property name="scaleMax"><value>86.0</value></property> <!-- 30 degrees C = 86 F -->
					<property name="delta"><value>3279800</value></property> <!-- 30 degrees C, (30 + 32768)*100 -->
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="smartSendThreshold"><value>0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>return temp</value></property>
					<property name="channel"><value>4</value></property>
					<property name="min"><value>32.0</value></property>
					<property name="max"><value>90.0</value></property>
					<property name="type"><value>33</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="scaleMin"><value>32.0</value></property>
					<property name="scaleMax"><value>86.0</value></property> <!-- 30 degrees C = 86 F -->
					<property name="delta"><value>3279800</value></property> <!-- 30 degrees C, (30 + 32768)*100 -->
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="smartSendThreshold"><value>0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>supply flow</value></property>
					<property name="channel"><value>5</value></property>
					<property name="type"><value>31</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="min"><value>0.0</value></property>
					<property name="max"><value>500.0</value></property>
					<property name="scaleMin"><value>0.0</value></property>
					<property name="scaleMax"><value>475.5</value></property> <!-- 30 L/Sec = ~475.5 Gallons/Min -->
					<property name="delta"><value>3279800</value></property> <!-- 30 L/sec, (30 + 32768)*100 -->
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="dataclass"><value>209</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="smartSendThreshold"><value>0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>energy</value></property>
					<property name="channel"><value>7</value></property>
					<property name="min"><value>0.0</value></property>
					<property name="max"><value>200.0</value></property>
					<property name="type"><value>32</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="dataclass"><value>207</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="smartSendThreshold"><value>0</value></property>
				</object>
			</property>
		</object>

		<macids value="mac_id" />

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Single Flow CHW Energy</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Single Flow CHW Energy</property>
		<property name="maxflow" display="Max Flow" type="java.lang.Integer" editable="true" displayed="true"
				  valueChoices="10 L/s; 159 gpm:10,30 L/s; 476 gpm:30,60 L/s; 951 gpm:60,100 L/s; 1585 gpm:100,300 L/s; 4755 gpm:300">30
		</property>
		<property name="maxtemp" display="Max Temperature" type="java.lang.Integer" editable="true" displayed="true"
				  valueChoices="30 C (86 F):30,50 C (122 F):50,80 C (176 F):80">50
		</property>

		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

		<producer id="energy" datatype="power" name="Enthalpy (kWh per Sampling Interval)" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]' />

		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="x">
			<property>$node/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$node/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		<varbinding vars="maxflow"> <!-- delta -->
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="delta"]</property>
			<value>100 * (32768 + maxflow )</value>
		</varbinding>
		<varbinding vars="maxflow"> <!-- scaleMax -->
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="scaleMax"]</property>
			<value>maxflow * 15.85032</value>
		</varbinding>
		<varbinding vars="maxflow"> <!-- displayMax -->
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="max"]</property>
			<value>(maxflow * 15.85032) + 25</value>
		</varbinding>
		<varbinding vars="maxflow,sample_interval"> <!-- energy display max -->
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="max"]</property>
			<value>((maxflow * 63) * 0.0002778 * (sample_interval * 60) )</value>
		</varbinding>

		<varbinding vars="maxtemp">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="delta"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="delta"]</property>
			<value>(maxtemp + 32768) * 100</value>
		</varbinding>
		<varbinding vars="maxtemp">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="scaleMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="scaleMax"]</property>
			<value>(maxtemp * 1.8) + 32</value>
		</varbinding>
		<varbinding vars="maxtemp"> <!-- temp: displayMax -->
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="max"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="max"]</property>
			<value>((maxtemp * 1.8) + 32) + 10</value>
		</varbinding>


	</component>

</componentlib>