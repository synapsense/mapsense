<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
	name="Legacy 5.0 objects"
	version="&componentlib.version;"
	cversion="&componentlib.cversion;"
	oversion="&componentlib.oversion;"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
	>

	<component type="pressure" classes="placeable,haloed" grouppath="Environmentals;Legacy 5.0" filters="Environmentals">
		<display>
			<description>A sensor node used to capture subfloor differential pressure.  Upgrade from 4.0 only.</description>
			<name>Pressure</name>
			<shape>pressure</shape>
			<haloradius>151.4</haloradius>
			<halocolor>-16711936</halocolor>
		</display>
		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity">
				<value>5600.0</value>
			</property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status">
				<value>0</value>
			</property>
			<property name="platformId">
				<value>12</value>
			</property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>battery</value>
					</property>
					<property name="channel">
						<value>2</value>
					</property>
					<property name="min">
						<value>1.8</value>
					</property>
					<property name="max">
						<value>3.7</value>
					</property>
					<property name="aMin">
						<value>2.5</value>
					</property>
					<property name="aMax">
						<value>3.7</value>
					</property>
					<property name="rMin">
						<value>2.7</value>
					</property>
					<property name="rMax">
						<value>3.7</value>
					</property>
					<property name="type">
						<value>5</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>210</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>subfloor pressure</value>
					</property>
					<property name="channel">
						<value>3</value>
					</property>
					<property name="min">
						<value>0.0</value>
					</property>
					<property name="max">
						<value>0.5</value>
					</property>
					<property name="type">
						<value>24</value>
					</property>
					<property name="z">
						<value>256</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>202</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
			</property>
			<property name="batteryOperated">
				<value>1</value>
			</property>
			<property name="platformName">
				<value>Pressure Node</value>
			</property>
		</object>
		<object type="pressure" as="pressure">
			<dlid>DLID:-1</dlid>
			<property name="status">
				<value>1</value>
				<oldvalue/>
			</property>
		</object>
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true" dimension="">Pressure</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true" dimension="">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true" dimension="">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true" dimension=""/>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true" dimension="">Pressure</property>
		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="name">
			<property>$pressure/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="x">
			<property>$node/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<property>$pressure/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$node/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<property>$pressure/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<property>$pressure/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		<docking from="$pressure/properties[name=&quot;pressure&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;3&quot;]]"/>
		<macids value="mac_id"/>
	</component>


	<component type="pipe-temp-thermanode" classes="placeable" grouppath="Environmentals;Legacy 5.0" filters="Environmentals">
		<display>
			<description>A pipe monitoring assembly consisting of a ThermaNode and two external thermistors.  The two external thermistors capture the surface temperature of the supply and return pipes.</description>
			<name>Pipe Temp</name>
			<shape>plus_blue</shape>
		</display>
		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity">
				<value>5600.0</value>
			</property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status">
				<value>0</value>
			</property>
			<property name="platformId">
				<value>11</value>
			</property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>node temp</value>
					</property>
					<property name="channel">
						<value>0</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>1</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>humidity</value>
					</property>
					<property name="channel">
						<value>1</value>
					</property>
					<property name="min">
						<value>10.0</value>
					</property>
					<property name="max">
						<value>90.0</value>
					</property>
					<property name="type">
						<value>2</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>201</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>battery</value>
					</property>
					<property name="channel">
						<value>2</value>
					</property>
					<property name="min">
						<value>1.8</value>
					</property>
					<property name="max">
						<value>3.7</value>
					</property>
					<property name="type">
						<value>5</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>210</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>pipe supply temp</value>
					</property>
					<property name="channel">
						<value>3</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>pipe return temp</value>
					</property>
					<property name="channel">
						<value>4</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
			</property>
			<property name="batteryOperated">
				<value>1</value>
			</property>
			<property name="platformName">
				<value>ThermaNode</value>
			</property>
		</object>
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true" dimension="">Pipe Temp</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true" dimension="">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true" dimension="">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true" dimension=""/>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true" dimension="">Pipe temperature Thermanode</property>
		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>
		<varbinding vars="x">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
			<property>$node/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
			<property>$node/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		<macids value="mac_id"/>
	</component>


</componentlib>
