<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
	name="Modbus integrator object"
	version="&componentlib.version;"
	cversion="&componentlib.cversion;"
	oversion="&componentlib.oversion;"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
	>

	<component type="modbus-1reg-integrator" classes="placeable" grouppath="Calculations;Wired" filters="Calculations">
		<display>
			<description>Pulls a single configurable register from a Modbus device.</description>
			<name>Modbus Integrator</name>
			<shape>star</shape>
		</display>

		<object type="modbusdevice" as="device">
			<dlid>DLID:-1</dlid>
			<property name="maxpacketlen"><value>1</value></property>
			<property name="registers">
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>integration point</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>0</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
			</property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Modbus Integrator</property>
		<property name="devid" display="ModBus ID" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="register" display="ModBus Register" type="java.lang.Integer" editable="true" displayed="true">40001</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="pullPeriod" display="Sampling Interval (minutes)" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Modbus Integrator</property>

		<producer id="register" datatype="power" name="Register" object='$device/properties[name="registers"]/children[1]' />

		<varbinding vars="name">
			<property>$device/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="devid">
			<property>$device/properties[name="id"]</property>
			<value>devid</value>
		</varbinding>
		<varbinding vars="register">
			<property>$device/properties[name="registers"]/children[1]/properties[name="id"]</property>
			<value>register</value>
		</varbinding>
		<varbinding vars="pullPeriod">
			<!--
			<property>$device/properties[name="registers"]/children[1]/properties[name="period"]</property>
			<value>pullPeriod*60.0</value>
			-->

			<property>$device/properties[name="registers"]/children/properties[name="lastValue"]/tags[tagName='poll']</property>
			<value>pullPeriod * 60000</value>
		</varbinding>
	</component>


	<component type="modbus-1reg-integrator32" classes="placeable" grouppath="Calculations;Wired" filters="Calculations">
		<display>
			<description>Pulls a 32-bit configurable register from a Modbus device.</description>
			<name>Modbus Integrator32</name>
			<shape>star</shape>
		</display>

		<object type="modbusdevice" as="device">
			<dlid>DLID:-1</dlid>
			<property name="maxpacketlen"><value>42</value></property>
			<property name="registers">
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>integration point</value></property>
					<property name="scale"><value>1.0</value></property>
					<property name="id"><value>0</value></property>
					<property name="type"><value>INT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
			</property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Modbus Integrator32</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Modbus Integrator32</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>

		<property name="devid" display="ModBus ID" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="registerLSW" display="LSW ModBus Register" type="java.lang.Integer" editable="true" displayed="true">40001</property>
		<property name="registerMSW" display="MSW ModBus Register" type="java.lang.Integer" editable="true" displayed="true">40001</property>

		<property name="type" display="Data Type" type="java.lang.String" editable="true" displayed="true"
		valueChoices="Signed Integer:INT32,Unsigned Integer:UINT32,Floating Point:FLOAT32">
		INT32</property>

		<property name="scale" display="Scaling Factor" type="java.lang.Integer" editable="true" displayed="true">1</property>
		<property name="pullPeriod" display="Sampling Interval (minutes)" type="java.lang.Integer" editable="true" displayed="true">5</property>

		<producer id="register" datatype="power" name="Modbus Value" object='$device/properties[name="registers"]/children[1]' />

		<varbinding vars="name">
			<property>$device/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="devid">
			<property>$device/properties[name="id"]</property>
			<value>devid</value>
		</varbinding>

		<varbinding vars="registerLSW">
			<property>$device/properties[name="registers"]/children[1]/properties[name="id"]</property>
			<value>registerLSW</value>
		</varbinding>

		<varbinding vars="registerMSW">
			<property>$device/properties[name="registers"]/children[1]/properties[name="msw"]</property>
			<value>registerMSW</value>
		</varbinding>

		<varbinding vars="type">
			<property>$device/properties[name="registers"]/children[1]/properties[name="type"]</property>
			<value>type</value>
		</varbinding>

		<varbinding vars="scale">
			<property>$device/properties[name="registers"]/children[1]/properties[name="scale"]</property>
			<value>scale</value>
		</varbinding>

		<varbinding vars="pullPeriod">
			<!--
			<property>$device/properties[name="registers"]/children[1]/properties[name="period"]</property>
			<value>pullPeriod*60.0</value>
			-->

			<property>$device/properties[name="registers"]/children/properties[name="lastValue"]/tags[tagName='poll']</property>
			<value>pullPeriod * 60000</value>
		</varbinding>
	</component>

</componentlib>

