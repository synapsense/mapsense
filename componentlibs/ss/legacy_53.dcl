<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE componentlib [
	<!ENTITY % componentlibs SYSTEM "file:///componentlib.dtd">
	%componentlibs;
	<!ENTITY % control_parcels SYSTEM "file:///control_parcels.dtd">
	%control_parcels;
]>
<componentlib xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" name="Legacy 5.3 objects" version="&componentlib.version;" cversion="&componentlib.cversion;" oversion="&componentlib.oversion;" xsi:schemaLocation="http://www.synapsense.com componentlib.xsd">
	<component type="math_operation" classes="placeable" grouppath="Calculations;Power Calculations" filters="Calculations">
		<display>
			<description>Performs arithmetic operations on two sets of inputs.  Both input sets have a set operation performed on them (Sum, Return Smallest, Return Largest, Average) and then the results of both set operations have the Main Operation performed on them in the order (Set A Result) Main Operation (Set B Result).  The Main Operation can be one of Add, Subtract, Multiply, or Divide.  Both sets may have multiple inputs, but only require a single value.  Sets with a single input will ignore that set's Set Operation.  This Component goes in a Calculation Group and does not require a Data Source.</description>
			<name>Math Operation</name>
			<shape>pue_diamond_math</shape>
			<centerbadgecolor>00ff00</centerbadgecolor>
		</display>
		<object type="math_operation" as="math">
			<dlid>DLID:-1</dlid>
		</object>
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Math Operation</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Math Operation</property>
		<property name="notes" display="Notes" type="DeploymentLab.MultiLineContents" editable="true" displayed="true"/>
		<property name="opA" display="Operation on Set A" type="java.lang.String" editable="true" displayed="true" valueChoices="Sum All Elements:SUM,Return Smallest Element:MIN,Return Largest Element:MAX,Return Average of Elements:AVG">
		SUM</property>
		<property name="opB" display="Operation on Set B" type="java.lang.String" editable="true" displayed="true" valueChoices="Sum All Elements:SUM,Return Smallest Element:MIN,Return Largest Element:MAX,Return Average of Elements:AVG">
		SUM</property>
		<property name="opM" display="Main Operation" type="java.lang.String" editable="true" displayed="true" valueChoices="Add A + B:add,Subtract A - B:sub,Multiply A * B:mul,Divide A / B:div" centerbadge="true">add
		</property>
		<consumer id="inputA" datatype="power" name="Input Set A" collection="true" property="$math/properties[name=&quot;inputA&quot;]" required="true"/>
		<consumer id="inputB" datatype="power" name="Input Set B" collection="true" property="$math/properties[name=&quot;inputB&quot;]" required="true"/>
		<producer id="value" datatype="power" name="Output Value" object="$math"/>
		<varbinding vars="name">
			<property>$math/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="notes">
			<property>$math/properties[name="notes"]</property>
			<value>notes</value>
		</varbinding>
		<varbinding vars="opA">
			<property>$math/properties[name="opA"]</property>
			<value>opA</value>
		</varbinding>
		<varbinding vars="opB">
			<property>$math/properties[name="opB"]</property>
			<value>opB</value>
		</varbinding>
		<varbinding vars="opM">
			<property>$math/properties[name="opM"]</property>
			<value>opM</value>
		</varbinding>
	</component>


	<component type="crah-thermanode" classes="placeable,rotatable,controllable" grouppath="Environmentals;Legacy 5.3" filters="Environmentals">
		<display>
			<description>A CRAH monitoring assembly consisting of a ThermaNode with a single external thermistor.  The node captures the supply air temperature while the external thermistor captures the return air temperature.  Provides Rack Reference Temperature from the Supply Temperature.  This Component goes in a Room and a WSN Data Source.  4.0 upgrades only.</description>
			<name>Single</name>
			<shape>single_crac</shape>
		</display>
		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity"><value>5600.0</value></property>
			<property name="batteryOperated"><value>1</value></property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status"><value>0</value></property>
			<property name="platformId"><value>11</value></property>
			<property name="platformName"><value>ThermaNode</value></property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>supply temp</value></property>
					<property name="channel"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>1</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>supply humidity</value></property>
					<property name="channel"><value>1</value></property>
					<property name="min"><value>10.0</value></property>
					<property name="max"><value>90.0</value></property>
					<property name="type"><value>2</value></property>
					<property name="dataclass"><value>201</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>battery</value></property>
					<property name="channel"><value>2</value></property>
					<property name="min"><value>1.8</value></property>
					<property name="max"><value>3.7</value></property>
					<property name="aMin"><value>2.5</value></property>
					<property name="aMax"><value>3.7</value></property>
					<property name="rMin"><value>2.7</value></property>
					<property name="rMax"><value>3.7</value></property>
					<property name="type"><value>5</value></property>
					<property name="dataclass"><value>210</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>return temp</value></property>
					<property name="channel"><value>3</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
			</property>
		</object>
		<object type="crac" as="crac">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
		</object>
		<docking from="$crac/properties[name=&quot;supplyT&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;0&quot;]]"/>
		<docking from="$crac/properties[name=&quot;supplyRh&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;1&quot;]]"/>
		<docking from="$crac/properties[name=&quot;returnT&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;3&quot;]]"/>
		<!-- this is an object exposed for binding by other components -->
		<producer id="reftemp" datatype="reftemp" name="Reference Temperature" object="$crac/properties[name=&quot;supplyT&quot;]/referrent"/>
		<macids value="mac_id"/>
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Thermanode CRAH</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true" dimension="">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">72.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">36.0</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"/>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Single Thermanode CRAH</property>
		<property name="btu_table" display="BTU Table" type="DeploymentLab.ChartContentsBTU" editable="true" displayed="true"/>
		<property name="cfm_table" display="CFM Table" type="DeploymentLab.ChartContentsCFM" editable="true" displayed="true"/>

		<!-- Needed for the control config -->
		<inheritedproperty name="roomType" inheritedrole="room" displayed="false"/>
		<inheritedproperty name="controlType" inheritedrole="room" displayed="false"/>

		<property name="supplyLayerTop" display="Display Supply in LI Top" type="java.lang.Boolean" editable="true" displayed="true">false</property>
		<property name="supplyLayerMiddle" display="Display Supply in LI Middle" type="java.lang.Boolean" editable="true" displayed="true">false</property>
		<property name="supplyLayerBottom" display="Display Supply in LI Bottom" type="java.lang.Boolean" editable="true" displayed="true">false</property>
		<property name="supplyLayerSubfloor" display="Display Supply in LI Subfloor" type="java.lang.Boolean" editable="true" displayed="true">false</property>
		<property name="returnLayerTop" display="Display Return in LI Top" type="java.lang.Boolean" editable="true" displayed="true">false</property>
		<property name="returnLayerMiddle" display="Display Return in LI Middle" type="java.lang.Boolean" editable="true" displayed="true">false</property>
		<property name="returnLayerBottom" display="Display Return in LI Bottom" type="java.lang.Boolean" editable="true" displayed="true">false</property>
		<property name="returnLayerSubfloor" display="Display Return in LI Subfloor" type="java.lang.Boolean" editable="true" displayed="true">false</property>
		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>
		<varbinding vars="supplyLayerTop,supplyLayerMiddle,supplyLayerBottom,supplyLayerSubfloor">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="z"]</property>
			<value>
				top = 0;
				if (supplyLayerTop){ top = 0x20000; }
				middle = 0;
				if (supplyLayerMiddle){ middle = 0x4000; }
				bottom = 0;
				if (supplyLayerBottom){ bottom = 0x800; }
				sub = 0;
				if (supplyLayerSubfloor){ sub = 0x100; }
				total = top | middle | bottom | sub;
				total
			</value>
		</varbinding>
		<varbinding vars="returnLayerTop,returnLayerMiddle,returnLayerBottom,returnLayerSubfloor">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]</property>
			<value>
				top = 0;
				if (returnLayerTop){ top = 0x20000; }
				middle = 0;
				if (returnLayerMiddle){ middle = 0x4000; }
				bottom = 0;
				if (returnLayerBottom){ bottom = 0x800; }
				sub = 0;
				if (returnLayerSubfloor){ sub = 0x100; }
				total = top | middle | bottom | sub;
				total
			</value>
		</varbinding>
		<varbinding vars="cfm_table">
			<property>$crac/properties[name="cfm_table"]</property>
			<value>cfm_table</value>
		</varbinding>
		<varbinding vars="btu_table">
			<property>$crac/properties[name="btu_table"]</property>
			<value>btu_table</value>
		</varbinding>
		<varbinding vars="x">
			<property>$crac/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<value>x - (depth/4 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<value>x + (depth/4 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y">
			<property>$crac/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<value>y - (depth/4 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<value>y + (depth/4 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="name">
			<property>$crac/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="rotation">
			<property>$crac/properties[name="rotation"]</property>
			<value>rotation</value>
		</varbinding>
		<varbinding vars="width">
			<property>$crac/properties[name="width"]</property>
			<value>width</value>
		</varbinding>
		<varbinding vars="depth">
			<property>$crac/properties[name="depth"]</property>
			<value>depth</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<property>$crac/properties[name="location"]</property>
			<value>location</value>
		</varbinding>

		<!-- Optional Control Parcels -->
		&parcel.control.temperature_devices.crah;
		&parcel.control.vfd_devices.crah;

	</component>


	<component type="rack-control-lhs-endcap-rt-6t" classes="placeable,rotatable,controlinput,temperaturecontrol,airflowcontrol" grouppath="Environmentals;Wireless;Racks" filters="Environmentals">
		<display>
			<description>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors located on a left end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom and side middle.  This rack goes in a Room and requires a WSN Data Source.  This configuration is qualified for SynapSense Active Control™.</description>
			<name>LHS Endcap</name>
			<shape>rack-lhs-endcap-rt-6</shape>
		</display>
		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity"><value>5600.0</value></property>
			<property name="batteryOperated"><value>1</value></property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status"><value>0</value></property>
			<property name="platformId"><value>11</value></property>
			<property name="platformName"><value>ThermaNode</value></property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Node temp</value></property>
					<property name="channel"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="aMin"><value>59.0</value></property>
					<property name="aMax"><value>90.0</value></property>
					<property name="rMin"><value>64.4</value></property>
					<property name="rMax"><value>80.6</value></property>
					<property name="type"><value>1</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>131072</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>humidity</value></property>
					<property name="channel"><value>1</value></property>
					<property name="min"><value>10.0</value></property>
					<property name="max"><value>90.0</value></property>
					<property name="aMin"><value>20.0</value></property>
					<property name="aMax"><value>80.0</value></property>
					<property name="rMin"><value>40.0</value></property>
					<property name="rMax"><value>55.0</value></property>
					<property name="type"><value>2</value></property>
					<property name="dataclass"><value>201</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>131072</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>battery</value></property>
					<property name="channel"><value>2</value></property>
					<property name="min"><value>1.8</value></property>
					<property name="max"><value>3.7</value></property>
					<property name="aMin"><value>2.5</value></property>
					<property name="aMax"><value>3.7</value></property>
					<property name="rMin"><value>2.7</value></property>
					<property name="rMax"><value>3.7</value></property>
					<property name="type"><value>5</value></property>
					<property name="z"><value>0</value></property>
					<property name="dataclass"><value>210</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>cold top temp</value></property>
					<property name="channel"><value>3</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>131072</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>cold middle temp</value></property>
					<property name="channel"><value>4</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>16384</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>cold bottom temp</value></property>
					<property name="channel"><value>5</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>2048</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>hot top temp</value></property>
					<property name="channel"><value>6</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>131072</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>hot middle/bottom temp</value></property>
					<property name="channel"><value>7</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>18432</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>side middle temp</value></property>
					<property name="channel"><value>8</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>16384</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
			</property>
		</object>
		<docking from="$rack/properties[name=&quot;nodeTemp&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;0&quot;]]"/>
		<docking from="$rack/properties[name=&quot;rh&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;1&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;3&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;4&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;5&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;6&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;7&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;7&quot;]]"/>

		<!-- Must be after the cTop, etc., dockings or the references to them will be null. -->
		&object.env.rack.tb; <!-- $rack -->

		<consumer id="reftemp" datatype="reftemp" name="Rack Ref Temp" property="$rack/properties[name=&quot;ref&quot;]" required="true"/>
		<conducer id="PEBridge" name="Power And Environmental Rack Pair">
			<producer id="envRack" datatype="PEBridge_E" name="Environmental Rack Output" object="$rack"/>
			<consumer id="powerRack" datatype="PEBridge_P" name="Power Rack Input" property="$rack/properties[name='powerRack']" required="false"/>
		</conducer>
		<macids value="mac_id"/>
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">LHS Endcap</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">42.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">24.0</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"/>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		<property name="min_allow_h" display="Min Allowed Humidity" type="java.lang.Double" editable="true" displayed="true">20.0</property>
		<property name="max_allow_h" display="Max Allowed Humidity" type="java.lang.Double" editable="true" displayed="true">80.0</property>
		<property name="min_recommend_h" display="Min Recommended Humidity" type="java.lang.Double" editable="true" displayed="true">40.0</property>
		<property name="max_recommend_h" display="Max Recommended Humidity" type="java.lang.Double" editable="true" displayed="true">55.0</property>
		<property name="cold_delta" display="Intake Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">5</property>
		<property name="hot_delta" display="Exhaust Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">10</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">ThermaNode Endcap for LHS</property>
		<varbinding vars="x,depth,rotation">
			<!---hot side-->
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="x"]</property>
			<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="y"]</property>
			<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,width,rotation">
			<!--end cap side -->
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="x"]</property>
			<value>x - (width/2 * Math.cos(rotation+Math.PI/2))</value>
		</varbinding>
		<varbinding vars="y,width,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="y"]</property>
			<value>y - (width/2 * Math.sin(rotation+Math.PI/2))</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<!--node and cold side-->
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="x"]</property>
			<property>$node/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="y"]</property>
			<property>$node/properties[name="y"]</property>
			<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x">
			<property>$rack/properties[name="x"]</property>
			<value>x</value>
			<property>$rack/properties[name='displaypoints']/children[1]/properties[name='x']</property>
		</varbinding>
		<varbinding vars="y">
			<property>$rack/properties[name="y"]</property>
			<value>y</value>
			<property>$rack/properties[name='displaypoints']/children[1]/properties[name='y']</property>
		</varbinding>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="name">
			<property>$rack/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="rotation">
			<property>$rack/properties[name="rotation"]</property>
			<value>rotation</value>
		</varbinding>
		<varbinding vars="width">
			<property>$rack/properties[name="width"]</property>
			<value>width</value>
		</varbinding>
		<varbinding vars="depth">
			<property>$rack/properties[name="depth"]</property>
			<value>depth</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<property>$rack/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		<varbinding vars="min_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		<varbinding vars="min_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMin"]</property>
			<value>min_allow_h</value>
		</varbinding>
		<varbinding vars="max_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMax"]</property>
			<value>max_allow_h</value>
		</varbinding>
		<varbinding vars="min_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMin"]</property>
			<value>min_recommend_h</value>
		</varbinding>
		<varbinding vars="max_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMax"]</property>
			<value>max_recommend_h</value>
		</varbinding>
		<varbinding vars="cold_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(cold_delta)</value>
		</varbinding>
		<varbinding vars="hot_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(hot_delta)</value>
		</varbinding>
		<docking to="$rack/properties[name='coldDp']" from="$rack/properties[name='displaypoints']/children[1]/properties[name='lastValue']"/>
	</component>


	<component type="rack-control-rhs-endcap-rt-6t" classes="placeable,rotatable,controlinput,temperaturecontrol,airflowcontrol" grouppath="Environmentals;Wireless;Racks" filters="Environmentals">
		<display>
			<description>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors located on a right end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom and side middle. This rack goes in a Room and requires a WSN Data Source. This configuration is qualified for SynapSense Active Control™.</description>
			<name>RHS Endcap</name>
			<shape>rack-rhs-endcap-rt-6</shape>
		</display>
		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity"><value>5600.0</value></property>
			<property name="batteryOperated"><value>1</value></property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status"><value>0</value></property>
			<property name="platformId"><value>11</value></property>
			<property name="platformName"><value>ThermaNode</value></property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>node temp</value></property>
					<property name="channel"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="aMin"><value>59.0</value></property>
					<property name="aMax"><value>90.0</value></property>
					<property name="rMin"><value>64.4</value></property>
					<property name="rMax"><value>80.6</value></property>
					<property name="type"><value>1</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>131072</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>humidity</value></property>
					<property name="channel"><value>1</value></property>
					<property name="min"><value>10.0</value></property>
					<property name="max"><value>90.0</value></property>
					<property name="aMin"><value>20.0</value></property>
					<property name="aMax"><value>80.0</value></property>
					<property name="rMin"><value>40.0</value></property>
					<property name="rMax"><value>55.0</value></property>
					<property name="type"><value>2</value></property>
					<property name="dataclass"><value>201</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>131072</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>battery</value></property>
					<property name="channel"><value>2</value></property>
					<property name="min"><value>1.8</value></property>
					<property name="max"><value>3.7</value></property>
					<property name="aMin"><value>2.5</value></property>
					<property name="aMax"><value>3.7</value></property>
					<property name="rMin"><value>2.7</value></property>
					<property name="rMax"><value>3.7</value></property>
					<property name="type"><value>5</value></property>
					<property name="dataclass"><value>210</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>cold top temp</value></property>
					<property name="channel"><value>3</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>131072</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>cold middle temp</value></property>
					<property name="channel"><value>4</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>16384</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>cold bottom temp</value></property>
					<property name="channel"><value>5</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>2048</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>hot top temp</value></property>
					<property name="channel"><value>6</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>131072</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>hot middle/bottom temp</value></property>
					<property name="channel"><value>7</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>18432</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>side middle temp</value></property>
					<property name="channel"><value>8</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>16384</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
			</property>
		</object>

		<docking from="$rack/properties[name=&quot;nodeTemp&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;0&quot;]]"/>
		<docking from="$rack/properties[name=&quot;rh&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;1&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;3&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;4&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;5&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;6&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;7&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;7&quot;]]"/>

		<!-- Must be after the cTop, etc., dockings or the references to them will be null. -->
		&object.env.rack.tb; <!-- $rack -->

		<consumer id="reftemp" datatype="reftemp" name="Rack Ref Temp" property="$rack/properties[name=&quot;ref&quot;]" required="true"/>
		<conducer id="PEBridge" name="Power And Environmental Rack Pair">
			<producer id="envRack" datatype="PEBridge_E" name="Environmental Rack Output" object="$rack"/>
			<consumer id="powerRack" datatype="PEBridge_P" name="Power Rack Input" property="$rack/properties[name='powerRack']" required="false"/>
		</conducer>
		<macids value="mac_id"/>
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">RHS Endcap</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">42.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">24.0</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"/>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		<property name="min_allow_h" display="Min Allowed Humidity" type="java.lang.Double" editable="true" displayed="true">20.0</property>
		<property name="max_allow_h" display="Max Allowed Humidity" type="java.lang.Double" editable="true" displayed="true">80.0</property>
		<property name="min_recommend_h" display="Min Recommended Humidity" type="java.lang.Double" editable="true" displayed="true">40.0</property>
		<property name="max_recommend_h" display="Max Recommended Humidity" type="java.lang.Double" editable="true" displayed="true">55.0</property>
		<property name="cold_delta" display="Intake Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">5</property>
		<property name="hot_delta" display="Exhaust Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">10</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">ThermaNode Endcap for RHS</property>
		<!--hot aisle-->
		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="x"]</property>
			<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="y"]</property>
			<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<!--side aisle-->
		<varbinding vars="x,width,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="x"]</property>
			<value>x + (width/2 * Math.cos(rotation+Math.PI/2))</value>
		</varbinding>
		<varbinding vars="y,width,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="y"]</property>
			<value>y + (width/2 * Math.sin(rotation+Math.PI/2))</value>
		</varbinding>
		<!--node and cold aisle-->
		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="x"]</property>
			<property>$node/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="y"]</property>
			<property>$node/properties[name="y"]</property>
			<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x">
			<property>$rack/properties[name="x"]</property>
			<value>x</value>
			<property>$rack/properties[name='displaypoints']/children[1]/properties[name='x']</property>
		</varbinding>
		<varbinding vars="y">
			<property>$rack/properties[name="y"]</property>
			<value>y</value>
			<property>$rack/properties[name='displaypoints']/children[1]/properties[name='y']</property>
		</varbinding>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="name">
			<property>$rack/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="rotation">
			<property>$rack/properties[name="rotation"]</property>
			<value>rotation</value>
		</varbinding>
		<varbinding vars="width">
			<property>$rack/properties[name="width"]</property>
			<value>width</value>
		</varbinding>
		<varbinding vars="depth">
			<property>$rack/properties[name="depth"]</property>
			<value>depth</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<property>$rack/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		<varbinding vars="min_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		<varbinding vars="min_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMin"]</property>
			<value>min_allow_h</value>
		</varbinding>
		<varbinding vars="max_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMax"]</property>
			<value>max_allow_h</value>
		</varbinding>
		<varbinding vars="min_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMin"]</property>
			<value>min_recommend_h</value>
		</varbinding>
		<varbinding vars="max_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMax"]</property>
			<value>max_recommend_h</value>
		</varbinding>
		<varbinding vars="cold_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(cold_delta)</value>
		</varbinding>
		<varbinding vars="hot_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(hot_delta)</value>
		</varbinding>
		<docking to="$rack/properties[name='coldDp']" from="$rack/properties[name='displaypoints']/children[1]/properties[name='lastValue']"/>
	</component>
</componentlib>
