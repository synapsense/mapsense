<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" name="racks without ref. temp " version="&componentlib.version;" cversion="&componentlib.cversion;" oversion="&componentlib.oversion;" xsi:schemaLocation="http://www.synapsense.com componentlib.xsd">
	<component type="rack-control-rearexhaust-nsf-nrt-thermanode" classes="placeable,rotatable,controlinput,temperaturecontrol,airflowcontrol" grouppath="Environmentals;Wireless;No Ref. Temp Racks" filters="Environmentals">
		<display>
			<description>Rack monitoring assembly consisting of a ThermaNode equipped with 5 external sensors.  Designed for front inlet, rear exhaust configurations.  Node is installed at rack top with sense points  located at cabinet intake top, intake middle, intake bottom, exhaust top, and exhaust middle/bottom.  This rack goes in a Room and requires a WSN Data Source.  This configuration is qualified for SynapSense Active Control™.</description>
			<name>Rear Exhaust</name>
			<shape>rack-rearexhaust-nsf-nrt</shape>
		</display>
		<object as="node" type="wsnnode">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity"><value>5600.0</value></property>
			<property name="batteryOperated"><value>1</value></property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status"><value>0</value></property>
			<property name="platformId"><value>11</value></property>
			<property name="platformName"><value>ThermaNode</value></property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>node temp</value></property>
					<property name="channel"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="aMin"><value>59.0</value></property>
					<property name="aMax"><value>90.0</value></property>
					<property name="rMin"><value>64.4</value></property>
					<property name="rMax"><value>80.6</value></property>
					<property name="type"><value>1</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>humidity</value></property>
					<property name="channel"><value>1</value></property>
					<property name="min"><value>10.0</value></property>
					<property name="max"><value>90.0</value></property>
					<property name="aMin"><value>20.0</value></property>
					<property name="aMax"><value>80.0</value></property>
					<property name="rMin"><value>40.0</value></property>
					<property name="rMax"><value>55.0</value></property>
					<property name="type"><value>2</value></property>
					<property name="dataclass"><value>201</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>131072</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>battery</value></property>
					<property name="channel"><value>2</value></property>
					<property name="min"><value>1.8</value></property>
					<property name="max"><value>3.7</value></property>
					<property name="aMin"><value>2.5</value></property>
					<property name="aMax"><value>3.7</value></property>
					<property name="rMin"><value>2.7</value></property>
					<property name="rMax"><value>3.7</value></property>
					<property name="type"><value>5</value></property>
					<property name="dataclass"><value>210</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>cold top temp</value></property>
					<property name="channel"><value>3</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>131072</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>cold middle temp</value></property>
					<property name="channel"><value>4</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>16384</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>cold bottom temp</value></property>
					<property name="channel"><value>5</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>2048</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>hot top temp</value></property>
					<property name="channel"><value>6</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>131072</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>hot middle/bottom temp</value></property>
					<property name="channel"><value>7</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>27</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>18432</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>30</value></property>
				</object>
			</property>
		</object>

		<docking from="$rack/properties[name=&quot;nodeTemp&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;0&quot;]]"/>
		<docking from="$rack/properties[name=&quot;rh&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;1&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;3&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;4&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;5&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hTop&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;6&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hMid&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;7&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hBot&quot;]" to="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;7&quot;]]"/>

		<!-- Must be after the cTop, etc., dockings or the references to them will be null. -->
		&object.env.rack.tb; <!-- $rack -->

		<conducer id="PEBridge" name="Power And Environmental Rack Pair">
			<producer id="envRack" datatype="PEBridge_E" name="Environmental Rack Output" object="$rack"/>
			<consumer id="powerRack" datatype="PEBridge_P" name="Power Rack Input" property="$rack/properties[name='powerRack']" required="false"/>
		</conducer>
		<macids value="mac_id"/>
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Rear Exhaust</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">42.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">24.0</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"/>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		<property name="min_allow_h" display="Min Allowed Humidity" type="java.lang.Double" editable="true" displayed="true">20.0</property>
		<property name="max_allow_h" display="Max Allowed Humidity" type="java.lang.Double" editable="true" displayed="true">80.0</property>
		<property name="min_recommend_h" display="Min Recommended Humidity" type="java.lang.Double" editable="true" displayed="true">40.0</property>
		<property name="max_recommend_h" display="Max Recommended Humidity" type="java.lang.Double" editable="true" displayed="true">55.0</property>
		<property name="cold_delta" display="Intake Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">5</property>
		<property name="hot_delta" display="Exhaust Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">10</property>
		<property name="powerSource" display="Power Source" type="java.lang.Integer" editable="true" displayed="true" valueChoices="Battery Powered:1,Wall Powered:0">1</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">ThermaNode Rack Rear Exhaust</property>
		<varbinding vars="powerSource">
			<property>$node/properties[name="batteryOperated"]</property>
			<value>powerSource</value>
		</varbinding>
		<!-- hot aisle -->
		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="x"]</property>
			<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="y"]</property>
			<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<!-- cold aisle -->
		<varbinding vars="x,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="y"]</property>
			<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<!--  node, rack and battery -->
		<varbinding vars="x">
			<property>$node/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<property>$rack/properties[name="x"]</property>
			<value>x</value>
			<property>$rack/properties[name='displaypoints']/children[1]/properties[name='x']</property>
		</varbinding>
		<varbinding vars="y">
			<property>$node/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<property>$rack/properties[name="y"]</property>
			<value>y</value>
			<property>$rack/properties[name='displaypoints']/children[1]/properties[name='y']</property>
		</varbinding>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="name">
			<property>$rack/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="rotation">
			<property>$rack/properties[name="rotation"]</property>
			<value>rotation</value>
		</varbinding>
		<varbinding vars="width">
			<property>$rack/properties[name="width"]</property>
			<value>width</value>
		</varbinding>
		<varbinding vars="depth">
			<property>$rack/properties[name="depth"]</property>
			<value>depth</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<property>$rack/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		<varbinding vars="min_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		<varbinding vars="min_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMin"]</property>
			<value>min_allow_h</value>
		</varbinding>
		<varbinding vars="max_allow_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMax"]</property>
			<value>max_allow_h</value>
		</varbinding>
		<varbinding vars="min_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMin"]</property>
			<value>min_recommend_h</value>
		</varbinding>
		<varbinding vars="max_recommend_h">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMax"]</property>
			<value>max_recommend_h</value>
		</varbinding>
		<varbinding vars="cold_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(cold_delta)</value>
		</varbinding>
		<varbinding vars="hot_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(hot_delta)</value>
		</varbinding>
		<docking to="$rack/properties[name='coldDp']" from="$rack/properties[name='displaypoints']/children[1]/properties[name='lastValue']"/>
	</component>


</componentlib>
