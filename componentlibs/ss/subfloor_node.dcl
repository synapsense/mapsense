<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
		name="subfloor node"
		version="&componentlib.version;"
		cversion="&componentlib.cversion;"
		oversion="&componentlib.oversion;"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
		>

	<component type="subfloor_node" classes="placeable,haloed,controlinput,pressurecontrol" grouppath="Environmentals;Wireless;Pressure" filters="Environmentals">
		<display>
			<description>A sensor node used to capture subfloor differential pressure and temperature.  Temperature is provided by adding an optional external sensor.  This Component goes in a Zone and a WSN Data Source.  This configuration is qualified for SynapSense Active Control™.</description>
			<name>Subfloor Node</name>
			<shape>pressure_temp</shape>
			<haloradius>151.4</haloradius>
			<halocolor>-16711936</halocolor>
		</display>

		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity"><value>2400.0</value></property>
			<property name="batteryOperated"><value>1</value></property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status"><value>0</value></property>
			<property name="platformId"><value>32</value></property>
			<property name="platformName"><value>Subfloor Node</value></property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>battery</value></property>
					<property name="channel"><value>2</value></property>
					<property name="min"><value>1.8</value></property>
					<property name="max"><value>3.7</value></property>
					<property name="aMin"><value>2.5</value></property>
					<property name="aMax"><value>3.7</value></property>
					<property name="rMin"><value>2.7</value></property>
					<property name="rMax"><value>3.7</value></property>
					<property name="type"><value>43</value></property>
					<property name="dataclass"><value>210</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>subfloor pressure</value></property>
					<property name="channel"><value>3</value></property>
					<property name="min"><value>-2.0</value></property>
					<property name="max"><value>2.0</value></property>
					<property name="type"><value>29</value></property>
					<property name="z"><value>256</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="dataclass"><value>202</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="subSamples"><value>15</value></property>
					<property name="smartSendCriticalTemp"><value>0.02</value></property>
					<property name="smartSendDeltaTempLow"><value>0.005</value></property>
					<property name="smartSendDeltaTempHigh"><value>0.005</value></property>
				</object>
			</property>
		</object>

		<!-- Must be after the sensor docking or the references to it will be null. -->
		&object.env.pressure; <!-- $pressure -->

		<macids value="mac_id" />

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Subfloor Node</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="criticalPressure" display="Critical Send Pressure" type="java.lang.Double" editable="true" displayed="true" dimension="pressure">0.02</property>
		<property name="deltaPressureLow" display="Low Pressure Send Threshold" type="java.lang.Double" editable="true" displayed="true" dimension="pressureDelta">0.005</property>
		<property name="deltaPressureHigh" display="High Pressure Send Threshold" type="java.lang.Double" editable="true" displayed="true" dimension="pressureDelta">0.005</property>
		<property name="hasTempSensor" display="Temperature Sensor?" type="java.lang.Boolean" editable="true" displayed="true">false</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Subfloor Node</property>

		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="x">
			<property>$node/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$node/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		<varbinding vars="criticalPressure">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendCriticalTemp"]</property>
			<value>criticalPressure</value>
		</varbinding>
		<varbinding vars="deltaPressureLow">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendDeltaTempLow"]</property>
			<value>deltaPressureLow</value>
		</varbinding>
		<varbinding vars="deltaPressureHigh">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendDeltaTempHigh"]</property>
			<value>deltaPressureHigh</value>
		</varbinding>


		<parcel name='hasTempSensorFalse' var='hasTempSensor' display='No Temp Sensor'>
			<value>if( hasTempSensor == 'false' ) { return true; } else { return false; }</value>
			<display setting='shape'>pressure</display>
		</parcel>

		<parcel name='hasTempSensorTrue' var='hasTempSensor' display='With Temp Sensor'>
			<value>if( hasTempSensor == 'true' ) { return true; } else { return false; }</value>
			<display setting='shape'>pressure_temp</display>

			<viscera>
				<object type="wsnsensor" location='$node/properties[name="sensepoints"]'>
					<dlid>DLID:-1</dlid>
					<property name="name"><value>subfloor temp</value></property>
					<property name="channel"><value>4</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
					<property name="type"><value>44</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>256</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
					<property name="subSamples"><value>15</value></property>
					<property name="smartSendCriticalTemp"><value>65.0</value></property>
					<property name="smartSendDeltaTempLow"><value>15</value></property>
					<property name="smartSendDeltaTempHigh"><value>8</value></property>
				</object>

				<object type='generictemperature' as='temperature' autochild='false'>
					<dlid>DLID:-1</dlid>
					<property name='status'><value>1</value></property>
				</object>

				<docking from='$temperature/properties[name="sensor"]' to='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]' />

				<property name="criticalTemp" display="Critical Send Temperature" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">65.0</property>
				<property name="deltaTempLow" display="Low Temperature Send Threshold" type="java.lang.Double" editable="true" displayed="true" dimension="temperatureDelta">15</property>
				<property name="deltaTempHigh" display="High Temperature Send Threshold" type="java.lang.Double" editable="true" displayed="true" dimension="temperatureDelta">8</property>

				<producer id="reftemp" datatype="reftemp" name="Reference Temperature" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]' />

				<varbinding vars='name'>
					<property>$temperature/properties[name='name']</property>
					<value>name + ' - Temperature'</value>
				</varbinding>
				<varbinding vars='x'>
					<property>$temperature/properties[name='x']</property>
					<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
					<value>x</value>
				</varbinding>
				<varbinding vars='y'>
					<property>$temperature/properties[name='y']</property>
					<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
					<value>y</value>
				</varbinding>
				<varbinding vars='location'>
					<property>$temperature/properties[name='location']</property>
					<value>location</value>
				</varbinding>

				<varbinding vars="criticalTemp">
					<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendCriticalTemp"]</property>
					<value>criticalTemp</value>
				</varbinding>
				<varbinding vars="deltaTempLow">
					<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendDeltaTempLow"]</property>
					<value>deltaTempLow</value>
				</varbinding>
				<varbinding vars="deltaTempHigh">
					<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendDeltaTempHigh"]</property>
					<value>deltaTempHigh</value>
				</varbinding>
			</viscera>
		</parcel>
	</component>

</componentlib>
