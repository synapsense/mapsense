<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib name='Custom Modbus' version="&componentlib.version;" cversion="&componentlib.cversion;" oversion="&componentlib.oversion;">

	<component type='generic-modbus-5' classes='placeable' grouppath="Calculations;Wired" filters="Calculations">
		<display>
			<description>Pulls 5 configurable values from a Modbus device and returns those results to a calculation graph.  This Component goes in a Calculation Group and requires a Modbus Data Source.</description>
			<name>5-Point Modbus Integrator</name>
			<shape>modbus_input_numeric</shape>
		</display>

		<property name='name' display='Name' type='java.lang.String' editable='true' displayed='true' dimension=''>Generic Modbus 5</property>
		<property name='x' display='x' type='java.lang.Double' editable='true' displayed='true' dimension='distance'>0</property>
		<property name='y' display='y' type='java.lang.Double' editable='true' displayed='true' dimension='distance'>0</property>
		<property name='configuration' display='Configuration' type='java.lang.String' editable='false' displayed='true' dimension=''>Generic Modbus 5</property>
		<property name='location' display='Location' type='java.lang.String' editable='true' displayed='true' dimension=''></property>
		<property name='deviceid' display='Device ID' type='java.lang.String' editable='true' displayed='true' dimension=''>0</property>
		<property name="pullPeriod" display="Sampling Interval(minutes)" type="java.lang.Integer" editable="true" displayed="true">5</property>

		<property name='$modbusdevice_registers_register1_id' display='Register 1 ID' type='java.lang.Integer' editable='true' displayed='true' dimension=''>0</property>
		<property name="registerMSW_1" display="Register 1 Low (if 32 bit)" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name='$modbusdevice_registers_register1_scale' display='Register 1 Scale' type='java.lang.Double' editable='true' displayed='true' dimension=''>10</property>
		<property name="type_1" display="Register 1 Data Type" type="java.lang.String" editable="true" displayed="true"
		valueChoices="Signed Integer 16:INT16,Unsigned Integer 16:UINT16,Signed Integer 32:INT32,Unsigned Integer 32:UINT32,Floating Point 32:FLOAT32">
		INT16</property>

		<property name='$modbusdevice_registers_register2_id' display='Register 2 ID' type='java.lang.Integer' editable='true' displayed='true' dimension=''>0</property>
		<property name="registerMSW_2" display="Register 2 Low (if 32 bit)" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name='$modbusdevice_registers_register2_scale' display='Register 2 Scale' type='java.lang.Double' editable='true' displayed='true' dimension=''>10</property>
		<property name="type_2" display="Register 2 Data Type" type="java.lang.String" editable="true" displayed="true"
		valueChoices="Signed Integer 16:INT16,Unsigned Integer 16:UINT16,Signed Integer 32:INT32,Unsigned Integer 32:UINT32,Floating Point 32:FLOAT32">
		INT16</property>

		<property name='$modbusdevice_registers_register3_id' display='Register 3 ID' type='java.lang.Integer' editable='true' displayed='true' dimension=''>0</property>
		<property name="registerMSW_3" display="Register 3 Low (if 32 bit)" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name='$modbusdevice_registers_register3_scale' display='Register 3 Scale' type='java.lang.Double' editable='true' displayed='true' dimension=''>10</property>
		<property name="type_3" display="Register 3 Data Type" type="java.lang.String" editable="true" displayed="true"
		valueChoices="Signed Integer 16:INT16,Unsigned Integer 16:UINT16,Signed Integer 32:INT32,Unsigned Integer 32:UINT32,Floating Point 32:FLOAT32">
		INT16</property>

		<property name='$modbusdevice_registers_register4_id' display='Register 4 ID' type='java.lang.Integer' editable='true' displayed='true' dimension=''>0</property>
		<property name="registerMSW_4" display="Register 4 Low (if 32 bit)" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name='$modbusdevice_registers_register4_scale' display='Register 4 Scale' type='java.lang.Double' editable='true' displayed='true' dimension=''>10</property>
		<property name="type_4" display="Register 4 Data Type" type="java.lang.String" editable="true" displayed="true"
		valueChoices="Signed Integer 16:INT16,Unsigned Integer 16:UINT16,Signed Integer 32:INT32,Unsigned Integer 32:UINT32,Floating Point 32:FLOAT32">
		INT16</property>

		<property name='$modbusdevice_registers_register5_id' display='Register 5 ID' type='java.lang.Integer' editable='true' displayed='true' dimension=''>0</property>
		<property name="registerMSW_5" display="Register 5 Low (if 32 bit)" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name='$modbusdevice_registers_register5_scale' display='Register 5 Scale' type='java.lang.Double' editable='true' displayed='true' dimension=''>10</property>
		<property name="type_5" display="Register 5 Data Type" type="java.lang.String" editable="true" displayed="true"
		valueChoices="Signed Integer 16:INT16,Unsigned Integer 16:UINT16,Signed Integer 32:INT32,Unsigned Integer 32:UINT32,Floating Point 32:FLOAT32">
		INT16</property>


		<varbinding vars='$modbusdevice_registers_register1_id'>
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register1"]]/properties[name="id"]</property>
			<value>$modbusdevice_registers_register1_id</value>
		</varbinding>
		<varbinding vars='$modbusdevice_registers_register1_scale'>
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register1"]]/properties[name="scale"]</property>
			<value>$modbusdevice_registers_register1_scale</value>
		</varbinding>

		<varbinding vars="registerMSW_1">
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register1"]]/properties[name="msw"]</property>
			<value>registerMSW_1</value>
		</varbinding>
		<varbinding vars="type_1">
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register1"]]/properties[name="type"]</property>
			<value>type_1</value>
		</varbinding>


		<varbinding vars='$modbusdevice_registers_register2_id'>
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register2"]]/properties[name="id"]</property>
			<value>$modbusdevice_registers_register2_id</value>
		</varbinding>
		<varbinding vars='$modbusdevice_registers_register2_scale'>
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register2"]]/properties[name="scale"]</property>
			<value>$modbusdevice_registers_register2_scale</value>
		</varbinding>

		<varbinding vars="registerMSW_2">
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register2"]]/properties[name="msw"]</property>
			<value>registerMSW_2</value>
		</varbinding>
		<varbinding vars="type_2">
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register2"]]/properties[name="type"]</property>
			<value>type_2</value>
		</varbinding>

		<varbinding vars='$modbusdevice_registers_register3_id'>
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register3"]]/properties[name="id"]</property>
			<value>$modbusdevice_registers_register3_id</value>
		</varbinding>
		<varbinding vars='$modbusdevice_registers_register3_scale'>
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register3"]]/properties[name="scale"]</property>
			<value>$modbusdevice_registers_register3_scale</value>
		</varbinding>

		<varbinding vars="registerMSW_3">
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register3"]]/properties[name="msw"]</property>
			<value>registerMSW_3</value>
		</varbinding>
		<varbinding vars="type_3">
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register3"]]/properties[name="type"]</property>
			<value>type_3</value>
		</varbinding>

		<varbinding vars='$modbusdevice_registers_register4_id'>
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register4"]]/properties[name="id"]</property>
			<value>$modbusdevice_registers_register4_id</value>
		</varbinding>
		<varbinding vars='$modbusdevice_registers_register4_scale'>
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register4"]]/properties[name="scale"]</property>
			<value>$modbusdevice_registers_register4_scale</value>
		</varbinding>

		<varbinding vars="registerMSW_4">
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register4"]]/properties[name="msw"]</property>
			<value>registerMSW_4</value>
		</varbinding>
		<varbinding vars="type_4">
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register4"]]/properties[name="type"]</property>
			<value>type_4</value>
		</varbinding>

		<varbinding vars='$modbusdevice_registers_register5_id'>
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register5"]]/properties[name="id"]</property>
			<value>$modbusdevice_registers_register5_id</value>
		</varbinding>
		<varbinding vars='$modbusdevice_registers_register5_scale'>
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register5"]]/properties[name="scale"]</property>
			<value>$modbusdevice_registers_register5_scale</value>
		</varbinding>

		<varbinding vars="registerMSW_5">
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register5"]]/properties[name="msw"]</property>
			<value>registerMSW_5</value>
		</varbinding>
		<varbinding vars="type_5">
			<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register5"]]/properties[name="type"]</property>
			<value>type_5</value>
		</varbinding>

		<varbinding vars='deviceid'>
			<property>$modbusdevice/properties[name="id"]</property>
			<value>deviceid</value>
		</varbinding>
		<varbinding vars='name'>
			<property>$modbusdevice/properties[name="name"]</property>
			<value>name</value>
		</varbinding>

		<varbinding vars="pullPeriod">
			<property>$modbusdevice/properties[name="registers"]/children/properties[name="lastValue"]/tags[tagName='poll']</property>
			<value>pullPeriod * 60000</value>
		</varbinding>

		<producer id='register1' datatype='power' name='Register 1' object='$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register1"]]' />
		<producer id='register2' datatype='power' name='Register 2' object='$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register2"]]' />
		<producer id='register3' datatype='power' name='Register 3' object='$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register3"]]' />
		<producer id='register4' datatype='power' name='Register 4' object='$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register4"]]' />
		<producer id='register5' datatype='power' name='Register 5' object='$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register5"]]' />

		<object as='modbusdevice' type='modbusdevice'>
			<dlid>DLID:-1</dlid>
			<property name='maxpacketlen'><value>42</value></property>

			<property name='registers'>
				<object type='modbusproperty'>
					<dlid>DLID:-1</dlid>
					<property name='name'><value>register1</value></property>
				</object>
				<object type='modbusproperty'>
					<dlid>DLID:-1</dlid>
					<property name='name'><value>register2</value></property>
				</object>
				<object type='modbusproperty'>
					<dlid>DLID:-1</dlid>
					<property name='name'><value>register3</value></property>
				</object>
				<object type='modbusproperty'>
					<dlid>DLID:-1</dlid>
					<property name='name'><value>register4</value></property>
				</object>
				<object type='modbusproperty'>
					<dlid>DLID:-1</dlid>
					<property name='name'><value>register5</value></property>
				</object>
			</property>
        </object>
	</component>
</componentlib>