<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
		name="proto rack with node child"
		version="&componentlib.version;"
		cversion="&componentlib.cversion;"
		oversion="&componentlib.oversion;"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
		>

	<component type="proto-rack-control-rearexhaust-sf-thermanode2" classes="placeable,rotatable,controlinput,temperaturecontrol,airflowcontrol" grouppath="Environmentals;Wireless;Racks" filters="Environmentals">
		<display>
			<description> (Rack->Node Child Link Prototype) Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors. Designed for front inlet, rear exhaust configurations. Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle, exhaust bottom and subfloor. This configuration is qualified for SynapSense Active Control™.</description>
			<name>(Prototype) Rear Exhaust w/ Subfloor (TN2)</name>
			<shape>rack-rearexhaust-sf-rt</shape>
		</display>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true" dimension="">Rear Exhaust (Prototype)</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true" dimension="">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">42.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">24.0</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true" dimension="">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true" dimension="">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true" dimension=""/>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		<property name="min_allow_h" display="Min Allowed Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">20.0</property>
		<property name="max_allow_h" display="Max Allowed Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">80.0</property>
		<property name="min_recommend_h" display="Min Recommended Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">40.0</property>
		<property name="max_recommend_h" display="Max Recommended Humidity" type="java.lang.Double" editable="true" displayed="true" dimension="">55.0</property>
		<property name="cold_delta" display="Intake Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">5</property>
		<property name="hot_delta" display="Exhaust Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">10</property>
		<property name="powerSource" display="Power Source" type="java.lang.Integer" editable="true" displayed="true" valueChoices="Battery Powered:1,Wall Powered:0">1</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true" dimension="">ThermaNode II Rack Rear Exhaust with Subfloor</property>
		<producer id="reftemp" datatype="reftemp" name="Reference Temperature" object="$rack/properties[name=&quot;ref&quot;]/referrent"/>
		<conducer id="PEBridge" name="Power And Environmental Rack Pair">
			<producer id="envRack" datatype="PEBridge_E" name="Environmental Rack Output" object="$rack"/>
			<consumer id="powerRack" datatype="PEBridge_P" name="Power Rack Input" property="$rack/properties[name='powerRack']" required="false"/>
		</conducer>
		<varbinding vars="powerSource">
			<property>$rack/properties[name="node"]/children[1]/properties[name="batteryOperated"]</property>
			<value>powerSource</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="x"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="x"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="x"]</property>
			<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="y"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="y"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="y"]</property>
			<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="x"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="9"]]/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="y"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="9"]]/properties[name="y"]</property>
			<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x">
			<property>$rack/properties[name="node"]/children[1]/properties[name="x"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<property>$rack/properties[name="x"]</property>
			<value>x</value>
			<property name="property"><value>$rack/properties[name='displaypoints']/children[1]/properties[name='x']</value></property>
		</varbinding>
		<varbinding vars="y">
			<property>$rack/properties[name="node"]/children[1]/properties[name="y"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<property>$rack/properties[name="y"]</property>
			<value>y</value>
			<property name="property"><value>$rack/properties[name='displaypoints']/children[1]/properties[name='y']</value></property>
		</varbinding>
		<varbinding vars="name">
			<property>$rack/properties[name="node"]/children[1]/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="name">
			<property>$rack/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="rotation">
			<property>$rack/properties[name="rotation"]</property>
			<value>rotation</value>
		</varbinding>
		<varbinding vars="width">
			<property>$rack/properties[name="width"]</property>
			<value>width</value>
		</varbinding>
		<varbinding vars="depth">
			<property>$rack/properties[name="depth"]</property>
			<value>depth</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$rack/properties[name="node"]/children[1]/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$rack/properties[name="node"]/children[1]/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$rack/properties[name="node"]/children[1]/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$rack/properties[name="node"]/children[1]/properties[name="location"]</property>
			<property>$rack/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		<varbinding vars="min_allow_t">
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMin"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMin"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMax"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMax"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMin"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMin"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMax"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMax"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		<varbinding vars="min_allow_h">
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMin"]</property>
			<value>min_allow_h</value>
		</varbinding>
		<varbinding vars="max_allow_h">
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="aMax"]</property>
			<value>max_allow_h</value>
		</varbinding>
		<varbinding vars="min_recommend_h">
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMin"]</property>
			<value>min_recommend_h</value>
		</varbinding>
		<varbinding vars="max_recommend_h">
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="rMax"]</property>
			<value>max_recommend_h</value>
		</varbinding>
		<varbinding vars="cold_delta">
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendThreshold"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendThreshold"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(cold_delta)</value>
		</varbinding>
		<varbinding vars="hot_delta">
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="smartSendThreshold"]</property>
			<property>$rack/properties[name="node"]/children[1]/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="smartSendThreshold"]</property>
			<value>(double)(hot_delta)</value>
		</varbinding>
		<docking from="$rack/properties[name=&quot;nodeTemp&quot;]" to="$rack/properties[name='node']/children[1]/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;0&quot;]]"/>
		<docking from="$rack/properties[name=&quot;rh&quot;]" to="$rack/properties[name='node']/children[1]/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;1&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cTop&quot;]" to="$rack/properties[name='node']/children[1]/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;3&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cMid&quot;]" to="$rack/properties[name='node']/children[1]/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;4&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cBot&quot;]" to="$rack/properties[name='node']/children[1]/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;5&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hTop&quot;]" to="$rack/properties[name='node']/children[1]/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;6&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hMid&quot;]" to="$rack/properties[name='node']/children[1]/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;7&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hBot&quot;]" to="$rack/properties[name='node']/children[1]/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;8&quot;]]"/>
		<docking from="$rack/properties[name=&quot;ref&quot;]" to="$rack/properties[name='node']/children[1]/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;9&quot;]]"/>
		<macids value="mac_id"/>
		<docking to="$rack/properties[name='coldDp']" from="$rack/properties[name='displaypoints']/children[1]/properties[name='lastValue']"/>

		<!-- Must be after the cTop, etc., dockings or the references to them will be null. -->
		&object.env.rack.tb.nodeChildPrototype; <!-- $rack -->

	</component>

</componentlib>
