<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
		name="ez racks"
		version="&componentlib.version;"
		cversion="&componentlib.cversion;"
		oversion="&componentlib.oversion;"

		>


<component type='ex-rack-super2' classes='placeable,rotatable' grouppath="New for Io" filters="Environmentals">
	<display>
		<description>Super Y-Wing 2.  This Component goes in a Room and a WSN Data Source.  This configuration is qualified for SynapSense Active Control™.</description>
		<name>Y-Wing Super 2</name>
		<shape>ywing</shape>
	</display>

	<object as='coldNode1' type='wsnnode'>
		<dlid>DLID:-1</dlid>
		<property name="batteryCapacity"><value>2800.0</value></property>
		<property name="batteryOperated"><value>1</value></property>
		<property name="batteryStatus"><value>0</value></property>
		<property name="status"><value>0</value></property>
		<property name="platformId"><value>82</value></property>
		<property name="platformName"><value>YWing</value></property>
		<property name="sensepoints">
			<object type="wsnsensor">
				<dlid>DLID:-1</dlid>
				<property name="name"><value>battery</value></property>
				<property name="channel"><value>2</value></property>
				<property name="min"><value>1.8</value></property>
				<property name="max"><value>3.7</value></property>
				<property name="aMin"><value>2.5</value></property>
				<property name="aMax"><value>3.7</value></property>
				<property name="rMin"><value>2.7</value></property>
				<property name="rMax"><value>3.7</value></property>
				<property name="type"><value>5</value></property>
				<property name="dataclass"><value>210</value></property>
				<property name="enabled"><value>1</value></property>
				<property name="mode"><value>instant</value></property>
				<property name="interval"><value>0</value></property>
				<property name="z"><value>0</value></property>
				<property name="status"><value>1</value></property>
				<property name="lastValue"><value>-5000.0</value></property>
			</object>
			<object type="wsnsensor">
				<dlid>DLID:-1</dlid>
				<property name="name"><value>intake temp</value></property>
				<property name="channel"><value>3</value></property>
				<property name="min"><value>40.0</value></property>
				<property name="max"><value>100.0</value></property>
				<property name="type"><value>39</value></property>
				<property name="dataclass"><value>200</value></property>
				<property name="enabled"><value>1</value></property>
				<property name="mode"><value>instant</value></property>
				<property name="interval"><value>0</value></property>
				<property name="z"><value>0</value></property>
				<property name="status"><value>1</value></property>
				<property name="lastValue"><value>-5000.0</value></property>
				<property name="subSamples"><value>15</value></property>
				<property name="smartSendCriticalTemp"><value>64.4</value></property>
				<property name="smartSendDeltaTempLow"><value>5</value></property>
				<property name="smartSendDeltaTempHigh"><value>5</value></property>
			</object>
		</property>
	</object>


	<macids value="coldMacId1" />

	<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Super Y-Wing 2</property>
	<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true" dimension="">0</property>
	<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">42.0</property>
	<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">24.0</property>
	<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
	<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
	<property name="coldMacId1" display="Intake MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
	<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
	<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"/>
	<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Super Y-Wing 2</property>

	<property name="intake2" display="intake2" type="java.lang.Boolean" editable="true" displayed="true">false</property>
	<property name="intake3" display="intake3" type="java.lang.Boolean" editable="true" displayed="true">false</property>


	<parcel name="intake2" var="intake2" display="intake2" active="false">
		<value>intake2</value>
		<display setting="nebadge">triangle_l_red</display>

		<viscera>
			<object as='coldNode2' type='wsnnode'>
				<dlid>DLID:-1</dlid>
				<property name="batteryCapacity"><value>2800.0</value></property>
				<property name="batteryOperated"><value>1</value></property>
				<property name="batteryStatus"><value>0</value></property>
				<property name="status"><value>0</value></property>
				<property name="platformId"><value>82</value></property>
				<property name="platformName"><value>YWing</value></property>
				<property name="sensepoints">
					<object type="wsnsensor">
						<dlid>DLID:-1</dlid>
						<property name="name"><value>battery</value></property>
						<property name="channel"><value>2</value></property>
						<property name="min"><value>1.8</value></property>
						<property name="max"><value>3.7</value></property>
						<property name="aMin"><value>2.5</value></property>
						<property name="aMax"><value>3.7</value></property>
						<property name="rMin"><value>2.7</value></property>
						<property name="rMax"><value>3.7</value></property>
						<property name="type"><value>5</value></property>
						<property name="dataclass"><value>210</value></property>
						<property name="enabled"><value>1</value></property>
						<property name="mode"><value>instant</value></property>
						<property name="interval"><value>0</value></property>
						<property name="z"><value>0</value></property>
						<property name="status"><value>1</value></property>
						<property name="lastValue"><value>-5000.0</value></property>
					</object>
					<object type="wsnsensor">
						<dlid>DLID:-1</dlid>
						<property name="name"><value>exhaust temp</value></property>
						<property name="channel"><value>3</value></property>
						<property name="min"><value>40.0</value></property>
						<property name="max"><value>100.0</value></property>
						<property name="type"><value>39</value></property>
						<property name="dataclass"><value>200</value></property>
						<property name="enabled"><value>1</value></property>
						<property name="mode"><value>instant</value></property>
						<property name="interval"><value>0</value></property>
						<property name="z"><value>0</value></property>
						<property name="status"><value>1</value></property>
						<property name="lastValue"><value>-5000.0</value></property>
						<property name="subSamples"><value>15</value></property>
						<property name="smartSendCriticalTemp"><value>80.6</value></property>
						<property name="smartSendDeltaTempLow"><value>5</value></property>
						<property name="smartSendDeltaTempHigh"><value>5</value></property>
					</object>
				</property>
			</object>

			<macids value="coldMacId2" />

			<property name="coldMacId2" display="Cold MAC ID 2" type="java.lang.String" editable="true" displayed="true">0</property>

			<varbinding vars="name">
				<property>$coldNode2/properties[name="name"]</property>
				<value>'Node ' + name + ' Exhaust'</value>
			</varbinding>

			<varbinding vars="x,depth,rotation">
				<property>$coldNode2/properties[name="x"]</property>
				<property>$coldNode2/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
				<property>$coldNode2/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
				<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
			</varbinding>

			<varbinding vars="y,depth,rotation">
				<property>$coldNode2/properties[name="y"]</property>
				<property>$coldNode2/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
				<property>$coldNode2/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
				<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
			</varbinding>

			<varbinding vars="coldMacId2">
				<property>$coldNode2/properties[name="mac"]</property>
				<value>Long.parseLong(coldMacId2,16)</value>
			</varbinding>

			<varbinding vars="coldMacId2">
				<property>$coldNode2/properties[name="id"]</property>
				<value>lid.generateLogicalId(coldMacId2, self)</value>
			</varbinding>

			<varbinding vars="sample_interval">
				<property>$coldNode2/properties[name="period"]</property>
				<value>sample_interval + ' min'</value>
			</varbinding>
			<varbinding vars="location">
				<property>$coldNode2/properties[name="location"]</property>
				<value>location</value>
			</varbinding>

			<docking from='$rack/properties[name="cMid"]' to='$coldNode2/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]'/>

		</viscera>
	</parcel>


	<parcel name="intake3" var="intake3" display="intake3" active="false">
		<value>intake3</value>
		<display setting="sebadge">triangle_l_red</display>

		<viscera>
			<object as='coldNode3' type='wsnnode'>
				<dlid>DLID:-1</dlid>
				<property name="batteryCapacity"><value>2800.0</value></property>
				<property name="batteryOperated"><value>1</value></property>
				<property name="batteryStatus"><value>0</value></property>
				<property name="status"><value>0</value></property>
				<property name="platformId"><value>82</value></property>
				<property name="platformName"><value>YWing</value></property>
				<property name="sensepoints">
					<object type="wsnsensor">
						<dlid>DLID:-1</dlid>
						<property name="name"><value>battery</value></property>
						<property name="channel"><value>2</value></property>
						<property name="min"><value>1.8</value></property>
						<property name="max"><value>3.7</value></property>
						<property name="aMin"><value>2.5</value></property>
						<property name="aMax"><value>3.7</value></property>
						<property name="rMin"><value>2.7</value></property>
						<property name="rMax"><value>3.7</value></property>
						<property name="type"><value>5</value></property>
						<property name="dataclass"><value>210</value></property>
						<property name="enabled"><value>1</value></property>
						<property name="mode"><value>instant</value></property>
						<property name="interval"><value>0</value></property>
						<property name="z"><value>0</value></property>
						<property name="status"><value>1</value></property>
						<property name="lastValue"><value>-5000.0</value></property>
					</object>
					<object type="wsnsensor">
						<dlid>DLID:-1</dlid>
						<property name="name"><value>exhaust temp</value></property>
						<property name="channel"><value>3</value></property>
						<property name="min"><value>40.0</value></property>
						<property name="max"><value>100.0</value></property>
						<property name="type"><value>39</value></property>
						<property name="dataclass"><value>200</value></property>
						<property name="enabled"><value>1</value></property>
						<property name="mode"><value>instant</value></property>
						<property name="interval"><value>0</value></property>
						<property name="z"><value>0</value></property>
						<property name="status"><value>1</value></property>
						<property name="lastValue"><value>-5000.0</value></property>
						<property name="subSamples"><value>15</value></property>
						<property name="smartSendCriticalTemp"><value>80.6</value></property>
						<property name="smartSendDeltaTempLow"><value>5</value></property>
						<property name="smartSendDeltaTempHigh"><value>5</value></property>
					</object>
				</property>
			</object>

			<macids value="coldMacId3" />

			<property name="coldMacId3" display="Cold MAC ID 3" type="java.lang.String" editable="true" displayed="true">0</property>

			<varbinding vars="name">
				<property>$coldNode3/properties[name="name"]</property>
				<value>'Node ' + name + ' Intake 3'</value>
			</varbinding>

			<varbinding vars="x,depth,rotation">
				<property>$coldNode3/properties[name="x"]</property>
				<property>$coldNode3/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
				<property>$coldNode3/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
				<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
			</varbinding>

			<varbinding vars="y,depth,rotation">
				<property>$coldNode3/properties[name="y"]</property>
				<property>$coldNode3/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
				<property>$coldNode3/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
				<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
			</varbinding>

			<varbinding vars="coldMacId3">
				<property>$coldNode3/properties[name="mac"]</property>
				<value>Long.parseLong(coldMacId3,16)</value>
			</varbinding>

			<varbinding vars="coldMacId3">
				<property>$coldNode3/properties[name="id"]</property>
				<value>lid.generateLogicalId(coldMacId3 , self)</value>
			</varbinding>

			<varbinding vars="sample_interval">
				<property>$coldNode3/properties[name="period"]</property>
				<value>sample_interval + ' min'</value>
			</varbinding>
			<varbinding vars="location">
				<property>$coldNode3/properties[name="location"]</property>
				<value>location</value>
			</varbinding>

			<docking from='$rack/properties[name="cBot"]' to='$coldNode3/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]'/>

		</viscera>
	</parcel>


	<varbinding vars='name'>
		<property>$cpTemp/properties[name="name"]</property>
		<value>name</value>
	</varbinding>

	<consumer id="reftemp" datatype="reftemp" name="Rack Ref Temp" property="$rack/properties[name=&quot;ref&quot;]" required="false"/>
	<consumer id="exhausttemp" datatype="exhausttemp" name="Rack Exhaust Temp" property="$rack/properties[name=&quot;hTop&quot;]" required="false"/>	

	<varbinding vars="name">
		<property>$rack/properties[name="name"]</property>
		<value>name</value>
	</varbinding>
	<varbinding vars="name">
		<property>$coldNode1/properties[name="name"]</property>
		<value>'Node ' + name + ' Intake'</value>
	</varbinding>

	<varbinding vars="rotation">
		<property>$rack/properties[name="rotation"]</property>
		<value>rotation</value>
	</varbinding>
	<varbinding vars="width">
		<property>$rack/properties[name="width"]</property>
		<value>width</value>
	</varbinding>
	<varbinding vars="depth">
		<property>$rack/properties[name="depth"]</property>
		<value>depth</value>
	</varbinding>
	<varbinding vars="x">
		<property>$rack/properties[name="x"]</property>
		<value>x</value>
	</varbinding>
	<varbinding vars="x,depth,rotation">
		<property>$coldNode1/properties[name="x"]</property>
		<property>$coldNode1/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
		<property>$coldNode1/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
		<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
	</varbinding>

	<varbinding vars="y">
		<property>$rack/properties[name="y"]</property>
		<value>y</value>
	</varbinding>
	<varbinding vars="y,depth,rotation">
		<property>$coldNode1/properties[name="y"]</property>
		<property>$coldNode1/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
		<property>$coldNode1/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
		<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
	</varbinding>

	<varbinding vars="coldMacId1">
		<property>$coldNode1/properties[name="mac"]</property>
		<value>Long.parseLong(coldMacId1,16)</value>
	</varbinding>

	<varbinding vars="coldMacId1">
		<property>$coldNode1/properties[name="id"]</property>
		<value>lid.generateLogicalId(coldMacId1 , self)</value>
	</varbinding>

	<varbinding vars="sample_interval">
		<property>$coldNode1/properties[name="period"]</property>
		<value>sample_interval + ' min'</value>
	</varbinding>
	<varbinding vars="location">
		<property>$coldNode1/properties[name="location"]</property>
		<property>$rack/properties[name="location"]</property>
		<value>location</value>
	</varbinding>

	<docking from="$rack/properties[name=&quot;nodeTemp&quot;]" to="$coldNode1/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;3&quot;]]"/>
	<docking from='$rack/properties[name="cTop"]' to='$coldNode1/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]'/>

	

	<!-- Must be after the cTop, etc., dockings or the references to them will be null. -->
	&object.env.rack.tb.nohumidity; <!-- $rack -->

</component>


</componentlib>