<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
	name="ipmi integration"
	version="&componentlib.version;"
	cversion="&componentlib.cversion;"
	oversion="&componentlib.oversion;"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
	>

	<component type="ipmipowerrack-demo" classes="placeable,rotatable" grouppath="Environmentals;Wired;IPMI"  filters="Environmentals">
		<display>
			<description>IPMI Rack Power</description>
			<name>IPMI Rack Power</name>
			<shape>ipmi-inlet-outlet</shape>
		</display>

		<object type="ipmiserver" as="top">
			<dlid>DLID:-1</dlid>
			<property name="ipmisensors">
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>intaketemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="z"><value>131072</value></property>
					<property name="lastValue"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
				</object>
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>exhausttemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="z"><value>131072</value></property>
					<property name="lastValue"><value>0</value></property>
				</object>
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>power</value></property>
					<property name="lastValue"><value>0</value></property>
				</object>
			</property>
		</object>
		
		<object type="ipmiserver" as="middle">
			<dlid>DLID:-1</dlid>
			<property name="ipmisensors">
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>intaketemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="z"><value>16384</value></property>
					<property name="lastValue"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
				</object>
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>exhausttemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="z"><value>16384</value></property>
					<property name="lastValue"><value>0</value></property>
				</object>
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>power</value></property>
					<property name="lastValue"><value>0</value></property>
				</object>
			</property>
		</object>
		
		<object type="ipmiserver" as="bottom">
			<dlid>DLID:-1</dlid>
			<property name="ipmisensors">
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>intaketemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="z"><value>2048</value></property>
					<property name="lastValue"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
				</object>
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>exhausttemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="z"><value>2048</value></property>
					<property name="lastValue"><value>0</value></property>
				</object>
			</property>
		</object>
		
		<object type="rack" as="rack">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
		</object>
		
		<object type="power_rack" as="power_rack">
			<dlid>DLID:-1</dlid>
			<property name="status"> <value>1</value> </property>
			<property name="lastResetTime"><value>0</value></property>
			<property name="platform"><value>SC</value></property>
			<property name="instrumentation"><value>0</value></property>
			<property name="maxUBlock"><value>25</value></property>
			<property name="servers">
				<object type="server">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Dell PowerEdge R310</value></property>
					<property name="model"><value>Dell PowerEdge R310</value></property>
					<property name="uLocation"><value>22</value></property>
					<property name="uHeight"><value>1</value></property>
				</object>
				<object type="server">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Dell PowerEdge R515</value></property>
					<property name="model"><value>Dell PowerEdge R515</value></property>
					<property name="uLocation"><value>17</value></property>
					<property name="uHeight"><value>2</value></property>
				</object>
				<object type="server">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Dell PowerEdge 2950</value></property>
					<property name="model"><value>Dell PowerEdge 2950</value></property>
					<property name="uLocation"><value>10</value></property>
					<property name="uHeight"><value>2</value></property>
				</object>
			</property>
		</object>
		
		<object type="xerox" as="top_xerox">
			<dlid>DLID:-1</dlid>
		</object>
		<docking to="$top/children[properties[name='name'][value='power']]" from="$top_xerox/properties[name='input']"/>
		<docking to='$power_rack/children[properties[name="name"][value="Dell PowerEdge R310"]]/properties[name="fpDemandPower"]' from="$top_xerox/properties[name='output']"/>
		
		<object type="xerox" as="middle_xerox">
			<dlid>DLID:-1</dlid>
		</object>	
		<docking to="$middle/children[properties[name='name'][value='power']]" from="$middle_xerox/properties[name='input']"/>
		<docking to='$power_rack/children[properties[name="name"][value="Dell PowerEdge R515"]]/properties[name="fpDemandPower"]' from="$middle_xerox/properties[name='output']"/>
		
		
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">IPMI Rack Power</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true" dimension="">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">42.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">24.0</property>
		
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true" dimension=""/>
		
		<property name="period" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		
		<property name="privilegelevel" display="Privilege Level" type="java.lang.String" editable="true" displayed="true" valueChoices="Administrator:Administrator,Operator:Operator,User:User,Callback:Callback">User</property>
		<property name="user" display="User" type="java.lang.String" editable="true" displayed="true">ipmiuser</property>
		<property name="password" display="Password" type="java.lang.String" editable="true" displayed="true">Synap4ip</property>
		
		<!--Top server property -->
		<property name="topaddress" display="Top Server: Address" type="java.lang.String" editable="true" displayed="true">172.30.100.90</property>
		<property name="topinletsensorid" display="Top Server: ID of Intake Temp Sensor" type="java.lang.Integer" editable="true" displayed="true">4</property>
		<property name="topoutletsensorid" display="Top Server: ID of Exhaust Temp Sensor" type="java.lang.Integer" editable="true" displayed="true">7</property>
		<property name="toppowersensorid" display="Top Server: ID of Power Sensor" type="java.lang.Integer" editable="true" displayed="true">60</property>
		
		<!--Middle server property -->
		<property name="middleaddress" display="Middle Server: Address" type="java.lang.String" editable="true" displayed="true">172.30.100.93</property>
		<property name="middleinletsensorid" display="Middle Server: ID of Intake Temp Sensor" type="java.lang.Integer" editable="true" displayed="true">8</property>
		<property name="middleoutletsensorid" display="Middle Server: ID of Exhaust Temp Sensor" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="middlepowersensorid" display="Middle Server: ID of Power Sensor" type="java.lang.Integer" editable="true" displayed="true">67</property>
		
		<!--Bottom server property -->
		<property name="bottomaddress" display="Bottom Server: Address" type="java.lang.String" editable="true" displayed="true">172.30.100.92</property>
		<property name="bottominletsensorid" display="Bottom Server: ID of Intake Temp Sensor" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="bottomoutletsensorid" display="Bottom Server: ID of Exhaust Temp Sensor" type="java.lang.Integer" editable="true" displayed="true">4</property>
		
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">IPMI Rack Power</property>
		
		
		<property name="valueconv" type="java.lang.String" editable="false" displayed="false">return (Double) (value / 1000)</property>
		<!--
		<producer id="topinlet" datatype="temperature" name="Top Inlet Temp" object="$rack/properties[name=&quot;cTop&quot;]/referrent"/>
		<producer id="midinlet" datatype="temperature_mid" name="Middle Inlet Temp" object="$rack/properties[name=&quot;cMid&quot;]/referrent"/>
		<producer id="botinlet" datatype="temperature_bot" name="Bottom Inlet Temp" object="$rack/properties[name=&quot;cBot&quot;]/referrent"/>-->
		<consumer id="reftemp" datatype="reftemp" name="Rack Ref Temp" property="$rack/properties[name=&quot;ref&quot;]" required="false"/>
		
		<varbinding vars="name">
			<property>$top/properties[name="name"]</property>
			<property>$middle/properties[name="name"]</property>
			<property>$bottom/properties[name="name"]</property>
			<property>$rack/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="privilegelevel">
			<property>$top/properties[name="privilegelevel"]</property>
			<property>$middle/properties[name="privilegelevel"]</property>
			<property>$bottom/properties[name="privilegelevel"]</property>
			<value>privilegelevel</value>
		</varbinding>
		<varbinding vars="user">
			<property>$top/properties[name="user"]</property>
			<property>$middle/properties[name="user"]</property>
			<property>$bottom/properties[name="user"]</property>
			<value>user</value>
		</varbinding>
		<varbinding vars="password">
			<property>$top/properties[name="password"]</property>
			<property>$middle/properties[name="password"]</property>
			<property>$bottom/properties[name="password"]</property>
			<value>password</value>
		</varbinding>
		
		<!--Set polling interval-->
		<varbinding vars="period">
			<property>$top/children[properties[name="name"][value="intaketemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<property>$top/children[properties[name="name"][value="exhausttemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<property>$top/children[properties[name="name"][value="power"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<property>$middle/children[properties[name="name"][value="intaketemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<property>$middle/children[properties[name="name"][value="exhausttemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<property>$middle/children[properties[name="name"][value="power"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<property>$bottom/children[properties[name="name"][value="intaketemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<property>$bottom/children[properties[name="name"][value="exhausttemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<value>60000*period</value>
		</varbinding>
		
		<!-- Var binding Top Server -->
		<varbinding vars="topaddress">
			<property>$top/properties[name="address"]</property>
			<value>topaddress</value>
		</varbinding>
		<varbinding vars="topinletsensorid">
			<property>$top/children[properties[name="name"][value="intaketemp"]]/properties[name="sensorid"]</property>
			<value>topinletsensorid</value>
		</varbinding>
		<varbinding vars="topoutletsensorid">
			<property>$top/children[properties[name="name"][value="exhausttemp"]]/properties[name="sensorid"]</property>
			<value>topoutletsensorid</value>
		</varbinding>
		<varbinding vars="toppowersensorid">
			<property>$top/children[properties[name="name"][value="power"]]/properties[name="sensorid"]</property>
			<value>toppowersensorid</value>
		</varbinding>
		
		<!-- Var binding Middle Server -->
		<varbinding vars="middleaddress">
			<property>$middle/properties[name="address"]</property>
			<value>middleaddress</value>
		</varbinding>
		<varbinding vars="middleinletsensorid">
			<property>$middle/children[properties[name="name"][value="intaketemp"]]/properties[name="sensorid"]</property>
			<value>middleinletsensorid</value>
		</varbinding>
		<varbinding vars="middleoutletsensorid">
			<property>$middle/children[properties[name="name"][value="exhausttemp"]]/properties[name="sensorid"]</property>
			<value>middleoutletsensorid</value>
		</varbinding>
		<varbinding vars="middlepowersensorid">
			<property>$middle/children[properties[name="name"][value="power"]]/properties[name="sensorid"]</property>
			<value>middlepowersensorid</value>
		</varbinding>
		
		<!-- Var binding Bottom Server -->
		<varbinding vars="bottomaddress">
			<property>$bottom/properties[name="address"]</property>
			<value>bottomaddress</value>
		</varbinding>
		<varbinding vars="bottominletsensorid">
			<property>$bottom/children[properties[name="name"][value="intaketemp"]]/properties[name="sensorid"]</property>
			<value>bottominletsensorid</value>
		</varbinding>
		<varbinding vars="bottomoutletsensorid">
			<property>$bottom/children[properties[name="name"][value="exhausttemp"]]/properties[name="sensorid"]</property>
			<value>bottomoutletsensorid</value>
		</varbinding> 

		<!--Rack varbinding-->
		<varbinding vars="x">
			<property>$rack/properties[name="x"]</property>
			<property>$power_rack/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$rack/properties[name="y"]</property>
			<property>$power_rack/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="rotation">
			<property>$rack/properties[name="rotation"]</property>
			<property>$power_rack/properties[name="rotation"]</property>
			<value>rotation</value>
		</varbinding>
		<varbinding vars="width">
			<property>$rack/properties[name="width"]</property>
			<property>$power_rack/properties[name="width"]</property>
			<value>width</value>
		</varbinding>
		<varbinding vars="depth">
			<property>$rack/properties[name="depth"]</property>
			<property>$power_rack/properties[name="depth"]</property>
			<value>depth</value>
		</varbinding>
		<varbinding vars="location">
			<property>$rack/properties[name="location"]</property>
			<property>$power_rack/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		
		<!-- Min max-->
		<varbinding vars="min_allow_t">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMin"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMin"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMax"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMax"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMin"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMin"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMax"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMax"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		
		<!--Rotatable-->
		<varbinding vars="x,depth,rotation">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="x"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="x"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="y"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="y"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="y"]</property>
			<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		
		<varbinding vars="x,depth,rotation">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="exhausttemp"]]/properties[name="x"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="exhausttemp"]]/properties[name="x"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="exhausttemp"]]/properties[name="x"]</property>
			<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="exhausttemp"]]/properties[name="y"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="exhausttemp"]]/properties[name="y"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="exhausttemp"]]/properties[name="y"]</property>
			<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		
		<varbinding vars="valueconv">
			<property>$top/children[properties[name="name"][value="power"]]/properties[name="lastValue"]/tags[tagName='elConvGet']</property>
			<property>$middle/children[properties[name="name"][value="power"]]/properties[name="lastValue"]/tags[tagName='elConvGet']</property>
			<value>valueconv</value>
		</varbinding>

		<docking from="$rack/properties[name=&quot;cTop&quot;]" to="$top/children[properties[name=&quot;name&quot;][value=&quot;intaketemp&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cMid&quot;]" to="$middle/children[properties[name=&quot;name&quot;][value=&quot;intaketemp&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cBot&quot;]" to="$bottom/children[properties[name=&quot;name&quot;][value=&quot;intaketemp&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hTop&quot;]" to="$top/children[properties[name=&quot;name&quot;][value=&quot;exhausttemp&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hMid&quot;]" to="$middle/children[properties[name=&quot;name&quot;][value=&quot;exhausttemp&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hBot&quot;]" to="$bottom/children[properties[name=&quot;name&quot;][value=&quot;exhausttemp&quot;]]"/>
		
		<docking from="$rack/properties[name=&quot;powerRack&quot;]" to="$power_rack"/>
		
		<docking from="$power_rack/properties[name=&quot;envRack&quot;]" to="$rack"/>
		
	</component>
	
</componentlib>