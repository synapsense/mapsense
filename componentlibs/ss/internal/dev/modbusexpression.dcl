<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
		name="modbus expressions"
		version="&componentlib.version;"
		cversion="&componentlib.cversion;"
		oversion="&componentlib.oversion;"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
		>

	<component type="modbus-input-expression" classes="placeable" grouppath="Calculations;Testing" filters="Calculations">
		<display>
			<description>Pulls a single configurable register from a Modbus device and returns that result to a calculation graph.  This Component goes in a Calculation Group and requires a Modbus Data Source.</description>
			<name>Modbus Expression Integrator</name>
			<shape>modbus_input_numeric</shape>
		</display>

		<object type="modbusdevice" as="device">
			<dlid>DLID:-1</dlid>
			<property name="maxpacketlen"><value>1</value></property>
			<property name="registers">
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
				</object>
			</property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Modbus Integrator</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Modbus Integrator</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="devid" display="Modbus ID" type="java.lang.Integer" editable="true" displayed="false">0</property>
		<property name="register" display="Register" type="java.lang.Integer" editable="true" displayed="false">40001</property>
		<property name="registerMSW" display="Low Register (if 32 bit)" type="java.lang.Integer" editable="true" displayed="false">0</property>
		<property name="type" display="Data Type" type="java.lang.String" editable="true" displayed="false"
				  valueChoices="Signed Integer 16:INT16,Unsigned Integer 16:UINT16,Signed Integer 32:INT32,Unsigned Integer 32:UINT32,Floating Point 32:FLOAT32">
			INT16</property>
		<property name="scale" display="Scaling Factor" type="java.lang.Integer" editable="true" displayed="false">1</property>
		<property name="pullPeriod" display="Sampling Interval (minutes)" type="java.lang.Integer" editable="true" displayed="true">5</property>

		<property name='setpointRead' display='Value Read' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
		<property name='setpointReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
		<property name='setpointReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>
		<!--varbinding vars='setpointRead'> <property>$config/properties[name="setpointRead"]</property> <value>setpointRead</value> </varbinding-->
		<varbinding vars='setpointRead'>
			<property>$device/properties[name="registers"]/children[1]/properties[name="id"]</property>
			<value>  </value>
		</varbinding>

		<producer id="register" datatype="power" name="Modbus Value" object='$device/properties[name="registers"]/children[1]' />

		<varbinding vars="name">
			<property>$device/properties[name="name"]</property>
			<property>$device/properties[name="registers"]/children[1]/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="devid">
			<property>$device/properties[name="id"]</property>
			<value>devid</value>
		</varbinding>
		<varbinding vars="register">
			<property>$device/properties[name="registers"]/children[1]/properties[name="id"]</property>
			<value>register</value>
		</varbinding>

		<varbinding vars="registerMSW">
			<property>$device/properties[name="registers"]/children[1]/properties[name="msw"]</property>
			<value>registerMSW</value>
		</varbinding>

		<varbinding vars="type">
			<property>$device/properties[name="registers"]/children[1]/properties[name="type"]</property>
			<value>type</value>
		</varbinding>

		<varbinding vars="scale">
			<property>$device/properties[name="registers"]/children[1]/properties[name="scale"]</property>
			<value>scale</value>
		</varbinding>

		<varbinding vars="pullPeriod">
			<property>$device/properties[name="registers"]/children/properties[name="lastValue"]/tags[tagName='poll']</property>
			<value>pullPeriod * 60000</value>
		</varbinding>
	</component>

</componentlib>
