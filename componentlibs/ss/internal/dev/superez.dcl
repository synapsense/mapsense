<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
		name="ez racks"
		version="&componentlib.version;"
		cversion="&componentlib.cversion;"
		oversion="&componentlib.oversion;"

		>


<component type='ex-rack-super' classes='placeable,rotatable' grouppath="New for Jupiter2" filters="Environmentals">
	<display>
		<description>Super Y-Wing.  This Component goes in a Room and a WSN Data Source.  This configuration is qualified for SynapSense Active Control™.</description>
		<name>Y-Wing Super</name>
		<shape>ywing</shape>
	</display>

	<object as='coldNode' type='wsnnode'>
		<dlid>DLID:-1</dlid>
		<property name="batteryCapacity"><value>2800.0</value></property>
		<property name="batteryOperated"><value>1</value></property>
		<property name="batteryStatus"><value>0</value></property>
		<property name="status"><value>0</value></property>
		<property name="platformId"><value>82</value></property>
		<property name="platformName"><value>YWing</value></property>
		<property name="sensepoints">
			<object type="wsnsensor">
				<dlid>DLID:-1</dlid>
				<property name="name"><value>battery</value></property>
				<property name="channel"><value>2</value></property>
				<property name="min"><value>1.8</value></property>
				<property name="max"><value>3.7</value></property>
				<property name="aMin"><value>2.5</value></property>
				<property name="aMax"><value>3.7</value></property>
				<property name="rMin"><value>2.7</value></property>
				<property name="rMax"><value>3.7</value></property>
				<property name="type"><value>5</value></property>
				<property name="dataclass"><value>210</value></property>
				<property name="enabled"><value>1</value></property>
				<property name="mode"><value>instant</value></property>
				<property name="interval"><value>0</value></property>
				<property name="z"><value>0</value></property>
				<property name="status"><value>1</value></property>
				<property name="lastValue"><value>-5000.0</value></property>
			</object>
			<object type="wsnsensor">
				<dlid>DLID:-1</dlid>
				<property name="name"><value>intake temp</value></property>
				<property name="channel"><value>3</value></property>
				<property name="min"><value>40.0</value></property>
				<property name="max"><value>100.0</value></property>
				<property name="type"><value>39</value></property>
				<property name="dataclass"><value>200</value></property>
				<property name="enabled"><value>1</value></property>
				<property name="mode"><value>instant</value></property>
				<property name="interval"><value>0</value></property>
				<property name="z"><value>0</value></property>
				<property name="status"><value>1</value></property>
				<property name="lastValue"><value>-5000.0</value></property>
				<property name="subSamples"><value>15</value></property>
				<property name="smartSendCriticalTemp"><value>64.4</value></property>
				<property name="smartSendDeltaTempLow"><value>5</value></property>
				<property name="smartSendDeltaTempHigh"><value>5</value></property>
			</object>
		</property>
	</object>


	<macids value="coldMacId" />

	<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Y-Wing 1/1</property>
	<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true" dimension="">0</property>
	<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">42.0</property>
	<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">24.0</property>
	<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
	<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
	<property name="coldMacId" display="Intake MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
	<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
	<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"/>
	<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Super Y-Wing 1/1</property>

	<property name="intake_layer" display="Intake Side Layer" type="java.lang.String" editable="true" displayed="true" valueChoices="Top:top,Middle:middle,Bottom:bottom">top</property>

	<property name="exhaust1" display="exhaust1" type="java.lang.Boolean" editable="true" displayed="true">false</property>
	<property name="exhaust2" display="exhaust2" type="java.lang.Boolean" editable="true" displayed="true">false</property>


	<multidocking var='intake_layer' target='$coldNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]' mode='to' >
		<option name='top' path='$rack/properties[name="cTop"]'  >
			<display setting="nwbadge">control_input_temperature</display>
			<setproperty path='$coldNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]'>131072</setproperty>
		</option>

		<option name='middle' path='$rack/properties[name="cMid"]' >
			<display setting="nwbadge">control_input_temperature_mid</display>
			<setproperty path='$coldNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]'>16384</setproperty>
		</option>

		<option name='bottom' path='$rack/properties[name="cBot"]'  >
			<display setting="nwbadge">control_input_temperature_bot</display>
			<setproperty path='$coldNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]'>2048</setproperty>
		</option>
	</multidocking>


	<parcel name="exhaust1" var="exhaust1" display="exhaust1" active="false">
		<value>exhaust1</value>
		<display setting="nebadge">triangle_l_red</display>

		<viscera>
			<object as='hotNode' type='wsnnode'>
				<dlid>DLID:-1</dlid>
				<property name="batteryCapacity"><value>2800.0</value></property>
				<property name="batteryOperated"><value>1</value></property>
				<property name="batteryStatus"><value>0</value></property>
				<property name="status"><value>0</value></property>
				<property name="platformId"><value>82</value></property>
				<property name="platformName"><value>YWing</value></property>
				<property name="sensepoints">
					<object type="wsnsensor">
						<dlid>DLID:-1</dlid>
						<property name="name"><value>battery</value></property>
						<property name="channel"><value>2</value></property>
						<property name="min"><value>1.8</value></property>
						<property name="max"><value>3.7</value></property>
						<property name="aMin"><value>2.5</value></property>
						<property name="aMax"><value>3.7</value></property>
						<property name="rMin"><value>2.7</value></property>
						<property name="rMax"><value>3.7</value></property>
						<property name="type"><value>5</value></property>
						<property name="dataclass"><value>210</value></property>
						<property name="enabled"><value>1</value></property>
						<property name="mode"><value>instant</value></property>
						<property name="interval"><value>0</value></property>
						<property name="z"><value>0</value></property>
						<property name="status"><value>1</value></property>
						<property name="lastValue"><value>-5000.0</value></property>
					</object>
					<object type="wsnsensor">
						<dlid>DLID:-1</dlid>
						<property name="name"><value>exhaust temp</value></property>
						<property name="channel"><value>3</value></property>
						<property name="min"><value>40.0</value></property>
						<property name="max"><value>100.0</value></property>
						<property name="type"><value>39</value></property>
						<property name="dataclass"><value>200</value></property>
						<property name="enabled"><value>1</value></property>
						<property name="mode"><value>instant</value></property>
						<property name="interval"><value>0</value></property>
						<property name="z"><value>0</value></property>
						<property name="status"><value>1</value></property>
						<property name="lastValue"><value>-5000.0</value></property>
						<property name="subSamples"><value>15</value></property>
						<property name="smartSendCriticalTemp"><value>80.6</value></property>
						<property name="smartSendDeltaTempLow"><value>5</value></property>
						<property name="smartSendDeltaTempHigh"><value>5</value></property>
					</object>
				</property>
			</object>

			<macids value="hotMacId" />

			<property name="hotMacId" display="Exhaust MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
			<property name="exhaust_layer1" display="Exhaust Side 1 Layer" type="java.lang.String" editable="true" displayed="true" valueChoices="Top:top,Middle:middle,Bottom:bottom">top</property>


			<multidocking var='exhaust_layer1' target='$hotNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]' mode='to' >
				<option name='top' path='$rack/properties[name="hTop"]' >
					<setproperty path='$hotNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]'>131072</setproperty>
				</option>

				<option name='middle' path='$rack/properties[name="hMid"]' >
					<setproperty path='$hotNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]'>16384</setproperty>
				</option>

				<option name='bottom' path='$rack/properties[name="hBot"]' >
					<setproperty path='$hotNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]'>2048</setproperty>
				</option>
			</multidocking>

			<varbinding vars="name">
				<property>$hotNode/properties[name="name"]</property>
				<value>'Node ' + name + ' Exhaust'</value>
			</varbinding>

			<varbinding vars="x,depth,rotation">
				<property>$hotNode/properties[name="x"]</property>
				<property>$hotNode/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
				<property>$hotNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
				<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
			</varbinding>

			<varbinding vars="y,depth,rotation">
				<property>$hotNode/properties[name="y"]</property>
				<property>$hotNode/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
				<property>$hotNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
				<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
			</varbinding>

			<varbinding vars="hotMacId">
				<property>$hotNode/properties[name="mac"]</property>
				<value>Long.parseLong(hotMacId,16)</value>
			</varbinding>

			<varbinding vars="hotMacId">
				<property>$hotNode/properties[name="id"]</property>
				<value>lid.generateLogicalId(hotMacId , self)</value>
			</varbinding>

			<varbinding vars="sample_interval">
				<property>$hotNode/properties[name="period"]</property>
				<value>sample_interval + ' min'</value>
			</varbinding>
			<varbinding vars="location">
				<property>$hotNode/properties[name="location"]</property>
				<value>location</value>
			</varbinding>


		</viscera>
	</parcel>


	<parcel name="exhaust2" var="exhaust2" display="exhaust2" active="false">
		<value>exhaust2</value>
		<display setting="sebadge">triangle_l_red</display>

		<viscera>
			<object as='hotNode2' type='wsnnode'>
				<dlid>DLID:-1</dlid>
				<property name="batteryCapacity"><value>2800.0</value></property>
				<property name="batteryOperated"><value>1</value></property>
				<property name="batteryStatus"><value>0</value></property>
				<property name="status"><value>0</value></property>
				<property name="platformId"><value>82</value></property>
				<property name="platformName"><value>YWing</value></property>
				<property name="sensepoints">
					<object type="wsnsensor">
						<dlid>DLID:-1</dlid>
						<property name="name"><value>battery</value></property>
						<property name="channel"><value>2</value></property>
						<property name="min"><value>1.8</value></property>
						<property name="max"><value>3.7</value></property>
						<property name="aMin"><value>2.5</value></property>
						<property name="aMax"><value>3.7</value></property>
						<property name="rMin"><value>2.7</value></property>
						<property name="rMax"><value>3.7</value></property>
						<property name="type"><value>5</value></property>
						<property name="dataclass"><value>210</value></property>
						<property name="enabled"><value>1</value></property>
						<property name="mode"><value>instant</value></property>
						<property name="interval"><value>0</value></property>
						<property name="z"><value>0</value></property>
						<property name="status"><value>1</value></property>
						<property name="lastValue"><value>-5000.0</value></property>
					</object>
					<object type="wsnsensor">
						<dlid>DLID:-1</dlid>
						<property name="name"><value>exhaust temp</value></property>
						<property name="channel"><value>3</value></property>
						<property name="min"><value>40.0</value></property>
						<property name="max"><value>100.0</value></property>
						<property name="type"><value>39</value></property>
						<property name="dataclass"><value>200</value></property>
						<property name="enabled"><value>1</value></property>
						<property name="mode"><value>instant</value></property>
						<property name="interval"><value>0</value></property>
						<property name="z"><value>0</value></property>
						<property name="status"><value>1</value></property>
						<property name="lastValue"><value>-5000.0</value></property>
						<property name="subSamples"><value>15</value></property>
						<property name="smartSendCriticalTemp"><value>80.6</value></property>
						<property name="smartSendDeltaTempLow"><value>5</value></property>
						<property name="smartSendDeltaTempHigh"><value>5</value></property>
					</object>
				</property>
			</object>

			<macids value="hotMacId2" />

			<property name="hotMacId2" display="Exhaust 2 MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
			<property name="exhaust_layer2" display="Exhaust Side 2 Layer" type="java.lang.String" editable="true" displayed="true" valueChoices="Top:top,Middle:middle,Bottom:bottom">bottom</property>


			<multidocking var='exhaust_layer2' target='$hotNode2/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]' mode='to' >
				<option name='top' path='$rack/properties[name="hTop"]' >
					<setproperty path='$hotNode2/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]'>131072</setproperty>
				</option>

				<option name='middle' path='$rack/properties[name="hMid"]' >
					<setproperty path='$hotNode2/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]'>16384</setproperty>
				</option>

				<option name='bottom' path='$rack/properties[name="hBot"]' >
					<setproperty path='$hotNode2/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]'>2048</setproperty>
				</option>
			</multidocking>

			<varbinding vars="name">
				<property>$hotNode2/properties[name="name"]</property>
				<value>'Node ' + name + ' Exhaust 2'</value>
			</varbinding>

			<varbinding vars="x,depth,rotation">
				<property>$hotNode2/properties[name="x"]</property>
				<property>$hotNode2/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
				<property>$hotNode2/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
				<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
			</varbinding>

			<varbinding vars="y,depth,rotation">
				<property>$hotNode2/properties[name="y"]</property>
				<property>$hotNode2/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
				<property>$hotNode2/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
				<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
			</varbinding>

			<varbinding vars="hotMacId2">
				<property>$hotNode2/properties[name="mac"]</property>
				<value>Long.parseLong(hotMacId2,16)</value>
			</varbinding>

			<varbinding vars="hotMacId2">
				<property>$hotNode2/properties[name="id"]</property>
				<value>lid.generateLogicalId(hotMacId2 , self)</value>
			</varbinding>

			<varbinding vars="sample_interval">
				<property>$hotNode2/properties[name="period"]</property>
				<value>sample_interval + ' min'</value>
			</varbinding>
			<varbinding vars="location">
				<property>$hotNode2/properties[name="location"]</property>
				<value>location</value>
			</varbinding>

		</viscera>
	</parcel>


	<varbinding vars='name'>
		<property>$cp/properties[name="name"]</property>
		<value>name</value>
	</varbinding>

	<consumer id="reftemp" datatype="reftemp" name="Rack Ref Temp" property="$rack/properties[name=&quot;ref&quot;]" required="false"/>

	<varbinding vars="name">
		<property>$rack/properties[name="name"]</property>
		<value>name</value>
	</varbinding>
	<varbinding vars="name">
		<property>$coldNode/properties[name="name"]</property>
		<value>'Node ' + name + ' Intake'</value>
	</varbinding>

	<varbinding vars="rotation">
		<property>$rack/properties[name="rotation"]</property>
		<value>rotation</value>
	</varbinding>
	<varbinding vars="width">
		<property>$rack/properties[name="width"]</property>
		<value>width</value>
	</varbinding>
	<varbinding vars="depth">
		<property>$rack/properties[name="depth"]</property>
		<value>depth</value>
	</varbinding>
	<varbinding vars="x">
		<property>$rack/properties[name="x"]</property>
		<value>x</value>
	</varbinding>
	<varbinding vars="x,depth,rotation">
		<property>$coldNode/properties[name="x"]</property>
		<property>$coldNode/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
		<property>$coldNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
		<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
	</varbinding>

	<varbinding vars="y">
		<property>$rack/properties[name="y"]</property>
		<value>y</value>
	</varbinding>
	<varbinding vars="y,depth,rotation">
		<property>$coldNode/properties[name="y"]</property>
		<property>$coldNode/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
		<property>$coldNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
		<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
	</varbinding>

	<varbinding vars="coldMacId">
		<property>$coldNode/properties[name="mac"]</property>
		<value>Long.parseLong(coldMacId,16)</value>
	</varbinding>

	<varbinding vars="coldMacId">
		<property>$coldNode/properties[name="id"]</property>
		<value>lid.generateLogicalId(coldMacId , self)</value>
	</varbinding>

	<varbinding vars="sample_interval">
		<property>$coldNode/properties[name="period"]</property>
		<value>sample_interval + ' min'</value>
	</varbinding>
	<varbinding vars="location">
		<property>$coldNode/properties[name="location"]</property>
		<property>$rack/properties[name="location"]</property>
		<value>location</value>
	</varbinding>


	<!-- Must be after the cTop, etc., dockings or the references to them will be null. -->
	&object.env.rack.tb.nohumidity; <!-- $rack -->

</component>


</componentlib>