<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
	name="ipmi integration"
	version="&componentlib.version;"
	cversion="&componentlib.cversion;"
	oversion="&componentlib.oversion;"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
	>

	<component type="ipmirack-intake" classes="placeable,rotatable" grouppath="Environmentals;Wired;IPMI"  filters="Environmentals">
		<display>
			<description>IPMI Rack Intake</description>
			<name>IPMI Rack Intake</name>
			<shape>rack-ipmi-intake</shape>
		</display>

		<object type="ipmiserver" as="top">
			<dlid>DLID:-1</dlid>
			<property name="ipmisensors">
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>intaketemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="lastValue"><value>0.0</value></property>
					<property name="z"><value>131072</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
				</object>
			</property>
		</object>
		
		<object type="ipmiserver" as="middle">
			<dlid>DLID:-1</dlid>
			<property name="ipmisensors">
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>intaketemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="lastValue"><value>0.0</value></property>
					<property name="z"><value>16384</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
				</object>
			</property>
		</object>
		
		<object type="ipmiserver" as="bottom">
			<dlid>DLID:-1</dlid>
			<property name="ipmisensors">
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>intaketemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="lastValue"><value>0.0</value></property>
					<property name="z"><value>2048</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
				</object>
			</property>
		</object>
		
		<object type="rack" as="rack">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
		</object>
		
		<object type="displaypoint" as="dptop">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>top</value></property>
			<property name="status"><value>1</value></property>
			<property name="dataclass"><value>200</value></property>
			<property name="z"><value>131072</value></property>
		</object>
		<object type="displaypoint" as="dpmiddle">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>middle</value></property>
			<property name="status"><value>1</value></property>
			<property name="dataclass"><value>200</value></property>
			<property name="z"><value>16384</value></property>
		</object>
		<object type="displaypoint" as="dpbottom">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>bottom</value></property>
			<property name="status"><value>1</value></property>
			<property name="dataclass"><value>200</value></property>
			<property name="z"><value>2048</value></property>
		</object>
		
		<docking to="$top/children[properties[name='name'][value='intaketemp']]/properties[name='lastValue']" from="$dptop/properties[name='lastValue']"/>
		<docking to="$middle/children[properties[name='name'][value='intaketemp']]/properties[name='lastValue']" from="$dpmiddle/properties[name='lastValue']"/>
		<docking to="$bottom/children[properties[name='name'][value='intaketemp']]/properties[name='lastValue']" from="$dpbottom/properties[name='lastValue']"/>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">IPMI Rack Intake</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true" dimension="">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">42.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">24.0</property>
		
		
		
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true" dimension=""/>
		
		<property name="period" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		
		<property name="user" display="User" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="password" display="Password" type="java.lang.String" editable="true" displayed="true"></property>
		
		<!--Top server property -->
		<property name="topaddress" display="Top Server: Address" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="topsensorid" display="Top Server: ID of Sensor" type="java.lang.Integer" editable="true" displayed="true"></property>
		
		<!--Middle server property -->
		<property name="middleaddress" display="Middle Server: Address" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="middlesensorid" display="Middle Server: ID of Sensor" type="java.lang.Integer" editable="true" displayed="true"></property>
		
		<!--Bottom server property -->
		<property name="bottomaddress" display="Bottom Server: Address" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="bottomsensorid" display="Bottom Server: ID of Sensor" type="java.lang.Integer" editable="true" displayed="true"></property>
		
		
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">IPMI Rack Intake</property>
		
		<producer id="topinlet" datatype="temperature" name="Top Inlet Temp" object="$rack/properties[name=&quot;cTop&quot;]/referrent"/>
		<producer id="midinlet" datatype="temperature_mid" name="Middle Inlet Temp" object="$rack/properties[name=&quot;cMid&quot;]/referrent"/>
		<producer id="botinlet" datatype="temperature_bot" name="Bottom Inlet Temp" object="$rack/properties[name=&quot;cBot&quot;]/referrent"/>
		
		<consumer id="reftemp" datatype="reftemp" name="Rack Ref Temp" property="$rack/properties[name=&quot;ref&quot;]" required="false"/>
		
		<varbinding vars="name">
			<property>$rack/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="name">
			<property>$top/properties[name="name"]</property>
			<value>name+': Top'</value>
		</varbinding>
		<varbinding vars="name">
			<property>$middle/properties[name="name"]</property>
			<value>name+': Middle'</value>
		</varbinding>
		<varbinding vars="name">
			<property>$bottom/properties[name="name"]</property>
			<value>name+': Bottom'</value>
		</varbinding>
		
		<varbinding vars="user">
			<property>$top/properties[name="user"]</property>
			<property>$middle/properties[name="user"]</property>
			<property>$bottom/properties[name="user"]</property>
			<value>user</value>
		</varbinding>
		<varbinding vars="password">
			<property>$top/properties[name="password"]</property>
			<property>$middle/properties[name="password"]</property>
			<property>$bottom/properties[name="password"]</property>
			<value>password</value>
		</varbinding>
		
		<!--Set polling interval-->
		<varbinding vars="period">
			<property>$top/children[properties[name="name"][value="intaketemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<property>$middle/children[properties[name="name"][value="intaketemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<property>$bottom/children[properties[name="name"][value="intaketemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<value>60000*period</value>
		</varbinding>
		
		<!-- Var binding Top Server -->
		
		<varbinding vars="topaddress">
			<property>$top/properties[name="address"]</property>
			<value>topaddress</value>
		</varbinding>
		<varbinding vars="topsensorid">
			<property>$top/children[properties[name="name"][value="intaketemp"]]/properties[name="sensorid"]</property>
			<value>topsensorid</value>
		</varbinding>
		
		<!-- Var binding Middle Server -->
		<varbinding vars="middleaddress">
			<property>$middle/properties[name="address"]</property>
			<value>middleaddress</value>
		</varbinding>
		<varbinding vars="middlesensorid">
			<property>$middle/children[properties[name="name"][value="intaketemp"]]/properties[name="sensorid"]</property>
			<value>middlesensorid</value>
		</varbinding>
		
		<!-- Var binding Bottom Server -->
		<varbinding vars="bottomaddress">
			<property>$bottom/properties[name="address"]</property>
			<value>bottomaddress</value>
		</varbinding>
		<varbinding vars="bottomsensorid">
			<property>$bottom/children[properties[name="name"][value="intaketemp"]]/properties[name="sensorid"]</property>
			<value>bottomsensorid</value>
		</varbinding>
		
		
		<!-- Rack varbinding -->
		<varbinding vars="x">
			<property>$rack/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		
		<varbinding vars="y">
			<property>$rack/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		
		<varbinding vars="rotation">
			<property>$rack/properties[name="rotation"]</property>
			<value>rotation</value>
		</varbinding>
		<varbinding vars="width">
			<property>$rack/properties[name="width"]</property>
			<value>width</value>
		</varbinding>
		<varbinding vars="depth">
			<property>$rack/properties[name="depth"]</property>
			<value>depth</value>
		</varbinding>
		<varbinding vars="location">
			<property>$rack/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		
		<!-- Min max-->
		<varbinding vars="min_allow_t">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMin"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMin"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMax"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMax"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMin"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMin"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMax"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMax"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		
		<!-- Rotatable-->
		<varbinding vars="x,depth,rotation">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="x"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="x"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<property>$dptop/properties[name="x"]</property>
			<property>$dpmiddle/properties[name="x"]</property>
			<property>$dpbottom/properties[name="x"]</property>
			<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		
		<varbinding vars="y,depth,rotation">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="y"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="y"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="y"]</property>
			<property>$dptop/properties[name="y"]</property>
			<property>$dpmiddle/properties[name="y"]</property>
			<property>$dpbottom/properties[name="y"]</property>
			<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		
		<!-- Docking -->
		<docking from="$rack/properties[name=&quot;cTop&quot;]" to="$top/children[properties[name=&quot;name&quot;][value=&quot;intaketemp&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cMid&quot;]" to="$middle/children[properties[name=&quot;name&quot;][value=&quot;intaketemp&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cBot&quot;]" to="$bottom/children[properties[name=&quot;name&quot;][value=&quot;intaketemp&quot;]]"/>

	</component>
	
	
	
	<component type="ipmirack-exhaust" classes="placeable,rotatable" grouppath="Environmentals;Wired;IPMI"  filters="Environmentals">
		<display>
			<description>IPMI Rack Intake and Exhaust</description>
			<name>IPMI Rack Intake and Exhaust</name>
			<shape>rack-ipmi</shape>
		</display>

		<object type="ipmiserver" as="top">
			<dlid>DLID:-1</dlid>
			<property name="ipmisensors">
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>intaketemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="z"><value>131072</value></property>
					<property name="lastValue"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
				</object>
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>exhausttemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="z"><value>131072</value></property>
					<property name="lastValue"><value>0</value></property>
				</object>
			</property>
		</object>
		
		<object type="ipmiserver" as="middle">
			<dlid>DLID:-1</dlid>
			<property name="ipmisensors">
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>intaketemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="z"><value>16384</value></property>
					<property name="lastValue"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
				</object>
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>exhausttemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="z"><value>16384</value></property>
					<property name="lastValue"><value>0</value></property>
				</object>
			</property>
		</object>
		
		<object type="ipmiserver" as="bottom">
			<dlid>DLID:-1</dlid>
			<property name="ipmisensors">
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>intaketemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="z"><value>2048</value></property>
					<property name="lastValue"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
				</object>
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>exhausttemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="z"><value>2048</value></property>
					<property name="lastValue"><value>0</value></property>
				</object>
			</property>
		</object>
		
		<object type="rack" as="rack">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
		</object>
		
		<object type="displaypoint" as="dpintaketop">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>topintake</value></property>
			<property name="status"><value>1</value></property>
			<property name="dataclass"><value>200</value></property>
			<property name="z"><value>131072</value></property>
		</object>
		<object type="displaypoint" as="dpintakemiddle">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>middleintake</value></property>
			<property name="status"><value>1</value></property>
			<property name="dataclass"><value>200</value></property>
			<property name="z"><value>16384</value></property>
		</object>
		<object type="displaypoint" as="dpintakebottom">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>bottomintake</value></property>
			<property name="status"><value>1</value></property>
			<property name="dataclass"><value>200</value></property>
			<property name="z"><value>2048</value></property>
		</object>
		<object type="displaypoint" as="dpexhausttop">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>topexhaust</value></property>
			<property name="status"><value>1</value></property>
			<property name="dataclass"><value>200</value></property>
			<property name="z"><value>131072</value></property>
		</object>
		<object type="displaypoint" as="dpexhaustmiddle">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>middleexhaust</value></property>
			<property name="status"><value>1</value></property>
			<property name="dataclass"><value>200</value></property>
			<property name="z"><value>16384</value></property>
		</object>
		<object type="displaypoint" as="dpexhaustbottom">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>bottomexhaust</value></property>
			<property name="status"><value>1</value></property>
			<property name="dataclass"><value>200</value></property>
			<property name="z"><value>2048</value></property>
		</object>
		
		<docking to="$top/children[properties[name='name'][value='intaketemp']]/properties[name='lastValue']" from="$dpintaketop/properties[name='lastValue']"/>
		<docking to="$top/children[properties[name='name'][value='exhausttemp']]/properties[name='lastValue']" from="$dpexhausttop/properties[name='lastValue']"/>
		<docking to="$middle/children[properties[name='name'][value='intaketemp']]/properties[name='lastValue']" from="$dpintakemiddle/properties[name='lastValue']"/>
		<docking to="$middle/children[properties[name='name'][value='exhausttemp']]/properties[name='lastValue']" from="$dpexhaustmiddle/properties[name='lastValue']"/>
		<docking to="$bottom/children[properties[name='name'][value='intaketemp']]/properties[name='lastValue']" from="$dpintakebottom/properties[name='lastValue']"/>
		<docking to="$bottom/children[properties[name='name'][value='exhausttemp']]/properties[name='lastValue']" from="$dpexhaustbottom/properties[name='lastValue']"/>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">IPMI Rack Intake and Exhaust</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true" dimension="">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">42.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">24.0</property>
		
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true" dimension=""/>
		
		<property name="period" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		
		<property name="user" display="User" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="password" display="Password" type="java.lang.String" editable="true" displayed="true"></property>
		
		<!--Top server property -->
		<property name="topaddress" display="Top Server: Address" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="topinletsensorid" display="Top Server: ID of Intake Temp Sensor" type="java.lang.Integer" editable="true" displayed="true"></property>
		<property name="topoutletsensorid" display="Top Server: ID of Exhaust Temp Sensor" type="java.lang.Integer" editable="true" displayed="true"></property>
		
		<!--Middle server property -->
		<property name="middleaddress" display="Middle Server: Address" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="middleinletsensorid" display="Middle Server: ID of Intake Temp Sensor" type="java.lang.Integer" editable="true" displayed="true"></property>
		<property name="middleoutletsensorid" display="Middle Server: ID of Exhaust Temp Sensor" type="java.lang.Integer" editable="true" displayed="true"></property>
		
		<!--Bottom server property -->
		<property name="bottomaddress" display="Bottom Server: Address" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="bottominletsensorid" display="Bottom Server: ID of Intake Temp Sensor" type="java.lang.Integer" editable="true" displayed="true"></property>
		<property name="bottomoutletsensorid" display="Bottom Server: ID of Exhaust Temp Sensor" type="java.lang.Integer" editable="true" displayed="true"></property>
		
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">IPMI Rack Intake and Exhaust</property>
		
		<producer id="topinlet" datatype="temperature" name="Top Inlet Temp" object="$rack/properties[name=&quot;cTop&quot;]/referrent"/>
		<producer id="midinlet" datatype="temperature_mid" name="Middle Inlet Temp" object="$rack/properties[name=&quot;cMid&quot;]/referrent"/>
		<producer id="botinlet" datatype="temperature_bot" name="Bottom Inlet Temp" object="$rack/properties[name=&quot;cBot&quot;]/referrent"/>
		<consumer id="reftemp" datatype="reftemp" name="Rack Ref Temp" property="$rack/properties[name=&quot;ref&quot;]" required="false"/>
		
		
		<varbinding vars="name">
			<property>$rack/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="name">
			<property>$top/properties[name="name"]</property>
			<value>name+': Top'</value>
		</varbinding>
		<varbinding vars="name">
			<property>$middle/properties[name="name"]</property>
			<value>name+': Middle'</value>
		</varbinding>
		<varbinding vars="name">
			<property>$bottom/properties[name="name"]</property>
			<value>name+': Bottom'</value>
		</varbinding>
		
		<varbinding vars="user">
			<property>$top/properties[name="user"]</property>
			<property>$middle/properties[name="user"]</property>
			<property>$bottom/properties[name="user"]</property>
			<value>user</value>
		</varbinding>
		<varbinding vars="password">
			<property>$top/properties[name="password"]</property>
			<property>$middle/properties[name="password"]</property>
			<property>$bottom/properties[name="password"]</property>
			<value>password</value>
		</varbinding>
		
		<!--Set polling interval-->
		<varbinding vars="period">
			<property>$top/children[properties[name="name"][value="intaketemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<property>$top/children[properties[name="name"][value="exhausttemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<property>$middle/children[properties[name="name"][value="intaketemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<property>$middle/children[properties[name="name"][value="exhausttemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<property>$bottom/children[properties[name="name"][value="intaketemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<property>$bottom/children[properties[name="name"][value="exhausttemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<value>60000*period</value>
		</varbinding>
		
		<!-- Var binding Top Server -->
		<varbinding vars="topaddress">
			<property>$top/properties[name="address"]</property>
			<value>topaddress</value>
		</varbinding>
		<varbinding vars="topinletsensorid">
			<property>$top/children[properties[name="name"][value="intaketemp"]]/properties[name="sensorid"]</property>
			<value>topinletsensorid</value>
		</varbinding>
		<varbinding vars="topoutletsensorid">
			<property>$top/children[properties[name="name"][value="exhausttemp"]]/properties[name="sensorid"]</property>
			<value>topoutletsensorid</value>
		</varbinding>
		
		<!-- Var binding Middle Server -->
		<varbinding vars="middleaddress">
			<property>$middle/properties[name="address"]</property>
			<value>middleaddress</value>
		</varbinding>
		<varbinding vars="middleinletsensorid">
			<property>$middle/children[properties[name="name"][value="intaketemp"]]/properties[name="sensorid"]</property>
			<value>middleinletsensorid</value>
		</varbinding>
		<varbinding vars="middleoutletsensorid">
			<property>$middle/children[properties[name="name"][value="exhausttemp"]]/properties[name="sensorid"]</property>
			<value>middleoutletsensorid</value>
		</varbinding>
		
		<!-- Var binding Bottom Server -->
		<varbinding vars="bottomaddress">
			<property>$bottom/properties[name="address"]</property>
			<value>bottomaddress</value>
		</varbinding>
		<varbinding vars="bottominletsensorid">
			<property>$bottom/children[properties[name="name"][value="intaketemp"]]/properties[name="sensorid"]</property>
			<value>bottominletsensorid</value>
		</varbinding>
		<varbinding vars="bottomoutletsensorid">
			<property>$bottom/children[properties[name="name"][value="exhausttemp"]]/properties[name="sensorid"]</property>
			<value>bottomoutletsensorid</value>
		</varbinding> 

		<!--Rack varbinding-->
		<varbinding vars="x">
			<property>$rack/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$rack/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="rotation">
			<property>$rack/properties[name="rotation"]</property>
			<value>rotation</value>
		</varbinding>
		<varbinding vars="width">
			<property>$rack/properties[name="width"]</property>
			<value>width</value>
		</varbinding>
		<varbinding vars="depth">
			<property>$rack/properties[name="depth"]</property>
			<value>depth</value>
		</varbinding>
		<varbinding vars="location">
			<property>$rack/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		
		<!-- Min max-->
		<varbinding vars="min_allow_t">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMin"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMin"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMax"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMax"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMin"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMin"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMax"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMax"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		
		<!--Rotatable-->
		<varbinding vars="x,depth,rotation">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="x"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="x"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<property>$dpintaketop/properties[name="x"]</property>
			<property>$dpintakemiddle/properties[name="x"]</property>
			<property>$dpintakebottom/properties[name="x"]</property>
			<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="y"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="y"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="y"]</property>
			<property>$dpintaketop/properties[name="y"]</property>
			<property>$dpintakemiddle/properties[name="y"]</property>
			<property>$dpintakebottom/properties[name="y"]</property>
			<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		
		<varbinding vars="x,depth,rotation">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="exhausttemp"]]/properties[name="x"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="exhausttemp"]]/properties[name="x"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="exhausttemp"]]/properties[name="x"]</property>
			<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<property>$dpexhausttop/properties[name="x"]</property>
			<property>$dpexhaustmiddle/properties[name="x"]</property>
			<property>$dpexhaustbottom/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="exhausttemp"]]/properties[name="y"]</property>
			<property>$middle/properties[name="ipmisensors"]/children[properties[name="name"][value="exhausttemp"]]/properties[name="y"]</property>
			<property>$bottom/properties[name="ipmisensors"]/children[properties[name="name"][value="exhausttemp"]]/properties[name="y"]</property>
			<property>$dpexhausttop/properties[name="y"]</property>
			<property>$dpexhaustmiddle/properties[name="y"]</property>
			<property>$dpexhaustbottom/properties[name="y"]</property>
			<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>

		<docking from="$rack/properties[name=&quot;cTop&quot;]" to="$top/children[properties[name=&quot;name&quot;][value=&quot;intaketemp&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cMid&quot;]" to="$middle/children[properties[name=&quot;name&quot;][value=&quot;intaketemp&quot;]]"/>
		<docking from="$rack/properties[name=&quot;cBot&quot;]" to="$bottom/children[properties[name=&quot;name&quot;][value=&quot;intaketemp&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hTop&quot;]" to="$top/children[properties[name=&quot;name&quot;][value=&quot;exhausttemp&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hMid&quot;]" to="$middle/children[properties[name=&quot;name&quot;][value=&quot;exhausttemp&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hBot&quot;]" to="$bottom/children[properties[name=&quot;name&quot;][value=&quot;exhausttemp&quot;]]"/>
		
	</component>
	
	<component type="ipmi-temperature-sensor" classes="placeable" grouppath="Environmentals;Wired;IPMI"  filters="Environmentals">
		<display>
			<description>IPMI Temperature Sensor</description>
			<name>IPMI Temperature Sensor</name>
			<shape>temperature-sensor-ipmi</shape>
		</display>
		
		<object type="ipmiserver" as="server">
			<dlid>DLID:-1</dlid>
			<property name="ipmisensors">
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>tempsensor</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="lastValue"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
				</object>
			</property>
		</object>
		<object type="generictemperature" as="generictemperature">
				<dlid>DLID:-1</dlid>
				<property name="status"><value>1</value></property>
		</object>
		<object type="displaypoint" as="display">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>temp</value></property>
			<property name="status"><value>1</value></property>
			<property name="dataclass"><value>200</value></property>
		</object>
		
		<docking to="$server/children[properties[name='name'][value='tempsensor']]/properties[name='lastValue']" from="$display/properties[name='lastValue']"/>
		

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">IPMI Temperature Sensor</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>
		<!--Polling interval-->
		<property name="period" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		
		<property name="li_layer" display="LI Layer" type="java.lang.Integer" editable="true" displayed="true" valueChoices="None:0,Top:131072,Middle:16384,Bottom:2048">131072</property>
		
		
		<!--Top server property -->
		<property name="user" display="User" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="password" display="Password" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="address" display="Address" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="sensorid" display="ID of Sensor" type="java.lang.Integer" editable="true" displayed="true"></property>
		
		
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">IPMI Temperature Sensor</property>
		
		<producer datatype="temperature" id="topinlet" name="Top Inlet Temp" object="$server/children[properties[name=&quot;name&quot;][value=&quot;tempsensor&quot;]]" />
        <producer datatype="temperature_mid" id="midinlet" name="Middle Inlet Temp" object="$server/children[properties[name=&quot;name&quot;][value=&quot;tempsensor&quot;]]" />
        <producer datatype="temperature_bot" id="botinlet" name="Bottom Inlet Temp" object="$server/children[properties[name=&quot;name&quot;][value=&quot;tempsensor&quot;]]" />
		
		<varbinding vars="name">
			<property>$server/properties[name="name"]</property>
			<property>$generictemperature/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="user">
			<property>$server/properties[name="user"]</property>
			<value>user</value>
		</varbinding>
		<varbinding vars="password">
			<property>$server/properties[name="password"]</property>
			<value>password</value>
		</varbinding>
		<varbinding vars="address">
			<property>$server/properties[name="address"]</property>
			<value>address</value>
		</varbinding>
		<varbinding vars="sensorid">
			<property>$server/children[properties[name="name"][value="tempsensor"]]/properties[name="sensorid"]</property>
			<value>sensorid</value>
		</varbinding>
		<varbinding vars="x">
			<property>$server/children[properties[name="name"][value="tempsensor"]]/properties[name="x"]</property>
			<property>$generictemperature/properties[name="x"]</property>
			<property>$display/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$server/children[properties[name="name"][value="tempsensor"]]/properties[name="y"]</property>
			<property>$generictemperature/properties[name="y"]</property>
			<property>$display/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="location">
			<property>$generictemperature/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		
		
        <varbinding vars="li_layer">
            <property>$server/children[properties[name="name"][value="tempsensor"]]/properties[name="z"]</property>
			<property>$display/properties[name="z"]</property>
            <value>li_layer</value>
        </varbinding>
		
		<!-- Min max-->
		<varbinding vars="min_allow_t">
			<property>$server/properties[name="ipmisensors"]/children[properties[name="name"][value="tempsensor"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$server/properties[name="ipmisensors"]/children[properties[name="name"][value="tempsensor"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$server/properties[name="ipmisensors"]/children[properties[name="name"][value="tempsensor"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$server/properties[name="ipmisensors"]/children[properties[name="name"][value="tempsensor"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		
		<varbinding vars="period">
			<property>$server/children[properties[name="name"][value="tempsensor"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<value>60000*period</value>
		</varbinding>
		
		<docking from="$generictemperature/properties[name=&quot;sensor&quot;]" to="$server/children[properties[name=&quot;name&quot;][value=&quot;tempsensor&quot;]]"/>
		
		
	</component>
	
	<component type="ipmirack-top-monitoring" classes="placeable,rotatable" grouppath="Environmentals;Wired;IPMI"  filters="Environmentals">
		<display>
			<description>IPMI Rack Top Monitoring</description>
			<name>IPMI Rack Top Monitoring</name>
			<shape>rack-ipmi-top</shape>
		</display>

		<object type="ipmiserver" as="top">
			<dlid>DLID:-1</dlid>
			<property name="ipmisensors">
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>intaketemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="lastValue"><value>0</value></property>
					<property name="z"><value>131072</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
				</object>
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>exhausttemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="z"><value>131072</value></property>
					<property name="lastValue"><value>0</value></property>
				</object>
			</property>
		</object>
		
		<object type="rack" as="rack">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
		</object>
		
		<object type="displaypoint" as="dpintake">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>intaketemp</value></property>
			<property name="status"><value>1</value></property>
			<property name="dataclass"><value>200</value></property>
			<property name="z"><value>131072</value></property>
		</object>
		<object type="displaypoint" as="dpexhaust">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>exhausttemp</value></property>
			<property name="status"><value>1</value></property>
			<property name="dataclass"><value>200</value></property>
			<property name="z"><value>131072</value></property>
		</object>
		
		<docking to="$top/children[properties[name='name'][value='intaketemp']]/properties[name='lastValue']" from="$dpintake/properties[name='lastValue']"/>
		<docking to="$top/children[properties[name='name'][value='exhausttemp']]/properties[name='lastValue']" from="$dpexhaust/properties[name='lastValue']"/>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">IPMI Rack Top Monitoring</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true" dimension="">0</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="true" displayed="true" dimension="distance">42.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="true" displayed="true" dimension="distance">24.0</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true" dimension=""/>
		
		<!--Polling interval-->
	
		<property name="period" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
		
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		
		<property name="user" display="User" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="password" display="Password" type="java.lang.String" editable="true" displayed="true"></property>
		
		<!--server property -->

		<property name="address" display="Address" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="inletsensorid" display="ID of Intake Temp Sensor" type="java.lang.Integer" editable="true" displayed="true"></property>
		<property name="outletsensorid" display="ID of Exhaust Temp Sensor" type="java.lang.Integer" editable="true" displayed="true"></property>
		
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">IPMI Rack Top Monitoring</property>
		
		<producer id="topinlet" datatype="temperature" name="Top Inlet Temp" object="$rack/properties[name=&quot;cTop&quot;]/referrent"/>
		<consumer id="reftemp" datatype="reftemp" name="Rack Ref Temp" property="$rack/properties[name=&quot;ref&quot;]" required="false"/>
		
		<varbinding vars="name">
			<property>$rack/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="name">
			<property>$top/properties[name="name"]</property>
			<value>name+': Top'</value>
		</varbinding>
		
		<varbinding vars="user">
			<property>$top/properties[name="user"]</property>
			<value>user</value>
		</varbinding>
		<varbinding vars="password">
			<property>$top/properties[name="password"]</property>
			<value>password</value>
		</varbinding>
		
		<!-- Set polling-->
		<varbinding vars="period">
			<property>$top/children[properties[name="name"][value="intaketemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<property>$top/children[properties[name="name"][value="exhausttemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<value>60000*period</value>
		</varbinding>		
		<!-- Var binding Top Server -->
		<varbinding vars="address">
			<property>$top/properties[name="address"]</property>
			<value>address</value>
		</varbinding>
		<varbinding vars="inletsensorid">
			<property>$top/children[properties[name="name"][value="intaketemp"]]/properties[name="sensorid"]</property>
			<value>inletsensorid</value>
		</varbinding>
		<varbinding vars="outletsensorid">
			<property>$top/children[properties[name="name"][value="exhausttemp"]]/properties[name="sensorid"]</property>
			<value>outletsensorid</value>
		</varbinding>		
		
		<!-- Rack varbinding -->
		<varbinding vars="x">
			<property>$rack/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		
		<varbinding vars="y">
			<property>$rack/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		
		<varbinding vars="rotation">
			<property>$rack/properties[name="rotation"]</property>
			<value>rotation</value>
		</varbinding>
		<varbinding vars="width">
			<property>$rack/properties[name="width"]</property>
			<value>width</value>
		</varbinding>
		<varbinding vars="depth">
			<property>$rack/properties[name="depth"]</property>
			<value>depth</value>
		</varbinding>
		<varbinding vars="location">
			<property>$rack/properties[name="location"]</property>
			<value>location</value>
		</varbinding>
		
		<!-- Min max-->
		<varbinding vars="min_allow_t">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		
		<!-- Rotatable-->
		<varbinding vars="x,depth,rotation">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<property>$dpintake/properties[name="x"]</property>
			<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="intaketemp"]]/properties[name="y"]</property>
			<property>$dpintake/properties[name="y"]</property>
			<value>y + (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="exhausttemp"]]/properties[name="x"]</property>
			<value>x - (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,depth,rotation">
			<property>$dpexhaust/properties[name="x"]</property>
			<value>x + (depth/2 * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,depth,rotation">
			<property>$top/properties[name="ipmisensors"]/children[properties[name="name"][value="exhausttemp"]]/properties[name="y"]</property>
			<property>$dpexhaust/properties[name="y"]</property>
			<value>y - (depth/2 * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		
		<!-- Docking -->
		<docking from="$rack/properties[name=&quot;cTop&quot;]" to="$top/children[properties[name=&quot;name&quot;][value=&quot;intaketemp&quot;]]"/>
		<docking from="$rack/properties[name=&quot;hTop&quot;]" to="$top/children[properties[name=&quot;name&quot;][value=&quot;exhausttemp&quot;]]"/>
	
	</component>
	
	
	
	
	<!--The next components are reserved for future development-->
	
	
	<!--<component type="ipmi-power-sensor" classes="placeable" grouppath="Environmentals;Wired;Racks"  filters="Environmentals">
		<display>
			<description>IPMI Power Sensor</description>
			<name>IPMI Power Sensor</name>
			<shape>ipmi-power-sensor</shape>
		</display>
		
		<object type="ipmiserver" as="server">
			<dlid>DLID:-1</dlid>
			<property name="ipmisensors">
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>power</value></property>
					<property name="lastValue"><value>0</value></property>
				</object>
			</property>
		</object>
		
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">IPMI Power Sensor</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>-->
		
		<!--Polling interval-->
		<!--<property name="period" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>-->
		
		<!--Top server property -->
		<!--<property name="privilegelevel" display="Privilege Level" type="java.lang.String" editable="true" displayed="true" valueChoices="Administrator:Administrator,Operator:Operator,User:User">Administrator</property>
		<property name="user" display="User" type="java.lang.String" editable="true" displayed="true">admin</property>
		<property name="password" display="Password" type="java.lang.String" editable="true" displayed="true">admin</property>
		<property name="address" display="Address" type="java.lang.String" editable="true" displayed="true">172.30.201.204</property>
		<property name="port" display="Port" type="java.lang.Integer" editable="false" displayed="true">623</property>
		<property name="sensorid" display="ID of Sensor" type="java.lang.Integer" editable="true" displayed="true">640</property>
		
		
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">IPMI Power Sensor</property>
		
		<producer datatype="power" id="pue" name="Power" object="$server/children[properties[name=&quot;name&quot;][value=&quot;power&quot;]]" />
		
		<varbinding vars="name">
			<property>$server/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="privilegelevel">
			<property>$server/properties[name="privilegelevel"]</property>
			<value>privilegelevel</value>
		</varbinding>
		<varbinding vars="user">
			<property>$server/properties[name="user"]</property>
			<value>user</value>
		</varbinding>
		<varbinding vars="password">
			<property>$server/properties[name="password"]</property>
			<value>password</value>
		</varbinding>
		<varbinding vars="address">
			<property>$server/properties[name="address"]</property>
			<value>address</value>
		</varbinding>
		<varbinding vars="port">
			<property>$server/properties[name="port"]</property>
			<value>port</value>
		</varbinding>
		<varbinding vars="sensorid">
			<property>$server/children[properties[name="name"][value="power"]]/properties[name="sensorid"]</property>
			<value>sensorid</value>
		</varbinding>
		<varbinding vars="x">
			<property>$server/children[properties[name="name"][value="power"]]/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$server/children[properties[name="name"][value="power"]]/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		
		<varbinding vars="period">
			<property>$server/children[properties[name="name"][value="power"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<value>60000*period</value>
		</varbinding>
			
	</component>-->
	
	<!--
	<component type="ipmi-server-monitoring" classes="placeable" grouppath="Environmentals;Wired;Racks"  filters="Environmentals">
		<display>
			<description>IPMI Server Monitoring</description>
			<name>IPMI Server Monitoring</name>
			<shape>ywing</shape>
		</display>
		
		<object type="ipmiserver" as="server">
			<dlid>DLID:-1</dlid>
			<property name="ipmisensors">
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>inlettemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="lastValue"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
				</object>
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>outlettemp</value></property>
					<property name="dataclass"><value>200</value></property>
					<property name="lastValue"><value>0</value></property>
					<property name="min"><value>40.0</value></property>
					<property name="max"><value>100.0</value></property>
				</object>
				<object type="ipmisensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>power</value></property>
					<property name="lastValue"><value>0</value></property>
				</object>
			</property>
		</object>
		
		

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">IPMI Server Monitoring</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>-->
		<!--Polling interval-->
		<!--
		<property name="period" display="Sampling Interval" type="java.lang.String" editable="true" displayed="true">5</property>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		
		<property name="li_layer" display="LI Layer" type="java.lang.Integer" editable="true" displayed="true" valueChoices="None:0,Top:131072,Middle:16384,Bottom:2048">131072</property>-->
		
		
		<!--Top server property -->
		<!--
		<property name="privilegelevel" display="Privilege Level" type="java.lang.String" editable="true" displayed="true" valueChoices="Administrator:Administrator,Operator:Operator,User:User">Administrator</property>
		<property name="user" display="User" type="java.lang.String" editable="true" displayed="true">admin</property>
		<property name="password" display="Password" type="java.lang.String" editable="true" displayed="true">admin</property>
		<property name="address" display="Address" type="java.lang.String" editable="true" displayed="true">172.30.201.204</property>
		<property name="port" display="Port" type="java.lang.Integer" editable="false" displayed="true">623</property>
		<property name="inletsensorid" display="ID of Inlet Temp Sensor" type="java.lang.Integer" editable="true" displayed="true">1280</property>
		<property name="outletsensorid" display="ID of Outlet Temp Sensor" type="java.lang.Integer" editable="true" displayed="true">1280</property>
		<property name="powersensorid" display="ID of Power Sensor" type="java.lang.Integer" editable="true" displayed="true">1280</property>
		
		
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">IPMI Temperature Sensor</property>-->
		
		<!--Conversion from celsius to fahrenheit-->
		<!--
		<property name="valueconv" type="java.lang.String" editable="false" displayed="false">return (Double) (value * (9/5) + 32)</property>
		

		<producer datatype="temperature" id="topinlet" name="Top Inlet Temp" object="$server/children[properties[name=&quot;name&quot;][value=&quot;inlettemp&quot;]]" />
        <producer datatype="temperature_mid" id="midinlet" name="Middle Inlet Temp" object="$server/children[properties[name=&quot;name&quot;][value=&quot;inlettemp&quot;]]" />
        <producer datatype="temperature_bot" id="botinlet" name="Bottom Inlet Temp" object="$server/children[properties[name=&quot;name&quot;][value=&quot;inlettemp&quot;]]" />
		
		<producer datatype="power" id="pue" name="Power" object="$server/children[properties[name=&quot;name&quot;][value=&quot;power&quot;]]" />
		
		<varbinding vars="name">
			<property>$server/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="privilegelevel">
			<property>$server/properties[name="privilegelevel"]</property>
			<value>privilegelevel</value>
		</varbinding>
		<varbinding vars="user">
			<property>$server/properties[name="user"]</property>
			<value>user</value>
		</varbinding>
		<varbinding vars="password">
			<property>$server/properties[name="password"]</property>
			<value>password</value>
		</varbinding>
		<varbinding vars="address">
			<property>$server/properties[name="address"]</property>
			<value>address</value>
		</varbinding>
		<varbinding vars="port">
			<property>$server/properties[name="port"]</property>
			<value>port</value>
		</varbinding>
		
		<varbinding vars="inletsensorid">
			<property>$server/children[properties[name="name"][value="inlettemp"]]/properties[name="sensorid"]</property>
			<value>inletsensorid</value>
		</varbinding>
		<varbinding vars="outletsensorid">
			<property>$server/children[properties[name="name"][value="outlettemp"]]/properties[name="sensorid"]</property>
			<value>outletsensorid</value>
		</varbinding>
		<varbinding vars="powersensorid">
			<property>$server/children[properties[name="name"][value="power"]]/properties[name="sensorid"]</property>
			<value>powersensorid</value>
		</varbinding>
		
		<varbinding vars="x">
			<property>$server/children[properties[name="name"][value="inlettemp"]]/properties[name="x"]</property>
			<property>$server/children[properties[name="name"][value="outlettemp"]]/properties[name="x"]</property>
			<property>$server/children[properties[name="name"][value="power"]]/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$server/children[properties[name="name"][value="inlettemp"]]/properties[name="y"]</property>
			<property>$server/children[properties[name="name"][value="outlettemp"]]/properties[name="y"]</property>
			<property>$server/children[properties[name="name"][value="power"]]/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		
        <varbinding vars="li_layer">
            <property>$server/children[properties[name="name"][value="inlettemp"]]/properties[name="z"]</property>
			<property>$server/children[properties[name="name"][value="outlettemp"]]/properties[name="z"]</property>
            <value>li_layer</value>
        </varbinding>-->
		
		<!-- Min max-->
		<!--
		<varbinding vars="min_allow_t">
			<property>$server/properties[name="ipmisensors"]/children[properties[name="name"][value="inlettemp"]]/properties[name="aMin"]</property>
			<property>$server/properties[name="ipmisensors"]/children[properties[name="name"][value="outlettemp"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$server/properties[name="ipmisensors"]/children[properties[name="name"][value="inlettemp"]]/properties[name="aMax"]</property>
			<property>$server/properties[name="ipmisensors"]/children[properties[name="name"][value="outlettemp"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$server/properties[name="ipmisensors"]/children[properties[name="name"][value="inlettemp"]]/properties[name="rMin"]</property>
			<property>$server/properties[name="ipmisensors"]/children[properties[name="name"][value="outlettemp"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$server/properties[name="ipmisensors"]/children[properties[name="name"][value="inlettemp"]]/properties[name="rMax"]</property>
			<property>$server/properties[name="ipmisensors"]/children[properties[name="name"][value="outlettemp"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>-->
		
		<!-- Set conversion from celsius to fahrenheit-->
		<!--
		<varbinding vars="valueconv">
			<property>$server/children[properties[name="name"][value="inlettemp"]]/properties[name="lastValue"]/tags[tagName='elConvGet']</property>
			<property>$server/children[properties[name="name"][value="outlettemp"]]/properties[name="lastValue"]/tags[tagName='elConvGet']</property>
			<value>valueconv</value>
		</varbinding>-->
		
		<!-- Set polling interval-->
		
		<!--<varbinding vars="period">
			<property>$server/children[properties[name="name"][value="inlettemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<property>$server/children[properties[name="name"][value="outlettemp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<value>60000*period</value>
		</varbinding>
		<varbinding vars="powersensorid,period">
			<property>$server/children[properties[name="name"][value="power"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			<value>if (powersensorid==0){0}else{period* 60000}</value>
		</varbinding>
		
		
	</component>-->
	
</componentlib>