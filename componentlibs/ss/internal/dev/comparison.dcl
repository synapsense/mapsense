<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
		name='Calculation Comparisons'
		version="&componentlib.version;"
		cversion="&componentlib.cversion;"
		oversion="&componentlib.oversion;"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
		>


	<component type='pue_two_input_switch' classes='placeable' grouppath="Calculations;Equipment Status Calculations" filters="Calculations">
		<display>
			<description>Produces one of two values based on a Status input.  Has an Equipment Status Consumer, two numeric Consumers, and a single numeric Producer.  If the input Status is ON, the Producer supplies the value of the first numeric Consumer.  If the input Status is OFF, the Producer supplies the value of the second numeric Consumer.  This Component goes in a Calculation Group and does not require a Data Source.</description>
			<name>Two Input Switch</name>
			<shape>pue_diamond_switch</shape>
		</display>
		<property name='name' display='Name' type='java.lang.String' editable='true' displayed='true' dimension=''>Two Input Switch</property>
		<property name='x' display='x' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
		<property name='y' display='y' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
		<property name='configuration' display='Configuration' type='java.lang.String' editable='false' displayed='true' dimension=''>Two Input Switch</property>
		<property name='notes' display='Notes' type='DeploymentLab.MultiLineContents' editable='true' displayed='true' dimension=''></property>

		<varbinding vars='name'>
			<property>$switch/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars='notes'>
			<property>$switch/properties[name="notes"]</property>
			<value>notes</value>
		</varbinding>
		<producer id='output' datatype='power' name='Output Value' object='$switch' />
		<consumer id='input' datatype='status' name='Status Input' collection='false' property='$switch/properties[name="control"]' required='true' />

		<consumer id='value' datatype='power' name='First Input' collection='false' property='$switch/properties[name="value"]' required='true' />
		<consumer id='default' datatype='power' name='Second Input' collection='false' property='$switch/properties[name="default"]' required='true' />

		<object as='switch' type='switch'>
			<dlid>DLID:-1</dlid>
		</object>
	</component>


	<component type='pue_two_input_compare' classes='placeable' grouppath="Calculations;Equipment Status Calculations" filters="Calculations">
		<display>
			<description>Produces one of two values based on a Status input.  Has an Equipment Status Consumer, two numeric Consumers, and a single numeric Producer.  If the input Status is ON, the Producer supplies the value of the first numeric Consumer.  If the input Status is OFF, the Producer supplies the value of the second numeric Consumer.  This Component goes in a Calculation Group and does not require a Data Source.</description>
			<name>Two Input Compare Switch</name>
			<shape>pue_diamond_switch</shape>
		</display>
		<property name='name' display='Name' type='java.lang.String' editable='true' displayed='true' dimension=''>Two Input Compare Switch</property>
		<property name='x' display='x' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
		<property name='y' display='y' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
		<property name='configuration' display='Configuration' type='java.lang.String' editable='false' displayed='true' dimension=''>Two Input Compare Switch</property>
		<property name='notes' display='Notes' type='DeploymentLab.MultiLineContents' editable='true' displayed='true' dimension=''></property>

		<property name="opM" display="Main Operation" type="java.lang.String" editable="true" displayed="true"
				  valueChoices="A &#x003c; B:lt,A &gt; B:gt,A = B:eq,A &#x2260; B:neq">lt
		</property>

		<varbinding vars='name'>
			<property>$switch/properties[name="name"]</property>
			<property>$math/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars='notes'>
			<property>$switch/properties[name="notes"]</property>
			<property>$math/properties[name="notes"]</property>
			<value>notes</value>
		</varbinding>
		<varbinding vars="opM">
			<property>$math/properties[name="opM"]</property>
			<value>opM</value>
		</varbinding>

		<docking from='$switch/properties[name="control"]' to="$math" />

		<producer id='output' datatype='power' name='Output Value' object='$switch' />

		<!--consumer id='input' datatype='status' name='Status Input' collection='false' property='$switch/properties[name="control"]' required='true' /-->
		<!--consumer id='value' datatype='power' name='First Input' collection='false' property='$switch/properties[name="value"]' required='true' />
		<consumer id='default' datatype='power' name='Second Input' collection='false' property='$switch/properties[name="default"]' required='true' /-->

		<consumer id='value' datatype='power' name='First Input' collection='false' property='$metricA/properties[name="input"]' required='true' />
		<consumer id='default' datatype='power' name='Second Input' collection='false' property='$metricB/properties[name="input"]' required='true' />

		<docking from='$switch/properties[name="value"]' to='$metricA' />
		<docking from='$switch/properties[name="value2"]' to='$metricB' />

		<docking from='$math/properties[name="inputA"]' to='$metricA' />
		<docking from='$math/properties[name="inputB"]' to='$metricB' />


		<object as='switch' type='switch'>
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
		</object>

		<object type="math_operation" as="math">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
			<!--property name="opM"><value>or</value></property-->
			<property name="opA"><value>solo</value></property>
			<property name="opB"><value>solo</value></property>
		</object>

		<object type="metric" as="metricA">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>First Value Landing Pad</value></property>
			<property name="status"><value>1</value></property>
		</object>

		<object type="metric" as="metricB">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>Second Value Landing Pad</value></property>
			<property name="status"><value>1</value></property>
		</object>

	</component>


</componentlib>