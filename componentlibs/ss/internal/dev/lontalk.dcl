<?xml version="1.0" encoding="UTF-8"?>
<componentlib
	name="lontalk integration"
	version="1.0"
	cversion="1.0"
	oversion="2.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
	>


	 <component type="lonrouter" classes="placeable,network" grouppath="Calculations;Wired Integration"  filters="Calculations">
		 <display>
			 <description>Represents a LonTalk/IP Router.  This Component serves as the Data Source for any LonTalk Devices that use this Router to communicate to the network.  This Component goes in a Calculation Group and requires a LonTalk Network Data Source.</description>
			 <name>LonTalk/IP Router</name>
			 <shape>lonrouter</shape>
		 </display>

		 <object type="lonrouter" as="lontalkrouter">
			 <dlid>DLID:-1</dlid>
		 </object>

		 <property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">LonTalk/IP Router</property>
		 <property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		 <property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		 <property name="configuration" display="Configuration" type="java.lang.String" editable="true" displayed="true">LonTalk/IP Router</property>

		 <property name="address" display="Address" type="java.lang.String" editable="true" displayed="true"></property>
		 <property name="port" display="Port" type="java.lang.Integer" editable="true" displayed="true">1628</property>
		 <property name="retries" display="Retries" type="java.lang.Integer" editable="true" displayed="true">0</property>
		 <property name="timeout" display="Timeout" type="java.lang.Integer" editable="true" displayed="true">0</property>


		 <varbinding vars="name">
			 <property>$lontalkrouter/properties[name="name"]</property>
			 <value>name</value>
		 </varbinding>
		 <varbinding vars="address">
			 <property>$lontalkrouter/properties[name="address"]</property>
			 <value>address</value>
		 </varbinding>
		 <varbinding vars="port">
			 <property>$lontalkrouter/properties[name="port"]</property>
			 <value>port</value>
		 </varbinding>
		 <varbinding vars="retries">
			 <property>$lontalkrouter/properties[name="retries"]</property>
			 <value>retries</value>
		 </varbinding>
		 <varbinding vars="timeout">
			 <property>$lontalkrouter/properties[name="timeout"]</property>
			 <value>timeout</value>
		 </varbinding>
	 </component>



	<component type="lonnode" classes="placeable,dynamicchildplaceable" grouppath="Calculations;Wired Integration" filters="Calculations">
		<display>
			<description>Represents a LonTalk Device that can be queried for data.  Individual LonTalk Properties must be attached to read specific SNVTs.  This Component goes in a Calculation Group and requires a LonTalk/IP Router Data Source.</description>
			<name>LonTalk Device</name>
			<shape>lontalk_device</shape>
		</display>

		<object type="lonnode" as="lonnode">
			<dlid>DLID:-1</dlid>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">LonTalk Device</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">LonTalk Device</property>
		<property name="subnet" display="Subnet" type="java.lang.String" editable="true" displayed="true">0</property>
		<property name="node" display="Node" type="java.lang.Integer" editable="true" displayed="true">0</property>

		<varbinding vars="name">
			<property>$lonnode/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="subnet">
			<property>$lonnode/properties[name="subnet"]</property>
			<value>subnet</value>
		</varbinding>
		<varbinding vars="node">
			<property>$lonnode/properties[name="node"]</property>
			<value>node</value>
		</varbinding>
	</component>


	<component type="lonproperty" classes="placeable,dynamicchild" grouppath="Calculations;Wired Integration" filters="Calculations">
		<display>
			<description>Represents a LonTalk Property, a single value from a SNVT.  These components are added to LonTalk Devices as their children to reflect properties on that device.  This Component makes a single data point available to a Calculation Graph.  This Component goes in a Calculation Group and requires a LonTalk/IP Router Data Source.</description>
			<name>LonTalk Property</name>
			<shape>snvt</shape>
		</display>

		<object type="lonproperty" as="lonproperty">
			<dlid>DLID:-1</dlid>
		</object>

		<producer id="lonvalue" name="Value" datatype="power" object='$lonproperty'/>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">LonTalk Property</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">LonTalk Property</property>

		<property name="type" display="Type" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="nvindex" display="NVIndex" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="selector" display="Selector" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="fieldname" display="Fieldname" type="java.lang.String" editable="true" displayed="true"></property>

		<varbinding vars="name">
			<property>$lonproperty/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="type">
			<property>$lonproperty/properties[name="type"]</property>
			<value>type</value>
		</varbinding>
		<varbinding vars="nvindex">
			<property>$lonproperty/properties[name="nvindex"]</property>
			<value>nvindex</value>
		</varbinding>
		<varbinding vars="selector">
			<property>$lonproperty/properties[name="selector"]</property>
			<value>selector</value>
		</varbinding>
		<varbinding vars="fieldname">
			<property>$lonproperty/properties[name="fieldname"]</property>
			<value>fieldname</value>
		</varbinding>
	</component>

<!--
	 <component type="lontalk-node" classes="placeable,dynamicchildplaceable" grouppath="lontalk" filters="Calculations">
		 <display>
			 <description>Represents a LonTalk Device that can be queried for data to be returned to a calculation graph.  Individual LonTalk objects must be attached to read specific SNVT.  This Component goes in a Calculation Group and requires a LonTalk/IP Router Data Source.</description>
			 <name>LonTalk Device</name>
			 <shape>lontalk_device</shape>
		 </display>

		 <object type="lontalknode" as="lontalknode">
			 <dlid>DLID:-1</dlid>
		 </object>

		 <property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">LonTalk Device</property>
		 <property name="address" display="Address" type="java.lang.String" editable="true" displayed="true"></property>
		 <property name="port" display="Port" type="java.lang.Integer" editable="true" displayed="true"></property>
		 <property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		 <property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		 <property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">LonTalk Device</property>

		 <varbinding vars="name">
			 <property>$lontalknode/properties[name="name"]</property>
			 <value>name</value>
		 </varbinding>

		 <varbinding vars="address">
			 <property>$lontalknode/properties[name="address"]</property>
			 <value>address</value>
		 </varbinding>

		 <varbinding vars="port">
			 <property>$lontalknode/properties[name="port"]</property>
			 <value>port</value>
		 </varbinding>

	 </component>



	 <component type="lontalk-snvt-amp" classes="placeable,dynamicchild" grouppath="Calculations;Wired Integration" filters="Calculations">
		 <display>
			 <description>Represents a electric current LonTalk network variable that can be queried for data to be returned to a calculation graph.  Must be attached to a LonTalk Device.  This Component goes in a Calculation Group and requires a LonTalk Network Data Source.</description>
			 <name>SNVT_amp</name>
			 <shape>snvt_amp</shape>
		 </display>

		 <object type="snvt" as="snvt">
			 <dlid>DLID:-1</dlid>
			 <property name="fields">
				 <object type="snvtfield">
					 <dlid>DLID:-1</dlid>
					 <property name="name"><value>amp</value></property>
					 <property name="scaling"><value>A</value></property>
					 <property name="lastValue"><value>-5000.0</value></property>
				 </object>
			 </property>
		 </object>

		 <property name="name" display="name" type="java.lang.String" editable="true" displayed="true">SNVT_amp</property>
		 <property name="index" display="Index" type="java.lang.Integer" editable="true" displayed="true"></property>
		 <property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		 <property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		 <property name="pullPeriod" display="Sampling Interval (minutes)" type="java.lang.Integer" editable="true" displayed="true">5</property>

		 <producer id="snvt_amp" name="SNVT_amp" datatype="power" object='$snvt/properties[name="fields"]/children[properties[name="name"][value="amp"]]'/>

		 <varbinding vars="index">
			<property>$snvt/properties[name="index"]</property>
			<value>index</value>
		 </varbinding>

		 <varbinding vars="name">
			<property>$snvt/properties[name="name"]</property>
			<value>name</value>
		 </varbinding>

		 <varbinding vars="pullPeriod">
			 <property>$snvt/properties[name="fields"]/children[properties[name="name"][value="amp"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			 <value>pullPeriod * 60000.0</value>
		 </varbinding>
	 </component>

	 <component type="lontalk-snvt-hvac-status" classes="placeable,dynamicchild" grouppath="Calculations;Wired Integration" filters="Calculations">
		 <display>
			 <description>Represents heating, ventilation and air-conditioning LonTalk network variable that can be queried for data to be returned to a calculation graph.  Must be attached to a LonTalk Device.  This Component goes in a Calculation Group and requires a LonTalk Network Data Source.</description>
			 <name>SNVT_hvac</name>
			 <shape>snvt_hvac</shape>
		 </display>

		 <object type="snvt" as="snvt">
			 <dlid>DLID:-1</dlid>
			 <property name="fields">
				 <object type="snvtfield">
					 <dlid>DLID:-1</dlid>
					 <property name="name"><value>heat_output_primary</value></property>
					 <property name="scaling"><value>A</value></property>
					 <property name="lastValue"><value>-5000.0</value></property>
				 </object>
				 <object type="snvtfield">
					 <dlid>DLID:-1</dlid>
					 <property name="name"><value>heat_output_secondary</value></property>
					 <property name="scaling"><value>A</value></property>
					 <property name="lastValue"><value>-5000.0</value></property>
				 </object>
				 <object type="snvtfield">
					 <dlid>DLID:-1</dlid>
					 <property name="name"><value>cool_output</value></property>
					 <property name="scaling"><value>A</value></property>
					 <property name="lastValue"><value>-5000.0</value></property>
				 </object>
				 <object type="snvtfield">
					 <dlid>DLID:-1</dlid>
					 <property name="name"><value>econ_output</value></property>
					 <property name="scaling"><value>A</value></property>
					 <property name="lastValue"><value>-5000.0</value></property>
				 </object>
				 <object type="snvtfield">
					 <dlid>DLID:-1</dlid>
					 <property name="name"><value>fan_output</value></property>
					 <property name="scaling"><value>A</value></property>
					 <property name="lastValue"><value>-5000.0</value></property>
				 </object>
				 <object type="snvtfield">
					 <dlid>DLID:-1</dlid>
					 <property name="name"><value>in_alarm</value></property>
					 <property name="scaling"><value>A</value></property>
					 <property name="lastValue"><value>-5000.0</value></property>
				 </object>
		 </property>
		 </object>

		 <property name="name" display="name" type="java.lang.String" editable="true" displayed="true">SNVT_hvac_status</property>
		 <property name="index" display="Index" type="java.lang.Integer" editable="true" displayed="true"></property>
		 <property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		 <property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		 <property name="pullPeriod" display="Sampling Interval (minutes)" type="java.lang.Integer" editable="true" displayed="true">5</property>

		 <producer id="heat_output_primary" name="Heat output primary" datatype="power" object='$snvt/properties[name="fields"]/children[properties[name="name"][value="heat_output_primary"]]'/>
		 <producer id="heat_output_secondary" name="Heat output secondary" datatype="power" object='$snvt/properties[name="fields"]/children[properties[name="name"][value="heat_output_secondary"]]'/>
		 <producer id="cool_output" name="Cool output" datatype="power" object='$snvt/properties[name="fields"]/children[properties[name="name"][value="cool_output"]]'/>
		 <producer id="econ_output" name="Econ output" datatype="power" object='$snvt/properties[name="fields"]/children[properties[name="name"][value="econ_output"]]'/>
		 <producer id="fan_output" name="Fan output" datatype="power" object='$snvt/properties[name="fields"]/children[properties[name="name"][value="fan_output"]]'/>
		 <producer id="in_alarm" name="In alarm" datatype="power" object='$snvt/properties[name="fields"]/children[properties[name="name"][value="in_alarm"]]'/>

		 <varbinding vars="index">
			<property>$snvt/properties[name="index"]</property>
			<value>index</value>
		 </varbinding>

		 <varbinding vars="name">
			<property>$snvt/properties[name="name"]</property>
			<value>name</value>
		 </varbinding>

		 <varbinding vars="pullPeriod">
			 <property>$snvt/properties[name="fields"]/children[properties[name="name"][value="heat_output_primary"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			 <property>$snvt/properties[name="fields"]/children[properties[name="name"][value="heat_output_secondary"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			 <property>$snvt/properties[name="fields"]/children[properties[name="name"][value="cool_output"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			 <property>$snvt/properties[name="fields"]/children[properties[name="name"][value="econ_output"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			 <property>$snvt/properties[name="fields"]/children[properties[name="name"][value="fan_output"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			 <property>$snvt/properties[name="fields"]/children[properties[name="name"][value="in_alarm"]]/properties[name="lastValue"]/tags[tagName='poll']</property>
			 <value>pullPeriod * 60000.0</value>
		 </varbinding>
	 </component>

 -->

</componentlib>