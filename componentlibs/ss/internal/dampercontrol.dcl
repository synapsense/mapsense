<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib name='Control Components' version="&componentlib.version;"
	cversion="&componentlib.cversion;"
	oversion="&componentlib.oversion;">

    <component type='control_rtu_manager_bacnet' classes='placeable,airflowcontrol,temperaturecontrol,manualexpressions,rtumanager' grouppath='Control;Damper Control' filters="Control">
        <display>
            <description>The exalted ruler of Roof Top Units (RTU) using BACnet.</description>
            <name>BACnet RTU Manager</name>
            <shape>rtu-manager_bacnet</shape>
        </display>

        <property name='name' display='Name' type='java.lang.String' editable='true' displayed='true' dimension=''>RTU Manager</property>
        <property name='x' display='x' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
        <property name='y' display='y' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
        <property name='configuration' display='Configuration' type='java.lang.String' editable='false' displayed='true' dimension=''>BACnet RTU Manager</property>
        <property name='notes' display='Notes' type='DeploymentLab.MultiLineContents' editable='true' displayed='true' dimension=''></property>

        <property name='ip' display='IP Address' type='java.lang.String' editable='true' displayed='true' dimension=''>0</property>
        <property name='network' display='BACnet network' type='java.lang.Integer' editable='true' displayed='true' dimension=''>0</property>
        <property name='device' display='BACnet device' type='java.lang.Integer' editable='true' displayed='true' dimension=''>0</property>

        <property name='pushTime' display='Device Write Timer (sec)' type='java.lang.Integer' editable='true' displayed='true' >60</property>
        <property name='pullTime' display='Device Read Timer (sec)' type='java.lang.Integer' editable='true' displayed='true' >30</property>

        <property name='ductPressureRead' display='Duct Pressure' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='ductPressureReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='ductPressureReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='ductPressureSetpointRead' display='Duct Pressure Setpoint' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='ductPressureSetpointReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='ductPressureSetpointReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='tempSetpointRead' display='Temp Setpoint' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='tempSetpointReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='tempSetpointReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='tempSetpointWrite' display='Write Temp Setpoint' type='DeploymentLab.ConfigBacnetWrite' editable='true' displayed='true' dimension=''></property>
        <property name='tempSetpointWriteString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>2;0;Analog Output;None;10;10;true;85;</property>
        <property name='tempSetpointWriteValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>0</property>

        <property name='commRead' display='Comm Read' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='commReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='commReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>


        <varbinding vars='name'>
            <property>$strategy/properties[name="name"]</property>
            <property>$strategy/properties[name="outputs"]/children[1]/properties[name="name"]</property>
            <value>name</value>
        </varbinding>

        <varbinding vars='x'>
            <property>$strategy/properties[name="x"]</property>
            <property>$strategy/properties[name="outputs"]/children[1]/properties[name="x"]</property>
            <value>x</value>
        </varbinding>

        <varbinding vars='y'>
            <property>$strategy/properties[name="y"]</property>
            <property>$strategy/properties[name="outputs"]/children[1]/properties[name="y"]</property>
            <value>y</value>
        </varbinding>

        <varbinding vars='notes'><property>$strategy/properties[name="outputs"]/children[1]/properties[name="notes"]</property><value>notes</value></varbinding>
        <varbinding vars='ip'> <property>$config/properties[name="ip"]</property> <value>ip</value> </varbinding>
        <varbinding vars='network'> <property>$config/properties[name="network"]</property> <value>network</value> </varbinding>
        <varbinding vars='device'> <property>$config/properties[name="devid"]</property> <value>device</value> </varbinding>

        <varbinding vars='pushTime'> <property>$strategy/properties[name="outputs"]/children[1]/properties[name="pushTime"]</property> <value>pushTime</value> </varbinding>
        <varbinding vars='pullTime'> <property>$strategy/properties[name="outputs"]/children[1]/properties[name="pullTime"]</property> <value>pullTime</value> </varbinding>

        <varbinding vars='ductPressureRead'> <property>$config/properties[name="ductPressureRead"]</property> <value>ductPressureRead</value> </varbinding>
        <varbinding vars='ductPressureSetpointRead'> <property>$config/properties[name="ductPressureSetpointRead"]</property> <value>ductPressureSetpointRead</value> </varbinding>
        <varbinding vars='tempSetpointRead'> <property>$config/properties[name="tempSetpointRead"]</property> <value>tempSetpointRead</value> </varbinding>
        <varbinding vars='tempSetpointWrite'> <property>$config/properties[name="tempSetpointWrite"]</property> <value>tempSetpointWrite</value> </varbinding>
        <varbinding vars='commRead'> <property>$config/properties[name="commRead"]</property> <value>commRead</value> </varbinding>

        <varbinding vars='ductPressureReadValid,ductPressureSetpointReadValid,tempSetpointReadValid,tempSetpointWriteValid,commReadValid'>
            <property>$config/properties[name="validated"]</property>
            <value>
                if ( ductPressureReadValid == '1' &amp;&amp; ductPressureSetpointReadValid == '1' &amp;&amp; tempSetpointReadValid == '1' &amp;&amp; tempSetpointWriteValid == '1' &amp;&amp; commReadValid == '1' ){
                    return 1;
                } else {
                    return 0;
                }
            </value>
        </varbinding>


        <conducer id="RTUMgrChildLink" name="RTU Manager and Child Strategy Link" required="true">
            <producer id="rtuManagerP" datatype="RTULink_Parent" name="RTU Manager Strategy Output" object="$strategy"/>
            <consumer id="childStrategies" datatype="RTULink_Child" name="Child Strategy Input" property="$strategy/properties[name='childStrategies']" required="false"/>
        </conducer>

        <conducer id="RTUMgrRtuLink" name="RTU Manager and RTU Link">
            <producer id="rtuManager" datatype="RTULink_Manager" name="RTU Manager Strategy Output" object="$strategy"/>
            <consumer id="rtus" datatype="RTULink_RTU" name="RTU Strategy Input" property="$strategy/properties[name='rtus']" required="false"/>
        </conducer>

        <docking from='$strategy/properties[name="outputs"]/children[1]/properties[name="config"]' to='$config'/>


        <object as="strategy" type="controlalg_rtumgr">
            <dlid>DLID:-1</dlid>
            <property name="resource"><value>temperature</value></property>
            <property name="manualOutput"><value>72.0</value></property>
            <property name="outputMin"><value>65.0</value></property>
            <property name="outputMax"><value>90.0</value></property>
            <property name="lowCutoff"><value>50.0</value></property>
            <property name="highCutoff"><value>80.0</value></property>
            <property name="adjustment"><value>1.0</value></property>
            <property name="invertError"><value>1</value></property>
            <property name="restPeriod"><value>600.0</value></property>
            <property name="output"><value>72.0</value></property>
            <property name="mode"><value>manual</value></property>
            <property name="outputs">
                <object as="controlout_rtumgr" type="controlout_rtumgr">
                    <dlid>DLID:-1</dlid>
                    <property name="resource"><value>temperature</value></property>
                    <property name="unitstatus"><value>text/online</value></property>
                    <property name="engaged"><value>0</value></property>
                </object>
            </property>
        </object>

        <object as="config" type="cfg_rtumgr_bacnet">
            <dlid>DLID:-1</dlid>
        </object>
    </component>


    <component type='control_rtu_manager_modbus' classes='placeable,airflowcontrol,temperaturecontrol,manualexpressions,rtumanager' grouppath='Control;Damper Control' filters="Control">
        <display>
            <description>The exalted ruler of Roof Top Units (RTU) using Modbus.</description>
            <name>Modbus RTU Manager</name>
            <shape>rtu-manager_modbus</shape>
        </display>

        <property name='name' display='Name' type='java.lang.String' editable='true' displayed='true' dimension=''>RTU Manager</property>
        <property name='x' display='x' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
        <property name='y' display='y' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
        <property name='configuration' display='Configuration' type='java.lang.String' editable='false' displayed='true' dimension=''>Modbus RTU Manager</property>
        <property name='notes' display='Notes' type='DeploymentLab.MultiLineContents' editable='true' displayed='true' dimension=''></property>

        <property name='ip' display='IP Address' type='java.lang.String' editable='true' displayed='true' dimension=''>0</property>
        <property name='devid' display='Device ID' type='java.lang.Integer' editable='true' displayed='true' dimension=''>0</property>

        <property name='pushTime' display='Device Write Timer (sec)' type='java.lang.Integer' editable='true' displayed='true' >60</property>
        <property name='pullTime' display='Device Read Timer (sec)' type='java.lang.Integer' editable='true' displayed='true' >30</property>

         <property name='ductPressureRead' display='Duct Pressure' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='ductPressureReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='ductPressureReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='ductPressureSetpointRead' display='Duct Pressure Setpoint' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='ductPressureSetpointReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='ductPressureSetpointReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='tempSetpointRead' display='Temp Setpoint' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='tempSetpointReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='tempSetpointReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='tempSetpointWrite' display='Write Temp Setpoint' type='DeploymentLab.ConfigModbusWrite' editable='true' displayed='true' dimension=''></property>
        <property name='tempSetpointWriteString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>2;40001;Int16;none;10;10;true</property>
        <property name='tempSetpointWriteValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>0</property>

        <property name='commRead' display='Comm Read' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='commReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='commReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>


        <varbinding vars='name'>
            <property>$strategy/properties[name="name"]</property>
            <property>$strategy/properties[name="outputs"]/children[1]/properties[name="name"]</property>
            <value>name</value>
        </varbinding>

        <varbinding vars='x'>
            <property>$strategy/properties[name="x"]</property>
            <property>$strategy/properties[name="outputs"]/children[1]/properties[name="x"]</property>
            <value>x</value>
        </varbinding>

        <varbinding vars='y'>
            <property>$strategy/properties[name="y"]</property>
            <property>$strategy/properties[name="outputs"]/children[1]/properties[name="y"]</property>
            <value>y</value>
        </varbinding>

        <varbinding vars='notes'><property>$strategy/properties[name="outputs"]/children[1]/properties[name="notes"]</property><value>notes</value></varbinding>
        <varbinding vars='ip'> <property>$config/properties[name="ip"]</property> <value>ip</value> </varbinding>
        <varbinding vars='devid'> <property>$config/properties[name="devid"]</property> <value>devid</value> </varbinding>

        <varbinding vars='pushTime'> <property>$strategy/properties[name="outputs"]/children[1]/properties[name="pushTime"]</property> <value>pushTime</value> </varbinding>
        <varbinding vars='pullTime'> <property>$strategy/properties[name="outputs"]/children[1]/properties[name="pullTime"]</property> <value>pullTime</value> </varbinding>

        <varbinding vars='ductPressureRead'> <property>$config/properties[name="ductPressureRead"]</property> <value>ductPressureRead</value> </varbinding>
        <varbinding vars='ductPressureSetpointRead'> <property>$config/properties[name="ductPressureSetpointRead"]</property> <value>ductPressureSetpointRead</value> </varbinding>
        <varbinding vars='tempSetpointRead'> <property>$config/properties[name="tempSetpointRead"]</property> <value>tempSetpointRead</value> </varbinding>
        <varbinding vars='tempSetpointWrite'> <property>$config/properties[name="tempSetpointWrite"]</property> <value>tempSetpointWrite</value> </varbinding>
        <varbinding vars='commRead'> <property>$config/properties[name="commRead"]</property> <value>commRead</value> </varbinding>

        <varbinding vars='ductPressureReadValid,ductPressureSetpointReadValid,tempSetpointReadValid,tempSetpointWriteValid,commReadValid'>
            <property>$config/properties[name="validated"]</property>
            <value>
                if ( ductPressureReadValid == '1' &amp;&amp; ductPressureSetpointReadValid == '1' &amp;&amp; tempSetpointReadValid == '1' &amp;&amp; tempSetpointWriteValid == '1' &amp;&amp; commReadValid == '1' ){
                    return 1;
                } else {
                    return 0;
                }
            </value>
        </varbinding>


        <conducer id="RTUMgrChildLink" name="RTU Manager and Child Strategy Link" required="true">
            <producer id="rtuManagerP" datatype="RTULink_Parent" name="RTU Manager Strategy Output" object="$strategy"/>
            <consumer id="childStrategies" datatype="RTULink_Child" name="Child Strategy Input" property="$strategy/properties[name='childStrategies']" required="false"/>
        </conducer>

        <conducer id="RTUMgrRtuLink" name="RTU Manager and RTU Link">
            <producer id="rtuManager" datatype="RTULink_Manager" name="RTU Manager Strategy Output" object="$strategy"/>
            <consumer id="rtus" datatype="RTULink_RTU" name="RTU Strategy Input" property="$strategy/properties[name='rtus']" required="false"/>
        </conducer>

        <docking from='$strategy/properties[name="outputs"]/children[1]/properties[name="config"]' to='$config'/>


        <object as="strategy" type="controlalg_rtumgr">
            <dlid>DLID:-1</dlid>
            <property name="resource"><value>temperature</value></property>
            <property name="manualOutput"><value>72.0</value></property>
            <property name="outputMin"><value>65.0</value></property>
            <property name="outputMax"><value>90.0</value></property>
            <property name="lowCutoff"><value>50.0</value></property>
            <property name="highCutoff"><value>80.0</value></property>
            <property name="adjustment"><value>1.0</value></property>
            <property name="invertError"><value>1</value></property>
            <property name="restPeriod"><value>600.0</value></property>
            <property name="output"><value>72.0</value></property>
            <property name="mode"><value>manual</value></property>
            <property name="outputs">
                <object as="controlout_rtumgr" type="controlout_rtumgr">
                    <dlid>DLID:-1</dlid>
                    <property name="resource"><value>temperature</value></property>
                    <property name="unitstatus"><value>text/online</value></property>
                    <property name="engaged"><value>0</value></property>
                </object>
            </property>
        </object>

        <object as="config" type="cfg_rtumgr_modbus">
            <dlid>DLID:-1</dlid>
        </object>
    </component>


    <component type='control_register_damper' classes='placeable,airflowcontrol,temperaturecontrol,damper' grouppath='Control;Damper Control' filters="Control">
		<display>
			<description>A Register Damper </description>
			<name>Register Damper</name>
			<shape>register-damper-control</shape>
		</display>
		<property name='name' display='Name' type='java.lang.String' editable='true' displayed='true' dimension=''>Register Damper</property>
		<property name='x' display='x' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
		<property name='y' display='y' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="false" displayed="true">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>
        <property name='configuration' display='Configuration' type='java.lang.String' editable='false' displayed='true' dimension=''>Register Damper</property>
		<property name='notes' display='Notes' type='DeploymentLab.MultiLineContents' editable='true' displayed='true' dimension=''></property>


		<property name="direction" display="direction" type="java.lang.Integer" editable="true" displayed="true"
				  valueChoices="Clockwise:1,Counterclockwise:2">1</property>
		<property name="failsafeSetPoint" display="failsafeSetPoint" type="java.lang.Integer" editable="true" displayed="true">100</property>
		<property name="minimumSetting" display="minimumSetting" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="maximumSetting" display="maximumSetting" type="java.lang.Integer" editable="true" displayed="true">100</property>
		<property name="travelTime" display="travelTime" type="java.lang.Double" editable="true" displayed="true">150</property>

	    <property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

        <!--objects-->

        <object as='strategy' type='controlalg_rd'>
            <dlid>DLID:-1</dlid>
            <property name="outputs">
                <object as='controlout_damper' type='controlout_damper'>
                    <dlid>DLID:-1</dlid>
                    <property name="unitstatus">
                    	<value>text/online</value>
                    </property>
                    <property name="resource">
                    	<value>airflow</value>
                    </property>
                </object>
            </property>
            <property name="failsafeTimeout">
            	<value>1200</value>
            </property>

            <property name="manualOutput">
            	<value>100.0</value>
            </property>
            <property name="outputMin">
            	<value>25.0</value>
            </property>
            <property name="outputMax">
            	<value>100.0</value>
            </property>
            <property name="stratDeadband">
            	<value>1.5</value>
            </property>
            <property name="tempDeadband">
            	<value>1.5</value>
            </property>
            <property name="baseAdjustment">
            	<value>5.0</value>
            </property>
            <property name="output">
            	<value>100.0</value>
            </property>
            <property name="mode">
            	<value>manual</value>
            </property>
            <property name="resource">
            	<value>airflow</value>
            </property>
        </object>



        <!--object as='damper' type='damper'></object-->
        <object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryOperated"><value>0</value></property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status"><value>0</value></property>
			<property name="platformId"><value>80</value></property>
			<property name="platformName"><value>Damper Controller</value></property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>belimo</value></property>
					<property name="channel"><value>0</value></property>
					<property name="min"><value>1.8</value></property>
					<property name="max"><value>3.7</value></property>
					<property name="type"><value>5</value></property>
					<property name="dataclass"><value>210</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
			</property>
			<property name="actuators">
				<object type="wsnactuator">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>belimo</value></property>
					<property name="actuatorID"><value>0</value></property>
                    <property name="positionTolerance"><value>3</value></property>
				</object>
			</property>
		</object>

        <macids value="mac_id" />

        <!--varbinding-->
		<varbinding vars='name'>
			<property>$strategy/properties[name="name"]</property>
            <property>$strategy/properties[name="outputs"]/children[1]/properties[name="name"]</property>
            <!--property>$damper/properties[name="name"]</property-->
            <value>name</value>
		</varbinding>

        <varbinding vars="x">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$node/properties[name="x"]</property>
            <property>$strategy/properties[name="outputs"]/children[1]/properties[name="x"]</property>
            <!--property>$damper/properties[name="x"]</property-->
			<value>x</value>
		</varbinding>

		<varbinding vars="y">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$node/properties[name="y"]</property>
            <property>$strategy/properties[name="outputs"]/children[1]/properties[name="y"]</property>
            <!--property>$damper/properties[name="y"]</property-->
			<value>y</value>
		</varbinding>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<value>location</value>
		</varbinding>

		<varbinding vars="direction"> <property>$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="direction"]</property><value>direction</value></varbinding>
		<varbinding vars="failsafeSetPoint"> <property>$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="failsafeSetPoint"]</property><value>failsafeSetPoint</value></varbinding>
		<varbinding vars="minimumSetting"> <property>$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="minimumSetting"]</property><value>minimumSetting</value></varbinding>
		<varbinding vars="maximumSetting"> <property>$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="maximumSetting"]</property><value>maximumSetting</value></varbinding>
		<varbinding vars="travelTime"> <property>$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="travelTime"]</property><value>travelTime</value></varbinding>


		<!--docking point-->
        <docking from='$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="sensor"]' to='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]'/>
		<docking from='$strategy/properties[name="outputs"]/children[1]/properties[name="actuator"]' to='$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]' />
        
        <conducer id="RegisterDamperParentLink" name="Register Damper and Parent Strategy Link">
            <producer id="registerDamper" datatype="RTULink_Child" name="Register Damper Child Strategy Output" object="$strategy" exclusive="true"/>
            <consumer id="parentStrategy" datatype="RTULink_Parent" name="Parent Strategy Input" property="$strategy/properties[name='parentStrategy']" required="false"/>
        </conducer>

	</component>


    <component type='control_zone_damper' classes='placeable,damper' grouppath='Control;Damper Control' filters="Control">
		<display>
			<description>A Zone Damper </description>
			<name>Zone Damper</name>
			<shape>zone-damper-control</shape>
		</display>
		<property name='name' display='Name' type='java.lang.String' editable='true' displayed='true' dimension=''>Zone Damper</property>
		<property name='x' display='x' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
		<property name='y' display='y' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="false" displayed="true">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>
        <property name='configuration' display='Configuration' type='java.lang.String' editable='false' displayed='true' dimension=''>Zone Damper</property>
		<property name='notes' display='Notes' type='DeploymentLab.MultiLineContents' editable='true' displayed='true' dimension=''></property>

        <property name="direction" display="direction" type="java.lang.Integer" editable="true" displayed="true" valueChoices="Clockwise:1,Counterclockwise:2">1</property>
		<property name="failsafeSetPoint" display="failsafeSetPoint" type="java.lang.Integer" editable="true" displayed="true">100</property>
		<property name="minimumSetting" display="minimumSetting" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="maximumSetting" display="maximumSetting" type="java.lang.Integer" editable="true" displayed="true">100</property>
		<property name="travelTime" display="travelTime" type="java.lang.Double" editable="true" displayed="true">150</property>

	    <property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

        <!--objects-->

        <object as='strategy' type='controlalg_zd'>
            <dlid>DLID:-1</dlid>
            <property name="outputs">
                <object as='controlout_damper' type='controlout_damper'>
                    <dlid>DLID:-1</dlid>
                    <property name="unitstatus">
                    	<value>text/online</value>
                    </property>
                    <property name="resource">
                    	<value>airflow</value>
                    </property>
                </object>
            </property>
            <property name="manualOutput">
            	<value>100.0</value>
            </property>
            <property name="outputMin">
            	<value>25.0</value>
            </property>
            <property name="outputMax">
            	<value>100.0</value>
            </property>
            <property name="lowCutoff">
            	<value>50.0</value>
            </property>
            <property name="highCutoff">
                <value>80.0</value>
            </property>
            <property name="adjustment">
                <value>10.0</value>
            </property>
            <property name="invertError"><value>-1</value></property>
            <property name="restPeriod"><value>600.0</value></property>
            <property name="output">
            	<value>100.0</value>
            </property>
            <property name="mode">
            	<value>manual</value>
            </property>
            <property name="resource">
            	<value>airflow</value>
            </property>
        </object>

        <!--object as='damper' type='damper'></object-->
        <object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryOperated"><value>0</value></property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status"><value>0</value></property>
			<property name="platformId"><value>80</value></property>
			<property name="platformName"><value>Damper Controller</value></property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>belimo</value></property>
					<property name="channel"><value>0</value></property>
					<property name="min"><value>1.8</value></property>
					<property name="max"><value>3.7</value></property>
					<property name="type"><value>5</value></property>
					<property name="dataclass"><value>210</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
			</property>
			<property name="actuators">
				<object type="wsnactuator">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>belimo</value></property>
					<property name="actuatorID"><value>0</value></property>
                    <property name="positionTolerance"><value>3</value></property>
				</object>
			</property>
			
		</object>

        <macids value="mac_id" />

        <!--varbinding-->
		<varbinding vars='name'>
			<property>$strategy/properties[name="name"]</property>
            <property>$strategy/properties[name="outputs"]/children[1]/properties[name="name"]</property>
            <!--property>$damper/properties[name="name"]</property-->
            <value>name</value>
		</varbinding>

        <varbinding vars="x">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$node/properties[name="x"]</property>
            <property>$strategy/properties[name="outputs"]/children[1]/properties[name="x"]</property>
            <!--property>$damper/properties[name="x"]</property-->
			<value>x</value>
		</varbinding>

		<varbinding vars="y">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$node/properties[name="y"]</property>
            <property>$strategy/properties[name="outputs"]/children[1]/properties[name="y"]</property>
            <!--property>$damper/properties[name="y"]</property-->
			<value>y</value>
		</varbinding>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<value>location</value>
		</varbinding>

        <varbinding vars="direction"> <property>$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="direction"]</property><value>direction</value></varbinding>
		<varbinding vars="failsafeSetPoint"> <property>$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="failsafeSetPoint"]</property><value>failsafeSetPoint</value></varbinding>
		<varbinding vars="minimumSetting"> <property>$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="minimumSetting"]</property><value>minimumSetting</value></varbinding>
		<varbinding vars="maximumSetting"> <property>$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="maximumSetting"]</property><value>maximumSetting</value></varbinding>
		<varbinding vars="travelTime"> <property>$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="travelTime"]</property><value>travelTime</value></varbinding>

        <!--docking point-->
        <docking from='$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="sensor"]' to='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]'/>
        <docking from='$strategy/properties[name="outputs"]/children[1]/properties[name="actuator"]' to='$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]' />

        <conducer id="ZoneDamperParentLink" name="Zone Damper and Parent Strategy Link">
            <producer id="zoneDamperC" datatype="RTULink_Child" name="Zone Damper Child Strategy Output" object="$strategy" exclusive="true"/>
            <consumer id="parentStrategy" datatype="RTULink_Parent" name="Parent Strategy Input" property="$strategy/properties[name='parentStrategy']" required="false"/>
        </conducer>

        <conducer id="ZoneDamperChildLink" name="Zone Damper and Child Strategy Link" required="true">
            <producer id="zoneDamperP" datatype="RTULink_Parent" name="Zone Damper Parent Strategy Output" object="$strategy"/>
            <consumer id="childStrategies" datatype="RTULink_Child" name="Child Strategy Input" property="$strategy/properties[name='childStrategies']" required="false"/>
        </conducer>
	</component>
	
	
	<component type='control_rtu_chw_modbus' classes='placeable,airflowcontrol,temperaturecontrol,rtu' grouppath='Control;Damper Control' filters="Control">
        <display>
            <description>RTU CHW Modbus</description>
            <name>Modbus RTU CHW </name>
            <shape>rtu-chw_modbus</shape>
        </display>

        <!--objects-->
        <object as='controlout_rtuchw' type='controlout_rtuchw'>
            <dlid>DLID:-1</dlid>
            <property name="unitstatus">
                <value>text/online</value>
            </property>
            <property name="engaged">
                <value>0</value>
            </property>
        </object>

        <object as='cfg_rtuchw_modbus' type='cfg_rtuchw_modbus'>
            <dlid>DLID:-1</dlid>
        </object>

        <property name='name' display='Name' type='java.lang.String' editable='true' displayed='true' dimension=''>Modbus RTU CHW</property>
        <property name='x' display='x' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
        <property name='y' display='y' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
        <property name='configuration' display='Configuration' type='java.lang.String' editable='false' displayed='true' dimension=''>Modbus RTU CHW</property>
        <property name='notes' display='Notes' type='DeploymentLab.MultiLineContents' editable='true' displayed='true' dimension=''></property>

        <property name='ip' display='IP Address' type='java.lang.String' editable='true' displayed='true' dimension=''>0</property>
        <property name='devid' display='Device ID' type='java.lang.Integer' editable='true' displayed='true' dimension=''>0</property>

        <property name='pullTime' display='Device Read Timer (sec)' type='java.lang.Integer' editable='true' displayed='true' >30</property>

        <property name='returnTempRead' display='Return Temp' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='returnTempReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='returnTempReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='supplyTempRead' display='Supply Temp' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='supplyTempReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='supplyTempReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='coolingCapacityRead' display='Cooling Capacity' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='coolingCapacityReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='coolingCapacityReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='fanSpeedRead' display='Fan Speed' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='fanSpeedReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='fanSpeedReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='fanPowerRead' display='Fan Power' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='fanPowerReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='fanPowerReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='commRead' display='Comm Read' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='commReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='commReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='standbyRead' display='Stanby Read' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='standbyReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='standbyReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <varbinding vars='name'>
            <property>$controlout_rtuchw/properties[name="name"]</property>
            <value>name</value>
        </varbinding>

        <varbinding vars='x'>
            <property>$controlout_rtuchw/properties[name="x"]</property>
            <value>x</value>
        </varbinding>

        <varbinding vars='y'>
            <property>$controlout_rtuchw/properties[name="y"]</property>
            <value>y</value>
        </varbinding>

        <varbinding vars='pullTime'> <property>$controlout_rtuchw/properties[name="pullTime"]</property> <value>pullTime</value> </varbinding>

        <varbinding vars='ip'> <property>$cfg_rtuchw_modbus/properties[name="ip"]</property> <value>ip</value> </varbinding>
        <varbinding vars='devid'> <property>$cfg_rtuchw_modbus/properties[name="devid"]</property> <value>devid</value> </varbinding>
        <varbinding vars='returnTempRead'> <property>$cfg_rtuchw_modbus/properties[name="returnTempRead"]</property> <value>returnTempRead</value> </varbinding>
        <varbinding vars='supplyTempRead'> <property>$cfg_rtuchw_modbus/properties[name="supplyTempRead"]</property> <value>supplyTempRead</value> </varbinding>
        <varbinding vars='coolingCapacityRead'> <property>$cfg_rtuchw_modbus/properties[name="coolingCapacityRead"]</property> <value>coolingCapacityRead</value> </varbinding>
        <varbinding vars='fanSpeedRead'> <property>$cfg_rtuchw_modbus/properties[name="fanSpeedRead"]</property> <value>fanSpeedRead</value> </varbinding>
        <varbinding vars='fanPowerRead'> <property>$cfg_rtuchw_modbus/properties[name="fanPowerRead"]</property> <value>fanPowerRead</value> </varbinding>
		<varbinding vars='commRead'> <property>$cfg_rtuchw_modbus/properties[name="commRead"]</property> <value>commRead</value> </varbinding>
        <varbinding vars='standbyRead'> <property>$cfg_rtuchw_modbus/properties[name="standbyRead"]</property> <value>standbyRead</value> </varbinding>

        <varbinding vars='returnTempReadValid,supplyTempReadValid,coolingCapacityReadValid,fanSpeedReadValid,fanPowerReadValid,commReadValid,standbyReadValid'>
            <property>$cfg_rtuchw_modbus/properties[name="validated"]</property>
            <value>
                if ( returnTempReadValid == '1' &amp;&amp; supplyTempReadValid == '1' &amp;&amp; coolingCapacityReadValid == '1' &amp;&amp; fanSpeedReadValid == '1' &amp;&amp; fanPowerReadValid == '1' &amp;&amp; commReadValid == '1' &amp;&amp; standbyReadValid == '1' ){
                    return 1;
                } else {
                    return 0;
                }
            </value>
        </varbinding>

        <docking from='$controlout_rtuchw/properties[name="config"]' to='$cfg_rtuchw_modbus'/>

        <conducer id="RTUMgrRtuLink" name="RTU Manager and RTU Link" required="true">
            <producer id="rtus" datatype="RTULink_RTU" name="RTU Output" object="$controlout_rtuchw"/>
            <consumer id="rtuManagerR" datatype="RTULink_Manager" name="RTU Manager Input" property="$controlout_rtuchw/properties[name='strategy']" required="false"/>
        </conducer>
    </component>
	
	
	<component type='control_rtu_chw_bacnet' classes='placeable,airflowcontrol,temperaturecontrol,manualexpressions,rtu' grouppath='Control;Damper Control' filters="Control">
        <display>
            <description>RTU CHW Bacnet</description>
            <name>BACnet RTU CHW</name>
            <shape>rtu-chw_bacnet</shape>
        </display>

        <object as="controlout_rtuchw" type="controlout_rtuchw">
            <dlid>DLID:-1</dlid>
            <property name="unitstatus">
                <value>text/online</value>
            </property>

            <property name="engaged">
                <value>0</value>
            </property>
        </object>

        <object as="cfg_rtuchw_bacnet" type="cfg_rtuchw_bacnet">
            <dlid>DLID:-1</dlid>
        </object>

        <property name='name' display='Name' type='java.lang.String' editable='true' displayed='true' dimension=''>BACnet RTU CHW</property>
        <property name='x' display='x' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
        <property name='y' display='y' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
        <property name='configuration' display='Configuration' type='java.lang.String' editable='false' displayed='true' dimension=''>BACnet RTU CHW</property>
        <property name='notes' display='Notes' type='DeploymentLab.MultiLineContents' editable='true' displayed='true' dimension=''></property>

        <property name='ip' display='IP Address' type='java.lang.String' editable='true' displayed='true' dimension=''>0</property>
        <property name='network' display='BACnet network' type='java.lang.Integer' editable='true' displayed='true' dimension=''>0</property>
        <property name='device' display='BACnet device' type='java.lang.Integer' editable='true' displayed='true' dimension=''>0</property>

        <property name='pullTime' display='Device Read Timer (sec)' type='java.lang.Integer' editable='true' displayed='true' >30</property>

        <property name='returnTempRead' display='Return Temp' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='returnTempReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='returnTempReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='supplyTempRead' display='Supply Temp' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='supplyTempReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='supplyTempReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='coolingCapacityRead' display='Cooling Capacity' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='coolingCapacityReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='coolingCapacityReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='fanSpeedRead' display='Fan Speed' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='fanSpeedReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='fanSpeedReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='fanPowerRead' display='Fan Power' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='fanPowerReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='fanPowerReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='commRead' display='Comm Read' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='commReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='commReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='standbyRead' display='Stanby Read' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='standbyReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='standbyReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

         <varbinding vars='name'>
            <property>$controlout_rtuchw/properties[name="name"]</property>
            <value>name</value>
        </varbinding>

        <varbinding vars='x'>
            <property>$controlout_rtuchw/properties[name="x"]</property>
            <value>x</value>
        </varbinding>

        <varbinding vars='y'>
            <property>$controlout_rtuchw/properties[name="y"]</property>
            <value>y</value>
        </varbinding>

        <varbinding vars='pullTime'> <property>$controlout_rtuchw/properties[name="pullTime"]</property> <value>pullTime</value> </varbinding>

        <varbinding vars='ip'> <property>$cfg_rtuchw_bacnet/properties[name="ip"]</property> <value>ip</value> </varbinding>
        <varbinding vars='device'> <property>$cfg_rtuchw_bacnet/properties[name="devid"]</property> <value>device</value> </varbinding>
        <varbinding vars='network'> <property>$cfg_rtuchw_bacnet/properties[name="network"]</property> <value>network</value> </varbinding>
        <varbinding vars='returnTempRead'> <property>$cfg_rtuchw_bacnet/properties[name="returnTempRead"]</property> <value>returnTempRead</value> </varbinding>
        <varbinding vars='supplyTempRead'> <property>$cfg_rtuchw_bacnet/properties[name="supplyTempRead"]</property> <value>supplyTempRead</value> </varbinding>
        <varbinding vars='coolingCapacityRead'> <property>$cfg_rtuchw_bacnet/properties[name="coolingCapacityRead"]</property> <value>coolingCapacityRead</value> </varbinding>
        <varbinding vars='fanSpeedRead'> <property>$cfg_rtuchw_bacnet/properties[name="fanSpeedRead"]</property> <value>fanSpeedRead</value> </varbinding>
        <varbinding vars='fanPowerRead'> <property>$cfg_rtuchw_bacnet/properties[name="fanPowerRead"]</property> <value>fanPowerRead</value> </varbinding>
		<varbinding vars='commRead'> <property>$cfg_rtuchw_bacnet/properties[name="commRead"]</property> <value>commRead</value> </varbinding>
        <varbinding vars='standbyRead'> <property>$cfg_rtuchw_bacnet/properties[name="standbyRead"]</property> <value>standbyRead</value> </varbinding>

        <varbinding vars='returnTempReadValid,supplyTempReadValid,coolingCapacityReadValid,fanSpeedReadValid,fanPowerReadValid,commReadValid,standbyReadValid'>
            <property>$cfg_rtuchw_bacnet/properties[name="validated"]</property>
            <value>
                if ( returnTempReadValid == '1' &amp;&amp; supplyTempReadValid == '1' &amp;&amp; coolingCapacityReadValid == '1' &amp;&amp; fanSpeedReadValid == '1' &amp;&amp; fanPowerReadValid == '1' &amp;&amp; commReadValid == '1' &amp;&amp; standbyReadValid == '1' ){
                    return 1;
                } else {
                    return 0;
                }
            </value>
        </varbinding>

        <docking from='$controlout_rtuchw/properties[name="config"]' to='$cfg_rtuchw_bacnet'/>

        <conducer id="RTUMgrRtuLink" name="RTU Manager and RTU Link" required="true">
            <producer id="rtus" datatype="RTULink_RTU" name="RTU Output" object="$controlout_rtuchw"/>
            <consumer id="rtuManagerR" datatype="RTULink_Manager" name="RTU Manager Input" property="$controlout_rtuchw/properties[name='strategy']" required="false"/>
        </conducer>
   </component>


    <component type='control_rtu_dx_modbus' classes='placeable,airflowcontrol,temperaturecontrol,rtu' grouppath='Control;Damper Control' filters="Control">
        <display>
            <description>RTU DX Modbus</description>
            <name>Modbus RTU DX </name>
            <shape>rtu-dx_modbus</shape>
        </display>

        <!--objects-->
        <object as='controlout_rtudx' type='controlout_rtudx'>
            <dlid>DLID:-1</dlid>
            <property name="unitstatus">
                <value>text/online</value>
            </property>
            <property name="engaged">
                <value>0</value>
            </property>
        </object>

        <object as='cfg_rtudx_modbus' type='cfg_rtudx_modbus'>
            <dlid>DLID:-1</dlid>
        </object>

        <property name='name' display='Name' type='java.lang.String' editable='true' displayed='true' dimension=''>Modbus RTU DX</property>
        <property name='x' display='x' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
        <property name='y' display='y' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
        <property name='configuration' display='Configuration' type='java.lang.String' editable='false' displayed='true' dimension=''>Modbus RTU DX</property>
        <property name='notes' display='Notes' type='DeploymentLab.MultiLineContents' editable='true' displayed='true' dimension=''></property>

        <property name='ip' display='IP Address' type='java.lang.String' editable='true' displayed='true' dimension=''>0</property>
        <property name='devid' display='Device ID' type='java.lang.Integer' editable='true' displayed='true' dimension=''>0</property>

        <property name='pullTime' display='Device Read Timer (sec)' type='java.lang.Integer' editable='true' displayed='true' >30</property>

        <property name='returnTempRead' display='Return Temp' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='returnTempReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='returnTempReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='supplyTempRead' display='Supply Temp' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='supplyTempReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='supplyTempReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='compressorStagesRead' display='Compressor Stages' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='compressorStagesReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='compressorStagesReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='coolingCapacityRead' display='Cooling Capacity' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='coolingCapacityReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='coolingCapacityReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='fanSpeedRead' display='Fan Speed' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='fanSpeedReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='fanSpeedReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='fanPowerRead' display='Fan Power' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='fanPowerReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='fanPowerReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='commRead' display='Comm Read' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='commReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='commReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='standbyRead' display='Stanby Read' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='standbyReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;40001;Int16;none;10;10;true</property>
        <property name='standbyReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <varbinding vars='name'>
            <property>$controlout_rtudx/properties[name="name"]</property>
            <value>name</value>
        </varbinding>

        <varbinding vars='x'>
            <property>$controlout_rtudx/properties[name="x"]</property>
            <value>x</value>
        </varbinding>

        <varbinding vars='y'>
            <property>$controlout_rtudx/properties[name="y"]</property>
            <value>y</value>
        </varbinding>

        <varbinding vars='pullTime'> <property>$controlout_rtudx/properties[name="pullTime"]</property> <value>pullTime</value> </varbinding>

        <varbinding vars='ip'> <property>$cfg_rtudx_modbus/properties[name="ip"]</property> <value>ip</value> </varbinding>
        <varbinding vars='devid'> <property>$cfg_rtudx_modbus/properties[name="devid"]</property> <value>devid</value> </varbinding>
        <varbinding vars='returnTempRead'> <property>$cfg_rtudx_modbus/properties[name="returnTempRead"]</property> <value>returnTempRead</value> </varbinding>
        <varbinding vars='supplyTempRead'> <property>$cfg_rtudx_modbus/properties[name="supplyTempRead"]</property> <value>supplyTempRead</value> </varbinding>
        <varbinding vars='compressorStagesRead'> <property>$cfg_rtudx_modbus/properties[name="compressorStagesRead"]</property> <value>compressorStagesRead</value> </varbinding>
        <varbinding vars='coolingCapacityRead'> <property>$cfg_rtudx_modbus/properties[name="coolingCapacityRead"]</property> <value>coolingCapacityRead</value> </varbinding>
        <varbinding vars='fanSpeedRead'> <property>$cfg_rtudx_modbus/properties[name="fanSpeedRead"]</property> <value>fanSpeedRead</value> </varbinding>
        <varbinding vars='fanPowerRead'> <property>$cfg_rtudx_modbus/properties[name="fanPowerRead"]</property> <value>fanPowerRead</value> </varbinding>
		<varbinding vars='commRead'> <property>$cfg_rtudx_modbus/properties[name="commRead"]</property> <value>commRead</value> </varbinding>
        <varbinding vars='standbyRead'> <property>$cfg_rtudx_modbus/properties[name="standbyRead"]</property> <value>standbyRead</value> </varbinding>

        <varbinding vars='returnTempReadValid,supplyTempReadValid,compressorStagesRead,coolingCapacityReadValid,fanSpeedReadValid,fanPowerReadValid,commReadValid,standbyReadValid'>
            <property>$cfg_rtudx_modbus/properties[name="validated"]</property>
            <value>
                if ( returnTempReadValid == '1' &amp;&amp; supplyTempReadValid == '1' &amp;&amp; compressorStagesRead == '1' &amp;&amp; coolingCapacityReadValid == '1' &amp;&amp; fanSpeedReadValid == '1' &amp;&amp; fanPowerReadValid == '1' &amp;&amp; commReadValid == '1' &amp;&amp; standbyReadValid == '1' ){
                    return 1;
                } else {
                    return 0;
                }
            </value>
        </varbinding>

        <docking from='$controlout_rtudx/properties[name="config"]' to='$cfg_rtudx_modbus'/>

        <conducer id="RTUMgrRtuLink" name="RTU Manager and RTU Link" required="true">
            <producer id="rtus" datatype="RTULink_RTU" name="RTU Output" object="$controlout_rtudx"/>
            <consumer id="rtuManagerR" datatype="RTULink_Manager" name="RTU Manager Input" property="$controlout_rtudx/properties[name='strategy']" required="false"/>
        </conducer>
    </component>


    <component type='control_rtu_dx_bacnet' classes='placeable,airflowcontrol,temperaturecontrol,manualexpressions,rtu' grouppath='Control;Damper Control' filters="Control">
        <display>
            <description>RTU DX Bacnet</description>
            <name>BACnet RTU DX</name>
            <shape>rtu-dx_bacnet</shape>
        </display>

        <object as="controlout_rtudx" type="controlout_rtudx">
            <dlid>DLID:-1</dlid>
            <property name="unitstatus">
                <value>text/online</value>
            </property>
            <property name="engaged">
                <value>0</value>
            </property>
        </object>

        <object as="cfg_rtudx_bacnet" type="cfg_rtudx_bacnet">
            <dlid>DLID:-1</dlid>
        </object>

        <property name='name' display='Name' type='java.lang.String' editable='true' displayed='true' dimension=''>BACnet RTU DX</property>
        <property name='x' display='x' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
        <property name='y' display='y' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
        <property name='configuration' display='Configuration' type='java.lang.String' editable='false' displayed='true' dimension=''>BACnet RTU DX</property>
        <property name='notes' display='Notes' type='DeploymentLab.MultiLineContents' editable='true' displayed='true' dimension=''></property>

        <property name='ip' display='IP Address' type='java.lang.String' editable='true' displayed='true' dimension=''>0</property>
        <property name='network' display='BACnet network' type='java.lang.Integer' editable='true' displayed='true' dimension=''>0</property>
        <property name='device' display='BACnet device' type='java.lang.Integer' editable='true' displayed='true' dimension=''>0</property>

        <property name='pullTime' display='Device Read Timer (sec)' type='java.lang.Integer' editable='true' displayed='true' >30</property>

        <property name='returnTempRead' display='Return Temp' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='returnTempReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='returnTempReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='supplyTempRead' display='Supply Temp' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='supplyTempReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='supplyTempReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='compressorStagesRead' display='Compressor Stages' type='DeploymentLab.ConfigModbusRead' editable='true' displayed='true' dimension=''></property>
        <property name='compressorStagesReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='compressorStagesReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='coolingCapacityRead' display='Cooling Capacity' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='coolingCapacityReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='coolingCapacityReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='fanSpeedRead' display='Fan Speed' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='fanSpeedReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='fanSpeedReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='fanPowerRead' display='Fan Power' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='fanPowerReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='fanPowerReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='commRead' display='Comm Read' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='commReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='commReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

        <property name='standbyRead' display='Stanby Read' type='DeploymentLab.ConfigBacnetRead' editable='true' displayed='true' dimension=''></property>
        <property name='standbyReadString' display=' ' type='java.lang.String' editable='true' displayed='false' dimension=''>0;0;Analog Input;None;10;10;true;85;</property>
        <property name='standbyReadValid' display=' ' type='java.lang.Integer' editable='true' displayed='false' dimension=''>1</property>

         <varbinding vars='name'>
            <property>$controlout_rtudx/properties[name="name"]</property>
            <value>name</value>
        </varbinding>

        <varbinding vars='x'>
            <property>$controlout_rtudx/properties[name="x"]</property>
            <value>x</value>
        </varbinding>

        <varbinding vars='y'>
            <property>$controlout_rtudx/properties[name="y"]</property>
            <value>y</value>
        </varbinding>

        <varbinding vars='pullTime'> <property>$controlout_rtudx/properties[name="pullTime"]</property> <value>pullTime</value> </varbinding>

        <varbinding vars='ip'> <property>$cfg_rtudx_bacnet/properties[name="ip"]</property> <value>ip</value> </varbinding>
        <varbinding vars='device'> <property>$cfg_rtudx_bacnet/properties[name="devid"]</property> <value>device</value> </varbinding>
        <varbinding vars='network'> <property>$cfg_rtudx_bacnet/properties[name="network"]</property> <value>network</value> </varbinding>
        <varbinding vars='returnTempRead'> <property>$cfg_rtudx_bacnet/properties[name="returnTempRead"]</property> <value>returnTempRead</value> </varbinding>
        <varbinding vars='supplyTempRead'> <property>$cfg_rtudx_bacnet/properties[name="supplyTempRead"]</property> <value>supplyTempRead</value> </varbinding>
        <varbinding vars='compressorStagesRead'> <property>$cfg_rtudx_bacnet/properties[name="compressorStagesRead"]</property> <value>compressorStagesRead</value> </varbinding>
        <varbinding vars='coolingCapacityRead'> <property>$cfg_rtudx_bacnet/properties[name="coolingCapacityRead"]</property> <value>coolingCapacityRead</value> </varbinding>
        <varbinding vars='fanSpeedRead'> <property>$cfg_rtudx_bacnet/properties[name="fanSpeedRead"]</property> <value>fanSpeedRead</value> </varbinding>
        <varbinding vars='fanPowerRead'> <property>$cfg_rtudx_bacnet/properties[name="fanPowerRead"]</property> <value>fanPowerRead</value> </varbinding>
        <varbinding vars='commRead'> <property>$cfg_rtudx_bacnet/properties[name="commRead"]</property> <value>commRead</value> </varbinding>
        <varbinding vars='standbyRead'> <property>$cfg_rtudx_bacnet/properties[name="standbyRead"]</property> <value>standbyRead</value> </varbinding>

        <varbinding vars='returnTempReadValid,supplyTempReadValid,compressorStagesRead,coolingCapacityReadValid,fanSpeedReadValid,fanPowerReadValid,commReadValid,standbyReadValid'>
            <property>$cfg_rtudx_bacnet/properties[name="validated"]</property>
            <value>
                if ( returnTempReadValid == '1' &amp;&amp; supplyTempReadValid == '1' &amp;&amp; compressorStagesRead == '1' &amp;&amp; coolingCapacityReadValid == '1' &amp;&amp; fanSpeedReadValid == '1' &amp;&amp; fanPowerReadValid == '1' &amp;&amp; commReadValid == '1' &amp;&amp; standbyReadValid == '1' ){
                    return 1;
                } else {
                    return 0;
                }
            </value>
        </varbinding>

        <docking from='$controlout_rtudx/properties[name="config"]' to='$cfg_rtudx_bacnet'/>

        <conducer id="RTUMgrRtuLink" name="RTU Manager and RTU Link" required="true">
            <producer id="rtus" datatype="RTULink_RTU" name="RTU Output" object="$controlout_rtudx"/>
            <consumer id="rtuManagerR" datatype="RTULink_Manager" name="RTU Manager Input" property="$controlout_rtudx/properties[name='strategy']" required="false"/>
        </conducer>
   </component>


</componentlib>
