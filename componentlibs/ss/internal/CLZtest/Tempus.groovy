package com.synapsense.deploymentlab;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;;

/**
 Takes a user defined input and jiggles it randomly within a user defined value.
 */
public class Tempus implements RuleAction {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(Tempus.class);
	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.info("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");
		try {
			def finalValue = 42.0
			logger.info("TEMPUS ${triggeredRule.getName()} results in $finalValue"  )
			return (double) finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}
