<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib name="test objects" version="&componentlib.version;" oversion="&componentlib.oversion;" cversion="&componentlib.cversion;">
	
	<component type="random" classes="placeable" grouppath="Calculations;Testing" filters="Calculations">
		<display>
			<description>A pseudo-random number generator.</description>
			<name>Random Number</name>
			<shape>pue_circle_diamond_spiral</shape>
		</display>

		<object type="random" as="random">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
			<property name="name"><value>Random Number</value></property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Random Number</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false">0.0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false">0.0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Random Number</property>
		<property name="notes" display="Notes" type="DeploymentLab.MultiLineContents" editable="true" displayed="true"></property>
		<property name="max" display="Maximum Value" type="java.lang.Double" editable="true" displayed="true">10</property>

		<producer id="value" datatype="power" name="Random Number" object='$random' />

		<varbinding vars="name">
			<property>$random/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="notes">
			<property>$random/properties[name="notes"]</property>
			<value>notes</value>
		</varbinding>
		<varbinding vars="max">
			<property>$random/properties[name="max"]</property>
			<value>max</value>
		</varbinding>
	</component>


	<component type="wobulator" classes="placeable" grouppath="Calculations;Testing" filters="Calculations">
		<display>
			<description>A Wobulation Machine.</description>
			<name>Wobulator</name>
			<shape>pue_circle_diamond_spiral</shape>
		</display>

		<object type="wobulator" as="wobulator">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
			<property name="name"><value>wobulator</value></property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Wobulator</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false">0.0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false">0.0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Wobulator</property>
		<property name="notes" display="Notes" type="DeploymentLab.MultiLineContents" editable="true" displayed="true"></property>

		<property name="baseline" display="Baseline Value" type="java.lang.Double" editable="true" displayed="true">10</property>
		<property name="jiggle" display="Max Jiggle Factor" type="java.lang.Double" editable="true" displayed="true">2</property>

		<producer id="value" datatype="power" name="Wobulated Number" object='$wobulator' />

		<varbinding vars="name">
			<property>$wobulator/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="notes">
			<property>$wobulator/properties[name="notes"]</property>
			<value>notes</value>
		</varbinding>
		<varbinding vars="baseline">
			<property>$wobulator/properties[name="baseline"]</property>
			<value>baseline</value>
		</varbinding>
		<varbinding vars="jiggle">
			<property>$wobulator/properties[name="jiggle"]</property>
			<value>jiggle</value>
		</varbinding>
	</component>


	<component type='pue_user_status' classes='placeable' grouppath="Calculations;Testing" filters="Calculations">
		<display>
		  <description>A user entered Status Value</description>
		  <name>User Status Value</name>
		  <shape>pue_circle_status</shape>
		</display>
		<property name='name' display='Name' type='java.lang.String' editable='true' displayed='true' dimension=''>Status Value</property>
		<property name='x' display='x' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
		<property name='y' display='y' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
		<property name='configuration' display='Configuration' type='java.lang.String' editable='false' displayed='true' dimension=''>Status Value</property>
		<property name='notes' display='Notes' type='DeploymentLab.MultiLineContents' editable='true' displayed='true' dimension=''></property>
		<property name='value' display='User Value' type='java.lang.Double' editable='true' displayed='true' valueChoices="On:1.0,Off:0.0">1.0</property>
	<varbinding vars='name'>
	  <property>$userinput_status/properties[name="name"]</property>
	  <value>name</value>
	</varbinding>
	<varbinding vars='notes'>
	  <property>$userinput_status/properties[name="notes"]</property>
	  <value>notes</value>
	</varbinding>
	<varbinding vars='value'>
	  <property>$userinput_status/properties[name="lastValue"]</property>
	  <value>value</value>
	</varbinding>

	<producer id='value' datatype='status' name='User Value' object='$userinput_status' />

	<object as='userinput_status' type='userinput_status'>
	  <dlid>DLID:-1</dlid>
		<property name="status"><value>1</value></property>
	</object>
	</component>


	<component type='pue_random_status' classes='placeable' grouppath="Calculations;Testing" filters="Calculations">
		<display>
			<description>A random Status Value</description>
			<name>Random Status</name>
			<shape>pue_circle_diamond_spiral</shape>
		</display>
		<property name='name' display='Name' type='java.lang.String' editable='true' displayed='true' dimension=''>Random Status</property>
		<property name='x' display='x' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
		<property name='y' display='y' type='java.lang.Double' editable='true' displayed='false' dimension='distance'>0</property>
		<property name='configuration' display='Configuration' type='java.lang.String' editable='false' displayed='true' dimension=''>Random Status</property>
		<property name='notes' display='Notes' type='DeploymentLab.MultiLineContents' editable='true' displayed='true' dimension=''></property>
		<varbinding vars='name'>
			<property>$random_status/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars='notes'>
			<property>$random_status/properties[name="notes"]</property>
			<value>notes</value>
		</varbinding>
		<producer id='value' datatype='status' name='Random Status' object='$random_status' />
		<object as='random_status' type='random_status'>
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
		</object>
	</component>


	<component type="tempus" classes="placeable" grouppath="Calculations;Testing" filters="Calculations">
		<display>
			<description>Wobulates based on the time of day.</description>
			<name>Tempus Fidgeter</name>
			<shape>pue_circle_diamond_spiral</shape>
		</display>

		<object type="tempus" as="tempus">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Tempus Fidgeter</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false">0.0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false">0.0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Tempus Fidgeter</property>
		<property name="notes" display="Notes" type="DeploymentLab.MultiLineContents" editable="true" displayed="true"></property>

		<property name="highval" display="High Value" type="java.lang.Double" editable="true" displayed="true">100</property>
		<property name="lowval" display="Low Value" type="java.lang.Double" editable="true" displayed="true">25</property>
		<property name="jiggle" display="Max Jiggle Factor" type="java.lang.Double" editable="true" displayed="true">10</property>

		<property name="start" display="Start Time" type="java.lang.Double" editable="true" displayed="true"
		valueChoices="1am:1,2am:2,3am:3,4am:4,5am:5,6am:6,7am:7,8am:8,9am:9,10am:10,11am:11,noon:12,1pm:13,2pm:14,3pm:15,4pm:16,5pm:17,6pm:18,7pm:19,8pm:20,9pm:21,10pm:22,11pm:23,midnight:24">8</property>


		<property name="finish" display="End Time" type="java.lang.Double" editable="true" displayed="true"
		valueChoices="1am:1,2am:2,3am:3,4am:4,5am:5,6am:6,7am:7,8am:8,9am:9,10am:10,11am:11,noon:12,1pm:13,2pm:14,3pm:15,4pm:16,5pm:17,6pm:18,7pm:19,8pm:20,9pm:21,10pm:22,11pm:23,midnight:24">18</property>

		<producer id="value" datatype="power" name="Time-Wobulated Number" object='$tempus' />

		<varbinding vars="name">
			<property>$tempus/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="notes">
			<property>$tempus/properties[name="notes"]</property>
			<value>notes</value>
		</varbinding>
		<varbinding vars="highval">
			<property>$tempus/properties[name="highval"]</property>
			<value>highval</value>
		</varbinding>
		<varbinding vars="lowval">
			<property>$tempus/properties[name="lowval"]</property>
			<value>lowval</value>
		</varbinding>
		<varbinding vars="jiggle">
			<property>$tempus/properties[name="jiggle"]</property>
			<value>jiggle</value>
		</varbinding>
		<varbinding vars="start">
			<property>$tempus/properties[name="start"]</property>
			<value>start</value>
		</varbinding>
		<varbinding vars="finish">
			<property>$tempus/properties[name="finish"]</property>
			<value>finish</value>
		</varbinding>
	</component>


	<component type="test_sequence" classes="placeable" grouppath="Calculations;Testing" filters="Calculations">
		<display>
			<description>Runs through a set of values in order</description>
			<name>Sequence</name>
			<shape>pue_circle_diamond_spiral</shape>
		</display>

		<object type="test_sequence" as="sequence">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>sequence</value></property>
			<property name="status"><value>1</value></property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Sequence</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false">0.0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false">0.0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Sequence</property>
		<property name="notes" display="Notes" type="DeploymentLab.MultiLineContents" editable="true" displayed="true"></property>

		<property name="sequence" display="Sequence Values" type="java.lang.String" editable="true" displayed="true">5.0,10.0,15.0</property>
		<property name="statusSequence" display="Status Sequence Values" type="java.lang.String" editable="true" displayed="true">1,1,1</property>

		<producer id="value" datatype="power" name="sequence Number" object='$sequence' />

		<varbinding vars="name">
			<property>$sequence/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="notes">
			<property>$sequence/properties[name="notes"]</property>
			<value>notes</value>
		</varbinding>
		<varbinding vars="sequence">
			<property>$sequence/properties[name="sequence"]</property>
			<value>sequence</value>
		</varbinding>
		<varbinding vars="statusSequence">
			<property>$sequence/properties[name="statusSequence"]</property>
			<value>statusSequence</value>
		</varbinding>
	</component>


	<component type="make_it_null" classes="placeable" grouppath="Calculations;Testing" filters="Calculations">
		<display>
			<description>Makes it Null!</description>
			<name>NULL!</name>
			<shape>pue_circle_diamond_spiral</shape>
		</display>

		<object type="make_it_null" as="make_it_null">
			<dlid>DLID:-1</dlid>
			<property name="name"><value>make_it_null</value></property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">NULL!</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false">0.0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false">0.0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">NULL!</property>
		<property name="notes" display="Notes" type="DeploymentLab.MultiLineContents" editable="true" displayed="true"></property>
		<property name="status" display="Status" type="java.lang.Integer" editable="true" displayed="true">1</property>

		<producer id="value" datatype="power" name="NULL" object='$make_it_null' />

		<varbinding vars="name">
			<property>$make_it_null/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="notes">
			<property>$make_it_null/properties[name="notes"]</property>
			<value>notes</value>
		</varbinding>
		<varbinding vars="status">
			<property>$make_it_null/properties[name="status"]</property>
			<value>status</value>
		</varbinding>
	</component>


	<component type="xerox" classes="placeable" grouppath="Calculations;Testing" filters="Calculations">
		<display>
			<description>A xerox Machine.</description>
			<name>xerox</name>
			<shape>pue_circle_diamond_spiral</shape>
		</display>

		<object type="wobulator" as="wobulator">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
			<property name="name"><value>wobulator</value></property>
		</object>

		<object type="xerox" as="xerox">
			<dlid>DLID:-1</dlid>
		</object>

		<object type="userinput" as="userinput">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">xerox</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false">0.0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false">0.0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">xerox</property>
		<property name="notes" display="Notes" type="DeploymentLab.MultiLineContents" editable="true" displayed="true"></property>

		<property name="baseline" display="Baseline Value" type="java.lang.Double" editable="true" displayed="true">10</property>
		<property name="jiggle" display="Max Jiggle Factor" type="java.lang.Double" editable="true" displayed="true">2</property>


		<producer id="value" datatype="power" name="User Value" object='$userinput' />


		<docking to="$wobulator" from="$xerox/properties[name='input']"/>
		<docking to="$userinput/properties[name='lastValue']" from="$xerox/properties[name='output']"/>

		<varbinding vars="name">
			<property>$wobulator/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="notes">
			<property>$wobulator/properties[name="notes"]</property>
			<value>notes</value>
		</varbinding>
		<varbinding vars="baseline">
			<property>$wobulator/properties[name="baseline"]</property>
			<value>baseline</value>
		</varbinding>
		<varbinding vars="jiggle">
			<property>$wobulator/properties[name="jiggle"]</property>
			<value>jiggle</value>
		</varbinding>
	</component>

</componentlib>
