<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
		name="ITS A TRAP"
		version="&componentlib.version;"
		cversion="&componentlib.cversion;"
		oversion="&componentlib.oversion;"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
		>


	<component type="ackbar-test" classes="placeable" grouppath="Test" filters="Environmentals">
		<display>
			<description>Admirial Ackbar knows when something is a trap.  This Component goes in a Room and a WSN Data Source.</description>
			<name>Admiral Ackbar</name>
			<shape>ackbar</shape>
		</display>

		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryOperated"><value>0</value></property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status"><value>0</value></property>
			<property name="platformId"><value>80</value></property>
			<property name="platformName"><value>Damper Controller</value></property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>belimo</value></property>
					<property name="channel"><value>0</value></property>
					<property name="min"><value>1.8</value></property>
					<property name="max"><value>3.7</value></property>
					<property name="type"><value>5</value></property>
					<property name="dataclass"><value>210</value></property>
					<property name="enabled"><value>1</value></property>
					<property name="mode"><value>instant</value></property>
					<property name="interval"><value>0</value></property>
					<property name="z"><value>0</value></property>
					<property name="status"><value>1</value></property>
					<property name="lastValue"><value>-5000.0</value></property>
				</object>
			</property>
			<property name="actuators">
				<object type="wsnactuator">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>belimo</value></property>
					<property name="actuatorID"><value>0</value></property>
					<property name="positionTolerance"><value>3</value></property>
				</object>
			</property>
		</object>

		<macids value="mac_id" />

		<docking from='$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="sensor"]' to='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]'/>

		<producer id="strategy" datatype="control_strategy_airflow" name="Driver" object='$node' />

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Admiral Ackbar</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="false" displayed="true">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Admiral Ackbar</property>

		<property name="direction" display="direction" type="java.lang.Integer" editable="true" displayed="true"
				  valueChoices="Clockwise:1,Counterclockwise:2">1</property>
		<property name="failsafeSetPoint" display="failsafeSetPoint" type="java.lang.Integer" editable="true" displayed="true">100</property>
		<property name="minimumSetting" display="minimumSetting" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="maximumSetting" display="maximumSetting" type="java.lang.Integer" editable="true" displayed="true">100</property>
		<property name="travelTime" display="travelTime" type="java.lang.Double" editable="true" displayed="true">95</property>

		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

		<varbinding vars="x">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$node/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$node/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<value>location</value>
		</varbinding>

		<varbinding vars="direction"> <property>$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="direction"]</property><value>direction</value></varbinding>
		<varbinding vars="failsafeSetPoint"> <property>$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="failsafeSetPoint"]</property><value>failsafeSetPoint</value></varbinding>
		<varbinding vars="minimumSetting"> <property>$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="minimumSetting"]</property><value>minimumSetting</value></varbinding>
		<varbinding vars="maximumSetting"> <property>$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="maximumSetting"]</property><value>maximumSetting</value></varbinding>
		<varbinding vars="travelTime"> <property>$node/properties[name="actuators"]/children[properties[name="actuatorID"][value="0"]]/properties[name="travelTime"]</property><value>travelTime</value></varbinding>
	</component>

</componentlib>