<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
		name='PUE Equipment Status Converters'
		version="&componentlib.version;"
		cversion="&componentlib.cversion;"
		oversion="&componentlib.oversion;"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
		>

	<component type='number-to-status' classes='placeable' grouppath="Calculations;Equipment Status Calculations" filters="Calculations">
		<display>
			<description>Converts a numeric value to a status value.  If the returned value is present in the whitelist, this component returns a status of ON, otherwise it returns a status of OFF.  This Component goes in a Calculation Group and requires an SNMP Data Source.</description>
			<name>Number to Status</name>
			<shape>pue_diamond_numbertostatus</shape>
		</display>

		<object type="producer_status" as="producer_status">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Number to Status Converter</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Number to Status Converter</property>
		<property name='notes' display='Notes' type='DeploymentLab.MultiLineContents' editable='true' displayed='true' dimension=''></property>
		<property name="whitelist" display="Whitelist" type="java.lang.String" editable="true" displayed="true"></property>

		<consumer id='value' datatype='power' name='Value Input' collection='false' property='$producer_status/properties[name="input"]' required='true' />

		<producer id="status" datatype="status" name="Status Value" object='$producer_status' />

		<varbinding vars="name">
			<property>$producer_status/properties[name="name"]</property>
			<value>name</value>
		</varbinding>

		<varbinding vars="whitelist">
			<property>$producer_status/properties[name="whitelist"]</property>
			<value>whitelist</value>
		</varbinding>
	</component>

	<component type='status-to-number' classes='placeable' grouppath="Calculations;Equipment Status Calculations" filters="Calculations">
		<display>
			<description>Converts a status value to a numeric value.  If the returned value is present in the whitelist, this component returns a status of ON, otherwise it returns a status of OFF.  This Component goes in a Calculation Group and requires an SNMP Data Source.</description>
			<name>Status to Number</name>
			<shape>pue_diamond_statustonumber</shape>
		</display>

		<object type="producer_power" as="producer_power">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Status to Number Converter</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Status to Number Converter</property>
		<property name='notes' display='Notes' type='DeploymentLab.MultiLineContents' editable='true' displayed='true' dimension=''></property>

		<consumer id='status' datatype='status' name='Status Input' collection='false' property='$producer_power/properties[name="input"]' required='true' />
		<producer id="power" datatype="power" name="Numeric Value" object='$producer_power' />

		<varbinding vars="name">
			<property>$producer_power/properties[name="name"]</property>
			<value>name</value>
		</varbinding>

	</component>

</componentlib>
