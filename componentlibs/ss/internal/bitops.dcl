<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib name="bitwise ops" version="&componentlib.version;" oversion="&componentlib.oversion;" cversion="&componentlib.cversion;">


	<component type="math_operation_bit_or" classes="placeable" grouppath="Calculations;Power Calculations" filters="Calculations">
		<display>
			<description>Bitwise OR.  This Component goes in a Calculation Group and does not require a Data Source.</description>
			<name>Bitwise OR</name>
			<shape>pue_diamond_bitand</shape>
		</display>

		<object type="math_operation" as="math">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
			<property name="opM"><value>or</value></property>
			<property name="opA"><value>solo</value></property>
			<property name="opB"><value>solo</value></property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Bitwise OR Operation</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Bitwise OR Operation</property>
		<property name="notes" display="Notes" type="DeploymentLab.MultiLineContents" editable="true" displayed="true"></property>

		<consumer id="inputA" datatype="power" name="Left Side" collection="false" property='$math/properties[name="inputA"]' required="true" />
		<consumer id="inputB" datatype="power" name="Right Side" collection="false" property='$math/properties[name="inputB"]' required="true" />

		<producer id="value" datatype="power" name="Result" object='$math' />

		<varbinding vars="name">
			<property>$math/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="notes">
			<property>$math/properties[name="notes"]</property>
			<value>notes</value>
		</varbinding>
	</component>

	<component type="math_operation_bit_xor" classes="placeable" grouppath="Calculations;Power Calculations" filters="Calculations">
		<display>
			<description>Bitwise XOR.  This Component goes in a Calculation Group and does not require a Data Source.</description>
			<name>Bitwise XOR</name>
			<shape>pue_diamond_bitxor</shape>
		</display>

		<object type="math_operation" as="math">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
			<property name="opM"><value>xor</value></property>
			<property name="opA"><value>solo</value></property>
			<property name="opB"><value>solo</value></property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Bitwise XOR Operation</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Bitwise XOR Operation</property>
		<property name="notes" display="Notes" type="DeploymentLab.MultiLineContents" editable="true" displayed="true"></property>

		<consumer id="inputA" datatype="power" name="Left Side" collection="false" property='$math/properties[name="inputA"]' required="true" />
		<consumer id="inputB" datatype="power" name="Right Side" collection="false" property='$math/properties[name="inputB"]' required="true" />

		<producer id="value" datatype="power" name="Result" object='$math' />

		<varbinding vars="name">
			<property>$math/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="notes">
			<property>$math/properties[name="notes"]</property>
			<value>notes</value>
		</varbinding>
	</component>

	<component type="math_operation_bit_and" classes="placeable" grouppath="Calculations;Power Calculations" filters="Calculations">
		<display>
			<description>Bitwise AND.  This Component goes in a Calculation Group and does not require a Data Source.</description>
			<name>Bitwise AND</name>
			<shape>pue_diamond_bitand</shape>
		</display>

		<object type="math_operation" as="math">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
			<property name="opM"><value>AND</value></property>
			<property name="opA"><value>solo</value></property>
			<property name="opB"><value>solo</value></property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Bitwise AND Operation</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Bitwise AND Operation</property>
		<property name="notes" display="Notes" type="DeploymentLab.MultiLineContents" editable="true" displayed="true"></property>

		<consumer id="inputA" datatype="power" name="Left Side" collection="false" property='$math/properties[name="inputA"]' required="true" />
		<consumer id="inputB" datatype="power" name="Right Side" collection="false" property='$math/properties[name="inputB"]' required="true" />

		<producer id="value" datatype="power" name="Result" object='$math' />

		<varbinding vars="name">
			<property>$math/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="notes">
			<property>$math/properties[name="notes"]</property>
			<value>notes</value>
		</varbinding>
	</component>


	<component type="math_operation_bit_shiftl" classes="placeable" grouppath="Calculations;Power Calculations" filters="Calculations">
		<display>
			<description>Bitwise Shift Left.  This Component goes in a Calculation Group and does not require a Data Source.</description>
			<name>Bitwise Shift Left</name>
			<shape>pue_diamond_shiftl</shape>
		</display>

		<object type="math_operation" as="math">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
			<property name="opM"><value>shiftl</value></property>
			<property name="opA"><value>solo</value></property>
			<property name="opB"><value>solo</value></property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Bitwise Shift Left Operation</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Bitwise Shift Left Operation</property>
		<property name="notes" display="Notes" type="DeploymentLab.MultiLineContents" editable="true" displayed="true"></property>

		<consumer id="inputA" datatype="power" name="Left Side" collection="false" property='$math/properties[name="inputA"]' required="true" />
		<consumer id="inputB" datatype="power" name="Right Side" collection="false" property='$math/properties[name="inputB"]' required="true" />

		<producer id="value" datatype="power" name="Result" object='$math' />

		<varbinding vars="name">
			<property>$math/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="notes">
			<property>$math/properties[name="notes"]</property>
			<value>notes</value>
		</varbinding>
	</component>

	<component type="math_operation_bit_shiftr" classes="placeable" grouppath="Calculations;Power Calculations" filters="Calculations">
		<display>
			<description>Bitwise Shift Right.  This Component goes in a Calculation Group and does not require a Data Source.</description>
			<name>Bitwise Shift Right</name>
			<shape>pue_diamond_shiftr</shape>
		</display>

		<object type="math_operation" as="math">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
			<property name="opM"><value>shiftr</value></property>
			<property name="opA"><value>solo</value></property>
			<property name="opB"><value>solo</value></property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Bitwise Shift Right Operation</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Bitwise Shift Right Operation</property>
		<property name="notes" display="Notes" type="DeploymentLab.MultiLineContents" editable="true" displayed="true"></property>

		<consumer id="inputA" datatype="power" name="Left Side" collection="false" property='$math/properties[name="inputA"]' required="true" />
		<consumer id="inputB" datatype="power" name="Right Side" collection="false" property='$math/properties[name="inputB"]' required="true" />

		<producer id="value" datatype="power" name="Result" object='$math' />

		<varbinding vars="name">
			<property>$math/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="notes">
			<property>$math/properties[name="notes"]</property>
			<value>notes</value>
		</varbinding>
	</component>

	<component type="math_operation_bit_extract" classes="placeable" grouppath="Calculations;Power Calculations" filters="Calculations">
		<display>
			<description>Extract a single bit from a value.  This Component goes in a Calculation Group and does not require a Data Source.</description>
			<name>Bit Extract</name>
			<shape>pue_diamond_index</shape>
		</display>

		<object type="math_operation" as="math">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
			<property name="opM"><value>bitextract</value></property>
			<property name="opA"><value>solo</value></property>
			<property name="opB"><value>solo</value></property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Bit Extract Operation</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Bit Extract Operation</property>
		<property name="notes" display="Notes" type="DeploymentLab.MultiLineContents" editable="true" displayed="true"></property>

		<consumer id="inputA" datatype="power" name="Value" collection="false" property='$math/properties[name="inputA"]' required="true" />
		<consumer id="inputB" datatype="power" name="Bit Index" collection="false" property='$math/properties[name="inputB"]' required="true" />

		<producer id="value" datatype="power" name="Result" object='$math' />

		<varbinding vars="name">
			<property>$math/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="notes">
			<property>$math/properties[name="notes"]</property>
			<value>notes</value>
		</varbinding>
	</component>

	<component type="math_operation_bit_not" classes="placeable" grouppath="Calculations;Power Calculations" filters="Calculations">
		<display>
			<description>Bitwise NOT of an input value.  This Component goes in a Calculation Group and does not require a Data Source.</description>
			<name>Bitwise NOT</name>
			<shape>pue_diamond_bitnot</shape>
		</display>
		<object type="math_single" as="math">
			<dlid>DLID:-1</dlid>
			<property name="status"><value>1</value></property>
			<property name="op"><value>not</value></property>
		</object>
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Bitwise NOT Operation</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="false" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Bitwise NOT Operation</property>
		<property name="notes" display="Notes" type="DeploymentLab.MultiLineContents" editable="true" displayed="true"></property>
		<consumer id="input" datatype="power" name="Input" collection="false" property='$math/properties[name="input"]' required="true" />
		<producer id="value" datatype="power" name="Natural Logarithm" object='$math' />
		<varbinding vars="name">
			<property>$math/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="notes">
			<property>$math/properties[name="notes"]</property>
			<value>notes</value>
		</varbinding>
	</component>



</componentlib>