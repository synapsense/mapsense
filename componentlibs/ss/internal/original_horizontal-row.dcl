<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
	name="Row-level monitoring"
	version="&componentlib.version;"
	cversion="&componentlib.cversion;"
	oversion="&componentlib.oversion;"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
	>

	<component type="horizontal-row-thermanode" classes="placeable,rotatable" grouppath="Custom" filters="Environmentals">
		<display>
			<description>Array of sensors used to monitor the horizontal temperature distribution at various points in a row of racks.  There are 6 external thermistors, each displaced from the center where the ThermaNode is located.</description>
			<name>Horizontal Row (TNode)</name>
			<shape>triangle_orange</shape>
		</display>
		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity">
				<value>5600.0</value>
			</property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status">
				<value>0</value>
			</property>
			<property name="platformId">
				<value>11</value>
			</property>
			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>internal temp</value>
					</property>
					<property name="channel">
						<value>0</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="aMin">
						<value>59.0</value>
					</property>
					<property name="aMax">
						<value>90.0</value>
					</property>
					<property name="rMin">
						<value>64.4</value>
					</property>
					<property name="rMax">
						<value>80.6</value>
					</property>
					<property name="type">
						<value>1</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>humidity</value>
					</property>
					<property name="channel">
						<value>1</value>
					</property>
					<property name="min">
						<value>10.0</value>
					</property>
					<property name="max">
						<value>90.0</value>
					</property>
					<property name="aMin">
						<value>20.0</value>
					</property>
					<property name="aMax">
						<value>80.0</value>
					</property>
					<property name="rMin">
						<value>40.0</value>
					</property>
					<property name="rMax">
						<value>55.0</value>
					</property>
					<property name="type">
						<value>2</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>201</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>battery</value>
					</property>
					<property name="channel">
						<value>2</value>
					</property>
					<property name="min">
						<value>1.8</value>
					</property>
					<property name="max">
						<value>3.7</value>
					</property>
					<property name="aMin">
						<value>2.5</value>
					</property>
					<property name="aMax">
						<value>3.7</value>
					</property>
					<property name="rMin">
						<value>2.7</value>
					</property>
					<property name="rMax">
						<value>3.7</value>
					</property>
					<property name="type">
						<value>5</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>210</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>channel 3 temp</value>
					</property>
					<property name="channel">
						<value>3</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold"><value/></property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>channel 4 temp</value>
					</property>
					<property name="channel">
						<value>4</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>channel 5 temp</value>
					</property>
					<property name="channel">
						<value>5</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>channel 6 temp</value>
					</property>
					<property name="channel">
						<value>6</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>channel 7 temp</value>
					</property>
					<property name="channel">
						<value>7</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>channel 8 temp</value>
					</property>
					<property name="channel">
						<value>8</value>
					</property>
					<property name="min">
						<value>40.0</value>
					</property>
					<property name="max">
						<value>100.0</value>
					</property>
					<property name="type">
						<value>27</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>200</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>instant</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>
					<property name="smartSendThreshold">
						<value/>
					</property>
					<property name="subSamples"><value>30</value></property>
				</object>
			</property>
			<property name="batteryOperated">
				<value>1</value>
			</property>
			<property name="platformName">
				<value>ThermaNode</value>
			</property>
		</object>
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true" dimension="">Horizontal Row (TNode)</property>
		<property name="rotation" display="Rotation" type="java.lang.Double" editable="true" displayed="true" dimension="">0</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true" dimension="">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true" dimension="">5</property>
		<property name="depth" display="Depth" type="java.lang.Double" editable="false" displayed="false" dimension="">30.0</property>
		<property name="width" display="Width" type="java.lang.Double" editable="false" displayed="false" dimension="">30.0</property>
		<property name="li_layer" display="LI Layer" type="java.lang.Integer" editable="true" displayed="true" dimension="" valueChoices="None:0,Top:131072,Middle:16384,Bottom:2048,Subfloor:256">0</property>
		<property name="min_allow_t" display="Min Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">59.0</property>
		<property name="max_allow_t" display="Max Allowed Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">90.0</property>
		<property name="min_recommend_t" display="Min Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">64.4</property>
		<property name="max_recommend_t" display="Max Recommended Temp" type="java.lang.Double" editable="true" displayed="true" dimension="temperature">80.6</property>
		<property name="c3_offset" display="Ch3 Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">216.0</property>
		<property name="c4_offset" display="Ch4 Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">144.0</property>
		<property name="c5_offset" display="Ch5 Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">72.0</property>
		<property name="c6_offset" display="Ch6 Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">72.0</property>
		<property name="c7_offset" display="Ch7 Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">144.0</property>
		<property name="c8_offset" display="Ch8 Offset" type="java.lang.Double" editable="true" displayed="true" dimension="distance">216.0</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true" dimension=""/>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true" dimension="">Horizontal Row (TNode)</property>
		<property name="smartsend_delta" display="Temperature Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">5</property>
		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

        <!--
         todo: This was not on the COIN Mgmt conversion list but has temp producers. These are breaking unit tests so they are being disabled for now.
         If this component is used, we will need to figure out if it should have had the COIN Mgmt conversion done on it.
		<producer id="temp3" datatype="temperature" name="Channel 3 Temperature" object="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;3&quot;]]"/>
		<producer id="temp4" datatype="temperature" name="Channel 4 Temperature" object="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;4&quot;]]"/>
		<producer id="temp5" datatype="temperature" name="Channel 5 Temperature" object="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;5&quot;]]"/>
		<producer id="temp6" datatype="temperature" name="Channel 6 Temperature" object="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;6&quot;]]"/>
		<producer id="temp7" datatype="temperature" name="Channel 7 Temperature" object="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;7&quot;]]"/>
		<producer id="temp8" datatype="temperature" name="Channel 8 Temperature" object="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;8&quot;]]"/>
		-->
		<varbinding vars="li_layer">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="z"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="z"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="z"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="z"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="z"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="z"]</property>
			<value>li_layer</value>
		</varbinding>
		<varbinding vars="min_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="aMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="aMin"]</property>
			<value>min_allow_t</value>
		</varbinding>
		<varbinding vars="max_allow_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="aMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="aMax"]</property>
			<value>max_allow_t</value>
		</varbinding>
		<varbinding vars="min_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="rMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="rMin"]</property>
			<value>min_recommend_t</value>
		</varbinding>
		<varbinding vars="max_recommend_t">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="rMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="rMax"]</property>
			<value>max_recommend_t</value>
		</varbinding>
		<varbinding vars="x,c3_offset,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<value>x - (c3_offset * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,c3_offset,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<value>y - (c3_offset * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,c4_offset,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
			<value>x - (c4_offset * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,c4_offset,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
			<value>y - (c4_offset * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,c5_offset,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="x"]</property>
			<value>x - (c5_offset * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,c5_offset,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="y"]</property>
			<value>y - (c5_offset * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,c6_offset,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="x"]</property>
			<value>x + (c6_offset * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,c6_offset,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="y"]</property>
			<value>y + (c6_offset * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,c7_offset,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="x"]</property>
			<value>x + (c7_offset * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,c7_offset,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="y"]</property>
			<value>y + (c7_offset * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="x,c8_offset,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="x"]</property>
			<value>x + (c8_offset * Math.sin(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="y,c8_offset,rotation">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="y"]</property>
			<value>y + (c8_offset * Math.cos(Math.toRadians(-rotation)))</value>
		</varbinding>
		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="x">
			<property>$node/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$node/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<value>location</value>
		</varbinding>

		<varbinding vars="smartsend_delta">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="smartSendThreshold"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="smartSendThreshold"]</property>
			<value>(int)(smartsend_delta)</value>
		</varbinding>


		<macids value="mac_id"/>
	</component>

</componentlib>
