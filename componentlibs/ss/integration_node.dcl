<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib name="Constellation II Integration edition" version="&componentlib.version;" oversion="&componentlib.oversion;" cversion="&componentlib.cversion;">
	
	<component type="generic-constellation2" classes="placeable" grouppath="Environmentals;Wireless" filters="Environmentals">
		<display>
			<description>A generic, configurable Constellation 2 node.</description>
			<name>Integration Node</name>
			<shape>star</shape>
		</display>
		<object type="wsnnode" as="node">
			<dlid>DLID:-1</dlid>
			<property name="batteryCapacity"> <value>5600.0</value> </property>
			<property name="batteryStatus"><value>0</value></property>
			<property name="status"> <value>0</value> </property>
			<property name="platformId"> <value>15</value> </property>
			<property name="batteryOperated"> <value>1</value> </property>
			<property name="platformName"> <value>Constellation 2</value> </property>

			<property name="sensepoints">
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"> <value>battery</value> </property>
					<property name="channel"> <value>2</value> </property>
					<property name="min"> <value>1.8</value> </property>
					<property name="max"> <value>3.7</value> </property>
					<property name="aMin"> <value>2.5</value> </property>
					<property name="aMax"> <value>3.7</value> </property>
					<property name="rMin"> <value>2.7</value> </property>
					<property name="rMax"> <value>3.7</value> </property>
					<property name="type"> <value>5</value> </property>
					<property name="z"> <value>0</value> </property>
					<property name="status"> <value>1</value> </property>
					<property name="lastValue"> <value>-5000.0</value> </property>
					<property name="dataclass">	<value>210</value> </property>
					<property name="enabled"> <value>1</value> </property>
					<property name="mode"> <value>averaging</value> </property>
					<property name="interval"> <value>0</value> </property>
				</object>

				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name"> <value>Ch3 Variable 4-20mA input</value> </property>
					<property name="channel"> <value>3</value> </property>
					<property name="min"> <value>0.0</value> </property>
					<property name="max"> <value>200.0</value> </property>
					<property name="type"> <value>34</value> </property>
					<property name="z"> <value>0</value> </property>
					<property name="status"> <value>1</value> </property>
					<property name="lastValue"> <value>-5000.0</value> </property>
					<property name="dataclass"> <value>999</value> </property>
					<property name="enabled"> <value>1</value> </property>
					<property name="mode"> <value>averaging</value> </property>
					<property name="interval"> <value>0</value> </property>
				</object>

				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>Ch4 Variable 4-20mA input</value>
					</property>
					<property name="channel">
						<value>4</value>
					</property>
					<property name="min">
						<value>0.0</value>
					</property>
					<property name="max">
						<value>200.0</value>
					</property>
					<property name="type">
						<value>34</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>999</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>averaging</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>


				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>Ch5 Variable 4-20mA input</value>
					</property>
					<property name="channel">
						<value>5</value>
					</property>
					<property name="min">
						<value>0.0</value>
					</property>
					<property name="max">
						<value>200.0</value>
					</property>
					<property name="type">
						<value>34</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>999</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>averaging</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>


				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>Ch6 0-10V input</value>
					</property>
					<property name="channel">
						<value>6</value>
					</property>
					<property name="min">
						<value>0.0</value>
					</property>
					<property name="max">
						<value>1.0</value>
					</property>
					<property name="type">
						<value>9</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="scaleMin">
						<value>0</value>
					</property>
					<property name="scaleMax">
						<value>1</value>
					</property>
					<property name="dataclass">
						<value>211</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>edge_detect</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>


				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>Ch7 Variable 4-20mA input</value>
					</property>
					<property name="channel">
						<value>7</value>
					</property>
					<property name="min">
						<value>0.0</value>
					</property>
					<property name="max">
						<value>200.0</value>
					</property>
					<property name="type">
						<value>34</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="dataclass">
						<value>999</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>averaging</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>


				</object>
				<object type="wsnsensor">
					<dlid>DLID:-1</dlid>
					<property name="name">
						<value>Ch8 0-10V input</value>
					</property>
					<property name="channel">
						<value>8</value>
					</property>
					<property name="min">
						<value>0.0</value>
					</property>
					<property name="max">
						<value>1.0</value>
					</property>
					<property name="type">
						<value>9</value>
					</property>
					<property name="z">
						<value>0</value>
					</property>
					<property name="status">
						<value>1</value>
					</property>
					<property name="lastValue">
						<value>-5000.0</value>
					</property>
					<property name="scaleMin">
						<value>0</value>
					</property>
					<property name="scaleMax">
						<value>1</value>
					</property>
					<property name="dataclass">
						<value>211</value>
					</property>
					<property name="enabled">
						<value>1</value>
					</property>
					<property name="mode">
						<value>edge_detect</value>
					</property>
					<property name="interval">
						<value>0</value>
					</property>


				</object>
			</property>

		</object>
		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true" dimension="">Integration Node</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true" dimension="">0</property>
		<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true" dimension="">5</property>
		<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true" dimension=""/>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true" dimension="">Integration Node</property>

		<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

		<property name="c3_enable" display="Ch3 Enable" type="java.lang.Boolean" editable="true" displayed="true" dimension="">false</property>
		<property name="c3_type" display="Ch3 Data Type" type="java.lang.Integer" editable="true" displayed="true" dimension="" valueChoices="Energy (kWh):20,Current:30,Liquid Flow:31,Enthalpy:32,Temperature:33,4-20mA Input:34">34</property>
		<property name="c3_4ma" display="Ch3 4mA Value" type="java.lang.Double" editable="true" displayed="true" dimension="">0.0</property>
		<property name="c3_20ma" display="Ch3 20mA Value" type="java.lang.Double" editable="true" displayed="true" dimension="">100.0</property>

		<property name="c4_enable" display="Ch4 Enable" type="java.lang.Boolean" editable="true" displayed="true" dimension="">false</property>
		<property name="c4_type" display="Ch4 Data Type" type="java.lang.Integer" editable="true" displayed="true" dimension="" valueChoices="Energy (kWh):20,Current:30,Liquid Flow:31,Enthalpy:32,Temperature:33,4-20mA Input:34">34</property>
		<property name="c4_4ma" display="Ch4 4mA Value" type="java.lang.Double" editable="true" displayed="true" dimension="">0.0</property>
		<property name="c4_20ma" display="Ch4 20mA Value" type="java.lang.Double" editable="true" displayed="true" dimension="">100.0</property>

		<property name="c5_enable" display="Ch5 Enable" type="java.lang.Boolean" editable="true" displayed="true" dimension="">false</property>
		<property name="c5_type" display="Ch5 Data Type" type="java.lang.Integer" editable="true" displayed="true" dimension="" valueChoices="Energy (kWh):20,Current:30,Liquid Flow:31,Enthalpy:32,Temperature:33,4-20mA Input:34">34</property>
		<property name="c5_4ma" display="Ch5 4mA Value" type="java.lang.Double" editable="true" displayed="true" dimension="">0.0</property>
		<property name="c5_20ma" display="Ch5 20mA Value" type="java.lang.Double" editable="true" displayed="true" dimension="">100.0</property>

		<property name="c7_enable" display="Ch7 Enable" type="java.lang.Boolean" editable="true" displayed="true" dimension="">false</property>
		<property name="c7_type" display="Ch7 Data Type" type="java.lang.Integer" editable="true" displayed="true" dimension="" valueChoices="Energy (kWh):20,Current:30,Liquid Flow:31,Enthalpy:32,Temperature:33,4-20mA Input:34">34</property>
		<property name="c7_4ma" display="Ch7 4mA Value" type="java.lang.Double" editable="true" displayed="true" dimension="">0.0</property>
		<property name="c7_20ma" display="Ch7 20mA Value" type="java.lang.Double" editable="true" displayed="true" dimension="">100.0</property>

		<property name="c6_enable" display="Ch6 Enable" type="java.lang.Boolean" editable="true" displayed="true" dimension="">false</property>
		<property name="c6_type" display="Ch6 Data Type" type="java.lang.Integer" editable="true" displayed="true" dimension="" valueChoices="Open/Closed:9,Liquid Present:12,Energy (kWh):20,0-10V Contactor:35,Equipment State:100000">35</property>
		<property name="c6_pulseCount" display="Ch6 Pulse Count/Edge Detect" type="java.lang.Integer" editable="true" displayed="true" dimension="" valueChoices="Pulse Count:1,Edge Detect:0">1</property>
		<property name="c6_pulseSendTime" display="Ch6 Pulse Send Interval" type="java.lang.Integer" editable="true" displayed="true" dimension="">300</property>
		<property name="c6_scale" display="Ch6 Scale" type="java.lang.Double" editable="true" displayed="true" dimension="">1</property>
		<property name="c6_pulseWidth" display="Ch6 Pulse Width" type="java.lang.Integer" editable="true" displayed="true" dimension="">2000</property>

		<property name="c8_enable" display="Ch8 Enable" type="java.lang.Boolean" editable="true" displayed="true" dimension="">false</property>
		<property name="c8_type" display="Ch8 Data Type" type="java.lang.Integer" editable="true" displayed="true" dimension="" valueChoices="Open/Closed:9,Liquid Present:12,Energy (kWh):20,0-10V Contactor:35,Equipment State:100000">35</property>
		<property name="c8_pulseCount" display="Ch8 Pulse Count/Edge Detect" type="java.lang.Integer" editable="true" displayed="true" dimension="" valueChoices="Pulse Count:1,Edge Detect:0">1</property>
		<property name="c8_pulseSendTime" display="Ch8 Pulse Send Interval" type="java.lang.Integer" editable="true" displayed="true" dimension="">300</property>
		<property name="c8_scale" display="Ch8 Scale" type="java.lang.Double" editable="true" displayed="true" dimension="">1</property>
		<property name="c8_pulseWidth" display="Ch8 Pulse Width" type="java.lang.Integer" editable="true" displayed="true" dimension="">2000</property>

		<producer id="channel3" datatype="power" name="Channel 3" object="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;3&quot;]]"/>
		<producer id="channel4" datatype="power" name="Channel 4" object="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;4&quot;]]"/>
		<producer id="channel5" datatype="power" name="Channel 5" object="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;5&quot;]]"/>
		<producer id="channel6" datatype="power" name="Channel 6" object="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;6&quot;]]"/>
		<producer id="channel7" datatype="power" name="Channel 7" object="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;7&quot;]]"/>
		<producer id="channel8" datatype="power" name="Channel 8" object="$node/properties[name=&quot;sensepoints&quot;]/children[properties[name=&quot;channel&quot;][value=&quot;8&quot;]]"/>

		<varbinding vars="name">
			<property>$node/properties[name="name"]</property>
			<value>'Node ' + name</value>
		</varbinding>
		<varbinding vars="x">
			<property>$node/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="x"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$node/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="y"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="mac"]</property>
			<value>Long.parseLong(mac_id,16)</value>
		</varbinding>
		<varbinding vars="mac_id">
			<property>$node/properties[name="id"]</property>
			<value>lid.generateLogicalId(mac_id , self)</value>
		</varbinding>
		<varbinding vars="sample_interval">
			<property>$node/properties[name="period"]</property>
			<value>sample_interval + ' min'</value>
		</varbinding>
		<varbinding vars="location">
			<property>$node/properties[name="location"]</property>
			<value>location</value>
		</varbinding>

		<varbinding vars="c3_enable">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="enabled"]</property>
			<value>if(c3_enable) return 1; else return 0;</value>
		</varbinding>
		<varbinding vars="c3_type">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="type"]</property>
			<value>c3_type</value>
		</varbinding>

		<varbinding vars="c3_type">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="dataclass"]</property>
			<value>if (c3_type==20) return 207; else if (c3_type==30) return 206; else if (c3_type==31) return 209; else if (c3_type==32) return 207; else if (c3_type==33) return 200; else if (c3_type==34) return 999;</value>
		</varbinding>

		<varbinding vars="c3_4ma">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="scaleMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="min"]</property>
			<value>c3_4ma</value>
		</varbinding>
		<varbinding vars="c3_20ma">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="scaleMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="max"]</property>
			<value>c3_20ma</value>
		</varbinding>


		<varbinding vars="c4_enable">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="enabled"]</property>
			<value>if(c4_enable) return 1; else return 0;</value>
		</varbinding>

		<varbinding vars="c4_type">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="type"]</property>
			<value>c4_type</value>
		</varbinding>
		<varbinding vars="c4_type">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="dataclass"]</property>
			<value>if (c4_type==20) return 207; else if (c4_type==30) return 206; else if (c4_type==31) return 209; else if (c4_type==32) return 207; else if (c4_type==33) return 200; else if (c4_type==34) return 999;</value>
		</varbinding>

		<varbinding vars="c4_4ma">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="scaleMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="min"]</property>
			<value>c4_4ma</value>
		</varbinding>
		<varbinding vars="c4_20ma">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="scaleMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="max"]</property>
			<value>c4_20ma</value>
		</varbinding>


		<varbinding vars="c5_enable">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="enabled"]</property>
			<value>if(c5_enable) return 1; else return 0;</value>
		</varbinding>

		<varbinding vars="c5_type">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="type"]</property>
			<value>c5_type</value>
		</varbinding>
		<varbinding vars="c5_type">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="dataclass"]</property>
			<value>if (c5_type==20) return 207; else if (c5_type==30) return 206; else if (c5_type==31) return 209; else if (c5_type==32) return 207; else if (c5_type==33) return 200; else if (c5_type==34) return 999;</value>
		</varbinding>

		<varbinding vars="c5_4ma">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="scaleMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="min"]</property>
			<value>c5_4ma</value>
		</varbinding>
		<varbinding vars="c5_20ma">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="scaleMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="max"]</property>
			<value>c5_20ma</value>
		</varbinding>


		<varbinding vars="c7_enable">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="enabled"]</property>
			<value>if(c7_enable) return 1; else return 0;</value>
		</varbinding>
		<varbinding vars="c7_type">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="type"]</property>
			<value>c7_type</value>
		</varbinding>
		<varbinding vars="c7_type">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="dataclass"]</property>
			<value>if (c7_type==20) return 207; else if (c7_type==30) return 206; else if (c7_type==31) return 209; else if (c7_type==32) return 207; else if (c7_type==33) return 200; else if (c7_type==34) return 999;</value>
		</varbinding>

		<varbinding vars="c7_4ma">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="scaleMin"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="min"]</property>
			<value>c7_4ma</value>
		</varbinding>
		<varbinding vars="c7_20ma">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="scaleMax"]</property>
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="max"]</property>
			<value>c7_20ma</value>
		</varbinding>


		<varbinding vars="c6_type">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="type"]</property>
			<value>c6_type</value>
		</varbinding>
		<varbinding vars="c6_type">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="dataclass"]</property>
			<value>if (c6_type==9) return 211; else if (c6_type==12) return 213; else if (c6_type==20) return 207; else if (c6_type==35) return 211; else if (c6_type==100000) return 214;</value>
		</varbinding>
		<varbinding vars="c6_enable">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="enabled"]</property>
			<value>if(c6_enable) return 1; else return 0;</value>
		</varbinding>
		<varbinding vars="c6_pulseCount">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="mode"]</property>
			<value>if (c6_pulseCount == 1) return "state_change_count"; else return "edge_detect";</value>
		</varbinding>
		<varbinding vars="c6_pulseSendTime">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="interval"]</property>
			<value>c6_pulseSendTime</value>
		</varbinding>
		<varbinding vars="c6_scale">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="scaleMax"]</property>
			<value>c6_scale</value>
		</varbinding>
		<varbinding vars="c6_pulseWidth">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="pulseWidth"]</property>
			<value>c6_pulseWidth</value>
		</varbinding>


		<varbinding vars="c8_type">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="type"]</property>
			<value>c8_type</value>
		</varbinding>
		<varbinding vars="c8_type">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="dataclass"]</property>
			<value>if (c8_type==9) return 211; else if (c8_type==12) return 213; else if (c8_type==20) return 207; else if (c8_type==35) return 211; else if (c8_type==100000) return 214;</value>
		</varbinding>
		<varbinding vars="c8_enable">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="enabled"]</property>
			<value>if(c8_enable) return 1; else return 0;</value>
		</varbinding>
		<varbinding vars="c8_pulseCount">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="mode"]</property>
			<value>if (c8_pulseCount == 1) return "state_change_count"; else return "edge_detect";</value>
		</varbinding>
		<varbinding vars="c8_pulseSendTime">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="interval"]</property>
			<value>c8_pulseSendTime</value>
		</varbinding>
		<varbinding vars="c8_scale">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="scaleMax"]</property>
			<value>c8_scale</value>
		</varbinding>
		<varbinding vars="c8_pulseWidth">
			<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="pulseWidth"]</property>
			<value>c8_pulseWidth</value>
		</varbinding>
		<macids value="mac_id"/>
	</component>

</componentlib>

