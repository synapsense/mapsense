<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
	name="PDI BCMS components"
	version="&componentlib.version;"
	cversion="&componentlib.cversion;"
	oversion="&componentlib.oversion;"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
	>

	<component type="modbus-pdi-bcms-custom" classes="placeable" grouppath="Power &amp; Energy;Wired;BCMS" filters="Calculations">
		<display>
			<description>PDI BCMS device loaded with SynapSense custom firmware.  Provides Current, kW and Power Factor per circuit.  Requires a Modbus Data Source.</description>
			<name>PDI BCMS Panel</name>
			<shape>bcms_custom</shape>
		</display>

		<object type="modbusdevice" as="pdi">
			<dlid>DLID:-1</dlid>
			<property name="maxpacketlen"><value>42</value></property>
			<property name="registers">
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 1</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40001</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 2</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40002</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 3</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40003</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 4</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40004</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 5</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40005</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 6</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40006</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 7</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40007</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 8</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40008</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 9</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40009</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 10</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40010</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 11</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40011</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 12</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40012</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 13</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40013</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 14</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40014</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 15</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40015</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 16</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40016</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 17</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40017</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 18</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40018</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 19</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40019</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 20</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40020</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 21</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40021</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 22</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40022</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 23</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40023</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 24</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40024</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 25</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40025</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 26</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40026</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 27</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40027</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 28</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40028</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 29</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40029</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 30</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40030</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 31</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40031</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 32</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40032</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 33</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40033</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 34</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40034</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 35</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40035</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 36</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40036</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 37</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40037</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 38</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40038</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 39</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40039</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 40</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40040</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 41</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40041</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Current Channel 42</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40042</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 1</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40178</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 2</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40179</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 3</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40180</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 4</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40181</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 5</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40182</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 6</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40183</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 7</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40184</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 8</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40185</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 9</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40186</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 10</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40187</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 11</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40188</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 12</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40189</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 13</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40190</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 14</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40191</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 15</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40192</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 16</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40193</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 17</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40194</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 18</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40195</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 19</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40196</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 20</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40197</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 21</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40198</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 22</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40199</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 23</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40200</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 24</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40201</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 25</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40202</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 26</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40203</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 27</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40204</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 28</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40205</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 29</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40206</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 30</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40207</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 31</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40208</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 32</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40209</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 33</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40210</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 34</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40211</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 35</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40212</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 36</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40213</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 37</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40214</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 38</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40215</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 39</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40216</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 40</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40217</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 41</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40218</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Channel 42</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40219</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 1</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40304</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 2</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40305</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 3</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40306</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 4</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40307</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 5</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40308</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 6</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40309</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 7</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40310</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 8</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40311</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 9</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40312</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 10</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40313</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 11</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40314</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 12</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40315</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 13</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40316</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 14</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40317</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 15</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40318</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 16</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40319</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 17</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40320</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 18</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40321</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 19</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40322</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 20</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40323</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 21</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40324</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 22</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40325</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 23</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40326</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 24</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40327</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 25</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40328</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 26</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40329</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 27</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40330</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 28</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40331</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 29</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40332</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 30</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40333</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 31</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40334</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 32</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40335</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 33</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40336</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 34</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40337</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 35</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40338</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 36</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40339</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 37</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40340</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 38</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40341</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 39</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40342</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 40</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40343</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 41</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40344</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Power Factor Channel 42</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40345</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Phase A Current</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40357</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Phase B Current</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40358</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Phase C Current</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40359</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>AB Voltage</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40370</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>BC Voltage</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40371</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>CA Voltage</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40372</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>AN Voltage</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40373</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>BN Voltage</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40374</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>CN Voltage</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40375</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Phase A Real Power</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40388</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Phase B Real Power</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40389</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Phase C Real Power</value></property>
					<property name="scale"><value>10.0</value></property>
					<property name="id"><value>40390</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Phase A Power Factor</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40400</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Phase B Power Factor</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40401</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
				<object type="modbusproperty">
					<dlid>DLID:-1</dlid>
					<property name="name"><value>Phase C Power Factor</value></property>
					<property name="scale"><value>1000.0</value></property>
					<property name="id"><value>40402</value></property>
					<property name="type"><value>UINT16</value></property>
					<property name="msw"><value>0</value></property>
				</object>
			</property>
		</object>

		<object type="panel" as="panel">
			<dlid>DLID:-1</dlid>
			<property name="circuits">
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>1</value></property>
					<property name="name"><value>Circuit 1</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>2</value></property>
					<property name="name"><value>Circuit 2</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>3</value></property>
					<property name="name"><value>Circuit 3</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>4</value></property>
					<property name="name"><value>Circuit 4</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>5</value></property>
					<property name="name"><value>Circuit 5</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>6</value></property>
					<property name="name"><value>Circuit 6</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>7</value></property>
					<property name="name"><value>Circuit 7</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>8</value></property>
					<property name="name"><value>Circuit 8</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>9</value></property>
					<property name="name"><value>Circuit 9</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>10</value></property>
					<property name="name"><value>Circuit 10</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>11</value></property>
					<property name="name"><value>Circuit 11</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>12</value></property>
					<property name="name"><value>Circuit 12</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>13</value></property>
					<property name="name"><value>Circuit 13</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>14</value></property>
					<property name="name"><value>Circuit 14</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>15</value></property>
					<property name="name"><value>Circuit 15</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>16</value></property>
					<property name="name"><value>Circuit 16</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>17</value></property>
					<property name="name"><value>Circuit 17</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>18</value></property>
					<property name="name"><value>Circuit 18</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>19</value></property>
					<property name="name"><value>Circuit 19</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>20</value></property>
					<property name="name"><value>Circuit 20</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>21</value></property>
					<property name="name"><value>Circuit 21</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>22</value></property>
					<property name="name"><value>Circuit 22</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>23</value></property>
					<property name="name"><value>Circuit 23</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>24</value></property>
					<property name="name"><value>Circuit 24</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>25</value></property>
					<property name="name"><value>Circuit 25</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>26</value></property>
					<property name="name"><value>Circuit 26</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>27</value></property>
					<property name="name"><value>Circuit 27</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>28</value></property>
					<property name="name"><value>Circuit 28</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>29</value></property>
					<property name="name"><value>Circuit 29</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>30</value></property>
					<property name="name"><value>Circuit 30</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>31</value></property>
					<property name="name"><value>Circuit 31</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>32</value></property>
					<property name="name"><value>Circuit 32</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>33</value></property>
					<property name="name"><value>Circuit 33</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>34</value></property>
					<property name="name"><value>Circuit 34</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>35</value></property>
					<property name="name"><value>Circuit 35</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>36</value></property>
					<property name="name"><value>Circuit 36</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>37</value></property>
					<property name="name"><value>Circuit 37</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>38</value></property>
					<property name="name"><value>Circuit 38</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>39</value></property>
					<property name="name"><value>Circuit 39</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>40</value></property>
					<property name="name"><value>Circuit 40</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>41</value></property>
					<property name="name"><value>Circuit 41</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>42</value></property>
					<property name="name"><value>Circuit 42</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
			</property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">PDI BCMS Panel</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">PDI BCMS Panel</property>

		<property name="devid" display="ModBus ID" type="java.lang.Integer" editable="true" displayed="true"></property>
		<property name="maxCurrent" display="Current Design Limit (A)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="midCurrent" display="Current Warning Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="lowCurrent" display="Current Normal Operational Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="maxPower" display="Power Design Limit (kW)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="midPower" display="Power Warning Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="lowPower" display="Power Normal Operational Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="lowPowerFactor" display="Power Factor Warning Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="midPowerFactor" display="Power Factor Normal Operational Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="pullPeriod" display="Sampling Interval(minutes)" type="java.lang.Integer" editable="true" displayed="true">5</property>

		<producer id="panel" datatype="panel" name="Panel" object='$panel' />


		<producer id='phasearealpower' datatype='power' name='Phase A Real Power' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Phase A Real Power"]]' />
		<producer id='phasebrealpower' datatype='power' name='Phase B Real Power' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Phase B Real Power"]]' />
		<producer id='phasecrealpower' datatype='power' name='Phase C Real Power' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Phase C Real Power"]]' />

		<producer id='powerchannel1' datatype='power' name='Power Channel 01' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 1"]]' />
		<producer id='powerchannel2' datatype='power' name='Power Channel 02' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 2"]]' />
		<producer id='powerchannel3' datatype='power' name='Power Channel 03' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 3"]]' />
		<producer id='powerchannel4' datatype='power' name='Power Channel 04' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 4"]]' />
		<producer id='powerchannel5' datatype='power' name='Power Channel 05' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 5"]]' />
		<producer id='powerchannel6' datatype='power' name='Power Channel 06' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 6"]]' />
		<producer id='powerchannel7' datatype='power' name='Power Channel 07' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 7"]]' />
		<producer id='powerchannel8' datatype='power' name='Power Channel 08' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 8"]]' />
		<producer id='powerchannel9' datatype='power' name='Power Channel 09' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 9"]]' />
		<producer id='powerchannel10' datatype='power' name='Power Channel 10' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 10"]]' />
		<producer id='powerchannel11' datatype='power' name='Power Channel 11' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 11"]]' />
		<producer id='powerchannel12' datatype='power' name='Power Channel 12' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 12"]]' />
		<producer id='powerchannel13' datatype='power' name='Power Channel 13' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 13"]]' />
		<producer id='powerchannel14' datatype='power' name='Power Channel 14' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 14"]]' />
		<producer id='powerchannel15' datatype='power' name='Power Channel 15' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 15"]]' />
		<producer id='powerchannel16' datatype='power' name='Power Channel 16' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 16"]]' />
		<producer id='powerchannel17' datatype='power' name='Power Channel 17' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 17"]]' />
		<producer id='powerchannel18' datatype='power' name='Power Channel 18' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 18"]]' />
		<producer id='powerchannel19' datatype='power' name='Power Channel 19' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 19"]]' />
		<producer id='powerchannel20' datatype='power' name='Power Channel 20' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 20"]]' />
		<producer id='powerchannel21' datatype='power' name='Power Channel 21' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 21"]]' />
		<producer id='powerchannel22' datatype='power' name='Power Channel 22' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 22"]]' />
		<producer id='powerchannel23' datatype='power' name='Power Channel 23' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 23"]]' />
		<producer id='powerchannel24' datatype='power' name='Power Channel 24' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 24"]]' />
		<producer id='powerchannel25' datatype='power' name='Power Channel 25' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 25"]]' />
		<producer id='powerchannel26' datatype='power' name='Power Channel 26' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 26"]]' />
		<producer id='powerchannel27' datatype='power' name='Power Channel 27' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 27"]]' />
		<producer id='powerchannel28' datatype='power' name='Power Channel 28' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 28"]]' />
		<producer id='powerchannel29' datatype='power' name='Power Channel 29' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 29"]]' />
		<producer id='powerchannel30' datatype='power' name='Power Channel 30' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 30"]]' />
		<producer id='powerchannel31' datatype='power' name='Power Channel 31' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 31"]]' />
		<producer id='powerchannel32' datatype='power' name='Power Channel 32' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 32"]]' />
		<producer id='powerchannel33' datatype='power' name='Power Channel 33' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 33"]]' />
		<producer id='powerchannel34' datatype='power' name='Power Channel 34' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 34"]]' />
		<producer id='powerchannel35' datatype='power' name='Power Channel 35' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 35"]]' />
		<producer id='powerchannel36' datatype='power' name='Power Channel 36' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 36"]]' />
		<producer id='powerchannel37' datatype='power' name='Power Channel 37' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 37"]]' />
		<producer id='powerchannel38' datatype='power' name='Power Channel 38' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 38"]]' />
		<producer id='powerchannel39' datatype='power' name='Power Channel 39' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 39"]]' />
		<producer id='powerchannel40' datatype='power' name='Power Channel 40' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 40"]]' />
		<producer id='powerchannel41' datatype='power' name='Power Channel 41' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 41"]]' />
		<producer id='powerchannel42' datatype='power' name='Power Channel 42' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 42"]]' />


		<conducer id="CircuitPhase01" name="Circuit 01" >
			<producer id="circuit1" datatype="circuit" name="Circuit 01" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]' />
			<consumer id="phase_for_circuit1" datatype="phase" name="Circuit 01" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase02" name="Circuit 02" >
			<producer id="circuit2" datatype="circuit" name="Circuit 02" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]' />
			<consumer id="phase_for_circuit2" datatype="phase" name="Circuit 02" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase03" name="Circuit 03" >
			<producer id="circuit3" datatype="circuit" name="Circuit 03" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]' />
			<consumer id="phase_for_circuit3" datatype="phase" name="Circuit 03" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase04" name="Circuit 04" >
			<producer id="circuit4" datatype="circuit" name="Circuit 04" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]' />
			<consumer id="phase_for_circuit4" datatype="phase" name="Circuit 04" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase05" name="Circuit 05" >
			<producer id="circuit5" datatype="circuit" name="Circuit 05" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]' />
			<consumer id="phase_for_circuit5" datatype="phase" name="Circuit 05" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase06" name="Circuit 06" >
			<producer id="circuit6" datatype="circuit" name="Circuit 06" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]' />
			<consumer id="phase_for_circuit6" datatype="phase" name="Circuit 06" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase07" name="Circuit 07" >
			<producer id="circuit7" datatype="circuit" name="Circuit 07" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]' />
			<consumer id="phase_for_circuit7" datatype="phase" name="Circuit 07" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase08" name="Circuit 08" >
			<producer id="circuit8" datatype="circuit" name="Circuit 08" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]' />
			<consumer id="phase_for_circuit8" datatype="phase" name="Circuit 08" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase09" name="Circuit 09" >
			<producer id="circuit9" datatype="circuit" name="Circuit 09" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]' />
			<consumer id="phase_for_circuit9" datatype="phase" name="Circuit 09" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase10" name="Circuit 10" >
			<producer id="circuit10" datatype="circuit" name="Circuit 10" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]' />
			<consumer id="phase_for_circuit10" datatype="phase" name="Circuit 10" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase11" name="Circuit 11" >
			<producer id="circuit11" datatype="circuit" name="Circuit 11" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]' />
			<consumer id="phase_for_circuit11" datatype="phase" name="Circuit 11" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase12" name="Circuit 12" >
			<producer id="circuit12" datatype="circuit" name="Circuit 12" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]' />
			<consumer id="phase_for_circuit12" datatype="phase" name="Circuit 12" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase13" name="Circuit 13" >
			<producer id="circuit13" datatype="circuit" name="Circuit 13" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="13"]]' />
			<consumer id="phase_for_circuit13" datatype="phase" name="Circuit 13" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="13"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase14" name="Circuit 14" >
			<producer id="circuit14" datatype="circuit" name="Circuit 14" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="14"]]' />
			<consumer id="phase_for_circuit14" datatype="phase" name="Circuit 14" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="14"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase15" name="Circuit 15" >
			<producer id="circuit15" datatype="circuit" name="Circuit 15" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="15"]]' />
			<consumer id="phase_for_circuit15" datatype="phase" name="Circuit 15" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="15"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase16" name="Circuit 16" >
			<producer id="circuit16" datatype="circuit" name="Circuit 16" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="16"]]' />
			<consumer id="phase_for_circuit16" datatype="phase" name="Circuit 16" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="16"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase17" name="Circuit 17" >
			<producer id="circuit17" datatype="circuit" name="Circuit 17" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="17"]]' />
			<consumer id="phase_for_circuit17" datatype="phase" name="Circuit 17" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="17"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase18" name="Circuit 18" >
			<producer id="circuit18" datatype="circuit" name="Circuit 18" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="18"]]' />
			<consumer id="phase_for_circuit18" datatype="phase" name="Circuit 18" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="18"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase19" name="Circuit 19" >
			<producer id="circuit19" datatype="circuit" name="Circuit 19" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="19"]]' />
			<consumer id="phase_for_circuit19" datatype="phase" name="Circuit 19" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="19"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase20" name="Circuit 20" >
			<producer id="circuit20" datatype="circuit" name="Circuit 20" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="20"]]' />
			<consumer id="phase_for_circuit20" datatype="phase" name="Circuit 20" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="20"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase21" name="Circuit 21" >
			<producer id="circuit21" datatype="circuit" name="Circuit 21" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="21"]]' />
			<consumer id="phase_for_circuit21" datatype="phase" name="Circuit 21" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="21"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase22" name="Circuit 22" >
			<producer id="circuit22" datatype="circuit" name="Circuit 22" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="22"]]' />
			<consumer id="phase_for_circuit22" datatype="phase" name="Circuit 22" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="22"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase23" name="Circuit 23" >
			<producer id="circuit23" datatype="circuit" name="Circuit 23" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="23"]]' />
			<consumer id="phase_for_circuit23" datatype="phase" name="Circuit 23" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="23"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase24" name="Circuit 24" >
			<producer id="circuit24" datatype="circuit" name="Circuit 24" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="24"]]' />
			<consumer id="phase_for_circuit24" datatype="phase" name="Circuit 24" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="24"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase25" name="Circuit 25" >
			<producer id="circuit25" datatype="circuit" name="Circuit 25" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="25"]]' />
			<consumer id="phase_for_circuit25" datatype="phase" name="Circuit 25" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="25"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase26" name="Circuit 26" >
			<producer id="circuit26" datatype="circuit" name="Circuit 26" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="26"]]' />
			<consumer id="phase_for_circuit26" datatype="phase" name="Circuit 26" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="26"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase27" name="Circuit 27" >
			<producer id="circuit27" datatype="circuit" name="Circuit 27" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="27"]]' />
			<consumer id="phase_for_circuit27" datatype="phase" name="Circuit 27" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="27"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase28" name="Circuit 28" >
			<producer id="circuit28" datatype="circuit" name="Circuit 28" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="28"]]' />
			<consumer id="phase_for_circuit28" datatype="phase" name="Circuit 28" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="28"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase29" name="Circuit 29" >
			<producer id="circuit29" datatype="circuit" name="Circuit 29" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="29"]]' />
			<consumer id="phase_for_circuit29" datatype="phase" name="Circuit 29" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="29"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase30" name="Circuit 30" >
			<producer id="circuit30" datatype="circuit" name="Circuit 30" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="30"]]' />
			<consumer id="phase_for_circuit30" datatype="phase" name="Circuit 30" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="30"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase31" name="Circuit 31" >
			<producer id="circuit31" datatype="circuit" name="Circuit 31" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="31"]]' />
			<consumer id="phase_for_circuit31" datatype="phase" name="Circuit 31" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="31"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase32" name="Circuit 32" >
			<producer id="circuit32" datatype="circuit" name="Circuit 32" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="32"]]' />
			<consumer id="phase_for_circuit32" datatype="phase" name="Circuit 32" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="32"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase33" name="Circuit 33" >
			<producer id="circuit33" datatype="circuit" name="Circuit 33" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="33"]]' />
			<consumer id="phase_for_circuit33" datatype="phase" name="Circuit 33" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="33"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase34" name="Circuit 34" >
			<producer id="circuit34" datatype="circuit" name="Circuit 34" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="34"]]' />
			<consumer id="phase_for_circuit34" datatype="phase" name="Circuit 34" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="34"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase35" name="Circuit 35" >
			<producer id="circuit35" datatype="circuit" name="Circuit 35" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="35"]]' />
			<consumer id="phase_for_circuit35" datatype="phase" name="Circuit 35" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="35"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase36" name="Circuit 36" >
			<producer id="circuit36" datatype="circuit" name="Circuit 36" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="36"]]' />
			<consumer id="phase_for_circuit36" datatype="phase" name="Circuit 36" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="36"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase37" name="Circuit 37" >
			<producer id="circuit37" datatype="circuit" name="Circuit 37" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="37"]]' />
			<consumer id="phase_for_circuit37" datatype="phase" name="Circuit 37" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="37"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase38" name="Circuit 38" >
			<producer id="circuit38" datatype="circuit" name="Circuit 38" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="38"]]' />
			<consumer id="phase_for_circuit38" datatype="phase" name="Circuit 38" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="38"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase39" name="Circuit 39" >
			<producer id="circuit39" datatype="circuit" name="Circuit 39" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="39"]]' />
			<consumer id="phase_for_circuit39" datatype="phase" name="Circuit 39" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="39"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase40" name="Circuit 40" >
			<producer id="circuit40" datatype="circuit" name="Circuit 40" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="40"]]' />
			<consumer id="phase_for_circuit40" datatype="phase" name="Circuit 40" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="40"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase41" name="Circuit 41" >
			<producer id="circuit41" datatype="circuit" name="Circuit 41" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="41"]]' />
			<consumer id="phase_for_circuit41" datatype="phase" name="Circuit 41" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="41"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase42" name="Circuit 42" >
			<producer id="circuit42" datatype="circuit" name="Circuit 42" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="42"]]' />
			<consumer id="phase_for_circuit42" datatype="phase" name="Circuit 42" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="42"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>


		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40001"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40002"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40003"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40004"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40005"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40006"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40007"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40008"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40009"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40010"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40011"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40012"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="13"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40013"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="14"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40014"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="15"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40015"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="16"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40016"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="17"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40017"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="18"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40018"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="19"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40019"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="20"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40020"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="21"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40021"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="22"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40022"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="23"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40023"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="24"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40024"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="25"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40025"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="26"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40026"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="27"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40027"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="28"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40028"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="29"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40029"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="30"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40030"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="31"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40031"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="32"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40032"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="33"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40033"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="34"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40034"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="35"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40035"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="36"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40036"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="37"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40037"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="38"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40038"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="39"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40039"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="40"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40040"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="41"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40041"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="42"]]/properties[name="current"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40042"]]' />

		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40178"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40179"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40180"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40181"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40182"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40183"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40184"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40185"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40186"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40187"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40188"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40189"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="13"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40190"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="14"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40191"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="15"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40192"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="16"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40193"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="17"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40194"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="18"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40195"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="19"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40196"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="20"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40197"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="21"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40198"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="22"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40199"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="23"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40200"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="24"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40201"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="25"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40202"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="26"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40203"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="27"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40204"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="28"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40205"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="29"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40206"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="30"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40207"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="31"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40208"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="32"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40209"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="33"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40210"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="34"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40211"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="35"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40212"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="36"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40213"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="37"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40214"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="38"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40215"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="39"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40216"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="40"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40217"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="41"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40218"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="42"]]/properties[name="power"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40219"]]' />

		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40304"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40305"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40306"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40307"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40308"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40309"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40310"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40311"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40312"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40313"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40314"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40315"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="13"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40316"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="14"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40317"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="15"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40318"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="16"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40319"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="17"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40320"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="18"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40321"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="19"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40322"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="20"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40323"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="21"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40324"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="22"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40325"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="23"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40326"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="24"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40327"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="25"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40328"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="26"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40329"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="27"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40330"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="28"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40331"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="29"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40332"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="30"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40333"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="31"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40334"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="32"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40335"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="33"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40336"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="34"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40337"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="35"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40338"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="36"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40339"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="37"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40340"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="38"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40341"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="39"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40342"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="40"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40343"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="41"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40344"]]' />
		<docking from='$panel/properties[name="circuits"]/children[properties[name="num"][value="42"]]/properties[name="powerFactor"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40345"]]' />

		<docking from='$panel/properties[name="currentA"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40357"]]' />
		<docking from='$panel/properties[name="currentB"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40358"]]' />
		<docking from='$panel/properties[name="currentC"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40359"]]' />

		<docking from='$panel/properties[name="voltageAB"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40370"]]' />
		<docking from='$panel/properties[name="voltageBC"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40371"]]' />
		<docking from='$panel/properties[name="voltageCA"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40372"]]' />

		<docking from='$panel/properties[name="voltageAN"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40373"]]' />
		<docking from='$panel/properties[name="voltageBN"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40374"]]' />
		<docking from='$panel/properties[name="voltageCN"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40375"]]' />

		<docking from='$panel/properties[name="powerA"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40388"]]' />
		<docking from='$panel/properties[name="powerB"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40389"]]' />
		<docking from='$panel/properties[name="powerC"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40390"]]' />

		<docking from='$panel/properties[name="powerFactorA"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40400"]]' />
		<docking from='$panel/properties[name="powerFactorB"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40401"]]' />
		<docking from='$panel/properties[name="powerFactorC"]'
			to='$pdi/properties[name="registers"]/children[properties[name="id"][value="40402"]]' />

		<varbinding vars="x">
			<property>$panel/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$panel/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="name">
			<property>$panel/properties[name="name"]</property>
			<property>$pdi/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="devid">
			<property>$pdi/properties[name="id"]</property>
			<value>devid</value>
		</varbinding>
		<varbinding vars="maxPower">
			<property>$panel/properties[name="circuits"]/children/properties[name="maxPower"]</property>
			<value>maxPower</value>
		</varbinding>
		<varbinding vars="midPower">
			<property>$panel/properties[name="circuits"]/children/properties[name="midPower"]</property>
			<value>midPower</value>
		</varbinding>
		<varbinding vars="lowPower">
			<property>$panel/properties[name="circuits"]/children/properties[name="lowPower"]</property>
			<value>lowPower</value>
		</varbinding>
		<varbinding vars="maxCurrent">
			<property>$panel/properties[name="circuits"]/children/properties[name="maxCurrent"]</property>
			<value>maxCurrent</value>
		</varbinding>
		<varbinding vars="midCurrent">
			<property>$panel/properties[name="circuits"]/children/properties[name="midCurrent"]</property>
			<value>midCurrent</value>
		</varbinding>
		<varbinding vars="lowCurrent">
			<property>$panel/properties[name="circuits"]/children/properties[name="lowCurrent"]</property>
			<value>lowCurrent</value>
		</varbinding>
		<varbinding vars="midPowerFactor">
			<property>$panel/properties[name="circuits"]/children/properties[name="midPowerFactor"]</property>
			<value>midPowerFactor</value>
		</varbinding>
		<varbinding vars="lowPowerFactor">
			<property>$panel/properties[name="circuits"]/children/properties[name="lowPowerFactor"]</property>
			<value>lowPowerFactor</value>
		</varbinding>
		<varbinding vars="pullPeriod">
			<!-- <property>$pdi/properties[name="registers"]/children/properties[name="period"]</property>-->
			<property>$pdi/properties[name="registers"]/children/properties[name="lastValue"]/tags[tagName='poll']</property>
			<value>pullPeriod*60000</value>
		</varbinding>
	</component>


</componentlib>

