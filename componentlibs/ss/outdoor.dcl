<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
	name="outdoor temp and humidity"
	version="&componentlib.version;"
	cversion="&componentlib.cversion;"
	oversion="&componentlib.oversion;"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
	>



<component type="outdoor_temp_humidity" classes="placeable" grouppath="Environmentals;Wireless;Other" filters="Environmentals">
	<display>
		<description>A Constellation II node attached to an outdoor temp and humidity sensor.  This Component goes in a Room and a WSN Data Source.</description>
		<name>Outdoor Temp/Humidity</name>
		<shape>outdoor_temp_humidity</shape>
	</display>

	<object type="wsnnode" as="node">
		<dlid>DLID:-1</dlid>
		<property name="batteryCapacity"><value>5600.0</value></property>
		
		<property name="batteryStatus"><value>0</value></property>
		<property name="status"><value>0</value></property>
		<property name="platformId"><value>15</value></property>
		<property name="platformName"><value>Constellation 2</value></property>
		<property name="batteryOperated"><value>0</value></property>
		<property name="sensepoints">
			<object type="wsnsensor">
				<dlid>DLID:-1</dlid>
				<property name="name"><value>battery</value></property>
				<property name="channel"><value>2</value></property>
				<property name="min"><value>1.8</value></property>
				<property name="max"><value>3.7</value></property>
				<property name="aMin"><value>2.5</value></property>
				<property name="aMax"><value>3.7</value></property>
				<property name="rMin"><value>2.7</value></property>
				<property name="rMax"><value>3.7</value></property>
				<property name="type"><value>5</value></property>
				<property name="z"><value>0</value></property>
				<property name="status"><value>1</value></property>
				<property name="lastValue"><value>-5000.0</value></property>
				<property name="dataclass"><value>210</value></property>
				<property name="enabled"><value>1</value></property>
				<property name="mode"><value>averaging</value></property>
				<property name="interval"><value>0</value></property>
				<property name="smartSendThreshold"><value>0</value></property>
			</object>
			<object type="wsnsensor">
				<dlid>DLID:-1</dlid>
				<property name="name"><value>temp</value></property>
				<property name="channel"><value>3</value></property>
				<property name="min"><value>-40</value></property>
				<property name="max"><value>158</value></property>
				<property name="type"><value>27</value></property>
				<property name="z"><value>0</value></property>
				<property name="status"><value>1</value></property>
				<property name="scaleMin"><value>-31.0</value></property>
				<property name="scaleMax"><value>95.0</value></property>
				<property name="lastValue"><value>-5000.0</value></property>
				<property name="dataclass"><value>200</value></property>
				<property name="enabled"><value>1</value></property>
				<property name="mode"><value>averaging</value></property>
				<property name="interval"><value>0</value></property>
			</object>
			<object type="wsnsensor">
				<dlid>DLID:-1</dlid>
				<property name="name"><value>humidity</value></property>
				<property name="channel"><value>4</value></property>
				<property name="min"><value>0</value></property>
				<property name="max"><value>100</value></property>
				<property name="type"><value>2</value></property>
				<property name="z"><value>0</value></property>
				<property name="status"><value>1</value></property>
				<property name="scaleMin"><value>0</value></property>
				<property name="scaleMax"><value>95</value></property>
				<property name="lastValue"><value>-5000.0</value></property>
				<property name="dataclass"><value>201</value></property>
				<property name="enabled"><value>1</value></property>
				<property name="mode"><value>averaging</value></property>
				<property name="interval"><value>0</value></property>
			</object>

		</property>
	</object>

	<macids value="mac_id" />

	<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Outdoor Temp/Humidity</property>
	<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
	<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
	<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true">0</property>
	<property name="sample_interval" display="Sampling Interval" type="java.lang.Integer" editable="true" displayed="true">5</property>
	<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>
	<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Outdoor Temp/Humidity</property>

	<property name="temprange" display="Temperature Range" type="java.lang.String" editable="true" displayed="true"
	valueChoices="-35C to 35C (-31F to 95F):A,0C to 50C (32F to 122F):B,-40C to 70C (-40F to 158F):C">A
	</property>

	<property name='manualWSNConfig' display='Manual WSN Configuration' type='java.lang.Boolean' editable='true' displayed='false' dimension=''>false</property>

	<varbinding vars="name">
		<property>$node/properties[name="name"]</property>
		<value>'Node ' + name</value>
	</varbinding>
	<varbinding vars="x">
		<property>$node/properties[name="x"]</property>
		<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
		<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
		<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
		<value>x</value>
	</varbinding>
	<varbinding vars="y">
		<property>$node/properties[name="y"]</property>
		<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
		<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
		<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
		<value>y</value>
	</varbinding>
	<varbinding vars="mac_id">
		<property>$node/properties[name="mac"]</property>
		<value>Long.parseLong(mac_id,16)</value>
	</varbinding>
	<varbinding vars="mac_id">
		<property>$node/properties[name="id"]</property>
		<value>lid.generateLogicalId(mac_id , self)</value>
	</varbinding>
	<varbinding vars="sample_interval">
		<property>$node/properties[name="period"]</property>
		<value>sample_interval + ' min'</value>
	</varbinding>
	<varbinding vars="location">
		<property>$node/properties[name="location"]</property>
		<value>location</value>
	</varbinding>

	<varbinding vars="temprange">
		<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="scaleMin"]</property>
		<value>
			if (temprange == 'A') { return -31; }
			if (temprange == 'B') { return 32; }
			if (temprange == 'C') { return -40; }
		</value>
	</varbinding>

	<varbinding vars="temprange">
		<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="scaleMax"]</property>
		<value>
			if (temprange == 'A') { return 95; }
			if (temprange == 'B') { return 122; }
			if (temprange == 'C') { return 158; }
		</value>
	</varbinding>

</component>

</componentlib>