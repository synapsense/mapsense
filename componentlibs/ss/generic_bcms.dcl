<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE componentlib SYSTEM "file:///componentlib.dtd">
<componentlib
		name="Generic BCMS"
		version="&componentlib.version;"
		cversion="&componentlib.cversion;"
		oversion="&componentlib.oversion;"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.synapsense.com componentlib.xsd"
		>

	<component type="generic-bcms-custom" classes="placeable" grouppath="Power &amp; Energy;Wired;BCMS" filters="Power">
		<display>
			<description>Integration-only BCMS.</description>
			<name>Generic 12-Channel BCMS Panel</name>
			<shape>bcms_custom</shape>
		</display>



		<object type="panel" as="panel">
			<dlid>DLID:-1</dlid>
			<property name="circuits">
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>1</value></property>
					<property name="name"><value>Circuit 1</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>2</value></property>
					<property name="name"><value>Circuit 2</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>3</value></property>
					<property name="name"><value>Circuit 3</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>4</value></property>
					<property name="name"><value>Circuit 4</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>5</value></property>
					<property name="name"><value>Circuit 5</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>6</value></property>
					<property name="name"><value>Circuit 6</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>7</value></property>
					<property name="name"><value>Circuit 7</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>8</value></property>
					<property name="name"><value>Circuit 8</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>9</value></property>
					<property name="name"><value>Circuit 9</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>10</value></property>
					<property name="name"><value>Circuit 10</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>11</value></property>
					<property name="name"><value>Circuit 11</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>12</value></property>
					<property name="name"><value>Circuit 12</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>

			</property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Generic 12-Channel BCMS Panel</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Generic 12-Channel BCMS Panel</property>

		<property name="maxCurrent" display="Current Design Limit (A)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="midCurrent" display="Current Warning Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="lowCurrent" display="Current Normal Operational Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="maxPower" display="Power Design Limit (kW)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="midPower" display="Power Warning Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="lowPower" display="Power Normal Operational Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="lowPowerFactor" display="Power Factor Warning Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="midPowerFactor" display="Power Factor Normal Operational Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>

		<producer id="panel" datatype="panel" name="Panel" object='$panel' exclusive='true'/>

		<conducer id="CircuitPhase01" name="Circuit 01" >
			<producer id="circuit1" datatype="circuit" name="Circuit 01" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]' />
			<consumer id="phase_for_circuit1" datatype="phase" name="Circuit 01" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase02" name="Circuit 02" >
			<producer id="circuit2" datatype="circuit" name="Circuit 02" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]' />
			<consumer id="phase_for_circuit2" datatype="phase" name="Circuit 02" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase03" name="Circuit 03" >
			<producer id="circuit3" datatype="circuit" name="Circuit 03" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]' />
			<consumer id="phase_for_circuit3" datatype="phase" name="Circuit 03" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase04" name="Circuit 04" >
			<producer id="circuit4" datatype="circuit" name="Circuit 04" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]' />
			<consumer id="phase_for_circuit4" datatype="phase" name="Circuit 04" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase05" name="Circuit 05" >
			<producer id="circuit5" datatype="circuit" name="Circuit 05" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]' />
			<consumer id="phase_for_circuit5" datatype="phase" name="Circuit 05" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase06" name="Circuit 06" >
			<producer id="circuit6" datatype="circuit" name="Circuit 06" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]' />
			<consumer id="phase_for_circuit6" datatype="phase" name="Circuit 06" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase07" name="Circuit 07" >
			<producer id="circuit7" datatype="circuit" name="Circuit 07" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]' />
			<consumer id="phase_for_circuit7" datatype="phase" name="Circuit 07" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase08" name="Circuit 08" >
			<producer id="circuit8" datatype="circuit" name="Circuit 08" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]' />
			<consumer id="phase_for_circuit8" datatype="phase" name="Circuit 08" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase09" name="Circuit 09" >
			<producer id="circuit9" datatype="circuit" name="Circuit 09" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]' />
			<consumer id="phase_for_circuit9" datatype="phase" name="Circuit 09" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase10" name="Circuit 10" >
			<producer id="circuit10" datatype="circuit" name="Circuit 10" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]' />
			<consumer id="phase_for_circuit10" datatype="phase" name="Circuit 10" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase11" name="Circuit 11" >
			<producer id="circuit11" datatype="circuit" name="Circuit 11" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]' />
			<consumer id="phase_for_circuit11" datatype="phase" name="Circuit 11" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase12" name="Circuit 12" >
			<producer id="circuit12" datatype="circuit" name="Circuit 12" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]' />
			<consumer id="phase_for_circuit12" datatype="phase" name="Circuit 12" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>


		<consumer id="circuit1current" datatype="power" name="Circuit 01 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit2current" datatype="power" name="Circuit 02 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit3current" datatype="power" name="Circuit 03 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit4current" datatype="power" name="Circuit 04 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit5current" datatype="power" name="Circuit 05 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit6current" datatype="power" name="Circuit 06 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit7current" datatype="power" name="Circuit 07 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit8current" datatype="power" name="Circuit 08 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit9current" datatype="power" name="Circuit 09 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit10current" datatype="power" name="Circuit 10 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit11current" datatype="power" name="Circuit 11 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit12current" datatype="power" name="Circuit 12 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]/properties[name="current"]' collection="false" required="false"  />

		<consumer id="circuit1power" datatype="power" name="Circuit 01 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit2power" datatype="power" name="Circuit 02 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit3power" datatype="power" name="Circuit 03 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit4power" datatype="power" name="Circuit 04 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit5power" datatype="power" name="Circuit 05 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit6power" datatype="power" name="Circuit 06 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit7power" datatype="power" name="Circuit 07 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit8power" datatype="power" name="Circuit 08 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit9power" datatype="power" name="Circuit 09 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit10power" datatype="power" name="Circuit 10 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit11power" datatype="power" name="Circuit 11 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit12power" datatype="power" name="Circuit 12 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]/properties[name="power"]' collection="false" required="false"  />

		<consumer id="circuit1powerfactor" datatype="power" name="Circuit 01 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit2powerfactor" datatype="power" name="Circuit 02 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit3powerfactor" datatype="power" name="Circuit 03 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit4powerfactor" datatype="power" name="Circuit 04 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit5powerfactor" datatype="power" name="Circuit 05 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit6powerfactor" datatype="power" name="Circuit 06 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit7powerfactor" datatype="power" name="Circuit 07 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit8powerfactor" datatype="power" name="Circuit 08 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit9powerfactor" datatype="power" name="Circuit 09 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit10powerfactor" datatype="power" name="Circuit 10 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit11powerfactor" datatype="power" name="Circuit 11 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit12powerfactor" datatype="power" name="Circuit 12 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]/properties[name="powerFactor"]' collection="false" required="false"  />

		<consumer id="dev1" datatype="power" name="Current A" property='$panel/properties[name="currentA"]'  collection="false" required="false"  />
		<consumer id="dev2" datatype="power" name="Current B" property='$panel/properties[name="currentB"]'  collection="false" required="false"  />
		<consumer id="dev3" datatype="power" name="Current C" property='$panel/properties[name="currentC"]'  collection="false" required="false"  />

		<consumer id="dev4" datatype="power" name="Voltage AB" property='$panel/properties[name="voltageAB"]'  collection="false" required="false"  />
		<consumer id="dev5" datatype="power" name="Voltage BC" property='$panel/properties[name="voltageBC"]'  collection="false" required="false"  />
		<consumer id="dev6" datatype="power" name="Voltage CA" property='$panel/properties[name="voltageCA"]'  collection="false" required="false"  />

		<consumer id="dev7" datatype="power" name="Voltage AN" property='$panel/properties[name="voltageAN"]'  collection="false" required="false"  />
		<consumer id="dev8" datatype="power" name="Voltage BN" property='$panel/properties[name="voltageBN"]'  collection="false" required="false"  />
		<consumer id="dev9" datatype="power" name="Voltage CN" property='$panel/properties[name="voltageCN"]' collection="false" required="false"  />

		<consumer id="dev10" datatype="power" name="Power A" property='$panel/properties[name="powerA"]' collection="false" required="false"  />
		<consumer id="dev11" datatype="power" name="Power B" property='$panel/properties[name="powerB"]' collection="false" required="false"  />
		<consumer id="dev12" datatype="power" name="Power C" property='$panel/properties[name="powerC"]' collection="false" required="false"  />

		<consumer id="dev13" datatype="power" name="Power Factor A" property='$panel/properties[name="powerFactorA"]' collection="false" required="false"  />
		<consumer id="dev14" datatype="power" name="Power Factor B" property='$panel/properties[name="powerFactorB"]' collection="false" required="false"  />
		<consumer id="dev15" datatype="power" name="Power Factor C" property='$panel/properties[name="powerFactorC"]' collection="false" required="false"  />


		<varbinding vars="x">
			<property>$panel/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$panel/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="name">
			<property>$panel/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="maxPower">
			<property>$panel/properties[name="circuits"]/children/properties[name="maxPower"]</property>
			<value>maxPower</value>
		</varbinding>
		<varbinding vars="midPower">
			<property>$panel/properties[name="circuits"]/children/properties[name="midPower"]</property>
			<value>midPower</value>
		</varbinding>
		<varbinding vars="lowPower">
			<property>$panel/properties[name="circuits"]/children/properties[name="lowPower"]</property>
			<value>lowPower</value>
		</varbinding>
		<varbinding vars="maxCurrent">
			<property>$panel/properties[name="circuits"]/children/properties[name="maxCurrent"]</property>
			<value>maxCurrent</value>
		</varbinding>
		<varbinding vars="midCurrent">
			<property>$panel/properties[name="circuits"]/children/properties[name="midCurrent"]</property>
			<value>midCurrent</value>
		</varbinding>
		<varbinding vars="lowCurrent">
			<property>$panel/properties[name="circuits"]/children/properties[name="lowCurrent"]</property>
			<value>lowCurrent</value>
		</varbinding>
		<varbinding vars="midPowerFactor">
			<property>$panel/properties[name="circuits"]/children/properties[name="midPowerFactor"]</property>
			<value>midPowerFactor</value>
		</varbinding>
		<varbinding vars="lowPowerFactor">
			<property>$panel/properties[name="circuits"]/children/properties[name="lowPowerFactor"]</property>
			<value>lowPowerFactor</value>
		</varbinding>
	</component>

	<component type="generic-bcms-custom-24" classes="placeable" grouppath="Power &amp; Energy;Wired;BCMS" filters="Power">
		<display>
			<description>Integration-only BCMS with 24 Channels.</description>
			<name>Generic 24-Channel BCMS Panel</name>
			<shape>bcms_custom</shape>
		</display>

		<object type="panel" as="panel">
			<dlid>DLID:-1</dlid>
			<property name="circuits">
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>1</value></property>
					<property name="name"><value>Circuit 1</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>2</value></property>
					<property name="name"><value>Circuit 2</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>3</value></property>
					<property name="name"><value>Circuit 3</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>4</value></property>
					<property name="name"><value>Circuit 4</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>5</value></property>
					<property name="name"><value>Circuit 5</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>6</value></property>
					<property name="name"><value>Circuit 6</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>7</value></property>
					<property name="name"><value>Circuit 7</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>8</value></property>
					<property name="name"><value>Circuit 8</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>9</value></property>
					<property name="name"><value>Circuit 9</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>10</value></property>
					<property name="name"><value>Circuit 10</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>11</value></property>
					<property name="name"><value>Circuit 11</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>12</value></property>
					<property name="name"><value>Circuit 12</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>13</value></property>
					<property name="name"><value>Circuit 13</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>14</value></property>
					<property name="name"><value>Circuit 14</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>15</value></property>
					<property name="name"><value>Circuit 15</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>16</value></property>
					<property name="name"><value>Circuit 16</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>17</value></property>
					<property name="name"><value>Circuit 17</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>18</value></property>
					<property name="name"><value>Circuit 18</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>19</value></property>
					<property name="name"><value>Circuit 19</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>20</value></property>
					<property name="name"><value>Circuit 20</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>21</value></property>
					<property name="name"><value>Circuit 21</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>22</value></property>
					<property name="name"><value>Circuit 22</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>23</value></property>
					<property name="name"><value>Circuit 23</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>24</value></property>
					<property name="name"><value>Circuit 24</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>

			</property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Generic 24-Channel BCMS Panel</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Generic 24-Channel BCMS Panel</property>

		<property name="maxCurrent" display="Current Design Limit (A)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="midCurrent" display="Current Warning Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="lowCurrent" display="Current Normal Operational Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="maxPower" display="Power Design Limit (kW)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="midPower" display="Power Warning Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="lowPower" display="Power Normal Operational Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="lowPowerFactor" display="Power Factor Warning Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="midPowerFactor" display="Power Factor Normal Operational Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>

		<producer id="panel" datatype="panel" name="Panel" object='$panel' exclusive='true'/>

		<conducer id="CircuitPhase01" name="Circuit 1" >
			<producer id="circuit1" datatype="circuit" name="Circuit 1" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]' />
			<consumer id="phase_for_circuit1" datatype="phase" name="Circuit 1" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase02" name="Circuit 2" >
			<producer id="circuit2" datatype="circuit" name="Circuit 2" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]' />
			<consumer id="phase_for_circuit2" datatype="phase" name="Circuit 2" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase03" name="Circuit 3" >
			<producer id="circuit3" datatype="circuit" name="Circuit 3" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]' />
			<consumer id="phase_for_circuit3" datatype="phase" name="Circuit 3" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase04" name="Circuit 4" >
			<producer id="circuit4" datatype="circuit" name="Circuit 4" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]' />
			<consumer id="phase_for_circuit4" datatype="phase" name="Circuit 4" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase05" name="Circuit 5" >
			<producer id="circuit5" datatype="circuit" name="Circuit 5" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]' />
			<consumer id="phase_for_circuit5" datatype="phase" name="Circuit 5" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase06" name="Circuit 6" >
			<producer id="circuit6" datatype="circuit" name="Circuit 6" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]' />
			<consumer id="phase_for_circuit6" datatype="phase" name="Circuit 6" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase07" name="Circuit 7" >
			<producer id="circuit7" datatype="circuit" name="Circuit 7" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]' />
			<consumer id="phase_for_circuit7" datatype="phase" name="Circuit 7" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase08" name="Circuit 8" >
			<producer id="circuit8" datatype="circuit" name="Circuit 8" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]' />
			<consumer id="phase_for_circuit8" datatype="phase" name="Circuit 8" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase09" name="Circuit 9" >
			<producer id="circuit9" datatype="circuit" name="Circuit 9" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]' />
			<consumer id="phase_for_circuit9" datatype="phase" name="Circuit 9" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase10" name="Circuit 10" >
			<producer id="circuit10" datatype="circuit" name="Circuit 10" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]' />
			<consumer id="phase_for_circuit10" datatype="phase" name="Circuit 10" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase11" name="Circuit 11" >
			<producer id="circuit11" datatype="circuit" name="Circuit 11" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]' />
			<consumer id="phase_for_circuit11" datatype="phase" name="Circuit 11" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase12" name="Circuit 12" >
			<producer id="circuit12" datatype="circuit" name="Circuit 12" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]' />
			<consumer id="phase_for_circuit12" datatype="phase" name="Circuit 12" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase13" name="Circuit 13" >
			<producer id="circuit13" datatype="circuit" name="Circuit 13" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="13"]]' />
			<consumer id="phase_for_circuit13" datatype="phase" name="Circuit 13" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="13"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase14" name="Circuit 14" >
			<producer id="circuit14" datatype="circuit" name="Circuit 14" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="14"]]' />
			<consumer id="phase_for_circuit14" datatype="phase" name="Circuit 14" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="14"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase15" name="Circuit 15" >
			<producer id="circuit15" datatype="circuit" name="Circuit 15" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="15"]]' />
			<consumer id="phase_for_circuit15" datatype="phase" name="Circuit 15" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="15"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase16" name="Circuit 16" >
			<producer id="circuit16" datatype="circuit" name="Circuit 16" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="16"]]' />
			<consumer id="phase_for_circuit16" datatype="phase" name="Circuit 16" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="16"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase17" name="Circuit 17" >
			<producer id="circuit17" datatype="circuit" name="Circuit 17" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="17"]]' />
			<consumer id="phase_for_circuit17" datatype="phase" name="Circuit 17" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="17"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase18" name="Circuit 18" >
			<producer id="circuit18" datatype="circuit" name="Circuit 18" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="18"]]' />
			<consumer id="phase_for_circuit18" datatype="phase" name="Circuit 18" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="18"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase19" name="Circuit 19" >
			<producer id="circuit19" datatype="circuit" name="Circuit 19" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="19"]]' />
			<consumer id="phase_for_circuit19" datatype="phase" name="Circuit 19" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="19"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase20" name="Circuit 20" >
			<producer id="circuit20" datatype="circuit" name="Circuit 20" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="20"]]' />
			<consumer id="phase_for_circuit20" datatype="phase" name="Circuit 20" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="20"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase21" name="Circuit 21" >
			<producer id="circuit21" datatype="circuit" name="Circuit 21" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="21"]]' />
			<consumer id="phase_for_circuit21" datatype="phase" name="Circuit 21" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="21"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase22" name="Circuit 22" >
			<producer id="circuit22" datatype="circuit" name="Circuit 22" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="22"]]' />
			<consumer id="phase_for_circuit22" datatype="phase" name="Circuit 22" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="22"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase23" name="Circuit 23" >
			<producer id="circuit23" datatype="circuit" name="Circuit 23" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="23"]]' />
			<consumer id="phase_for_circuit23" datatype="phase" name="Circuit 23" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="23"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase24" name="Circuit 24" >
			<producer id="circuit24" datatype="circuit" name="Circuit 24" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="24"]]' />
			<consumer id="phase_for_circuit24" datatype="phase" name="Circuit 24" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="24"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>


		<consumer id="circuit1current" datatype="power" name="Circuit 1 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit2current" datatype="power" name="Circuit 2 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit3current" datatype="power" name="Circuit 3 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit4current" datatype="power" name="Circuit 4 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit5current" datatype="power" name="Circuit 5 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit6current" datatype="power" name="Circuit 6 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit7current" datatype="power" name="Circuit 7 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit8current" datatype="power" name="Circuit 8 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit9current" datatype="power" name="Circuit 9 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit10current" datatype="power" name="Circuit 10 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit11current" datatype="power" name="Circuit 11 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit12current" datatype="power" name="Circuit 12 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit13current" datatype="power" name="Circuit 13 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="13"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit14current" datatype="power" name="Circuit 14 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="14"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit15current" datatype="power" name="Circuit 15 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="15"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit16current" datatype="power" name="Circuit 16 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="16"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit17current" datatype="power" name="Circuit 17 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="17"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit18current" datatype="power" name="Circuit 18 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="18"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit19current" datatype="power" name="Circuit 19 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="19"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit20current" datatype="power" name="Circuit 20 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="20"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit21current" datatype="power" name="Circuit 21 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="21"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit22current" datatype="power" name="Circuit 22 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="22"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit23current" datatype="power" name="Circuit 23 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="23"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit24current" datatype="power" name="Circuit 24 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="24"]]/properties[name="current"]' collection="false" required="false"  />

		<consumer id="circuit1power" datatype="power" name="Circuit 1 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit2power" datatype="power" name="Circuit 2 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit3power" datatype="power" name="Circuit 3 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit4power" datatype="power" name="Circuit 4 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit5power" datatype="power" name="Circuit 5 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit6power" datatype="power" name="Circuit 6 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit7power" datatype="power" name="Circuit 7 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit8power" datatype="power" name="Circuit 8 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit9power" datatype="power" name="Circuit 9 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit10power" datatype="power" name="Circuit 10 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit11power" datatype="power" name="Circuit 11 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit12power" datatype="power" name="Circuit 12 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit13power" datatype="power" name="Circuit 13 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="13"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit14power" datatype="power" name="Circuit 14 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="14"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit15power" datatype="power" name="Circuit 15 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="15"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit16power" datatype="power" name="Circuit 16 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="16"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit17power" datatype="power" name="Circuit 17 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="17"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit18power" datatype="power" name="Circuit 18 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="18"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit19power" datatype="power" name="Circuit 19 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="19"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit20power" datatype="power" name="Circuit 20 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="20"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit21power" datatype="power" name="Circuit 21 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="21"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit22power" datatype="power" name="Circuit 22 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="22"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit23power" datatype="power" name="Circuit 23 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="23"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit24power" datatype="power" name="Circuit 24 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="24"]]/properties[name="power"]' collection="false" required="false"  />

		<consumer id="circuit1powerfactor" datatype="power" name="Circuit 1 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit2powerfactor" datatype="power" name="Circuit 2 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit3powerfactor" datatype="power" name="Circuit 3 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit4powerfactor" datatype="power" name="Circuit 4 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit5powerfactor" datatype="power" name="Circuit 5 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit6powerfactor" datatype="power" name="Circuit 6 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit7powerfactor" datatype="power" name="Circuit 7 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit8powerfactor" datatype="power" name="Circuit 8 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit9powerfactor" datatype="power" name="Circuit 9 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit10powerfactor" datatype="power" name="Circuit 10 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit11powerfactor" datatype="power" name="Circuit 11 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit12powerfactor" datatype="power" name="Circuit 12 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit13powerfactor" datatype="power" name="Circuit 13 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="13"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit14powerfactor" datatype="power" name="Circuit 14 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="14"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit15powerfactor" datatype="power" name="Circuit 15 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="15"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit16powerfactor" datatype="power" name="Circuit 16 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="16"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit17powerfactor" datatype="power" name="Circuit 17 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="17"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit18powerfactor" datatype="power" name="Circuit 18 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="18"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit19powerfactor" datatype="power" name="Circuit 19 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="19"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit20powerfactor" datatype="power" name="Circuit 20 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="20"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit21powerfactor" datatype="power" name="Circuit 21 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="21"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit22powerfactor" datatype="power" name="Circuit 22 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="22"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit23powerfactor" datatype="power" name="Circuit 23 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="23"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit24powerfactor" datatype="power" name="Circuit 24 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="24"]]/properties[name="powerFactor"]' collection="false" required="false"  />

		<consumer id="dev1" datatype="power" name="Current A" property='$panel/properties[name="currentA"]'  collection="false" required="false"  />
		<consumer id="dev2" datatype="power" name="Current B" property='$panel/properties[name="currentB"]'  collection="false" required="false"  />
		<consumer id="dev3" datatype="power" name="Current C" property='$panel/properties[name="currentC"]'  collection="false" required="false"  />

		<consumer id="dev4" datatype="power" name="Voltage AB" property='$panel/properties[name="voltageAB"]'  collection="false" required="false"  />
		<consumer id="dev5" datatype="power" name="Voltage BC" property='$panel/properties[name="voltageBC"]'  collection="false" required="false"  />
		<consumer id="dev6" datatype="power" name="Voltage CA" property='$panel/properties[name="voltageCA"]'  collection="false" required="false"  />

		<consumer id="dev7" datatype="power" name="Voltage AN" property='$panel/properties[name="voltageAN"]'  collection="false" required="false"  />
		<consumer id="dev8" datatype="power" name="Voltage BN" property='$panel/properties[name="voltageBN"]'  collection="false" required="false"  />
		<consumer id="dev9" datatype="power" name="Voltage CN" property='$panel/properties[name="voltageCN"]' collection="false" required="false"  />

		<consumer id="dev10" datatype="power" name="Power A" property='$panel/properties[name="powerA"]' collection="false" required="false"  />
		<consumer id="dev11" datatype="power" name="Power B" property='$panel/properties[name="powerB"]' collection="false" required="false"  />
		<consumer id="dev12" datatype="power" name="Power C" property='$panel/properties[name="powerC"]' collection="false" required="false"  />

		<consumer id="dev13" datatype="power" name="Power Factor A" property='$panel/properties[name="powerFactorA"]' collection="false" required="false"  />
		<consumer id="dev14" datatype="power" name="Power Factor B" property='$panel/properties[name="powerFactorB"]' collection="false" required="false"  />
		<consumer id="dev15" datatype="power" name="Power Factor C" property='$panel/properties[name="powerFactorC"]' collection="false" required="false"  />


		<varbinding vars="x">
			<property>$panel/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$panel/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="name">
			<property>$panel/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="maxPower">
			<property>$panel/properties[name="circuits"]/children/properties[name="maxPower"]</property>
			<value>maxPower</value>
		</varbinding>
		<varbinding vars="midPower">
			<property>$panel/properties[name="circuits"]/children/properties[name="midPower"]</property>
			<value>midPower</value>
		</varbinding>
		<varbinding vars="lowPower">
			<property>$panel/properties[name="circuits"]/children/properties[name="lowPower"]</property>
			<value>lowPower</value>
		</varbinding>
		<varbinding vars="maxCurrent">
			<property>$panel/properties[name="circuits"]/children/properties[name="maxCurrent"]</property>
			<value>maxCurrent</value>
		</varbinding>
		<varbinding vars="midCurrent">
			<property>$panel/properties[name="circuits"]/children/properties[name="midCurrent"]</property>
			<value>midCurrent</value>
		</varbinding>
		<varbinding vars="lowCurrent">
			<property>$panel/properties[name="circuits"]/children/properties[name="lowCurrent"]</property>
			<value>lowCurrent</value>
		</varbinding>
		<varbinding vars="midPowerFactor">
			<property>$panel/properties[name="circuits"]/children/properties[name="midPowerFactor"]</property>
			<value>midPowerFactor</value>
		</varbinding>
		<varbinding vars="lowPowerFactor">
			<property>$panel/properties[name="circuits"]/children/properties[name="lowPowerFactor"]</property>
			<value>lowPowerFactor</value>
		</varbinding>
	</component>

	<component type="generic-bcms-custom-42" classes="placeable" grouppath="Power &amp; Energy;Wired;BCMS" filters="Power">
		<display>
			<description>Integration-only BCMS with 42 Channels.</description>
			<name>Generic 42-Channel BCMS Panel</name>
			<shape>bcms_custom</shape>
		</display>

		<object type="panel" as="panel">
			<dlid>DLID:-1</dlid>
			<property name="circuits">
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>1</value></property>
					<property name="name"><value>Circuit 1</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>2</value></property>
					<property name="name"><value>Circuit 2</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>3</value></property>
					<property name="name"><value>Circuit 3</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>4</value></property>
					<property name="name"><value>Circuit 4</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>5</value></property>
					<property name="name"><value>Circuit 5</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>6</value></property>
					<property name="name"><value>Circuit 6</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>7</value></property>
					<property name="name"><value>Circuit 7</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>8</value></property>
					<property name="name"><value>Circuit 8</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>9</value></property>
					<property name="name"><value>Circuit 9</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>10</value></property>
					<property name="name"><value>Circuit 10</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>11</value></property>
					<property name="name"><value>Circuit 11</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>12</value></property>
					<property name="name"><value>Circuit 12</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>13</value></property>
					<property name="name"><value>Circuit 13</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>14</value></property>
					<property name="name"><value>Circuit 14</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>15</value></property>
					<property name="name"><value>Circuit 15</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>16</value></property>
					<property name="name"><value>Circuit 16</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>17</value></property>
					<property name="name"><value>Circuit 17</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>18</value></property>
					<property name="name"><value>Circuit 18</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>19</value></property>
					<property name="name"><value>Circuit 19</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>20</value></property>
					<property name="name"><value>Circuit 20</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>21</value></property>
					<property name="name"><value>Circuit 21</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>22</value></property>
					<property name="name"><value>Circuit 22</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>23</value></property>
					<property name="name"><value>Circuit 23</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>24</value></property>
					<property name="name"><value>Circuit 24</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>25</value></property>
					<property name="name"><value>Circuit 25</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>26</value></property>
					<property name="name"><value>Circuit 26</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>27</value></property>
					<property name="name"><value>Circuit 27</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>28</value></property>
					<property name="name"><value>Circuit 28</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>29</value></property>
					<property name="name"><value>Circuit 29</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>30</value></property>
					<property name="name"><value>Circuit 30</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>31</value></property>
					<property name="name"><value>Circuit 31</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>32</value></property>
					<property name="name"><value>Circuit 32</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>33</value></property>
					<property name="name"><value>Circuit 33</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>34</value></property>
					<property name="name"><value>Circuit 34</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>35</value></property>
					<property name="name"><value>Circuit 35</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>36</value></property>
					<property name="name"><value>Circuit 36</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>37</value></property>
					<property name="name"><value>Circuit 37</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>38</value></property>
					<property name="name"><value>Circuit 38</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>39</value></property>
					<property name="name"><value>Circuit 39</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>40</value></property>
					<property name="name"><value>Circuit 40</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>41</value></property>
					<property name="name"><value>Circuit 41</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>
				<object type="circuit">
					<dlid>DLID:-1</dlid>
					<property name="num"><value>42</value></property>
					<property name="name"><value>Circuit 42</value></property>
					<property name="maxPowerFactor"><value>1.0</value></property>
				</object>

			</property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Generic 42-Channel BCMS Panel</property>
		<property name="x" display="x" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="y" display="y" type="java.lang.Double" editable="true" displayed="true" dimension="distance">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Generic 42-Channel BCMS Panel</property>

		<property name="maxCurrent" display="Current Design Limit (A)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="midCurrent" display="Current Warning Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="lowCurrent" display="Current Normal Operational Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="maxPower" display="Power Design Limit (kW)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="midPower" display="Power Warning Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="lowPower" display="Power Normal Operational Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="lowPowerFactor" display="Power Factor Warning Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="midPowerFactor" display="Power Factor Normal Operational Threshold (%)" type="java.lang.Double" editable="true" displayed="true">0.0</property>

		<producer id="panel" datatype="panel" name="Panel" object='$panel' exclusive='true'/>

		<conducer id="CircuitPhase01" name="Circuit 1" >
			<producer id="circuit1" datatype="circuit" name="Circuit 1" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]' />
			<consumer id="phase_for_circuit1" datatype="phase" name="Circuit 1" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase02" name="Circuit 2" >
			<producer id="circuit2" datatype="circuit" name="Circuit 2" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]' />
			<consumer id="phase_for_circuit2" datatype="phase" name="Circuit 2" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase03" name="Circuit 3" >
			<producer id="circuit3" datatype="circuit" name="Circuit 3" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]' />
			<consumer id="phase_for_circuit3" datatype="phase" name="Circuit 3" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase04" name="Circuit 4" >
			<producer id="circuit4" datatype="circuit" name="Circuit 4" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]' />
			<consumer id="phase_for_circuit4" datatype="phase" name="Circuit 4" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase05" name="Circuit 5" >
			<producer id="circuit5" datatype="circuit" name="Circuit 5" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]' />
			<consumer id="phase_for_circuit5" datatype="phase" name="Circuit 5" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase06" name="Circuit 6" >
			<producer id="circuit6" datatype="circuit" name="Circuit 6" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]' />
			<consumer id="phase_for_circuit6" datatype="phase" name="Circuit 6" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase07" name="Circuit 7" >
			<producer id="circuit7" datatype="circuit" name="Circuit 7" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]' />
			<consumer id="phase_for_circuit7" datatype="phase" name="Circuit 7" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase08" name="Circuit 8" >
			<producer id="circuit8" datatype="circuit" name="Circuit 8" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]' />
			<consumer id="phase_for_circuit8" datatype="phase" name="Circuit 8" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase09" name="Circuit 9" >
			<producer id="circuit9" datatype="circuit" name="Circuit 9" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]' />
			<consumer id="phase_for_circuit9" datatype="phase" name="Circuit 9" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase10" name="Circuit 10" >
			<producer id="circuit10" datatype="circuit" name="Circuit 10" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]' />
			<consumer id="phase_for_circuit10" datatype="phase" name="Circuit 10" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase11" name="Circuit 11" >
			<producer id="circuit11" datatype="circuit" name="Circuit 11" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]' />
			<consumer id="phase_for_circuit11" datatype="phase" name="Circuit 11" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase12" name="Circuit 12" >
			<producer id="circuit12" datatype="circuit" name="Circuit 12" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]' />
			<consumer id="phase_for_circuit12" datatype="phase" name="Circuit 12" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase13" name="Circuit 13" >
			<producer id="circuit13" datatype="circuit" name="Circuit 13" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="13"]]' />
			<consumer id="phase_for_circuit13" datatype="phase" name="Circuit 13" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="13"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase14" name="Circuit 14" >
			<producer id="circuit14" datatype="circuit" name="Circuit 14" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="14"]]' />
			<consumer id="phase_for_circuit14" datatype="phase" name="Circuit 14" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="14"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase15" name="Circuit 15" >
			<producer id="circuit15" datatype="circuit" name="Circuit 15" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="15"]]' />
			<consumer id="phase_for_circuit15" datatype="phase" name="Circuit 15" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="15"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase16" name="Circuit 16" >
			<producer id="circuit16" datatype="circuit" name="Circuit 16" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="16"]]' />
			<consumer id="phase_for_circuit16" datatype="phase" name="Circuit 16" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="16"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase17" name="Circuit 17" >
			<producer id="circuit17" datatype="circuit" name="Circuit 17" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="17"]]' />
			<consumer id="phase_for_circuit17" datatype="phase" name="Circuit 17" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="17"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase18" name="Circuit 18" >
			<producer id="circuit18" datatype="circuit" name="Circuit 18" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="18"]]' />
			<consumer id="phase_for_circuit18" datatype="phase" name="Circuit 18" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="18"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase19" name="Circuit 19" >
			<producer id="circuit19" datatype="circuit" name="Circuit 19" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="19"]]' />
			<consumer id="phase_for_circuit19" datatype="phase" name="Circuit 19" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="19"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase20" name="Circuit 20" >
			<producer id="circuit20" datatype="circuit" name="Circuit 20" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="20"]]' />
			<consumer id="phase_for_circuit20" datatype="phase" name="Circuit 20" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="20"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase21" name="Circuit 21" >
			<producer id="circuit21" datatype="circuit" name="Circuit 21" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="21"]]' />
			<consumer id="phase_for_circuit21" datatype="phase" name="Circuit 21" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="21"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase22" name="Circuit 22" >
			<producer id="circuit22" datatype="circuit" name="Circuit 22" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="22"]]' />
			<consumer id="phase_for_circuit22" datatype="phase" name="Circuit 22" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="22"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase23" name="Circuit 23" >
			<producer id="circuit23" datatype="circuit" name="Circuit 23" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="23"]]' />
			<consumer id="phase_for_circuit23" datatype="phase" name="Circuit 23" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="23"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase24" name="Circuit 24" >
			<producer id="circuit24" datatype="circuit" name="Circuit 24" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="24"]]' />
			<consumer id="phase_for_circuit24" datatype="phase" name="Circuit 24" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="24"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase25" name="Circuit 25" >
			<producer id="circuit25" datatype="circuit" name="Circuit 25" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="25"]]' />
			<consumer id="phase_for_circuit25" datatype="phase" name="Circuit 25" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="25"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase26" name="Circuit 26" >
			<producer id="circuit26" datatype="circuit" name="Circuit 26" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="26"]]' />
			<consumer id="phase_for_circuit26" datatype="phase" name="Circuit 26" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="26"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase27" name="Circuit 27" >
			<producer id="circuit27" datatype="circuit" name="Circuit 27" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="27"]]' />
			<consumer id="phase_for_circuit27" datatype="phase" name="Circuit 27" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="27"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase28" name="Circuit 28" >
			<producer id="circuit28" datatype="circuit" name="Circuit 28" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="28"]]' />
			<consumer id="phase_for_circuit28" datatype="phase" name="Circuit 28" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="28"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase29" name="Circuit 29" >
			<producer id="circuit29" datatype="circuit" name="Circuit 29" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="29"]]' />
			<consumer id="phase_for_circuit29" datatype="phase" name="Circuit 29" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="29"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase30" name="Circuit 30" >
			<producer id="circuit30" datatype="circuit" name="Circuit 30" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="30"]]' />
			<consumer id="phase_for_circuit30" datatype="phase" name="Circuit 30" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="30"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase31" name="Circuit 31" >
			<producer id="circuit31" datatype="circuit" name="Circuit 31" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="31"]]' />
			<consumer id="phase_for_circuit31" datatype="phase" name="Circuit 31" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="31"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase32" name="Circuit 32" >
			<producer id="circuit32" datatype="circuit" name="Circuit 32" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="32"]]' />
			<consumer id="phase_for_circuit32" datatype="phase" name="Circuit 32" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="32"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase33" name="Circuit 33" >
			<producer id="circuit33" datatype="circuit" name="Circuit 33" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="33"]]' />
			<consumer id="phase_for_circuit33" datatype="phase" name="Circuit 33" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="33"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase34" name="Circuit 34" >
			<producer id="circuit34" datatype="circuit" name="Circuit 34" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="34"]]' />
			<consumer id="phase_for_circuit34" datatype="phase" name="Circuit 34" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="34"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase35" name="Circuit 35" >
			<producer id="circuit35" datatype="circuit" name="Circuit 35" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="35"]]' />
			<consumer id="phase_for_circuit35" datatype="phase" name="Circuit 35" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="35"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase36" name="Circuit 36" >
			<producer id="circuit36" datatype="circuit" name="Circuit 36" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="36"]]' />
			<consumer id="phase_for_circuit36" datatype="phase" name="Circuit 36" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="36"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase37" name="Circuit 37" >
			<producer id="circuit37" datatype="circuit" name="Circuit 37" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="37"]]' />
			<consumer id="phase_for_circuit37" datatype="phase" name="Circuit 37" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="37"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase38" name="Circuit 38" >
			<producer id="circuit38" datatype="circuit" name="Circuit 38" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="38"]]' />
			<consumer id="phase_for_circuit38" datatype="phase" name="Circuit 38" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="38"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase39" name="Circuit 39" >
			<producer id="circuit39" datatype="circuit" name="Circuit 39" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="39"]]' />
			<consumer id="phase_for_circuit39" datatype="phase" name="Circuit 39" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="39"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase40" name="Circuit 40" >
			<producer id="circuit40" datatype="circuit" name="Circuit 40" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="40"]]' />
			<consumer id="phase_for_circuit40" datatype="phase" name="Circuit 40" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="40"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase41" name="Circuit 41" >
			<producer id="circuit41" datatype="circuit" name="Circuit 41" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="41"]]' />
			<consumer id="phase_for_circuit41" datatype="phase" name="Circuit 41" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="41"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>

		<conducer id="CircuitPhase42" name="Circuit 42" >
			<producer id="circuit42" datatype="circuit" name="Circuit 42" object='$panel/properties[name="circuits"]/children[properties[name="num"][value="42"]]' />
			<consumer id="phase_for_circuit42" datatype="phase" name="Circuit 42" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="42"]]/properties[name="phase"]'  collection="false" required="false"  />
		</conducer>


		<consumer id="circuit1current" datatype="power" name="Circuit 1 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit2current" datatype="power" name="Circuit 2 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit3current" datatype="power" name="Circuit 3 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit4current" datatype="power" name="Circuit 4 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit5current" datatype="power" name="Circuit 5 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit6current" datatype="power" name="Circuit 6 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit7current" datatype="power" name="Circuit 7 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit8current" datatype="power" name="Circuit 8 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit9current" datatype="power" name="Circuit 9 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit10current" datatype="power" name="Circuit 10 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit11current" datatype="power" name="Circuit 11 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit12current" datatype="power" name="Circuit 12 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit13current" datatype="power" name="Circuit 13 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="13"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit14current" datatype="power" name="Circuit 14 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="14"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit15current" datatype="power" name="Circuit 15 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="15"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit16current" datatype="power" name="Circuit 16 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="16"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit17current" datatype="power" name="Circuit 17 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="17"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit18current" datatype="power" name="Circuit 18 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="18"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit19current" datatype="power" name="Circuit 19 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="19"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit20current" datatype="power" name="Circuit 20 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="20"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit21current" datatype="power" name="Circuit 21 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="21"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit22current" datatype="power" name="Circuit 22 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="22"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit23current" datatype="power" name="Circuit 23 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="23"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit24current" datatype="power" name="Circuit 24 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="24"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit25current" datatype="power" name="Circuit 25 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="25"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit26current" datatype="power" name="Circuit 26 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="26"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit27current" datatype="power" name="Circuit 27 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="27"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit28current" datatype="power" name="Circuit 28 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="28"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit29current" datatype="power" name="Circuit 29 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="29"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit30current" datatype="power" name="Circuit 30 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="30"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit31current" datatype="power" name="Circuit 31 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="31"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit32current" datatype="power" name="Circuit 32 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="32"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit33current" datatype="power" name="Circuit 33 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="33"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit34current" datatype="power" name="Circuit 34 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="34"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit35current" datatype="power" name="Circuit 35 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="35"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit36current" datatype="power" name="Circuit 36 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="36"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit37current" datatype="power" name="Circuit 37 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="37"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit38current" datatype="power" name="Circuit 38 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="38"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit39current" datatype="power" name="Circuit 39 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="39"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit40current" datatype="power" name="Circuit 40 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="40"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit41current" datatype="power" name="Circuit 41 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="41"]]/properties[name="current"]' collection="false" required="false"  />
		<consumer id="circuit42current" datatype="power" name="Circuit 42 Current" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="42"]]/properties[name="current"]' collection="false" required="false"  />

		<consumer id="circuit1power" datatype="power" name="Circuit 1 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit2power" datatype="power" name="Circuit 2 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit3power" datatype="power" name="Circuit 3 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit4power" datatype="power" name="Circuit 4 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit5power" datatype="power" name="Circuit 5 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit6power" datatype="power" name="Circuit 6 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit7power" datatype="power" name="Circuit 7 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit8power" datatype="power" name="Circuit 8 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit9power" datatype="power" name="Circuit 9 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit10power" datatype="power" name="Circuit 10 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit11power" datatype="power" name="Circuit 11 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit12power" datatype="power" name="Circuit 12 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit13power" datatype="power" name="Circuit 13 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="13"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit14power" datatype="power" name="Circuit 14 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="14"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit15power" datatype="power" name="Circuit 15 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="15"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit16power" datatype="power" name="Circuit 16 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="16"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit17power" datatype="power" name="Circuit 17 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="17"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit18power" datatype="power" name="Circuit 18 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="18"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit19power" datatype="power" name="Circuit 19 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="19"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit20power" datatype="power" name="Circuit 20 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="20"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit21power" datatype="power" name="Circuit 21 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="21"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit22power" datatype="power" name="Circuit 22 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="22"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit23power" datatype="power" name="Circuit 23 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="23"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit24power" datatype="power" name="Circuit 24 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="24"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit25power" datatype="power" name="Circuit 25 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="25"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit26power" datatype="power" name="Circuit 26 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="26"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit27power" datatype="power" name="Circuit 27 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="27"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit28power" datatype="power" name="Circuit 28 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="28"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit29power" datatype="power" name="Circuit 29 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="29"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit30power" datatype="power" name="Circuit 30 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="30"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit31power" datatype="power" name="Circuit 31 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="31"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit32power" datatype="power" name="Circuit 32 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="32"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit33power" datatype="power" name="Circuit 33 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="33"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit34power" datatype="power" name="Circuit 34 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="34"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit35power" datatype="power" name="Circuit 35 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="35"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit36power" datatype="power" name="Circuit 36 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="36"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit37power" datatype="power" name="Circuit 37 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="37"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit38power" datatype="power" name="Circuit 38 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="38"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit39power" datatype="power" name="Circuit 39 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="39"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit40power" datatype="power" name="Circuit 40 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="40"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit41power" datatype="power" name="Circuit 41 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="41"]]/properties[name="power"]' collection="false" required="false"  />
		<consumer id="circuit42power" datatype="power" name="Circuit 42 Power" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="42"]]/properties[name="power"]' collection="false" required="false"  />

		<consumer id="circuit1powerfactor" datatype="power" name="Circuit 1 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="1"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit2powerfactor" datatype="power" name="Circuit 2 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="2"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit3powerfactor" datatype="power" name="Circuit 3 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="3"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit4powerfactor" datatype="power" name="Circuit 4 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="4"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit5powerfactor" datatype="power" name="Circuit 5 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="5"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit6powerfactor" datatype="power" name="Circuit 6 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="6"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit7powerfactor" datatype="power" name="Circuit 7 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="7"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit8powerfactor" datatype="power" name="Circuit 8 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="8"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit9powerfactor" datatype="power" name="Circuit 9 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="9"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit10powerfactor" datatype="power" name="Circuit 10 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="10"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit11powerfactor" datatype="power" name="Circuit 11 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="11"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit12powerfactor" datatype="power" name="Circuit 12 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="12"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit13powerfactor" datatype="power" name="Circuit 13 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="13"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit14powerfactor" datatype="power" name="Circuit 14 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="14"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit15powerfactor" datatype="power" name="Circuit 15 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="15"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit16powerfactor" datatype="power" name="Circuit 16 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="16"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit17powerfactor" datatype="power" name="Circuit 17 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="17"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit18powerfactor" datatype="power" name="Circuit 18 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="18"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit19powerfactor" datatype="power" name="Circuit 19 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="19"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit20powerfactor" datatype="power" name="Circuit 20 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="20"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit21powerfactor" datatype="power" name="Circuit 21 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="21"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit22powerfactor" datatype="power" name="Circuit 22 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="22"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit23powerfactor" datatype="power" name="Circuit 23 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="23"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit24powerfactor" datatype="power" name="Circuit 24 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="24"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit25powerfactor" datatype="power" name="Circuit 25 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="25"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit26powerfactor" datatype="power" name="Circuit 26 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="26"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit27powerfactor" datatype="power" name="Circuit 27 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="27"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit28powerfactor" datatype="power" name="Circuit 28 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="28"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit29powerfactor" datatype="power" name="Circuit 29 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="29"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit30powerfactor" datatype="power" name="Circuit 30 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="30"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit31powerfactor" datatype="power" name="Circuit 31 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="31"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit32powerfactor" datatype="power" name="Circuit 32 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="32"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit33powerfactor" datatype="power" name="Circuit 33 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="33"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit34powerfactor" datatype="power" name="Circuit 34 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="34"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit35powerfactor" datatype="power" name="Circuit 35 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="35"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit36powerfactor" datatype="power" name="Circuit 36 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="36"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit37powerfactor" datatype="power" name="Circuit 37 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="37"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit38powerfactor" datatype="power" name="Circuit 38 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="38"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit39powerfactor" datatype="power" name="Circuit 39 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="39"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit40powerfactor" datatype="power" name="Circuit 40 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="40"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit41powerfactor" datatype="power" name="Circuit 41 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="41"]]/properties[name="powerFactor"]' collection="false" required="false"  />
		<consumer id="circuit42powerfactor" datatype="power" name="Circuit 42 Power Factor" property='$panel/properties[name="circuits"]/children[properties[name="num"][value="42"]]/properties[name="powerFactor"]' collection="false" required="false"  />

		<consumer id="dev1" datatype="power" name="Current A" property='$panel/properties[name="currentA"]'  collection="false" required="false"  />
		<consumer id="dev2" datatype="power" name="Current B" property='$panel/properties[name="currentB"]'  collection="false" required="false"  />
		<consumer id="dev3" datatype="power" name="Current C" property='$panel/properties[name="currentC"]'  collection="false" required="false"  />

		<consumer id="dev4" datatype="power" name="Voltage AB" property='$panel/properties[name="voltageAB"]'  collection="false" required="false"  />
		<consumer id="dev5" datatype="power" name="Voltage BC" property='$panel/properties[name="voltageBC"]'  collection="false" required="false"  />
		<consumer id="dev6" datatype="power" name="Voltage CA" property='$panel/properties[name="voltageCA"]'  collection="false" required="false"  />

		<consumer id="dev7" datatype="power" name="Voltage AN" property='$panel/properties[name="voltageAN"]'  collection="false" required="false"  />
		<consumer id="dev8" datatype="power" name="Voltage BN" property='$panel/properties[name="voltageBN"]'  collection="false" required="false"  />
		<consumer id="dev9" datatype="power" name="Voltage CN" property='$panel/properties[name="voltageCN"]' collection="false" required="false"  />

		<consumer id="dev10" datatype="power" name="Power A" property='$panel/properties[name="powerA"]' collection="false" required="false"  />
		<consumer id="dev11" datatype="power" name="Power B" property='$panel/properties[name="powerB"]' collection="false" required="false"  />
		<consumer id="dev12" datatype="power" name="Power C" property='$panel/properties[name="powerC"]' collection="false" required="false"  />

		<consumer id="dev13" datatype="power" name="Power Factor A" property='$panel/properties[name="powerFactorA"]' collection="false" required="false"  />
		<consumer id="dev14" datatype="power" name="Power Factor B" property='$panel/properties[name="powerFactorB"]' collection="false" required="false"  />
		<consumer id="dev15" datatype="power" name="Power Factor C" property='$panel/properties[name="powerFactorC"]' collection="false" required="false"  />


		<varbinding vars="x">
			<property>$panel/properties[name="x"]</property>
			<value>x</value>
		</varbinding>
		<varbinding vars="y">
			<property>$panel/properties[name="y"]</property>
			<value>y</value>
		</varbinding>
		<varbinding vars="name">
			<property>$panel/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
		<varbinding vars="maxPower">
			<property>$panel/properties[name="circuits"]/children/properties[name="maxPower"]</property>
			<value>maxPower</value>
		</varbinding>
		<varbinding vars="midPower">
			<property>$panel/properties[name="circuits"]/children/properties[name="midPower"]</property>
			<value>midPower</value>
		</varbinding>
		<varbinding vars="lowPower">
			<property>$panel/properties[name="circuits"]/children/properties[name="lowPower"]</property>
			<value>lowPower</value>
		</varbinding>
		<varbinding vars="maxCurrent">
			<property>$panel/properties[name="circuits"]/children/properties[name="maxCurrent"]</property>
			<value>maxCurrent</value>
		</varbinding>
		<varbinding vars="midCurrent">
			<property>$panel/properties[name="circuits"]/children/properties[name="midCurrent"]</property>
			<value>midCurrent</value>
		</varbinding>
		<varbinding vars="lowCurrent">
			<property>$panel/properties[name="circuits"]/children/properties[name="lowCurrent"]</property>
			<value>lowCurrent</value>
		</varbinding>
		<varbinding vars="midPowerFactor">
			<property>$panel/properties[name="circuits"]/children/properties[name="midPowerFactor"]</property>
			<value>midPowerFactor</value>
		</varbinding>
		<varbinding vars="lowPowerFactor">
			<property>$panel/properties[name="circuits"]/children/properties[name="lowPowerFactor"]</property>
			<value>lowPowerFactor</value>
		</varbinding>
	</component>

</componentlib>