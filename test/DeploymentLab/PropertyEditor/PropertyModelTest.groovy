package DeploymentLab.PropertyEditor

import DeploymentLab.Model.ObjectModel

import static DeploymentLab.TestUtils.assertAlmostEquals

import org.junit.*

import com.synapsense.util.unitconverter.UnitConverter
import DeploymentLab.CentralCatalogue
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentProperty
import DeploymentLab.Model.DLComponent
import DeploymentLab.SelectionChangeEvent
import DeploymentLab.TestUtils

/**
 * Unit tests for the PropertyModel class.
 */
class PropertyModelTest {

    static final String TEST_COMPONENT   = 'rack-control-rearexhaust-nsf-rt-thermanode2'
    static final String TEST_COMPONENT_2 = 'crah-single-plenumrated-thermanode'
    static final String NONEDITABLE_PROP = 'configuration'
    static final String EDITABLE_PROP    = 'name'
    static final String CONVERTABLE_PROP = 'depth'

    // Cheating way to use this method without extending GroovyTestCase.
    final shouldFail = new GroovyTestCase().&shouldFail

    ObjectModel objModel = null
    ComponentModel compModel = null
    PropertyModel  propModel = null

    /**
     * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
     */
    @BeforeClass
    static void classSetup() {
        // Turn on error logging to help when something fails internal to a method we are testing.
        TestUtils.enableLogging( DeploymentLab.channellogger.LogLevel.ERROR )
    }

    /**
     * One-time cleanup after all tests.
     */
    /*@AfterClass
    static void classTearDown() {
    }*/

    /**
     * Initialization called before each test.
     */
    @Before
    void setup() {
        (objModel,compModel) = TestUtils.initModels()

        propModel = new PropertyModel( compModel, CentralCatalogue.getUndoBuffer(), null )
        assert propModel

        // Creating a PropertyTable is a pain and a UI thing that we don't want to deal with right now.
        propModel.setTable( null )
    }

    /**
     * Cleanup after each test.
     */
    @After
    void tearDown() {
        propModel = null
        objModel = null
        compModel = null
    }

    /**
     * Checks the initial state of the model. Most of the methods should just be valid when there is a component
     * selected. Some still return a valid response in this state. Others return null or throw exceptions.
     * Since this is a class that has existed for a long time prior to this test case, we will verify that they
     * behave at the time this test was written so that this behavior isn't changed without warning, just in case
     * something else depends on that specific behavior.
     */
    @Test
    void verifyInitialState() {
        assert !propModel.systemConverter
        assert !propModel.undoOperationInProgress()

        assertModelBasics( propModel, 0, 0 )

        assert propModel.getCellClassName(0,0)
        assert !propModel.isCellEditable(0,0)
        assert !propModel.getCellValues(0,0)
        assert !propModel.getProperty(0)
        assert !propModel.getValueAt(0,0)

        shouldFail( NoSuchElementException ) { propModel.component }
        shouldFail( NullPointerException ) { propModel.isCellEditable(0,1) }
        shouldFail( NullPointerException ) { propModel.getCellClassName(0,1) }
        shouldFail( NullPointerException ) { propModel.getCellValues(0,1) }
    }

    /**
     * Tests the getDescriptionProperty method.
     */
    @Test
    void testGetDescriptionProperty() {
        DLComponent comp = compModel.newComponent( TEST_COMPONENT )
        assert comp

        ComponentProperty cprop = propModel.getDescriptionProperty( comp )
        assert cprop
        assert comp.description == cprop.value
    }

    /**
     * Tests the property model's handling of a single selection. The state should match the selected item.
     */
    @Test
    void testSingleSelection() {

        // Create a selection event with one selected item.
        DLComponent comp = compModel.newComponent( TEST_COMPONENT )
        SelectionChangeEvent event = new SelectionChangeEvent()
        event.add( comp )

        propModel.selectionChanged( event )

        // The expected row cnt is +1 because "Description" is not a component prop but is a property in the model.
        assertModelBasics( propModel, 1, (comp.listVisiblePropertyNames().size() + 1) )
        assert comp == propModel.component

        for( i in 0 ..< propModel.rowCount ) {
            assertComponent( propModel, i, comp.getComponentProperty( propModel.getProperty(i).name ) )
        }
    }

    /**
     * Tests the property model's handling of selections changing.
     */
    @Test
    void testSelectionChanges() {
        
        // Create a selection event with one selected item.
        DLComponent comp1 = compModel.newComponent( TEST_COMPONENT )
        SelectionChangeEvent event = new SelectionChangeEvent()
        event.add( comp1 )

        propModel.selectionChanged( event )

        assertModelBasics( propModel, 1, (comp1.listVisiblePropertyNames().size() + 1) )
        assert comp1 == propModel.component

        // Add another component as a separate event.
        DLComponent comp2 = compModel.newComponent( TEST_COMPONENT )
        event = new SelectionChangeEvent()
        event.add( comp2 )

        propModel.selectionChanged( event )

        assertModelBasics( propModel, 2, (comp2.listVisiblePropertyNames().size() + 1) )

        // Remove the first one.
        event = new SelectionChangeEvent()
        event.remove( comp1 )

        propModel.selectionChanged( event )

        assertModelBasics( propModel, 1, (comp2.listVisiblePropertyNames().size() + 1) )
        assert comp2 == propModel.component
    }

    /**
     * Tests that the model properly handles 'description'. This is done separately than other property change tests
     * because description is not treated like a normal property.
     */
    @Test
    void testChangeDescription() {

        // Create a selection event with one selected item.
        DLComponent comp = compModel.newComponent( TEST_COMPONENT )
        SelectionChangeEvent event = new SelectionChangeEvent()
        event.add( comp )

        propModel.selectionChanged( event )

        // Find what row has description.
        int row = findRow( propModel, PropertyModel.DESCRIPTION_PSEUDO_PROPERTY )

        // Try to change it. Should silently fail and a get will still be the original.
        def origVal = propModel.getValueAt( row, 1 )
        assert origVal

        propModel.setValueAt( 'New Value', row, 1 )
        assert origVal == propModel.getValueAt( row, 1 )
    }

    /**
     * Tests that the property model properly handles a non-editable property being changed.
     * NOTE: At the time this test was created, the model did not stop this from happening. Since it has behaved that
     * way for a long time, this test expects the same behavior and acts as a warning if that behavior changes since
     * something may depend on it.
     */
    @Test
    void testChangeNoneditableProp() {

        // Create a selection event with one selected item.
        DLComponent comp = compModel.newComponent( TEST_COMPONENT )
        SelectionChangeEvent event = new SelectionChangeEvent()
        event.add( comp )

        propModel.selectionChanged( event )

        // Find what row has the property.
        int row = findRow( propModel, NONEDITABLE_PROP )

        // Try to change it. The model currently does not stop this and makes the change. TODO: Should it?
        def origVal = propModel.getValueAt( row, 1 )
        assert origVal

        propModel.setValueAt( 'New Value', row, 1 )
        assert 'New Value' == propModel.getValueAt( row, 1 )
        //assert origVal == propModel.getValueAt( row, VALUE_COLUMN )
    }

    /**
     * Tests that the property model properly handles a property being changed.
     */
    @Test
    void testChangeEditableProp() {

        // Create a selection event with one selected item.
        DLComponent comp = compModel.newComponent( TEST_COMPONENT )
        SelectionChangeEvent event = new SelectionChangeEvent()
        event.add( comp )

        propModel.selectionChanged( event )

        // Find what row has the property.
        int row = findRow( propModel, EDITABLE_PROP )

        // Change the value and verify that it worked.
        def origVal = propModel.getValueAt( row, 1 )
        assert origVal

        propModel.setValueAt( 'New Value', row, 1 )
        assert 'New Value' == propModel.getValueAt( row, 1 )

        // Change it back just for gits n shiggles.
        propModel.setValueAt( origVal, row, 1 )
        assert origVal == propModel.getValueAt( row, 1 )
    }

    /**
     * Tests the use of a converter when setting and getting properties.
     */
    @Test
    void testUnitConverter() {

        // Set a unit converter for the Property Model to use.
        propModel.setUnitSystem( CentralCatalogue.INSTANCE.unitSystems.getSystemConverter('Imperial US', 'SI'),
                                 CentralCatalogue.INSTANCE.unitSystems )

        assert propModel.systemConverter
        assert 'Imperial US' == propModel.systemConverter.baseSystemName
        assert 'SI' == propModel.systemConverter.targetSystemName

        assert propModel.reverseSystemConverter
        assert 'SI' == propModel.reverseSystemConverter.baseSystemName
        assert 'Imperial US' == propModel.reverseSystemConverter.targetSystemName

        // Create a selection event with one selected item.
        DLComponent comp = compModel.newComponent( TEST_COMPONENT )
        SelectionChangeEvent event = new SelectionChangeEvent()
        event.add( comp )

        propModel.selectionChanged( event )

        // Find what row has the property that would be affected by a converter.
        int row = findRow( propModel, CONVERTABLE_PROP )

        String dimension = propModel.getProperty( row ).dimensionName
        assert dimension

        UnitConverter conv = propModel.systemConverter.getConverter( dimension )

        // Get the original value from the component and make sure we get the converted value from the model.
        double origValUS = comp.getPropertyValue( CONVERTABLE_PROP )
        double origValSI = conv.convert( origValUS )
        assertAlmostEquals( origValSI, propModel.getValueAt( row, 1 ) )

        // Change it. Pass in the value as a String since that's how the current UI does it.
        double newValUS = 55.0
        double newValSI = propModel.systemConverter.getConverter( dimension ).convert( newValUS )

        propModel.setValueAt( "$newValSI", row, 1 )

        // Make sure we get the converted value from the model but the Imperial US value is stored in the component.
        assertAlmostEquals( newValSI, propModel.getValueAt( row, 1 ) )
        assertAlmostEquals( newValUS, propModel.component.getPropertyValue( CONVERTABLE_PROP ) )

        // One more test to make sure it handles an object of a non-String type since it theoretically could be either.
        propModel.setValueAt( origValSI, row, 1 )
        assertAlmostEquals( origValSI, propModel.getValueAt( row, 1 ) )
        assertAlmostEquals( origValUS, propModel.component.getPropertyValue( CONVERTABLE_PROP ) )
    }

    /**
     * Tests the property model's handling of multiple selections of like components.
     */
    @Test
    void testMultiSelectSameCompSameProps() {

        // Create a few components of the same type.
        List<DLComponent> comps = []
        SelectionChangeEvent event = new SelectionChangeEvent()
        for( i in 0..2 ) {
            comps += compModel.newComponent( TEST_COMPONENT )
            event.add( comps[i] )
        }

        propModel.selectionChanged( event )

        assertModelBasics( propModel, comps.size(), (comps.first().listVisiblePropertyNames().size() + 1) )

        // Since they are the same, the properties should be just like a single selection.
        for( i in 0 ..< propModel.rowCount ) {
            assertComponent( propModel, i, comps.first().getComponentProperty( propModel.getProperty(i).name ) )
        }
    }

    /**
     * Tests the property model's handling of multiple selections of the same component type with different property
     * values.
     */
    @Test
    void testMultiSelectSameCompDiffProps() {

        // Create a few components of the same type.
        List<DLComponent> comps = []
        SelectionChangeEvent event = new SelectionChangeEvent()
        for( i in 0..2 ) {
            // Create a component and change a common property in each.
            comps += compModel.newComponent( TEST_COMPONENT )
            comps[i].setPropertyValue( EDITABLE_PROP, "Component $i".toString() )
            event.add( comps[i] )
            assert "Component $i".toString() == comps[i].getPropertyValue( EDITABLE_PROP )
        }

        propModel.selectionChanged( event )

        assertModelBasics( propModel, comps.size(), (comps.first().listVisiblePropertyNames().size() + 1) )

        // Everything should be the same as before, except the property we changed will have no value returned.
        for( i in 0 ..< propModel.rowCount ) {
            // Expect no value for the prop that we changed, so don't blindly use the default expected value.
            ComponentProperty expected = comps.first().getComponentProperty( propModel.getProperty(i).name )

            assertComponent( propModel, i, expected, propModel.getProperty(i).name.equalsIgnoreCase( EDITABLE_PROP ) )
        }
    }

    /**
     * Tests the property model's handling of multiple selections of different component types.
     */
    @Test
    void testMultiSelectDiffComp() {
        // Create a selection event with two different types of items selected.
        List<DLComponent> comps = []

        comps += compModel.newComponent( TEST_COMPONENT )
        comps += compModel.newComponent( TEST_COMPONENT_2 )
        SelectionChangeEvent event = new SelectionChangeEvent()
        comps.each{ event.add( it ) }

        propModel.selectionChanged( event )

        // Expected row cnt should be the intersection of the different components.
        assertModelBasics( propModel, comps.size(),
                           comps.first().listVisiblePropertyNames().intersect( comps.last().listVisiblePropertyNames() ).size() )

        for( i in 0 ..< propModel.rowCount ) {
            for( comp in comps ) {
                // Expect no value for props with different values, so pass-in an empty string to force that part of the check to pass.
                ComponentProperty expected = comp.getComponentProperty( propModel.getProperty(i).name )

                assertComponent( propModel, i, expected, ( propModel.getValueAt( i, 1 ) == '' ) )
            }
        }
    }


    ////////////////////  PRIVATE NON-TEST HELPER METHODS  ////////////////////

    private int findRow( PropertyModel pm, String name ) {
        int row = -1
        for( i in 0 ..< pm.rowCount ) {
            if( pm.getProperty(i).name.equalsIgnoreCase( name ) ) {
                row = i
                break
            }
        }
        assert row >= 0

        return row
    }


    private void assertModelBasics( PropertyModel pm, int selectedCnt, int expectedRowCnt ) {

        assert 2 == pm.columnCount
        for( i in 0 ..< pm.columnCount ) {
            assert pm.getColumnName(i)
        }

        assert pm.getColumnClass(0)
        assert pm.getColumnClass(1)

        assert selectedCnt == pm.numberOfComponents
        assert expectedRowCnt == pm.rowCount
    }


    private void assertComponent( PropertyModel pm, int row, ComponentProperty expected, boolean multiselectMismatch = false ) {

        ComponentProperty actual = pm.getProperty( row )

        // Description is a PITA because it's not a property in the component model, so just skip it for now.
        if( actual.name.equalsIgnoreCase( PropertyModel.DESCRIPTION_PSEUDO_PROPERTY ) ) {
            return
        }

        assert expected, "Property '$actual.name'"

        // Column 0 checks.
        assert !pm.isCellEditable( row, 0 ), "Property '$actual.name'"
        assert !pm.getCellValues( row, 0 ), "Property '$actual.name'"
        assert pm.getCellClassName( row, 0 ), "Property '$actual.name'"
        assert expected.displayName == pm.getValueAt( row, 0 ), "Property '$actual.name'"

        // Column 1 checks.
        assert expected.editable == pm.isCellEditable( row, 1 ), "Property '$actual.name'"
        assert expected.type == pm.getCellClassName( row, 1 ), "Property '$actual.name'"
        assert expected.valueChoices == pm.getCellValues( row, 1 ), "Property '$actual.name'"

        // For a combobox, getValueAt returns the selection string not the index value.
        def valAt = pm.getValueAt( row, 1 )
        if( expected.valueChoices ) {
            valAt = actual.valueForChoice( (String) valAt )
        }

        // The table always has an empty string for mismatches.
        if( multiselectMismatch ) {
            assert valAt == '', "Property '$actual.name'"
        } else {
            assert expected.type == valAt.class.name, "Property '$actual.name'"
            def expectedVal = expected?.value

            // Need to call a different assert for floating point values.
            if( ( expected.type == 'java.lang.Double' ) || ( expected.type == 'java.lang.Float' ) ) {
                assertAlmostEquals( "Property '$actual.name'", (double) expectedVal, (double) valAt )
            } else {
                assert expectedVal == valAt, "Property '$actual.name'"
            }
        }
    }
}
