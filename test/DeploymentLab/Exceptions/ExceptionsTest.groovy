package DeploymentLab.Exceptions

import org.apache.commons.jxpath.JXPathContext

import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Ignore
import org.junit.Test
import org.junit.Assert

import DeploymentLab.Exceptions.*
import DeploymentLab.Model.*
import DeploymentLab.ComponentLibrary
import DeploymentLab.DLProject
import DeploymentLab.TestUtils

/**
 * Unit test for all Custom Exception classes.
 * @author Gabriel Helman
 * @since Mars
 * Date: 3/28/11
 * Time: 2:00 PM
 */
class ExceptionsTest {
	static ObjectModel omodel
	static ComponentModel cmodel

	@BeforeClass
	static void beforeClass() {
		(omodel,cmodel) = TestUtils.initModels()
		def clib = ComponentLibrary.INSTANCE;
		clib.init(cmodel,null,null);
	}

	@Test
	void exceptionConstructorTests(){
		try{
			throw new UnknownComponentException()
		} catch ( Exception e ){
			assert e instanceof UnknownComponentException
		}

		try{
			throw new UnknownComponentException("Bad Component")
		} catch ( Exception e ){
			assert e.getMessage().equals("Bad Component")
			assert e.getCause() == null
		}

		try{
			throw new UnknownComponentException("Bad Component", new NullPointerException())
		} catch ( Exception e ){
			assert e.getMessage().equals("Bad Component")
			assert e.getCause() instanceof NullPointerException
		}

		try{
			throw new UnknownComponentException( new NullPointerException() )
		} catch ( Exception e ){
			assert e.getMessage().equals("java.lang.NullPointerException")
			assert e.getCause() instanceof NullPointerException
		}

		try{
			def e = new DuplicateObjectException("Duplicate")
			throw e
		} catch ( Exception e ){
			assert e.getMessage().equals("Duplicate")
			assert e instanceof DuplicateObjectException
		}

		try{
			throw new IncompatibleTypeException("Duplicate")
		} catch ( Exception e ){
			assert e.getMessage().equals("Duplicate")
			assert e instanceof IncompatibleTypeException
		}

		try{
			throw new DeserializationException("Duplicate")
		} catch ( Exception e ){
			assert e.getMessage().equals("Duplicate")
			assert e instanceof DeserializationException
		}

		try{
			throw new UnknownPropertyTypeException("Duplicate")
		} catch ( Exception e ){
			assert e.getMessage().equals("Duplicate")
			assert e instanceof UnknownPropertyTypeException
		}

		try{
			throw new ConversionNotSupportedException("Image", "HashMap")
		} catch ( Exception e ){
			assert e.getMessage().equals("Cannot convert from 'java.lang.String' to 'HashMap'!")
			assert e instanceof ConversionNotSupportedException
		}

	}


	@Test
	void modelCatastrophieExceptionTests(){
		try{
			throw new ModelCatastrophe("SOMETHING TERRIBLE HAS HAPPENED!")
		} catch ( Error e ){
			assert e.getMessage().equals("SOMETHING TERRIBLE HAS HAPPENED!")
			assert e instanceof ModelCatastrophe
		}

		try{
			throw new ModelCatastrophe("SOMETHING TERRIBLE HAS HAPPENED!", new RuntimeException())
		} catch ( Error e ){
			assert e.getMessage().equals("SOMETHING TERRIBLE HAS HAPPENED!")
			assert e instanceof ModelCatastrophe
			assert e.getCause() instanceof RuntimeException
		}

		try{
			throw new ModelCatastrophe(new RuntimeException())
		} catch ( Error e ){
			assert e instanceof ModelCatastrophe
			assert e.getCause() instanceof RuntimeException
		}

		try{
			throw new ModelCatastrophe()
		} catch ( Error e ){
			assert e instanceof ModelCatastrophe
		}
	}

	@Test(expected=ModelCatastrophe.class)
	void generateModelCatastrophe(){
		// Trying to get the Drawing before it has been added to a parent results in a ModelCatastrophe.
		cmodel.newComponent("pue_main").getDrawing()
	}

	@Test(expected=UnknownComponentException.class)
	void unknownComponentExceptionTest(){
		def shouldBreak = cmodel.newComponent("fakeor")
	}


	@Test(expected=UnknownObjectException.class)
	void UnknownObjectExceptionTests(){
		try{
			throw new UnknownObjectException("SOMETHING TERRIBLE HAS HAPPENED!")
		} catch ( Exception e ){
			assert e.getMessage().equals("SOMETHING TERRIBLE HAS HAPPENED!")
			assert e instanceof UnknownObjectException
		}

		try{
			throw new UnknownObjectException("SOMETHING TERRIBLE HAS HAPPENED!", new RuntimeException())
		} catch ( Exception e ){
			assert e.getMessage().equals("SOMETHING TERRIBLE HAS HAPPENED!")
			assert e instanceof UnknownObjectException
			assert e.getCause() instanceof RuntimeException
		}

		try{
			throw new UnknownObjectException(new RuntimeException())
		} catch ( Exception e ){
			assert e instanceof UnknownObjectException
			assert e.getCause() instanceof RuntimeException
		}

		try{
			throw new UnknownObjectException()
		} catch ( Exception e ){
			assert e instanceof UnknownObjectException
		}

		def shouldBreak = omodel.createObject("o_fakor")
	}


	@Test
	void projectFileCatastropheExceptionTests(){
		try{
			throw new ProjectFileCatastrophe("SOMETHING TERRIBLE HAS HAPPENED!")
		} catch ( Exception e ){
			assert e.getMessage().equals("SOMETHING TERRIBLE HAS HAPPENED!")
			assert e instanceof ProjectFileCatastrophe
		}

		try{
			throw new ProjectFileCatastrophe("SOMETHING TERRIBLE HAS HAPPENED!", new RuntimeException())
		} catch ( Exception e ){
			assert e.getMessage().equals("SOMETHING TERRIBLE HAS HAPPENED!")
			assert e instanceof ProjectFileCatastrophe
			assert e.getCause() instanceof RuntimeException
		}

		try{
			throw new ProjectFileCatastrophe(new RuntimeException())
		} catch ( Exception e ){
			assert e instanceof ProjectFileCatastrophe
			assert e.getCause() instanceof RuntimeException
		}

		try{
			throw new ProjectFileCatastrophe()
		} catch ( Exception e ){
			assert e instanceof ProjectFileCatastrophe
		}
	}

//	@Test(expected=ProjectFileCatastrophe.class)
//	void generateProjectFileCatastrophe(){
//		def p = new DLProject(new File("BAD"))
//	}

	@AfterClass
	static void afterClass() {
		cmodel = null
		omodel = null
		System.gc();
		System.gc();
		println "All Exception Tests Completed"
	}


}
