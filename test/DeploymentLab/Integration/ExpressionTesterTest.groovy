package DeploymentLab.Integration

import org.junit.*;


import org.apache.commons.io.FileUtils

/**
 *
 * @author Gabriel Helman
 * @since
 * Date: 9/19/11
 * Time: 12:16 PM
 */
class ExpressionTesterTest {


	@Test
	void testURLConnection() {
		//def url = new URL("file://good.xml")
		def f = new File("build/testFiles/good.xml")
		def url = f.toURI().toURL()

		URLConnection urlConnection = url.openConnection();
		InputStream input = urlConnection.getInputStream();

		int data = input.read();
		while (data != -1) {
			System.out.print((char) data);
			data = input.read();
		}
		input.close();
	}


	@Test
	void documentAsString(){
		def f = new File("build/testFiles/good.xml")
		def url = f.toURI().toURL()
		def s = ExpressionTester.getDocumentAsString(url.toString(), "", "")
		//println s
		def fileString = FileUtils.readFileToString(f)
		//assert fileString.equals(s)
		def fileXml =  new XmlParser().parseText(fileString)
		def docXML = new XmlParser().parseText(s)
		assert fileXml.reply.result.equals( docXML.reply.result )
		assert docXML.reply.message.equals( fileXml.reply.message )
	}

/*
	@Test
	void modbusExpressionTester(){

		def f = new File("build/testFiles")
		def url = f.toURI().toURL()

		def host = url.toString()
		def page = "good.xml"

		def params = [:]
		//ip=$modbusHost&expr=$encoded&device=$devid&value=$value&pointname=$pointName
		params["ip"] = "1"
		params["expr"] = "read_int16(40000 + 12)"
		params["device"] = "1"
		params["value"] = "1"
		params["pointname"] = "1"

		def username = "admin"
		def pwd = "admin"

		def result = ExpressionTester.test(host, page, params, username, pwd)

		println result
	}
  */


}
