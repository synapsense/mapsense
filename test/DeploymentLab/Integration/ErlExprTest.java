package DeploymentLab.Integration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.FileReader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.*;
import static org.junit.Assert.*;


public class ErlExprTest {

	//public static final String testDir = "../test/expressions";
	public static final String testDir = "build/testFiles/expressions";

	//public static void main(String[] args) {
	@Test
	public void expressionTests(){
		System.out.println("Running tests from directory '" + testDir + "'");

		File directory = new File(testDir);
		if (!directory.isDirectory()) {
			System.err.println(testDir + " is not a directory!");
			assert false;
			//System.exit(-1);
		}
		int i = 1;
		File[] exprFiles = directory.listFiles(
				new FilenameFilter() {
					public boolean accept(File d, String n) {
						return n.endsWith(".erl");
					}
				}
		);
		for (File in : exprFiles) {
			File out = new File(in.toString() + ".out");
			String expr = readLines(in);
			String expectedResult = readLines(out);
			Result r = Evaluator.evaluate(expr);
			assertTrue( expectedResult.equals(r.getMessage()) );

			if (expectedResult.equals(r.getMessage())) {
				System.out.println("Test " + i + "/" + exprFiles.length + " passed: " + in.getName());
				System.out.println("");
			} else {
				System.out.println("Test " + i + "/" + exprFiles.length + "failed: " + in.getName());
				System.out.println("Expression: '" + expr + "'");
				System.out.println("Expected: '" + expectedResult + "'");
				System.out.println("Actual  : '" + r.getMessage() + "'");
				System.out.println("");
			}
			i++;
		}
	}

	public static String readLines(File f) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(f));
		} catch (FileNotFoundException e) {
			System.err.println("Not Found reading file: '" + f + "'");
			return "";
		}
		StringBuilder result = new StringBuilder();
		String line = null;
		try {
			result.append(br.readLine());
			while ((line = br.readLine()) != null)
				result.append("\n" + line);
			br.close();
		} catch (IOException e) {
			System.err.println("Error reading file: '" + f + "'");
		}
		return result.toString();
	}


	List<File> getFilesForDir(final String dirname, final String extension){
		List<File> result = new ArrayList<File>();

		File directory = new File(dirname);
		int i = 1;
		File[] exprFiles = directory.listFiles(
				new FilenameFilter() {
					public boolean accept(File d, String n) {
						return n.endsWith(extension);
					}
				}
		);
		result = Arrays.asList(exprFiles);
		return result;
	}

	@Test
	public void appliesToAllModbus(){
		//test all syntaxes
		List<File> exprFiles = getFilesForDir(testDir, ".erl");

		for (File in : exprFiles) {
			File out = new File(in.toString() + ".out");
			String expr = readLines(in);
			String expectedResult = readLines(out);
			Result r = Evaluator.evaluate(expr);
			assertTrue( expectedResult.equals(r.getMessage()) );
		}
	}

	@Test
	public void appliesToAllBacnet(){
		//test all syntaxes
		List<File> exprFiles = getFilesForDir(testDir, ".erl");

		for (File in : exprFiles) {
			File out = new File(in.toString() + ".out");
			String expr = readLines(in);
			String expectedResult = readLines(out);
			Result r = Evaluator.evaluate(expr);

			if ( ! expectedResult.equals(r.getMessage()) ){
				System.out.println( "expected " + expectedResult );
				System.out.println( "got " + r.getMessage() );
			}

			assertTrue( expectedResult.equals(r.getMessage()) );
		}
	}

	@Test
	public void modbusOnly(){
		List<File> exprFiles = getFilesForDir(testDir + "/modbus", ".erl");

		for (File in : exprFiles) {
			File out = new File(in.toString() + ".out");
			String expr = readLines(in);
			String expectedResult = readLines(out);
			Result r = Evaluator.evaluate(expr);
			assertTrue( expectedResult.equals(r.getMessage()) );
		}
	}

	@Test
	public void bacnetOnly(){
		List<File> exprFiles = getFilesForDir(testDir + "/bacnet", ".erl");

		for (File in : exprFiles) {
			File out = new File(in.toString() + ".out");
			String expr = readLines(in);
			String expectedResult = readLines(out);
			Result r = Evaluator.evaluate(expr);
			if ( ! expectedResult.equals(r.getMessage()) ){
				System.out.println( "file " + in.getName() );
				System.out.println( "expected " + expectedResult );
				System.out.println( "got " + r.getMessage() );
			}
			assertTrue( expectedResult.equals(r.getMessage()) );
		}
	}

}

