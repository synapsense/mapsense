package DeploymentLab;


import DeploymentLab.Examinations.ComponentExaminations
import DeploymentLab.Examinations.Examination
import DeploymentLab.Examinations.WSNExamination
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.channellogger.LogLevel
import org.junit.Before
import org.junit.Test

import java.lang.reflect.Method
import org.junit.runners.Parameterized
import org.junit.runner.RunWith

/**
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 8/15/12
 * Time: 6:03 PM
 */
@RunWith(Parameterized.class)
public class UpgradeRegressionTest {

	/**
	 * Produces the parameters for the test - this MUST be a collection of arrays.
	 *
	 * Hang on, bear with me.  Each test run requires an array that represents the parameter list for the class constructor.
	 * (In our case, that's just the one string - the filename.)
	 *
	 * Then, we need a Collection, where each entry is the array of parameters for a given test run.
	 *
	 * And then Groovy's collection syntax sugar gives us cancer of the semicolon.
	 *
	 * To add a new DL(z) file to the test suite, add a line like those below to stick it into the params list as a single item Object array.
	 *
	 *
	 * @return the set of files to test on.
	 */
	@Parameterized.Parameters
	public static Collection<Object[]> data(){
		def params = []
		params += ["build/testFiles/regression/Lint_504.dl"] as Object[]
		params += ["build/testFiles/regression/Lint_510.dl"] as Object[]
		params += ["build/testFiles/regression/Lint_520.dl"] as Object[]
		// TODO: This is causing Jenkins to hang for some strange reason. Disable for now, but we should get it working.
		// params += ["build/testFiles/Jupiter2THREETHINGSControl.dlz"] as Object[]
		return params
	}

	@Before
	void setup() {
		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		//cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))

		//need to replumb the message logger so dialogs don't show up
		//also, don't write user prefs to registry?

		//overload the default user preferences
		System.setProperty("java.util.prefs.PreferencesFactory", "DeploymentLab.TestingPreferencesFactory");

	}

	private String filename

	public UpgradeRegressionTest(String fn){
		filename = fn
	}

	@Test
	public void testUpgrade() throws Exception {
		println "upgrade test for '$filename'"
		DeploymentLabWindow w = new DeploymentLabWindow(null, true);
		w.showUI = false
		w.start(null, 3);
		assert w.undoBuffer.getParent() == w.deploymentLabFrame

		int baseSize = w.componentModel.getComponents().size()

		//squash message boxes
		ChannelManager cm = ChannelManager.getInstance()
		cm.listeners.put("message", null)

		//w.doFileOpen( new File("build/testFiles/Jupiter2THREETHINGSControl.dlz") )
		//w.waitForBackgroundTask()
		//w.doLoad(new File("build/testFiles/Jupiter2THREETHINGSControl.dlz"))
		//w.doLoad(new File("build/testFiles/Lint_504.dl"))
		w.doLoad(new File(filename))

		assert w.componentModel.getComponents().size() > baseSize

		w.componentModel.getComponents().each { c ->
			if ( c.getDisplaySetting("deprecated") != 'true'){
				println "testing $c"
				//println "$c is '${c.getDisplaySetting("deprecated")}'"
				WSNExamination.checkNodes(c)
				WSNExamination.checkNodes(c)
				for (Method method : ComponentExaminations.getMethods()) {
					if (method.isAnnotationPresent(Examination.class)) {
						println "invoke ${method.name}($c)"
						method.invoke(null, c)
					}
				}

			}
		}

		w.doFileClose()

	}

}

