package DeploymentLab

import DeploymentLab.Model.ModelChangeListener
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.MetamorphosisEvent

/**
 * Stores a stack of the model change events
 * @author Gabriel Helman
 * @since
 * Date: 10/12/12
 * Time: 10:46 AM
 */
class ModelObserver implements ModelChangeListener {
	private ComponentModel componentModel

	ArrayDeque eventStack = new ArrayDeque()

	public ModelObserver(ComponentModel cm){
		this.componentModel = cm
		this.componentModel.addModelChangeListener(this)
	}


	@Override
	void componentAdded(DLComponent component) {
		eventStack.push( "componentAdded $component" )
	}

	@Override
	void componentRemoved(DLComponent component) {
		eventStack.push( "componentRemoved $component" )
	}

	@Override
	void childAdded(DLComponent parent, DLComponent child) {
		eventStack.push( "childAdded $parent, $child" )
	}

	@Override
	void childRemoved(DLComponent parent, DLComponent child) {
		eventStack.push( "childRemoved $parent, $child" )
	}

	@Override
	void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
		eventStack.push( "associationAdded $producer, $producerId, $consumer, $consumerId" )
	}

	@Override
	void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
		eventStack.push( "associationRemoved $producer, $producerId, $consumer, $consumerId" )
	}

	@Override
	void modelMetamorphosisStarting(MetamorphosisEvent event) {
		eventStack.push( "modelMetamorphosisStarting ${event.mode}" )
	}

	@Override
	void modelMetamorphosisFinished(MetamorphosisEvent event) {
		eventStack.push( "modelMetamorphosisFinished ${event.mode}" )
	}
}
