package DeploymentLab

import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ObjectModel
import org.junit.BeforeClass
import org.junit.Before
import org.junit.After
import org.junit.Test
import DeploymentLab.Exceptions.ModelCatastrophe
import java.awt.Color

/**
 *
 * Checks that the inscrutable .properties files have been correctly updated.
 *
 * @author Gabriel Helman
 * @since Jupiter
 * Date: 3/9/12
 * Time: 10:52 AM
 */
class CheckPropertiesTest {

	ComponentModel cmodel = null
	ObjectModel omodel = null

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
	}

	/**
	 * Checks that the required properties have been added for each association.
	 *
	 * Each association needs:
	 *
	 * <ul>
	 * 		<li>A short name (Used in the associate dialog box)</li>
	 * 		<li>A long name (Used in the view options dialog boc)</li>
	 * 		<li>A color (rgb value in hex)</li>
	 * </ul>
	 *
	 * AOEs need something similar:
	 * <ul>
	 *     <li>A color</li>
	 *     <li>An alpha value</li>
	 * </ul>
	 *
	 *
	 */
	@Test
	void associationProperties(){
		Properties ap = CentralCatalogue.INSTANCE.getAssociationProperties()
		Set<String> alltypes = new HashSet<String>();
		Set<String> componentTypes = cmodel.listComponentTypes()
		for ( String t : componentTypes.sort() ){
			if( !cmodel.listComponentRoles(t).contains("placeable") ){
				continue
			}
			//check all producer/consumers
			def c = cmodel.newComponent(t)
			c.listProducers(true).each{ p->
				def datatype = c.getProducer(p).getDatatype()
				alltypes.add(datatype)
			}
			c.listConsumers(true).each{ p->
				def datatype = c.getConsumer(p).getDatatype()
				alltypes.add(datatype)
			}

			//while we're here, check AOEs as well
			String aoename = null
			if(c.hasRole("temperaturecontrol")){
				aoename = "temp"
			}else if (c.hasRole("pressurecontrol")){
				aoename = "pressure"
			}else if (c.hasRole("airflowcontrol")){
				aoename = "airflow"
			}
			if(aoename){
				assert ap.containsKey("aoe.color.$aoename".toString())
				assert ap.containsKey("aoe.alpha.$aoename".toString())
			}
		}
		
		for(String datatype : alltypes){
			println "checking $datatype"
			assert ap.containsKey("associations.descr.$datatype".toString())
			assert ap.containsKey("associations.descr.viewoption.$datatype".toString())
			assert ap.containsKey("associations.color.$datatype".toString())
		}
	}

	/**
	 * Make sure that anything in the associations.types also has descriptions, regardless as to whether the type is used in a component
	 */
	@Test
	public void associationsSanityCheck(){
		def associationTypes = CentralCatalogue.getAssoc("associations.types").split(',')
		def associationProperties = CentralCatalogue.getInstance().getAssociationProperties()

		associationTypes.each{ at ->
			println "check $at"
			def str1 = associationProperties.getProperty("associations.descr."+at)
			def str2 = associationProperties.getProperty("associations.descr.viewoption."+at)
			def c1 = new Color( Integer.parseInt(associationProperties.getProperty("associations.color."+at), 16) )

			assert str1.length() > 1
			assert str2.length() > 1

		}
	}

}
