package DeploymentLab

import DeploymentLab.Model.ComponentType
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.Tools.ToolManager;
import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Model.ComponentModel

import javax.swing.ButtonGroup
import java.nio.file.DirectoryStream
import java.nio.file.FileSystem
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardCopyOption

import static DeploymentLab.TestUtils.shouldFail
import DeploymentLab.Model.DLComponent
import DeploymentLab.Tools.ConfigurationReplacer
import javax.swing.JFrame
import DeploymentLab.Tree.ConfigurationReplacerTree
import DeploymentLab.Tree.ConfigurationReplacerTreeModel
import DeploymentLab.Model.DLProxy

/**
 * Tests the DLProject class.
 *
 * @author Ken Scoggins
 * @since Jupiter
 */
public class DLProjectTest {

	static ObjectModel objectModel
	static ComponentModel componentModel
	static SelectModel selectModel
	static UndoBuffer undoBuffer
	static ConfigurationReplacerTree tree
	static ConfigurationReplacer cr
	static DeploymentPanel dp


    /**
     * One-time setup called before all tests run.
     */
    @BeforeClass()
    static void classSetup() {
	    TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
    }

    /**
     * Setup called before each test.
     */
    @Before
    void setup() {
	    (objectModel,componentModel) = TestUtils.initModels( false )
		def treeModels = []
		def cmtm = new ConfigurationReplacerTreeModel(componentModel, "FakeTree")
		treeModels.add(cmtm)
		tree = new ConfigurationReplacerTree(cmtm)

		//trees go in clib
		def clib = ComponentLibrary.INSTANCE;
		clib.init(componentModel,null,treeModels);

		selectModel = CentralCatalogue.getSelectModel()
		undoBuffer = CentralCatalogue.getUndoBuffer()

	    dp = new DeploymentPanel(selectModel, undoBuffer, componentModel, new ToolManager(new ButtonGroup()), new DisplayProperties( selectModel ))
	    CentralCatalogue.setDeploymentPanel( dp )

		//and a replacer
		cr = new ConfigurationReplacer(selectModel, componentModel, tree, undoBuffer, new JFrame())
    }

    /**
     * Cleanup called after each test.
     */
    @After
    void tearDown() {
        objectModel = null
        componentModel = null
		selectModel = null
		undoBuffer = null
	    dp = null
		tree = null
		cr = null
    }

    /**
     * One-time cleanup called after all tests run.
     */
    @AfterClass()
    static void classTearDown() {

    }


    private void loadAndTest( String projectFile ) {

		TestUtils.loadProjectFile(projectFile,undoBuffer,objectModel,componentModel, cr)

	}


    /**
     * Tests loading a file that doesn't exist.
     */
    @Test
    void testNonexistantFile() {
        shouldFail( FileNotFoundException ) { loadAndTest('build/testFiles/bogus-file.dlz') }

        // Make sure an empty file wasn't accidentally created when it wasn't found.
        assert ! new File('build/testFiles/bogus-file.dlz').exists()
    }

    /**
     * Tests loading a DLZ file.
     */
    @Test
    void testLoadDLZ() {
        loadAndTest('build/testFiles/multydc.dlz')
    }

    /**
     * Tests loading from an exposed XML file, like an exploded DLZ. Speaking of... we'll explode the standard LINT and
     * copy out the XML to do this.
     */
    @Test
    void testLoadXML() {
	    FileSystem fileSystem = null
	    DirectoryStream<Path> dirs = null
	    Path dlXml = null

	    try {
		    URI uri = URI.create("jar:" + FileSystems.getDefault().getPath( TestUtils.CURRENT_VERSION_LINT ).toUri().toString() );

		    fileSystem = FileSystems.newFileSystem( uri, new HashMap<String, String>() );
		    dirs = Files.newDirectoryStream( fileSystem.getRootDirectories().iterator().next() )

		    for( Path ze : dirs ) {
			    if (ze.toString().toLowerCase().endsWith(".xml")) {
				    if (!ze.toString().toLowerCase().endsWith("access_model.xml")) {
					    // Copy to a temp file.
					    dlXml = File.createTempFile("unittestcopy","xml").toPath()
					    Files.copy( ze, dlXml, StandardCopyOption.REPLACE_EXISTING )
				    }
			    }
		    }

		    assert dlXml

	        loadAndTest( dlXml.toString() )

	    } finally {
		    if( fileSystem != null ) fileSystem.close()
		    if( dirs != null ) dirs.close()
		    if( dlXml != null ) Files.deleteIfExists( dlXml )
	    }
    }

    /**
     * Tests loading of an old-style DL with embedded images.
     */
    @Test
    void testLoadDL() {
        // todo: Get an old DL into testFiles.
        //loadAndTest('build/testFiles/???')
    }

	@Test
	void testExportExclusions() {
		DLProject origProject = TestUtils.loadProjectFile( TestUtils.CURRENT_VERSION_LINT, undoBuffer, objectModel, componentModel, cr )
		selectModel.setActiveDrawing( componentModel.getComponentsByType( ComponentType.DRAWING ).first() )

		// Add a proxy to the project.
		componentModel.getComponentsInRole('placeable').each { c ->
			if( DLProxy.canProxy( c ) ) {
				DLComponent proxy = DLProxy.createProxy( c )
				selectModel.getActiveDrawing().addChild( proxy )
				assert componentModel.getComponents().contains( proxy )
			}
		}

		// Make sure an orphanage has been created.
		componentModel.getOrphanage( selectModel.getActiveDrawing() )

		// Get some counts, including the objects we know are not exportable by default.
		int origAllCompCnt = componentModel.getComponents().size()
		Set<DLComponent> origNotExporting = componentModel.getComponentsInRole('proxy')
		origNotExporting.addAll( componentModel.getComponentsByType( ComponentType.ORPHANAGE ) )
		origNotExporting.addAll( componentModel.getComponentsByType( ComponentType.PLANNING_GROUP ) )
		origNotExporting.addAll( componentModel.getComponentsByType( ComponentType.RACK_UNINSTRUMENTED ) )

		// Don't forget the kids of planning groups that are not exporting.
		componentModel.getComponentsByType( ComponentType.PLANNING_GROUP ).each {
			if( !it.getPropertyValue('export') ) {
				origNotExporting.addAll( it.getChildComponents() )
			}
		}

		assert origAllCompCnt > 0
		assert origNotExporting.size() > 0

		int notExportableCompCnt = 0
		componentModel.getComponents().each { c ->
			if( !c.isExportable() ) {
				notExportableCompCnt++
				if( !origNotExporting.contains( c ) ) {
					println "\nNot exporting but we the test thinks it should: $c\n"
				}
			}
		}
		assert notExportableCompCnt == origNotExporting.size()

		int origAllObjsCnt = objectModel.getObjectCount()
		int notExportableObjsCnt = 0
		objectModel.getObjects().each { o ->
			if( !o.isExportable() ) {
				notExportableObjsCnt++
			}
		}
		assert origAllObjsCnt >= origAllCompCnt
		assert notExportableObjsCnt >= notExportableCompCnt

		// Now export the project.
		File exportedFile = File.createTempFile( origProject.getFile().toFile().getName(), ".dlz")
		origProject.export( exportedFile )

		// Load the exported version and make sure the non-exportable components and objects were not saved.
		ObjectModel newObjectModel = TestUtils.loadObjectModel()
		ComponentModel newComponentModel = TestUtils.loadComponentModel( newObjectModel )
		DLProject exportedProject = TestUtils.loadProjectFile( exportedFile.getAbsolutePath(), undoBuffer, newObjectModel, newComponentModel, cr )

		assert newComponentModel.getComponentsByType('proxy').isEmpty()
		assert newComponentModel.getComponentsByType( ComponentType.ORPHANAGE ).isEmpty()
		assert newComponentModel.getComponentsByType( ComponentType.PLANNING_GROUP ).isEmpty()
		assert newComponentModel.getComponents().size() == (origAllCompCnt - notExportableCompCnt)
		assert newObjectModel.getObjectCount() == (origAllObjsCnt - notExportableObjsCnt)
	}

	/**
	 * Tests loading an old control DLZ with deprecated components n groups n such.
	 */
	@Test
	void testLoadOldControlDLZ() {
		DLProject project = TestUtils.loadProjectFile('build/testFiles/jupiter2aoetest.dlz', undoBuffer, objectModel, componentModel, cr )

		// TODO: Need to check some things to see if the upgraders worked.
	}
}
