package DeploymentLab.Util

import DeploymentLab.DeploymentLabWindow;
import DeploymentLab.Image.DLImage
import DeploymentLab.Model.ComponentType
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.ObjectType
import DeploymentLab.TestUtils
import DeploymentLab.channellogger.Logger
import com.panduit.sz.api.ss.assets.Floorplan
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

import javax.imageio.ImageIO
import java.awt.image.BufferedImage

/**
 * Created by LNI on 1/18/2016.
 */
public class SmartZoneResponseAssemblerTest {
    DeploymentLabWindow w
    def drawing

    DLImage bgImage

    private static final Logger log = Logger.getLogger(SmartZoneResponseAssemblerTest.class.getName())

    @BeforeClass
    static void classSetup() {
        // Turn on error logging to help when something fails internal to a method we are testing.
        TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
    }

    @Before
    void setup() {
        w = new DeploymentLabWindow(null, true);
        w.showUI = false
        w.start(null)

        File imgFile = new File('build/testFiles/simplebg.png')
        InputStream is = new FileInputStream(imgFile)
        byte[] bytes = is.getBytes()

        BufferedImage bi = ImageIO.read(new ByteArrayInputStream(bytes))
        bgImage = new DLImage(bi)
    }

    /**
     * Cleanup after each test.
     */
    @After
    void tearDown() {
        if (drawing != null) {
            drawing.clearDrawing()
        }
    }

    @Test
    public void testValidPropertiesWithBG() {
        drawing = setDrawingProperties(1, "SZLocation1", true, bgImage, 2.0)
        setUninstrumentedRackProperties(11, "RACK1", 45, 20.4, 20.4, 100, 100)

        Floorplan fp = SmartZoneResponseAssembler.buildFloorplan(w.componentModel, drawing, null)
        assert fp!=null
        assert fp.getId() == 1
        assert fp.getWidth() == drawing.getPropertyValue('width') * 2.0
        assert fp.getHeight() == drawing.getPropertyValue('height') * 2.0
    }

    @Test
    public void testValidPropertiesWithNoBG() {
        drawing = setDrawingProperties(1, "SZLocation1", false, null, 2.0)
        setUninstrumentedRackProperties(11, "RACK1", 45, 20.4, 20.4, 100, 100)

        Floorplan fp = SmartZoneResponseAssembler.buildFloorplan(w.componentModel, drawing, null)
        assert fp!=null
        assert fp.getId() == 1
        assert fp.getWidth() == drawing.getPropertyValue('width') * 2.0
        assert fp.getHeight() == drawing.getPropertyValue('height') * 2.0
        assert drawing.getPropertyValue('hasImage') == false
        assert drawing.getPropertyValue('image_data') == null
    }

    private DLComponent setDrawingProperties(Integer smartZoneId,
                                             String name,
                                             boolean hasImage,
                                             DLImage imgData,
                                             double scale) {
        def drawing = w.componentModel.newComponent(ComponentType.DRAWING)

        drawing.setSmartZoneId(smartZoneId)
        drawing.setPropertyValue('name', name)
        drawing.setPropertyValue("hasImage", hasImage)
        drawing.setPropertyValue('image_data', imgData)
        drawing.setPropertyValue('scale', scale)
    }

    private void setUninstrumentedRackProperties(Integer smartZoneId,
                                                 String name,
                                                 double rotation,
                                                 double depth,
                                                 double width,
                                                 double x,
                                                 double y) {
        def rack = w.componentModel.newComponent(ComponentType.RACK_UNINSTRUMENTED)
        rack.setDrawing(drawing)
        rack.setSmartZoneId(smartZoneId)
        rack.setPropertyValue('name', name)
        rack.setPropertyValue('rotation', rotation)
        rack.setPropertyValue('depth', depth)
        rack.setPropertyValue('width', width)
        rack.setPropertyValue('x', x)
        rack.setPropertyValue('y', y)
    }
}