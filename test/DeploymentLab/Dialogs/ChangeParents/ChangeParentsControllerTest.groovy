package DeploymentLab.Dialogs.ChangeParents

import DeploymentLab.CentralCatalogue;
import DeploymentLab.Exceptions.ModelCatastrophe
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentType
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.ObjectModel
import DeploymentLab.ProblemLogger
import DeploymentLab.SelectModel
import DeploymentLab.TestUtils
import DeploymentLab.UndoBuffer
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.channellogger.LogLevel
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

import javax.swing.JFrame

/**
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 6/15/12
 * Time: 3:34 PM
 */
public class ChangeParentsControllerTest {

	ComponentModel cmodel = null
	ObjectModel omodel = null
	UndoBuffer undoBuffer = null
	SelectModel selectModel = null

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()
		undoBuffer = CentralCatalogue.getUndoBuffer()

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
		undoBuffer = null
		selectModel = null
	}

	@Test
	public void basicSantityCheck() {
		cmodel.newComponent("pressure2")
		cmodel.newComponent("pressure2")
		cmodel.newComponent("pressure2")
		def cpd = new ChangeParentsController(cmodel.getComponents(), ParentCategory.GROUPING, undoBuffer, selectModel)
	}

	@Test
	public void dialogSanity() {
		cmodel.newComponent("pressure2")
		cmodel.newComponent("pressure2")
		cmodel.newComponent("pressure2")
		def cpd = new ChangeParentsController(cmodel.getComponents(), ParentCategory.GROUPING, undoBuffer, selectModel)

		def dialog = cpd.getDialog(new JFrame())
		dialog.initDialog()
		assert dialog == cpd.dialog
		dialog.close()
		dialog.dispose()
	}

	@Test
	public void testIsValid() {
		//what is invalid?  No selection and no valid parents

		def room = cmodel.newComponent("room")
		def group = cmodel.newComponent( ComponentType.PLANNING_GROUP )
		def a = cmodel.newComponent("pressure2")
		def b = cmodel.newComponent("pressure2")
		def c = cmodel.newComponent("pressure2")
		selectModel.getActiveDrawing().addChild(room)
		selectModel.getActiveDrawing().addChild(group)

		room.addChild(a)
		room.addChild(b)
		room.addChild(c)

		def first = new ChangeParentsController([a, b, c], ParentCategory.GROUPING, undoBuffer, selectModel)
		assert first.isValid()

		def noSel = new ChangeParentsController([], ParentCategory.GROUPING, undoBuffer, selectModel)
		assert !noSel.isValid()

		def d = cmodel.newComponent("pue_main")
		def lz = cmodel.newComponent("logicalzone")
		selectModel.getActiveDrawing().addChild(lz)
		lz.addChild(d)

		def noParents = new ChangeParentsController([a, b, c, d], ParentCategory.GROUPING, undoBuffer, selectModel)
		assert !noParents.isValid()

	}


	@Test
	public void testCheckboxCheckedAndApply() {
		def room = cmodel.newComponent("room")
		def net = cmodel.newComponent("network")
		def a = cmodel.newComponent("pressure2")
		def b = cmodel.newComponent("pressure2")
		def c = cmodel.newComponent("pressure2")
		selectModel.getActiveDrawing().addChild(room)
		selectModel.getActiveDrawing().addChild(net)
		room.addChild(a)
		room.addChild(b)
		room.addChild(c)
		net.addChild(a)
		net.addChild(b)
		net.addChild(c)

		def group = cmodel.newComponent( ComponentType.PLANNING_GROUP )
		def group2 = cmodel.newComponent( ComponentType.PLANNING_GROUP )
		def group3 = cmodel.newComponent( ComponentType.PLANNING_GROUP )
		selectModel.getActiveDrawing().addChild(group)
		selectModel.getActiveDrawing().addChild(group2)
		selectModel.getActiveDrawing().addChild(group3)
		def cp1 = cmodel.newComponent( TestUtils.DEFAULT_RACK_NO_STRAT )
		def cp2 = cmodel.newComponent( TestUtils.DEFAULT_RACK_NO_STRAT )
		group.addChild(cp1)
		group.addChild(cp2)

		def cpc = new ChangeParentsController([cp1, cp2], ParentCategory.GROUPING, undoBuffer, selectModel)

		assert cpc.getData().size() == 3
		assert cpc.getData()[group]
		assert !cpc.getData()[group2]
		assert !cpc.getData()[group3]

		cpc.checkboxChecked(group2)

		assert cpc.getData().size() == 3
		assert cpc.getData()[group]
		assert cpc.getData()[group2]
		assert !cpc.getData()[group3]

		cpc.checkboxChecked(group3)

		assert cpc.getData().size() == 3
		assert cpc.getData()[group]
		assert cpc.getData()[group2]
		assert cpc.getData()[group3]

		cpc.checkboxChecked(group)

		assert cpc.getData().size() == 3
		assert !cpc.getData()[group]
		assert cpc.getData()[group2]
		assert cpc.getData()[group3]

		assert cp1.isChildComponentOf(group)
		assert !cp1.isChildComponentOf(group2)
		assert !cp1.isChildComponentOf(group3)

		cpc.applyChanges(true)

		assert !cp1.isChildComponentOf(group)
		assert cp1.isChildComponentOf(group2)
		assert cp1.isChildComponentOf(group3)


	}

	@Test
	public void testRadioButtonCheckedAndApply() {
		def group = cmodel.newComponent( ComponentType.PLANNING_GROUP )
		def group2 = cmodel.newComponent( ComponentType.PLANNING_GROUP )
		def net = cmodel.newComponent("network")
		def a = cmodel.newComponent("pressure2")
		def b = cmodel.newComponent("pressure2")
		def c = cmodel.newComponent("pressure2")
		selectModel.getActiveDrawing().addChild(group)
		selectModel.getActiveDrawing().addChild(group2)
		selectModel.getActiveDrawing().addChild(net)
		group.addChild(a)
		group.addChild(b)
		group.addChild(c)
		net.addChild(a)
		net.addChild(b)
		net.addChild(c)

		def cpc = new ChangeParentsController([a, b, c], ParentCategory.GROUPING, undoBuffer, selectModel)

		assert cpc.getData().size() == 2
		assert cpc.getData()[group] == true
		assert cpc.getData()[group2] == false

		cpc.radioButtonChecked(group2)

		assert cpc.getData().size() == 2
		assert cpc.getData()[group] == false
		assert cpc.getData()[group2] == true

		cpc.radioButtonChecked(group)

		assert cpc.getData().size() == 2
		assert cpc.getData()[group] == true
		assert cpc.getData()[group2] == false

		cpc.radioButtonChecked(group2)

		assert cpc.getData().size() == 2
		assert cpc.getData()[group] == false
		assert cpc.getData()[group2] == true

		assert a.isChildComponentOf(group)
		assert !a.isChildComponentOf(group2)

		cpc.applyChanges(false)

		assert !a.isChildComponentOf(group)
		assert a.isChildComponentOf(group2)
	}

	@Test
	public void testMixedCaseGroup() {
		def room = cmodel.newComponent( ComponentType.ROOM )
		def group = cmodel.newComponent( ComponentType.PLANNING_GROUP )
		def group2 = cmodel.newComponent( ComponentType.PLANNING_GROUP )
		def group3 = cmodel.newComponent( ComponentType.PLANNING_GROUP )
		def net = cmodel.newComponent("network")
		def a = cmodel.newComponent("pressure2")
		def b = cmodel.newComponent("pressure2")
		def c = cmodel.newComponent("pressure2")
		selectModel.getActiveDrawing().addChild(room)
		selectModel.getActiveDrawing().addChild(group)
		selectModel.getActiveDrawing().addChild(group2)
		selectModel.getActiveDrawing().addChild(group3)
		selectModel.getActiveDrawing().addChild(net)
		room.addChild(a)
		room.addChild(b)
		room.addChild(c)
		group.addChild(a)
		group2.addChild(b)
		group3.addChild(c)
		net.addChild(a)
		net.addChild(b)
		net.addChild(c)

		def cpc = new ChangeParentsController([a, b, c], ParentCategory.GROUPING, undoBuffer, selectModel)

		cpc.radioButtonChecked(group3)
		cpc.applyChanges(false)

		assert !a.isChildComponentOf(group)
		assert !b.isChildComponentOf(group)
		assert !c.isChildComponentOf(group)
		assert !a.isChildComponentOf(group2)
		assert !b.isChildComponentOf(group2)
		assert !c.isChildComponentOf(group2)

		assert a.isChildComponentOf(group3)
		assert b.isChildComponentOf(group3)
		assert c.isChildComponentOf(group3)
	}

	@Test
	public void testAllowsMoreThanOneParent() {
		def room = cmodel.newComponent("room")
		def net = cmodel.newComponent("network")
		def a = cmodel.newComponent("pressure2")
		def b = cmodel.newComponent("pressure2")
		def c = cmodel.newComponent("pressure2")
		selectModel.getActiveDrawing().addChild(room)
		selectModel.getActiveDrawing().addChild(net)
		room.addChild(a)
		room.addChild(b)
		room.addChild(c)
		net.addChild(a)
		net.addChild(b)
		net.addChild(c)

		def pg = cmodel.newComponent( ComponentType.PLANNING_GROUP )
		selectModel.getActiveDrawing().addChild(pg)
		def cp1 = cmodel.newComponent( TestUtils.DEFAULT_RACK_NO_STRAT )
		def cp2 = cmodel.newComponent( TestUtils.DEFAULT_RACK_NO_STRAT )
		pg.addChild(cp1)
		pg.addChild(cp2)

		def first = new ChangeParentsController([a, b, c], ParentCategory.GROUPING, undoBuffer, selectModel)
		assert !first.allowsMoreThanOneParent()

		def second = new ChangeParentsController([a, b, c], ParentCategory.NETWORK, undoBuffer, selectModel)
		assert !second.allowsMoreThanOneParent()

		// Unlike hte old REINs, planning groups do not allow more than one parent.
		def third = new ChangeParentsController([cp1, cp2], ParentCategory.GROUPING, undoBuffer, selectModel)
		assert !third.allowsMoreThanOneParent()
	}

	@Test
	public void testGetParentCategory() {
		def g = new ChangeParentsController([], ParentCategory.GROUPING, undoBuffer, selectModel)
		assert g.getParentCategory() == ParentCategory.GROUPING

		def n = new ChangeParentsController([], ParentCategory.NETWORK, undoBuffer, selectModel)
		assert n.getParentCategory() == ParentCategory.NETWORK

		def c = new ChangeParentsController([], ParentCategory.GROUPING, undoBuffer, selectModel)
		assert c.getParentCategory() == ParentCategory.GROUPING
	}


	@Test
	void parentCategoryToString() {
		assert ParentCategory.GROUPING.toString().size() > 0
		assert ParentCategory.NETWORK.toString().size() > 0
	}


	@Test(expected = ModelCatastrophe.class)
	void notOnADrawing() {
		//the cp is not on a drawing, so the CPC should freak out
		def cp1 = cmodel.newComponent("pressure2")

		def pg = cmodel.newComponent( ComponentType.PLANNING_GROUP )
		selectModel.getActiveDrawing().addChild(pg)
		def room = cmodel.newComponent("room")
		selectModel.getActiveDrawing().addChild(room)
		def rd = cmodel.newComponent("control_register_damper")
		room.addChild(rd)
		def cpc = new ChangeParentsController([cp1], ParentCategory.GROUPING, undoBuffer, selectModel)
		cpc.getData()
	}

	@Test
	void moreThanOneDrawing() {
		def drawing1 = selectModel.getActiveDrawing()
		def drawing2 = cmodel.newComponent("drawing")
		drawing2.setPropertyValue("name", "the party floor.")

		//drawing 1
		def group1 = cmodel.newComponent( ComponentType.PLANNING_GROUP )
		drawing1.addChild(group1)
		def room1 = cmodel.newComponent( ComponentType.ROOM )
		drawing1.addChild(room1)
		def rack1 = cmodel.newComponent( TestUtils.DEFAULT_RACK )
		room1.addChild(rack1)

		//drawing 2
		def group2 = cmodel.newComponent( ComponentType.PLANNING_GROUP )
		group2.setPropertyValue("name", "the party group.")
		drawing2.addChild(group2)
		def room2 = cmodel.newComponent( ComponentType.ROOM )
		drawing2.addChild(room2)
		def rack2 = cmodel.newComponent( TestUtils.DEFAULT_RACK )
		room2.addChild(rack2)

		// Now see about adding the racks to the group.
		def cpc = new ChangeParentsController([rack1], ParentCategory.GROUPING, undoBuffer, selectModel)
		assert cpc.getData().size() == 1
		assert cpc.getData().containsKey(group1)
		assert !cpc.getData().containsKey(room1)
		assert !cpc.getData().containsKey(group2)
		assert !cpc.getData().containsKey(room2)

		//and a coin for drawing2:
		cpc = new ChangeParentsController([rack2], ParentCategory.GROUPING, undoBuffer, selectModel)
		assert cpc.getData().size() == 1
		assert !cpc.getData().containsKey(group1)
		assert !cpc.getData().containsKey(room1)
		assert cpc.getData().containsKey(group2)
		assert !cpc.getData().containsKey(room2)
	}

}
