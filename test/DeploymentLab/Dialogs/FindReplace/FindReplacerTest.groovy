package DeploymentLab.Dialogs.FindReplace

import DeploymentLab.CentralCatalogue
import org.junit.Before
import DeploymentLab.TestUtils
import DeploymentLab.SelectModel

import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.ProblemLogger
import DeploymentLab.channellogger.LogLevel
import org.junit.After
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Model.ComponentModel
import org.junit.Test
import DeploymentLab.UndoBuffer
import javax.swing.JFrame

import DeploymentLab.Dialogs.SearchScope
import DeploymentLab.NamePair;

/**
 * Test the FindReplaceController
 * @author Gabriel Helman
 * @since Jupiter2
 * Date: 7/12/12
 * Time: 4:48 PM
 */
public class FindReplacerTest {

	static ObjectModel omodel
	static ComponentModel cmodel
	static SelectModel selectModel
	static UndoBuffer undoBuffer

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()
		undoBuffer = CentralCatalogue.getUndoBuffer()

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		//cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))
	}

	@After
	void tearDown() {
		cmodel = null
		omodel = null
		selectModel = null
		undoBuffer = null
		//have fun, garbage collector!
	}


	protected NamePair findField(FindReplaceController frc, String fieldName){
		def pair = frc.getFieldsInScope().find {it.internal == fieldName}
		return pair
	}


	@Test
	void sanityCheck(){
		def frc = new FindReplaceController(cmodel,selectModel,undoBuffer)
		def dialog = frc.getDialog()
		dialog.initDialog()
		assert dialog == frc.dialog
		assert dialog.controller == frc
		dialog.close()
		dialog.dispose()
	}

	@Test
	void basicReplaceAll(){
		def room = cmodel.newComponent("room")
		def net = cmodel.newComponent("network")
		def a = cmodel.newComponent("pressure2")
		def b = cmodel.newComponent("pressure2")
		def c = cmodel.newComponent("pressure2")
		selectModel.getActiveDrawing().addChild(room)
		selectModel.getActiveDrawing().addChild(net)
		room.addChild(a)
		room.addChild(b)
		room.addChild(c)
		net.addChild(a)
		net.addChild(b)
		net.addChild(c)

		def frc = new FindReplaceController(cmodel,selectModel,undoBuffer)


		frc.setScope(SearchScope.All)
		frc.setSelectedField(findField(frc,"name"))
		frc.setFindStr("Pressure")
		frc.setReplaceStr("PARTYTIME")

		frc.getInputsReplace()

		assert a.getPropertyStringValue("name") == "PARTYTIME2"
		assert b.getPropertyStringValue("name") == "PARTYTIME2"
		assert c.getPropertyStringValue("name") == "PARTYTIME2"
	}


	@Test
	void basicReplaceActive(){
		def room = cmodel.newComponent("room")
		def net = cmodel.newComponent("network")
		def a = cmodel.newComponent("pressure2")
		def b = cmodel.newComponent("pressure2")
		def c = cmodel.newComponent("pressure2")
		selectModel.getActiveDrawing().addChild(room)
		selectModel.getActiveDrawing().addChild(net)
		room.addChild(a)
		room.addChild(b)
		room.addChild(c)
		net.addChild(a)
		net.addChild(b)
		net.addChild(c)

		def drawing2 = cmodel.newComponent("drawing")
		def room2 = cmodel.newComponent("room")
		def net2 = cmodel.newComponent("network")
		def d = cmodel.newComponent("pressure2")
		selectModel.getActiveDrawing().addChild(room2)
		selectModel.getActiveDrawing().addChild(net2)
		room2.addChild(d)
		net2.addChild(d)

		def frc = new FindReplaceController(cmodel,selectModel,undoBuffer)

		frc.setScope(SearchScope.Active)
		frc.setSelectedField(findField(frc,"name"))
		frc.setFindStr("Pressure")
		frc.setReplaceStr("PARTYTIME")

		frc.getInputsReplace()

		assert a.getPropertyStringValue("name") != "PARTYTIME2"
		assert b.getPropertyStringValue("name") != "PARTYTIME2"
		assert c.getPropertyStringValue("name") != "PARTYTIME2"
		assert d.getPropertyStringValue("name") == "PARTYTIME2"
	}


	@Test
	void basicReplaceSelection(){
		def room = cmodel.newComponent("room")
		def net = cmodel.newComponent("network")
		def a = cmodel.newComponent("pressure2")
		def b = cmodel.newComponent("pressure2")
		def c = cmodel.newComponent("pressure2")
		selectModel.getActiveDrawing().addChild(room)
		selectModel.getActiveDrawing().addChild(net)
		room.addChild(a)
		room.addChild(b)
		room.addChild(c)
		net.addChild(a)
		net.addChild(b)
		net.addChild(c)

		def frc = new FindReplaceController(cmodel,selectModel,undoBuffer)


		selectModel.setSelection([b])

		frc.setScope(SearchScope.Selection)
		frc.setSelectedField(findField(frc,"name"))
		frc.setFindStr("Pressure")
		frc.setReplaceStr("PARTYTIME")

		frc.getInputsReplace()

		assert a.getPropertyStringValue("name") != "PARTYTIME2"
		assert b.getPropertyStringValue("name") == "PARTYTIME2"
		assert c.getPropertyStringValue("name") != "PARTYTIME2"
	}


	@Test
	void basicReplaceSelectionNoSelection(){
		def room = cmodel.newComponent("room")
		def net = cmodel.newComponent("network")
		def a = cmodel.newComponent("pressure2")
		def b = cmodel.newComponent("pressure2")
		def c = cmodel.newComponent("pressure2")
		selectModel.getActiveDrawing().addChild(room)
		selectModel.getActiveDrawing().addChild(net)
		room.addChild(a)
		room.addChild(b)
		room.addChild(c)
		net.addChild(a)
		net.addChild(b)
		net.addChild(c)

		def frc = new FindReplaceController(cmodel,selectModel,undoBuffer)

		selectModel.setSelection([])

		frc.setScope(SearchScope.Selection)
		frc.setSelectedField(findField(frc,"name"))
		frc.setFindStr("Pressure")
		frc.setReplaceStr("PARTYTIME")

		frc.getInputsReplace()

		assert a.getPropertyStringValue("name") != "PARTYTIME2"
		assert b.getPropertyStringValue("name") != "PARTYTIME2"
		assert c.getPropertyStringValue("name") != "PARTYTIME2"
	}

	@Test
	void basicReplaceBAdNoScope(){
		def room = cmodel.newComponent("room")
		def net = cmodel.newComponent("network")
		def a = cmodel.newComponent("pressure2")
		def b = cmodel.newComponent("pressure2")
		def c = cmodel.newComponent("pressure2")
		selectModel.getActiveDrawing().addChild(room)
		selectModel.getActiveDrawing().addChild(net)
		room.addChild(a)
		room.addChild(b)
		room.addChild(c)
		net.addChild(a)
		net.addChild(b)
		net.addChild(c)

		def frc = new FindReplaceController(cmodel,selectModel,undoBuffer)

		selectModel.setSelection([b])


		frc.setScope(null)
		frc.setSelectedField(findField(frc,"name"))
		frc.setFindStr("Pressure")
		frc.setReplaceStr("PARTYTIME")

		frc.getInputsReplace()

		assert a.getPropertyStringValue("name") != "PARTYTIME2"
		assert b.getPropertyStringValue("name") != "PARTYTIME2"
		assert c.getPropertyStringValue("name") != "PARTYTIME2"
	}


	@Test
	void basicReplaceBAdNoFindText(){
		def room = cmodel.newComponent("room")
		def net = cmodel.newComponent("network")
		def a = cmodel.newComponent("pressure2")
		def b = cmodel.newComponent("pressure2")
		def c = cmodel.newComponent("pressure2")
		selectModel.getActiveDrawing().addChild(room)
		selectModel.getActiveDrawing().addChild(net)
		room.addChild(a)
		room.addChild(b)
		room.addChild(c)
		net.addChild(a)
		net.addChild(b)
		net.addChild(c)

		def frc = new FindReplaceController(cmodel,selectModel,undoBuffer)

		selectModel.setSelection([b])


		frc.setScope(SearchScope.All)
		frc.setSelectedField(findField(frc,"name"))
		frc.setFindStr(null)
		frc.setReplaceStr("PARTYTIME")

		frc.getInputsReplace()

		assert a.getPropertyStringValue("name") != "PARTYTIME2"
		assert b.getPropertyStringValue("name") != "PARTYTIME2"
		assert c.getPropertyStringValue("name") != "PARTYTIME2"

		frc.setFindStr("")
		frc.getInputsReplace()

		assert a.getPropertyStringValue("name") != "PARTYTIME2"
		assert b.getPropertyStringValue("name") != "PARTYTIME2"
		assert c.getPropertyStringValue("name") != "PARTYTIME2"
	}

	@Test
	void basicReplaceAllDifferentField(){
		def room = cmodel.newComponent("room")
		def net = cmodel.newComponent("network")
		def a = cmodel.newComponent("pressure2")
		def b = cmodel.newComponent("pressure2")
		def c = cmodel.newComponent("pressure2")
		selectModel.getActiveDrawing().addChild(room)
		selectModel.getActiveDrawing().addChild(net)
		room.addChild(a)
		room.addChild(b)
		room.addChild(c)
		net.addChild(a)
		net.addChild(b)
		net.addChild(c)
		a.setPropertyValue("x", 1.12)
		b.setPropertyValue("x", 1.12)
		c.setPropertyValue("x", 1.12)

		def frc = new FindReplaceController(cmodel,selectModel,undoBuffer)


		frc.setScope(SearchScope.All)
		frc.setSelectedField(findField(frc,"x"))
		frc.setFindStr(".12")
		frc.setReplaceStr("0")

		frc.getInputsReplace()

		assert a.getPropertyValue("x") == 10
		assert b.getPropertyValue("x") == 10
		assert c.getPropertyValue("x") == 10
	}


	@Test
	void fieldsInScopeAll(){

	}

}
