package DeploymentLab.Dialogs.Association

import javax.swing.JComboBox
import javax.swing.JToggleButton
import org.junit.Test
import DeploymentLab.Dialogs.Association.AssociationBundle
import DeploymentLab.Dialogs.Association.AssociationDialogRegistry

/**
 * Basic full-coverage test for the AssociationDialogRegistry
 * @author Gabriel Helman
 * @since
 * Date: 3/23/12
 * Time: 4:35 PM
 */
class AssociationDialogRegistryTest {

	@Test
	void testGetBundle() {
		def b = new AssociationBundle()
		def chk = new JToggleButton()
		def select = new JComboBox();
		def reg = new AssociationDialogRegistry(b, chk, select);
		assert b == reg.getBundle()
	}

	@Test
	void testSetBundle() {
		def b = new AssociationBundle()
		def chk = new JToggleButton()
		def select = new JComboBox();
		def reg = new AssociationDialogRegistry(b, chk, select);
		assert b == reg.getBundle()
		reg.setBundle(null)
		assert reg.getBundle() != b
	}

	@Test
	void testGetButton() {
		def b = new AssociationBundle()
		def chk = new JToggleButton()
		def select = new JComboBox();
		def reg = new AssociationDialogRegistry(b, chk, select);
		assert reg.getButton() == chk
	}

	@Test
	void testSetButton() {
		def b = new AssociationBundle()
		def chk = new JToggleButton()
		def select = new JComboBox();
		def reg = new AssociationDialogRegistry(b, chk, select);
		assert reg.getButton() == chk
		def button2 = new JToggleButton()
		reg.setButton(button2)
		assert reg.getButton() != chk
		assert reg.getButton() == button2
	}

	@Test
	void testGetSelection() {
		def b = new AssociationBundle()
		def chk = new JToggleButton()
		def select = new JComboBox();
		def reg = new AssociationDialogRegistry(b, chk, select);
		assert reg.getSelection() == select
	}

	@Test
	void testSetSelection() {
		def b = new AssociationBundle()
		def chk = new JToggleButton()
		def select = new JComboBox();
		def reg = new AssociationDialogRegistry(b, chk, select);
		assert reg.getSelection() == select

		def select2 = new JComboBox()
		reg.setSelection(select2)
		assert reg.getSelection() != select
		assert reg.getSelection() == select2
	}

	@Test
	void testToString() {
		def b = new AssociationBundle()
		def chk = new JToggleButton()
		def select = new JComboBox();
		def reg = new AssociationDialogRegistry(b, chk, select);
		assert reg.toString() != null
		assert reg.toString().length() > 0
	}
}
