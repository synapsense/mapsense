package DeploymentLab.Dialogs.Association

import org.junit.*;
import static org.junit.Assert.*

import DeploymentLab.Model.*
import DeploymentLab.Image.ComponentIconFactory

import DeploymentLab.TestUtils;

/**
 *
 * @author Gabriel Helman
 * @since Mars
 * Date: 4/20/11
 * Time: 2:37 PM
 */
class AssociationControllerTest {

	static ObjectModel omodel
	static ComponentModel cmodel

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
	}


	@After
	void tearDown() {
		cmodel = null
		omodel = null
		println "Association Controller Test Complete"
	}


	@Test
	void someBasics() {
		def userinput = cmodel.newComponent("userinput")
		def summation = cmodel.newComponent("sum_operation")
		def ac = new AssociationController(userinput, summation)
		assert ac.getYin() == userinput
		assert ac.getYang() == summation
		assert ac.getYinIcon() == ComponentIconFactory.getComponentIcon(userinput)
		assert ac.getYangIcon() == ComponentIconFactory.getComponentIcon(summation)

		def doNothingBozo = cmodel.newComponent("remote-gateway")
		def theNonWonderDog = cmodel.newComponent("modbus-virtual-gateway")
		def ac2 = new AssociationController(doNothingBozo, theNonWonderDog)
		assertFalse(ac2.canAssociate())
	}

	@Test
	void testBasicPUEConnection() {
		println "testing controller"

		def userinput = cmodel.newComponent("userinput")
		def summation = cmodel.newComponent("sum_operation")

		def ac = new AssociationController(userinput, summation)

		assertTrue(ac.canAssociate())

		AssociationBundle resultBundle = ac.canSolo()
		assertTrue(resultBundle.isValid)
		assertTrue(resultBundle.make)

		assertTrue(resultBundle.getProducer() == resultBundle.producerComponent.getProducer(resultBundle.producerName))
		assertTrue(resultBundle.getConsumer() == resultBundle.consumerComponent.getConsumer(resultBundle.consumerName))

		assertTrue(resultBundle.getProducer() == userinput.getProducer(resultBundle.producerName))
		assertTrue(resultBundle.getConsumer() == summation.getConsumer(resultBundle.consumerName))


		def current = ac.calculateAssociations(userinput, summation, false)
		assert current == []
		current = ac.calculateAssociations(summation, userinput, false)
		assert current == []

		current = ac.getYangToYin()  //ac.calculateAssociations(summation, userinput, true)
		assert current.size() == 0

		current = ac.getYinToYang()  //ac.calculateAssociations(userinput, summation, true)
		assert current.size() == 1

		assert ac.calculateConducers(false).size() == 0

		assertFalse(ac.associationExists(resultBundle))
		ac.executeBundle(resultBundle)
		assertTrue(ac.associationExists(resultBundle))

		def undoBundle = new AssociationBundle(resultBundle)
		undoBundle.make = false
		ac.executeBundle(undoBundle)
		assertFalse(ac.associationExists(resultBundle))

		ac.applyChanges([resultBundle])
		assertTrue(ac.associationExists(resultBundle))

	}

	@Test
	void testDefaults() {
		println "testing default conections"

		// Producer/Consumer set must have matching names to be a default! Not id or type... but name... for some reason.
		def consumer = cmodel.newComponent("ez-exhaust")
		def producer = cmodel.newComponent("ez-rack-1-0")
		def ac = new AssociationController(consumer, producer)
		assert ac.hasDefaults()
		assert ac.getYinToYangDefaults().size() == 1
		assert ac.getYangToYinDefaults().size() == 0
	}

	/*
		Tests to add:
		don't count other associations against this one

		 */


	@Test
	void testLongestName() {
		DLComponent userinput = cmodel.newComponent("userinput")
		DLComponent summation = cmodel.newComponent("sum_operation")
		def ac = new AssociationController(userinput, summation)
		assertTrue(ac.canAssociate())
		String datatype = "power"
		String longestConsumerName = ac.getLongestConsumerName(datatype)
		String otherLongestName = ""
		userinput.listConsumers(false).each { c ->
			def con = userinput.getConsumer(c)
			if (con.getDatatype().equals(datatype)) {
				if (con.getName().length() > otherLongestName.length()) {
					otherLongestName = con.getName()
				}
			}
		}
		summation.listConsumers(false).each { c ->
			def con = summation.getConsumer(c)
			if (con.getDatatype().equals(datatype)) {
				if (con.getName().length() > otherLongestName.length()) {
					otherLongestName = con.getName()
				}
			}
		}
		assert longestConsumerName.equals(otherLongestName)
	}

	@Test
	void testSecondaryName() {

		/*
		secondary components are:
		Inspectors (host -> Insp, >1 possible producer on host but only 1 at a time

		Inspectors: use 'normal' logic, add producer name if there is >1 unused connection possible
		 */

		DLComponent optimus = cmodel.newComponent("transformer_secondary")
		optimus.setPropertyValue("name", "Optimus Prime")
		DLComponent eyeball = cmodel.newComponent("power_inspector")
		eyeball.setPropertyValue("name", "Eyeballz")

		def ac = new AssociationController(optimus, eyeball)
		assertTrue(ac.canAssociate())

		def possible = ac.calculateAssociations(optimus, eyeball, true)
		assert possible.size() == 3
		def bundleToMake = new AssociationBundle(possible[0])
		bundleToMake.make = true
		bundleToMake.consumerName = bundleToMake.consumerComponent.getAllConsumers().first().getId()
		ac.executeBundle(bundleToMake)

		def name = ac.calculateSecondaryName(optimus, eyeball)
		assert name.equals("Optimus Prime: ${bundleToMake.getProducer().getName()} - Inspector".toString())
	}


	/**
	 * If you have two bundles with the same connections, but one with make=true and one make=false, the result
	 * should be to make the connection
	 *
	 * Bug 6318
	 */
	@Test
	void multipleBundlesWithSameConnections() {

		String rackName = "Death Star Central Computer"
		def rack = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode2")
		rack.setPropertyValue("name", rackName)

		def refTemp = cmodel.newComponent("reftemp-thermanode")
		refTemp.setPropertyValue("name", "basement temperature")

		def ac = new AssociationController(rack, refTemp)

		List<AssociationBundle> bundles = []
		bundles.addAll(ac.calculateAssociations(rack, refTemp, true))
		bundles.addAll(ac.calculateAssociations(refTemp, rack, true))

		def initialBundle = bundles[0]
		initialBundle.isValid = true
		initialBundle.consumerName = initialBundle.getPossibleConsumers().first().getId()
		def dupeBundle = new AssociationBundle(initialBundle)
		dupeBundle.make = true
		dupeBundle.isValid = true
		def otherDupe = new AssociationBundle(initialBundle)
		bundles.add(dupeBundle)
		bundles.add(otherDupe)

		//bonus: make sure that equalsExceptForMake() works
		assert initialBundle.equalsExceptForMake(dupeBundle)
		assert !initialBundle.equals(dupeBundle)

		ac.applyChanges(bundles)
		//the two SHOULD be associated at this point
		assert ac.associationExists(bundles[0])
	}

	@Test
	void showTheButton() {
		def rplo = cmodel.newComponent("transformer_secondary")
		def avg = cmodel.newComponent("average_operation")
		def user = cmodel.newComponent("userinput")
		def rpli = cmodel.newComponent("transformer_primary")
		def sub = cmodel.newComponent("math_operation_subtraction")
		def cyberex = cmodel.newComponent("modbus-cyberex-bcms-custom")
		def squared = cmodel.newComponent("modbus-squared-bcms-custom")
		def pdu = cmodel.newComponent("pdi-pdu")
		def pue = cmodel.newComponent("pue_main")

		def ac1 = new AssociationController(rplo, avg)
		def ac2 = new AssociationController(rpli, rplo)
		def ac3 = new AssociationController(user, sub)
		def ac4 = new AssociationController(cyberex, pdu)
		def ac5 = new AssociationController(squared, pdu)
		def ac6 = new AssociationController(user, pue)
		def ac7 = new AssociationController(avg, pue)

		//println "show the button?"
		ac1.calculateAssociations(ac1.getYin(), ac1.getYang(), true).each { a ->
			//println "$a --> " + ac1.allowMoreThanOneConnection(a)
			assertFalse(ac1.allowMoreThanOneConnection(a))
		}
		ac2.calculateAssociations(ac2.getYin(), ac2.getYang(), true).each { a ->
			//println "$a --> " + ac2.allowMoreThanOneConnection(a)
			assertFalse(ac2.allowMoreThanOneConnection(a))
		}
		ac3.calculateAssociations(ac3.getYin(), ac3.getYang(), true).each { a ->
			//println "$a --> " + ac3.allowMoreThanOneConnection(a)
			assertTrue(ac3.allowMoreThanOneConnection(a))
		}
		ac4.calculateAssociations(ac4.getYin(), ac4.getYang(), true).each { a ->
			//println "$a --> " + ac4.allowMoreThanOneConnection(a)
			assertFalse(ac4.allowMoreThanOneConnection(a))
		}

		ac5.calculateAssociations(ac5.getYin(), ac5.getYang(), true).each { a ->
			//println "$a --> " + ac5.allowMoreThanOneConnection(a)
			assertFalse(ac5.allowMoreThanOneConnection(a))
		}
		ac6.calculateAssociations(ac6.getYin(), ac6.getYang(), true).each { a ->
			//println "$a --> " + ac6.allowMoreThanOneConnection(a)
			assertTrue(ac6.allowMoreThanOneConnection(a))
		}
		ac7.calculateAssociations(ac7.getYin(), ac7.getYang(), true).each { a ->
			//println "$a --> " + ac7.allowMoreThanOneConnection(a)
			assertTrue(ac7.allowMoreThanOneConnection(a))
		}

	}

	@Test
	void conducerExists() {
		def bcms = cmodel.newComponent("modbus-squared-bcms-custom")
		def rack = cmodel.newComponent("standalone_power_rack")
		def ac = new AssociationController(bcms, rack)
		def bundle = new AssociationBundle(bcms, bcms.getConducer("CircuitPhase01"), rack)
		assertFalse(bcms.getConducer("CircuitPhase01").hasProsumer())
		assertTrue(bundle.isConducer())
		assertFalse(bundle.alpha.isProsuming(bundle.omega))
		assertFalse(ac.associationExists(bundle))
	}

	@Test
	void multipleConducersExist() {
		def bcms = cmodel.newComponent("modbus-squared-bcms-custom")
		def rack = cmodel.newComponent("standalone_power_rack")
		def ac = new AssociationController(bcms, rack)
		def bundles = ac.calculateConducers(true)
		assertFalse(ac.associationsExist(bundles))
		def first = bundles.first()
		first.omega = first.omegaComponent.getConducer(first.omegaComponent.listConducers().first())
		first.make = true
		ac.executeBundle(first)
		assertFalse(ac.associationsExist(bundles))
	}

	@Test
	void correctNumberOfConducers() {
		def bcms = cmodel.newComponent("modbus-squared-bcms-custom")
		def rack = cmodel.newComponent("standalone_power_rack")
		def ac = new AssociationController(bcms, rack)
		assert ac.calculateConducers(true).size() == 42
		assert bcms.listConducers().size() == 42
		def ac2 = new AssociationController(rack, bcms)
		assert ac2.calculateConducers(true).size() == 6
		assert rack.listConducers().size() == 7
	}


	@Test
	void thePlusButton() {
		//println "*****"
		//println "THE PLUS BUTTON TEST"
		def avg = cmodel.newComponent("average_operation")
		def add = cmodel.newComponent("math_operation_addition")

		def ac = new AssociationController(avg, add)
		//println "--init dialog"
		ac.initDialog()
		//println "--checking starting associations"
		assert ac.calculateAssociations(ac.getYin(), ac.getYang(), true).size() == 1
		assert ac.calculateAssociations(ac.getYang(), ac.getYin(), true).size() == 1


		assert ac.dialog.registry.size() == 2 //total lines in dialog box
		//for( def r : ac.dialog.registry ){
		//	println r
		//}

		//String buttonKey = "average_operation#value"
		//println "-- ** FIRST EVENT **"
		//def event = new ActionEvent("hi!", 13, buttonKey)
		//ac.actionPerformed(event)
		ac.dialog.plusButtons.first().doClick()

		//println "--registry after first event"
		//for( def r : ac.dialog.registry ){
		//	println r
		//}
		assert ac.dialog.registry[0].getBundle().producerComponent == avg
		assert ac.dialog.registry[1].getBundle().producerComponent == avg
		assert ac.dialog.registry[2].getBundle().producerComponent == add

		//println "--checking controller after first event"
		assert ac.calculateAssociations(ac.getYin(), ac.getYang(), true).size() == 2
		assert ac.calculateAssociations(ac.getYang(), ac.getYin(), true).size() == 1

		//println "--registry after first event and recalculations"
		assert ac.dialog.registry.size() == 3 //total lines in dialog box
		//for( def r : ac.dialog.registry ){
		//	println r
		//}

		//println "--second event"
		//ac.actionPerformed(event)
		ac.dialog.plusButtons.first().doClick()
		assert ac.calculateAssociations(ac.getYin(), ac.getYang(), true).size() == 3
		assert ac.calculateAssociations(ac.getYang(), ac.getYin(), true).size() == 1

		assert ac.dialog.registry[0].getBundle().producerComponent == avg
		assert ac.dialog.registry[1].getBundle().producerComponent == avg
		assert ac.dialog.registry[2].getBundle().producerComponent == avg
		assert ac.dialog.registry[3].getBundle().producerComponent == add

		assert ac.dialog.plusButtons.size() == 1

		ac.dialog.plusButtons.last().doClick()
		assert ac.calculateAssociations(ac.getYin(), ac.getYang(), true).size() == 4
		assert ac.calculateAssociations(ac.getYang(), ac.getYin(), true).size() == 1

		assert ac.dialog.registry[0].getBundle().producerComponent == avg
		assert ac.dialog.registry[1].getBundle().producerComponent == avg
		assert ac.dialog.registry[2].getBundle().producerComponent == avg
		assert ac.dialog.registry[3].getBundle().producerComponent == avg
		assert ac.dialog.registry[4].getBundle().producerComponent == add
	}

	@Test
	void testAssociateToProducerRack() {
		println "testAssociateToProducer()"
		def rackPro = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode2")
		def rackCon = cmodel.newComponent("rack-control-toprearexhaust-nsf-rt-thermanode2")

		def ac = new AssociationController(rackPro, rackCon)
		assertTrue(ac.canAssociate())
		def solo = ac.canSolo()
		println solo
		assert solo.isValid
		assert solo.make
		Producer p = rackPro.getProducer("reftemp")
		def result = ac.associateToProducer(rackPro, p, rackCon)
		assertTrue(result)
		def ab = new AssociationBundle(rackPro, "reftemp", rackCon, "reftemp", true)
		assertTrue(ac.associationExists(ab))
	}

	/* todo: I think this is a valid test that needs to be done with different components now that COINs are gone.
	@Test
	void testAssociateToProducerDualInlet() {
		println "testAssociateToProducer()"
		def rack = cmodel.newComponent("dual_inlet_rack")
		def coin1 = cmodel.newComponent("controlpoint_single_temperature")
		def coin2 = cmodel.newComponent("controlpoint_single_temperature")

		def ac = new AssociationController(rack, coin1)
		assertTrue(ac.canAssociate())
		def solo = ac.canSolo()
		println solo

		Producer p1 = rack.getProducer("rackAtop")
		assertTrue(ac.associateToProducer(rack, p1, coin1))
		def firstBundle = new AssociationBundle(rack, p1.getId(), coin1, "input", true)
		assertTrue(ac.associationExists(firstBundle))

		def ac2 = new AssociationController(rack, coin2)
		assertTrue(ac2.canAssociate())

		Producer p2 = rack.getProducer("rackBtop")
		assertTrue(ac2.associateToProducer(rack, p2, coin2))
		def secondBundle = new AssociationBundle(rack, p2.getId(), coin2, "input", true)
		assertTrue(ac.associationExists(secondBundle))

		assert rack.getProducerConsumers().size() == 2
	}
*/

	@Test
	void keepEditsOnPlus() {
		def exp = cmodel.newComponent("math_operation_exponentiation")
		def rpl = cmodel.newComponent("transformer_primary")

		def ac = new AssociationController(rpl, exp)

		ac.initDialog()
		//println "--checking starting associations"
		assert ac.calculateAssociations(ac.getYin(), ac.getYang(), true).size() == 3
		assert ac.calculateAssociations(ac.getYang(), ac.getYin(), true).size() == 1

		assert ac.dialog.registry.size() == 4 //total lines in dialog box

		assert ((Consumer) ac.dialog.selectionCombos[0].getSelectedItem()).getName().equals("Base")
		assert ((Consumer) ac.dialog.selectionCombos[1].getSelectedItem()).getName().equals("Base")
		assert ((Consumer) ac.dialog.selectionCombos[2].getSelectedItem()).getName().equals("Base")
		assert ((Consumer) ac.dialog.selectionCombos[3].getSelectedItem()).getName().equals("Input Power")

		ac.dialog.selectionCombos[1].setSelectedItem(ac.dialog.selectionCombos[0].getItemAt(1))

		assert ((Consumer) ac.dialog.selectionCombos[0].getSelectedItem()).getName().equals("Base")
		assert ((Consumer) ac.dialog.selectionCombos[1].getSelectedItem()).getName().equals("Exponent")
		assert ((Consumer) ac.dialog.selectionCombos[2].getSelectedItem()).getName().equals("Base")
		assert ((Consumer) ac.dialog.selectionCombos[3].getSelectedItem()).getName().equals("Input Power")

		//whack the plus button
		ac.dialog.plusButtons.first().doClick()

		assert ac.dialog.registry.size() == 5 //total lines in dialog box

		assert ((Consumer) ac.dialog.selectionCombos[0].getSelectedItem()).getName().equals("Base")
		assert ((Consumer) ac.dialog.selectionCombos[1].getSelectedItem()).getName().equals("Base")
		assert ((Consumer) ac.dialog.selectionCombos[2].getSelectedItem()).getName().equals("Exponent")
		assert ((Consumer) ac.dialog.selectionCombos[3].getSelectedItem()).getName().equals("Base")
		assert ((Consumer) ac.dialog.selectionCombos[4].getSelectedItem()).getName().equals("Input Power")

	}
/*
	@Test
	void nullAssociate(){
		def userinput = cmodel.newComponent("userinput")
		def summation = cmodel.newComponent("sum_operation")
		def ac = new AssociationController(userinput, summation)
		assert ac.getYin() == userinput
		assert ac.getYang() == summation
		
		assert ac.associate(null)
		assert userinput.getProducerConsumers().contains(summation)


		def rplo = cmodel.newComponent("transformer_secondary")
		def avg = cmodel.newComponent("average_operation")
		def ac2 = new AssociationController(rplo, avg)
		assert ac2.associate(null) == false
		assert rplo.getProducerConsumers().isEmpty()
		assert avg.getProducerConsumers().isEmpty()
	}
*/
	
	@Test
	void currentAssocs(){
		def avg = cmodel.newComponent("average_operation")
		def add = cmodel.newComponent("sum_operation")
		def ac = new AssociationController(avg, add)
		assert ac.currentConnections() == 0
		assert ac.totalConnections() > 0
		def bundle = new AssociationBundle(avg,"value",add,"inputA",true)
		bundle.make = true
		ac.executeBundle(bundle)
		assert ac.currentConnections() == 1
	}

	@Test
	void simpleAssociate(){
		def avg = cmodel.newComponent("average_operation")
		def add = cmodel.newComponent("sum_operation")
		def ac = new AssociationController(avg, add)
		assert ac.currentConnections() == 0
		assert ac.totalConnections() > 0
		assert ac.associate(avg,"value",add,"inputA")
		assert ac.currentConnections() == 1
	}

}
