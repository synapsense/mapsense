package DeploymentLab.Dialogs.Association

import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ObjectModel
import org.junit.BeforeClass
import DeploymentLab.TestUtils
import org.junit.Before

import org.junit.After
import org.junit.Test
import DeploymentLab.Dialogs.Association.AssociationBundle

/**
 * 
 * @author Gabriel Helman
 * @since 
 * Date: 3/21/12
 * Time: 2:50 PM
 */
class AssociationBundleTest {


	ComponentModel cmodel = null
	ObjectModel omodel = null

	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
	}

	@Test
	void testGetProducer() {
		def avg = cmodel.newComponent("average_operation")
		def add = cmodel.newComponent("sum_operation")
		def bundle = new AssociationBundle(avg,"value",add,"inputA",true)
		assert bundle.getProducer() == avg.getProducer("value")
	}

	@Test
	void testGetConsumer() {
		def avg = cmodel.newComponent("average_operation")
		def add = cmodel.newComponent("sum_operation")
		def bundle = new AssociationBundle(avg,"value",add,"inputA",true)
		assert bundle.getConsumer() == add.getConsumer("inputA")
	}

	@Test
	void testGetAlphaComponent() {
		def bcms = cmodel.newComponent("modbus-squared-bcms-custom")
		def rack = cmodel.newComponent("standalone_power_rack")
		def bundle = new AssociationBundle(bcms, bcms.getConducer("CircuitPhase01"), rack)
		assert bundle.getAlphaComponent() == bcms
	}

	@Test
	void testGetOmegaComponent() {
		def bcms = cmodel.newComponent("modbus-squared-bcms-custom")
		def rack = cmodel.newComponent("standalone_power_rack")
		def bundle = new AssociationBundle(bcms, bcms.getConducer("CircuitPhase01"), rack)
		assert bundle.getOmegaComponent() == rack
	}

	@Test
	void testIsConducer() {
		def bcms = cmodel.newComponent("modbus-squared-bcms-custom")
		def rack = cmodel.newComponent("standalone_power_rack")
		def bundle1 = new AssociationBundle(bcms, bcms.getConducer("CircuitPhase01"), rack)
		assert bundle1.isConducer()

		def avg = cmodel.newComponent("average_operation")
		def add = cmodel.newComponent("sum_operation")
		def bundle2 = new AssociationBundle(avg,"value",add,"inputA",true)
		assert ! bundle2.isConducer()
	}

	@Test
	void testConsumerIsCollection() {
		def bcms = cmodel.newComponent("modbus-squared-bcms-custom")
		def rack = cmodel.newComponent("standalone_power_rack")
		def bundle1 = new AssociationBundle(bcms, bcms.getConducer("CircuitPhase01"), rack)
		assert ! bundle1.consumerIsCollection()

		def avg = cmodel.newComponent("average_operation")
		def add = cmodel.newComponent("sum_operation")
		def bundle2 = new AssociationBundle(avg,"value",add,"inputA",true)
		assert bundle2.consumerIsCollection()

		def sqrt = cmodel.newComponent("math_operation_sqrt")
		def bundle3 = new AssociationBundle(avg,"value",sqrt,"input",true)
		assert !bundle3.consumerIsCollection()
	}

	@Test
	void testProducerIsSolo() {
		assert ! new AssociationBundle().producerIsSolo()
	}
/*
	@Test
	void testGetPossibleConsumers() {

	}

	@Test
	void testIsDefault() {

	}

	@Test
	void testToString() {

	}

	@Test
	void testCompareTo() {

	}
*/
	@Test
	void testEquals() {
		def avg = cmodel.newComponent("average_operation")
		def add = cmodel.newComponent("sum_operation")
		def bundle1 = new AssociationBundle(avg,"value",add,"inputA",true)
		def bundle2 = new AssociationBundle(avg,"value",add,"inputA",true)
		def sqrt = cmodel.newComponent("math_operation_sqrt")
		def bundle3 = new AssociationBundle(avg,"value",sqrt,"input",true)
		
		assert bundle1.equals(bundle2)
		assert !bundle1.equals(bundle3)
		assert !bundle1.equals(42)
	}

	@Test
	void testEqualsExceptForMake() {
		def avg = cmodel.newComponent("average_operation")
		def add = cmodel.newComponent("sum_operation")
		def bundle1 = new AssociationBundle(avg,"value",add,"inputA",true)
		def bundle2 = new AssociationBundle(avg,"value",add,"inputA",true)
		bundle1.make = true
		bundle2.make = false
		assert !bundle1.equals(bundle2)
	}

	@Test
	void testHashCode() {
		def avg = cmodel.newComponent("average_operation")
		def add = cmodel.newComponent("sum_operation")
		def bundle1 = new AssociationBundle(avg,"value",add,"inputA",true)
		def bundle2 = new AssociationBundle(avg,"value",add,"inputA",true)
		def sqrt = cmodel.newComponent("math_operation_sqrt")
		def bundle3 = new AssociationBundle(avg,"value",sqrt,"input",true)

		assert bundle1.hashCode().equals(bundle2.hashCode())
		assert !bundle1.hashCode().equals(bundle3.hashCode())
		assert !bundle1.hashCode().equals(42.hashCode())
	}
/*
	@Test
	void testContainsExceptForMake() {

	}
	*/
}
