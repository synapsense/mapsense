package DeploymentLab.Dialogs.StyledMessage

import org.junit.Test;

/**
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/23/12
 * Time: 3:53 PM
 */
public class StyledMessageControllerTest {

	@Test
	void basics(){
		String contents = "<html><head></head><body>Hi there!</body></html>"
		String title = "title"
		int type = 42

		def smc = new StyledMessageController(contents,title,type)

		assert smc.getContents() == contents
		assert smc.getMessageTitle() == title
		assert smc.getMessageType() == type

		smc.getDialog().getDialog() //init the dialog, but don't show it
		//assert smc.getDialog().scrollingMessageText.text == contents //actually, the html jpane reworks this a biy
		smc.getDialog().ok()

		//assert smc.dialog == null
	}

}
