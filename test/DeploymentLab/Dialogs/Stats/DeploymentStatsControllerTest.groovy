package DeploymentLab.Dialogs.Stats

import DeploymentLab.CentralCatalogue
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ObjectModel
import org.junit.BeforeClass
import DeploymentLab.TestUtils
import org.junit.Before
import DeploymentLab.UndoBuffer
import javax.swing.JFrame
import org.junit.After
import org.junit.Test
import DeploymentLab.channellogger.Logger

/**
 * Tests for the stats dialog
 * @author Gabriel Helman
 * @since Jupiter
 * Date: 4/3/12
 * Time: 10:00 AM
 */
class DeploymentStatsControllerTest {
	ComponentModel cmodel = null
	ObjectModel omodel = null
	static UndoBuffer ub
	private static final Logger log = Logger.getLogger(DeploymentStatsControllerTest.class.getName())

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		ub = CentralCatalogue.getUndoBuffer()
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
	}


	// Expected Results for "build/testFiles/statsTest.dlz"
	private static final List<String> thingsNotInTotal = ["Drawing","Room","Orphanage","Planning Group","Uninstrumented Rack","Door","Text"]
	private static final Map<String,Integer> thingsInTotal = ["Gateway" : 1,
	                                                          "WSN Network" : 1,
	                                                          "Rear Exhaust w/ Subfloor (TN2)" : 5,
	                                                          "CRAH Temp/Hum" : 2]

	@Test
	void basicStatsTest() {
		def stats = new DeploymentStatsController(cmodel, new JFrame())
		TestUtils.loadProjectFile("build/testFiles/statsTest.dlz",ub, omodel, cmodel)

		// Make sure the test project has some of the negative test types so we don't get false positives just cuz it's not in the project.
		Set<String> thingsWeHave = [] as Set
		cmodel.getComponents().each { thingsWeHave += it.getDisplaySetting('name') }
		assert thingsWeHave.containsAll( thingsNotInTotal )

		stats.populate()
		assert stats.rooms.size() == 3  // 2 Rooms + Orphanage
		assert stats.zones.size() == 1  // Planning Group
		assert stats.gateways.size() == 1
		assert stats.totals.size() == thingsInTotal.size()

		thingsNotInTotal.each { assert !stats.totals.containsKey(it) }

		thingsInTotal.each { k, v ->
			assert stats.totals.containsKey( k )
			assert stats.totals.get( k ) == v
		}

		String text = stats.updateText()

		assert text != null
		assert text.size() > 1
	}

	@Test
	void statsClear() {
		def stats = new DeploymentStatsController(cmodel, new JFrame())
		TestUtils.loadProjectFile("build/testFiles/statsTest.dlz", ub, omodel, cmodel)
		stats.populate()
		stats.updateText()
		stats.populate()
		stats.updateText()

		assert stats.zones.size() > 0
		assert stats.gateways.size() > 0
		assert stats.totals.size() > 0
		assert stats.rooms.size() > 0
		assert stats.drawings.size() > 0

		cmodel.clear()

		assert stats.rooms.size() == 0
		assert stats.gateways.size() == 0
		assert stats.totals.size() == 0
		assert stats.rooms.size() == 0
		assert stats.drawings.size() == 0
	}

}
