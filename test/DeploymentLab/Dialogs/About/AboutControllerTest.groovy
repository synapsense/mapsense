package DeploymentLab.Dialogs.About

import org.junit.Test
import javax.swing.JFrame;

/**
 * Basic test for the revised About box.  Really, we're just checking that nothing blows up.
 * (And keeping the unit test package percentage at 100%)
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 6/25/12
 * Time: 12:17 PM
 */
public class AboutControllerTest {


	@Test
	public void basicTest() {
		def ac = new AboutController()
	}

	@Test
	public void aboutBox() {
		def ac = new AboutController()
		//ac.showDialog(new JFrame())
		def about = ac.getAboutDialog(new JFrame())
		about.initDialog()
		assert about == ac.dialog
		about.close()
		about.dispose()
	}

	@Test
	public void licenseBox() {
		def ac = new AboutController()
		//ac.showLicenseDialog(new JFrame())
		def lic = ac.getLicenseDialog(new JFrame())
		lic.initDialog()
		assert lic == ac.licenseDialog
		lic.close()
		lic.dispose()
	}

}
