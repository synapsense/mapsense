package DeploymentLab.Dialogs.ChooseDrawing

import DeploymentLab.CentralCatalogue
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Model.ObjectType
import DeploymentLab.SelectModel
import DeploymentLab.TestUtils
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

import javax.swing.JFrame

import DeploymentLab.Model.DLComponentNameComparator
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.ProblemLogger
import DeploymentLab.channellogger.LogLevel

/**
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/6/12
 * Time: 11:47 AM
 */
public class ChooseDrawingControllerTest {

	ComponentModel cmodel = null
	ObjectModel omodel = null
	SelectModel selectModel = null

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
		selectModel = null
	}

	@Test
	public void basicSantityCheck() {
		assert ChooseDrawingController.chooseDrawing(selectModel, cmodel).size() == 1
	}


	@Test
	public void dialogSanity() {
		def cfd = new ChooseDrawingController(selectModel, cmodel)
		def dialog = cfd.getDialog()
		dialog.initDialog()
		assert dialog == cfd.dialog
		dialog.close()
		dialog.dispose()
	}

	@Test
	public void multiSelection() {
		def drawing = selectModel.getActiveDrawing()
		def drawing2 = cmodel.newComponent("drawing")
		def drawing3 = cmodel.newComponent("drawing")
		drawing.setPropertyValue("name", "first")
		drawing2.setPropertyValue("name", "second")
		drawing3.setPropertyValue("name", "third")
		println "there are ${cmodel.getComponentsByType("drawing").size()} drawings!!"

		selectModel.setActiveDrawing(drawing)

		def cfc = new ChooseDrawingController(selectModel, cmodel)
		assert cfc.getData().keySet().size() == 3

		assert cfc.getData()[drawing]
		assert !cfc.getData()[drawing2]
		assert !cfc.getData()[drawing3]

		//this is what the dialog does to sort the data
		HashMap<DLComponent, Boolean> data = cfc.getData()
		ArrayList<DLComponent> sortedKeys = new ArrayList<DLComponent>(data.keySet())
		sortedKeys.sort(new DLComponentNameComparator())
		assert sortedKeys[0] == drawing
		assert sortedKeys[1] == drawing2
		assert sortedKeys[2] == drawing3

		cfc.checkboxChecked(drawing)

		assert !cfc.getData()[drawing]

		cfc.checkboxChecked(drawing2)
		cfc.checkboxChecked(drawing3)

		assert !cfc.getData()[drawing]
		assert cfc.getData()[drawing2]
		assert cfc.getData()[drawing3]

		cfc.cancel()

		assert cfc.generateResults().size() == 0

		cfc.ok()

		assert cfc.generateResults().size() == 2
		//generate results should hand them back out in the same order as the dialog shows them
		assert cfc.generateResults()[0] == drawing2
		assert cfc.generateResults()[1] == drawing3
	}

	@Test
	void noDrawingSelected(){
		def drawing = selectModel.getActiveDrawing()
		def drawing2 = cmodel.newComponent("drawing")
		def drawing3 = cmodel.newComponent("drawing")
		drawing.setPropertyValue("name", "first")
		drawing2.setPropertyValue("name", "second")
		drawing3.setPropertyValue("name", "third")
		println "there are ${cmodel.getComponentsByType("drawing").size()} drawings!!"

		selectModel.setActiveDrawing(null)

		def cfc = new ChooseDrawingController(selectModel, cmodel)
		assert cfc.getData().keySet().size() == 3
		cfc.getData().each{ k, v ->
			assert v == false
		}
		assert !cfc.getData()[drawing]
		assert !cfc.getData()[drawing2]
		assert !cfc.getData()[drawing3]
	}

	@Test
	void getObjectsTest(){
		def drawings =  ChooseDrawingController.chooseDrawing(selectModel, cmodel)
		def obs = ChooseDrawingController.getDrawingObjects(drawings)
		assert obs.size() == 1
		assert obs[0] == drawings[0].getRoot( ObjectType.DRAWING )
	}

	@Test(expected=UnsupportedOperationException.class)
	void badGetObjectsTest(){
		cmodel.newComponent("pressure2")
		cmodel.newComponent("pressure2")
		def obs = ChooseDrawingController.getDrawingObjects(cmodel.getComponents())
	}

	@Test
	void singleSelectMode(){
		assert ChooseDrawingController.chooseOneDrawing(selectModel, cmodel).size() == 1
	}

	@Test
	void singleSelectModeWithMoreThanOne(){
		def drawing = selectModel.getActiveDrawing()
		def drawing2 = cmodel.newComponent("drawing")
		def drawing3 = cmodel.newComponent("drawing")
		drawing.setPropertyValue("name", "first")
		drawing2.setPropertyValue("name", "second")
		drawing3.setPropertyValue("name", "third")
		println "there are ${cmodel.getComponentsByType("drawing").size()} drawings!!"

		selectModel.setActiveDrawing(drawing)

		def cfc = new ChooseDrawingController(selectModel, cmodel)
		cfc.setAllowsMoreThanOneDrawing(false)
		assert cfc.getData().keySet().size() == 3

		assert cfc.getData()[drawing]
		assert !cfc.getData()[drawing2]
		assert !cfc.getData()[drawing3]

		//this is what the dialog does to sort the data
		HashMap<DLComponent, Boolean> data = cfc.getData()
		ArrayList<DLComponent> sortedKeys = new ArrayList<DLComponent>(data.keySet())
		sortedKeys.sort(new DLComponentNameComparator())
		assert sortedKeys[0] == drawing
		assert sortedKeys[1] == drawing2
		assert sortedKeys[2] == drawing3

		//cfc.checkboxChecked(drawing)

		//assert !cfc.getData()[drawing]

		cfc.checkboxChecked(drawing2)
		cfc.checkboxChecked(drawing3)

		assert !cfc.getData()[drawing]
		assert !cfc.getData()[drawing2]
		assert cfc.getData()[drawing3]

		cfc.cancel()

		assert cfc.generateResults().size() == 0

		cfc.ok()

		assert cfc.generateResults().size() == 1
		//generate results should hand them back out in the same order as the dialog shows them
		assert cfc.generateResults()[0] == drawing3
		//assert cfc.generateResults()[1] == drawing3
	}

	@Test
	void selectAllAndNone(){
		def drawing = selectModel.getActiveDrawing()
		def drawing2 = cmodel.newComponent("drawing")
		def drawing3 = cmodel.newComponent("drawing")
		drawing.setPropertyValue("name", "first")
		drawing2.setPropertyValue("name", "second")
		drawing3.setPropertyValue("name", "third")
		println "there are ${cmodel.getComponentsByType("drawing").size()} drawings!!"

		selectModel.setActiveDrawing(drawing)

		def cfc = new ChooseDrawingController(selectModel, cmodel)
		cfc.setAllowsMoreThanOneDrawing(false)
		assert cfc.getData().keySet().size() == 3

		assert cfc.getData()[drawing]
		assert !cfc.getData()[drawing2]
		assert !cfc.getData()[drawing3]

		cfc.selectAll()
		assert cfc.getData()[drawing]
		assert cfc.getData()[drawing2]
		assert cfc.getData()[drawing3]

		cfc.selectNone()
		assert !cfc.getData()[drawing]
		assert !cfc.getData()[drawing2]
		assert !cfc.getData()[drawing3]

	}

}
