package DeploymentLab.Dialogs

import DeploymentLab.CentralCatalogue

import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

import DeploymentLab.Model.ComponentBinding
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLObject
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Model.Setting
import DeploymentLab.TestUtils
import DeploymentLab.UndoBuffer


/**
 *
 *
 * @author Ken Scoggins
 * @since  Europa
  */
 class ScanMACDialogTest {

	ComponentModel cmodel = null
	ObjectModel omodel = null
	UndoBuffer undoBuffer = null

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		//TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.WARN)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		undoBuffer = CentralCatalogue.getUndoBuffer()
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
		undoBuffer = null
	}


	/**
	 * The dialog is assuming that MAC ID properties only map to a single object. Run a check just to make sure.
	 */
	@Test
	void checkMacIdBindings() {
		TestUtils.loadProjectFile( TestUtils.CURRENT_VERSION_LINT, undoBuffer, omodel, cmodel )

		for( DLComponent c : cmodel.getComponents() ) {
			for( String pName : c.listMacIdProperties() ) {
				def boundObjs = [] as Set
				for( ComponentBinding binding : c.getBindingsForProp( pName ) ) {
					for( Setting setting : binding.getTargetSettings() ) {
						DLObject o = setting.getOwner()
						if( o.getType().startsWith('wsn') ) {
							boundObjs.add( o )

							if( o.getType().startsWith('wsnnode') ) {
								// Also make sure it has the platform info we are relying on.
								assert o.getObjectProperty('platformName').getValue(), "Node is missing platformName. $c :: $o"
							    assert o.getObjectProperty('platformId').getValue() > 0, "Node is missing platformId. $c :: $o"
							}
						}
					}
				}

				assert boundObjs.size() == 1, "Property '$pName' bound to multiple nodes $boundObjs.  $c"
			}
		}
	}
 }
