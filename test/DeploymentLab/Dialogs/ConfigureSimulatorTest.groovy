package DeploymentLab.Dialogs

import org.junit.*;
import static org.junit.Assert.*

/**
 * 
 * @author Gabriel Helman
 * @since 
 * Date: 8/31/11
 * Time: 11:46 AM
 */
 class ConfigureSimulatorTest {

	 static ArrayList columnNames = ["Component Name", "sensor name", "value", "time", "dlid"]

	 @Test
	 void tableModelTest(){

		 def mapList = [
		         ["Component Name" : "first", "sensor name" : "hot top", "value" : "0", "time" : "30", "dlid" : "100"],
				 ["Component Name" : "first", "sensor name" : "cold top", "value" : "1", "time" : "30", "dlid" : "101"],
				 ["Component Name" : "second", "sensor name" : "hot top", "value" : "2", "time" : "30", "dlid" : "102"],
				 ["Component Name" : "second", "sensor name" : "cold top", "value" : "3", "time" : "30", "dlid" : "103"],
		 ]



		 ConfigureSimulatorTableModel tm = new ConfigureSimulatorTableModel(mapList, columnNames, [2,3])

		 println tm.getData()

		 println tm.getDataAsList()


	 }

}
