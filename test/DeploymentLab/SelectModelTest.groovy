package DeploymentLab

import org.junit.*
import DeploymentLab.Model.*
import DeploymentLab.channellogger.LogLevel
import DeploymentLab.channellogger.ChannelManager;

/**
 * Test the SelectModel as hard as we can
 * @author Gabriel Helman
 * @since Mars
 * Date: 7/8/11
 * Time: 11:32 AM
 */
class SelectModelTest {

	static ObjectModel omodel
	static ComponentModel cmodel
	static SelectModel selectModel


	private SelectionChangeEvent currentEvent

	private class FakeListener implements SelectionChangeListener{
		@Override
		void selectionChanged(SelectionChangeEvent e) {
			currentEvent = e
		}

		@Override
		void activeDrawingChanged(DLComponent oldDrawing, DLComponent newDrawing) {
			//println "new ADC: $newDrawing"
		}
	}


	private class FakeListenerStack implements SelectionChangeListener{
		private ArrayDeque<SelectionChangeEvent> events = new ArrayDeque<>();

		@Override
		void selectionChanged(SelectionChangeEvent e) {
			this.events.push(e)
		}

		@Override
		void activeDrawingChanged(DLComponent oldDrawing, DLComponent newDrawing) {
			//println "new ADC: $newDrawing"
		}

		public clearStack(){
			this.events.clear()
		}

		public ArrayDeque<SelectionChangeEvent> stack(){
			return events
		}
	}

	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		//TestUtils.enableLogging( DeploymentLab.channellogger.LogLevel.ERROR )
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()

		//selectModel.addSelectionChangeListener( { sce -> if(sce instanceof DeploymentLab.SelectionChangeEvent){currentEvent = sce} } as SelectionChangeListener )
		selectModel.addSelectionChangeListener(new FakeListener())
		selectModel.addSelectionChangeListener(new SelectModelWatcher())

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))
	}


	@After
	void tearDown() {
		cmodel = null
		omodel = null
		selectModel = null
		//have fun, garbage collector!
	}


	@Test
	void basicSelectTest(){
		def calc = cmodel.newComponent("logicalzone")
		selectModel.getActiveDrawing().addChild(calc)

		def sum = cmodel.newComponent("sum_operation")
		def user = cmodel.newComponent("userinput")
		user.setPropertyValue("name", "Ultimate Answer")
		user.setPropertyValue("value", "42")

		calc.addChild(sum)
		calc.addChild(user)


		selectModel.setSelection(sum)
		assert selectModel.getExpandedSelection().size() == 1

		selectModel.select(user)
		assert selectModel.getExpandedSelection().size() == 2

		selectModel.unSelect(sum)
		assert selectModel.getExpandedSelection().size() == 1

		//println currentEvent

	}

	/**
	 * Tests that all 4 logical group ops leave the operated-on components selected.
	 * Regression test for Bug 4195
	 */
	@Test
	void groupOps(){
		DLComponent room = cmodel.newComponent( ComponentType.ROOM )
		selectModel.getActiveDrawing().addChild( room )

		DLComponent group1 = cmodel.newComponent( ComponentType.PLANNING_GROUP )
		DLComponent group2 = cmodel.newComponent( ComponentType.PLANNING_GROUP )
		selectModel.getActiveDrawing().addChild( group1 )
		selectModel.getActiveDrawing().addChild( group2 )

		def rack1 = cmodel.newComponent( TestUtils.DEFAULT_RACK )
		def rack2 = cmodel.newComponent( TestUtils.DEFAULT_RACK )
		def rack3 = cmodel.newComponent( TestUtils.DEFAULT_RACK )
		def rack4 = cmodel.newComponent( TestUtils.DEFAULT_RACK )
		room.addChild( rack1 )
		room.addChild( rack2 )
		room.addChild( rack3 )
		room.addChild( rack4 )

		def selection = [ rack1, rack2, rack3 ]

		selectModel.setSelectedObjects( selection )
		assert selectModel.getSelectedObjects().size() == selection.size() && selectModel.getSelectedObjects().containsAll( selection )

		// Stay selected during an add?
		selection.each { group1.addChild( it ) }
		assert group1.getChildComponents().size() == selection.size() && group1.getChildComponents().containsAll( selection )
		assert group2.getChildComponents().isEmpty()
		assert selectModel.getSelectedObjects().size() == selection.size() && selectModel.getSelectedObjects().containsAll( selection )

		// Stay selected during a move? (Also covers remove since a move is a remove + add)
		selection.each { group1.removeChild( it ) }
		assert group1.getChildComponents().isEmpty()
		assert group2.getChildComponents().isEmpty()
		assert selectModel.getSelectedObjects().size() == selection.size() && selectModel.getSelectedObjects().containsAll( selection )

		selection.each { group2.addChild( it ) }
		assert group1.getChildComponents().isEmpty()
		assert group2.getChildComponents().size() == selection.size() && group2.getChildComponents().containsAll( selection )
		assert selectModel.getSelectedObjects().size() == selection.size() && selectModel.getSelectedObjects().containsAll( selection )

		// Stay selected when the group is removed?
		cmodel.remove( group2 )
		assert cmodel.getComponents().contains( group1 )
		assert !cmodel.getComponents().contains( group2 )
		assert group1.getChildComponents().isEmpty()
		assert group2.getChildComponents().isEmpty()
		assert selectModel.getSelectedObjects().size() == selection.size() && selectModel.getSelectedObjects().containsAll( selection )

		cmodel.remove( group1 )
		assert !cmodel.getComponents().contains( group1 )
		assert !cmodel.getComponents().contains( group2 )
		assert group1.getChildComponents().isEmpty()
		assert group2.getChildComponents().isEmpty()
		assert selectModel.getSelectedObjects().size() == selection.size() && selectModel.getSelectedObjects().containsAll( selection )

		selectModel.clearSelection()
		assert selectModel.getSelectedObjects().isEmpty()
	}

	@Test
	void modifySelection(){
		//a note on list compares: the list-returning methods of the selectModel & events are not order preserving (by design)
		//hence, the .sort() calls all over the place

		DLComponent drawing = cmodel.newComponent("drawing")
		selectModel.setActiveDrawing(drawing)
		DLComponent calcs = cmodel.newComponent("logicalzone")
		drawing.addChild(calcs)
		selectModel.setActiveZone(calcs)
		def outlets = []
		for ( int i = 0; i < 10; i++){
			DLComponent o = cmodel.newComponent("graph_outlet")
			o.setPropertyValue("name", "outlet$i")
			outlets += o
			calcs.addChild(o)
		}

		selectModel.setSelection(outlets)
		assert selectModel.getBaseSelection().size() == 10
		assert selectModel.getExpandedSelection().size() == 10

		assert currentEvent.getExpandedAddition() == outlets
		assert currentEvent.getExpandedRemoval() == []
		assert new ArrayList(currentEvent.getAddedSet()).sort() == outlets.sort()
		assert new ArrayList(currentEvent.getRemovedSet()).sort() == []


		selectModel.setSelection(calcs)
		assert selectModel.getBaseSelection().size() == 1
		assert selectModel.getExpandedSelection().size() == 10

		assert currentEvent.getExpandedAddition().sort() == outlets.sort()
		assert currentEvent.getExpandedRemoval().sort() == outlets.sort()
		assert new ArrayList(currentEvent.getAddedSet()).sort() == [calcs]
		assert new ArrayList(currentEvent.getRemovedSet()).sort() == outlets.sort()

		DLComponent room = cmodel.newComponent("room")
		drawing.addChild(room)
		DLComponent rack1 = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode2")
		rack1.setPropertyValue("name", "rack1")
		room.addChild(rack1)
		DLComponent rack2 = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode2")
		rack2.setPropertyValue("name", "rack2")
		room.addChild(rack2)

		//note: in the future, we should improve selectModel to not need these next two lines
		selectModel.setSelection([])

		assert currentEvent.getExpandedAddition() == []
		assert currentEvent.getExpandedRemoval().sort() == outlets.sort()
		assert new ArrayList(currentEvent.getAddedSet()).sort() == []
		assert new ArrayList(currentEvent.getRemovedSet()).sort() == [calcs]

		selectModel.setSelection(outlets)

		assert currentEvent.getExpandedAddition().sort() == outlets.sort()
		assert currentEvent.getExpandedRemoval() == []
		assert new ArrayList(currentEvent.getAddedSet()).sort() == outlets.sort()
		assert new ArrayList(currentEvent.getRemovedSet()) == []


		//println selectModel.getExpandedSelection()

		def allThings = []
		allThings.addAll(room.getChildComponents())
		allThings.addAll(calcs.getChildComponents())
		selectModel.toggleSelect(allThings)
		//println allThings
		//println selectModel.getExpandedSelection()
		assert selectModel.getExpandedSelection().size() == 2
		assert selectModel.getExpandedSelection().sort{it.getName()} == [rack1, rack2]

		assert currentEvent.getExpandedAddition() == room.getChildComponents()
		assert currentEvent.getExpandedRemoval().sort() == outlets.sort()
		assert new ArrayList(currentEvent.getAddedSet()).sort() == room.getChildComponents().sort()
		assert new ArrayList(currentEvent.getRemovedSet()).sort() == outlets.sort()
	}

	@Test
	void activeTest(){
		DLComponent drawing = cmodel.newComponent("drawing")
		selectModel.setActiveDrawing(drawing)
		DLComponent calcs = cmodel.newComponent("logicalzone")
		drawing.addChild(calcs)
		selectModel.setActiveZone(calcs)
		List<DLComponent>  users = []
		for ( int i = 0; i < 10; i++){
			def o = cmodel.newComponent("userinput")
			users += o
			calcs.addChild(o)
		}

		DLComponent net = cmodel.newComponent("network")
		net.setPropertyValue("name", "THE NETWORK")
        drawing.addChild(net)

		selectModel.setActiveNetwork(net)
		selectModel.setActiveZone(calcs)
		assert selectModel.getActiveZone() == calcs
		assert selectModel.getActiveNetwork() == net

		selectModel.setActiveZone(null)
		selectModel.setActiveNetwork(null)
		assert selectModel.getActiveZone() != calcs
		assert selectModel.getActiveZone() == null

		assert selectModel.getActiveNetwork() != net
		assert selectModel.getActiveNetwork() == null

	}

	@Test (expected=Exception.class)
	void badActiveDrawing(){
		DLComponent rack1 = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode2")
		selectModel.setActiveDrawing(rack1)
	}

	@Test (expected=Exception.class)
	void badActiveZone(){
		DLComponent rack1 = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode2")
		selectModel.setActiveZone(rack1)
	}

	@Test (expected=Exception.class)
	void badActiveNet(){
		DLComponent rack1 = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode2")
		selectModel.setActiveNetwork(rack1)
	}



	@Test
	void keepBaseSelectionAround(){
		DLComponent drawing = cmodel.newComponent("drawing")
		selectModel.setActiveDrawing(drawing)
		DLComponent calcs = cmodel.newComponent("logicalzone")
		drawing.addChild(calcs)
		selectModel.setActiveZone(calcs)
		List<DLComponent>  outlets = []
		for ( int i = 0; i < 10; i++){
			def o = cmodel.newComponent("graph_outlet")
			outlets += o
			calcs.addChild(o)
		}

		selectModel.addToSelection(outlets[0])
		selectModel.addToSelection(outlets[1])
		selectModel.addToSelection(outlets[2])

		assert selectModel.getBaseSelection().size() == 3
		List<DLComponent> base = selectModel.getBaseSelection()
		assert base.size() == 3

		selectModel.clearSelection()
		assert selectModel.getBaseSelection().size() == 0
		assert base.size() == 3
		assert selectModel.getBaseSelection() != base

		selectModel.addToSelection(outlets[5])
		assert selectModel.getBaseSelection().size() == 1
		assert base.size() == 3

		selectModel.setSelection(outlets)
		assert selectModel.getBaseSelection().size() == 10
		assert base.size() == 3

		selectModel.setSelection(base)
		assert selectModel.getBaseSelection().size() == 3
		assert selectModel.getBaseSelection() == base
		assert base[0] == selectModel.getBaseSelection()[0]
		assert base[1] == selectModel.getBaseSelection()[1]
		assert base[2] == selectModel.getBaseSelection()[2]
	}



	@Test
	void keepExpandedSelectionAround(){
		DLComponent drawing = cmodel.newComponent("drawing")
		selectModel.setActiveDrawing(drawing)

		DLComponent calcs = cmodel.newComponent("logicalzone")
		drawing.addChild(calcs)
		selectModel.setActiveZone(calcs)

		List<DLComponent>  outlets = []
		for ( int i = 0; i < 10; i++){
			def o = cmodel.newComponent("graph_outlet")
			outlets += o
			calcs.addChild(o)
		}

		DLComponent room = cmodel.newComponent("room")
		drawing.addChild(room)
		DLComponent rack1 = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode2")
		room.addChild(rack1)
		DLComponent rack2 = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode2")
		room.addChild(rack2)

		// Rooms don't select children like Zones did, so we changed this in Aireo to use a Planning Group.
		DLComponent group = TestUtils.newComponentWithParents( cmodel, ComponentType.PLANNING_GROUP, drawing )
		room.getChildComponents().each { group.addChild(it) }

		selectModel.addToSelection(outlets[0])
		selectModel.addToSelection(outlets[1])
		selectModel.addToSelection(outlets[2])
		selectModel.addToSelection(group)

		assert selectModel.getBaseSelection().size() == 4
		assert selectModel.getExpandedSelection().size() == 5
		List<DLComponent> expanded = selectModel.getExpandedSelection()
		assert expanded.size() == 5

		selectModel.clearSelection()
		assert selectModel.getExpandedSelection().size() == 0
		assert expanded.size() == 5
		assert selectModel.getExpandedSelection() != expanded

		selectModel.addToSelection(outlets[5])
		assert selectModel.getExpandedSelection().size() == 1
		assert expanded.size() == 5

		selectModel.setSelection(outlets)
		selectModel.addToSelection(group)
		assert selectModel.getExpandedSelection().size() == 12
		assert expanded.size() == 5

		selectModel.setSelection(expanded)
		assert selectModel.getBaseSelection().size() == 5
		assert selectModel.getExpandedSelection().size() == 5
		assert selectModel.getBaseSelection() == expanded
		assert selectModel.getExpandedSelection() == expanded
		assert expanded[0] == selectModel.getBaseSelection()[0]
		assert expanded[1] == selectModel.getBaseSelection()[1]
		assert expanded[2] == selectModel.getBaseSelection()[2]
		assert expanded[4] == selectModel.getBaseSelection()[4]
		assert expanded[5] == selectModel.getBaseSelection()[5]
	}

	/**
	 * Make sure the same thing can't get into the selected list twice
	 */
	@Test
	void dontSelectTwice(){
		def calc = cmodel.newComponent("logicalzone")
		selectModel.getActiveDrawing().addChild(calc)

		def sum = cmodel.newComponent("sum_operation")
		def user = cmodel.newComponent("userinput")
		user.setPropertyValue("name", "Ultimate Answer")
		user.setPropertyValue("value", "42")

		calc.addChild(sum)
		calc.addChild(user)

		selectModel.clearSelection()
		selectModel.addToSelection(sum)
		selectModel.addToSelection(user)
		selectModel.addToSelection(sum)
		assert selectModel.selectedObjects.size() == 2

		selectModel.clearSelection()
		def comps = []
		comps += sum
		comps += user
		comps += sum
		
		selectModel.setSelection(comps)
		assert selectModel.selectedObjects.size() == 2

		selectModel.clearSelection()
		selectModel.addToSelection(comps)
		assert selectModel.selectedObjects.size() == 2

		selectModel.clearSelection()
		selectModel.setSelection(comps)
		selectModel.addToSelection(comps)
		assert selectModel.selectedObjects.size() == 2
	}


	@Test
	void changeDrawingClears(){
		def drawing = selectModel.getActiveDrawing()
		def lz = cmodel.newComponent("logicalzone")
		def sum = cmodel.newComponent("sum_operation")
		def user = cmodel.newComponent("userinput")
		user.setPropertyValue("name", "Ultimate Answer")
		user.setPropertyValue("value", "42")
		drawing.addChild(lz)
		lz.addChild(sum)
		lz.addChild(user)

		def drawing2 = cmodel.newComponent("drawing")

		selectModel.setActiveDrawing(drawing)
		selectModel.clearSelection()
		selectModel.addToSelection(sum)
		selectModel.addToSelection(user)
		selectModel.setActiveDrawing(drawing2)
		assert selectModel.getSelectedObjects().size() == 0
	}

	@Test
	void noEventsOnClear(){
		def fls = new FakeListenerStack()
		selectModel.addSelectionChangeListener(fls)
		def drawing = selectModel.getActiveDrawing()
		def lz = cmodel.newComponent("logicalzone")
		def sum = cmodel.newComponent("sum_operation")
		def user = cmodel.newComponent("userinput")
		user.setPropertyValue("name", "Ultimate Answer")
		user.setPropertyValue("value", "42")
		drawing.addChild(lz)
		lz.addChild(sum)
		lz.addChild(user)
		def drawing2 = cmodel.newComponent("drawing")

		fls.clearStack()

		cmodel.clear()

		assert fls.stack().size() == 0
	}

	@Test
	void noEventsOnLoad(){
		def fls = new FakeListenerStack()
		selectModel.addSelectionChangeListener(fls)
		TestUtils.loadProjectFile( TestUtils.CURRENT_VERSION_LINT, CentralCatalogue.getUndoBuffer(), omodel, cmodel )
		assert fls.stack().size() == 0
	}

	@Test
	void dontFireAnyEventsWhenClearingAnEmptySelection(){
		def spc = new SpecificChannelLogger(LogLevel.TRACE, "default")
		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(spc)

		selectModel.setSelection([])
		assert spc.stack.size() == 1 //the message that nofify listeners will not fire an empty event
		spc.stack.clear()

		def fls = new FakeListenerStack()
		selectModel.addSelectionChangeListener(fls)

		selectModel.clearSelection()
		assert fls.stack().size() == 0
		assert spc.stack.size() == 0

	}

}
