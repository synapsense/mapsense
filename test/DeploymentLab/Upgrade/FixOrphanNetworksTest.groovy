package DeploymentLab.Upgrade

import DeploymentLab.CentralCatalogue
import org.junit.Test
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ObjectModel
import org.junit.BeforeClass
import DeploymentLab.TestUtils
import org.junit.Before
import javax.swing.JFrame
import org.junit.After
import DeploymentLab.ModelObserver
import DeploymentLab.Tree.ConfigurationReplacerTreeModel
import DeploymentLab.Tree.ConfigurationReplacerTree
import DeploymentLab.ComponentLibrary
import DeploymentLab.Tools.ConfigurationReplacer
import DeploymentLab.ProjectSettings
import DeploymentLab.DLProject

/**
 * @author Gabriel Helman
 * @since Date: 10/12/12
 *        Time: 10:40 AM
 */
public class FixOrphanNetworksTest {


	static ComponentModel cmodel = null
	static ObjectModel omodel = null
	static UpgradeController upgradeController

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()

		// a whole bunch of scaffolding so we can get a config replacer. oh gawd...
		def treeModels = []
		def cmtm = new ConfigurationReplacerTreeModel(cmodel, "FakeTree")
		treeModels.add(cmtm)
		ConfigurationReplacerTree tree = new ConfigurationReplacerTree(cmtm)
		def clib = ComponentLibrary.INSTANCE
		clib.init(cmodel,null,treeModels)
		ConfigurationReplacer cr = new ConfigurationReplacer( CentralCatalogue.getSelectModel(), cmodel, tree,
		                                                      CentralCatalogue.getUndoBuffer(), new JFrame() )

		ProjectSettings ps = ProjectSettings.getInstance();
		ps.create()
		upgradeController = new UpgradeController( new DLProject( CentralCatalogue.getUndoBuffer(), cmodel, omodel, cr, false ) )
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
	}

	@Test
	public void testRequirementsCheck() throws Exception {
		def mo = new ModelObserver(cmodel)
		def font = new FixOrphanNetworks()
		assert font.requirementsCheck(upgradeController,1.0)
		assert mo.getEventStack().size() == 0
	}

	@Test
	public void testPreUpdateYes() throws Exception {
		//set up WITH the problem
		def drawing = cmodel.newComponent("drawing")
		def net1 = cmodel.newComponent("network")
		def net2 = cmodel.newComponent("network")
		def net3 = cmodel.newComponent("network")
		drawing.addChild(net1)
		drawing.addChild(net2)

		def font = new FixOrphanNetworks()
		def mo = new ModelObserver(cmodel)
		assert !font.preUpdate(upgradeController).isEmpty()
		assert mo.getEventStack().size() == 1
	}

	@Test
	public void testPreUpdateNo() throws Exception {
		//set up WITHOUT the problem
		def drawing = cmodel.newComponent("drawing")
		def net1 = cmodel.newComponent("network")
		def net2 = cmodel.newComponent("network")
		//def net3 = cmodel.newComponent("network")
		drawing.addChild(net1)
		drawing.addChild(net2)

		def font = new FixOrphanNetworks()
		def mo = new ModelObserver(cmodel)
		assert font.preUpdate(upgradeController).isEmpty()
		assert mo.getEventStack().size() == 0
	}

	@Test
	public void fromController() throws Exception {
		//set up WITH the problem
		def drawing = cmodel.newComponent("drawing")
		def net1 = cmodel.newComponent("network")
		def net2 = cmodel.newComponent("network")
		def net3 = cmodel.newComponent("network")
		drawing.addChild(net1)
		drawing.addChild(net2)

		def font = new FixOrphanNetworks()
		def mo = new ModelObserver(cmodel)
		//assert font.preUpdate(upgradeController)
		upgradeController.performUpgrade(1.0)
		assert mo.getEventStack().contains("componentRemoved $net3")
	}

	@Test
	public void testPostUpdate() throws Exception {
		def mo = new ModelObserver(cmodel)
		def font = new FixOrphanNetworks()
		assert font.postUpdate(upgradeController) == null
		assert mo.getEventStack().size() == 0
	}
}
