package DeploymentLab.Transfer

import org.junit.Test
import DeploymentLab.CentralCatalogue

/**
 * Exercise the DLCopyVessel.
 * @author Gabriel Helman
 * @since Jupiter
 * Date: 3/19/12
 * Time: 1:50 PM
 */
class DLCopyVesselTest extends GroovyTestCase {
	
	@Test
	void testGetPayload() {
		assert new DLCopyVessel().getPayload().equals("")
		assert new DLCopyVessel("xml").getPayload().equals("xml")
		assert new DLCopyVessel("xml", 13).getPayload().equals("xml")		
	}

	@Test
	void testSetPayload() {
		def cv = new DLCopyVessel()
		assert cv.getPayload().equals("")
		cv.setPayload("some random xml")
		assert cv.getPayload().equals("some random xml")
	}

	@Test
	void testGetVersionNumber() {
		assert new DLCopyVessel().getVersionNumber() == Double.parseDouble(CentralCatalogue.getApp("model.version"))
		assert new DLCopyVessel("xml").getVersionNumber() == Double.parseDouble(CentralCatalogue.getApp("model.version"))
		assert new DLCopyVessel("xml", 13).getVersionNumber() == 13.0
	}

	@Test
	void testSetVersionNumber() {
		def cv = new DLCopyVessel()
		assert cv.getVersionNumber() == Double.parseDouble(CentralCatalogue.getApp("model.version"))
		cv.setVersionNumber(42)
		assert cv.getVersionNumber() == 42.0
	}

	@Test
	void testToString() {
		String PAYLOAD = "some random payload"
		def cv = new DLCopyVessel(PAYLOAD)
		String goal = "DLCopyVessel{payload='$PAYLOAD', versionNumber=${CentralCatalogue.getApp("model.version")}}"
		assert cv.toString().equals(goal)
	}
}
