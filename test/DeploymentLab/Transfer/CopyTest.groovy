package DeploymentLab.Transfer

import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentType
import DeploymentLab.Model.ObjectModel
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.Tools.NodeAdder
import DeploymentLab.Tools.NodeOperator
import DeploymentLab.Tools.ToolManager
import groovy.mock.interceptor.StubFor
import javax.swing.JFrame
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import DeploymentLab.*

/**
 * Test the copy-paste via clipboard
 * @author Gabriel Helman
 * @since Jupiter
 * Date: 2/23/12
 * Time: 3:28 PM
 */
class CopyTest {

	ComponentModel cmodel = null
	ObjectModel omodel = null
	SelectModel selectModel = null

	UndoBuffer undoBuffer
	NodeOperator nodeOperator
	DeploymentPanel deploymentPanel
	def progress = new StubFor(ProgressManager)

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()
		undoBuffer = CentralCatalogue.getUndoBuffer()

		//groovy stubs to fake out the progress manager
		//using .ignore since we're not testing calls to the PM
		progress.ignore.doTask { c -> c.call() }
		progress.ignore.bump { }
		progress.ignore.setMaxProgress { }

		ToolManager tm = new ToolManager(null)
		DisplayProperties displayProperties = new DisplayProperties( selectModel )
		deploymentPanel = new DeploymentPanel(selectModel, undoBuffer, cmodel, tm, displayProperties)
		CentralCatalogue.setDeploymentPanel( deploymentPanel )

		new DLProject( undoBuffer, cmodel, omodel, null, true )

		tm.registerTool('adder', new NodeAdder(selectModel, cmodel, undoBuffer, null, UIDisplay.INSTANCE, null, null, CentralCatalogue.INSTANCE.getUIStrings(), deploymentPanel), false, true)

		//NodeOperator(SelectModel sm, UndoBuffer ub, ComponentModel cm, DeploymentPanel dp){
		nodeOperator = new NodeOperator(selectModel, undoBuffer, cmodel, deploymentPanel)
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
		selectModel = null
	}

	/**
	 * Make one of every component type.
	 * Copy it.
	 * Then paste it.
	 */
	@Test
	void copyEverything() {
		progress.use {
			def drawing = cmodel.getComponentsByType("drawing").first()
			def room = cmodel.newComponent("room")
			def calcg = cmodel.newComponent("logicalzone")
			def net = cmodel.newComponent("network")
			def mod = cmodel.newComponent("modbus-tcp-network")
			def snmp1 = cmodel.newComponent("snmp-v1-agent")
			def snmp2 = cmodel.newComponent("snmp-v2c-agent")
			def snmp3 = cmodel.newComponent("snmp-v3-agent")
			def group = cmodel.newComponent( ComponentType.PLANNING_GROUP )
			def bacnet = cmodel.newComponent("bacnetnetwork")
			//def lon = cmodel.newComponent("lonnetwork")

			drawing.addChild(room)
			drawing.addChild(calcg)
			drawing.addChild(net)
			drawing.addChild(mod)
			drawing.addChild(snmp1)
			drawing.addChild(snmp2)
			drawing.addChild(snmp3)
			drawing.addChild(group)
			drawing.addChild(bacnet)
			//drawing.addChild(lon)

			selectModel.setActiveDrawing(drawing)
			selectModel.setActiveZone(group)
			selectModel.setActiveNetwork(net)

			Set<String> componentTypes = cmodel.listComponentTypes()
			for (String t: componentTypes.sort()) {

				//these need some fine tuning
				if (t.contains("lon")) {
					continue
				}

				//we currently don't suppport copying of non-placeables
				if(!cmodel.listComponentRoles(t).contains("placeable")){
					continue
				}

				if(cmodel.listComponentRoles(t).contains("dynamicchild")){
					continue
				}
				
				def c = TestUtils.newComponentWithParents( cmodel, t, drawing )

				println "testing copy&paste $t"

				if (t.equals("contained_poly")) {
					//set up some points
					c.setPropertyValue("points", "254.08295,283.13364;343.447,384.8848;583.2258,251.28111;442.5438,66.35944;385.03226,224.73732;306.2857,168.1106")
				}

				// Make sure we have the correct active parents
				def groups = cmodel.listGroupings(t)
				if (groups.size() > 0) {
					def hostG = cmodel.getComponentsByType(groups.first())
					selectModel.setActiveZone(hostG.first())
				}

				def datasources = cmodel.listDatasources(t)
				if (datasources.size() > 0) {
					def hostNet = cmodel.getComponentsByType(datasources.first())
					selectModel.setActiveNetwork(hostNet.first())
				}

				selectModel.setSelection(c)

				String payload = SelectionTransferForge.copyToXML(selectModel);
				def vessel = new DLCopyVessel(payload)
				//println payload
				boolean result = SelectionTransferForge.pasteFromXML(vessel.getPayload(), cmodel, selectModel, undoBuffer, nodeOperator, deploymentPanel);
				//assert result

				if (c.hasRole("dynamicchild") || c.hasRole("staticchild")) {
					if(result){println "bad paste for $c!"}
					assert !result
				} else {
					if(!result){println "bad paste for $c!"}
					assert result
				}

			}
		}

	}
}
