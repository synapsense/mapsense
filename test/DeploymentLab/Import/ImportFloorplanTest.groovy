package DeploymentLab.Import

import DeploymentLab.DLProject
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ObjectModel
import DeploymentLab.SelectModel
import DeploymentLab.TestUtils
import DeploymentLab.Tree.ConfigurationReplacerTree
import DeploymentLab.Tree.ConfigurationReplacerTreeModel
import DeploymentLab.UndoBuffer

import org.junit.*
import DeploymentLab.Tools.ConfigurationReplacer
import DeploymentLab.CentralCatalogue

/**
 * Created by IntelliJ IDEA.
 * User: HLim
 * Date: 8/2/12
 * Time: 5:25 PM
 * To change this template use File | Settings | File Templates.
 */

public class ImportProjectTest {
	private ObjectModel objectModel
	private ComponentModel componentModel
	private UndoBuffer undoBuffer
	private SelectModel selectModel
	private def configurationReplacerTree
	private ImportProjectController ifc

	/**
	 * One-time setup called before all tests run.
	 */
	@BeforeClass()
	static void classSetup() {
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.WARN)
		//ChannelManager cm = ChannelManager.getInstance()
		//cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
	}

	/**
	 * Setup called before each test.
	 */
	@Before
	void setup() {
		(objectModel, componentModel) = TestUtils.initModels()

		selectModel = CentralCatalogue.getSelectModel()
		undoBuffer = CentralCatalogue.getUndoBuffer()

		def treeModels = []
		def cmtm = new ConfigurationReplacerTreeModel(componentModel, "FakeTree")
		treeModels.add(cmtm)
		configurationReplacerTree = new ConfigurationReplacerTree(cmtm)
		ConfigurationReplacer cr = new ConfigurationReplacer(selectModel, componentModel, configurationReplacerTree, undoBuffer, CentralCatalogue.getParentFrame())

		DLProject project = TestUtils.loadProjectFile('build/testFiles/import1.dlz', objectModel, componentModel, cr )

		ifc = new ImportProjectController( project, selectModel, configurationReplacerTree)

	}

	/**
	 * Cleanup called after each test.
	 */
	@After
	void tearDown() {
		ifc.close()
		objectModel = null
		componentModel = null
		undoBuffer = null
		selectModel = null
	}

	/**
	 * One-time cleanup called after all tests run.
	 */
	@AfterClass()
	static void classTearDown() {

	}

	private void importProject(String projectFile) {
		int objectCnt1 = objectModel.getObjectCount()

		File importedFile = new File(projectFile)
		ifc.doImportProject(importedFile, true)

		int objectCnt2 = ifc.iObjectModel.getObjectCount()
		assert objectCnt2 > 0
		int totalObjectCnt = objectModel.getObjectCount()
		println "imported object count:" + objectCnt2
		println "total object count:" + totalObjectCnt
		assert totalObjectCnt == (objectCnt1 + objectCnt2)
	}


	@Test
	void testImportProject() {
		println "Test basic import drawing"
		importProject('build/testFiles/import2.dlz')
		assert !ifc.esKeyCollision
	}

	/*
		@Test
		void testImportProjectCaseOldVersion(){
			println "old version project test"
			openProjectFile('build/testFiles/import1.dlz')
			importProject('build/testFiles/venus_5_3.dl')

		} */

	@Test
	void testImportProjectCaseDeletedObjects() {
		println "Test to import drawing with deleted and exported objects"
		importProject('build/testFiles/import_deleted.dlz')
		assert !ifc.esKeyCollision

	}

	@Test
	void testImportProjectCaseESCollosion() {
		println "Test to import drawing with duplicate ES key"
		importProject('build/testFiles/import_es_collision.dlz')
		assert ifc.esKeyCollision
		//check that we scrubbed out the keys
		def objectsWithEskey = ifc.iObjectModel.getObjects().findAll {it -> it.getKey() != ''}
		int countObjectsWithEskey = objectsWithEskey.size()
		assert countObjectsWithEskey == 0
	}

	@Test
	void testImportProjectCaseMultiDrawing() {
		println "test to import multiple drawings"
		importProject('build/testFiles/import_multydc.dlz')
		assert !ifc.esKeyCollision
	}

	@Test
	void testImportProjectCaseProxies() {
		println "test to import drawing with proxies"
		importProject('build/testFiles/proxies.dlz')
		// For gits n shiggles, import it a second time to see if collisions are handled properly.
		importProject('build/testFiles/proxies.dlz')
	}
}