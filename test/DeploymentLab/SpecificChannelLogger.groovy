package DeploymentLab

import DeploymentLab.channellogger.ChannelListener
import DeploymentLab.channellogger.LogData
import DeploymentLab.channellogger.LogLevel

/**
 * Test utility logger to check if a given channel gets a message at all (and optionally, at a given level).
 * @author Gabriel Helman
 * @since Juptier 2
 * Date: 8/7/12
 * Time: 11:50 AM
 */
class SpecificChannelLogger implements ChannelListener {

	private LogLevel levelICareAbout
	private String channelICareAbout

	protected Deque<String> msgStack = new ArrayDeque<String>()


	public SpecificChannelLogger(LogLevel l, String channel) {
		this.levelICareAbout = l
		this.channelICareAbout = channel
	}

	@Override
	void messageReceived(LogData l) {
		if(l.getChannel().equals(channelICareAbout)){
			if(levelICareAbout.ordinal() >= l.level.ordinal()){
				msgStack.push(l.message)
			}
		}
	}

	public Deque<String> getStack(){
		return msgStack
	}
}
