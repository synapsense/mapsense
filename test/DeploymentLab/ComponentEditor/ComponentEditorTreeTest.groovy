package DeploymentLab.ComponentEditor

import DeploymentLab.Model.ComponentModel;
import DeploymentLab.Model.ObjectModel;
import DeploymentLab.TestUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import DeploymentLab.Model.DLComponent

/**
 * @author Gabriel Helman
 * @since Jupiter 
 * Date: 2/22/12
 * Time: 3:39 PM
 */
public class ComponentEditorTreeTest {
	ComponentModel cmodel = null
	ObjectModel omodel = null
	ComponentEditorTree editorTree
	DLComponent rack

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()

		rack = cmodel.newComponent("rack-control-toprearexhaust-nsf-rt-thermanode2")
		editorTree = new ComponentEditorTree(rack)
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
	}

	
	@Test
	public void testGetCellEditor() throws Exception {
		assert editorTree.getCellEditor().class.equals( ComponentTreeCellEditor.class )
	}

	/*
	@Test
	public void testIsPathEditable() throws Exception {

	}

	@Test
	public void testComponentPropertyChanged() throws Exception {

	}
	*/

	@Test
	public void testExpandAll() throws Exception {
		editorTree.expandAll(null)
		editorTree.expandAll(editorTree.getRoot())
	}


	@Test
	public void testUseOldValue() throws Exception {
		assert !editorTree.isEditingOldValue()
		editorTree.useOldValue(true)
		assert editorTree.isEditingOldValue()
	}

	@Test
	public void testIsEditingOldValue() throws Exception {
		assert !editorTree.isEditingOldValue()
	}

	@Test
	public void testGetRoot() throws Exception {
		assert editorTree.getRoot().getComponent() == rack
	}

	@Test
	public void testWithTags(){
		editorTree = new ComponentEditorTree(cmodel.newComponent("network"))
	}

	@Test
	public void testWithRules(){
		editorTree = new ComponentEditorTree(cmodel.newComponent("average_operation"))
	}


}
