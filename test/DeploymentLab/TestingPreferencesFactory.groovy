package DeploymentLab

import java.util.prefs.AbstractPreferences
import java.util.prefs.BackingStoreException
import java.util.prefs.Preferences
import java.util.prefs.PreferencesFactory

/**
 * Provides a custom user-preferences implementation for use in unit tests.
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 8/16/12
 * Time: 11:43 AM
 */
class TestingPreferencesFactory implements PreferencesFactory {

	@Override
	Preferences systemRoot() {
		return new EmptyPreferences()
	}

	@Override
	Preferences userRoot() {
		return new EmptyPreferences()
	}
}


public class EmptyPreferences extends AbstractPreferences {

	public EmptyPreferences() {
		super(null, "");
	}

	protected void putSpi(String key, String value) {

	}

	protected String getSpi(String key) {
		return null;
	}

	protected void removeSpi(String key) {

	}

	protected void removeNodeSpi() throws BackingStoreException {

	}

	protected String[] keysSpi() throws BackingStoreException {
		return new String[0];
	}

	protected String[] childrenNamesSpi() throws BackingStoreException {
		return new String[0];
	}

	protected AbstractPreferences childSpi(String name) {
		return null;
	}

	protected void syncSpi() throws BackingStoreException {

	}

	protected void flushSpi() throws BackingStoreException {

	}
}