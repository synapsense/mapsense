package DeploymentLab.Palette

import DeploymentLab.ComponentLibrary
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ObjectModel
import DeploymentLab.ProblemLogger
import DeploymentLab.SelectModel
import DeploymentLab.TestUtils
import DeploymentLab.Tools.CurrentLocation
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.channellogger.LogLevel
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.BeforeClass
import javax.swing.JComponent
import javax.swing.JPanel
import javax.xml.XMLConstants
import java.awt.event.MouseEvent

import DeploymentLab.Tools.ToolManager
import DeploymentLab.DisplayProperties
import javax.swing.ButtonGroup
import DeploymentLab.Tools.NodeAdder
import DeploymentLab.Tools.ControlInputAdder
import DeploymentLab.UndoBuffer
import DeploymentLab.CentralCatalogue
import DeploymentLab.UIDisplay

import javax.swing.JFrame

import DeploymentLab.Tools.DynamicChildAdder
import DeploymentLab.Tools.ArbitraryShaper
import DeploymentLab.Tools.StaticChildrenNodeAdder
import DeploymentLab.Tools.PanZoomer
import DeploymentLab.Tools.NodeHover
import DeploymentLab.Tools.KeyboardCommands
import DeploymentLab.Tools.FasterNodeNamer
import DeploymentLab.Tools.ConfigurationReplacer
import DeploymentLab.Tree.ConfigurationReplacerTreeModel
import DeploymentLab.Tree.ConfigurationReplacerTree

import javax.swing.JButton

/**
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/30/12
 * Time: 12:44 PM
 */
public class PaletteControllerTest {
	static ObjectModel omodel
	static ComponentModel cmodel
	static SelectModel selectModel
	static PaletteController paletteController
	static ToolManager toolManager


	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		//TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.WARN)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()

		paletteController = new PaletteController(cmodel, selectModel)
		def paletteTreeModels = []
		paletteTreeModels.add(paletteController)
		selectModel.addSelectionChangeListener(paletteController)

		ComponentLibrary.INSTANCE.init(cmodel, paletteTreeModels, [])
		ComponentLibrary.INSTANCE.loadDefaultComponents()

		def allFiles = []
		allFiles.addAll( new File( "componentlibs/ss/" ).listFiles() )
		allFiles.addAll( new File( "componentlibs/ss/internal/" ).listFiles() )
		allFiles.addAll( new File( "componentlibs/ss/custom/" ).listFiles() )

		allFiles.each{
			//println "loading backcompat ${it.getAbsolutePath()}"
			if( it.absolutePath.endsWith('.dcl') ) {
				//def compXml = new XmlSlurper().parse( new File( it.absolutePath ) )
				XmlSlurper slurper = new XmlSlurper( false, false, true )
				slurper.setEntityResolver( ComponentLibrary.CATALOG_RESOLVER )
				slurper.setProperty( XMLConstants.ACCESS_EXTERNAL_DTD, "all" )
				def compXml = slurper.parse( new File( it.absolutePath ) )
				//componentModel.loadComponent( compXml )
				ComponentLibrary.INSTANCE.loadCustomComponentLibrary(it, compXml)
			}
		}

		//stub in the tool manager and the tools so we can test that the palette activates the right ones for a given component
		//this also includes the other "default" tools we get along with various adders

		toolManager = new ToolManager(new ButtonGroup())

		UndoBuffer undoBuffer = CentralCatalogue.getUndoBuffer()
		def viewOptionConfigTree = null
		def deploymentLabFrame = new JFrame()
		def viewOptionDialog = null
		def uiDisplay = UIDisplay.INSTANCE
		def uiStrings = CentralCatalogue.INSTANCE.getUIStrings()
		def deploymentPanel = null
		def displayProperties = new DisplayProperties( selectModel )

		toolManager.registerTool('adder', new NodeAdder(selectModel, cmodel, undoBuffer, viewOptionConfigTree, uiDisplay, deploymentLabFrame, viewOptionDialog, uiStrings, deploymentPanel), false, true)
		toolManager.registerTool('controlinputadder', new ControlInputAdder(selectModel, cmodel, undoBuffer, viewOptionConfigTree, uiDisplay, deploymentPanel, displayProperties, deploymentLabFrame, viewOptionDialog), false, true)
		toolManager.registerTool('dynamicchildadder', new DynamicChildAdder(selectModel, cmodel, undoBuffer, viewOptionConfigTree, uiDisplay, deploymentPanel, displayProperties, deploymentLabFrame, viewOptionDialog, uiStrings), false, true)
		toolManager.registerTool('arbitraryshaper', new ArbitraryShaper(selectModel, cmodel, undoBuffer, viewOptionConfigTree, uiDisplay, deploymentLabFrame, viewOptionDialog, uiStrings, deploymentPanel, toolManager), false, false)
		toolManager.registerTool('staticchildrennodeadder', new StaticChildrenNodeAdder(selectModel, cmodel, undoBuffer, viewOptionConfigTree, uiDisplay, deploymentLabFrame, viewOptionDialog, uiStrings, deploymentPanel), false, true)
		//toolManager.setActiveTools(['hover', 'panzoomer', 'adder', 'keycommands', 'popupmenu', 'fasternamer'])
		toolManager.registerTool('panzoomer', new PanZoomer(deploymentPanel))
		def hovermatic = new NodeHover(deploymentPanel, displayProperties)
		cmodel.addModelChangeListener(hovermatic)
		toolManager.registerTool('hover', hovermatic)
		toolManager.registerTool('currentlocation', new CurrentLocation(deploymentPanel, selectModel), true)

		def cmtm = new ConfigurationReplacerTreeModel(cmodel, "FakeTree")
		//treeModels.add(cmtm)
		def tree = new ConfigurationReplacerTree(cmtm)
		def configurationReplacer = new ConfigurationReplacer(selectModel, cmodel, tree, undoBuffer, new JFrame())

		def fakeDLW = new Expando()
		fakeDLW.setProperty("selectModel", selectModel)
		fakeDLW.setProperty("toolManager", toolManager)
		fakeDLW.setProperty("componentModel", cmodel)

		def popupMenu = new DeploymentLab.Tools.PopupMenu(fakeDLW, undoBuffer, uiDisplay, configurationReplacer, deploymentPanel)
		FasterNodeNamer fasterNamer = new FasterNodeNamer(deploymentPanel, uiDisplay, undoBuffer)
		toolManager.registerTool('keycommands', new KeyboardCommands(cmodel, undoBuffer, selectModel, uiDisplay, deploymentPanel))
		toolManager.registerTool('popupmenu', popupMenu)
		toolManager.registerTool('fasternamer', fasterNamer )

		//set the default tools that to tool manager will return to with a call to activateDefaults
		//toolManager.setDefaultTools(['hover', 'rectselect', 'panzoomer', 'selector', 'mover', 'keycommands', 'popupmenu', 'fasternamer'], new JButton(), {paletteController.clearSelection()})
		toolManager.setDefaultTools(['hover', 'panzoomer', 'keycommands', 'popupmenu', 'fasternamer'], new JButton(), {paletteController.clearSelection()})
		toolManager.activateDefaults()

		paletteController.setToolManager(toolManager)


		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))
		//cm.addChannelListener(new ProblemLogger(LogLevel.WARN))
	}


	@After
	void tearDown() {
		cmodel = null
		omodel = null
		selectModel = null
		paletteController = null
		toolManager = null
		//have fun, garbage collector!
	}


	@Test
	void someBasicExercise(){
		def pal = paletteController.getPalette()

		assert pal instanceof JComponent
		assert pal instanceof JPanel

		//make sure the palette has everything it should (and nothing it shouldn't)
		def palTypes = new HashSet<String>( paletteController.allTypes.collect { it.getType()} )
		def cmTypes = new HashSet<String>(cmodel.listTypesWithRole("placeable").findAll { cmodel.getTypeAttribute(it,"grouppath").trim().size()>0 })
		def bonus = cmTypes.minus(palTypes)
		//println bonus
		assert bonus.size() == 0
		assert palTypes.equals(cmTypes)
	}


	@Test
	void makeSureTheTagsDialogInits(){
		def filterDialog = paletteController.getFilterDialog(null)
		assert filterDialog instanceof PaletteFilterDialog
		filterDialog.initDialog()
		assert filterDialog.dialogDelegate != null
		paletteController.cancelFilterChanges()
		assert filterDialog.dialogDelegate == null
	}

	@Test
	void dialogCheckboxes(){
		def filterDialog = paletteController.getFilterDialog(null)
		assert filterDialog instanceof PaletteFilterDialog
		filterDialog.initDialog()

		filterDialog.filterNone()
		paletteController.filterTags.values().each{ assert !it }
		assert paletteController.listModel.size() == 0

		filterDialog.filterAll()
		paletteController.filterTags.values().each{ assert it }
		assert paletteController.listModel.size() == paletteController.allTypes.size()
	}

	@Test
	void checkTheRenderer(){
		def list = paletteController.getPaletteList()
		list.setSelectedIndex(1)
		def val = list.getSelectedValue()
		def renderer = list.getCellRenderer().getListCellRendererComponent(list,val,1,true,true)
		assert renderer instanceof FancyPanel
	}

	@Test
	void clearSelection(){
		def list = paletteController.getPaletteList()
		list.setSelectedIndex(1)
		paletteController.clearSelection()
		assert list.getSelectedIndex() == -1
	}

	@Test
	void unload(){
		def cXml = cmodel.componentTypes["average_operation"]
		int first = paletteController.allTypes.size()
		paletteController.unloadConfiguration(cXml)
		int second = paletteController.allTypes.size()
		assert (first - 1) == second
	}

	@Test
	void confirmToolTips(){
		def list = paletteController.getPaletteList()
		list.setSize(200, list.getHeight()) //HACKXXXX - without this, the list has a width of 0 and no mouse events are contained within it

		list.setSelectedIndex(1)
		ComponentTypeDef val = list.getSelectedValue()
		def p = list.indexToLocation(1)
		//println "point is $p"
		def evt = new MouseEvent(list, MouseEvent.MOUSE_MOVED, new Date().time, 0, (int)p.x, (int)p.y, 0, false, MouseEvent.NOBUTTON);
		//println list.getToolTipText(evt)

		def renderer = list.getCellRenderer().getListCellRendererComponent(list,val,1,true,true)
		assert renderer instanceof FancyPanel
		//String renderTTT = renderer.getToolTipText()

		assert list.getToolTipText(evt) == renderer.getToolTipText()
	}


	@Test
	void selectATool(){
		def list = paletteController.getPaletteList()
		list.setSize(200, list.getHeight()) //HACKXXXX - without this, the list has a width of 0 and no mouse events are contained within it
		def p = list.indexToLocation(2)
		def evt = new MouseEvent(list, MouseEvent.MOUSE_CLICKED, new Date().time, 0, (int)p.x, (int)p.y, 1, false, MouseEvent.NOBUTTON);

		assert list.getSelectedIndex() == -1
		assert !toolManager.getActiveTools().contains("adder")

		//paletteController.mouseClicked(evt)
		list.processMouseEvent(evt)
		list.setSelectedIndex(2) //? shouldn't this be done for us via the mouse event?

		assert list.getSelectedIndex() == 2
		assert toolManager.getActiveTools().contains("adder")
	}


	void checkTypeAndTool(String typename, String toolname, boolean alsoCheckMultiDropMode = true){
		println "checkTypeAndTool $typename, $toolname, $alsoCheckMultiDropMode"

		assert cmodel.componentTypes.containsKey(typename)

		def cXml = cmodel.componentTypes[typename]
		def ctd = new ComponentTypeDef(cXml)

		assert !toolManager.getActiveTools().contains(toolname)
		assert paletteController.typeSelected(ctd,1)
		assert toolManager.getActiveTools().contains(toolname)
		assert toolManager.getSingleShotMode()

		if(alsoCheckMultiDropMode){
			toolManager.activateDefaults()
			assert !toolManager.getActiveTools().contains(toolname)
			assert !paletteController.typeSelected(ctd,2)
			assert toolManager.getActiveTools().contains(toolname)
			assert !toolManager.getSingleShotMode()
		}
	}

	@Test
	void checkforArbyTool(){
		//no multi-drop mode for contained polys.
		checkTypeAndTool("contained_poly", "arbitraryshaper", false)
	}

	@Test
	void checkforStaticChildTool(){
		checkTypeAndTool("dual-horizontal-row-thermanode2", "staticchildrennodeadder")
	}

	@Test
	void checkforRegularTool(){
		checkTypeAndTool("rack-control-rearexhaust-nsf-rt-thermanode2", "adder")
	}



}
