package DeploymentLab.Palette

import DeploymentLab.Model.ObjectModel
import DeploymentLab.Model.ComponentModel
import org.junit.BeforeClass
import org.junit.Before
import DeploymentLab.TestUtils
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.ProblemLogger
import DeploymentLab.channellogger.LogLevel
import org.junit.After
import org.junit.Test;

/**
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/30/12
 * Time: 1:22 PM
 */
public class ComponentTypeDefTest {

	static ObjectModel omodel
	static ComponentModel cmodel


	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		//TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.WARN)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))
		//cm.addChannelListener(new ProblemLogger(LogLevel.WARN))
	}


	@After
	void tearDown() {
		cmodel = null
		omodel = null
		//have fun, garbage collector!
	}

	@Test
	void construction(){
		def cXml = cmodel.componentTypes["average_operation"]
		assert cXml
		def ctd = new ComponentTypeDef(cXml)
		assert ctd
		assert ctd.toString()

		assert ctd.groupPath == cXml.@grouppath.text()
		assert ctd.filters == cXml.@filters.text()
		assert ctd.type == cXml.@type.text()
		assert ctd.roles == new HashSet<String>(cXml.@classes.text().split(",").toList())
		assert ctd.displayName == cXml.display.name.text()
		assert ctd.description == cXml.display.description.text()
		assert ctd.getCategory().equals("Calculations")


		cXml = cmodel.componentTypes["ct-constellation2"]
		assert cXml
		def ctd2 = new ComponentTypeDef(cXml)
		assert ctd2.getCategory().equals("Power")

	}

	@Test(expected=groovy.lang.MissingFieldException)
	void blank(){
		def blank = new ComponentTypeDef("")
	}

	@Test
	void hashing(){
		def set = new HashSet<ComponentTypeDef>()
		def cXml = cmodel.componentTypes["average_operation"]
		def ctd = new ComponentTypeDef(cXml)
		set.add(ctd)
		set.add(ctd)
		assert set.size() == 1

		def ctd2 = new ComponentTypeDef(cXml)
		set.add(ctd2)
		assert set.size() == 1
	}

	@Test
	void equals(){
		def cXml = cmodel.componentTypes["average_operation"]
		def ctd = new ComponentTypeDef(cXml)
		def ctd2 = new ComponentTypeDef(cXml)
		assert ctd.equals(ctd)
		assert !ctd.equals( new StringBuilder("something other than else!"))

		assert ctd.equals(ctd2)
		assert ctd == ctd2
		assert !ctd.is(ctd2)
	}


}
