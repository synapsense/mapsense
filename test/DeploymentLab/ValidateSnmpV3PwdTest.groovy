package DeploymentLab

import DeploymentLab.Import.ImportProjectController
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Tools.ConfigurationReplacer
import DeploymentLab.Tree.ConfigurationReplacerTree
import DeploymentLab.Tree.ConfigurationReplacerTreeModel
import org.junit.*

/**
 * Created by IntelliJ IDEA.
 * User: CCr
 * Date: 12/30/15
 * Time: 10:08 AM
 */
public class ValidateSnmpV3PwdTest {
	private ObjectModel objectModel
	private ComponentModel componentModel
	private UndoBuffer undoBuffer
	private SelectModel selectModel
	private def configurationReplacerTree
	private ImportProjectController ifc
	private Validator v

	/**
	 * One-time setup called before all tests run.
	 */
	@BeforeClass()
	static void classSetup() {
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.WARN)
	}

	/**
	 * Setup called before each test.
	 */
	@Before
	void setup() {
		(objectModel, componentModel) = TestUtils.initModels()

		selectModel = CentralCatalogue.getSelectModel()
		undoBuffer = CentralCatalogue.getUndoBuffer()

		def treeModels = []
		def cmtm = new ConfigurationReplacerTreeModel(componentModel, "FakeTree")
		treeModels.add(cmtm)
		configurationReplacerTree = new ConfigurationReplacerTree(cmtm)
		ConfigurationReplacer cr = new ConfigurationReplacer(selectModel, componentModel, configurationReplacerTree, undoBuffer, CentralCatalogue.getParentFrame())

		DLProject project = TestUtils.loadProjectFile('build/testFiles/import_short_pwds.dlz', objectModel, componentModel, cr )

		ifc = new ImportProjectController( project, selectModel, configurationReplacerTree)

		v = new Validator(objectModel, componentModel, ProjectSettings.getInstance())

	}

	/**
	 * Cleanup called after each test.
	 */
	@After
	void tearDown() {
		ifc.close()
		objectModel = null
		componentModel = null
		undoBuffer = null
		selectModel = null
		v = null
	}

	/**
	 * One-time cleanup called after all tests run.
	 */
	@AfterClass()
	static void classTearDown() {

	}

	@Test
	void testExportPrevalidationWithShortPasswords() {
		println("Starting testExportPrevalidationWithShortPasswords ")

		def result = v.validate()
		boolean shortAuthPasswordDetected = false
		boolean shortPrivPasswordDetected = false
		if (result != Validator.RESULT_PASS) {
			v.getFailures().each {
				String itString = (String)it
				if (itString.startsWith("Auth Password must be at least 8 characters long.")) {
					shortAuthPasswordDetected = true
				} else if (itString.startsWith("Private Password must be at least 8 characters long.")) {
					shortPrivPasswordDetected = true
				}
			}
			assert shortAuthPasswordDetected : "Short Auth Password not detected."
			assert shortPrivPasswordDetected : "Short Private Password not detected."
		} else {
			assert true: "It was supposed to fail validation of SNMP V3 short passwords, but did not."
		}
	}
}