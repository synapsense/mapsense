package DeploymentLab.SmartZone;

import com.panduit.sz.api.ss.assets.SsAssets;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import static com.panduit.sz.client.shared.SmartZoneRestClient.AUTHENTICATE_PATH;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.header;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

public class SmartZoneApiTestBase {

    protected static final MediaType SZAPI_UTF8 = new MediaType( MediaType.APPLICATION_JSON.getType(),
                                                                 MediaType.APPLICATION_JSON.getSubtype(),
                                                                 StandardCharsets.UTF_8 );
    public static final String SMARTZONE_USER = "admin";
    public static final String SMARTZONE_PWD  = "Panduit12";

    // A known dev/test server with data -- if needed.
    public static final String SMARTZONE_HOST = "10.74.58.104";
    public static final int SMARTZONE_PORT = 8443;

    protected MockRestServiceServer mocksvr;
    private String mockSessionId;


    protected void setupMockServer( RestTemplate template ) {
        mocksvr = MockRestServiceServer.createServer( template );
        expectAuthentication();
    }

    protected void expectAuthentication() {
        mockSessionId = "JSESSIONID=" + System.currentTimeMillis();

        HttpHeaders rspHdr = new HttpHeaders();
        rspHdr.add( HttpHeaders.SET_COOKIE, mockSessionId );

        mocksvr.expect( requestTo( uri(AUTHENTICATE_PATH) ) )
               .andExpect( method(HttpMethod.POST) )
               .andExpect( content().string("username=" + SMARTZONE_USER + "&password=" + SMARTZONE_PWD) )
               .andRespond( withSuccess("{\"success\":true,\"errorCode\":-1,\"errorMessages\":[]}", MediaType.APPLICATION_JSON).headers( rspHdr ) );
    }

    protected void expectLocationEndpoint() {
        mocksvr.expect( requestTo( uri( SsAssets.locationsPath() ) ) )
               .andExpect(  method( HttpMethod.GET ) )
               .andExpect( header( HttpHeaders.COOKIE, mockSessionId ) )
               .andRespond( withSuccess( resource( SsAssets.locationsPath() ), SZAPI_UTF8 ) );
    }

    protected void expectFloorplanEndpoint( int id ) {
        String endpoint = SsAssets.floorplanPath(id);
        mocksvr.expect( requestTo( uri( endpoint ) ) )
               .andExpect(  method( HttpMethod.GET ) )
               .andExpect( header( HttpHeaders.COOKIE, mockSessionId ) )
               .andRespond( withSuccess( resource( endpoint ), SZAPI_UTF8 ) );
    }

    protected void expectFloorplanPrevUninstrumented(int id, HttpMethod method) {
        String endpoint = SsAssets.instrumentationPath(id);

        //determine response type based on HTTP method.
        String response = null;
        response = (method == HttpMethod.PUT) ?"true" : response;
        response = (method == HttpMethod.DELETE) ?"false" : response;


        mocksvr.expect(requestTo(uri(endpoint)))
                .andExpect(method(method))
                .andExpect(header(HttpHeaders.COOKIE, mockSessionId))
                .andRespond(withSuccess(response, SZAPI_UTF8));
    }

    protected void expectFloorplanPrevInstrumented(int id, HttpMethod method) {
        String endpoint = SsAssets.instrumentationPath(id);

        //determine response type based on HTTP method.
        String response = null;
        response = (method == HttpMethod.PUT) ?"false" : response;
        response = (method == HttpMethod.DELETE) ?"true" : response;

        mocksvr.expect(requestTo(uri(endpoint)))
                .andExpect(method(method))
                .andExpect(header(HttpHeaders.COOKIE, mockSessionId))
                .andRespond(withSuccess(response, SZAPI_UTF8));
    }


    /** Reads a resource file from the same package as the test class */
    protected String resource( String resource ) {
        try {
            return new String(Files.readAllBytes(Paths.get("build/testFiles/smartzone/rest", resource + ".json" )));
        } catch( IOException e ) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Builds a full URI
     *
     * @param resourcePath a resource path, relative to the context path of the webapp. Ex: {@code /api/ss/floorplans/21}. Note that
     *                     variables are already expanded.
     * @return the full URI. Ex: {@code https://localhost:8443/rest/api/ss/floorplans/21}
     */
    protected static URI uri(String resourcePath) {
        return UriComponentsBuilder.newInstance()
                .scheme("https")
                .host( SMARTZONE_HOST )
                .port( SMARTZONE_PORT )
                .path( "rest" )
                .path( resourcePath )
                .build()
                .encode()
                .toUri();
    }
}
