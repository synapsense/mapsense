package DeploymentLab.SmartZone

import DeploymentLab.TestUtils
import DeploymentLab.channellogger.LogLevel
import com.panduit.sz.api.ss.assets.Floorplan
import com.panduit.sz.api.ss.assets.Location
import com.panduit.sz.api.ss.assets.SsAssets
import com.panduit.sz.client.shared.SmartZoneRestClient
import com.panduit.sz.shared.AuthenticationException
import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Ignore
import org.junit.Test
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.test.web.client.MockRestServiceServer

import static org.springframework.test.web.client.match.MockRestRequestMatchers.content
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess

/**
 *
 *
 * @author Ken Scoggins
 * @since SmartZone 8.0.0
 */
class SmartZoneClientTest extends SmartZoneApiTestBase {

    private static final String FILE_PATH = "C:\\Users\\Test.dlz";
    /**
     * One-time setup called before all tests run.
     */
    @BeforeClass()
    static void classSetup() {
        TestUtils.enableLogging(LogLevel.WARN)
    }

    /**
     * Setup called before each test.
     */
    @Before
    void setup() {
    }

    /**
     * Cleanup called after each test.
     */
    @After
    void tearDown() {
    }

    /**
     * One-time cleanup called after all tests run.
     */
    @AfterClass()
    static void classTearDown() {
    }


    @Test(expected = AuthenticationException.class)
    void testInvalidUsername() {
        SmartZoneClient svc = new SmartZoneClient( SMARTZONE_HOST, SMARTZONE_PORT, "bad-user", SMARTZONE_PWD )

        // Testing failed authentication, so the setup helper isn't helpful.
        mocksvr = MockRestServiceServer.createServer( svc.getTemplate() );
        mocksvr.expect( requestTo( uri( SmartZoneRestClient.AUTHENTICATE_PATH ) ) )
               .andExpect( method( HttpMethod.POST ) )
               .andExpect( content().string( "username=bad-user&password=" + SMARTZONE_PWD ) )
               .andRespond( withSuccess( "{\"success\":false,\"errorCode\":19,\"errorMessages\":[\"Bad user\"]}", MediaType.APPLICATION_JSON ) );

        List<Location> locs = svc.getLocationTree()

        mocksvr.verify()
        assert locs == null
    }

    @Test(expected = AuthenticationException.class)
    void testInvalidPassword() {
        SmartZoneClient svc = new SmartZoneClient( SMARTZONE_HOST, SMARTZONE_PORT, SMARTZONE_USER, "bad-password" )

        // Testing failed authentication, so the setup helper isn't helpful.
        mocksvr = MockRestServiceServer.createServer( svc.getTemplate() );
        mocksvr.expect( requestTo( uri( SmartZoneRestClient.AUTHENTICATE_PATH ) ) )
               .andExpect( method( HttpMethod.POST ) )
               .andExpect( content().string( "username=" + SMARTZONE_USER + "&password=bad-password" ) )
               .andRespond( withSuccess( "{\"success\":false,\"errorCode\":19,\"errorMessages\":[\"Bad password\"]}", MediaType.APPLICATION_JSON ) );

        List<Location> locs = svc.getLocationTree()

        mocksvr.verify()
        assert locs == null
    }

    @Test
    void testGetLocationTree() {
        SmartZoneClient svc = new SmartZoneClient( SMARTZONE_HOST, SMARTZONE_PORT, SMARTZONE_USER, SMARTZONE_PWD )

        setupMockServer( svc.getTemplate() )

        // We'll make two calls to the service.
        expectLocationEndpoint()
        expectLocationEndpoint()

        List<Location> locs = svc.getLocationTree()
        assert locs != null

        // Asserts are specific to the test data we are using
        assert locs.size() > 0
        locs.each { loc ->
            assert !loc.getName().isEmpty()
        }
        int firstSize = locs.size()

        // Make sure it works on the second call with the same session.
        locs = svc.getLocationTree()
        assert locs != null
        assert locs.size() == firstSize

        mocksvr.verify()
    }

    @Test
    void testGetFloorplan() {
        SmartZoneClient svc = new SmartZoneClient( SMARTZONE_HOST, SMARTZONE_PORT, SMARTZONE_USER, SMARTZONE_PWD )
        setupMockServer( svc.getTemplate() )

        // TODO: getLocations and use real ids.

        // We'll make two calls to the service.
        expectFloorplanEndpoint(11)
        expectFloorplanEndpoint(17)

        Floorplan floorplan = svc.getFloorplan(11)
        assert floorplan != null

        // Make sure it works on the second call with the same session.
        floorplan = svc.getFloorplan(17)
        assert floorplan != null

        mocksvr.verify()
    }

    @Test
    void testInstrumentFloorplan() {
        //setup
        SmartZoneClient svc = new SmartZoneClient( SMARTZONE_HOST, SMARTZONE_PORT, SMARTZONE_USER, SMARTZONE_PWD )
        setupMockServer( svc.getTemplate() )
        expectFloorplanPrevUninstrumented(11, HttpMethod.PUT);

        //execute
        boolean wasInstrumented = svc.instrumentFloorplan(11, FILE_PATH);

        //verify
        assert wasInstrumented == true;
        mocksvr.verify();
    }

    @Test
    void testInstrumentPrevInstrumentedFloorplan() {
        //setup
        SmartZoneClient svc = new SmartZoneClient( SMARTZONE_HOST, SMARTZONE_PORT, SMARTZONE_USER, SMARTZONE_PWD )
        setupMockServer( svc.getTemplate() )
        expectFloorplanPrevInstrumented(11, HttpMethod.PUT);

        //execute
        boolean wasInstrumented = svc.instrumentFloorplan(11, FILE_PATH);

        //verify
        assert wasInstrumented == false;
        mocksvr.verify();
    }

    @Test
    void testUninstrumentPrevInstrumentedFloorplan() {
        //setup
        SmartZoneClient svc = new SmartZoneClient( SMARTZONE_HOST, SMARTZONE_PORT, SMARTZONE_USER, SMARTZONE_PWD )
        setupMockServer( svc.getTemplate() )
        expectFloorplanPrevInstrumented(11, HttpMethod.DELETE);

        //execute
        boolean wasInstrumented = svc.uninstrumentFloorplan(11);

        //verify
        assert wasInstrumented == true;
        mocksvr.verify();
    }

    @Test
    void testUninstrumentPrevUninstrumentedFloorplan() {
        //setup
        SmartZoneClient svc = new SmartZoneClient( SMARTZONE_HOST, SMARTZONE_PORT, SMARTZONE_USER, SMARTZONE_PWD )
        setupMockServer( svc.getTemplate() )
        expectFloorplanPrevUninstrumented(11, HttpMethod.DELETE);

        //execute
        boolean wasInstrumented = svc.uninstrumentFloorplan(11);

        //verify
        assert wasInstrumented == false;
        mocksvr.verify();
    }

    //
    // Direct Server Tests -- MUST RUN MANUALLY
    //

    @Test
    @Ignore("invokeRealLocationsEndpoint() must be run manually because it requires a real server")
    void invokeRealLocationsEndpoint() {
        println new SmartZoneClient( SMARTZONE_HOST, SMARTZONE_PORT, SMARTZONE_USER, SMARTZONE_PWD ).getLocationTree()
    }

    @Test(expected = AuthenticationException)
    @Ignore("invokeRealLocationsEndpointWithWrongCredentials() must be run manually because it requires a real server")
    void invokeRealLocationsEndpointWithWrongCredentials() {
        new SmartZoneClient( SMARTZONE_HOST, SMARTZONE_PORT, SMARTZONE_USER, "bad-password" ).getLocationTree()
    }

    @Test
    @Ignore("invokeRealLocationsEndpoint() must be run manually because it requires a real server")
    void invokeRealFloorplanEndpoint() {
        println new SmartZoneClient( SMARTZONE_HOST, SMARTZONE_PORT, SMARTZONE_USER, SMARTZONE_PWD ).getFloorplan(14)
    }

    @Test
    @Ignore("invokeRealLocalInstrumentationEndpoint() must be run manually because it requires a real (local) SmartZone server")
    void invokeRealLocalInstrumentationEndpoint() {
        println SsAssets.instrumentationPath(3);
        println new SmartZoneClient( "localhost", SMARTZONE_PORT, SMARTZONE_USER, SMARTZONE_PWD ).instrumentFloorplan(3)
        println new SmartZoneClient( "localhost", SMARTZONE_PORT, SMARTZONE_USER, SMARTZONE_PWD ).uninstrumentFloorplan(3)
    }
}
