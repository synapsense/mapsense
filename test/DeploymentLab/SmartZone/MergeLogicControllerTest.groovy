package DeploymentLab.SmartZone

import DeploymentLab.CentralCatalogue;
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLObject;
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Model.ObjectType;
import DeploymentLab.SelectModel
import DeploymentLab.TestUtils;
import DeploymentLab.UndoBuffer
import com.panduit.sz.api.shared.Point2D
import com.panduit.sz.api.ss.assets.Floorplan
import com.panduit.sz.api.ss.assets.Rack
import com.sun.scenario.effect.Merge;
import org.junit.After;
import org.junit.Before
import org.junit.Test

import javax.swing.JFrame;

/**
 * Created by LNI on 3/8/2016.
 */
public class MergeLogicControllerTest {

    static ObjectModel omodel
    static ComponentModel cmodel
    static UndoBuffer ub
    static SelectModel selectModel

    List<Rack> racks = new ArrayList()

    static final String SZ_LOCATION_NAME = "Floor1"

    @Before
    void setup() {
        (omodel,cmodel) = TestUtils.initModels()
        selectModel = CentralCatalogue.getSelectModel()
        ub = CentralCatalogue.getUndoBuffer()
    }

    /**
     * Cleanup after each test.
     */
    @After
    void tearDown() {
        omodel = null
        cmodel = null
        selectModel = null
    }

    @Test
    void test_CreateInMS() {
        //Setup
        DLComponent drawing = mockDrawing()
        def room = cmodel.newComponent("room")
        drawing.addChild(room)

        Rack szRack = mockSZRack("Rack1", 1, 200.0, 200.0, 160.0, 260.0, 0.0)
        racks.add(szRack)

        //Verify that there is no Rack component initially
        assert !rackExistsInMS(1, drawing)

        ub.startOperation()
        MergeLogicController controller = new MergeLogicController(drawing,
                mockFloorplan(racks),
                cmodel, omodel, ub, null)
        controller.runMergeLogic()

        //Verify the new component is created
        assert rackExistsInMS(1, drawing)

        //Verify
        assert cmodel.getComponentsInDrawing(drawing).size() == 3
        assert controller.getMsConflictColumns().size() == 0 && controller.getSzConflictColumns().size() == 0
        assert controller.getMsTableDataList().size() == 0 && controller.getSzTableDataList().size() == 0

        assert drawing.getPropertyValue('name') == SZ_LOCATION_NAME
    }

    @Test
    void test_RemoveInMS() {
        //Setup
        DLComponent drawing = mockDrawing()
        def room = cmodel.newComponent("room")
        DLComponent msRack = mockMSRack("Rack1", 1, 200.0, 200.0, 160.0, 260.0, 0.0)
        drawing.addChild(room)
        room.addChild(msRack)

        //Verify that the Rack component exists in the drawing initially
        assert rackExistsInMS(1, drawing)
        ub.startOperation()
        MergeLogicController controller = new MergeLogicController(drawing, mockFloorplan(new ArrayList()),
                                                                   cmodel, omodel, ub, null)
        controller.runMergeLogic()

        //Verify that the component is removed
        assert !rackExistsInMS(1, drawing)

        //Verify
        assert cmodel.getComponentsInDrawing(drawing).size() == 2
        assert controller.getMsConflictColumns().size() == 0 && controller.getSzConflictColumns().size() == 0
        assert controller.getMsTableDataList().size() == 0 && controller.getSzTableDataList().size() == 0

        assert drawing.getPropertyValue('name') == SZ_LOCATION_NAME
    }

    @Test
    void test_MergeResults() {
        //Setup
        DLComponent drawing = mockDrawing()
        def room = cmodel.newComponent("room")
        DLComponent msRack = mockMSRack("Rack1", 1, 200.0, 200.0, 160.0, 260.0, 0.0)
        drawing.addChild(room)
        room.addChild(msRack)

        Rack szRack = mockSZRack("Rack1", 1, 200.0, 230.0, 160.0, 260.0, 0.0)
        racks.add(szRack)
        ub.startOperation()
        MergeLogicController controller = new MergeLogicController(drawing, mockFloorplan(racks),
                                                               cmodel, omodel, ub, null)
        controller.runMergeLogic()

        //Verify
        assert cmodel.getComponentsInDrawing(drawing).size() == 3
        assert controller.getMsConflictColumns().size() == 1 && controller.getSzConflictColumns().size() == 1
        assert controller.getMsTableDataList().size() != 0 && controller.getSzTableDataList().size() != 0

        assert drawing.getPropertyValue('name') == SZ_LOCATION_NAME
    }

    @Test
    void test_multipleDrawings() {

    }

    @Test
    void testSame_OldValNewVal() {
        DLComponent drawing = mockDrawing()
        def room = cmodel.newComponent("room")
        DLComponent msRack = mockMSRack("Rack1", 1, 200.0, 200.0, 160.0, 260.0, 0.0)
        drawing.addChild(room)
        room.addChild(msRack)

        List<DLObject> msRackObjs = msRack.getObjectsOfType( ObjectType.RACK )
        msRackObjs.get(0).getObjectSetting('name').setOldValue("Rack1")
        msRackObjs.get(0).getObjectSetting('x').setOldValue("200.0")
        msRackObjs.get(0).getObjectSetting('y').setOldValue("200.0")
        msRackObjs.get(0).getObjectSetting('width').setOldValue("160.0")
        msRackObjs.get(0).getObjectSetting('depth').setOldValue("260.0")
        msRackObjs.get(0).getObjectSetting('rotation').setOldValue("0.0")

        Rack szRack = mockSZRack("Rack1", 1, 200.0, 230.0, 160.0, 260.0, 0.0)
        racks.add(szRack)
        ub.startOperation()
        MergeLogicController controller = new MergeLogicController(drawing, mockFloorplan(racks), cmodel, omodel, ub, null)
        controller.runMergeLogic()

        //Verify
        assert controller.getMsConflictColumns().size() == 0 && controller.getSzConflictColumns().size() == 0
        assert controller.getMsTableDataList().size() == 0 && controller.getSzTableDataList().size() == 0

        assert drawing.getPropertyValue('name') == SZ_LOCATION_NAME

        //Verify that the SZ values are used
        for (DLComponent comp : cmodel.getComponentsInDrawing(drawing)) {
            if (comp.hasManagedObjectOfType(ObjectType.RACK) && comp.getPropertyValue('smartZoneId') == 1) {
                assert comp.getPropertyValue('name') == szRack.getName()
                assert comp.getPropertyValue('x') == szRack.getLocation().getX()
                assert comp.getPropertyValue('y') == szRack.getLocation().getY()
                assert comp.getPropertyValue('width') == szRack.getWidth()
                assert comp.getPropertyValue('depth') == szRack.getDepth()
                assert comp.getPropertyValue('rotation') == szRack.getRotation()
            }
        }
    }

    @Test
    void testSame_SZValOldVal() {
        DLComponent drawing = mockDrawing()
        def room = cmodel.newComponent("room")
        DLComponent msRack = mockMSRack("Rack1", 1, 200.0, 200.0, 160.0, 260.0, 0.0)
        drawing.addChild(room)
        room.addChild(msRack)

        List<DLObject> msRackObjs = msRack.getObjectsOfType( ObjectType.RACK )
        msRackObjs.get(0).getObjectSetting('name').setOldValue("Rack1")
        msRackObjs.get(0).getObjectSetting('x').setOldValue("200.0")
        msRackObjs.get(0).getObjectSetting('y').setOldValue("230.0")
        msRackObjs.get(0).getObjectSetting('width').setOldValue("100.0")
        msRackObjs.get(0).getObjectSetting('depth').setOldValue("260.0")
        msRackObjs.get(0).getObjectSetting('rotation').setOldValue("0.0")

        Rack szRack = mockSZRack("Rack1", 1, 200.0, 230.0, 100.0, 260.0, 0.0)
        racks.add(szRack)
        ub.startOperation()
        MergeLogicController controller = new MergeLogicController(drawing, mockFloorplan(racks),
                                                           cmodel, omodel, ub, null)
        controller.runMergeLogic()

        //Verify
        assert controller.getMsConflictColumns().size() == 0 && controller.getSzConflictColumns().size() == 0
        assert controller.getMsTableDataList().size() == 0 && controller.getSzTableDataList().size() == 0

        assert drawing.getPropertyValue('name') == SZ_LOCATION_NAME

        //Verify that the MS new values are used
        for (DLComponent comp : cmodel.getComponentsInDrawing(drawing)) {
            if (comp.hasManagedObjectOfType(ObjectType.RACK) && comp.getPropertyValue('smartZoneId') == 1) {
                assert comp.getPropertyValue('name') == "Rack1"
                assert comp.getPropertyValue('x') == 200.0
                assert comp.getPropertyValue('y') == 200.0
                assert comp.getPropertyValue('width') == 100.0
                assert comp.getPropertyValue('depth') == 260.0
                assert comp.getPropertyValue('rotation') == 0.0
            }
        }
    }

    @Test
    void testSame_SZValNewVal() {
        DLComponent drawing = mockDrawing()
        def room = cmodel.newComponent("room")
        DLComponent msRack = mockMSRack("Rack1", 1, 200.0, 200.0, 160.0, 260.0, 0.0)
        drawing.addChild(room)
        room.addChild(msRack)

        List<DLObject> msRackObjs = msRack.getObjectsOfType( ObjectType.RACK )
        msRackObjs.get(0).getObjectSetting('name').setOldValue("OldRack1")
        msRackObjs.get(0).getObjectSetting('x').setOldValue("200.0")
        msRackObjs.get(0).getObjectSetting('y').setOldValue("230.0")
        msRackObjs.get(0).getObjectSetting('width').setOldValue("160.0")
        msRackObjs.get(0).getObjectSetting('depth').setOldValue("260.0")
        msRackObjs.get(0).getObjectSetting('rotation').setOldValue("45.0")

        Rack szRack = mockSZRack("Rack1", 1, 200.0, 200.0, 160.0, 260.0, 0.0)
        racks.add(szRack)
        ub.startOperation()
        MergeLogicController controller = new MergeLogicController(drawing, mockFloorplan(racks),
                                                       cmodel, omodel, ub, null)
        controller.runMergeLogic()

        //Verify
        assert controller.getMsConflictColumns().size() == 0 && controller.getSzConflictColumns().size() == 0
        assert controller.getMsTableDataList().size() == 0 && controller.getSzTableDataList().size() == 0

        assert drawing.getPropertyValue('name') == SZ_LOCATION_NAME

        //Verify that this is NOOP and the MS new values are retained.
        for (DLComponent comp : cmodel.getComponentsInDrawing(drawing)) {
            if (comp.hasManagedObjectOfType(ObjectType.RACK) && comp.getPropertyValue('smartZoneId') == 1) {
                assert comp.getPropertyValue('name') == "Rack1"
                assert comp.getPropertyValue('x') == 200.0
                assert comp.getPropertyValue('y') == 200.0
                assert comp.getPropertyValue('width') == 160.0
                assert comp.getPropertyValue('depth') == 260.0
                assert comp.getPropertyValue('rotation') == 0.0
            }
        }
    }

    @Test
    void testDifferent_SZValOldValNewVal() {
        DLComponent drawing = mockDrawing()
        def room = cmodel.newComponent("room")
        DLComponent msRack = mockMSRack("Rack1", 1, 200.0, 200.0, 160.0, 260.0, 0.0)
        drawing.addChild(room)
        room.addChild(msRack)

        List<DLObject> msRackObjs = msRack.getObjectsOfType( ObjectType.RACK )
        msRackObjs.get(0).getObjectSetting('name').setOldValue("Rack12")
        msRackObjs.get(0).getObjectSetting('x').setOldValue("200.0")
        msRackObjs.get(0).getObjectSetting('y').setOldValue("230.0")
        msRackObjs.get(0).getObjectSetting('width').setOldValue("160.0")
        msRackObjs.get(0).getObjectSetting('depth').setOldValue("260.0")
        msRackObjs.get(0).getObjectSetting('rotation').setOldValue("45.0")

        Rack szRack = mockSZRack("Rack11", 1, 200.0, 150.0, 160.0, 260.0, 75.0)
        racks.add(szRack)
        ub.startOperation()
        MergeLogicController controller = new MergeLogicController(drawing, mockFloorplan(racks),
                                              cmodel, omodel, ub, null)
        controller.runMergeLogic()

        //Verify
        assert controller.getMsConflictColumns().size() != 0 && controller.getSzConflictColumns().size() != 0
        assert controller.getMsTableDataList().size() != 0 && controller.getSzTableDataList().size() != 0
    }

    @Test
    void testDifferent_SZValNewVal_nullOldVal() {
        DLComponent drawing = mockDrawing()
        def room = cmodel.newComponent("room")
        DLComponent msRack = mockMSRack("Rack1", 1, 200.0, 200.0, 160.0, 260.0, 0.0)
        drawing.addChild(room)
        room.addChild(msRack)

        List<DLObject> msRackObjs = msRack.getObjectsOfType( ObjectType.RACK )
        msRackObjs.get(0).getObjectSetting('name').setOldValue("Rack12")
        msRackObjs.get(0).getObjectSetting('x').setOldValue(null)
        msRackObjs.get(0).getObjectSetting('y').setOldValue("")
        msRackObjs.get(0).getObjectSetting('width').setOldValue("160.0")
        msRackObjs.get(0).getObjectSetting('depth').setOldValue("260.0")
        msRackObjs.get(0).getObjectSetting('rotation').setOldValue(null)

        Rack szRack = mockSZRack("Rack11", 1, 200.0, 150.0, 160.0, 260.0, 75.0)
        racks.add(szRack)
        ub.startOperation()
        MergeLogicController controller = new MergeLogicController(drawing, mockFloorplan(racks),
                                              cmodel, omodel, ub, null)
        controller.runMergeLogic()

        //Verify
        assert controller.getMsConflictColumns().size() != 0 && controller.getSzConflictColumns().size() != 0
        assert controller.getMsTableDataList().size() != 0 && controller.getSzTableDataList().size() != 0
    }

    @Test
    void testRounding_WidthDepth() {
        DLComponent drawing = mockDrawing()
        def room = cmodel.newComponent("room")
        DLComponent msRack = mockMSRack("Rack1", 1, 200.0, 200.0, 160.598, 42, 0.0)
        drawing.addChild(room)
        room.addChild(msRack)

        Rack szRack = mockSZRack("Rack1", 1, 200.0, 200.0, 160.932134, 41.9, 75.0)
        racks.add(szRack)
        ub.startOperation()
        MergeLogicController controller = new MergeLogicController(drawing, mockFloorplan(racks),
                cmodel, omodel, ub, null)
        controller.runMergeLogic()

        //Verify
        assert controller.getMsConflictColumns().size() == 1 && controller.getSzConflictColumns().size() == 1
        assert controller.getMsTableDataList().size() == 1 && controller.getSzTableDataList().size() == 1

        String msWD = controller.getMsTableDataList().get(0).getTableProperties().get(3)
        String szWD = controller.getSzTableDataList().get(0).getTableProperties().get(2)

        String[] msWDArray = msWD.split("x")
        String[] szWDArray = szWD.split("x")
        assert msWDArray[0] == szWDArray[0] && msWDArray[1] == szWDArray[1]
    }

    @Test
    void testDecimal_position_rotation() {
        DLComponent drawing = mockDrawing()
        def room = cmodel.newComponent("room")
        DLComponent msRack = mockMSRack("Rack1", 1, 200.234234, 200.99999, 160.598, 260.1287, 75.98123)
        drawing.addChild(room)
        room.addChild(msRack)

        Rack szRack = mockSZRack("Rack1", 1, 200.98986, 200.94456, 160.632134, 260.119, 75.18787)
        racks.add(szRack)
        ub.startOperation()
        MergeLogicController controller = new MergeLogicController(drawing, mockFloorplan(racks),
                cmodel, omodel, ub, null)
        controller.runMergeLogic()

        //Verify
        assert controller.getMsConflictColumns().size() == 2 && controller.getSzConflictColumns().size() == 2
        assert controller.getMsTableDataList().size() == 1 && controller.getSzTableDataList().size() == 1
    }

    @Test
    void test_AllComponentTypes() {
//         Set<String> componentTypes = cmodel.listComponentTypes()
//         println("TYPES: " +omodel.getTypes())
    }

    @Test
    void test_MultipleMergeConflicts() {

    }

    @Test
    void test_NOOP() {
        //Setup
        DLComponent drawing = mockDrawing()
        def room = cmodel.newComponent("room")
        DLComponent msRack = mockMSRack("Rack1", 1, 200.0, 200.0, 160.0, 260.0, 0.0)
        drawing.addChild(room)
        room.addChild(msRack)

        omodel.listDeletedObjects().add(msRack.getObject(ObjectType.RACK))
        ub.startOperation()
        MergeLogicController controller = new MergeLogicController(drawing, mockFloorplan(new ArrayList()),
                                                               cmodel, omodel, ub, null)
        controller.runMergeLogic()

        //Verify
        assert controller.getMsConflictColumns().size() == 0 && controller.getSzConflictColumns().size() == 0
        assert controller.getMsTableDataList().size() == 0 && controller.getSzTableDataList().size() == 0

        assert !rackExistsInMS(1, drawing)
    }

    private boolean rackExistsInMS(Integer id, DLComponent drawing) {
        for (DLComponent comp : cmodel.getComponentsInDrawing(drawing)) {
            if (comp.hasManagedObjectOfType(ObjectType.RACK) && comp.getPropertyValue('smartZoneId') == id) {
                return true
            }
        }
        return false
    }

    private DLComponent mockDrawing() {
        DLComponent drawing = cmodel.newComponent("drawing")
        selectModel.setActiveDrawing(drawing)
        drawing.setPropertyValue('smartZoneId', 10)
        drawing.setPropertyValue('name', 'drawing1')
        return drawing
    }

    private DLComponent mockMSRack(String name,
                                   Integer szId,
                                   Double x,
                                   Double y,
                                   Double width,
                                   Double depth,
                                   Double rotation) {
        DLComponent ez_rack = cmodel.newComponent("ez-rack-1-0")
        ez_rack.setPropertyValue('smartZoneId', szId)
        ez_rack.setPropertyValue('name', name)
        ez_rack.setPropertyValue('x', x)
        ez_rack.setPropertyValue('y', y)
        ez_rack.setPropertyValue('width', width)
        ez_rack.setPropertyValue('depth', depth)
        ez_rack.setPropertyValue('rotation', rotation)
        return ez_rack
    }

    private Rack mockSZRack(String name,
                                  Integer id,
                                  Double x,
                                  Double y,
                                  Double width,
                                  Double depth,
                                  Double rotation) {
        return Rack.builder().id(id).name(name).location(Point2D.builder().x(x).y(y).build()).width(width).depth(depth).rotation(rotation).build()

    }

    private Floorplan mockFloorplan(List<Rack> racks) {
        return Floorplan.builder().id(10).name(SZ_LOCATION_NAME).width(0.0).height(0.0).background(Optional.empty()).racks(racks.asImmutable()).build()
    }
}