package DeploymentLab.Security

import DeploymentLab.CentralCatalogue
import org.junit.AfterClass

import org.junit.BeforeClass

import org.junit.Test

import DeploymentLab.Model.*
import org.junit.Before
import org.junit.After
import DeploymentLab.TestUtils

class CRCTest {

	static ObjectModel omodel
	static ComponentModel cmodel
	static LogicalId lid



	/**
	 * Constructs a base model we can use to do tests on
	 */
	@Before
	void beforeClass() {
		(omodel,cmodel) = TestUtils.initModels()

		lid = new LogicalId( cmodel, omodel )

	}

	@Test
	void macChecksumTest(){
		assert ! MacChecksum.isValidMac(1)
		assert ! MacChecksum.isValidMac(42)
		assert ! MacChecksum.isValidMac(123456789)
		//some random pressure nodes:
		assert MacChecksum.isValidMac( 0x01acc6eb130000d1 )
		assert MacChecksum.isValidMac( 0x0160c1eb13000062 )
		assert MacChecksum.isValidMac( 0x0156fbeb13000004 )

		//also check against the platformid
		assert MacChecksum.isValidMac( 0x01acc6eb130000d1, 13 )
		assert MacChecksum.isValidMac( 0x0160c1eb13000062, 13 )
		assert MacChecksum.isValidMac( 0x0156fbeb13000004, 13 )

		assert !MacChecksum.isValidMac( 0x01acc6eb130000d1, 80 )
		assert !MacChecksum.isValidMac( 0x0160c1eb13000062, 82 )
		assert !MacChecksum.isValidMac( 0x0156fbeb13000004, 92 )
	}


	@Test
	void EUI64Checksums(){
		assert MacChecksum.isValidMac( 0x00336C4A0014AB1F )
		assert MacChecksum.isValidMac( 0x00336C36001587D9 )
		assert MacChecksum.isValidMac( 0x00336C1F0004F7EA )
		assert MacChecksum.isValidMac( 0x00336C070011D4C8 )
		assert MacChecksum.isValidMac( 0x00336C1E00037EEA )

		//also check against the platformid
		assert MacChecksum.isValidMac( 0x00336C4A0014AB1F, 80 )
		assert MacChecksum.isValidMac( 0x00336C36001587D9, 82 )
		assert MacChecksum.isValidMac( 0x00336C1F0004F7EA, 92 )
		assert MacChecksum.isValidMac( 0x00336C070011D4C8, 82 )
		assert MacChecksum.isValidMac( 0x00336C1E00037EEA, 82 )

		assert !MacChecksum.isValidMac( 0x00336C4A0014AB1F, 13 )
		assert !MacChecksum.isValidMac( 0x00336C36001587D9, 14 )
		assert !MacChecksum.isValidMac( 0x00336C1F0004F7EA, 15 )
		assert !MacChecksum.isValidMac( 0x00336C070011D4C8, 16 )
		assert !MacChecksum.isValidMac( 0x00336C1E00037EEA, 52 )
	}

	@Test
	void crc16Test(){
		def hash = new CRC16()
		hash.reset()
		hash.update("1")
		assert hash.getValue() == 38014
		hash.reset()
		hash.update("gabe")
		assert hash.getValue() == 50599
		hash.reset()
		hash.update("42")
		assert hash.getValue() == 42390
		hash.reset()
		hash.update("13")
		assert hash.getValue() == 13652
		hash.reset()
		hash.update("123456789")
		assert hash.getValue() == 19255

		hash.reset()
		hash.update("01acc6eb130000d1")
		assert hash.getValue() == 50712
		//println hash.getValue()
		hash.reset()
		hash.update("0160c1eb13000062")
		assert hash.getValue() == 55276
		//println hash.getValue()
		hash.reset()
		hash.update("0156fbeb13000004")
		assert hash.getValue() == 63847
		//println hash.getValue()

		//not checking output, but making sure this doesn't blow up or break something
		CRC16.main("")

	}
/*
	@Test
	void crc16CollisionTest(){

		def range = 0..10000
		def results = [:]
		def hash = new CRC16()
		range.each{ num ->
			hash.reset()

			def n = Integer.toHexString(num)

		if ( n.length() < 16  ){
			//pad out to sixteen chars
			def diff = 16 - n.length()
			n = n.padLeft(diff, "0")
			//println "treating mac to $n"
		}
			hash.update(n)


			def current = hash.getValue()
			if ( results.containsKey( current ) ){
				//collision!
				results[current] += n
			} else {
				results[current] = []
				results[current] += n
			}
		}

		println "collisons on:"
		results.each{ k, v ->
			if ( v.size() > 1){
				println "hash $k resulted from $v"
			}
		}


	}
*/

	@Test
	void crc16SpecificCollisionTest(){

		def hash = new CRC16()

		hash.reset()
		hash.update("0000000000400")
		def result1 = hash.getValue()

		hash.reset()
		hash.update("0000000000805")
		def result2 = hash.getValue()

		assert result1 == result2
	}


	@Test
	void logicalIDTest(){

		//create some nodes
		//assign some mac ids
		//compute the lids

		DLComponent network = cmodel.newComponent('network')
		network.setPropertyValue('name', 'network1')
		//network.setPropertyValue('pan_id', 'beef')


		DLComponent room = cmodel.newComponent( ComponentType.ROOM )

		CentralCatalogue.getSelectModel().getActiveDrawing().addChild( room )
		CentralCatalogue.getSelectModel().getActiveDrawing().addChild( network )

		DLComponent rack = cmodel.newComponent('rack-control-rearexhaust-nsf-rt-thermanode')
		rack.setPropertyValue('name', 'new rack name')
		rack.setPropertyValue('mac_id', "01acc6eb130000d1" )

		DLComponent crah = cmodel.newComponent("crah-single-plenumrated-thermanode")
		//crah.setPropertyValue('mac_id', "01acc6eb130000d1")
		crah.setPropertyValue('mac_id', "9")

		assert rack.isAssociatiable()
		assert crah.isAssociatiable()
		rack.associate( "reftemp", crah, "reftemp" )
		assert rack.getAllAssociatedComponents().contains(crah)
		assert crah.getAllAssociatedComponents().contains(rack)

		room.addChild(rack)
		room.addChild(crah)
		network.addChild(rack)
		network.addChild(crah)

		DLComponent rack2 = cmodel.newComponent('rack-control-rearexhaust-nsf-rt-thermanode')
		rack2.setPropertyValue('mac_id', '0160c1eb13000062')
		DLComponent rack3 = cmodel.newComponent('rack-control-rearexhaust-nsf-rt-thermanode')
		rack3.setPropertyValue('mac_id', '0156fbeb13000004')

		//values [0000000000400, 0000000000805] will collide
		DLComponent rack4 = cmodel.newComponent('rack-control-rearexhaust-nsf-rt-thermanode')
		rack4.setPropertyValue('mac_id', '0000000000400')
		DLComponent rack5 = cmodel.newComponent('rack-control-rearexhaust-nsf-rt-thermanode')
		rack5.setPropertyValue('mac_id', '0000000000805')
		//DLComponent rack6 = cmodel.newComponent('rack-control-rearexhaust-nsf-rt-thermanode')
		//rack6.setPropertyValue('mac_id', '300')

		room.addChild(rack2)
		room.addChild(rack3)
		room.addChild(rack4)
		room.addChild(rack5)

		network.addChild(rack2)
		network.addChild(rack3)
		network.addChild(rack4)
		network.addChild(rack5)

		//and some gateways!
		def gw1 = cmodel.newComponent("remote-gateway")
		gw1.setPropertyValue('mac_id', '0000000000001')
		def gw2 = cmodel.newComponent("remote-gateway")
		gw2.setPropertyValue('mac_id', '0000000000002')
		def gw3 = cmodel.newComponent("remote-gateway")
		gw3.setPropertyValue('mac_id', '0000000000805')

		network.addChild(gw1)
		network.addChild(gw2)
		network.addChild(gw3)

		cmodel.assignLogicalIDs()

		def allLids = [:]

		omodel.getObjects().each{ o ->
			if ( o.hasObjectProperty("mac") && o.hasObjectProperty("id") ){

				def currentLid = o.getObjectProperty("id").getValue()
				if ( allLids.containsKey(currentLid) ){
					allLids[currentLid] += 1
				} else {
					allLids[currentLid] = 1
				}

				///println Long.toHexString(o.getObjectProperty("mac").getValue()) + " ==> " +  Long.toHexString(o.getObjectProperty("id").getValue())
				if ( o.getType().equalsIgnoreCase("wsngateway") ){
					assert o.getObjectProperty("id").getValue() == lid.generateGatewayLogicalId( Long.toHexString(o.getObjectProperty("mac").getValue()), cmodel.getOwner(o) )
				} else {
					assert o.getObjectProperty("id").getValue() == lid.generateLogicalId( Long.toHexString(o.getObjectProperty("mac").getValue()), cmodel.getOwner(o) )
				}

			}
		}

		allLids.each{ k, v ->
			if ( v > 1){
				assert false
			}
		}

		
	}

	@Test
	void massUpdateTest(){

		DLComponent rack = cmodel.newComponent('rack-control-rearexhaust-nsf-rt-thermanode')
		rack.setPropertyValue('name', 'new rack name')
		rack.setPropertyValue('mac_id', "01acc6eb130000d1" )

		def gw1 = cmodel.newComponent("remote-gateway")
		gw1.setPropertyValue('mac_id', '0000000000001')

		cmodel.assignLogicalIDs()

		DLComponent rack2 = cmodel.newComponent('rack-control-rearexhaust-nsf-rt-thermanode')
		rack2.setPropertyValue('name', 'new rack name')
		rack2.setPropertyValue('mac_id', "000042" )
		rack2.getObject("node").getObjectProperty("id").setValue(0)

		def gw2 = cmodel.newComponent("remote-gateway")
		gw2.setPropertyValue('mac_id', '1138')
		gw2.getObject("gateway").getObjectProperty("id").setValue(0)

		cmodel.assignMissingLogicalIDs()

		omodel.getObjects().each{ o->

			if ( o.hasObjectProperty("id") ){
				assert o.getObjectProperty("id").getValue() != null
			}

		}
	}

	@Test
	void noLidsForNullMacsTest(){

		DLComponent rack4 = cmodel.newComponent('rack-control-rearexhaust-nsf-rt-thermanode')
		rack4.setPropertyValue('name', 'rack4')
		rack4.setPropertyValue('mac_id', "04" )

		def gw4 = cmodel.newComponent("remote-gateway")
		gw4.setPropertyValue('mac_id', '0404')

		DLComponent nullrack = cmodel.newComponent('rack-control-rearexhaust-nsf-rt-thermanode')
		nullrack.getObject("node").getObjectProperty("mac").setValue(null)
		nullrack.getObject("node").getObjectProperty("id").setValue(null)

		DLComponent nullgw = cmodel.newComponent("remote-gateway")
		nullgw.getObject("gateway").getObjectProperty("mac").setValue(null)
		nullgw.getObject("gateway").getObjectProperty("id").setValue(null)

		lid.assignNodeLids( [rack4.getObject("node"), nullrack.getObject("node")] )
		assert rack4.getObject("node").getObjectProperty("id").getValue() != null
		assert nullrack.getObject("node").getObjectProperty("id").getValue() == null

		lid.assignGatewayLids( [ gw4.getObject("gateway"), nullgw.getObject("gateway") ] )
		assert gw4.getObject("gateway").getObjectProperty("id").getValue() != null
		assert nullgw.getObject("gateway").getObjectProperty("id").getValue() == null

	}


/*
	@Test
	void thousandNodeTest(){
		List thousandNodes = FileUtil.fileToList("build\\IDtable.txt")
		DLComponent network = cmodel.newComponent('network')
		network.setPropertyValue('name', 'network1')
		network.setPropertyValue('pan_id', 'beef')
		DLComponent room = cmodel.newComponent('room')
		int counter = 0

		while (counter <= 10){
			//println "creating rack $counter of 250"
			DLComponent rack = cmodel.newComponent('rack-control-rearexhaust-nsf-rt-thermanode')
			rack.setPropertyValue('name', "thousandRack$counter")
			rack.setPropertyValue('mac_id', thousandNodes[counter] )
			room.addChild(rack)
			network.addChild(rack)

			counter ++
		}

		def allLids = [:]
		omodel.getObjects().each{ o ->
			if ( o.hasObjectProperty("mac") && o.hasObjectProperty("id") ){
				def currentLid = o.getObjectProperty("id").getValue()

				if ( allLids.containsKey(currentLid) ){
					allLids[currentLid] += 1
				} else {
					allLids[currentLid] = 1
				}
			}
		}

		allLids.each{ k, v ->
			if ( v > 1){
				assert false
			}
		}

	}
*/

/*
	@Test
	void panIDGenerationTest(){
		def results = []
		for (int i = 0; i < 50; i ++){
			String p = PanIDFactory.makePanID()
			results += p
		}
		def set = new HashSet( results )
		assert set.size() == results.size()
	}
  */

	@Test
	void panIDGenerationUniqueTest(){
		def results = []
		for (int i = 0; i < 500; i ++){
			def n = cmodel.newComponent("network")
			n.setPropertyValue('pan_id', PanIDFactory.makePanID(cmodel) )
			results += n.getPropertyValue("pan_id")

			//cook up some racks for this network
			//for ( int j = 0; j < 5; j ++){
			//	def r = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode2")
			//	n.addChild(r)
			//}
		}
		def set = new HashSet( results )
		assert set.size() == results.size()
		
		results.each{ panid ->
			assert panid != 0
		}
		
		omodel.getObjectsByType("wsnnetwork").each{ n ->
			//println "comp panid = ${cmodel.getOwner(n).getPropertyValue("pan_id")} == obj panid = ${n.getObjectProperty("panId").getValue()}"
			assert n.getObjectProperty("panId").getValue() != 0
		}
	}

	@Test
	void checkForUniqueValues(){
		def n = cmodel.newComponent("network")
		n.setPropertyValue('pan_id', "0bfb" )
		assert ! PanIDFactory.isUniquePanID(cmodel, "0bfb")
	}

	@After
	void afterClass() {
		omodel = null
		cmodel = null
		lid = null
		System.gc();
		System.gc();
		println "CRC Test Completed"
	}

}
