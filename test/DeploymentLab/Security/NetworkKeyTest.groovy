package DeploymentLab.Security

import DeploymentLab.Pair
import com.synapsense.dto.BinaryData
import java.security.KeyPair
import org.junit.Test
import java.security.MessageDigest
import javax.crypto.spec.SecretKeySpec
import javax.crypto.Cipher

/**
 * Tests the NetworkKey class.
 * 
 * @author Ken Scoggins
 * @since Jupiter
 */
public class NetworkKeyTest {

    private static final int NUM_RUNS = 100

    /**
     * Tests the generateKey method.
     */
    @Test
    void testGenerateKey() {
        KeyPair keys = NetworkKey.generateKey()

        assert keys
        assert keys.getPublic().getAlgorithm() == "EC"
        assert keys.getPrivate().getAlgorithm() == "EC"
    }

    /**
     * Tests the toFixedByteArray method.
     */
    @Test
    void testToFixedByteArray() {

        // Keys that are known to produce BigInteger byte arrays of certain sizes we want to test.
        Pair<Integer,String>[] tests = [ new Pair<>( 31, '28eb846090ce3541b7f5b6089f29443aaeda5ec6301305275b72223daddfc3'),
                                         new Pair<>( 32, '4605c6fcb0ed18e8c713bacabf19408aa834728a46082465a39e93b96304c431'),
                                         new Pair<>( 33, 'a7dbc3298b827bf3e05186a145da39bc738c58528c42851e5e49921d3ad35a56') ]

        for( Pair<Integer,String> test : tests ) {

            // Create the BigInt of a known size.
            BigInteger orig = new BigInteger( test.b, 16 )
            byte[] origArray = orig.toByteArray()
            assert origArray.length == test.a

            // Get the fixed size byte array.
            byte[] fixedArray = NetworkKey.toFixedByteArray( orig, 32 )
            assert fixedArray.length == 32

            // Make sure the value isn't mangled and the same number is created by the fixed byte array.
            BigInteger convertedBack = null
            if( fixedArray[0] < 0 ) {
                // Need to add the zero MSB back when the MSB is negative for BigInt to translate it correctly.
                byte[] foo = new int[fixedArray.length+1]
                System.arraycopy( fixedArray, 0, foo, 1, fixedArray.length )
                convertedBack = new BigInteger( foo )
            } else {
                convertedBack = new BigInteger( fixedArray )
            }
            assert orig.equals( convertedBack )
        }
    }

    /**
     * Tests the makeNetworkKeys method.
     */
    @Test
    void testMakeNetworkKeys() {
        // Run multiple times to make sure things are generated correctly since there is some randomness to it all.
        for( int i = 0; i < NUM_RUNS; i++ ) {
            Pair<BinaryData,BinaryData> keys = NetworkKey.makeNetworkKeys()

            assert keys
            assert keys.a
            assert keys.b

            // Public key should be the concatenation of the 256-bit X & Y values.
            assert keys.a.value.length == 64

            // Private key should be a Yann-256 value.
            assert keys.b.value.length == 64

			//slice up the private key and see if the hash is right
			//byte[] full = keys.b.value

			//decipher
			// Generate the secret key specs.
			byte[] raw = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
			SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");

			// Instantiate the cipher
			Cipher cipher = Cipher.getInstance("AES/ECB/NOPADDING");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec);
			byte[] full = cipher.doFinal( keys.b.value);

			byte[] pKey = full[0..31]
			byte[] hash = full[32..63]

			MessageDigest md = MessageDigest.getInstance("SHA-256")
			md.update(pKey)
			def privateHash = md.digest()

			assert privateHash == hash


        }
    }
}
