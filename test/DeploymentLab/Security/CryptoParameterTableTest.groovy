package DeploymentLab.Security;

import org.junit.Test
import java.security.spec.AlgorithmParameterSpec
import java.security.spec.ECParameterSpec
import static DeploymentLab.TestUtils.shouldFail

/**
 * Test cases for the CryptoParameterTable class.
 *
 * @author Ken Scoggins
 * @since  Jupiter
 */
public class CryptoParameterTableTest {

    private static final String  VALID_ALGORITHM   = "secp256k1"
    private static final String  INVALID_ALGORITHM = "bad-algo"


    /**
     * Tests the fromHex method with a valid value.
     */
    @Test
    void testFromHexValid() {
        assert CryptoParameterTable.fromHex("79").intValue() == 121
    }

    /**
     * Tests the fromHex method with an invalid value.
     */
    @Test
    void testFromHexInvalid() {
        // An odd number of characters is not accepted.
        shouldFail( IllegalArgumentException ) { CryptoParameterTable.fromHex("79B") }
    }

    /**
     * Tests the getByName method with a valid algorithm name.
     */
    @Test
    void testGetByNameValid() {
        AlgorithmParameterSpec spec = CryptoParameterTable.getByName( VALID_ALGORITHM )

        assert spec
        assert spec instanceof ECParameterSpec
    }

    /**
     * Tests the getByName method with a invalid algorithm name.
     */
    @Test
    void testGetByNameInvalid() {
        shouldFail( IllegalArgumentException ) { CryptoParameterTable.getByName( INVALID_ALGORITHM ) }
    }
}
