package DeploymentLab

import DeploymentLab.Image.ARGBConverter
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Tree.ConfigurationReplacerTree
import DeploymentLab.Tree.ConfigurationReplacerTreeModel
import DeploymentLab.channellogger.ChannelListener
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.channellogger.Logger
import javax.swing.JFrame
import org.apache.commons.io.FileUtils
import org.junit.After
import org.junit.Before
import org.junit.Test
import DeploymentLab.Image.DLImage
import DeploymentLab.channellogger.LogLevel

/**
 *
 * @author Gabriel Helman
 * @since
 * Date: 2/6/12
 * Time: 11:33 AM
 */
class ValidatorTest {


	static ObjectModel omodel
	static ComponentModel cmodel

	static SelectModel selectModel
	static UndoBuffer ub

	static UIDisplay uiDisplay
	static ConfigurationReplacerTree tree

	boolean lognow = false
	private Logger log = Logger.getLogger(ValidatorTest.class.getName())

	Validator v

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()
		ub = CentralCatalogue.getUndoBuffer()

		def treeModels = []
		def cmtm = new ConfigurationReplacerTreeModel(cmodel, "FakeTree")
		treeModels.add(cmtm)
		tree = new ConfigurationReplacerTree(cmtm)

		//trees go in clib
		def clib = ComponentLibrary.INSTANCE;
		clib.init(cmodel, null, treeModels);

		v = new Validator(omodel, cmodel, ProjectSettings.getInstance())

		//fake in a stdout logger
		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener({logData -> if (lognow && logData.level.ordinal() <= LogLevel.WARN.ordinal()) {print logData.toString()} } as ChannelListener)
		lognow = false
	}

	@After
	void tearDown() {
		cmodel = null
		omodel = null
		selectModel = null
		ub = null
		uiDisplay = null
		tree = null
		v = null
		//have fun, garbage collector!
	}

	def validateAndReport( String testName ) {
		def result = v.validate()

		if( result != Validator.RESULT_PASS ) {
			println "--- ${this.class.name}.$testName"
			v.getFailures().each { println "\tERROR  : $it" }
			v.getWarnings().each { println "\tWARNING: $it" }
			v.getInfos().each    { println "\tINFO   : $it" }
		}
		return result
	}

	@Test
	void doTheseStubsWork() {
		println v.toString()
	}

	/**
	 * Checks that the validation suite will run without throwing an exception
	 */
	@Test
	void validateLintFile(){
		TestUtils.loadProjectFile( TestUtils.CURRENT_VERSION_LINT, ub, omodel, cmodel )
		lognow = true
		assert validateAndReport("validateLintFile") != Validator.RESULT_ABORT
	}

	/**
	 * Checks validations fixed in Bug 8947 related to associations and proxies.
	 */
	@Test
	void validateBug8947() {
		TestUtils.loadProjectFile("build/testFiles/Bug8947 - Proxy Assoc Validations.dlz", ub, omodel, cmodel )
		lognow = true
		assert validateAndReport("validateBug8947") < Validator.RESULT_FAIL
	}
}
