package DeploymentLab

import DeploymentLab.channellogger.ChannelListener
import DeploymentLab.channellogger.LogData
import DeploymentLab.channellogger.LogLevel

/**
 * Test utility to freak out if a certain log level happens.
 * @author Gabriel Helman
 * @since
 * Date: 7/10/12
 * Time: 11:37 AM
 */
class ProblemLogger implements ChannelListener {

	private LogLevel levelICareAbout

	public ProblemLogger(LogLevel l) {
		this.levelICareAbout = l
	}

	@Override
	void messageReceived(LogData l) {
		//if(l.level == this.levelICareAbout){
		//	println "FAILURE: Log Message of level ${l.level}!"
		//	println l.message
		//}
		assert l.level != this.levelICareAbout
	}
}
