package DeploymentLab

import DeploymentLab.Model.ComponentType
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.Tools.ToolManager

import javax.swing.ButtonGroup

import static org.junit.Assert.assertEquals

import java.util.regex.Pattern

import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ObjectModel
import DeploymentLab.channellogger.ChannelListener
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.channellogger.LogLevel
import DeploymentLab.Model.ModelState
import DeploymentLab.Model.DLComponent
import DeploymentLab.Tools.ConfigurationReplacer

/**
 * Common utilities useful in unit tests.
 */
class TestUtils {
    
    // Reference to GroovyTestCase.shouldFail so we don't need to extend it.
    private static final SHOULD_FAIL_REF = new GroovyTestCase().&shouldFail

    // Default relative error for comparing floating point values.
    private static final double EPSILON = 0.00001;

    // Slashy mess and replacement pattern to find the last "cfg/" in a path and add and "ss/".
    private static final Pattern CFG_PATTERN = Pattern.compile('(((/|\\\\)+|^)cfg)(/|\\\\)+(\\w*\\.?\\w+(/|\\\\)*$)')
    private static final String CFG_SS_DIR   = '$1$4ss$4$5'

	// These are controlinputs now. Some tests we don't want both types, so the no_strat version is for temp only use.
	public static final String DEFAULT_RACK = "rack-control-rearexhaust-sf-thermanode2"
	public static final String DEFAULT_RACK_NO_STRAT = "ez-rack-1-1"

	public static final String CURRENT_VERSION_LINT = "build/testFiles/Lint_7.0_Aireo.dlz"


    /**
     * Helper method for comparing floating point values. This uses a relative error rather than a constant epsilon.
     *
     * @param message   Optional identifying message.
     * @param expected  Expected value to compare.
     * @param actual    Actual value to compare.
     * @param epsilon   Optional relative error. If not supplied, {@link #EPSILON} is used.
     */
    static void assertAlmostEquals( String message = null, double expected, double actual, double epsilon = EPSILON ) {
        if( message ) {
            assertEquals( message, expected, actual, (epsilon * expected) )
        } else {
            assertEquals( expected, actual, (epsilon * expected) )
        }
    }

    /**
     * Helper method for asserting that a test should result in an exception. This is mostly just a hack work-around
     * for Groovy's {@link GroovyTestCase#shouldFail} since it doesn't have one that accepts a message. So in some
     * cases, you don't have enough info to really know what exactly failed. This isn't a great solution, but at least
     * will display a message so you can track down what failed a little easier.
     *
     * @param message   Identifying message.
     * @param clazz     Expected exception class.
     * @param code      Code to be tested.
     *
     * @return The message of the throwable.
     */
    @SuppressWarnings('Println')
    static String shouldFail( def message, Class clazz, Closure code ) {
        try {
            code.call()
            println "Failed shouldFail: $message"
        } catch( Exception e ){
            if( !clazz.isInstance( e ) ) {
                println "Failed shouldFail: $message"
            }
        }
        return SHOULD_FAIL_REF( clazz, code )
    }

    /**
     * Just a convenience wrapper to our shouldFail reference.
     * @param clazz  Expected exception class.
     * @param code   Code to be tested.
     *
     * @return  The message of the throwable.
     */
    static String shouldFail( Class clazz, Closure code ) {
        return SHOULD_FAIL_REF( clazz, code )
    }

    /**
     * Registers a channel listener to print logger messages to the terminal.
     *
     * @param logLevel  Logging threshold.
     */
    @SuppressWarnings('Println')
    static void enableLogging( LogLevel logLevel ) {
        ChannelManager.instance.addChannelListener({ logData ->
                                                       if( logLevel.ordinal() >= logData.level.ordinal() ) {
                                                           print logData.toString()
                                                       }
                                                   } as ChannelListener )
    }

	/**
	 * Creates the Object Model, Component Model, Select Model and Undo Buffer.  These are not set as the app-wide
	 * instances and are only returned.
	 *
	 * @return Array of [ ObjectModel, ComponentModel, SelectModel, UndoBuffer ]
	 */
	static def initSideModels() {
		ObjectModel omodel = loadObjectModel()
		ComponentModel cmodel = loadComponentModel( omodel )

		SelectModel selectModel = new SelectModel()
		cmodel.addModelChangeListener(selectModel)

		UndoBuffer undoBuffer = new UndoBuffer( cmodel, selectModel )
		// NEEDED?		ub.setParent(new JFrame())

		//groovy stubs to fake out the progress manager
		ProgressManager.metaClass.static.doTask = {c -> c.call()}
		ProgressManager.metaClass.static.bump = {}
		ProgressManager.metaClass.static.setMaxProgress = {}

		return [ omodel, cmodel, selectModel, undoBuffer ]
	}

	/**
	 * Creates the Object Model, Component Model, Select Model and Undo Buffer.  The Central Catalogue is updated with
	 * the instance so that the rest of the system can get them.  Items not stored in the catalog are returned.
	 *
	 * @param populateWithBasics  Creates some basic core components if true. Otherwise, an empty model is created.
	 * @return Array of [ ObjectModel, ComponentModel ]
	 */
	static def initModels( boolean populateWithBasics = true ) {
		def( omodel, cmodel, selectModel, undoBuffer ) = initSideModels()

		// Make these the app-wide models.
		CentralCatalogue.setDeploymentPanel( null )
		CentralCatalogue.setSelectModel( selectModel )
		CentralCatalogue.setUndoBuffer( undoBuffer )

		// Due to the legacy defaultmodel stuff, many tests expect there to be a DRAWING after this is called.
		if( populateWithBasics ) {
			def c = cmodel.newComponent( ComponentType.DRAWING )
		}

		return [ omodel, cmodel ]
	}

    /**
     * Returns a new instance of the Object Model created by loading from the common project configuration.
     *
     * @return  An instance of the Object Model.
     */
	static ObjectModel loadObjectModel() {

        def xml = ComponentLibrary.parseXmlFile( CentralCatalogue.getApp('objects.definitions.main').replaceFirst( CFG_PATTERN, CFG_SS_DIR ) )

        ObjectModel objectModel = new ObjectModel( xml )

        CentralCatalogue.getApp('objects.definitions.extra').split(',').each {
            def name = it.trim().replaceFirst( CFG_PATTERN, CFG_SS_DIR )
            objectModel.addTypes( ComponentLibrary.parseXmlFile(name) )
        }

        return objectModel
    }

    /**
     * Returns a new instance of the Componenent Model created by loading from the common project configuration.
     *
     * @param objectModel   Optional Object Model. A new one will automatically be loaded if this is not provided.
     *
     * @return  An instance of the Componenent Model.
     */
    static ComponentModel loadComponentModel( ObjectModel objectModel = null ) {

        // Auto-load the model if one was not provided.
        objectModel = objectModel ?: loadObjectModel()

        // Load the common components required by the application.
        def commonCompXml = ComponentLibrary.parseXmlFile('cfg/ss/commoncomponents.xml')
        ComponentModel componentModel = new ComponentModel( objectModel, commonCompXml )
        componentModel.setCLib( ComponentLibrary.INSTANCE )
        componentModel.metamorphosisStarting( ModelState.INITIALIZING )

        // Load the optional components.
        def compDirName = CentralCatalogue.getComp('components.path').replaceFirst( CFG_PATTERN, CFG_SS_DIR )
		//println "compDirName = $compDirName"
        new File( compDirName ).listFiles().each {
            if( it.absolutePath.endsWith('.xml') ) {
                def compXml = ComponentLibrary.parseXmlFile( it.absolutePath )
                compXml.component.each{ xml->
                    componentModel.loadComponent( xml )
                }
            }
        }

        // Load the compatibility components.
        //def bcCompName = CentralCatalogue.getComp('components.backcompatability.path').replaceFirst( CFG_PATTERN, CFG_SS_DIR )
		def bcCompName = CentralCatalogue.getComp('components.backcompatability.path').replace( "cfg", "build/cfg" )
		//println "bcCompName = $bcCompName"
        new File( bcCompName ).listFiles().each{
			//println "loading backcompat ${it.getAbsolutePath()}"
			if( it.absolutePath.endsWith('.xml') ) {
				def compXml = ComponentLibrary.parseXmlFile( it.absolutePath )
				componentModel.loadComponent( compXml )

			}
        }

        componentModel.metamorphosisFinished( ModelState.INITIALIZING )

        return componentModel
    }

	public static DLProject loadProjectFile( String projectFile, ObjectModel objectModel, ComponentModel componentModel, ConfigurationReplacer cr = null ) {
		return loadProjectFile( projectFile, CentralCatalogue.getUndoBuffer(), objectModel, componentModel, cr )
	}

	public static DLProject loadProjectFile( String projectFile, UndoBuffer undoBuffer, ObjectModel objectModel, ComponentModel componentModel, ConfigurationReplacer cr = null ) {
		DLProject project = null
		try{
			// Need a DeploymentPanel for DLProject. Create one if needed.
			if( CentralCatalogue.getDeploymentPanel() == null ) {
				DeploymentPanel dp = new DeploymentPanel( CentralCatalogue.getSelectModel(),
				                                          undoBuffer,
				                                          componentModel,
				                                          new ToolManager(new ButtonGroup()),
				                                          new DisplayProperties( CentralCatalogue.getSelectModel() ) )
				CentralCatalogue.setDeploymentPanel( dp )
			}

			File dlz = new File( projectFile )

			int origNumObjs  = objectModel.getObjectCount()
			int origNumComps = componentModel.getComponents().size()

			// Load the file.
			project = new DLProject( dlz, undoBuffer, cr, componentModel, objectModel )

			assert objectModel.getObjectCount() > origNumObjs
			assert componentModel.getComponents().size() > origNumComps

			//images loaded in DLProject constructor
			for( DLComponent drawing : componentModel.getComponentsByType("drawing") ) {
				// As of 7.0, blank backgrounds are now a non-image canvas, so they don't have a saved image.
				if( drawing.getPropertyValue('hasImage') ) {
					assert drawing.getPropertyValue("image_data") != null
				}
			}
			componentModel.metamorphosisFinished( ModelState.INITIALIZING )
		} finally {
			if(project){
				project.unlockProjectFile()
			}
		}
		return project
	}

    public static void setLocation( DLComponent c, double x, double y ) {
        c.setPropertyValue("x", x)
        c.setPropertyValue("y", y)
    }

    public static void whereIsEverybody( ComponentModel cmodel, String label = null ) {
        if( label ) println "\n----- ${label.toUpperCase()} -----"
        cmodel.getComponentsInRole('drawing').each { drawing ->
            println "\nDrawing: \"${drawing.name}\""
            cmodel.getComponentsInDrawing( drawing ).each{ c ->
                println "       $c (${c.getPropertyValue("x")},${c.getPropertyValue("y")})"
            }
        }
        println ""
    }

	/**
	 * Creates a new component as a descendant of the given drawing. If the component must belong to a Group and/or
	 * Data Source, it will be childed to a Group and Data Source that is a child of the drawing. If desired, an
	 * existing Group and Data Source will be used if one exists. Otherwise, new ones will be created.
	 *
	 * @param cmodel       The Component Model for these components.
	 * @param compType     The type of new component to create.
	 * @param drawing           The drawing the new component will belong to.
	 * @param useExisting  If true, use existing Groups and Data Sources rather than creating new ones.
	 *
	 * @return  The new Component, properly childed to the drawing or a required Group and Drawing.
	 */
	public static DLComponent newComponentWithParents( ComponentModel cmodel, String compType, DLComponent drawing, boolean useExisting = true ) {

		// Handle name migrations since sime tests may be creating a new component based on an old loaded project... and that old project is necessary for the test.
		if( CentralCatalogue.hasComponentConversion( compType ) ) {
			compType = CentralCatalogue.getComponentConversion( compType )
		}

		DLComponent newComp = cmodel.newComponent( compType )

		// Child to a ROOM, if it can be. Otherwise, find another group.
		DLComponent parent = drawing

		List<String> groupTypes
		if( ( compType != ComponentType.ROOM ) && cmodel.typeCanBeChild( ComponentType.ROOM, compType ) ) {
			groupTypes = [ ComponentType.ROOM ]
		} else {
			groupTypes = cmodel.listGroupings( compType, true )
		}

		if( !groupTypes.isEmpty() ) {
			DLComponent group = null

			if( useExisting ) {
				// See if the Drawing already has a group we can use. Just grab any that we find.
				for( String gType : groupTypes ) {
					List<DLComponent> groupList = cmodel.getComponentsByType( gType, drawing )
					if( !groupList.isEmpty() ) {
						group = groupList.get(0)
						break
					}
				}
			}

			if( group == null ) {
				// I guess we need to create a new one! Just grab any.
				group = cmodel.newComponent( groupTypes.get(0) )
				drawing.addChild( group )
			}

			parent = group
		}

		parent.addChild( newComp )

		List<String> dsTypes = cmodel.listDatasources( compType )
		if( !dsTypes.isEmpty() ) {
			DLComponent ds = null

			if( useExisting ) {
				// See if the Drawing already has a data source we can use. Just grab any that we find.
				for( String dsType : dsTypes ) {
					List<DLComponent> dsList = cmodel.getComponentsByType( dsType, drawing )
					if( !dsList.isEmpty() ) {
						ds = dsList.get(0)
						break
					}
				}
			}

			if( ds == null ) {
				// I guess we need to create a new one! Just grab any.
				ds = cmodel.newComponent( dsTypes.get(0) )
				drawing.addChild( ds )
			}

			ds.addChild( newComp )
		}

		return newComp
	}
}
