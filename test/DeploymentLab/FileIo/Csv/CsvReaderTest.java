package DeploymentLab.FileIo.Csv;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.NoSuchElementException;

import junit.framework.Assert;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import DeploymentLab.FileIo.BadHeaderRowException;
import DeploymentLab.FileIo.FlatFileReaderException;
import DeploymentLab.FileIo.FlatFileRow;

public class CsvReaderTest {

	@Test
	public void readDataWithDefaultDelimiter() throws FlatFileReaderException, IOException {
		checkTrivialFile(new CsvReader("test/testFiles/trivial.txt"));
	}

	@Test
	public void readDataWithSemicolonDelimiter() throws FlatFileReaderException, IOException {
		checkTrivialFile(new CsvReader("test/testFiles/semicolon.txt", ";"));
	}

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void readDoublesIntegers() throws FlatFileReaderException, IOException {
		thrown.expect(NumberFormatException.class);
		CsvReader importer = new CsvReader("test/testFiles/trivialNum.txt");

		FlatFileRow row = importer.nextRow();

		Assert.assertEquals(new Integer(20), row.getIntegerValue("Header1"));
		Assert.assertEquals(25.4, row.getDoubleValue("Header2"));
		Assert.assertNull(row.getDoubleValue("Header3"));
		Assert.assertEquals("string", row.getStringValue("Header4"));

		row = importer.nextRow();

		Assert.assertEquals(20, row.getIntegerValue("Header1").intValue());
		// throws exception
		Assert.assertEquals(25, row.getIntegerValue("Header2").intValue());
	}

	@Test
	public void fileDoesntExist() throws FlatFileReaderException, IOException {
		thrown.expect(FileNotFoundException.class);
		new CsvReader("test/testFiles/nofile.txt");
	}

	@Test
	public void throwsExceptionFileIsEmpty() throws FlatFileReaderException, IOException {
		thrown.expect(BadHeaderRowException.class);
		new CsvReader("test/testFiles/empty.txt");
	}

	@Test
	public void throwsNoSuchElement() throws FlatFileReaderException, IOException {
		thrown.expect(NoSuchElementException.class);
		CsvReader reader = new CsvReader("test/testFiles/trivial.txt");
		while (reader.nextRow() != null)
			;
	}

	private void checkTrivialFile(CsvReader importer) throws FlatFileReaderException {
		FlatFileRow row = importer.nextRow();

		Assert.assertEquals("Data1", row.getStringValue("Header1"));
		Assert.assertEquals("Data2", row.getStringValue("Header2"));
		Assert.assertEquals("Data3", row.getStringValue("Header3"));
		Assert.assertEquals("Data4", row.getStringValue("Header4"));

		// only one row
		Assert.assertFalse(importer.hasMoreRows());
	}
}
