package DeploymentLab.FileIo.Excel;

import java.io.FileNotFoundException;
import java.io.IOException;

import junit.framework.Assert;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import DeploymentLab.FileIo.BadHeaderRowException;
import DeploymentLab.FileIo.FlatFileReaderException;
import DeploymentLab.FileIo.FlatFileRow;

public class ExcelReaderTest {

	@Test
	public void readStringDataWithDefaultHeaderRow() throws FlatFileReaderException, IOException {
		ExcelReader importer = new ExcelReader("test/testFiles/trivial.xlsx");
		FlatFileRow row = importer.nextRow();

		Assert.assertEquals("Data1", row.getStringValue("Header1"));
		Assert.assertEquals("Data2", row.getStringValue("Header2"));
		Assert.assertEquals("Data3", row.getStringValue("Header3"));
		Assert.assertEquals("Data4", row.getStringValue("Header4"));

		// only one row
		Assert.assertFalse(importer.hasMoreRows());
	}

	@Test
	public void readNumbericValuesFromSecondSheetAndFifthRow() throws FlatFileReaderException, IOException {
		ExcelReader importer = new ExcelReader("test/testFiles/trivial.xlsx", 2, 6);

		FlatFileRow row1 = importer.nextRow();
		Assert.assertEquals(12.0, row1.getDoubleValue("Num1"));
		Assert.assertEquals("dozen", row1.getStringValue("String1"));
		Assert.assertEquals(34.5, row1.getDoubleValue("Num2"));
		Assert.assertEquals("thirty four", row1.getStringValue("String2"));

		FlatFileRow row2 = importer.nextRow();
		Assert.assertEquals(13.0, row2.getDoubleValue("Num1"));
		Assert.assertNull(row2.getStringValue("String1"));
		Assert.assertNull(row2.getDoubleValue("Num2"));
		Assert.assertEquals("null", row2.getStringValue("String2"));

		// only one row
		Assert.assertFalse(importer.hasMoreRows());
	}

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void readIntegers() throws FlatFileReaderException, IOException {
		thrown.expect(NumberFormatException.class);
		ExcelReader importer = new ExcelReader("test/testFiles/trivial.xlsx", 3, 1);

		FlatFileRow row1 = importer.nextRow();
		Assert.assertEquals(3, row1.getIntegerValue("Int1").intValue());
		Assert.assertEquals(0, row1.getIntegerValue("Int2").intValue());
		Assert.assertEquals(-1, row1.getDoubleValue("Int3").intValue());

		// only one row
		Assert.assertFalse(importer.hasMoreRows());
		Assert.assertEquals(1, row1.getIntegerValue("Int4").intValue());
	}

	@Test
	public void throwsBadHeaderRowException() throws FlatFileReaderException, IOException {
		thrown.expect(BadHeaderRowException.class);
		new ExcelReader("test/testFiles/trivial.xlsx", 2, 21);
	}

	@Test
	public void fileDoesntExist() throws FlatFileReaderException, IOException {
		thrown.expect(FileNotFoundException.class);
		new ExcelReader("test/testFiles/notexist.xlsx", 2, 21);
	}

	@Test
	public void throwsBadSheetNumberException() throws FlatFileReaderException, IOException {
		thrown.expect(BadSheetNumberException.class);
		new ExcelReader("test/testFiles/trivial.xlsx", 11, 2);
	}

	@Test
	public void readMultipleSheetWorkbook() throws IOException, FlatFileReaderException {
		ExcelSheetReader reader = new ExcelSheetReader("test/testFiles/sheets.xlsx") ;
		ExcelReader sheet = reader.nextSheet();
		FlatFileRow row = sheet.nextRow();

		System.out.println(row.toString());
		Assert.assertEquals(-11, row.getIntegerValue("Header11").intValue());
		Assert.assertEquals("hello", row.getStringValue("Header12"));
		Assert.assertEquals("1ff1", row.getStringValue("Header13"));

		sheet = reader.nextSheet();
		row = sheet.nextRow();

		Assert.assertEquals(42, row.getIntegerValue("Header21").intValue());
		Assert.assertNull(row.getStringValue("Header22"));
		Assert.assertEquals("NO", row.getStringValue("Header23"));

		sheet = reader.nextSheet();
		row = sheet.nextRow();

		Assert.assertEquals("Test", row.getStringValue("Header31"));
		Assert.assertEquals("is", row.getStringValue("Header32"));
		Assert.assertEquals("nice!", row.getStringValue("Header33"));

	}
}
