package DeploymentLab;


import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import DeploymentLab.Model.Dlid
import groovy.util.slurpersupport.GPathResult

/**
 * @author Ken Scoggins
 * @since Jupiter
 */
public class ProjectSettingsTest {

    private ProjectSettings ps

    /**
     * One-time setup called before all tests run.
     */
    @BeforeClass()
    static void classSetup() {

    }

    /**
     * Setup called before each test.
     */
    @Before
    void setup() {
        ps = ProjectSettings.getInstance()
    }

    /**
     * Cleanup called after each test.
     */
    @After
    void tearDown() {

    }

    /**
     * One-time cleanup called after all tests run.
     */
    @AfterClass()
    static void classTearDown() {

    }

    /**
     * Tests the getInstance method.
     */
    @Test
    void testGetInstance() {
        assert ps != null
        assert ps == ProjectSettings.getInstance()
    }

    /**
     * Tests the create method.
     */
    @Test
    void testCreate() {
        ps.create()

        assert ps.editHistory != null
        //assert ps.editHistory.size() == 1  Code looks like it should be this, but reset is called right after the entry is added :/
        //assert ps.editHistory.iterator().next().get('upgrade') == 'created'
        assert ps.editHistory.size() == 0
        
        assert ps.esHost == 'localhost'
        assert ps.acHost == ''
        assert ps.activeDrawing == ''
        assert ps.unitSystem == 'Imperial US'
        assert ps.refreshAllLinks
        assert ps.refreshAllRules
        assert ps.refreshAllObjects
        assert ps.hasBeenUpgraded
        assert !ps.currentExport
        assert ps.currentUpgrade.contains('created')
        assert ps.currentAnnotation == ''

        assert ps.editHistory != null
        assert ps.editHistory.size() == 0

        assert ps.configuredDefaults != null
        assert ps.configuredDefaults.size() == 0

        assert ps.configuredSimulator != null
        assert ps.configuredSimulator.size() == 0

        assert !ps.needSave()
    }

    /**
     * Tests the DLID Map.
     */
    @Test
    void testDlidMap() {
        // Create some entries and add them.
        List<Dlid> oldIds = []
        List<Dlid> newIds = []
        for( i in 1..10 ) {
            def oid = Dlid.getDlid()
            def nid = Dlid.getDlid()
            ps.replaceDlid( oid, nid )
            oldIds += oid
            newIds += nid
        }

        // Do some lookups
        for( i in 0..(oldIds.size()-1) ) {
            // Direct lookup New->Old and Old->New.
            assert ps.findOldDlid( newIds.get(i) ) == oldIds.get(i)
            assert ps.findNewDlid( oldIds.get(i) ) == newIds.get(i)

            // Should only exist on one side of the map.
            assert ps.findOldDlid( oldIds.get(i) ) == null
            assert ps.findNewDlid( newIds.get(i) ) == null

            // Should be found, regardless of the side it is on.
            assert ps.findOtherDlid( newIds.get(i) ) == oldIds.get(i)
            assert ps.findOtherDlid( oldIds.get(i) ) == newIds.get(i)
        }

        // Change one again.
        Dlid newNewId = Dlid.getDlid()
        ps.replaceDlid( newIds.get(0), newNewId )

        assert ps.findOldDlid( newNewId ) == newIds.get(0)
        assert ps.findNewDlid( newIds.get(0) ) == newNewId

        // Also make sure that the previous entry was removed.
        assert ps.findOldDlid( newIds.get(0) ) == null
        assert ps.findOtherDlid( oldIds.get(0) ) == null
    }

    /**
     * Tests setting some values, save, reset then load the saved settings.
     */
    @Test
    void testChangeSaveResetLoad() {
        ps.create()

        // Change settings that are directly accessed.
        ps.esHost = "SomeEsHost"
        ps.acHost = "SomeAcHost"
        ps.unitSystem = "Galactic Standard Notation"
        ps.activeDrawing = Dlid.getDlid().toString()

        // Change settings that have methods that manipulate them.
        ps.edit("myFile")
        ps.upgrade("Update: 1.0 to 2.0")
        ps.exported()
        ps.addAnnotation("My Very Eager Mother Just Saw Nine Unique Planets - IAU can go to hell")

        ps.smartZoneHost = "1.2.3.4"
        ps.smartZoneHost = "8675309"

        for( i in 1..10 ) {
            ps.replaceDlid( Dlid.getDlid(), Dlid.getDlid() )
        }

        // Save it.
        String origSettings = ps.toString()
        StringWriter xmlStr = new StringWriter()
        def builder = new groovy.xml.MarkupBuilder( xmlStr )
        builder.model('version': '1.0') {
            ps.save( builder )
        }

        // Reset everything.
        ps.reset()
        assert ps.esHost == 'localhost'
        assert ps.acHost == ''
        assert ps.activeDrawing == ''
        assert ps.unitSystem == 'Imperial US'
        assert ps.refreshAllLinks
        assert ps.refreshAllRules
        assert ps.refreshAllObjects
        assert !ps.hasBeenUpgraded
        assert !ps.currentExport
        assert ps.currentUpgrade == ''
        assert ps.currentAnnotation == ''
        assert ps.editHistory.size() == 0
        assert ps.configuredDefaults.size() == 0
        assert ps.configuredSimulator.size() == 0

        assert ps.smartZoneHost == ""
        assert ps.smartZonePort == ""

        // Reload.
        GPathResult xml = new XmlSlurper().parseText( xmlStr.toString() )
        assert xml != null
        assert !xml.isEmpty()
        
        ps.load( xml )
        assert ps.toString() == origSettings

    }
}
