package DeploymentLab.Image

import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import java.awt.Dimension
import java.awt.image.BufferedImage
import java.awt.Rectangle


/**
 * Tests the various creation methods for the DLImage class. It also verifies that each creation method properly caches
 * and manages the memory as described in the DLImage class Javadoc.
 *
 * @author Ken Scoggins
 * @since Jupiter
 */
public class DLImageTest {

    private static File      IMAGE_FILE = new File('build/testFiles/validations/DLID_1425.png')
    private static Dimension IMAGE_SIZE = new Dimension( 1005, 505 )

    /**
     * One-time setup called before all tests run.
     */
    @BeforeClass()
    static void classSetup() {
        assert IMAGE_FILE.exists()
    }

    /**
     * Setup called before each test.
     */
    @Before
    void setup() {

    }

    /**
     * Cleanup called after each test.
     */
    @After
    void tearDown() {

    }

    /**
     * One-time cleanup called after all tests run.
     */
    @AfterClass()
    static void classTearDown() {

    }

    /**
     * This test makes sure the memory management aspects work correctly.
     */
    void memoryTest( DLImage image ) {

        BufferedImage bi = image.getImage()
        assert bi

        // We grabbed a reference, so it better be in memory!
        assert image.isInMemory()

        assert image.getHeight() == IMAGE_SIZE.getHeight()
        assert image.getWidth() == IMAGE_SIZE.getWidth()

        // Dump the hard ref and use up the memory so it gets flushed.
        bi = null
        ArrayList<Object[]> memorySuck = new ArrayList<>()
        OutOfMemoryError fail = null
        try {
			def rt = Runtime.getRuntime()
			long totalBytes = rt.maxMemory()

            // Java 8 seems to no longer allow arrays larger than MAX_INT? Get NegativeArraySizeException. So keep creating
            // until fail. Some claim the safe limit is MAX-2 or 5 or 8. So just in case this isn't B.S., we'll do that.
            while( totalBytes >= 0 ) {
                totalBytes -= (Integer.MAX_VALUE - 8)
                memorySuck.add( new byte[Integer.MAX_VALUE - 8] )
            }
        } catch( OutOfMemoryError e ) {
            fail = e
        }
        assert fail

        // Should not be in memory.
        assert ! image.isInMemory()

        // Flush our memory hoarder.
        memorySuck = null
        System.gc()
        System.gc()

        // Reference the image again and make sure it is reloaded.
        bi = image.getImage()
        assert bi
        assert image.isInMemory()
        assert image.getHeight() == IMAGE_SIZE.getHeight()
        assert image.getWidth() == IMAGE_SIZE.getWidth()
    }


    /**
     * Tests a DLImage created from an image file.
     */
    @Test
    void testFromFile() {
        // Load from a file.
        DLImage image = new DLImage( IMAGE_FILE )
        assert image

        // The image should be in memory since we load it immediately so we can detect bad images during load.
        assert image.isInMemory()

        // Make sure an image loaded from a file is correctly cached and managed.
        memoryTest( image )

        println "DLImage Test: Using ${IMAGE_FILE.getAbsolutePath()}:\n  ${image}"
    }


    /**
     * Tests a DLImage created from an existing BufferedImage.
     */
    @Test
    void testFromImage() {
        // Just use the known file image since we've already tested it.
        DLImage refImage = new DLImage( IMAGE_FILE )
        assert refImage

        DLImage image = new DLImage( refImage.getImage() )
        assert image

        // Should be in memory since the new image was constructed from an image that was already in memory.
        assert image.isInMemory()

        // Make sure an image created this way is correctly cached and managed.
        memoryTest( image )
    }


    /**
     * Tests a DLImage created from an existing BufferedImage.
     */
    @Test
    void testFromScratch() {
        // Just use the known file image since we've already tested it.
        DLImage image = new DLImage( (int)IMAGE_SIZE.getWidth(), (int)IMAGE_SIZE.getHeight(), BufferedImage.TYPE_INT_ARGB )
        assert image

        // Should be in memory since it is a dynamic creation from scratch.
        assert image.isInMemory()

        assert image.getType() == BufferedImage.TYPE_INT_ARGB

        // Make sure an image created this way is correctly cached and managed.
        memoryTest( image )
    }


    /**
     * Makes sure that the overloaded BufferedImage methods properly wrap the image. It does not test the BufferedImage
     * methods themselves, as it is expected that these work. This test only checks that the wrapped method returns the
     * same thing.
     */
    @Test
    void testOverloadedMethods() {
        DLImage dlImage = new DLImage( IMAGE_FILE )
        assert dlImage

        BufferedImage bufImage = dlImage.getImage()
        assert bufImage

        // NOTE 1: BufferedImage ignores the observer args anyway, so NULL is used wherevr they are needed.
        // NOTE 2: The methods with void returns are still here for code coverage pandering :^) I guess it would also
        //         catch where the DLImage version changes its signature without warning. Yeah, that's it.
        assert dlImage.getWidth( null ) == bufImage.getWidth( null )
        assert dlImage.getHeight( null ) == bufImage.getHeight( null )
        assert dlImage.getSource() == bufImage.getSource()
        assert dlImage.addTileObserver( null ) == bufImage.addTileObserver( null )
        assert dlImage.removeTileObserver( null ) == bufImage.removeTileObserver( null )
        assert dlImage.getWritableTile(0,0) == bufImage.getWritableTile(0,0)
        assert dlImage.releaseWritableTile(0,0) == bufImage.releaseWritableTile(0,0)
        assert dlImage.isTileWritable(0,0) == bufImage.isTileWritable(0,0)
        assert dlImage.getWritableTileIndices() == bufImage.getWritableTileIndices()
        assert dlImage.hasTileWriters() == bufImage.hasTileWriters()
        assert dlImage.setData( dlImage.getData() ) == bufImage.setData( bufImage.getData() )
        assert dlImage.getSources() == bufImage.getSources()
        assert dlImage.getColorModel() == bufImage.getColorModel()
        assert dlImage.getSampleModel() == bufImage.getSampleModel()
        assert dlImage.getWidth() == bufImage.getWidth()
        assert dlImage.getHeight() == bufImage.getHeight()
        assert dlImage.getMinX() == bufImage.getMinX()
        assert dlImage.getMinY() == bufImage.getMinY()
        assert dlImage.getNumXTiles() == bufImage.getNumXTiles()
        assert dlImage.getNumYTiles() == bufImage.getNumYTiles()
        assert dlImage.getMinTileX() == bufImage.getMinTileX()
        assert dlImage.getMinTileY() == bufImage.getMinTileY()
        assert dlImage.getTileWidth() == bufImage.getTileWidth()
        assert dlImage.getTileHeight() == bufImage.getTileHeight()
        assert dlImage.getTileGridXOffset() == bufImage.getTileGridXOffset()
        assert dlImage.getTileGridYOffset() == bufImage.getTileGridYOffset()
        assert dlImage.getTile(0,0) == bufImage.getTile(0,0)
        assert dlImage.copyData( dlImage.getWritableTile(0,0) ) == bufImage.copyData( bufImage.getWritableTile(0,0) )
        assert dlImage.getTransparency() == bufImage.getTransparency()

        // There are most likely no properties. Use one if there is or just verify the not-found case at least behaves the same.
        assert dlImage.getPropertyNames() == bufImage.getPropertyNames()

        String prop = ( bufImage.getPropertyNames() != null ) ? bufImage.getPropertyNames()[0] : "foo"
        assert dlImage.getProperty( prop, null  ) == bufImage.getProperty( prop, null )
        assert dlImage.getProperty( prop ) == bufImage.getProperty( prop )

        // These return copies and the @#$! AWT classes used do not override equals, so you get identity checks. Cheat and compare the strings.
        assert dlImage.getGraphics().toString() == bufImage.getGraphics().toString()
        assert dlImage.getData().toString() == bufImage.getData().toString()
        assert dlImage.getData( new Rectangle( 10, 10 ) ).toString() == bufImage.getData( new Rectangle( 10, 10 ) ).toString()
    }
}
