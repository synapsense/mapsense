package DeploymentLab.Image

import org.junit.*;
import static org.junit.Assert.*

import edu.umd.cs.piccolo.*

import DeploymentLab.Model.*
import DeploymentLab.ComponentLibrary
import java.util.zip.ZipEntry
import DeploymentLab.StringUtil
import java.util.zip.ZipFile
import java.awt.image.BufferedImage
import DeploymentLab.CentralCatalogue
import DeploymentLab.TestUtils

/**
 *
 * Test the various factories in the Image package.  ALSO check to make sure all components have their svg / icon.
 * @author Gabriel Helman
 * @since Mars
 * Date: 6/23/11
 * Time: 11:25 AM
 */
class ImageTest {

	static ObjectModel omodel
	static ComponentModel cmodel

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
	}


	@After
	void tearDown() {
		cmodel = null
		omodel = null
		//have fun, garbage collector!
	}

	@Test
	void testShapeFactory() {
		//println "looking for shapes"
		Set<String> componentTypes = cmodel.listComponentTypes()
		for (String t: componentTypes.sort()) {

			if (!cmodel.listComponentRoles(t).contains("placeable")) {
				continue
			}
			//println "testing $t"
			DLComponent c = cmodel.newComponent(t)
			PNode node = DeploymentLab.SceneGraph.ShapeFactory.getShape(c.getShape())
			assertNotNull(node)
			assert node.getChildrenCount() > 0
			/*
			if ( node.getChildrenCount() == 0 ){
				println "$t has no children!"
			}
			*/
		}
	}

	@Test
	void testIconFactory() {
		//println "looking for small icons"
		def smallIcon = ComponentIconFactory.getDefaultIcon(ComponentIconFactory.SMALL)
		def medIcon = ComponentIconFactory.getDefaultIcon(ComponentIconFactory.MEDIUM)
		def largeIcon = ComponentIconFactory.getDefaultIcon(ComponentIconFactory.LARGE)

		def otherSmall = ComponentIconFactory.getDefaultIcon()
		assert otherSmall == smallIcon

		Set<String> componentTypes = cmodel.listComponentTypes()
		for (String t: componentTypes.sort()) {
			//non-placeable components may have icons, but we don't care
			if (!cmodel.listComponentRoles(t).contains("placeable")) {
				continue
			}

			println "testing $t"
			DLComponent c = cmodel.newComponent(t)

			def otherS = ComponentIconFactory.getComponentIcon(c)
			def s = ComponentIconFactory.getComponentIcon(c, ComponentIconFactory.SMALL)
			def m = ComponentIconFactory.getComponentIcon(c, ComponentIconFactory.MEDIUM)
			def l = ComponentIconFactory.getComponentIcon(c, ComponentIconFactory.LARGE)

			def shapeS = ComponentIconFactory.getShapeIcon(c.getShape())
			def shapeM = ComponentIconFactory.getShapeIcon(c.getShape(), ComponentIconFactory.MEDIUM)
			def shapeL = ComponentIconFactory.getShapeIcon(c.getShape(), ComponentIconFactory.LARGE)

			if (s == smallIcon) {
				println "$t has no small icon!"
				continue
			}

			//assert that none of the icons are the default icon, which means we have one missing
			if( m == medIcon){
				println "$t has no medium icon!"
			}
			if( l == largeIcon){
				println "$t has no large icon!"
			}
			assert s != smallIcon
			assert m != medIcon
			assert l != largeIcon
			assert s == otherS

			if (c.getShape() != null) {
				assert s == shapeS
				assert m == shapeM
				assert l == shapeL
			}

			if (s == smallIcon) { println "$t has no small icon!"}
			if (m == medIcon) { println "$t has no small icon!"}
			if (l == largeIcon) { println "$t has no small icon!"}
		}
	}
	
	@Test
	void defaultIcons(){
		assert ComponentIconFactory.getWarningIcon() == ComponentIconFactory.getImageIcon('/resources/warning_16.png')
		assert ComponentIconFactory.getInfoIcon() == ComponentIconFactory.getImageIcon('/resources/about_16.png')
		assert ComponentIconFactory.getDefaultIcon(ComponentIconFactory.SMALL) == ComponentIconFactory.getImageIcon("/resources/network_${ComponentIconFactory.SMALL}.png")
		assert ComponentIconFactory.getDefaultIcon(ComponentIconFactory.MEDIUM) == ComponentIconFactory.getImageIcon("/resources/network_${ComponentIconFactory.MEDIUM}.png")
		assert ComponentIconFactory.getDefaultIcon(ComponentIconFactory.LARGE) == ComponentIconFactory.getImageIcon("/resources/network_${ComponentIconFactory.LARGE}.png")
	}

	@Test
	void theAppIcon(){
		def app = ComponentIconFactory.getAppIcon()
		assert ComponentIconFactory.getAppIcon() == ComponentIconFactory.getImageIcon('/cfg/appicon.png')
		//while the default icon and the app icon are the same icon visually, they're stored as different icons on the drive
		assert ComponentIconFactory.getAppIcon() != ComponentIconFactory.getDefaultIcon(ComponentIconFactory.SMALL)
	}
	
	@Test
	void datasourceIcons(){
		Set<String> componentTypes = cmodel.listComponentTypes()
		for (String t: componentTypes.sort()) {
			if (!cmodel.listComponentRoles(t).contains("network") || cmodel.listComponentRoles(t).contains("placeable")) {
				continue
			}
			println "DS: testing $t"
			DLComponent c = cmodel.newComponent(t)
			assert ComponentIconFactory.getComponentIcon(c) != ComponentIconFactory.getDefaultIcon(ComponentIconFactory.SMALL)
		}
	}

	@Test
	void groupingIcons(){
		Set<String> componentTypes = cmodel.listComponentTypes()
		for (String t: componentTypes.sort()) {
			if( !cmodel.listComponentRoles(t).contains( ComponentRole.ZONE ) &&
			    !cmodel.listComponentRoles(t).contains( ComponentType.DRAWING ) ) {
				continue
			}
			println "GR: testing $t"
			DLComponent c = cmodel.newComponent(t)
			assert ComponentIconFactory.getComponentIcon(c) != ComponentIconFactory.getDefaultIcon(ComponentIconFactory.SMALL)
		}
	}

	@Test
	void invalidType(){
		assert ComponentIconFactory.getComponentIcon(42) == ComponentIconFactory.getDefaultIcon(ComponentIconFactory.SMALL)
		assert ComponentIconFactory.getComponentIcon( new Random() ) == ComponentIconFactory.getDefaultIcon(ComponentIconFactory.SMALL)
		assert ComponentIconFactory.getComponentIcon( {} ) == ComponentIconFactory.getDefaultIcon(ComponentIconFactory.SMALL)
		Object ob;
		ob = new Integer(13)
		assert ComponentIconFactory.getComponentIcon(ob, ComponentIconFactory.MEDIUM) == ComponentIconFactory.getDefaultIcon(ComponentIconFactory.MEDIUM)
	}

	@Test
	void gettingStrings(){
		assert ComponentIconFactory.getComponentIcon("stugg") == ComponentIconFactory.getDefaultIcon(ComponentIconFactory.SMALL)
		assert ComponentIconFactory.getComponentIcon( ComponentType.PLANNING_GROUP ) == ComponentIconFactory.getComponentIcon( cmodel.newComponent( ComponentType.PLANNING_GROUP ) )
	}
	
	@Test
	void basicIconNameProvider(){
		def provider = { return "DS_WebServices.png"}
		assert ComponentIconFactory.getComponentIcon(provider as IconNameProvider) == ComponentIconFactory.getImageIcon("/resources/DS_WebServices.png")
	}

	@Test
	void failedIconNameProvider(){
		def provider = { return "notanimage.png"}
		assert ComponentIconFactory.getComponentIcon(provider as IconNameProvider) == ComponentIconFactory.getDefaultIcon(ComponentIconFactory.SMALL)
	}

	@Test
	void nullIconNameProvider(){
		def provider = { return null }
		assert ComponentIconFactory.getComponentIcon(provider as IconNameProvider) == ComponentIconFactory.getDefaultIcon(ComponentIconFactory.SMALL)
	}


	@Test
	void shapeIconOverload(){
		assert ComponentIconFactory.getShapeIcon("arrow") ==  ComponentIconFactory.getShapeIcon("arrow", ComponentIconFactory.SMALL)
	}
	
	@Test
	void iconByName(){
		assert ComponentIconFactory.getIconByName("add") == ComponentIconFactory.getImageIcon("/resources/add.png")
	}

	@Test
	void getAnImageFromAStream(){
		assert ComponentIconFactory.getImageIconFromStream("something", null) == null
		
		ComponentLibrary.INSTANCE.init(cmodel, null, null);
		def file = new File("build/testFiles/tempus.clz")
		ZipFile zf =  null;
		def xml
		zf =  new ZipFile(file);
		for( ZipEntry e : zf.entries() ){
			if ( e.getName().equals( StringUtil.replaceExtension( file.getName() , "dcl") ) ){
				InputStream zis = zf.getInputStream(e)
				xml = new XmlSlurper().parse( zis )
				zis.close()
			}
		}
		ComponentLibrary.INSTANCE.loadCustomComponentLibrary(file, xml)

		def tempusComponent = cmodel.newComponent("tempus")
		assert ComponentIconFactory.getComponentIcon(tempusComponent, ComponentIconFactory.SMALL) != ComponentIconFactory.getDefaultIcon(ComponentIconFactory.SMALL)
		//the second time will read from the cache, not the file
		assert ComponentIconFactory.getComponentIcon(tempusComponent, ComponentIconFactory.SMALL) != ComponentIconFactory.getDefaultIcon(ComponentIconFactory.SMALL)
		}

	@Test
	public void blankFactory(){
		def blank = ImageFactory.getBlankBgImage()
		assert blank instanceof DLImage
		assert blank.getImage() instanceof BufferedImage
		int width = Integer.parseInt(CentralCatalogue.getApp("virtuallayer.bg.width"))
		int height = Integer.parseInt(CentralCatalogue.getApp("virtuallayer.bg.height"))
		assert blank.getImage().getWidth() == width
		assert blank.getImage().getHeight() == height
	}

}
