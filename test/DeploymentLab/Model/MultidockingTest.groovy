package DeploymentLab.Model;

import DeploymentLab.ProblemLogger;
import DeploymentLab.TestUtils;
import DeploymentLab.channellogger.ChannelManager;
import DeploymentLab.channellogger.LogLevel;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Gabriel Helman
 * @since Io
 * Date: 1/15/13
 * Time: 11:36 AM
 */
public class MultidockingTest {

	static ObjectModel omodel
	static ComponentModel cmodel
	static DLComponent goldLeader
	static Multidocking targetingComputer

	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.INFO)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		//cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))

		goldLeader = cmodel.newComponent("ez")
		targetingComputer = goldLeader._varMap["li_layer"].first()
	}


	@After
	void tearDown() {
		cmodel = null
		omodel = null
		goldLeader =  null
		targetingComputer = null
		//have fun, garbage collector!
	}


	@Test
	public void testGetTraitId() throws Exception {
		assert targetingComputer.getTraitId() == null
	}

	@Test
	public void testGetTraitDisplayName() throws Exception {
		assert targetingComputer.getTraitDisplayName() == null
	}

	@Test
	public void testGetOwner() throws Exception {
		assert targetingComputer.getOwner() == goldLeader
	}

	@Test
	public void testListVariables() throws Exception {
		assert targetingComputer.listVariables() == ["li_layer"]
	}

	@Test
	public void testEval() throws Exception {

		//setting the bound property evals the multidocking:
		//for standalone y-wings, this changes the pointers on the coin object, changes the z value of the sensor, and sets the position property of the coin

		DLObject gentemp = goldLeader.getObject("logicalobject")
		Dockingpoint gentempSensor = (Dockingpoint)gentemp.getObjectProperty("sensor")
		DLObject cp = goldLeader.getObject("cpTemp")
		Setting position = cp.getObjectSetting("position")
		DLObject sensor = goldLeader.getManagedObjectsOfType("wsnsensor").find{ it.getObjectSetting("channel").getValue() == 3 }

		assert sensor.getObjectSetting("z").getValue() == 131072
		assert ((Pier)cp.getObjectProperty("rTop")).getReferent() == gentempSensor
		assert ((Pier)cp.getObjectProperty("rTop")).getReferentObject() == gentemp
		assert ((Pier)cp.getObjectProperty("rBottom")).getReferent() == null
		assert  position.getValue() == "t"

		goldLeader.setPropertyValue("li_layer", "2048")

		assert sensor.getObjectSetting("z").getValue() == 2048
		assert ((Pier)cp.getObjectProperty("rTop")).getReferent() == null
		assert ((Pier)cp.getObjectProperty("rBottom")).getReferent() == gentempSensor
		assert ((Pier)cp.getObjectProperty("rBottom")).getReferentObject() == gentemp
		assert  position.getValue() == "b"


	}
/*
	@Test
	public void testSerialize() throws Exception {

	}

	@Test
	public void testSerializeAsPrototype() throws Exception {

	}

	@Test
	public void testGetParcel() throws Exception {
		assert targetingComputer.getParcel() == ""
	}

	@Test
	public void testSetParcel() throws Exception {

	}
	*/
}
