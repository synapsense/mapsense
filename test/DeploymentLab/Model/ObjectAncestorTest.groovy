package DeploymentLab.Model

import DeploymentLab.CentralCatalogue
import DeploymentLab.SelectModel
import org.junit.BeforeClass
import DeploymentLab.TestUtils
import org.junit.Before
import org.junit.After
import org.junit.Test
import DeploymentLab.Exceptions.ModelCatastrophe

/**
 * Test the "find my ancestor / drawing widget added to support multyDrawing at the object level
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/6/12
 * Time: 1:01 PM
 */
class ObjectAncestorTest {


	ComponentModel cmodel = null
	ObjectModel omodel = null
	SelectModel selectModel = null

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
		selectModel = null
	}

	void threeDrawingPlan(){
		def drawing = selectModel.getActiveDrawing()
		def drawing2 = cmodel.newComponent("drawing")
		def drawing3 = cmodel.newComponent("drawing")
		drawing.setPropertyValue("name", "first")
		drawing2.setPropertyValue("name", "second")
		drawing3.setPropertyValue("name", "third")
		println "there are ${cmodel.getComponentsByType("drawing").size()} drawings!!"

		selectModel.setActiveDrawing(drawing)

		def room = cmodel.newComponent("room")
		drawing.addChild(room)
		def net = cmodel.newComponent("network")
		drawing.addChild(net)

		def room2 = cmodel.newComponent("room")
		drawing2.addChild(room2)
		def net2 = cmodel.newComponent("network")
		drawing2.addChild(net2)


		for(int i = 0; i < 10; i++){
			def rack = cmodel.newComponent("rack-control-toprearexhaust-nsf-rt-thermanode2")
			rack.setPropertyValue("name", "TRE-RACK $i")
			def crah = cmodel.newComponent("crah-single-plenumrated-thermanode")
			crah.setPropertyValue("name", "CRAH==$i")
			room.addChild(rack)
			room.addChild(crah)
			net.addChild(rack)
			net.addChild(crah)
		}

		for(int i = 10; i < 20; i++){
			def rack = cmodel.newComponent("rack-control-toprearexhaust-nsf-rt-thermanode2")
			rack.setPropertyValue("name", "TRE-RACK $i")
			def crah = cmodel.newComponent("crah-single-plenumrated-thermanode")
			crah.setPropertyValue("name", "CRAH==$i")
			room2.addChild(rack)
			room2.addChild(crah)
			net2.addChild(rack)
			net2.addChild(crah)
		}


	}


	@Test
	void sanityCheck(){
		threeDrawingPlan()
		assert cmodel.getComponentsByTypePrefix("rack-control-").size() == 20
	}

	@Test
	void getObjectAncestor(){
		threeDrawingPlan()
		def drawing = selectModel.getActiveDrawing()
		omodel.getObjects().each{ o ->
			assert o.getAncestor() == cmodel.getManagingComponent(o).getDrawing().getObject( ObjectType.DRAWING )
		}
	}

	@Test(expected=ModelCatastrophe.class)
	void doubleDrawing(){
		def drawing = selectModel.getActiveDrawing()
		def drawing2 = cmodel.newComponent("drawing")
		def room = cmodel.newComponent("room")
		drawing.addChild(room)
		drawing2.addChild(room)
		def net = cmodel.newComponent("network")
		drawing.addChild(net)
		drawing2.addChild(net)
		def rack = cmodel.newComponent("rack-control-toprearexhaust-nsf-rt-thermanode2")
		rack.setPropertyValue("name", "TRE-RACK")
		room.addChild(rack)
		net.addChild(rack)

		// Flush the caches so a true check is performed.
		for( DLComponent c : cmodel.getComponents() ) {
			c.clearDrawing()
		}

		rack.getObject("rack").getAncestor()
	}

	@Test
	void iWentBackInTimeAndBecameMyOwnParent(){
		def drawing = selectModel.getActiveDrawing()
		cmodel.remove(drawing)
		def rack = cmodel.newComponent("rack-control-toprearexhaust-nsf-rt-thermanode2")
		rack.setPropertyValue("name", "TRE-RACK")
		assert rack.getObject("rack").getAncestor() == rack.getObject("rack")
	}

	@Test
	void objectsByType(){
		threeDrawingPlan()
		def drawing = selectModel.getActiveDrawing()
		def drawingo = drawing.getObject( ObjectType.DRAWING )

		assert omodel.getObjectsByType("rack").size() == 20

		def s = new HashSet<DLObject>()
		s.add(drawingo)
		assert omodel.getObjectsByTypeInDrawings("rack", s).size() == 10

		//get the other drawing
		def drawing2 = cmodel.getComponents().find {it.getName().equals("second")}
		def drawing2o = drawing2.getObject( ObjectType.DRAWING )
		def s2 = new HashSet<>()
		s2.add(drawing2o)
		assert omodel.getObjectsByTypeInDrawings("rack", s2).size() == 10

		assert omodel.getObjectsByTypeInDrawings("rack", s).disjoint( omodel.getObjectsByTypeInDrawings("rack", s2) )


	}


}
