package DeploymentLab.Model

import org.junit.BeforeClass
import DeploymentLab.TestUtils
import org.junit.Before
import DeploymentLab.UndoBuffer
import javax.swing.JFrame
import org.junit.After
import org.junit.Test

/**
 * Tests the object type : instances multimap in the ObjectModel.
 *
 * @author Gabriel Helman
 * @since Jupiter
 * Date: 3/12/12
 * Time: 5:56 PM
 */
class ObjectTypeMultiMapTest {

	ComponentModel cmodel = null
	ObjectModel omodel = null

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
	}


	def oldGetObjectsByType(String type) {
		return omodel.getObjects().findAll{ it.type == type }
	}
	
	def buildTypeMap(){
		Map<String,java.util.Collection> typesMap = [:]
		omodel.getTypes().each{ t ->
			typesMap[t] = oldGetObjectsByType(t)
		}
		return typesMap
	}
	
	@Test
	void addAndRemoveABunchOfObjects(){
		for(int i = 0; i < 20; i++){
			def rack = cmodel.newComponent("rack-control-toprearexhaust-nsf-rt-thermanode2")
			rack.setPropertyValue("name", "TRE-RACK $i")
			def crah = cmodel.newComponent("crah-single-plenumrated-thermanode")
			crah.setPropertyValue("name", "CRAH==$i")
			def user = cmodel.newComponent("userinput")
			user.setPropertyValue("name", "USER_$i")
		}

		assert omodel.getObjects().size() == omodel._objectTypeToInstances.size()

		def types = omodel.getTypes()
		//build the lists by hand
		def typesMap = buildTypeMap()
		types.each{ t ->
			assert typesMap[t].size() == omodel.getObjectsByType(t).size()
		}

		def rack1 = cmodel.getComponentsByType("rack-control-toprearexhaust-nsf-rt-thermanode2")[0]
		def rack2 = cmodel.getComponentsByType("rack-control-toprearexhaust-nsf-rt-thermanode2")[1]

		cmodel.remove( rack1 )
		cmodel.remove( rack2 )

		typesMap = buildTypeMap()
		types.each{ t ->
			assert typesMap[t].size() == omodel.getObjectsByType(t).size()
		}

		cmodel.undelete(rack2)

		typesMap = buildTypeMap()
		types.each{ t ->
			assert typesMap[t].size() == omodel.getObjectsByType(t).size()
		}


	}
}
