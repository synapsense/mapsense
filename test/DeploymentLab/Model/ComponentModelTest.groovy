package DeploymentLab.Model

import DeploymentLab.CentralCatalogue
import org.junit.BeforeClass
import DeploymentLab.TestUtils
import org.junit.Before
import DeploymentLab.UndoBuffer
import javax.swing.JFrame
import org.junit.After
import org.junit.Test
import org.apache.commons.io.FileUtils
import DeploymentLab.ComponentLibrary
import DeploymentLab.SelectModel
import DeploymentLab.ProgressManager
import DeploymentLab.channellogger.Logger
import com.google.common.collect.ArrayListMultimap

/**
 * 
 * @author Gabriel Helman
 * @since 
 * Date: 2/29/12
 * Time: 10:01 AM
 */
class ComponentModelTest {
	private static final Logger log = Logger.getLogger(ComponentModelTest.class.getName())
	static ComponentModel cmodel = null
	static ObjectModel omodel = null
	static ModelObserver tron = null
	static SelectModel selectModel
	static UndoBuffer ub

	private class ModelObserver implements ModelChangeListener{
		
		List<String> eventStack = []
		List<String> initEventStack = []
		
		@Override
		void componentAdded(DLComponent component) {
			eventStack += "componentAdded"
		}

		@Override
		void componentRemoved(DLComponent component) {
			eventStack += "componentAdded"
		}

		@Override
		void childAdded(DLComponent parent, DLComponent child) {
			eventStack += "componentRemoved"
		}

		@Override
		void childRemoved(DLComponent parent, DLComponent child) {
			eventStack += "childRemoved"
		}

		@Override
		void associationAdded(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
			eventStack += "associationAdded"
		}

		@Override
		void associationRemoved(DLComponent producer, String producerId, DLComponent consumer, String consumerId) {
			eventStack += "associationRemoved"
		}

		@Override
		void modelMetamorphosisStarting( MetamorphosisEvent event ) {
			eventStack += "modelMetamorphosisStarting.${event.getMode()}"
			initEventStack += "modelMetamorphosisStarting.${event.getMode()}"
		}

		@Override
        void modelMetamorphosisFinished( MetamorphosisEvent event ) {
            eventStack += "modelMetamorphosisFinished.${event.getMode()}"
            initEventStack += "modelMetamorphosisFinished.${event.getMode()}"
        }
    }


	void loadADLFile(String filename){
		def f = new File("build/testFiles/$filename")
		def rawXML = FileUtils.readFileToString(f, "UTF-8")
		def xml = new XmlSlurper().parseText(rawXML)
		ub.stopTracking()
		cmodel.clear()
		log.trace("Loading Object Model...", "progress")
		omodel.deserialize(xml.objects)
		log.trace("Loading Component Model...", "progress")
		cmodel.deserialize(xml.components, ComponentLibrary.INSTANCE)
		//toss the parse tree
		xml = null
		ub.resumeTracking()
	}

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()
		ub = CentralCatalogue.getUndoBuffer()

		tron = new ModelObserver()
		cmodel.addModelChangeListener(tron);

		def clib = ComponentLibrary.INSTANCE;
		clib.init(cmodel,null,null);
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
		tron = null
	}
	
	@Test
	void testingStubs(){
		println cmodel
		println tron
	}

	
	@Test
	void modelInitEvents(){
		loadADLFile("Jupiter2MarsLint2.xml")
		assert tron.initEventStack == ["modelMetamorphosisStarting.CLEARING", "modelMetamorphosisFinished.CLEARING",
                                       "modelMetamorphosisStarting.INITIALIZING", "modelMetamorphosisFinished.INITIALIZING"]
	}


	@Test
	void getComponentsByTypeForDrawing(){
		assert cmodel.getComponentsByType("drawing").size() == 1

		def room1 = cmodel.newComponent("room")
		def drawing1 = selectModel.getActiveDrawing()
		drawing1.setPropertyValue("name", "drawing1")
		drawing1.addChild(room1)
		DLComponent firstBatchRack
		for(int i = 0; i < 20; i++){
			def rack = cmodel.newComponent("rack-control-toprearexhaust-nsf-rt-thermanode2")
			rack.setPropertyValue("name", "TRE-RACK $i")
			room1.addChild(rack)
			//def crah = cmodel.newComponent("crah-single-plenumrated-thermanode")
			//crah.setPropertyValue("name", "CRAH==$i")
			//room1.addChild(crah)

			firstBatchRack = rack
		}

		def drawing2 = cmodel.newComponent("drawing")
		drawing2.setPropertyValue("name", "drawing2")
		def room2 = cmodel.newComponent("room")
		drawing2.addChild(room2)
		for(int i = 21; i <= 30; i++){
			def rack = cmodel.newComponent("rack-control-toprearexhaust-nsf-rt-thermanode2")
			rack.setPropertyValue("name", "TRE-RACK $i")
			room2.addChild(rack)
			//def crah = cmodel.newComponent("crah-single-plenumrated-thermanode")
			//crah.setPropertyValue("name", "CRAH==$i")
			//room2.addChild(crah)
		}
/*
		println cmodel.getComponents()
		println room2.getDrawing()
		println "there are ${cmodel.getComponentsByType("drawing")} drawings"
		println "$firstBatchRack is in ${firstBatchRack.getDrawing()}"
		//println cmodel.getComponentsByType("rack")
		cmodel.getComponentsByType("rack").each{
			println "${it.getName()} in ${it.getDrawing()}"
		}
*/
		assert cmodel.getComponentsByTypePrefix("rack", drawing1).size() == 20
		assert cmodel.getComponentsByTypePrefix("rack", drawing2).size() == 10

	}
	
	@Test
	void onlyOneOrphanagePerDrawing(){
		assert ! cmodel.hasOrphanage(selectModel.activeDrawing)
		def ur1 = cmodel.getOrphanage(selectModel.activeDrawing)
		def ur2 = cmodel.getOrphanage(selectModel.activeDrawing)
		def ur3 = cmodel.getOrphanage(selectModel.activeDrawing)
		assert cmodel.getComponentsByType( ComponentType.ORPHANAGE ).size() == 1
		assert ur1 == ur2
	}


	@Test
	void listComponentRoles(){
		Set<String> componentTypes = cmodel.listComponentTypes().sort()

		def doubleTypes = []
		doubleTypes.addAll(componentTypes)
		doubleTypes.addAll(componentTypes)

		for (String t: doubleTypes) {
			println "testing TYPE $t"
			DLComponent candidate = cmodel.newComponent(t)
			//assert candidate.listRoles() == cmodel.listComponentRoles(t)
			def compRoles = new HashSet<String>()
			compRoles.addAll(candidate.listRoles())
			def modelRoles = new HashSet<String>()
			modelRoles.addAll(cmodel.listComponentRoles(t))
			assert compRoles.equals(modelRoles)
		}
	}

	@Test
	void listComponentRolesOfFakeType(){
		assert cmodel.listComponentRoles("fakor") == []
	}


	@Test
	void listTypesWithRole(){
		//first, build something to check against:
		def roles = new ArrayListMultimap<String,DLComponent>()
		for (String t:  cmodel.listComponentTypes().sort()) {
			DLComponent candidate = cmodel.newComponent(t)
			candidate.listRoles().each{ r ->
				roles.put(r,candidate.getType())
			}
		}

		println roles.keySet()

		roles.keySet().each{ r ->
			def modelRoles = new HashSet<String>()
			modelRoles.addAll(cmodel.listTypesWithRole(r))

			Set compRoles = new HashSet<String>()
			compRoles.addAll(roles.get(r))

			assert modelRoles == compRoles

		}
	}
}
