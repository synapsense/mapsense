package DeploymentLab.Model;

import DeploymentLab.ProblemLogger;
import DeploymentLab.TestUtils;
import DeploymentLab.channellogger.ChannelManager;
import DeploymentLab.channellogger.LogLevel;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass
import org.junit.Test;

/**
 * Basic exercise for Turbobindings
 * @author Gabriel Helman
 * @since Io
 * Date: 1/15/13
 * Time: 5:17 PM
 *
 *
 *
 */
public class TurboBindingTest {

	static ObjectModel omodel
	static ComponentModel cmodel
	static DLComponent goldLeader
	static TurboBinding r2Unit

	static String example = """
<turbobinding vars="name">
self.getObject("node").getObjectSetting("name").setValue("Node " + name)
</turbobinding>
"""

	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.INFO)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))

		goldLeader = cmodel.newComponent("ez")
		//targetingComputer = goldLeader._varMap["name"].first()
	}

	@After
	void tearDown() {
		cmodel = null
		omodel = null
		goldLeader =  null
		//targetingComputer = null
		//have fun, garbage collector!
	}

	@Test
	void createAndEval(){
		goldLeader.setPropertyValue("name", "Dutch")
		def node = goldLeader.getObject("node")
		assert node.getObjectSetting("name").getValue() == "Node Dutch"

		def xml = new XmlSlurper().parseText(example);
		r2Unit = new TurboBinding(xml,goldLeader)
		goldLeader._varMap["name"] = [r2Unit]

		goldLeader.setPropertyValue("name", "Lando")
		assert node.getObjectSetting("name").getValue() == "Node Lando"

		assert r2Unit.getOwner() == goldLeader

		//these are here so that they'll fail if this behavior ever changes and act as a reminder to start testing it
		assert r2Unit.getTraitId() == null
		assert r2Unit.getTraitDisplayName() == null

	}

}
