package DeploymentLab.Model

import DeploymentLab.CentralCatalogue
import DeploymentLab.ComponentLibrary
import DeploymentLab.Tools.ConfigurationReplacer
import DeploymentLab.Tree.ConfigurationReplacerTree
import DeploymentLab.Tree.ConfigurationReplacerTreeModel
import DeploymentLab.UIDisplay
import org.junit.*;
import static org.junit.Assert.*
import DeploymentLab.Dialogs.Association.AssociationController
import DeploymentLab.DLStopwatch
import DeploymentLab.channellogger.ChannelListener
import DeploymentLab.channellogger.LogData
import DeploymentLab.channellogger.LogLevel
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.TestUtils
import DeploymentLab.UndoBuffer
import javax.swing.JFrame
import DeploymentLab.SelectModel
import DeploymentLab.ProblemLogger
import DeploymentLab.Dialogs.Association.AssociationBundle

/**
 *
 * @author Gabriel Helman
 * @since Mars
 * Date: 5/13/11
 * Time: 10:40 AM
 */
class DLComponentTest {

	static ObjectModel omodel
	static ComponentModel cmodel
	static SelectModel selectModel = null
	static UndoBuffer undoBuffer
	static UIDisplay uiDisplay
	static ConfigurationReplacerTree tree
	static ConfigurationReplacer cr

	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.WARN)

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))
		//cm.addChannelListener(new ProblemLogger(LogLevel.WARN))

		// Why is this being done here and in setup()? Seems redundant.
		(omodel,cmodel) = TestUtils.initModels( false )
		selectModel = CentralCatalogue.getSelectModel()
		undoBuffer = CentralCatalogue.getUndoBuffer()

		def treeModels = []
		def cmtm = new ConfigurationReplacerTreeModel(cmodel, "FakeTree")
		treeModels.add(cmtm)
		tree = new ConfigurationReplacerTree(cmtm)

		//trees go in clib
		def clib = ComponentLibrary.INSTANCE;
		clib.init(cmodel,null,treeModels);

		//and a replacer
		cr = new ConfigurationReplacer(selectModel, cmodel, tree, undoBuffer, new JFrame())
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()
		undoBuffer = CentralCatalogue.getUndoBuffer()
	}


	@After
	void tearDown() {
		cmodel = null
		omodel = null
		selectModel = null
	}


	@Test
	public void copySettingsTest() {
		def rack1 = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode")
		def rack2 = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode")
		rack1.setPropertyValue("name", "rack1")
		rack1.setPropertyValue("hot_delta", 13)
		rack1.setPropertyValue("cold_delta", 2)
		rack1.setPropertyValue("location", "somewhere")
		rack1.setPropertyValue("sample_interval", 42)
		rack2.copySettingsFrom(rack1, false)
		assert rack2.getPropertyValue("location") == "somewhere"
		assert rack2.getPropertyValue("name") != "rack1"
		assert rack2.getPropertyValue("cold_delta") == 2
		rack2.copySettingsFrom(rack1, true)
		assert rack2.getPropertyValue("name") == "rack1"
		assert rack2.getPropertyValue("cold_delta") == 2
	}


	@Test
	public void copySettingsEditableOnly(){
		def rack1 = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode")
		def rack2 = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode")
		rack1.setPropertyValue("name", "rack1")
		rack2.setPropertyValue("name", "rack2")

		rack1.setPropertyValue("location", "location 1")
		rack2.setPropertyValue("location", "location 2")

		rack1.setPropertyValue("configuration", "changed value")

		rack2.copySettingsFrom(rack1, false)

		assert rack2.getPropertyValue("location").equals("location 1")
		assert  ! rack2.getPropertyValue("name").equals("rack1")

		assert  ! rack2.getPropertyValue("configuration").equals("changed value")
	}

	//test all methods that were extended to hand out the fake keys and dlids properties

	@Test
	public void testHasPropertyNamed() {
		DLComponent rack1 = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode2")
		assertTrue(rack1.hasPropertyNamed("name"))
		assertTrue(rack1.hasPropertyNamed("x"))
		assertFalse(rack1.hasPropertyNamed("gabe"))
		//consumer
		assertFalse(rack1.hasPropertyNamed("reftemp"))
		assertTrue(rack1.hasPropertyNamed("Rack Ref Temp"))

		//producer
		//assertFalse(rack1.hasPropertyNamed("topinlet"))
		//assertTrue(rack1.hasPropertyNamed("Top Inlet Temp"))

		//"secret keys"
		assertTrue(rack1.hasPropertyNamed(DLComponent.KEYS_NAME))
		assertTrue(rack1.hasPropertyNamed(DLComponent.DLIDS_NAME))
	}

	@Test
	public void testGetPropertyNamedValue() {
		DLComponent rack = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode2")
		rack.setPropertyValue("name", "Death Star Central Computer")
		rack.setPropertyValue("sample_interval", 10)
		rack.setPropertyValue("min_recommend_t", 61.9)
		assertTrue(rack.getPropertyValue("name").equals("Death Star Central Computer"))
		assertTrue(rack.getPropertyValue("sample_interval") == 10)
		//println rack.getPropertyValue("min_recommend_t")
		assertTrue(rack.getPropertyValue("min_recommend_t") == 61.9)
		assertTrue(rack.getPropertyNamedValue("Name").equals("Death Star Central Computer"))

		// Due to Localization, these next two come back as Strings.
		assertTrue(rack.getPropertyNamedValue("Sampling Interval").equals('10'))
		assertTrue(rack.getPropertyNamedValue("Min Recommended Temp").equals('61.9'))
		def rackDLID = rack.getManagedObjectsOfType("rack").first().getDlid().toString()
		assertTrue(rack.getPropertyNamedValue(DLComponent.DLIDS_NAME).toString().contains(rackDLID))
		assertTrue(rack.getPropertyNamedValue(DLComponent.KEYS_NAME).toString().equals(""))
	}

	@Test
	public void testListVisiblePropertyNamePairs(){
		DLComponent rack = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode2")
		def pairs = rack.listVisiblePropertyNamePairs()
		//all properties, producers, consumers, and the two fake ones
		def correctList = []
		correctList.addAll(rack.listVisiblePropertyNames())
		correctList.addAll(rack.listProducers(true))
		correctList.addAll(rack.listConsumers(true))
		assert pairs.sort().size() == (correctList.sort().size() + 2)

	}


	@Test
	public void testHasProducersAndConsumers(){
		DLComponent con = cmodel.newComponent("rack-control-toprearexhaust-nsf-rt-6t")
		DLComponent pro = cmodel.newComponent("reftemp-thermanode")

		assert !con.getConsumer("reftemp").hasProducer()
		assert !pro.getProducer("reftemp").hasConsumer()

		def ac = new AssociationController(con,pro)
		AssociationBundle bundle = ac.canSolo()
		ac.executeBundle( bundle )

		assert con.getConsumer("reftemp").hasProducer()
		assert pro.getProducer("reftemp").hasConsumer()

		assert con.getConsumer("reftemp").hasProducer( pro, "reftemp" )
		assert pro.getProducer("reftemp").hasConsumer( con, "reftemp" )

		// A proxy should not match when the anchor is associated.
		DLComponent conProxy = DLProxy.createProxy( con )
		DLComponent proProxy = DLProxy.createProxy( pro )

		assert !con.getConsumer("reftemp").hasProducer( proProxy, bundle.getProducer().getId() )
		assert !pro.getProducer("reftemp").hasConsumer( conProxy, "reftemp" )
	}

	@Test
	void getComponentsByType(){
		for (int i = 1; i<= 10; i ++){
			cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode")
			cmodel.newComponent("crah-single-plenumrated-thermanode")
			cmodel.newComponent("userinput")
		}

		assert cmodel.getComponents().size() == 31
		assert cmodel.getComponentsByType("rack-control-rearexhaust-nsf-rt-thermanode").size() == 10
		cmodel.getComponentsByType("rack-control-rearexhaust-nsf-rt-thermanode").each{ c ->
			assert c.getType().equals("rack-control-rearexhaust-nsf-rt-thermanode")
		}

		assert cmodel.getComponentsByType("rack-control-r").isEmpty()
	}

	@Test
	void getComponentsByTypePrefix(){
		for (int i = 1; i<= 10; i ++){
			cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode")
			cmodel.newComponent("crah-single-plenumrated-thermanode")
			cmodel.newComponent("userinput")
			cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode2")
		}

		assert cmodel.getComponents().size() == 41
		assert cmodel.getComponentsByType("rack-control-rearexhaust-nsf-rt-thermanode2").size() == 10
		cmodel.getComponentsByType("rack-control-rearexhaust-nsf-rt-thermanode2").each{ c ->
			assert c.getType().equals("rack-control-rearexhaust-nsf-rt-thermanode2")
		}

		assert cmodel.getComponentsByTypePrefix("rack-control-r").size() == 20

		cmodel.getComponentsByTypePrefix("rack-control-r").each{ c ->
			assert c.getType().equals("rack-control-rearexhaust-nsf-rt-thermanode") || c.getType().equals("rack-control-rearexhaust-nsf-rt-thermanode2")
		}
	}


	@Test
	void hasRole(){
		def watch = new DLStopwatch()
		def rack1 = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode")
		assert rack1.hasRole("placeable")
		assert rack1.hasRole("rotatable")
		assert !rack1.hasRole("fakeomatic")
		//assert rack1.listRoles() == ["placeable", "rotateable"]
		println watch.finishMessage()
	}

	@Test
	void testIsSecondary(){
		def rack1 = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode")
		def insp = cmodel.newComponent("power_inspector")
		assert !rack1.isSecondary()
		assert insp.isSecondary()
	}


	@Test
	void checkForWarningsWhileCreating(){
		def observer = new WarningLogger()
		ChannelManager.getInstance().addChannelListener(observer)

		Set<String> componentTypes = cmodel.listComponentTypes()
		for ( String t : componentTypes.sort() ){
			if ( !cmodel.listComponentRoles(t).contains("placeable") ){
				continue
			}
			DLComponent rack = cmodel.newComponent(t)
			if(observer.problems.size() > 0){
				println "$t had problems!"
			}
			assert observer.problems.size() == 0
			observer.problems = []
		}
	}

	@Test
	void checkNodesAutoChildedToEnvObject() {
		// Objects that can have a wsnnode child
		List<String> envObjsToTest = ["rack","crac","pressure","generictemperature","verticaltemperature",
		                              "genericdevice","equipmentstatus","ion_wireless","door","leak",]

		for( String ctype : cmodel.listComponentTypes() ) {
			// Does this type have any of our env objects of interest?
			def otypes = cmodel.listManagedObjectTypes( ctype )
			otypes.retainAll( envObjsToTest )

			if( !otypes.isEmpty() ) {
				DLComponent c = cmodel.newComponent( ctype )

				for( DLObject node : c.getManagedObjectsOfType('wsnnode') ) {
					DLObject parent = null
					for( DLObject p : node.getParents() ) {
						if( otypes.contains( p.getType() ) ) {
							parent = p
							break
						}
					}
					assertNotNull("${c.getType()}: Node '${node.getAsName()}' didn't auto-child to an env parent ${otypes}", parent )
				}
			}
		}
	}

	class WarningLogger implements ChannelListener{
		List<LogData> problems = []
		@Override
		void messageReceived(LogData l) {
			if( l.level == LogLevel.WARN || l.level == LogLevel.ERROR ){
				problems += l
			}
		}
	}

	@Test
	void testRootsAreManagedObjects() {
		TestUtils.loadProjectFile(TestUtils.CURRENT_VERSION_LINT,undoBuffer,omodel,cmodel, cr)

		assert cmodel.getComponents().size() > 1

		// Verify that root objects of components are a subset of its managed objects.
		for( DLComponent c : cmodel.getComponents() ) {
			assert c.getManagedObjects().containsAll( c.getRoots() )
		}
	}

	@Test
	void testRootsAreManagedObjectsWithProxies() {
		SelectModel selectModel = new SelectModel()
		UndoBuffer ub = new UndoBuffer( cmodel, selectModel )
		ub.setParent(new JFrame())

		TestUtils.loadProjectFile( "build/testFiles/proxies.dlz", ub, omodel, cmodel )

		// Verify that root objects of components are a subset of its managed objects.
		for( DLComponent c : cmodel.getComponents() ) {
			assert c.getManagedObjects().containsAll( c.getRoots() )
		}
	}


	/**
	 * Tests the case where a proxy/anchor is associated to a component and you try to associate a sister proxy/anchor.
	 * The other association should be removed, regardless of the association type, since they point to the same objects.
	 * So it is essentially a duplicate association.
	 *
	 * This specific version tests a Consumer that is not a collection.
	 */
	@Test
	void associationsSingles() {
		def con  = cmodel.newComponent("transformer_primary")
		def pro1 = cmodel.newComponent("userinput")
		def pro2 = cmodel.newComponent("userinput")

		assert con.getAllAssociatedComponents().size() == 0

		// Associate both. The second one should replace the first.
		con.associate( "inputPower", pro2, "value" )
		assert con.getAllAssociatedComponents() == [ pro2 ]

		con.associate( "inputPower", pro1, "value" )
		assert con.getAllAssociatedComponents() == [ pro1 ]

		// Associate the proxy. The anchor should be disconnected.
		DLProxy pro1Proxy1 = DLProxy.createProxy( pro1 )
		con.associate( "inputPower", pro1Proxy1, "value" )

		assert con.getAllAssociatedComponents() == [ pro1Proxy1 ]

		// Associate a 2nd proxy. The 1st proxy should be disconnected.
		DLProxy pro1Proxy2 = DLProxy.createProxy( pro1 )
		con.associate( "inputPower", pro1Proxy2, "value" )

		assert con.getAllAssociatedComponents() == [ pro1Proxy2 ]

		// For gits n shiggles, do the other one.
		DLProxy pro2Proxy = DLProxy.createProxy( pro2 )
		con.associate( "inputPower", pro2Proxy, "value" )

		assert con.getAllAssociatedComponents() == [ pro2Proxy ]

		// Reverse it and reassociate the anchor to make sure it works in that direction.
		con.associate( "inputPower", pro2, "value" )
		assert con.getAllAssociatedComponents() == [ pro2 ]

		// Make the consumer a proxy. The anchor consumer should drop its association.
		DLProxy conProxy = DLProxy.createProxy( con )
		conProxy.associate( "inputPower", pro1, "value" )

		assert conProxy.getAllAssociatedComponents() == [ pro1 ]
		assert con.getAllAssociatedComponents().size() == 0

		// Now with both as proxies.
		conProxy.associate( "inputPower", pro1Proxy1, "value" )

		assert conProxy.getAllAssociatedComponents() == [ pro1Proxy1 ]
		assert con.getAllAssociatedComponents().size() == 0

		// Back to a normal, non-Proxy connection.
		con.associate( "inputPower", pro1, "value" )
		assert con.getAllAssociatedComponents() == [ pro1 ]
		assert conProxy.getAllAssociatedComponents().size() == 0
	}


	/**
	 * Tests the case where a proxy/anchor is associated to a component and you try to associate a sister proxy/anchor.
	 * The other association should be removed, regardless of the association type, since they point to the same objects.
	 * So it is essentially a duplicate association.
	 *
	 * This specific version tests a Consumer that is a collection.
	 */
	@Test
	void associationsCollection() {
		def con  = cmodel.newComponent("sum_operation")
		def pro1 = cmodel.newComponent("userinput")
		def pro2 = cmodel.newComponent("userinput")

		assert con.getAllAssociatedComponents().size() == 0

		// Associate both. Both should be associated.
		con.associate( "inputA", pro1, "value" )

		assert con.getAllAssociatedComponents() == [ pro1 ]

		con.associate( "inputA", pro2, "value" )
		assert con.getAllAssociatedComponents().size() == 2
		assert con.getAllAssociatedComponents().containsAll([ pro1, pro2 ])

		// Associate the proxy. The anchor should be disconnected.
		DLProxy pro1Proxy1 = DLProxy.createProxy( pro1 )
		con.associate( "inputA", pro1Proxy1, "value" )

		assert con.getAllAssociatedComponents().size() == 2
		assert con.getAllAssociatedComponents().containsAll([ pro1Proxy1, pro2 ])

		// Associate a 2nd proxy of the same anchor. The 1st proxy should be disconnected.
		DLProxy pro1Proxy2 = DLProxy.createProxy( pro1 )
		con.associate( "inputA", pro1Proxy2, "value" )

		assert con.getAllAssociatedComponents().size() == 2
		assert con.getAllAssociatedComponents().containsAll([ pro1Proxy2, pro2 ])

		// For gits n shiggles, do the other one.
		DLProxy pro2Proxy = DLProxy.createProxy( pro2 )
		con.associate( "inputA", pro2Proxy, "value" )

		assert con.getAllAssociatedComponents().size() == 2
		assert con.getAllAssociatedComponents().containsAll([ pro1Proxy2, pro2Proxy ])

		// Reverse it and reassociate the anchors to make sure it works in that direction.
		con.associate( "inputA", pro1, "value" )
		assert con.getAllAssociatedComponents().size() == 2
		assert con.getAllAssociatedComponents().containsAll([ pro1, pro2Proxy ])

		con.associate( "inputA", pro2, "value" )
		assert con.getAllAssociatedComponents().size() == 2
		assert con.getAllAssociatedComponents().containsAll([ pro1, pro2 ])

		// Make the consumer a proxy. The anchor should drop the matching association.
		DLProxy conProxy = DLProxy.createProxy( con )
		conProxy.associate( "inputA", pro1, "value" )

		assert conProxy.getAllAssociatedComponents() == [ pro1 ]
		assert con.getAllAssociatedComponents() == [ pro2 ]

		// Now with both as proxies.
		conProxy.associate( "inputA", pro2Proxy, "value" )

		assert conProxy.getAllAssociatedComponents().size() == 2
		assert conProxy.getAllAssociatedComponents().containsAll([ pro1, pro2Proxy ])
		assert con.getAllAssociatedComponents().size() == 0

		// Back to a normal, non-Proxy connections.
		con.associate( "inputA", pro1, "value" )
		assert con.getAllAssociatedComponents() == [ pro1 ]
		assert conProxy.getAllAssociatedComponents() == [ pro2Proxy ]

		con.associate( "inputA", pro2, "value" )
		assert con.getAllAssociatedComponents().size() == 2
		assert con.getAllAssociatedComponents().containsAll([ pro1, pro2 ])
		assert conProxy.getAllAssociatedComponents().size() == 0
	}


	/**
	 * Tests the case where a proxy/anchor is associated to a component and you try to associate a sister proxy/anchor.
	 * The other association should be removed, regardless of the association type, since they point to the same objects.
	 * So it is essentially a duplicate association.
	 *
	 * This specific version tests a Consumer that is a collection but the producer is Exclusive.
	 */
	@Test
	void associationsCollectionExclusiveProducer() {
		def con  = cmodel.newComponent("pdi-pdu") // panel# (1-10)
		def pro1 = cmodel.newComponent("modbus-cyberex-bcms-custom") // panel
		def pro2 = cmodel.newComponent("modbus-cyberex-bcms-custom")

		assert con.getAllAssociatedComponents().size() == 0

		// Associate both, but with different consumers on the component. Both should be associated.
		con.associate( "panel1", pro1, "panel" )
		assert con.getAllAssociatedComponents() == [ pro1 ]

		con.associate( "panel2", pro2, "panel" )
		assert con.getAllAssociatedComponents().size() == 2
		assert con.getAllAssociatedComponents().containsAll([ pro1, pro2 ])

		// Associate the proxy to a 3rd consumer of that component. The anchor should be disconnected since it is exclusive and already connected to consumer 1.
		DLProxy pro1Proxy1 = DLProxy.createProxy( pro1 )
		con.associate( "panel3", pro1Proxy1, "panel" )

		assert con.getAllAssociatedComponents().size() == 2
		assert con.getAllAssociatedComponents().containsAll([ pro1Proxy1, pro2 ])

		// Associate a 2nd proxy of the same anchor to a different consumer than proxy1. The 1st proxy should be disconnected.
		DLProxy pro1Proxy2 = DLProxy.createProxy( pro1 )
		con.associate( "panel4", pro1Proxy2, "panel" )

		assert con.getAllAssociatedComponents().size() == 2
		assert con.getAllAssociatedComponents().containsAll([ pro1Proxy2, pro2 ])

		// For gits n shiggles, do the other one.
		DLProxy pro2Proxy = DLProxy.createProxy( pro2 )
		con.associate( "panel5", pro2Proxy, "panel" )

		assert con.getAllAssociatedComponents().size() == 2
		assert con.getAllAssociatedComponents().containsAll([ pro1Proxy2, pro2Proxy ])

		// Reverse it and reassociate the anchors to make sure it works in that direction.
		con.associate( "panel6", pro1, "panel" )
		assert con.getAllAssociatedComponents().size() == 2
		assert con.getAllAssociatedComponents().containsAll([ pro1, pro2Proxy ])

		con.associate( "panel7", pro2, "panel" )
		assert con.getAllAssociatedComponents().size() == 2
		assert con.getAllAssociatedComponents().containsAll([ pro1, pro2 ])

		// Make the consumer a proxy. The anchor should drop the matching association.
		DLProxy conProxy = DLProxy.createProxy( con )
		conProxy.associate( "panel8", pro1, "panel" )

		assert conProxy.getAllAssociatedComponents() == [ pro1 ]
		assert con.getAllAssociatedComponents() == [ pro2 ]

		// Now with both as proxies.
		conProxy.associate( "panel9", pro2Proxy, "panel" )

		assert conProxy.getAllAssociatedComponents().size() == 2
		assert conProxy.getAllAssociatedComponents().containsAll([ pro1, pro2Proxy ])
		assert con.getAllAssociatedComponents().size() == 0

		// Back to a normal, non-Proxy connections.
		con.associate( "panel1", pro1, "panel" )
		assert con.getAllAssociatedComponents() == [ pro1 ]
		assert conProxy.getAllAssociatedComponents() == [ pro2Proxy ]

		con.associate( "panel2", pro2, "panel" )
		assert con.getAllAssociatedComponents().size() == 2
		assert con.getAllAssociatedComponents().containsAll([ pro1, pro2 ])
		assert conProxy.getAllAssociatedComponents().size() == 0
	}


	/**
	 * Tests the case where a proxy/anchor is associated to a component that has multiple consumer targets of the same
	 * type, the producer is not Exclusive and you try to associate a sister proxy/anchor. The association should be
	 * allowed if it is a different consumer target. If they are the same consumer target, the other association should
	 * be removed since they point to the same objects and it would be a duplicate association.
	 */
	@Test
	void associationsMultiCollectionSingleConsumer() {
		def con  = cmodel.newComponent("math_operation_division") // inputA, inputB
		def pro1 = cmodel.newComponent("userinput") // value
		def pro2 = cmodel.newComponent("userinput")

		assert con.getAllAssociatedComponents().size() == 0

		// Associate both, but with different consumers on the component. Both should be associated.
		con.associate( "inputA", pro1, "value" )
		assert con.getConsumerProducers("inputA") == [ pro1 ]

		con.associate( "inputB", pro2, "value" )
		assert con.getConsumerProducers("inputA") == [ pro1 ]
		assert con.getConsumerProducers("inputB") == [ pro2 ]

		// Associate the producers with the other consumer. They should be accepted and the second is still there
		// since the consumer is a collection.
		con.associate( "inputB", pro1, "value" )
		assert pro1.getAllAssociatedComponents() == [ con ]
		assert pro2.getAllAssociatedComponents() == [ con ]
		assert con.getConsumerProducers("inputA") == [ pro1 ]
		assert con.getConsumerProducers("inputB").size() == 2
		assert con.getConsumerProducers("inputB").containsAll([ pro1, pro2 ])

		con.associate( "inputA", pro2, "value" )
		assert pro1.getAllAssociatedComponents() == [ con ]
		assert pro2.getAllAssociatedComponents() == [ con ]
		assert con.getConsumerProducers("inputA").size() == 2
		assert con.getConsumerProducers("inputA").containsAll([ pro1, pro2 ])
		assert con.getConsumerProducers("inputB").size() == 2
		assert con.getConsumerProducers("inputB").containsAll([ pro1, pro2 ])

		// Associate the proxy to one of the consumers. The existing association to that consumer should be
		// disconnected, but not the one on the other consumer.
		DLProxy pro1Proxy1 = DLProxy.createProxy( pro1 )
		con.associate( "inputA", pro1Proxy1, "value" )

		assert pro1.getAllAssociatedComponents() == [ con ]
		assert pro2.getAllAssociatedComponents() == [ con ]
		assert pro1Proxy1.getAllAssociatedComponents() == [ con ]
		assert con.getConsumerProducers("inputA").size() == 2
		assert con.getConsumerProducers("inputA").containsAll([ pro1Proxy1, pro2 ])
		assert con.getConsumerProducers("inputB").size() == 2
		assert con.getConsumerProducers("inputB").containsAll([ pro1, pro2 ])

		// Associate a 2nd proxy of the same anchor to a different consumer than proxy1. The 1st proxy should remain and
		// the anchor for that consumer will be disconnected.
		DLProxy pro1Proxy2 = DLProxy.createProxy( pro1 )
		con.associate( "inputB", pro1Proxy2, "value" )

		assert pro1.getAllAssociatedComponents().isEmpty()
		assert pro2.getAllAssociatedComponents() == [ con ]
		assert pro1Proxy1.getAllAssociatedComponents() == [ con ]
		assert pro1Proxy2.getAllAssociatedComponents() == [ con ]
		assert con.getConsumerProducers("inputA").size() == 2
		assert con.getConsumerProducers("inputA").containsAll([ pro1Proxy1, pro2 ])
		assert con.getConsumerProducers("inputB").size() == 2
		assert con.getConsumerProducers("inputB").containsAll([ pro1Proxy2, pro2 ])

		// Associate the 2nd proxy to the other consumer. The 1st proxy should be totally disconnected while the 2nd
		// proxy holds both associations.
		con.associate( "inputA", pro1Proxy2, "value" )

		assert pro1.getAllAssociatedComponents().isEmpty()
		assert pro2.getAllAssociatedComponents() == [ con ]
		assert pro1Proxy1.getAllAssociatedComponents().isEmpty()
		assert pro1Proxy2.getAllAssociatedComponents() == [ con ]
		assert con.getConsumerProducers("inputA").size() == 2
		assert con.getConsumerProducers("inputA").containsAll([ pro1Proxy2, pro2 ])
		assert con.getConsumerProducers("inputB").size() == 2
		assert con.getConsumerProducers("inputB").containsAll([ pro1Proxy2, pro2 ])

		// Reverse it and reassociate the anchor to make sure it works in that direction.
		con.associate( "inputA", pro1, "value" )

		assert pro1.getAllAssociatedComponents() == [ con ]
		assert pro2.getAllAssociatedComponents() == [ con ]
		assert pro1Proxy1.getAllAssociatedComponents().isEmpty()
		assert pro1Proxy2.getAllAssociatedComponents() == [ con ]
		assert con.getConsumerProducers("inputA").size() == 2
		assert con.getConsumerProducers("inputA").containsAll([ pro1, pro2 ])
		assert con.getConsumerProducers("inputB").size() == 2
		assert con.getConsumerProducers("inputB").containsAll([ pro1Proxy2, pro2 ])

		con.associate( "inputB", pro1, "value" )

		assert pro1.getAllAssociatedComponents() == [ con ]
		assert pro2.getAllAssociatedComponents() == [ con ]
		assert pro1Proxy1.getAllAssociatedComponents().isEmpty()
		assert pro1Proxy2.getAllAssociatedComponents().isEmpty()
		assert con.getConsumerProducers("inputA").size() == 2
		assert con.getConsumerProducers("inputA").containsAll([ pro1, pro2 ])
		assert con.getConsumerProducers("inputB").size() == 2
		assert con.getConsumerProducers("inputB").containsAll([ pro1, pro2 ])

		// Make the consumer a proxy. The anchor should drop the matching association.
		DLProxy conProxy = DLProxy.createProxy( con )
		conProxy.associate( "inputA", pro1, "value" )

		assert pro1.getAllAssociatedComponents().size() == 2
		assert pro1.getAllAssociatedComponents().containsAll([ con, conProxy ])
		assert pro2.getAllAssociatedComponents() == [ con ]
		assert pro1Proxy1.getAllAssociatedComponents().isEmpty()
		assert pro1Proxy2.getAllAssociatedComponents().isEmpty()
		assert con.getConsumerProducers("inputA") == [ pro2 ]
		assert con.getConsumerProducers("inputB").size() == 2
		assert con.getConsumerProducers("inputB").containsAll([ pro1, pro2 ])
		assert conProxy.getConsumerProducers("inputA") == [ pro1 ]
		assert conProxy.getConsumerProducers("inputB").isEmpty()

		// Now with both as proxies.
		conProxy.associate( "inputB", pro1Proxy1, "value" )

		assert pro1.getAllAssociatedComponents() == [ conProxy ]
		assert pro2.getAllAssociatedComponents() == [ con ]
		assert pro1Proxy1.getAllAssociatedComponents() == [ conProxy ]
		assert pro1Proxy2.getAllAssociatedComponents().isEmpty()
		assert con.getConsumerProducers("inputA") == [ pro2 ]
		assert con.getConsumerProducers("inputB") == [ pro2 ]
		assert conProxy.getConsumerProducers("inputA") == [ pro1 ]
		assert conProxy.getConsumerProducers("inputB") == [ pro1Proxy1 ]

		// Back to a normal, non-Proxy connections.
		con.associate( "inputA", pro1, "value" )

		assert pro1.getAllAssociatedComponents() == [ con ]
		assert pro2.getAllAssociatedComponents() == [ con ]
		assert pro1Proxy1.getAllAssociatedComponents() == [ conProxy ]
		assert pro1Proxy2.getAllAssociatedComponents().isEmpty()
		assert con.getConsumerProducers("inputA").size() == 2
		assert con.getConsumerProducers("inputA").containsAll([ pro1, pro2 ])
		assert con.getConsumerProducers("inputB") == [ pro2 ]
		assert conProxy.getConsumerProducers("inputA").isEmpty()
		assert conProxy.getConsumerProducers("inputB") == [ pro1Proxy1 ]

		con.associate( "inputB", pro1, "value" )

		assert pro1.getAllAssociatedComponents() == [ con ]
		assert pro2.getAllAssociatedComponents() == [ con ]
		assert pro1Proxy1.getAllAssociatedComponents().isEmpty()
		assert pro1Proxy2.getAllAssociatedComponents().isEmpty()
		assert con.getConsumerProducers("inputA").size() == 2
		assert con.getConsumerProducers("inputA").containsAll([ pro1, pro2 ])
		assert con.getConsumerProducers("inputB").size() == 2
		assert con.getConsumerProducers("inputB").containsAll([ pro1, pro2 ])
		assert conProxy.getConsumerProducers("inputA").isEmpty()
		assert conProxy.getConsumerProducers("inputB").isEmpty()
	}


	/**
	 * Tests the case where a proxy/anchor is associated to a component and you try to associate a sister proxy/anchor.
	 * The other association should be removed, regardless of the association type, since they point to the same objects.
	 * So it is essentially a duplicate association.
	 *
	 * This specific version tests a single-connection Conducers.
	 */
	@Test
	void associationsConducerSingles() {
		def con  = cmodel.newComponent("standalone_power_rack")
		def pro1 = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode2")
		def pro2 = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode2")

		assert con.getAllAssociatedComponents().size() == 0

		// Associate both. The second one should replace the first.
		con.associate( con.getConducer("PEBridge"), pro2, pro2.getConducer("PEBridge") )
		assert con.getAllAssociatedComponents().size() == 1
		assert con.getConsumerProducers() == [ pro2 ]
		assert con.getProducerConsumers() == [ pro2 ]

		con.associate( con.getConducer("PEBridge"), pro1, pro1.getConducer("PEBridge") )
		assert con.getAllAssociatedComponents().size() == 1
		assert con.getConsumerProducers() == [ pro1 ]
		assert con.getProducerConsumers() == [ pro1 ]

		// Associate the proxy. The anchor should be disconnected.
		DLProxy pro1Proxy1 = DLProxy.createProxy( pro1 )
		con.associate( con.getConducer("PEBridge"), pro1Proxy1, pro1Proxy1.getConducer("PEBridge") )

		assert con.getAllAssociatedComponents().size() == 1
		assert con.getConsumerProducers() == [ pro1Proxy1 ]
		assert con.getProducerConsumers() == [ pro1Proxy1 ]

		// Associate a 2nd proxy. The 1st proxy should be disconnected.
		DLProxy pro1Proxy2 = DLProxy.createProxy( pro1 )
		con.associate( con.getConducer("PEBridge"), pro1Proxy2, pro1Proxy2.getConducer("PEBridge") )

		assert con.getAllAssociatedComponents().size() == 1
		assert con.getConsumerProducers() == [ pro1Proxy2 ]
		assert con.getProducerConsumers() == [ pro1Proxy2 ]

		// For gits n shiggles, do the other one.
		DLProxy pro2Proxy = DLProxy.createProxy( pro2 )
		con.associate( con.getConducer("PEBridge"), pro2Proxy, pro2Proxy.getConducer("PEBridge") )

		assert con.getAllAssociatedComponents().size() == 1
		assert con.getConsumerProducers() == [ pro2Proxy ]
		assert con.getProducerConsumers() == [ pro2Proxy ]

		// Reverse it and reassociate the anchor to make sure it works in that direction.
		con.associate( con.getConducer("PEBridge"), pro2, pro2.getConducer("PEBridge") )

		assert con.getAllAssociatedComponents().size() == 1
		assert con.getConsumerProducers() == [ pro2 ]
		assert con.getProducerConsumers() == [ pro2 ]

		// Make the consumer a proxy. The anchor should drop its association.
		DLProxy conProxy = DLProxy.createProxy( con )
		conProxy.associate( conProxy.getConducer("PEBridge"), pro1, pro1.getConducer("PEBridge") )

		assert conProxy.getAllAssociatedComponents().size() == 1
		assert conProxy.getConsumerProducers() == [ pro1 ]
		assert conProxy.getProducerConsumers() == [ pro1 ]

		assert con.getAllAssociatedComponents().size() == 0

		// Now with both as proxies.
		conProxy.associate( conProxy.getConducer("PEBridge"), pro2Proxy, pro2Proxy.getConducer("PEBridge") )

		assert conProxy.getAllAssociatedComponents().size() == 1
		assert conProxy.getConsumerProducers() == [ pro2Proxy ]
		assert conProxy.getProducerConsumers() == [ pro2Proxy ]

		assert con.getAllAssociatedComponents().size() == 0

		// Back to a normal, non-Proxy connections.
		con.associate( con.getConducer("PEBridge"), pro1, pro1.getConducer("PEBridge") )

		assert con.getAllAssociatedComponents().size() == 1
		assert con.getConsumerProducers() == [ pro1 ]
		assert con.getProducerConsumers() == [ pro1 ]

		assert conProxy.getAllAssociatedComponents().size() == 0
	}


	/**
	 * Tests the case where a proxy/anchor is associated to a component and you try to associate a sister proxy/anchor.
	 * The other association should be removed, regardless of the association type, since they point to the same objects.
	 * So it is essentially a duplicate association.
	 *
	 * This specific version tests a Conducer that is a collection.
	 */
	@Test
	void associationsConducerCollection() {
		def con  = cmodel.newComponent("control_rtu_manager_bacnet")
		def pro1 = cmodel.newComponent("control_register_damper")
		def pro2 = cmodel.newComponent("control_register_damper")

		assert con.getAllAssociatedComponents().size() == 0

		// Associate both. Both should be associated.
		con.associate( con.getConducer("RTUMgrChildLink"), pro1, pro1.getConducer("RegisterDamperParentLink") )
		assert con.getAllAssociatedComponents().size() == 1
		assert con.getConsumerProducers() == [ pro1 ]
		assert con.getProducerConsumers() == [ pro1 ]

		con.associate( con.getConducer("RTUMgrChildLink"), pro2, pro2.getConducer("RegisterDamperParentLink") )
		assert con.getAllAssociatedComponents().size() == 2
		assert con.getConsumerProducers().containsAll([ pro1, pro2 ])
		assert con.getProducerConsumers().containsAll([ pro1, pro2 ])

		// Associate the proxy. The anchor should be disconnected.
		DLProxy pro1Proxy1 = DLProxy.createProxy( pro1 )
		con.associate( con.getConducer("RTUMgrChildLink"), pro1Proxy1, pro1Proxy1.getConducer("RegisterDamperParentLink") )

		assert con.getAllAssociatedComponents().size() == 2
		assert con.getConsumerProducers().containsAll([ pro1Proxy1, pro2 ])
		assert con.getProducerConsumers().containsAll([ pro1Proxy1, pro2 ])

		// Associate a 2nd proxy of the same anchor. The 1st proxy should be disconnected.
		DLProxy pro1Proxy2 = DLProxy.createProxy( pro1 )
		con.associate( con.getConducer("RTUMgrChildLink"), pro1Proxy2, pro1Proxy2.getConducer("RegisterDamperParentLink") )

		assert con.getAllAssociatedComponents().size() == 2
		assert con.getConsumerProducers().containsAll([ pro1Proxy2, pro2 ])
		assert con.getProducerConsumers().containsAll([ pro1Proxy2, pro2 ])

		// For gits n shiggles, do the other one.
		DLProxy pro2Proxy = DLProxy.createProxy( pro2 )
		con.associate( con.getConducer("RTUMgrChildLink"), pro2Proxy, pro2Proxy.getConducer("RegisterDamperParentLink") )

		assert con.getAllAssociatedComponents().size() == 2
		assert con.getConsumerProducers().containsAll([ pro1Proxy2, pro2Proxy ])
		assert con.getProducerConsumers().containsAll([ pro1Proxy2, pro2Proxy ])

		// Reverse it and reassociate the anchors to make sure it works in that direction.
		con.associate( con.getConducer("RTUMgrChildLink"), pro1, pro1.getConducer("RegisterDamperParentLink") )
		assert con.getAllAssociatedComponents().size() == 2
		assert con.getConsumerProducers().containsAll([ pro1, pro2Proxy ])
		assert con.getProducerConsumers().containsAll([ pro1, pro2Proxy ])

		con.associate( con.getConducer("RTUMgrChildLink"), pro2, pro2.getConducer("RegisterDamperParentLink") )
		assert con.getAllAssociatedComponents().size() == 2
		assert con.getConsumerProducers().containsAll([ pro1, pro2 ])
		assert con.getProducerConsumers().containsAll([ pro1, pro2 ])

		// Make the consumer a proxy. The anchor should drop the matching association.
		DLProxy conProxy = DLProxy.createProxy( con )
		conProxy.associate( conProxy.getConducer("RTUMgrChildLink"), pro1, pro1.getConducer("RegisterDamperParentLink") )

		assert conProxy.getAllAssociatedComponents().size() == 1
		assert conProxy.getConsumerProducers().containsAll([ pro1 ])
		assert conProxy.getProducerConsumers().containsAll([ pro1 ])

		assert con.getAllAssociatedComponents().size() == 1
		assert con.getConsumerProducers().containsAll([ pro2 ])
		assert con.getProducerConsumers().containsAll([ pro2 ])

		// Now with both as proxies.
		conProxy.associate( conProxy.getConducer("RTUMgrChildLink"), pro2Proxy, pro2Proxy.getConducer("RegisterDamperParentLink") )

		assert conProxy.getAllAssociatedComponents().size() == 2
		assert conProxy.getConsumerProducers().containsAll([ pro1, pro2Proxy ])
		assert conProxy.getProducerConsumers().containsAll([ pro1, pro2Proxy ])

		assert con.getAllAssociatedComponents().size() == 0

		// Back to a normal, non-Proxy connections.
		con.associate( con.getConducer("RTUMgrChildLink"), pro1, pro1.getConducer("RegisterDamperParentLink") )
		assert con.getAllAssociatedComponents().size() == 1
		assert con.getConsumerProducers().containsAll([ pro1 ])
		assert con.getProducerConsumers().containsAll([ pro1 ])

		assert conProxy.getAllAssociatedComponents().size() == 1
		assert conProxy.getConsumerProducers().containsAll([ pro2Proxy ])
		assert conProxy.getProducerConsumers().containsAll([ pro2Proxy ])

		con.associate( con.getConducer("RTUMgrChildLink"), pro2, pro2.getConducer("RegisterDamperParentLink") )
		assert con.getAllAssociatedComponents().size() == 2
		assert con.getConsumerProducers().containsAll([ pro1, pro2 ])
		assert con.getProducerConsumers().containsAll([ pro1, pro2 ])

		assert conProxy.getAllAssociatedComponents().size() == 0
	}


	/**
	 * Tests the case where a proxy/anchor is associated to a component and you try to associate a sister proxy/anchor.
	 * The other association should be removed, regardless of the association type, since they point to the same objects.
	 * So it is essentially a duplicate association.
	 *
	 * This specific version tests a Consumer that is a collection but the producer is Exclusive.
	 */
	@Test
	void associationsConducerCollectionExclusiveProducer() {
		// We need two consumers. Make one a proxy so that stuff gets exercised.
		def con1 = cmodel.newComponent("control_rtu_manager_bacnet")
		def con2 = DLProxy.createProxy( cmodel.newComponent("control_rtu_manager_bacnet") )
		def pro1 = cmodel.newComponent("control_register_damper")
		def pro2 = cmodel.newComponent("control_register_damper")

		assert con1.getAllAssociatedComponents().size() == 0
		assert con2.getAllAssociatedComponents().size() == 0

		// Associate both, but with different consumers. Both should be associated.
		con1.associate( con1.getConducer("RTUMgrChildLink"), pro1, pro1.getConducer("RegisterDamperParentLink") )
		assert con1.getAllAssociatedComponents().size() == 1
		assert con1.getConsumerProducers() == [ pro1 ]
		assert con1.getProducerConsumers() == [ pro1 ]

		con2.associate( con2.getConducer("RTUMgrChildLink"), pro2, pro2.getConducer("RegisterDamperParentLink") )
		assert con2.getAllAssociatedComponents().size() == 1
		assert con2.getConsumerProducers() == [ pro2 ]
		assert con2.getProducerConsumers() == [ pro2 ]

		// Associate the first one with the other consumer. The first connection should be disconnected since it is exclusive and already connected to consumer 1.
		con2.associate( con2.getConducer("RTUMgrChildLink"), pro1, pro1.getConducer("RegisterDamperParentLink") )

		assert con1.getAllAssociatedComponents().size() == 0

		assert con2.getAllAssociatedComponents().size() == 2
		assert con2.getConsumerProducers().containsAll([ pro1, pro2 ])
		assert con2.getProducerConsumers().containsAll([ pro1, pro2 ])

		// Associate a proxy of the first consumer to the other producer. The anchor should be disconnected.
		DLProxy pro1Proxy1 = DLProxy.createProxy( pro1 )
		con1.associate( con1.getConducer("RTUMgrChildLink"), pro1Proxy1, pro1Proxy1.getConducer("RegisterDamperParentLink") )

		assert con1.getAllAssociatedComponents().size() == 1
		assert con1.getConsumerProducers().containsAll([ pro1Proxy1 ])
		assert con1.getProducerConsumers().containsAll([ pro1Proxy1 ])

		assert con2.getAllAssociatedComponents().size() == 1
		assert con2.getConsumerProducers().containsAll([ pro2 ])
		assert con2.getProducerConsumers().containsAll([ pro2 ])

		// Associate a proxy of the same anchor to a different consumer than proxy1. The 1st proxy should be disconnected.
		DLProxy pro1Proxy2 = DLProxy.createProxy( pro1 )
		con2.associate( con2.getConducer("RTUMgrChildLink"), pro1Proxy2, pro1Proxy2.getConducer("RegisterDamperParentLink") )

		assert con1.getAllAssociatedComponents().size() == 0

		assert con2.getAllAssociatedComponents().size() == 2
		assert con2.getConsumerProducers().containsAll([ pro1Proxy2, pro2 ])
		assert con2.getProducerConsumers().containsAll([ pro1Proxy2, pro2 ])

		// For gits n shiggles, do the other one.
		DLProxy pro2Proxy = DLProxy.createProxy( pro2 )
		con1.associate( con1.getConducer("RTUMgrChildLink"), pro2Proxy, pro2Proxy.getConducer("RegisterDamperParentLink") )

		assert con1.getAllAssociatedComponents().size() == 1
		assert con1.getConsumerProducers().containsAll([ pro2Proxy ])
		assert con1.getProducerConsumers().containsAll([ pro2Proxy ])

		assert con2.getAllAssociatedComponents().size() == 1
		assert con2.getConsumerProducers().containsAll([ pro1Proxy2 ])
		assert con2.getProducerConsumers().containsAll([ pro1Proxy2 ])

		// Reverse it and reassociate the anchors to make sure it works in that direction.
		con1.associate( con1.getConducer("RTUMgrChildLink"), pro1, pro1.getConducer("RegisterDamperParentLink") )
		assert con1.getAllAssociatedComponents().size() == 2
		assert con1.getConsumerProducers().containsAll([ pro1, pro2Proxy ])
		assert con1.getProducerConsumers().containsAll([ pro1, pro2Proxy ])

		assert con2.getAllAssociatedComponents().size() == 0

		con2.associate( con2.getConducer("RTUMgrChildLink"), pro2, pro2.getConducer("RegisterDamperParentLink") )
		assert con1.getAllAssociatedComponents().size() == 1
		assert con1.getConsumerProducers() == [ pro1 ]
		assert con1.getProducerConsumers() == [ pro1 ]

		assert con2.getAllAssociatedComponents().size() == 1
		assert con2.getConsumerProducers().containsAll([ pro2 ])
		assert con2.getProducerConsumers().containsAll([ pro2 ])
	}

	/**
	 * Make sure the myDrawing cache gets flushed.
	 */
	@Test
	void drawingCacheGetsFlushed() {
		def origDrawing = cmodel.getComponentsByType('drawing').first()
		def rack = TestUtils.newComponentWithParents( cmodel, TestUtils.DEFAULT_RACK, origDrawing )

		assert rack.getDrawing() == origDrawing

		// Make a new Drawing
		def newDrawing = cmodel.newComponent('drawing')
		def newNet = cmodel.newComponent("network")
		newDrawing.addChild( newNet )
		def newRoom = cmodel.newComponent("room")
		newDrawing.addChild( newRoom )

		// Remove the rack from the first Drawing.
		def origParents = []
		for( def parent : rack.getParentComponents() ) {
			parent.removeChild( rack )
			origParents.add( parent )
		}

		// removeChild no longer flushes the cache since too many things need a component to always have a Drawing
		// except the brief period between creation and it getting childed to something.
		// shouldFail( ModelCatastrophe ) { rack.getDrawing() }
		assert rack.getDrawing() == origDrawing

		// Now make it a child of the new Drawing's room/net. The getDrawing should have the new one.
		newNet.addChild( rack )
		assert rack.getDrawing() == newDrawing
		newRoom.addChild( rack )
		assert rack.getDrawing() == newDrawing

		// For gits n shiggles, move it back.
		newNet.removeChild( rack )
		assert rack.getDrawing() == newDrawing
		newRoom.removeChild( rack )
		assert rack.getDrawing() == newDrawing

		for( def parent : origParents ) {
			parent.addChild( rack )
			assert rack.getDrawing() == origDrawing
		}
	}
}
