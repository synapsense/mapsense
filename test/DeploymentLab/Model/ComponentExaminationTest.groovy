package DeploymentLab.Model

import org.junit.BeforeClass
import DeploymentLab.TestUtils
import org.junit.Before
import javax.swing.JFrame
import org.junit.After
import org.junit.Test
import java.lang.reflect.Method
import DeploymentLab.Examinations.ComponentExaminations
import DeploymentLab.Examinations.Examination

/**
 *
 * @author Gabriel Helman
 * @since
 * Date: 7/11/12
 * Time: 12:08 PM
 */
class ComponentExaminationTest {

	static final String RACKNAME = "RACKNAME"

	ComponentModel cmodel = null
	ObjectModel omodel = null

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
	}
	/**
	 * Run any method marked as an Examination
	 */
	@Test
	void runExaminations() {
		Set<String> componentTypes = cmodel.listComponentTypes()
		for (Method method : ComponentExaminations.getMethods()) {
			if (method.isAnnotationPresent(Examination.class)) {
				println "invoke ${method.name}"
				for (String t : componentTypes.sort()) {
					DLComponent c = cmodel.newComponent(t)
					println "--testing $t"
					c.setPropertyValue('name', RACKNAME)
					method.invoke(null, c)
				}
			}
		}
	}

}
