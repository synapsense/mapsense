package DeploymentLab.Model

import org.junit.Before
import DeploymentLab.TestUtils
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.ProblemLogger
import DeploymentLab.channellogger.LogLevel
import org.junit.After
import org.junit.Test
import com.synapsense.dto.RuleType
import org.junit.BeforeClass;

/**
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/17/12
 * Time: 6:38 PM
 */
public class JavaRuleTest {
	static ObjectModel omodel
	static ComponentModel cmodel

	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.INFO)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		//cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))
	}


	@After
	void tearDown() {
		cmodel = null
		omodel = null
		//have fun, garbage collector!
	}

	private static final String TESTVAL = "testval"

	@Test
	void basicjrule(){
		//def math = cmodel.newComponent("math_operation")
		//def mo = math.getObject("math")
		def power = cmodel.newComponent("standalone_power_rack")
		def pr = power.getObject("power_rack")

		pr.setKey("FAKE:42")

		def jrule = pr.getObjectProperty("demandPower")
		assert jrule instanceof Rule

		assert jrule.getType() == "rule"
		assert jrule.getValueType() == "java.lang.Double"
		assert jrule.hasSchedule()
		assert jrule.getSchedule() == "0 0/15 * * * ?"

		assert jrule.getHistoric()

		assert jrule.getRuleClass() == "com.synapsense.rulesengine.core.script.metric.PowerRackMetrics"
		assert jrule.getSourceName() == "com.synapsense.rulesengine.core.script.metric.PowerRackMetrics"

		def bind = jrule.getBindings()
		assert bind.size() == 0


		RuleType rt = jrule.createRuleType()
		assert rt != null
		assert rt.getSourceType() == "Java"

		assert jrule.getServerTypeName() == "com.synapsense.rulesengine.core.script.metric.PowerRackMetrics"
		def ruleName = pr.getKey() + jrule.name //not quite what we use in production, but close enough here
		jrule.setServerInstanceName(ruleName)
		assert jrule.getServerInstanceName() == ruleName


		assert jrule.toString().size() > 0
	}

}
