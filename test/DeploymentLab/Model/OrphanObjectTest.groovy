package DeploymentLab.Model

import org.junit.BeforeClass
import DeploymentLab.TestUtils
import org.junit.Before
import DeploymentLab.UndoBuffer
import javax.swing.JFrame
import org.junit.After
import org.junit.Test

/**
 * Chedk for objects with no possible parents.
 * @author Gabriel Helman
 * @since Jupiter2
 * Date: 6/29/12
 * Time: 10:30 AM
 */
class OrphanObjectTest {

	private static final orphanTypes = [ ObjectType.DRAWING, "configuration_data", "group", "calced_sensor", "controller_status"]

	//ComponentModel cmodel = null
	ObjectModel omodel = null

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		omodel = TestUtils.loadObjectModel()
		//cmodel = TestUtils.loadComponentModel(omodel)
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		//cmodel = null
	}

	@Test
	void lookForOrphans(){
		for(String type : omodel.getTypes() ){
			//cfg objects are for composition with control objects and really don't need parents
			if(type.contains("cfg_")){
				continue
			}
			if(orphanTypes.contains(type)){
				continue
			}
			//assert omodel.listParentTypes(type).size() > 0
			if(omodel.listParentTypes(type).size() == 0){
				println "no parents for $type"
			}
		}
	}

	@Test
	void lookForUnknownChildren(){
		def allKnownTypes = new HashSet<String>( omodel.objTypes.keySet() )
		for(String type : omodel.getTypes() ){
			for(String childtype : omodel.listChildObjectTypes(type) ){
				assert allKnownTypes.contains(childtype)
			}
		}
	}


}
