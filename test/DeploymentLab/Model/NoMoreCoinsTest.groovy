package DeploymentLab.Model

import DeploymentLab.CentralCatalogue
import DeploymentLab.SelectModel
import DeploymentLab.TestUtils
import DeploymentLab.UndoBuffer
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

import javax.swing.JFrame

import static org.junit.Assert.fail
import DeploymentLab.ProjectSettings
import DeploymentLab.Tools.ConfigurationReplacer
import DeploymentLab.Tree.ConfigurationReplacerTree
import DeploymentLab.Tree.ConfigurationReplacerTreeModel
import DeploymentLab.ComponentLibrary

/**
 * COINs have been removed from the Component Model and the input object is now a member of each Component that can be
 * used by Active Control. These tests make sure that the object model remains unaffected by this change.
 *
 * @author Ken Scoggins
 * @since  Io
 */
class NoMoreCoinsTest {
	private static final List<String> COIN_TYPES = ['controlpoint_single_pressure','controlpoint_single_temperature',
	                                                'controlpoint_single_temperature_mid', 'controlpoint_single_temperature_bot',
	                                                'control_input_strat_tb', 'control_input_strat_tm', 'control_input_strat_mb']

	private static final List<String> COIN_OBJECT_TYPES = ['controlpoint_singlecompare','controlpoint_strat']

	private static final List<String> CONTROL_PRODUCER_TYPES = ['pressure','temperature','temperature_mid','temperature_bot']

	private static final List<String> EXPECTED_NEW_OBJECTS = ['sensorlayercontroller','room']

	private static final String TEST_FILE = "build/testFiles/allThingsWithCoins_v6.3.dlz"

	ComponentModel cmodel = null
	ObjectModel omodel = null
	UndoBuffer undoBuffer = null
	SelectModel selectModel = null
	ProjectSettings ps = null

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		//TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.WARN)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()
		undoBuffer = CentralCatalogue.getUndoBuffer()
		ps = ProjectSettings.getInstance()
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
		selectModel = null
		undoBuffer = null
		ps = null
	}


	/**
	 * Makes sure that the new model is as expected when we create a new project from scratch.
	 */
	@Test
	void modelFromScratch() {
		// Load our baseline project into a different model.
		def (origObjModel,origCompModel,origSelectModel,origUndoBuffer) = TestUtils.initSideModels()

		// Don't provide a ConfigReplacer so it is null and upgrading gets skipped.
		TestUtils.loadProjectFile( TEST_FILE, origUndoBuffer, origObjModel, origCompModel )

		// Make sure we didn't get upgraded. We should have an old-style model with at least one of each COIN type.
		// Do it the hard way since @#$! getComponentsByType is actually a startWith search and controlpoint_single_temperature messes that up.
		List<DLComponent> origCoins = origCompModel.getComponentsInRole('controlinput')
		for( String coinType : COIN_TYPES ) {
			boolean found = false
			for( DLComponent c : origCoins ) {
				if( c.getType() == coinType ) {
					found = true
					break
				}
			}
			assert found, "Loaded test file is missing COIN type '$coinType'"
		}

		// Create the same components from scratch, excluding the COINs. (NOTE: The default loaded model already has a Drawing)
		DLComponent newDrawing = cmodel.getComponentsByType('drawing').first()
		int deprecatedCnt = 0
		for( DLComponent drawing : origCompModel.getComponentsByType('datacenter') ) { // Remember, the old model component was "datacenter"

			// All through this block, keep track of the DLID mapping, much like an upgrade, for later use.
			ps.replaceDlid( drawing.getDlid(), newDrawing.getDlid() )

			// The non-placeables first so the others have their parents.
			for( DLComponent c : origCompModel.getComponentsInDrawing( drawing ) ) {
				if( !c.hasRole('placeable') && (c.getType() != 'datacenter') ) {
					// Zones are deprecated, but we'll convert them to a room for simplicity. Just count all the others since removing them might delete the children.
					if( c.isDeprecated() && c.getType() != 'zone' ) {
						deprecatedCnt++
						continue
					}

					DLComponent newComp = TestUtils.newComponentWithParents( cmodel,
					                                                         (c.getType() == 'zone' ? ComponentType.ROOM : c.getType()),
					                                                         newDrawing )
					ps.replaceDlid( c.getDlid(), newComp.getDlid() )
				}
			}

			// Everything else, except COINs and children that get created when the parent is created.
			for( DLComponent c : origCompModel.getComponents() ) {
				if( c.hasRole('placeable') && !c.hasRole('controlinput') && !c.hasRole('staticchild') ) {
					if( c.isDeprecated() ) {
						// Can't create a deprecated component so skip it and remove from the old model so the compare matches.
						origCompModel.remove( c )
						continue
					}
					DLComponent newComp = TestUtils.newComponentWithParents( cmodel, c.getType(), newDrawing )
					ps.replaceDlid( c.getDlid(), newComp.getDlid() )

					// Don't forget the static children!
					Map<String, Map<String, String>> childrenMap = newComp.getPlaceableChildrenMap();
					for( String childName : childrenMap.keySet() ) {
						DLComponent oldChildComp = c.getChildComponents().find {child -> child.getPropertyValue("child_id").equals(childName)}
						DLComponent newChildComp = newComp.getChildComponents().find {child -> child.getPropertyValue("child_id").equals(childName)}
						ps.replaceDlid( oldChildComp.getDlid(), newChildComp.getDlid() )
					}
				}
			}
		}

		// Should have the same number of components, minus the COINs and deprecated components.
		assert cmodel.getComponents().size() == (origCompModel.getComponents().size() - origCoins.size() - deprecatedCnt)

		// Compare the two models.
		compareModels( origCompModel, origObjModel, cmodel, omodel )
	}


	/**
	 * Makes sure that the new model is as expected when we create a new project from scratch.
	 */
	@Test
	void modelFromUpgrade() {
		// Load our baseline project into a different model.
		def (origObjModel,origCompModel,origSelectModel,origUndoBuffer) = TestUtils.initSideModels()

		// Don't provide a ConfigReplacer so it is null and upgrading gets skipped.
		TestUtils.loadProjectFile( TEST_FILE, origUndoBuffer, origObjModel, origCompModel )

		// Make sure we didn't get upgraded. We should have an old-style model with at least one of each COIN type.
		// Do it the hard way since @#$! getComponentsByType is actually a startWith search and controlpoint_single_temperature messes that up.
		List<DLComponent> origCoins = origCompModel.getComponentsInRole('controlinput')
		for( String coinType : COIN_TYPES ) {
			boolean found = false
			for( DLComponent c : origCoins ) {
				if( c.getType() == coinType ) {
					found = true
					break
				}
			}
			assert found, "Loaded test file is missing COIN type '$coinType'"
		}


		// Load the project again but painfully provide a ConfigReplacer so it gets upgraded.
		def treeModels = []
		def treeModel = new ConfigurationReplacerTreeModel( cmodel, "FakeTree")
		treeModels.add( treeModel )
		ConfigurationReplacerTree ctree = new ConfigurationReplacerTree( treeModel )
		ConfigurationReplacer cr = new ConfigurationReplacer( selectModel, cmodel, ctree, undoBuffer, new JFrame() )
		def clib = ComponentLibrary.INSTANCE;
		clib.init( cmodel, null, treeModels )

		TestUtils.loadProjectFile( TEST_FILE, undoBuffer, omodel, cmodel, cr )

		// Make sure COIN Components are gone.
		for( String coinType : COIN_TYPES ) {
			assert cmodel.getComponentsByType( coinType ).isEmpty()
		}

		// Aireo also purged control devices, strategies, and reins, so count those.
		int purgedCnt = origCompModel.getComponentsInAnyRole(['controlstrategy','controlleddevice','set']).size()

		// Should have the same number of components, minus the COINs.
		assert cmodel.getComponents().size() == (origCompModel.getComponents().size() - origCoins.size() - purgedCnt)

		// Compare the two models.
		compareModels( origCompModel, origObjModel, cmodel, omodel )

		// Make sure the control object properties were carried over.
		for( DLComponent cNew : cmodel.getComponentsInRole('controlinput') ) {
			DLComponent cOld = origCompModel.getComponentFromDLID( ps.findOldDlid( cNew.getDlid() ) )

			for( DLComponent oldCoin : cOld.getProducerConsumers() ) {
				if( oldCoin.hasRole('controlinput') ) {
					DLObject oldObj = oldCoin.getManagedObjects().find { it.getType().startsWith('controlpoint_') }
					assert oldObj != null

					List<DLObject> newObjs = cNew.getObjectsOfType( oldObj.getType() )
					DLObject newObj = null

					if( oldCoin.getType().startsWith('control_input_strat') ) {
						// No strat as of 7.0 so skip this one. Done after searching for the object so we can assert the upgrade excludes them.
						assert newObjs.size() == 0
						continue
					} else if( newObjs.size() == 2 ) {
						// If this is a dual inlet rack, there's 2 sets of objects. We need to grab the correct one.
						String producerId = oldCoin.getConsumer( oldCoin.getType().contains('strat') ? 'input_top' : 'input' ).listProducers().first().b
						if( producerId.startsWith('rackA') ) {
							for( DLObject o : newObjs ) {
								for( DLObject p : o.getParents() ) {
									if( p.getAsName() == 'rack1' ) {
										newObj = o
									}
								}
							}
						} else if( producerId.startsWith('rackB') ) {
							for( DLObject o : newObjs ) {
								for( DLObject p : o.getParents() ) {
									if( p.getAsName() == 'rack2' ) {
										newObj = o
									}
								}
							}
						} else {
							fail("New component has multiple objects that can't be matched up. Old COIN: $oldCoin   New Comp: $cNew   New Objects: $newObjs")
						}
					} else {
						assert newObjs.size() == 1
						newObj = newObjs.first()
					}

					assert oldObj.getKey() == newObj.getKey()

					for( Property p : oldObj.getPropertiesByType("setting") ) {
						Setting oldProp = (Setting) p;

						// Skip position since it is checked later. Skip name because the naming rules are different.
						if( oldProp.getName() in ['position','name'] )
							continue

						// LOL... turns out that the old Bottom COIN had a bug and was not setting status by default, so skip it.
						if( oldCoin.getType() == 'controlpoint_single_temperature_bot' && oldProp.getName() == 'status' && oldProp.getValue() == null ) {
							continue
						}

						if( newObj.hasObjectProperty( oldProp.getName() ) ) {
							Setting newProp = (Setting) newObj.getObjectProperty( oldProp.getName() )

							assert oldProp.getValue() == newProp.getValue()
							assert oldProp.getRealValue() == newProp.getRealValue()
							assert oldProp.getOldValue() == newProp.getOldValue()
							assert oldProp.getOverride() == newProp.getOverride()

							for( CustomTag oldTag : oldProp.getTags() ) {
								if( newProp.hasTag( oldTag.getTagName() ) ) {
									CustomTag newTag = newProp.getTag( oldTag.getTagName() )

									assert oldTag.getValue() == newTag.getValue()
									assert oldTag.getRealValue() == newTag.getRealValue()
									assert oldTag.getOldValue() == newTag.getOldValue()
									assert oldTag.getOverride() == newTag.getOverride()
								}
							}
						}
					}

					// Check the position, which is set depending on the old COIN type.
					String pos = null;
					switch( oldCoin.getType() ) {
						case "controlpoint_single_temperature" :
							pos = "t";
							break;
						case "controlpoint_single_temperature_mid" :
							pos = "m";
							break;
						case "controlpoint_single_temperature_bot" :
							pos = "b";
							break;
						case "control_input_strat_tb" :
							pos = "tb";
							break;
						case "control_input_strat_tm" :
							pos = "tm";
							break;
						case "control_input_strat_mb" :
							pos = "mb";
							break;
					}

					assert newObj.getObjectSetting("position").getValue() == pos

					// Make sure dockingpoints in the new object still point to the same target object.
					for( Property p : oldObj.getPropertiesByType("dockingpoint") ) {
						Dockingpoint oldProp = (Dockingpoint) p;

						assert newObj.hasObjectProperty( oldProp.getName() )

						Dockingpoint newProp = (Dockingpoint) newObj.getObjectProperty( oldProp.getName() )

						assert oldProp.getReferrent().getDlid() == ps.findOldDlid( newProp.getReferrent().getDlid() )
					}

					// Make sure piers in the new object still point to the same object prop.
					for( Property p : oldObj.getPropertiesByType("pier") ) {
						Pier oldProp = (Pier) p;

						// Old model didn't have some of these, but we still get a null property.
						if( oldProp.getReferent() != null ) {
							assert newObj.hasObjectProperty( oldProp.getName() )

							Pier newProp = (Pier) newObj.getObjectProperty( oldProp.getName() )

							// Since the target has a new DLID, ake sure to compare its old id.
							assert oldProp.getReferentObject().getDlid() == ps.findOldDlid( newProp.getReferentObject().getDlid() )
							assert oldProp.getReferentName() == newProp.getReferentName()
						}
					}
				} else {
					println "> Skipping $oldCoin  ${oldCoin.listRoles()}"
				}
			}
		}
	}

	private void compareModels( ComponentModel oldCompModel, ObjectModel oldObjModel,
	                            ComponentModel newCompModel, ObjectModel newObjModel ) {
		int expectedPressureExtras = 0
		int expectedTempExtras = 0
		int expectedAirflowExtras = 0
		Set<DLObject> deprecatedObjs = [] as Set
		for( DLComponent cOld : oldCompModel.getComponents() ) {
			if( cOld.hasRole('placeable') && !cOld.hasRole('controlinput') && !cOld.hasRole('staticchild') ) {
				if( cOld.isDeprecated() ) {
					continue
				}
				DLComponent cNew = newCompModel.getComponentFromDLID( ps.findNewDlid( cOld.getDlid() ) )
				assert cNew != null, "Not found in new model: $cOld"

				// Based on the control producers on the original version, check the new component for the correct roles and control objects.
				boolean hasPressure = false
				boolean hasTemp = false
				boolean hasStrat = false
				for( Producer p : cOld.getAllProducers() ) {
					switch( p.getDatatype() ) {
						case 'pressure' :
							hasPressure = true
							List<DLObject> pObjs = cNew.getManagedObjectsOfType('controlpoint_singlecompare')
							assert pObjs.size() == 1, "Original Component: $cOld  $pObjs"
							assert pObjs.first().getObjectProperty('resource').getValue() == 'pressure', "Original Component: $cOld  $pObjs"
							break

						case 'temperature' :
						case 'temperature_mid' :
						case 'temperature_bot' :
							String resource = null
							List<DLObject> pObjs = []
							if( !hasTemp ) {
								hasTemp = true
								resource = 'temperature'
								pObjs = cNew.getManagedObjectsOfType('controlpoint_singlecompare')

							}
							/* As of 7.0, there is no strat so keep
							else if( !hasStrat ) {
								// At least two temps, so it is strat capable. Except a couple special cases.
								if( !cNew.getManagedObjectsOfType('generictemperature') ) {
									hasStrat = true
									resource = 'airflow'
									pObjs = cNew.getManagedObjectsOfType('controlpoint_strat')
								}

							}
							*/

							if( resource != null ) {
								// Need to know how many to expect since some components have more than one.
								int expectedObjs = 1
								List<DLObject> racks = cNew.getManagedObjectsOfType('rack')
								if( racks.size() > 1 ) {
									expectedObjs = racks.size()
								}

								assert pObjs.size() == expectedObjs, "Original Component: $cOld  '$resource'  $pObjs"
								pObjs.each { assert it.getObjectProperty('resource').getValue() == resource, "Original Component: $cOld  '$resource'  $pObjs" }
							}
							break
					}
				}

				// Make sure there's NOT an object when there is not that role.
				if( !hasPressure ) {
					List<DLObject> pObjs = cNew.getManagedObjectsOfType('controlpoint_singlecompare')
					assert pObjs.isEmpty() || pObjs.first().getObjectProperty('resource').getValue() == 'temperature', "Original Component: $cOld  $pObjs"
				}

				if( !hasTemp ) {
					List<DLObject> pObjs = cNew.getManagedObjectsOfType('controlpoint_singlecompare')
					assert pObjs.isEmpty() || pObjs.first().getObjectProperty('resource').getValue() == 'pressure', "Original Component: $cOld  $pObjs"
				}

				if( !hasStrat ) {
					assert cNew.getManagedObjectsOfType('controlpoint_strat').isEmpty(), "Original Component: $cOld"
				}

				// We'll expect some extra objects in the new model. Just count them so we can check later.
				expectedPressureExtras += countMissingCoins( cOld, cNew, 'pressurecontrol' )
				expectedTempExtras     += countMissingCoins( cOld, cNew, 'temperaturecontrol' )
				// None as of 7.0:    expectedAirflowExtras  += countMissingCoins( cOld, cNew, 'airflowcontrol' )

				// Don't forget the static children!
				Map<String, Map<String, String>> childrenMap = cNew.getPlaceableChildrenMap();

				for( String childName : childrenMap.keySet() ) {
					DLComponent oldChildComp = cOld.getChildComponents().find {child -> child.getPropertyValue("child_id").equals(childName)}
					DLComponent newChildComp = cNew.getChildComponents().find {child -> child.getPropertyValue("child_id").equals(childName)}

					expectedPressureExtras += countMissingCoins( oldChildComp, newChildComp, 'pressurecontrol' )
					expectedTempExtras     += countMissingCoins( oldChildComp, newChildComp, 'temperaturecontrol' )
					// None as of 7.0:    expectedAirflowExtras  += countMissingCoins( oldChildComp, newChildComp, 'airflowcontrol' )
				}
			} else if( !cOld.hasRole('placeable') && (cOld.getType() != 'datacenter') ) {
				// TODO: I don't think all the deprecatedObjs stuff was the correct solution, along with skipping deprecated in the above if block. Maybe rework.
				if( cOld.isDeprecated() || !cmodel.isLoadedComponentType( cOld.getType() ) ) {
					deprecatedObjs += cOld.getManagedObjects()
				}
			} else if( cOld.hasRole('controlinput') ) {
				// If this is only associated with deprecated components, it won't get handled correctly when doing the
				// object checks. So make sure they get on the deprecated list.
				boolean abandoned = true
				for( DLComponent c : cOld.getAllAssociatedComponents() ) {
					if( !c.isDeprecated() ) {
						abandoned = false
						break
					}
				}

				if( abandoned ) {
					deprecatedObjs += cOld.getManagedObjects()
				}
			}
		}

		// The new design should result in the same object model. Do a simple check to make sure the objects are there.
		java.util.Collection<DLObject> newObjs = new ArrayList<DLObject>( newObjModel.getObjects() )
		for( DLObject origObj : oldObjModel.getObjects() ) {

			// Except that we no longer have Strat COIN displaypoints or controlpoint_strat. So skip those. And skip deprecated objects.
			if( ((origObj.getType() == 'displaypoint') && (origObj.getName() == 'stratlayer')) ||
			    (origObj.getType() == 'controlpoint_strat') ||
			    deprecatedObjs.contains( origObj ) ) {
				continue
			}

			boolean found = false
			Iterator<DLObject> it = newObjs.iterator()
			while( it.hasNext() && !found ) {
				DLObject newObj = it.next()
				if( newObj.getType() == origObj.getType() ) {
					if( COIN_OBJECT_TYPES.contains( newObj.getType() ) ) {
						if( newObj.getObjectProperty('resource').getValue() == origObj.getObjectProperty('resource').getValue() ) {
							found = true
							it.remove()
						}
					} else if( newCompModel.getManagingComponent( newObj ).getType() ==
					           CentralCatalogue.getComponentConversion( oldCompModel.getManagingComponent( origObj ).getType() ) ) {
						found = true
						it.remove()
					}
				}
			}
			assert found, "Object not in new model '${origObj.getType()}'. Component: ${oldCompModel.getManagingComponent( origObj )}"
		}

		// All have been found. The new model has control objects embedded, so we should now have one left over for each
		// component in the original model that had no COIN attached to it.
		for( DLObject obj : newObjs ) {
			if( COIN_OBJECT_TYPES.contains( obj.getType() ) ) {
				String resourceType = obj.getObjectProperty('resource').getValue()
				switch( resourceType ) {
					case 'pressure' : expectedPressureExtras--; break;
					case 'temperature' : expectedTempExtras--; break;
					case 'airflow' : expectedAirflowExtras--; break;
					default:
						fail("Unknown COIN resource: $resourceType")
				}
			} else if( !EXPECTED_NEW_OBJECTS.contains( obj.getType() ) ) {
				// We expected certain non-coin related leftovers, but this ain't one of them.
				fail("Unexpected extra object in new model: $obj  ${newCompModel.getOwner(obj)}")
			}
		}

		// All objects should be accounted for now.
		assert expectedPressureExtras == 0
		assert expectedTempExtras == 0
		assert expectedAirflowExtras == 0

/*
		try {
			StringWriter stringWriter = new StringWriter()
			def builder = new groovy.xml.MarkupBuilder(stringWriter)
			builder.model('version': '1.0') {
				newObjModel.serialize(builder)
				newCompModel.serialize(builder)
			}
			println "${stringWriter.toString()}"
		} catch( Exception e ) {
			println "--> $e"
		}
*/
		// The objects are there, but are they hooked-up correctly! Should point to a sensor on the same component.
		for( DLObject obj : newObjModel.getObjectsByType('controlpoint_singlecompare') ) {
			DLObject sensor = obj.getObjectProperty('sensor')?.getReferrent()
			assert sensor != null
			assert newCompModel.getManagingComponent( obj ) == newCompModel.getManagingComponent( sensor )
		}

		for( DLObject obj : newObjModel.getObjectsByType('controlpoint_strat') ) {
			DLObject topSensor = obj.getObjectProperty('top')?.getReferrent()
			DLObject botSensor = obj.getObjectProperty('bottom')?.getReferrent()
			assert topSensor != null
			assert botSensor != null
			assert newCompModel.getManagingComponent( obj ) == newCompModel.getManagingComponent( topSensor )
			assert newCompModel.getManagingComponent( obj ) == newCompModel.getManagingComponent( botSensor )
		}
	}


	private int countMissingCoins( DLComponent cOld, DLComponent cNew, String dtype ) {
		int missingCoins = 0
		if( cNew.hasRole( dtype ) ) {

			// Some components have multiple racks, so figure out how many COINs the component could have.
			int allowedCoins = 1
			List<DLObject> racks = cNew.getManagedObjectsOfType('rack')
			if( racks.size() > 1 ) {
				allowedCoins = racks.size()
			}

			// Count how many of those were not in use since the new model will have an extra unused object for it.
			// NOTE: unique() since the same strat component will be attached to two producers.
			missingCoins = ( allowedCoins - cOld.getProducerConsumersOfRole( dtype ).unique().size() )

			// Just a sanity check since this found a bug in the test logic during development.
			assert missingCoins in 0..2, "$cNew  $dtype  ${cOld.getProducerConsumersOfRole( dtype )}"
		} else {
			// Original model has a COIN on this component when there shouldn't be one!
			assert cOld.getProducerConsumersOfRole( dtype ).isEmpty(), "Unexpected $dtype COIN: $cOld"
		}
		return missingCoins
	}
}
