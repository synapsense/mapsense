package DeploymentLab.Model

import org.junit.BeforeClass
import DeploymentLab.TestUtils
import org.junit.Before
import javax.swing.JFrame
import org.junit.After
import org.junit.Test

/**
 * Tests the various custom comparators.
 * @author Gabriel Helman
 * @since Jupiter
 * Date: 6/1/12
 * Time: 2:00 PM
 *
 * @see DLObjectDlidComparator
 * @see DLObjectComparator
 */
public class ComparatorTest {

	ComponentModel cmodel = null
	ObjectModel omodel = null

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
	}
	
	
	@Test
	void sortByDLIDOrder(){
		def components = new HashSet<>()
		for(int i = 0; i < 20; i++){
			def rack = cmodel.newComponent("rack-control-toprearexhaust-nsf-rt-thermanode2")
			rack.setPropertyValue("name", "TRE-RACK $i")
			def crah = cmodel.newComponent("crah-single-plenumrated-thermanode")
			crah.setPropertyValue("name", "CRAH==$i")
			components.add(rack)
			components.add(crah)
		}
		def objectsSet = new HashSet<>(omodel.getObjects())
		def objectsList = new ArrayList<>(objectsSet)

		//Collections.sort(objectsList,new DLObjectDlidComparator())
		def sorted = new TreeSet<DLObject>(new DLObjectDlidComparator())
		sorted.addAll(objectsList)
		int last = 0
		for( DLObject o : sorted ){
			int current = o.getDlid().hashCode()
			assert current > last;
			last = current
		}
	}


	@Test
	void sortByObject(){
		def components = new HashSet<>()
		for(int i = 0; i < 20; i++){
			def rack = cmodel.newComponent("rack-control-toprearexhaust-nsf-rt-thermanode2")
			rack.setPropertyValue("name", "TRE-RACK $i")
			def crah = cmodel.newComponent("crah-single-plenumrated-thermanode")
			crah.setPropertyValue("name", "CRAH==$i")
			components.add(rack)
			components.add(crah)
		}
		def objectsSet = new HashSet<DLObject>(omodel.getObjects())
		def objectsList = new ArrayList<DLObject>(objectsSet)

		def allNames = []
		objectsList.each {
			allNames.add( it.getName() )
		}

		Collections.sort(allNames)
		Collections.sort(objectsList,new DLObjectComparator())
		
		for(int i = 0; i < allNames.size(); i ++){
			assert allNames[i].equals(objectsList[i].getName())
		}
	}
	
	
}
