package DeploymentLab.Model

import DeploymentLab.TestUtils
import javax.swing.JFrame
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

/**
 * Checks to make sure we can set all properties of every component.
 *
 * Enforces that string component props that assign to number fields defautl to 0, not blank.
 * Checks for wacky exceptions caused by bad plumbing.
 * Also insane rounding errors, etc.
 *
 * TODO: Extend this to check that the correct value landed in the object model.
 *
 * @author Gabriel Helman
 * @since Jupiter
 * Date: 4/6/12
 * Time: 3:56 PM
 */
class SetAllPropertiesTest {

	private static final String TEXTVALUE = "The trouble with computers, of course, is that they're very sophisticated idiots."
	private static final Integer INTVALUE = 42
	private static final Double DOUBLEVALUE = 42.0
	private static final Boolean MOLTAR_YES = true

	private static final List complexButOkay = ["java.lang.Boolean", "DeploymentLab.MultiLineContents"]

	ComponentModel cmodel = null
	ObjectModel omodel = null

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
	}

	@Test
	void setEverySingleProperty() {

		Set<String> componentTypes = cmodel.listComponentTypes()
		for (String t: componentTypes.sort()) {
			println "testing TYPE $t"
			DLComponent candidate = cmodel.newComponent(t)

			for (String p: candidate.listProperties()) {
				ComponentProperty prop = candidate.getComponentProperty(p)
				if (prop.isEditable() && prop.isDisplayed() && (!PropertyConverter.isComplex(prop.getType()) && !complexButOkay.contains(prop.getType()))) {
					println "testing ${prop.getDisplayName()}"
					if(prop.getValueChoices()){
						println "testing valuechoices"
						for(def choice : prop.getValueChoices()){
							def val = prop.valueForChoice( choice )
							candidate.setPropertyValue(p,val)
							assert candidate.getPropertyValue(p) == val
							assert candidate.getPropertyStringValue(p) == val.toString()
						}
					} else {

					switch (prop.getType()) {
						case "java.lang.String":
						case "DeploymentLab.MultiLineContents":
							if (candidate.listMacIdProperties().contains(p) || candidate.getPropertyStringValue(p).equals("0")) {
								candidate.setPropertyValue(p, INTVALUE)
								assert candidate.getPropertyValue(p).equals(INTVALUE.toString())
								assert candidate.getPropertyStringValue(p).equals(INTVALUE.toString())
							} else {
								candidate.setPropertyValue(p, TEXTVALUE)
								assert candidate.getPropertyStringValue(p).equals(TEXTVALUE)
							}
							break
						case "java.lang.Integer":
						case "java.lang.Short":
						case "java.lang.Long":
							candidate.setPropertyValue(p, INTVALUE)
							assert candidate.getPropertyValue(p).equals(INTVALUE)
							assert candidate.getPropertyStringValue(p).equals(INTVALUE.toString())
							break

						case "java.lang.Float":
						case "java.lang.Double":
							candidate.setPropertyValue(p, DOUBLEVALUE)
							assert candidate.getPropertyValue(p).equals(DOUBLEVALUE)
							assert candidate.getPropertyStringValue(p).equals(DOUBLEVALUE.toString())
							break

						case "java.lang.Boolean":
							candidate.setPropertyValue(p, MOLTAR_YES)
							assert candidate.getPropertyValue(p).equals(MOLTAR_YES)
							assert candidate.getPropertyStringValue(p).equals(MOLTAR_YES.toString())
							break
					}
					}
				}
			}
		}
	}

}
