package DeploymentLab.Model;

import DeploymentLab.*
import DeploymentLab.SceneGraph.DeploymentPanel;
import DeploymentLab.Tools.ToolManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass
import org.junit.Test;

import javax.swing.*
import java.awt.geom.Point2D;

/**
 * Created by LNI on 2/10/2016.
 */
public class RoomControllerTest {

    static ObjectModel omodel
    static ComponentModel cmodel
    static SelectModel selectModel = null
    static UndoBuffer undoBuffer
//    DeploymentLabWindow w
    static RoomController rc
    DeploymentPanel dp
    def newDrawing
    def newRoom
    def rack

    @BeforeClass
    static void classSetup() {
        // Turn on error logging to help when something fails internal to a method we are testing.
        TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.WARN)
    }

    @Before
    void setup() {
        (omodel,cmodel) = TestUtils.initModels()
        selectModel = CentralCatalogue.getSelectModel()
        undoBuffer = CentralCatalogue.getUndoBuffer()

       dp = new DeploymentPanel(selectModel, undoBuffer, cmodel, new ToolManager(new ButtonGroup()), new DisplayProperties( selectModel ))

        rc = new RoomController(cmodel, selectModel, dp)

        newDrawing = cmodel.newComponent('drawing')
        def newNet = cmodel.newComponent("network")
        newDrawing.addChild( newNet )
    }

    @After
    void tearDown() {
        cmodel = null
        omodel = null
        selectModel = null
    }

    private void createRack() {
        rack = cmodel.newComponent("ez-rack-1-0")
        newRoom.addChild(rack)
        dp.childAdded(newRoom, rack)
    }

    private void createRoom() {
        newRoom = cmodel.newComponent("room")
        newDrawing.addChild(newRoom)
        dp.childAdded(newDrawing, newRoom)
    }

    @Test
    public void componentInRoomTest() {
        String roomPoints = "643.0,333.0;631.0,526.0;1028.0,681.0;1074.0,338.0"
        Double rackX = 765.0
        Double rackY = 441.0

        createRoom()
        createRack()
        roomPropertyChangedCall(roomPoints)
        componentPropertyChangedCall(rackX, rackY)
        println "ROOM POLYGON POINTS:" + newRoom.getPolygonPoints()

        assert !hasOrphans()
        assert componentsInRoom()
    }

    @Test
    public void roomPointAtZeroZeroTest() {
        String roomPoints = "0.0,0.0;631.0,526.0;1028.0,681.0;1074.0,338.0"
        Double rackX = 765.0
        Double rackY = 441.0

        createRoom()
        createRack()
        roomPropertyChangedCall(roomPoints)
        componentPropertyChangedCall(rackX, rackY)
        println "ROOM POLYGON POINTS:" + newRoom.getPolygonPoints()

        assert !hasOrphans()
        assert componentsInRoom()
    }

    @Test
    public void componentNotInRoomTest() {
        String roomPoints = "0.0,0.0;631.0,526.0;1028.0,681.0;1074.0,338.0"
        Double rackX = 1200.0
        Double rackY = 1200.0

        createRoom()
        createRack()
        roomPropertyChangedCall(roomPoints)
        componentPropertyChangedCall(rackX, rackY)
        println "ROOM POLYGON POINTS:" + newRoom.getPolygonPoints()

        assert hasOrphans()
        assert !componentsInRoom()
    }

    @Test
    public void componentIntersectsRoomTest() {
        String roomPoints = "643.0,330.0;631.0,526.0;1028.0,681.0;1074.0,338.0"
        Double rackX = 631.0
        Double rackY = 526.0

        createRoom()
        createRack()
        roomPropertyChangedCall(roomPoints)
        componentPropertyChangedCall(rackX, rackY)
        println "ROOM POLYGON POINTS:" + newRoom.getPolygonPoints()

        assert !hasOrphans()
        assert componentsInRoom()
    }

    @Test
    public void addComponentFirstRoomSecondTest() {
        rack = cmodel.newComponent("ez-rack-1-0")
        assert !componentsInRoom()

        newRoom = cmodel.newComponent("room")
        newDrawing.addChild(newRoom)
        dp.childAdded(newDrawing, newRoom)
        newRoom.addChild(rack)
        dp.childAdded(newRoom, rack)

        String roomPoints = "0.0,0.0;631.0,526.0;1028.0,681.0;1074.0,338.0"
        Double rackX = 765.0
        Double rackY = 441.0

        roomPropertyChangedCall(roomPoints)
        componentPropertyChangedCall(rackX, rackY)
        println "ROOM POLYGON POINTS:" + newRoom.getPolygonPoints()

        assert !hasOrphans()
        assert componentsInRoom()
    }

    private void roomPropertyChangedCall(String roomPoints) {
        newRoom.setPropertyValue("points", roomPoints)
        rc.roomPropertyChanged(newRoom, "points", "", roomPoints)
    }

    private void componentPropertyChangedCall(Double componentX, Double componentY) {
        rack.setPropertyValue("x", componentX)
        rack.setPropertyValue("y", componentY)
        rc.componentPropertyChanged(rack, "x", 0.0, componentX)
        rc.componentPropertyChanged(rack, "y", 0.0, componentY)
    }

    private boolean hasOrphans() {
        for( DLComponent orphanage : cmodel.getComponentsByType( ComponentType.ORPHANAGE ) ) {
            if (orphanage.getChildComponents().size() > 0) {
                return true
            } else {
                return false
            }
        }
    }

    private boolean componentsInRoom() {
        for( DLComponent room : cmodel.getComponentsByType( ComponentType.ROOM ) ) {
            if (room.getChildComponents().size() > 0) {
                return true
            } else {
                return false
            }
        }
    }
}