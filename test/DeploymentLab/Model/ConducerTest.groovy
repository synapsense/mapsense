package DeploymentLab.Model;


import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import DeploymentLab.TestUtils


/**
 * Tests the Conducer class as well as the Conducer versions of the DLComponent associate and unassociate methods.
 *
 * @author Ken Scoggins
 * @since Jupiter
 */
public class ConducerTest {

    private static final String MONO_PARTNER_A = "rack-control-rearexhaust-sf-thermanode2"
    private static final String MONO_PARTNER_B = "standalone_power_rack"
    private static final String MONO_CONDUCER  = "PEBridge"

    private static final String POLY_SWINGER   = "control_rtu_manager_modbus"
    private static final String POLY_EXCLUSIVE = "control_zone_damper"
    private static final String POLY_SWINGER_CONDUCER = "RTUMgrChildLink"
    private static final String POLY_EXCLUSIVE_CONDUCER = "ZoneDamperParentLink"

    ObjectModel omodel
    ComponentModel cmodel


    /**
     * One-time setup called before all tests run.
     */
    @BeforeClass()
    static void classSetup() {

    }

    /**
     * Setup called before each test.
     */
    @Before
    void setup() {
        (omodel,cmodel) = TestUtils.initModels()
    }

    /**
     * Cleanup called after each test.
     */
    @After
    void tearDown() {
        cmodel = null
        omodel = null
    }

    /**
     * One-time cleanup called after all tests run.
     */
    @AfterClass()
    static void classTearDown() {

    }

    /**
     * Tests non-group conducers that can only ever have 1:1 associations.
     */
    @Test
    void testMonogamousConducers() {

        // Create the components.
        DLComponent a1 = cmodel.newComponent( MONO_PARTNER_A )
        DLComponent b1 = cmodel.newComponent( MONO_PARTNER_B )
        DLComponent a2 = cmodel.newComponent( MONO_PARTNER_A )
        DLComponent b2 = cmodel.newComponent( MONO_PARTNER_B )

        assert a1
        assert b1
        assert a2
        assert b2

        // Get their conducers.
        Conducer a1Conducer = a1.getConducer( MONO_CONDUCER )
        Conducer b1Conducer = b1.getConducer( MONO_CONDUCER )
        Conducer a2Conducer = a2.getConducer( MONO_CONDUCER )
        Conducer b2Conducer = b2.getConducer( MONO_CONDUCER )

        assert a1Conducer
        assert b1Conducer
        assert a2Conducer
        assert b2Conducer

        assert ! a1Conducer.hasProsumer()
        assert ! b1Conducer.hasProsumer()
        assert ! a2Conducer.hasProsumer()
        assert ! b2Conducer.hasProsumer()

        assert a1Conducer.canProsume( b1Conducer )
        assert b1Conducer.canProsume( a1Conducer )
        assert a2Conducer.canProsume( b2Conducer )
        assert b2Conducer.canProsume( a2Conducer )

        assert a1Conducer.canProsume( b1 )
        assert b1Conducer.canProsume( a1 )
        assert a2Conducer.canProsume( b2 )
        assert b2Conducer.canProsume( a2 )

        // Normal associations: A1<->B1 and A2<->B2
        a1.associate( a1Conducer, b1, b1Conducer )
        a2.associate( a2Conducer, b2, b2Conducer )

        assert a1Conducer.isProsuming( b1Conducer )
        assert b1Conducer.isProsuming( a1Conducer )
        assert a2Conducer.isProsuming( b2Conducer )
        assert b2Conducer.isProsuming( a2Conducer )

        assert a1Conducer.isProsuming( b1 )
        assert b1Conducer.isProsuming( a1 )
        assert a2Conducer.isProsuming( b2 )
        assert b2Conducer.isProsuming( a2 )

        // A1 tries to hookup with an old friend. A1<->B2
        a1.associate( a1Conducer, b2, b2Conducer )

        // That should work...
        assert a1Conducer.isProsuming( b2Conducer )
        assert b2Conducer.isProsuming( a1Conducer )
        assert a1Conducer.isProsuming( b2 )
        assert b2Conducer.isProsuming( a1 )

        // And their spouses, A2 & B1, took off with the house and kids and half of the Garbage Pail Kids collection.
        assert ! a2Conducer.hasProsumer()
        assert ! b1Conducer.hasProsumer()

        // In the long run, it doesn't work out anyway
        a1.unassociate( a1Conducer, b2, b2Conducer )
        assert ! a1Conducer.hasProsumer()
        assert ! b2Conducer.hasProsumer()
    }


    /**
     * Tests 1:M group conducers, which can have multiple associations. There is currently no Conducer that allows M:M, so
     * this test is for a 1:M setup.
     */
    @Test
    void testPolygamousConducers() {

        // Create the components.
        DLComponent a1 = cmodel.newComponent( POLY_SWINGER )
        DLComponent b1 = cmodel.newComponent( POLY_EXCLUSIVE )
        DLComponent a2 = cmodel.newComponent( POLY_SWINGER )
        DLComponent b2 = cmodel.newComponent( POLY_EXCLUSIVE )

        assert a1
        assert b1
        assert a2
        assert b2

        // Get their conducers.
        Conducer a1Conducer = a1.getConducer( POLY_SWINGER_CONDUCER )
        Conducer b1Conducer = b1.getConducer( POLY_EXCLUSIVE_CONDUCER )
        Conducer a2Conducer = a2.getConducer( POLY_SWINGER_CONDUCER )
        Conducer b2Conducer = b2.getConducer( POLY_EXCLUSIVE_CONDUCER )

        assert a1Conducer
        assert b1Conducer
        assert a2Conducer
        assert b2Conducer

        assert ! a1Conducer.hasProsumer()
        assert ! b1Conducer.hasProsumer()
        assert ! a2Conducer.hasProsumer()
        assert ! b2Conducer.hasProsumer()

        assert a1Conducer.canProsume( b1Conducer )
        assert b1Conducer.canProsume( a1Conducer )
        assert a2Conducer.canProsume( b2Conducer )
        assert b2Conducer.canProsume( a2Conducer )

        assert a1Conducer.canProsume( b1 )
        assert b1Conducer.canProsume( a1 )
        assert a2Conducer.canProsume( b2 )
        assert b2Conducer.canProsume( a2 )

        // Normal associations: A1<->B1 and A2<->B2
        a1.associate( a1Conducer, b1, b1Conducer )
        a2.associate( a2Conducer, b2, b2Conducer )

        assert a1Conducer.isProsuming( b1Conducer )
        assert b1Conducer.isProsuming( a1Conducer )
        assert a2Conducer.isProsuming( b2Conducer )
        assert b2Conducer.isProsuming( a2Conducer )

        assert a1Conducer.isProsuming( b1 )
        assert b1Conducer.isProsuming( a1 )
        assert a2Conducer.isProsuming( b2 )
        assert b2Conducer.isProsuming( a2 )

        // A1 tries to hookup with an old friend. A1<->B2
        a1.associate( a1Conducer, b2, b2Conducer )

        // That should work...
        assert a1Conducer.isProsuming( b2Conducer )
        assert b2Conducer.isProsuming( a1Conducer )
        assert a1Conducer.isProsuming( b2 )
        assert b2Conducer.isProsuming( a1 )

        // A1's spouse is cool with it, so they are still hooked-up.
        assert a1Conducer.isProsuming( b1Conducer )
        assert b1Conducer.isProsuming( a1Conducer )
        assert a1Conducer.isProsuming( b1 )
        assert b1Conducer.isProsuming( a1 )

        assert a1Conducer.getProsumers().size() == 2
        assert b1Conducer.getProsumers().size() == 1

        // But the old friend left their partner alone with the kids and the house.
        assert ! a2Conducer.isProsuming( b2Conducer )
        assert ! b2Conducer.isProsuming( a2Conducer )
        assert ! a2Conducer.isProsuming( b2 )
        assert ! b2Conducer.isProsuming( a2 )

        assert a2Conducer.getProsumers().size() == 0
        assert b2Conducer.getProsumers().size() == 1

        // Eventually, the original gets tired and leaves, but the new friend sticks around.
        b1.unassociate( b1Conducer, a1, a1Conducer )

        assert ! a1Conducer.isProsuming( b1Conducer )
        assert ! b1Conducer.isProsuming( a1Conducer )
        assert ! a1Conducer.isProsuming( b1 )
        assert ! b1Conducer.isProsuming( a1 )

        assert a1Conducer.isProsuming( b2Conducer )
        assert b2Conducer.isProsuming( a1Conducer )
        assert a1Conducer.isProsuming( b2 )
        assert b2Conducer.isProsuming( a1 )

        assert a1Conducer.getProsumers().size() == 1
        assert b2Conducer.getProsumers().size() == 1
        assert b1Conducer.getProsumers().size() == 0

        // Then the new friend takes off and everyone is lonely.
        b2.unassociate( b2Conducer, a1, a1Conducer )

        assert ! a1Conducer.hasProsumer()
        assert ! b1Conducer.hasProsumer()
        assert ! a2Conducer.hasProsumer()
        assert ! b2Conducer.hasProsumer()
    }

    /**
     * Tests M:M group conducers.
     */
    @Test
    void testSwingerConducers() {
        // None exist yet.
    }

	/**
	 * Tests the overloaded method hackery.
	 */
	@Test
	void testOverloadedMethodHackeryValid() {
		DLComponent a1 = cmodel.newComponent( MONO_PARTNER_A )
		DLComponent b1 = cmodel.newComponent( MONO_PARTNER_B )
		Conducer a1Conducer = a1.getConducer( MONO_CONDUCER )
		Conducer b1Conducer = b1.getConducer( MONO_CONDUCER )

		a1.associate( a1Conducer, b1, b1Conducer )

		assert a1Conducer.isProsuming( b1Conducer )

		// isProsuming with a null should return false, regardless of the variable type.
		Conducer nullCon = null
		DLComponent nullDlc = null

		assert !a1Conducer.isProsuming( nullCon )
		assert !a1Conducer.isProsuming( nullDlc )
		assert !a1Conducer.isProsuming( (Conducer) null )
		assert !a1Conducer.isProsuming( (DLComponent) null )
	}

	/**
	 * Tests that the overloaded hackery still results in a Missing Method Exception if you try to use a type that
	 * that wasn't supported by the overloaded methods prior to the hackery.
	 */
	@Test(expected = MissingMethodException.class)
	void testOverloadedMethodHackeryInvalid() {
		DLComponent a1 = cmodel.newComponent( MONO_PARTNER_A )
		Conducer a1Conducer = a1.getConducer( MONO_CONDUCER )
		assert a1Conducer.isProsuming("Howdy, partner!")
	}
}
