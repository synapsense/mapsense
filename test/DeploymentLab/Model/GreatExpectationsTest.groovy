package DeploymentLab.Model

import DeploymentLab.CentralCatalogue
import DeploymentLab.TestUtils
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

import javax.swing.*

/**
 * Checks to make sure that a component has whatever is expected of it. For example, a WSN Node must have a
 * manualWSNConfig property or bad things happen.
 *
 * @author Ken Scoggins
 * @since Aireo
 */
class GreatExpectationsTest {

	ComponentModel cmodel = null
	ObjectModel omodel = null

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
	}

	@Test
	void wsnRequirements() {
		for( String ctype : cmodel.listComponentTypes() ) {
			if( ctype != ComponentType.WSN_GATEWAY && ComponentType.WSN_NETWORK in cmodel.listParentComponentsOfType( ctype ) ) {
				assert cmodel.getTypeDefaultValue( ctype, 'manualWSNConfig') != ""
			}
		}
	}

	// TODO: Grow this test.
}
