package DeploymentLab.Model;


import DeploymentLab.ProblemLogger
import DeploymentLab.TestUtils
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.channellogger.LogLevel
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

/**
 *
 * Test the Component Bindings.
 *
 * At the moment, the tests have a certain "copy the expressions out of the XML and compare" quality, but this allows us
 * to start messing with the internal implementation (which, as of this writing, is planned.)
 *
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/18/12
 * Time: 10:42 AM
 */
public class ComponentBindingTest {

	static ObjectModel omodel
	static ComponentModel cmodel

	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.WARN)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))
	}


	@After
	void tearDown() {
		cmodel = null
		omodel = null
		//have fun, garbage collector!
	}

	private static final String TESTNAME = "DEEP THOUGHT"
	private static final String TESTLOC = "The Lost Planet of Magrathea"

	@Test
	public void setPropertyValue() {
		def wob = cmodel.newComponent("sum_operation")
		def w = wob.getObject("math")

		assert !wob.getPropertyStringValue("name").equals(TESTNAME)
		assert !w.getObjectSetting("name").getValue().equals(TESTNAME)

		wob.setPropertyValue("name", TESTNAME)
		assert wob.getPropertyStringValue("name").equals(TESTNAME)
		assert w.getObjectSetting("name").getValue().equals(TESTNAME)


	}

	@Test
	public void testListVariables() throws Exception {
		def wob = cmodel.newComponent("sum_operation")
		ComponentBinding b = wob._varMap["name"].first()
		assert b.listVariables() == ["name"]
	}

	/*
	//serialization should all get tested somewhere else
	public void testSerialize() {}


	//eval gets tested via the setPropertyValue
	public void testEval(){}
	*/

	@Test
	public void testGetTargetSettings() throws Exception {
		def wob = cmodel.newComponent("sum_operation")
		ComponentBinding b = wob._varMap["name"].first()
		def w = wob.getObject("math")
		def oname = w.getObjectSetting("name")
		assert b.getTargetSettings() == [oname]
	}

	@Test
	public void moreThanOneTarget() {
		def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode2")
		def r = rack.getObject("rack")
		def node = rack.getObject("node")

		//there are two name bindings, one for each object
		rack.setPropertyValue("name", TESTNAME)
		assert r.getObjectSetting("name").getValue().equals(TESTNAME)
		assert node.getObjectSetting("name").getValue().equals("Node $TESTNAME".toString())

		//there is only one location binding, but it goes to both objects
		rack.setPropertyValue("location", TESTLOC)
		assert r.getObjectSetting("location").getValue().equals(TESTLOC)
		assert node.getObjectSetting("location").getValue().equals(TESTLOC)
	}

	private static final Double X = 100
	private static final Double Y = 100

	@Test
	public void sensorLocations() {
		def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode2")
		def r = rack.getObject("rack")
		def node = rack.getObject("node")

		def sensors = ((ChildLink) node.getObjectProperty("sensepoints")).getChildren()
		Map<Integer, DLObject> channels = [:]
		sensors.each {
			//println it
			channels[it.getObjectSetting("channel").getValue()] = it
		}
		//println channels

		//all the sensors and the rack get various locations based on what we just did:
		rack.setPropertyValue("x", X)
		rack.setPropertyValue("y", Y)

		def width = rack.getPropertyValue("width")
		def depth = rack.getPropertyValue("depth")
		def rotation = rack.getPropertyValue("rotation")

		//the same: node, rack, ch2
		assert r.getObjectSetting("x").getValue() == X
		assert r.getObjectSetting("y").getValue() == Y
		assert node.getObjectSetting("x").getValue() == X
		assert node.getObjectSetting("y").getValue() == Y
		assert channels[2].getObjectSetting("x").getValue() == X
		assert channels[2].getObjectSetting("y").getValue() == Y

		//front: channels 0,1,3,4,5,9
		assert channels[0].getObjectSetting("x").getValue() == X + (depth / 2 * Math.cos(Math.toRadians(-rotation)))
		assert channels[0].getObjectSetting("y").getValue() == Y + (depth / 2 * Math.sin(Math.toRadians(-rotation)))
		assert channels[1].getObjectSetting("x").getValue() == X + (depth / 2 * Math.cos(Math.toRadians(-rotation)))
		assert channels[1].getObjectSetting("y").getValue() == Y + (depth / 2 * Math.sin(Math.toRadians(-rotation)))
		assert channels[3].getObjectSetting("x").getValue() == X + (depth / 2 * Math.cos(Math.toRadians(-rotation)))
		assert channels[3].getObjectSetting("y").getValue() == Y + (depth / 2 * Math.sin(Math.toRadians(-rotation)))
		assert channels[4].getObjectSetting("x").getValue() == X + (depth / 2 * Math.cos(Math.toRadians(-rotation)))
		assert channels[4].getObjectSetting("y").getValue() == Y + (depth / 2 * Math.sin(Math.toRadians(-rotation)))
		assert channels[5].getObjectSetting("x").getValue() == X + (depth / 2 * Math.cos(Math.toRadians(-rotation)))
		assert channels[5].getObjectSetting("y").getValue() == Y + (depth / 2 * Math.sin(Math.toRadians(-rotation)))
		assert channels[9].getObjectSetting("x").getValue() == X + (depth / 2 * Math.cos(Math.toRadians(-rotation)))
		assert channels[9].getObjectSetting("y").getValue() == Y + (depth / 2 * Math.sin(Math.toRadians(-rotation)))

		//back: channels 6,7,8
		assert channels[6].getObjectSetting("x").getValue() == X - (depth / 2 * Math.cos(Math.toRadians(-rotation)))
		assert channels[6].getObjectSetting("y").getValue() == Y - (depth / 2 * Math.sin(Math.toRadians(-rotation)))
		assert channels[7].getObjectSetting("x").getValue() == X - (depth / 2 * Math.cos(Math.toRadians(-rotation)))
		assert channels[7].getObjectSetting("y").getValue() == Y - (depth / 2 * Math.sin(Math.toRadians(-rotation)))
		assert channels[8].getObjectSetting("x").getValue() == X - (depth / 2 * Math.cos(Math.toRadians(-rotation)))
		assert channels[8].getObjectSetting("y").getValue() == Y - (depth / 2 * Math.sin(Math.toRadians(-rotation)))

	}


}
