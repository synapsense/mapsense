package DeploymentLab.Model

import DeploymentLab.CentralCatalogue
import DeploymentLab.ComponentLibrary
import DeploymentLab.DeploymentLabVersion
import DeploymentLab.TestUtils
import groovy.xml.MarkupBuilder
import groovy.xml.MarkupBuilderHelper
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

import javax.swing.JFrame

/**
 * Checks that the serialized form of every component can be DE-serialized
 * @author Gabriel Helman
 * @since Io
 * Date: 1/15/13
 * Time: 2:41 PM
 */
@RunWith(Parameterized)
class ComponentSerializeTest {

	@Parameterized.Parameters
	static java.util.Collection<Object[]> proxyableComponents() {

		java.util.Collection<Object[]> comps = new ArrayList<>()

		def (omodel,cmodel) = TestUtils.initModels()

		// Can't seem to get the counter that JUnit uses, which makes finding which case failed difficult. So use our own.
		int counter = 0
		for(String cType : cmodel.listComponentTypes() ){
			if (cType.contains("lon") || cType.contains("bacnet")){
				//these need special handling
				continue
			}

			comps += [counter++, cType] as Object[]
		}

		println "ComponentSerializeTest: Found ${comps.size()} components to test serialize."

		return comps
	}

	ComponentSerializeTest(int counter, String componentType) {
		super()
		this.runCounter = counter
		this.compTypeToTest = componentType
	}

	private int runCounter
	private String compTypeToTest
	ComponentModel componentModel = null
	ObjectModel objectModel = null

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(objectModel,componentModel) = TestUtils.initModels()
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		objectModel = null
		componentModel = null
	}


	@Test
	void serializeEverything() {
		println "ComponentSerializeTest: [$runCounter] Testing $compTypeToTest"
		def candidate = TestUtils.newComponentWithParents( componentModel, compTypeToTest,
		                                                   CentralCatalogue.getSelectModel().getActiveDrawing() )

		//write to xml
		//make a new component from that xml
		//see what happens

		def writer = new StringWriter()
		def builder = new MarkupBuilder(writer)

		//install the XML declaration at the top of the file
		//<?xml version="1.0" encoding="UTF-8"?>
		def helper = new MarkupBuilderHelper(builder)
		helper.xmlDeclaration('version': 1.0, encoding: "UTF-8")
		builder.model('version': DeploymentLabVersion.getModelVersion()) {
			//project.getProjectSettings().save(builder)
			objectModel.serialize(builder, true)
			componentModel.serialize(builder, false, true)
		}

		String stringform = writer.toString()


		def xml = new XmlSlurper().parseText(stringform);


		componentModel.clear();
		componentModel.metamorphosisStarting(ModelState.INITIALIZING);

		//log.trace("Loading Object Model...", "progress");
		objectModel.deserialize(xml.getProperty("objects"));
		//log.trace("Loading Component Model...", "progress");
		componentModel.deserialize(xml.getProperty("components"), ComponentLibrary.INSTANCE);
		componentModel.metamorphosisFinished(ModelState.INITIALIZING);

		//TODO: check that the resuccitated version is actually the same as the old version (currently, we're just looking for Catastrophes)

	}


}
