package DeploymentLab.Model

import DeploymentLab.CentralCatalogue
import org.junit.BeforeClass
import DeploymentLab.TestUtils
import org.junit.Before
import DeploymentLab.UndoBuffer
import javax.swing.JFrame
import org.junit.After
import org.junit.Test
import DeploymentLab.SelectModel
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.ProblemLogger
import DeploymentLab.channellogger.LogLevel
import DeploymentLab.Upgrade.UpgradeController
import DeploymentLab.DeploymentLabVersion
import DeploymentLab.DLProject
import DeploymentLab.Tree.ConfigurationReplacerTreeModel
import DeploymentLab.Tree.ConfigurationReplacerTree
import DeploymentLab.Tools.ConfigurationReplacer
import DeploymentLab.ComponentLibrary

/**
 * Tests of parent-child relationships between components & objects
 * @author Gabriel Helman
 * @since
 * Date: 4/5/12
 * Time: 11:34 AM
 */
class ParentChildTest {

	/*
	methods to be tested:

	also, this API sucks

	//component based
	addChild
	removeChild
	getChildComponents
	getParentsOfRole
	getParentsOfType
	isChildComponentOf
	canChild
	canGoIn(str)
	canGoIn(dlcomponent)

	hasChildRole
	getParentContainers
	descendentOf
	isContainer
	isContainerOrNetwork
	getNumberOfParentContainers


	//object based
	listChildObjectTypes()
	getChildObjectsOfType


	getAllDescendantObjects
	getParentObjects
	getChildObjects
	getChildObjectsOfType



	//placeable children components

	getPlaceableChild
	getPlaceableChildType()
	getPlaceableChildren()
	getPlaceableChildrenMap()

	*/

	/*
	Sketch for a better API:
		addChild(dlc)
		removeChild(dlc)
		(set)getChildren()
		(set)getParents()
		(set)getChildrenOfType(str)
		(set)getParentsOfType(str)
		(set)getChildrenOfRole(str)
		(set)getParentsOfRole(str)
		isChildOf(dlc)
		isParentOf(dlc)
		canBeChildOf(dlc)
		canBeParentOf(dlc)
		canBeChildOfType(str)
		canBeParentOfType(str)

		isDescendantOf(dlc)
		(set)getAllDescendants()


	 */


	ComponentModel cmodel = null
	ObjectModel omodel = null
	UndoBuffer undoBuffer = null
	SelectModel selectModel = null

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))

	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()
		undoBuffer = CentralCatalogue.getUndoBuffer()
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
		undoBuffer = null
	}

	@Test
	void addAndRemoveChildTest(){

		def room = cmodel.newComponent( ComponentType.ROOM )
		selectModel.getActiveDrawing().addChild( room )

		def rack = cmodel.newComponent(TestUtils.DEFAULT_RACK)
		assert ! rack.isChildComponentOf(room)
		assert ! rack.getParentComponents().contains(room)
		assert ! room.getChildComponents().contains(rack)

		room.addChild(rack)
		assert rack.isChildComponentOf(room)
		assert rack.getParentComponents().contains(room)
		assert room.getChildComponents().contains(rack)

		room.removeChild(rack)
		assert ! rack.isChildComponentOf(room)
		assert ! rack.getParentComponents().contains(room)
		assert ! room.getChildComponents().contains(rack)
	}

	@Test
	void canChildTest(){
		def room = cmodel.newComponent("room")
		def wsn = cmodel.newComponent("network")
		def rack = cmodel.newComponent(TestUtils.DEFAULT_RACK)
		def ion = cmodel.newComponent("ion6200-wye")

		assert room.canChild(rack)
		assert room.canChild(ion)
		assert ! rack.canChild(room)
		assert wsn.canChild(rack)
		assert !wsn.canChild(ion)
	}

	@Test
	void canGoIn(){
		def room = cmodel.newComponent("room")
		def wsn = cmodel.newComponent("network")
		def rack = cmodel.newComponent(TestUtils.DEFAULT_RACK)
		def ion = cmodel.newComponent("ion6200-wye")

		assert !room.canGoIn(rack)
		assert !room.canGoIn(ion)
		assert rack.canGoIn(room)
		assert ion.canGoIn(room)
		assert rack.canGoIn(wsn)
		assert !ion.canGoIn(wsn)
	}


	@Test
	void canGoInByTypeNames(){
		def room = cmodel.newComponent("room")
		//def wsn = cmodel.newComponent("network")
		def rack = cmodel.newComponent(TestUtils.DEFAULT_RACK)
		def ion = cmodel.newComponent("ion6200-wye")

		assert !room.canGoIn(TestUtils.DEFAULT_RACK)
		assert !room.canGoIn("ion6200-wye")
		assert rack.canGoIn("room")
		assert ion.canGoIn("room")
		assert rack.canGoIn("network")
		assert !ion.canGoIn("network")
	}

	//static/dyanamic child api


	@Test
	void staticChildren(){
		assert cmodel.getComponents().size() == 1
		def rowl = cmodel.newComponent('dual-horizontal-row-thermanode2')
		assert cmodel.getComponents().size() == 8 //should have just added seven

		assert rowl.hasRole("staticchildplaceable")
		assert rowl.hasChildRole("staticchild")

		Set subracks = new HashSet<DLComponent>(cmodel.getComponentsByType("horizontal-row-rack-child") )
		assert subracks.size() == 6
		subracks.each { assert it.isStaticOrDynamicChild()}
		subracks.each { assert it.getStaticParent() == rowl}

		def children = new HashSet<DLComponent>(rowl.getStaticChildren())
		assert children.equals(subracks)
	}

	@Test
	void staticChildrenAntiCase(){
		def ion = cmodel.newComponent("energy-constellation2-displayed")
		assert ion.getStaticChildren() == []
		assert ion.getStaticParent() == null

		def room = cmodel.newComponent("room")
		def wsn = cmodel.newComponent("network")
		def drawing = cmodel.getComponentsByType("drawing").first()
		drawing.addChild(room)
		drawing.addChild(wsn)
		room.addChild(ion)
		wsn.addChild(ion)

		assert ion.getStaticChildren() == []
		assert ion.getStaticParent() == null
		assert room.getStaticChildren() == []
		assert room.getStaticParent() == null
	}

	@Test
	void dynamicChildrenAntiCase(){
		def ion = cmodel.newComponent("energy-constellation2-displayed")
		assert ion.getDynamicChildren() == []
		assert ion.getDynamicParent() == null

		def room = cmodel.newComponent("room")
		def wsn = cmodel.newComponent("network")
		def drawing = cmodel.getComponentsByType("drawing").first()
		drawing.addChild(room)
		drawing.addChild(wsn)
		room.addChild(ion)
		wsn.addChild(ion)

		assert ion.getDynamicChildren() == []
		assert ion.getDynamicParent() == null
		assert room.getDynamicChildren() == []
		assert room.getDynamicParent() == null
	}

	/**
	 * A few tests to check the results of the auto-childing logic.
	 */
	@Test
	void autoAdoptionCheck() {

		// Load the project again but painfully provide a ConfigReplacer so it gets upgraded.
		def treeModels = []
		def treeModel = new ConfigurationReplacerTreeModel( cmodel, "FakeTree")
		treeModels.add( treeModel )
		ConfigurationReplacerTree ctree = new ConfigurationReplacerTree( treeModel )
		ConfigurationReplacer cr = new ConfigurationReplacer( selectModel, cmodel, ctree, undoBuffer, new JFrame() )
		def clib = ComponentLibrary.INSTANCE;
		clib.init( cmodel, null, treeModels )

		DLProject project = TestUtils.loadProjectFile( TestUtils.CURRENT_VERSION_LINT, undoBuffer, omodel, cmodel, cr )

		if( !project.getProjectSettings().hasBeenUpgraded ) {
			// Force an update since some of these only occur at initial creation.
			UpgradeController uc = new UpgradeController( project )
			uc.performUpgrade( DeploymentLabVersion.getModelVersionByDouble() - 0.1 );
		}

		Set<String> typesCache = new HashSet<>()

		// Make sure objects don't have two of the same parent type.
		for( DLObject o : omodel.getObjects() ) {
			// Static Child and Dual Inlet component types are known to have nodes that are children of multiple racks, skip these creatures.
			if( o.getType() == 'wsnnode') {
				DLComponent owner = cmodel.getOwner(o)
				if( owner.hasRole('staticchildplaceable') || owner.getType() =~ 'dual.inlet' ) {
					continue
				}
			}

			typesCache.clear()
			for( DLObject p : o.getParents() ) {
				// Skip sets since we can have multiple set parents.
				assert ! typesCache.contains( p.getType() ), "Object has multiple parents of the same type. ${cmodel.getOwner(o)}::$o  Parents${o.getParents()}"
				typesCache.add( p.getType() )
			}
		}

		// Make sure objects in control-enabled components have, at most, 1 of each controlpoint type. [Bug 9412]
		for( DLComponent c : cmodel.getComponentsInRole('controlinput') ) {
			for( DLObject o : c.getRoots() ) {
				typesCache.clear()
				for( DLObject k : o.getChildren() ) {
					if( o.getType().startsWith('controlpoint_') ) {
						assert ! typesCache.contains( o.getType() ), "Too many control objects of the same type. $c::$o  Kids${o.getChildren()}"
						typesCache.add( o.getType() )
					}
				}
			}
		}

		// Make sure nodes are hooked to the correct rack by looking at the sensors that the rack is pointing to.
		// The auto-adopter had an issue with Dual Inlet type components. [Bug 9412]
		for( DLObject node : omodel.getObjectsByType('wsnnode') ) {
			DLComponent c = cmodel.getOwner( node )

			// Does this component have a root that has wsnnode children?
			boolean canAdoptNodes = false
			def parents = []
			for( DLObject o : c.getRoots() ) {
				if( omodel.listChildObjectTypes( o.getType() ).contains('wsnnode') ) {
					canAdoptNodes = true
				}

				if( node.getParents().contains( o ) ) {
					parents.add(o)
				}
			}

			// Skip the ones that don't fit the "node as a child of the rack" model.
			if( canAdoptNodes ) {
				// Do a quick check just to make sure the node has the correct number of root parents.
				int expectedParentCnt = 1
				if( c.hasRole('staticchildplaceable') ) {
					// These abominations have multiple parents, one for each of the child components.
					expectedParentCnt = 6
				} else if( c.getType() == 'dual_inlet_rack' ) {
					// Dual inlet has a single node connected to both racks.
					expectedParentCnt = 2
				} else if(( c.getType().startsWith('ez-dual-inlet') || c.getType().startsWith('h-wing-dual-inlet')) && node.getAsName().contains('hot') ) {
					// EZ(H) dual inlet hot nodes connect to both racks.
					expectedParentCnt = 2
				}
				assert parents.size() == expectedParentCnt, "Node $c::$node does not have the expected $expectedParentCnt parents.\n\tParents: $parents\n\tRoots: ${c.getRoots()}"

				// Now make sure it is the correct parent. Check the parent pointers to sensors to make sure one points to the node.
				boolean parentPointsToMySensor = false
				for( Dockingpoint dp : parents.first().getPropertiesByType('dockingpoint') ) {
					if( dp.getReferrent()?.getType() == 'wsnsensor' ) {
						for( DLObject p : dp.getReferrent().getParents() ) {
							if( p == node ) {
								parentPointsToMySensor = true
							}
						}
					}
				}
				assert parentPointsToMySensor, "Node $node is childed to the wrong component root ${parents.first()}."
			}
		}

		// Similar check, but from the other direction where we make sure the rack sensor pointers are to child nodes.
		// Probably not necessary, but can't hurt.
		def roomKids = omodel.listChildObjectTypes('room')
		for( DLComponent c : cmodel.getComponentsInRole('placeable') ) {
			for( DLObject o : c.getRoots() ) {
				// Only check the rack type roots. These will be a room child.
				// Also skip Bongo temp object since it is an odd duck that doesn't have the sensor as a child for explicit reasons.
				if( roomKids.contains( o.getType() ) && !( c.getType().equals('subfloor_node') && o.getType().equals('generictemperature') ) ) {
					for( Dockingpoint dp : o.getPropertiesByType('dockingpoint') ) {
						// We only care about those that are sensors on the same component, not things pointing to associated sensors like Ref Temp.
						if( (dp.getReferrent()?.getType() == 'wsnsensor') && c.getManagedObjects().contains( dp.getReferrent() ) ) {
							for( DLObject dpRefParent : dp.getReferrent().getParents() ) {
								if( dpRefParent.getType() == 'wsnnode' ) {
									if( !o.getChildren().contains( dpRefParent ) ) {
										println "--> ${o.getChildren()}  $dpRefParent  ${cmodel.getOwner(dpRefParent)}"
									}
									assert o.getChildren().contains( dpRefParent ), "Root has dockingpoint to a sensor that is not a child node.\n\tRoot: $c :: $o\n\tDock: ${dp}\n\tNode: ${dp.getReferrent().getParents().first()}  Parents: ${dp.getReferrent().getParents().first().getParents()}"
								}
							}
						}
					}
				}
			}
		}
	}
}
