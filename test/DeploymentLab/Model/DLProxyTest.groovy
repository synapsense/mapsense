package DeploymentLab.Model;


import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import DeploymentLab.TestUtils
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.junit.runners.Parameterized.Parameters
import DeploymentLab.Pair

/**
 * @author Ken Scoggins
 * @since Jupiter 2
 */
@RunWith(Parameterized)
public class DLProxyTest {

	private ComponentModel cmodel
	private ObjectModel    omodel
	private int            runCounter
	private String         compTypeToTest
	private DLComponent    anchor
	private DLComponent    mainDrawing


	/**
	 * Builds a set of components to test with proxies. This set contains all components that pass the canProxy test.
	 *
	 * Sure this is overkill, but proxies are trippy and we don't want some obscure component we didn't think about
	 * breaking something due to some unique difference that proxy doesn't handle well. In the future, this could be
	 * smarter and only have a subset that still covers all the variations. For example, racks that are pretty much the
	 * same except some object differences could be excluded once one of them is in the list.
	 *
	 * @return  A set of DLComponent types.
	 */
	@Parameters
	static java.util.Collection<Object[]> proxyableComponents() {

	    java.util.Collection<Object[]> comps = new ArrayList<>()

		def (omodel,cmodel) = TestUtils.initModels()

		// Can't seem to get the counter that JUnit uses, which makes finding which case failed difficult. So use our own.
		int counter = 0
		cmodel.listComponentTypes().each { cType ->
			// todo: Is there a cheaper way than creating a new component in order to check it?
			DLComponent c = cmodel.newComponent( cType )

			if( DLProxy.canProxy( c ) ) {
				comps += [ counter++, cType ] as Object[]
			} else {
				println "DLProxyTest: Skipping unproxyable $cType ${c.listRoles()}"
			}
		}

		println "DLProxyTest: Found ${comps.size()} proxyable components to test."

		return comps
	}

	/**
	 * Constructor necessary for doing parameterized testing. Called for each item in the parameters collection. Just
	 * save the parameters so they can be used by the  normal test methods.
	 *
	 * @param componentType   The DL Component Type to test.
	 */
	DLProxyTest( int counter, String componentType ) {
	    super()
		this.runCounter = counter
	    this.compTypeToTest = componentType
	}


	/**
	 * One-time setup called before all tests run.
	 */
	@BeforeClass()
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	/**
	 * Setup called before each test.
	 */
	@Before
	void setup() {
		// Fresh models for each test.
		(omodel,cmodel) = TestUtils.initModels()

		// Create the component for this test, complete with appropriate parenting.
		mainDrawing = cmodel.getComponentsByType('drawing').get(0)
		anchor = TestUtils.newComponentWithParents( cmodel, compTypeToTest, mainDrawing )
	}

	/**
	 * Cleanup called after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
	}

	/**
	 * One-time cleanup called after all tests run.
	 */
	@AfterClass()
	static void classTearDown() {
	}


	@Test
	void printComponentBeingTested() {
		println "DLProxyTest: [$runCounter] Testing $compTypeToTest ${anchor.listRoles()}"
	}

	/**
	 * Tests creating a proxy and checks that many of its features are correct, relative to its anchor.
	 */
	@Test
	void testBasicProxyFeatures() {

		//
		// Create a proxy from the anchor.
		//
		DLComponent proxyDrawing = cmodel.newComponent('drawing')
		DLProxy proxy = DLProxy.createProxy( anchor )
		proxyDrawing.addChild( proxy )

		assert proxy.getAnchor() == anchor

		assert !anchor.hasRole("proxy")
		assert proxy.hasRole("proxy")

		assert anchor.hasProxy()
		assert !proxy.hasProxy()

		assert anchor.getDrawing() == mainDrawing
		assert proxy.getDrawing() == proxyDrawing

		assert anchor.getProxies() == [ proxy ]

		// The difference in objects between the 2 should only be the proxy roots.
		def anchorOnlyObjs = ( anchor.getManagedObjects() - proxy.getManagedObjects() )
		def proxyOnlyObjs  = ( proxy.getManagedObjects() - anchor.getManagedObjects() )
 		assert proxy.getRoots() == ( anchorOnlyObjs + proxyOnlyObjs )


		//
		// Create a proxy from the proxy.
		//
		DLProxy proxy2 = DLProxy.createProxy( proxy )
		proxyDrawing.addChild( proxy2 )

		assert proxy.getDrawing() == proxyDrawing
		assert proxy.getAnchor() == anchor
		assert anchor.getProxies().containsAll( [ proxy, proxy2 ] )


		//
		// Delete the first proxy.
		//
		cmodel.remove( proxy )

		// Siblings still exist.
		assert cmodel.getComponents().contains( anchor )
		assert cmodel.getComponents().contains( proxy2 )

		assert anchor.getProxies() == [ proxy2 ]

		// Since the proxy points to the same objects as the anchor, make sure they didn't get deleted but the proxy specific ones did.
		assert omodel.getObjects().containsAll( anchor.getManagedObjects() )
		proxyOnlyObjs.each { assert !omodel.getObjects().contains( it ) }


		//
		// Delete the anchor.
		//
		def anchorObjs = anchor.getManagedObjects()
		def proxy2Objs = ( anchorObjs - proxy2.getManagedObjects() )

		cmodel.remove( anchor )

		// The remaining proxy should also be deleted. So should all the objects.
		assert !cmodel.getComponents().contains( anchor )
		assert !cmodel.getComponents().contains( proxy2 )

		anchorObjs.each { assert !omodel.getObjects().contains( it ) }
		proxy2Objs.each { assert !omodel.getObjects().contains( it ) }
	}

	/**
	 * Tests cloning a proxy.
	 */
	@Test
	void testCloneProxy() {
		DLComponent proxyDrawing = cmodel.newComponent('drawing')
		DLProxy proxy = DLProxy.createProxy( anchor )
		proxyDrawing.addChild( proxy )

		Random rand = new Random()

		// Change some properties of the proxy.
		proxy.mapProperties().each { pName, pObj ->
			if( pObj.isEditable() ) {
				def val = null
				if( pObj.isUnitConvertable() ) {
					val = rand.nextInt()
				} else if( pObj.getType().contains('String') ) {
					val = "${rand.nextInt()}"
				}

				if( val != null ) {
					proxy.setPropertyValue( pName, val )
				}
			}
		}

		// Clone it.
		DLProxy pClone = DLProxy.cloneProxy( anchor, proxy )

		// Check the props.
		proxy.listProperties().each { pName ->
			assert proxy.getPropertyValue( pName ) == pClone.getPropertyValue( pName )
		}
	}

	/**
	 * Tests basic association with the proxy consumers.
	 */
	@Test
	void testConsumerAssociations() {

		// Gimme a proxy.
		DLProxy proxy = DLProxy.createProxy( anchor )
		mainDrawing.addChild( proxy )

		// Get the consumers and grab a component type that has that producer.
		Map<Consumer,DLComponent> consumerFriends = [:]
		proxy.getAllConsumers().each { consumer ->
			// Ignore those that are in a Conducer.
			if( !consumer.hasConducer() ) {
				List<String> cTypes = cmodel.getComponentTypesByProducerDatatype( consumer.getDatatype() )
				if( !cTypes.isEmpty() ) {
					consumerFriends[ consumer ] = TestUtils.newComponentWithParents( cmodel, cTypes.get(0), mainDrawing )
				}
			}
		}

		// Let's make some associations!
		List<DLComponent> associates = []
		consumerFriends.each { consumer, friend ->

			// Associate with the proxy. Remember that the proxy is the consumer in this test.
			String producerId = friend.producersOfType( consumer.getDatatype()).get(0)
			proxy.associate( consumer.getId(), friend, producerId )
			associates += friend

			assert proxy.getConsumerProducers().size() == associates.size()
			assert proxy.getConsumerProducers().containsAll( associates )

			// The anchor should not be affected.
			assert anchor.getConsumerProducers().size() == 0

			// Make sure the friend knows he's associated with the proxy and not the anchor.
			assert friend.getProducerConsumers( producerId ) == [ proxy ]

			// But, the the object reference is with the same object that the anchor's consumer is referencing.
			assert friend.getProducer( producerId ).getConsumerObjects() == [ anchor.getConsumer( consumer.getId() ).getConsumerObject() ]
		}
	}

	/**
	 * Tests basic association with the proxy producers.
	 */
	@Test
	void testProducerAssociations() {

		// Gimme a proxy.
		DLProxy proxy = DLProxy.createProxy( anchor )
		mainDrawing.addChild( proxy )

		// Get the producers and grab a component type that has that consumer.
		Map<Producer,DLComponent> producerFriends = [:]
		proxy.getAllProducers().each { producer ->
			// Ignore those that are in a Conducer.
			if( !producer.hasConducer() ) {
				List<String> cTypes = cmodel.getComponentTypesByConsumerDatatype( producer.getDatatype() )
				if( !cTypes.isEmpty() ) {
					producerFriends[ producer ] = TestUtils.newComponentWithParents( cmodel, cTypes.get(0), mainDrawing )
				}
			}
		}

		// Let's make some associations!
		List<DLComponent> associates = []
		producerFriends.each { producer, friend ->

			// Associate with the proxy. Remember that the proxy is the producer in this test.
			String consumerId = friend.consumersOfType( producer.getDatatype()).get(0)
			friend.associate( consumerId, proxy, producer.getId() )
			associates += friend

			assert proxy.getProducerConsumers().size() == associates.size()
			assert proxy.getProducerConsumers().containsAll( associates )

			// The anchor should not be affected.
			assert anchor.getProducerConsumers().size() == 0

			// Make sure the friend knows he's associated with the proxy and not the anchor.
			assert friend.getConsumerProducers( consumerId ) == [ proxy ]

			// But, the the object reference is with the same object that the anchor's producer is referencing.
			assert friend.getConsumer( consumerId ).getProducerObjects() == [ anchor.getProducer( producer.getId() ).getProducerObject() ]
		}
	}

	/**
	 * Tests basic association with the proxy conducers.
	 */
	@Test
	void testConducerAssociations() {

		// Gimme a proxy.
		DLProxy proxy = DLProxy.createProxy( anchor )
		mainDrawing.addChild( proxy )

		// Get the conducers and grab a component type that has the matching conducer.
		Map<Conducer,DLComponent> conducerFriends = [:]
		proxy.getAllConducers().each { conducer ->
			// todo: conducer.getDatatype should probably return this Pair instead of just the Producer.
			List<String> cTypes = cmodel.getComponentTypesByConducerDatatype( new Pair( conducer.getConsumer().getDatatype(),
			                                                                            conducer.getProducer().getDatatype() ) )
			if( !cTypes.isEmpty() ) {
				conducerFriends[ conducer ] = TestUtils.newComponentWithParents( cmodel, cTypes.get(0), mainDrawing )
			}
		}

		// Let's make some associations!
		List<DLComponent> associates = []
		conducerFriends.each { conducer, friend ->

			// Associate with the proxy. Remember that the proxy is the producer in this test.
			Conducer friendConducer = friend.getConducersThatMatch( conducer ).get(0)
			proxy.associate( conducer, friend, friendConducer )
			associates += friend

			assert proxy.getConsumerProducers().size() == associates.size()
			assert proxy.getConsumerProducers().containsAll( associates )

			assert proxy.getProducerConsumers().size() == associates.size()
			assert proxy.getProducerConsumers().containsAll( associates )

			// The anchor should not be affected.
			assert anchor.getConsumerProducers().size() == 0
			assert anchor.getProducerConsumers().size() == 0

			// Make sure the friend knows he's associated with the proxy and not the anchor.
			assert friendConducer.getConsumer().mapProducers().keySet() == [ proxy ] as Set
			assert friendConducer.getProducer().mapConsumers().keySet() == [ proxy ] as Set

			// But, the the object reference is with the same object that the anchor's producer is referencing.
			Conducer anchorConducer = anchor.getConducer( conducer.getId() )
			assert friendConducer.getConsumer().getProducerObjects() == [ anchorConducer.getProducer().getProducerObject() ]
			assert friendConducer.getProducer().getConsumerObjects() == [ anchorConducer.getConsumer().getConsumerObject() ]
		}
	}
}
