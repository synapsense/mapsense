package DeploymentLab.Model

import DeploymentLab.SelectModel
import org.junit.BeforeClass
import DeploymentLab.TestUtils
import org.junit.Before
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.ProblemLogger
import DeploymentLab.channellogger.LogLevel
import org.junit.After
import org.junit.Test
import DeploymentLab.CentralCatalogue

/**
 * Test the export features within the object model
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/26/12
 * Time: 2:03 PM
 */
class ObjectModelExportTest {


	static ObjectModel omodel
	static ComponentModel cmodel
	static SelectModel selectModel

	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.WARN)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))

		//FakeAlertService.getInstance().reset()
		//FakeEnvironment.getInstance().reset()
		//FakeEnvironment.getInstance().makeTrouble(false)
	}


	@After
	void tearDown() {
		cmodel = null
		omodel = null
		selectModel = null
		//have fun, garbage collector!
	}


	@Test
	void basicDefFile(){

		String justInProject = omodel.makeDefinitionFileString(true)
		//println justInProject
		assert justInProject.length() > 0

		String everything = omodel.makeDefinitionFileString(false)
		//println everything
		assert everything.length() > justInProject.length()

		def justXml = new XmlSlurper().parseText(justInProject)
		//def allXml = new XmlSlurper().parseText(justInProject)

		//assert justXml.object.size() == 8
		def defaults = CentralCatalogue.getApp("default.object.types").split(",").toList()
		def justInProjectTypeNames = new HashSet<String>()
		justXml.object.each{ oxml ->
			justInProjectTypeNames.add(oxml.@type.text())
		}
		assert justInProjectTypeNames.contains( ObjectType.DRAWING )
		defaults.each{
			assert justInProjectTypeNames.contains(it)
		}

		assert justXml.object.size() == defaults.size() + 1 //the defaults plus the Drawing type, and nothing else
	}

	@Test
	void basicSerialize(){
		def drawing = selectModel.getActiveDrawing()
		def room = cmodel.newComponent("room")
		def wsn = cmodel.newComponent("network")
		def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode2")
		def gateway = cmodel.newComponent("remote-gateway")
		drawing.addChild(room)
		drawing.addChild(wsn)
		room.addChild(rack)
		wsn.addChild(rack)
		wsn.addChild(gateway)

		def model = omodel.makeObjectsString()
		//println model
		def xml = new XmlSlurper().parseText(model)
		assert xml.object.size() == omodel.getObjects().size()

		def dlids = new HashSet<String>()
		xml.object.each{ ox ->
			dlids.add(ox.dlid.text().toString())
		}
		omodel.getObjects().each{ o ->
			assert  dlids.contains(o.getDlid().toString())
		}

		//check each property as well?
	}
}
