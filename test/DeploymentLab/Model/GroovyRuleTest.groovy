package DeploymentLab.Model

import org.junit.Before
import DeploymentLab.TestUtils
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.ProblemLogger
import DeploymentLab.channellogger.LogLevel
import org.junit.After
import org.junit.Test
import com.synapsense.dto.RuleType
import org.junit.BeforeClass;

/**
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/17/12
 * Time: 6:38 PM
 */
public class GroovyRuleTest {
	static ObjectModel omodel
	static ComponentModel cmodel

	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.INFO)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		//cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))
	}


	@After
	void tearDown() {
		cmodel = null
		omodel = null
		//have fun, garbage collector!
	}

	private static final String TESTVAL = "testval"

	@Test
	void basicGrule(){
		def math = cmodel.newComponent("math_operation")
		def mo = math.getObject("math")

		mo.setKey("FAKE:42")

		def grule = mo.getObjectProperty("lastValue")
		assert grule instanceof GroovyRule

		assert grule.getType() == "groovyrule"
		assert grule.getValueType() == "java.lang.Double"

		// todo: Bug 8713 changed this. If the rule is changed back to not have a schedule, this should change back too.
		//assert ! grule.hasSchedule()
		//assert grule.getSchedule() == ""
		assert grule.hasSchedule()
		assert grule.getSchedule() == "0 0/5 * * * ?"

		assert !grule.getHistoric()

		assert grule.getRuleFile() == "MathOperation.groovy"
		assert grule.getSourceName() == "MathOperation.groovy"

		def bind = grule.getBindings()
		assert bind.size() == 2
		assert bind["inputA"] == "inputA"
		assert bind["inputB"] == "inputB"

		def contents =  grule.getRuleContents()
		assert contents != null
		assert contents.size() > 0


		RuleType rt = grule.createRuleType()
		assert rt != null
		assert rt.getSourceType() == "Groovy"

		assert grule.getServerTypeName() == "math_operation_lastValue"
		def ruleName = mo.getKey() + grule.name //not quite what we use in production, but close enough here
		grule.setServerInstanceName(ruleName)
		assert grule.getServerInstanceName() == ruleName


		grule.setRuleTo(TESTVAL)
		assert grule.getRuleTo() == TESTVAL

		assert grule.toString().size() > 0
	}

	@Test
	void schedules(){
		def wob = cmodel.newComponent("graph_inlet")
		def w = wob.getObject("graph_inlet")
		def grule = w.getObjectProperty("lastValue")
		assert grule instanceof GroovyRule
		assert grule.hasSchedule()
		assert grule.getSchedule() == "0 0/2 * * * ?"
		def rt = grule.createRuleType()
		assert rt.getSourceType() == "Groovy"
	}

	@Test
	void badFilename(){
		def wob = cmodel.newComponent("graph_inlet")
		def w = wob.getObject("graph_inlet")
		GroovyRule grule = new GroovyRule(w,"badname", "java.lang.Double", "esp.groovy", "", false, new HashMap<String, String>())
		//neither of these operations should work, but neither should they throw any exceptions
		assert grule.getRuleContents() == null
		assert grule.createRuleType() == null
	}

}
