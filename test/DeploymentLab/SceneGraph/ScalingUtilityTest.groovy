package DeploymentLab.SceneGraph

import DeploymentLab.CentralCatalogue
import java.awt.Point
import java.awt.geom.Point2D
import org.junit.*
import static org.junit.Assert.*
import static DeploymentLab.TestUtils.assertAlmostEquals

/**
 * Unit test for the ScalingUtility class. Every test is executed twice; once with an instance of the class with no unit
 * converter and once with an instance that has been provided a unit converter. Since the converter is only used
 * internally the same result should be received in both cases.
 */
class ScalingUtilityTest {

    /** Test default distance. Different than the internal class default to make sure we are getting the property. */
    private static final double EXPECTED_DEFAULT_DISTANCE = ( 2 * ScalingUtility.DISTANCE_DEFAULT_INCHES )

    /** Expected distance in the test. Different than the other defaults to make sure we get back what we give it. */
    private static final double EXPECTED_DISTANCE = ( 3 * ScalingUtility.DISTANCE_DEFAULT_INCHES )

    /** The number of pixels to test with. */
    private static final int NUM_PIXELS = 100

    private static ScalingUtility scalingNoConverter
    private static ScalingUtility scalingWithConverter

    @BeforeClass
    static void beforeClassSetup() {
        scalingNoConverter   = new ScalingUtility( null, null )
        scalingWithConverter = new ScalingUtility( null, CentralCatalogue.INSTANCE.getUnitSystems().getSystemConverter('Imperial US', 'SI'))

        assertNotNull( scalingNoConverter )
        assertNotNull( scalingWithConverter )
    }

    @AfterClass
    static void afterClassSetup() {
        scalingNoConverter   = null
        scalingWithConverter = null
    }

    @Before
    void setUp() {
        // Set the property instead of using the real property file so we don't break if the deployed property file is
        // changed. In the future, maybe we can specify a test property file instead of the hardcoded default.
        // This is done here instead of BeforeClass because some tests may change it.
        String defaultInFeet = Double.toString( EXPECTED_DEFAULT_DISTANCE / 12.0 )
        CentralCatalogue.INSTANCE.getAppProperties().setProperty( ScalingUtility.DISTANCE_PROPERTY_STR, defaultInFeet )

        assertEquals( defaultInFeet, CentralCatalogue.getApp( ScalingUtility.DISTANCE_PROPERTY_STR ) )
    }

    @After
    void tearDown() {
    }

    /**
     * Tests the handling of no default distance in application.properties.
     */
    @Test
    void noApplicationProperty() {
        // Remove the property from the system-wide application properties table.
        Object val = CentralCatalogue.INSTANCE.getAppProperties().remove( ScalingUtility.DISTANCE_PROPERTY_STR )
        assertNotNull( val )
        assertNull( CentralCatalogue.getApp( ScalingUtility.DISTANCE_PROPERTY_STR ) )

        assertEquals( ScalingUtility.DISTANCE_DEFAULT_INCHES, scalingNoConverter.getDefaultScaleDistance(), 0 )
        assertEquals( ScalingUtility.DISTANCE_DEFAULT_INCHES, scalingWithConverter.getDefaultScaleDistance(), 0 )
    }

    /**
     * Test the handling of an invalid default distance in application.properties.
     */
    @Test
    void invalidApplicationProperty() {
        // Set an invalid property in the system-wide application properties table.
        CentralCatalogue.INSTANCE.getAppProperties().setProperty( ScalingUtility.DISTANCE_PROPERTY_STR, '29fubar' )
        assertEquals( '29fubar', CentralCatalogue.getApp( ScalingUtility.DISTANCE_PROPERTY_STR ) )

        assertEquals( ScalingUtility.DISTANCE_DEFAULT_INCHES, scalingNoConverter.getDefaultScaleDistance(), 0 )
        assertEquals( ScalingUtility.DISTANCE_DEFAULT_INCHES, scalingWithConverter.getDefaultScaleDistance(), 0 )
    }

    /**
     * Verifies that we get back the correct default distance from the property file.
     */
    @Test
    void validDefaultScaleDistance() {
        assertAlmostEquals( EXPECTED_DEFAULT_DISTANCE, scalingNoConverter.getDefaultScaleDistance() )
        assertAlmostEquals( EXPECTED_DEFAULT_DISTANCE, scalingWithConverter.getDefaultScaleDistance() )
    }

    /**
     * Tests the method for retrieving a user entered scale distance. Since the UI won't be used during this test, it
     * should return the default that we give it. It still goes through the code, including any unit conversions, but
     * just skips the dialog popup and assumes the default was entered by the user.
     */
    @Test
    void getScaleDistance() {
        assertAlmostEquals( EXPECTED_DISTANCE, scalingNoConverter.getScaleDistance( EXPECTED_DISTANCE ) )
        assertAlmostEquals( EXPECTED_DISTANCE, scalingWithConverter.getScaleDistance( EXPECTED_DISTANCE ) )
    }

    /**
     * Tests the error handling of the basic Compute Scale method that expects a positive distance and number of pixels.
     */
    @Test
    public void computeScaleBadInput() {
        assertEquals( 0, scalingNoConverter.computeScale( 0, -1 ), 0 )
        assertEquals( 0, scalingWithConverter.computeScale( 0, -1 ), 0 )
    }

    /**
     * Tests the basic Compute Scale that takes in the raw parameters of a distance and number of pixels.
     */
    @Test
    public void computeScale() {
        double expected = ( EXPECTED_DISTANCE / NUM_PIXELS )
        assertAlmostEquals( expected, scalingNoConverter.computeScale( NUM_PIXELS, EXPECTED_DISTANCE ) )
        assertAlmostEquals( expected, scalingWithConverter.computeScale( NUM_PIXELS, EXPECTED_DISTANCE ) )
    }

    /**
     * Tests the error handling of the Compute Scale method that takes in two points from an image.
     */
    @Test
    public void computeScalePointsBadInput() {
        assertEquals( 0, scalingNoConverter.computeScale( null, null ), 0 )
        assertEquals( 0, scalingWithConverter.computeScale( null, null ), 0 )
    }

    /**
     * Tests the Compute Scale method that takes in two points from an image to make sure it handles two points that
     * result in zero pixels of distance.
     */
    @Test
    public void computeScalePointsZeroPixels() {
        Point2D ptA = new Point( NUM_PIXELS, NUM_PIXELS )
        Point2D ptB = new Point( NUM_PIXELS, NUM_PIXELS )

        assertEquals( 0, scalingNoConverter.computeScale( ptA, ptB ), 0 )
        assertEquals( 0, scalingWithConverter.computeScale( ptA, ptB ), 0 )
    }

    /**
     * Tests the Compute Scale method that takes in two points from an image.
     */
    @Test
    public void computeScalePoints() {
        // Assume NUM_PIXELS is 100 and use a simple right triangle to make sure computeScale is calculating a slope.
        Point2D ptA = new Point( 0, 0 )
        Point2D ptB = new Point( 60, 80 )

        double expected = ( EXPECTED_DEFAULT_DISTANCE / NUM_PIXELS )
        
        assertAlmostEquals( expected, scalingNoConverter.computeScale( ptA, ptB ) )
        assertAlmostEquals( expected, scalingWithConverter.computeScale( ptA, ptB ) )

        // Just for gits n shiggles, reverse the points and try again.
        assertAlmostEquals( expected, scalingNoConverter.computeScale( ptB, ptA ) )
        assertAlmostEquals( expected, scalingWithConverter.computeScale( ptB, ptA ) )
    }
}
