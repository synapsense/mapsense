package DeploymentLab.SceneGraph;

import org.junit.Test
import java.awt.geom.Point2D

/**
 * Sure!  Why not?
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/11/12
 * Time: 2:53 PM
 */
public class SpiralIteratorTest {
	@Test
	public void testHasNext() throws Exception {
		def si = new SpiralIterator(new Point2D.Double(10,10))
		assert si.hasNext()

	}

	@Test
	public void testNext() throws Exception {
		def si = new SpiralIterator(new Point2D.Double(10,10))
		def result = si.next()
		assert result.x == 4.238978280097804
		assert result.y == 8.323507010806445
	}

	@Test(expected=UnsupportedOperationException.class)
	public void testRemove() throws Exception {
		def si = new SpiralIterator(new Point2D.Double(10,10))
		si.remove()
	}

	@Test
	public void testToString() throws Exception {
		def si = new SpiralIterator(new Point2D.Double(10,10))
		assert si.toString().length() > 10
	}
}
