package DeploymentLab.SceneGraph;

import org.junit.Test
import java.awt.geom.Point2D;

/**
 * And we never need to touch this again.
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/11/12
 * Time: 3:11 PM
 */
public class LineIntersectionTest {
	@Test
	public void testGetInterPoint() throws Exception {

		def li = new LineIntersection(new Point2D.Double(0,0), new Point2D.Double(10,10), new Point2D.Double(0,10), new Point2D.Double(10,0) )
		def result =  li.getInterPoint()
		assert result.x == 5.0
		assert result.y == 5.0
	}
}
