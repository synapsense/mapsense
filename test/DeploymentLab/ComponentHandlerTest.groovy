package DeploymentLab

import DeploymentLab.Export.SoundBoard.MasterPanel
import DeploymentLab.Export.SoundBoard.PlaybackChannel
import org.junit.*;
import DeploymentLab.Export.SoundBoard.ComponentHandler
import javax.swing.JSpinner
import java.awt.Color;

class ComponentHandlerTest
{
    static ComponentHandler componentHandler
    static List<String> ids

    @BeforeClass
    public static void setUpClass() throws Exception
    {
        // Code executed before the first test method
        PlaybackChannel.getInstance(new Color(0,0,0))

        //Set up Component Handler
        componentHandler = new ComponentHandler(MasterPanel.Controller.TEMPERATURE, "Test Channel")
        componentHandler.setSpinner(new JSpinner())

        componentHandler.setMeanMax(90.0)
        componentHandler.setMeanMin(65.0)
        componentHandler.setStandardDeviation(5)
        componentHandler.setAuto(false)
        componentHandler.setAutoOnTime(300000)
        componentHandler.setAutoOffTime(300000)
        componentHandler.setSendDelay(60000)

        //Set up id list
        ids = new ArrayList<>();

        for(int i = 1; i <= 100; i++)
        {
            ids.add("WSNSENSOR:" + i);
        }

        componentHandler.setIds(ids);
    }

    @Before
    public void setUp() throws Exception
    {
        // Code executed before each test
        //No need niw
    }

    @Test
    public void testContinuousTrendNoSS()
    {
        //Test trend without smart send
        componentHandler.setMeanValue(70)
        componentHandler.setTrend(2)
        componentHandler.setThreshold(2)

        long currentStart = Math.round(System.nanoTime() / 1000000.0)
        List<Double> distribution = componentHandler.valueDistribution()
        List<ComponentHandler.Node> commands = componentHandler.getFutureSends(300000,true,currentStart,0,70,70,0,distribution)
        ArrayList<Integer> numTimes = new ArrayList<Integer>(Collections.nCopies(100, 0))

        assert(commands.size() == 500)

        //Test adding data to buffer
        commands.addAll(componentHandler.getFutureSends(420000,false,currentStart,5,80,70,0,distribution))

        assert(commands.size() == 700)

        for(ComponentHandler.Node node : commands)
        {
            //Make sure each value is incremented correctly
            assert(node.getType().equals(ComponentHandler.nodeType.normal))

            int index = ids.indexOf(node.getNodeId())
            assert(node.getCycle() == numTimes.get(index))

            assert(node.getValue() == (distribution.get(index) + (numTimes.get(index) * componentHandler.getTrend())))

            int num = numTimes.get(index) + 1
            numTimes.set(index,num)
        }
    }

    @Test
    public void testContinuousTrendWithSS()
    {
        //Test with trend and smart send
        componentHandler.setMeanValue(65)
        componentHandler.setTrend(5)
        componentHandler.setThreshold(2)

        long currentStart = Math.round(System.nanoTime() / 1000000.0)
        List<Double> distribution = componentHandler.valueDistribution()
        List<ComponentHandler.Node> commands = componentHandler.getFutureSends(180000,true,currentStart,0,65,65,0,distribution)
        ArrayList<Integer> numTimes = new ArrayList<Integer>(Collections.nCopies(100, 0))
        ArrayList<Double> prev = new ArrayList<Double>(Collections.nCopies(100, 0))

        //Add data for test to make sure that works
        commands.addAll(componentHandler.getFutureSends(300000,false,currentStart,3,80,65,0,distribution))

        for(ComponentHandler.Node node : commands)
        {
            int index = ids.indexOf(node.getNodeId())

            if(node.getType().equals(ComponentHandler.nodeType.normal))
            {
                //Make sure everything equals what is expected for normal
                assert (node.getCycle() == numTimes.get(index))

                assert (node.getValue() == (distribution.get(index) + (numTimes.get(index) * componentHandler.getTrend())))

                int num = numTimes.get(index) + 1
                numTimes.set(index, num)
            }

            if(prev.get(index) == 0)
            {
                prev.set(index,node.getValue())
            }
            else
            {
                //Smart send should never let values increment more than ss
                assert(Math.round(node.getValue() - prev.get(index)) <= componentHandler.getThreshold())
                prev.set(index,node.getValue())
            }

        }

        for(int times : numTimes)
        {
            //Make sure everything happened right amount of times
            assert (times == 5)
        }
    }

    @Test
    public void testSettingChanges()
    {
        //Make sure data change works for everything
        componentHandler.setMeanValue(70)
        componentHandler.setTrend(0)
        componentHandler.setThreshold(2)

        componentHandler.setAllowSend(true)
        assert (componentHandler.isRunning())
        componentHandler.setMeanValue(75)
        assert (!componentHandler.isRunning() && componentHandler.getMeanValue() == 75)

        componentHandler.setAllowSend(true)
        assert (componentHandler.isRunning())
        componentHandler.setStandardDeviation(6)
        assert (!componentHandler.isRunning() && componentHandler.getStandardDeviation() == 6)

        componentHandler.setAllowSend(true)
        assert (componentHandler.isRunning())
        componentHandler.setTrend(1)
        assert (!componentHandler.isRunning() && componentHandler.getTrend() == 1)

        componentHandler.setAllowSend(true)
        assert (componentHandler.isRunning())
        componentHandler.setThreshold(3)
        assert (!componentHandler.isRunning() && componentHandler.getThreshold() == 3)

        //Test live cycle time change
        componentHandler.setAllowSend(true)
        assert (componentHandler.isRunning())
        Thread.sleep(2000)
        List<ComponentHandler.Node> old = new ArrayList<ComponentHandler.Node>()

        for(ComponentHandler.Node nodeOldValue : componentHandler.control.buffer)
        {
            //copy by value
            old.add(new ComponentHandler.Node(nodeOldValue))
        }

        componentHandler.setSendDelay(120000)
        Thread.sleep(2000)

        List<ComponentHandler.Node> newBuf = new ArrayList<ComponentHandler.Node>(componentHandler.control.buffer)
        assert(componentHandler.isRunning())

        for(int i = old.size() - newBuf.size(); i < old.size(); i++)
        {
            //Compare old times with new times making sure id and value stay the same while time changes
            assert(old.get(i).getValue() == newBuf.get(i - (old.size() - newBuf.size())).getValue() && old.get(i).getNodeId() == newBuf.get(i- (old.size() - newBuf.size())).getNodeId() && old.get(i).getTime() != newBuf.get(i- (old.size() - newBuf.size())).getTime())
        }

        componentHandler.setAllowSend(false)
    }

    @After
    public void tearDown() throws Exception
    {
        // Code executed after each test
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
        // Code executed after the last test method
    }
}
