package test.DeploymentLab

import DeploymentLab.Model.ComponentType
import DeploymentLab.TestUtils
import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import static DeploymentLab.TestUtils.shouldFail
import DeploymentLab.CentralCatalogue

/**
 * Created by IntelliJ IDEA.
 * User: HLim
 * Date: 9/4/12
 * Time: 3:03 PM
 * To change this template use File | Settings | File Templates.
 */
class VirtualLayerTest {
    private static String defaultModel = "cfg/ss/defaultmodel.xml"

    private ObjectModel objectModel
    private ComponentModel componentModel


    /**
     * One-time setup called before all tests run.
     */
    @BeforeClass()
    static void classSetup() {

    }

    /**
     * Setup called before each test.
     */
    @Before
    void setup() {
        (objectModel,componentModel) = TestUtils.initModels( false )
    }

    /**
     * Cleanup called after each test.
     */
    @After
    void tearDown() {
        objectModel = null
        componentModel = null
    }

    /**
     * One-time cleanup called after all tests run.
     */
    @AfterClass()
    static void classTearDown() {

    }

    @Test
    void testNewVirtualLayer() {
        println "new virtual layer"
        DLComponent drawing = componentModel.newComponent( ComponentType.DRAWING )
        drawing.setPropertyValue('name', "virtual layer")

    }
}
