package DeploymentLab

import static DeploymentLab.TestUtils.assertAlmostEquals
import static DeploymentLab.TestUtils.shouldFail

import java.text.DecimalFormatSymbols

import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.junit.runners.Parameterized.Parameters
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.ComponentProperty
import com.synapsense.util.unitconverter.ConverterException


/**
 * Tests the Localizer class.
 *
 * @author Ken Scoggins
 * @since  Jupiter
 */
@RunWith(Parameterized)
class LocalizerTest {

    private static final int STR_IDX = 0
    private static final int VAL_IDX = 1

    private Locale locale
    private DecimalFormatSymbols symbols


    /**
     * Builds a set of locales to test against. There are, at last count, over 150 supported locales. However, most
     * of them use the same symbol set, especially when that set is limited to only those we care about. So this checks
     * them all and creates a set of unique symbol combinations.
     *
     * @return  The set of Locales to test.
     */
    @Parameters
    static Collection<Object[]> allLocales() {

        def locales = []

        // Add a null entry to test the case where no Locale is specified and the Localizer default behavior is used.
        locales += [ null, null ] as Object[]

        // Check each of the available Locales and find unique symbol combinations.
        def uniqueSets = []
        Locale.availableLocales.each {

            DecimalFormatSymbols sym = DecimalFormatSymbols.getInstance( it )

            // Easy way to find uniques. Create a string of the symbols we care about & save if it's not in our unique list.
            def symStr = "${sym.minusSign}${sym.groupingSeparator}${sym.decimalSeparator}"

            if( !uniqueSets.contains( symStr ) ) {
                uniqueSets += symStr
                locales += [ it, sym ] as Object[]
            }
        }
        return locales
    }

    /**
     * Constructor necessary for doing parameterized testing. Simply save the parameters so they can be used by the
     * normal test methods.
     *
     * @param locale   Locale to use for this test.
     * @param symbols  The symbol set associated with this Locale.
     */
    LocalizerTest( Locale locale, DecimalFormatSymbols symbols ) {
        super()
        this.locale = locale
        this.symbols = symbols
    }

    /**
     * One-time setup before all tests run.
     */
    @BeforeClass
    static void classSetup() {
        // Turn on error logging to help when something fails internal to a method we are testing.
        TestUtils.enableLogging( DeploymentLab.channellogger.LogLevel.ERROR )
    }

    /**
     * Initialization called before each test.
     */
    @Before
    void setup() {
        // Let Localizer use the default if locale not specified, otherwise change it.
        if( locale ) {
            // Change the locale to the one provided for the test.
            Localizer.instance.changeLocale( locale )
        } else {
            // Let Localizer use the default behavior and
            locale  = Localizer.instance.locale
            symbols = DecimalFormatSymbols.getInstance( this.locale )
        }

        // Let the test determine if it needs a converter.
        CentralCatalogue.instance.setSystemConverter( null )
    }

    /**
     * Just here to print the locale being tested since JUnit calls both the constructor and @Setup before each test, so
     * putting this in there results in it being printed once for each test in this file.
     */
    @Test
    void printLocaleBeingTested() {
        println "Localizer: Testing locale $locale (L: ${locale.displayLanguage}, C: ${locale.displayCountry}, R: ${locale.displayVariant})   " +
                "Symbols: Neg ${symbols.minusSign}  Grp ${symbols.groupingSeparator}  Dec ${symbols.decimalSeparator}"
    }

    /**
     * Tests the isLocalization checker that uses a Component Property.
     */
    @Test
    void testIsLocalizableByComponent() {
        def (omodel,cmodel) = TestUtils.initModels()
        DLComponent comp = cmodel.newComponent('rack-control-rearexhaust-nsf-rt-thermanode2')

        assert !Localizer.isLocalizable( (ComponentProperty) null )
        assert !Localizer.isLocalizable( comp.getComponentProperty('name') )
        assert Localizer.isLocalizable( comp.getComponentProperty('depth') )
    }

    /**
     * Tests the isLocalization checker that uses class names as a string.
     */
    @Test
    void testIsLocalizableByName() {
        assert !Localizer.isLocalizable( (String) null )
        assert !Localizer.isLocalizable('')
        assert !Localizer.isLocalizable( String.name )
        assert Localizer.isLocalizable( Float.name )
        assert Localizer.isLocalizable( Double.name )
        assert Localizer.isLocalizable( Integer.name )
        assert Localizer.isLocalizable( Long.name )
    }

    /**
     * Tests the isLocalization checker that uses the Class.
     */
    @Test
    void testIsLocalizableByClass() {
        assert !Localizer.isLocalizable( (Class) null )
        assert !Localizer.isLocalizable( String )
        assert Localizer.isLocalizable( Float )
        assert Localizer.isLocalizable( Double )
        assert Localizer.isLocalizable( Integer )
        assert Localizer.isLocalizable( Long )
    }

    /**
     * Tests the getInstance method.
     */
    @Test
    void testGetInstance() {
        assert Localizer.instance
    }

    /**
     * Tests the parse method with valid integer strings.
     */
    @Test
    void testParseValidInts() {

        validInts.each {
            def num = Localizer.parse( it[STR_IDX] )
            assert num, "Failed to parse '${it[STR_IDX]}'."
            assert num == it[VAL_IDX]
        }
    }

    /**
     * Tests the parse method with valid double strings.
     */
    @Test
    void testParseValidDoubles() {

        validDoubles.each {
            def num = Localizer.parse( it[STR_IDX] )
            assert num, "Failed to parse '${it[STR_IDX]}'."
            assert num == it[VAL_IDX]
        }
    }

    /**
     * Tests the parse method with invalid strings.
     */
    @Test
    void testParseInvalidNums() {

        invalidNums.each { str ->
            shouldFail("Test string '$str'.", NumberFormatException ) { Localizer.parse( str ) }
        }
    }

    /**
     * Tests the parseDouble method with valid values.
     */
    @Test
    void testParseDoubleValid() {
        validDoubles.each {
            def num = Localizer.parseDouble( it[STR_IDX] )
            assert num, "Failed to parse '${it[STR_IDX]}'."
            assert num == it[VAL_IDX]
        }
    }

    /**
     * Tests the parseDouble method with invalid values.
     */
    @Test
    void testParseDoubleInvalid() {
        invalidNums.each { str ->
            shouldFail("Test string '$str'.", NumberFormatException ) { Localizer.parseDouble( str ) }
        }
    }

    /**
     * Tests the parseFloat method with valid floats.
     */
    @Test
    void testParseFloatValid() {
        validDoubles.each {
            def num = Localizer.parseFloat( it[STR_IDX] )
            assert num, "Failed to parse '${it[STR_IDX]}'."
            assert num == (float) it[VAL_IDX]
        }
    }

    /**
     * Tests the parseFloat method with invalid floats.
     */
    @Test
    void testParseFloatInvalid() {
        invalidNums.each { str ->
            shouldFail("Test string '$str'.", NumberFormatException ) { Localizer.parseFloat( str ) }
        }
    }

    /**
     * Tests the parseInt method with valid integers.
     */
    @Test
    void testParseIntValid() {
        validInts.each {
            def num = Localizer.parseInt( it[STR_IDX] )
            assert num, "Failed to parse '${it[STR_IDX]}'."
            assert num == it[VAL_IDX]
        }
    }

    /**
     * Tests the parseInt method with invalid integers.
     */
    @Test
    void testParseIntInvalid() {
        invalidNums.each { str ->
            shouldFail("Test string '$str'.", NumberFormatException ) { Localizer.parseInt( str ) }
        }
    }

    /**
     * Tests the parseLong method with valid integers.
     */
    @Test
    void testParseLongValid() {
        validInts.each {
            def num = Localizer.parseLong( it[STR_IDX] )
            assert num, "Failed to parse '${it[STR_IDX]}'."
            assert num == it[VAL_IDX]
        }
    }

    /**
     * Tests the parseLong method with invalid integers.
     */
    @Test
    void testParseLongInvalid() {
        invalidNums.each { str ->
            shouldFail("Test string '$str'.", NumberFormatException ) { Localizer.parseLong( str ) }
        }
    }

    /**
     * Tests that the format method properly formats valid doubles.
     */
    @Test
    void testFormatNumberDoubles() {
        validDoubles.each {
            def str = Localizer.format( it[VAL_IDX] )
            assert str, "Failed to format '${it[VAL_IDX]}'."

            // Convert it back to see if the format was correct. Remember that it may have rounded fractional digits.
            def val = Localizer.parseDouble( str )
            assertAlmostEquals("Formatted string '${it[VAL_IDX]}' doesn't equal the value $val",
                               val, it[VAL_IDX].round( Localizer.instance.maximumFractionalDigits ) )
         }
    }

    /**
     * Tests that the format method properly parses valid strings.
     */
    @Test
    void testFormatNumberInts() {
        validInts.each {
            def str = Localizer.format( it[VAL_IDX] )
            assert str, "Failed to format '${it[VAL_IDX]}'."

            // Convert it back to see if the format was correct.
            def val = Localizer.parseDouble( str )
            assert val == it[VAL_IDX], "Formatted string '${it[VAL_IDX]}' doesn't equal the value $val"
        }
    }

    /**
     * Tests the error handling of the format method.
     */
    @Test
    void testFormatError() {

        assert Localizer.format( null ) == null
        assert Localizer.format('') == ''
        assert Localizer.format('12345.9876') == '12345.9876'
    }

    /**
     * Tests the system converter convenience methods with no converter in place.
     */
    @Test
    void testConvertNoConverter() {

        assert Localizer.getUnits('distance') == 'in'
        assert Localizer.getUnits('distance', true ) == 'inch'
        shouldFail('getUnits() with invalid dimension name', ConverterException ) { Localizer.getUnits('fubar') }

        assert Localizer.convert( 1.0, 'distance' ) == 1.0
        assert Localizer.convert( 32, 'temperature') == 32
        assert Localizer.convert( 100, 'fubar' ) == 100

        assert Localizer.convertFrom( 1.0, 'distance' ) == 1.0
        assert Localizer.convertFrom( 32, 'temperature') == 32
        assert Localizer.convertFrom( 100, 'fubar' ) == 100
    }

    /**
     * Tests the system converter convenience methods with a converter specified.
     */
    @Test
    void testConvertWithConverter() {
        CentralCatalogue.instance.setSystemConverter( CentralCatalogue.instance.unitSystems.getSystemConverter('Imperial US', 'SI') )

        assert Localizer.getUnits('distance') == 'cm'
        assert Localizer.getUnits('distance', true ) == 'centimeter'
        shouldFail('getUnits() with invalid dimension name', ConverterException ) { Localizer.getUnits('fubar') }

        assert Localizer.convert( 1.0, 'distance' ).round(2) == 2.54
        assert Localizer.convert( 32, 'temperature') == 0
        shouldFail('convert() with invalid dimension name', ConverterException ) { Localizer.convert( 100, 'fubar' ) }

        assert Localizer.convertFrom( 2.54, 'distance' ).round(2) == 1.0
        assert Localizer.convertFrom( 0, 'temperature') == 32
        shouldFail('convertFrom() with invalid dimension name', ConverterException ) { Localizer.convertFrom( 100, 'fubar' ) }
    }

    /**
     * Tests passing in a null to reset the locale to the system default.
     */
    @Test
    void testLocaleReset(){
        Localizer.instance.changeLocale( null )
        assert Localizer.instance.locale == Locale.default
    }
    
    ////////////////////  PRIVATE NON-TEST HELPER METHODS  ////////////////////

    private def getValidInts() {

        char neg = symbols.minusSign
        char k   = symbols.groupingSeparator

        def ints = []

        ints += [ '1234',        1234 ] as Object[]
        ints += [ "${neg}1234", -1234 ] as Object[]

        if( Localizer.instance.groupingUsed ) {
            ints += [ "1${k}234",        1234 ] as Object[]
            ints += [ "${neg}1${k}234", -1234 ] as Object[]
        }

        return ints
    }

    private def getValidDoubles() {

        char neg = symbols.minusSign
        char k   = symbols.groupingSeparator
        char dec = symbols.decimalSeparator

        def dbls = []

        dbls += [ "1234${dec}9876",        1234.9876d ] as Object[]
        dbls += [ "${dec}9876",               0.9876d ] as Object[]
        dbls += [ "0${dec}9876",              0.9876d ] as Object[]
        dbls += [ "${neg}1234${dec}9876", -1234.9876d ] as Object[]

        if( Localizer.instance.groupingUsed ) {
            dbls += [ "1${k}234${dec}9876",        1234.9876d ] as Object[]
            dbls += [ "${neg}1${k}234${dec}9876", -1234.9876d ] as Object[]
        }

        return dbls
    }

    private def getInvalidNums() {

        char neg = symbols.minusSign
        char k   = symbols.groupingSeparator
        char dec = symbols.decimalSeparator

        def nums = []

        nums += ''
        nums += 'asdf1234'
        nums += '1234asdf'
        nums += '12asdf34'
        nums += '1234${dec}abcd'

        nums += "12${neg}34"
        nums += "1234${neg}"

        nums += "1${dec}${dec}9876"
        nums += "1${dec}234${dec}9876"

        if( Localizer.instance.groupingUsed ) {
            // TBD: DecimalFormat currently has a bug (#7049000) that produces an incorrect value for strings with badly formatted grouping.
            //      So these pass when they shouldn't. Like "12,34,5" actually produces a number 12345!
            //nums += "12${k}${k}345"
            //nums += "12${k}34${k}5"
            //nums += "12${k}34"
            //nums += "${k}123"
            nums += "1234${k}"
            nums += "1${dec}234${k}9876"
        }

        return nums
    }
}
