package DeploymentLab

import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentType
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLProxy
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Tools.ConfigurationReplacer
import DeploymentLab.Tree.ConfigurationReplacerTree
import DeploymentLab.Tree.ConfigurationReplacerTreeModel
import org.junit.*

import javax.swing.*

import static DeploymentLab.TestUtils.shouldFail

/**
 * Tests the headless mode of MapSense, useful for things like UnitTests, to make sure we don't have issues like popups
 * that can cause the automated testing to hang waiting for input.
 *
 * @author Ken Scoggins
 * @since Aireo
 */
public class HeadlessMapSenseTest {

	ComponentModel cmodel = null
	ObjectModel omodel = null
	SelectModel selectModel = null

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		// Fresh models for each test. A dontcare JFrame is used to keep internal UndoBuffer code happy.
		omodel = TestUtils.loadObjectModel()
		cmodel = TestUtils.loadComponentModel(omodel)
		selectModel = new SelectModel()
		cmodel.addModelChangeListener(selectModel)
		UndoBuffer undoBuffer = new UndoBuffer(cmodel,selectModel)
		undoBuffer.setParent(new JFrame())
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
		selectModel = null
	}

	@Test
	void channelLoggerTest() {
	}
}
