package DeploymentLab;

import org.junit.Test
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Model.ComponentModel
import org.junit.BeforeClass
import org.junit.Before
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.channellogger.LogLevel
import org.junit.After
import DeploymentLab.Model.DLComponent

/**
 * Checks that the cyclechecker is correctly checking for... cycles?
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 8/7/12
 * Time: 6:01 PM
 */
public class CycleCheckerTest {
	static ObjectModel omodel
	static ComponentModel cmodel
	static SelectModel selectModel
	static CycleChecker cycleChecker

	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))

		//no reason to instantiate this in each test
		cycleChecker = new CycleChecker(cmodel);
	}

	@After
	void cleanup(){
		omodel = null
		cmodel = null
		selectModel = null
		cycleChecker = null
	}
/*
	//the original types list
	String[] types = {"math_operation", "average_operation", "sum_operation", "transformer_secondary", "transformer_primary", "3PhaseTokW",
		"lessthan_filter", "math_operation_addition", "math_operation_subtraction", "math_operation_multiplication", "math_operation_division",
		"math_operation_exponentiation", "math_operation_abs", "math_operation_square", "math_operation_sqrt", "math_operation_loge", "math_operation_log10",
		"graph_inlet", "graph_outlet"
	};
*/
	@Test
	public void testFindCycles() throws Exception {
		DLComponent sum = cmodel.newComponent("sum_operation")
		DLComponent avg = cmodel.newComponent("average_operation")
		DLComponent square = cmodel.newComponent("math_operation_square")
		DLComponent root = cmodel.newComponent("math_operation_sqrt")

		assert cycleChecker.findCycles().size() == 0

		sum.associate("inputA", avg, "value")
		avg.associate("inputA", square, "value")
		square.associate("inputA", root, "value")
		root.associate("input", sum, "value")

		assert cycleChecker.findCycles().size() == 1

		root.unassociate("input", sum, "value")

		assert cycleChecker.findCycles().size() == 0
	}

	@Test
	public void testFindInletOutletConnections() throws Exception {
		DLComponent sum = cmodel.newComponent("sum_operation")
		DLComponent avg = cmodel.newComponent("average_operation")
		DLComponent square = cmodel.newComponent("math_operation_square")
		DLComponent root = cmodel.newComponent("math_operation_sqrt")

		DLComponent inlet = cmodel.newComponent("graph_inlet")
		DLComponent outlet = cmodel.newComponent("graph_outlet")

		assert cycleChecker.findInletOutletConnections().size() == 0

		sum.associate("inputA", avg, "value")
		avg.associate("inputA", square, "value")
		square.associate("inputA", root, "value")
		root.associate("input", inlet, "value")

		outlet.associate("input", sum, "value")

		//inlet.associate("input", sum, "value")

		//looks for any connection between an inlet and and outlet
		assert cycleChecker.findInletOutletConnections().size() == 1
		assert cycleChecker.findCycles().size() == 0

	}

	/**
	 * Self references should never be allowed, but if a glitch happens and one emerges,
	 * we need to prevent it from exporting all the same
	 */
	@Test
	public void selfConnection(){
		DLComponent sum = cmodel.newComponent("sum_operation")
		sum.associate("inputA", sum, "value")


		assert cycleChecker.findInletOutletConnections().size() == 0
		assert cycleChecker.findCycles().size() == 1

		//now add a second component
		DLComponent avg = cmodel.newComponent("average_operation")
		sum.associate("inputA", avg, "value")
		assert cycleChecker.findInletOutletConnections().size() == 0
		assert cycleChecker.findCycles().size() == 1
	}


	@Test
	public void selfConnectionWithInlets(){
		DLComponent inlet = cmodel.newComponent("graph_inlet")
		DLComponent sum = cmodel.newComponent("sum_operation")
		sum.associate("inputA", sum, "value")
		sum.associate("inputA", inlet, "value")

		assert cycleChecker.findInletOutletConnections().size() == 1
		assert cycleChecker.findCycles().size() == 1
	}

	/**
	 * The old fixed-list cycle checker didn't know about status components
	 */
	//@Test //currently, the new cycle checker don't know about status either
	public void alsoStatus(){
		DLComponent status1 = cmodel.newComponent("pue_status_inverter")
		DLComponent status2 = cmodel.newComponent("pue_status_inverter")
		assert cycleChecker.findCycles().size() == 0

		status1.associate("input", status2, "value")
		status2.associate("input", status1, "value")

		assert cycleChecker.findCycles().size() == 1
	}

}
