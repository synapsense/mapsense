package DeploymentLab

import com.panduit.sz.api.ss.assets.Location
import com.panduit.sz.api.ss.assets.LocationLevel
import org.junit.Test

import javax.swing.tree.DefaultMutableTreeNode

/**
 * Created by ccr on 1/5/2016.
 */

class TreeTest {

    /**
     * This test creates a tree if nodes that contains non floor locations that will be pruned:
     *
     * CA
     * -> Folsom
     *  --> Floor
     * -> Sacramento
     *  (no floors, so it gets pruned)
     */
    @Test
    void testPruning() {
        Location folsomFloorLocation  = Location.builder().name("FolsomFloor").locationLevel(LocationLevel.FLOOR).build();

        List<Location> folsomChildrenLocations = new ArrayList<Location>();
        folsomChildrenLocations.add(folsomFloorLocation);
        Location folsomLocation  = Location.builder().name("Folsom").locationLevel(LocationLevel.CITY).children(folsomChildrenLocations).build();

        Location sacramentoLocation  = Location.builder().name("Sacramento").locationLevel(LocationLevel.CITY).build();

        List<Location> stateChildrenLocations = new ArrayList<Location>();
        stateChildrenLocations.add(folsomLocation);
        stateChildrenLocations.add(sacramentoLocation);

        Location stateLocation  = Location.builder().name("CA").locationLevel(LocationLevel.STATE).children(stateChildrenLocations).build();

        // should still have a floor branch
        def root = DeploymentLabWindow.mapLocationToSwingTreeWithPruning(stateLocation);

        // only one child after pruning
        assert root.getChildCount() == 1
        DefaultMutableTreeNode actualFolsomNode = root.children().nextElement();
        Location actualFolsomLocation = actualFolsomNode.getUserObject()
        assert actualFolsomLocation.name == folsomLocation.name
    }
}
