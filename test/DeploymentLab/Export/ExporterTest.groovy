package DeploymentLab.Export

import DeploymentLab.CentralCatalogue
import DeploymentLab.DLProject
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ComponentRole
import DeploymentLab.Model.ComponentType
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.DLObject
import DeploymentLab.Model.LogicalGroupController
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Model.Property
import DeploymentLab.Model.Setting
import DeploymentLab.ProblemLogger
import DeploymentLab.SelectModel
import DeploymentLab.TestUtils
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.channellogger.LogLevel
import GroovyRuleStubs.FakeAlertService
import GroovyRuleStubs.FakeContext
import GroovyRuleStubs.FakeEnvironment
import com.synapsense.dto.TOFactory
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

/**
 *
 *
 * @author Ken Scoggins
 * @since SmartZone 8.0.0
 */
public class ExporterTest {

    static ObjectModel omodel
    static ComponentModel cmodel
    static SelectModel selectModel

    static ServerContext context
    static Exporter exporter


    @BeforeClass
    static void classSetup() {
        // Turn on error logging to help when something fails internal to a method we are testing.
        TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
    }

    @Before
    void setup() {
        (omodel,cmodel) = TestUtils.initModels(false)
        selectModel = CentralCatalogue.getSelectModel()

        cmodel.addModelChangeListener(new LogicalGroupController() )

        ChannelManager cm = ChannelManager.getInstance()
        cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
        //cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))

        FakeAlertService.getInstance().reset()
        FakeEnvironment.getInstance().reset()
        FakeEnvironment.getInstance().makeTrouble(false)

        context = new FakeContext()
        exporter = new Exporter( context )
    }

    @After
    void tearDown() {
        cmodel = null
        omodel = null
        selectModel = null
        context = null
        exporter = null
        //have fun, garbage collector!
    }

    private void exportAndValidate( DLProject proj ) {
        // Get the project version that has been prepped for export.
        File exportFile = File.createTempFile( proj.getFile().toFile().getName(), ".dlt" )
        DLProject subProject = new DLProject( proj.undoBuffer, cmodel, omodel, null, false )
        subProject.setFileButDontLock( exportFile )
        subProject.export( exportFile )

        boolean exportSuccess = exporter.dlisExport( omodel, false, false, false, exportFile )
        assert exportSuccess

        for( DLObject o : omodel.getObjects() ) {
            // Objects that were once exported, but weren't this time, get tricky since the old value isn't null,
            // but may not match. So leave it to the caller to check their specific situation.
            DLComponent owner = cmodel.getManagingComponent(o)
            assert owner, "$o appears to not belong to any component!"
            List<DLComponent> pgroups = owner.getParentsOfType( ComponentType.PLANNING_GROUP )
            boolean deepDive = true
            for( DLComponent pg : pgroups ) {
                if( !pg.getPropertyValue('export') ) {
                    //println "Skipping basic oldval check for not exported object that was previously exported: $o  $owner"
                    deepDive = false
                    break
                }
            }

            if( deepDive ) {
                for( Property p : o.getProperties().findAll { it.type.equalsIgnoreCase("setting") } ) {
                    Setting prop = (Setting) p

                    // BTW, binary data will always have null oldvalues. And SmartZone linked objects need to always save,
                    // even if not exported.  And the "owner && o &&" in asserts is just so the error shows the offending object.
                    if( ( o.isExportable() || o.isSmartZoneLinked() ) && !prop.getValueType().equalsIgnoreCase("com.synapsense.dto.BinaryData") ) {
                        assert owner && o && ( prop.getValue() == prop.getOldValue() )
                    } else if( prop.getValueType().equalsIgnoreCase("java.lang.Integer") ||
                               prop.getValueType().equalsIgnoreCase("java.lang.Double") ) {
                        assert owner && o && (( prop.getOldValue() == null ) || ( prop.getOldValue() == 0 ))
                    } else if( prop.getValueType().equalsIgnoreCase("java.lang.String") ) {
                        assert owner && o && (( prop.getOldValue() == null ) || ( prop.getOldValue().toString().isEmpty() ))
                    } else if( prop.getValueType().equalsIgnoreCase("com.synapsense.dto.TO") ) {
                        assert owner && o && (( prop.getOldValue() == null ) || ( prop.getOldValue() == TOFactory.EMPTY_TO ))
                    } else {
                        assert owner && o && ( prop.getOldValue() == null )
                    }
                }
            }
        }
    }


    @Test
    void dlisExportSetsOldValueCorrectly() {
        DLProject proj = TestUtils.loadProjectFile( TestUtils.CURRENT_VERSION_LINT, CentralCatalogue.getUndoBuffer(), omodel, cmodel )
		selectModel.setActiveDrawing( cmodel.getComponentsByType( ComponentType.DRAWING ).first() )

        // We'll need to know what planning group is set to not export and there should have been at least one in the LINT file.
        List<DLComponent> wasUnexported = []
        for( DLComponent c : cmodel.getComponentsByType(ComponentType.PLANNING_GROUP ) ) {
            if( !c.getPropertyValue('export') ) {
                wasUnexported.add( c )
            }
        }
        assert wasUnexported.size() > 0

        // Add new components that can be SmartZone linked so it looks like a linked project.
        int szId = 0
        selectModel.getActiveDrawing().setSmartZoneId( ++szId )
        for( String ctype : cmodel.listComponentTypes() ) {
            if( ( ctype != ComponentType.DRAWING ) && ( cmodel.getTypeProp( ctype, 'smartZoneId' ) != null ) ) {
                // One that's normal...
                DLComponent c = TestUtils.newComponentWithParents( cmodel, ctype, selectModel.getActiveDrawing() )
                c.setSmartZoneId( ++szId )
                c.setPropertyValue('name', "${c.getPropertyValue('name')}$szId-SZLINKED" )
                c.setPropertyValue('x', 100 )
                c.setPropertyValue('y', 100 )

                // ... and one that is in a non-exporting group.
                c = TestUtils.newComponentWithParents( cmodel, ctype, selectModel.getActiveDrawing() )
                c.setSmartZoneId( ++szId )
                c.setPropertyValue('name', "${c.getPropertyValue('name')}$szId-SZLINKEDNE" )
                c.setPropertyValue('x', 100 )
                c.setPropertyValue('y', 100 )

                wasUnexported.first().addChild( c )
            }
        }

        println "\n***** dlisExportSetsOldValueCorrectly: doing initial clean export\n"
        // Do a clean export to populate the fake server.
        omodel.clearKeys()
        exportAndValidate( proj )

        println "\n***** dlisExportSetsOldValueCorrectly: change unexported Planning Group to export\n"
        // Change the planning group to exported then export again.
        for( DLComponent c : wasUnexported ) {
            c.getChildComponents().each { assert !it.isExportable() }
            c.setPropertyValue( 'export', true )
            c.getChildComponents().each { assert it.isExportable() == it.isExportableDefault() }
        }

        exportAndValidate( proj )

        println "\n***** dlisExportSetsOldValueCorrectly: change Planning Group back to not export and change some child values\n"

        // Change the group back to not export and change some props. The old value should not get updated.
        for( DLComponent c : wasUnexported ) {
            for( DLComponent kid : c.getChildComponents() ) {
                assert kid.isExportable() == kid.isExportableDefault()
                for( DLObject o : kid.getManagedObjects() ) {
                    if( o.hasObjectProperty('name') ) {
                        o.getObjectSetting('name').setValue("${o.getObjectSetting('name').getValue().toString()}-NOEXPORT")
                    }

                    if( o.hasObjectProperty('x') ) {
                        o.getObjectSetting('x').setValue( o.getObjectSetting('x').getValue() + 5 )
                    }

                    if( o.hasObjectProperty('y') ) {
                        o.getObjectSetting('y').setValue( o.getObjectSetting('y').getValue() + 5 )
                    }
                }
            }

            // Add a new kid to the group.
            DLComponent newKid = TestUtils.newComponentWithParents( cmodel, TestUtils.DEFAULT_RACK, c.getDrawing() )
            newKid.setPropertyValue( 'name', "NEWKID-NOEXPORT" )
            c.addChild( newKid )

            c.setPropertyValue( 'export', false )
            c.getChildComponents().each { assert !it.isExportable() }
        }

        exportAndValidate( proj )

        // Make sure the old value didn't get updated.
        for( DLComponent pg : wasUnexported ) {
            for( DLComponent c : pg.getChildComponents() ) {
                if( c.getPropertyValue('name') == 'NEWKID-NOEXPORT' ) {
                    // Should not have been exported. Old values being null got checked already.
                    c.getManagedObjects().each { assert pg && !it.hasKey() }
                } else {
                    for( DLObject o : c.getManagedObjects() ) {
                        for( Property p : o.getProperties().findAll { it.type.equalsIgnoreCase("setting") } ) {
                            Setting prop = (Setting) p

                            if( c.isExportableDefault() ) {
                                switch( prop.getName() ) {
                                    case 'name' :
                                        assert c && o && prop.getValue().toString().contains('-NOEXPORT')
                                        assert c && o && !prop.getOldValue().toString().contains('-NOEXPORT')
                                        break

                                    case 'x' :
                                        assert c && o && ( prop.getValue() == ( prop.getOldValue() + 5 ) )
                                        break

                                    case 'y' :
                                        assert c && o && ( prop.getValue() == ( prop.getOldValue() + 5 ) )
                                        break
                                }
                            } else {
                                // Those that will never export do get updated, though.
                                assert c && o && ( prop.getValue() == prop.getOldValue() )
                            }
                        }
                    }
                }
            }
        }

        // Unlink the project from SmartZone and the behavior should return to normal SynapSoft.
        proj.unlinkFromSmartZone()

        for( DLComponent c : cmodel.getComponents() ) {
            if( c.canSmartZoneLink() ) {
                assert c.getSmartZoneId() == 0
                assert !c.isSmartZoneLinked()

                for( DLObject o : c.getManagedObjects() ) {
                    if( o.canSmartZoneLink() ) {
                        assert o.getSmartZoneId() == 0
                    }
                }
            }
        }

        exportAndValidate( proj )

        proj.close()
    }


    @Test
    void dlisExportPrunesDeletedList() {
        DLProject proj = TestUtils.loadProjectFile( TestUtils.CURRENT_VERSION_LINT, CentralCatalogue.getUndoBuffer(), omodel, cmodel )
		selectModel.setActiveDrawing( cmodel.getComponentsByType( ComponentType.DRAWING ).first() )

        // Nothing should be deleted right now.
        assert omodel.listDeletedObjects().isEmpty()

        // Make sure everything has been exported.
        exportAndValidate( proj )
        assert omodel.listDeletedObjects().isEmpty()

        // Delete a few things.
        int cnt = 0
        for( DLComponent c : cmodel.getComponentsInRoles([ ComponentRole.PLACEABLE, ComponentRole.CONTROL_INPUT ]) ) {
            if( c.isExported() ) {
                cmodel.remove( c )

                if( ++cnt == 5 ) {
                    break
                }
            }
        }
        assert !omodel.listDeletedObjects().isEmpty()

        // Export again and make sure the components are pruned.
        exportAndValidate( proj )
        assert omodel.listDeletedObjects().isEmpty()

        proj.close()
    }
}
