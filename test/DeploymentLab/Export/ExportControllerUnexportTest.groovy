package DeploymentLab.Export

import DeploymentLab.CentralCatalogue;
import org.junit.Test
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Model.ComponentModel
import DeploymentLab.SelectModel
import org.junit.BeforeClass
import DeploymentLab.TestUtils
import org.junit.Before
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.ProblemLogger
import DeploymentLab.channellogger.LogLevel
import GroovyRuleStubs.FakeAlertService
import GroovyRuleStubs.FakeEnvironment
import GroovyRuleStubs.FakeContext
import org.junit.After
import DeploymentLab.DLProject
import javax.swing.JFrame

import DeploymentLab.Image.DLImage

/**
 * Tests the unexporting options of the new ExportController
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/27/12
 * Time: 1:30 PM
 */
public class ExportControllerUnexportTest {

	private static File      IMAGE_FILE = new File('build/testFiles/validations/DLID_1425.png')

	static ObjectModel omodel
	static ComponentModel cmodel
	static SelectModel selectModel
	static def ctx
	static DLProject project

	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.INFO)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		//cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))

		FakeAlertService.getInstance().reset()
		FakeEnvironment.getInstance().reset()
		FakeEnvironment.getInstance().makeTrouble(false)

		//expected version is appProperties.getProperty('build.version')
		ctx = new FakeContext()
		project = new DLProject(CentralCatalogue.getUndoBuffer(),cmodel,omodel,null,false)
		project.setFile(File.createTempFile(this.class.getName(), ".test"))

		def drawing = selectModel.getActiveDrawing()
		drawing.setPropertyValue( "image_data", new DLImage( IMAGE_FILE ) );
	}


	@After
	void tearDown() {
		cmodel = null
		omodel = null
		selectModel = null
		ctx = null
		project = null
		//have fun, garbage collector!
	}


	@Test
	public void testFastUnExport() throws Exception {
		def drawing = selectModel.getActiveDrawing()
		def room = cmodel.newComponent("room")
		def wsn = cmodel.newComponent("network")
		def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode2")
		def gateway = cmodel.newComponent("remote-gateway")
		drawing.addChild(room)
		drawing.addChild(wsn)
		room.addChild(rack)
		wsn.addChild(rack)
		wsn.addChild(gateway)

		def ex = new Exporter(ctx)
		ex.updateExportTs(omodel,cmodel)
		ex.export(omodel)

		assert ctx.getEnv().getAllWorldProperties().size() > 6
		//ctx.getEnv().getAllWorldProperties().each{ k, v ->
		//	println "$k, $v"
		//}

		//def unex = new UnExporter(ctx)
		//assert unex.checkSync(omodel)
		//assert unex.fastUnExport(omodel)

		def ec = new ExportController(project)
		//ec.removeSchemaFromServer(true)
		ec.doUnExportSchema(ctx,true)

		assert ctx.getEnv().getAllWorldProperties().size() == 2 // ROOT & CONFIG get created by the FakeEnv


	}


	@Test
	public void testFastUnExportTwoDrawings() throws Exception {
		def drawing = selectModel.getActiveDrawing()
		def room = cmodel.newComponent("room")
		def wsn = cmodel.newComponent("network")
		def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode2")
		def gateway = cmodel.newComponent("remote-gateway")
		drawing.addChild(room)
		drawing.addChild(wsn)
		room.addChild(rack)
		wsn.addChild(rack)
		wsn.addChild(gateway)

		def drawing2 = cmodel.newComponent("drawing")
		drawing2.setPropertyValue( "image_data", new DLImage( IMAGE_FILE ) );
		def room2 = cmodel.newComponent("room")
		def wsn2 = cmodel.newComponent("network")
		def rack2 = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode2")
		def gateway2 = cmodel.newComponent("remote-gateway")
		drawing2.addChild(room2)
		drawing2.addChild(wsn2)
		room2.addChild(rack2)
		wsn2.addChild(rack2)
		wsn2.addChild(gateway2)

		def ex = new Exporter(ctx)
		ex.updateExportTs(omodel,cmodel)
		ex.export(omodel)

		assert ctx.getEnv().getAllWorldProperties().size() > 6
		//ctx.getEnv().getAllWorldProperties().each{ k, v ->
		//	println "$k, $v"
		//}

		//def unex = new UnExporter(ctx)
		//assert unex.checkSync(omodel)
		//assert unex.fastUnExport(omodel)

		def ec = new ExportController(project)
		//ec.removeSchemaFromServer(true)
		ec.doUnExportSchema(ctx,true)

		assert ctx.getEnv().getAllWorldProperties().size() == 2 // ROOT & CONFIG get created by the FakeEnv
	}


	@Test
	public void testUnExport() throws Exception {
		def drawing = selectModel.getActiveDrawing()
		def room = cmodel.newComponent("room")
		def wsn = cmodel.newComponent("network")
		def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode2")
		def gateway = cmodel.newComponent("remote-gateway")
		drawing.addChild(room)
		drawing.addChild(wsn)
		room.addChild(rack)
		wsn.addChild(rack)
		wsn.addChild(gateway)

		def ex = new Exporter(ctx)
		ex.updateExportTs(omodel,cmodel)
		ex.export(omodel)

		assert ctx.getEnv().getAllWorldProperties().size() > 6
		//ctx.getEnv().getAllWorldProperties().each{ k, v ->
		//	println "$k, $v"
		//}
		//def unex = new UnExporter(ctx)
		//assert unex.checkSync(omodel)
		//assert unex.export(omodel)

		def ec = new ExportController(project)
		//ec.removeSchemaFromServer(false)
		ec.doUnExportSchema(ctx,false)

		assert ctx.getEnv().getAllWorldProperties().size() == 2 // ROOT & CONFIG get created by the FakeEnv
	}


	@Test
	public void testUnExportTwoDrawings() throws Exception {
		def drawing = selectModel.getActiveDrawing()
		def room = cmodel.newComponent("room")
		def wsn = cmodel.newComponent("network")
		def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode2")
		def gateway = cmodel.newComponent("remote-gateway")
		drawing.addChild(room)
		drawing.addChild(wsn)
		room.addChild(rack)
		wsn.addChild(rack)
		wsn.addChild(gateway)


		def drawing2 = cmodel.newComponent("drawing")
		drawing2.setPropertyValue( "image_data", new DLImage( IMAGE_FILE ) );
		def room2 = cmodel.newComponent("room")
		def wsn2 = cmodel.newComponent("network")
		def rack2 = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode2")
		def gateway2 = cmodel.newComponent("remote-gateway")
		drawing2.addChild(room2)
		drawing2.addChild(wsn2)
		room2.addChild(rack2)
		wsn2.addChild(rack2)
		wsn2.addChild(gateway2)

		def ex = new Exporter(ctx)
		ex.updateExportTs(omodel,cmodel)
		ex.export(omodel)

		assert ctx.getEnv().getAllWorldProperties().size() > 6
		//ctx.getEnv().getAllWorldProperties().each{ k, v ->
		//	println "$k, $v"
		//}
		//def unex = new UnExporter(ctx)
		//assert unex.checkSync(omodel)
		//assert unex.export(omodel)

		def ec = new ExportController(project)
		//ec.removeSchemaFromServer(false)
		ec.doUnExportSchema(ctx,false)

		assert ctx.getEnv().getAllWorldProperties().size() == 2 // ROOT & CONFIG get created by the FakeEnv
	}

}
