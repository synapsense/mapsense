package DeploymentLab.Export

import DeploymentLab.CentralCatalogue
import DeploymentLab.Model.ObjectType
import org.junit.Before
import DeploymentLab.TestUtils
import DeploymentLab.UndoBuffer
import javax.swing.JFrame
import org.junit.After
import DeploymentLab.Model.ComponentModel
import org.junit.BeforeClass
import org.junit.Test
import DeploymentLab.Model.ObjectModel
import org.apache.commons.io.FileUtils
import DeploymentLab.Model.DLObject
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.ProblemLogger
import DeploymentLab.channellogger.LogLevel

/**
 *
 * @author Gabriel Helman
 * @since
 * Date: 2/17/12
 * Time: 10:28 AM
 */
class DLObjectValueExporterTest {

	ComponentModel cmodel = null
	ObjectModel omodel = null

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}
	
	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))

	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
	}
	
	@Test
	void generateNodeNames(){
		for(int i = 0; i < 20; i++){
			def rack = cmodel.newComponent("rack-control-toprearexhaust-nsf-rt-thermanode2")
			rack.setPropertyValue("name", "TRE-RACK $i")
			def crah = cmodel.newComponent("crah-single-plenumrated-thermanode")
			crah.setPropertyValue("name", "CRAH==$i")
		}
		List<String[]> result = DLObjectValueExporter.exportNodeNames(omodel)
		assert result
		assert result.size() > 0
		assert ! result.contains("Node")
	}
	
	@Test
	void listToCSV(){
		List<String[]> data = []
		for (int i = 0; i < 10; i++){
			String[] current = new String[10]
			for(int j = 0; j < 10; j++ ){
				current[j] = "${i}${j}" as String  // as String to workaround a Groovy 2.4 bug
			}
			data += current
		}
		
		//println data
		def filename = "build/testFiles/csvexport.csv"
		DLObjectValueExporter.exportToCSV(data,filename)
		
		//now, read that file back and check it
		def f = new File(filename)
		List fromFile = FileUtils.readLines(f, "UTF-8")
		assert fromFile.size() == 10
		int line = 0
		for(String l : fromFile){
			def cells = l.split(",")
			assert cells.length == 10
			for(int j=0; j< 10; j++){
				//println "\"${line}${j}\""
				assert cells[j].equals("\"${line}${j}\"".toString())
				String computedValue = '"' + line.toString() + j.toString() + '"'
				//println "should be '$computedValue', is '${cells[j]}'"
				//assert cells[j].equals(computedValue)
			}
			line++
		}
	}

	@Test
	void nodeNamesNoDrawings(){
		for(int i = 0; i < 20; i++){
			def rack = cmodel.newComponent("rack-control-toprearexhaust-nsf-rt-thermanode2")
			rack.setPropertyValue("name", "TRE-RACK $i")
			def crah = cmodel.newComponent("crah-single-plenumrated-thermanode")
			crah.setPropertyValue("name", "CRAH==$i")
		}
		List<String[]> result = DLObjectValueExporter.exportNodeNames(omodel, new HashSet<DLObject>())
		assert result != null
		assert result.size() == 0
	}


	@Test
	void nodeNamesByDrawing(){
		def drawing = cmodel.newComponent("drawing")
		def drawing2 = cmodel.newComponent("drawing")
		def room = cmodel.newComponent("room")
		drawing.addChild(room)

		for(int i = 0; i < 20; i++){
			def rack = cmodel.newComponent("rack-control-toprearexhaust-nsf-rt-thermanode2")
			rack.setPropertyValue("name", "TRE-RACK $i")
			def crah = cmodel.newComponent("crah-single-plenumrated-thermanode")
			crah.setPropertyValue("name", "CRAH==$i")
			room.addChild(rack)
			room.addChild(crah)
		}

		def drawings = new HashSet<DLObject>()
		drawings.add(drawing.getObject( ObjectType.DRAWING ))
		List<String[]> result = DLObjectValueExporter.exportNodeNames(omodel, drawings)
		assert result != null
		assert result.size() > 0
		assert ! result.contains("Node")

		drawings.clear()
		drawings.add(drawing2.getObject( ObjectType.DRAWING ))
		result = DLObjectValueExporter.exportNodeNames(omodel, drawings)
		assert result != null
		assert result.size() == 0
	}

}
