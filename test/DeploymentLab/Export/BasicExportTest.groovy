package DeploymentLab.Export

import DeploymentLab.CentralCatalogue
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLObject
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Model.ObjectType
import DeploymentLab.Model.Setting
import DeploymentLab.ProblemLogger
import DeploymentLab.SelectModel
import DeploymentLab.TestUtils
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.channellogger.LogLevel
import GroovyRuleStubs.FakeAlertService
import GroovyRuleStubs.FakeContext
import GroovyRuleStubs.FakeEnvironment
import com.synapsense.dto.TOFactory
import com.synapsense.exception.PropertyNotFoundException
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import DeploymentLab.SpecificChannelLogger

/**
 * Tests around the export process
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/24/12
 * Time: 11:25 AM
 */
class BasicExportTest {

	static ObjectModel omodel
	static ComponentModel cmodel
	static SelectModel selectModel

	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		//cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))

		FakeAlertService.getInstance().reset()
		FakeEnvironment.getInstance().reset()
		FakeEnvironment.getInstance().makeTrouble(false)
		FakeEnvironment.getInstance().autoCreateType( true )

		//expected version is appProperties.getProperty('build.version')
	}


	@After
	void tearDown() {
		cmodel = null
		omodel = null
		selectModel = null
		//have fun, garbage collector!
	}

	@Test
	void checkVersionNoRoot() {
		println "checkVersionNoRoot"
		def ctx = new FakeContext()

		// Model creates ROOT & CONFIG by default, so reset without it.
		FakeEnvironment.getInstance().reset( false )

		def ex = new Exporter(ctx)
		assert !ex.checkVersion()
	}

	@Test
	void checkVersionRootNoVersion() {
		println "checkVersionRootNoVersion"
		def ctx = new FakeContext()

		// Model creates ROOT & CONFIG by default, so reset without it.
		FakeEnvironment.getInstance().reset( false )

		ctx.getEnv().createObject("ROOT")
		def ex = new Exporter(ctx)
		assert !ex.checkVersion()
	}

	@Test
	void checkVersionRootBadVersion() {
		println "checkVersionRootBadVersion"
		def ctx = new FakeContext()
		def to = ctx.getEnv().getObjectsByType("ROOT").first()
		ctx.getEnv().setPropertyValue(to, "version", "1.0")
		def ex = new Exporter(ctx)
		assert !ex.checkVersion()
	}

	@Test
	void checkVersionRootNullVersion() {
		println "checkVersionRootNullVersion"
		def ctx = new FakeContext()
		def to = ctx.getEnv().getObjectsByType("ROOT").first()
		ctx.getEnv().setPropertyValue(to, "version", null)
		def ex = new Exporter(ctx)
		assert !ex.checkVersion()
	}

	@Test
	void checkVersionCorrectVersion() {
		println "checkVersionCorrectVersion"
		def ctx = new FakeContext()
		def to = ctx.getEnv().getObjectsByType("ROOT").first()
		ctx.getEnv().setPropertyValue(to, "version", CentralCatalogue.getApp("db.version"))
		def ex = new Exporter(ctx)
		assert ex.checkVersion()
	}


	@Test
	void configDataNoPrior() {
		println "configDataNoPrior"
		def ctx = new FakeContext()

		// Model creates ROOT & CONFIG by default, so reset without it.
		FakeEnvironment.getInstance().reset( false )

		def to = ctx.getEnv().createObject("ROOT")
		def ex = new Exporter(ctx)
		ex.updateConfigData(to)
		//println ctx.getEnv().getAllWorldProperties()
		def cd = new ArrayList(ctx.getEnv().getObjectsByType("CONFIGURATION_DATA")).first()
		assert ctx.getEnv().getPropertyValue(cd, "name", String.class) == CentralCatalogue.getApp("application.shortname")
		assert ctx.getEnv().getPropertyValue(cd, "config", String.class) != null
	}

	@Test
	void configDataUpdate() {
		println "configDataUpdate"
		def ctx = new FakeContext()
		def to = ctx.getEnv().getObjectsByType("ROOT").first()
		def ex = new Exporter(ctx)
		def cd = ctx.getEnv().getObjectsByType("CONFIGURATION_DATA").first()
		ctx.getEnv().setPropertyValue(cd, "name", CentralCatalogue.getApp("application.shortname"))
		ctx.getEnv().setRelation(to, cd)

		ex.updateConfigData(to)
		//println ctx.getEnv().getAllWorldProperties()
		assert new ArrayList(ctx.getEnv().getObjectsByType("CONFIGURATION_DATA")).size() == 1
		//def cd2 = new ArrayList(ctx.getEnv().getObjectsByType("CONFIGURATION_DATA")).first()
		//assert ctx.getEnv().getPropertyValue(cd, "name", String.class) == CentralCatalogue.getApp("application.shortname")
		assert ctx.getEnv().getPropertyValue(cd, "config", String.class) != null
	}


	@Test
	void configDataOthersNoDL() {
		println "configDataOthersNoDL"
		def ctx = new FakeContext()
		def to = ctx.getEnv().getObjectsByType("ROOT").first()
		def ex = new Exporter(ctx)

		def cd = ctx.getEnv().getObjectsByType("CONFIGURATION_DATA").first()
		ctx.getEnv().setPropertyValue(cd, "name", "panopticon")
		ctx.getEnv().setRelation(to, cd)

		ex.updateConfigData(to)
		//println ctx.getEnv().getAllWorldProperties()
		assert new ArrayList(ctx.getEnv().getObjectsByType("CONFIGURATION_DATA")).size() == 2
		def cd2
		ctx.getEnv().getObjectsByType("CONFIGURATION_DATA").each {
			if (ctx.getEnv().getPropertyValue(it, "name", String.class) == CentralCatalogue.getApp("application.shortname")) {
				cd2 = it
			}
		}
		assert cd2 != null
		assert ctx.getEnv().getPropertyValue(cd2, "config", String.class) != null
	}

	@Test
	void configDataOthersUpdate() {
		println "configDataOthersUpdate"
		def ctx = new FakeContext()
		def to = ctx.getEnv().getObjectsByType("ROOT").first()
		def ex = new Exporter(ctx)

		def p = ctx.getEnv().getObjectsByType("CONFIGURATION_DATA").first()
		ctx.getEnv().setPropertyValue(p, "name", "panopticon")
		ctx.getEnv().setRelation(to, p)

		def cd = ctx.getEnv().createObject("CONFIGURATION_DATA")
		ctx.getEnv().setPropertyValue(cd, "name", CentralCatalogue.getApp("application.shortname"))
		ctx.getEnv().setRelation(to, cd)

		ex.updateConfigData(to)
		//println ctx.getEnv().getAllWorldProperties()
		assert new ArrayList(ctx.getEnv().getObjectsByType("CONFIGURATION_DATA")).size() == 2
		def cd2
		ctx.getEnv().getObjectsByType("CONFIGURATION_DATA").each {
			if (ctx.getEnv().getPropertyValue(it, "name", String.class) == CentralCatalogue.getApp("application.shortname")) {
				cd2 = it
			}
		}
		assert cd2 != null
		assert ctx.getEnv().getPropertyValue(cd2, "config", String.class) != null
	}


	@Test
	void checkSyncNoRoot() {
		println "checkSyncNoRoot"
		def ctx = new FakeContext()

		// Model creates ROOT & CONFIG by default, so reset without it.
		FakeEnvironment.getInstance().reset( false )

		def ex = new Exporter(ctx)
		assert !ex.checkSync(omodel)
	}


	@Test
	void checkSyncNeverExported() {
		println "checkSyncNeverExported"
		def ctx = new FakeContext()
		def ex = new Exporter(ctx)
		assert ex.checkSync(omodel)
	}

	@Test
	void checkSyncServerNoKeys() {
		println "checkSyncServerNoKeys"
		def drawing = selectModel.getActiveDrawing().getObject( ObjectType.DRAWING )
		drawing.setKey("FAKE:42")
		def ctx = new FakeContext()
		def ex = new Exporter(ctx)
		assert ex.checkSync(omodel)
	}


	@Test
	void checkSyncOutOfSync() {
		println "checkSyncOutOfSync"

		def ctx = new FakeContext()
		def root = ctx.getEnv().getObjectsByType("ROOT").first()
		def to = ctx.getEnv().createObject( ObjectType.DRAWING.toUpperCase() )

		DLObject drawing = selectModel.getActiveDrawing().getObject( ObjectType.DRAWING )
		drawing.setKey( to.toString() )
		drawing.getObjectSetting("lastExportTs").setValue(13)

		ctx.getEnv().setRelation(root, to)
		ctx.getEnv().setPropertyValue(to, "lastExportTs", 42)
		def ex = new Exporter(ctx)
		assert !ex.checkSync(omodel)
	}

	@Test
	void checkSyncInSync() {
		println "checkSyncInSync"
		def ctx = new FakeContext()
		def root = ctx.getEnv().getObjectsByType("ROOT").first()
		def to = ctx.getEnv().createObject( ObjectType.DRAWING.toUpperCase() )
		ctx.getEnv().setRelation(root, to)
		ctx.getEnv().setPropertyValue(to, "lastExportTs", 42)

		DLObject drawing = selectModel.getActiveDrawing().getObject( ObjectType.DRAWING )
		drawing.setKey( to.toString() )
		drawing.getObjectSetting("lastExportTs").setValue(42)

		def ex = new Exporter(ctx)
		assert ex.checkSync(omodel)
	}

	@Test
	void checkSyncInSyncShowMessage() {
		println "checkSyncInSyncShowMessage"
		def ctx = new FakeContext()
		def root = ctx.getEnv().getObjectsByType("ROOT").first()
		def to = ctx.getEnv().createObject( ObjectType.DRAWING.toUpperCase() )
		ctx.getEnv().setRelation(root, to)
		ctx.getEnv().setPropertyValue(to, "lastExportTs", 42)

		DLObject drawing = selectModel.getActiveDrawing().getObject( ObjectType.DRAWING )
		drawing.setKey( to.toString() )
		drawing.getObjectSetting("lastExportTs").setValue(42)

		def spc = new SpecificChannelLogger(LogLevel.TRACE, "message")
		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(spc)

		def ex = new Exporter(ctx)
		assert ex.checkSync(omodel,true)
		//check for a message
		assert spc.getStack().size() == 1

		cm.removeChannelListener(spc)
	}

	//for for multyDrawingCases
	@Test
	void checkSyncNoRootMulty() {
		println "checkSyncNoRootMulty"
		cmodel.newComponent("drawing")

		def ctx = new FakeContext()
		def to = ctx.getEnv().deleteObject( ctx.getEnv().getObjectsByType("ROOT").first() )
		def ex = new Exporter(ctx)
		assert !ex.checkSync(omodel)
	}


	@Test
	void checkSyncNeverExportedMulty() {
		println "checkSyncNeverExportedMulty"
		cmodel.newComponent("drawing")
		def ctx = new FakeContext()
		def ex = new Exporter(ctx)
		assert ex.checkSync(omodel)
	}

	@Test
	void checkSyncServerNoKeysMulty() {
		println "checkSyncServerNoKeysMulty"
		cmodel.newComponent("drawing")
		def drawing = selectModel.getActiveDrawing().getObject( ObjectType.DRAWING )
		drawing.setKey("FAKE:42")
		def ctx = new FakeContext()
		def ex = new Exporter(ctx)
		assert ex.checkSync(omodel)
	}


	@Test
	void checkSyncOutOfSyncMultyOneBlankOneBad() {
		println "checkSyncOutOfSyncMultyOneBlankOneBad"
		def ctx = new FakeContext()
		def root = ctx.getEnv().getObjectsByType("ROOT").first()
		def to = ctx.getEnv().createObject( ObjectType.DRAWING.toUpperCase() )
		ctx.getEnv().setRelation(root, to)
		ctx.getEnv().setPropertyValue(to, "lastExportTs", 42)

		cmodel.newComponent("drawing")
		DLObject drawing = selectModel.getActiveDrawing().getObject( ObjectType.DRAWING )
		drawing.setKey( to.toString() )
		drawing.getObjectSetting("lastExportTs").setValue(13)

		def ex = new Exporter(ctx)
		assert !ex.checkSync(omodel)
	}

	@Test
	void checkSyncInSyncMultyOneBlankOneGood() {
		println "checkSyncInSyncMultyOneBlankOneGood"
		def ctx = new FakeContext()
		def root = ctx.getEnv().getObjectsByType("ROOT").first()
		def to = ctx.getEnv().createObject( ObjectType.DRAWING.toUpperCase() )
		ctx.getEnv().setRelation(root, to)
		ctx.getEnv().setPropertyValue(to, "lastExportTs", 42)

		cmodel.newComponent("drawing")
		DLObject drawing = selectModel.getActiveDrawing().getObject( ObjectType.DRAWING )
		drawing.setKey( to.toString() )
		drawing.getObjectSetting("lastExportTs").setValue(42)

		def ex = new Exporter(ctx)
		assert ex.checkSync(omodel)
	}

	@Test
	void checkSyncInSyncMultyOneBadOneGood() {
		println "checkSyncInSyncMultyOneBadOneGood"

		def ctx = new FakeContext()
		def root = ctx.getEnv().getObjectsByType("ROOT").first()
		def to = ctx.getEnv().createObject( ObjectType.DRAWING.toUpperCase() )
		ctx.getEnv().setRelation(root, to)
		ctx.getEnv().setPropertyValue(to, "lastExportTs", 42)

		DLObject drawing = selectModel.getActiveDrawing().getObject( ObjectType.DRAWING )
		drawing.setKey( to.toString() )
		drawing.getObjectSetting("lastExportTs").setValue(42)


		def to2 = ctx.getEnv().createObject( ObjectType.DRAWING.toUpperCase() )
		ctx.getEnv().setRelation(root, to2)
		ctx.getEnv().setPropertyValue(to2, "lastExportTs", 42)

		def drawing2 = cmodel.newComponent("drawing")
		drawing2.getObject( ObjectType.DRAWING ).setKey( to2.toString() )
		drawing2.getObject( ObjectType.DRAWING ).getObjectSetting("lastExportTs").setValue(13)


		def ex = new Exporter(ctx)
		assert !ex.checkSync(omodel)
	}


	@Test
	void checkSyncInSyncMultyTwoGood() {
		println "checkSyncInSyncMultyTwoGood"

		def ctx = new FakeContext()
		def root = ctx.getEnv().getObjectsByType("ROOT").first()
		def to = ctx.getEnv().createObject( ObjectType.DRAWING.toUpperCase() )
		ctx.getEnv().setRelation(root, to)
		ctx.getEnv().setPropertyValue(to, "lastExportTs", 42)

		DLObject drawing = selectModel.getActiveDrawing().getObject( ObjectType.DRAWING )
		drawing.setKey( to.toString() )
		drawing.getObjectSetting("lastExportTs").setValue(42)


		def to2 = ctx.getEnv().createObject( ObjectType.DRAWING.toUpperCase() )
		ctx.getEnv().setRelation(root, to2)
		ctx.getEnv().setPropertyValue(to2, "lastExportTs", 42)

		def drawing2 = cmodel.newComponent("drawing")
		drawing2.getObject( ObjectType.DRAWING ).setKey( to2.toString() )
		drawing2.getObject( ObjectType.DRAWING ).getObjectSetting("lastExportTs").setValue(42)


		def ex = new Exporter(ctx)
		assert ex.checkSync(omodel)
	}


	@Test
	void updateTSNoRoot() {
		println "updateTSNoRoot"
		def ctx = new FakeContext()
		def to = ctx.getEnv().deleteObject( ctx.getEnv().getObjectsByType("ROOT").first() )
		def ex = new Exporter(ctx)
		assert ctx.getEnv().getObjectsByType("ROOT").size() == 0
		assert !ex.updateExportTs(omodel, cmodel)
	}


	@Test
	void updateTSKeysInSync() {
		println "updateTSKeysInSync"

		def ctx = new FakeContext()
		def root = ctx.getEnv().getObjectsByType("ROOT").first()
		def to = ctx.getEnv().createObject( ObjectType.DRAWING.toUpperCase() )
		ctx.getEnv().setRelation(root, to)
		ctx.getEnv().setPropertyValue(to, "lastExportTs", 42)

		DLObject drawing = selectModel.getActiveDrawing().getObject( ObjectType.DRAWING )
		drawing.setKey( to.toString() )
		drawing.getObjectSetting("lastExportTs").setValue(42)

		def ex = new Exporter(ctx)
		assert ex.updateExportTs(omodel, cmodel)
		assert drawing.getKey() != ""
	}

	@Test
	void updateTSNoKeys() {
		println "updateTSNoKeys"
		DLObject drawing = selectModel.getActiveDrawing().getObject( ObjectType.DRAWING )
		def ctx = new FakeContext()
		def root = ctx.getEnv().getObjectsByType("ROOT").first()
		def ex = new Exporter(ctx)
		assert ex.updateExportTs(omodel, cmodel)
		assert Long.parseLong(drawing.getObjectSetting("lastExportTs").getValue().toString()) > 0
		assert drawing.getKey() == ""

	}

	@Test
	void updateTSOnlyLocalKey() {
		println "updateTSOnlyLocalKey"
		DLObject drawing = selectModel.getActiveDrawing().getObject( ObjectType.DRAWING )
		drawing.setKey("${ObjectType.DRAWING.toUpperCase()}:2")
		drawing.getObjectSetting("lastExportTs").setValue(42)

		def ctx = new FakeContext()
		def root = ctx.getEnv().getObjectsByType("ROOT").first()

		def ex = new Exporter(ctx)
		assert ex.updateExportTs(omodel, cmodel)
		assert Long.parseLong(drawing.getObjectSetting("lastExportTs").getValue().toString()) > 0
		assert drawing.getKey() == ""
	}

	@Test
	void updateTSKeysOutOfSync() {
		println "updateTSKeysOutOfSync"

		def ctx = new FakeContext()
		def root = ctx.getEnv().getObjectsByType("ROOT").first()
		def to = ctx.getEnv().createObject( ObjectType.DRAWING.toUpperCase() )
		ctx.getEnv().setRelation(root, to)
		ctx.getEnv().setPropertyValue(to, "lastExportTs", 42)

		DLObject drawing = selectModel.getActiveDrawing().getObject( ObjectType.DRAWING )
		drawing.setKey( to.toString() )
		drawing.getObjectSetting("lastExportTs").setValue(13)

		def ex = new Exporter(ctx)
		assert !ex.updateExportTs(omodel, cmodel)
		assert drawing.getKey() != ""
	}

	//updateTS multy cases
	//blank-good
	//diff-good
	//(good-good)
	//

	@Test
	void updateTSOneBlankOneGood() {
		println "updateTSOneBlankOneGood"
		def ctx = new FakeContext()
		def root = ctx.getEnv().getObjectsByType("ROOT").first()
		def to = ctx.getEnv().createObject( ObjectType.DRAWING.toUpperCase() )
		ctx.getEnv().setRelation(root, to)
		ctx.getEnv().setPropertyValue(to, "lastExportTs", 42)

		cmodel.newComponent("drawing")
		DLObject drawing = selectModel.getActiveDrawing().getObject( ObjectType.DRAWING )
		drawing.setKey( to.toString() )
		drawing.getObjectSetting("lastExportTs").setValue(42)

		def ex = new Exporter(ctx)

		assert ex.updateExportTs(omodel, cmodel)
		assert drawing.key == to.toString() //should not be wiped in this case

	}

	@Test
	void updateTSOneBadOneGood() {
		println "updateTSOneBadOneGood"

		def ctx = new FakeContext()
		def root = ctx.getEnv().getObjectsByType("ROOT").first()

		def to = ctx.getEnv().createObject( ObjectType.DRAWING.toUpperCase() )
		ctx.getEnv().setRelation(root, to)
		ctx.getEnv().setPropertyValue(to, "lastExportTs", 42)

		DLObject drawing = selectModel.getActiveDrawing().getObject( ObjectType.DRAWING )
		drawing.setKey( to.toString() )
		drawing.getObjectSetting("lastExportTs").setValue(42)

		def to2 = ctx.getEnv().createObject( ObjectType.DRAWING.toUpperCase() )
		ctx.getEnv().setRelation(root, to2)
		ctx.getEnv().setPropertyValue(to2, "lastExportTs", 42)

		def drawing2 = cmodel.newComponent("drawing")
		drawing2.getObject( ObjectType.DRAWING ).setKey( to2.toString() )
		drawing2.getObject( ObjectType.DRAWING ).getObjectSetting("lastExportTs").setValue(13)

		def ex = new Exporter(ctx)

		assert !ex.updateExportTs(omodel, cmodel)
		assert drawing.key == to.toString() //should not be wiped in this case
		assert drawing2.getObject( ObjectType.DRAWING ).key == to2.toString()
	}

	@Test
	public void testAPIExport() throws Exception {
		println "testAPIExport"
		def drawing = selectModel.getActiveDrawing()
		println "the drawing is $drawing"
		def room = cmodel.newComponent("room")
		def wsn = cmodel.newComponent("network")
		def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode2")
		def gateway = cmodel.newComponent("remote-gateway")
		drawing.addChild(room)
		drawing.addChild(wsn)
		room.addChild(rack)
		room.addChild(gateway)
		wsn.addChild(rack)
		wsn.addChild(gateway)

		println "drawing children are ${drawing.getObject( ObjectType.DRAWING ).getChildren()}"

		def ctx = new FakeContext()
		def ex = new Exporter(ctx)

		ex.export(omodel)

		FakeEnvironment env = FakeEnvironment.instance;

		//println env.getAllWorldProperties()
		omodel.getObjects().each { o ->
			assert o.hasKey()
			println "$o == ${o.getKey()}"
			o.getPropertiesByType("setting").each { p ->
				Setting s = (Setting) p

				// exporter doesn't export null properties.
				if( s.getValue() == null ) {
					TestUtils.shouldFail( PropertyNotFoundException ) {
						env.getPropertyValue( TOFactory.getInstance().loadTO(o.getKey()), s.getName(), Class.forName( s.getValueType() ) )
					}
				} else {
					assert env.getPropertyValue( TOFactory.getInstance().loadTO(o.getKey()), s.getName(), Class.forName( s.getValueType() ) ).equals(s.getValue())
				}
			}
		}
	}


}
