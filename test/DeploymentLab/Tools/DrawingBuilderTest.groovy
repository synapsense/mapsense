package DeploymentLab.Tools

import DeploymentLab.CentralCatalogue
import DeploymentLab.DeploymentLabWindow
import DeploymentLab.Model.ObjectType
import DeploymentLab.SmartZone.LinkedProjectListener
import com.panduit.sz.api.shared.ByteArray
import com.panduit.sz.api.shared.Point2D
import com.panduit.sz.api.ss.assets.Floorplan
import com.panduit.sz.api.ss.assets.Rack;
import org.junit.Test
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.ObjectModel
import DeploymentLab.UndoBuffer
import DeploymentLab.SelectModel
import DeploymentLab.channellogger.Logger
import org.junit.BeforeClass
import DeploymentLab.TestUtils
import org.junit.Before
import org.junit.After
import DeploymentLab.UserPreferences;

/**
 * Tests for the DrawingBuilder.
 *
 *
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 10/10/12
 * Time: 2:30 PM
 */
public class DrawingBuilderTest {
	ComponentModel cmodel = null
	ObjectModel omodel = null
	static UndoBuffer ub
	static SelectModel selectModel
	static DrawingBuilder drawingBuilder
	LinkedProjectListener linkedProjectListener

	private static final Logger log = Logger.getLogger(DrawingBuilderTest.class.getName())

	private static final Integer SZ_LOCATION_ID = 1
	private static final String SZ_LOCATION_NAME = "SZLocation1"
	private static final String SZ_RACK_NAME = "SZRack1"
	private byte[] bytes

	/**
	 * One-time setup before all tests run. Creates a common static instance of the model for all the tests to use.
	 */
	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {

		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()
		ub = CentralCatalogue.getUndoBuffer()

		def up = new UserPreferences()
		def pa = new ProxyAdder(selectModel,cmodel,ub)
		drawingBuilder = new DrawingBuilder(cmodel,up,pa,selectModel)
		cmodel.clear()

		File imgFile = new File('build/testFiles/simplebg.png')
		InputStream is = new FileInputStream(imgFile)
		bytes = is.getBytes()

		linkedProjectListener = new LinkedProjectListener()
		cmodel.addModelChangeListener(linkedProjectListener)
	}

	/**
	 * Cleanup after each test.
	 */
	@After
	void tearDown() {
		omodel = null
		cmodel = null
	}


	//these first two spawn UI, so we'll need to do something a little more clever to test them directly

	//@Test
	public void testAddDrawing() throws Exception {

	}

	//@Test
	public void testMoveSelectionToAnotherDrawing() throws Exception {

	}

	@Test
	public void testMakeDrawingFromSelection() throws Exception {
		TestUtils.loadProjectFile("build/testFiles/HWings two floors plus calcs.dlz", ub, omodel, cmodel)

		def selection = []
		//to make this a little more forwards compatible, need to select by names
		//need to grab everything in "room3" and "hwing math"
		selection.addAll( cmodel.getComponentsByName("hwing math").first().getChildComponents() )
		selection.addAll( cmodel.getComponentsByName("room3").first().getChildComponents() )

		println selection
		assert selection.size() == 10
		selectModel.setSelection(selection)

		int numberOfComponentsBefore = cmodel.getComponents().size()

		drawingBuilder.makeDrawingFromSelection(selection)

		assert cmodel.getComponents().size() == numberOfComponentsBefore + 3 //plus one proxy, one Drawing, and one cloned container
	}

	@Test
	public void testFP_WithNoBG_NoComponents() {
		drawingBuilder.addDrawing(Floorplan.builder().id(1).name(SZ_LOCATION_NAME).background(Optional.empty()).width(0.0).height(0.0).build())
		assert cmodel.getComponents().size() == 1
		assertsForFPWithNoBG()
	}

	@Test
	public void testFP_WithBG_NoComponents() {
		drawingBuilder.addDrawing(Floorplan.builder().id(1).name(SZ_LOCATION_NAME).background(ByteArray.of(bytes)).width(4800).height(6000).build())
		assert cmodel.getComponents().size() == 1
		assertsForFPWithBG()
	}

	@Test
	public void testFP_WithNoBG_WithRacks() {
		List racks = new ArrayList();
		racks.add(Rack.builder().id(1).name(SZ_RACK_NAME).location(Point2D.builder().x(0.0).y(0.0).build()).width(50.0).depth(50.0).rotation(0.0).build());
		drawingBuilder.addDrawing(Floorplan.builder().id(1).name(SZ_LOCATION_NAME).width(0.0).height(0.0).background(Optional.empty()).racks(racks.asImmutable()).build())

		assertsForFPWithNoBG()
		assert !cmodel.getComponentsByName(SZ_RACK_NAME, null).get(0).getComponentProperty('width').isEditable()
		assert !cmodel.getComponentsByName(SZ_RACK_NAME, null).get(0).getComponentProperty('depth').isEditable()
	}

	@Test
	public void testFP_WithBG_WithRacks() {

		List racks = new ArrayList();
		racks.add(Rack.builder().id(1).name(SZ_RACK_NAME).location(Point2D.builder().x(100.0).y(100.0).build()).width(50.0).depth(50.0).rotation(0.0).build());
		drawingBuilder.addDrawing(Floorplan.builder().id(1).name(SZ_LOCATION_NAME).width(4800).height(6000).background(ByteArray.of(bytes)).racks(racks.asImmutable()).build())

		assert !cmodel.getComponentsByName(SZ_RACK_NAME, null).get(0).getComponentProperty('width').isEditable()
		assert !cmodel.getComponentsByName(SZ_RACK_NAME, null).get(0).getComponentProperty('depth').isEditable()
		assertsForFPWithBG()
	}

	private void assertsForFPWithBG() {
		assert cmodel.getComponents() != null
		assert selectModel.getActiveDrawing().getScale() == 12
		assert selectModel.getActiveDrawing().getSmartZoneId() == SZ_LOCATION_ID
		assert selectModel.getActiveDrawing().getName() == SZ_LOCATION_NAME
		assert selectModel.getActiveDrawing().getComponentProperty('width').value == 400
		assert selectModel.getActiveDrawing().getComponentProperty('height').value == 584
	}

	private void assertsForFPWithNoBG() {
		assert cmodel.getComponents() != null
		assert selectModel.getActiveDrawing().getScale() == 1
		assert selectModel.getActiveDrawing().getName() == SZ_LOCATION_NAME
		assert selectModel.getActiveDrawing().getComponentProperty('width').value == 1800
		assert selectModel.getActiveDrawing().getComponentProperty('height').value == 1200
	}

}
