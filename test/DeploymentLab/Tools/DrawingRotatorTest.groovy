package DeploymentLab.Tools

import DeploymentLab.CentralCatalogue
import DeploymentLab.DisplayProperties
import DeploymentLab.Image.DLImage
import DeploymentLab.Model.ComponentModel
import DeploymentLab.Model.DLComponent
import DeploymentLab.Model.ObjectModel
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.SelectModel
import DeploymentLab.TestUtils
import DeploymentLab.UndoBuffer
import org.junit.After
import org.junit.Before
import org.junit.Test

import javax.swing.ButtonGroup;

/**
 * Created by LNI on 4/12/2016.
 */
public class DrawingRotatorTest {
    static ObjectModel omodel
    static ComponentModel cmodel
    static UndoBuffer ub
    static SelectModel selectModel
    static DeploymentPanel dp
    static ToolManager toolManager

    final static double ROTATE_RIGHT = Math.PI / 2.0
    final static double ROTATE_LEFT = -Math.PI / 2.0

    DLComponent drawing

    @Before
    void setup() {
        (omodel,cmodel) = TestUtils.initModels()
        selectModel = CentralCatalogue.getSelectModel()
        ub = CentralCatalogue.getUndoBuffer()

        toolManager = new ToolManager(new ButtonGroup())

        dp = new DeploymentPanel(selectModel, ub, cmodel, new ToolManager(new ButtonGroup()), new DisplayProperties( selectModel ))
        CentralCatalogue.setDeploymentPanel( dp )

        toolManager.registerTool('mover', new NodeMover(ub, dp, selectModel))

        drawing = cmodel.newComponent("drawing")
        selectModel.setActiveDrawing(drawing)
    }

    /**
     * Cleanup after each test.
     */
    @After
    void tearDown() {
        omodel = null
        cmodel = null
        selectModel = null
    }

    @Test
    void testRotateDrawingRight_WithBg() {
        File imgFile = new File('build/testFiles/simplebg.png')
        DLImage bgImage = new DLImage(imgFile)

        drawing.setPropertyValue("hasImage", true)
        drawing.setPropertyValue('image_data', bgImage)
        drawing.setPropertyValue('width', bgImage.getWidth())
        drawing.setPropertyValue('height', bgImage.getHeight())

        DLComponent ez_rack = mockRack()

        drawing.addChild(ez_rack)

        assert dp.getBackgroundImageWidth() == 400
        assert dp.getBackgroundImageHeight() == 584
        assert dp.getBlankCanvasWidth() == 400
        assert dp.getBlankCanvasHeight() == 584

        DrawingRotator dRotator = new DrawingRotator(cmodel, selectModel, ub, dp)
        dRotator.editRotate90(ROTATE_RIGHT)

        assert ez_rack.getPropertyValue("rotation") == 270
        assert dp.getBackgroundImageWidth() == 584
        assert dp.getBackgroundImageHeight() == 400
        assert dp.getBlankCanvasWidth() == 584
        assert dp.getBlankCanvasHeight() == 400

        dRotator.editRotate90(ROTATE_RIGHT)
        assert ez_rack.getPropertyValue("rotation") == 180

        dRotator.editRotate90(ROTATE_RIGHT)
        assert ez_rack.getPropertyValue("rotation") == 90

        dRotator.editRotate90(ROTATE_RIGHT)

        assert ez_rack.getPropertyValue("x") == 197
        assert ez_rack.getPropertyValue("y") == 209
        assert ez_rack.getPropertyValue("rotation") == 0
    }

    @Test
    void testRotateDrawingLeft_WithoutBg() {
        DLComponent ez_rack = mockRack()
        drawing.addChild(ez_rack)

        DrawingRotator dRotator = new DrawingRotator(cmodel, selectModel, ub, dp)
        dRotator.editRotate90(ROTATE_LEFT)
        assert ez_rack.getPropertyValue("rotation") == 90

        dRotator.editRotate90(ROTATE_LEFT)
        assert ez_rack.getPropertyValue("rotation") == 180

        dRotator.editRotate90(ROTATE_LEFT)
        assert ez_rack.getPropertyValue("rotation") == 270

        dRotator.editRotate90(ROTATE_LEFT)

        assert ez_rack.getPropertyValue("x") == 197
        assert ez_rack.getPropertyValue("y") == 209
        assert ez_rack.getPropertyValue("rotation") == 360
    }

    @Test
    void testRotate_RectangleShaped() {
        DLComponent containedRec = cmodel.newComponent("contained_rectangle")
        containedRec.setPropertyValue('width', 42)
        containedRec.setPropertyValue('depth', 24)
        drawing.addChild(containedRec)

        DrawingRotator dRotator = new DrawingRotator(cmodel, selectModel, ub, dp)
        dRotator.editRotate90(ROTATE_LEFT)
        assert containedRec.getPropertyValue('width') == 24
        assert containedRec.getPropertyValue('depth') == 42

        dRotator.editRotate90(ROTATE_RIGHT)
        assert containedRec.getPropertyValue('width') == 42
        assert containedRec.getPropertyValue('depth') == 24
    }

    private DLComponent mockRack() {
        DLComponent ez_rack = cmodel.newComponent("ez-rack-1-0")
        ez_rack.setPropertyValue('name', "Rack1")
        ez_rack.setPropertyValue('x', 197)
        ez_rack.setPropertyValue('y', 209)
        ez_rack.setPropertyValue('width', 42)
        ez_rack.setPropertyValue('depth', 24)
        ez_rack.setPropertyValue('rotation', 0)

        return ez_rack
    }
}