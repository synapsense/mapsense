package DeploymentLab.Tools

import DeploymentLab.CentralCatalogue
import DeploymentLab.DLProject
import DeploymentLab.Exceptions.UnknownComponentException
import DeploymentLab.Model.LogicalGroupController
import DeploymentLab.Model.ObjectModel
import DeploymentLab.Model.ComponentModel
import DeploymentLab.SelectModel
import org.junit.Before
import org.junit.After
import DeploymentLab.UndoBuffer
import DeploymentLab.UIDisplay
import DeploymentLab.Tree.ConfigurationReplacerTreeModel
import DeploymentLab.Tree.ConfigurationReplacerTree
import javax.swing.JFrame
import DeploymentLab.ComponentLibrary
import org.junit.Test
import org.apache.commons.io.FileUtils
import DeploymentLab.channellogger.ChannelManager
import DeploymentLab.channellogger.Logger
import DeploymentLab.Model.Dockingpoint
import DeploymentLab.Model.DLObject
import org.junit.BeforeClass
import DeploymentLab.TestUtils
import DeploymentLab.Model.DLComponent
import javax.swing.ButtonGroup
import DeploymentLab.DisplayProperties
import DeploymentLab.SceneGraph.DeploymentPanel
import DeploymentLab.ProblemLogger
import DeploymentLab.channellogger.LogLevel
import DeploymentLab.Model.ModelState
import DeploymentLab.ProjectSettings

/**
 *
 * @author Gabriel Helman
 * @since
 * Date: 2/2/12
 * Time: 10:05 AM
 */
class ConfigurationReplacerTest {

	static ObjectModel omodel
	static ComponentModel cmodel

	static SelectModel selectModel
	static UndoBuffer ub
    static DeploymentPanel deploymentPanel
    static DisplayProperties displayProperties

	static UIDisplay uiDisplay
	static ConfigurationReplacerTree tree
	static ConfigurationReplacer cr
	private static final Logger log = Logger.getLogger(ConfigurationReplacerTest.class.getName())

    static final controlRoles = ["controlstrategy","controlleddevice"]

	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging(DeploymentLab.channellogger.LogLevel.ERROR)
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()
		ub = CentralCatalogue.getUndoBuffer()

		def treeModels = []
		def cmtm = new ConfigurationReplacerTreeModel(cmodel, "FakeTree")
		treeModels.add(cmtm)
		tree = new ConfigurationReplacerTree(cmtm)

		//trees go in clib
		def clib = ComponentLibrary.INSTANCE;
		clib.init(cmodel,null,treeModels);

		cr = new ConfigurationReplacer(selectModel, cmodel, tree, ub, new JFrame())
		//fortunately, the tool manager doesn't need to DO anything here
		def toolManager = new ToolManager(new ButtonGroup())
		displayProperties = new DisplayProperties( selectModel )
        selectModel.addSelectionChangeListener( displayProperties )
		deploymentPanel = new DeploymentPanel(selectModel, ub, cmodel, toolManager, displayProperties)
		CentralCatalogue.setDeploymentPanel( deploymentPanel )

		ChannelManager cm = ChannelManager.getInstance()
		cm.addChannelListener(new ProblemLogger(LogLevel.FATAL))
		//cm.addChannelListener(new ProblemLogger(LogLevel.ERROR))
		cmodel.metamorphosisFinished( ModelState.INITIALIZING )

		// So exportable gets set correctly upon upgrade.
		cmodel.addModelChangeListener( new LogicalGroupController()  )
	}


	@After
	void tearDown() {
		cmodel = null
		omodel = null
		selectModel = null
		ub = null
		uiDisplay = null
		tree = null
		cr = null
		//have fun, garbage collector!
	}

	
	@Test
	void doTheseStubsWork(){
		log.warn("doTheseStubsWork")
		println cr.toString()
	}

	@Test
	void basicReplaceAll(){
		log.warn("basicReplaceAll")
		new DLProject( CentralCatalogue.getUndoBuffer(), cmodel, omodel, null, true )
		def room = cmodel.newComponent("room")
		selectModel.getActiveDrawing().addChild(room)
		def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode")
		rack.setPropertyValue("name", "rackname")
		def rack2 = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode")
		rack2.setPropertyValue("name", "otherrack")
		room.addChild(rack)
		room.addChild(rack2)
		cr.replaceEverything(false,false)
	}

	@Test
	void basicUpdateAll(){
		log.warn("basicUpdateAll")
		new DLProject( CentralCatalogue.getUndoBuffer(), cmodel, omodel, null, true )
		def room = cmodel.newComponent("room")
		selectModel.getActiveDrawing().addChild(room)
		def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode")
		rack.setPropertyValue("name", "rackname")
		def rack2 = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode")
		rack2.setPropertyValue("name", "otherrack")
		room.addChild(rack)
		room.addChild(rack2)
		cr.replaceEverything(true,false)
	}

	@Test
	void loadAFileKeepKeys(){
		log.warn("loadAFileKeepKeys")

		// Don't provide the CR so this will load without upgrading. And use the current LINT so we don't have issues
		// that would require the Upgrade Controller.
		TestUtils.loadProjectFile( TestUtils.CURRENT_VERSION_LINT, ub, omodel, cmodel )

		//work up a map of old DLID : ES Key
		def oldKeys = [:]
		def notExported = []
		for(DLObject o : omodel.getObjects()){
			// Make sure the project file is in the state we expect... except for things that don't get exported.
			if( o.isExportable() ) {
				assert o.hasKey(), "BEFORE UPGRADE: no key for $o on component ${cmodel.getManagingComponent(o)}"
			} else {
				notExported.add( o.getDlid() )
			}
			oldKeys[o.getDlid()] = o.getKey()
		}

		cr.replaceEverything(true,false)
		log.info("update done, checking keys...")
		for(DLObject o : omodel.getObjects()){

			// Get the new id. Obsolete components are not upgraded so the "new" id is the old id.
			def oldid = ProjectSettings.instance.findOldDlid(o.getDlid())
			if( oldid == null ) {
				oldid = o.getDlid()
			}

			if( o.isExportable() ) {
				assert o.hasKey(), "no key for $o (old: $oldid) on component ${cmodel.getManagingComponent(o)} (old: ${ProjectSettings.instance.findOldDlid(cmodel.getManagingComponent(o).getDlid())})"
				assert !notExported.contains( oldid ), "Object is exportable when it wasn't before the upgrade: $o (old: $oldid)  owner: ${cmodel.getManagingComponent(o)} (old: ${ProjectSettings.instance.findOldDlid(cmodel.getManagingComponent(o).getDlid())})"
			} else {
				assert notExported.contains( oldid ), "Object is not exportable when it was before the upgrade: $o (old: $oldid)  owner: ${cmodel.getManagingComponent(o)} (old: ${ProjectSettings.instance.findOldDlid(cmodel.getManagingComponent(o).getDlid())})"
			}

			if(oldKeys.containsKey(oldid)){
				assert oldKeys[oldid] == o.getKey()
			} else {
				assert o.getKey() == ""
			}

            // Also check network security keys, for objects that have them.
            if( o.hasObjectProperty('networkPublicKey') ) {
                assert o.getObjectProperty('networkPublicKey').getValue()
            }

            if( o.hasObjectProperty('networkPrivateKey') ) {
                assert o.getObjectProperty('networkPrivateKey').getValue()
            }

			//also check all dockingpoints
			for(Dockingpoint d : o.getPropertiesByType("dockingpoint")){
				if (d.getReferrent()){
					assert !d.getReferrent().deleted

				}
			}
		}
		//now make sure we can close the file
		cmodel.clear()
	}



	/**
	 * Bug 7150: J_Racks can't be turned into anything else.
	 *
	 * NOTE: As of Aireo, j_racks are no longer supported. Since they may return, I just changed this to expect the
	 * failure so we're notified if it is reactivated and can re-enable this test.
	 */
	@Test(expected = UnknownComponentException.class)
	void jawaReplacementEdgeCase(){
		log.warn("jawaReplacementEdgeCase")
        def drawing = cmodel.newComponent("drawing")
		def wsnnet = cmodel.newComponent("network")
		def room = cmodel.newComponent("room")

		def jawa = cmodel.newComponent("j_rack")
		def sc = cmodel.newComponent("sc_poweronly")

        drawing.addChild(wsnnet)
        drawing.addChild(room)
		wsnnet.addChild(sc)
		room.addChild(sc)
		room.addChild(jawa)

		assert cr.isReplacable(sc, "sc_poweronly")
		assert cr.isReplacable(sc, "rack-control-rearexhaust-sf-rt-sch-sc")

		assert cr.isReplacable(jawa, "j_rack")
		assert ! cr.isReplacable(jawa, "rack-control-rearexhaust-sf-rt-sch-sc")
		assert ! cr.isReplacable(jawa, "sc_poweronly")
	}

	/**
	 * Every kind of rack should be replaceable into any other kind of rack
	 */
	@Test
	void racksCanBeAnyOtherRack(){
		log.warn("racksCanBeAnyOtherRack")
        def drawing = cmodel.newComponent("drawing")
		def wsnnet = cmodel.newComponent("network")
		def room = cmodel.newComponent("room")
        drawing.addChild(wsnnet)
        drawing.addChild(room)

		//find everything that counts as a "Rack"
		def racks = []
		Set<String> placeableTypes = cmodel.listTypesWithRole("placeable")
		Set<String> staticchildTypes = cmodel.listTypesWithRole("staticchild")
		Set<String> componentTypes = placeableTypes - staticchildTypes
		for (String t: componentTypes.sort()) {
			def comp = cmodel.newComponent(t)
			if(comp.getManagedObjectsOfType("rack").size() > 0){
				racks += t
			}
		}

		for(String r : racks){
			println "racksCanBeAnyOtherRack: Checking $r"
			def currentRack = cmodel.newComponent(r)
			wsnnet.addChild(currentRack)
			room.addChild(currentRack)

			for(String other : racks){
				assert cr.isReplacable(currentRack,other)
			}
		}

	}

	/**
	 * Every kind of crac/crah should be replaceable into any other kind of crac/crah
	 */
	@Test
	void crahsCanBeAnyOtherCrah(){
		log.warn("crahsCanBeAnyOtherCrah")
		def drawing = cmodel.newComponent("drawing")
		def wsnnet = cmodel.newComponent("network")
		def room = cmodel.newComponent("room")
		drawing.addChild(wsnnet)
		drawing.addChild(room)

		//find everything that counts as a "CRAC/CRAH"
		def crahs = []
		Set<String> placeableTypes = cmodel.listTypesWithRole("placeable")
		Set<String> staticchildTypes = cmodel.listTypesWithRole("staticchild")
		Set<String> componentTypes = placeableTypes - staticchildTypes
		for (String t: componentTypes.sort()) {
			if( cmodel.listManagedObjectTypes(t).contains('crac') ){
				crahs += t
			}
		}

		assert crahs.size() > 1

		for(String t : crahs){
			println "crahsCanBeAnyOtherCrah: Checking $t"
			def crah = cmodel.newComponent(t)
			wsnnet.addChild( crah )
			room.addChild( crah )

			for( String otherT : crahs ) {
				assert cr.isReplacable( crah, otherT )
			}
		}
	}

	/**
	 * Types with different parent types (networks/groupings) can't be turned into each other
	 */
	@Test
	void oneThingCantBeOtherThing(){
		log.warn("oneThingCantBeOtherThing")
        def drawing = cmodel.newComponent("drawing")
		def modnet = cmodel.newComponent("modbus-tcp-network")
		def wsnnet = cmodel.newComponent("network")
		def room = cmodel.newComponent("room")

        drawing.addChild(modnet)
        drawing.addChild(wsnnet)
        drawing.addChild(room)

		def modbus = cmodel.newComponent("modbus-input-numeric")
		def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode2")

		modnet.addChild(modbus)
		wsnnet.addChild(rack)
		room.addChild(rack)

		assert ! cr.isReplacable(modbus, "rack-control-rearexhaust-sf-thermanode2")
		assert ! cr.isReplacable(rack, "modbus-input-numeric")
	}


	/**
	 * Types on the irreplaceable list can only be converted into themselves, nothing else.
	 */
	@Test
	void irreplacables(){
		log.warn("irreplacables")
		assert !cmodel.isLoadedComponentType('j_rack') // safety net in case we want to go back to using jawa for type1
		String type1 = "contained_poly" //"j_rack"
		String type2 = "contained_rectangle"

		def drawing = cmodel.newComponent("drawing")
		def room = cmodel.newComponent("room")
		def c1 = cmodel.newComponent( type1 )
        drawing.addChild(room)
		room.addChild(c1)

		assert cr.isReplacable( c1, type1 )
		def c2 = cmodel.newComponent( type2 )
		drawing.addChild(c2)

		assert ! cr.isReplacable( c2, type1 )
		assert cr.isReplacable( c2, type2 )
		assert ! cr.isReplacable( c1, type2 )
	}

    /**
     * Control components can be replaced by other control components in the same role.
     */
	@Test
    void controlComponentsCanBeOthersOfSameRole() {
        log.warn("controlComponentsCanBeOthersOfSameRole")

        controlRoles.each { role ->
            // Get all the component types with this role.
            List<String> cTypes = cmodel.listTypesWithRole( role )

            // Create components for them and add them to the REIN. Repeat if there aren't enough to have each REIN case satisfied.
            List<DLComponent> comps = []

			cTypes.each { type ->
				DLComponent c = cmodel.newComponent( type )
				comps.add( c )
			}
			
            // See if each can be replaced by the others.
            comps.each { c ->
                cTypes.each { type ->
					//we only want to compare things with the same set of roles (except for a few cases)
					Set oldRoles = c.rolesSet()
					Set newRoles = new HashSet<String>(cmodel.listComponentRoles(type))
					oldRoles.remove("defaultcontrolinput")
					newRoles.remove("defaultcontrolinput")
					if( oldRoles.equals(newRoles) ){
						//println "checking for ${c.getType()} to $type"
                    	assert cr.isReplacable( c, type )
					}
                }
            }
        }
    }

    // todo: Should control components be allowed to be replaced by one of a different role? (strategy -> device?) Currently works but wasn't sure if it was desired.


    /**
     * Repalcing a rack that has a COIN that is inside a damper halo was causing an exception. This test mimics that
     * specific setup and action.
     */
    //@Test //lets not worry about dampers
    void rackWithCoinsChildedToDamper() {

        // Setup a simple Drawing with a damper that has a COIN in its halo.
        def drawing = cmodel.newComponent("drawing")
        def net = cmodel.newComponent("network")
        drawing.addChild(net)
        def room = cmodel.newComponent("room")
        drawing.addChild(room)

        def rack = cmodel.newComponent("rack-control-rearexhaust-nsf-rt-thermanode2")
        room.addChild(rack)
        net.addChild(rack)
        TestUtils.setLocation(rack,200,200)

        def damper = cmodel.newComponent("control_register_damper")
        room.addChild(damper)
        net.addChild(damper)
        TestUtils.setLocation(damper,150,150)

        ControlInputAdder coinAdder = new ControlInputAdder(selectModel, cmodel, ub, null, null, deploymentPanel, displayProperties, new JFrame(), null)
        def coin = coinAdder.attachControlPoint( rack, rack.getProducer("topinlet"), "controlpoint_single_temperature", false )
        TestUtils.setLocation( coin, (double) rack.getPropertyValue('x'), (double) rack.getPropertyValue('y') )

        // The COIN should be a child of the damper now.
        assert damper.getChildComponents().size() == 1
        assert damper.getChildComponents().get(0) == coin
        assert !cmodel.getComponentsByType("rack-control-rearexhaust-sf-thermanode2")

        // Replace the rack with soemthing else.
        tree.setConfiguration("rack-control-rearexhaust-sf-thermanode2")
        def replacables = [ rack ]

        cr.replaceConfig( replacables, false )

        // Makes sure it was replaced.
        assert cmodel.getComponentsByType("rack-control-rearexhaust-sf-thermanode2")
        assert !cmodel.getComponentsByType("rack-control-rearexhaust-nsf-rt-thermanode2")

    }

	@Test
	void replaceWithProxies() {
		// Load the proxy project.
		TestUtils.loadProjectFile( "build/testFiles/proxies.dlz", ub, omodel, cmodel )

		// Save some original model stats.
		int origNumObjs  = omodel.getObjectCount()
		int origNumComps = cmodel.getComponents().size()
		int origNumProxyObjs = omodel.getObjectsByType('proxy').size()

		int origNumProxyComps = 0
		int origNumCompsWithProxies = 0
		for( DLComponent c : cmodel.getComponents() ) {
			if( c.hasRole("proxy") ) {
				origNumProxyComps++
			} else if( c.hasProxy() ) {
				origNumCompsWithProxies++
			}
		}

		// Replace!
		cr.replaceEverything(true,false)

		// Compute some new stats.
		int numProxyComps = 0
		int numCompsWithProxies = 0
		for( DLComponent c : cmodel.getComponents() ) {
			if( c.hasRole("proxy") ) {
				numProxyComps++
			} else if( c.hasProxy() ) {
				numCompsWithProxies++
			}
		}

		// Compare the before and after.
		assert origNumObjs == omodel.getObjectCount()
		assert origNumComps == cmodel.getComponents().size()
		assert origNumProxyObjs == omodel.getObjectsByType('proxy').size()
		assert origNumProxyComps == numProxyComps
		assert origNumCompsWithProxies == numCompsWithProxies
	}
}
