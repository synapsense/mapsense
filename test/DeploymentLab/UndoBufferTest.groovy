package DeploymentLab

import org.junit.*
import DeploymentLab.Model.*
import groovy.mock.interceptor.StubFor
import javax.swing.JFrame

/**
 *
 * @author Gabriel Helman
 * @since
 * Date: 10/25/11
 * Time: 11:31 AM
 */
class UndoBufferTest {

	static ObjectModel omodel
	static ComponentModel cmodel
	static UndoBuffer ub
	static SelectModel selectModel
	//static DeploymentLabWindow dlw

	def progress = new StubFor(ProgressManager)

	@BeforeClass
	static void classSetup() {
		// Turn on error logging to help when something fails internal to a method we are testing.
		TestUtils.enableLogging( DeploymentLab.channellogger.LogLevel.WARN )
	}

	@Before
	void setup() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()
		ub = CentralCatalogue.getUndoBuffer()

		//groovy stubs to fake out the progress manager
		//using .ignore since we're not testing calls to the PM
		progress.ignore.doTask{ c -> c.call() }
		progress.ignore.bump{ }
		progress.ignore.setMaxProgress{ }
	}

	@Test
	void basicUndoTest(){
		def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode")
		rack.setPropertyValue("name", "original name")
		rack.setPropertyValue("name", "new name")
		assert rack.getPropertyStringValue("name").equals("new name")
		progress.use{ ub.undo()	}
		assert rack.getPropertyStringValue("name").equals("original name")
	}

	@Test
	void doUndoDoUndoTest(){
		progress.use{
			def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode")
			rack.setPropertyValue("name", "original name")
			rack.setPropertyValue("name", "new name")
			assert rack.getPropertyStringValue("name").equals("new name")
			ub.undo()
			assert rack.getPropertyStringValue("name").equals("original name")

			rack.setPropertyValue("name", "some other name")
			assert rack.getPropertyStringValue("name").equals("some other name")

			ub.undo()

			assert rack.getPropertyStringValue("name").equals("original name")
			println "wooo"
		}
	}

	@Test
	void doUndoDoUndoCompositeLocationTest(){
		progress.use{
			def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode")
			assert rack.getPropertyValue("x") == 0
			assert rack.getPropertyValue("y") == 0

			ub.startOperation()
			rack.setPropertyValue("x", 11)
			rack.setPropertyValue("y", 12)
			ub.finishOperation()
			assert rack.getPropertyValue("x") == 11
			assert rack.getPropertyValue("y") == 12

			ub.undo()
			assert rack.getPropertyValue("x") == 0
			assert rack.getPropertyValue("y") == 0

			ub.startOperation()
			rack.setPropertyValue("x", 21)
			rack.setPropertyValue("y", 22)
			ub.finishOperation()
			assert rack.getPropertyValue("x") == 21
			assert rack.getPropertyValue("y") == 22

			ub.undo()
			assert rack.getPropertyValue("x") == 0
			assert rack.getPropertyValue("y") == 0

			println "wooo2"
		}
	}

	
	@Test
	void doUndoDoUndoCompositeNotLocationTest(){
		progress.use{
			def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode")
			rack.setPropertyValue("name", "start name")
			rack.setPropertyValue("location", "start loc")
			assert rack.getPropertyValue("name").equals("start name")
			assert rack.getPropertyValue("location").equals("start loc")

			ub.startOperation()
			rack.setPropertyValue("name", "name 1")
			rack.setPropertyValue("location", "loc 1")
			ub.finishOperation()
			assert rack.getPropertyValue("name").equals("name 1")
			assert rack.getPropertyValue("location").equals("loc 1")

			ub.undo()
			assert rack.getPropertyValue("name").equals("start name")
			assert rack.getPropertyValue("location").equals("start loc")

			ub.startOperation()
			rack.setPropertyValue("name", "name 2")
			rack.setPropertyValue("location", "loc 2")
			ub.finishOperation()
			assert rack.getPropertyValue("name").equals("name 2")
			assert rack.getPropertyValue("location").equals("loc 2")

			ub.undo()
			assert rack.getPropertyValue("name").equals("start name")
			assert rack.getPropertyValue("location").equals("start loc")

			println "wooo3"
		}
	}
	
	
	@Test
	void doUndoDoUndoRotationTest(){
		progress.use{
			def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode")
			//println "set1"
			rack.setPropertyValue("rotation", 0.0)
			assert rack.getPropertyValue("rotation") == 0.0

			//println "set2"
			rack.setPropertyValue("rotation", 45.0)
			assert rack.getPropertyValue("rotation") == 45.0

			ub.undo()
			assert rack.getPropertyValue("rotation") == 0.0

			//println "set3"
			rack.setPropertyValue("rotation", 90.0)
			assert rack.getPropertyValue("rotation") == 90.0

			ub.undo()
			assert rack.getPropertyValue("rotation") == 0.0
			println "wooo4"
		}
	}

	/**
	 * Multiple x & y changes to the same component in a row, within the same operation, should get "squashed" to a single undo action.
     *
     * The behavior was changed slightly under Bug 7311 (hence the commented-out lines in the test). Prior to this, multiple changes
     * would be squashed even across operations. This had at least one undesired side-effect where changing X/Y just after adding a
     * new component would actually remove the component when trying to undo the move since the X/Y change was bundled with the first
     * transaction, which happened to be the new component being added as well as the initial X/Y values getting set. This was seen
     * with one of the container components. This change still allows single operations, such as dragging a component, to be squashed.
	 */
	@Test
	void squashLocTest(){
		progress.use{
			def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode")
			assert rack.getPropertyValue("x") == 0
			assert rack.getPropertyValue("y") == 0

			ub.startOperation()
			rack.setPropertyValue("x", 11)
			rack.setPropertyValue("y", 12)
			//ub.finishOperation()
			assert rack.getPropertyValue("x") == 11
			assert rack.getPropertyValue("y") == 12

			//ub.startOperation()
			rack.setPropertyValue("x", 21)
			rack.setPropertyValue("y", 22)
			ub.finishOperation()
			assert rack.getPropertyValue("x") == 21
			assert rack.getPropertyValue("y") == 22

			ub.undo()
			assert rack.getPropertyValue("x") == 0
			assert rack.getPropertyValue("y") == 0

			println "wooo5"
		}
	}

	/**
	 * Any changes to things other than x, y, or rotation stop the squashing
	 */
	@Test
	void notSquashLocTest(){
		progress.use{
			def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode")
			assert rack.getPropertyValue("x") == 0
			assert rack.getPropertyValue("y") == 0

			ub.startOperation()
			rack.setPropertyValue("x", 11)
			rack.setPropertyValue("y", 12)
			ub.finishOperation()
			assert rack.getPropertyValue("x") == 11
			assert rack.getPropertyValue("y") == 12

			rack.setPropertyValue("name", "name 1")

			ub.startOperation()
			rack.setPropertyValue("x", 21)
			rack.setPropertyValue("y", 22)
			ub.finishOperation()
			assert rack.getPropertyValue("x") == 21
			assert rack.getPropertyValue("y") == 22

			ub.undo()
			assert rack.getPropertyValue("x") == 11
			assert rack.getPropertyValue("y") == 12
			assert rack.getPropertyValue("name").equals("name 1")
			println "wooo6"
		}
	}

    /**
     * Multiple x & y changes to the same component in a row across different operations, should NOT get "squashed" to a single undo action.
	 */
	@Test
	void notSquashMultiOpLocTest(){
		progress.use{
			def rack = cmodel.newComponent("rack-control-rearexhaust-sf-thermanode")
			assert rack.getPropertyValue("x") == 0
			assert rack.getPropertyValue("y") == 0

			ub.startOperation()
			rack.setPropertyValue("x", 11)
			rack.setPropertyValue("y", 12)
			ub.finishOperation()
			assert rack.getPropertyValue("x") == 11
			assert rack.getPropertyValue("y") == 12

			ub.startOperation()
			rack.setPropertyValue("x", 21)
			rack.setPropertyValue("y", 22)
			ub.finishOperation()
			assert rack.getPropertyValue("x") == 21
			assert rack.getPropertyValue("y") == 22

			ub.undo()
			assert rack.getPropertyValue("x") == 11
			assert rack.getPropertyValue("y") == 12

			println "wooo7"
		}
	}

	@Test
	public void trackActiveDrawingChanges(){
		progress.use{
			def drawing1 = selectModel.getActiveDrawing()
			def drawing2 = cmodel.newComponent("drawing")
			assert selectModel.getActiveDrawing() == drawing2

			def state1 = ub.getUndoState()
			selectModel.setActiveDrawing(drawing1)
			assert ub.getUndoState() == state1
			ub.startOperation()
			selectModel.setActiveDrawing(drawing2)
			ub.finishOperation()
			assert ub.getUndoState() != state1
			assert selectModel.getActiveDrawing() == drawing2
			ub.undo()
			assert selectModel.getActiveDrawing() == drawing1

			println "wooo8"
		}
	}
	
	@After
	void tearDown() {
		cmodel = null
		omodel = null
		ub = null
	}

}
