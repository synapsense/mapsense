import DeploymentLab.SmartZone.SmartZoneApiTestBase
import DeploymentLab.SmartZone.SmartZoneClient
import com.fasterxml.jackson.databind.ObjectMapper
import com.panduit.sz.api.ss.assets.Floorplan
import com.panduit.sz.api.ss.assets.Location
import com.panduit.sz.api.ss.assets.SsAssets
import com.panduit.sz.api.util.Jackson
import org.apache.commons.io.FileUtils

/**
 * Utility to probe a live SmartZone system to grab some real JSON to use in unit tests.
 *
 * @author Ken Scoggins
 * @since SmartZone 8.0.0
 */
class FetchResponseSamples {

	private static final String BASE_DIR = "test/testFiles/smartzone/rest"

    private SmartZoneClient smartzone
    private ObjectMapper om = Jackson.newDefaultObjectMapper()


	public FetchResponseSamples( String host, int port, String user, String pwd ) {
        smartzone = new SmartZoneClient( host, port, user, pwd )
    }

    public void fetch() {
		// Purge the old ones.
		println "Deleting existing files in 'rest'..."
		FileUtils.deleteDirectory( new File("$BASE_DIR") )

		// Grab all the locations.
		println "Fetching Location Tree..."
		List<Location> locs = smartzone.getLocationTree()
		createResourceFile( SsAssets.locationsPath(), locs )

		fetchLocations( locs )
	}

	private void fetchLocations( List<Location> locs ) {
		for( Location loc : locs ) {
			// Only floors are importable, so we only expect to ever fetch those and ignore the rest.
			if( loc.isFloorplan() ) {
				println "Fetching ${loc.getLocationLevel()} '${loc.getName()}'  id: ${loc.getId()}"
				Floorplan floor = smartzone.getFloorplan( loc.getId() )
				createResourceFile( SsAssets.floorplanPath(loc.getId()), floor )
			}

			// Drill down the tree.
			if( !loc.getChildren().isEmpty() ) {
				fetchLocations( loc.getChildren() )
			}
		}
	}

	private void createResourceFile( String endpoint, Object obj ) {
		File file = new File( "$BASE_DIR/${endpoint}.json" )

		if( !file.getParentFile().exists() ) {
			if( !file.getParentFile().mkdirs() ) {
				println "ERROR: Failed to create parent directories: ${file.getParentFile()}"
			}
		}

		println "Creating $file"
		om.writerWithDefaultPrettyPrinter().writeValue( file, obj )
	}

    // TODO: Support server args on the command line.
    public static void main(String[] args) {
        FetchResponseSamples fetcher = new FetchResponseSamples( SmartZoneApiTestBase.SMARTZONE_HOST,
                                                                 SmartZoneApiTestBase.SMARTZONE_PORT,
                                                                 SmartZoneApiTestBase.SMARTZONE_USER,
                                                                 SmartZoneApiTestBase.SMARTZONE_PWD )
        fetcher.fetch()
   	}
}
