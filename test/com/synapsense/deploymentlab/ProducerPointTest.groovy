package com.synapsense.deploymentlab

import org.junit.*
import static org.junit.Assert.*
import groovy.mock.interceptor.StubFor

import com.synapsense.rulesengine.core.environment.Property
import com.synapsense.rulesengine.core.environment.RuleAction
import com.synapsense.rulesengine.core.environment.RuleI
import com.synapsense.dto.TOFactory
import com.synapsense.deploymentlab.*
import com.synapsense.service.Environment
import GroovyRuleStubs.*
import com.synapsense.dto.TO
import com.synapsense.exception.PropertyNotFoundException

/**
 *
 * @author Gabriel Helman
 * @since
 * Date: 10/17/11
 * Time: 5:22 PM
 */
class ProducerPointTest {
	FakeEnvironment env = (FakeEnvironment)FakeEnvironment.getInstance()
	static al = FakeAlertService.getInstance()

	@Before
	void resetState(){
		env.makeTrouble(false)
		env.autoCreateType( true )
		env.reset()
		al.reset()
	}

	TO<?> rigProducerPoint(String name, String fieldName, Double value, Integer hostStatus = 1){

		//rig the fake environment
		//rig the host object
		def to = env.createObject("PRODUCERPOINT")
		env.setPropertyValue(to, "name", name)
		env.setPropertyValue(to, "fieldname", fieldName)
		env.setPropertyValue(to, "lastValue", null)

		def parent = env.createObject("HOST")
		env.setPropertyValue(parent, fieldName, value)
		if(hostStatus!=null){
			env.setPropertyValue(parent, "status", hostStatus)
		}

		env.setRelation(parent, to)
		return to
	}

	Double testerProducerPoint(TO<?> pp) {
		al.reset()
		def at = new ProducerPoint()
		return at.run(new RuleI("${pp}ProducerPoint"), new Property("lastValue", pp))
	}

	Double testerProducerPoint(String name, String fieldName, Double value) {
		TO<?> to = rigProducerPoint(name, fieldName, value)
		return testerProducerPoint(to)
	}

	Double testerProducerPointChangeValue(TO<?> t, String fieldName, Double v){
		//assume it's already been rigged
		def p = env.getParents(t).first()
		env.setPropertyValue(p, fieldName, v)
		return testerProducerPoint(t)
	}

	Double getStatus(TO<?> theDude){
		//def theDude = env.getParents(pp).first()
		return env.getPropertyValue(theDude, "status", Double.class)
	}


	Double testerProducerPower(Object input){
		al.reset()
		def to = env.createObject("PRODUCERPOWER")
		env.setPropertyValue(to, "lastValue", null)

		def sourceTO = env.createObject("SOURCE")
		env.setPropertyValue(sourceTO, "lastValue", input)

		def prop = new Property(sourceTO, to)
		def at = new ProducerPower()
		at.setInput(prop)
		return at.run(new RuleI("${to}ppower"), new Property("lastValue", to))
	}

	@Test
	void testProducerPoint(){
		assert testerProducerPoint("pp1", "fanspeed", 70) == 70
		assert testerProducerPoint("pp2", "fanspeed", null) == null
		assert testerProducerPoint("pp3", "fanspeed", -3000) == -3000
	}

	@Test
	void alsoTestStatuses(){
		def t = rigProducerPoint("pp4", "fanspeed", 70)
		assert testerProducerPoint(t) == 70
		//println env.getAllWorldProperties()
		assert getStatus(t) == 1
		assert testerProducerPointChangeValue(t, "fanspeed", null) == null
		assert getStatus(t) == 0
	}

	@Test
	void changeFieldName(){
		def t = rigProducerPoint("ppName", "fanspeed", 70)
		assert testerProducerPoint(t) == 70
		def p = env.getParents(t).first()
		env.setPropertyValue(p, "setpoint", 60)
		env.setPropertyValue(t, "fieldname", "setpoint")
		assert testerProducerPoint(t) == 60
		println env.getAllWorldProperties()
	}

	@Test(expected = PropertyNotFoundException.class)
	void nameDoesntExist(){
		println "WOOOOOOO1"
		def t = rigProducerPoint("ppChangeName", "fanspeed", 70)
		env.setPropertyValue(t, "fieldname", "setpoint")
		assert testerProducerPoint(t) == null
		assert getStatus(t) == 0
		println "WOOOOOOO"
	}

	@Test(expected = RuntimeException.class)
	void problemsTest(){
		env.makeTrouble(true)
		assert testerProducerPoint("pp0", "fanspeed", 70) == null
	}

	@Test
	void disabledPP(){
		def t = rigProducerPoint("ppd", "fanspeed", 70, 2)
		assert testerProducerPoint(t) == null
		assert getStatus(t) == 2
	}

	@Test
	void changeHostStatus(){
		TO<?> t = rigProducerPoint("ppd", "fanspeed", 70, 1)
		TO<?> p = env.getParents(t).toArray().first()
		assert testerProducerPoint(t) == 70
		assert getStatus(t) == 1
		env.setPropertyValue(p, "status", 2)
		assert testerProducerPoint(t) == null
		assert getStatus(t) == 2
		env.setPropertyValue(p, "status", 1)
		assert testerProducerPoint(t) == 70
		assert getStatus(t) == 1
	}

	@Test
	void noHostStatus(){
		def t = rigProducerPoint("ppName", "fanspeed", 70, null)
		assert testerProducerPoint(t) == 70
		assert getStatus(t) == 1
	}
	
	
	@Test
	void producerPower(){
		assert testerProducerPower(42) == 42
		assert testerProducerPower(null) == null
		assert testerProducerPower("gabe") == null
	}

}
