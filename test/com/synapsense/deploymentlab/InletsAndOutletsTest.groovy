package com.synapsense.deploymentlab

import org.junit.*
import static org.junit.Assert.*
import groovy.mock.interceptor.StubFor

import com.synapsense.rulesengine.core.environment.Property
import com.synapsense.rulesengine.core.environment.RuleAction
import com.synapsense.rulesengine.core.environment.RuleI
import com.synapsense.dto.TOFactory
import com.synapsense.deploymentlab.*
import com.synapsense.service.Environment
import com.synapsense.dto.TO
import org.apache.log4j.Logger
import GroovyRuleStubs.*

/**
 *
 * @author Gabriel Helman
 * @since
 * Date: 9/13/11
 * Time: 2:12 PM
 */
class InletsAndOutletsTest {

	static al = FakeAlertService.getInstance()
	static Environment env = FakeEnvironment.getInstance()

	@Before
	void resetState(){
		env.makeTrouble(false)
		env.autoCreateType( true )
		env.reset()
		al.reset()
	}

	Double testerOutlet(Double input, String name) {
		al.reset()
		//rig the host object
		def to = env.createObject("GRAPH_OUTLET")
		env.setPropertyValue(to, "name", name)
		//now, rig the source properties
		def r = env.createObject("SOURCE")
		env.setPropertyValue(r, "lastValue", input)
		def prop = new Property(r, to)
		def at = new Outlet()
		at.setInput(prop)
		def result = at.run(new RuleI("${to}outlet"), new Property("lastValue", to))
		env.setPropertyValue(to, "lastValue", result)
		assert at.getInput() == prop
		return result
	}

	def makeOutlet(String name){
		def to = env.createObject("GRAPH_OUTLET")
		env.setPropertyValue(to, "name", name)
		return to
	}

	Double testerOutlet(Double input, TO<?> to){
		def r = env.createObject("SOURCE")
		env.setPropertyValue(r, "lastValue", input)
		def prop = new Property(r, to)
		def at = new Outlet()
		at.setInput(prop)
		def result = at.run(new RuleI("${to}outlet"), new Property("lastValue", to))
		env.setPropertyValue(to, "lastValue", result)
		assert at.getInput() == prop
		return result
	}

	def makeInlet(Object outletName){
		def to = env.createObject("GRAPH_INLET")
		//env.setPropertyValue(to, "default", defaultValue)
		env.setPropertyValue(to, "outletName", outletName)
		env.setPropertyValue(to, "oldOutletName", null)
		env.setPropertyValue(to, "source", null)
		return to
	}

	Double testerInlet(Object defaultValue, Object outletName){
		al.reset()
		def to = makeInlet(defaultValue,outletName)
		def mo = new Inlet()
		return mo.run(new RuleI("inlet"), new Property("lastValue", to))
	}

	Double testerInlet(def to){
		al.reset()
		def mo = new Inlet()
		return mo.run(new RuleI("inlet"), new Property("lastValue", to))
	}


	void changeInletTarget(TO<?> to, String outletName){
		env.setPropertyValue(to, "outletName", outletName)
	}

	@Test
	void noOutletsTest(){
		def in1 = makeInlet("thereAreNoOutletsAtAll")
		assert testerInlet(in1) == null
		assert !al.hasAlert()
		assert env.getPropertyValue(in1,"source", TO.class) == null
	}

	@Test
	void inletAndOutletTest(){

		assert testerOutlet(5, "outlet1") == 5
		assert testerOutlet(300, "outlet3") == 300
		assert testerOutlet(0, "outlet2") == 0
		//change from beginning: nulls no longer coerce to zero
		assert testerOutlet(null, "outletNull") == null

		println env.getObjectsByType("GRAPH_OUTLET")
		println env.getAllWorld()
		println env.getAllWorldProperties()

		def in1 = makeInlet("outlet1")
		def in2 = makeInlet("outletQ")
		def in3 = makeInlet("outletMissing")

		assert testerInlet(in1) == 5
		assert !al.hasAlert()
		assert testerInlet(in2) == null
		assert al.checkAlert(al.noOut)
		assert testerInlet(in1) == 5
		assert !al.hasAlert()

		//previous bug: outlets handing 0 out were treated as being null (booo!)
		assert testerInlet(in3) == null
		assert testerOutlet(0, "outletMissing") == 0
		assert testerInlet(in3) == 0

		println env.getAllWorldProperties()
	}


	@Test
	void multiOutlets(){
		assert testerOutlet(20, "outletDual") == 20
		assert testerOutlet(5, "outletDual") == 5

		def inDual = makeInlet("outletDual")

		//in a more than one case, the inlet will use the LAST outlet it found and raise an alert
		assert testerInlet(inDual) == 5
		assert al.checkAlert(al.moreOut)
	}

	@Test
	void changeOutletName(){
		assert testerOutlet(4525, "outletFirst") == 4525
		assert testerOutlet(90, "outletSecond") == 90

		def inChange = makeInlet("none")

		assert testerInlet(inChange) == null
		changeInletTarget(inChange, "outletFirst")
		assert testerInlet(inChange) == 4525
		changeInletTarget(inChange, "outletSecond")
		assert testerInlet(inChange) == 90

		//change target before first run
		def inChange2 = makeInlet("none2")
		changeInletTarget(inChange2, "outletFirst")
		assert testerInlet(inChange2) == 4525

	}

	@Test
	void caseSensitive(){
		assert testerOutlet(42, "OutletA") == 42
		def inOtherCase = makeInlet("outleta")
		assert testerInlet(inOtherCase) == 42
		def correctCase = makeInlet("OutletA")
		assert testerInlet(correctCase) == 42
		assert testerInlet(inOtherCase) == 42
	}


	@Test
	void nullValue(){
		//outletNull may already exist, make a unique one
		assert testerOutlet(null, "outletNull2") == null

		def inNull = makeInlet("outletNull2")
		assert testerInlet(inNull) == null
		assert !al.hasAlert()
	}

	@Test
	void outletHasVanished(){
		//println "OUTLET HAS VANISHED"
		assert testerOutlet(42, "someOtherOutletWeShallIgnore") == 42
		assert testerOutlet(900, "outletFromSpace") == 900

		def inlet = makeInlet("outletFromSpace")
		//env.setPropertyValue(inlet, "source", TOFactory.getInstance().loadTO("notarealobject:900") )
		assert testerInlet(inlet) == 900

		//delete outlet
		String name = "outletFromSpace"
		def target
		env.getObjectsByType("GRAPH_OUTLET").each{ o ->
			if ( env.getPropertyValue(o, "name", String.class).equals(name) ){
				target = o
			}
		}
		//println "deleting outlet $target"
		env.deleteObject(target)

		assert testerInlet(inlet) == null
		//assert !al.hasAlert()
		assert al.checkAlert(al.noOut)
	}

	@Test
	void swapNames(){
		//println "SWAP NAMES"
		TO<?> inIT = makeInlet("Outlet IT")
		TO<?> inCool = makeInlet("Outlet Cooling")
		TO<?> inLoss = makeInlet("Outlet Losses")
		TO<?> inLight = makeInlet("Outlet Lighting")

		TO<?> outIT = makeOutlet("Outlet IT")
		TO<?> outCool = makeOutlet("Outlet Cooling")
		TO<?> outLoss = makeOutlet("Outlet Losses")
		TO<?> outLight = makeOutlet("Outlet Lighting")

		//println env.getObjectsByType("GRAPH_OUTLET")
		//println env.getAllWorldProperties()

		assert testerOutlet(10,outIT) == 10
		assert testerOutlet(11,outCool) == 11
		assert testerOutlet(12,outLoss) == 12
		assert testerOutlet(13,outLight) == 13

		assert testerInlet(inIT) == 10
		assert testerInlet(inCool) == 11
		assert testerInlet(inLoss) == 12
		assert testerInlet(inLight) == 13

		env.setPropertyValue(outIT, "name", "Outlet Cooling")
		env.setPropertyValue(outCool, "name", "Outlet IT")

		assert testerInlet(inIT) == 11
		assert testerInlet(inCool) == 10
		assert testerInlet(inLoss) == 12
		assert testerInlet(inLight) == 13
	}


	@Test(expected=RuntimeException.class)
	void andSomeExceptionsOutlet(){
		env.makeTrouble(true)
		Logger.setThrowing(false)
		assert testerOutlet(5, "outlet1") == null
		Logger.setThrowing(true)
		assert testerOutlet(5, "outlet1") == null
	}

	@Test(expected=RuntimeException.class)
	void andSomeExceptionsInlet(){
		env.makeTrouble(true)
		def to = makeInlet("outlet1")
		env.makeTrouble(true)
		Logger.setThrowing(false)
		assert testerInlet(to) == null
		Logger.setThrowing(true)
		assert testerInlet(to) == null
	}
}



