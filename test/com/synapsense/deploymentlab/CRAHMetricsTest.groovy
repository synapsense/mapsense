package com.synapsense.deploymentlab

import org.junit.*
import static org.junit.Assert.*
import groovy.mock.interceptor.StubFor

import com.synapsense.rulesengine.core.environment.Property
import com.synapsense.rulesengine.core.environment.RuleAction
import com.synapsense.rulesengine.core.environment.RuleI
import com.synapsense.dto.TOFactory
import com.synapsense.deploymentlab.*
import com.synapsense.service.Environment
import GroovyRuleStubs.*
import com.synapsense.dto.TO

/**
 * Unit tests for crah metrics rules
 * @author Gabriel Helman
 * @since Mars
 * Date: 9/13/11
 * Time: 9:42 AM
 */
class CRAHMetricsTest {

	//default tables:
	//def btutable = "40,;65,;70,;75,;80,;85,;90,;95,;100,;"
	//def cfmtable = "20,;30,;40,;50,;60,;70,;80,;90,;100,;"


	static al = FakeAlertService.getInstance()
	static Environment env = FakeEnvironment.getInstance()

	@Before
	void resetState(){
		env.makeTrouble(false)
		env.autoCreateType( true )
		env.reset()
		al.reset()
	}

	Double testerActualTons(Double rat, Double sat, Double cfm, Integer status = 1) {
		al.reset()
		//rig the fake environment
		//rig the host object
		def to = env.createObject("CRAH")
		env.setPropertyValue(to, "status", status)
		//now, rig the source properties
		def r = env.createObject("SOURCE")
		env.setPropertyValue(r, "lastValue", rat)
		def propR = new Property(r, to)
		def s = env.createObject("SOURCE")
		env.setPropertyValue(s, "lastValue", sat)
		def propS = new Property(s, to)
		def propC = new Property(cfm, to)
		def at = new CRAHActualTons()
		at.setRAT(propR)
		at.setSAT(propS)
		at.setCFM(propC)
		return at.run(new RuleI("${to}crahActualTons"), new Property("lastValue", to))
	}

	Double testerDesignTons(Double input, String table, Integer status = 1){
		al.reset()
		def to = env.createObject("CRAH")
		env.setPropertyValue(to, "status", status)
		//input power
		def pwr = env.createObject("SOURCE")
		env.setPropertyValue(pwr, "lastValue", input)
		def propIn = new Property(pwr, to)
		//table
		def propTable = new Property(table, to)
		def dt = new CRAHDesignTons()
		dt.setInputPower(propIn)
		dt.setTable(propTable)
		return dt.run(new RuleI("${to}crahDesignTons"), new Property("lastValue", to))
	}

	Double testerCFM(Double input, String table, Integer status = 1){
		al.reset()
		def to = env.createObject("CRAH")
		env.setPropertyValue(to, "status", status)
		//input power
		def pwr = env.createObject("SOURCE")
		env.setPropertyValue(pwr, "lastValue", input)
		def propIn = new Property(pwr, to)
		//table
		def propTable = new Property(table, to)
		def dt = new TableFit2()
		dt.setInputPower(propIn)
		dt.setTable(propTable)
		return dt.run(new RuleI("${to}crahDesignTons"), new Property("lastValue", to))
	}

	Double testerTableFit2(TO<?> to, TO<?> pwr, String table){
		al.reset()
		def propIn = new Property(pwr, to)
		def propTable = new Property(table, to)
		//table
		def dt = new TableFit2()
		dt.setInputPower(propIn)
		dt.setTable(propTable)
		return dt.run(new RuleI("${to}tableFit"), new Property("tableResult", to))
	}

	Double testerEff(Double actual, Double design, Integer status = 1){
		al.reset()
		//rig the fake environment
		//rig the host object
		def to = env.createObject("CRAH")
		env.setPropertyValue(to, "status", status)
		//now, rig the source properties
		def propA = new Property(actual, to)
		def propD = new Property(design, to)
		def at = new CRAHCoolingEfficiency()
		at.setTonsActual(propA)
		at.setTonsDesign(propD)
		return at.run(new RuleI("${to}crahActualTons"), new Property("lastValue", to))
	}


	@Test
	void crahMetricsTest() {
		//(cfm * 1.085 * (return temp - supply temp)) / 12000

		//also test deactivated

		//rat, sat, cfm
		assert testerActualTons(70, 60, 20000) == 18.083333333333332
		assert testerActualTons(80, 70, 55299.539170506912442396313364055) == 50

		assert testerActualTons(null, 60, 20000) == null
		assert testerActualTons(70, null, 20000) == null
		assert testerActualTons(70, 60, null) == null

	}

	@Test
	void effTest(){
		assert testerEff(100,10) == 1000
		assert al.getLastAlert() == null
		assert testerEff(null,10) == null
		assert al.getLastAlert() == null
		assert testerEff(50,null) == null
		assert al.getLastAlert() == null
		assert testerEff(1234,80) == 1542.5
		assert al.getLastAlert() == null
		assert testerEff(42,0) == null
		assert al.getLastAlert() != null
		assert al.checkAlert(al.DZ)
	}

	@Test
	void designTonsTest(){
		assert testerDesignTons(50,"10,10;100,100") * 12000 == 50
		assert testerDesignTons(null,"10,10;100,100") == null
		assert testerDesignTons(50,null) == null
		assert testerDesignTons(50,"") == null
		assert testerDesignTons(50,"10,120000;100,1200000") == 50
		assert testerDesignTons(50,"10,120000;30,;100,1200000") == 50
		assert testerDesignTons(50,"75,75;10,10;30,;100,100") * 12000 == 50
	}

	@Test
	void cfmTableTest(){
		assert testerCFM(50,"10,10;100,100") == 50
		assert testerCFM(null,"10,10;100,100") == null
		assert testerCFM(50,null) == null
		assert testerCFM(50,"") == null
		assert testerCFM(50,"10,10;30,;100,100") == 50
		assert testerCFM(50,"75,75;10,10;30,;100,100") == 50
		//test a whole table all the way up
		assert testerCFM(10,"30,5;50,10;70,17.5;90,30;100,50;") == 0
		assert testerCFM(20,"30,5;50,10;70,17.5;90,30;100,50;") == 2.5
		assert testerCFM(30,"30,5;50,10;70,17.5;90,30;100,50;") == 5
		assert testerCFM(40,"30,5;50,10;70,17.5;90,30;100,50;") == 7.5
		assert testerCFM(50,"30,5;50,10;70,17.5;90,30;100,50;") == 10
		assert testerCFM(60,"30,5;50,10;70,17.5;90,30;100,50;") == 13.75
		assert testerCFM(70,"30,5;50,10;70,17.5;90,30;100,50;") == 17.5
		assert testerCFM(80,"30,5;50,10;70,17.5;90,30;100,50;") == 23.75
		assert testerCFM(90,"30,5;50,10;70,17.5;90,30;100,50;") == 30
		assert testerCFM(100,"30,5;50,10;70,17.5;90,30;100,50;") == 50
	}

	@Test
	void allTogetherTest(){
		def returnt = 85
		def supplyt = 70
		def fanspeed = 90
		def btutable = "40,;65,;70,50000;75,;80,;85,60000;90,;95,;100,70000;"
		def cfmtable = "20,3000;30,;40,;50,;60,;70,;80,;90,3502.30414;"

		def cfm = testerCFM(fanspeed,cfmtable)
		def actual = testerActualTons(returnt,supplyt,cfm)
		def design = testerDesignTons(returnt,btutable)
		def eff = testerEff(actual,design)

		println cfm
		println actual
		println design
		println eff

		assert cfm == 3502.30414
		assert actual == 4.749999989875
		assert design == 5.0
		assert eff == 94.9999997975
	}

	@Test
	void disabledTest(){
		assert testerActualTons(70, 60, 20000, 1) == 18.083333333333332
		assert testerActualTons(70, 60, 20000, 2) == null

		assert testerDesignTons(50,"10,10;100,100") * 12000 == 50
		assert testerDesignTons(50,"10,10;100,100", 2)  == null

		assert testerCFM(50,"10,10;100,100") == 50
		assert testerCFM(50,"10,10;100,100", 2) == null

		assert testerEff(100,10) == 1000
		assert testerEff(100,10,2) == null
	}

	/*
	@Test
	void tableFit2DisableEnable(){
		assert testerCFM(50,"10,10;100,100") == 50
		TO<?> tableTO = env.getObjectsByType("CRAH").toArray().first()
		TO<?> sourceTO = env.getObjectsByType("SOURCE").toArray().first()
		env.setPropertyValue(sourceTO, "status", 2)
		assert testerCFM(50,"10,10;100,100") == 2
	}
	*/

}
