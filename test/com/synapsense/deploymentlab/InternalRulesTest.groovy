package com.synapsense.deploymentlab

import com.synapsense.rulesengine.core.environment.Property
import com.synapsense.rulesengine.core.environment.RuleAction
import com.synapsense.rulesengine.core.environment.RuleI
import GroovyRuleStubs.FakeEnvironment
import GroovyRuleStubs.FakeAlertService
import org.junit.Before
import org.junit.Test
import GroovyRuleStubs.*
import com.synapsense.deploymentlab.*

/**
 * Tests rules that back internal testing components.
 * @author Gabriel Helman
 * @since Jupiter
 * Date: 3/9/12
 * Time: 2:05 PM
 */
class InternalRulesTest {

	FakeEnvironment env = (FakeEnvironment) FakeEnvironment.getInstance()
	static al = FakeAlertService.getInstance()

	@Before
	void resetState() {
		env.makeTrouble(false)
		env.autoCreateType( true )
		env.reset()
		al.reset()
	}


	@Test
	void makingItNull(){
		def to = env.createObject("MAKEITNULL")
		def dt = new MakeItNull()
		assert dt.run(new RuleI("${to}nullio"), new Property("nullioResult", to)) == null
	}
	
	@Test
	void random(){
		def to = env.createObject("RAND")
		env.setPropertyValue(to, "max", 42)
		def dt = new com.synapsense.deploymentlab.Random()
		def val = dt.run(new RuleI("${to}rand"), new Property("randresult", to))
		assert val != null
		assert val <= 42
		assert val >= 0
	}

	@Test
	void wobulator(){
		def to = env.createObject("WOBULATOR")
		env.setPropertyValue(to, "baseline", 42)
		env.setPropertyValue(to, "jiggle", 13)
		env.setPropertyValue(to, "lastValue", null)
		def dt = new com.synapsense.deploymentlab.Wobulator()
		for(int i = 0; i <10; i++ ){
			def val = dt.run(new RuleI("${to}wob"), new Property("wobresult", to))
			assert val != null
			assert val <= (42+13)
			assert val >= (42-13)
			env.setPropertyValue(to,"lastValue", val)
		}
	}

	@Test
	void testSequence(){
		def to = env.createObject("TestSequence")
		env.setPropertyValue(to, "sequence", "2.0,4.0,6.0,8.0,10.0")
		env.setPropertyValue(to, "lastValue", null)
		env.setPropertyValue(to, "storage", null)
		env.setPropertyValue(to, "statusSequence", null)
		def dt = new com.synapsense.deploymentlab.TestSequence()
		for(int i = 1; i <=5; i++ ){
			def val = dt.run(new RuleI("${to}rand"), new Property("randresult", to))
			assert val == i*2
			assert env.getPropertyValue(to, "status", Integer.class) == 1
		}
	}


	@Test
	void testSequenceWithStatus(){
		def to = env.createObject("TestSequence")
		env.setPropertyValue(to, "sequence", "2.0,4.0,6.0,8.0,10.0")
		env.setPropertyValue(to, "lastValue", null)
		env.setPropertyValue(to, "storage", null)
		env.setPropertyValue(to, "statusSequence", "2,2,2")
		def dt = new com.synapsense.deploymentlab.TestSequence()
		for(int i = 1; i <=10; i++ ){
			def val = dt.run(new RuleI("${to}rand"), new Property("randresult", to))


			if(i < 6){
				assert val == i*2
			} else {
				assert val == (i-5)*2
			}

			if(i<=3 || ( i>5 && ((i-5) <= 3))){
				assert env.getPropertyValue(to, "status", Integer.class) == 2
			} else {
				assert env.getPropertyValue(to, "status", Integer.class) == 1
			}

		}
	}

	@Test
	void testSequenceWithNulls(){
		def to = env.createObject("TestSequence")
		env.setPropertyValue(to, "sequence", "2.0,4.0,null,8.0,10.0")
		env.setPropertyValue(to, "lastValue", null)
		env.setPropertyValue(to, "storage", null)
		env.setPropertyValue(to, "statusSequence", "null,null,1,null,null")
		def dt = new com.synapsense.deploymentlab.TestSequence()
		for(int i = 1; i <=5; i++ ){
			def val = dt.run(new RuleI("${to}rand"), new Property("randresult", to))
			if(i!= 3){
				assert val == i*2
				assert env.getPropertyValue(to, "status", Integer.class) == null
			} else {
				assert val == null
				assert env.getPropertyValue(to, "status", Integer.class) == 1
			}
		}
	}

	@Test
	void tempusFidgit(){
		def to = env.createObject("TEMPUS")
		env.setPropertyValue(to, "highval", 42)
		env.setPropertyValue(to, "lowval", 20)
		env.setPropertyValue(to, "jiggle", 13)
		env.setPropertyValue(to, "start", 9)
		env.setPropertyValue(to, "finish", 17)
		env.setPropertyValue(to, "lastValue", null)

		def dt = new com.synapsense.deploymentlab.Tempus()
		for(int i = 0; i <10; i++ ){
			def val = dt.run(new RuleI("${to}wob"), new Property("wobresult", to))
			assert val != null
			assert (val <= (42+13)) || (val <= (20+13))
			assert (val >= (42-13)) || (val >= (20-13))
			env.setPropertyValue(to,"lastValue", val)
		}
	}

}
