package com.synapsense.deploymentlab

import org.junit.*
import static org.junit.Assert.*
import groovy.mock.interceptor.StubFor

import com.synapsense.rulesengine.core.environment.Property
import com.synapsense.rulesengine.core.environment.RuleAction
import com.synapsense.rulesengine.core.environment.RuleI
import com.synapsense.dto.TOFactory
import com.synapsense.deploymentlab.*
import com.synapsense.service.Environment
import GroovyRuleStubs.*
import com.synapsense.dto.TO
import com.synapsense.service.AlertService

/**
 *
 * @author Gabriel Helman
 * @since
 * Date: 9/13/11
 * Time: 4:00 PM
 */
class OddballRulesTest {

	static AlertService al = FakeAlertService.getInstance()
	static FakeEnvironment env = (FakeEnvironment)FakeEnvironment.getInstance()

	@Before
	void resetState(){
		env.makeTrouble(false)
		env.autoCreateType( true )
		env.reset()
		al.reset()
	}

	Double testerTableFit(Double input, String table, Integer status = 1) {
		al.reset()
		def to = env.createObject("tableFit")
		//input power
		def pwr = env.createObject("SOURCE")
		env.setPropertyValue(pwr, "lastValue", input)
		if(status){
			env.setPropertyValue(pwr, "status", status)
		}
		def propIn = new Property(pwr, to)
		//table
		//def propTable = new Property(table, to)
		def dt = new TableFit()
		dt.setInputPower(propIn)
		env.setPropertyValue(to,"table", table)
		return dt.run(new RuleI("${to}tableFit"), new Property("tableResult", to))
	}
	
	Double testerTableFit(TO<?> to, TO<?> pwr){
		al.reset()
		def propIn = new Property(pwr, to)
		//table
		def dt = new TableFit()
		dt.setInputPower(propIn)
		return dt.run(new RuleI("${to}tableFit"), new Property("tableResult", to))
	}


	Double testerThreePhase(Double inputA, Double inputB, Double inputC, Object volts, Object pf, Integer statusA = 1, Integer statusB = 1, Integer statusC = 1) {
		al.reset()
		def to = env.createObject("THREEPHASE")
/*		
		def tosA = []
		def tosB = []
		def tosC = []
		//inputA.each{ val ->
		for(int i = 0; i<inputA.size(); i++){
			def val = inputA[i]
			def t = env.createObject("SOURCEA")
			env.setPropertyValue(t, "lastValue", val)
			if(statusA.size() > i){
				env.setPropertyValue(t,"status", statusA[i])
			} //else {
//				env.setPropertyValue(t,"status", 1)
//			}
			tosA += t
		}
		//inputB.each{ val ->
		for(int i = 0; i<inputB.size(); i++){
			def val = inputB[i]
			def t = env.createObject("SOURCEB")
			env.setPropertyValue(t, "lastValue", val)
			if(statusB.size() > i){
				env.setPropertyValue(t,"status", statusB[i])
			} //else {
				//env.setPropertyValue(t,"status", 1)
			//}
			tosB += t
		}
		//inputC.each{ val ->
		for(int i = 0; i<inputC.size(); i++){
			def val = inputC[i]
			def t = env.createObject("SOURCEC")
			env.setPropertyValue(t, "lastValue", val)
			if(statusC.size() > i){
				env.setPropertyValue(t,"status", statusC[i])
			} //else {
				//env.setPropertyValue(t,"status", 1)
			//}
			tosC += t
		}
*/

		def ta = env.createObject("SOURCEA")
		env.setPropertyValue(ta, "lastValue", inputA)
		env.setPropertyValue(ta,"status", statusA)
		
		def tb = env.createObject("SOURCEB")
		env.setPropertyValue(tb, "lastValue", inputB)
		env.setPropertyValue(tb,"status", statusB)
		
		def tc = env.createObject("SOURCEC")
		env.setPropertyValue(tc, "lastValue", inputC)
		env.setPropertyValue(tc,"status", statusC)
		
		//set up some properties
		/*
		def propA = new Property(tosA, to)
		def propB = new Property(tosB, to)
		def propC = new Property(tosC, to)
		*/
		def propA = new Property(ta, to)
		def propB = new Property(tb, to)
		def propC = new Property(tc, to)


		def dt = new ThreePhaseTokW()
		dt.setPhaseA(propA)
		dt.setPhaseB(propB)
		dt.setPhaseC(propC)
		env.setPropertyValue(to,"volts", volts)
		env.setPropertyValue(to,"pf", pf)
		return dt.run(new RuleI("${to}ThreePhaseTokW"), new Property("ThreePhaseTokW", to))
	}

	Double testerThreePhase(TO<?> to, Object volts, Object pf){
		def propA = new Property(env.getObjectsByType("SOURCEA")[0], to)
		def propB = new Property(env.getObjectsByType("SOURCEB")[0], to)
		def propC = new Property(env.getObjectsByType("SOURCEC")[0], to)
		def dt = new ThreePhaseTokW()
		dt.setPhaseA(propA)
		dt.setPhaseB(propB)
		dt.setPhaseC(propC)
		env.setPropertyValue(to,"volts", volts)
		env.setPropertyValue(to,"pf", pf)
		return dt.run(new RuleI("${to}ThreePhaseTokW"), new Property("ThreePhaseTokW", to))		
	}
	

	Double testerMetric(Double input, Integer status = 1) {
		al.reset()
		//rig the host object
		def to = env.createObject("METRIC")
		//now, rig the source properties
		def r = env.createObject("SOURCE")
		env.setPropertyValue(r, "lastValue", input)
		if(status){
			env.setPropertyValue(r, "status", status)
		}
		def prop = new Property(r, to)
		def at = new Metric()
		at.setInput(prop)
		return at.run(new RuleI("${to}inspector"), new Property("lastValue", to))
	}

	Double testerMetric(TO<?> to, TO<?> r){
		def prop = new Property(r, to)
		def at = new Metric()
		at.setInput(prop)
		return at.run(new RuleI("${to}inspector"), new Property("lastValue", to))		
	}

	Double testerLessThan(Double input, Object minAllowed, Object defaultValue, Integer status = 1) {
		al.reset()
		//rig the host object
		def to = env.createObject("LESSTHAN")
		//now, rig the source properties
		def r = env.createObject("SOURCE")
		env.setPropertyValue(r, "lastValue", input)
		if(status){
			env.setPropertyValue(r, "status", status)
		}
		def prop = new Property(r, to)
		def at = new LessThanFilter()
		at.setInput(prop)
		env.setPropertyValue(to,"defaultValue", defaultValue)
		env.setPropertyValue(to,"minAllowed", minAllowed)
		return at.run(new RuleI("${to}inspector"), new Property("lastValue", to))
	}

	TO<?> testerPDU(Map<String,Map<String,String>> inputs ) {
		al.reset()
		//rig the host object
		String[] panels = ["panel1", "panel2", "panel3", "panel4"];
		String[] properties = ["currentA", "currentB", "currentC", "voltageAB", "voltageBC", "voltageCA", "voltageAN", "voltageBN", "voltageCN", "powerA", "powerB", "powerC", "powerFactorA", "powerFactorB", "powerFactorC"];
		def to = env.createObject("PDU")
		for(String propertyName: properties) {
			env.setPropertyValue(to, propertyName, null);
		}
		for(String propertyName: panels) {
			env.setPropertyValue(to, propertyName, null);
		}

		//now, rig the source properties
		inputs.keySet().each { panel ->
			def panelTO = env.createObject("PANEL")
			for(String propertyName: properties) {
				env.setPropertyValue(panelTO, propertyName, null);
			}
			env.setPropertyValue(to, panel, panelTO)
			inputs[panel].each{ k, v ->
				//env.setPropertyValue(panelTO, k, v)
				def modbusTO = env.createObject("MODBUS")
				env.setPropertyValue(modbusTO, "lastValue", v)
				env.setPropertyValue(panelTO, k, modbusTO)
			}
		}
		def at = new PdiPduAll()
		at.run(new RuleI("${to}inspector"), new Property("lastValue", to))
		return to
	}

	@Test
	void transformerTableTest() {
		assert testerTableFit(50, "10,10;100,100") == 50
		assert testerTableFit(null, "10,10;100,100") == null
		assert testerTableFit(50, null) == null
		assert testerTableFit(50, "") == null
		assert testerTableFit(50, "10,10;30,;100,100") == 50
		assert testerTableFit(50, "75,75;10,10;30,;100,100") == 50
		//println env.getAllWorldProperties()

		assert testerTableFit(50, "75,75") == 75
		assert testerTableFit(5, "75,75;10,10;30,;100,100") == 5
		assert testerTableFit(110, "75,75;10,10;30,;100,100") == 110
	}

	@Test
	void transformerDisabled(){
		assert testerTableFit(50, "10,10;100,100", 1) == 50
		//assert testerTableFit(50, "10,10;100,100", 2) == null
		//get table
		TO<?> tableTO = env.getObjectsByType("tableFit").toArray().first()
		//get source
		TO<?> sourceTO = env.getObjectsByType("SOURCE").toArray().first()
		env.setPropertyValue(sourceTO, "status", 2)
		assert testerTableFit(tableTO,sourceTO) == null
		assert env.getPropertyValue(tableTO, "status", Integer.class) == 2
		env.setPropertyValue(sourceTO, "status", 1)
		assert testerTableFit(tableTO,sourceTO) == 50
		assert env.getPropertyValue(tableTO, "status", Integer.class) == 1
	}

	@Test
	void transformerTableNoStatus() {
		assert testerTableFit(50, "10,10;100,100",null) == 50
		TO<?> tableTO = env.getObjectsByType("tableFit").toArray().first()
		env.setPropertyValue(tableTO, "status", 1)
	}

	@Test
	void threePhaseTest(){
		assert testerThreePhase(5,4,5, 110, 0.9 ) == 0.800184
		assert testerThreePhase(50,10,45, 210, 0.85 ) == 10.82067

		assert testerThreePhase(null,4,5, 110, 0.9 ) == null
		assert al.checkAlert(al.NI)
		assert testerThreePhase(5,null,5, 110, 0.9 ) == null
		assert al.checkAlert(al.NI)
		assert testerThreePhase(5,4,null, 110, 0.9 ) == null
		assert al.checkAlert(al.NI)

		assert testerThreePhase(0,4,5, 110, 0.9 ) == 0.514404
		assert al.checkAlert(null)
		assert testerThreePhase(5,0,5, 110, 0.9 ) == 0.57156
		assert al.checkAlert(null)
		assert testerThreePhase(5,4,0, 110, 0.9 ) == 0.514404
		assert al.checkAlert(null)

		assert testerThreePhase(0,4,5, 110, 0.9 ) == 0.514404
		assert al.checkAlert(null)
		assert testerThreePhase(5,0,5, 110, 0.9 ) == 0.57156
		assert al.checkAlert(null)
		assert testerThreePhase(5,4,0, 110, 0.9 ) == 0.514404
		assert al.checkAlert(null)
	}

	@Test
	void threePhaseDisabled(){
		assert testerThreePhase(5,5,5, 110, 0.9, 1,1,1 ) == 0.85734
		assert testerThreePhase(5,5,5, 110, 0.9, 2,1,1 ) == null
		assert testerThreePhase(5,5,5, 110, 0.9, 1,2,1 ) == null
		assert testerThreePhase(5,5,5, 110, 0.9, 1,1,2 ) == null
	}

	@Test
	void threePhaseNoStatus(){
		assert testerThreePhase(5,5,5, 110, 0.9, null,null,null) == 0.85734
		TO<?> threephaseTO = env.getObjectsByType("THREEPHASE").toArray().first()
		assert env.getPropertyValue(threephaseTO,"status", Integer.class) == 1
	}

	@Test
	void threePhaseDisabledReEnable(){
		assert testerThreePhase(5,5,5, 110, 0.9, 2,2,2 ) == null
		TO<?> threephaseTO = env.getObjectsByType("THREEPHASE").toArray().first()
		assert env.getPropertyValue(threephaseTO,"status", Integer.class) == 2
		assert testerThreePhase(threephaseTO,110,0.9) == null
		
		//swap all those input statuses back
		env.getObjectsByType("SOURCEA").each{
			env.setPropertyValue(it, "status", 1)
		}
		env.getObjectsByType("SOURCEB").each{
			env.setPropertyValue(it, "status", 1)
		}
		env.getObjectsByType("SOURCEC").each{
			env.setPropertyValue(it, "status", 1)
		}
		assert testerThreePhase(threephaseTO,110,0.9) == 0.85734
		assert env.getPropertyValue(threephaseTO,"status", Integer.class) == 1
	}

	@Test
	void testMetric() {
		assert testerMetric(0) == 0
		assert testerMetric(10) == 10
		assert testerMetric(13) == 13
		assert testerMetric(42) == 42
		assert testerMetric(12345.67890) == 12345.67890
		assert testerMetric(null) == null
		assert al.getLastAlert() == null
	}


	@Test
	void metricDisabled(){
		assert testerMetric(42,1) == 42
		assert testerMetric(42,2) == null
	}


	@Test
	void metricDisabledEnabled(){
		assert testerMetric(42,1) == 42
		TO<?> metricTO = env.getObjectsByType("METRIC").toArray().first()
		TO<?> sourceTO = env.getObjectsByType("SOURCE").toArray().first()
		assert env.getPropertyValue(metricTO, "status", Integer.class) == 1
		env.setPropertyValue(sourceTO,"status",2)
		assert testerMetric(metricTO,sourceTO) == null
		assert env.getPropertyValue(metricTO, "status", Integer.class) == 2

		env.setPropertyValue(sourceTO,"status",1)
		assert testerMetric(metricTO,sourceTO) == 42
		assert env.getPropertyValue(metricTO, "status", Integer.class) == 1
	}

	
	@Test
	void testLessThan(){
		assert testerLessThan(20,5,10) == 20
		assert testerLessThan(1,5,10) == 10
		assert testerLessThan(0,5,0) == 0
		assert testerLessThan(null,5,10) == 10
		assert testerLessThan(-5,5,10) == 10
		assert testerLessThan(null,-5,10) == 10
		assert testerLessThan(20,5,10,2) == 10
	}

	@Test
	void lessThanNoStatus(){
		assert testerLessThan(42,10,13,null) == 42
		assert testerLessThan(42,50,13,null) == 13
	}

	@Test
	void metricNoStatus(){
		assert testerMetric(42,null) == 42
		TO<?> metricTO = env.getObjectsByType("METRIC").toArray().first()
		assert env.getPropertyValue(metricTO, "status", Integer.class) == 1
	}

	@Test
	void pduTest(){
		//String[] panels = ["panel1", "panel2", "panel3", "panel4"];
		//String[] properties = ["currentA", "currentB", "currentC", "voltageAB", "voltageBC", "voltageCA", "voltageAN", "voltageBN", "voltageCN", "powerA", "powerB", "powerC", "powerFactorA", "powerFactorB", "powerFactorC"];
		def inputs = [:]
		inputs["panel1"] =	["currentA" : 0, "currentB" : 0, "currentC" : 0, "voltageAB" : 0, "voltageBC" : 0, "voltageCA" : 0, "voltageAN" : 0, "voltageBN" : 0, "voltageCN" : 0, "powerA" : 0, "powerB" : 0, "powerC" : 0, "powerFactorA" : 0, "powerFactorB" : 0, "powerFactorC" : 0];
		def pdu =  testerPDU(inputs)
		assert env.getPropertyValue(pdu, "currentA", Double.class) == 0
	}


	@Test
	void pduTestAverages(){
		//String[] panels = ["panel1", "panel2", "panel3", "panel4"];
		//String[] properties = ["currentA", "currentB", "currentC", "voltageAB", "voltageBC", "voltageCA", "voltageAN", "voltageBN", "voltageCN", "powerA", "powerB", "powerC", "powerFactorA", "powerFactorB", "powerFactorC"];
		def inputs = [:]
		inputs["panel1"] =	["currentA" : 0, "currentB" : 0, "currentC" : 0, "voltageAB" : 10, "voltageBC" : 1, "voltageCA" : 0, "voltageAN" : 0, "voltageBN" : 0, "voltageCN" : 0, "powerA" : 0, "powerB" : 0, "powerC" : 0, "powerFactorA" : 0, "powerFactorB" : 0, "powerFactorC" : 0];
		inputs["panel2"] =	["currentA" : 0, "currentB" : 0, "currentC" : 0, "voltageAB" : 20, "voltageBC" : 2, "voltageCA" : 0, "voltageAN" : 0, "voltageBN" : 0, "voltageCN" : 0, "powerA" : 0, "powerB" : 0, "powerC" : 0, "powerFactorA" : 0, "powerFactorB" : 0, "powerFactorC" : 0];
		inputs["panel3"] =	["currentA" : 0, "currentB" : 0, "currentC" : 0, "voltageAB" : 30, "voltageBC" : 3, "voltageCA" : 0, "voltageAN" : 0, "voltageBN" : 0, "voltageCN" : 0, "powerA" : 0, "powerB" : 0, "powerC" : 0, "powerFactorA" : 0, "powerFactorB" : 0, "powerFactorC" : 0];
		inputs["panel4"] =	["currentA" : 0, "currentB" : 0, "currentC" : 0, "voltageAB" : 40, "voltageBC" : null, "voltageCA" : 0, "voltageAN" : 0, "voltageBN" : 0, "voltageCN" : 0, "powerA" : 0, "powerB" : 0, "powerC" : 0, "powerFactorA" : 0, "powerFactorB" : 0, "powerFactorC" : 0];
		def pdu =  testerPDU(inputs)
		assert env.getPropertyValue(pdu, "voltageAB", Double.class) == 25
		assert env.getPropertyValue(pdu, "voltageBC", Double.class) == 2
	}

	@Test
	void pduTestAllEmpty(){
		def inputs = [:]
		def pdu =  testerPDU(inputs)
		assert env.getPropertyValue(pdu, "currentA", Double.class) == 0
	}

	@Test
	void pduTestAveragesPartlyNull(){
		//String[] panels = ["panel1", "panel2", "panel3", "panel4"];
		//String[] properties = ["currentA", "currentB", "currentC", "voltageAB", "voltageBC", "voltageCA", "voltageAN", "voltageBN", "voltageCN", "powerA", "powerB", "powerC", "powerFactorA", "powerFactorB", "powerFactorC"];
		def inputs = [:]
		inputs["panel1"] =	[ "voltageAB" : 10, "voltageBC" : 1];
		inputs["panel2"] =	[ "voltageAB" : 20, "voltageBC" : 2];
		inputs["panel3"] =	[ "voltageAB" : 30, "voltageBC" : null];
		def pdu =  testerPDU(inputs)
		assert env.getPropertyValue(pdu, "voltageAB", Double.class) == 20
		assert env.getPropertyValue(pdu, "voltageBC", Double.class) == 1.5
		assert env.getPropertyValue(pdu, "voltageCA", Double.class) == 0
	}

	@Test
	void pduTestTotals(){
		def inputs = [:]
		inputs["panel1"] =	[ "currentA" : 10, "powerC" : 1];
		inputs["panel2"] =	[ "currentA" : 20, "powerC" : 2];
		inputs["panel3"] =	[ "currentA" : 30, "powerC" : null];
		def pdu =  testerPDU(inputs)
		assert env.getPropertyValue(pdu, "voltageAB", Double.class) == 0
		assert env.getPropertyValue(pdu, "powerC", Double.class) == 3
		assert env.getPropertyValue(pdu, "currentA", Double.class) == 60
	}

}
