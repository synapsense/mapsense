package com.synapsense.deploymentlab

import org.junit.*
import static org.junit.Assert.*
import groovy.mock.interceptor.StubFor

import com.synapsense.rulesengine.core.environment.Property
import com.synapsense.rulesengine.core.environment.RuleAction
import com.synapsense.rulesengine.core.environment.RuleI
import com.synapsense.dto.TOFactory
import com.synapsense.deploymentlab.*
import com.synapsense.service.Environment
import GroovyRuleStubs.*
import com.synapsense.service.AlertService

/**
 * Testing PUE & Inspector groovy rules
 * @author Gabriel Helman
 * @since Mars
 * Date: 9/13/11
 * Time: 2:15 PM
 */
class PUEAndInspectorTest {

	static FakeAlertService al = FakeAlertService.getInstance()
	static FakeEnvironment env = (FakeEnvironment)FakeEnvironment.getInstance()

	@Before
	void resetState(){
		env.reset()
		env.makeTrouble(false)
		env.autoCreateType( true )
		al.reset()
	}

	Double testerInspector(Double input) {
		al.reset()
		//rig the host object
		def to = env.createObject("INSPECTOR")
		//now, rig the source properties
		def r = env.createObject("SOURCE")
		env.setPropertyValue(r, "lastValue", input)
		def prop = new Property(r, to)
		def at = new Inspector()
		at.setInput(prop)
		return at.run(new RuleI("${to}inspector"), new Property("lastValue", to))
	}


	Double testerLegs(def rule, String setter, List inputs, List statuses = [], String puetype = "full") {
		al.reset()
		//rig the host object
		def to = env.createObject("PUE")
		env.setPropertyValue(to,"type", puetype)
		def tosA = []
		//inputs.each { val ->
		for(int i = 0; i < inputs.size(); i++){
			def t = env.createObject("SOURCE")
			env.setPropertyValue(t, "lastValue", inputs[i])
			if(statuses.size() > i){
				env.setPropertyValue(t,"status", statuses[i])
			} //else {
				//env.setPropertyValue(t,"status", 1)
			//}

			tosA += t
		}
		def propInputs = new Property(tosA, to)
		GroovyObject groovyObject = (GroovyObject) rule;
		Object[] inputsArray = new Object[1]
		inputsArray[0] = propInputs
		groovyObject.invokeMethod(setter, inputsArray)
		Object[] argz = new Object[2];
		argz[0] = new RuleI("${to}pueLeg")
		argz[1] = new Property("lastValue", to)
		return groovyObject.invokeMethod("run", argz);
	}


	Double testerPUE(Object itPower, Object lightingPower, Object powerLoss, Object coolingPower, String puetype = "full"){
		al.reset()
		//rig the host object
		def to = env.createObject("PUE")
		env.setPropertyValue(to,"type", puetype)
		//now, rig the source properties
		def propA = new Property(itPower, to)

		def propB = new Property(lightingPower, to)

		def propC = new Property(powerLoss, to)

		def propD = new Property(coolingPower, to)

		def mo = new Pue()
		mo.setItPowerInputs(propA)
		mo.setLightingPowerInputs(propB)
		mo.setPowerLossInputs(propC)
		mo.setCoolingPowerInputs(propD)
		return mo.run(new RuleI("pueMain"), new Property("pue", to))
	}

	Double testerDCIE(Object itPower, Object lightingPower, Object powerLoss, Object coolingPower){
		al.reset()
		//rig the host object
		def to = env.createObject("PUE")
		//now, rig the source properties
		def propA = new Property(itPower, to)

		def propB = new Property(lightingPower, to)

		def propC = new Property(powerLoss, to)

		def propD = new Property(coolingPower, to)

		def mo = new Dcie()
		mo.setItPowerInputs(propA)
		mo.setLightingPowerInputs(propB)
		mo.setPowerLossInputs(propC)
		mo.setCoolingPowerInputs(propD)
		return mo.run(new RuleI("DcieMain"), new Property("Dcie", to))
	}


	Double testerDCE(Object itPower, Object coolingPower){
		al.reset()
		//rig the host object
		def to = env.createObject("PUE")
		//now, rig the source properties
		def propA = new Property(itPower, to)
		def propB = new Property(coolingPower, to)

		def mo = new Dce()
		mo.setItPowerInputs(propA)
		mo.setCoolingPowerInputs(propB)
		return mo.run(new RuleI("DceMain"), new Property("Dce", to))
	}

	Double testerIP(Object lightingPower, Object powerLoss, Object coolingPower){
		al.reset()
		//rig the host object
		def to = env.createObject("PUE")
		//now, rig the source properties
		def propA = new Property(lightingPower, to)
		def propB = new Property(powerLoss, to)
		def propC = new Property(coolingPower, to)
		def mo = new InfrastructurePower()
		mo.setLightingPowerInputs(propA)
		mo.setPowerLossInputs(propB)
		mo.setCoolingPowerInputs(propC)
		return mo.run(new RuleI("pueInf"), new Property("inf", to))
	}


	@Test
	void testInspector() {
		assert testerInspector(0) == 0
		assert testerInspector(10) == 10
		assert testerInspector(13) == 13
		assert testerInspector(42) == 42
		assert testerInspector(12345.67890) == 12345.67890
		assert testerInspector(null) == null
		assert al.getLastAlert() == null
	}

	/**
	 * Testing the four input legs of PUE
	 */
	@Test
	void testPUELegs(){
		//normal behavior
		assert testerLegs(new ItPower(), "setItPowerInputs", [1,2,3]) == 6
		assert testerLegs(new LightingPower(), "setLightingPowerInputs", [1,2,3]) == 6
		assert testerLegs(new PowerLoss(), "setPowerLossInputs", [1,2,3]) == 6
		assert testerLegs(new CoolingPower(), "setCoolingPowerInputs", [1,2,3]) == 6

		//null inputs
		assert testerLegs(new ItPower(), "setItPowerInputs", [1,2,null]) == null
		assert al.getLastAlert() != null
		assert al.checkAlert(al.NI)
		assert testerLegs(new LightingPower(), "setLightingPowerInputs", [1,2,null]) == null
		assert al.getLastAlert() != null
		assert al.checkAlert(al.NI)
		assert testerLegs(new PowerLoss(), "setPowerLossInputs", [1,2,null]) == null
		assert al.getLastAlert() != null
		assert al.checkAlert(al.NI)
		assert testerLegs(new CoolingPower(), "setCoolingPowerInputs", [1,2,null]) == null
		assert al.getLastAlert() != null
		assert al.checkAlert(al.NI)
	}


	@Test
	void pueLegsDisabledITPower(){
		assert testerLegs(new ItPower(), "setItPowerInputs", [1,2,3], [1,1,1]) == 6
		assert testerLegs(new ItPower(), "setItPowerInputs", [1,2,3], [1,2,1]) == 4
		assert !al.hasAlert()
		assert testerLegs(new ItPower(), "setItPowerInputs", [1,null,3], [1,2,1]) == 4
		assert !al.hasAlert()
		assert testerLegs(new ItPower(), "setItPowerInputs", [1,null,3], [2,2,1]) == 3
		assert !al.hasAlert()
		assert testerLegs(new ItPower(), "setItPowerInputs", [1,2,3], [2,2,2]) == null
		assert al.checkAlert(al.EI)
		assert testerLegs(new ItPower(), "setItPowerInputs", [null,null,null], [2,2,2]) == null
		assert al.checkAlert(al.EI)
	}

	@Test
	void pueLegsDisabledLighting(){
		assert testerLegs(new LightingPower(), "setLightingPowerInputs", [1,2,3], [1,1,1]) == 6
		assert testerLegs(new LightingPower(), "setLightingPowerInputs", [1,2,3], [1,2,1]) == 4
		assert !al.hasAlert()
		assert testerLegs(new LightingPower(), "setLightingPowerInputs", [1,null,3], [1,2,1]) == 4
		assert !al.hasAlert()
		assert testerLegs(new LightingPower(), "setLightingPowerInputs", [1,null,3], [2,2,1]) == 3
		assert !al.hasAlert()
		assert testerLegs(new LightingPower(), "setLightingPowerInputs", [1,2,3], [2,2,2]) == null
		assert al.checkAlert(al.EI)
		assert testerLegs(new LightingPower(), "setLightingPowerInputs", [null,null,null], [2,2,2]) == null
		assert al.checkAlert(al.EI)
	}

	@Test
	void pueLegsDisabledPowerLoss(){
		assert testerLegs(new PowerLoss(), "setPowerLossInputs", [1,2,3], [1,1,1]) == 6
		assert testerLegs(new PowerLoss(), "setPowerLossInputs", [1,2,3], [1,2,1]) == 4
		assert !al.hasAlert()
		assert testerLegs(new PowerLoss(), "setPowerLossInputs", [1,null,3], [1,2,1]) == 4
		assert !al.hasAlert()
		assert testerLegs(new PowerLoss(), "setPowerLossInputs", [1,null,3], [2,2,1]) == 3
		assert !al.hasAlert()
		assert testerLegs(new PowerLoss(), "setPowerLossInputs", [1,2,3], [2,2,2]) == null
		assert al.checkAlert(al.EI)
		assert testerLegs(new PowerLoss(), "setPowerLossInputs", [null,null,null], [2,2,2]) == null
		assert al.checkAlert(al.EI)
	}

	@Test
	void pueLegsDisabledCooling(){
		assert testerLegs(new CoolingPower(), "setCoolingPowerInputs", [1,2,3], [1,1,1]) == 6
		assert testerLegs(new CoolingPower(), "setCoolingPowerInputs", [1,2,3], [1,2,1]) == 4
		assert !al.hasAlert()
		assert testerLegs(new CoolingPower(), "setCoolingPowerInputs", [1,null,3], [1,2,1]) == 4
		assert !al.hasAlert()
		assert testerLegs(new CoolingPower(), "setCoolingPowerInputs", [1,null,3], [2,2,1]) == 3
		assert !al.hasAlert()
		assert testerLegs(new CoolingPower(), "setCoolingPowerInputs", [1,2,3], [2,2,2]) == null
		assert al.checkAlert(al.EI)
		assert testerLegs(new CoolingPower(), "setCoolingPowerInputs", [null,null,null], [2,2,2]) == null
		assert al.checkAlert(al.EI)
	}

	@Test(expected=RuntimeException)
	void pueLegsExceptions(){
		env.makeTrouble(true)
		assert testerLegs(new ItPower(), "setItPowerInputs", [1,2,3]) == null
		assert !al.hasAlert()
		assert testerLegs(new LightingPower(), "setLightingPowerInputs", [1,2,3]) == null
		assert !al.hasAlert()
		assert testerLegs(new PowerLoss(), "setPowerLossInputs", [1,2,3]) == null
		assert !al.hasAlert()
		assert testerLegs(new CoolingPower(), "setCoolingPowerInputs", [1,2,3]) == null
		assert !al.hasAlert()
	}

	/**
	 * Testing the rest of the PUE rules
	 */
	@Test
	void testPUEDependencies(){
		//pue == total / itpower
		assert testerPUE(1,2,3,4) == 10
		assert testerPUE(4,3,2,1) == 2.5
		assert testerPUE(null,2,3,4) == null
		assert testerPUE(1,null,3,4) == null
		assert testerPUE(1,2,null,4) == null
		assert testerPUE(1,2,3,null) == null
		assert testerPUE(10,0,0,0) == 1.0
		assert testerPUE(0,2,3,4) == null
		assert al.checkAlert(al.UMR)
		assert testerPUE(0,0,0,0) == null
		assert al.checkAlert(al.UMR)

		//dce == it / cooling
		assert testerDCE(8,4) == 2
		assert testerDCE(4,8) == 0.5
		assert testerDCE(null,2) == null
		assert testerDCE(1,null) == null
		assert testerDCE(null,null) == null
		assert testerDCE(10,0) == null
		assert testerDCE(0,2) == 0.0
		assert testerDCE(0,0) == null
		assert al.getLastAlert() == null

		//dcie == itpower / totalpower
		assert testerDCIE(1,2,3,4) == 0.1
		assert testerDCIE(4,3,2,1) == 0.4
		assert testerDCIE(null,2,3,4) == null
		assert testerDCIE(1,null,3,4) == null
		assert testerDCIE(1,2,null,4) == null
		assert testerDCIE(1,2,3,null) == null
		assert testerDCIE(10,0,0,0) == 1.0
		assert testerDCIE(0,2,3,4) == 0.0
		assert testerDCIE(0,0,0,0) == null
		assert al.getLastAlert() == null


		assert testerIP(1,2,3) == 6
		//now testing the partially-null pue-lite variant:
		assert testerIP(null,2,3) == 5
		assert testerIP(1,null,3) == 4
		assert testerIP(1,2,null) == 3
		assert testerIP(null,null,null) == null
		assert testerIP(0,2,3) == 5
		assert testerIP(1,0,3) == 4
		assert testerIP(1,2,0) == 3
		assert testerIP(0,0,0) == 0
	}

	/**
	 * Checks the two-leg variant, where only it-power and lighting are used, with lighting subbing in as "infrastructure"
	 */
	@Test
	void PUELite(){
		//just itpower & lighting and null on the other two are acceptable
		assert testerPUE(9.5, 20.2, null, null, "lite") == 3.1263157894736842105263157894737

		//must have it power to work
		assert testerPUE(null, 5.0, 20.2, null, "lite") == null
		//must have lighting to work
		assert testerPUE(9.5, null, 20.2, null, "lite") == null

		//must have both cooling&loss to work, can't just have one
		assert testerPUE(9.5, 5.0, 20.2, null, "lite") == null
		assert testerPUE(9.5, 5.0, null, 20.2, "lite") == null
	}

	/**
	 * Cooling power & power loss shouldn't  compute on PUE Lite
	 */
	@Test
	void noCoolingOrLossOnPueLite(){
		assert testerLegs(new ItPower(), "setItPowerInputs", [1,2,3], [], "lite") == 6
		assert testerLegs(new LightingPower(), "setLightingPowerInputs", [1,2,3], [], "lite") == 6
		assert testerLegs(new PowerLoss(), "setPowerLossInputs", [1,2,3], [], "lite") == null
		assert testerLegs(new CoolingPower(), "setCoolingPowerInputs", [1,2,3], [], "lite") == null
	}
}
