package com.synapsense.deploymentlab

import com.synapsense.deploymentlab.*

import GroovyRuleStubs.FakeAlertService
import GroovyRuleStubs.FakeEnvironment
import com.synapsense.rulesengine.core.environment.Property
import com.synapsense.rulesengine.core.environment.RuleAction
import com.synapsense.rulesengine.core.environment.RuleI
import org.junit.Before
import org.junit.Test

/**
 * Test status-based rules
 * @author Gabriel Helman
 * @since Jupiter
 * Date: 3/16/12
 * Time: 5:00 PM
 */
class StatusRuleTest {

	FakeEnvironment env = (FakeEnvironment) FakeEnvironment.getInstance()
	static al = FakeAlertService.getInstance()

	@Before
	void resetState() {
		env.makeTrouble(false)
		env.autoCreateType( true )
		env.reset()
		al.reset()
	}

	Double testerProducerStatus(Object input, String whitelist) {
		al.reset()
		def to = env.createObject("PRODUCERStatus")
		env.setPropertyValue(to, "lastValue", null)
		env.setPropertyValue(to, "whitelist", whitelist)

		def sourceTO = env.createObject("SOURCE")
		env.setPropertyValue(sourceTO, "lastValue", input)

		def prop = new Property(sourceTO, to)
		def at = new ProducerStatus()
		at.setInput(prop)
		return at.run(new RuleI("${to}pstatus"), new Property("lastValue", to))
	}

	@Test
	void producerStatus() {
		assert testerProducerStatus("A", "A,B,C") == 1.0
		assert testerProducerStatus("A", "B,C") == 0.0
		assert testerProducerStatus("A", "") == 0.0
		assert testerProducerStatus("A", null) == 0.0
		assert testerProducerStatus(42, "42,43,44") == 1.0

		assert testerProducerStatus(42, null) == 0.0
		assert testerProducerStatus(null, null) == 0.0
		assert testerProducerStatus("gabe", "gabe , is , awesome") == 1.0
		assert testerProducerStatus("gabe", "") == 0.0
	}
	
	
	Double testerRandomStatus(){
		al.reset()
		def to = env.createObject("RANDOM")
		def at = new RandomStatus()
		return at.run(new RuleI("${to}randStatus"), new Property("lastValue", to))
	}
	
	@Test
	void randomStatuses(){
		for(int i = 1; i < 50; i++){
			def status = testerRandomStatus()
			assert (status == 1.0 || status == 0.0)
		}
	}

	Double testerStatusInverter(Double input) {
		al.reset()
		def to = env.createObject("STATUSiNVERT")
		env.setPropertyValue(to, "lastValue", null)
		def sourceTO = env.createObject("SOURCE")
		env.setPropertyValue(sourceTO, "lastValue", input)
		def prop = new Property(sourceTO, to)
		def at = new StatusInverter()
		at.setInput(prop)
		return at.run(new RuleI("${to}STATiNVERT"), new Property("lastValue", to))
	}

	@Test
	void invertStatus(){
		assert testerStatusInverter(0.0) == 1.0
		assert testerStatusInverter(1.0) == 0.0
		assert testerStatusInverter(null) == 1.0
	}

	Double testerSwitch(Double input, Double status, Double defaultValue, Double value2 = null) {
		al.reset()
		def to = env.createObject("SWITCH")
		env.setPropertyValue(to, "lastValue", null)
		env.setPropertyValue(to, "default", defaultValue)

		def valueTO = env.createObject("SOURCE")
		env.setPropertyValue(valueTO, "lastValue", input)
		def propI = new Property(valueTO, to)

		def statusTO = env.createObject("SOURCE")
		env.setPropertyValue(statusTO, "lastValue", status)
		def propS = new Property(statusTO, to)

		def propI2
		if(value2 != null){
			def value2TO = env.createObject("SOURCE")
			env.setPropertyValue(value2TO, "lastValue", value2)
			propI2 = new Property(value2TO, to)
		}

		def at = new SwitchOperation()
		at.setValue(propI)
		at.setStatus(propS)
		if(value2!= null){
			at.setValue2(propI2)
		}
		return at.run(new RuleI("${to}STATiNVERT"), new Property("lastValue", to))
	}


	@Test
	void switches(){
		assert testerSwitch(5,1.0,10) == 5
		assert testerSwitch(5,0,10) == 10
		assert testerSwitch(5,0,null) == null

		assert testerSwitch(5,1.0,null,10) == 5
		assert testerSwitch(5,0,null,10) == 10
	}

}
