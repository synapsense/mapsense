package com.synapsense.deploymentlab

import GroovyRuleStubs.FakeAlertService
import GroovyRuleStubs.FakeEnvironment
import com.synapsense.dto.TO
import com.synapsense.rulesengine.core.environment.Property
import com.synapsense.rulesengine.core.environment.RuleI
import com.synapsense.service.Environment
import org.junit.Before
import org.junit.Test

/**
 *
 * @author Gabriel Helman
 * @since
 * Date: 9/9/11
 * Time: 12:34 PM
 */
class GroovyRulesTest {
	static FakeEnvironment env = (FakeEnvironment) FakeEnvironment.getInstance()
	static FakeAlertService al = FakeAlertService.getInstance()

	@Before
	void resetState() {
		env.makeTrouble(false)
		env.autoCreateType( true )
		env.reset()
		al.reset()
	}

	@Test
	void testAddition() {
/*
		String pluginName = "MathOperation.groovy"
		ClassLoader parent = this.class.getClassLoader();
		GroovyClassLoader loader = new GroovyClassLoader(parent);
		Class groovyClass = loader.parseClass(new File("build/cfg/rules/MathOperation.groovy"));
		GroovyObject groovyObject = (GroovyObject) groovyClass.newInstance();
		Object[] inputs = new Object[1]
		inputs[0] = new Property()
		groovyObject.invokeMethod("setInputA", inputs)
		groovyObject.invokeMethod("setInputB", inputs)
		Object[] argz = new Object[2];
		argz[0] = new RuleI("mathOp1")
		argz[1] = new Property()
		def result = groovyObject.invokeMethod("run", argz );
		println "Result = $result"
		groovyObject.invokeMethod("test", new Object[0] );
  */

		//turns out, we can do it this way too!
		def mo = new MathOperation()

		//rig the host object
		def to = env.createObject("EDDIE")
		env.setPropertyValue(to, "opA", "sum")
		env.setPropertyValue(to, "opB", "sum")
		env.setPropertyValue(to, "opM", "add")
		//now, rig the source properties
		def to1 = env.createObject("SOURCE")
		def to2 = env.createObject("SOURCE")
		def to3 = env.createObject("SOURCE")
		def toNull = env.createObject("SOURCE")
		env.setPropertyValue(to1, "lastValue", 42)
		env.setPropertyValue(to2, "lastValue", 42)
		env.setPropertyValue(to3, "lastValue", 42)
		env.setPropertyValue(toNull, "lastValue", null)

		//set up some properties
		def propA = new Property([to1], to)
		def propB = new Property([to2, to3], to)
		def propEmpty = new Property([], to)
		def propNull = new Property(null, to)
		def propPartNull = new Property([to1, to2, toNull], to)


		mo.setInputA(propA)
		mo.setInputB(propB)
		def result = mo.run(new RuleI("mathOp1"), new Property("lastValue", to))
		println "Result = $result"
		assert result == 126.0

		//check vs null
		//one null
		mo.setInputA(propEmpty)
		mo.setInputB(propB)
		result = mo.run(new RuleI("mathOp1"), new Property("lastValue", to))
		println "Result = $result"
		assert result == null //was 84.0

		//both  null
		mo.setInputA(propEmpty)
		mo.setInputB(propEmpty)
		result = mo.run(new RuleI("mathOp1"), new Property("lastValue", to))
		println "Result = $result"
		assert result == null

		mo.setInputA(propNull)
		mo.setInputB(propB)
		result = mo.run(new RuleI("mathOp1"), new Property("lastValue", to))
		println "Result = $result"
		//assert result == null //84.0
		assert result == null

		//both  null
		mo.setInputA(propNull)
		mo.setInputB(propNull)
		result = mo.run(new RuleI("mathOp1"), new Property("lastValue", to))
		println "Result = $result"
		assert result == null

		mo.setInputA(propPartNull)
		mo.setInputB(propB)
		result = mo.run(new RuleI("mathOp1"), new Property("lastValue", to))
		println "Result = $result"
		assert result == null
	}


	Double tester(String OpA, String OpB, String OpM, List inputA, List inputB, List statusA = [], List statusB = []) {
		FakeAlertService.getInstance().reset()
		//rig the fake environment
		Environment env = FakeEnvironment.getInstance()
		//rig the host object
		def to = env.createObject("EDDIE")
		env.setPropertyValue(to, "opA", OpA)
		env.setPropertyValue(to, "opB", OpB)
		env.setPropertyValue(to, "opM", OpM)
		env.setPropertyValue(to, "status", 1)
		//now, rig the source properties
		def tosA = []
		def tosB = []
		for (int i = 0; i < inputA.size(); i++) {
			def val = inputA[i]
			def t = env.createObject("SOURCEA")
			env.setPropertyValue(t, "lastValue", val)
			if (statusA.size() > i) {
				env.setPropertyValue(t, "status", statusA[i])
			}
			tosA += t
		}
		for (int i = 0; i < inputB.size(); i++) {
			def val = inputB[i]
			def t = env.createObject("SOURCEB")
			if (statusB.size() > i) {
				env.setPropertyValue(t, "status", statusB[i])
			}
			env.setPropertyValue(t, "lastValue", val)
			tosB += t
		}
		//set up some properties
		def propA = new Property(tosA, to)
		def propB = new Property(tosB, to)
		def mo = new MathOperation()
		mo.setInputA(propA)
		mo.setInputB(propB)
		return mo.run(new RuleI("mathOp1"), new Property("lastValue", to))
	}

	Double tester(TO<?> to, List tosA, List tosB) {
		FakeAlertService.getInstance().reset()
		def propA = new Property(tosA, to)
		def propB = new Property(tosB, to)
		def mo = new MathOperation()
		mo.setInputA(propA)
		mo.setInputB(propB)
		return mo.run(new RuleI("mathOp1"), new Property("lastValue", to))
	}


	@Test
	public void MathOperationBuiltIn() {
		def mo = new MathOperation()
		def li = [1, 2, 3, 4, 5]
		//println "List Operations:"
		assert mo.listSum(li) == 15 //15
		assert mo.listMin(li) == 1 //1
		assert mo.listMax(li) == 5 //5
		assert mo.listAvg(li) == 3 //3

		//println "Main Operations:"
		//15 op 3 :
		assert mo.mainMath(li, li, mo.listSum, mo.listAvg, mo.opAdd) == 18 //18
		assert mo.mainMath(li, li, mo.listSum, mo.listAvg, mo.opSub) == 12
		assert mo.mainMath(li, li, mo.listSum, mo.listAvg, mo.opMul) == 45
		assert mo.mainMath(li, li, mo.listSum, mo.listAvg, mo.opDiv) == 5.0
		assert mo.mainMath(li, li, mo.listSum, mo.listAvg, mo.opMod) == 0.0
		assert mo.mainMath(li, li, mo.listSum, mo.listAvg, mo.opPow) == 3375.0
		assert mo.mainMath(li, li, mo.listSum, mo.listAvg, mo.opMin) == 3.0
		assert mo.mainMath(li, li, mo.listSum, mo.listAvg, mo.opMax) == 15.0
		assert mo.mainMath(li, li, mo.listSum, mo.listAvg, mo.opAvg) == 9
		//15 op 6
		assert mo.mainMath(li, [1, 2, 3], mo.listSum, mo.listSum, mo.opMod) == 3.0
		assert mo.mainMath(li, [1, 2, 3], mo.listSum, mo.listSum, mo.opAvg) == 10.5
		assert mo.mainMath(li, [1, 2, 3], mo.listSum, mo.listSum, mo.opDiv) == 2.5 // (? should be 2.666->)
		assert mo.mainMath(li, [1, 2, 3], mo.getListOp("sum"), mo.getListOp("+"), mo.getMainOp("%")) == 3.0
		//15 op []
		assert mo.mainMath(li, [], mo.listSum, mo.listSum, mo.opNoOp) == 15
		assert mo.mainMath([], [], mo.listSum, mo.listSum, mo.opAdd) == null //was 0
		assert mo.mainMath(li, li, mo.listSize, mo.listFirst, mo.opA) == 5
		assert mo.mainMath(li, li, mo.listSize, mo.listFirst, mo.opB) == 1
		assert mo.mainMath(li, li, mo.listSize, mo.listLast, mo.opB) == 5

		//println "Take the average of six inputs:"
		def six = [1, 2, 3, 4, 5, 6]
		assert mo.mainMath(six, [], mo.listAvg, mo.listFirst, mo.opA) == 3.5
		six = [2, 4, 6, 8, 10, 12]
		assert mo.mainMath([], six, mo.listAvg, mo.listAvg, mo.opNoOp) == 7

		//println mainMath( [6,7,8], [1], listSolo, listSum, opSqrt )

	}


	@Test
	void testsFromSpecAdd() {
		assert tester("Sum", "Sum", "Add", [5, 6], [7, 8]) == 26
		assert al.checkAlert(null)
		assert tester("Sum", "Sum", "Add", [5, 6], [7, null]) == null
		assert al.checkAlert(al.NI)
		assert tester("Sum", "Sum", "Add", [5, 6], [null]) == null
		assert al.checkAlert(al.NI)
		assert tester("Sum", "Sum", "Add", [null], [7, 8]) == null
		assert al.checkAlert(al.NI)
		assert tester("Sum", "Sum", "Add", [null], [null]) == null
		assert al.checkAlert(al.NI)
		assert tester("Sum", "Sum", "Add", [5, 6], []) == null
		assert al.checkAlert(al.EI)
		assert tester("Sum", "Sum", "Add", [], [7, 8]) == null
		assert al.checkAlert(al.EI)
		assert tester("Sum", "Sum", "Add", [], []) == null
		assert al.getLastAlert() != null
		assert al.checkAlert(al.EI)
		assert tester("Sum", "Sum", "Add", [5, 6], [0]) == 11
		assert al.getLastAlert() == null
		assert al.checkAlert(null)
	}

	@Test
	void testsFromSpecSub() {
		assert tester("Sum", "Sum", "Sub", [5, 6], [7, 8]) == -4
		assert !al.hasAlert()
		assert tester("Sum", "Sum", "Sub", [5, 6], [7, null]) == null
		assert al.checkAlert(al.NI)
		assert tester("Sum", "Sum", "Sub", [5, 6], [null]) == null
		assert al.checkAlert(al.NI)
		assert tester("Sum", "Sum", "Sub", [null], [7, 8]) == null
		assert al.checkAlert(al.NI)
		assert tester("Sum", "Sum", "Sub", [null], [null]) == null
		assert al.checkAlert(al.NI)
		assert tester("Sum", "Sum", "Sub", [5, 6], [0]) == 11
		assert !al.hasAlert()
	}

	@Test
	void testsFromSpecDiv() {
		assert tester("Sum", "Sum", "Div", [5, 6], [7, 8]) == 0.7333333333333333
		assert !al.checkAlert(al.NI)
		assert al.checkAlert(null)
		assert tester("Sum", "Sum", "Div", [5, 6], [7, null]) == null
		assert tester("Sum", "Sum", "Div", [5, 6], [null]) == null
		assert tester("Sum", "Sum", "Div", [null], [7, 8]) == null
		assert tester("Sum", "Sum", "Div", [null], [null]) == null
		assert al.checkAlert(al.NI)
		assert tester("Sum", "Sum", "Div", [5, 6], [0]) == null
		assert al.checkAlert(al.UMR)
	}

	@Test
	void testsFromSpecAvg() {
		assert tester("Avg", "NoOp", "NoOp", [5, 6], [null]) == 5.5
		assert al.checkAlert(null)
		assert tester("Avg", "NoOp", "NoOp", [5, 6], []) == 5.5
		assert al.checkAlert(null)
		assert tester("NoOp", "Avg", "NoOp", [5, 6], [null]) == null
		assert al.checkAlert(al.NI)
		assert tester("NoOp", "Avg", "NoOp", [5, 6], []) == null
		assert al.checkAlert(al.EI)
		assert tester("NoOp", "Avg", "NoOp", [null], [5, 6]) == 5.5
		assert !al.checkAlert(al.NI)
		assert al.checkAlert(null)
		assert tester("NoOp", "Avg", "NoOp", [], [5, 6]) == 5.5
		assert al.checkAlert(null)
		assert tester("Avg", "NoOp", "NoOp", [null], [null]) == null
		assert al.checkAlert(al.NI)
		assert tester("Avg", "NoOp", "NoOp", [5, null, 6], [null]) == null
		assert !al.checkAlert(null)
	}

	@Test
	void testsFromSpecHoles() {
		//coverage holes:
		//if both lists exist, main noop returns result of op A
		assert tester("Avg", "Avg", "NoOp", [3, 4], [5, 6]) == 3.5
		assert tester("Sum", "Sum", "or", [10], [20]) == 30
		assert tester("Sum", "Sum", "xor", [10], [20]) == 30
		assert tester("Sum", "Sum", "and", [10], [20]) == 0
		assert tester("Sum", "Sum", "xor", [6], [6]) == 0
		assert tester("Sum", "Sum", "or", [6], [6]) == 6
		assert tester("Sum", "Sum", "and", [6], [6]) == 6
		assert tester("Sum", "Sum", "xor", [6], [15]) == 9
		assert tester("Sum", "Sum", "or", [6], [15]) == 15
		assert tester("Sum", "Sum", "and", [6], [15]) == 6

		assert tester("Sum", "Sum", "shiftl", [13], [2]) == 52
		assert tester("Sum", "Sum", "shiftr", [13], [2]) == 3
		assert tester("Sum", "Sum", "shiftl", [42], [3]) == 336
		assert tester("Sum", "Sum", "shiftr", [42], [3]) == 5

		assert tester("Sum", "Sum", "bitextract", [10], [2]) == 0
		assert tester("Sum", "Sum", "bitextract", [13], [2]) == 1
		assert tester("Sum", "Sum", "bitextract", [42], [0]) == 0
		assert tester("Sum", "Sum", "bitextract", [42], [3]) == 1
		assert tester("Sum", "Sum", "bitextract", [0xffff], [15]) == 1

		assert tester("sum", "sum", "lt", [1], [2])
		assert !tester("sum", "sum", "lt", [2], [1])

		assert !tester("sum", "sum", "gt", [1], [2])
		assert tester("sum", "sum", "gt", [2], [1])

		assert !tester("sum", "sum", "eq", [1], [2])
		assert tester("sum", "sum", "eq", [2], [2])

		assert tester("sum", "sum", "neq", [1], [2])
		assert !tester("sum", "sum", "neq", [2], [2])


		assert tester("Sum", "Sum", "mul", [6], [15]) == 90
		assert tester("Sum", "Sum", "min", [6], [15]) == 6
		assert tester("Sum", "Sum", "max", [6], [15]) == 15
		assert tester("Sum", "Sum", "avg", [6], [15]) == 10.5

		assert tester("Sum", "Sum", "noop", [6], [15]) == 6
		assert tester("Sum", "Sum", "nil", [6], [15]) == 6
		assert tester("Sum", "Sum", "garbage", [6], [15]) == 6

		assert tester("min", "max", "mul", [6, 15], [15, 6]) == 90
		assert tester("max", "min", "mul", [6, 15], [15, 6]) == 90
		assert tester("solo", "solo", "mul", [6, 15], [15, 6]) == 90

		assert tester("solo", "nil", "mul", [6, 15], [15, 6]) == 90
		assert tester("default", "junk", "mul", [6, 15], [15, 6]) == 90

	}

	@Test
	void testsFromSpecDisabled() {
		assert tester("Sum", "Sum", "Add", [5, 6], [1], [2, 1]) == 7
		assert !al.hasAlert()
		assert tester("Sum", "Sum", "Add", [5, 6, 5, 6], [1, 2, null, null], [2, 2, 1, 1], [1, 2, 2, 2]) == 12
		assert !al.hasAlert()
		assert tester("Sum", "Sum", "Add", [5], [1], [2], [1]) == null
		assert al.checkAlert(al.EI)
		assert tester("Sum", "Sum", "Add", [5, 6], [null], [2, 2], [2]) == null
		assert al.checkAlert(al.EI)

		assert tester("Sum", "Sum", "Sub", [5, 6], [0], [1, 2]) == 5
		assert !al.hasAlert()
		assert tester("Sum", "Sum", "Sub", [0, 6], [5], [1, 2]) == -5
		assert !al.hasAlert()
		assert tester("Sum", "Sum", "Sub", [5, 6], [7, 8], [2, 2], [2, 2]) == null
		assert al.checkAlert(al.EI)


		assert tester("Avg", "NoOp", "NoOp", [5, 6], [null], [1, 2]) == 5.0
		assert al.checkAlert(null)
		assert tester("Avg", "NoOp", "NoOp", [5, 6, 20], [null], [1, 1, 2]) == 5.5
		assert al.checkAlert(null)
		assert tester("Avg", "NoOp", "NoOp", [5, 6], [null], [1, 1], [2]) == 5.5
		assert al.checkAlert(null)
		assert tester("Avg", "NoOp", "NoOp", [5, 6], [null], [2, 2]) == null
		assert al.checkAlert(al.EI)
	}

	/**
	 * Error codes -3000, and -2000 should all be treated the same as null.
	 * Error code -5000 should be treated the same as disabled.
	 * Other giant negative numbers should be treated like normal.
	 */
	@Test
	void errorCodeHandling() {
		println "errorCodeHandling()"
		//assert tester("Avg", "Avg", "Add", [5, 6], [7, 8, -5000]) == null
		assert tester("Avg", "Avg", "Add", [5, 6], [7, 8, -5000]) == 13
		assert al.checkAlert(null)
		//assert tester("Avg", "Avg", "Add", [5, 6], [7, 8, -5000.00]) == null
		assert tester("Avg", "Avg", "Add", [5, 6], [7, 8, -5000.00]) == 13
		assert al.checkAlert(null)
		assert tester("Avg", "Avg", "Add", [5, 6], [7, 8, -5001]) == -1656.5
		assert al.checkAlert(null)

		//assert tester("Avg", "Avg", "Add", [5, 6, -5000], [7, 8]) == null
		assert tester("Avg", "Avg", "Add", [5, 6, -5000], [7, 8]) == 13
		assert al.checkAlert(null)
		//assert tester("Avg", "Avg", "Add", [5, 6, -5000.00], [7, 8]) == null
		assert tester("Avg", "Avg", "Add", [5, 6, -5000.00], [7, 8]) == 13
		assert al.checkAlert(null)
		assert tester("Avg", "Avg", "Add", [5, 6, -5001], [7, 8]) == -1655.8333333333333
		assert al.checkAlert(null)

		assert tester("Avg", "Avg", "Add", [5, 6], [7, 8, -3000]) == null
		assert !al.checkAlert(null)
		assert tester("Avg", "Avg", "Add", [5, 6], [7, 8, -2000]) == null
		assert !al.checkAlert(null)

		assert tester("Avg", "noop", "noop", [5, 6, -3000], []) == null
		assert !al.checkAlert(null)
		assert tester("Avg", "noop", "noop", [5, 6, -2000], []) == null
		assert !al.checkAlert(null)
		assert tester("Avg", "noop", "noop", [5, 6, -2500], []) == -829.66666666666666666666666666667
		assert al.checkAlert(null)
	}

	@Test
	void disabled5000inputB() {
		assert tester("Sum", "Sum", "Add", [5, 6], [7, 8]) == 26
		TO<?> mathTO = env.getObjectsByType("EDDIE").toArray().first()
		List<TO<?>> sourceATOs = env.getObjectsByType("SOURCEA").toList()
		List<TO<?>> sourceBTOs = env.getObjectsByType("SOURCEB").toList()
		assert tester(mathTO, sourceATOs, sourceBTOs) == 26
		assert env.getPropertyValue(mathTO, "status", Integer.class) == 1

		for (def to: sourceBTOs) {
			env.setPropertyValue(to, "lastValue", -5000.00)
		}
		assert tester(mathTO, sourceATOs, sourceBTOs) == null
		assert env.getPropertyValue(mathTO, "status", Integer.class) == 2
		assert al.checkAlert(al.EI)

		for (def to: sourceBTOs) {
			env.setPropertyValue(to, "lastValue", 3.0)
		}

		assert tester(mathTO, sourceATOs, sourceBTOs) == 17
		assert env.getPropertyValue(mathTO, "status", Integer.class) == 1
		assert !al.hasAlert()
	}


	@Test
	void disabled5000inputA() {
		assert tester("Sum", "Sum", "Add", [5, 6], [7, 8]) == 26
		TO<?> mathTO = env.getObjectsByType("EDDIE").toArray().first()
		List<TO<?>> sourceATOs = env.getObjectsByType("SOURCEA").toList()
		List<TO<?>> sourceBTOs = env.getObjectsByType("SOURCEB").toList()
		assert tester(mathTO, sourceATOs, sourceBTOs) == 26
		assert env.getPropertyValue(mathTO, "status", Integer.class) == 1

		for (def to: sourceATOs) {
			env.setPropertyValue(to, "lastValue", -5000.00)
		}
		assert tester(mathTO, sourceATOs, sourceBTOs) == null
		assert env.getPropertyValue(mathTO, "status", Integer.class) == 2
		assert al.checkAlert(al.EI)

		for (def to: sourceATOs) {
			env.setPropertyValue(to, "lastValue", 3.0)
		}

		assert tester(mathTO, sourceATOs, sourceBTOs) == 21
		assert env.getPropertyValue(mathTO, "status", Integer.class) == 1
		assert !al.hasAlert()
	}


	@Test
	void synonyms() {
		assert tester("Sum", "Sum", "Add", [5, 6], [7, 8]) == tester("Sum", "Sum", "+", [5, 6], [7, 8])
		assert tester("Sum", "Sum", "Sub", [5, 6], [7, 8]) == tester("Sum", "Sum", "-", [5, 6], [7, 8])
		assert tester("Sum", "Sum", "Mul", [5, 6], [7, 8]) == tester("Sum", "Sum", "*", [5, 6], [7, 8])
		assert tester("Sum", "Sum", "div", [5, 6], [7, 8]) == tester("Sum", "Sum", "/", [5, 6], [7, 8])
		assert tester("Sum", "Sum", "mod", [5, 6], [7, 8]) == tester("Sum", "Sum", "%", [5, 6], [7, 8])
		assert tester("Sum", "Sum", "pow", [5, 6], [7, 8]) == tester("Sum", "Sum", "^", [5, 6], [7, 8])
		assert tester("Sum", "Sum", "pow", [5, 6], [7, 8]) == tester("Sum", "Sum", "**", [5, 6], [7, 8])
	}


	@Test
	void mathEnabledDisabled() {
		println "mathEnabledDisabled START"
		assert tester("Sum", "Sum", "Add", [5, 6], [7, 8]) == 26
		TO<?> mathTO = env.getObjectsByType("EDDIE").toArray().first()
		List<TO<?>> sourceATOs = env.getObjectsByType("SOURCEA").toList()
		List<TO<?>> sourceBTOs = env.getObjectsByType("SOURCEB").toList()
		assert tester(mathTO, sourceATOs, sourceBTOs) == 26

		assert env.getPropertyValue(mathTO, "status", Integer.class) == 1
		for (def to: sourceBTOs) {
			env.setPropertyValue(to, "status", 2)
		}
		assert tester(mathTO, sourceATOs, sourceBTOs) == null
		assert env.getPropertyValue(mathTO, "status", Integer.class) == 2

		for (def to: sourceBTOs) {
			env.setPropertyValue(to, "status", 1)
		}
		assert tester(mathTO, sourceATOs, sourceBTOs) == 26
		assert env.getPropertyValue(mathTO, "status", Integer.class) == 1

		println "mathEnabledDisabled END"
	}


	Double testerSingle(String Op, Object input, Integer status = 1) {
		FakeAlertService.getInstance().reset()
		//rig the fake environment
		Environment env = FakeEnvironment.getInstance()
		//rig the host object
		def to = env.createObject("EDDIE")
		env.setPropertyValue(to, "op", Op)
		//now, rig the source properties

		def t = env.createObject("SOURCE")
		if (status != null) {
			env.setPropertyValue(t, "status", status)
		}
		env.setPropertyValue(t, "lastValue", input)
		def prop = new Property(t, to)

		def mo = new MathSingle()
		mo.setInput(prop)
		return mo.run(new RuleI("mathSingleT"), new Property("lastValue", to))
	}

	Double testerSingle(TO<?> to, TO<?> t) {
		def prop = new Property(t, to)
		def mo = new MathSingle()
		mo.setInput(prop)
		return mo.run(new RuleI("mathSingleT"), new Property("lastValue", to))
	}


	@Test
	void mathSingleBuiltIn() {
		def ms = new MathSingle()
		//ms.test()

		//println "Solo Ops test:"
		def val = 13.13
		assert ms.soloSqrt(val) == 3.623534186398688
		assert ms.soloSin(val) == 0.5342577081986536
		assert ms.soloCos(val) == 0.8453216554840662
		assert ms.soloTan(val) == 0.632017060881654
		assert ms.soloRound(val) == 13
		assert ms.soloFloor(val) == 13.0
		assert ms.soloCeil(val) == 14.0
		assert ms.soloNoOp(val) == 13.13
		assert ms.mainMath(val, ms.soloSqrt) == 3.623534186398688

	}


	@Test
	void mathSingle() {

		def ms = new MathSingle()
		//rig the fake environment
		Environment env = FakeEnvironment.getInstance()

		//rig the host object
		def to = env.createObject("EDDIE")
		env.setPropertyValue(to, "op", "sqrt")
		env.setPropertyValue(to, "status", 1)

		//now, rig the source properties
		def to1 = env.createObject("SOURCE")
		def to2 = env.createObject("SOURCE")
		def toNeg = env.createObject("SOURCE")
		def toNull = env.createObject("SOURCE")

		env.setPropertyValue(to1, "lastValue", 42)
		env.setPropertyValue(to2, "lastValue", 42)
		env.setPropertyValue(toNeg, "lastValue", -1)
		env.setPropertyValue(toNull, "lastValue", null)

		env.setPropertyValue(to1, "status", 1)
		env.setPropertyValue(to2, "status", 1)
		env.setPropertyValue(toNeg, "status", 1)
		env.setPropertyValue(toNull, "status", 1)

		//single can only accept single inputs
		def propValue = new Property(to1, to)
		def propNull = new Property(toNull, to)
		def propNeg = new Property(toNeg, to)


		ms.setInput(propValue)
		def result = ms.run(new RuleI("mathOpS"), new Property("lastValue", to))
		//println "Result = $result"
		assert result == Math.sqrt(42)
		assert testerSingle("sqrt", 42) == Math.sqrt(42)


		ms.setInput(propNull)
		result = ms.run(new RuleI("mathOpS"), new Property("lastValue", to))
		//println "Result = $result"
		assert result == null

		ms.setInput(propNeg)
		result = ms.run(new RuleI("mathOpS"), new Property("lastValue", to))
		//println "Result = $result"
		assert result == null

		assert testerSingle("loge", 42) == Math.log(42)
		assert testerSingle("log10", 42) == Math.log10(42)

		assert testerSingle("loge", null) == null
		assert al.checkAlert(al.NI)
		assert testerSingle("log10", null) == null
		assert al.checkAlert(al.NI)
		assert testerSingle("sqrt", null) == null
		assert al.checkAlert(al.NI)

		assert testerSingle("abs", -5000) == null
		assert al.checkAlert(null)
		assert testerSingle("abs", -2000) == null
		assert al.checkAlert(al.NI)
		assert testerSingle("abs", -3000) == null
		assert al.checkAlert(al.NI)
		assert testerSingle("abs", -1000) == 1000
		assert al.checkAlert(null)
		assert testerSingle("abs", -2000.1) == 2000.1
		assert al.checkAlert(null)

		assert testerSingle("not", 13.0) == -14
		assert al.checkAlert(null)

	}


	@Test(expected = RuntimeException.class)
	void andSomeExceptionsMainMath() {
		env.makeTrouble(true)
		tester("Sum", "Sum", "Add", [5, 6], [7, 8])
	}

	@Test(expected = RuntimeException.class)
	void andSomeExceptionsSingleMath() {
		env.makeTrouble(true)
		testerSingle("sqrt", 42)
	}

	@Test
	void bonusSingleOperations() {
		assert testerSingle("sin", 42) == Math.sin(42)
		assert testerSingle("cos", 42) == Math.cos(42)
		assert testerSingle("tan", 42) == Math.tan(42)
		assert testerSingle("round", 42.2) == 42.0
		assert testerSingle("floor", 42.2) == 42.0
		assert testerSingle("ceil", 42.2) == 43.0
		assert testerSingle("nil", 42) == 42
		assert testerSingle("GABEISTHEMOSTAWESOMEPROGRAMMERALIVE", 42) == 42
	}

	@Test
	void singleDisabled() {
		//assert testerSingle("loge", 42) == Math.log(42)
		assert testerSingle("loge", 42, 2) == null
		//GO GET THAT MATH OBJECT
		TO<?> mathTO = env.getObjectsByType("EDDIE").toArray().first()
		TO<?> sourceTO = env.getObjectsByType("SOURCE").toArray().first()
		println env.getAllWorldProperties()
		assert env.getPropertyValue(mathTO, "status", Integer.class) == 2
		assert testerSingle(mathTO, sourceTO) == null
		env.setPropertyValue(sourceTO, "status", 1)
		assert testerSingle(mathTO, sourceTO) == Math.log(42)
		assert env.getPropertyValue(mathTO, "status", Integer.class) == 1
	}

	@Test
	void singleDisabled5000() {
		assert testerSingle("loge", 42) == Math.log(42)
		//GO GET THAT MATH OBJECT
		TO<?> mathTO = env.getObjectsByType("EDDIE").toArray().first()
		TO<?> sourceTO = env.getObjectsByType("SOURCE").toArray().first()
		assert env.getPropertyValue(mathTO, "status", Integer.class) == 1

		env.setPropertyValue(sourceTO, "lastValue", -5000.00)
		assert testerSingle(mathTO, sourceTO) == null
		assert env.getPropertyValue(mathTO, "status", Integer.class) == 2
		assert al.checkAlert(null)

		env.setPropertyValue(sourceTO, "lastValue", 13)
		assert testerSingle(mathTO, sourceTO) == Math.log(13)
		assert env.getPropertyValue(mathTO, "status", Integer.class) == 1
	}


	@Test
	void singleNoStatus() {
		assert testerSingle("sin", 42, null) == Math.sin(42)
		assert testerSingle("sin", 42, 1) == Math.sin(42)
	}

}
