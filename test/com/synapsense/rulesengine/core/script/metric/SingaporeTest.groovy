package com.synapsense.rulesengine.core.script.metric

import GroovyRuleStubs.FakeEnvironment
import GroovyRuleStubs.FakeAlertService
import org.junit.Before
import org.junit.*
import static org.junit.Assert.*
import groovy.mock.interceptor.StubFor

import com.synapsense.rulesengine.core.environment.Property
import com.synapsense.rulesengine.core.environment.RuleAction
import com.synapsense.rulesengine.core.environment.RuleI
import com.synapsense.dto.TOFactory
import com.synapsense.deploymentlab.*
import com.synapsense.service.Environment
import GroovyRuleStubs.*
import com.synapsense.dto.TO
import com.synapsense.service.AlertService

/**
 * Testing the singapore metrics in the FakeServer stubs.
 * @author Gabriel Helman
 * @since
 * Date: 3/29/12
 * Time: 11:56 AM
 */
class SingaporeTest {

	static FakeEnvironment env = (FakeEnvironment) FakeEnvironment.getInstance()
	static FakeAlertService al = FakeAlertService.getInstance()

	@Before
	void resetState() {
		env.makeTrouble(false)
		env.autoCreateType( true )
		env.reset()
		al.reset()
	}

	Double testerLD(Double lighting, Double area, boolean makePUE = true) {
		al.reset()
		def dc = env.createObject("DC")
		env.setPropertyValue(dc, "area", area)
		if (makePUE) {
			def pueTO = env.createObject("PUE")
			env.setRelation(dc, pueTO)
			env.setPropertyValue(pueTO, "lightingPower", lighting)
		}
		def LD = new LightingDensity()
		return LD.run(new RuleI("LD"), new Property("DC", dc))
	}

	@Test
	void lightingDensity() {
		//watts = null
		assert testerLD(null, null) == null
		assert !al.hasAlert()

		assert testerLD(null, -5) == null
		assert !al.hasAlert()

		assert testerLD(null, 0) == null
		assert !al.hasAlert()

		assert testerLD(null, 100) == null
		assert !al.hasAlert()

		//watts < 0

		assert testerLD(-5, null) == null
		assert !al.hasAlert()

		assert testerLD(-5, -10) == 500
		assert !al.hasAlert()

		assert testerLD(-5, 0) == null
		assert al.checkAlert(al.UMR)

		assert testerLD(-5, 100) == -50
		assert !al.hasAlert()

		//watts = 0
		assert testerLD(0, null) == null
		assert !al.hasAlert()

		//minus zero acts a little weird, and really, who cares?
		//assert testerLD(0,-10) == -0.0
		//assert !al.hasAlert()

		assert testerLD(0, 0) == null
		assert al.checkAlert(al.UMR)

		assert testerLD(0, 100) == 0
		assert !al.hasAlert()

		//watts > 0
		assert testerLD(5, null) == null
		assert !al.hasAlert()

		assert testerLD(5, -10) == -500
		assert !al.hasAlert()

		assert testerLD(5, 0) == null
		assert al.checkAlert(al.UMR)

		assert testerLD(5, 100) == 50
		assert !al.hasAlert()

		//with no PUE

		assert testerLD(5, 100, false) == null
		assert !al.hasAlert()
	}


	TO<?> startADC() {
		def dc = env.createObject("DC")
		return dc
	}

	Double testerRH(TO<?> dc) {
		al.reset()
		def RH = new RHAmbient()
		return RH.run(new RuleI("RHAmbient"), new Property("DC", dc))
	}

	TO<?> populateDCWithRack(TO<?> dc, Double value, boolean hasRH = true, Integer sensorStatus = 1, Integer rackStatus = 1) {
		def rack = env.createObject("RACK")
		def room = env.createObject("ROOM")

		def node = env.createObject("WSNNODE")
		env.setRelation(dc, rack)
		env.setRelation(dc, room)
		env.setRelation(room, node)
		env.setPropertyValue(rack, "status", rackStatus)
		env.setPropertyValue(node, "status", 1)
		//not a real property
		env.setPropertyValue(rack, "node", node)

		def battery = env.createObject("WSNSENSOR")
		env.setRelation(node, battery)
		env.setPropertyValue(battery, "status", 1)
		env.setPropertyValue(battery, "dataclass", 999)
		env.setPropertyValue(battery, "lastValue", value)
		env.setPropertyValue(battery, "name", "battery, yo")


		def sensor = null
		if (hasRH) {
			sensor = env.createObject("WSNSENSOR")
			env.setRelation(node, sensor)
			env.setPropertyValue(sensor, "status", sensorStatus)
			env.setPropertyValue(sensor, "dataclass", 201)
			env.setPropertyValue(sensor, "lastValue", value)
			env.setPropertyValue(sensor, "name", "rack rh")
		}
		env.setPropertyValue(rack, "rh", sensor)
		return sensor
	}

	TO<?> populateDCWithCrac(TO<?> dc, Double value, boolean makeOtherSensor = true, boolean makeTheMainSensor = true, Integer sensorStatus = 1, Integer cracStatus = 1) {
		def crac = env.createObject("CRAC")
		def room = env.createObject("ROOM")

		def node = env.createObject("WSNNODE")
		env.setRelation(dc, crac)
		env.setRelation(dc, room)
		env.setRelation(room, node)
		env.setPropertyValue(crac, "status", cracStatus)
		env.setPropertyValue(node, "status", 1)
		//not a real property
		env.setPropertyValue(crac, "node", node)

		def battery = env.createObject("WSNSENSOR")
		env.setRelation(node, battery)
		env.setPropertyValue(battery, "status", 1)
		env.setPropertyValue(battery, "dataclass", 999)
		env.setPropertyValue(battery, "lastValue", value)
		env.setPropertyValue(battery, "name", "battery, yo")

		def sensor = null
		if (makeTheMainSensor) {
			sensor = env.createObject("WSNSENSOR")
			env.setRelation(node, sensor)
			env.setPropertyValue(sensor, "status", sensorStatus)
			env.setPropertyValue(sensor, "dataclass", 201)
			env.setPropertyValue(sensor, "lastValue", value)
			env.setPropertyValue(sensor, "name", "crac supply")
		}
		env.setPropertyValue(crac, "supplyRh", sensor)


		if (makeOtherSensor) {
			def sensor2 = env.createObject("WSNSENSOR")
			env.setPropertyValue(sensor2, "status", sensorStatus)
			env.setPropertyValue(sensor2, "dataclass", 201)
			env.setPropertyValue(sensor2, "lastValue", (value ? value + 10 : value))
			env.setPropertyValue(sensor2, "name", "crac return")
			env.setPropertyValue(crac, "returnRh", sensor2)
		} else {
			env.setPropertyValue(crac, "returnRh", null)
		}

		return sensor
	}

	TO<?> populateDCWithRHSensor(TO<?> dc, Double value, Integer sensorStatus = 1) {
		def room = env.createObject("ROOM")
		def sensor = env.createObject("WSNSENSOR")
		def node = env.createObject("WSNNODE")
		env.setRelation(dc, room)
		env.setRelation(room, node)
		env.setRelation(node, sensor)
		env.setPropertyValue(sensor, "status", sensorStatus)
		env.setPropertyValue(node, "status", 1)
		env.setPropertyValue(sensor, "dataclass", 201)
		env.setPropertyValue(sensor, "lastValue", value)
		env.setPropertyValue(sensor, "name", "random rh")

		def battery = env.createObject("WSNSENSOR")
		env.setRelation(node, battery)
		env.setPropertyValue(battery, "status", 1)
		env.setPropertyValue(battery, "dataclass", 999)
		env.setPropertyValue(battery, "lastValue", value)
		env.setPropertyValue(battery, "name", "battery, yo")

		return sensor
	}


	@Test
	void basicRHfromRacks() {
		def dc = startADC()
		for (int i = 0; i < 10; i++) {
			populateDCWithRack(dc, i)
		}
		assert testerRH(dc) == 4.5
	}

	@Test
	void basicRHfromCracs() {
		def dc = startADC()
		for (int i = 0; i < 10; i++) {
			populateDCWithCrac(dc, i)
		}
		assert testerRH(dc) == 4.5
	}

	@Test
	void basicRHfromOther() {
		def dc = startADC()
		for (int i = 0; i < 10; i++) {
			populateDCWithRHSensor(dc, i)
		}
		assert testerRH(dc) == 4.5
	}


	@Test
	void allThreeTypes() {
		def dc = startADC()
		for (int i = 0; i < 5; i++) {
			populateDCWithRack(dc, i)
		}
		for (int i = 5; i < 10; i++) {
			populateDCWithCrac(dc, i)
		}
		for (int i = 10; i < 15; i++) {
			populateDCWithRHSensor(dc, i)
		}
		assert testerRH(dc) == 7.0
	}

	@Test
	void noRH() {
		def dc = startADC()
		assert testerRH(dc) == null
		assert !al.hasAlert()
	}



	@Test
	void testPlanNull() {
		//sum = null
		def dcNull = startADC()

		List<TO<?>> sensors = []
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithRHSensor(dcNull, null)
		}
		assert testerRH(dcNull) == null
		assert al.checkAlert(al.NI)
	}

	@Test
	void testPlanDisabled() {
		///sum = disabled
		def dcDisabled = startADC()

		List<TO<?>> sensors = []
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithRHSensor(dcDisabled, i)
		}
		env.setPropertyValue(sensors[4], "status", 2)
		assert testerRH(dcDisabled) == 1.5
		assert !al.hasAlert()

		for (def t: sensors) {
			env.setPropertyValue(t, "status", 2)
		}
		assert testerRH(dcDisabled) == null
		assert !al.hasAlert()
	}

	@Test
	void testPlanErrorCodes() {
		def dc = startADC()
		//sum = -2000 or -3000
		List<TO<?>> sensors = []
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithRHSensor(dc, i)
		}
		env.setPropertyValue(sensors[4], "lastValue", -2000.00)
		assert testerRH(dc) == null
		assert al.checkAlert(al.NI)

		env.setPropertyValue(sensors[4], "lastValue", 4)
		env.setPropertyValue(sensors[3], "lastValue", -3000.00)
		assert testerRH(dc) == null
		assert al.checkAlert(al.NI)

		//sum = -5000

		env.setPropertyValue(sensors[3], "lastValue", 3)
		env.setPropertyValue(sensors[2], "lastValue", -5000.00)
		assert testerRH(dc) == 2
		assert !al.hasAlert()

		//now, for all of them:

		for (def t: sensors) {
			env.setPropertyValue(t, "lastValue", -2000.00)
		}
		assert testerRH(dc) == null
		assert al.checkAlert(al.NI)

		for (def t: sensors) {
			env.setPropertyValue(t, "lastValue", -3000.00)
		}
		assert testerRH(dc) == null
		assert al.checkAlert(al.NI)

		for (def t: sensors) {
			env.setPropertyValue(t, "lastValue", -5000.00)
		}
		assert testerRH(dc) == null
		assert !al.hasAlert()
	}

	@Test
	void testPlanValues() {
		println "testPlanValues()"
		def dc = startADC()
		List<TO<?>> sensors = []
		//sum < 0
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithRHSensor(dc, -i)
		}
		assert testerRH(dc) == -2
		assert !al.hasAlert()

		//sum = 0
		for (def t: sensors) {
			env.setPropertyValue(t, "lastValue", 00)
		}
		assert testerRH(dc) == 0
		assert !al.hasAlert()

		//sum > 0
		int i = 0
		for (def t: sensors) {
			env.setPropertyValue(t, "lastValue", i)
			i++
		}
		assert testerRH(dc) == 2
		assert !al.hasAlert()
	}



	@Test
	void testPlanNullRack() {
		//sum = null
		def dcNull = startADC()

		List<TO<?>> sensors = []
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithRack(dcNull, null)
		}
		assert testerRH(dcNull) == null
		assert al.checkAlert(al.NI)
	}

	@Test
	void testPlanDisabledRack() {
		///sum = disabled
		def dcDisabled = startADC()

		List<TO<?>> sensors = []
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithRack(dcDisabled, i)
		}
		env.setPropertyValue(sensors[4], "status", 2)
		assert testerRH(dcDisabled) == 1.5
		assert !al.hasAlert()

		for (def t: sensors) {
			env.setPropertyValue(t, "status", 2)
		}
		assert testerRH(dcDisabled) == null
		assert !al.hasAlert()
	}

	@Test
	void testPlanErrorCodesRack() {
		def dc = startADC()
		//sum = -2000 or -3000
		List<TO<?>> sensors = []
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithRack(dc, i)
		}
		env.setPropertyValue(sensors[4], "lastValue", -2000.00)
		assert testerRH(dc) == null
		assert al.checkAlert(al.NI)

		env.setPropertyValue(sensors[4], "lastValue", 4)
		env.setPropertyValue(sensors[3], "lastValue", -3000.00)
		assert testerRH(dc) == null
		assert al.checkAlert(al.NI)

		//sum = -5000

		env.setPropertyValue(sensors[3], "lastValue", 3)
		env.setPropertyValue(sensors[2], "lastValue", -5000.00)
		assert testerRH(dc) == 2
		assert !al.hasAlert()

		//now, for all of them:

		for (def t: sensors) {
			env.setPropertyValue(t, "lastValue", -2000.00)
		}
		assert testerRH(dc) == null
		assert al.checkAlert(al.NI)

		for (def t: sensors) {
			env.setPropertyValue(t, "lastValue", -3000.00)
		}
		assert testerRH(dc) == null
		assert al.checkAlert(al.NI)

		for (def t: sensors) {
			env.setPropertyValue(t, "lastValue", -5000.00)
		}
		assert testerRH(dc) == null
		assert !al.hasAlert()
	}

	@Test
	void testPlanValuesRack() {
		def dc = startADC()
		List<TO<?>> sensors = []
		//sum < 0
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithRack(dc, -i)
		}
		assert testerRH(dc) == -2
		assert !al.hasAlert()

		//sum = 0
		for (def t: sensors) {
			env.setPropertyValue(t, "lastValue", 00)
		}
		assert testerRH(dc) == 0
		assert !al.hasAlert()

		//sum > 0
		int i = 0
		for (def t: sensors) {
			env.setPropertyValue(t, "lastValue", i)
			i++
		}
		assert testerRH(dc) == 2
		assert !al.hasAlert()
	}

	//

	@Test
	void testPlanNullCrac() {
		//sum = null
		def dcNull = startADC()

		List<TO<?>> sensors = []
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithCrac(dcNull, null)
		}
		assert testerRH(dcNull) == null
		assert al.checkAlert(al.NI)
	}

	@Test
	void testPlanDisabledCrac() {
		///sum = disabled
		def dcDisabled = startADC()

		List<TO<?>> sensors = []
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithCrac(dcDisabled, i)
		}
		env.setPropertyValue(sensors[4], "status", 2)
		assert testerRH(dcDisabled) == 1.5
		assert !al.hasAlert()

		for (def t: sensors) {
			env.setPropertyValue(t, "status", 2)
		}
		assert testerRH(dcDisabled) == null
		assert !al.hasAlert()
	}

	@Test
	void testPlanErrorCodesCrac() {
		def dc = startADC()
		//sum = -2000 or -3000
		List<TO<?>> sensors = []
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithCrac(dc, i)
		}
		env.setPropertyValue(sensors[4], "lastValue", -2000.00)
		assert testerRH(dc) == null
		assert al.checkAlert(al.NI)

		env.setPropertyValue(sensors[4], "lastValue", 4)
		env.setPropertyValue(sensors[3], "lastValue", -3000.00)
		assert testerRH(dc) == null
		assert al.checkAlert(al.NI)

		//sum = -5000

		env.setPropertyValue(sensors[3], "lastValue", 3)
		env.setPropertyValue(sensors[2], "lastValue", -5000.00)
		assert testerRH(dc) == 2
		assert !al.hasAlert()

		//now, for all of them:

		for (def t: sensors) {
			env.setPropertyValue(t, "lastValue", -2000.00)
		}
		assert testerRH(dc) == null
		assert al.checkAlert(al.NI)

		for (def t: sensors) {
			env.setPropertyValue(t, "lastValue", -3000.00)
		}
		assert testerRH(dc) == null
		assert al.checkAlert(al.NI)

		for (def t: sensors) {
			env.setPropertyValue(t, "lastValue", -5000.00)
		}
		assert testerRH(dc) == null
		assert !al.hasAlert()
	}

	@Test
	void testPlanValuesCrac() {
		def dc = startADC()
		List<TO<?>> sensors = []
		//sum < 0
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithCrac(dc, -i)
		}
		assert testerRH(dc) == -2
		assert !al.hasAlert()

		//sum = 0
		for (def t: sensors) {
			env.setPropertyValue(t, "lastValue", 00)
		}
		assert testerRH(dc) == 0
		assert !al.hasAlert()

		//sum > 0
		int i = 0
		for (def t: sensors) {
			env.setPropertyValue(t, "lastValue", i)
			i++
		}
		assert testerRH(dc) == 2
		assert !al.hasAlert()
	}

	@Test
	void singleSensorCracs() {
		def dc = startADC()
		List<TO<?>> sensors = []
		//sum < 0
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithCrac(dc, -i, false)
		}
		assert testerRH(dc) == -2
		assert !al.hasAlert()

		//sum = 0
		for (def t: sensors) {
			env.setPropertyValue(t, "lastValue", 00)
		}
		assert testerRH(dc) == 0
		assert !al.hasAlert()

		//sum > 0
		int i = 0
		for (def t: sensors) {
			env.setPropertyValue(t, "lastValue", i)
			i++
		}
		assert testerRH(dc) == 2
		assert !al.hasAlert()

	}

	@Test
	void someRacksWithoutRH() {
		def dc = startADC()
		List<TO<?>> sensors = []
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithRack(dc, i)
		}
		for (int i = 5; i < 10; i++) {
			populateDCWithRack(dc, i, false)
		}
		assert testerRH(dc) == 2
		assert !al.hasAlert()
	}

	@Test
	void someCracsWithoutRH() {
		def dc = startADC()
		List<TO<?>> sensors = []
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithCrac(dc, i)
		}
		for (int i = 5; i < 10; i++) {
			populateDCWithCrac(dc, i, false, false)
		}
		assert testerRH(dc) == 2
		assert !al.hasAlert()
	}

	@Test
	void racksSomeDisabledSensors() {
		def dc = startADC()
		List<TO<?>> sensors = []
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithRack(dc, i)
		}
		for (int i = 5; i < 10; i++) {
			populateDCWithRack(dc, i, true, 2)
		}
		assert testerRH(dc) == 2
		assert !al.hasAlert()
	}

	@Test
	void cracsSomeDisabledSensors() {
		def dc = startADC()
		List<TO<?>> sensors = []
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithCrac(dc, i)
		}
		for (int i = 5; i < 10; i++) {
			populateDCWithCrac(dc, i, true, true, 2)
		}
		assert testerRH(dc) == 2
		assert !al.hasAlert()
	}

	@Test
	void someDisabledSensors() {
		def dc = startADC()
		List<TO<?>> sensors = []
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithRHSensor(dc, i)
		}
		for (int i = 5; i < 10; i++) {
			populateDCWithRHSensor(dc, i, 2)
		}
		for (int i = 5; i < 10; i++) {
			populateDCWithRHSensor(dc, -5000.00, 1)
		}
		for (int i = 5; i < 10; i++) {
			populateDCWithRHSensor(dc, -5000.00, 2)
		}
		assert testerRH(dc) == 2
		assert !al.hasAlert()
	}

	@Test
	void nullSensorStatus() {
		def dc = startADC()
		List<TO<?>> sensors = []
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithRHSensor(dc, i, null)
		}
		assert testerRH(dc) == 2
		assert !al.hasAlert()
	}


	@Test
	void disabledRacks() {
		def dc = startADC()
		List<TO<?>> sensors = []
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithRack(dc, i)
		}
		for (int i = 5; i < 10; i++) {
			populateDCWithRack(dc, i, true, 1, 2)
		}
		assert testerRH(dc) == 2
		assert !al.hasAlert()
	}

	@Test
	void disabledCracs() {
		def dc = startADC()
		List<TO<?>> sensors = []
		for (int i = 0; i < 5; i++) {
			sensors += populateDCWithCrac(dc, i)
		}
		for (int i = 5; i < 10; i++) {
			populateDCWithCrac(dc, i, true, true, 1, 2)
		}
		assert testerRH(dc) == 2
		assert !al.hasAlert()
	}

}
