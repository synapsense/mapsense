package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;
import java.util.Date;

import org.apache.log4j.Logger;

import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.util.LocalizationUtils;

/**
 * Calculates the Lighting Density of a Datacenter. Requires both that a PUE
 * reporter be configured and that an Area is specified in the DC Properties.
 * 
 * LD (W/m2) = Total Lighting Power (in watts) / DC area (sq. meters).
 * 
 * The area is converted from sq ft to sq meters.
 * 
 * @author Gabriel Helman
 * @since Jupiter
 */
public class LightingDensity implements RuleAction {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(LightingDensity.class);

	public Object run(RuleI triggeredRule, Property calculated) {
		logger.info("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			Double finalValue = null;

			// LD is watts of lighting / square meters
			// go get kw from PUE reporter; convert to watts
			// go get dc area, convert to m2
			// do math

			// lightingPower

			Collection<TO<?>> pue = env.getRelatedObjects(nativeTO, "PUE", true);
			if (pue.size() == 0) {
				logger.info("No PUE Reporter configured, cannot calculate Lighting Density.");
				return null;
			}

			Double lightingPower = 0.0;
			for (TO<?> p : pue) {
				lightingPower = env.getPropertyValue(p, "lightingPower", Double.class);
			}

			if (lightingPower == null) {
				logger.info("Lighting Power is null, cannot calculate Lighting Density.");
				return null;
			}

			Double area = env.getPropertyValue(nativeTO, "area", Double.class);
			if (area == null) {
				logger.info("No DC area entered, cannot calculate Lighting Density.");
				return null;
			}

			finalValue = (lightingPower * 1000) / area;
			if (finalValue.isInfinite() || finalValue.isNaN()) {
				logger.warn("Rule ${triggeredRule.getName()}: Unacceptable Math Result: $finalValue");
				throwAlert(triggeredRule, calculated, nativeTO,
				        LocalizationUtils.getLocalizedString("alert_message_unacceptable_math_result", "$finalValue"));
				return null;
			}
			return finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

	private void throwAlert(RuleI triggeredRule, final Property calculated, TO<?> nativeTO, String message) {
		AlertService alertService = calculated.getAlertService();
		alertService.raiseAlert(new Alert("Math Error", "Math Error Alert", new Date(), LocalizationUtils
		        .getLocalizedString("alert_message_math_error_has_been_detected_in_rule", triggeredRule.getName())
		        + " $message", nativeTO));
	}

}
