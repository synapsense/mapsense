package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.util.LocalizationUtils;

/**
 * Calculate the average humidity in a datacenter. Literally the average of all
 * rack relative humidity measurements (per standard two or more measurements) taken
 * on the datacenter floor.
 * 
 * @author Gabriel Helman
 * @since Jupiter
 * 
 */
public class RHAmbient implements RuleAction {

	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(RHAmbient.class);
	private final static double ERROR1 = -2000.0;
	private final static double ERROR2 = -3000.0;
	private final static double ERROR3 = -5000.0;

	public Object run(RuleI triggeredRule, Property calculated) {
		logger.info("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			Double finalValue = null;
			// get all humidity sensors in this DC
			// average their RH values

			
			//from racks: rh
			//from cracs: supplyRh, ignore returnRh
			//from all other wsnnodes that are humidity
			Set<TO<?>> rhSensors = new HashSet<TO<?>>();
			Set<TO<?>> sensorsToIgnore = new HashSet<TO<?>>();
			Double total = 0.0;
			Double count = 0.0;

			//RACKS
			Collection<TO<?>> racks = env.getRelatedObjects(nativeTO, "RACK", true);
			for (TO<?> rackTO : racks) {
				// check for disabled
				Integer status = env.getPropertyValue(rackTO, "status", Integer.class);
				TO<?> sensorTO = env.getPropertyValue(rackTO, "rh", TO.class);
				if (status != null && status >= 2) {
					if(sensorTO != null){
						sensorsToIgnore.add(sensorTO);
					}
					
				} else if (sensorTO != null) {
					rhSensors.add(sensorTO);
				}
			}

			//CRACs
			Collection<TO<?>> cracs = env.getRelatedObjects(nativeTO, "CRAC", true);
			for (TO<?> cracTO : cracs) {
				// check for disabled
				Integer status = env.getPropertyValue(cracTO, "status", Integer.class);
				TO<?> sensorTO = env.getPropertyValue(cracTO, "supplyRh", TO.class);
				if (status != null && status >= 2) {
					if(sensorTO != null){
						sensorsToIgnore.add(sensorTO);
					}
					
				} else if (sensorTO != null) {
					rhSensors.add(sensorTO);
				}

				TO<?> intakeSensorTO = env.getPropertyValue(cracTO, "returnRh", TO.class);
				if(intakeSensorTO!= null){
					sensorsToIgnore.add(intakeSensorTO);
				}
			}

			//any other WSNSENSOR measuring humidity
			Collection<TO<?>> allSensors = env.getRelatedObjects(nativeTO, "WSNSENSOR", true);
			for (TO<?> sensorTO : allSensors) {
				if(!rhSensors.contains(sensorTO) && !sensorsToIgnore.contains(sensorTO)){
					if( env.getPropertyValue(sensorTO, "dataclass", Integer.class ) == 201 ){
						rhSensors.add(sensorTO);
					}
				}
			}

			
			for(TO<?> sensorTO : rhSensors){
				//ignore disabled sensors as well
				Double value = env.getPropertyValue(sensorTO, "lastValue", Double.class);
				Integer sensorStatus = env.getPropertyValue(sensorTO, "status", Integer.class);
				if( (sensorStatus != null && sensorStatus >= 2) || (value == ERROR3)) {
					continue;
				}
				// skip error codes
				if ((value == ERROR1) || (value == ERROR2)) {
					value = null;
				}
				if (value == null) {
					throwAlert(triggeredRule, calculated, nativeTO,
					        LocalizationUtils.getLocalizedString("alert_message_null_input_to_rule"));
					return null;
				}


				logger.trace("averaging with sensor $sensorTO ${env.getPropertyValue(sensorTO,"name",String.class)} = $value")

				total += value;
				count += 1.0;
			}
			
			if(count == 0){
				logger.warn("Empty inputs to rule ${triggeredRule.getName()}");
				return null;
			}
			
			finalValue = total / count;

			if (finalValue.isInfinite() || finalValue.isNaN()) {
				logger.warn("Rule ${triggeredRule.getName()}: Unacceptable Math Result: $finalValue");
				throwAlert(triggeredRule, calculated, nativeTO,
				        LocalizationUtils.getLocalizedString("alert_message_unacceptable_math_result", "$finalValue"));
				return null;
			}

			logger.info("RHAmbient is " + total + " / " + count + " = " + finalValue);
			return finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

	private void throwAlert(RuleI triggeredRule, final Property calculated, TO<?> nativeTO, String message) {
		AlertService alertService = calculated.getAlertService();
		alertService.raiseAlert(new Alert("Math Error", "Math Error Alert", new Date(), LocalizationUtils
		        .getLocalizedString("alert_message_math_error_has_been_detected_in_rule", triggeredRule.getName())
		        + " $message", nativeTO));
	}

}
