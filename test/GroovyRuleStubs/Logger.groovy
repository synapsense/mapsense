package org.apache.log4j;

//TODO: replace this stub with the real deal

/**
 * 
 * @author Gabriel Helman
 * @since 
 * Date: 9/9/11
 * Time: 12:58 PM
 */
 class Logger {

	 //def invokeMethod(String name, args) {
	//	 println "logged something!"
	 //}

	 public static boolean throwExceptions = true

	 public static setThrowing(boolean t){
		 throwExceptions = t
	 }

	 public void log(def msg, Exception e){
		 if ( e ){
			 println "LOG: $msg $e"
		 } else {
			 println "LOG: $msg"
		 }


		 if ( e != null && throwExceptions ){
			 //println "got exception $e"
			 //e.printStackTrace()
			 throw e
		 }
	 }

	 public void error(def msg, Exception e=null){ log(msg,e) }
	 public void warn(def msg, Exception e=null){ log(msg,e) }
	 public void info(def msg, Exception e=null){ log(msg,e) }
	 public void debug(def msg, Exception e=null){ log(msg,e) }
	 public void trace(def msg, Exception e=null){ log(msg,e) }

	 public static Logger getLogger(Object anything){
		 return new Logger()
	 }

}
