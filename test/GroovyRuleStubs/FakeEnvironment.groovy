package GroovyRuleStubs

import DeploymentLab.CentralCatalogue
import com.synapsense.dto.EnvObject
import com.synapsense.exception.IllegalInputParameterException
import com.synapsense.exception.TagNotFoundException
import com.synapsense.service.Environment
import com.synapsense.dto.TO
import com.synapsense.dto.ObjectType
import com.synapsense.dto.TOFactory
import com.synapsense.dto.ValueTO
import com.synapsense.dto.TOBundle
import com.synapsense.dto.EnvObjectBundle
import com.synapsense.dto.ObjectTypeMatchMode
import com.synapsense.search.QueryDescription
import com.synapsense.exception.ObjectNotFoundException
import com.synapsense.dto.Tag
import com.synapsense.exception.PropertyNotFoundException
import com.synapsense.dto.Relation
import com.synapsense.dto.CollectionTO
import com.synapsense.dto.PropertyDescr
import com.synapsense.dto.TagDescriptor
import com.synapsense.dto.EnvObjectHistory

/**
 *  Stub for testing various mapsense things against the server.  Now in it's own file.
 *
 *  Note that methods as yet unimplemented throw an UnsupportedOperationException().  If you get one of those,
 *  that means you get to go implement that function.
 *
 * @author Gabriel Helman
 * @since Mars
 * Date: 7/24/12
 * Time: 11:37 AM
 */

public class FakeEnvironment implements Environment {
	private static int TOcounter;

	private static FakeEnvironment inst;
	static Map<TO<?>, Map> objectProps = new HashMap<>()   //map of to : map(prop name: prop value)
	private static Map<String,ObjectType> types = new HashMap<>()  //map of typename : [tos of that type]
	private static Map<String,List<TO<?>>> objectsByType = new HashMap<>()  //map of typename : [tos of that type]

	private static Map<TO<?>,Set<TO<?>>> relations = new HashMap<>() //map of to :  children tos

	private static Map<TO<?>,List<Tag>> internalsTags = new HashMap<>()

	private boolean trouble = false
	private boolean autoCreateType = false

	/**
	 * SINGLE.  TON.
	 */
	private FakeEnvironment() {
        reset()
	}

	public static Environment getInstance() {
		if (inst == null) {
			inst = new FakeEnvironment()
		}
		return inst
	}


	def invokeMethod(String name, args) {
		//System.out.println( "ALERT! $args" )
		System.out.println("Fake Env: invoked method $name(${args.join(', ')})")
		//return 42
	}

	List<TO<?>> getAllWorld(){
		def result = []
		objectsByType.each{ k, v ->
			result.addAll(v)
		}
		return result
	}

	def getAllWorldProperties(){
		return objectProps
	}

	void makeTrouble(boolean now){
		trouble = now
	}

	void autoCreateType( boolean autoCreate ){
		autoCreateType = autoCreate
	}

	void reset( boolean initdb = true ){
		TOcounter = 1
		objectProps.clear()
		relations.clear()
		types.clear()
        objectsByType.clear()
        internalsTags.clear()

        // Create the things that the DB script seeds at install.
        createObjectType("ROOT")
        createObjectType("CONFIGURATION_DATA")

        if( initdb ) {
            TO<?> to = createObject("ROOT")
            setPropertyValue( to, "name", "Root" )
            setPropertyValue( to, "version", CentralCatalogue.getApp("db.version") )

            createObject("CONFIGURATION_DATA")
        }
	}

	@Override
	ObjectType createObjectType(String s) {
        String sup = s.toUpperCase()
		if ( ! types.containsKey(sup) ){
			types[sup] = new FakeObjectType(s)
            objectsByType[sup] = []
		}
		return this.getObjectType(s)
	}

	@Override
	ObjectType[] getObjectTypes(String[] strings) {
		throw new UnsupportedOperationException()
	}

	@Override
	ObjectType getObjectType(String s) {
		return types.get(s.toUpperCase())
	}

	@Override
	String[] getObjectTypes() {
		return types.keySet().toArray( new String[types.size()] )
	}

	@Override
	void updateObjectType(ObjectType objectType) {
		//println "ENV: update accepted!"
	}

	@Override
	void deleteObjectType(String s) {
		throw new UnsupportedOperationException()
	}

	@Override
	TO<?> createObject(String s) {
		def to = TOFactory.getInstance().loadTO("${s.toUpperCase()}:$TOcounter")
        TOcounter++
        getObjectsByType(s).add( to )
		objectProps[to] = [:]
		return to
	}

	@Override
	TO<?> createObject(String s, ValueTO[] valueTOs) {
		//println "ENV: Create object type $s"
		def to = this.createObject(s)
		valueTOs.each{ v ->
			//println "set ${v.getPropertyName()} to ${v.getValue()} for $to"
			this.setPropertyValue(to, v.getPropertyName(), v.getValue())
		}
		return to
	}

	@Override
	TOBundle createObjects(EnvObjectBundle envObjectBundle) {
		TOBundle result = new TOBundle();

		for( String label : envObjectBundle.getLabels() ) {
			EnvObject envObj = envObjectBundle.get(label);
			TO to = createObject( envObj.getTypeName(), envObj.getValues().toArray( new ValueTO[envObj.getValues().size()] ) )
			result.add( label, to )
		}

		return result
	}

	@Override
	Collection<TO<?>> getObjectsByType(String s) {
        if( autoCreateType && getObjectType( s ) == null ) {
            createObjectType( s )
		}
        return objectsByType[s.toUpperCase()]
	}

	@Override
	Collection<TO<?>> getObjectsByType(String s, ObjectTypeMatchMode objectTypeMatchMode) {
		throw new UnsupportedOperationException()
	}

	@Override
	Collection<TO<?>> getObjects(String s, ValueTO[] valueTOs) {
		throw new UnsupportedOperationException()
	}

	@Override
	Collection<TO<?>> getObjects(QueryDescription queryDescription) {
		throw new UnsupportedOperationException()
	}

	@Override
	Collection<TO<?>> getObjects(String s, ObjectTypeMatchMode objectTypeMatchMode, ValueTO[] valueTOs) {
		throw new UnsupportedOperationException()
	}

	@Override
	void deleteObject(TO<?> to) {
		objectProps.remove(to)
		objectsByType.each{ k, v ->
			if( v.contains(to)){
				v.remove(to)
			}
		}
	}

	@Override
	void deleteObject(TO<?> to, int i) {

		if ( i == -1 ){
			this.getChildren(to).each{
				this.deleteObject(it, -1)
			}
		}

		this.deleteObject(to)
	}

	@Override
	Collection<ValueTO> getAllPropertiesValues(TO<?> to) {
		if(!objectProps.containsKey(to)){
			throw new ObjectNotFoundException(to)
		}
		def result = new HashSet<ValueTO>()
		objectProps[to].each{ k, v ->
			result.add(new ValueTO(k,v))
		}
		return result
	}

	@Override
	void setAllPropertiesValues(TO<?> to, ValueTO[] valueTOs) {
		valueTOs.each{ v ->
			this.setPropertyValue(to, v.getPropertyName(), v.getValue())
		}
	}

	@Override
	void setAllPropertiesValues(TO<?> to, Collection<ValueTO> valueTOs, Collection<Tag> tags) {
		valueTOs.each{ v ->
			this.setPropertyValue(to, v.getPropertyName(), v.getValue())
		}
		setAllTagsValues( to, tags )
	}

	@Override
	def <RETURN_TYPE> RETURN_TYPE getPropertyValue(TO<?> to, String s, Class<RETURN_TYPE> return_typeClass) {
		if (trouble){
			throw new RuntimeException("AHHH!  Trouble!")
		}
		if (! objectProps.containsKey(to) ){
			throw new ObjectNotFoundException("Object with TO  [$to] does not exist")
			//return null
		}
		//getPropertyValue should return null if the value is null but PropertyNotFound if the type doesn't have a value?
		if (! objectProps[to].containsKey(s.toUpperCase()) ){
			throw new PropertyNotFoundException("Property [$s] does not exist on object $to")
			//return null
		}
		//println "getPropertyValue($to, $s)"
		return (RETURN_TYPE) objectProps[to][s.toUpperCase()]
	}

	@Override
	def <REQUESTED_TYPE> void setPropertyValue(TO<?> to, String s, REQUESTED_TYPE value) {
		def omap = objectProps.get(to)
		omap[s.toUpperCase()] = value
	}

	@Override
	def <REQUESTED_TYPE> void addPropertyValue(TO<?> to, String s, REQUESTED_TYPE requested_type) {
		throw new UnsupportedOperationException()
	}

	@Override
	def <REQUESTED_TYPE> void removePropertyValue(TO<?> to, String s, REQUESTED_TYPE value) {
        objectProps[to].remove( s )
	}

	@Override
	def <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> to, String s, Class<RETURN_TYPE> return_typeClass) {
		throw new UnsupportedOperationException()
	}

	@Override
	def <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> to, String s, int i, int i1, Class<RETURN_TYPE> return_typeClass) {
		throw new UnsupportedOperationException()
	}

	@Override
	def <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> to, String s, Date date, Date date1, Class<RETURN_TYPE> return_typeClass) {
		throw new UnsupportedOperationException()
	}

	@Override
	def <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> to, String s, Date date, int i, Class<RETURN_TYPE> return_typeClass) {
		throw new UnsupportedOperationException()
	}

	@Override
	Integer getHistorySize(TO<?> to, String s) {
		throw new UnsupportedOperationException()
	}

	@Override
	void setRelation(TO<?> to, TO<?> to1) {
		if ( ! relations.containsKey(to)){
			relations[to] = new HashSet<TO<?>>()
		}
		relations[to].add(to1)
	}

	@Override
	void setRelation(List<Relation> relations) {
		relations.each{ r ->
			r.getChildren().each{ c ->
				this.setRelation(r.parent, c)
			}
		}
	}

	@Override
	void removeRelation(TO<?> to, TO<?> to1) {
		relations[to].remove(to1)

	}

	@Override
	void removeRelation(List<Relation> relations) {
		relations.each{ r ->
			r.getChildren().each{ c ->
				this.removeRelation(r.parent, c)
			}
		}
	}

	@Override
	Collection<TO<?>> getParents(TO<?> to, String s) {
		throw new UnsupportedOperationException()
	}

	@Override
	Collection<TO<?>> getParents(TO<?> to) {
		//the tricky one
		def results = []

		relations.each{ k, v ->
			if ( v.contains(to) ){
				results += k
			}
		}
		return results
	}

	@Override
	Collection<TO<?>> getChildren(TO<?> to) {
        if( relations.containsKey(to) ) {
            return relations[to]
        }
		return []
	}

	@Override
	Collection<TO<?>> getChildren(TO<?> to, String s) {
		def result = []
		this.getChildren(to).each{ c ->
			if(objectsByType[s.toUpperCase()].contains(c)){
				result += c
			}
		}

		return result
	}

	@Override
	Collection<CollectionTO> getChildren(Collection<TO<?>> tos, String s) {
		throw new UnsupportedOperationException()
	}

	@Override
	Collection<CollectionTO> getChildren(Collection<TO<?>> tos) {
		throw new UnsupportedOperationException()
	}

	@Override
	long getPropertyTimestamp(TO<?> to, String s) {
		throw new UnsupportedOperationException()
	}

	@Override
	PropertyDescr getPropertyDescriptor(String s, String s1) {
		throw new UnsupportedOperationException()
	}

	@Override
	Collection<CollectionTO> getPropertyValue(Collection<TO<?>> tos, String[] strings) {
		throw new UnsupportedOperationException()
	}

	@Override
	Collection<CollectionTO> getAllPropertiesValues(Collection<TO<?>> tos) {
		throw new UnsupportedOperationException()
	}

	@Override
	Collection<TO<?>> getRelatedObjects(TO<?> to, String s, boolean b) {
		if(!b){return this.getParents(to)}
		if(!objectsByType.containsKey(s.toUpperCase())){return []}
		def result = []
		if(relations.containsKey(to)){
			relations[to].each{ t ->

				if(objectsByType[s.toUpperCase()].contains(t)){
					result += t
				}
				result.addAll(findChildren(t,s))
			}
		}
		return result
	}

	Collection<TO<?>> findChildren(TO<?> to, String s){
		List found = []
		for(TO t : relations[to]){
			if ( objectsByType[s.toUpperCase()].contains(t) ){
				found.add(t)
			}
			found.addAll( findChildren(t,s) )
		}
		return found
	}

	@Override
	def <RETURN_TYPE> RETURN_TYPE getPropertyValue(String typeName, String propName, Class<RETURN_TYPE> return_typeClass) {
		throw new UnsupportedOperationException()
	}

	@Override
	def <REQUESTED_TYPE> void setPropertyValue(String s, String s1, REQUESTED_TYPE requested_type) {
		throw new UnsupportedOperationException()
		//		objectProps.put(s1, requested_type)

	}

	@Override
	void configurationComplete() {
		//println "ENV: config stop"
	}

	@Override
	void configurationStart() {
		//println "ENV: config start"
	}

	@Override
	void setAllPropertiesValues(TO<?> to, ValueTO[] valueTOs, boolean b) {
		throw new UnsupportedOperationException()
	}

	@Override
	def <REQUESTED_TYPE> void setPropertyValue(TO<?> to, String s, REQUESTED_TYPE requested_type, boolean b) {
		def omap = objectProps.get(to)
		omap[s.toUpperCase()] = requested_type

	}

	@Override
	boolean exists(TO<?> to) {
		return objectProps.containsKey( to )
	}

	@Override
	TagDescriptor getTagDescriptor(String typeName, String propName, String tagName) {

        if (typeName == null) {
            throw new IllegalInputParameterException("Supplied 'typeName' is null");
        }

        if (typeName.isEmpty()) {
            throw new IllegalInputParameterException("Supplied 'typeName' is empty");
        }

        if (propName == null) {
            throw new IllegalInputParameterException("Supplied 'propName' is null");
        }

        if (propName.isEmpty()) {
            throw new IllegalInputParameterException("Supplied 'propName' is empty");
        }

        if (tagName == null) {
            throw new IllegalInputParameterException("Supplied 'tagName' is null");
        }

        if (tagName.isEmpty()) {
            throw new IllegalInputParameterException("Supplied 'tagName' is empty");
        }

        // gets object type by name
        ObjectType type = getObjectType(typeName);
        // if no such
        if (type == null) {
            throw new ObjectNotFoundException("There is no type with name : " + typeName);
        }

        PropertyDescr propDescr = type.getPropertyDescriptor(propName);
        // finds property with the specified name

        // if no such property
        if (propDescr == null) {
            throw new PropertyNotFoundException("There is no property : " + propName + " of type  : " + typeName);
        }

        TagDescriptor result = propDescr.getTagDescriptor(tagName);
        // if no such tag
        if (result == null) {
            throw new TagNotFoundException("There is no tag : " + tagName + " of property  : " + propName
                                                   + " in type : " + typeName);
        }
        return result;
    }

	@Override
	def <RETURN_TYPE> RETURN_TYPE getTagValue(TO<?> to, String s, String s1, Class<RETURN_TYPE> return_typeClass) {
		throw new UnsupportedOperationException()
	}

	@Override
	Collection<Tag> getAllTags(TO<?> to, String s) {
		throw new UnsupportedOperationException()
	}

	@Override
	Collection<Tag> getAllTags(TO<?> to) {
		throw new UnsupportedOperationException()
	}

	@Override
	Map<TO<?>, Collection<Tag>> getAllTags(String s) {
		throw new UnsupportedOperationException()
	}

	@Override
	Map<TO<?>, Map<String, Collection<Tag>>> getTags(Collection<TO<?>> tos, List<String> strings, List<String> strings1) {
		throw new UnsupportedOperationException()
	}

	@Override
	void setTagValue(TO<?> to, String s, String s1, Object o) {
		throw new UnsupportedOperationException()
	}

	@Override
	void setAllTagsValues(TO<?> to, Collection<Tag> tags) {
		if(!internalsTags.containsKey(to)){
			internalsTags[to] = []
		}

		internalsTags[to].addAll(tags)

	}

	@Override
	EnvObjectHistory getHistory(TO<?> to, Collection<String> strings, Date date, Date date1) {
		throw new UnsupportedOperationException()
	}


}

class FakeObjectType implements ObjectType{
	private String n;

	public FakeObjectType(String s){
		this.setName(s)
	}

	@Override
	String getName() {
		return n
	}

	@Override
	void setName(String s) {
		n = s.toUpperCase()
	}

	private Set<PropertyDescr> guts = new HashSet<PropertyDescr>()

	@Override
	Set<PropertyDescr> getPropertyDescriptors() {
		return guts
	}

	@Override
	PropertyDescr getPropertyDescriptor(String propertyName) {
        for (PropertyDescr pd : guts ) {
            if( pd.getName().equals( propertyName ) ) {
                return pd
            }
        }
        return null
	}

    String toString() {
        return "FakeObjectType[name='$n',guts='$guts']"
    }
}
