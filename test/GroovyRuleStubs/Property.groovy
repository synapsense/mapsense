package com.synapsense.rulesengine.core.environment

import com.synapsense.service.AlertService
import com.synapsense.service.Environment
import com.synapsense.dto.*
import GroovyRuleStubs.*

/**
 *
 * @author Gabriel Helman
 * @since
 * Date: 9/9/11
 * Time: 12:55 PM
 */
public class Property {

	static Environment env = FakeEnvironment.getInstance()
	static AlertService al = FakeAlertService.getInstance()

	private Object val;
	private TO<?> to;

	public Property(def val, def to) {
		this.val = val
		this.to = to
	}

	public Object getValue() {
		return val;
	}

	def getName(){
		return val.toString()
	}

	def invokeMethod(String name, args) {
		return "$name"
	}

	public getAlertService() {
		//return new FakeAlertService()
		return al
	}

	public Environment getEnvironment() {
		return env
	}

	public getDataSource() {
		return new FakeDataSource(to);
	}

	public String toString() {
		return "($to*$val)"
	}

}

