package GroovyRuleStubs

import com.synapsense.service.RulesEngine
import com.synapsense.service.RulesEngine.Result
import com.synapsense.dto.TO
import com.synapsense.dto.CRERule
import com.synapsense.dto.RuleType
import com.synapsense.dto.TOFactory


/**
 * Why not?
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/25/12
 * Time: 6:30 PM
 */
class FakeCRE implements RulesEngine {
	private static FakeCRE inst;
	private int TOcounter

	private Map<TO<?>,RuleType> typesByTo = [:]
	private Map<TO<?>,List<CRERule>> rulesByType = [:]
	private Map<TO<?>,CRERule> rulesByTO = [:]

	/**
	 * SINGLE.  TON.
	 */
	private FakeCRE() {
		TOcounter = 1
	}

	public static RulesEngine getInstance() {
		if (inst == null) {
			inst = new FakeCRE()
		}
		return inst
	}

	@Override
	TO<?> addRule(CRERule creRule) {
		def to = TOFactory.getInstance().loadTO("CRERULE:$TOcounter")
		TOcounter++

		rulesByTO.put( to, creRule )
		rulesByType.get( creRule.getRuleType() ).add( creRule )

		return to
	}

	@Override
	TO<?> addRuleType(RuleType ruleType) {
		def to = TOFactory.getInstance().loadTO("CRERULE:$TOcounter")
		TOcounter++

		typesByTo.put( to, ruleType )
		rulesByType.put( to, [] )

		return to
	}

	@Override
	void calculateProperties(Collection<TO<?>> tos, Set<String> strings) {
		throw new UnsupportedOperationException()
	}

	@Override
	Result calculateProperty(TO<?> to, String s, Boolean aBoolean) {
		throw new UnsupportedOperationException()
	}

	@Override
	CRERule getRule(TO<?> to) {
		return rulesByTO.get( to )
	}

	@Override
	TO<?> getRuleId(String ruleName) {
		return rulesByTO.find { to, rule -> rule.getName() == ruleName }?.getKey()
	}

	@Override
	Collection<CRERule> getRulesByObjectId(TO<?> to) {
		List<CRERule> results = []
		for( CRERule r : rulesByTO.values() ) {
			if( r.getDestObjId() == to ) {
				results.add( r )
			}
		}
		return results
	}

	@Override
	Collection<CRERule> getRulesByType(TO<?> to) {
		return rulesByType.get( to )
	}

	@Override
	Collection<CRERule> getRulesByType(String typeName) {
		return getRulesByType( getRuleTypeId( typeName ) )
	}

	@Override
	RuleType getRuleType(TO<?> to) {
		return typesByTo.get( to )
	}

	@Override
	TO<?> getRuleTypeId(String typeName) {
		return typesByTo.find { to, type -> type.getName() == typeName }?.getKey()
	}

	@Override
	Collection<RuleType> getRuleTypes() {
		return typesByTo.values()
	}

	@Override
	boolean isCalculated(TO<?> to, String s) {
		throw new UnsupportedOperationException()
	}

	@Override
	void recalculateAllProperties() {
		throw new UnsupportedOperationException()
	}

	@Override
	boolean removeRule(String ruleName) {
		return removeRule( getRuleId( ruleName ) )
	}

	@Override
	boolean removeRule(TO<?> to) {
		return rulesByTO.remove( to ) != null
	}

	@Override
	boolean removeRuleType(TO<?> to) {
		typesByTo.remove( to )
		List<CRERule> deadRules = rulesByType.remove( to )
		if( deadRules ) {
			rulesByTO.values().removeAll( deadRules )
		}
	}

	@Override
	void updateRuleType(TO<?> to, RuleType ruleType) {
		typesByTo.put( to, ruleType )
	}
}

