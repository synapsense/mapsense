package GroovyRuleStubs

import DeploymentLab.Export.ServerContext
import com.synapsense.service.Environment
import com.synapsense.service.DLImportService
import com.synapsense.service.RulesEngine

/**
 *
 * @author Gabriel Helman
 * @since Jupiter 2
 * Date: 7/24/12
 * Time: 11:32 AM
 */
class FakeContext implements ServerContext {

	DLImportService dlis

	@Override
	Environment getEnv() {
		return FakeEnvironment.getInstance();
	}

	@Override
	DLImportService getImporter() {
		if( dlis == null ) {
			dlis = new FakeDLImportService()
		}
		return dlis
	}

	@Override
	RulesEngine getCRE() {
		return FakeCRE.instance
	}

	@Override
	boolean testConnect() {
		return true
	}
}
