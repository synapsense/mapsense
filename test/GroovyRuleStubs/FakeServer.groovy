package GroovyRuleStubs

import com.synapsense.service.AlertService
import com.synapsense.service.Environment
import com.synapsense.dto.*
import com.synapsense.search.QueryDescription
import com.synapsense.exception.PropertyNotFoundException
import com.synapsense.exception.ObjectNotFoundException

public class FakeAlertService implements AlertService {
	/*
	def invokeMethod(String name, args) {
		System.out.println("ALERT! $args")
		//System.out.println("invoked method $name(${args.join(', ')})")
	}
	*/

	private static FakeAlertService inst
	private FakeAlertService() {}

	public static FakeAlertService getInstance() {
		if (inst == null) {
			inst = new FakeAlertService()
		}
		return inst
	}

	private Alert lastAlert = null

	public Alert getLastAlert(){
		return lastAlert
	}

	public boolean hasAlert(){
		return lastAlert != null
	}

	public reset(){
		lastAlert = null;
	}

	public static final UMR = "alert_message_unacceptable_math_result"
	public static final NI = "alert_message_null_input_to_rule"
	public static final EI = "alert_message_empty_input_to_rule"
	public static final DZ = "alert_message_divide_by_zero"
	public static final noOut = "alert_message_outlet_not_found"
	public static final moreOut = "alert_message_found_more_than_one_outlet"

	public boolean checkAlert(String message){
		if ( message == null ){
			return lastAlert == null
		}

		if ( message != null && lastAlert == null){
			return false
		}

		boolean result = false

		if ( lastAlert.getMessage().contains(message) ){
			result = true
		} else {
			println "message was ${lastAlert.getMessage()}"
		}

		return result
	}

	@Override
	void raiseAlert(Alert alert) {
		lastAlert = alert;
		println "ALERT: $alert"
	}

	@Override
	Alert getAlert(Integer integer) {
		return null  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	Collection<Alert> getAlerts(AlertFilter alertFilter) {
		return null  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	Collection<Alert> getActiveAlerts(TO<?> to, boolean b) {
		return null  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	Collection<Alert> getActiveAlerts(Collection<TO<?>> tos, boolean b) {
		return null  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	Collection<Alert> getAlertsHistory(TO<?> to, Date date, Date date1, boolean b) {
		return null  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	Collection<Alert> getAlerts(Integer[] integers) {
		return null  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	Collection<AlertType> getAllAlertTypes() {
		return null  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	Collection<AlertType> getAlertTypes(String[] strings) {
		return null  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	AlertType getAlertType(String string) {
		return null  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void dismissAlert(Alert alert, String s) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void resolveAlert(Integer integer, String s) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void acknowledgeAlert(Integer integer, String s) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void dismissAlert(Integer integer, String s) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void dismissAlerts(TO<?> to, boolean b, String s) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void createAlertType(AlertType alertType) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void updateAlertType(String s, AlertType alertType) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void deleteAlertType(String s) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void deleteAllAlerts() {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void deleteAlerts(TO<?> to, boolean b) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void deleteAlertsHistoryOlderThan(Date date) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	boolean hasAlerts(TO<?> to, boolean b) {
		return false  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void addActionDestination(String s, String s1, MessageType messageType, String s2) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void removeActionDestination(String s, String s1, MessageType messageType, String s2) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	Collection<String> getActionDestinations(String s, String s1, MessageType messageType) {
		return null  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	Collection<String> getAlertPrioritiesNames() {
		return null  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	AlertPriority getAlertPriority(String s) {
		return null  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void updateAlertPriority(String s, AlertPriority alertPriority) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void setTemplate(String s, MessageTemplate messageTemplate) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	void removeTemplate(String s, MessageType messageType) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	String getActionDestination(String s, String s1, MessageType messageType, String s2) {
		return null  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	int getNumActiveAlerts(TO<?> to, boolean b) {
		return 0  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	Collection<AlertType> getAlertTypesByDestination(String s, MessageType messageType) {
		return null  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	int getNumAlerts(com.synapsense.dto.AlertFilter af){
		return 0;
	}

}


public class FakeDataSource {
	private TO<?> to

	FakeDataSource(TO<?> to) {
		this.to = to
	}


	def invokeMethod(String name, args) {
		//System.out.println( "ALERT! $args" )
		//System.out.println("FakeDataSource: invoked method $name(${args.join(', ')})")
		//return TOFactory.getInstance().loadTO("EDDIE:42");
		return to;
	}

}

