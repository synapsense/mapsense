/**
 * Tests the WSN Object model is being used correctly in components.
 * @author Gabriel Helman
 * Date: 2/14/11
 * Time: 10:07 AM
 * @since Earth
 */

package test

import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test

import DeploymentLab.Model.*
import DeploymentLab.TestUtils

class WSNObjectTest {

	static final String RACKNAME = "RACKNAME"
	static final List<String> typesToIgnore = ["test-tnode2"] //, "generic-constellation2"]

	static ObjectModel omodel
	static ComponentModel cmodel

	/**
	 * Constructs a base model we can use to do tests on
	 */
	@BeforeClass
	static void beforeClass() {
		(omodel,cmodel) = TestUtils.initModels()
	}

	@Test
	public void WSNNetTest() {
		DLComponent net = cmodel.newComponent("network")
		List<DLObject> nets = net.getObjectsOfType("wsnnetwork")
		assert nets.size() == 1
		for (DLObject o : nets) {
			//name
			assert o.getObjectProperty("name").getValue() == "Network"
			//tag
			assert o.getObjectProperty("name").hasTags()
			assert o.getObjectProperty("name").getTag("pluginId").getValue() == "wsn"
			//desc
			assert o.getObjectProperty("desc").getValue() == "WSN Network"
			//version
			assert o.getObjectProperty("version").getValue() == "N29"
			//panId
			assert o.getObjectProperty("panId").getValue() == 0
			//status
			assert o.getObjectProperty("status").getValue() == 1
		}
	}

	@Test
	public void WSNGatewayTest() {
		DLComponent gw = cmodel.newComponent("remote-gateway")
		gw.setPropertyValue("mac_id", "01acc6eb130000d1")
		List<DLObject> nets = gw.getObjectsOfType("wsngateway")
		assert nets.size() == 1
		for (DLObject o : nets) {
			//name
			assert o.getObjectProperty("name").getValue() == "Gateway"
			//location
			assert o.getObjectProperty("location").getValue() == ""
			//mac & id
			assert o.getObjectProperty("mac").getValue() == Long.parseLong("01acc6eb130000d1", 16)
			assert o.getObjectProperty("id").getValue() == Long.parseLong("F618", 16)
			//address
			assert o.getObjectProperty("address").getValue() == ""
			//protocol
			assert o.getObjectProperty("protocol").getValue() == "TCP_RG2"
			//securityMode
			assert o.getObjectProperty("securityMode").getValue() == "None"
			//key
			assert o.getObjectProperty("key").getValue() == ""
			//status
			assert o.getObjectProperty("status").getValue() == 1
			//x & y
			assert o.getObjectProperty("x").getValue() == 0
			assert o.getObjectProperty("y").getValue() == 0
		}
	}


	@Test
	public void WSNObjectModelTest() {
		//println acceptableNames
		Set<String> componentTypes = cmodel.listComponentTypes()
		for (String t : componentTypes.sort()) {
			if (typesToIgnore.contains(t)) {
				continue
			}
			println "testing $t"
			DLComponent rack = cmodel.newComponent(t)
			if (rack.listMacIdProperties().size() == 0) {
				//not a wsn-based node
				continue
			}
			println "testing $t"
			rack.setPropertyValue('name', RACKNAME)
			//rack.setPropertyValue('mac_id', "01acc6eb130000d1" )
			rack.listMacIdProperties().each { midp ->
				//println "setting macid prop '$midp'"
				rack.setPropertyValue(midp, "01acc6eb130000d1")
			}
			DeploymentLab.Examinations.WSNExamination.checkNodes(rack)
		}
	}



	@AfterClass
	static void afterClass() {
		System.gc();
		System.gc();
		println "All WSN Model Tests Completed"
	}


}
