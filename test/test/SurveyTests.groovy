/**
 * Test wrapper to handle surveys of the codebase
 * @author Gabriel Helman
 * @since Mars
 * Date: 2/14/11
 * Time: 10:07 AM
 *
 */

package test

import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test

import DeploymentLab.Model.*
import DeploymentLab.SelectModel
import DeploymentLab.TestUtils

class SurveyTests {

	static final String RACKNAME = "RACKNAME"


	static ObjectModel omodel
	static ComponentModel cmodel
	static SelectModel selectModel

	/**
	 * Constructs a base model we can use to do tests on
	 */
	@BeforeClass
	static void beforeClass() {
		(omodel,cmodel) = TestUtils.initModels()
	}


	@Test
	public void lookForChildren() {
		Set<String> componentTypes = cmodel.listComponentTypes()
		for (String t: componentTypes.sort()) {
			DLComponent rack = cmodel.newComponent(t)
			DeploymentLab.Examinations.ObjectExamination.lookForChildren(rack)
		}
	}

	@Test
	public void lookForUniqueSensorNames() {
		println "***Sensor Names***"
		Set<String> componentTypes = cmodel.listComponentTypes()
		for (String t: componentTypes.sort()) {
			DLComponent rack = cmodel.newComponent(t)
			DeploymentLab.Examinations.ComponentExaminations.lookForSensorNames(rack)
		}
	}

	@Test
	public void lookForModbusNames() {
		println "***Modbus Names***"
		Set<String> componentTypes = cmodel.listComponentTypes()
		for (String t: componentTypes.sort()) {
			DLComponent rack = cmodel.newComponent(t)
			DeploymentLab.Examinations.ComponentExaminations.lookForModbusNames(rack)
		}
	}


	//@Test
	public void thingsThatCanHaveCoins(){
		Set<String> ignoreMe = new HashSet<String>()
		ignoreMe.add("displaypoint")
		ignoreMe.add("producerpoint")
		ignoreMe.add("power_rack")


		Set<String> temp = new HashSet<String>()
		Set<String> strat = new HashSet<String>()
		Map<String,String> objectTypes = new HashMap<String,String>()

		Set<String> componentTypes = cmodel.listComponentTypes()
		for (String t: componentTypes.sort()) {
			DLComponent rack = cmodel.newComponent(t)
			int coinslots = 0
			coinslots += rack.producersOfType("temperature").size()
			coinslots += rack.producersOfType("temperature_mid").size()
			coinslots += rack.producersOfType("temperature_bot").size()
			if(coinslots > 0){
				temp.add(t)
			}
			if(coinslots >=2 ){
				strat.add(t)
			}

			//find object types
			def objs = ""
			rack.getManagedObjects().each{ o ->
				if (!o.getType().contains("wsn") && !ignoreMe.contains(o.getType())){
					objs += o.getType().toUpperCase() + " "
				}
			}
			objectTypes[t] = objs

		}
		println strat.sort()
		println temp.sort()
		println (temp.minus(strat).sort())
		temp.sort().each{ t ->
			def hasStrat = "no"
			if( strat.contains(t) ){
				hasStrat = "yes"
			}
			//typename, display name, supports single temp, supports strat, obj types
			println "$t, ${cmodel.getTypeDisplayName(t)}, yes, $hasStrat, no, ${objectTypes[t]}"
		}

		//now, with pressure!
		println "pressure2, ${cmodel.getTypeDisplayName("pressure2")}, no, no, yes, ${objectTypes["pressure2"]}"

	}


	//@Test
	public void everyObjectWithALastValue(){
		def results = []
		omodel.getTypes().each{ t ->
			DLObject o = omodel.createObject(t)
			if(o.hasObjectProperty("lastValue")){
				results += t.toUpperCase()
			}
		}

		results = results.sort()
		results.each{ println it }
	}


	@AfterClass
	static void afterClass() {
		//System.gc();
		//System.gc();
		println "All Surveys Completed"
	}


}
