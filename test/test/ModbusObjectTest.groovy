package test

import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test

import DeploymentLab.Model.*
import DeploymentLab.TestUtils

/**
 * Created by IntelliJ IDEA.
 * User: ghelman
 * Date: 4/4/11
 * Time: 4:02 PM
 * To change this template use File | Settings | File Templates.
 */
class ModbusObjectTest {

	static final String RACKNAME = "RACKNAME"
	static List<String> typesToIgnore = []

	static ObjectModel omodel
	static ComponentModel cmodel

	/**
	 * Constructs a base model we can use to do tests on
	 */
	@BeforeClass
	static void beforeClass() {
		(omodel,cmodel) = TestUtils.initModels()
	}


	@Test
	public void WSNObjectModelTest(){
		//println acceptableNames

		Set<String> componentTypes = cmodel.listComponentTypes()
		for ( String t : componentTypes.sort() ){

			//println "should we test $t ?"

			//if ( typesToIgnore.contains(t) ){
			//	continue
			//}

			DLComponent mod = cmodel.newComponent(t)


			//println   mod.getManagedObjectsOfType("modbusproperty")
			if (  ! mod.hasManagedObjectOfType("modbusproperty") && !mod.hasManagedObjectOfType("modbusdevice")  ){
				//not a modbus-based device
				continue
			}

			//println "testing $t"
			mod.setPropertyValue('name', RACKNAME)

			///checkNodes(rack)
			DeploymentLab.Examinations.WSNExamination.checkModbus(mod)


		}

	}




	@AfterClass
	static void afterClass() {
		System.gc();
		System.gc();
		println "All Modbus Model Tests Completed"
	}

}
