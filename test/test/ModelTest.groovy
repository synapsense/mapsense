
package test

import DeploymentLab.CentralCatalogue
import org.apache.commons.jxpath.JXPathContext

import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Ignore
import org.junit.Test

import DeploymentLab.Model.*
import DeploymentLab.TestUtils

class ModelTest {
	static ObjectModel omodel
	static ComponentModel cmodel

	@BeforeClass
	static void beforeClass() {
		(omodel,cmodel) = TestUtils.initModels()
	}


	@Test
	void dlidCreate() {
		Dlid id1 = omodel.getDlid('DLID:-1')
		assert id1.equals(omodel.getDlid(id1.toString()))

		Dlid id3 = omodel.getDlid('DLID:123')
		assert id3.toString() == 'DLID:123'

		HashMap map = new HashMap()
		map.put(id1, '1')
		map.put(id3, '3')

		Dlid testid = omodel.getDlid('DLID:123')
		println "testid: '$testid'"
		assert id3 == testid
		assert id3.equals(testid)
		assert id3.hashCode() == testid.hashCode()
		println map
		println "test lookup (should be '3') : " + map[testid]
		println "test lookup (should be '3') : " + map.get(testid)
		println "test lookup (should be '3') : " + map[id3]
		println "test lookup (should be '3') : " + map.get(id3)
		map.put(testid, '4')
		println map
		//println map.getClass().getName()
	}


	@Test
	void typesLoaded() {
		// .types is an implicit call to .getTypes()
		println 'omodel.types: ' + omodel.types
		//assert omodel.types == ["location", "datacenter", "room", "wsn-network", "gateway", "node", "node-sensepoint", "dataclass", "modbus-network", "modbus-device", "modbus-register", "modbus-coil", "snmp-network", "snmp-device", "snmp-sensepoint", "rack", "crah"]


	}

	@Test
	void objectProperties() {
		DLObject network = omodel.createObject('wsnnetwork')
		println 'network.listProperties(): ' + network.listProperties()
		//assert network.listProperties() == ["name", "panid", "nodes", "gateways"]
        }

	@Test
	void bigFatNotARealUnitTestButIntegrationTest() {
		DLComponent network = cmodel.newComponent('network')
		println "component network.listProperties(): " + network.listProperties()
		network.setPropertyValue('name', 'network1')

		DLComponent room = cmodel.newComponent('room')

		DLComponent rack = cmodel.newComponent('rack-control-rearexhaust-nsf-rt-thermanode')
		rack.setPropertyValue('name', 'new rack name')
		rack.setPropertyValue('x', 200)
		rack.setPropertyValue('y', 100)
		println "Rack name : " + rack.getObject('rack').getObjectProperty('name')

		println "Rack producers: ${rack.listProducers()}"
		println "Rack consumers: ${rack.listConsumers()}"

		DLComponent crah = cmodel.newComponent("crah-thermanode")
		crah.setPropertyValue('name', 'new crah')

		println "Crah producers: ${crah.listProducers()}"
		println "Crah consumers: ${crah.listConsumers()}"
		assert rack.isAssociatiable()
		assert crah.isAssociatiable()
		rack.associate( "reftemp", crah, "reftemp" )
		assert rack.getAllAssociatedComponents().contains(crah)
		assert crah.getAllAssociatedComponents().contains(rack)
		println "Crah reftep successfully associated with rack."
		rack.unassociate( "reftemp", crah, "reftemp" )
		assert !rack.getAllAssociatedComponents().contains(crah)
		assert !crah.getAllAssociatedComponents().contains(rack)

		CentralCatalogue.getSelectModel().getActiveDrawing().addChild( network )
		CentralCatalogue.getSelectModel().getActiveDrawing().addChild( room )

		room.addChild(rack)
		room.addChild(crah)
		network.addChild(rack)
		network.addChild(crah)

		println "crah owns:" + crah.getRoots().collect{ it.getDlid() }

		def sw = new StringWriter()
		def xml = new groovy.xml.MarkupBuilder(sw)
		omodel.serialize(xml)
		//println "Serialized model: " + sw

		sw = new StringWriter()
		xml = new groovy.xml.MarkupBuilder(sw)
		cmodel.serialize(xml)
		//println "Serialized components: " + sw

		println "Children of network: " + network.getChildComponents().collect{it.getName()}
		println "Children of room: " + room.getChildComponents().collect{it.getName() }

		println "Components by type 'network' : " + cmodel.getComponentsByType('network').collect{it.getPropertyValue("name")}
		println "Components by type 'rack' : " + cmodel.getComponentsByTypePrefix('rack').collect{it.getPropertyValue("name")}
	}

	@Test
	/**
	 * Adds and removes a COIN, checking to see if the "isControl" status is correct.
	 */
	void basicControlTest(){
		assert !cmodel.isControl()
		def crac = cmodel.newComponent('crah-single-plenumrated-thermanode')

		// Need to fake the inherited control properties.
		crac.setPropertyValue( RoomType._PROPERTY, RoomType.SLAB )
		crac.setPropertyValue( RoomControlType._PROPERTY, RoomControlType.REMOTE )

		assert !cmodel.isControl()

		crac.setPropertyValue( 'controlDeviceTemp', 'liebert' )
		assert cmodel.isControl()

		crac.setPropertyValue( 'controlDeviceTemp', 'none' )
		assert !cmodel.isControl()

		crac.setPropertyValue( 'controlDeviceTemp', 'liebert' )
		assert cmodel.isControl()

		cmodel.remove(crac)
		assert !cmodel.isControl()

		cmodel.undelete(crac)
		assert cmodel.isControl()

		cmodel.remove(crac)
		assert !cmodel.isControl()
	}

	@Test
	void calcTest(){
		//add some things to search for
		def group = cmodel.newComponent("logicalzone")
		def sum = cmodel.newComponent("sum_operation")
		def user = cmodel.newComponent("userinput")

		CentralCatalogue.getSelectModel().getActiveDrawing().addChild( group )

		assert group.canChild(sum)
		group.addChild(sum)
		group.addChild(user)

		assert sum.getNumberOfParentContainers() == 1
		assert sum.descendentOf(group)



	}

	@Test
	/**
	 * Exercises the spotlight search.
	 */
	void spotlightTest(){
		def sum = cmodel.newComponent("sum_operation")
		def user = cmodel.newComponent("userinput")
		user.setPropertyValue("name", "Ultimate Answer")
		user.setPropertyValue("value", "42")

		def results = cmodel.spotlight("42", "", false, [] )
		def comps = results.collect{ it["component"] }
		assert comps.contains(user)

		results = cmodel.spotlight("42", "badfieldname", false, [] )
		comps = results.collect{ it["component"] }
		assert !comps.contains(user)

		results = cmodel.spotlight("42", "value", false, [] )
		comps = results.collect{ it["component"] }
		assert comps.contains(user)

		def fakeSelection = cmodel.getComponents().tail()
		results = cmodel.spotlight("42", "value", false, fakeSelection )
		comps = results.collect{ it["component"] }
		assert comps.contains(user)

		def allPropertyNames = cmodel.getPropertyNames(fakeSelection).collect{ it.getInternal() }
		//println allPropertyNames
		assert allPropertyNames.contains("name")
		assert allPropertyNames.contains("value")
		assert !allPropertyNames.contains("opM") //nothing in the selection with a visible opM property

		def math = cmodel.newComponent("math_operation")
		allPropertyNames = cmodel.getPropertyNames(fakeSelection).collect{ it.getInternal() }
		assert !allPropertyNames.contains("opM") //math has a visible opM, but it's not in the selection

		fakeSelection = cmodel.getComponents().tail() //reselect
		allPropertyNames = cmodel.getPropertyNames(fakeSelection).collect{ it.getInternal() }
		assert allPropertyNames.contains("opM") //the math component, and therefore its opM are now in the list of visible props

	}


	@Test
	void componentEditorTest(){
		//settings
		DLComponent sum = cmodel.newComponent("average_operation")
		Setting name = (Setting) sum.getObject("math").getObjectProperty("name")

		assert name.getValue().equals( "Average Operation" )
		name.setValue("Default Name")
		assert name.getValue().equals( "Default Name" )
		name.setOverride("OVERIDE")
		assert name.getValue().equals( "OVERIDE" )
		assert name.getOverride().equals( "OVERIDE" )
		assert name.getRealValue().equals("Default Name")
		name.setValue("Other Name")
		assert name.getValue().equals( "OVERIDE" )
		name.setOverride(null)
		assert name.getValue().equals( "Other Name" )

		assert name.valueChanged()
		name.resetValue()
		assert !name.valueChanged()
		name.setOverride("over")
		assert name.valueChanged()
		name.resetValue()
		assert !name.valueChanged()
		name.setOverride(null)
		assert name.valueChanged()
		name.resetValue()
		assert ! name.valueChanged()


		//tags

		DLComponent WSN1 = cmodel.newComponent("network")
		Property wsn_name = WSN1.getObject("network").getObjectProperty("name")
		assert wsn_name.hasTags()
		assert wsn_name.hasTag("pluginId")
		assert wsn_name.getTag("pluginId").getValue() == "wsn"

		CustomTag pid = wsn_name.getTag("pluginId")

		pid.setValue("modbus")
		assert pid.getValue().equals("modbus")
		pid.setOverride("sim")
		assert pid.getOverride().equals("sim")
		assert pid.getValue().equals("sim")
		assert pid.getRealValue().equals("modbus")

		pid.setValue("wsn")
		assert pid.getValue().equals("sim")
		pid.setOverride(null)
		assert wsn_name.getTag("pluginId").getValue() == "wsn"

		assert pid.valueChanged()
		pid.resetValue()
		assert !pid.valueChanged()
		pid.setOverride("over")
		assert pid.valueChanged()
		pid.resetValue()
		assert !pid.valueChanged()
		pid.setOverride(null)
		assert pid.valueChanged()
		pid.resetValue()
		assert ! pid.valueChanged()


	}

	@Test
	void customTagsTest(){

		//these things all have tags
		DLComponent WSN1 = cmodel.newComponent("network")
		Property wsn_name = WSN1.getObject("network").getObjectProperty("name")

		DLComponent modbus = cmodel.newComponent("modbus-tcp-network")
		Property mod_name = modbus.getObject("modbusnetwork").getObjectProperty("name")

		DLComponent ion = cmodel.newComponent("ion6200-wye")
		Property ion_name = ion.getObject("ionmeter").getObjectProperty("name")
		Property register = ion.getObject("ionmeter").getChildren().first().getObjectProperty("lastValue")

		//and this one has a tag on a grule - new for MARS
		DLComponent reporter = cmodel.newComponent("pue_main")
		Property pue = reporter.getObject("pue").getObjectProperty("pue")

		//this does not
		DLComponent sum = cmodel.newComponent("sum_operation")
		Property sum_name = sum.getObject("math").getObjectProperty("name")

		assert wsn_name.hasTags()
		assert mod_name.hasTags()
		assert ! ion_name.hasTags()
		assert register.hasTags()
		assert ! sum_name.hasTags()
		assert pue.hasTags()

		assert wsn_name.hasTag("pluginId")
		assert mod_name.hasTag("pluginId")
		assert ! ion_name.hasTag("pluginId")
		assert register.hasTag("poll")
		assert register.hasTag("elConvGet")
		assert pue.hasTag("aMax")

		assert wsn_name.getTags().size() == 1
		assert mod_name.getTags().size() == 1
		assert register.getTags().size() == 2
		assert pue.getTags().size() == 2

		assert wsn_name.getTag("pluginId").getValue() == "wsn"
		assert wsn_name.getTag("pluginId").getTagName() == "pluginId"
		assert wsn_name.getTag("pluginId").getTagType() == "java.lang.String"
		assert pue.getTag("aMax").getTagType() == "java.lang.Double"

		assert register.getTag("poll").getTagType() == "java.lang.Long"
		assert register.getTag("poll").getValue() == 300000
		//println register.printTags()
		assert register.printTags().length() > 0
		assert sum_name.printTags().length() == 0
		assert pue.getTag("aMax").getValue() == 2.5

		//add
		sum_name.addTag("Fake Tag", "java.lang.String", "TAGS ARE AWESOME")
		assert sum_name.hasTags()
		sum_name.addTag(  new CustomTag( sum_name, "another fake tag", "java.lang.Integer", 42 ) )
		assert sum_name.getTags().size() == 2
		assert sum_name.getTag("Fake Tag").getValue() == "TAGS ARE AWESOME"

		//remove & update
		sum_name.removeTag("Fake Tag")
		assert sum_name.getTags().size() == 1
		assert sum_name.getTag("Fake Tag") == null
		assert sum_name.getTag("another fake tag").getValue() == 42
		sum_name.updateTag("another fake tag", 13, 0)
		assert sum_name.getTag("another fake tag").getValue() != 42
		assert sum_name.getTag("another fake tag").getValue() == 13


	}

	@AfterClass
	static void afterClass() {
		cmodel = null
		omodel = null
		System.gc();
		System.gc();
		println "All Model Tests Completed"
	}
}

