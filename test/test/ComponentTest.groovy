package test

import DeploymentLab.CentralCatalogue
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test


import DeploymentLab.Model.*
import org.junit.Before
import org.junit.After
import DeploymentLab.SceneGraph.StaticChildrenNode
import DeploymentLab.SelectModel
import DeploymentLab.UndoBuffer
import DeploymentLab.DisplayProperties
import DeploymentLab.TestUtils

/**
 *
 * @author Gabriel Helman
 * @since
 * Date: 4/25/11
 * Time: 10:52 AM
 */
class ComponentTest {
	static ObjectModel omodel
	static ComponentModel cmodel
	static SelectModel selectModel
	static UndoBuffer ub
	static DisplayProperties p

	/**
	 * Constructs a base model we can use to do tests on
	 */
	@Before
	void before() {
		(omodel,cmodel) = TestUtils.initModels()
		selectModel = CentralCatalogue.getSelectModel()
		ub = CentralCatalogue.getUndoBuffer()
		p = new DisplayProperties( selectModel )
	}

	@After
	void afterClass() {
		omodel = null
		cmodel = null
		ub = null
		selectModel = null
		p = null
	}

	@AfterClass
	static void allDone(){
		println "All Rotation Tests Completed"
	}

	private final static double EPSILON = 0.00001; //this is a decent epsilon factor for IEEE doubles
	/**
	 * Compares two IEEE doubles for equality via an epsilon factor.
	 * @param first the first double to compare
	 * @param second the second double to compare
	 * @return true if first and second differ by less than Epsilon, false if they differ by more than Epsilon
	 */
	private static boolean epsilonEquals(double first, double second) {
		if (Math.abs(first - second) > EPSILON) {
			return false
		}
		return true
	}

	/**
	 * Test that sensor & rack rotation for hrows work like we want them to.
	 *
	 */
	@Test
	void hrowRotation() {

		DLComponent rowLite = cmodel.newComponent("dual-horizontal-row-thermanode2")
		
		//NEW FOR Mars; turns out hrows don't work without a scenegraph node
		//BOOOOOOOO
		def sceneNode = new StaticChildrenNode(rowLite,p,cmodel,selectModel,ub)

		//make sure we start where we think we're starting:
		assert rowLite.getPropertyValue("rotation") == 0
		assert rowLite.getPropertyValue("x") == 0
		assert rowLite.getPropertyValue("y") == 0
		assert rowLite.getPropertyValue("rack1_offset") == -216.0
		assert rowLite.getPropertyValue("rack2_offset") == -144.0
		assert rowLite.getPropertyValue("rack3_offset") == -72.0
		assert rowLite.getPropertyValue("rack4_offset") == 72.0
		assert rowLite.getPropertyValue("rack5_offset") == 144.0
		assert rowLite.getPropertyValue("rack6_offset") == 216.0

		def width = rowLite.getPropertyValue("width")
		def depth = rowLite.getPropertyValue("depth")
		assert width == 30.0
		assert depth == 30.0
		def half = depth / 2
		
		
		println rowLite.listRootNames()
		println rowLite.getManagedObjects()
		println rowLite.getPlaceableChildrenMap()
		println rowLite.getChildComponents()
		rowLite.getChildComponents().each{ c ->
			println c
			println c.getPropertyStringValue("child_id")
		}

		//at zero rotation, we're perfectly vertical, rack 1 to the top

		def rack1 = rowLite.getPlaceableChild("rack1")
		//println rack1.mapPropertyNamesValues(true,false)
		def orack1 = rack1.getObject("rack")
		//println orack1.mapPropertyNamesValues()

		def rack2 = rowLite.getPlaceableChild("rack2")
		def orack2 = rack2.getObject("rack")
		def rack3 = rowLite.getPlaceableChild("rack3")
		def orack3 = rack3.getObject("rack")
		def rack4 = rowLite.getPlaceableChild("rack4")
		def orack4 = rack4.getObject("rack")
		def rack5 = rowLite.getPlaceableChild("rack5")
		def orack5 = rack5.getObject("rack")
		def rack6 = rowLite.getPlaceableChild("rack6")
		def orack6 = rack6.getObject("rack")

		def rackObjects = []
		rackObjects += orack1
		rackObjects += orack2
		rackObjects += orack3
		rackObjects += orack4
		rackObjects += orack5
		rackObjects += orack6

		assert orack1.getObjectProperty("x").getValue() == 0
		assert orack2.getObjectProperty("x").getValue() == 0
		assert orack3.getObjectProperty("x").getValue() == 0
		assert orack4.getObjectProperty("x").getValue() == 0
		assert orack5.getObjectProperty("x").getValue() == 0
		assert orack6.getObjectProperty("x").getValue() == 0

		assert orack1.getObjectProperty("y").getValue() == -216.0
		assert orack2.getObjectProperty("y").getValue() == -144.0
		assert orack3.getObjectProperty("y").getValue() == -72.0
		assert orack4.getObjectProperty("y").getValue() == 72.0
		assert orack5.getObjectProperty("y").getValue() == 144.0
		assert orack6.getObjectProperty("y").getValue() == 216.0

		//check node & internal sensors
		def intake = rowLite.getObject("intake_node")
		def exhaust = rowLite.getObject("exhaust_node")
		assert intake.getObjectProperty("x").getValue() == half
		assert intake.getObjectProperty("y").getValue() == 0
		assert exhaust.getObjectProperty("x").getValue() == -half
		assert exhaust.getObjectProperty("y").getValue() == 0

		intake.getChildren().each { o ->
			def ch = o.getObjectProperty("channel").getValue()
			if (ch == 0 || ch == 1 || ch == 2) {
				assert o.getObjectProperty("x").getValue() == intake.getObjectProperty("x").getValue()
				assert o.getObjectProperty("y").getValue() == intake.getObjectProperty("y").getValue()
			}
		}
		exhaust.getChildren().each { o ->
			def ch = o.getObjectProperty("channel").getValue()
			if (ch == 0 || ch == 1 || ch == 2) {
				assert o.getObjectProperty("x").getValue() == exhaust.getObjectProperty("x").getValue()
				assert o.getObjectProperty("y").getValue() == exhaust.getObjectProperty("y").getValue()
			}
		}

		//NOW, having done all that, spin the rack in a circle and make sure it still all works:
		for (int degrees = 0; degrees < 360; degrees++) {
			println "at rotation $degrees deg"
			rowLite.setPropertyValue("rotation", degrees)

			assert epsilonEquals(intake.getObjectProperty("x").getValue(), (depth / 2 * Math.cos(Math.toRadians(-degrees))))
			assert epsilonEquals(intake.getObjectProperty("y").getValue(), (depth / 2 * Math.sin(Math.toRadians(-degrees))))

			assert epsilonEquals(exhaust.getObjectProperty("x").getValue(), -(depth / 2 * Math.cos(Math.toRadians(-degrees))))
			assert epsilonEquals(exhaust.getObjectProperty("y").getValue(), -(depth / 2 * Math.sin(Math.toRadians(-degrees))))

			for(DLObject o : intake.getChildren()){
				println "intake child $o"
				def ch = o.getObjectProperty("channel").getValue()
				if (ch == 0 || ch == 1 || ch == 2) {
					assert o.getObjectProperty("x").getValue() == intake.getObjectProperty("x").getValue()
					assert o.getObjectProperty("y").getValue() == intake.getObjectProperty("y").getValue()
				}
			}
			for(DLObject o : exhaust.getChildren()){
				println "exhaust child $o"
				def ch = o.getObjectProperty("channel").getValue()
				if (ch == 0 || ch == 1 || ch == 2) {
					assert o.getObjectProperty("x").getValue() == exhaust.getObjectProperty("x").getValue()
					assert o.getObjectProperty("y").getValue() == exhaust.getObjectProperty("y").getValue()
				}
			}

			//each rack:
			for (int rack = 1; rack < 7; rack++) {
				println "testing rack $rack at rotation $degrees"
				//def currentRack = rowLite.getObject("rack$rack")

				DLObject currentRack = rackObjects[rack-1]

				println "$currentRack ${currentRack.getName()}"
				def rackX = currentRack.getObjectProperty("x").getValue()
				def rackY = currentRack.getObjectProperty("y").getValue()

				def x = -(rowLite.getPropertyValue("rack${rack}_offset") * Math.sin(Math.toRadians(-degrees)))
				def y = (rowLite.getPropertyValue("rack${rack}_offset") * Math.cos(Math.toRadians(-degrees)))
				assert epsilonEquals(rackX, x)
				assert epsilonEquals(rackY, y)


				//check that the correct sensor is in the right place

				//find the right channel for the new layout:
				int channel
				if(rack == 3){
					channel = 3
				}else if (rack == 1){
					channel = 5
				} else {
					channel = rack +2
				}

				def intakeSensor = rowLite.getObject("intake_node").getChildren().find { it.getObjectProperty("channel").getValue() == channel }
				def exhaustSensor = rowLite.getObject("exhaust_node").getChildren().find { it.getObjectProperty("channel").getValue() == channel }

				//confirm they're in the same place; the sensors should be on the correct "side" of the rack
				def iX = intakeSensor.getObjectProperty("x").getValue()
				def iY = intakeSensor.getObjectProperty("y").getValue()
				//compute location from RL center
				def sensorX = (-(rowLite.getPropertyValue("rack${rack}_offset") * Math.sin(Math.toRadians(-degrees)))) + (depth/2 * Math.cos(Math.toRadians(-degrees)))
				def sensorY = (rowLite.getPropertyValue("rack${rack}_offset") * Math.cos(Math.toRadians(-degrees))) + (depth/2 * Math.sin(Math.toRadians(-degrees)))
				assert epsilonEquals(iX, sensorX)
				assert epsilonEquals(iY, sensorY)

				//do the same for the exhaust side
				def eX = exhaustSensor.getObjectProperty("x").getValue()
				def eY = exhaustSensor.getObjectProperty("y").getValue()
				def computedExhaustSensorX = (-(rowLite.getPropertyValue("rack${rack}_offset") * Math.sin(Math.toRadians(-degrees)))) - (depth/2 * Math.cos(Math.toRadians(-degrees)))
				def computedExhaustSensorY = (rowLite.getPropertyValue("rack${rack}_offset") * Math.cos(Math.toRadians(-degrees))) - (depth/2 * Math.sin(Math.toRadians(-degrees)))
				assert epsilonEquals(eX, computedExhaustSensorX)
				assert epsilonEquals(eY, computedExhaustSensorY)
			}
		}
	}
}
