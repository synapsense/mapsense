ruleset {

    description '''
        DeploymentLab CodeNarc RuleSet specification. Based on the rules available in CodeNarc v0.16.1
        When modifying this file, add a comment explaining why a rule was disabled or parameter changed. Priority
        changes do not need a comment since it does not affect the processing of the rule. The rule is still applied
        and reported, only the classification changes.
    '''

    // rulesets/basic.xml
    AssignmentInConditional(priority:1)
    BigDecimalInstantiation
    BitwiseOperatorInConditional(priority:1)
    BooleanGetBoolean
    BrokenOddnessCheck
    ClassForName
    ComparisonOfTwoConstants
    ComparisonWithSelf
    ConstantIfExpression
    ConstantTernaryExpression
    DeadCode
    DoubleNegative
    DuplicateCaseStatement(priority:1)
    DuplicateMapKey(priority:1)
    DuplicateSetValue(priority:1)
    EmptyCatchBlock
    EmptyElseBlock
    EmptyFinallyBlock
    EmptyForStatement
    EmptyIfStatement
    EmptyInstanceInitializer
    EmptyMethod
    EmptyStaticInitializer
    EmptySwitchStatement
    EmptySynchronizedStatement
    EmptyTryBlock
    EmptyWhileStatement
    EqualsAndHashCode
    EqualsOverloaded
    ExplicitGarbageCollection
    ForLoopShouldBeWhileLoop
    HardCodedWindowsFileSeparator
    HardCodedWindowsRootDirectory
    IntegerGetInteger
    RandomDoubleCoercedToZero(priority:1)
    RemoveAllOnSelf(priority:1)
    ReturnFromFinallyBlock
    ThrowExceptionFromFinallyBlock

    // rulesets/braces.xml
    ElseBlockBraces
    ForStatementBraces
    IfStatementBraces
    WhileStatementBraces

    // rulesets/concurrency.xml
    BusyWait
    DoubleCheckedLocking(priority:1)
    InconsistentPropertyLocking(priority:1)
    InconsistentPropertySynchronization(priority:1)
    NestedSynchronization(priority:1)
    StaticCalendarField(priority:1)
    StaticConnection(priority:1)
    StaticDateFormatField(priority:1)
    StaticMatcherField(priority:1)
    StaticSimpleDateFormatField(priority:1)
    SynchronizedMethod(priority:1)
    SynchronizedOnBoxedPrimitive(priority:1)
    SynchronizedOnGetClass(priority:1)
    SynchronizedOnReentrantLock(priority:1)
    SynchronizedOnString(priority:1)
    SynchronizedOnThis
    SynchronizedReadObjectMethod(priority:1)
    SystemRunFinalizersOnExit(priority:1)
    ThreadGroup(priority:1)
    ThreadLocalNotStaticFinal(priority:1)
    ThreadYield(priority:1)
    UseOfNotifyMethod(priority:1)
    VolatileArrayField(priority:1)
    VolatileLongOrDoubleField
    WaitOutsideOfWhileLoop(priority:1)

    // rulesets/convention.xml
    ConfusingTernary
    CouldBeElvis
    InvertedIfElse
    LongLiteralWithLowerCaseL

    // rulesets/design.xml
    AbstractClassWithPublicConstructor
    AbstractClassWithoutAbstractMethod
    BooleanMethodReturnsNull
    BuilderMethodWithSideEffects
    CloneableWithoutClone
    CloseWithoutCloseable
    CompareToWithoutComparable
    ConstantsOnlyInterface
    EmptyMethodInAbstractClass
    FinalClassWithProtectedMember
    ImplementationAsType
    PublicInstanceField
    ReturnsNullInsteadOfEmptyArray
    ReturnsNullInsteadOfEmptyCollection
    SimpleDateFormatMissingLocale
    StatelessSingleton

    // rulesets/dry.xml -- These are violated beyond reasonable repair. Maybe re-enable and enforce in the future.
    // DuplicateListLiteral
    // DuplicateMapLiteral
    // DuplicateNumberLiteral
    // DuplicateStringLiteral

    // rulesets/exceptions.xml
    CatchArrayIndexOutOfBoundsException
    CatchError
    CatchException
    CatchIllegalMonitorStateException
    CatchIndexOutOfBoundsException
    CatchNullPointerException
    CatchRuntimeException
    CatchThrowable
    ConfusingClassNamedException
    ExceptionExtendsError(priority:1)
    MissingNewInThrowStatement(priority:1)
    ReturnNullFromCatchBlock
    SwallowThreadDeath
    ThrowError(priority:1)
    ThrowException
    ThrowNullPointerException(priority:1)
    ThrowRuntimeException
    ThrowThrowable

    // rulesets/formatting.xml
    BracesForClass
    BracesForForLoop
    BracesForIfElse
    BracesForMethod
    BracesForTryCatchFinally
    ClassJavadoc
    //LineLength -- Violated everywhere. Maybe re-enable and enforce in the future.

    // rulesets/generic.xml
    IllegalClassReference
    IllegalPackageReference
    IllegalRegex
    RequiredRegex
    RequiredString
    StatelessClass

    // rulesets/grails.xml
    GrailsDomainHasEquals
    GrailsDomainHasToString
    GrailsPublicControllerMethod
    GrailsServletContextReference
    GrailsSessionReference
    GrailsStatelessService

    // rulesets/groovyism.xml
    AssignCollectionSort
    AssignCollectionUnique
    ClosureAsLastMethodParameter
    CollectAllIsDeprecated
    ConfusingMultipleReturns(priority:1)
    ExplicitArrayListInstantiation
    ExplicitCallToAndMethod
    ExplicitCallToCompareToMethod
    ExplicitCallToDivMethod
    ExplicitCallToEqualsMethod
    ExplicitCallToGetAtMethod
    ExplicitCallToLeftShiftMethod
    ExplicitCallToMinusMethod
    ExplicitCallToModMethod
    ExplicitCallToMultiplyMethod
    ExplicitCallToOrMethod
    ExplicitCallToPlusMethod
    ExplicitCallToPowerMethod
    ExplicitCallToRightShiftMethod
    ExplicitCallToXorMethod
    ExplicitHashMapInstantiation
    ExplicitHashSetInstantiation
    ExplicitLinkedHashMapInstantiation
    ExplicitLinkedListInstantiation
    ExplicitStackInstantiation
    ExplicitTreeSetInstantiation
    GStringAsMapKey(priority:1)
    GetterMethodCouldBeProperty
    GroovyLangImmutable(priority:1)
    UseCollectMany
    UseCollectNested

    // rulesets/imports.xml
    DuplicateImport
    ImportFromSamePackage
    ImportFromSunPackages
    MisorderedStaticImports
    UnnecessaryGroovyImport
    UnusedImport

    // rulesets/jdbc.xml
    DirectConnectionManagement(priority:1)
    JdbcConnectionReference
    JdbcResultSetReference
    JdbcStatementReference

    // rulesets/junit.xml
    ChainedTest
    CoupledTestCase
    JUnitAssertAlwaysFails
    JUnitAssertAlwaysSucceeds
    JUnitFailWithoutMessage
    JUnitPublicNonTestMethod
    JUnitSetUpCallsSuper
    JUnitStyleAssertions
    JUnitTearDownCallsSuper
    JUnitTestMethodWithoutAssert
    JUnitUnnecessarySetUp
    JUnitUnnecessaryTearDown
    SpockIgnoreRestUsed
    UnnecessaryFail
    UseAssertEqualsInsteadOfAssertTrue
    UseAssertFalseInsteadOfNegation
    UseAssertNullInsteadOfAssertEquals
    UseAssertSameInsteadOfAssertTrue
    UseAssertTrueInsteadOfAssertEquals
    UseAssertTrueInsteadOfNegation

    // rulesets/logging.xml
    LoggerForDifferentClass(priority:1)
    LoggerWithWrongModifiers
    LoggingSwallowsStacktrace
    MultipleLoggers
    PrintStackTrace
    Println
    SystemErrPrint
    SystemOutPrint

    // rulesets/naming.xml
    AbstractClassName
    ClassName
    ConfusingMethodName
    FactoryMethodName
    FieldName
    InterfaceName
    MethodName
    ObjectOverrideMisspelledMethodName
    //PackageName -- Current style violates this. No reason to check it when it is intentional.
    ParameterName
    PropertyName
    VariableName

    // rulesets/security.xml
    //FileCreateTempFile -- Many violations and not really an issue in our case.
    InsecureRandom
    //JavaIoPackageAccess -- Many violations and not really an issue in our case.
    NonFinalPublicField
    NonFinalSubclassOfSensitiveInterface(priority:1)
    ObjectFinalize(priority:1)
    PublicFinalizeMethod(priority:1)
    SystemExit
    UnsafeArrayDeclaration

    // rulesets/serialization.xml
    SerialPersistentFields(priority:1)
    SerialVersionUID(priority:1)
    SerializableClassMustDefineSerialVersionUID(priority:1)

    // rulesets/size.xml
    AbcComplexity {
        // Unit tests tend to rank high even if they aren't very complex. I think it's the way asserts are counted.
        ignoreMethodNames = "test*"
    }
    ClassSize
    CyclomaticComplexity
    MethodCount
    MethodSize
    NestedBlockDepth

    // rulesets/unnecessary.xml
    AddEmptyString
    ConsecutiveLiteralAppends
    ConsecutiveStringConcatenation
    UnnecessaryBigDecimalInstantiation
    UnnecessaryBigIntegerInstantiation
    UnnecessaryBooleanExpression
    UnnecessaryBooleanInstantiation
    UnnecessaryCallForLastElement
    UnnecessaryCallToSubstring
    UnnecessaryCatchBlock
    UnnecessaryCollectCall
    UnnecessaryCollectionCall
    UnnecessaryConstructor
    // UnnecessaryDefInFieldDeclaration -- Ignore this since keeping def is sometimes a readabilty decision.
    // UnnecessaryDefInMethodDeclaration -- Ignore this since keeping def is sometimes a readabilty decision.
    // UnnecessaryDefInVariableDeclaration -- Ignore this since keeping def is sometimes a readabilty decision.
    UnnecessaryDotClass
    UnnecessaryDoubleInstantiation
    UnnecessaryElseStatement
    UnnecessaryFinalOnPrivateMethod
    UnnecessaryFloatInstantiation
    // UnnecessaryGString -- Violated all over and not really a problem, not even a performance one.
    // UnnecessaryGetter -- Violated all over and not really a problem and in some cases having 'get' improves readability.
    UnnecessaryIfStatement
    UnnecessaryInstanceOfCheck
    UnnecessaryInstantiationToGetClass
    UnnecessaryIntegerInstantiation
    UnnecessaryLongInstantiation
    UnnecessaryModOne
    UnnecessaryNullCheck
    UnnecessaryNullCheckBeforeInstanceOf
    UnnecessaryObjectReferences
    UnnecessaryOverridingMethod
    UnnecessaryPackageReference
    UnnecessaryParenthesesForMethodCallWithClosure
    // UnnecessaryPublicModifier -- Ignore this since keeping the modifier is sometimes a readabilty decision.
    // UnnecessaryReturnKeyword -- Not really a bad thing and is actually kinda dangerous. I'd rather check the opposite!
    UnnecessarySelfAssignment
    UnnecessarySemicolon
    UnnecessaryStringInstantiation
    UnnecessarySubstring
    UnnecessaryTernaryExpression
    UnnecessaryTransientModifier

    // rulesets/unused.xml
    UnusedArray
    UnusedMethodParameter
    UnusedObject
    UnusedPrivateField
    UnusedPrivateMethod
    UnusedPrivateMethodParameter
    UnusedVariable
}