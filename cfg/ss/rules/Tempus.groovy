
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;;

/**
	Takes a user defined input and jiggles it randomly within a user defined value.
*/
public class Tempus implements RuleAction {


	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(Tempus.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {

			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			def finalValue = 0.0

			double highval = env.getPropertyValue( nativeTO, "highval", Double.class )
			double lowval = env.getPropertyValue( nativeTO, "lowval", Double.class )
			double jiggle = env.getPropertyValue( nativeTO, "jiggle", Double.class )
			double start = env.getPropertyValue( nativeTO, "start", Double.class )
			double end = env.getPropertyValue( nativeTO, "finish", Double.class )

			double lastValue = env.getPropertyValue( nativeTO, "lastValue", Double.class ) ?: 0
			logger.trace("TEMPUS ${triggeredRule.getName()} last value was $lastValue"  )
			finalValue = lastValue

			def jiggleAmount = Math.random() * jiggle
			logger.trace("TEMPUS ${triggeredRule.getName()} jiggle factor is $jiggleAmount"  )

			Calendar rightNow = Calendar.getInstance();
			def currentHour = rightNow.get(Calendar.HOUR_OF_DAY)

			logger.trace("TEMPUS ${triggeredRule.getName()} current hour is $currentHour"  )

			if ( currentHour == start && lastValue < highval ){
				//time to go up!
				logger.trace("TEMPUS ${triggeredRule.getName()} going up!"  )
				finalValue += jiggleAmount

			} else if (currentHour == end && lastValue > lowval ){
				//time to go down!
				logger.trace("TEMPUS ${triggeredRule.getName()} going down!"  )
				finalValue -= jiggleAmount


			} else {
				//just jiggle a tad
				//this behaves just like a wobulator, except that we cut the entered jiggle factor in half

				jiggleAmount = jiggleAmount / 2
				def baseline = 0.0
				if ( currentHour >= start && currentHour < end ){
					baseline = highval
				} else {
					baseline = lowval
				}

				logger.trace("TEMPUS ${triggeredRule.getName()} just jigglin' around $baseline!"  )

				//roughly alternate up and down jiggle directions.  This keeps our graph looking like a sisemometer at a monster truck rally.
				if (lastValue < baseline) {
					finalValue = lastValue + jiggleAmount
				} else {
					finalValue = lastValue - jiggleAmount
				}

				//adjust to keep it in the implied "wobble zone"
				if (finalValue > baseline + jiggle)
				{
					finalValue = baseline + jiggle
				}

				if (finalValue < baseline - jiggle)
				{
					finalValue = baseline - jiggle
				}


			}


			logger.debug("TEMPUS ${triggeredRule.getName()} results in $finalValue"  )
			return (double) finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}
