
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;
public class Random implements RuleAction {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(Random.class);
	
	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");
		try {
			//logger.trace("Generating a Random Number.");
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			Double max = env.getPropertyValue(nativeTO, "max", Double.class);
			def currentNumber = Math.random()
			currentNumber = currentNumber * max
			//logger.trace("Random Number was " + currentNumber )
			//logger.trace("Random " + triggeredRule.getName() + "resulted in $currentNumber" )
			return (double) currentNumber
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}