
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;;

/**
	Iterates though a user-defined sequence of values.
*/
public class TestSequence implements RuleAction {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(TestSequence.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			Double finalValue = null
			List values = []
			env.getPropertyValue( nativeTO, "sequence", String.class ).split(",").each{
				if ( it.equalsIgnoreCase("null") ){
					values += null
				} else {
					values += Double.parseDouble(it)
				}
			}
			logger.trace(" sequence is $values ")
			List statuses = []
			String statusStr = env.getPropertyValue( nativeTO, "statusSequence", String.class )
			if(statusStr){
				statusStr.split(",").each{
					if ( it.equalsIgnoreCase("null") ){
						statuses += null
					} else {
						statuses += Double.parseDouble(it)
					}
				}
			}
			logger.trace("status sequence is $statuses ")
			//storage should store index, not value
			Integer currentIndex = (Integer) env.getPropertyValue( nativeTO, "storage", Double.class ) ?: 0
			if(currentIndex >= values.size()){
				currentIndex=0
			}
			finalValue = values[currentIndex]
			if(statuses.size() > currentIndex){
				env.setPropertyValue(nativeTO, "status", statuses[currentIndex])
			} else {
				env.setPropertyValue(nativeTO, "status", 1)
			}
			env.setPropertyValue(nativeTO, "storage", (currentIndex+1))
			//logger.debug("sequence has idx $currentIndex , returning $finalValue from $values")
			return finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}
