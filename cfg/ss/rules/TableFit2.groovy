package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import java.awt.geom.Point2D;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO
import com.synapsense.exception.PropertyNotFoundException;

/**

*/
public class TableFit2 implements RuleAction {

	private Property inputPower
	public Property getInputPower() {
		return inputPower
	}
    public void setInputPower(Property i) {
		inputPower = i
	}

	private Property table
	public Property getTable() {
		return table
	}
    public void setTable(Property i) {
		table = i
	}

	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(TableFit2.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			//check for deactivated status
			try{
				Integer status = (Integer) env.getPropertyValue(nativeTO, "status", Integer.class)
				if ( status >= 2 ){
					return null
				}
			} catch(PropertyNotFoundException pnfe){
				logger.trace("This lookup table does not have a status property.")
			}

			def finalValue = 0
			String tableString = (String) table.getValue()
/*
			double powerValue = 0
			int inputCount = 0
			for(TO<?> t : inputPower.getValue()) {
				powerValue += (double)(env.getPropertyValue(t, "lastValue", Double.class) ?: 0)
				//logger.trace("${triggeredRule.getName()} -->  ${env.getPropertyValue(t, "lastValue", Double.class)}" )
				inputCount += 1
			}
*/
			if ( inputPower == null || inputPower.getValue() == null ){
				logger.info("Rule '${triggeredRule.getName()}', property '${calculated.getName()}' has null input.");
				return null;
			}

			Double powerValue = env.getPropertyValue((TO<?>) inputPower.getValue(), "lastValue", Double.class);
			// validate sensor values are valid
			if ((powerValue == null) || (powerValue < -1000)) {
				logger.info("Rule '${triggeredRule.getName()}', property '${calculated.getName()}' inputs are invalid (" + powerValue +  ") .");
				return null;
			}
			/*
			if ( inputCount == 0 ) {
				//logger.warn("${triggeredRule.getName()} 0 powervalue value in tablefit!" );
				logger.warn("${triggeredRule.getName()} no input values to tablefit!" );
				finalValue = null
			} else
*/
			if ( tableString == null ){
				logger.warn("null table value in ${triggeredRule.getName()}." );
				finalValue = null
			} else {
				def loadPercent = powerValue
				def result = tableFunction( loadPercent, parseTable( tableString ) )
				finalValue = result
			}
			//return the final value
			logger.debug("TABLEFIT ${triggeredRule.getName()} on $powerValue results in $finalValue");
			return (Double) finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}


	//new test table: 0,0;10,50;20,75;30,90;40,95;50,95;60,95;70,95;80,80;90,55;100,25

	//String fitTable = "0.0,0.0;5.0,1.0;10.0,2.0;15.0,3.0";

// a list of Point2D.Doubles, sorted in ascending order of X coordinate

	Double tableFunction(double input, def table) {
		if ( table.size() == 1) {
			logger.debug("Single Value table returning ${table[0].y}")
			return table[0].y
		} else if ( table.size() < 1 ){
			logger.warn("Table with no values returning null")
			return null
		}

		if(input < table[0].x) {
			return input * slope(table[0], table[1]) + intercept(table[0], table[1]);
		}
		if(input > table[-1].x) {
			return input * slope(table[-2], table[-1]) + intercept(table[-2], table[-1]);
		}
		int i = 0
		for(Point2D.Double point: table) {
			if(point.x == input)
				return point.y
			if(point.x > input)
				return input * slope(table[i-1], table[i]) + intercept(table[i-1], table[i])
			i++
		}
		throw new Exception("Programming Error")
	}

	double slope(Point2D.Double a, Point2D.Double b) {
		return (b.y - a.y) / (b.x - a.x)
	}

	double intercept(Point2D.Double a, Point2D.Double b) {
		return a.y - slope(a, b) * a.x
	}

	def parseTable(String sTable) {
		if (sTable.trim().length() == 0 ) {
			return []
		}
		def pairs = sTable.split(';')
		//supports tablestring with empty y-values
		def table = []
		for( String p : pairs ){
			def elems = p.split(',')
			if ( elems.length > 1 && elems[0].isDouble() && elems[1].isDouble() ){
				table += new Point2D.Double(Double.parseDouble(elems[0]), Double.parseDouble(elems[1]))
			}
		}
		table.sort{ a, b -> a.x.compareTo(b.x) }
		logger.debug("TableString $sTable parsed to $table")
		return table
	}
}