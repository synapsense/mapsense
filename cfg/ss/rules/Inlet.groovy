/**
 * A Calculation Graph Inlet; used to import values from a graph in another project file.
 * The inlet goes out and finds the outlet with the correct name
 * @author Gabriel Helman
 * @since Mars
 * Date: 2/16/11
 * Time: 11:29 AM
 */

package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO
import com.synapsense.dto.Alert
import com.synapsense.service.AlertService;
import com.synapsense.util.LocalizationUtils;

public class Inlet implements RuleAction {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(Inlet.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			String outletName = env.getPropertyValue(nativeTO, "outletName", String.class)
			String oldOutletName = env.getPropertyValue(nativeTO, "oldOutletName", String.class)

			TO<?> source = env.getPropertyValue(nativeTO, "source", TO.class)

			Double finalValue = null

			logger.trace("${triggeredRule.getName()} Inlet looking for oulet with the name '$outletName'")

			boolean outletHasChanged = false
			if (!outletName.equals(oldOutletName)) {
				outletHasChanged = true
				logger.trace("${triggeredRule.getName()} Outlet name has changed from '$oldOutletName' to '$outletName'")
			}

			try{
				if ( source != null ){
					String sourceName = env.getPropertyValue(source, "name", String.class)
					if (!sourceName.equalsIgnoreCase(outletName)){
						logger.trace("The name of the outlet stored as the source has changed, look for a new one.")
						source = null
					}
				}
			} catch (Exception e) {
				logger.warn("${triggeredRule.getName()} Unable to find outlet with TO '$source'!  Searching for a new Outlet named '$outletName'.")
				source = null
			}

			TO<?> ourFriend = null

			if (outletHasChanged || source == null) {

				//go find the right outlet
				Collection<TO<?>> outlets = env.getObjectsByType("GRAPH_OUTLET")

				if (outlets.size() == 0) {
					logger.warn("${triggeredRule.getName()}: No Outlets found!")
					//make sure the source property is null
					env.setPropertyValue(nativeTO, "source", null)
					finalValue = null
				} else {
					int validOutlets = 0
					for (TO<?> out: outlets) {
						//logger.debug("${triggeredRule.getName()} this outlet I found is named '" + env.getPropertyValue(out, "name", String.class) + "'")
						if (env.getPropertyValue(out, "name", String.class).equalsIgnoreCase(outletName)) {
							validOutlets++
							ourFriend = out
						}
					}
					if (validOutlets > 1) {
						logger.warn("${triggeredRule.getName()} More than one Outlet named '$outletName' was found!  Using Outlet with TO $ourFriend")
						throwAlert(triggeredRule, calculated, nativeTO, LocalizationUtils.getLocalizedString("alert_message_found_more_than_one_outlet", outletName));
					}
					if (ourFriend == null) {
						logger.warn("${triggeredRule.getName()} Correct Outlet could not be found!")
						//make sure the source property is null
						env.setPropertyValue(nativeTO, "source", null)
						throwAlert(triggeredRule, calculated, nativeTO, LocalizationUtils.getLocalizedString("alert_message_outlet_not_found", outletName));
						finalValue = null
					} else {
						//finalValue = env.getPropertyValue(ourFriend, "lastValue", Double.class )
						logger.trace("${triggeredRule.getName()} I've found the right outlet, now to do the heavy lifting!")
						//TO<?> source = env.getPropertyValue(nativeTO, "source", TO.class )
						env.setPropertyValue(nativeTO, "source", ourFriend)
						env.setPropertyValue(nativeTO, "oldOutletName", outletName)
					}
				}
			} else {
				logger.trace("${triggeredRule.getName()} No changes to outlet, using existing TO.")
			}
			TO<?> sourceToUse = null
			if (ourFriend != null) {
				//take advantage of the fact we already have the TO and don't want to wait for the property to update via the DBBatchUpdater
				sourceToUse = ourFriend
			} else if (source != null) {
				sourceToUse = source
			}
			if (sourceToUse != null) {
				//grab the input and copy over
				try {
					def currentValue = env.getPropertyValue(sourceToUse, "lastValue", Double.class)
					if (currentValue == null) {
						logger.warn("Null input to inlet ${triggeredRule.getName()}")
						//finalValue = defaultValue
						finalValue = null
					} else {
						finalValue = (double) currentValue
					}
				} catch (Exception e) {
					logger.warn("${triggeredRule.getName()} Unable to find outlet '$outletName'!  Returning default value instead.")
					//finalValue = defaultValue
					finalValue = null
					env.setPropertyValue(nativeTO, "source", null)
				}
			}
			logger.trace("Inlet ${triggeredRule.getName()} value is " + finalValue);
			return finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

	private throwAlert(RuleI triggeredRule, final Property calculated, TO<?> nativeTO, String message) {
		AlertService alertService = calculated.getAlertService();
		alertService.raiseAlert(
				new Alert("Math Error", "Math Error Alert", new Date(), LocalizationUtils.getLocalizedString("alert_message_math_error_has_been_detected_in_rule", triggeredRule.getName()) + " $message", nativeTO)
		);
	}

}