
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;;

public class PdiPduAll implements RuleAction {
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(PdiPduAll.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			String[] panels = ["panel1", "panel2", "panel3", "panel4"];
			String[] properties = ["currentA", "currentB", "currentC", "voltageAB", "voltageBC", "voltageCA", "voltageAN", "voltageBN", "voltageCN", "powerA", "powerB", "powerC", "powerFactorA", "powerFactorB", "powerFactorC"];
			def totals = [:]; // not java compliant...
			def counts = [:]; // not java compliant...

			for(String prop: properties) {
				counts[prop] = 0;
				totals[prop] = 0.0;
			}

			logger.trace("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " totalling panel data");

			for(String panelName: panels) {
				TO<?> panel = env.getPropertyValue(nativeTO, panelName, TO.class);
				if(panel != null) {
					for(String propertyName: properties) {
						TO<?> modbusproperty = env.getPropertyValue(panel, propertyName, TO.class);
						if (modbusproperty != null) {
							Double pVal = env.getPropertyValue(modbusproperty, "lastValue", Double.class);
							if(pVal != null) {
								counts[propertyName] ++;
								totals[propertyName] += pVal;
							}
						}
					}
				}
			}

			logger.trace("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " processing panel data");

			for(String toAvg: ["voltageAB", "voltageBC", "voltageCA", "voltageAN", "voltageBN", "voltageCN", "powerFactorA", "powerFactorB", "powerFactorC"]) {
				if(counts[toAvg] != 0)
					totals[toAvg] = totals[toAvg] / counts[toAvg];
			}

			logger.trace("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " storing processed data");

			for(String propertyName: properties) {
				env.setPropertyValue(nativeTO, propertyName, (double)totals[propertyName]);
			}

			logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " finished");

			return (double)totals["currentA"];
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}
