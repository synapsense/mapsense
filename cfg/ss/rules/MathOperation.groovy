package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO
import com.synapsense.dto.Alert
import com.synapsense.service.AlertService;
import com.synapsense.util.LocalizationUtils
import com.synapsense.dto.ValueTO;

public class MathOperation implements RuleAction {

	private Property _inputA
	private Property _inputB

	public Property getInputA() {
		return _inputA
	}

	public void setInputA(Property i) {
		_inputA = i
	}

	public Property getInputB() {
		return _inputB
	}

	public void setInputB(Property i) {
		_inputB = i
	}


	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(MathOperation.class);

	private final static double ERROR1 = -2000.0
	private final static double ERROR2 = -3000.0
	private final static double ERROR3 = -5000.0

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		TO<?> nativeTO
		try {
			Environment env = calculated.getEnvironment();
			nativeTO = calculated.getDataSource().getHostObjectTO();
			//logger.trace("Math Operation Happening!");
			//math goes here.
			//start function pseudo code:

			//get params
			//turn inputs into groovy lists
			//select operations
			//call mainMath
			//return result

			Double finalValue = null
			def opA = env.getPropertyValue(nativeTO, "opA", String.class).toLowerCase().trim()
			def opB = env.getPropertyValue(nativeTO, "opB", String.class).toLowerCase().trim()
			def opM = env.getPropertyValue(nativeTO, "opM", String.class).toLowerCase().trim()

			def inputAList = []
			def inputBList = []
			int inputADisabled = 0
			int inputBDisabled = 0

			for (TO<?> t: inputA.getValue()) {
				Double current = env.getPropertyValue(t, "lastValue", Double.class)
				Integer status = null;
				for(ValueTO v : env.getAllPropertiesValues(t)){
					if(v.propertyName.equalsIgnoreCase("status")){
						status = v.getValue()
					}
				}
				if((status && status >= 2) || (current == ERROR3) ){
					inputADisabled++
					continue
				}


				if ( (current == ERROR1) || (current == ERROR2)  ){ //|| (current == ERROR3)
					//logger.trace("current now null")
					current = null
				}
				if (current != null) {
					inputAList += current
				} else {
					logger.warn("Null input to rule ${triggeredRule.getName()}")
					if ( ! opA.equals("noop") ){
						throwAlert(triggeredRule, calculated, nativeTO,LocalizationUtils.getLocalizedString("alert_message_null_input_to_rule"))
						return null
					}
				}
			}

			for (TO<?> t: inputB.getValue()) {
				Double current = env.getPropertyValue(t, "lastValue", Double.class)
				Integer status = null;
				for(ValueTO v : env.getAllPropertiesValues(t)){
					if(v.propertyName.equalsIgnoreCase("status")){
						status = v.getValue()
					}
				}
				if((status && status >= 2) || (current == ERROR3)){
					inputBDisabled++
					continue
				}

				if ( (current == ERROR1) || (current == ERROR2)  ){
					logger.trace("current now null")
					current = null
				}
				if (current != null) {
					inputBList += current
				} else {
					logger.warn("Null input to rule ${triggeredRule.getName()}")
					if ( ! opB.equals("noop") ){
						throwAlert(triggeredRule, calculated, nativeTO, LocalizationUtils.getLocalizedString("alert_message_null_input_to_rule"))
						return null
					}
				}
			}

			if (inputAList.size() == 0 && !opA.equals("noop")) {
				if(inputADisabled == inputA.getValue()?.size()){
					logger.warn"All input As to ${triggeredRule.getName()} were disabled."
					env.setPropertyValue(nativeTO,"status", 2)
				}
				logger.warn("Empty inputs to rule ${triggeredRule.getName()}")
				throwAlert(triggeredRule, calculated, nativeTO, LocalizationUtils.getLocalizedString("alert_message_empty_input_to_rule"))
				return null
			}

			if (inputBList.size() == 0 && !opB.equals("noop")) {
				if(inputBDisabled == inputB.getValue()?.size()){
					logger.warn"All input Bs to ${triggeredRule.getName()} were disabled."
					env.setPropertyValue(nativeTO,"status", 2)
				}
				logger.warn("Empty inputs to rule ${triggeredRule.getName()}")
				throwAlert(triggeredRule, calculated, nativeTO, LocalizationUtils.getLocalizedString("alert_message_empty_input_to_rule"))
				return null
			}

			//if we've made is this far, we can set our status to 1
			env.setPropertyValue(nativeTO,"status",1)
			logger.debug(" " + triggeredRule.getName() + " inputs: a = $inputAList b = $inputBList")
			def listAOp = getListOp(opA)
			def listBOp = getListOp(opB)
			def mainOp = getMainOp(opM)

			finalValue = mainMath(inputAList, inputBList, listAOp, listBOp, mainOp)
			//logger.debug("Math Operation results in " + finalValue ?: "NULLLLLLLLL!!!!!" );
			logger.debug(" MATH OPERATION " + triggeredRule.getName() + " with ( $opA$opB$opM ) = $finalValue")

			if (finalValue.isInfinite() || finalValue.isNaN()) {
				logger.warn("Rule ${triggeredRule.getName()}: Unacceptable Math Result: $finalValue")
				throwAlert(triggeredRule, calculated, nativeTO, LocalizationUtils.getLocalizedString("alert_message_unacceptable_math_result", "$finalValue"));
				return null
			}

			return finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			throwAlert(triggeredRule, calculated, nativeTO, "error: $e");
			return null;
		}
	}

	private throwAlert(RuleI triggeredRule, final Property calculated, TO<?> nativeTO, String message) {
		AlertService alertService = calculated.getAlertService();
		alertService.raiseAlert(
				//new Alert("Rack P3 SmartPlugs are not reporting", "Node not reporting", new Date(), "Multiple P3 SmartPlugs are not reporting. Unable to perform power calculations.", null, rack.getObjId())
				new Alert("Math Error", "Math Error Alert", new Date(), LocalizationUtils.getLocalizedString("alert_message_math_error_has_been_detected_in_rule", triggeredRule.getName()) + " $message", nativeTO)
		);
	}

	//list operation closures:
	def listSum = { list -> return list?.sum() }
	def listMin = { list -> return list?.min() }
	def listMax = { list -> return list?.max() }
	def listAvg = { list -> return list?.sum() / list?.size() }
	def listSize = { list -> return list?.size() }
	def listFirst = { list -> return list?.first() }
	def listLast = { list -> return list?.last() }
	def listSolo = { list -> return list[0] }  //for passing lists of one value through to the main op

	//main operation closures:
	def opAdd = { a, b -> return a + b }
	def opSub = { a, b -> return a - b}
	def opMul = { a, b -> return a * b}
	def opDiv = { a, b -> return (double) a / (double) b}
	def opMod = { a, b -> return (double) a % (double) b}
	//def opPow = { a, b -> return Math.pow((double)a,(double)b) }
	def opPow = { a, b -> return (double) a ** (double) b }
	def opMin = { a, b -> return Math.min((double) a, (double) b) }
	def opMax = { a, b -> return Math.max((double) a, (double) b) }
	def opAvg = { a, b -> return (a + b) / 2 }
	def opA = { a, b -> return a }
	def opB = { a, b -> return b }
	def opNoOp = {a, b -> return a ?: b }
	//boolean math
	def opOR = { a, b -> return (a.longValue() | b.longValue()) }
	def opXOR = { a, b -> return (a.longValue() ^ b.longValue()) }
	def opAND = { a, b -> return (a.longValue() & b.longValue()) }
	def opShiftL = { a, b -> return (a.longValue() << b.longValue()) }
	def opShiftR = { a, b -> return (a.longValue() >> b.longValue()) }

	def opBitExtract = { a, b ->
		BigInteger value = new BigInteger(a.longValue().toString())
		Double result
		if(value.testBit(b.intValue())){
			result = 1.0
		} else {
			result = 0.0
		}
		return result
	}
	//comparisons
	//it's the cheesiest casting from boolean to double ever!
	def opLt = {a, b -> return( a < b ? 1.0 : 0.0) }
	def opGt = {a, b -> return( a > b ? 1.0 : 0.0) }
	def opEq = {a, b -> return( a == b ? 1.0 : 0.0) }
	def opNEq = {a, b -> return( a != b ? 1.0 : 0.0) }

	/**
	 Returns the correct list operation closure based on a string input (presumably handed down from the MapSense GUI.)
	 */
	def getListOp(String selection) {
		def listOp
		switch (selection.toLowerCase().trim()) {
			case "sum":
			case "+":
				listOp = listSum
				break
			case "min":
				listOp = listMin
				break
			case "max":
				listOp = listMax
				break
			case "avg":
				listOp = listAvg
				break
			case "noop":
				listOp = listFirst
				break
			case "solo":
				listOp = listSolo
				break
			case "nil":
			default:
				listOp = listFirst
				break
		}
		return listOp
	}

	/**
	 Returns the correct Main (or binary) operation closure based on a string input (presumably handed down from the MapSense GUI.)
	 */
	def getMainOp(String selection) {
		def mainOp
		switch (selection.toLowerCase().trim()) {
			case "add":
			case "+":
				mainOp = opAdd
				break
			case "sub":
			case "-":
				mainOp = opSub
				break
			case "mul":
			case "*":
				mainOp = opMul
				break
			case "div":
			case "/":
				mainOp = opDiv
				break
			case "mod":
			case "%":
				mainOp = opMod
				break
			case "pow":
			case "**":
			case "^":
				mainOp = opPow
				break
			case "min":
				mainOp = opMin
				break
			case "max":
				mainOp = opMax
				break
			case "avg":
				mainOp = opAvg
				break
			case "or":
				mainOp = opOR
				break
			case "xor":
				mainOp = opXOR
				break
			case "and":
				mainOp = opAND
				break
			case "shiftl":
				mainOp = opShiftL
				break
			case "shiftr":
				mainOp = opShiftR
				break
			case "bitextract":
				mainOp = opBitExtract
				break
			case "lt":
				mainOp = opLt
				break
			case "gt":
				mainOp = opGt
				break
			case "eq":
				mainOp = opEq
				break
			case "neq":
				mainOp = opNEq
				break
			case "noop":
				mainOp = opNoOp
				break
			case "nil":
			default:
				//mainOp = opNil
				mainOp = opNoOp
				break
		}
		return mainOp
	}

	/**
	 Does the actual math based on the lists and operations handed in as closures.
	 */
	Double mainMath(listA, listB, Closure opA, Closure opB, Closure opM) {
		if (listA && listB) {
			return opM(opA(listA), opB(listB))
		}
		else if (listA) {
			return opA(listA)
		}
		else if (listB) {
			return opB(listB)
		}
		else {return null}
	}
}