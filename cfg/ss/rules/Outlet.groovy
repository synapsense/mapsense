
/**
 * A Calculation Graph Outlet; used to import values to a graph in another project file.
 * Primarily, this rule's job is to move the value from 'input' to the 'lastValue' field of the GRAPH_OUTLET object.
 * @author Gabriel Helman
 * @since Mars
 * Date: 2/16/11
 * Time: 11:29 AM
 */

package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;

public class Outlet implements RuleAction {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(Outlet.class);

	private Property input
	public Property getInput() {
		return input
	}
    public void setInput(Property i) {
		input = i
	}

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			Double finalValue = env.getPropertyValue((TO<?>) input.getValue(), "lastValue", Double.class);
			if ( finalValue == null ){
				logger.warn("Null input to outlet ${triggeredRule.getName()}")
			}
			logger.debug("Outlet ${triggeredRule.getName()} value is " + finalValue);
			return finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}