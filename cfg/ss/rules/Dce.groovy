
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;;

public class Dce implements RuleAction {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(Dce.class);

	private Property itPowerInputs
	public Property getItPowerInputs() {
		return itPowerInputs
	}
    public void setItPowerInputs(Property i) {
		itPowerInputs = i
	}
	private Property coolingPowerInputs
	public Property getCoolingPowerInputs() {
		return coolingPowerInputs
	}
    public void setCoolingPowerInputs(Property i) {
		coolingPowerInputs = i
	}

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			//Double itPower = env.getPropertyValue(nativeTO, "itPower", Double.class);
			//Double coolingPower = env.getPropertyValue(nativeTO, "coolingPower", Double.class);
			//now with dependent inputs
			Double itPower = itPowerInputs.getValue()
			Double coolingPower = coolingPowerInputs.getValue()
			if(itPower == null || coolingPower == null) {
				logger.warn("One or more DCE inputs is NULL");
				return null;
			}
			Double dce = itPower / coolingPower;
			if(dce == Double.NaN || dce == Double.POSITIVE_INFINITY || dce == Double.NEGATIVE_INFINITY) {
				logger.warn(calculated.getName() + " value calculated to '" + dce + "' returning null");
				return null;
			} else {
				//logger.debug(calculated.getName() + " returning " + dce);
				return dce;
			}
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}