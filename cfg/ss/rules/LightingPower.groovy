package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO
import com.synapsense.dto.Alert
import com.synapsense.service.AlertService;;
import com.synapsense.util.LocalizationUtils
import com.synapsense.dto.ValueTO;

public class LightingPower implements RuleAction {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(LightingPower.class);

	private Property _lightingPowerInputs

	public Property getLightingPowerInputs() {
		return _lightingPowerInputs
	}

	public void setLightingPowerInputs(Property i) {
		_lightingPowerInputs = i
	}

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			Double total = null;
			for (TO<?> t: lightingPowerInputs.getValue()) {
				Integer status = null;
				for(ValueTO v : env.getAllPropertiesValues(t)){
					if(v.propertyName.equalsIgnoreCase("status")){
						status = v.getValue()
					}
				}
				if(status && status >= 2){
					continue
				}
				def current = env.getPropertyValue(t, "lastValue", Double.class)
				if (current != null) {
					if (total == null) { total = 0 }
					total += current
				} else {
					logger.warn("Null input to PUE reporter ${triggeredRule.getName()}")
					throwAlert(triggeredRule, calculated, nativeTO, LocalizationUtils.getLocalizedString("alert_message_null_input_to_rule"))
					return null
				}
			}
			if (total == null){
				logger.warn("Empty inputs to rule ${triggeredRule.getName()}")
				throwAlert(triggeredRule, calculated, nativeTO, LocalizationUtils.getLocalizedString("alert_message_empty_input_to_rule"))
				return null
			}
			return total;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

	private throwAlert(RuleI triggeredRule, final Property calculated, TO<?> nativeTO, String message) {
		AlertService alertService = calculated.getAlertService();
		alertService.raiseAlert(
				new Alert("Math Error", "Math Error Alert", new Date(), LocalizationUtils.getLocalizedString("alert_message_math_error_has_been_detected_in_rule", triggeredRule.getName()) + " $message", nativeTO)
		);
	}
}