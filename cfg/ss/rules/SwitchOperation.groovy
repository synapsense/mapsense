package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;

public class SwitchOperation implements RuleAction {

	private Property _value
	private Property _value2
	private Property _status

	public Property getValue() {
		return _value
	}

	public void setValue(Property i) {
		_value = i
	}

	public Property getValue2() {
		return _value2
	}

	public void setValue2(Property i) {
		_value2 = i
	}

	public Property getStatus() {
		return _status
	}

	public void setStatus(Property i) {
		_status = i
	}

	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(SwitchOperation.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			def finalValue = 0.0
			Double defaultValue = env.getPropertyValue(nativeTO, "default", Double.class)
			//double inputValue = 0
			double controlSignal = 0
			/*
			for (TO<?> t: value.getValue()) {
				inputValue += (double) (env.getPropertyValue(t, "lastValue", Double.class) ?: 0)
			}
			*/

			Double inputValue = env.getPropertyValue((TO<?>) value.getValue(), "lastValue", Double.class)

			Double inputValue2
			if(value2){
				inputValue2 = env.getPropertyValue((TO<?>) value2.getValue(), "lastValue", Double.class)
			}


			//this is a status signal, so make sure to defend against strings
			def currentValue
			for (TO<?> t: status.getValue()) {
				currentValue = env.getPropertyValue(t, "lastValue", Double.class)
				if (!(currentValue instanceof Double)) {
					logger.warn("Non-Double status in Switch ${triggeredRule.getName()}")
				} else {
					controlSignal += currentValue
				}
			}
			if (controlSignal == 1.0) {
				finalValue = inputValue
			} else {
				if(defaultValue!=null){
					finalValue = defaultValue
				} else {
					finalValue = inputValue2
				}
			}
			logger.debug("Switch Operation with $controlSignal ? $inputValue : ($defaultValue || $inputValue2) results in " + finalValue);
			return finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

}
