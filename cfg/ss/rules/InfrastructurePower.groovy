package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;

public class InfrastructurePower implements RuleAction {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(InfrastructurePower.class);

	private Property powerLossInputs

	public Property getPowerLossInputs() {
		return powerLossInputs
	}

	public void setPowerLossInputs(Property i) {
		powerLossInputs = i
	}

	private Property lightingPowerInputs

	public Property getLightingPowerInputs() {
		return lightingPowerInputs
	}

	public void setLightingPowerInputs(Property i) {
		lightingPowerInputs = i
	}

	private Property coolingPowerInputs

	public Property getCoolingPowerInputs() {
		return coolingPowerInputs
	}

	public void setCoolingPowerInputs(Property i) {
		coolingPowerInputs = i
	}

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			//depending on other rules
			Double coolingPower = coolingPowerInputs.getValue()
			Double lightingPower = lightingPowerInputs.getValue()
			Double powerLoss = powerLossInputs.getValue()

			if (coolingPower == null || lightingPower == null || powerLoss == null) {
				logger.warn("One or more InfrastructurePower inputs is NULL");
				//return null;
			}
			if (coolingPower == null && lightingPower == null && powerLoss == null) {
				logger.warn("All InfrastructurePower inputs are NULL");
				return null;
			}
			double infrastructurePower = (coolingPower?:0) + (lightingPower?:0) + (powerLoss?:0);
			//logger.debug(calculated.getName() + " returning " + infrastructurePower);
			return infrastructurePower;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}