package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO
import com.synapsense.dto.Alert
import com.synapsense.service.AlertService;
import com.synapsense.util.LocalizationUtils
import com.synapsense.dto.ValueTO;


public class MathSingle implements RuleAction {

	private Property _input

	public Property getInput() {
		return _input
	}

	public void setInput(Property i) {
		_input = i
	}

	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(MathSingle.class);

	private final static double ERROR1 = -2000.0
	private final static double ERROR2 = -3000.0
	private final static double ERROR3 = -5000.0

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");
		Environment env
		TO<?> nativeTO

		try {
			env = calculated.getEnvironment()
			nativeTO = calculated.getDataSource().getHostObjectTO()

			Double inputValue = env.getPropertyValue((TO<?>) input.getValue(), "lastValue", Double.class)

			Integer status = null;
			for(ValueTO v : env.getAllPropertiesValues((TO<?>) input.getValue())){
				if(v.propertyName.equalsIgnoreCase("status")){
					status = v.getValue()
				}
			}
			if((status && status >= 2) || (inputValue == ERROR3) ){
				logger.warn("Disabled input to ${triggeredRule.getName()}.")
				env.setPropertyValue(nativeTO, "status", 2)
				return null
			} else {
				env.setPropertyValue(nativeTO, "status", 1)
			}

			Double finalValue = null
			String op = env.getPropertyValue(nativeTO, "op", String.class)

			if ( (inputValue == null) || (inputValue == ERROR1) || (inputValue == ERROR2) ){
				//logger.trace("current now null")
				inputValue = null
			}

			if (inputValue == null) {
				logger.warn("Rule ${triggeredRule.getName()}: All Inputs Null")
				throwAlert(triggeredRule, calculated, nativeTO, LocalizationUtils.getLocalizedString("alert_message_null_input_to_rule"))
				return null
			}

			finalValue = mainMath(inputValue, getSoloOp(op))
			logger.debug("Single Math Operation: op =  $op ,  input = $input, finalValue = $finalValue")

			if (finalValue.isInfinite() || finalValue.isNaN()) {
				logger.warn("Rule ${triggeredRule.getName()}: Unacceptable Math Result: $finalValue")
				throwAlert(triggeredRule, calculated, nativeTO, LocalizationUtils.getLocalizedString("alert_message_unacceptable_math_result", "$finalValue"));
				return null
			}
			return finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			logger.error(e)
			throwAlert(triggeredRule, calculated, nativeTO, "Error: $e")
			return null;
		}
	}

	private throwAlert(RuleI triggeredRule, final Property calculated, TO<?> nativeTO, String message) {
		AlertService alertService = calculated.getAlertService();
		alertService.raiseAlert(
				new Alert("Math Error", "Math Error Alert", new Date(), LocalizationUtils.getLocalizedString("alert_message_math_error_has_been_detected_in_rule", triggeredRule.getName()) + " $message", nativeTO)
		);
	}

	//solo operations:
	def soloNoOp = { a -> return a }
	def soloSqrt = { a -> return Math.sqrt(a) }

	//trig functions: assumes input in radians
	def soloSin = { a -> return Math.sin(a) }
	def soloCos = { a -> return Math.cos(a) }
	def soloTan = { a -> return Math.tan(a) }

	def soloRound = { a -> return Math.round(a) }
	def soloFloor = { a -> return Math.floor(a) }
	def soloCeil = { a -> return Math.ceil(a) }

	def soloAbs = { a -> return Math.abs(a) }

	def soloNot = { a -> return (~(a.longValue())) }

	//logs!
	def soloLoge = { a -> return (Math.log(a)) }
	def soloLog10 = { a -> return (Math.log10(a)) }

	//also add OR,AND,NAND,XOR,NOT (?)

	def getSoloOp(String selection) {
		def listOp
		switch (selection.toLowerCase().trim()) {
			case "sqrt":
				listOp = soloSqrt
				break
			case "sin":
				listOp = soloSin
				break
			case "cos":
				listOp = soloCos
				break
			case "tan":
				listOp = soloTan
				break
			case "round":
				listOp = soloRound
				break
			case "floor":
				listOp = soloFloor
				break
			case "ceil":
				listOp = soloCeil
				break
			case "abs":
				listOp = soloAbs
				break
			case "not":
				listOp = soloNot
				break
			case "loge":
				listOp = soloLoge
				break
			case "log10":
				listOp = soloLog10
				break
			case "nil":
			default:
				listOp = soloNoOp
				break
		}
		return listOp
	}

	double mainMath(input, Closure op) {
		return op(input)
	}
}
