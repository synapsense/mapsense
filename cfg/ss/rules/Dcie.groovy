
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;;

public class Dcie implements RuleAction {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(Dcie.class);

	private Property itPowerInputs
	public Property getItPowerInputs() {
		return itPowerInputs
	}
    public void setItPowerInputs(Property i) {
		itPowerInputs = i
	}

	private Property powerLossInputs
	public Property getPowerLossInputs() {
		return powerLossInputs
	}
    public void setPowerLossInputs(Property i) {
		powerLossInputs = i
	}

	private Property lightingPowerInputs
	public Property getLightingPowerInputs() {
		return lightingPowerInputs
	}
    public void setLightingPowerInputs(Property i) {
		lightingPowerInputs = i
	}

	private Property coolingPowerInputs
	public Property getCoolingPowerInputs() {
		return coolingPowerInputs
	}
    public void setCoolingPowerInputs(Property i) {
		coolingPowerInputs = i
	}
	
	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			/*
			Double itPower = env.getPropertyValue(nativeTO, "itPower", Double.class);
			Double coolingPower = env.getPropertyValue(nativeTO, "coolingPower", Double.class);
			Double lightingPower = env.getPropertyValue(nativeTO, "lightingPower", Double.class);
			Double powerLoss = env.getPropertyValue(nativeTO, "powerLoss", Double.class);
			*/

			//depending on other rules
			Double itPower = itPowerInputs.getValue()
			Double coolingPower = coolingPowerInputs.getValue()
			Double lightingPower = lightingPowerInputs.getValue()
			Double powerLoss = powerLossInputs.getValue()

			if(itPower == null || coolingPower == null || lightingPower == null || powerLoss == null) {
				logger.warn("DCiE: One or more DCiE inputs is NULL");
				//return null;
			}
			if(itPower == null || lightingPower == null) {
				logger.warn("DCiE: Either the itPower or lighting inputs to DCiE are NULL, returning NULL");
				return null;
			}
			if(coolingPower == null ^ powerLoss == null) {
				logger.warn("DCiE: One or both of coolingPower and powerLoss are NULL, returning NULL");
				return null;
			}

			double totalPower = itPower + (coolingPower?:0) + (lightingPower?:0) + (powerLoss?:0);
			Double dcie = itPower / totalPower;
			if(dcie == Double.NaN || dcie == Double.POSITIVE_INFINITY || dcie == Double.NEGATIVE_INFINITY) {
				logger.warn(calculated.getName() + " value calculated to '" + dcie + "' returning null");
				return null;
			} else {
				//logger.debug(calculated.getName() + " returning " + dcie);
				return dcie;
			}
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}