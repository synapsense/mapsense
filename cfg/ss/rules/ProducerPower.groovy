
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;

/**
 * The PowerProducer gets used as a translation layer in interface objects.
 * It accepts an input value, and then makes sure it's a valid power datatype value.
 * This means making damn sure it hands out a double.
 *
 * For power, this mostly just means casting strings with numbers in them or tossing null values.
 */
public class ProducerPower implements RuleAction {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(ProducerPower.class);

	private Property _input
	public Property getInput() {
		return _input
	}
    public void setInput(Property i) {
		_input = i
	}

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			Double finalValue = null
			for(TO<?> t : input.getValue()) {
				def currentValue = env.getPropertyValue(t, "lastValue", Object.class).toString()
				if ( !currentValue ) {
					logger.warn("Null or invalid input to ProducerPower ${triggeredRule.getName()}")
				} else {
					//finalValue += (double)currentValue
					if ( currentValue.isDouble() ) {
						if ( !finalValue ) {
							finalValue = 0.0
						}
						finalValue += currentValue.toDouble()
					} else {
						logger.warn("Non-numeric input ($currentValue) to ProducerPower ${triggeredRule.getName()}")
					}
				}
			}
			//logger.debug("PowerProducer ${triggeredRule.getName()} value is " + finalValue);
			//return (double) finalValue;
			return finalValue
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}