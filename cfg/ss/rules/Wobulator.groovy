
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;;

/**
	Takes a user defined input and jiggles it randomly within a user defined value.
*/
public class Wobulator implements RuleAction {

	/**
	Returns a map of this object's properties.
	*/
/*
	public static java.util.HashMap getMyProperties( com.synapsense.rulesengine.core.environment.Property calculated ) {
		def myMap = [:]
		Environment env = calculated.getEnvironment();
		TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
		def everything = env.getAllPropertiesValues( nativeTO )
		for (e in everything ) {
			def currentValue = e.getValue()
 			if ( currentValue instanceof TO<?> ) {
				TO<?> source = env.getPropertyValue(nativeTO, e.getPropertyName(), TO.class);
				myMap[ e.getPropertyName() ]  = env.getPropertyValue(source, "lastValue", Double.class);
			} else if ( currentValue instanceof Collection< TO<?> > ) {
				Collection< TO<?> > currentCollection = env.getPropertyValue(nativeTO, e.getPropertyName(), Collection.class);
				def currentList = []
				for (TO<?> input in currentCollection) {
					def inputValue = env.getPropertyValue(input, "lastValue", Double.class) //NOTE: assumes inputs are all doubles
					if(inputValue != null) {
						currentList.add( inputValue )
					}
				}
				myMap[ e.getPropertyName() ] = currentList
			} else {
				myMap[ e.getPropertyName() ] = e.getValue()
			}
		}
 		return ( myMap )
	}
*/

	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(Wobulator.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {
			//logger.trace("WOBULATION! ('Hey Everybody!  Commence the jigglin'!')")
			//def myProperties = getMyProperties(calculated)
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			def finalValue = 0.0
 
			//def baseline = myProperties["baseline"]
			//def jiggle = myProperties["jiggle"]
			//def lastValue = myProperties["lastValue"] ?: 0

			double baseline = env.getPropertyValue( nativeTO, "baseline", Double.class )
			double jiggle = env.getPropertyValue( nativeTO, "jiggle", Double.class )
			double lastValue = env.getPropertyValue( nativeTO, "lastValue", Double.class ) ?: 0
			
			//logger.trace("WOBULATOR: baseline: " + baseline + " jiggle: " + jiggle + " lastValue: " + lastValue )
			
			def jiggleAmount = Math.random() * jiggle
			
//			if ((Math.random() * 10) < 5) { jiggleAmount = 1 - jiggleAmount }  //do a +/- thing
//			finalValue = lastValue + jiggleAmount

			//roughly alternate up and down jiggle directions.  This keeps our graph looking like a sisemometer at a monster truck rally.
			if (lastValue < baseline) {
				finalValue = lastValue + jiggleAmount
			} else {
				finalValue = lastValue - jiggleAmount
			}
			
			//logger.trace("WOBULATOR: pre-adjusted value= " + finalValue )
			//adjust to keep it in the implied "wobble zone"
			if (finalValue > baseline + jiggle)
			{
				finalValue = baseline + jiggle
			}
			
			if (finalValue < baseline - jiggle)
			{
				finalValue = baseline - jiggle
			}
			
			//logger.debug("Wobbulation results in " + finalValue);

			logger.debug("WOBULATION! ${triggeredRule.getName()} ('Hey Everybody!  Commence the jigglin'!') results in $finalValue"  )
			return (double) finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}
