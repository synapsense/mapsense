package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import java.awt.geom.Point2D;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO
import com.synapsense.dto.ValueTO;

/**
	
*/
public class TableFit implements RuleAction {

	private Property _inputPower

	public Property getInputPower() {
		return _inputPower
	}
    public void setInputPower(Property i) {
		_inputPower = i
	}

	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(TableFit.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {
			//logger.trace("TABLE.  FIT.")
			//def myProperties = getMyProperties(calculated)
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();


			def finalValue = 0.0
			String tableString = env.getPropertyValue( nativeTO, "table", String.class ) //the table of values entered by the user

			if ( tableString == null ){
				logger.info("Rule '${triggeredRule.getName()}', property '${calculated.getName()}' has null table.");
				return null;
			}

			if ( inputPower == null || inputPower.getValue() == null ){
				logger.info("Rule '${triggeredRule.getName()}', property '${calculated.getName()}' has null input.");
				return null;
			}

			def props = env.getAllPropertiesValues((TO<?>) inputPower.getValue())
			Integer status = null;
			for(ValueTO v : props){
				if(v.propertyName.equalsIgnoreCase("status")){
					status = v.getValue()
				}
			}
			if(status && status >= 2){
				logger.warn("Disabled input to tablefit.")
				env.setPropertyValue(nativeTO, "status", 2)
				return null
			} else {
				env.setPropertyValue(nativeTO, "status", 1)
			}
			
			Double powerValue = env.getPropertyValue((TO<?>) inputPower.getValue(), "lastValue", Double.class);
			if ( powerValue == null ){
				logger.info("Rule '${triggeredRule.getName()}', property '${calculated.getName()}' has null input.");
				return null;
			}

			if ( powerValue == 0 ) {
				//logger.warn("0 value in tablefit: powervalue = $powerValue, top = $top " );
				logger.warn("0 powervalue value in tablefit!" );
			} else {
				//double loadPercent = (powerValue / top) * 100
				def loadPercent = powerValue
				//logger.trace("Load Percent is $loadPercent")
				def result = tableFunction( loadPercent, parseTable( tableString ) )
				//finalValue = (result / 100)
				finalValue = result
			}
			//return the final value
			//logger.debug("TABLEFIT ${triggeredRule.getName()} on $powerValue results in an efficiency of $finalValue");
			return (Double) finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}


	//new test table: 0,0;10,50;20,75;30,90;40,95;50,95;60,95;70,95;80,80;90,55;100,25

	//String fitTable = "0.0,0.0;5.0,1.0;10.0,2.0;15.0,3.0";

// a list of Point2D.Doubles, sorted in ascending order of X coordinate

	Double tableFunction(double input, def table) {
		//assert table.size() > 1
		if ( table.size() == 1) {
			logger.debug("Single Value table returning ${table[0].y}")
			return table[0].y
		} else if ( table.size() < 1 ){
			logger.warn("Table with no values returning null")
			return null
		}


		if(input < table[0].x) {
			return input * slope(table[0], table[1]) + intercept(table[0], table[1]);
		}
		if(input > table[-1].x) {
			return input * slope(table[-2], table[-1]) + intercept(table[-2], table[-1]);
		}
		int i = 0
		for(Point2D.Double point: table) {
			if(point.x == input)
				return point.y
			if(point.x > input)
				return input * slope(table[i-1], table[i]) + intercept(table[i-1], table[i])
			i++
		}
		throw new Exception("Programming Error")
	}

	double slope(Point2D.Double a, Point2D.Double b) {
		return (b.y - a.y) / (b.x - a.x)
	}

	double intercept(Point2D.Double a, Point2D.Double b) {
		return a.y - slope(a, b) * a.x
	}

	/*
	def parseTable(String sTable) {
		def pairs = sTable.split(';')
		def table = pairs.collect{
			def elems = it.split(',')
			return new Point2D.Double(Double.parseDouble(elems[0]), Double.parseDouble(elems[1]))
		}
		table.sort{ a, b -> a.x.compareTo(b.x) }
	}
	*/

	def parseTable(String sTable) {
		if (sTable.trim().length() == 0 ) {
			return []
		}
		def pairs = sTable.split(';')
		//supports tablestring with empty y-values
		def table = []
		for( String p : pairs ){
			def elems = p.split(',')
			if ( elems.length > 1 && elems[0].isDouble() && elems[1].isDouble() ){
				table += new Point2D.Double(Double.parseDouble(elems[0]), Double.parseDouble(elems[1]))
			}
		}
		table.sort{ a, b -> a.x.compareTo(b.x) }
		logger.debug("TableString $sTable parsed to $table")
		return table
	}

}