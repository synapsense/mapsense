package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import java.awt.geom.Point2D;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;
import com.synapsense.dto.ValueTO;

/**

 ActualTons = CFM * (RAT - SAT)

*/
public class CRAHActualTons implements RuleAction {

	private Property CFM
	public Property getCFM() {
		return CFM
	}
    public void setCFM(Property i) {
		CFM = i
	}

	private Property RAT
	public Property getRAT() {
		return RAT
	}
    public void setRAT(Property i) {
		RAT = i
	}

	private Property SAT
	public Property getSAT() {
		return SAT
	}
    public void setSAT(Property i) {
		SAT = i
	}


	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(CRAHActualTons.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {

			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			//check for deactivated status
			Integer status = (Integer) env.getPropertyValue(nativeTO, "status", Integer.class)
			if ( status >= 2 ){
				logger.debug("Deactivated Status, returning null.")
				return null
			}

			Double finalValue = 0.0

			/*
			double ratValue = 0
			for(TO<?> t : RAT.getValue()) {
				ratValue += (double)(env.getPropertyValue(t, "lastValue", Double.class) ?: 0)
			}

			double satValue = 0
			for(TO<?> t : SAT.getValue()) {
				satValue += (double)(env.getPropertyValue(t, "lastValue", Double.class) ?: 0)
			}
			*/
/*
			def rat = env.getPropertyValue(nativeTO, "returnT", ValueTO.class)
			double ratValue = (double)(env.getPropertyValue(rat.getValue(), "lastValue", Double.class) ?: 0)

			def sat = env.getPropertyValue(nativeTO, "supplyT", ValueTO.class)
			double satValue = (double)(env.getPropertyValue(sat.getValue(), "lastValue", Double.class) ?: 0)
  */


			Double ratValue = env.getPropertyValue((TO<?>) RAT.getValue(), "lastValue", Double.class);
			Double satValue = env.getPropertyValue((TO<?>) SAT.getValue(), "lastValue", Double.class);


			// validate sensor values are valid
			if ((ratValue == null) || (satValue == null) || (ratValue < -1000) || (satValue < -1000)) {
				logger.info("Rule '${triggeredRule.getName()}', property '${calculated.getName()}' inputs are invalid (RAT = $ratValue, SAT = $satValue).");
				return null;
			}

			if ( CFM.getValue() != null ) {

				Double cfmValue = (Double) ( CFM.getValue() ?: 0 )

				finalValue = ( (ratValue - satValue) * cfmValue * 1.085 ) / 12000
	
				logger.debug( "(ACTUAL TONS ${triggeredRule.getName()}: $ratValue - $satValue * $cfmValue * 1.085) / 12000 = $finalValue" )
			} else {
				finalValue = null
			}

			//return the final value
			logger.debug("ACTUAL TONS ${triggeredRule.getName()} results in $finalValue");
			return finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}


}