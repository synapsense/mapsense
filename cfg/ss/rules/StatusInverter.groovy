
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;;

public class StatusInverter implements RuleAction {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(StatusInverter.class);

	private Property _input

	public Property getInput() {
		return _input
	}
    public void setInput(Property i) {
		_input = i
	}

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			Double finalValue = 0
			Double inputValue = 0
			//this is a status value, so we need to defend against strings
			def currentValue
			for(TO<?> t : input.getValue()) {
				//inputValue += (double)(env.getPropertyValue(t, "lastValue", Double.class) ?: 0)
				currentValue = env.getPropertyValue(t, "lastValue", Double.class)
				if ( !(currentValue instanceof Double ) ) {
					logger.warn("Non-Double status in Switch ${triggeredRule.getName()}")
				} else {
					inputValue += currentValue
				}
			}
			if ( inputValue == 1.0 ) {
				finalValue = 0.0
			} else {
				finalValue = 1.0
			}
			//logger.debug("Inverter: $inputValue --> $finalValue ");
			return (double) finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}
