
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO
import com.synapsense.service.AlertService
import com.synapsense.dto.Alert;
import com.synapsense.util.LocalizationUtils
import com.synapsense.dto.ValueTO;

public class ThreePhaseTokW implements RuleAction {
	private Property _phaseA
	private Property _phaseB
	private Property _phaseC
	public Property getPhaseA() {
		return _phaseA
	}
    public void setPhaseA(Property i) {
		_phaseA = i
	}
	public Property getPhaseB() {
		return _phaseB
	}
    public void setPhaseB(Property i) {
		_phaseB = i
	}
	public Property getPhaseC() {
		return _phaseC
	}
    public void setPhaseC(Property i) {
		_phaseC = i
	}

	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(ThreePhaseTokW.class);

	/**
	 * Convert 3 phases of current into kW.
	 * Formula: ((phaseA + phaseB + phaseC) * pf * volts) / 1000
	 */
	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");
		try {
 			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			double volts = env.getPropertyValue( nativeTO, "volts", Double.class )
			double pf = env.getPropertyValue( nativeTO, "pf", Double.class )

			Double A = 0
			Double B = 0
			Double C = 0
			Integer statusA = null;
			Integer statusB = null;
			Integer statusC = null;

			for(ValueTO v : env.getAllPropertiesValues((TO<?>) phaseA.getValue())){
				if(v.propertyName.equalsIgnoreCase("status")){
					statusA = v.getValue()
				}
			}
			if(statusA ==  null || statusA == 1){
				A = env.getPropertyValue((TO<?>) phaseA.getValue(), "lastValue", Double.class);
			}

			for(ValueTO v : env.getAllPropertiesValues((TO<?>) phaseB.getValue())){
				if(v.propertyName.equalsIgnoreCase("status")){
					statusB = v.getValue()
				}
			}
			if(statusB ==  null || statusB == 1){
				B = env.getPropertyValue((TO<?>) phaseB.getValue(), "lastValue", Double.class);
			}
			
			for(ValueTO v : env.getAllPropertiesValues((TO<?>) phaseC.getValue())){
				if(v.propertyName.equalsIgnoreCase("status")){
					statusC = v.getValue()
				}
			}
			if(statusC ==  null || statusC == 1){
				C = env.getPropertyValue((TO<?>) phaseC.getValue(), "lastValue", Double.class);
			}
			
			//any disabled?
			if((statusA && statusA >=2) || (statusB && statusB >= 2) || (statusC && statusC >= 2)){
				logger.warn("All Disabled inputs to ${triggeredRule.getName()}")
				env.setPropertyValue(nativeTO, "status", 2)
				return null
			} else {
				env.setPropertyValue(nativeTO, "status", 1)
			}
			
			//any null?
			if(A == null || B == null || C == null){
				logger.warn("Null input to rule ${triggeredRule.getName()}")
				throwAlert(triggeredRule, calculated, nativeTO,LocalizationUtils.getLocalizedString("alert_message_null_input_to_rule"))
				return null
			}

			// See Bug 8841 for an explanation of this formula.
			Double finalValue = ( ( (A + B + C) / 3 ) * pf * volts * 1.732 ) / 1000
			logger.debug("${calculated.getName()} results in $finalValue")
			return finalValue
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

	private throwAlert(RuleI triggeredRule, final Property calculated, TO<?> nativeTO, String message) {
		AlertService alertService = calculated.getAlertService();
		alertService.raiseAlert(
				new Alert("Math Error", "Math Error Alert", new Date(), LocalizationUtils.getLocalizedString("alert_message_math_error_has_been_detected_in_rule", triggeredRule.getName()) + " $message", nativeTO)
		);
	}

}