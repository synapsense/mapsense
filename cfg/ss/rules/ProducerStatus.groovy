
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;

/**
 * The StatusProducer gets used as a translation layer in interface objects.
 * It accepts an input value, and then makes sure it's a valid status datatype value.
 * This means making damn sure it hands out a double that's either 0 or 1, and nothing else.
 *
 * There is also a whitelist of on or off values that the rule uses to handle casts.
 * So, one data source can have "true" mean 1, but another can only use "on", etc.
 *
 * The real job of the StatusProducer is to make sure that a string value never, ever escapes into the calculation graph on a status association.
 *
 * Note also that unlike the PowerProducer, this rule can *never* produce a null value, only 0 or 1.  (Either a whitelist value is found or it isn't.)
 */
public class ProducerStatus implements RuleAction {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(ProducerStatus.class);

	private Property _input
	public Property getInput() {
		return _input
	}
    public void setInput(Property i) {
		_input = i
	}


	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			Double finalValue = 0.0
			String whitelistStr = env.getPropertyValue( nativeTO, "whitelist", String.class )
			if ( whitelistStr ) {
				def whitelist = Arrays.asList(whitelistStr.split(","))
				whitelist = whitelist.collect{it.toLowerCase().trim()}
				//logger.trace("status producer whitelist is $whitelist")
				for(TO<?> t : input.getValue()) {
					def currentValue = env.getPropertyValue(t, "lastValue", Object.class).toString()
					if ( !currentValue ) {
						logger.warn("Null input to ProducerStatus ${triggeredRule.getName()}")
					} else {
						//if the currentValue is in the whitelist, return 1, else return 0
						//logger.trace("status producer input is $currentValue")
						currentValue = currentValue.toLowerCase().trim()
						//logger.trace("status producer input has become $currentValue")
						logger.trace("ProducerStatus: looking for '$currentValue' in $whitelist")
						if ( currentValue in whitelist ) {
							finalValue = 1
						} else {
							finalValue = 0
						}
					}
				}
			}
			logger.debug("StatusProducer ${triggeredRule.getName()} value is " + finalValue);
			return (double) finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}