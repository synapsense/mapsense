package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO
import com.synapsense.dto.ValueTO;;

public class LessThanFilter implements RuleAction {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(LessThanFilter.class);

	private Property _input
	public Property getInput() {
		return _input
	}
    public void setInput(Property i) {
		_input = i
	}

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			Double inputValue = env.getPropertyValue((TO<?>) input.getValue(), "lastValue", Double.class);
			Double finalValue = null
			def defaultValue = env.getPropertyValue( nativeTO, "defaultValue", Double.class )
			def minAllowed = env.getPropertyValue( nativeTO, "minAllowed", Double.class )
			Integer inputStatus = null;
			for(ValueTO v : env.getAllPropertiesValues((TO<?>) input.getValue())){
				if(v.propertyName.equalsIgnoreCase("status")){
					inputStatus = v.getValue()
				}
			}
			if ((inputStatus && inputStatus >= 2) || inputValue == null || inputValue < minAllowed ) {
				finalValue = defaultValue
			} else {
				finalValue = inputValue
			}
			logger.debug("Filter ${triggeredRule.getName()} ($minAllowed:$defaultValue) recieved $inputValue and returned $finalValue");
			return (double) finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}