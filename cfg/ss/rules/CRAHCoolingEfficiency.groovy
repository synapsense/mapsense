package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import java.awt.geom.Point2D;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO
import com.synapsense.dto.Alert
import com.synapsense.service.AlertService;
import com.synapsense.util.LocalizationUtils;

/**

 ActualTons = CFM * (RAT - SAT)

*/
public class CRAHCoolingEfficiency implements RuleAction {

	private Property tonsActual
	public Property getTonsActual() {
		return tonsActual
	}
    public void setTonsActual(Property i) {
		tonsActual = i
	}

	private Property tonsDesign
	public Property getTonsDesign() {
		return tonsDesign
	}
    public void setTonsDesign(Property i) {
		tonsDesign = i
	}


	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(CRAHCoolingEfficiency.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			//check for deactivated status
			Integer status = (Integer) env.getPropertyValue(nativeTO, "status", Integer.class)
			if ( status >= 2 ){
				logger.debug("Deactivated Status, returning null.")
				return null
			}

			Double finalValue = 0.0

			if ( tonsActual.getValue() == null || tonsDesign.getValue() == null ) {
				logger.warn("Null input to ${triggeredRule.getName()}")
				return null
			}
			if ( tonsDesign.getValue() == 0 ){
				logger.warn("Zero Design Tons to ${triggeredRule.getName()}")
				throwAlert(triggeredRule, calculated, nativeTO, LocalizationUtils.getLocalizedString("alert_message_divide_by_zero"));
				return null
			}

			Double actual = tonsActual.getValue()
			Double design = tonsDesign.getValue()
			finalValue = ( actual / design ) * 100

			//return the final value
			logger.debug("CRAH EFF ${triggeredRule.getName()} results in $finalValue");
			return finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

	private throwAlert(RuleI triggeredRule, final Property calculated, TO<?> nativeTO, String message) {
		AlertService alertService = calculated.getAlertService();
		alertService.raiseAlert(
				//new Alert("Rack P3 SmartPlugs are not reporting", "Node not reporting", new Date(), "Multiple P3 SmartPlugs are not reporting. Unable to perform power calculations.", null, rack.getObjId())
				new Alert("Math Error", "Math Error Alert", new Date(), LocalizationUtils.getLocalizedString("alert_message_math_error_has_been_detected_in_rule", triggeredRule.getName()) + " $message", nativeTO)
		);
	}


}