
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO
import com.synapsense.dto.ValueTO;

/**
 * The ProducerPoint gets used as a translation layer in interface objects.
 * This is designed to work in concert with the PRODUCERPOINT object,
 * which is designed to be bolted as a child on to any existing object and
 * turn any arbitrary field into one that's "producer compatible".  This means that
 * it uses the fieldname setting on this object to find the field of the parent object
 * to hand out as the lastValue of the PRODUCERPOINT.
 * This way we can have object pointer links to any random field.
 *
 * Also, the rule will flip the PRODUCERPOINT's status field to 1 or 0 depending on if the input is null or not.
 *
 */
public class ProducerPoint implements RuleAction {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(ProducerPoint.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			Double finalValue = null
			def name = env.getPropertyValue(nativeTO, "name", String.class)
			def fieldname = env.getPropertyValue(nativeTO, "fieldname", String.class)
			def parents = env.getParents( nativeTO )
			def realValue = null
			Integer parentStatus = null
			for(TO<?> t : parents ) {
				realValue = env.getPropertyValue(t, fieldname, Double.class)
				def props = env.getAllPropertiesValues(t)
				for(ValueTO v : props){
					if(v.propertyName.equalsIgnoreCase("status")){
						parentStatus = v.getValue()
					}
				}
			}
			//check the status field of the parent
			if(parentStatus && parentStatus >= 2){
				logger.debug("Disabled input to ${triggeredRule.getName()}.")
				env.setPropertyValue(nativeTO, "status", 2)
				realValue = null
			} else if ( realValue == null || realValue < -1000 ){
				logger.debug("ProducerPoint ${triggeredRule.getName()} for $fieldname -> $name has received an invalid value (" + realValue +  ").")
				env.setPropertyValue(nativeTO, "status", 0)
			} else {
				//we got a value, status to 1
				env.setPropertyValue(nativeTO, "status", 1)
			}
			finalValue = realValue
			//logger.debug("ProducerPoint ${triggeredRule.getName()} value is " + finalValue);
			return finalValue
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}