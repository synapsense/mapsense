
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO
import com.synapsense.service.AlertService
import com.synapsense.dto.Alert;
import com.synapsense.util.LocalizationUtils;

public class Pue implements RuleAction {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(Pue.class);

	private Property itPowerInputs
	public Property getItPowerInputs() {
		return itPowerInputs
	}
    public void setItPowerInputs(Property i) {
		itPowerInputs = i
	}

	private Property powerLossInputs
	public Property getPowerLossInputs() {
		return powerLossInputs
	}
    public void setPowerLossInputs(Property i) {
		powerLossInputs = i
	}

	private Property lightingPowerInputs
	public Property getLightingPowerInputs() {
		return lightingPowerInputs
	}
    public void setLightingPowerInputs(Property i) {
		lightingPowerInputs = i
	}

	private Property coolingPowerInputs
	public Property getCoolingPowerInputs() {
		return coolingPowerInputs
	}
    public void setCoolingPowerInputs(Property i) {
		coolingPowerInputs = i
	}

	private Property infrastructurePower
	public Property getInfrastructurePower() {
		return infrastructurePower
	}
	public void setInfrastructurePower(Property i) {
		infrastructurePower = i
	}

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			/*
			//original version
			Double itPower = env.getPropertyValue(nativeTO, "itPower", Double.class);
			Double coolingPower = env.getPropertyValue(nativeTO, "coolingPower", Double.class);
			Double lightingPower = env.getPropertyValue(nativeTO, "lightingPower", Double.class);
			Double powerLoss = env.getPropertyValue(nativeTO, "powerLoss", Double.class);
            */

			/*
			//using dependent collections
			Double itPower = 0
			Double coolingPower = 0
			Double lightingPower = 0
			Double powerLoss = 0

			for(TO<?> t : itPowerInputs.getValue()) {
				itPower += (double)(env.getPropertyValue(t, "lastValue", Double.class) ?: 0)
			}
			for(TO<?> t : powerLossInputs.getValue()) {
				powerLoss += (double)(env.getPropertyValue(t, "lastValue", Double.class) ?: 0)
			}
			for(TO<?> t : lightingPowerInputs.getValue()) {
				lightingPower += (double)(env.getPropertyValue(t, "lastValue", Double.class) ?: 0)
			}
			for(TO<?> t : coolingPowerInputs.getValue()) {
				coolingPower += (double)(env.getPropertyValue(t, "lastValue", Double.class) ?: 0)
			}
			*/

			//depending on other rules
			Double itPower = itPowerInputs.getValue()
			Double coolingPower = coolingPowerInputs.getValue()
			Double lightingPower = lightingPowerInputs.getValue()
			Double powerLoss = powerLossInputs.getValue()

			if(itPower == null || coolingPower == null || lightingPower == null || powerLoss == null) {
				logger.warn("PUE: One or more PUE inputs is NULL");
				//return null;
			}
			if(itPower == null || lightingPower == null) {
				logger.warn("PUE: Either the itPower or lighting inputs to PUE are NULL, returning NULL");
				return null;
			}
			if(coolingPower == null ^ powerLoss == null) {
				logger.warn("PUE: One or both of coolingPower and powerLoss are NULL, returning NULL");
				return null;
			}

			double totalPower = itPower + (coolingPower?:0) + (lightingPower?:0) + (powerLoss?:0);
			Double pue = totalPower / itPower;
			if(pue == Double.NaN || pue == Double.POSITIVE_INFINITY || pue == Double.NEGATIVE_INFINITY) {
				logger.warn(calculated.getName() + " value calculated to '" + pue + "' returning null");
				throwAlert(triggeredRule, calculated, nativeTO, LocalizationUtils.getLocalizedString("alert_message_unacceptable_math_result", "$pue"));
				return null
			} else {
				//logger.debug(calculated.getName() + " returning " + pue);
				return pue;
			}
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
	private throwAlert(RuleI triggeredRule, final Property calculated, TO<?> nativeTO, String message) {
		AlertService alertService = calculated.getAlertService();
		alertService.raiseAlert(
				new Alert("Math Error", "Math Error Alert", new Date(), LocalizationUtils.getLocalizedString("alert_message_math_error_has_been_detected_in_rule", triggeredRule.getName()) + " $message", nativeTO)
		);
	}
}