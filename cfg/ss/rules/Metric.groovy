
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO
import com.synapsense.dto.ValueTO;;

public class Metric implements RuleAction {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(Metric.class);

	private Property _input
	public Property getInput() {
		return _input
	}
    public void setInput(Property i) {
		_input = i
	}

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			//logger.trace("Metric Happening!");
			def props = env.getAllPropertiesValues((TO<?>) input.getValue())
			Integer status = null;
			for(ValueTO v : props){
				if(v.propertyName.equalsIgnoreCase("status")){
					status = v.getValue()
				}
			}
			if(status && status >= 2){
				logger.warn("Disabled input to ${triggeredRule.getName()}.")
				env.setPropertyValue(nativeTO, "status", 2)
				return null
			} else {
				env.setPropertyValue(nativeTO, "status", 1)
			}
			Double finalValue = env.getPropertyValue((TO<?>) input.getValue(), "lastValue", Double.class);
			//logger.debug("Metric ${triggeredRule.getName()} value is " + finalValue);
			return finalValue;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}
