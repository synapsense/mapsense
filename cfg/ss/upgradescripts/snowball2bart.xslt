<?xml version="1.0" encoding="UTF-8"?>

<!--
see wiki page for spec: http://wiki.synapsense.int/moinmoin/DeploymentLab/UpgradeProcess
-->

<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/02/xpath-functions"
	xmlns:dl="http://www.synapsense.com"
	exclude-result-prefixes="xsl xs fn dl"
	>
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	
	<xsl:template match="model">
		<model version="1.2">
			<xsl:copy-of select="*[not(self::objects) and not(self::components)]" />
			<xsl:apply-templates select="objects" />
			<xsl:apply-templates select="components" />
		</model>
	</xsl:template>

	<xsl:template match="objects">
		<objects>
			<xsl:attribute name="version">1.3</xsl:attribute>
			<xsl:attribute name="lastid">
				<xsl:value-of select="@lastid" />
			</xsl:attribute>
			<xsl:apply-templates select="*" />
		</objects>
	</xsl:template>
	
	<xsl:template match='object'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*" />
		</object>
	</xsl:template>

	<xsl:template match='object[@type="rack"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*" />
			<property name='nodeTemp' valuetype='dlid'>
				<xsl:value-of select='property[@name="cTop"]/text()' />
			</property>
		</object>
	</xsl:template>

	<xsl:template match='object[@type="sensor"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select='*[not(self::property[@name="subsamples"])]' />
			<property name="subSamples">
				<value><xsl:value-of select='property[@name="subsamples"]/value/text()' /></value>
				<oldvalue><xsl:value-of select='property[@name="subsamples"]/oldvalue/text()' /></oldvalue>
			</property>
		</object>
	</xsl:template>

	<xsl:template match='object[@type="modbusproperty"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*" />
			<property name="type"><value>UINT16</value></property>
			<property name="msw"><value>0</value></property>
		</object>
	</xsl:template>

	<xsl:template match="components">
		<components>
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:apply-templates select="*" />
		</components>
	</xsl:template>
	
	<xsl:template match='component'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
		</component>
	</xsl:template>

	<xsl:template match='component[@type="generic-constellation2"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>

			<xsl:copy-of select="*[not(self::consumer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>

			<producer id="channel3" datatype="power" name="Channel 3" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]' />
			<producer id="channel4" datatype="power" name="Channel 4" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]' />
			<producer id="channel5" datatype="power" name="Channel 5" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]' />
			<producer id="channel6" datatype="power" name="Channel 6" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]' />
			<producer id="channel7" datatype="power" name="Channel 7" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]' />
			<producer id="channel8" datatype="power" name="Channel 8" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]' />
		</component>
	</xsl:template>

	<xsl:template match='component[@type="energy-constellation2"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<producer id="energy" datatype="power" name="Power" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]' />
		</component>
	</xsl:template>
	
	<xsl:template match='component[@type="singleflow-enthalpy-constellation2"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<producer id="energy" datatype="power" name="Energy (kWh per Sampling Interval)" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]' />
			<property name="maxflow" display="Max Flow" type="java.lang.Integer" editable="true" displayed="true" valueChoices="10 L/s; 159 gpm:10,30 L/s; 476 gpm:30,60 L/s; 951 gpm:60,100 L/s; 1585 gpm:100,300 L/s; 4755 gpm:300">30</property>
			<varbinding vars="maxflow"> <!-- delta -->
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="delta"]</property>
				<value>100 * (32768 + maxflow )</value>
			</varbinding>
			<varbinding vars="maxflow"> <!-- scaleMax -->
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="scaleMax"]</property>
				<value>maxflow * 15.85032</value>
			</varbinding>
		</component>
	</xsl:template>

	<xsl:template match='component[@type="dualflow-enthalpy-constellation2"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<producer id="energy" datatype="power" name="Energy (kWh per Sampling Interval)" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]' />
			<property name="maxflow" display="Max Flow" type="java.lang.Integer" editable="true" displayed="true" valueChoices="10 L/s; 159 gpm:10,30 L/s; 476 gpm:30,60 L/s; 951 gpm:60,100 L/s; 1585 gpm:100,300 L/s; 4755 gpm:300">30</property>
			<varbinding vars="maxflow"> <!-- delta -->
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="delta"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="delta"]</property>
				<value>100 * (32768 + maxflow )</value>
			</varbinding>
			<varbinding vars="maxflow"> <!-- scaleMax -->
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="scaleMax"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="scaleMax"]</property>
				<value>maxflow * 15.85032</value>
			</varbinding>
		</component>
	</xsl:template>

	<xsl:template match='component[@type="equipment-status-constellation2"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<producer id="status" datatype="status" name="Equipment Status" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]' />
		</component>
	</xsl:template>

	<xsl:template match='component[@type="ct-constellation2"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<producer id="phaseA" datatype="power" name="Phase A Current (amps)" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]' />
			<producer id="phaseB" datatype="power" name="Phase B Current (amps)" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]' />
			<producer id="phaseC" datatype="power" name="Phase C Current (amps)" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]' />		
		</component>
	</xsl:template>

	<xsl:template match='component[@type="modbus-pdi-bcms-custom"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::property) and not(self::varbinding) and not(self::consumer) and not(self::display)]" />
			
			<xsl:apply-templates select='property[@name != "pullPeriod"]' />
			<property name="pullPeriod" display="Sample Interval (minutes)" type="java.lang.Integer" editable="true" displayed="true">5</property>

			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>

			<producer id='phasearealpower' datatype='power' name='Phase A Real Power' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Phase A Real Power"]]' />
			<producer id='phasebrealpower' datatype='power' name='Phase B Real Power' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Phase B Real Power"]]' />
			<producer id='phasecrealpower' datatype='power' name='Phase C Real Power' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Phase C Real Power"]]' />

			<producer id='powerchannel1' datatype='power' name='Power Channel 1' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 1"]]' />
			<producer id='powerchannel2' datatype='power' name='Power Channel 2' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 2"]]' />
			<producer id='powerchannel3' datatype='power' name='Power Channel 3' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 3"]]' />
			<producer id='powerchannel4' datatype='power' name='Power Channel 4' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 4"]]' />
			<producer id='powerchannel5' datatype='power' name='Power Channel 5' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 5"]]' />
			<producer id='powerchannel6' datatype='power' name='Power Channel 6' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 6"]]' />
			<producer id='powerchannel7' datatype='power' name='Power Channel 7' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 7"]]' />
			<producer id='powerchannel8' datatype='power' name='Power Channel 8' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 8"]]' />
			<producer id='powerchannel9' datatype='power' name='Power Channel 9' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 9"]]' />
			<producer id='powerchannel10' datatype='power' name='Power Channel 10' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 10"]]' />
			<producer id='powerchannel11' datatype='power' name='Power Channel 11' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 11"]]' />
			<producer id='powerchannel12' datatype='power' name='Power Channel 12' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 12"]]' />
			<producer id='powerchannel13' datatype='power' name='Power Channel 13' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 13"]]' />
			<producer id='powerchannel14' datatype='power' name='Power Channel 14' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 14"]]' />
			<producer id='powerchannel15' datatype='power' name='Power Channel 15' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 15"]]' />
			<producer id='powerchannel16' datatype='power' name='Power Channel 16' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 16"]]' />
			<producer id='powerchannel17' datatype='power' name='Power Channel 17' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 17"]]' />
			<producer id='powerchannel18' datatype='power' name='Power Channel 18' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 18"]]' />
			<producer id='powerchannel19' datatype='power' name='Power Channel 19' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 19"]]' />
			<producer id='powerchannel20' datatype='power' name='Power Channel 20' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 20"]]' />
			<producer id='powerchannel21' datatype='power' name='Power Channel 21' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 21"]]' />
			<producer id='powerchannel22' datatype='power' name='Power Channel 22' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 22"]]' />
			<producer id='powerchannel23' datatype='power' name='Power Channel 23' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 23"]]' />
			<producer id='powerchannel24' datatype='power' name='Power Channel 24' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 24"]]' />
			<producer id='powerchannel25' datatype='power' name='Power Channel 25' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 25"]]' />
			<producer id='powerchannel26' datatype='power' name='Power Channel 26' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 26"]]' />
			<producer id='powerchannel27' datatype='power' name='Power Channel 27' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 27"]]' />
			<producer id='powerchannel28' datatype='power' name='Power Channel 28' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 28"]]' />
			<producer id='powerchannel29' datatype='power' name='Power Channel 29' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 29"]]' />
			<producer id='powerchannel30' datatype='power' name='Power Channel 30' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 30"]]' />
			<producer id='powerchannel31' datatype='power' name='Power Channel 31' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 31"]]' />
			<producer id='powerchannel32' datatype='power' name='Power Channel 32' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 32"]]' />
			<producer id='powerchannel33' datatype='power' name='Power Channel 33' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 33"]]' />
			<producer id='powerchannel34' datatype='power' name='Power Channel 34' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 34"]]' />
			<producer id='powerchannel35' datatype='power' name='Power Channel 35' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 35"]]' />
			<producer id='powerchannel36' datatype='power' name='Power Channel 36' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 36"]]' />
			<producer id='powerchannel37' datatype='power' name='Power Channel 37' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 37"]]' />
			<producer id='powerchannel38' datatype='power' name='Power Channel 38' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 38"]]' />
			<producer id='powerchannel39' datatype='power' name='Power Channel 39' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 39"]]' />
			<producer id='powerchannel40' datatype='power' name='Power Channel 40' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 40"]]' />
			<producer id='powerchannel41' datatype='power' name='Power Channel 41' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 41"]]' />
			<producer id='powerchannel42' datatype='power' name='Power Channel 42' object='$pdi/properties[name="registers"]/children[properties[name="name"][value="Power Channel 42"]]' />

			<xsl:apply-templates select="varbinding[@vars !='pullPeriod']"/>
			<varbinding vars="pullPeriod">
				<property>$pdi/properties[name="registers"]/children/properties[name="period"]</property>
				<value>pullPeriod*60.0</value>
			</varbinding>
		</component>
	</xsl:template>
	
	<xsl:template match='component[@type="pdi-pdu"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::property) and not(self::varbinding) and not(self::consumer) and not(self::display)]" />
			
			<xsl:apply-templates select='property' />

			<property name="location" display="Location" type="java.lang.String" editable="true" displayed="true"></property>
			
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="consumer"/>
			
			<xsl:apply-templates select="varbinding"/>	
			<varbinding vars="location">
				<property>$pdu/properties[name="location"]</property>
				<value>location</value>
			</varbinding>
		</component>
	</xsl:template>
	
	<xsl:template match="varbinding">
		<varbinding>
			<xsl:attribute name="vars"><xsl:value-of select="@vars"/></xsl:attribute>
			<xsl:copy-of select="*"/>
		</varbinding>
	</xsl:template>	
	
	<xsl:template match='property'>
		<property>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="display"><xsl:value-of select="@display" /></xsl:attribute>
			<xsl:attribute name="type"><xsl:value-of select="@type" /></xsl:attribute>
			<xsl:attribute name="editable"><xsl:value-of select="@editable" /></xsl:attribute>
			<xsl:attribute name="displayed"><xsl:value-of select="@displayed" /></xsl:attribute>
			<xsl:if test="@dimension">
				<xsl:attribute name="dimension"><xsl:value-of select="@dimension" /></xsl:attribute>
			</xsl:if>
			<xsl:if test="@valueChoices">
				<xsl:attribute name="valueChoices"><xsl:value-of select="@valueChoices" /></xsl:attribute>
			</xsl:if>
			<xsl:value-of select="." />
		</property>
	</xsl:template>

	<xsl:template match='property[@name="x" or @name="y"]'>
		<property>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="display"><xsl:value-of select="@display" /></xsl:attribute>
			<xsl:attribute name="type"><xsl:text>java.lang.Double</xsl:text></xsl:attribute>
			<xsl:attribute name="editable"><xsl:value-of select="@editable" /></xsl:attribute>
			<xsl:attribute name="displayed"><xsl:value-of select="@displayed" /></xsl:attribute>
			<xsl:attribute name="dimension"><xsl:value-of select="@dimension" /></xsl:attribute>
			<xsl:value-of select="." />
		</property>
	</xsl:template>
	
	<xsl:template match='consumer'>
		<consumer>
			<xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
			<xsl:attribute name="datatype"><xsl:value-of select="@datatype" /></xsl:attribute>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="property"><xsl:value-of select="@property" /></xsl:attribute>
			<xsl:attribute name="required"><xsl:value-of select="@required" /></xsl:attribute>
			<xsl:copy-of select="*" />
		</consumer>
	</xsl:template>

	<xsl:template match='producer'>
		<producer>
			<xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
			<xsl:attribute name="datatype"><xsl:value-of select="@datatype" /></xsl:attribute>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="object"><xsl:value-of select="@object" /></xsl:attribute>
			<xsl:copy-of select="*" />
		</producer>
	</xsl:template>

	<xsl:template match='display'>
		<display>
			<xsl:copy-of select="*[not(self::haloradius)]" />
			<xsl:choose>
				<xsl:when test='haloradius = "134.2"'><haloradius><xsl:text>151.4</xsl:text></haloradius></xsl:when>
				<xsl:when test='haloradius'><haloradius><xsl:value-of select="haloradius" /></haloradius></xsl:when>
			</xsl:choose>
		</display>
	</xsl:template>
</xsl:stylesheet>
