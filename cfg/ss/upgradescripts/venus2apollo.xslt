<?xml version="1.0" encoding="UTF-8"?>

<!--
see wiki page for spec: http://wiki.synapsense.int/moinmoin/DeploymentLab/UpgradeProcess
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/02/xpath-functions"
	xmlns:dl="http://www.synapsense.com"
	exclude-result-prefixes="xsl xs fn dl"
	>
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	
	<xsl:template match="model">
		<model version="1.5">
			<xsl:copy-of select="*[not(self::objects) and not(self::components)]" />
			<xsl:apply-templates select="objects" />
			<xsl:apply-templates select="components" />
		</model>
	</xsl:template>

	<xsl:template match="objects">
		<objects>
			<xsl:attribute name="version">1.6</xsl:attribute>
			<xsl:attribute name="lastid">
				<xsl:value-of select="@lastid" />
			</xsl:attribute>
			<xsl:apply-templates select="*" />
		</objects>
	</xsl:template>
	
	<xsl:template match='object'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*" />
		</object>
	</xsl:template>
	
	<!-- 1. orange band : controlalg_trimrespond -->
	<xsl:template match='object[@type="controlalg_trimrespond"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*" />
			
			<property name='maxUncontrollable'>
        		<value>10.0</value>
        		<oldvalue></oldvalue>
      		</property>

			<property name='failsafeTimeout'>
        		<value>1200.0</value>
        		<oldvalue></oldvalue>
      		</property>

			<property name='minAvgPressure'>
        		<value>0.015</value>
        		<oldvalue></oldvalue>
      		</property>
	
		</object>
	</xsl:template>

	<!-- 1. orange band : controlpoint_singlecompare -->
	<xsl:template match='object[@type="controlpoint_singlecompare"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			
			<xsl:copy-of select="*" />
			
			<xsl:variable name="controltype" select="property[@name='controltype']/value/text()"/>
			
			<xsl:choose>
				<xsl:when test='$controltype="pressure"'>
					<property name='alarmThreshold'>
        				<value>0.015</value>
        				<oldvalue></oldvalue>
      				</property>
				</xsl:when>
				<xsl:otherwise>
					<property name='alarmThreshold'>
        				<value>89.0</value>
        				<oldvalue></oldvalue>
      				</property>
				</xsl:otherwise>
			</xsl:choose>

			<property name='controllable'>
        		<value>1</value>
        		<oldvalue></oldvalue>
      		</property>
			
			
		</object>
	</xsl:template>

	<xsl:template match="components">
		<components>
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:apply-templates select="*" />
		</components>
	</xsl:template>
	
	<xsl:template match='component'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:call-template name="grouppath"><xsl:with-param name="cfg" select="@type" /></xsl:call-template>
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
		</component>
	</xsl:template>
	
	<xsl:template match="varbinding">
		<varbinding>
			<xsl:attribute name="vars"><xsl:value-of select="@vars"/></xsl:attribute>
			<xsl:copy-of select="*"/>
		</varbinding>
	</xsl:template>	
	
	<xsl:template match='property'>
		<property>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="display"><xsl:value-of select="@display" /></xsl:attribute>
			<xsl:attribute name="type"><xsl:value-of select="@type" /></xsl:attribute>
			<xsl:attribute name="editable"><xsl:value-of select="@editable" /></xsl:attribute>
			<xsl:attribute name="displayed"><xsl:value-of select="@displayed" /></xsl:attribute>
			<xsl:if test="@dimension">
				<xsl:attribute name="dimension"><xsl:value-of select="@dimension" /></xsl:attribute>
			</xsl:if>
			<xsl:if test="@valueChoices">
				<xsl:attribute name="valueChoices"><xsl:value-of select="@valueChoices" /></xsl:attribute>
			</xsl:if>
			<xsl:value-of select="." />
		</property>
	</xsl:template>
	
	<xsl:template match='consumer'>
		<consumer>
			<xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
			<xsl:attribute name="datatype"><xsl:value-of select="@datatype" /></xsl:attribute>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="property"><xsl:value-of select="@property" /></xsl:attribute>
			<xsl:attribute name="required"><xsl:value-of select="@required" /></xsl:attribute>
			<xsl:copy-of select="*" />
		</consumer>
	</xsl:template>

	<xsl:template match='producer'>
		<producer>
			<xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
			<xsl:attribute name="datatype"><xsl:value-of select="@datatype" /></xsl:attribute>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="object"><xsl:value-of select="@object" /></xsl:attribute>
			<xsl:copy-of select="*" />
		</producer>
	</xsl:template>

	<xsl:template match='display'>
		<display>
			<xsl:copy-of select="*[not(self::shape) and not(self::name) and not(self::description)]" />
			<xsl:if test='shape != ""'>
				<shape><xsl:call-template name="display-shape"><xsl:with-param name="cfg" select="../@type" /></xsl:call-template></shape>
			</xsl:if>
			<xsl:if test='name != ""'>
				<name><xsl:call-template name="display-name"><xsl:with-param name="cfg" select="../@type" /></xsl:call-template></name>
			</xsl:if>
			<xsl:if test='description != ""'>
				<description><xsl:call-template name="display-description"><xsl:with-param name="cfg" select="../@type" /></xsl:call-template></description>
			</xsl:if>
		</display>
	</xsl:template>
	
	<!-- upgraded components -->
	<!-- Dispaly tempalte. -->
	<xsl:template name="display-shape">
		<xsl:param name="cfg" />
		<!-- xsl:choose>
			<xsl:when test='$cfg = "rack-control-rearexhaust-sf-thermanode2"'><xsl:text>rack-rearexhaust-sf-rt</xsl:text></xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="shape" />
			</xsl:otherwise>
		</xsl:choose-->
		<xsl:value-of select="shape" />
	</xsl:template>
	
	<!--  Display name Change -->
	<xsl:template name="display-name">
		<xsl:param name="cfg" />
		<xsl:choose>
			<xsl:when test='$cfg = "controlpoint_single_pressure"'><xsl:text>Differential Pressure</xsl:text></xsl:when>
			<xsl:when test='$cfg = "controlpoint_single_temperature"'><xsl:text>Top Intake </xsl:text></xsl:when>
			<xsl:when test='$cfg = "controlpoint_single_temperature_mid"'><xsl:text>Middle Intake</xsl:text></xsl:when>
			<xsl:when test='$cfg = "controlpoint_single_temperature_bot"'><xsl:text>Bottom Intake</xsl:text></xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="name" />
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
	<!--   All Display Descriptions have changed -->
	<xsl:template name="display-description">
		<xsl:param name="cfg" />
		<xsl:choose>
			<xsl:when test='$cfg = "crah-single-plenumrated-thermanode"'><xsl:text>A CRAH monitoring assembly consisting of one node (with built-in humidity sensor) and two external thermistors which sense supply and return air temperature. The node is placed in the CRAH return air stream for collecting return humidity readings.  Provides Rack Reference Temperature from the Supply Temperature.  This Component goes in a Zone and a WSN Data Source.  This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "pressure2"'><xsl:text>A sensor node used to capture subfloor differential pressure.  This Component goes in a Zone and a WSN Data Source.  This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "control-verticalstring-li-thermanode"'><xsl:text>A ThermaNode and sensor assembly consisting of 3 thermistors.  Sense points will appear in LI images on Top, Middle and Bottom layers.  This Component goes in a Zone and a WSN Data Source.  This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>

			<xsl:when test='$cfg = "controlpoint_single_pressure"'><xsl:text>A virtual element to capture the differential pressure from its associated pressure monitoring device and make this pressure available for SynapSense Active Control™.  This Component goes in a Region of Influence and does not require a Data Source.  It must be attached to a Component that provides Pressure.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "controlpoint_single_temperature"'><xsl:text>A virtual element to capture the top intake temperature from its associated rack and make this temperature available for SynapSense Active Control™.  This Component goes in a Region of Influence and does not require a Data Source.  It must be attached to a Component that provides Temperature.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "controlpoint_single_temperature_mid"'><xsl:text>A virtual element to capture the Middle intake temperature from its associated rack and make this temperature available for SynapSense Active Control™.  This Component goes in a Region of Influence and does not require a Data Source.  It must be attached to a Component that provides Temperature.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "controlpoint_single_temperature_bot"'><xsl:text>A virtual element to capture the Bottom intake temperature from its associated rack and make this temperature available for SynapSense Active Control™.  This Component goes in a Region of Influence and does not require a Data Source.  It must be attached to a Component that provides Temperature.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "control_vfd_trim"'><xsl:text>A virtual element that represents a Variable Frequency Drive (VFD) that is dynamically controlled by the SynapSense Active Control™ module. The VFD manages the fan speed of the motor in the CRAH unit.  This Component goes in a Region of Influence and does not require a Data Source.  It must be attached to an Environmental CRAH.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "control_crah_trim"'><xsl:text>A virtual element that represents a Computer Room Air Handler (CRAH) unit that is dynamically controlled by the SynapSense Active Control™ module.  This Component goes in a Region of Influence and does not require a Data Source.  It must be attached to an Environmental CRAH.</xsl:text></xsl:when>

			<xsl:when test='$cfg = "rack-control-rearexhaust-nsf-rt-thermanode"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 5 external sensors. Designed for front inlet, rear exhaust configurations. Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top and exhaust middle/bottom.  This rack goes in a Zone and requires a WSN Data Source.  This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-rearexhaust-sf-thermanode"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors.  Designed for front inlet, rear exhaust configurations.  Node is installed at rack top, sense points are located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom, subfloor.  This rack goes in a Zone and requires a WSN Data Source.  This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-toprearexhaust-nsf-rt-6t"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors. Designed for front inlet, rear exhaust configurations. Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom,  chimney exhaust, exhaust top and exhaust middle/bottom.  This rack goes in a Zone and requires a WSN Data Source.  This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-toprearexhaust-sf-6t"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors.  Designed for front inlet, chimney/rear exhaust configurations.  Node is installed at rack top, sense points are located at cabinet intake top, intake middle, intake bottom, Chimney exhaust, exhaust top and subfloor.  This rack goes in a Zone and requires a WSN Data Source.  This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-lhs-endcap-rt-6t"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors located on a left end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom and side middle.  This rack goes in a Zone and requires a WSN Data Source.  This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-rhs-endcap-rt-6t"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors located on a right end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom and side middle. This rack goes in a Zone and requires a WSN Data Source. This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-rearexhaust-nsf-nrt-thermanode"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 5 external sensors.  Designed for front inlet, rear exhaust configurations.  Node is installed at rack top with sense points  located at cabinet intake top, intake middle, intake bottom, exhaust top, and exhaust middle/bottom.  This rack goes in a Zone and requires a WSN Data Source.  This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-toprearexhaust-nsf-nrt-6t"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors. Designed for front inlet, rear exhaust configurations.    Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom,  chimney exhaust, exhaust top and exhaust middle/bottom.  This rack goes in a Zone and requires a WSN Data Source. This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-lhs-endcap-nrt-6t"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors located on a left end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom and side middle.  This rack goes in a Zone and requires a WSN Data Source. This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-rhs-endcap-nrt-6t"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors located on a right end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom and side middle.  This rack goes in a Zone and requires a WSN Data Source. This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>

			<xsl:when test='$cfg = "dual-horizontal-row-thermanode2"'><xsl:text>Two arrays of sensors, each with a ThermaNode, used to monitor the horizontal temperature distribution at various points along the top of a row of racks.  The two arrays are placed one on the front and back of the racks. Each ThermaNode has 6 external thermistors displaced from the center where the ThermaNode is located.  A pair of sensors from each ThermaNode is represented as a Rack.  This component goes in a Zone and requires a WSN Data Source.  This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "single-horizontal-row-thermanode2"'><xsl:text>Array of sensors used to monitor the horizontal temperature distribution at various points in a row of racks at the top of each rack.  There are 6 external thermistors, each displaced from the center where the ThermaNode is located.  This component goes in a Zone and requires a WSN Data Source. This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>


			<!-- TN2 Lib -->
			<xsl:when test='$cfg= "rack-control-rearexhaust-nsf-rt-thermanode2"'><xsl:text>>Rack monitoring assembly consisting of a ThermaNode II equipped with 6 external sensors. Designed for front inlet, rear exhaust configurations. Node is installed at rack top with sense points located at cabinet cold top, cold middle, cold bottom, hot top, hot middle and hot bottom.  This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-rearexhaust-sf-thermanode2"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors. Designed for front inlet, rear exhaust configurations. Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle, exhaust bottom and subfloor. This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-toprearexhaust-sf-thermanode2"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors.  Designed for front inlet, top/rear exhaust configurations.  Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, chimney exhaust, exhaust top, exhaust mid/bottom and subfloor.  This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-toprearexhaust-nsf-rt-thermanode2"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors. Designed for front inlet, rear exhaust configurations. Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom,  chimney exhaust, exhaust top, exhaust middle, exhaust bottom. This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-lhs-endcap-rt-thermanode2"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors located on a left end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom, side top and side middle/bottom. This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-rhs-endcap-rt-thermanode2"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors located on a right end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom, side top and side middle/bottom. This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>

			<xsl:when test='$cfg= "rack-control-toprearexhaust-nsf-nrt-thermanode2"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors. Designed for front inlet, rear exhaust configurations.    Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom,  chimney exhaust, exhaust top, exhaust middle, exhaust bottom. This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-lhs-endcap-nrt-thermanode2"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors located on a left end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom, side top and side middle/bottom. This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-rhs-endcap-nrt-thermanode2"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors located on a right end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom, side top and side middle/bottom. This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>

			<!-- 5.2.1 Lib -->
			<xsl:when test='$cfg= "rack-control-rearexhaust-nsf-nrt-thermanode2"'><xsl:text>>Rack monitoring assembly consisting of a ThermaNode II equipped with 6 external sensors.  Designed for front inlet, rear exhaust configurations.  Node is installed at rack top with sense points are located at cabinet cold top, cold middle, cold bottom, hot top, hot middle and hot bottom.  This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-lhs-endcap-rt-thermanode2"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors located on a left end of an aisle (facing the cold aisle). Node is installed at rack top with sense points located at cabinet cold top, cold middle, cold bottom, hot top, hot middle/bottom, side top and side middle/bottom. This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-lhs-endcap-nrt-thermanode2"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors located on a left end of an aisle (facing the cold aisle). Node is installed at rack top with sense points located at cabinet cold top, cold middle, cold bottom, hot top, hot middle/bottom, side top and side middle/bottom. This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-rhs-endcap-rt-thermanode2"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors located on a right end of an aisle (facing the cold aisle). Node is installed at rack top with sense points located at cabinet cold top, cold middle, cold bottom, hot top, hot middle/bottom, side top and side middle/bottom. This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-rhs-endcap-nrt-thermanode2"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors located on a right end of an aisle (facing the cold aisle). Node is installed at rack top with sense points located at cabinet cold top, cold middle, cold bottom, hot top, hot middle/bottom, side top and side middle/bottom. This configuration is qualified for SynapSense Active Control™.</xsl:text></xsl:when>

			<xsl:otherwise>
				<xsl:value-of select="description" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- 2) Row Lite -->
	<xsl:template match='component[@type="dual-horizontal-row-thermanode2"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<property name="blank" display="blank" type="java.lang.String" editable="false" displayed="false"></property>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding[not(@vars="min_allow_t") and not(@vars="max_allow_t") and not(@vars="min_recommend_t") and not(@vars="max_recommend_t")]' />
			<varbinding vars="min_allow_t">
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="AMin"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="AMin"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="AMin"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="AMin"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="AMin"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="AMin"]</property>
				<value>min_allow_t</value>
			</varbinding>
			<varbinding vars="max_allow_t">
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="AMax"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="AMax"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="AMax"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="AMax"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="AMax"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="AMax"]</property>
	
				<value>max_allow_t</value>
			</varbinding>
			<varbinding vars="min_recommend_t">
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="RMin"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="RMin"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="RMin"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="RMin"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="RMin"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="RMin"]</property>
				
				<value>min_recommend_t</value>
			</varbinding>
			<varbinding vars="max_recommend_t">
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="RMax"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="RMax"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="RMax"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="RMax"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="RMax"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="RMax"]</property>
	
				<value>max_recommend_t</value>
			</varbinding>
			<!-- blank out the exhaust-side -->
			<varbinding vars="blank">
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="AMin"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="AMin"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="AMin"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="AMin"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="AMin"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="AMin"]</property>

				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="AMax"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="AMax"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="AMax"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="AMax"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="AMax"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="AMax"]</property>

				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="RMin"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="RMin"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="RMin"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="RMin"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="RMin"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="RMin"]</property>

				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="RMax"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="RMax"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="RMax"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="RMax"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="RMax"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="RMax"]</property>

				<value>blank</value>
			</varbinding>
			
		</component>
	</xsl:template>
	
	<xsl:template match='component[@type="control_crah_trim"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
		
			<property name='valveControl' display='Valve Control' type='java.lang.Integer' editable='true' displayed='true'
			valueChoices='Return Air Temperature:1,Supply Air Temperature:2' >1
			</property>
		
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<varbinding vars='valveControl'> <property>$controlout_crah/properties[name="valveControl"]</property> <value>valveControl</value> </varbinding>
			
		</component>
	</xsl:template>

	<!-- 7) horizontal row to LI -->
	<xsl:template match='component[@type="single-horizontal-row-thermanode2"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>

			<property name="li_layer" display="LI Layer" type="java.lang.Integer" editable="true" displayed="true" valueChoices="None:0,Top:131072,Middle:16384,Bottom:2048,Subfloor:256">131072</property>

			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>

			<varbinding vars="li_layer">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="visualLayer"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="visualLayer"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="visualLayer"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="visualLayer"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="visualLayer"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="visualLayer"]</property>
				<value>li_layer</value>
			</varbinding>

		</component>
	</xsl:template>


	<!-- Fix grouppaths for 5.2.1 TN2 components -->
	<xsl:template name="grouppath">
		<xsl:param name="cfg" />
		<xsl:choose>
			<xsl:when test='$cfg= "rack-control-rearexhaust-nsf-nrt-thermanode2"'><xsl:text>Environmentals;Legacy 5.2.1</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-lhs-endcap-rt-thermanode2"'><xsl:text>Environmentals;Legacy 5.2.1</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-lhs-endcap-nrt-thermanode2"'><xsl:text>Environmentals;Legacy 5.2.1</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-rhs-endcap-rt-thermanode2"'><xsl:text>Environmentals;Legacy 5.2.1</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-rhs-endcap-nrt-thermanode2"'><xsl:text>Environmentals;Legacy 5.2.1</xsl:text></xsl:when>

			<xsl:otherwise>
				<xsl:value-of select="@grouppath" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


</xsl:stylesheet>
