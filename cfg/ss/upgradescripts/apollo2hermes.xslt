<?xml version="1.0" encoding="UTF-8"?>

<!--
see wiki page for spec: http://wiki.synapsense.int/moinmoin/DeploymentLab/UpgradeProcess
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/02/xpath-functions"
	xmlns:dl="http://www.synapsense.com"
	exclude-result-prefixes="xsl xs fn dl"
	>
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	
	<xsl:template match="model">
		<model version="1.6">
			<xsl:copy-of select="*[not(self::objects) and not(self::components)]" />
			<xsl:apply-templates select="objects" />
			<xsl:apply-templates select="components" />
		</model>
	</xsl:template>

	<xsl:template match="objects">
		<objects>
			<xsl:attribute name="version">1.7</xsl:attribute>
			<xsl:attribute name="lastid">
				<xsl:value-of select="@lastid" />
			</xsl:attribute>
			<xsl:apply-templates select="*" />
		</objects>
	</xsl:template>
	
	<xsl:template match='object'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*" />
		</object>
	</xsl:template>


	<xsl:template match='object[@type="controlout_vfd" or @type="controlout_crah"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*" />
			<property name='validated'>
        		<value>1</value>
        		<oldvalue></oldvalue>
      		</property>
			<property name='engaged'>
        		<value>1</value>
        		<oldvalue></oldvalue>
      		</property>
			<property name='unitstatus'>
        		<value>text/online</value>
        		<oldvalue></oldvalue>
      		</property>
		</object>
	</xsl:template>


	<xsl:template match="components">
		<components>
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:apply-templates select="*" />
		</components>
	</xsl:template>
	
	<xsl:template match='component'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select="varbinding"/>
		</component>
	</xsl:template>
	
	<xsl:template match="varbinding">
		<varbinding>
			<xsl:attribute name="vars"><xsl:value-of select="@vars"/></xsl:attribute>
			<xsl:copy-of select="*"/>
		</varbinding>
	</xsl:template>	
	
	<xsl:template match='property'>
		<property>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="display"><xsl:value-of select="@display" /></xsl:attribute>
			<xsl:attribute name="type"><xsl:value-of select="@type" /></xsl:attribute>
			<xsl:attribute name="editable"><xsl:value-of select="@editable" /></xsl:attribute>
			<xsl:attribute name="displayed"><xsl:value-of select="@displayed" /></xsl:attribute>
			<xsl:if test="@dimension">
				<xsl:attribute name="dimension"><xsl:value-of select="@dimension" /></xsl:attribute>
			</xsl:if>
			<xsl:if test="@valueChoices">
				<xsl:attribute name="valueChoices"><xsl:value-of select="@valueChoices" /></xsl:attribute>
			</xsl:if>
			<xsl:value-of select="." />
		</property>
	</xsl:template>
	
	<xsl:template match='consumer'>
		<consumer>
			<xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
			<xsl:attribute name="datatype"><xsl:value-of select="@datatype" /></xsl:attribute>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="property"><xsl:value-of select="@property" /></xsl:attribute>
			<xsl:attribute name="required"><xsl:value-of select="@required" /></xsl:attribute>
			<xsl:copy-of select="*" />
		</consumer>
	</xsl:template>

	<xsl:template match='producer'>
		<producer>
			<xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
			<xsl:attribute name="datatype"><xsl:value-of select="@datatype" /></xsl:attribute>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="object"><xsl:value-of select="@object" /></xsl:attribute>
			<xsl:copy-of select="*" />
		</producer>
	</xsl:template>

	<xsl:template match='display'>
		<display>
			<xsl:copy-of select="*[not(self::shape) and not(self::name) and not(self::description)]" />
			<xsl:if test='shape != ""'>
				<shape><xsl:call-template name="display-shape"><xsl:with-param name="cfg" select="../@type" /></xsl:call-template></shape>
			</xsl:if>
			<xsl:if test='name != ""'>
				<name><xsl:call-template name="display-name"><xsl:with-param name="cfg" select="../@type" /></xsl:call-template></name>
			</xsl:if>
			<xsl:if test='description != ""'>
				<description><xsl:call-template name="display-description"><xsl:with-param name="cfg" select="../@type" /></xsl:call-template></description>
			</xsl:if>
		</display>
	</xsl:template>
	
	<!-- upgraded components -->
	<!-- Display template. -->
	<xsl:template name="display-shape">
		<xsl:param name="cfg" />
		<!-- xsl:choose>
			<xsl:when test='$cfg = "rack-control-rearexhaust-sf-thermanode2"'><xsl:text>rack-rearexhaust-sf-rt</xsl:text></xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="shape" />
			</xsl:otherwise>
		</xsl:choose-->
		<xsl:value-of select="shape" />
	</xsl:template>
	
	<!--  2-2) Display name Change -->
	<xsl:template name="display-name">
		<xsl:param name="cfg" />
		<!-- xsl:choose>
			<xsl:when test='$cfg = "power_inspector"'><xsl:text>Calculation Inspector</xsl:text></xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="name" />
			</xsl:otherwise>
		</xsl:choose-->
		<xsl:value-of select="name" />
	</xsl:template>
	
	<!-- 2-3)  All Display Descriptions have changed -->
	<xsl:template name="display-description">
		<xsl:param name="cfg" />
		<xsl:value-of select="description" />

		<!-- xsl:choose>
			<xsl:when test='$cfg = "pue_fixed_value_switch"'><xsl:text>Produces one of two fixed values based on a Status input. Has a single Equipment Status Consumer, and a single numeric Producer. If the input Status is ON, the Producer supplies the contents of the Fixed Value property. If the input Status is OFF, the Producer supplies the contents of the Default Value property. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="description" />
			</xsl:otherwise>
		</xsl:choose-->
	</xsl:template>
	
</xsl:stylesheet>
