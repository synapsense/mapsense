<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/02/xpath-functions"
	xmlns:dl="http://www.synapsense.com"
	exclude-result-prefixes="xsl xs fn dl"
	>
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

<!--
5.0.x -> 5.1 changes:
 * Object model version 1.1 -> 1.2
 * Properties added to racks:
  * pCircuits
  * bCircuits

-->

	<xsl:template match="model">
		<model version="1.1">
			<settings>
				<xsl:copy-of select="imagePath" />
				<esHost><xsl:value-of select="eshost" /></esHost>
				<unit>Imperial US</unit>
			</settings>
			<xsl:apply-templates select="objects" />
			<xsl:apply-templates select="components" />
		</model>
	</xsl:template>


	<xsl:template match="objects">
		<objects>
			<xsl:attribute name="version">1.2</xsl:attribute>
			<xsl:attribute name="lastid">
				<xsl:value-of select="@lastid" />
			</xsl:attribute>
			<xsl:apply-templates select="*" />
		</objects>
	</xsl:template>

	<xsl:template match='object[@type="dc"]'>
		<object type="dc">
			<xsl:copy-of select='*[not(self::property[@name = "IT_energy"]) and not(self::property[@name = "Infra_energy"]) and not(self::property[@name = "Energy_saved"]) and not(self::property[@name = "Baseline_PUE"]) and not(self::property[@name = "CO2_abated_index"]) and not(self::property[@name = "Lighting_energy"])]' />
		</object>
	</xsl:template>

	<xsl:template match='object[@type="rack"]'>
		<object type="rack">
			<xsl:copy-of select="*[not(self::refid)]" />
			<property name='pCircuits' />
			<property name='bCircuits' />
		</object>
	</xsl:template>

	<xsl:template match='object[@type="sensor"]'>
		<object type="sensor">
			<xsl:copy-of select='*[not(self::property[@name = "dataClass"])]' />
		</object>
	</xsl:template>

	<xsl:template match='object'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::refid)]" />
		</object>
	</xsl:template>


	<xsl:template match="components">
		<components>
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:apply-templates select="*" />
		</components>
	</xsl:template>

	<xsl:template match='component[@type="network"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select='property[@name != "version"]' />
			<property name='version' display='Network Version' type='java.lang.String' editable='true' displayed='true' valueChoices='N28:N28,N29:N29'>N28</property>
			<xsl:apply-templates select="bindingpost" />
			<xsl:apply-templates select="bindingslot" />
			<xsl:copy-of select='*[not(self::display) and not(self::property) and not(self::bindingpost) and not(self::bindingslot) and not(self::varbinding[@vars="pan_id"])]' />
			<varbinding vars="pan_id">
				<property>$network/properties[name="panId"]</property>
				<value>if(pan_id == "") { return 0; } else if(pan_id.length() == 4 &amp;&amp; pan_id.matches("[0-9a-fA-F]+")) { return Integer.parseInt(pan_id, 16); } else { return 0; }</value>
			</varbinding>
		</component>
	</xsl:template>

	<xsl:template match='component[contains(@type,"rack")]'>
		<xsl:variable name="type" select="@type" />
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property" />
			<xsl:apply-templates select="bindingpost" />
			<xsl:apply-templates select="bindingslot" />
			<xsl:copy-of select='*[not(self::display) and not(self::property) and not(self::bindingpost) and not(self::bindingslot)]' />

			<consumer id="primaryCircuits" datatype="circuit" name="Primary Circuits" collection="true" property='$rack/properties[name="pCircuits"]' required="false" />
			<consumer id="backupCircuits" datatype="circuit" name="Backup Circuits" collection="true" property='$rack/properties[name="bCircuits"]' required="false" />

			<xsl:if test='contains(@type, "thermanode")'>
				<property name="cold_delta" display="Cold Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">5</property>
				<property name="hot_delta" display="Hot Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">10</property>
				<xsl:choose>
					<xsl:when test='contains(@type, "endcap")'>
						<varbinding vars="cold_delta">
							<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="delta"]</property>
							<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="delta"]</property>
							<value>(int)(cold_delta*100)</value>
						</varbinding>
						<varbinding vars="hot_delta">
							<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="delta"]</property>
							<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="delta"]</property>
							<value>(int)(hot_delta*100)</value>
						</varbinding>
					</xsl:when>
					<xsl:otherwise>
						<varbinding vars="cold_delta">
							<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="delta"]</property>
							<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="delta"]</property>
							<value>(int)(cold_delta*100)</value>
						</varbinding>
						<varbinding vars="hot_delta">
							<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="delta"]</property>
							<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="delta"]</property>
							<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="delta"]</property>
							<value>(int)(hot_delta*100)</value>
						</varbinding>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</component>
	</xsl:template>

	<xsl:template match='component'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property" />
			<xsl:apply-templates select="bindingpost" />
			<xsl:apply-templates select="bindingslot" />
			<xsl:apply-templates select="varbinding" />
			<macids>
				<xsl:attribute name="value">
					<xsl:choose>
						<xsl:when test='@type = "remote-gateway"'><xsl:text>uid</xsl:text></xsl:when>
						<xsl:otherwise><xsl:value-of select="macids/@value" /></xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
			</macids>
			<xsl:copy-of select="*[not(self::display) and not(self::property) and not(self::bindingpost) and not(self::bindingslot) and not(self::macids) and not(self::varbinding)]" />
			<!-- xsl:if test='@type = "reftemp-thermanode"'>
				<producer id="reftemp" datatype="reftemp" name="Reference Temperature" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]' />
			</xsl:if-->
		</component>
	</xsl:template>

	<xsl:template name="shape-name">
		<xsl:param name="cfg" />
		<xsl:choose>
			<xsl:when test='$cfg = "rack-interior-nsf-rt-thermanode"'><xsl:text>triangle_orange</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-interior-nsf-nrt-thermanode"'><xsl:text>triangle_red</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-interior-fusion"'><xsl:text>triangle_fusion</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-interior-sf-thermanode"'><xsl:text>triangle_sf</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-lhs-endcap-rt-thermanode"'><xsl:text>triangle_l_orange</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-lhs-endcap-nrt-thermanode"'><xsl:text>triangle_l_red</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-rhs-endcap-rt-thermanode"'><xsl:text>triangle_r_orange</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-rhs-endcap-nrt-thermanode"'><xsl:text>triangle_r_red</xsl:text></xsl:when>
			<xsl:when test='$cfg = "crah-thermanode"'><xsl:text>single_crac</xsl:text></xsl:when>
			<xsl:when test='$cfg = "crah-dual-fusion"'><xsl:text>dual_crac</xsl:text></xsl:when>
			<xsl:when test='$cfg = "crah-dual-thermanode"'><xsl:text>dual_crac</xsl:text></xsl:when>
			<xsl:when test='$cfg = "pressure"'><xsl:text>octagon</xsl:text></xsl:when>
			<xsl:when test='$cfg = "pressure2"'><xsl:text>octagon</xsl:text></xsl:when>
			<xsl:when test='$cfg = "energy-constellation"'><xsl:text>dome_orange</xsl:text></xsl:when>
			<xsl:when test='$cfg = "flow-constellation"'><xsl:text>wave</xsl:text></xsl:when>
			<xsl:when test='$cfg = "pipe-temp-thermanode"'><xsl:text>plus_blue</xsl:text></xsl:when>
			<xsl:when test='$cfg = "reftemp-thermanode"'><xsl:text>star_orange</xsl:text></xsl:when>
			<xsl:when test='$cfg = "standalone-thermanode"'><xsl:text>star_red</xsl:text></xsl:when>
			<xsl:when test='$cfg = "37li-thermanode"'><xsl:text>pentagon</xsl:text></xsl:when>
			<xsl:when test='$cfg = "ct-constellation"'><xsl:text>dome</xsl:text></xsl:when>
			<xsl:when test='$cfg = "leakdetector-constellation"'><xsl:text>plus_magenta</xsl:text></xsl:when>
			<xsl:when test='$cfg = "door-constellation"'><xsl:text>door</xsl:text></xsl:when>
			<xsl:when test='$cfg = "dualflow-btu-constellation"'><xsl:text>dome</xsl:text></xsl:when>
			<xsl:when test='$cfg = "singleflow-btu-constellation"'><xsl:text>dome</xsl:text></xsl:when>
			<xsl:when test='$cfg = "remote-gateway"'><xsl:text>square</xsl:text></xsl:when>
			<xsl:otherwise>
				<xsl:message>No shape defined for configuration "<xsl:value-of select="$cfg" />"</xsl:message>
				<xsl:text></xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match='display'>
		<display>
			<xsl:copy-of select="*[not(self::shape) and not(self::color)]" />
			<xsl:if test='shape != ""'>
				<shape><xsl:call-template name="shape-name"><xsl:with-param name="cfg" select="../@type" /></xsl:call-template></shape>
			</xsl:if>
		</display>
	</xsl:template>

	<xsl:template name="dimension">
		<xsl:param name="property" />
		<xsl:variable name="name" select="$property/@name" />
		<xsl:choose>
			<xsl:when test='$name="depth" or $name="width" or $name="x" or $name="y"'>
				<xsl:text>distance</xsl:text>
			</xsl:when>
			<xsl:when test='$name="min_allow_t" or $name="max_allow_t" or $name="min_recommend_t" or $name="max_recommend_t"'>
				<xsl:text>temperature</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text></xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match='property'>
		<property>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="display"><xsl:value-of select="@display" /></xsl:attribute>
			<xsl:attribute name="type"><xsl:value-of select="@type" /></xsl:attribute>
			<xsl:attribute name="editable"><xsl:value-of select="@editable" /></xsl:attribute>
			<xsl:attribute name="displayed"><xsl:value-of select="@displayed" /></xsl:attribute>
			<xsl:attribute name="dimension">
				<xsl:call-template name="dimension"><xsl:with-param name="property" select="." /></xsl:call-template>
			</xsl:attribute>
			<xsl:if test='@valueChoices != ""'>
				<xsl:attribute name="valueChoices"><xsl:text>N28:N28,N29:N29</xsl:text></xsl:attribute>
			</xsl:if>
			<xsl:value-of select="." />
		</property>
	</xsl:template>

	<xsl:template match='bindingpost'>
		<producer>
			<xsl:attribute name="id">
				<xsl:text>reftemp</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="datatype">
				<xsl:text>reftemp</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="name">
				<xsl:text>Reference Temperature</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="object">
				<xsl:choose>
					<xsl:when test='starts-with(../@type, "rack")'>
						<xsl:text>$rack/properties[name="ref"]/referrent</xsl:text>
					</xsl:when>
					<xsl:when test='starts-with(../@type, "crah")'>
						<xsl:text>$crac/properties[name="supplyT"]/referrent</xsl:text>
					</xsl:when>
					<xsl:when test='../@type = "reftemp-thermanode"'>
						<xsl:text>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:message>Can't process bindingpost for object type <xsl:value-of select="../@type" />!</xsl:message>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:variable name="refSensorId">
				<xsl:choose>
					<xsl:when test='starts-with(../@type, "rack")'>
						<xsl:variable name="rackid" select='../object[@type = "rack"]/@dlid' />
						<xsl:value-of select='/model/objects/object[dlid/text() = $rackid]/property[@name="ref"]/text()' />
					</xsl:when>
					<xsl:when test='starts-with(../@type, "crah")'>
						<xsl:variable name="cracid" select='../object[@type = "crac"]/@dlid' />
						<xsl:value-of select='/model/objects/object[dlid/text() = $cracid]/property[@name="supplyT"]/text()' />
					</xsl:when>
					<xsl:when test='../@type = "reftemp-thermanode"'>
						<xsl:variable name="nodeid" select='../object[@type = "node"]/@dlid' />
						<xsl:value-of select='/model/objects/object[parentid/text() = $nodeid and property[@name="channel"]/value/text() = "0"]/dlid/text()' />
					</xsl:when>
					<xsl:otherwise>
						<xsl:message>Can't process bindingpost for object type <xsl:value-of select="../@type" />!</xsl:message>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<xsl:variable name="managed" select="../managedobjects/dlid/text()" />
			<xsl:variable name="sensors" select="/model/objects/object[dlid/text() = $refSensorId]/refid/text()" />
			<xsl:variable name="consumers" select="dl:valueseq-difference($sensors, $managed)" />

			<xsl:for-each select="$consumers">
				<consumer>
					<xsl:attribute name="objectid">
						<xsl:value-of select="." />
					</xsl:attribute>
					<xsl:attribute name="id">
						<xsl:text>reftemp</xsl:text>
					</xsl:attribute>
				</consumer>
			</xsl:for-each>
		</producer>
	</xsl:template>

	<xsl:template match='bindingslot'>
		<consumer>
			<xsl:attribute name="id">
				<xsl:text>reftemp</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="datatype">
				<xsl:text>reftemp</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="name">
				<xsl:text>Rack Ref Temp</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="collection">
				<xsl:text>false</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="property">
				<xsl:text>$rack/properties[name="ref"]</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="required">
				<xsl:text>true</xsl:text>
			</xsl:attribute>
			<xsl:variable name="rackid" select='../object[@type = "rack"]/@dlid' />
			<xsl:for-each select='/model/objects/object[dlid/text() = $rackid]/property[@name="ref"]/text()'>
				<producer>
					<xsl:attribute name="objectid">
						<xsl:value-of select='.' />
					</xsl:attribute>
					<xsl:attribute name="id">
						<xsl:text>reftemp</xsl:text>
					</xsl:attribute>
				</producer>
			</xsl:for-each>
		</consumer>
	</xsl:template>

	<xsl:template match='varbinding[@vars="uid"]'>
		<varbinding vars="uid">
			<property>$gateway/properties[name="uniqueId"]</property>
			<value>Long.parseLong(uid,16)</value>
		</varbinding>
	</xsl:template>

	<xsl:template match='varbinding'>
		<xsl:copy-of select="." />
	</xsl:template>

	<xsl:function name="dl:valueseq-intersect">
		<xsl:param name="seq1" />
		<xsl:param name="seq2" />
		<xsl:sequence select="distinct-values($seq2[.=$seq1])" />
	</xsl:function>

	<xsl:function name="dl:valueseq-difference">
		<xsl:param name="seq1" />
		<xsl:param name="seq2" />
		<xsl:sequence select="distinct-values($seq1[not(.=$seq2)])" />
	</xsl:function>

</xsl:stylesheet>

