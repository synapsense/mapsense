<?xml version="1.0" encoding="UTF-8"?>

<!--
see wiki page for spec: http://wiki.synapsense.int/moinmoin/DeploymentLab/UpgradeProcess
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/02/xpath-functions"
	xmlns:dl="http://www.synapsense.com"
	exclude-result-prefixes="xsl xs fn dl"
	>
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	
	<xsl:template match="model">
		<model version="1.4">
			<xsl:copy-of select="*[not(self::objects) and not(self::components)]" />
			<xsl:apply-templates select="objects" />
			<xsl:apply-templates select="components" />
		</model>
	</xsl:template>

	<xsl:template match="objects">
		<objects>
			<xsl:attribute name="version">1.5</xsl:attribute>
			<xsl:attribute name="lastid">
				<xsl:value-of select="@lastid" />
			</xsl:attribute>
			<xsl:apply-templates select="*" />
		</objects>
	</xsl:template>
	
	<xsl:template match='object'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*" />
		</object>
	</xsl:template>
	
	<!-- remove map property from set object -->
	<xsl:template match='object[@type="set"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select='*[not(self::property[@name="map"])]' />
		</object>
	</xsl:template>
	
		<!-- 2. inspector object: add unit property -->
	<xsl:template match='object[@type="inspector"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*" />
			
			<property name='units'>
		        <value></value>
		        <oldvalue />
		    </property>
		</object>
	</xsl:template>
	
	<!-- 3. crac object: add properties for new metrics -->
	<xsl:template match='object[@type="crac"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*" />
			
			<property name='btu_table'>
		       <value></value>
		       <oldvalue />
		     </property>
		     <property name='cfm_table'>
		       <value></value>
		       <oldvalue />
		     </property>
		     <property name='in_fanspeed' valuetype='dlid'>
		     </property>
		      <property name='tonsDesign'>
		     </property>
		     <property name='cfm'>
		     </property>
		     <property name='tonsActual'>
		     </property>
		     <property name='coolingEfficiency'>
		     </property>
		</object>
	</xsl:template>

         <!-- 4. fix scaleMin, sensorDisplayMin  for enthalpy node-->
        <xsl:template match='object[@type="sensor"]'>
            <object>
                <xsl:attribute name="type">
        		<xsl:value-of select="@type" />
		</xsl:attribute>

                <xsl:choose>
                        <xsl:when test='property[@name="type"]/value/text()=33'>
                            <xsl:copy-of select='*[not(self::property[@name="scaleMin"]) and not(self::property[@name="sensorDisplayMin"])]' />
                            <property name='scaleMin'>
                                <value>32.0</value>
                                <oldvalue></oldvalue>
                            </property>
                            <property name='sensorDisplayMin'>
                                <value>32.0</value>
                                <oldvalue></oldvalue>
                            </property>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:copy-of select="*" />
                        </xsl:otherwise>
                </xsl:choose>
            </object>
        </xsl:template>

	<!-- 4. New dockingpoints to controlout_crah object  -->
	<!-- xsl:template match='object[@type="controlout_crah"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*" />
			
			<property name='supplyT' valuetype='dlid' />
			<property name='returnT' valuetype='dlid' /> 
			<property name='objects'>
				<current/>
				<old/>
			</property>
		</object>
	</xsl:template-->
	<xsl:template match="components">
		<components>
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:apply-templates select="*" />
		</components>
	</xsl:template>
	
	<xsl:template match='component'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:call-template name="grouppath"><xsl:with-param name="cfg" select="@type" /></xsl:call-template>
				<!-- xsl:value-of select="@type"/-->
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
		</component>
	</xsl:template>
	
	<xsl:template match="varbinding">
		<varbinding>
			<xsl:attribute name="vars"><xsl:value-of select="@vars"/></xsl:attribute>
			<xsl:copy-of select="*"/>
		</varbinding>
	</xsl:template>	
	
	<xsl:template match='property'>
		<property>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="display"><xsl:value-of select="@display" /></xsl:attribute>
			<!-- 10. notes property' type should be DeploymentLab.MultiLineContents  -->
			<xsl:attribute name="type">
					<xsl:choose>
						<xsl:when test='@name="notes"'>
							<xsl:text>DeploymentLab.MultiLineContents</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="@type" />
						</xsl:otherwise>
					</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="editable"><xsl:value-of select="@editable" /></xsl:attribute>
			<xsl:attribute name="displayed"><xsl:value-of select="@displayed" /></xsl:attribute>
			<xsl:if test="@dimension">
				<xsl:attribute name="dimension"><xsl:value-of select="@dimension" /></xsl:attribute>
			</xsl:if>
			<xsl:if test="@valueChoices">
				<xsl:attribute name="valueChoices"><xsl:value-of select="@valueChoices" /></xsl:attribute>
			</xsl:if>
			<xsl:value-of select="." />
		</property>
	</xsl:template>
	
	<xsl:template match='consumer'>
		<consumer>
			<xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
			<xsl:attribute name="datatype"><xsl:value-of select="@datatype" /></xsl:attribute>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="property"><xsl:value-of select="@property" /></xsl:attribute>
			<xsl:attribute name="required"><xsl:value-of select="@required" /></xsl:attribute>
			<xsl:copy-of select="*" />
		</consumer>
	</xsl:template>

	<xsl:template match='producer'>
		<producer>
			<xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
			<xsl:attribute name="datatype"><xsl:value-of select="@datatype" /></xsl:attribute>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="object"><xsl:value-of select="@object" /></xsl:attribute>
			<xsl:copy-of select="*" />
		</producer>
	</xsl:template>

	<xsl:template match='display'>
		<display>
			<xsl:copy-of select="*[not(self::shape) and not(self::name) and not(self::description)]" />
			<xsl:if test='shape != ""'>
				<shape><xsl:call-template name="display-shape"><xsl:with-param name="cfg" select="../@type" /></xsl:call-template></shape>
			</xsl:if>
			<xsl:if test='name != ""'>
				<name><xsl:call-template name="display-name"><xsl:with-param name="cfg" select="../@type" /></xsl:call-template></name>
			</xsl:if>
			<xsl:if test='description != ""'>
				<description><xsl:call-template name="display-description"><xsl:with-param name="cfg" select="../@type" /></xsl:call-template></description>
			</xsl:if>
		</display>
	</xsl:template>
	
	<xsl:template name="display-shape">
		<xsl:param name="cfg" />
		<xsl:choose>
			<xsl:when test='$cfg = "rack-control-rearexhaust-sf-thermanode2"'><xsl:text>rack-rearexhaust-sf-rt</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-rearexhaust-nsf-rt-thermanode2"'><xsl:text>rack-rearexhaust-nsf-rt</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-toprearexhaust-sf-thermanode2"'><xsl:text>rack-toprearexhaust-sf-rt</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-toprearexhaust-nsf-rt-thermanode2"'><xsl:text>rack-toprearexhaust-nsf-rt</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-lhs-endcap-rt-thermanode2"'><xsl:text>rack-lhs-endcap-rt</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-rhs-endcap-rt-thermanode2"'><xsl:text>rack-rhs-endcap-rt</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-rearexhaust-nsf-nrt-thermanode2"'><xsl:text>rack-rearexhaust-nsf-nrt</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-toprearexhaust-nsf-nrt-thermanode2"'><xsl:text>rack-toprearexhaust-nsf-nrt</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-lhs-endcap-nrt-thermanode2"'><xsl:text>rack-lhs-endcap-nrt</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-rhs-endcap-nrt-thermanode2"'><xsl:text>rack-rhs-endcap-nrt</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-rearexhaust-nsf-rt-thermanode"'><xsl:text>rack-rearexhaust-nsf-rt</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-rearexhaust-sf-thermanode"'><xsl:text>rack-rearexhaust-sf-rt-6</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-rearexhaust-nsf-nrt-thermanode"'><xsl:text>rack-rearexhaust-nsf-nrt</xsl:text></xsl:when>
			<xsl:when test='$cfg = "userinput"'><xsl:text>userinput</xsl:text></xsl:when>
			<xsl:when test='$cfg = "power_inspector"'><xsl:text>power_inspector</xsl:text></xsl:when>
			<xsl:when test='$cfg = "status_inspector"'><xsl:text>status_inspector</xsl:text></xsl:when>
			<xsl:when test='$cfg = "webservice-input-numeric"'><xsl:text>webservice_input_numeric</xsl:text></xsl:when>
			<xsl:when test='$cfg = "webservice-input-status"'><xsl:text>webservice_input_status</xsl:text></xsl:when>
			<xsl:when test='$cfg = "snmpobject-input-numeric"'><xsl:text>snmpobject_input_numeric</xsl:text></xsl:when>
			<xsl:when test='$cfg = "snmpobject-input-status"'><xsl:text>snmpobject_input_status</xsl:text></xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="shape" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="display-name">
		<xsl:param name="cfg" />
		<xsl:choose>
			<!-- 3.  power_inspector renaming -->
			<xsl:when test='$cfg = "power_inspector"'><xsl:text>Calculation Inspector</xsl:text></xsl:when>
			<!-- 6. rename "PDI PDU" to "PDU" : -->
			<xsl:when test='$cfg = "pdi-pdu"'><xsl:text>PDU</xsl:text></xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="name" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="display-description">
		<xsl:param name="cfg" />
		<xsl:choose>
			<xsl:when test='$cfg = "pue_input_value_switch"'><xsl:text>Produces one of two values based on a Status input. Has an Equipment Status Consumer, a numeric Consumer, and a single numeric Producer. If the input Status is ON, the Producer supplies the value of the numeric Producer. If the input Status is OFF, the Producer supplies the contents of the Default Value property. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "pue_status_inverter"'><xsl:text>Inverts a Status Value, turning a value of ON to OFF, and vice-versa. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "power_inspector"'><xsl:text>Provides an attachment point for viewing a Numeric or Calculated value. Use this component when a calculation or raw data needs to be displayed in the web console. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "status_inspector"'><xsl:text>Provides an attachment point for viewing a Status value in a Graph. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "pue_main"'><xsl:text>Calculates final PUE values and reports them to Console. This Component does not go in a Grouping and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "3PhaseTokW"'><xsl:text>Converts 3-Phase Current to kW. Amps and Power Factor must be supplied. Intended for use with the 3-Collar CT, or other similar device. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "math_operation_abs"'><xsl:text>Computes the Absolute Value of an input value. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "math_operation_addition"'><xsl:text>Performs addition on two sets of inputs. Both input sets have a set operation performed on them (Sum, Return Smallest, Return Largest, Average) and then the results of both set operations will be Added. Both sets may have multiple inputs, but only require a single value. Sets with a single input will ignore that set's Set Operation. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "average_operation"'><xsl:text>Averages all the values in a single input set. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "math_operation_division"'><xsl:text>Performs division on two sets of inputs. Both input sets have a set operation performed on them (Sum, Return Smallest, Return Largest, Average) and then the results of Set A will be divided by the result of Set B. Both sets may have multiple inputs, but only require a single value. Sets with a single input will ignore that set's Set Operation. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "math_operation_exponentiation"'><xsl:text>Raises a value to the power of the a second value. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "lessthan_filter"'><xsl:text>Accepts an input value and replaces it with the Default Value if the input is less than the specified minimum allowed value. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "math_operation_loge"'><xsl:text>Computes the Natural Logarithm (base e) of an input value. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "math_operation_log10"'><xsl:text>Computes the Common Logarithm (base 10) of an input value. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "math_operation"'><xsl:text>Performs arithmetic operations on two sets of inputs. Both input sets have a set operation performed on them (Sum, Return Smallest, Return Largest, Average) and then the results of both set operations have the Main Operation performed on them in the order (Set A Result) Main Operation (Set B Result). The Main Operation can be one of Add, Subtract, Multiply, or Divide. Both sets may have multiple inputs, but only require a single value. Sets with a single input will ignore that set's Set Operation. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "math_operation_multiplication"'><xsl:text>Performs multiplication on two sets of inputs. Both input sets have a set operation performed on them (Sum, Return Smallest, Return Largest, Average) and then the results of both set operations will be Multiplied. Both sets may have multiple inputs, but only require a single value. Sets with a single input will ignore that set's Set Operation. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "transformer_primary"'><xsl:text>Simulates a piece of equipment with a Rated Power Loss (such as a Transformer or a UPS) where the only known value is the Input (Primary) power. The Component accepts an efficiency table from the user and then calculates the Output (Secondary) power and power loss. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "transformer_secondary"'><xsl:text>Simulates a piece of equipment with a Rated Power Loss (such as a Transformer or a UPS) where the only known value is the Output (Secondary) power. The Component accepts an efficiency table from the user and then calculates the Input (Primary) power and power loss. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "math_operation_square"'><xsl:text>Squares an input value. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "math_operation_sqrt"'><xsl:text>Computes the Square Root of an input value. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "math_operation_subtraction"'><xsl:text>Performs subtraction on two sets of inputs. Both input sets have a set operation performed on them (Sum, Return Smallest, Return Largest, Average) and then the results of Set B will be Subtracted from the result of Set A. Both sets may have multiple inputs, but only require a single value. Sets with a single input will ignore that set's Set Operation. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "sum_operation"'><xsl:text>Sums all values in a single input set. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "userinput"'><xsl:text>Allows the user to provide a fixed value. Commonly used when power does not vary and is always on. This Component goes in a Calculation Group and does not require a Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "snmpobject-input-numeric"'><xsl:text>Queries a specified SNMP OID and returns the result as a number to a calculation graph. This Component goes in a Calculation Group and requires an SNMP Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "webservice-input-numeric"'><xsl:text>Executes an XPath expression against the document at a given URI and returns the result as a number to a calculation graph. This Component goes in a Calculation Group and requires a WebServices Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "snmpobject-input-status"'><xsl:text>Queries a specified SNMP OID and compares the result to the supplied whitelist. If the returned value is present in the whitelist, this component returns a status of ON, otherwise it returns a status of OFF. This Component goes in a Calculation Group and requires an SNMP Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "webservice-input-status"'><xsl:text>Executes an XPath expression against the document at a given URI and compares the result to the supplied whitelist. If the returned value is present in the whitelist, this component returns a status of ON, otherwise it returns a status of OFF. This Component goes in a Calculation Group and requires a WebServices Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "controlpoint_single_temperature_bot"'><xsl:text>A virtual element to capture the Bottom intake temperature from its associated rack and make this temperature available for Adaptive Control. This Component goes in a Region of Influence and does not require a Data Source. It must be attached to a Component that provides Temperature.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "controlpoint_single_temperature"'><xsl:text>A virtual element to capture the top intake temperature from its associated rack and make this temperature available for Adaptive Control. This Component goes in a Region of Influence and does not require a Data Source. It must be attached to a Component that provides Temperature.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "controlpoint_single_temperature_mid"'><xsl:text>A virtual element to capture the Middle intake temperature from its associated rack and make this temperature available for Adaptive Control. This Component goes in a Region of Influence and does not require a Data Source. It must be attached to a Component that provides Temperature.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "controlpoint_single_pressure"'><xsl:text>A virtual element to capture the differential pressure from its associated pressure monitoring device and make this pressure available for Adaptive Control. This Component goes in a Region of Influence and does not require a Data Source. It must be attached to a Component that provides Pressure.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "control_crah_trim"'><xsl:text>A virtual element that represents a Computer Room Air Handler (CRAH) unit that is dynamically controlled by the Adaptive Control module. This Component goes in a Region of Influence and does not require a Data Source. It must be attached to an Environmental CRAH.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "control_vfd_trim"'><xsl:text>A virtual element that represents a Variable Frequency Drive (VFD) that is dynamically controlled by the Adaptive Control module. The VFD manages the fan speed of the motor in the CRAH unit. This Component goes in a Region of Influence and does not require a Data Source. It must be attached to an Environmental CRAH.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "crah-single-plenumrated-thermanode"'><xsl:text>A CRAH monitoring assembly consisting of one node (with built-in humidity sensor) and two external thermistors which sense supply and return air temperature. The node is placed in the CRAH return air stream for collecting return humidity readings. Provides Rack Reference Temperature from the Supply Temperature. This Component goes in a Zone and a WSN Data Source. This configuration is qualified for Adaptive Control.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "crah-dual-thermanode"'><xsl:text>A CRAH monitoring assembly consisting of two ThermaNodes. The first node captures the return air temperature while the second node captures the supply air temperature. Provides Rack Reference Temperature from the Supply Temperature. This Component goes in a Zone and a WSN Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "crah-dual-probe-thermanode"'><xsl:text>A CRAH monitoring assembly consisting of two ThermaNodes with external temperature probes. The first node captures the return air temperature while the second node captures the supply air temperature. Provides Rack Reference Temperature from the Supply Temperature. This Component goes in a Zone and a WSN Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "crah-thermanode"'><xsl:text>A CRAH monitoring assembly consisting of a ThermaNode with a single external thermistor. The node captures the supply air temperature while the external thermistor captures the return air temperature. Provides Rack Reference Temperature from the Supply Temperature. This Component goes in a Zone and a WSN Data Source. 4.0 upgrades only.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "single-horizontal-row-thermanode2"'><xsl:text>Array of sensors used to monitor the horizontal temperature distribution at various points in a row of racks at the top of each rack. There are 6 external thermistors, each displaced from the center where the ThermaNode is located. This component goes in a Zone and requires a WSN Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "standalone-thermanode"'><xsl:text>A ThermaNode that reports its internal temperature and humidity. This Component goes in a Zone and a WSN Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "control-verticalstring-li-thermanode"'><xsl:text>A ThermaNode and sensor assembly consisting of 3 thermistors. Sense points will appear in LI images on Top, Middle and Bottom layers. This Component goes in a Zone and a WSN Data Source. This configuration is qualified for Adaptive Control.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "pressure2"'><xsl:text>A sensor node used to capture subfloor differential pressure. This Component goes in a Zone and a WSN Data Source. This configuration is qualified for Adaptive Control.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-toprearexhaust-nsf-nrt-6t"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors. Designed for front inlet, rear exhaust configurations. Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, chimney exhaust, exhaust top and exhaust middle/bottom. This rack goes in a Zone and requires a WSN Data Source. This configuration is qualified for Adaptive Control.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-lhs-endcap-nrt-6t"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors located on a left end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom and side middle. This rack goes in a Zone and requires a WSN Data Source. This configuration is qualified for Adaptive Control.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-rhs-endcap-nrt-6t"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors located on a right end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom and side middle. This rack goes in a Zone and requires a WSN Data Source. This configuration is qualified for Adaptive Control.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-rearexhaust-nsf-nrt-thermanode"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 5 external sensors. Designed for front inlet, rear exhaust configurations. Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, and exhaust middle/bottom. This rack goes in a Zone and requires a WSN Data Source. This configuration is qualified for Adaptive Control.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "dual-horizontal-row-thermanode2"'><xsl:text>Array of sensors used to monitor the horizontal temperature distribution at various points in a row of racks. There are 6 external thermistors, each displaced from the center where the ThermaNode is located. This component goes in a Zone and requires a WSN Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-toprearexhaust-nsf-rt-6t"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors. Designed for front inlet, rear exhaust configurations. Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, chimney exhaust, exhaust top and exhaust middle/bottom. This rack goes in a Zone and requires a WSN Data Source. This configuration is qualified for Adaptive Control.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-toprearexhaust-sf-6t"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors. Designed for front inlet, chimney/rear exhaust configurations. Node is installed at rack top, sense points are located at cabinet intake top, intake middle, intake bottom, Chimney exhaust, exhaust top and subfloor. This rack goes in a Zone and requires a WSN Data Source. This configuration is qualified for Adaptive Control.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-lhs-endcap-rt-6t"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors located on a left end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom and side middle. This rack goes in a Zone and requires a WSN Data Source. This configuration is qualified for Adaptive Control.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-rhs-endcap-rt-6t"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors located on a right end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom and side middle. This rack goes in a Zone and requires a WSN Data Source. This configuration is qualified for Adaptive Control.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "reftemp-thermanode"'><xsl:text>A ThermaNode installed in the subfloor to report its internal temperature for use as the Rack reference temperature in place of CRAH supply. The temperature from this node will be displayed in the subfloor layer of LiveImaging. This component goes in a Zone and requires a WSN Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-rearexhaust-nsf-rt-thermanode"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 5 external sensors. Designed for front inlet, rear exhaust configurations. Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top and exhaust middle/bottom. This rack goes in a Zone and requires a WSN Data Source. This configuration is qualified for Adaptive Control.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "rack-control-rearexhaust-sf-thermanode"'><xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors. Designed for front inlet, rear exhaust configurations. Node is installed at rack top, sense points are located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom, subfloor. This rack goes in a Zone and requires a WSN Data Source. This configuration is qualified for Adaptive Control.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "modbus-virtual-gateway"'><xsl:text>Represents a Virtual Modbus gateway on the floorplan. Requires a Modbus Data Source, but not all Modbus Data Sources require a gateway.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "pdi-pdu"'><xsl:text>Virtual component representing one to four panels.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "door-constellation2"'><xsl:text>A Constellation II node and external sensor assembly that reports the status of a door. This Component goes in a Zone and requires a WSN Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "equipment-status-constellation2"'><xsl:text>Monitors the ON/OFF state of a piece of equipment. This Component goes in a Zone and requires a WSN Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "remote-gateway"'><xsl:text>A SynapSense wireless remote gateway. At least one is required for each WSN Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "leakdetector-constellation2"'><xsl:text>A Constellation II node and external sensor assembly that reports the presence of liquid. This Component goes in a Zone and requires a WSN Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "modbus-pdi-bcms-custom"'><xsl:text>PDI BCMS device loaded with SynapSense custom firmware. Provides Current, kW and Power Factor per circuit. Requires a Modbus Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "modbus-squared-bcms-custom"'><xsl:text>A Square-D BCMS device loaded with SynapSense custom firmware. Provides Current, kW, and Power Factor per circuit. Requires a Modbus Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "ion6200-delta"'><xsl:text>An ION 6200 Power Meter in the Delta configuration, physically wired to a Modbus connection. This Component goes in a Zone and requires a Modbus Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "ion6200-wye"'><xsl:text>An ION 6200 Power Meter in the Wye configuration physically wired to a Modbus connection. This Component goes in a Zone and requires a Modbus Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "ct-constellation2"'><xsl:text>A Constellation II node and three (3) external sensor assembly that reports current readings acquired from three phases. This Component goes in a Zone and requires a WSN Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "dualflow-enthalpy-constellation2"'><xsl:text>A Chilled-water energy node, 2 external temp sensors and 2 external flow sensors for measuring energy carried by chilled water. This Component goes in a Zone and requires a WSN Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "energy-constellation2-displayed"'><xsl:text>A Constellation II node and external sensor assembly that reports energy readings and is displayed on the floorplan. This Component goes in a Zone and requires a WSN Data Source.</xsl:text></xsl:when>
			<xsl:when test='$cfg = "singleflow-enthalpy-constellation2"'><xsl:text>A Chilled-water energy node, 2 external temp sensors and 1 external flow sensor for measuring energy moved by a CRAH. This Component goes in a Zone and requires a WSN Data Source.</xsl:text></xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="description" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- upgraded components -->
	
	<!-- A. common upgrade process between mercury2venus and att_mercury2venus -->
	<!--  5. remove temperature producers from CRAH units &  add fanspeed consumers to CRAH -->
	<xsl:template match='component[@type="crah-single-plenumrated-thermanode"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:call-template name="grouppath"><xsl:with-param name="cfg" select="@type" /></xsl:call-template>
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			
			<xsl:apply-templates select="display"/>
			<!-- 2-8) Remove depth and width properties -->
			<xsl:apply-templates select='property[not(@name="depth") and not(@name="width")]'/>
			<!-- add more properties for new metric rules -->
			<property name="btu_table" display="BTU Table" type="DeploymentLab.ChartContentsBTU" editable="true" displayed="true"></property>
      		<property name="cfm_table" display="CFM Table" type="DeploymentLab.ChartContentsCFM" editable="true" displayed="true"></property>
        	<property name="btu_table_name" display="BTU Table Name" type="java.lang.String" editable="true" displayed="false"></property>
        	<property name="cfm_table_name" display="CFM Table Name" type="java.lang.String" editable="true" displayed="false"></property>
			
			<!-- 2-8) Remove depth and width properties -->
			<xsl:apply-templates select='varbinding[not(@vars="depth") and not(@vars="width")]'/>
			<varbinding vars="cfm_table">
				<property>$crac/properties[name="cfm_table"]</property>
				<value>cfm_table</value>
			</varbinding>
			<varbinding vars="btu_table">
				<property>$crac/properties[name="btu_table"]</property>
				<value>btu_table</value>
			</varbinding>
			
			<xsl:apply-templates select="consumer"/>
			<consumer id="in_fanspeed" datatype="power" name="Fanspeed" collection="false" property='$crac/properties[name="in_fanspeed"]' required="false" />
			
			<xsl:apply-templates select='producer[not(@id="supplytemp") and not(@id="returntemp")]' />
			<producer id="rat" datatype="control_env_temperature" name="Return Air Temperature" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]' />
			<producer id="sat" datatype="control_env_temperature" name="Supply Air Temperature" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]' />
		</component>
	</xsl:template>
	
	<xsl:template match='component[@type="crah-thermanode"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:call-template name="grouppath"><xsl:with-param name="cfg" select="@type" /></xsl:call-template>
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			
			<xsl:apply-templates select="display"/>
			<xsl:apply-templates select="property"/>
			
			<!-- add more properties for new metric rules -->
			<property name="btu_table" display="BTU Table" type="DeploymentLab.ChartContentsBTU" editable="true" displayed="true"></property>
      		<property name="cfm_table" display="CFM Table" type="DeploymentLab.ChartContentsCFM" editable="true" displayed="true"></property>
        	<property name="btu_table_name" display="BTU Table Name" type="java.lang.String" editable="true" displayed="false"></property>
        	<property name="cfm_table_name" display="CFM Table Name" type="java.lang.String" editable="true" displayed="false"></property>
			
			<varbinding vars="cfm_table">
				<property>$crac/properties[name="cfm_table"]</property>
				<value>cfm_table</value>
			</varbinding>
			<varbinding vars="btu_table">
				<property>$crac/properties[name="btu_table"]</property>
				<value>btu_table</value>
			</varbinding>
			
			<xsl:apply-templates select="consumer"/>
			<!-- Add fanspeed -->
			<consumer id="in_fanspeed" datatype="power" name="Fanspeed" collection="false" property='$crac/properties[name="in_fanspeed"]' required="false" />
			
			<xsl:apply-templates select='producer' />
			<producer id="sat" datatype="control_env_temperature" name="Supply Air Temperature" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]' />
			<producer id="rat" datatype="control_env_temperature" name="Return Air Temperature" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]' />

		</component>
	</xsl:template>
	
	<xsl:template match='component[@type="crah-dual-thermanode"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:call-template name="grouppath"><xsl:with-param name="cfg" select="@type" /></xsl:call-template>
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			
			<xsl:apply-templates select="display"/>
			<xsl:apply-templates select="property"/>
			<!-- add more properties for new metric rules -->
			<property name="btu_table" display="BTU Table" type="DeploymentLab.ChartContentsBTU" editable="true" displayed="true"></property>
      		<property name="cfm_table" display="CFM Table" type="DeploymentLab.ChartContentsCFM" editable="true" displayed="true"></property>
        	<property name="btu_table_name" display="BTU Table Name" type="java.lang.String" editable="true" displayed="false"></property>
        	<property name="cfm_table_name" display="CFM Table Name" type="java.lang.String" editable="true" displayed="false"></property>
			
			<varbinding vars="cfm_table">
				<property>$crac/properties[name="cfm_table"]</property>
				<value>cfm_table</value>
			</varbinding>
			<varbinding vars="btu_table">
				<property>$crac/properties[name="btu_table"]</property>
				<value>btu_table</value>
			</varbinding>
			
			<xsl:apply-templates select="consumer"/>
			<consumer id="in_fanspeed" datatype="power" name="Fanspeed" collection="false" property='$crac/properties[name="in_fanspeed"]' required="false" />
			
			<xsl:apply-templates select='producer[not(@id="supplytemp") and not(@id="returntemp")]' />
			<producer id="sat" datatype="control_env_temperature" name="Supply Air Temperature" object='$sNode/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]' />
			<producer id="rat" datatype="control_env_temperature" name="Return Air Temperature" object='$rNode/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]' />
		</component>
	</xsl:template>
	
	<xsl:template match='component[@type="crah-dual-probe-thermanode"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:call-template name="grouppath"><xsl:with-param name="cfg" select="@type" /></xsl:call-template>
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			
			<xsl:apply-templates select="display"/>
			<xsl:apply-templates select="property"/>
			<!-- add more properties for new metric rules -->
			<property name="btu_table" display="BTU Table" type="DeploymentLab.ChartContentsBTU" editable="true" displayed="true"></property>
      		<property name="cfm_table" display="CFM Table" type="DeploymentLab.ChartContentsCFM" editable="true" displayed="true"></property>
        	<property name="btu_table_name" display="BTU Table Name" type="java.lang.String" editable="true" displayed="false"></property>
        	<property name="cfm_table_name" display="CFM Table Name" type="java.lang.String" editable="true" displayed="false"></property>
			
			<varbinding vars="cfm_table">
				<property>$crac/properties[name="cfm_table"]</property>
				<value>cfm_table</value>
			</varbinding>
			<varbinding vars="btu_table">
				<property>$crac/properties[name="btu_table"]</property>
				<value>btu_table</value>
			</varbinding>
			
			<xsl:apply-templates select="consumer"/>
			<consumer id="in_fanspeed" datatype="power" name="Fanspeed" collection="false" property='$crac/properties[name="in_fanspeed"]' required="false" />
	
			<xsl:apply-templates select='producer[not(@id="supplytemp") and not(@id="returntemp")]' />
			<producer id="sat" datatype="control_env_temperature" name="Supply Air Temperature" object='$sNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]' />
			<producer id="rat" datatype="control_env_temperature" name="Return Air Temperature" object='$rNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]' />
		</component>
	</xsl:template>
	
	<!-- 3.  power_inspector renaming and adding unit property to object & add inspector role to power_inspector and status_inspector-->
	<xsl:template match='component[@type="power_inspector"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:text>placeable,inspector</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:call-template name="grouppath"><xsl:with-param name="cfg" select="@type" /></xsl:call-template>
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			
			<xsl:apply-templates select="display"/>
			<xsl:apply-templates select="property[not(@id=configuration)]"/>
			<property name='units' display='Units' type='java.lang.String' editable='true' displayed='true' dimension=''></property>
			<property name='configuration' display='Configuration' type='java.lang.String' editable='false' displayed='true' dimension=''>Calculation Inspector</property>
			
			<varbinding vars='units'>
				<property>$inspector/properties[name="units"]</property>
				<value>units</value>
			</varbinding>
		
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select='producer' />
			
		</component>
	</xsl:template>
	<xsl:template match='component[@type="status_inspector"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:text>placeable,inspector</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:call-template name="grouppath"><xsl:with-param name="cfg" select="@type" /></xsl:call-template>
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select='producer' />
			<xsl:apply-templates select='property'/>
			<xsl:apply-templates select="display"/>
		</component>
	</xsl:template>
	
	<!-- 4.  add maxTemp property and varbinding to singleflow-enthalpy-constellation2 and dualflow-enthalpy-constellation2
	 & add new varbindings (sensorDisplayMax) to btu meter components-->
	<xsl:template match='component[@type="singleflow-enthalpy-constellation2"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:call-template name="grouppath"><xsl:with-param name="cfg" select="@type" /></xsl:call-template>
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			
			<xsl:apply-templates select="display"/>
			<xsl:apply-templates select="property"/>
			
			<property name="maxtemp" display="Max Temperature" type="java.lang.Integer" editable="true" displayed="true" valueChoices="30 C (86 F):30,50 C (122 F):50,80 C (176 F):80">30</property>
			
			<varbinding vars="maxflow"> <!-- displayMax -->
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="sensorDisplayMax"]</property>
				<value>(maxflow * 15.85032) + 25</value>
			</varbinding>
			<varbinding vars="maxflow,sample_interval"> <!-- energy display max -->
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="sensorDisplayMax"]</property>
				<value>((maxflow * 63) * 0.0002778 * (sample_interval * 60) )</value>
			</varbinding>
			<varbinding vars="maxtemp">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="delta"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="delta"]</property>
				<value>(maxtemp + 32768) * 100</value>
			</varbinding>
			<varbinding vars="maxtemp">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="scaleMax"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="scaleMax"]</property>
				<value>(maxtemp * 1.8) + 32</value>
			</varbinding>
			<varbinding vars="maxtemp"> <!-- temp: displayMax -->
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="sensorDisplayMax"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="sensorDisplayMax"]</property>
				<value>((maxtemp * 1.8) + 32) + 10</value>
			</varbinding>

			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select='producer' />
			
		</component>
	</xsl:template>
	
	<xsl:template match='component[@type="dualflow-enthalpy-constellation2"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:call-template name="grouppath"><xsl:with-param name="cfg" select="@type" /></xsl:call-template>
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			
			<xsl:apply-templates select="display"/>
			<xsl:apply-templates select="property"/>
			
			<property name="maxtemp" display="Max Temperature" type="java.lang.Integer" editable="true" displayed="true" valueChoices="30 C (86 F):30,50 C (122 F):50,80 C (176 F):80">30</property>
			
			<varbinding vars="maxflow"> <!-- flow: displayMax -->
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="sensorDisplayMax"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="sensorDisplayMax"]</property>
				<value>(maxflow * 15.85032) + 25</value>
			</varbinding>
			<varbinding vars="maxflow,sample_interval"> <!-- energy display max -->
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="sensorDisplayMax"]</property>
				<value>((maxflow * 63) * 0.0002778 * (sample_interval * 60) )</value>
			</varbinding>
			<varbinding vars="maxtemp"> <!-- temp: delta -->
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="delta"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="delta"]</property>
				<value>(maxtemp + 32768) * 100</value>
			</varbinding>
			<varbinding vars="maxtemp">  <!-- temp: scaleMax -->
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="scaleMax"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="scaleMax"]</property>
				<value>(maxtemp * 1.8) + 32</value>
			</varbinding>
			<varbinding vars="maxtemp"> <!-- temp: displayMax -->
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="sensorDisplayMax"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="sensorDisplayMax"]</property>
				<value>((maxtemp * 1.8) + 32) + 10</value>
			</varbinding>
		
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select='producer' />
			
		</component>
	</xsl:template>
	
	<!-- 6. rename "PDI PDU" to "PDU" : implemented in display-name template -->
	<xsl:template match='component[@type="pdi-pdu"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:call-template name="grouppath"><xsl:with-param name="cfg" select="@type" /></xsl:call-template>
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			
			<xsl:apply-templates select="display"/>
			
			<xsl:apply-templates select='property[not(@id="configuration")]'/>
			<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">PDU</property>
			
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select='producer' />
		</component>
	</xsl:template>

	<!-- 7. change display shape : implemented in display-shape template-->
	
	<!-- 8. update grouppath  -->
	<xsl:template name="grouppath">
		<xsl:param name="cfg" />
		<xsl:choose>
			<xsl:when test='$cfg= "remote-gateway"'><xsl:text>General;Wireless</xsl:text></xsl:when>
			<xsl:when test='$cfg= "door-constellation2"'><xsl:text>General;Wireless</xsl:text></xsl:when>
			<xsl:when test='$cfg= "leakdetector-constellation2"'><xsl:text>General;Wireless</xsl:text></xsl:when>
			<xsl:when test='$cfg= "equipment-status-constellation2"'><xsl:text>General;Wireless</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "modbus-virtual-gateway"'><xsl:text>General;Virtual</xsl:text></xsl:when>
			<xsl:when test='$cfg= "pdi-pdu"'><xsl:text>General;Virtual</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "rack-control-rearexhaust-nsf-rt-thermanode"'><xsl:text>Environmentals;Wireless;Racks;Require Ref. Temp</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-rearexhaust-sf-thermanode"'><xsl:text>Environmentals;Wireless;Racks;Require Ref. Temp</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-toprearexhaust-nsf-rt-6t"'><xsl:text>Environmentals;Wireless;Racks;Require Ref. Temp</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-toprearexhaust-sf-6t"'><xsl:text>Environmentals;Wireless;Racks;Require Ref. Temp</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-lhs-endcap-rt-6t"'><xsl:text>Environmentals;Wireless;Racks;Require Ref. Temp</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-rhs-endcap-rt-6t"'><xsl:text>Environmentals;Wireless;Racks;Require Ref. Temp</xsl:text></xsl:when>
			<xsl:when test='$cfg= "reftemp-thermanode"'><xsl:text>Environmentals;Wireless;Racks;Require Ref. Temp</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "rack-control-rearexhaust-nsf-nrt-thermanode"'><xsl:text>Environmentals;Wireless;Racks;No Ref. Temp</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-toprearexhaust-nsf-nrt-6t"'><xsl:text>Environmentals;Wireless;Racks;No Ref. Temp</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-lhs-endcap-nrt-6t"'><xsl:text>Environmentals;Wireless;Racks;No Ref. Temp</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-rhs-endcap-nrt-6t"'><xsl:text>Environmentals;Wireless;Racks;No Ref. Temp</xsl:text></xsl:when>
			<xsl:when test='$cfg= "dual-horizontal-row-thermanode2"'><xsl:text>Environmentals;Wireless;Racks;No Ref. Temp</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "crah-single-plenumrated-thermanode"'><xsl:text>Environmentals;Wireless;CRAC/CRAH</xsl:text></xsl:when>
			<xsl:when test='$cfg= "crah-thermanode"'><xsl:text>Environmentals;Wireless;CRAC/CRAH</xsl:text></xsl:when>
			<xsl:when test='$cfg= "crah-dual-thermanode"'><xsl:text>Environmentals;Wireless;CRAC/CRAH</xsl:text></xsl:when>
			<xsl:when test='$cfg= "crah-dual-probe-thermanode"'><xsl:text>Environmentals;Wireless;CRAC/CRAH</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "pressure2"'><xsl:text>Environmentals;Wireless;Pressure</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "control-verticalstring-li-thermanode"'><xsl:text>Environmentals;Wireless;Other</xsl:text></xsl:when>
			<xsl:when test='$cfg= "standalone-thermanode"'><xsl:text>Environmentals;Wireless;Other</xsl:text></xsl:when>
			<xsl:when test='$cfg= "single-horizontal-row-thermanode2"'><xsl:text>Environmentals;Wireless;Other</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "energy-constellation2-displayed"'><xsl:text>Power &amp; Energy;Wireless</xsl:text></xsl:when>
			<xsl:when test='$cfg= "ct-constellation2"'><xsl:text>Power &amp; Energy;Wireless</xsl:text></xsl:when>
			<xsl:when test='$cfg= "dualflow-enthalpy-constellation2"'><xsl:text>Power &amp; Energy;Wireless</xsl:text></xsl:when>
			<xsl:when test='$cfg= "singleflow-enthalpy-constellation2"'><xsl:text>Power &amp; Energy;Wireless</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "modbus-pdi-bcms-custom"'><xsl:text>Power &amp; Energy;Wired;BCMS</xsl:text></xsl:when>
			<xsl:when test='$cfg= "modbus-squared-bcms-custom"'><xsl:text>Power &amp; Energy;Wired;BCMS</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "ion6200-wye"'><xsl:text>Power &amp; Energy;Wired;Power</xsl:text></xsl:when>
			<xsl:when test='$cfg= "ion6200-delta"'><xsl:text>Power &amp; Energy;Wired;Power</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "pue_main"'><xsl:text>Calculations;PUE</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "math_operation"'><xsl:text>Calculations;Power Calculations</xsl:text></xsl:when>
			<xsl:when test='$cfg= "average_operation"'><xsl:text>Calculations;Power Calculations</xsl:text></xsl:when>
			<xsl:when test='$cfg= "sum_operation"'><xsl:text>Calculations;Power Calculations</xsl:text></xsl:when>
			<xsl:when test='$cfg= "transformer_secondary"'><xsl:text>Calculations;Power Calculations</xsl:text></xsl:when>
			<xsl:when test='$cfg= "transformer_primary"'><xsl:text>Calculations;Power Calculations</xsl:text></xsl:when>
			<xsl:when test='$cfg= "userinput"'><xsl:text>Calculations;Power Calculations</xsl:text></xsl:when>
			<xsl:when test='$cfg= "3PhaseTokW"'><xsl:text>Calculations;Power Calculations</xsl:text></xsl:when>
			<xsl:when test='$cfg= "lessthan_filter"'><xsl:text>Calculations;Power Calculations</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "pue_fixed_value_switch"'><xsl:text>Calculations;Equipment Status Calculations</xsl:text></xsl:when>
			<xsl:when test='$cfg= "pue_input_value_switch"'><xsl:text>Calculations;Equipment Status Calculations</xsl:text></xsl:when>
			<xsl:when test='$cfg= "pue_status_inverter"'><xsl:text>Calculations;Equipment Status Calculations</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "snmpobject-input-numeric"'><xsl:text>Calculations;Wired Integration</xsl:text></xsl:when>
			<xsl:when test='$cfg= "snmpobject-input-status"'><xsl:text>Calculations;Wired Integration</xsl:text></xsl:when>
			<xsl:when test='$cfg= "webservice-input-numeric"'><xsl:text>Calculations;Wired Integration</xsl:text></xsl:when>
			<xsl:when test='$cfg= "webservice-input-status"'><xsl:text>Calculations;Wired Integration</xsl:text></xsl:when>

			<xsl:when test='$cfg= "power_inspector"'><xsl:text>Calculations;Inspectors</xsl:text></xsl:when>
			<xsl:when test='$cfg= "status_inspector"'><xsl:text>Calculations;Inspectors</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "controlpoint_single_pressure"'><xsl:text>Control;Control Inputs</xsl:text></xsl:when>
			<xsl:when test='$cfg= "controlpoint_single_temperature"'><xsl:text>Control;Control Inputs</xsl:text></xsl:when>
			<xsl:when test='$cfg= "controlpoint_single_temperature_mid"'><xsl:text>Control;Control Inputs</xsl:text></xsl:when>
			<xsl:when test='$cfg= "controlpoint_single_temperature_bot"'><xsl:text>Control;Control Inputs</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "control_vfd_trim"'><xsl:text>Control;Controlled Devices</xsl:text></xsl:when>
			<xsl:when test='$cfg= "control_crah_trim"'><xsl:text>Control;Controlled Devices</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "doe-ups-modbus-integrator"'><xsl:text>Customer Specific</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "horizontal-row-thermanode"'><xsl:text>Custom</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "generic-constellation2"'><xsl:text>General;Wireless</xsl:text></xsl:when>
	
			<xsl:when test='$cfg= "rack-control-toprearexhaust-sf-thermanode"'><xsl:text>Environmentals;Legacy 5.2</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-toprearexhaust-nsf-rt-thermanode"'><xsl:text>Environmentals;Legacy 5.2</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-toprearexhaust-nsf-nrt-thermanode"'><xsl:text>Environmentals;Legacy 5.2</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "energy-constellation2"'><xsl:text>Power &amp; Energy;Legacy 5.2</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "rack-interior-nsf-rt-thermanode"'><xsl:text>Environmentals;Legacy 5.1</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-interior-sf-thermanode"'><xsl:text>Environmentals;Legacy 5.1</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-lhs-endcap-rt-thermanode"'><xsl:text>Environmentals;Legacy 5.1</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-rhs-endcap-rt-thermanode"'><xsl:text>Environmentals;Legacy 5.1</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-interior-nsf-nrt-thermanode"'><xsl:text>Environmentals;Legacy 5.1</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-lhs-endcap-nrt-thermanode"'><xsl:text>Environmentals;Legacy 5.1</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-rhs-endcap-nrt-thermanode"'><xsl:text>Environmentals;Legacy 5.1</xsl:text></xsl:when>
			<xsl:when test='$cfg= "37li-thermanode"'><xsl:text>Environmentals;Legacy 5.1</xsl:text></xsl:when>
	
			<xsl:when test='$cfg= "leakdetector-constellation"'><xsl:text>General;Legacy 5.0</xsl:text></xsl:when>
			<xsl:when test='$cfg= "door-constellation"'><xsl:text>General;Legacy 5.0</xsl:text></xsl:when>
			<xsl:when test='$cfg= "flow-constellation"'><xsl:text>General;Legacy 5.0</xsl:text></xsl:when>
		
			<xsl:when test='$cfg= "crah-dual-fusion"'><xsl:text>General;Legacy 5.0</xsl:text></xsl:when>
		
			<xsl:when test='$cfg= "rack-interior-fusion"'><xsl:text>Environmentals;Legacy 5.0</xsl:text></xsl:when>
			<xsl:when test='$cfg= "pressure"'><xsl:text>Environmentals;Legacy 5.0</xsl:text></xsl:when>
			<xsl:when test='$cfg= "pipe-temp-thermanode"'><xsl:text>Environmentals;Legacy 5.0</xsl:text></xsl:when>
	
			<xsl:when test='$cfg= "energy-constellation"'><xsl:text>Power &amp; Energy;Legacy 5.0</xsl:text></xsl:when>
			<xsl:when test='$cfg= "ct-constellation"'><xsl:text>Power &amp; Energy;Legacy 5.0</xsl:text></xsl:when>
			<xsl:when test='$cfg= "dualflow-btu-constellation"'><xsl:text>Power &amp; Energy;Legacy 5.0</xsl:text></xsl:when>
			<xsl:when test='$cfg= "singleflow-btu-constellation"'><xsl:text>Power &amp; Energy;Legacy 5.0</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "modbus-1reg-integrator"'><xsl:text>General;Wired</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "soapservice-input-numeric"'><xsl:text>Calculations;Wired Integration</xsl:text></xsl:when>
			<xsl:when test='$cfg= "soapservice-input-status"'><xsl:text>Calculations;Wired Integration</xsl:text></xsl:when>
			
			<xsl:when test='$cfg= "random"'><xsl:text>Calculations;Testing</xsl:text></xsl:when>
			<xsl:when test='$cfg= "wobulator"'><xsl:text>Calculations;Testing</xsl:text></xsl:when>
			<xsl:when test='$cfg= "status_producer_test"'><xsl:text>Calculations;Testing</xsl:text></xsl:when>
			<xsl:when test='$cfg= "pue_user_status"'><xsl:text>Calculations;Testing</xsl:text></xsl:when>
			<xsl:when test='$cfg= "pue_random_status"'><xsl:text>Calculations;Testing</xsl:text></xsl:when>
	
			<xsl:when test='$cfg= "rack-control-rearexhaust-nsf-rt-thermanode2"'><xsl:text>Environmentals;Wireless;Racks;Require Ref. Temps</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-rearexhaust-sf-thermanode2"'><xsl:text>Environmentals;Wireless;Racks;Require Ref. Temps</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-toprearexhaust-sf-thermanode2"'><xsl:text>Environmentals;Wireless;Racks;Require Ref. Temps</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-toprearexhaust-nsf-rt-thermanode2"'><xsl:text>Environmentals;Wireless;Racks;Require Ref. Temps</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-lhs-endcap-rt-thermanode2"'><xsl:text>Environmentals;Wireless;Racks;Require Ref. Temps</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-rhs-endcap-rt-thermanode2"'><xsl:text>Environmentals;Wireless;Racks;Require Ref. Temps</xsl:text></xsl:when>

			<xsl:when test='$cfg= "rack-control-toprearexhaust-nsf-nrt-thermanode2"'><xsl:text>Environmentals;Wireless;Racks;No Ref. Temp</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-lhs-endcap-nrt-thermanode2"'><xsl:text>Environmentals;Wireless;Racks;No Ref. Temp</xsl:text></xsl:when>
			<xsl:when test='$cfg= "rack-control-rhs-endcap-nrt-thermanode2"'><xsl:text>Environmentals;Wireless;Racks;No Ref. Temp</xsl:text></xsl:when>
	
			<xsl:when test='$cfg= "test-tnode2"'><xsl:text>Test</xsl:text></xsl:when>
	
			<xsl:otherwise>
				<xsl:text>Custom</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- 9. Change descrption of pressure2 component : implemented in display-description -->
	
	<!-- 10. notes property' type should be DeploymentLab.MultiLineContents  -->
	<!-- implemented by property template -->		
	
	<!-- 11. Remove control input producers for reftemp-thermanode and standalone-thermanode -->
	<xsl:template match='component[@type="reftemp-thermanode"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select='producer[not(@id="temp")]'/>
		</component>
	</xsl:template>
	<xsl:template match='component[@type="standalone-thermanode"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select='producer[not(@id="temp")]'/>
		</component>
	</xsl:template>
	
	<!-- 12. add note property into snmpobject-input-numeric, snmpobject-input-status, webservice-input-numeric,webservice-input-status  -->
	<xsl:template match='component[@type="snmpobject-input-numeric" or @type="snmpobject-input-status" or @type="webservice-input-numeric" or @type="webservice-input-status"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<property name='notes' display='Notes' type='DeploymentLab.MultiLineContents' editable='true' displayed='true' dimension=''></property>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
		</component>
	</xsl:template>
	
	<!-- B. only att. -->
	
	<!-- 1. Add defaultcontrolinpit role to controlpoint_single_temperature and controlpoint_single_pressure-->
	<xsl:template match='component[@type="controlpoint_single_pressure" or @type="controlpoint_single_temperature"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:text>placeable,controlinput,defaultcontrolinput</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:call-template name="grouppath"><xsl:with-param name="cfg" select="@type" /></xsl:call-template>
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			
			<xsl:apply-templates select="display"/>
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select='producer' />
		</component>
	</xsl:template>
	
	<!-- 2. Producers to Control VFD Components:  -->
	<!-- xsl:template match='component[@type="control_vfd_trim"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:text>placeable,controlleddevice</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:call-template name="grouppath"><xsl:with-param name="cfg" select="@type" /></xsl:call-template>
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			
			<xsl:apply-templates select="display"/>
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select='producer' />
			<producer id="fanspeed" datatype="power" name="Fanspeed" object='$controlout_vfd/properties[name="producerpoints"]/children[properties[name="name"][value="fanspeed"]]' />
			
		</component>
	</xsl:template-->
	<!-- 3.Consumers to control_crah_trim component: :  -->
	<!-- xsl:template match='component[@type="control_vfd_trim"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:text>placeable,controlleddevice</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:call-template name="grouppath"><xsl:with-param name="cfg" select="@type" /></xsl:call-template>
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			
			<xsl:apply-templates select="display"/>
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<consumer id='nodeSat' datatype='power' name='Supply Air Temperature' collection='false' property='$controlout_crah/properties[name="supplyT"]' required='false' />
        	<consumer id='nodeRat' datatype='power' name='Return Air Temperature' collection='false' property='$controlout_crah/properties[name="returnT"]' required='false' />
			
			<xsl:apply-templates select='producer' />
		</component>
	</xsl:template-->
</xsl:stylesheet>