<?xml version="1.0" encoding="UTF-8"?>

<!--
see wiki page for spec: http://wiki.synapsense.int/moinmoin/DeploymentLab/WsnModelMigration
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/02/xpath-functions"
	xmlns:dl="http://www.synapsense.com"
	exclude-result-prefixes="xsl xs fn dl"
	>
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<xsl:variable name="tab" select="'&#09;'" />
	<xsl:variable name="newLine" select="'&#xa;'" />
	
	<xsl:template match="model">
		<model version="2.0">
			<xsl:copy-of select="*[not(self::objects) and not(self::components)]" />
			<xsl:apply-templates select="objects" />
			<xsl:apply-templates select="components" />
		</model>
	</xsl:template>

	<xsl:template match="objects">
		<objects>
			<xsl:attribute name="version">2.0</xsl:attribute>
			<xsl:attribute name="lastid">
				<xsl:value-of select="@lastid" />
			</xsl:attribute>
			<xsl:apply-templates select="*" />
		</objects>
	</xsl:template>
	
	<xsl:template match='object'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*" />
		</object>
	</xsl:template>
	
	<!-- object migration -->
	<!-- DC -->
    <xsl:template match="object[@type='dc']">
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::property[@name='gateways'])]" />

			<!-- B) PI upgrades -->
			<property name="derateFactor"><value>80</value><oldvalue></oldvalue></property>
			<property name="unbalancedPercentage"><value>10</value><oldvalue></oldvalue></property>
			<property name="resetDays"><value>90</value><oldvalue></oldvalue></property>
			<property name="powerFactor"><value>1.0</value><oldvalue></oldvalue></property>
		</object>
	</xsl:template>



	<!-- GATEWAY -->
	<xsl:template match="object[@type='gateway']">
		<xsl:variable name="cleanAddress" select='normalize-space( replace( property[@name="address"]/value/text(), "TCP:", "" ) )'/>
		<xsl:variable name="parentId" select='//object[@type="network"]/property[@name="comport"]/value[contains(text(),$cleanAddress)]/../../dlid/text()'/>

		<xsl:variable name="numberOfNetworks" select="count( //object[@type='network'] ) div 2"/>
		<xsl:variable name="firstNetworkId" select="//object[@type='network']/dlid/text()"/>

		<xsl:choose>
			<!--<xsl:when test="string-length($parentId) > 0">-->
			<xsl:when test="($parentId and $cleanAddress) or ($numberOfNetworks=1)">

				<object type="wsngateway">
				<xsl:variable name="address" select="property[@name='address']/value/text()"/>
				<xsl:for-each select="property">
					<xsl:choose>
						<xsl:when test="@name='uniqueId'">
							<property name='mac'>
								<xsl:copy-of select="*"/>
							</property>
						</xsl:when>
						<xsl:when test="@name='networkId'"/>
						<xsl:when test="@name='address'"/>
						<xsl:otherwise>
							<xsl:copy-of select="self::*"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>

				<xsl:choose>
					<xsl:when test="contains($address, 'TCP:')">
						<property name="protocol">
							<value>TCP_RG2</value>
							<oldvalue>TCP_RG2</oldvalue>
						</property>
						<property name="address">
							<value><xsl:value-of select="substring-after($address,'TCP:')"/></value>
							<oldvalue><xsl:value-of select="substring-after($address,'TCP:')"/></oldvalue>
						</property>
						<xsl:call-template name="getGatewayParent">
							<xsl:with-param name="address" select="substring-after($address,'TCP:')"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="contains($address, 'COM:')">
						<property name="protocol">
							<value>COM</value>
							<oldvalue>COM</oldvalue>
						</property>
						<property name="address">
							<value><xsl:value-of select="substring-after($address,'COM:')"/></value>
							<oldvalue><xsl:value-of select="substring-after($address,'COM:')"/></oldvalue>
						</property>
						<xsl:call-template name="getGatewayParent">
							<xsl:with-param name="address" select="substring-after($address,'COM:')"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<property name="protocol">
							<value></value>
							<oldvalue></oldvalue>
						</property>
						<property name="address">
							<value><xsl:value-of select="$address"/></value>
							<oldvalue><xsl:value-of select="$address"/></oldvalue>
						</property>
						<xsl:call-template name="getGatewayParent">
							<xsl:with-param name="address" select="$address"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>

				<property name="securityMode">
					<value>none</value>
					<oldvalue>none</oldvalue>
				</property>

				<property name="key">
					<value></value>
					<oldvalue></oldvalue>
				</property>

				<xsl:copy-of select="*[not(self::property) and not(self::parentid) and not(self::deleted)]" />

				</object>
			</xsl:when>
			<!-- xsl:otherwise> <xsl:message>GATEWAY BALEETED</xsl:message> </xsl:otherwise -->
		</xsl:choose>

    </xsl:template>
    
    <!-- NETWORK -->
    <xsl:template match="object[@type='network']">
    	<object type="wsnnetwork">
		<xsl:for-each select='property[not(@name="softbased") and not(@name="networkDefaultSampleInterval") and not(@name="networkStartTimestamp") and not(@name="comport") and not(@name="baseStationPort") and not(@name="baseStationHostname")]'>
			<xsl:choose>
				<xsl:when test="@name='networkDescription'">
					<property name='desc'>
						<xsl:copy-of select="*"/>
					</property>
				</xsl:when>
				<xsl:otherwise>
					<xsl:copy-of select="self::*"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		<property name="gateways">
			<xsl:variable name="comport_value" select="normalize-space(property[@name='comport']/value/text())"/>
			<xsl:variable name="numberOfNetworks" select="count( //object[@type='network'] ) div 2"/>
			<!-- xsl:message>comport: '<xsl:value-of select="$comport_value"/>' number of networks:<xsl:value-of select="$numberOfNetworks"/>  </xsl:message -->
			<xsl:choose>
				<xsl:when test='$numberOfNetworks=1'>
					<current>
						<xsl:for-each select="//object[@type='gateway']">

							<xsl:if test="dlid/text()">
								<child>
									<!-- xsl:message> Making child GW with value of <xsl:value-of select="dlid/text()"/> </xsl:message -->
									<xsl:value-of select="dlid/text()"/>
								</child>
							</xsl:if>
						</xsl:for-each>
					</current>
					<old/>
				</xsl:when>

				<!-- <xsl:when test='$comport_value="TCP:" or $comport_value=""'> -->
				<xsl:when test='$comport_value="TCP:" or string-length($comport_value)=0'>
					<current/>
					<old/>
				</xsl:when>
				<xsl:otherwise>
					<current>
						<xsl:call-template name="addressoutput-tokens">
							<xsl:with-param name="list" select="property[@name='comport']/value/text()"/>
							<xsl:with-param name="delimiter">,</xsl:with-param>
							<xsl:with-param name="attr_name">child</xsl:with-param>
						</xsl:call-template>
					</current>
					<old>
						<xsl:call-template name="addressoutput-tokens">
							<xsl:with-param name="list" select="property[@name='comport']/value/text()"/>
							<xsl:with-param name="delimiter">,</xsl:with-param>
							<xsl:with-param name="attr_name">child</xsl:with-param>
						</xsl:call-template>
					</old>
				</xsl:otherwise>
			</xsl:choose>
		</property>
		<xsl:copy-of select="*[not(self::property)]"/>
		<property name="status">
			<value>1</value>
			<oldvalue></oldvalue>
		</property>
    	</object>
    </xsl:template>
   
   <!-- NODE --> 
	<xsl:template match="object[@type='node']">
    	<object type='wsnnode'>
		<xsl:for-each select="property">
			<xsl:choose>
				<xsl:when test="@name='nodeDescription'">
					<property name="desc">
						<xsl:copy-of select="*"/>
					</property>
				</xsl:when>
				<xsl:when test="@name='platform'">
					<property name="platformId">
						<!-- xsl:copy-of select="*"/ -->

						<xsl:choose>
							<xsl:when test="value='18'">
								<value>17</value>
								<oldvalue></oldvalue>
							</xsl:when>
							<xsl:otherwise>
								<xsl:copy-of select="*"/>
							</xsl:otherwise>

						</xsl:choose>


					</property>
				</xsl:when>
				<xsl:when test="@name='nodeSamplingInterval'">
					<property name="period">
						<xsl:copy-of select="*"/>
					</property>
				</xsl:when>
				<xsl:when test="@name='nodePhysicalId'">
					<property name="mac">
						<xsl:copy-of select="*"/>
					</property>
				</xsl:when>
				<xsl:when test="@name='nodeLogicalId'">
					<property name="id">
						<xsl:copy-of select="*"/>
					</property>
				</xsl:when>
				<xsl:when test="@name='status'">
					<property name="status">
						<value>
						<xsl:if test="value='false'"><xsl:text>0</xsl:text></xsl:if>
						<xsl:if test="value='true'"><xsl:text>1</xsl:text></xsl:if>
						</value>
						<oldvalue>
						<xsl:if test="value='false'"><xsl:text>0</xsl:text></xsl:if>
						<xsl:if test="value='true'"><xsl:text>1</xsl:text></xsl:if>
						</oldvalue>
        			</property>
				</xsl:when>
				<!--  remove unused properties -->
				<xsl:when test="@name='batteryCapacityTimestamp'"/>
				<xsl:when test="@name='nodeType'"/>
				<xsl:when test="@name='nodeIsPanCoordinatorFlg'"/>
				<xsl:when test="@name='panId'"/>
				<!-- remainingDays value needs to be set depending on the status of battery operated or not. -->
				<xsl:when test="@name= 'remainingDays'"/>
				<xsl:otherwise>
					<xsl:copy-of select="self::*"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		<!--  add new properties -->
		<property name="platformName">
			<value>
				 <xsl:call-template name="getNodePlatformNameById">
		            <xsl:with-param name="id" select="property[@name='platform']/value/text()" />
		        </xsl:call-template>
			</value>
			<oldvalue>
				 <xsl:call-template name="getNodePlatformNameById">
		            <xsl:with-param name="id" select="property[@name='platform']/oldvalue/text()" />
		        </xsl:call-template>
			</oldvalue>
		</property>
		
		<xsl:variable name="isBatteryOperated">
			<xsl:call-template  name="getNodeBatteryOperated">
					<xsl:with-param name="node_dlid" select="dlid/text()"/>
					<xsl:with-param name="node_platform_id" select="property[@name='platform']/value/text()"/>
				</xsl:call-template>
		</xsl:variable>
		
		<xsl:choose>
			<xsl:when test="$isBatteryOperated='0'">
				<property name="remainingDays">
					<value>-1</value>
					<oldvalue>-1</oldvalue>
				</property>
			</xsl:when>
			<xsl:otherwise>
				<property name="remainingDays">
					<value><xsl:value-of select="property[@name='remainingDays']/value/text()"/></value>
					<oldvalue><xsl:value-of select="property[@name='remainingDays']/oldvalue/text()"/></oldvalue>
				</property>
			</xsl:otherwise>
		</xsl:choose>
		<property name="batteryOperated">
			<value>
				<xsl:value-of select="$isBatteryOperated"/>
			</value>
			<oldvalue>
				<xsl:value-of select="$isBatteryOperated"/>
			</oldvalue>
		</property>
		
		<property name="route">
			<value></value>
			<oldvalue></oldvalue>
		</property>
						
		<property name="program">
			<value></value>
			<oldvalue></oldvalue>
		</property>

		<xsl:copy-of select="*[not(self::property)]"/>

    	</object>
    </xsl:template>   
    
    <!-- SENSOR -->
    <xsl:template match="object[@type='sensor']">
    	<object type="wsnsensor">

		<xsl:variable name="minVal" select="property[@name='sensorDisplayMin']/value/text()"/>
		<xsl:variable name="sensorName" select="property[@name='name']/value/text()"/>
		<xsl:variable name="sensorType" select="property[@name='type']/value/text()"/>
		<xsl:variable name="sensorChannel" select="property[@name='channel']/value/text()"/>
		<xsl:variable name="parentNodeId" select="parentid/text()"/>
		<xsl:variable name="parentComponentType" select='//component/object[@dlid=$parentNodeId]/../@type'/>
		<xsl:variable name="scaleMinVal" select="property[@name='scaleMin']/value/text()"/>

		<xsl:for-each select="property">
			<xsl:choose>
				<xsl:when test="@name='sensorDescription'">
					<property name="desc">
						<xsl:copy-of select="*"/>
					</property>
				</xsl:when>
				<xsl:when test="@name='sensorDisplayMin'">
					<!--
					<property name="min">
						<xsl:copy-of select="*"/>
					</property>
					-->
					<!-- C9) fill in min field to 0 if empty-->
					<property name="min">
					<xsl:choose>
						<xsl:when test="$minVal">
							<xsl:copy-of select="*"/>
						</xsl:when>
						<xsl:otherwise>
							<value>0.0</value>
							<oldvalue></oldvalue>
						</xsl:otherwise>
					</xsl:choose>
					</property>
				</xsl:when>
				<xsl:when test="@name='sensorDisplayMax'">
					<property name="max">
						<xsl:copy-of select="*"/>
					</property>
				</xsl:when>
				<xsl:when test="@name='AMin'">
					<property name="aMin">
						<xsl:copy-of select="*"/>
					</property>
				</xsl:when>
				<xsl:when test="@name='AMax'">
					<property name="aMax">
						<xsl:copy-of select="*"/>
					</property>
				</xsl:when>
				<xsl:when test="@name='RMin'">
					<property name="rMin">
						<xsl:copy-of select="*"/>
					</property>
				</xsl:when>
				<xsl:when test="@name='RMax'">
					<property name="rMax">
						<xsl:copy-of select="*"/>
					</property>
				</xsl:when>

				<xsl:when test="@name='scaleMin'">
					<property name="scaleMin">

						<!-- C19) fill in bad null scaleMins for const2 contactors -->
						<xsl:choose>
							<xsl:when test="$parentComponentType='leakdetector-constellation2' and $sensorChannel='6'">
								<value>0.0</value>
								<oldvalue></oldvalue>
							</xsl:when>

							<xsl:when test="$parentComponentType='door-constellation2' and $sensorChannel='6'">
								<value>0.0</value>
								<oldvalue></oldvalue>
							</xsl:when>

							<xsl:when test="$parentComponentType='equipment-status-constellation2' and $sensorChannel='6'">
								<value>0.0</value>
								<oldvalue></oldvalue>
							</xsl:when>

							<xsl:otherwise>
								<xsl:copy-of select="*"/>
							</xsl:otherwise>
						</xsl:choose>

					</property>
				</xsl:when>



				<xsl:when test="@name='visualLocX'">
					<property name="x">
						<xsl:copy-of select="*"/>
					</property>
				</xsl:when>
				<xsl:when test="@name='visualLocY'">
					<property name="y">
						<xsl:copy-of select="*"/>
					</property>
				</xsl:when>
				<xsl:when test="@name='visualLayer'">
					<property name="z">
						<!-- C11) adjust for badly assigned z-layers -->

						<xsl:choose>
							<xsl:when test="$sensorName = 'hot top temp'">
								 <value>131072</value>
								<oldvalue></oldvalue>
							</xsl:when>
							<xsl:when test="$sensorName = 'hot middle temp'">
								 <value>16384</value>
								<oldvalue></oldvalue>
							</xsl:when>
							<xsl:when test="$sensorName = 'hot bottom temp'">
								 <value>2048</value>
								<oldvalue></oldvalue>
							</xsl:when>
							<xsl:when test="$sensorName = 'hot middle/bottom temp'">
								 <value>18432</value>
								<oldvalue></oldvalue>
							</xsl:when>
							<xsl:when test="$sensorName = 'cold top temp'">
								 <value>131072</value>
								<oldvalue></oldvalue>
							</xsl:when>
							<xsl:when test="$sensorName = 'cold middle temp'">
								 <value>16384</value>
								<oldvalue></oldvalue>
							</xsl:when>
							<xsl:when test="$sensorName = 'cold bottom temp'">
								 <value>2048</value>
								<oldvalue></oldvalue>
							</xsl:when>
							<!-- and to cover a misspelling -->
							<xsl:when test="$sensorName = 'col bottom temp'">
								 <value>2048</value>
								<oldvalue></oldvalue>
							</xsl:when>
							<xsl:when test="$sensorName = 'cold middle/bottom temp'">
								 <value>18432</value>
								<oldvalue></oldvalue>
							</xsl:when>
							<xsl:when test="$sensorChannel = '1' and $sensorType = '2' and $parentComponentType = 'dual-horizontal-row-thermanode2' ">
								<value>131072</value>
								<oldvalue>0</oldvalue>
<!--
								<xsl:message>Sensor Parent ID is '<xsl:value-of select="$parentNodeId"/>' </xsl:message>
								<xsl:variable name="nodePlatform" select='//object[dlid/text()=$parentNodeId]/property[@name="platform"]/value/text()'/>
								<xsl:variable name="nodeName" select='//object[dlid/text()=$parentNodeId]/property[@name="name"]/value/text()'/>
								<xsl:message>Sensor Parent Platform is '<xsl:value-of select="$nodePlatform"/>' </xsl:message>
								<xsl:message>Sensor Parent Name is '<xsl:value-of select="$nodeName"/>' </xsl:message>
								<xsl:message>Sensor Component Type is '<xsl:value-of select="$parentComponentType"/>' </xsl:message>
-->
							</xsl:when>
							<xsl:otherwise>
								<xsl:copy-of select="*"/>
							</xsl:otherwise>
						</xsl:choose>

						<!-- <xsl:copy-of select="*"/> -->
					</property>
				</xsl:when>

				<xsl:when test="@name='delta'">
					<xsl:choose>
						<xsl:when test="$sensorType = '31' or $sensorType='32' or $sensorType='33'">
							<property name="delta">
								<xsl:copy-of select="*"/>
							</property>
						</xsl:when>
						<xsl:otherwise>
							<!-- in all other cases, ditch the delta field -->
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>


				<xsl:when test="@name='name'">
					<property name="name">
						<xsl:choose>
							<xsl:when test="$sensorName = 'col bottom temp'">
								<value>cold bottom temp</value>
								<oldvalue></oldvalue>
							</xsl:when>
							<xsl:otherwise>
								<xsl:copy-of select="*"/>
							</xsl:otherwise>
						</xsl:choose>

					</property>
				</xsl:when>

				<xsl:when test="@name='firmwareType'"/>
				<xsl:when test="@name='sensorId'"/>
				<xsl:when test="@name='nodeId'"/>
				<xsl:when test="@name='networkId'"/>
				<xsl:when test="@name='topologyId'"/>
				<xsl:otherwise>
					<xsl:copy-of select="self::*"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		<!-- add new properties -->
		<property name="dataclass">
			<value>
				<xsl:call-template name="getSensorDataClassByTypeId">
					<xsl:with-param name="typeId" select="property[@name='type']/value/text()"/>
				</xsl:call-template>
			</value>
			<oldvalue/>
		</property>

		<property name="enabled">
			<value>
				<xsl:call-template name="getSensorEnabled">
					<xsl:with-param name="node_dlid" select="parentid/text()"/>
					<xsl:with-param name="channel" select='property[@name="channel"]/value/text()'/>
					<xsl:with-param name="delta" select='property[@name="delta"]/value/text()'/>
				</xsl:call-template>
			</value>
			<oldvalue/>
		</property>
		
		<property name="smartSendThreshold">
			<value>
				<xsl:call-template name="getSensorSmartSendThreshold">
					<xsl:with-param name="node_dlid" select="parentid/text()"/>
					<xsl:with-param name="delta" select='property[@name="delta"]/value/text()'/>
				</xsl:call-template>
			</value>
			<oldvalue/>
		</property>
		
		<property name="mode">
			<value>
				<xsl:call-template name="getSensorMode">
					<xsl:with-param name="node_dlid" select="parentid/text()"/>
					<xsl:with-param name="channel" select='property[@name="channel"]/value/text()'/>
					<xsl:with-param name="delta" select='property[@name="delta"]/value/text()'/>
				</xsl:call-template>
			</value>
			<oldvalue/>
		</property>
		
		<property name="interval">
			<value>
				<xsl:call-template name="getSensorInterval">
					<xsl:with-param name="node_dlid" select="parentid/text()"/>
					<xsl:with-param name="channel" select='property[@name="channel"]/value/text()'/>
					<xsl:with-param name="delta" select='property[@name="delta"]/value/text()'/>
				</xsl:call-template>
			</value>
			<oldvalue/>
		</property>

		<!-- C13) fill in subSamples for external thermistors -->
		<xsl:call-template name="getSensorSubSamples">
			<xsl:with-param name="typeId" select="property[@name='type']/value/text()"/>
		</xsl:call-template>
		
		<xsl:copy-of select="*[not(self::property)]"/>
    	</object>
    </xsl:template>


	<!-- A) DM REFACTORING: add custom tags where they need to be -->
	<!-- A1) poll tags for polling period -->
<!--
object - tag to replace
modbusproperty - period
snmpobject - period
webservice - pullPeriod
soapmethod - pullPeriod
-->

	<xsl:template match='object[@type="modbusproperty"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:variable name="poll" select="property[@name='period']/value/text()"/>
			<xsl:for-each select="property">
				<xsl:choose>
					<xsl:when test="@name='lastValue'">
						<property name='lastValue'>
							<xsl:copy-of select="*"/>
							<customtag name='poll'><xsl:value-of select="$poll"/></customtag>
						</property>
					</xsl:when>
					<xsl:when test="@name='period'"/>
					<xsl:otherwise>
						<xsl:copy-of select="self::*"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			<xsl:copy-of select="*[not(self::property)]" />
		</object>
	</xsl:template>

	<xsl:template match='object[@type="snmpobject"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:variable name="poll" select="property[@name='period']/value/text()"/>
			<xsl:for-each select="property">
				<xsl:choose>
					<xsl:when test="@name='lastValue'">
						<property name='lastValue'>
							<xsl:copy-of select="*"/>
							<customtag name='poll'><xsl:value-of select="$poll"/></customtag>
						</property>
					</xsl:when>
					<xsl:when test="@name='period'"/>
					<xsl:otherwise>
						<xsl:copy-of select="self::*"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			<xsl:copy-of select="*[not(self::property)]" />
		</object>
	</xsl:template>

	<xsl:template match='object[@type="webservice"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:variable name="poll" select="property[@name='pullPeriod']/value/text()"/>
			<xsl:for-each select="property">
				<xsl:choose>
					<xsl:when test="@name='lastValue'">
						<property name='lastValue'>
							<xsl:copy-of select="*"/>
							<customtag name='poll'><xsl:value-of select="$poll"/></customtag>
						</property>
					</xsl:when>
					<xsl:when test="@name='pullPeriod'"/>
					<xsl:otherwise>
						<xsl:copy-of select="self::*"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			<xsl:copy-of select="*[not(self::property)]" />
		</object>
	</xsl:template>

	<xsl:template match='object[@type="soapmethod"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:variable name="poll" select="property[@name='pullPeriod']/value/text()"/>
			<xsl:for-each select="property">
				<xsl:choose>
					<xsl:when test="@name='lastValue'">
						<property name='lastValue'>
							<xsl:copy-of select="*"/>
							<customtag name='poll'><xsl:value-of select="$poll"/></customtag>
						</property>
					</xsl:when>
					<xsl:when test="@name='pullPeriod'"/>
					<xsl:otherwise>
						<xsl:copy-of select="self::*"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			<xsl:copy-of select="*[not(self::property)]" />
		</object>
	</xsl:template>


	<!-- C) other object upgrades (Apollo -> Earth) -->


	<!-- C2) Missing "status" fields (Bug 4698) -->
	<xsl:template match='object[@type="ion_wye"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*" />
			<property name='status'>
				<value>1</value>
				<oldvalue></oldvalue>
			</property>
		</object>
	</xsl:template>	

	<xsl:template match='object[@type="ion_delta"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*" />
			<property name='status'>
				<value>1</value>
				<oldvalue></oldvalue>
			</property>
		</object>
	</xsl:template>


	<!-- C6) new properties on control devices -->
<!-- now handled by apollo2hermes
	<xsl:template match='object[@type="controlout_crah" or @type="controlout_vfd"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*" />
			<property name='validated'>
				<value>1</value>
				<oldvalue></oldvalue>
			</property>
			<property name='engaged'>
				<value>0</value>
				<oldvalue></oldvalue>
			</property>
		</object>
	</xsl:template>
-->


	<!-- C20) zone.map -> zone.image -->
	<xsl:template match='object[@type="zone"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>

			<xsl:for-each select="property">
				<xsl:choose>
					<xsl:when test="@name='map'">
						<property name="image">
							<xsl:copy-of select="*"/>
						</property>
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy-of select="self::*"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>

			<xsl:copy-of select="*[not(self::property)]" />

		</object>
	</xsl:template>

	<!-- C22) remove unused pcircuits / bcircuits -->
	<xsl:template match='object[@type="rack"]'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>

			<xsl:for-each select="property">
				<xsl:choose>
					<xsl:when test="@name='pCircuits'" />
					<xsl:when test="@name='bCircuits'" />
					<xsl:otherwise>
						<xsl:copy-of select="self::*"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>

			<xsl:copy-of select="*[not(self::property)]" />

		</object>
	</xsl:template>

    <!-- component migration -->
    
    <xsl:template match="components">
		<components>
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:apply-templates select="*" />
		</components>
	</xsl:template>
	
	<xsl:template match='component'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath"/>
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::object) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			
			<xsl:for-each select="object">
				<object>
					<xsl:attribute name="type">
						<xsl:choose>
							<xsl:when test='@type="network" or @type="gateway" or @type="node" or @type="sensor"'>
								<xsl:value-of select='concat("wsn",@type)'/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="@type"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="as"><xsl:value-of select="@as"/></xsl:attribute>				
					<xsl:attribute name="dlid"><xsl:value-of select="@dlid"/></xsl:attribute>				
				</object>	
			</xsl:for-each>
			
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select="varbinding"/>
			
		</component>
	</xsl:template>


	<xsl:template match='component[@type="remote-gateway"]'>
		<xsl:variable name="cleanAddress" select='normalize-space( replace( property[@name="address"]/text(), "TCP:", "" ) )'/>
		<xsl:variable name="parentId" select='//object[@type="network"]/property[@name="comport"]/value[contains(text(),$cleanAddress)]/../../dlid/text()'/>

		<xsl:variable name="numberOfNetworks" select="count( //object[@type='network'] ) div 2"/>
		<xsl:variable name="firstNetworkId" select="//object[@type='network']/dlid/text()"/>

		<!--<xsl:message>Gateway with [<xsl:value-of select="$cleanAddress"/>] [<xsl:value-of select="$parentId"/>]  </xsl:message>-->
		<xsl:choose>
			<!--<xsl:when test="string-length($parentId) > 0 and string-length($cleanAddress) > 0 ">-->
			<xsl:when test="($parentId and $cleanAddress) or ($numberOfNetworks=1)">

				<component>
					<xsl:attribute name="type">
						<xsl:value-of select="@type" />
					</xsl:attribute>
					<xsl:attribute name="classes">
						<xsl:value-of select="@classes" />
					</xsl:attribute>
					<xsl:attribute name="grouppath">
						<xsl:value-of select="@grouppath"/>
					</xsl:attribute>
					<xsl:copy-of select="*[not(self::consumer) and not(self::object) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
					<xsl:apply-templates select="display" />

					<object>
						<xsl:attribute name="type"><xsl:text>wsngateway</xsl:text></xsl:attribute>
						<xsl:attribute name="as"><xsl:value-of select='object/@as'/></xsl:attribute>
						<xsl:attribute name="dlid"><xsl:value-of select="object/@dlid"/></xsl:attribute>
					</object>
					<xsl:apply-templates select='property[not(@name="address")]'/>
					<xsl:variable name="address" select='property[@name="address"]/text()'/>

					<xsl:choose>
						<xsl:when test='contains($address,":")'>
							<property name="address" display="Gateway Address" type='java.lang.String' editable='true' displayed='true' dimension=''><xsl:value-of select='substring-after($address,":")'/></property>
						</xsl:when>
						<xsl:otherwise>
							<property name="address" display="Gateway Address" type='java.lang.String' editable='true' displayed='true' dimension=''><xsl:value-of select='$address'/></property>
						</xsl:otherwise>
					</xsl:choose>

					<property name="protocol" display="Protocol" type="java.lang.String" editable="true" displayed="true">
						<xsl:choose>
							<xsl:when test='contains($address,"TCP:")'>
								<xsl:text>TCP_RG2</xsl:text>
							</xsl:when>
							<xsl:when test='contains($address,"COM:")'>
								<xsl:text>COM</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>TCP_RG2</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</property>
					<property name="security_mode" display="Security Mode" type="java.lang.String" editable="true" displayed="true" valueChoices="AES-CFB-PSK:AES-CFB-PSK,None:None">None</property>
					<property name="key" display="Key" type="java.lang.String" editable="true" displayed="true"></property>

					<xsl:apply-templates select="consumer"/>
					<xsl:apply-templates select="producer"/>
					<xsl:apply-templates select="varbinding"/>

					<varbinding vars="protocol">
						<property>$gateway/properties[name="protocol"]</property>
					<value>protocol</value>
					</varbinding>
					<varbinding vars="security_mode">
						<property>$gateway/properties[name="securityMode"]</property>
						<value>security_mode</value>
					</varbinding>
					<varbinding vars="key">
						<property>$gateway/properties[name="key"]</property>
						<value>key</value>
					</varbinding>
					<varbinding vars="mac_id">
						<property>$gateway/properties[name="id"]</property>
						<value>lid.generateGatewayLogicalId(mac_id , self)</value>
					</varbinding>
				</component>

			</xsl:when>
			<xsl:otherwise>
				<xsl:message>A valid Network for Gateway with name '<xsl:value-of select='property[@name="name"]/text()' />' could not be determined and has been removed. </xsl:message>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>
	
	<xsl:template match='component[@type="network"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath"/>
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::object) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			
			<object>
				<xsl:attribute name="type"><xsl:text>wsnnetwork</xsl:text></xsl:attribute>				
				<xsl:attribute name="as"><xsl:value-of select='object/@as'/></xsl:attribute>				
				<xsl:attribute name="dlid"><xsl:value-of select="object/@dlid"/></xsl:attribute>				
			</object>
			
			<xsl:apply-templates select='property[not(@name="gateways")]'/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding[not(@vars="gateways")]'/>
			
		</component>
	</xsl:template>
	
	<xsl:template match='consumer'>

		<xsl:choose>
			<xsl:when test="@id!='primaryCircuits' and @id!='backupCircuits'">
				<!-- C22) so long, pb circuits! -->

				<consumer>
					<xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
					<xsl:attribute name="datatype"><xsl:value-of select="@datatype" /></xsl:attribute>
					<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
					<xsl:attribute name="property"><xsl:value-of select="@property" /></xsl:attribute>
					<xsl:attribute name="required"><xsl:value-of select="@required" /></xsl:attribute>
					<xsl:copy-of select="*" />
				</consumer>

			</xsl:when>
		</xsl:choose>

	</xsl:template>

	<xsl:template match='producer'>
		<producer>
			<xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
			<xsl:attribute name="datatype"><xsl:value-of select="@datatype" /></xsl:attribute>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="object"><xsl:value-of select="@object" /></xsl:attribute>
			<xsl:copy-of select="*" />
		</producer>
	</xsl:template>

	<xsl:template match='property'>
		<property>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
<!--
			<xsl:attribute name="display"><xsl:value-of select="@display" /></xsl:attribute>
-->
			<xsl:attribute name="display">
				<xsl:choose>
					<!-- C21) rename BTU Table to BTUh table -->
					<xsl:when test="@name='btu_table'">
						BTUh Table
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@display" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>

			<xsl:attribute name="type"><xsl:value-of select="@type" /></xsl:attribute>
			<xsl:attribute name="editable"><xsl:value-of select="@editable" /></xsl:attribute>
			<xsl:attribute name="displayed"><xsl:value-of select="@displayed" /></xsl:attribute>
			<xsl:attribute name="dimension"><xsl:value-of select="@dimension" /></xsl:attribute>
			<xsl:if test="@valueChoices">
				<xsl:attribute name="valueChoices"><xsl:value-of select="@valueChoices" /></xsl:attribute>
			</xsl:if>
			<!--<xsl:value-of select="." />-->

			<!-- C10) convert rotation field value from radians to degrees -->
			<xsl:choose>
				<xsl:when test="@name='rotation'">
					<xsl:choose>
						<xsl:when test="number(text()) &lt;= 0">
							<xsl:value-of select="0 - round( text() * (180 div 3.141592653589793) )" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="(0 - round( text() * (180 div 3.141592653589793) )) + 360" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="." />
				</xsl:otherwise>
			</xsl:choose>

		</property>
	</xsl:template>

	<xsl:template match='display'>
		<display>

			<xsl:copy-of select="*[not(self::shape) and not(self::name) and not(self::description)]" />
			<xsl:if test='shape != ""'>
				<shape><xsl:call-template name="display-shape"><xsl:with-param name="cfg" select="../@type" /></xsl:call-template></shape>
			</xsl:if>

			<xsl:if test='name != ""'>
				<name><xsl:call-template name="display-name"><xsl:with-param name="cfg" select="../@type" /></xsl:call-template></name>
			</xsl:if>
			<xsl:if test='description != ""'>
				<description><xsl:call-template name="display-description"><xsl:with-param name="cfg" select="../@type" /></xsl:call-template></description>
			</xsl:if>

			<xsl:call-template name="deprecation"><xsl:with-param name="cfg" select="../@type" /></xsl:call-template>
			<xsl:call-template name="newHalos"><xsl:with-param name="cfg" select="../@type" /></xsl:call-template>
		</display>
	</xsl:template>


	<xsl:template name="display-shape">
		<xsl:param name="cfg" />
		<xsl:choose>
			<xsl:when test='$cfg = "generic-modbus-5"'><xsl:text>modbus_input_numeric</xsl:text></xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="shape" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!--  Display name Change -->
	<xsl:template name="display-name">
		<xsl:param name="cfg" />
		<xsl:choose>
			<xsl:when test='$cfg = "generic-modbus-5"'><xsl:text>5-Point Modbus Integrator</xsl:text></xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="name" />
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<xsl:template name="display-description">
		<xsl:param name="cfg" />
		<xsl:choose>
			<xsl:when test='$cfg = "generic-modbus-5"'><xsl:text>Pulls a 5 configurable values from a Modbus device and returns those results to a calculation graph.  This Component goes in a Calculation Group and requires a Modbus Data Source.</xsl:text></xsl:when>

			<xsl:otherwise>
				<xsl:value-of select="description" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>



	<xsl:template match="varbinding">
		<!--  varbinding>
			<xsl:attribute name="vars"><xsl:value-of select="@vars"/></xsl:attribute>
			<xsl:copy-of select="*"/>
		</varbinding-->
		<xsl:choose>
    		<xsl:when test="contains(property[1], 'networkId')"/>
    		
			<xsl:when test="contains(property[1], 'softbased')"/>
			<xsl:when test="contains(property[1], 'networkDefaultSampleInterval')"/>
			<xsl:when test="contains(property[1], 'networkStartTimestamp')"/>
			<xsl:when test="contains(property[1], 'comport')"/>
			<xsl:when test="contains(property[1], 'baseStationPort')"/>
			<xsl:when test="contains(property[1], 'baseStationHostname')"/>
			
			<xsl:when test="contains(property[1], 'batteryCapacityTimestamp')"/>
			<xsl:when test="contains(property[1], 'nodeType')"/>
			<xsl:when test="contains(property[1], 'nodeIsPanCoordinatorFlg')"/>
			
			<xsl:when test="contains(property[1], 'firmwareType')"/>
			<xsl:when test="contains(property[1], 'sensorId')"/>
			<xsl:when test="contains(property[1], 'networkId')"/>
			<xsl:when test="contains(property[1], 'topologyId')"/>
			
			<xsl:otherwise>
				<xsl:choose>
					<!-- GATEWAY -->
					<xsl:when test="contains(property[1], 'uniqueId')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'uniqueId'"/>
				    		<xsl:with-param name="newName" select="'mac'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>
				
					<!-- NETWORKS -->
					<xsl:when test="contains(property[1], 'networkDescription')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'networkDescription'"/>
				    		<xsl:with-param name="newName" select="'desc'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>
					
					<!-- NODES -->
					<xsl:when test="contains(property[1], 'nodeDescription')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'nodeDescription'"/>
				    		<xsl:with-param name="newName" select="'desc'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>
					<xsl:when test="contains(property[1], 'platform')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'platform'"/>
				    		<xsl:with-param name="newName" select="'platformId'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>
					<xsl:when test="contains(property[1], 'nodeSamplingInterval')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'nodeSamplingInterval'"/>
				    		<xsl:with-param name="newName" select="'period'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>
					<xsl:when test="contains(property[1], 'nodePhysicalId')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'nodePhysicalId'"/>
				    		<xsl:with-param name="newName" select="'mac'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>

						<xsl:variable name="nodename" select=" substring-before(property[1], '/') "/>

						<varbinding>
							<xsl:attribute name="vars"><xsl:value-of select="@vars"/></xsl:attribute>
							<property><xsl:value-of select="$nodename"></xsl:value-of>/properties[name="id"]</property>
							<value>lid.generateLogicalId(<xsl:value-of select="@vars"/> , self)</value>
						</varbinding>

					</xsl:when>
					<xsl:when test="contains(property[1], 'nodeLogicalId')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'nodeLogicalId'"/>
				    		<xsl:with-param name="newName" select="'id'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>
					
					
					<!-- SENSORS -->				
					<xsl:when test="contains(property[1], 'sensorDescription')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'sensorDescription'"/>
				    		<xsl:with-param name="newName" select="'desc'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>
					<xsl:when test="contains(property[1], 'sensorDisplayMin')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'sensorDisplayMin'"/>
				    		<xsl:with-param name="newName" select="'min'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>
					<xsl:when test="contains(property[1], 'sensorDisplayMax')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'sensorDisplayMax'"/>
				    		<xsl:with-param name="newName" select="'max'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>

					<xsl:when test="contains(property[1], 'AMin')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'AMin'"/>
				    		<xsl:with-param name="newName" select="'aMin'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>
					<xsl:when test="contains(property[1], 'AMax')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'AMax'"/>
				    		<xsl:with-param name="newName" select="'aMax'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>
					<xsl:when test="contains(property[1], 'RMin')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'RMin'"/>
				    		<xsl:with-param name="newName" select="'rMin'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>
					<xsl:when test="contains(property[1], 'RMax')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'RMax'"/>
				    		<xsl:with-param name="newName" select="'rMax'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>
					
					<xsl:when test="contains(property[1], 'visualLocX')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'visualLocX'"/>
				    		<xsl:with-param name="newName" select="'x'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>
					<xsl:when test="contains(property[1], 'visualLocY')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'visualLocY'"/>
				    		<xsl:with-param name="newName" select="'y'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>
					<xsl:when test="contains(property[2], 'visualLocX')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'visualLocX'"/>
				    		<xsl:with-param name="newName" select="'x'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>
					<xsl:when test="contains(property[2], 'visualLocY')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'visualLocY'"/>
				    		<xsl:with-param name="newName" select="'y'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>
					
					<xsl:when test="contains(property[1], 'visualLayer')">
				    	<xsl:call-template name="rename_properties">
				    		<xsl:with-param name="oldName" select="'visualLayer'"/>
				    		<xsl:with-param name="newName" select="'z'"/>
				    		<xsl:with-param name="vars" select="@vars"/>
				    		<xsl:with-param name="value" select="value"/>
				    	</xsl:call-template>
					</xsl:when>


					<xsl:when test="contains(property[1], 'delta')">
						<!-- replace varbindings to delta with varbindings to smartSendThreshold -->
						<!-- only do this when @vars is 'hot_delta', 'cold_delta', or 'delta' -->
						<!-- xsl:message> <xsl:value-of select="@vars"/>  </xsl:message -->
						<xsl:choose>
							<xsl:when test="@vars='hot_delta' or @vars='cold_delta' or @vars='delta'">
								<xsl:call-template name="rename_properties">
									<xsl:with-param name="oldName" select="'delta'"/>
									<xsl:with-param name="newName" select="'smartSendThreshold'"/>
									<xsl:with-param name="vars" select="@vars"/>
									<xsl:with-param name="value" select=" replace( value, '\*100', '' ) "/>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:copy-of select="self::*"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>

					<!-- C10) special case for hrows:
					 the hrows have a bunch of varbindings that use rotation but not a visual location prop, BUT
					 we don't store the rotation value directly into the rack objects, so we can just
					 do a brute force replace on rotations in the value field.
					 -->
					<xsl:when test="../@type='dual-horizontal-row-thermanode2' or ../@type='single-horizontal-row-thermanode2'">
						<varbinding>
							<xsl:attribute name="vars"><xsl:value-of select="@vars"/></xsl:attribute>
							<xsl:copy-of select="*[not(self::value)]"/>
							<value>
								<xsl:value-of select="replace(value, 'rotation', 'Math.toRadians(-rotation)' )"/>
							</value>
						</varbinding>
					</xsl:when>

					<xsl:otherwise>
						<xsl:copy-of select="self::*"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>	
	
	<xsl:template name="rename_properties">
    	<xsl:param name="oldName" select="''"/>
    	<xsl:param name="newName" select="''"/>
    	<xsl:param name="vars" select="''"/>
    	<xsl:param name="value" select="''"/>
    	
		<xsl:element name="varbinding">
			<xsl:attribute name="vars">
				<xsl:value-of select="$vars"/>
			</xsl:attribute>
	    	<xsl:for-each select="property">
	    		<xsl:variable name="cur" select="."/>
				<xsl:choose>
		    		<xsl:when test="contains($cur, $oldName)">
		    			<xsl:variable name="rep" select="concat(substring-before($cur,$oldName),$newName,substring-after($cur,$oldName))"/>
			    			<property>
			    				<xsl:value-of select="$rep"></xsl:value-of>
			    			</property>
		    		</xsl:when>
		    		<xsl:otherwise>
		    			<xsl:copy-of select="self::*"/>
		    		</xsl:otherwise>
		    	</xsl:choose>
	    	</xsl:for-each>
			<value>
				<!-- C10) add deg-> radians conversion.  Don't need a check here because this will only run on properties being renamed, which means this applies to the visual location varbindings, but not the passthru for the rotation itself. -->
				<xsl:value-of select="replace($value, 'rotation', 'Math.toRadians(-rotation)' )"/>
				<!-- <xsl:value-of select="$value"></xsl:value-of> -->
			</value>
    	</xsl:element>
    </xsl:template>


<!-- other component-level upgrades -->

<!-- A1) upgrade varbindings for poll tag -->
	<xsl:template match='component[@type="ion6200-wye" or @type="ion6200-delta"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding[not(@vars="pullPeriod") ]' />
			<varbinding vars="pullPeriod">
				<property>$ionmeter/properties[name="registers"]/children/properties[name="lastValue"]/tags[tagName='poll']</property>
				<value>pullPeriod*60000.0</value>
			</varbinding>
		</component>
	</xsl:template>

	<xsl:template match='component[@type="modbus-squared-bcms-custom" or @type="modbus-pdi-bcms-custom"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding[not(@vars="pullPeriod") ]' />
			<varbinding vars="pullPeriod">
				<property>$pdi/properties[name="registers"]/children/properties[name="lastValue"]/tags[tagName='poll']</property>
				<value>pullPeriod*60000.0</value>
			</varbinding>
		</component>
	</xsl:template>

	<xsl:template match='component[@type="soapservice-input-numeric" or @type="soapservice-input-status"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding[not(@vars="poll") ]' />
			<varbinding vars="poll">
				<property>$soapmethod/properties[name="lastValue"]/tags[tagName="poll"]</property>
				<value>poll * 60000</value>
			</varbinding>
		</component>
	</xsl:template>

	<xsl:template match='component[@type="webservice-input-numeric" or @type="webservice-input-status"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding[not(@vars="poll") ]' />
			<varbinding vars="poll">
				<property>$webservice/properties[name="lastValue"]/tags[tagName='poll']</property>
				<value>poll * 60000</value>
			</varbinding>
		</component>
	</xsl:template>

	<xsl:template match='component[@type="snmpobject-input-numeric" or @type="snmpobject-input-status"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding[not(@vars="poll") ]' />
			<varbinding vars="poll">
				<property>$snmpobject/properties[name="lastValue"]/tags[tagName='poll']</property>
				<value>poll*60000</value>
			</varbinding>
		</component>
	</xsl:template>


	<xsl:template match='component[@type="modbus-1reg-integrator" or @type="modbus-1reg-integrator32" or @type="doe-ups-modbus-integrator" or @type="stanford-pue-metering-modbus-integrator" ]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding[not(@vars="poll") ]' />
			<varbinding vars="pullPeriod">
				<property>$device/properties[name="registers"]/children/properties[name="lastValue"]/tags[tagName='poll']</property>
				<value>pullPeriod * 60000.0</value>
			</varbinding>
		</component>
	</xsl:template>

	<!-- A1) and C16) upgrade to El Cinco! -->
	<xsl:template match='component[@type="generic-modbus-5"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />

			<xsl:choose>
				<xsl:when test="not(property[@name='registerMSW_1'])">
					<!-- needs the upgrade; this is the 5.3.1 version or earlier.  Needs both the new properties and the scale prop rename. -->

					<xsl:apply-templates select="property[ not(@name='$modbusdevice_registers_register1_scale') and not(@name='$modbusdevice_registers_register2_scale')  and not(@name='$modbusdevice_registers_register3_scale') and not(@name='$modbusdevice_registers_register4_scale') and not(@name='$modbusdevice_registers_register5_scale') and not(@name='$modbusdevice_registers_register1_id') and not(@name='$modbusdevice_registers_register2_id')  and not(@name='$modbusdevice_registers_register3_id') and not(@name='$modbusdevice_registers_register4_id') and not(@name='$modbusdevice_registers_register5_id')  ]"/>

					<property name='$modbusdevice_registers_register1_id' display='Register 1 ID' type='java.lang.Integer' editable='true' displayed='true' dimension=''>
						<xsl:value-of select="property[@name='$modbusdevice_registers_register1_id']" />
					</property>
					<property name="registerMSW_1" display="Register 1 Low (if 32 bit)" type="java.lang.Integer" editable="true" displayed="true">0</property>
					<property name='$modbusdevice_registers_register1_scale' display='Register 1 Scale' type='java.lang.Double' editable='true' displayed='true' dimension=''>
						<xsl:value-of select="property[@name='$modbusdevice_registers_register1_scale']" />
					</property>
					<property name="type_1" display="Register 1 Data Type" type="java.lang.String" editable="true" displayed="true"
					valueChoices="Signed Integer 16:INT16,Unsigned Integer 16:UINT16,Signed Integer 32:INT32,Unsigned Integer 32:UINT32,Floating Point 32:FLOAT32">
					INT16</property>

					<property name='$modbusdevice_registers_register2_id' display='Register 2 ID' type='java.lang.Integer' editable='true' displayed='true' dimension=''>
						<xsl:value-of select="property[@name='$modbusdevice_registers_register2_id']" />
					</property>
					<property name="registerMSW_2" display="Register 2 Low (if 32 bit)" type="java.lang.Integer" editable="true" displayed="true">0</property>
					<property name='$modbusdevice_registers_register2_scale' display='Register 2 Scale' type='java.lang.Double' editable='true' displayed='true' dimension=''>
						<xsl:value-of select="property[@name='$modbusdevice_registers_register2_scale']" />
					</property>
					<property name="type_2" display="Register 2 Data Type" type="java.lang.String" editable="true" displayed="true"
					valueChoices="Signed Integer 16:INT16,Unsigned Integer 16:UINT16,Signed Integer 32:INT32,Unsigned Integer 32:UINT32,Floating Point 32:FLOAT32">
					INT16</property>

					<property name='$modbusdevice_registers_register3_id' display='Register 3 ID' type='java.lang.Integer' editable='true' displayed='true' dimension=''>
						<xsl:value-of select="property[@name='$modbusdevice_registers_register3_id']" />
					</property>
					<property name="registerMSW_3" display="Register 3 Low (if 32 bit)" type="java.lang.Integer" editable="true" displayed="true">0</property>
					<property name='$modbusdevice_registers_register3_scale' display='Register 3 Scale' type='java.lang.Double' editable='true' displayed='true' dimension=''>
						<xsl:value-of select="property[@name='$modbusdevice_registers_register3_scale']" />
					</property>
					<property name="type_3" display="Register 3 Data Type" type="java.lang.String" editable="true" displayed="true"
					valueChoices="Signed Integer 16:INT16,Unsigned Integer 16:UINT16,Signed Integer 32:INT32,Unsigned Integer 32:UINT32,Floating Point 32:FLOAT32">
					INT16</property>

					<property name='$modbusdevice_registers_register4_id' display='Register 4 ID' type='java.lang.Integer' editable='true' displayed='true' dimension=''>
						<xsl:value-of select="property[@name='$modbusdevice_registers_register4_id']" />
					</property>
					<property name="registerMSW_4" display="Register 4 Low (if 32 bit)" type="java.lang.Integer" editable="true" displayed="true">0</property>
					<property name='$modbusdevice_registers_register4_scale' display='Register 4 Scale' type='java.lang.Double' editable='true' displayed='true' dimension=''>
						<xsl:value-of select="property[@name='$modbusdevice_registers_register4_scale']" />
					</property>
					<property name="type_4" display="Register 4 Data Type" type="java.lang.String" editable="true" displayed="true"
					valueChoices="Signed Integer 16:INT16,Unsigned Integer 16:UINT16,Signed Integer 32:INT32,Unsigned Integer 32:UINT32,Floating Point 32:FLOAT32">
					INT16</property>

					<property name='$modbusdevice_registers_register5_id' display='Register 5 ID' type='java.lang.Integer' editable='true' displayed='true' dimension=''>
						<xsl:value-of select="property[@name='$modbusdevice_registers_register5_id']" />
					</property>
					<property name="registerMSW_5" display="Register 5 Low (if 32 bit)" type="java.lang.Integer" editable="true" displayed="true">0</property>
					<property name='$modbusdevice_registers_register5_scale' display='Register 5 Scale' type='java.lang.Double' editable='true' displayed='true' dimension=''>
						<xsl:value-of select="property[@name='$modbusdevice_registers_register5_scale']" />
					</property>
					<property name="type_5" display="Register 5 Data Type" type="java.lang.String" editable="true" displayed="true"
					valueChoices="Signed Integer 16:INT16,Unsigned Integer 16:UINT16,Signed Integer 32:INT32,Unsigned Integer 32:UINT32,Floating Point 32:FLOAT32">
					INT16</property>

				</xsl:when>
				<xsl:otherwise>
					<!-- this handles the enhanced component lib released with 5.3.2 - which already has all of this fixed, so we just copy it all on over. -->
					<xsl:apply-templates select="property"/>
				</xsl:otherwise>
			</xsl:choose>


			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding[not(@vars="pullPeriod") ]' />
			<varbinding vars="pullPeriod">
				<property>$modbusdevice/properties[name="registers"]/children/properties[name="lastValue"]/tags[tagName='poll']</property>
				<value>pullPeriod * 60000.0</value>
			</varbinding>

			<xsl:if test="not(varbinding[@vars='registerMSW_1'])">

				<varbinding vars="registerMSW_1">
					<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register1"]]/properties[name="msw"]</property>
					<value>registerMSW_1</value>
				</varbinding>
				<varbinding vars="type_1">
					<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register1"]]/properties[name="type"]</property>
					<value>type_1</value>
				</varbinding>

				<varbinding vars="registerMSW_2">
					<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register2"]]/properties[name="msw"]</property>
					<value>registerMSW_2</value>
				</varbinding>
				<varbinding vars="type_2">
					<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register2"]]/properties[name="type"]</property>
					<value>type_2</value>
				</varbinding>

				<varbinding vars="registerMSW_3">
					<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register3"]]/properties[name="msw"]</property>
					<value>registerMSW_3</value>
				</varbinding>
				<varbinding vars="type_3">
					<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register3"]]/properties[name="type"]</property>
					<value>type_3</value>
				</varbinding>

				<varbinding vars="registerMSW_4">
					<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register4"]]/properties[name="msw"]</property>
					<value>registerMSW_4</value>
				</varbinding>
				<varbinding vars="type_4">
					<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register4"]]/properties[name="type"]</property>
					<value>type_4</value>
				</varbinding>

				<varbinding vars="registerMSW_5">
					<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register5"]]/properties[name="msw"]</property>
					<value>registerMSW_5</value>
				</varbinding>
				<varbinding vars="type_5">
					<property>$modbusdevice/properties[name="registers"]/children[properties[name="name"][value="register5"]]/properties[name="type"]</property>
					<value>type_5</value>
				</varbinding>

			</xsl:if>

		</component>
	</xsl:template>


	<!-- B3) jawa conducers to env racks -->
	<xsl:template match='component[starts-with(@type, "rack-control-")]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding' />

			<conducer id="PEBridge" name="Power And Environmental Rack Pair">
				<producer id="envRack" datatype="PEBridge_E" name="Environmental Rack Output" object='$rack' />
				<consumer id="powerRack" datatype="PEBridge_P" name="Power Rack Input" property="$rack/properties[name='powerRack']" required="false" />
			</conducer>

		</component>
	</xsl:template>


	<!-- C3) missing varbindings for old revision dual probe crahs -->
	<!-- also C5) -->
	<xsl:template match='component[@type="crah-dual-probe-thermanode" ]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>

			<xsl:apply-templates select='varbinding[not(@vars="x") and not(@vars="y") ]' />

			<varbinding vars='x'>
			  <property>$crac/properties[name="x"]</property>
			  <value>x</value>
			</varbinding>
			<varbinding vars='x'>
			  <property>$rNode/properties[name="x"]</property>
			  <property>$rNode/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			  <property>$rNode/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			  <property>$rNode/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			  <property>$rNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			  <value>x-6</value>
			</varbinding>
			<varbinding vars='x'>
			  <property>$sNode/properties[name="x"]</property>
			  <property>$sNode/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="x"]</property>
			  <property>$sNode/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="x"]</property>
			  <property>$sNode/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
			  <property>$sNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
			  <value>x+6</value>
			</varbinding>
			<varbinding vars='y'>
			  <property>$crac/properties[name="y"]</property>
			  <value>y</value>
			</varbinding>
			<varbinding vars='y'>
			  <property>$rNode/properties[name="y"]</property>
			  <property>$rNode/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			  <property>$rNode/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			  <property>$rNode/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			  <property>$rNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			  <value>y-6</value>
			</varbinding>
			<varbinding vars='y'>
			  <property>$sNode/properties[name="y"]</property>
			  <property>$sNode/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="y"]</property>
			  <property>$sNode/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="y"]</property>
			  <property>$sNode/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
			  <property>$sNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
			  <value>y+6</value>
			</varbinding>

			<!-- new LI properties -->
			<property name="supplyLayerTop" display="Display Supply in LI Top" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="supplyLayerMiddle" display="Display Supply in LI Middle" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="supplyLayerBottom" display="Display Supply in LI Bottom" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="supplyLayerSubfloor" display="Display Supply in LI Subfloor" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="returnLayerTop" display="Display Return in LI Top" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="returnLayerMiddle" display="Display Return in LI Middle" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="returnLayerBottom" display="Display Return in LI Bottom" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="returnLayerSubfloor" display="Display Return in LI Subfloor" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<varbinding vars="supplyLayerTop,supplyLayerMiddle,supplyLayerBottom,supplyLayerSubfloor">
				<property>$sNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]</property>
				<value>
					top = 0;
					if (supplyLayerTop){ top = 0x20000; }
					middle = 0;
					if (supplyLayerMiddle){ middle = 0x4000; }
					bottom = 0;
					if (supplyLayerBottom){ bottom = 0x800; }
					sub = 0;
					if (supplyLayerSubfloor){ sub = 0x100; }
					total = top | middle | bottom | sub;
					total
				</value>
			</varbinding>
			<varbinding vars="returnLayerTop,returnLayerMiddle,returnLayerBottom,returnLayerSubfloor">
				<property>$rNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]</property>
				<value>
					top = 0;
					if (returnLayerTop){ top = 0x20000; }
					middle = 0;
					if (returnLayerMiddle){ middle = 0x4000; }
					bottom = 0;
					if (returnLayerBottom){ bottom = 0x800; }
					sub = 0;
					if (returnLayerSubfloor){ sub = 0x100; }
					total = top | middle | bottom | sub;
					total
				</value>
			</varbinding>

		</component>
	</xsl:template>

	<!-- C5) LI fields for other CRAHs -->

	<xsl:template match='component[@type="crah-single-plenumrated-thermanode" ]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding' />
			<!-- new LI properties -->
			<property name="supplyLayerTop" display="Display Supply in LI Top" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="supplyLayerMiddle" display="Display Supply in LI Middle" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="supplyLayerBottom" display="Display Supply in LI Bottom" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="supplyLayerSubfloor" display="Display Supply in LI Subfloor" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="returnLayerTop" display="Display Return in LI Top" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="returnLayerMiddle" display="Display Return in LI Middle" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="returnLayerBottom" display="Display Return in LI Bottom" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="returnLayerSubfloor" display="Display Return in LI Subfloor" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<varbinding vars="supplyLayerTop,supplyLayerMiddle,supplyLayerBottom,supplyLayerSubfloor">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="z"]</property>
				<value>
					top = 0;
					if (supplyLayerTop){ top = 0x20000; }
					middle = 0;
					if (supplyLayerMiddle){ middle = 0x4000; }
					bottom = 0;
					if (supplyLayerBottom){ bottom = 0x800; }
					sub = 0;
					if (supplyLayerSubfloor){ sub = 0x100; }
					total = top | middle | bottom | sub;
					total
				</value>
			</varbinding>
			<varbinding vars="returnLayerTop,returnLayerMiddle,returnLayerBottom,returnLayerSubfloor">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]</property>
				<value>
					top = 0;
					if (returnLayerTop){ top = 0x20000; }
					middle = 0;
					if (returnLayerMiddle){ middle = 0x4000; }
					bottom = 0;
					if (returnLayerBottom){ bottom = 0x800; }
					sub = 0;
					if (returnLayerSubfloor){ sub = 0x100; }
					total = top | middle | bottom | sub;
					total
				</value>
			</varbinding>
		</component>
	</xsl:template>


	<xsl:template match='component[@type="crah-thermanode" ]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding' />
			<!-- new LI properties -->
			<property name="supplyLayerTop" display="Display Supply in LI Top" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="supplyLayerMiddle" display="Display Supply in LI Middle" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="supplyLayerBottom" display="Display Supply in LI Bottom" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="supplyLayerSubfloor" display="Display Supply in LI Subfloor" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="returnLayerTop" display="Display Return in LI Top" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="returnLayerMiddle" display="Display Return in LI Middle" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="returnLayerBottom" display="Display Return in LI Bottom" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="returnLayerSubfloor" display="Display Return in LI Subfloor" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<varbinding vars="supplyLayerTop,supplyLayerMiddle,supplyLayerBottom,supplyLayerSubfloor">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="z"]</property>
				<value>
					top = 0;
					if (supplyLayerTop){ top = 0x20000; }
					middle = 0;
					if (supplyLayerMiddle){ middle = 0x4000; }
					bottom = 0;
					if (supplyLayerBottom){ bottom = 0x800; }
					sub = 0;
					if (supplyLayerSubfloor){ sub = 0x100; }
					total = top | middle | bottom | sub;
					total
				</value>
			</varbinding>
			<varbinding vars="returnLayerTop,returnLayerMiddle,returnLayerBottom,returnLayerSubfloor">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]</property>
				<value>
					top = 0;
					if (returnLayerTop){ top = 0x20000; }
					middle = 0;
					if (returnLayerMiddle){ middle = 0x4000; }
					bottom = 0;
					if (returnLayerBottom){ bottom = 0x800; }
					sub = 0;
					if (returnLayerSubfloor){ sub = 0x100; }
					total = top | middle | bottom | sub;
					total
				</value>
			</varbinding>
		</component>
	</xsl:template>


	<xsl:template match='component[@type="crah-dual-thermanode" ]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding' />
			<!-- new LI properties -->
			<property name="supplyLayerTop" display="Display Supply in LI Top" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="supplyLayerMiddle" display="Display Supply in LI Middle" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="supplyLayerBottom" display="Display Supply in LI Bottom" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="supplyLayerSubfloor" display="Display Supply in LI Subfloor" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="returnLayerTop" display="Display Return in LI Top" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="returnLayerMiddle" display="Display Return in LI Middle" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="returnLayerBottom" display="Display Return in LI Bottom" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<property name="returnLayerSubfloor" display="Display Return in LI Subfloor" type="java.lang.Boolean" editable="true" displayed="true">false</property>
			<varbinding vars="supplyLayerTop,supplyLayerMiddle,supplyLayerBottom,supplyLayerSubfloor">
				<property>$sNode/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="z"]</property>
				<value>
					top = 0;
					if (supplyLayerTop){ top = 0x20000; }
					middle = 0;
					if (supplyLayerMiddle){ middle = 0x4000; }
					bottom = 0;
					if (supplyLayerBottom){ bottom = 0x800; }
					sub = 0;
					if (supplyLayerSubfloor){ sub = 0x100; }
					total = top | middle | bottom | sub;
					total
				</value>
			</varbinding>
			<varbinding vars="returnLayerTop,returnLayerMiddle,returnLayerBottom,returnLayerSubfloor">
				<property>$rNode/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]/properties[name="z"]</property>
				<value>
					top = 0;
					if (returnLayerTop){ top = 0x20000; }
					middle = 0;
					if (returnLayerMiddle){ middle = 0x4000; }
					bottom = 0;
					if (returnLayerBottom){ bottom = 0x800; }
					sub = 0;
					if (returnLayerSubfloor){ sub = 0x100; }
					total = top | middle | bottom | sub;
					total
				</value>
			</varbinding>
		</component>
	</xsl:template>


	<!-- c7): add middle and bottom producer to all 5.2.1 legacy components -->
	<xsl:template match='component[@type="rack-lhs-endcap-rt-thermanode2" or @type="rack-lhs-endcap-nrt-thermanode2" or @type="rack-rhs-endcap-rt-thermanode2" or @type="rack-rhs-endcap-nrt-thermanode2" ]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding' />

			<xsl:if test='not(producer[@id="topinlet"])'>
				<producer id="topinlet" datatype="temperature" name="Top Inlet Temp" object='$rack/properties[name="cTop"]/referrent' />
			</xsl:if>
			<xsl:apply-templates select='producer[not(@id="midinlet") and not(@id="botinlet")]' />
			<producer id="midinlet" datatype="temperature_mid" name="Middle Inlet Temp" object='$rack/properties[name="cMid"]/referrent' />
			<producer id="botinlet" datatype="temperature_bot" name="Bottom Inlet Temp" object='$rack/properties[name="cBot"]/referrent' />

			<conducer id="PEBridge" name="Power And Environmental Rack Pair">
				<producer id="envRack" datatype="PEBridge_E" name="Environmental Rack Output" object='$rack' />
				<consumer id="powerRack" datatype="PEBridge_P" name="Power Rack Input" property="$rack/properties[name='powerRack']" required="false" />
			</conducer>
		</component>
	</xsl:template>


	<!-- C8) add retries field to modbus network -->
	<xsl:template match='component[@type="modbus-tcp-network" ]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding' />
			<property name="retries" display="Retries" type="java.lang.Integer" editable="true" displayed="true">3</property>
			<varbinding vars="retries">
				<property>$modbusnetwork/properties[name="retries"]</property>
				<value>retries</value>
			</varbinding>
		</component>
	</xsl:template>


	<!-- C14) halos to LI nodes -->
	<xsl:template match='component[@type="control-verticalstring-li-thermanode" ]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
				<xsl:text>,haloed</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding' />
		</component>
	</xsl:template>

	<xsl:template name="newHalos">
		<xsl:param name="cfg" />
		<xsl:choose>
			<xsl:when test='$cfg= "control-verticalstring-li-thermanode"'> <haloradius>60.0</haloradius> <halocolor>-16776961</halocolor> </xsl:when>

		</xsl:choose>
	</xsl:template>

	<!-- C15) smart send to hrows -->
	<!-- C17) rH to LI for hrows -->
	<xsl:template match='component[@type="single-horizontal-row-thermanode2" or @type="horizontal-row-thermanode"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select='property[not(@name="smartsend_delta") ]'/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding[not(@vars="smartsend_delta") and not(@vars="li_layer") ]' />

			<property name="smartsend_delta" display="Temperature Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">5</property>
			<varbinding vars="smartsend_delta">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendThreshold"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendThreshold"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="smartSendThreshold"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="smartSendThreshold"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="smartSendThreshold"]</property>
				<value>(int)(smartsend_delta)</value>
			</varbinding>

			<varbinding vars="li_layer">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="1"]]/properties[name="z"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="z"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="z"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="z"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="z"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="z"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="z"]</property>
				<value>li_layer</value>
			</varbinding>


		</component>
	</xsl:template>

	<xsl:template match='component[@type="dual-horizontal-row-thermanode2" ]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select='property[not(@name="cold_delta") and not(@name="hot_delta") ] '/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding[not(@vars="cold_delta") and not(@vars="hot_delta") ]' />

			<property name="cold_delta" display="Intake Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">5</property>
			<property name="hot_delta" display="Exhaust Delta Send Threshold" type="java.lang.Integer" editable="true" displayed="true" dimension="temperatureDelta">10</property>

			<varbinding vars="cold_delta">
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendThreshold"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendThreshold"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="smartSendThreshold"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="smartSendThreshold"]</property>
				<property>$intake_node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="smartSendThreshold"]</property>
				<value>(int)(cold_delta)</value>
			</varbinding>
			<varbinding vars="hot_delta">
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="smartSendThreshold"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="smartSendThreshold"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="smartSendThreshold"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="smartSendThreshold"]</property>
				<property>$exhaust_node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="smartSendThreshold"]</property>
				<value>(int)(hot_delta)</value>
			</varbinding>


		</component>
	</xsl:template>


	<!-- C18)  missing varbindings for extra thermistors on TN2 racks -->
	<xsl:template match='component[@type="rack-control-toprearexhaust-sf-thermanode2" ]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select='property'/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding[not(@vars="hot_delta") ]' />

			<varbinding vars="hot_delta">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="smartSendThreshold"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="smartSendThreshold"]</property>
				<value>(double)(hot_delta)</value>
			</varbinding>

		</component>
	</xsl:template>


	<xsl:template match='component[@type="rack-control-toprearexhaust-nsf-nrt-thermanode2" ]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select='property'/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding[not(@vars="hot_delta") ]' />

			<varbinding vars="hot_delta">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="smartSendThreshold"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="smartSendThreshold"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="9"]]/properties[name="smartSendThreshold"]</property>
				<value>(double)(hot_delta)</value>
			</varbinding>

		</component>
	</xsl:template>


	<xsl:template match='component[@type="rack-control-toprearexhaust-nsf-nrt-6t" ]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select='property'/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding[not(@vars="hot_delta") ]' />

			<varbinding vars="hot_delta">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="smartSendThreshold"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="smartSendThreshold"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="smartSendThreshold"]</property>
				<value>(double)(hot_delta)</value>
			</varbinding>

		</component>
	</xsl:template>


	<xsl:template match='component[@type="generic-constellation2" ]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:attribute name="grouppath">
				<xsl:value-of select="@grouppath" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select='property[not(@name="c3_adc") and not(@name="c4_adc") and not(@name="c5_adc") and not(@name="c7_adc") ]'/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<!-- xsl:apply-templates select='varbinding' /-->
			<!-- whack all the varbindings and replace -->

			<varbinding vars="name">
				<property>$node/properties[name="name"]</property>
				<value>'Node ' + name</value>
			</varbinding>
			<varbinding vars="x">
				<property>$node/properties[name="x"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="x"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="x"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="x"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="x"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="x"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="x"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="x"]</property>
				<value>x</value>
			</varbinding>
			<varbinding vars="y">
				<property>$node/properties[name="y"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="2"]]/properties[name="y"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="y"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="y"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="y"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="y"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="y"]</property>
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="y"]</property>
				<value>y</value>
			</varbinding>
			<varbinding vars="mac_id">
				<property>$node/properties[name="mac"]</property>
				<value>Long.parseLong(mac_id,16)</value>
			</varbinding>
			<varbinding vars="mac_id">
				<property>$node/properties[name="id"]</property>
				<value>lid.generateLogicalId(mac_id , self)</value>
			</varbinding>
			<varbinding vars="sample_interval">
				<property>$node/properties[name="period"]</property>
				<value>sample_interval + ' min'</value>
			</varbinding>
			<varbinding vars="location">
				<property>$node/properties[name="location"]</property>
				<value>location</value>
			</varbinding>

			<varbinding vars="c3_enable">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="enabled"]</property>
				<value>if(c3_enable) return 1; else return 0;</value>
			</varbinding>

			<varbinding vars="c3_type">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="type"]</property>
				<value>c3_type</value>
			</varbinding>
			<varbinding vars="c3_4ma">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="scaleMin"]</property>
				<value>c3_4ma</value>
			</varbinding>
			<varbinding vars="c3_20ma">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]/properties[name="scaleMax"]</property>
				<value>c3_20ma</value>
			</varbinding>


			<varbinding vars="c4_enable">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="enabled"]</property>
				<value>if(c4_enable) return 1; else return 0;</value>
			</varbinding>

			<varbinding vars="c4_type">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="type"]</property>
				<value>c4_type</value>
			</varbinding>
			<varbinding vars="c4_4ma">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="scaleMin"]</property>
				<value>c4_4ma</value>
			</varbinding>
			<varbinding vars="c4_20ma">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]/properties[name="scaleMax"]</property>
				<value>c4_20ma</value>
			</varbinding>


			<varbinding vars="c5_enable">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="enabled"]</property>
				<value>if(c5_enable) return 1; else return 0;</value>
			</varbinding>

			<varbinding vars="c5_type">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="type"]</property>
				<value>c5_type</value>
			</varbinding>
			<varbinding vars="c5_4ma">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="scaleMin"]</property>
				<value>c5_4ma</value>
			</varbinding>
			<varbinding vars="c5_20ma">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]/properties[name="scaleMax"]</property>
				<value>c5_20ma</value>
			</varbinding>


			<varbinding vars="c7_enable">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="enabled"]</property>
				<value>if(c7_enable) return 1; else return 0;</value>
			</varbinding>
			<varbinding vars="c7_type">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="type"]</property>
				<value>c7_type</value>
			</varbinding>
			<varbinding vars="c7_4ma">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="scaleMin"]</property>
				<value>c7_4ma</value>
			</varbinding>
			<varbinding vars="c7_20ma">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]/properties[name="scaleMax"]</property>
				<value>c7_20ma</value>
			</varbinding>


			<varbinding vars="c6_type">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="type"]</property>
				<value>c6_type</value>
			</varbinding>

			<varbinding vars="c6_enable">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="enabled"]</property>
				<value>if(c6_enable) return 1; else return 0;</value>
			</varbinding>
			<varbinding vars="c6_pulseCount">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="mode"]</property>
				<value>if (c6_pulseCount == 1) return "state_change_count"; else return "edge_detect";</value>
			</varbinding>
			<varbinding vars="c6_pulseSendTime">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="interval"]</property>
				<value>c6_pulseSendTime</value>
			</varbinding>

			<varbinding vars="c6_scale">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]/properties[name="scaleMax"]</property>
				<value>c6_scale</value>
			</varbinding>


			<varbinding vars="c8_type">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="type"]</property>
				<value>c8_type</value>
			</varbinding>

			<varbinding vars="c8_enable">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="enabled"]</property>
				<value>if(c8_enable) return 1; else return 0;</value>
			</varbinding>
			<varbinding vars="c8_pulseCount">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="mode"]</property>
				<value>if (c8_pulseCount == 1) return "state_change_count"; else return "edge_detect";</value>
			</varbinding>
			<varbinding vars="c8_pulseSendTime">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="interval"]</property>
				<value>c8_pulseSendTime</value>
			</varbinding>

			<varbinding vars="c8_scale">
				<property>$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]/properties[name="scaleMax"]</property>
				<value>c8_scale</value>
			</varbinding>


		</component>
	</xsl:template>

<!-- D) decommission EOL components -->

	<xsl:template name="deprecation">
		<xsl:param name="cfg" />
		<xsl:choose>
			<xsl:when test='$cfg= "modbus-rtu-network"'> <deprecated>true</deprecated> </xsl:when>
			<!-- fusion nodes -->
			<xsl:when test='$cfg= "rack-interior-fusion"'> <deprecated>true</deprecated> </xsl:when>
			<xsl:when test='$cfg= "crah-dual-fusion"'> <deprecated>true</deprecated> </xsl:when>
			<!-- constellation 1 -->
			<xsl:when test='$cfg= "leakdetector-constellation"'> <deprecated>true</deprecated> </xsl:when>
			<xsl:when test='$cfg= "door-constellation"'> <deprecated>true</deprecated> </xsl:when>
			<xsl:when test='$cfg= "flow-constellation"'> <deprecated>true</deprecated> </xsl:when>
			<xsl:when test='$cfg= "ct-constellation"'> <deprecated>true</deprecated> </xsl:when>
			<xsl:when test='$cfg= "dualflow-btu-constellation"'> <deprecated>true</deprecated> </xsl:when>
			<xsl:when test='$cfg= "singleflow-btu-constellation"'> <deprecated>true</deprecated> </xsl:when>
			<xsl:when test='$cfg= "energy-constellation"'> <deprecated>true</deprecated> </xsl:when>


		</xsl:choose>
	</xsl:template>


    <!-- user defined functions for migration -->
    <xsl:template name="addressoutput-tokens">
	    <xsl:param name="list" />
	    <xsl:param name="delimiter" />
	    <xsl:param name="attr_name" />
	    
	    <xsl:variable name="newlist">
			<xsl:choose>
				<xsl:when test="contains($list, $delimiter)"><xsl:value-of select="normalize-space($list)" /></xsl:when>
				
				<xsl:otherwise><xsl:value-of select="concat(normalize-space($list), $delimiter)"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
	    <xsl:variable name="first" select="substring-before($newlist, $delimiter)" />
	    <xsl:variable name="remaining" select="substring-after($newlist, $delimiter)" />
	    <xsl:element name="child">
			<xsl:call-template name="getGatewayIdByAddress">
				<xsl:with-param name="address_value" select="$first"/>
			</xsl:call-template>
			<!-- xsl:value-of select="$first"/-->
		</xsl:element>
	    <xsl:if test="$remaining">
	        <xsl:call-template name="addressoutput-tokens">
	            <xsl:with-param name="list" select="$remaining" />
				<xsl:with-param name="delimiter"><xsl:value-of select="$delimiter"/></xsl:with-param>
	        </xsl:call-template>
	    </xsl:if>
	</xsl:template>
	
    <xsl:template name="getGatewayIdByAddress">
    	<xsl:param name="address_value"/>
  		<xsl:value-of select='//object[@type="gateway"]/property[@name="address"][value=$address_value]/../dlid/text()'/>
    </xsl:template>

    <xsl:template name="getNodePlatformNameById">
    	<xsl:param name="id"/>
    	
    	<xsl:choose>
    		<xsl:when test="$id=1">
    			<xsl:text>Outdoor Node</xsl:text>
    		</xsl:when>
    		<xsl:when test="$id=10">
    			<xsl:text>Fusion</xsl:text>
    		</xsl:when>
    		<xsl:when test="$id=11">
    			<xsl:text>ThermaNode</xsl:text>
    		</xsl:when>
    		<xsl:when test="$id=12">
    			<xsl:text>Pressure Node</xsl:text>
    		</xsl:when>
    		<xsl:when test="$id=13">
    			<xsl:text>Pressure Node 2</xsl:text>
    		</xsl:when>
    		<xsl:when test="$id=14">
    			<xsl:text>Constellation</xsl:text>
    		</xsl:when>
    		<xsl:when test="$id=15">
    			<xsl:text>Constellation 2</xsl:text>
    		</xsl:when>
    		<xsl:when test="$id=16">
    			<xsl:text>Enthalpy Node</xsl:text>
    		</xsl:when>
			<!--
    		<xsl:when test="$id=17">
    			<xsl:text>Energy Meter Node</xsl:text>
    		</xsl:when>
    		<xsl:when test="$id=18">
    			<xsl:text>ThermaNode 2</xsl:text>
    		</xsl:when>
    		-->
			<xsl:when test="$id=17">
				<xsl:text>ThermaNode 2</xsl:text>
			</xsl:when>

			<xsl:when test="$id=18">
    			<xsl:text>ThermaNode 2</xsl:text>
    		</xsl:when>

    		<xsl:otherwise>
    			<xsl:text></xsl:text>
    		</xsl:otherwise>
    	</xsl:choose>
    </xsl:template>
    
    <xsl:template name="getNodeBatteryOperated">
    	<xsl:param name="node_dlid"/>
    	<xsl:param name="node_platform_id"/>
    	<xsl:choose>
    		<xsl:when test="$node_platform_id=16">
    			<xsl:text>0</xsl:text>
    		</xsl:when>
    		<xsl:when test="$node_platform_id=14 or $node_platform_id=15">
    			<xsl:variable name="component_type" select='//component[object/@type="node" and object/@dlid=$node_dlid]/@type'/>
    			<xsl:choose>
    				<xsl:when test='$component_type="ct-constellation" or $component_type="dualflow-btu-constellation" or $component_type="flow-constellation" or $component_type="leakdetector-constellation" or $component_type="singleflow-btu-constellation" or $component_type="ct-constellation2" or $component_type="leakdetector-constellation2" or $component_type="outdoor_temp_humidity" '>
    					<xsl:text>0</xsl:text>
    				</xsl:when>
    				<xsl:otherwise>
    					<xsl:text>1</xsl:text>
    				</xsl:otherwise>
    			</xsl:choose>
    		</xsl:when>
    		<xsl:otherwise>
    			<xsl:text>1</xsl:text>	
    		</xsl:otherwise>
    	</xsl:choose>
    </xsl:template>
    
    <xsl:template name="getSensorDataClassByTypeId">
    	<xsl:param name="typeId"/>
    	
    	<xsl:choose>
	    	<xsl:when test="$typeId=1 or $typeId=7 or $typeId=13 or $typeId=21 or $typeId=22 or $typeId=25 or $typeId=27 or $typeId=33 or $typeId=36">
	    		<xsl:text>200</xsl:text>
	    	</xsl:when>
	    	<xsl:when test="$typeId=2">
	    		<xsl:text>201</xsl:text>
	    	</xsl:when>
	    	<xsl:when test="$typeId=24 or $typeId=29"  >
	    		<xsl:text>202</xsl:text>
	    	</xsl:when>
	    	<xsl:when test="$typeId=6">
	    		<xsl:text>204</xsl:text>
	    	</xsl:when>
	    	<xsl:when test="$typeId=4">
	    		<xsl:text>205</xsl:text>
	    	</xsl:when>
	    	<xsl:when test="$typeId=8 or $typeId=14 or $typeId=15 or $typeId=16 or $typeId=17 or $typeId=18 or $typeId=19 or $typeId=23 or $typeId=26 or $typeId=30">
	    		<xsl:text>206</xsl:text>
	    	</xsl:when>
	    	<xsl:when test="$typeId=20  or $typeId=32">
	    		<xsl:text>207</xsl:text>
	    	</xsl:when>
	    	<xsl:when test="$typeId=28  or $typeId=31">
	    		<xsl:text>209</xsl:text>
	    	</xsl:when>
	    	<xsl:when test="$typeId=3  or $typeId=5">
	    		<xsl:text>210</xsl:text>
	    	</xsl:when>
	    	<xsl:when test="$typeId=9">
	    		<xsl:text>211</xsl:text>
	    	</xsl:when>
	    	<xsl:when test="$typeId=10">
	    		<xsl:text>212</xsl:text>
	    	</xsl:when>
	    	<xsl:when test="$typeId=12">
	    		<xsl:text>213</xsl:text>
	    	</xsl:when>
	    	<xsl:when test="$typeId=100000">
	    		<xsl:text>214</xsl:text>
	    	</xsl:when>
	    	<xsl:when test="$typeId=34  or $typeId=35">
	    		<xsl:text>999</xsl:text>
	    	</xsl:when>
	    	<xsl:otherwise>
	    		<xsl:text>0</xsl:text>
	    	</xsl:otherwise>
    	</xsl:choose>
    </xsl:template>
    
    <xsl:template name="getSensorEnabled">
    	<xsl:param name="node_dlid"/>
		<xsl:param name="channel"/>
		<xsl:param name="delta"/>
		
		<xsl:variable name="platform" select='//object[dlid/text()=$node_dlid]/property[@name="platform"]/value/text()'/>
		
		<xsl:variable name="deltaComparisonValue">
			<xsl:call-template name="operate_bitwise_and">
				<xsl:with-param name="comparator_base10" select="$delta div 100"/>
				<xsl:with-param name="basis_base2" select="'1000000000000000'"/>
			</xsl:call-template>
		</xsl:variable>
				
		<xsl:choose>
			<xsl:when test="$platform = 14 and $channel&gt;3 and $deltaComparisonValue = 0" >
				<xsl:value-of select="0"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="1"/>
			</xsl:otherwise>
		</xsl:choose>
    </xsl:template>
    
    <xsl:template name="getSensorSmartSendThreshold">
    	<xsl:param name="node_dlid"/>
		<xsl:param name="delta"/>
		<xsl:variable name="platform" select='//object[dlid/text()=$node_dlid]/property[@name="platform"]/value/text()'/>
		
		<xsl:choose>
			<xsl:when test="$delta">
				<xsl:choose>
					<xsl:when test="$platform = 11 or $platform = 18">
						<xsl:value-of select="$delta div 100.0"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="0"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text></xsl:text>
			</xsl:otherwise>
		</xsl:choose>
    </xsl:template>
    
    <xsl:template name="getSensorMode">
    <xsl:param name="node_dlid"/>
		<xsl:param name="channel"/>
		<xsl:param name="delta"/>
		
		<xsl:variable name="platform" select='//object[dlid/text()=$node_dlid]/property[@name="platform"]/value/text()'/>
		
		<xsl:choose>
			<xsl:when test="$platform = 15"> <!-- check if it is constellation2 or not -->
				<xsl:choose>
					<xsl:when test="$channel = 6 or $channel = 8">
						
						<xsl:variable name="deltaComparisonValue">
							<xsl:call-template name="operate_bitwise_and">
								<xsl:with-param name="comparator_base10" select="$delta div 100"/>
								<xsl:with-param name="basis_base2" select="'0111111111111111'"/>
							</xsl:call-template>
						</xsl:variable>
		
						<xsl:choose>
							<xsl:when test="$deltaComparisonValue = 0">
								<xsl:text>edge_detect</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>state_change_count</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>averaging</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>instant</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
    </xsl:template>
    
    <xsl:template name="getSensorInterval">
		<xsl:param name="node_dlid"/>
		<xsl:param name="channel"/>
		<xsl:param name="delta"/>
	
		<xsl:variable name="platform" select='//object[dlid/text()=$node_dlid]/property[@name="platform"]/value/text()'/>
		
		<xsl:choose>
			<xsl:when test="$platform = 15 and ($channel = 6 or $channel = 8)">
				<xsl:variable name="interval">
					<xsl:call-template name="operate_bitwise_and">
						<xsl:with-param name="comparator_base10" select="$delta div 100"/>
						<xsl:with-param name="basis_base2" select="'0111111111111111'"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="$interval"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>0</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

    <xsl:template name="getSensorSubSamples">
    	<xsl:param name="typeId"/>
    	<xsl:choose>
	    	<xsl:when test="$typeId=27 or $typeId=36">
				<property name="subSamples">
					<value>30</value>
					<oldvalue></oldvalue>
				</property>
	    	</xsl:when>
    	</xsl:choose>
    </xsl:template>

	
	<xsl:template name="getGatewayParent">
		<xsl:param name="address"/>
		<deleted><xsl:value-of select="deleted/text()"/></deleted>

		<xsl:variable name="numberOfNetworks" select="count( //object[@type='network'] ) div 2"/>
		<xsl:variable name="firstNetworkId" select="//object[@type='network']/dlid/text()"/>

		<xsl:choose>
			<xsl:when test="$numberOfNetworks=1">
				<parentid><xsl:value-of select="$firstNetworkId"/></parentid>
			</xsl:when>

			<xsl:when test="$address=''">
				<parentid></parentid>
			</xsl:when>

			<xsl:otherwise>
				<xsl:variable name="parentId" select='//object[@type="network"]/property[@name="comport"]/value[contains(text(),$address)]/../../dlid/text()'/>
				<parentid><xsl:value-of select="$parentId"/></parentid>
			</xsl:otherwise>
		</xsl:choose>		
	</xsl:template>
	    
    <!-- user defined math functions -->
    <xsl:template name="operate_bitwise_and">
		<xsl:param name="comparator_base10" select="''"/>
		<xsl:param name="basis_base2" select="''"/>
		
		<xsl:variable name="comparator_base2">
			<xsl:call-template name="convert_base_10_to_2">
				<xsl:with-param name="input" select="$comparator_base10"/>
			</xsl:call-template>
		</xsl:variable>
						
		<xsl:variable name="bitwiseResult">
			<xsl:choose>
				<xsl:when test="string-length(string($basis_base2))>string-length($comparator_base2)">
					<xsl:call-template name="bitwise_and_implementation">
						<xsl:with-param name="maxvalue" select="string-length(string($basis_base2))"/>
						<xsl:with-param name="input1" select="$basis_base2"/>
						<xsl:with-param name="input2" select="$comparator_base2"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="bitwise_and_implementation">
						<xsl:with-param name="maxvalue" select="string-length($comparator_base2)"/>
						<xsl:with-param name="input1" select="$comparator_base2"/>
						<xsl:with-param name="input2" select="$basis_base2"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
						
		<xsl:variable name="result">
			<xsl:call-template name="convert_base_2_to_10">
				<xsl:with-param name="input" select="translate(string($bitwiseResult),' ','')"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:value-of select="$result"/>
	</xsl:template>
	
	<xsl:template name="bitwise_and_implementation">
		<xsl:param name="maxvalue"/>
		<xsl:param name="input1"/>
		<xsl:param name="input2"/>
		<xsl:param name="i" select="1"/>
		<xsl:param name="result" select="''"/>
		<!-- Assume input1 is larger digit number -->
		<xsl:variable name="input2_length" select="string-length(string($input2))"/>
		<xsl:variable name="input2_comparator" select="$maxvalue - $input2_length"/>
		
		<xsl:variable name="andResult">
				<xsl:choose>
					<xsl:when test="$input2_comparator&gt;$i">
						<xsl:call-template name="operateAnd">
							<xsl:with-param name="compare1" select="substring(string($input1),$i,1)"/>
							<xsl:with-param name="compare2" select="0"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="operateAnd">
							<xsl:with-param name="compare1" select="substring(string($input1),$i,1)"/>
						<xsl:with-param name="compare2" select="substring(string($input2),$i - $input2_comparator,1)"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="$i&lt;$maxvalue">
				
				<xsl:call-template name="bitwise_and_implementation">
					<xsl:with-param name="maxvalue" select="$maxvalue"/>
					<xsl:with-param name="input1" select="$input1"/>
					<xsl:with-param name="input2" select="$input2"/>
					<xsl:with-param name="i" select="$i+1"/>
					<xsl:with-param name="result" select="concat($result, $andResult)"/>
					
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($result,$andResult)"/>
			</xsl:otherwise>
		</xsl:choose>	
	</xsl:template>
	
	<xsl:template name="operateAnd">
		<xsl:param name="compare1"/>
		<xsl:param name="compare2"/>
		
		<xsl:choose>
			<xsl:when test="string($compare1)='1' and string($compare2)='1'">
				<xsl:text>1</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>0</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="power">
		<xsl:param name="base" select="0"/>
		<xsl:param name="power" select="1"/>
		<xsl:param name="result" select="1"/>
		
		
		<xsl:choose>
			<xsl:when test="$power = 0">
				<xsl:value-of select="$result"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="power">
					<xsl:with-param name="base" select="$base"/>
					<xsl:with-param name="power" select="$power - 1"/>
					<xsl:with-param name="result" select="$result * $base"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
	<xsl:template name="convert_base_2_to_10">
		<xsl:param name="input"/>
		<xsl:param name="i" select="1"/>
		<xsl:param name="sum" select="0"/>
		
		<xsl:variable name="value_of_this_digit">
			<xsl:call-template name="power">
				<xsl:with-param name="base" select="2"/>
				<xsl:with-param name="power" select="$i -1"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="result" select="$sum + number($value_of_this_digit) * number(substring($input, (-1)*$i + string-length($input) + 1, 1))" />
		<xsl:choose>
			<xsl:when test="$i = string-length(string($input))">
				<xsl:value-of select="$result"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="convert_base_2_to_10">
					<xsl:with-param name="input" select="$input"/>
					<xsl:with-param name="i" select="$i + 1"/>
					<xsl:with-param name="sum" select="$result"/>
				</xsl:call-template>			
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
	<xsl:template name="convert_base_10_to_2">
		<xsl:param name="input"/>
		<xsl:param name="result" select="''"/>
		<xsl:variable name="reminder" select="$input mod 2" />
		<xsl:variable name="quotient" select="floor($input div 2)"/>
		<xsl:variable name="newinput" select="concat($result,$reminder)"/>
		<xsl:choose>
			<xsl:when test="$quotient>=2">
				<xsl:call-template name="convert_base_10_to_2">
					<xsl:with-param name="input" select="$quotient"/>
					<xsl:with-param name="result" select="$newinput"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($quotient,$reminder, $result)"/>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
</xsl:stylesheet>