<?xml version="1.0" encoding="UTF-8"?>

<!--
see wiki page for spec: http://wiki.synapsense.int/moinmoin/DeploymentLab/UpgradeProcess
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/02/xpath-functions"
	xmlns:dl="http://www.synapsense.com"
	exclude-result-prefixes="xsl xs fn dl"
	>
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	
	<xsl:template match="model">
		<model version="1.3">
			<xsl:copy-of select="*[not(self::objects) and not(self::components)]" />
			<xsl:apply-templates select="objects" />
			<xsl:apply-templates select="components" />
		</model>
	</xsl:template>

	<xsl:template match="objects">
		<objects>
			<xsl:attribute name="version">1.4</xsl:attribute>
			<xsl:attribute name="lastid">
				<xsl:value-of select="@lastid" />
			</xsl:attribute>
			<xsl:apply-templates select="*" />
		</objects>
	</xsl:template>
	
	<xsl:template match='object'>
		<object>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:copy-of select="*" />
		</object>
	</xsl:template>
	
	<xsl:template match="components">
		<components>
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:apply-templates select="*" />
		</components>
	</xsl:template>
	
	<xsl:template match='component'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
		</component>
	</xsl:template>
	
	<xsl:template match="varbinding">
		<varbinding>
			<xsl:attribute name="vars"><xsl:value-of select="@vars"/></xsl:attribute>
			<xsl:copy-of select="*"/>
		</varbinding>
	</xsl:template>	
	
	<xsl:template match='property'>
		<property>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="display">
				<xsl:choose>
					<xsl:when test='@name="cold_delta"'>
						<xsl:text>Intake Delta Send Threshold</xsl:text>						
					</xsl:when>
					<xsl:when test='@name="hot_delta"'>
						<xsl:text>Exhaust Delta Send Threshold</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@display" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="type"><xsl:value-of select="@type" /></xsl:attribute>
			<xsl:attribute name="editable"><xsl:value-of select="@editable" /></xsl:attribute>
			<xsl:attribute name="displayed"><xsl:value-of select="@displayed" /></xsl:attribute>
			<xsl:if test="@dimension">
				<xsl:attribute name="dimension"><xsl:value-of select="@dimension" /></xsl:attribute>
			</xsl:if>
			<xsl:if test="@valueChoices">
				<xsl:attribute name="valueChoices"><xsl:value-of select="@valueChoices" /></xsl:attribute>
			</xsl:if>
			<xsl:value-of select="." />
		</property>
	</xsl:template>
	
	<xsl:template match='consumer'>
		<consumer>
			<xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
			<xsl:attribute name="datatype"><xsl:value-of select="@datatype" /></xsl:attribute>
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:attribute name="property"><xsl:value-of select="@property" /></xsl:attribute>
			<xsl:attribute name="required"><xsl:value-of select="@required" /></xsl:attribute>
			<xsl:copy-of select="*" />
		</consumer>
	</xsl:template>

	<xsl:template match='producer'>
		<producer>
			<xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
			<xsl:attribute name="datatype"><xsl:value-of select="@datatype" /></xsl:attribute>
			<xsl:attribute name="name">
				<xsl:choose>
					<xsl:when test='@id="topinlet"'>
						<xsl:text>Top Intake Temp</xsl:text> 
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@name" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="object"><xsl:value-of select="@object" /></xsl:attribute>
			<xsl:copy-of select="*" />
		</producer>
	</xsl:template>

	<xsl:template match='display'>
		<display>
			<xsl:copy-of select="*[not(self::description)]" />
			<description><xsl:call-template name="new_description"><xsl:with-param name="cfg" select="../@type"/></xsl:call-template></description>
		</display>
	</xsl:template>
	
	<xsl:template name="new_description">
		<xsl:param name="cfg"/>
		<xsl:choose>
			<xsl:when test='$cfg = "rack-control-rearexhaust-sf-thermanode2"'>
				<xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors. Designed for front inlet, rear exhaust configurations. Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle, exhaust bottom and subfloor. This configuration is qualified for Adaptive Control.</xsl:text>
			</xsl:when>
			<xsl:when test='$cfg = "rack-control-rearexhaust-nsf-rt-thermanode2"'>
				<xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 6 external sensors. Designed for front inlet, rear exhaust configurations. Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle and exhaust bottom.  This configuration is qualified for Adaptive Control.</xsl:text>
			</xsl:when>
			<xsl:when test='$cfg = "rack-lhs-endcap-rt-thermanode2"'>
				<xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors located on a left end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom, side top and side middle/bottom. This configuration is qualified for Adaptive Control.</xsl:text>
			</xsl:when>
			<xsl:when test='$cfg = "rack-rhs-endcap-rt-thermanode2"'>
				<xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors located on a right end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom, side top and side middle/bottom. This configuration is qualified for Adaptive Control.</xsl:text>
			</xsl:when>
			<xsl:when test='$cfg = "rack-control-rearexhaust-nsf-nrt-thermanode2"'>
				<xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 6 external sensors.  Designed for front inlet, rear exhaust configurations.  Node is installed at rack top with sense points are located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle and hot bottom.  This configuration is qualified for Adaptive Control.</xsl:text>
			</xsl:when>
			<xsl:when test='$cfg = "rack-lhs-endcap-nrt-thermanode2"'>
				<xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors located on a left end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom, side top and side middle/bottom. This configuration is qualified for Adaptive Control.</xsl:text>
			</xsl:when>
			<xsl:when test='$cfg = "rack-rhs-endcap-nrt-thermanode2"'>
				<xsl:text>Rack monitoring assembly consisting of a ThermaNode II equipped with 7 external sensors located on a right end of an aisle (facing the intake aisle). Node is installed at rack top with sense points located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom, side top and side middle/bottom. This configuration is qualified for Adaptive Control.</xsl:text>
			</xsl:when>
			<xsl:when test='$cfg = "rack-control-rearexhaust-sf-thermanode"'>
				<xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors.  Designed for front inlet, rear exhaust configurations.  Node is installed at rack top, sense points are located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom, subfloor.  This configuration is qualified for Wireless Control.</xsl:text>
			</xsl:when>
			<xsl:when test='$cfg = "rack-control-toprearexhaust-sf-thermanode"'>
				<xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 6 external sensors.  Designed for front inlet, top/rear exhaust configurations.  Node is installed at rack top, sense points are located at cabinet intake top, intake middle, intake bottom, cabinet top, exhaust top, subfloor.  This configuration is qualified for Wireless Control.</xsl:text>
			</xsl:when>
			<xsl:when test='$cfg = "rack-control-rearexhaust-nsf-rt-thermanode"'>
				<xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 5 external sensors.  Designed for front inlet, rear exhaust configurations.  Node is installed at rack top, sense points are located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom.  This configuration is qualified for Wireless Control.</xsl:text>
			</xsl:when>
			<xsl:when test='$cfg = "rack-control-toprearexhaust-nsf-rt-thermanode"'>
				<xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 5 external sensors.  Designed for front inlet, top/rear exhaust configurations.  Node is installed at rack top, sense points are located at cabinet intake top, intake middle, intake bottom, cabinet top, exhaust top.  This configuration is qualified for Wireless Control.</xsl:text>
			</xsl:when>
			<xsl:when test='$cfg = "rack-control-rearexhaust-nsf-nrt-thermanode"'>
				<xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 5 external sensors.  Designed for front inlet, rear exhaust configurations.  Node is installed at rack top, sense points are located at cabinet intake top, intake middle, intake bottom, exhaust top, exhaust middle/bottom.  This configuration is qualified for Wireless Control.</xsl:text>
			</xsl:when>
			<xsl:when test='$cfg = "rack-control-toprearexhaust-nsf-nrt-thermanode"'>
				<xsl:text>Rack monitoring assembly consisting of a ThermaNode equipped with 5 external sensors.  Designed for front inlet, top/rear exhaust configurations.  Node is installed at rack top, sense points are located at cabinet intake top, intake middle, intake bottom, cabinet top, exhaust top.  This configuration is qualified for Wireless Control.</xsl:text>
			</xsl:when>
			<xsl:otherwise><xsl:value-of select="description/text()" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- upgraded components -->
	<!-- 1. adjust association for control inputs -->
	<!-- 2. Rename cold, hot to Intake, Exhaust for Rack Components-->
	<!-- horizontal-row-thermanode : add producers (channel 3 ~8 Temperatures -->
	<xsl:template match='component[@type="horizontal-row-thermanode"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			
			<producer id="temp3" datatype="temperature" name="Channel 3 Temperature" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]' />
			<producer id="temp4" datatype="temperature" name="Channel 4 Temperature" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]' />
			<producer id="temp5" datatype="temperature" name="Channel 5 Temperature" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="5"]]' />
			<producer id="temp6" datatype="temperature" name="Channel 6 Temperature" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="6"]]' />
			<producer id="temp7" datatype="temperature" name="Channel 7 Temperature" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="7"]]' />
			<producer id="temp8" datatype="temperature" name="Channel 8 Temperature" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="8"]]' />
			
		</component>
	</xsl:template>

	<!-- crah-single-plenumrated-thermanode : add producers (supply and return temperature) -->
	<xsl:template match='component[@type="crah-single-plenumrated-thermanode"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select='property[not(@name="depth") and not(@name="width")]'/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			<xsl:apply-templates select='varbinding[not(@vars="depth") and not(@vars="width")]'/>
			
			<producer id="supplytemp" datatype="temperature" name="Supply Temperature" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="4"]]' />
			<producer id="returntemp" datatype="temperature" name="Return Temperature" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]' />
	
		</component>
	</xsl:template>
	
	<!-- crah-dual-thermanode : add producers (supply and return temperature) -->
	<xsl:template match='component[@type="crah-dual-thermanode"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			
			<producer id="supplytemp" datatype="temperature" name="Supply Temperature" object='$sNode/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]' />
			<producer id="returntemp" datatype="temperature" name="Return Temperature" object='$rNode/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]' />

		</component>
	</xsl:template>
	
	<!-- crah-dual-probe-thermanode : add producers (supply and return temperature) -->
	<xsl:template match='component[@type="crah-dual-probe-thermanode"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<display>
				<description><xsl:value-of select="display/description/text()"/></description>
				<name><xsl:value-of select="display/name/text()"/></name>
				<shape>dual_crac_temp</shape>
			</display>
		
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			
			<producer id="supplytemp" datatype="temperature" name="Supply Temperature" object='$sNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]' />
			<producer id="returntemp" datatype="temperature" name="Return Temperature" object='$rNode/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]' />

		</component>
	</xsl:template>
	
	<!-- reftemp-thermanode  : add producers (Node Reference Temperature ) -->
	<xsl:template match='component[@type="reftemp-thermanode"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			
			<producer id="temp" datatype="temperature" name="Node Reference Temperature" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]' />

		</component>
	</xsl:template>
	
	<!-- standalone-thermanode   : add producers (Node Temperature ) -->
	<xsl:template match='component[@type="standalone-thermanode"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select="producer"/>
			
			<producer id="temp" datatype="temperature" name="Node Temp" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="0"]]' />

		</component>
	</xsl:template>
	
	<!-- any control rack components : remove producers (Middle inlet and bottom inlet Temperature ) -->
	<xsl:template match='component[starts-with(@type, "rack-control-")]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			
			<xsl:if test='not(producer[@id="topinlet"])'>
				<producer id="topinlet" datatype="temperature" name="Top Inlet Temp" object='$rack/properties[name="cTop"]/referrent' />
			</xsl:if>
			<xsl:apply-templates select='producer[not(@id="midinlet") and not(@id="botinlet")]' />
			
		</component>
	</xsl:template>
	
	<!-- control-verticalstring-li-thermanode : remove producers (Middle inlet and bottom inlet Temperature ) -->
	<xsl:template match='component[@type="control-verticalstring-li-thermanode"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:apply-templates select='producer[not(@id="mid") and not(@id="bot")]' />
		</component>
	</xsl:template>
	
	
	
	<!-- 3. change gateway unique id to Mac ID  -->
	
	<xsl:template match='component[@type="remote-gateway"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display) and not(self::macids) and not(self::varbinding)]" />
			<xsl:apply-templates select="display" />
			
			<macids value="mac_id" />
			
			<xsl:apply-templates select='property[not(@name="uid")]'/>
			<property name="mac_id" display="MAC ID" type="java.lang.String" editable="true" displayed="true"><xsl:value-of select='property[@name="uid"]/text()' /></property>
		
			<xsl:apply-templates select='varbinding[not(@vars="uid")]' />
			<varbinding vars="mac_id">
				<property>$gateway/properties[name="uniqueId"]</property>
				<value>Long.parseLong(mac_id,16)</value>
			</varbinding>
			
		</component>
	</xsl:template>
	
	<!--  Add missing producer for pressure2 -->
	<xsl:template match='component[@type="pressure2"]'>
		<component>
			<xsl:attribute name="type">
				<xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="classes">
				<xsl:value-of select="@classes" />
			</xsl:attribute>
			<xsl:copy-of select="*[not(self::consumer) and not(self::producer) and not(self::property) and not(self::display)]" />
			<xsl:apply-templates select="display" />
			<xsl:apply-templates select="property"/>
			<xsl:apply-templates select="consumer"/>
			<xsl:choose>
				<xsl:when test='producer'>
					<xsl:apply-templates select="producer"/>
				</xsl:when>
				<xsl:otherwise>
					<producer id="pressure" datatype="pressure" name="Pressure" object='$node/properties[name="sensepoints"]/children[properties[name="channel"][value="3"]]' />
				</xsl:otherwise>
			</xsl:choose>
		</component>
	</xsl:template>
	
</xsl:stylesheet>