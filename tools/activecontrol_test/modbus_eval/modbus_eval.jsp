<%-- Set the content type --%>
<%@ page contentType="text/xml" %>




<%-- Emit some XML --%>
<!--?xml version="1.0" encoding="UTF-8"?-->
<reply>

<%
//	<result>ok</result>
if( request.getParameter("expr").toString().contains("write_register32") ){
	out.println("<result>ABORT</result>");
	out.println("<message>Unknown function 'write_register32' called with arguments [{string,1,\"192.168.201.150\"}, \n" +
	            "                                                                    {integer,1,20800}, \n" +
	            "                                                                    {integer,1,1},\n" +
	            "                                                                    {integer,1,85}]</message>");
} else if( request.getParameter("expr").toString().contains("read_int32") ){
	out.println("<result>ABORT</result>");
	out.println("<message>Unknown function 'read_int32' called with arguments [{string,1,\"192.168.201.150\"}, \n" +
	            "                                                              {integer,1,20800}, \n" +
	            "                                                              {integer,1,1},\n" +
	            "                                                              {integer,1,85}]</message>");
} else if( request.getParameter("expr").toString().contains("40") ){
	out.println("<result>ok</result>");
	out.println("<message>Expression Result: \"42\"</message>");
} else {
	out.println("<result>false</result>");
	out.println("<message>Expression Result: \"42\"</message>");
}
%>

</reply>

