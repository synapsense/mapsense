package DeploymentLab

import groovy.xml.MarkupBuilder

import org.apache.batik.transcoder.TranscoderInput
import org.apache.batik.transcoder.TranscoderOutput
import org.apache.batik.transcoder.image.PNGTranscoder

import DeploymentLab.Model.PropertyConverter
import org.apache.commons.io.FileUtils
import groovy.io.FileType
import org.apache.commons.io.FilenameUtils
import groovyjarjarcommonscli.Options
import groovyjarjarcommonscli.CommandLineParser
import groovyjarjarcommonscli.PosixParser
import groovyjarjarcommonscli.CommandLine
import groovyjarjarcommonscli.HelpFormatter

/**
 * Generates the Component Encyclopdia LaTeX from the list of components.
 * This is based directly on the files, not the component model so that we could make this a standalone.
 *
 *
 * Stuff we care about:
 * type
 * name
 * palette location
 * visible properties (edit Y/N) with defaults
 * shape
 * producers
 * consumers
 * description
 *
 *
 * GIANT NOTE:
 * Requires the batik jars to work!
 *
 * Scan the components and componentlibs folders, grab everything.  Indicate if the component is in the default set or in a lib.
 *
 *
 */
public class Encyclopedia {

	public static void main(String[] args) {
		//def enc = new Encyclopedia();
		//enc.hyperIconTime()

		String option
		
		try {
			Options options = new Options();
			options.addOption("?", "help", true, "help")
			options.addOption("i", "icontime", true, "generate png icon for one svg shape")
			options.addOption("h", "hypericontime", false, "generate png icons for all svg shapes")

			options.addOption("n", "internal", false, "generate internal encyclopedia")
			options.addOption("e", "external", false, "generate external encyclopedia")

			options.addOption("b", "libinternal", true, "generate internal encyclopedia for one library")
			options.addOption("l", "libexternal", true, "generate external encyclopedia for one library")

			CommandLineParser parser = new PosixParser();
			CommandLine cmd = parser.parse(options, args);

			if (cmd.hasOption("?")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("Encyclopedia", options);
				return
			}

			def enc = new Encyclopedia()

			if(cmd.hasOption("h")){
				println "generate all icons"
				enc.hyperIconTime()
			} else if (cmd.hasOption("i") ){
				println "Genterate icon for shape ${cmd.getOptionValue("i")}"
				enc.iconTime(cmd.getOptionValue("i"))

			} else if (cmd.hasOption("n")){
				println "internal encyclopedia"
				enc.go("", true)
			} else if (cmd.hasOption("e")){
				println "external encyclopedia"
				enc.go("", false)

			} else if (cmd.hasOption("b")){
				println "internal library encyclopedia"
				enc.go(cmd.getOptionValue("b"), false)
			} else if (cmd.hasOption("l")){
				println "external library encyclopedia"
				enc.go(cmd.getOptionValue("l"), false)
			}

		} catch (Exception e) {
			println "bad command or filename"
			e.printStackTrace()
		}

	}


	private String convertUTF8Format(File file) {
		String convertedString = ""
		FileInputStream fis = new FileInputStream(file)
		byte[] contents = new byte[fis.available()]
		fis.read(contents, 0, contents.length)
		convertedString = new String(contents, "UTF-8")
		fis.close()
		return convertedString
	}

	public void go(String filename = "", boolean internalMode = false) {
		def encyc = []
		int count = 0

		//get all the component files into a list to read
		ClassLoader cl = this.getClass().getClassLoader()
		def componentsProperties = new Properties()
		componentsProperties.load(cl.getResourceAsStream("cfg/components.properties"))
		String dirPath = componentsProperties.getProperty('components.path')
		//String backPath = componentsProperties.getProperty('components.backcompatability.path')
		//backpath is just the standard set, none of the wackier subfolders of the dcl tree
		String backPath = "../componentlibs/ss"
		def componentLibraries = Arrays.asList(componentsProperties.getProperty('componentLibraries.list').split(","))
		//get everything in the components folder
		File compFolder = new File(dirPath)
		File backFolder = new File(backPath)
		List<File> allFilesList
		if (filename.length() == 0) {
			allFilesList = compFolder.listFiles()
			allFilesList.addAll(backFolder.listFiles())
		} else {
			allFilesList = []
			filename.split(",").each { fn ->
				if (fn.contains(".xml") || fn.contains(".dcl")) {
					def stdloc = new File(dirPath + fn.trim())
					if (stdloc.exists()) {
						allFilesList += stdloc
					} else {
						allFilesList += new File(backPath + "/" + fn.trim())
					}
					//allFilesList += new File(dirPath + fn.trim())
				} else {
					allFilesList += new File(dirPath + fn.trim() + ".xml")
				}
			}
		}

		//process xml into memory
		allFilesList.each { componentLibrary ->
			println "attempting to find $componentLibrary"

			if (componentLibrary.isFile() && (componentLibrary.getName().endsWith(".xml") || componentLibrary.getName().endsWith(".dcl"))) {
				println "reading xml file $componentLibrary"
				String filePath = componentLibrary.getAbsolutePath()
				println "filepath is $filePath"

				// Is the UTF8 conversion necessary? The helper method doesn't do it.
				// def componentLibraryXml = new XmlSlurper().parseText(convertUTF8Format(componentLibrary))
				def componentLibraryXml = ComponentLibrary.parseXmlFile( filePath )

				def fileCount = 0
				componentLibraryXml.component.each { componentXml ->
					println "!"
					def thisOne = processXML(componentXml, componentLibrary, componentLibraries)
					if (thisOne) {
						count += 1
						fileCount += 1
						encyc += thisOne
					}
				}
				//this grabs non-default stuff
				if (fileCount == 0) {
					def thisOne = processXML(componentLibraryXml, componentLibrary, componentLibraries)
					if (thisOne) {
						count += 1
						fileCount += 1
						encyc += thisOne
					}
				}
			}
		}


		encyc.sort {
			//(it["isDefault"] ? "A" : "B") + it["name"]

			//sort by palette:
			//(it["isDefault"] ? "A" : (it["path"].toString().substring( it["path"].toString().lastIndexOf(">")+1 ,it["path"].toString().length() ).trim()  )  ) + it["name"]

			//sort by filename
			(it["isDefault"] ? "A" : (it["filename"])) + it["name"]
		}

		/*
		OUTPUT SETUP
		*/
		def root = new File("encyclopedia")
		if (!root.exists()) {
			root.mkdir()
		}

		goTexOrGoHome(encyc, internalMode)

		associationList(encyc)
		allDescriptions(encyc)


		println "DONE AND DONE"

	}

	public void hyperIconTime(){
		//list all shapes, save as PNG
		new File("cfg/shapes").eachFile(FileType.FILES) { f ->
			if (f.getName().contains(".svg")){
				def shapename = FilenameUtils.removeExtension( f.getName() )
				this.iconTime(shapename)
			}
		}
		println "AND DONE"
	}
	
	public void iconTime(String shapename) {
		saveAsPNG("cfg/shapes/${shapename}.svg", "encyclopedia/${shapename}.png")
	}


	public boolean saveAsPNG(String svgFile, String pngFile) {
		def check = new File(pngFile)

		if (check.exists()) {
			return true
		}

		println "generating PNG $pngFile"

		// Create a PNG transcoder
		PNGTranscoder t = new PNGTranscoder();
		// Set the transcoding hints.
		t.addTranscodingHint(PNGTranscoder.KEY_HEIGHT, 200.0f);
		// Create the transcoder input.
		String svgURI = new File(svgFile).toURL().toString();
		TranscoderInput input = new TranscoderInput(svgURI);
		// Create the transcoder output.
		OutputStream ostream = new FileOutputStream(pngFile);
		TranscoderOutput output = new TranscoderOutput(ostream);
		// Save the image.
		try {
			t.transcode(input, output);
		} catch (Exception e) {
			println "Conversion Error: $e\n"
			return false
		}
		// Flush and close the stream.
		ostream.flush();
		ostream.close();

		//can we just resize with the transcoder?
		List<Float> sizes = [16.0f,24.0f,48.0f,100.0f,200.0f]
		for(Float size : sizes){
			t = new PNGTranscoder();

			//t.addTranscodingHint(PNGTranscoder.KEY_HEIGHT, size);
			//t.addTranscodingHint(PNGTranscoder.KEY_WIDTH, size);
			//we want the largest dimension to be <size> and the other dimension to be scaled appropriately
			t.addTranscodingHint(PNGTranscoder.KEY_MAX_HEIGHT, size);
			t.addTranscodingHint(PNGTranscoder.KEY_MAX_WIDTH, size);


			input = new TranscoderInput(svgURI);
			ostream = new FileOutputStream(pngFile.replace(".png", "_${size.toInteger()}.png"));
			output = new TranscoderOutput(ostream);
			try {
				t.transcode(input, output);
			} catch (Exception e) {
				println "Conversion Error: $e\n"
				return false
			}
			ostream.flush();
			ostream.close();
		}

		return true
	}


	public def processXML(componentXml, componentLibrary, componentLibraries) {

		println 'load component:' + componentXml.@type.text() + ' at path ' + componentXml.@grouppath.text()
		//if (componentXml.@grouppath.text() != null && !componentXml.@grouppath.text().equals("")) {

		//count += 1
		def comp = [:]
		comp["type"] = componentXml.@type.text()
		comp["roles"] = componentXml.@classes.text()
		comp["path"] = "" + (componentXml.@grouppath.text().replace(";", " > "))
		comp["name"] = componentXml.display.name.text()
		comp["shape"] = componentXml.display.shape.text()
		comp["description"] = componentXml.display.description.text()
		comp["shortDesc"] = componentXml.display.shortdescription.text()
		if(!comp["shortDesc"] || !(comp["shortDesc"].size() > 0)){
			//fill in from the main desc
			int period = comp["description"].indexOf('.')
			if ( period > 0){
				comp["shortDesc"] = comp["description"].substring(0, comp["description"].indexOf('.') + 1)
			} else {
				comp["shortDesc"] = comp["description"]
			}

		}

		comp["properties"] = []
		comp["consumers"] = []
		comp["producers"] = []
		comp["conducers"] = []
		comp["filename"] = componentLibrary.getName()

		comp["isDefault"] = false
		comp["isLegacy"] = false
		comp["isInternal"] = false

		if (componentLibraries.contains(comp["filename"])) {
			comp["isDefault"] = true
			comp["library"] = "Part of the Default Library."
		}

		if (comp["path"].contains("Legacy")) {
			comp["isLegacy"] = true
			comp["library"] = "Part of the Legacy Library."
		}

		if (!comp["isDefault"] && !comp["isLegacy"]) {
			comp["isInternal"] = true
			comp["library"] = "This is a custom component, not for general distribution."
		}

		//get the sensor data, if available
		comp["sensors"] = []
		def multiNode = false
		def nodes = 0
		componentXml.object.each { obj ->
			if (obj.@type == 'wsnnode') {
				nodes += 1
			}
		}
		if (nodes > 1) {
			multiNode = true
		}

		componentXml.object.each { obj ->
			if (obj.@type == 'wsnnode') {
				obj.property.each { pro ->
					if (pro.@name.equals("sensepoints")) {
						pro.object.each { s ->
							//name, visualLayer, channel, type
							def currentSensor = [:]
							s.property.each { op ->
								if (multiNode && op.@name.text().equals("channel")) {
									currentSensor[op.@name.text()] = obj.@as.text() + ":" + op.value.text()
								} else {
									currentSensor[op.@name.text()] = op.value.text()
								}
							}
							comp["sensors"] += currentSensor
						}
					}
				}
			}
		}


		componentXml.property.each { p ->

			//if ( p.@editable.equals("true") && p.@displayed.equals("true") ) {
			if (p.@displayed.equals("true")) {
				//comp["properties"] += p.@name
				def prop = [:]
				prop["name"] = p.@display
				prop["value"] = p.text().trim()

				String type = p.@type
				type = type.substring(type.lastIndexOf(".") + 1)
				prop["fullType"] = p.@type
				prop["type"] = type
				comp["properties"] += prop
			}
		}

/*
<consumer id="input" datatype="power" name="Input Value" collection="false" property='$math/properties[name="input"]' required="true" />
<producer id="value" datatype="power" name="Absolute Value" object='$math' />
*/

		componentXml.consumer.each { p ->
			//comp["consumers"] += p.@name
			def cons = [:]
			cons["id"] = p.@id
			cons["datatype"] = p.@datatype
			cons["name"] = p.@name.toString().replace("&", "\\&")
			cons["collection"] = p.@collection
			cons["required"] = p.@required
			comp["consumers"] += cons
		}
/*
			componentXml.conducer.consumer.each { p ->
				//comp["consumers"] += p.@name
				def cons = [:]
				cons["id"] = p.@id
				cons["datatype"] = p.@datatype
				cons["name"] = p.@name
				cons["collection"] = p.@collection
				cons["required"] = p.@required
				comp["consumers"] += cons
			}
  */
		componentXml.producer.each { p ->
			//comp["producers"] += p.@name
			def prod = [:]
			prod["id"] = p.@id
			prod["datatype"] = p.@datatype
			prod["name"] = p.@name.toString().replace("&", "\\&")
			comp["producers"] += prod
		}
/*
			componentXml.conducer.producer.each { p ->
				//comp["producers"] += p.@name
				def prod = [:]
				prod["id"] = p.@id
				prod["datatype"] = p.@datatype
				prod["name"] = p.@name
				comp["producers"] += prod
			}
*/

		componentXml.conducer.each { p ->
			def cond = [:]
			cond["id"] = p.@id
			cond["datatype"] = p.consumer.@datatype.toString() + "-" + p.producer.@datatype.toString()
			cond["name"] = p.@name.toString().replace("&", "\\&")
			comp["conducers"] += cond
		}


		println "just added " + comp["name"]
		//encyc += comp
		return comp
		//}
		//return null
	}


	public def allDescriptions(encyc) {

		def sorted = encyc.sort { it["path"] + it["name"] }

		def writer = new FileWriter("encyclopedia/AllDescriptions.html")
		def html = new MarkupBuilder(writer)
		html.html(lang: "en") {
			head {
				title "Component Encyclopedia: All Descriptions"
				link(rel: "stylesheet", href: "syse.css", type: "text/css", title: "MapSense Style")
			}
			body {
				sorted.each { c ->
					article {

						p {
							//strong "${c["name"]} -- (${c["path"]}) [${c["type"]}]"
							strong "${c["name"]} [${c["filename"]}:${c["type"]}]"

						}

						p c["shortDesc"]

						p c["description"]
					}

				} //sorted.each
			} //body
		} //html
	} //allDesc


	//<xsl:when test='$cfg = "power_inspector"'><xsl:text>Provides an attachment point for viewing a Numeric or Calculated value.</xsl:text></xsl:when>

	public def makeUpgrade(encyc) {

		def sorted = encyc.sort { it["path"] + it["name"] }

		def writer = new FileWriter("encyclopedia/upgrades.html")
		def html = new MarkupBuilder(writer)
		html.html(lang: "en") {
			head {
				title "Component Encyclopedia: All Descriptions"
				link(rel: "stylesheet", href: "syse.css", type: "text/css", title: "MapSense Style")
			}
			body {

				sorted.each { c ->

					def upgrade = "<xsl:when test='\$cfg = \"${c["type"]}\"'>"
					upgrade += "<xsl:text>${c["description"]}</xsl:text></xsl:when>"

					p upgrade


				} //sorted.each

			} //body
		} //html
	} //makeUpgrade

	/**
	 * How about.. XeTex?
	 * @param encyc List of maps for the encyc
	 * @return
	 */
	public def goTexOrGoHome(List encyc, boolean internalMode = false) {
		String preamble = """
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode

\\documentclass[12pt]{article}
\\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\\geometry{letterpaper}                   % ... or a4paper or a5paper or ...
\\usepackage{graphicx}
\\usepackage{amssymb}
%\\usepackage{longtable}
\\usepackage{fullpage}
\\usepackage{multicol}

\\usepackage{fontspec,xltxtra,xunicode}
\\defaultfontfeatures{Mapping=tex-text}
\\setmainfont[Mapping=tex-text]{Palatino Linotype}
\\setmonofont[Scale=MatchLowercase]{Consolas}

\\newcommand{\\HRule}{\\rule{\\linewidth}{0.5mm}}


\\include{synap_include}

\\begin{document}

%%\\newcolumntype{R}[1]{>{\\RaggedLeft\\hspace{0pt}}p{#1}}

\\newcolumntype{R}[1]{>{\\raggedleft\\arraybackslash}p{#1}}

"""

		if (!internalMode) {
			preamble += "\\include{frontmatter}"
		}


		preamble += """
\\tableofcontents
		"""

		String afterword = """
\\end{document}
		"""

		String main = ""

		main += "\\newpage \n"

		if (internalMode) {
			main += "\\section{Components}"
		} else {
			main += "\\section{Default Components}"
		}

		main += "\\newpage \n"

		boolean intoLegacy = false

		for (def e: encyc) {

			if (e["roles"].contains("staticchild") && !e["roles"].contains("staticchildplaceable")) {
				continue
			}

			if (!e["roles"].contains("placeable")){
				continue
			}

//			if (e["isInternal"] && !internalMode ){
			//				continue
			//			}

			println "processing page for ${e['name']}"

			main += "\\newpage \n"

			if (!intoLegacy && !e["isDefault"]) {
				if (!internalMode) {
					main += "\\section{Component Libraries}"
				} else {
					main += "\\section{Legacy \\& Custom Components}"
				}
				main += "\\newpage \n"
				intoLegacy = true
			}

			if (internalMode) {
				main += "\\subsection{ ${e['name']} (${e['type'].replace("_", "\\_")}) } \n"
			} else {
				if (e["isDefault"]) {
					main += "\\subsection{ ${e['name']} } \n"
				} else {
					//main += "\\subsection{ ${e['name']} (${e["path"].toString().substring( e["path"].toString().lastIndexOf(">")+1 ,e["path"].toString().length() ).trim() })} \n"
					//main += "\\subsection{ ${e['name']} (${e["filename"].replace("_", "\\_") })} \n"
					main += "\\subsection{ ${e['name']} } \n"
				}


			}

			main += "\\begin{description} \n"

			if (internalMode) {
				main += "\\item[Type] ${e['type'].replace("_", "\\_")} \n"

				main += "\\item[Filename] ${e['filename'].replace("_", "\\_")} \n"

				main += "\\item[Shape] ${e['shape'].replace("_", "\\_")} \n"


			}

			main += "\n\n"

			String path = e["path"].replace(">", '$\\to$').replace("&", "\\&")
			if (path.trim().length() == 0) { path = "N/A"}
			main += "\\item[Palette Location] $path \n"


			main += "\n\n"

			if (!e["isDefault"]) {
				String filename = e["filename"].replace("_", '\\_')
				main += "\\item[Component Library] $filename \n"

			}

			main += "\\end{description} \n"



			if (e["shape"] != "") {
				if (saveAsPNG("cfg/shapes/${e["shape"]}.svg", "encyclopedia/${e["shape"]}.png")) {

/*
					main += """
\\begin{wrapfigure}{l}{2cm}
	\\fbox{
		\\includegraphics{${e["shape"]}}
	}
\\end{wrapfigure}
					"""
*/

					main += """
					\\includegraphics[height=2cm, width=2cm, keepaspectratio=true]{${e["shape"].replace(".png", "_200.png")}}
					"""

				}
			}

			//main += "\\subsection{Description}"
			//main += "\\textbf{Description}"
			main += processText(e["description"])
			main += "\n\n"

			//main += "\\subsection{Properties} \n"
			main += """
\\begin{flushleft}
\\begin{tabularx}{\\textwidth}{@{} R{5cm}p{6.5cm}X @{}}
\\toprule
\\multicolumn{3}{l}{\\textbf{Properties}} \\\\
\\midrule
Name & Default Value & Type \\\\
\\midrule
"""
			e['properties'].each { p ->
				if (internalMode) {
					main += "${p['name'].toString().replace("%", "\\%")} & ${p['value'].toString().replace("_", "\\_")} & ${p['type']} \\\\ \n"
				} else {
					main += "${p['name'].toString().replace("%", "\\%")} & ${p['value'].toString().replace("_", "\\_")} & ${ PropertyConverter.toSimpleName(p['fullType'].toString()) } \\\\ \n"
				}
			}

			main += """
\\bottomrule
\\end{tabularx}
\\end{flushleft}
"""


			if (e['producers'].size() > 0) {
				def tableEnv = "tabular"
				def endhead = ""
				if(e['producers'].size() > 10) {
					tableEnv = "longtable"
					endhead = "\\endhead"
					//main += "\\newpage \n"
				}


				main += """
\\begin{flushleft}
\\begin{$tableEnv}[l]{@{} rl @{}}
\\toprule
\\multicolumn{2}{l}{\\textbf{Producers}} \\\\
\\midrule
Name & DataType \\\\
\\midrule $endhead
"""

				e['producers'].each { p ->

					if (internalMode) {
						main += "${p['name']} & \\texttt{${p['datatype'].toString().replace("_", "\\_")}} \\\\ \n"
					} else {
						main += "${p['name']} & \\texttt{${ CentralCatalogue.getInstance().getAssociationProperties().getProperty('associations.descr.' + p['datatype']).replace("&", "\\&")  }} \\\\ \n"
					}


				}

				main += """
\\bottomrule
\\end{$tableEnv}
\\end{flushleft}
"""

			}

			if (e['consumers'].size() > 0) {
				def tableEnv = "tabular"
				def endhead = ""
				if(e['consumers'].size() > 10) {
					tableEnv = "longtable"
					endhead = "\\endhead"
				}

				main += """
\\begin{flushleft}
\\begin{$tableEnv}[l]{@{} rlll @{}}
\\toprule
\\multicolumn{4}{l}{ \\textbf{Consumers}} \\\\
\\midrule
Name & DataType & Collection & Required \\\\
\\midrule $endhead
"""

				e["consumers"].each { c ->
					if (internalMode) {
						main += "${c['name']} & \\texttt{${c['datatype'].toString().replace("_", "\\_")}} & ${StringUtil.rns(c['collection'].toString(), "false")}  & ${c['required']} \\\\ \n"
					} else {
						main += "${c['name']} & \\texttt{${ CentralCatalogue.getInstance().getAssociationProperties().getProperty('associations.descr.' + c['datatype']).replace("&", "\\&") }} & ${StringUtil.rns(c['collection'].toString(), "false")}  & ${c['required']} \\\\ \n"
					}
				}

				main += """
\\bottomrule
\\end{$tableEnv}
\\end{flushleft}
"""
			}

			//[name:internal temp, channel:0, min:40.0, max:100.0, aMin:59.0, aMax:90.0, rMin:64.4, rMax:80.6, type:1, dataclass:200, enabled:0, mode:instant, interval:0, z:0, status:1, lastValue:-5000.0]
			if (e['sensors'].size() > 0) {
				main += """
					\\begin{flushleft}
					\\begin{tabular}{@{} rllll @{}}
					\\toprule
					\\multicolumn{5}{l}{\\textbf{Sensors}} \\\\
					\\midrule
					Channel & Name & LI & Min & Max \\\\
					\\midrule
					"""

				//" & Type & Dataclass  \\\\

				e["sensors"].each { c ->
					main += "${c['channel'].toString().replace("_", "\\_")} & ${c['name']} & ${ LICode(c['z'])} & "
					//main += "${ c['min'] }  & ${ c['max'] } &  ${ StringUtil.rns (c['scaleMin'], "--") } &   ${ StringUtil.rns (c['scaleMax'], "--") } " //& "
					main += "${ c['min'] }  & ${ c['max'] } " //& "
					//main += "${c['type']} & ${c['dataclass']} \\\\ "
					main += " \\\\ "
				}

				main += """
					\\bottomrule
					\\end{tabular}
					\\end{flushleft}
					"""
			}

			if (internalMode && true == false) {

				//sensor table the other direction;
				//just holds data not in the other table
				if (e['sensors'].size() > 0) {
					Set names = new TreeSet()
					names.addAll(e['sensors'].first().keySet())
					def values = [:]
					names.each { n ->

						values[n] = []
						e["sensors"].each { s ->
							values[n] += s[n]
						}
					}
					int numSensors = e["sensors"].size()

					main += """
					\\begin{flushleft}
					\\begin{tabular}{@{} r
					"""

					// \\begin{tabular}{@{} rllllllll @{}}
					for (int i = 0; i < numSensors; i++) {
						if (numSensors < 5) {
							main += "l"
						} else {
							def colIn = 5.75 / (numSensors + 1)
							main += "p{ ${colIn}in }"
						}
					}

					main += """
					@{}}
					\\toprule
					\\multicolumn{ ${numSensors + 1} }{l}{\\textbf{Sensors}} \\\\
					\\midrule
					Channel
					"""

					//Channel & Name & LI & Min & Max & Scale Min & Scale Max & Type & Dataclass  \\\\

					e['sensors'].each { s ->
						main += " & " + s['channel'].replace("_", "\\_")
					}


					main += """
					\\\\
					\\midrule
					"""
					/*
						e["sensors"].each { c->
							main += "${c['channel']} & ${c['name']} & ${c['z']} & "
							main += "${ c['min'] }  & ${ c['max'] } &  ${ StringUtil.rns (c['scaleMin'], "--") } &   ${ StringUtil.rns (c['scaleMax'], "--") } & "
							main += "${c['type']} & ${c['dataclass']} \\\\ "
						}
		*/

					values.each { name, valList ->
						if (name != "name") {


							main += name
							valList.each { val ->
								String currentValue = "$val"
								main += " & " + currentValue.replace("_", "\\_")
							}

							main += " \\\\ "

						}
					}


					main += """
					\\bottomrule
					\\end{tabular}
					\\end{flushleft}
					"""
				}
			}
		}

		//main += "\\newpage \n"
		//main += "\\appendix \n"
		//main += "\\section{Recognition Guide} \n"
		//main += makeIconGrid(encyc, internalMode)

		//BufferedWriter out = new BufferedWriter(new FileWriter("encyclopedia/encyc.tex"));
		//make sure to output the resulting file as UTF-8
		String filenameSuffix
		if (internalMode) {
			filenameSuffix = "_int"
		} else {
			filenameSuffix = "_ext"
		}

		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("encyclopedia/encyc${filenameSuffix}.tex"), "UTF8"));

		out.write(preamble);
		out.write(main)
		out.write(afterword)
		out.close();

	}


	public String processText(String input){
		String result
		result = input.replace("ThermaNode", "Therma\\-Node")

		return result
	}

	public String LICode(String code) {
		if (code == null) {
			return "--"
		}

		switch (Integer.parseInt(code)) {
			case 0x20000:
				return "Top"
			case 0x4000:
				return "Middle"
			case 0x800:
				return "Bottom"
			case 0x100:
				return "Subfloor"

			case 0x24800:
				return "Top, Middle, and Bottom"
			case 0x24000:
				return "Top and Middle"
			case 0x4800:
				return "Middle and Bottom"
			case 0x900:
				return "Bottom and Subfloor"
			case 0:
				return "--"

			default:
				return code
		}
	}


	public void associationList(List encyc) {
		//for each assoc datatype, we want a list of producers and consumers
		/*
		(map)[(str)datatype:
			(map)[(str)producer:

				(list of maps)[(str)type:(str)t, display name:n, assoc int name:in, assoc ext name:en],
				[type:t, display name:n, assoc int name:in, assoc ext name:en]
			[consumer:
				[same]





*/

		Map<String, Map<String, List<Map<String, String>>>> assocs = [:]

		//assocs["power"] = [:]
		//assocs["power"]["consumer"] = []
		//assocs["power"]["producer"] = []


		def datatypes = new HashSet<String>()
		encyc.each { e ->
			e["consumers"].each { a ->
				datatypes.add(a["datatype"].toString())
			}
			e["producers"].each { a ->
				datatypes.add(a["datatype"].toString())
			}

			e["conducers"].each { a ->
				datatypes.add(a["datatype"].toString())
			}
		}

		println datatypes

		datatypes.each { dt ->
			assocs[dt] = [:]
			//assocs[dt]["consumers"] = []
			//assocs[dt]["producers"] = []

			assocs[dt].put("consumers", [])
			assocs[dt].put("producers", [])

			assocs[dt].put("conducers", [])
		}

		println assocs

		encyc.each {e ->
			e["consumers"].each { c ->
				/*
				if( ! assocs.containsKey( c["datatype"] ) ){
					assocs[ c["datatype"] ] = [:]
				}
				if ( ! assocs[ c["datatype"] ].containsKey("consumer") ){
					assocs[ c["datatype"] ]["consumer"] = []
				}
				*/
				def current = [:]
				current["type"] = e["type"].toString()
				current["name"] = e["name"].toString()
				current["assocId"] = c["id"].toString()
				current["assocName"] = c["name"].toString()
				(assocs[c["datatype"].toString()]["consumers"]).add(current)
			}

			e["producers"].each { p ->
				/*
				if( ! assocs.containsKey( p["datatype"] ) ){
					assocs[ p["datatype"] ] = [:]
				}
				if ( ! assocs[ p["datatype"] ].containsKey("producers") ){
					assocs[ p["datatype"] ]["producers"] = []
				}
				*/
				def current = [:]
				current["type"] = e["type"].toString()
				current["name"] = e["name"].toString()
				current["assocId"] = p["id"].toString()
				current["assocName"] = p["name"].toString()
				assocs[p["datatype"].toString()]["producers"] += current
			}


			e["conducers"].each { p ->
				def current = [:]
				current["type"] = e["type"].toString()
				current["name"] = e["name"].toString()
				current["assocId"] = p["id"].toString()
				current["assocName"] = p["name"].toString()
				assocs[p["datatype"].toString()]["conducers"] += current
			}


		}

		/*
		assocs.each{ k, v ->
			println k
			println v
		}

		println assocs

		println assocs.keySet().size()
		println assocs.values().size()
		*/

		def sb = new StringBuilder()

		def csv = new StringBuilder()
		def le = System.getProperty("line.separator");

		assocs.each { datatype, members ->

			sb.append("\\newpage")
			sb.append("\\section{Associations of Datatype ``${datatype.replace("_", "\\_")}''}")

			//sb.append("\\subsection{Consumers}")

			if (members["consumers"].size() > 0) {
				sb.append("""
\\begin{flushleft}
\\begin{tabular}{@{} p{8cm}ll @{}}
\\toprule
\\multicolumn{3}{l}{\\textbf{Consumers}} \\\\
\\midrule
Component Name & Association Name & Association ID \\\\
\\midrule
""")

				members["consumers"].each { a ->
					//sb.append("Type: ${a["type"]}  Name: ${a["name"]}  Assoc Name: ${a["assocName"]}  Assoc ID: ${a["assocId"]}  \n")

					//sb.append("${a["type"].replace("_", "\\_")}  & ${a["name"]}  & ${a["assocName"]}  & ${a["assocId"].replace("_", "\\_")} \\\\ \n")
					sb.append("${a["name"]}  & ${a["assocName"]}  & ${a["assocId"].replace("_", "\\_")} \\\\ \n")

				}

				sb.append("""
\\bottomrule
\\end{tabular}
\\end{flushleft}
""")
			}

			//sb.append("\\subsection{Producers}")
			if (members["producers"].size() > 0) {
				sb.append("""
\\begin{flushleft}
\\begin{tabular}{@{} p{8cm}ll @{}}
\\toprule
\\multicolumn{3}{l}{\\textbf{Producers}} \\\\
\\midrule
Component Name & Association Name & Association ID \\\\
\\midrule
""")

				members["producers"].each { a ->
					//sb.append("Type: ${a["type"]}  Name: ${a["name"]}  Assoc Name: ${a["assocName"]}  Assoc ID: ${a["assocId"]}  \n")
					//sb.append("${a["type"].replace("_", "\\_")}  & ${a["name"]}  & ${a["assocName"]}  & ${a["assocId"].replace("_", "\\_")} \\\\ \n")
					sb.append(" ${a["name"]}  & ${a["assocName"]}  & ${a["assocId"].replace("_", "\\_")} \\\\ \n")
				}

				sb.append("""
\\bottomrule
\\end{tabular}
\\end{flushleft}
""")
			}

			if (members["conducers"].size() > 0) {

				sb.append("""
\\begin{flushleft}
\\begin{tabular}{@{} p{8cm}ll @{}}
\\toprule
\\multicolumn{3}{l}{\\textbf{Conducers}} \\\\
\\midrule
Component Name & Association Name & Association ID \\\\
\\midrule
""")

				members["conducers"].each { a ->
					//sb.append("Type: ${a["type"]}  Name: ${a["name"]}  Assoc Name: ${a["assocName"]}  Assoc ID: ${a["assocId"]}  \n")
					//sb.append("${a["type"].replace("_", "\\_")}  & ${a["name"]}  & ${a["assocName"]}  & ${a["assocId"].replace("_", "\\_")} \\\\ \n")
					sb.append(" ${a["name"]}  & ${a["assocName"]}  & ${a["assocId"].replace("_", "\\_")} \\\\ \n")
				}

				sb.append("""
\\bottomrule
\\end{tabular}
\\end{flushleft}
""")
			}

		}

		//println sb.toString()

		//FileUtils.writeStringToFile( new File("encyclopedia/assocs.txt"), sb.toString() )

		String preamble = """
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode

\\documentclass[10pt]{article}
\\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\\geometry{letterpaper}                   % ... or a4paper or a5paper or ...
\\usepackage{graphicx}
\\usepackage{amssymb}

\\usepackage{fontspec,xltxtra,xunicode}
\\defaultfontfeatures{Mapping=tex-text}
\\setmainfont[Mapping=tex-text]{Palatino Linotype}
\\setmonofont[Scale=MatchLowercase]{Consolas}

\\usepackage{booktabs}
\\usepackage{fullpage}
\\usepackage{float}
\\usepackage{wrapfig}
\\usepackage{hyperref}
\\usepackage{color}

\\newcommand{\\HRule}{\\rule{\\linewidth}{0.5mm}}

\\begin{document}

"""


		String afterword = """
\\end{document}
		"""

		String result = preamble + sb.toString() + afterword
		FileUtils.writeStringToFile(new File("encyclopedia/assocs.tex"), result)
		assocs.each { datatype, members ->
			//csv.append("Datatype $datatype $le")
			members["consumers"].each { a ->
				csv.append("$datatype,")
				csv.append("consumer,")
				csv.append(a["name"] + ",")
				csv.append(a["assocName"])
				csv.append(le)
			}
			members["producers"].each { a ->
				csv.append("$datatype,")
				csv.append("producer,")
				csv.append(a["name"] + ",")
				csv.append(a["assocName"] + ",")
				csv.append(le)
			}
			members["conducers"].each { a ->
				csv.append("$datatype,")
				csv.append("conducer,")
				csv.append(a["name"] + ",")
				csv.append(a["assocName"] + ",")
				csv.append(le)
			}
		}
		FileUtils.writeStringToFile(new File("encyclopedia/assocs.csv"), csv.toString())
	}


	String makeIconGrid(List encyc, boolean internalMode){
		StringBuilder result = new StringBuilder()


if(internalMode){

		result.append("""
\\begin{flushleft}
\\begin{longtable}{@{} p{1cm}p{7cm}l @{}}
\\toprule
Icon & Component & Internal \\\\
\\midrule
\\endhead
""")
		for(def e : encyc ){
			if(e["shape"] == null || e["shape"].size() == 0){
				continue
			}

			if (saveAsPNG("cfg/shapes/${e["shape"]}.svg", "encyclopedia/${e["shape"]}.png")) {
				result.append( """
					\\includegraphics[height=1cm, width=1cm, keepaspectratio=true]{${e["shape"].replace(".png", "_100.png")}}
					""")
			} else {
				result.append(" --- ")
			}
			result.append(" & ")
			result.append("${e['name']} \\newline \n")
			result.append("\\texttt{\\scriptsize ${e['shape'].replace("_", "\\_")} } \n" )

			result.append(" & ")

			result.append("\\texttt{\\scriptsize ${e['type'].replace("_", "\\_")} } \\newline " )
			//result.append("\\texttt{\\scriptsize ${e['shape'].replace("_", "\\_")} } \\\\ \n" )

			result.append(" \\\\ \n")
		}

	result.append("""
\\bottomrule
\\end{longtable}
\\end{flushleft}
""")

} else {
/*
	result.append("""
\\begin{flushleft}
\\begin{longtable}{@{} p{1cm}p{7cm} @{}}
\\toprule
Icon & Component  \\\\
\\midrule
\\endhead
""")
	for(def e : encyc ){
		if(e["shape"] == null || e["shape"].size() == 0){
			continue
		}

		if (saveAsPNG("cfg/shapes/${e["shape"]}.svg", "encyclopedia/${e["shape"]}.png")) {
			result.append( """
					\\includegraphics[height=1cm, width=1cm, keepaspectratio=true]{${e["shape"].replace(".png", "_100.png")}}
					""")
		} else {
			result.append(" --- ")
		}
		result.append(" & ")
		result.append("${e['name']} \\newline \n")

		result.append(" \\\\ \n")
	}
*/

	result.append("""
\\begin{multicols}{2}
\\begin{description}
""")

	for(def e : encyc ){
		if(e["shape"] == null || e["shape"].size() == 0){
			continue
		}
		result.append("\\item ")
		if (saveAsPNG("cfg/shapes/${e["shape"]}.svg", "encyclopedia/${e["shape"]}.png")) {
			result.append( """
					\\includegraphics[height=1cm, width=1cm, keepaspectratio=true]{${e["shape"].replace(".png", "_100.png")}}
					""")
		} else {
			result.append(" --- ")
		}


		result.append(" ${e['name']} \n")
	}
	result.append("""
\\end{description}
\\end{multicols}

""")

}





		return result.toString()
	}



} //class
