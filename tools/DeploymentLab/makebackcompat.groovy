package DeploymentLab

import groovy.io.FileType

public class Makebackcompat {

	public static void main(String[] args) {

		def path = new File("").getAbsolutePath()
		def destination = new File("").getAbsolutePath()
		println args
		if (args.length > 0) {
			path = args[0]
		}
		if (args.length > 1) {
			destination = args[1]
		}

		println "generating backcompat files"

		def list = []

		def dir = new File(path)
		dir.eachFileRecurse (FileType.FILES) { file ->
			if(file.getName().endsWith(".dcl") && !file.getAbsolutePath().contains("dev") ){
				list << file
			}
		}

		list.each {
			println it.path
			String guts = it.getText()
			String header = guts.substring( 0, guts.indexOf("<componentlib") )
			//okay, do some magic
			def comps = guts.findAll(/<component\s+type=["']([\S]*)["'][\s\S]*?\/component>/){m, a ->
				println a;
				def f = new File(destination, "${a}.xml")
				f.setText("$header \n $m", "UTF-8")
				return m
			}
			//println comps
		}

	}
}