package DeploymentLab

import javax.swing.JOptionPane

/**
 * Replaces the old encyc button from the lightning-bar
 * @author Gabriel Helman
 * @since
 * Date: 5/29/12
 * Time: 10:12 AM
 */
class EncycGui {
	public static void main(String[] args) {
		def enc = new Encyclopedia();
		def filename = JOptionPane.showInputDialog("(Internal Mode) Enter one or more library names")
		enc.go(filename, true);
	}
}