import xml.etree.ElementTree as etree
import os

#path = "D:\work\deploymentlab\componentlibs\ss"
path = os.getcwd()

##for dcl in os.listdir(path):
for dirpath, dirs, files in os.walk(path):
	#if 'internal' in dirpath:
	#	continue

	for dcl in files:
		if str(dcl).endswith(".dcl"):
			#print "parsing " + str(dcl)
			fullName = os.path.join(dirpath, dcl)
			print "parsing " + str(fullName)
			
			#tree = etree.parse(dcl) 
			tree = etree.parse(fullName) 
			root = tree.getroot()
			comps = root.findall('component')

			for c in comps:
				cname = c.attrib['type'] + ".xml"
				compTree = etree.ElementTree(c)
				f = open( cname, 'w' )
				compTree.write(f, "UTF-8", xml_declaration=True)