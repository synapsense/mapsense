<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
	<xsl:output method="text" />

	<xsl:template match="/">
<xsl:text>
digraph schema {
graph [ rankdir = "LR" ];
node [ fontsize = "12" shape = "record" ];
edge [];
</xsl:text>
		<xsl:for-each select="objects/object">
<xsl:text/>
"<xsl:value-of select="@type" />" [
	label = "<xsl:apply-templates mode="label" select="." />"
];
<xsl:text/>
		</xsl:for-each>
		
		<xsl:for-each select='objects/object/property[@type="childlink"]/reftype'>
<xsl:text/>
"<xsl:value-of select="../../@type" />":"<xsl:value-of select="../name" />" -> "<xsl:value-of select="." />":"obj-type" [
	label = "<xsl:value-of select="../name" /> -> <xsl:value-of select="." />"
	color = blue
];
<xsl:text/>
		</xsl:for-each>

		<xsl:for-each select='objects/object/property[@type="dockingpoint"]'>
<xsl:text/>
"<xsl:value-of select="../@type" />":"<xsl:value-of select="name" />" -> "<xsl:value-of select="reftype" />":"obj-type" [
	label = "<xsl:value-of select="name" /> -> <xsl:value-of select="reftype" />"
	color = red
];
<xsl:text/>
		</xsl:for-each>
} 
	</xsl:template>

	<xsl:template mode="label" match="object">
		<xsl:text/>&lt;obj-type&gt; <xsl:value-of select='@type' /> <xsl:text/>
		<xsl:apply-templates mode="label" select="property" />
	</xsl:template>

	<xsl:template mode="label" match='property[@type="childlink"]'>
		<xsl:text/> | &lt;<xsl:value-of select='name' />&gt; + <xsl:value-of select='name' /> : <xsl:value-of select='reftype' /> <xsl:text/>
	</xsl:template>

	<xsl:template mode="label" match='property[@type="setting"]'>
		<xsl:text/> | &lt;<xsl:value-of select='name' />&gt; + <xsl:value-of select='name' /> : <xsl:value-of select='valuetype' /> <xsl:text/>
	</xsl:template>

	<xsl:template mode="label" match='property[@type="rule"]'>
		<xsl:text/> | &lt;<xsl:value-of select='name' />&gt; + <xsl:value-of select='name' /> : <xsl:value-of select='ruleclass' /> <xsl:text/>
	</xsl:template>

	<xsl:template mode="label" match='property[@type="dockingpoint"]'>
		<xsl:text/> | &lt;<xsl:value-of select='name' />&gt; + <xsl:value-of select='name' /> : <xsl:value-of select='reftype' /> <xsl:text/>
	</xsl:template>

	<xsl:template mode="label" match='property[@type="collection"]'>
		<xsl:text/> | &lt;<xsl:value-of select='name' />&gt; + <xsl:value-of select='name' /> : <xsl:value-of select='reftype' /> <xsl:text/>
	</xsl:template>

	<xsl:template mode="label" match='property[@type="groovyrule"]'>
		<xsl:text/> | &lt;<xsl:value-of select='name' />&gt; + <xsl:value-of select='name' /> : <xsl:value-of select='rulefile' /> <xsl:text/>
	</xsl:template>
</xsl:stylesheet>

