<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="2.0"
	>

	<xsl:output method="html" />

	<xsl:param name="revision" />

	<xsl:template match="/">
		<html>
			<head>
				<title>DeploymentLab Components</title>
				<style type="text/css">
					.smaller { font-size: 50%; }
				</style>
			</head>
			<body>
				<p class="smaller">Document generated <xsl:value-of select="current-dateTime()" /> at revision <xsl:value-of select="$revision" /></p>
				<h1>Components:</h1>
				<ul>
				<xsl:for-each select="*/component">
					<li><a>
						<xsl:attribute name="href">
							<xsl:text />#<xsl:value-of select="display/name" />
						</xsl:attribute>
						<xsl:value-of select="display/name" />
					</a> (<xsl:value-of select="@type" />)</li>
				</xsl:for-each>
				</ul>
				<hr />
				
				<xsl:apply-templates select="components/*" />
				<xsl:apply-templates select="componentlib/*" />
			</body>
		</html>
	</xsl:template>

	<xsl:template match="component">
		<a>
			<xsl:attribute name="name">
				<xsl:value-of select="display/name" />
			</xsl:attribute>
		</a>
		<h2><xsl:value-of select="display/name" /> <span class="smaller"> (<xsl:value-of select="@type" />)</span></h2>
		Component type: <xsl:value-of select="@type" />
		<br/>
		DL classes: <xsl:value-of select="@classes" />
		<br />
		<h3>Display information</h3>
		Icon Shape: <xsl:value-of select="display/shape" />
		<xsl:if test="display/haloradius != ''">
		<br />
		Halo radius: <xsl:value-of select="display/haloradius" />in
		</xsl:if>
		<xsl:if test="display/halocolor != ''">
		<br />
		Halo color: <xsl:value-of select="display/halocolor" />in
		</xsl:if>				
		<br />
		Default color: <xsl:value-of select="display/color" />
		<br />
		Configuration description: <div style="width: 50ex; border-left: thin black solid; margin-left: 10ex;"><xsl:value-of select="display/description" /></div>
		<br/>
		<h3>Component properties</h3>
		<xsl:for-each select="property">
			<xsl:value-of select="@display"/>: <xsl:value-of select="."/>
			<br/>
		</xsl:for-each>

		<h3>Component field to sense-point mapping:</h3>
		<table border="1">
			<tr>
				<th>From</th>
				<th>To</th>
			</tr>
			<xsl:for-each select="docking">
				<tr>
					<td><xsl:value-of select="@from" /></td>
					<td><xsl:value-of select="@to" /></td>
				</tr>
			</xsl:for-each>
		</table>

		<xsl:apply-templates select="object[@type='node']" />

		<h3>Variable Binding:</h3>
		<xsl:apply-templates  select="varbinding"/>

		<h3>Producers:</h3>
		<table border="1">
			<tr>
				<th>Name</th>
				<th>Data Type</th>
				<th>ID</th>
				<th>Object</th>
			</tr>
			<xsl:for-each select="producer">
				<tr>
					<td><xsl:value-of select="@name" /></td>
					<td><xsl:value-of select="@datatype" /></td>
					<td><xsl:value-of select="@id" /></td>
					<td><xsl:value-of select="@object" /></td>
				</tr>
			</xsl:for-each>
		</table>

		<h3>Consumers:</h3>
		<table border="1">
			<tr>
				<th>Name</th>
				<th>Data Type</th>
				<th>ID</th>
				<th>Property</th>
				<th>Collection</th>
				<th>Required</th>
			</tr>
			<xsl:for-each select="consumer">
				<tr>
					<td><xsl:value-of select="@name" /></td>
					<td><xsl:value-of select="@datatype" /></td>
					<td><xsl:value-of select="@id" /></td>
					<td><xsl:value-of select="@property" /></td>
					<td><xsl:value-of select="@collection" /></td>
					<td><xsl:value-of select="@required" /></td>
				</tr>
			</xsl:for-each>
		</table>

		<hr />
	</xsl:template>
	
	<xsl:template match="varbinding">
		<table border="1">
			<tr>
				<td>Variable: </td>
				<td>
					<xsl:value-of select="@vars"/>
				</td>
			</tr>
			<tr>
				<td>Property: </td>
				<td>
					<xsl:for-each select="property">
						<xsl:value-of select="." /><br/>
					</xsl:for-each>
				</td>
			</tr>
			<tr>
				<td>Value: </td>
				<td><xsl:value-of select="value"/></td>
			</tr>
		</table><br/>
	</xsl:template>	

	<xsl:template match="object[@type='node']">
		<h3>Node Type: <xsl:call-template name="nodePlatform"><xsl:with-param name="platId" select="property[@name='platform']" /></xsl:call-template></h3>
		<p>
		
		Battery Capacity: <xsl:value-of select="property[@name='batteryCapacity']/value"/>
		<br/>
		Battery Capacity Time Stamp: <xsl:value-of select="property[@name='batteryCapacityTimestamp']/value"/>
		<br/>
		Remaining Days: <xsl:value-of select="property[@name='remainingDays']/value"/>
		<br/>
		Status: <xsl:value-of select="property[@name='status']/value"/>
		<br/>

		</p>
		<h4>Sensors</h4>
		<table border="1">
			<tr>
				<th>Name</th>
				<th>Channel</th>
				<th>Data class</th>
				<th>Sensor Display Range</th>
				<th>Allowable Range</th>
				<th>Recommended Range</th>	
				<th>Type</th>	
				<th>Visual Layer</th>
				<th>Status</th>
				<th>Last Value</th>	
			</tr>
			<xsl:apply-templates select="property[@name='sensepoints']/object" />
		</table>
	</xsl:template>
	
	<xsl:template match="property[@name='sensepoints']/object">
		<tr>
			<td><xsl:value-of select="property[@name='name']/value"/></td>
			<td><xsl:value-of select="property[@name='channel']/value"/></td>
			<td><xsl:value-of select="property[@name='dataClass']/value"/></td>
			<td>
				<xsl:choose>
					<xsl:when test="string-length(property[@name='sensorDisplayMax']/value) != 0">
						<xsl:text /><xsl:value-of select="property[@name='sensorDisplayMin']/value" /> - <xsl:value-of select="property[@name='sensorDisplayMax']/value" /><xsl:text />
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>N/A</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</td>
			<td>
				<xsl:choose>
					<xsl:when test="string-length(property[@name='AMin']/value) != 0">
						<xsl:text /><xsl:value-of select="property[@name='AMin']/value" /> - <xsl:value-of select="property[@name='AMax']/value" /><xsl:text />
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>N/A</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</td>		
			<td>
				<xsl:choose>
					<xsl:when test="string-length(property[@name='RMin']/value) != 0">
						<xsl:text /><xsl:value-of select="property[@name='RMin']/value" /> - <xsl:value-of select="property[@name='RMax']/value" /><xsl:text />
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>N/A</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</td>
			<td>
				<xsl:call-template name="sensorID">
					<xsl:with-param name="sensorid" select="property[@name='type']/value" />
				</xsl:call-template>
			</td>
			<td>
				<xsl:call-template name="liLayer">
					<xsl:with-param name="layer" select="property[@name='visualLayer']/value" />
				</xsl:call-template></td>
			<td><xsl:value-of select="property[@name='status']/value"/></td>
			<td><xsl:value-of select="property[@name='lastValue']/value"/></td>
		</tr>
	</xsl:template>

	<xsl:template name="sensorID">
		<xsl:param name="sensorid" />

		<xsl:choose>
			<xsl:when test="$sensorid=1">
				<xsl:text>Node-internal temperature</xsl:text>
			</xsl:when>
			<xsl:when test="$sensorid=2">
				<xsl:text>Node-internal humidity</xsl:text>
			</xsl:when>
			<xsl:when test="$sensorid=5">
				<xsl:text>Node-internal battery</xsl:text>
			</xsl:when>
			<xsl:when test="$sensorid=9">
				<xsl:text>Door open/closed</xsl:text>
			</xsl:when>
			<xsl:when test="$sensorid=12">
				<xsl:text>Liquid-level</xsl:text>
			</xsl:when>
			<xsl:when test="$sensorid=20">
				<xsl:text>Power</xsl:text>
			</xsl:when>
			<xsl:when test="$sensorid=21">
				<xsl:text>External thermistor (Standard node)</xsl:text>
			</xsl:when>
			<xsl:when test="$sensorid=24">
				<xsl:text>Pressure</xsl:text>
			</xsl:when>
			<xsl:when test="$sensorid=25">
				<xsl:text>External thermistor (Fusion node)</xsl:text>
			</xsl:when>
			<xsl:when test="$sensorid=26">
				<xsl:text>0-200A current transducer</xsl:text>
			</xsl:when>
			<xsl:when test="$sensorid=27">
				<xsl:text>External thermistor (Thermanode)</xsl:text>
			</xsl:when>
			<xsl:when test="$sensorid=28">
				<xsl:text>Water flow</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text/>Unknown sensor type (<xsl:value-of select="$sensorid" />)<xsl:text/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="liLayer">
		<xsl:param name="layer" />

		<xsl:choose>
			<xsl:when test="$layer=0">
				<xsl:text>N/A</xsl:text>
			</xsl:when>
			<xsl:when test="$layer=256">
				<xsl:text>Subfloor</xsl:text>
			</xsl:when>
			<xsl:when test="$layer=2048">
				<xsl:text>Floor</xsl:text>
			</xsl:when>
			<xsl:when test="$layer=16384">
				<xsl:text>Middle</xsl:text>
			</xsl:when>
			<xsl:when test="$layer=131072">
				<xsl:text>Top</xsl:text>
			</xsl:when>
			<xsl:when test="$layer=147456">
				<xsl:text>Top, Middle</xsl:text>
			</xsl:when>
			<xsl:when test="$layer=1048576">
				<xsl:text>Ceiling</xsl:text>
			</xsl:when>
			<xsl:when test="$layer=149504">
				<xsl:text>Floor, Middle, Top</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text/>Unknown LI layer (<xsl:value-of select="$layer"/>)<xsl:text/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="nodePlatform">
		<xsl:param name="platId" />

		<xsl:choose>
			<xsl:when test="$platId=1">
				<xsl:text>Standard Node</xsl:text>
			</xsl:when>
			<xsl:when test="$platId=10">
				<xsl:text>Fusion Node</xsl:text>
			</xsl:when>
			<xsl:when test="$platId=11">
				<xsl:text>Thermanode</xsl:text>
			</xsl:when>
			<xsl:when test="$platId=12">
				<xsl:text>Pressure Node</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text/>Unknown (<xsl:value-of select="$platId" />)<xsl:text/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
