<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
	<xsl:output method="xhtml" encoding="utf-8" />

	<xsl:template match="/">
		<html>
			<head>
				<title>SimDatacenter 3000 Object Schema</title>
				<style type="text/css">
					tr { border: thin black solid; }
					th { background-color: gray; }
					td { border-left: thin black solid; border-bottom: thin black solid; text-align: center; }
					.section { width: 20ex; float: left; }
					.level { margin-left: 1em; }
				</style>
			</head>
			<body>
				<h1>Objects</h1>
				<ul>
				<xsl:for-each select="objects/object">
					<li><a><xsl:attribute name="href"><xsl:text/>#<xsl:value-of select="@type" /></xsl:attribute><xsl:value-of select="@type" /></a></li>
				</xsl:for-each>
				</ul>
				<xsl:apply-templates select="objects/object" />
			</body>
		</html>
	</xsl:template>

	<xsl:template match="object">
		<a><xsl:attribute name="name"><xsl:value-of select="@type" /></xsl:attribute></a>
		<h2>Object Type: <xsl:value-of select="@type" /></h2>
		<div class="level">
		<xsl:if test='property[@type="setting"]'>
			<h4 class="section">Settings</h4>
			<div class="level">
				<table>
					<tr><th>Name</th><th>Historic</th><th>Required</th><th>Value Type</th></tr>
					<xsl:apply-templates select='property[@type="setting"]' />
				</table>
			</div>
			<br />
		</xsl:if>
		<xsl:if test='property[@type="childlink"]'>
			<h4 class="section">Children</h4>
			<div class="level">
				<table>
					<tr><th>Name</th><th>Required</th><th>Child Type</th></tr>
					<xsl:apply-templates select='property[@type="childlink"]' />
				</table>
			</div>
			<br />
		</xsl:if>
		<xsl:if test='property[@type="rule"]'>
			<h4 class="section">Rules</h4>
			<div class="level">
				<table>
					<tr><th>Name</th><th>Historic</th><th>Value Type</th><th>Schedule</th><th>Class</th></tr>
					<xsl:apply-templates select='property[@type="rule"]' />
				</table>
			</div>
			<br />
		</xsl:if>
		<xsl:if test='property[@type="dockingpoint"]'>
			<h4 class="section">Docking Points</h4>
			<div class="level">
				<table>
					<tr><th>Name</th><th>Required</th><th>Docking Object</th></tr>
					<xsl:apply-templates select='property[@type="dockingpoint"]' />
				</table>
			</div>
			<br />
		</xsl:if>
		</div>
	</xsl:template>

	<xsl:template match='property[@type="setting"]'>
		<tr>
			<td><xsl:value-of select="name" /></td>
			<td>
				<input>
					<xsl:attribute name="type">checkbox</xsl:attribute>
					<xsl:if test='historic="true"'>
						<xsl:attribute name="checked" />
					</xsl:if>
				</input>
			</td>
			<td>
				<input>
					<xsl:attribute name="type">checkbox</xsl:attribute>
					<xsl:if test='required="true"'>
						<xsl:attribute name="checked" />
					</xsl:if>
				</input>
			</td>
			<td><xsl:value-of select="valuetype" /></td>
		</tr>
	</xsl:template>

	<xsl:template match='property[@type="childlink"]'>
		<tr>
			<td><xsl:value-of select="name" /></td>
			<td>
				<input>
					<xsl:attribute name="type">checkbox</xsl:attribute>
					<xsl:if test='required="true"'>
						<xsl:attribute name="checked" />
					</xsl:if>
				</input>
			</td>
			<td>
				<xsl:for-each select="reftype">
					<a><xsl:attribute name="href"><xsl:text/>#<xsl:value-of select="." /></xsl:attribute><xsl:value-of select="." /></a>
				</xsl:for-each>
			</td>
		</tr>
	</xsl:template>

	<xsl:template match='property[@type="rule"]'>
		<tr>
			<td><xsl:value-of select="name" /></td>
			<td>
				<input>
					<xsl:attribute name="type">checkbox</xsl:attribute>
					<xsl:if test='historic="true"'>
						<xsl:attribute name="checked" />
					</xsl:if>
				</input>
			</td>
			<td><xsl:value-of select="valuetype" /></td>
			<td><xsl:value-of select="schedule" /></td>
			<td><xsl:value-of select="ruleclass" /></td>
		</tr>
	</xsl:template>

	<xsl:template match='property[@type="dockingpoint"]'>
		<tr>
			<td><xsl:value-of select="name" /></td>
			<td>
				<input>
					<xsl:attribute name="type">checkbox</xsl:attribute>
					<xsl:if test='required="true"'>
						<xsl:attribute name="checked" />
					</xsl:if>
				</input>
			</td>
			<td>
				<xsl:for-each select="reftype">
					<a><xsl:attribute name="href"><xsl:text/>#<xsl:value-of select="." /></xsl:attribute><xsl:value-of select="." /></a>
				</xsl:for-each>
			</td>
		</tr>
	</xsl:template>

</xsl:stylesheet>

