<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
	<xsl:output method="xml" indent="yes" />

	<xsl:template match="/">
		<boms>
			<xsl:apply-templates select="configurations/*" />
		</boms>
	</xsl:template>

	<xsl:template match="configuration">
		<bom>
			<xsl:attribute name="name">
				<xsl:value-of select="@name" />
			</xsl:attribute>
			<item>
				<xsl:attribute name="description">
					<xsl:text>placeholder</xsl:text>
				</xsl:attribute>
				<xsl:attribute name="name">
					<xsl:value-of select="@name" />
				</xsl:attribute>
				<xsl:attribute name="count">
					<xsl:text>1</xsl:text>
				</xsl:attribute>
				<xsl:attribute name="itemcost">
					<xsl:text>0.0</xsl:text>
				</xsl:attribute>
			</item>
		</bom>
	</xsl:template>
</xsl:stylesheet>
