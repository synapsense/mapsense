
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;;

public class SoapVoltAmp3Phase implements RuleAction {
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(SoapVoltAmp3Phase.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.info("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			TO<?> soapVoltsAB = env.getPropertyValue(nativeTO, "voltsAB", TO.class);
			TO<?> soapVoltsBC = env.getPropertyValue(nativeTO, "voltsBC", TO.class);
			TO<?> soapVoltsCA = env.getPropertyValue(nativeTO, "voltsCA", TO.class);
			TO<?> soapAmpsA = env.getPropertyValue(nativeTO, "ampsA", TO.class);
			TO<?> soapAmpsB = env.getPropertyValue(nativeTO, "ampsB", TO.class);
			TO<?> soapAmpsC = env.getPropertyValue(nativeTO, "ampsC", TO.class);

			String sVoltsAB = env.getPropertyValue(soapVoltsAB, "lastValue", String.class);
			String sVoltsBC = env.getPropertyValue(soapVoltsBC, "lastValue", String.class);
			String sVoltsCA = env.getPropertyValue(soapVoltsCA, "lastValue", String.class);
			String sAmpsA = env.getPropertyValue(soapAmpsA, "lastValue", String.class);
			String sAmpsB = env.getPropertyValue(soapAmpsB, "lastValue", String.class);
			String sAmpsC = env.getPropertyValue(soapAmpsC, "lastValue", String.class);

			if(sVoltsAB == null || sVoltsBC == null || sVoltsCA == null || sAmpsA == null || sAmpsB == null || sAmpsC == null) {
				logger.warn("Null input value, can't calculate kW");
				return null;
			}

			double powerFactor = env.getPropertyValue(nativeTO, "powerFactor", Double.class);

			double voltsAB = Double.parseDouble(sVoltsAB);
			double voltsBC = Double.parseDouble(sVoltsBC);
			double voltsCA = Double.parseDouble(sVoltsCA);

			double ampsA = Double.parseDouble(sAmpsA);
			double ampsB = Double.parseDouble(sAmpsB);
			double ampsC = Double.parseDouble(sAmpsC);

			logger.info(calculated.getName() + " voltsAB: " + voltsAB + " voltsBC: " + voltsBC + " voltsCA: " + voltsCA);
			logger.info(calculated.getName() + " amps A: " + ampsA + " ampsB: " + ampsB + " ampsC: " + ampsC);

			double avgPPVolts = (voltsAB + voltsBC + voltsCA) / 3.0
			double avgPNVolts = avgPPVolts / 1.732;

			double kvaA = (ampsA * avgPNVolts) / 1000.0;
			double kvaB = (ampsB * avgPNVolts) / 1000.0;
			double kvaC = (ampsC * avgPNVolts) / 1000.0;

			double totalkva = kvaA + kvaB + kvaC;

			double kw = totalkva * powerFactor;

			logger.info(calculated.getName() + " returning " + kw);
			return (double) kw;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}
