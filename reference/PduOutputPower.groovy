
package com.synapsense.deploymentlab.aetna;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;;

public class PduOutputPower implements RuleAction {
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(PduOutputPower.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.info("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			Collection< TO<?> > outputCircuits = env.getPropertyValue(nativeTO, "outputCircuits", Collection.class);

			double totalOutputPower = 0.0;

			for(TO<?> circuit: outputCircuits) {
				String sOutputPower = env.getPropertyValue(circuit, "lastValue", String.class);
				if(sOutputPower == null || sOutputPower.equals("")) {
					logger.warn("NULL/empty lastValue on PDU circuit: " + circuit);
					continue;
				}
				try {
					totalOutputPower += Double.parseDouble(sOutputPower);
				} catch(Exception e) {
					logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "' error converting to double: ", e);
				}
			}

			logger.info(calculated.getName() + " returning " + totalOutputPower);
			return totalOutputPower;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}
