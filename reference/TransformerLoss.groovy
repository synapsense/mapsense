
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;;

public class TransformerLoss implements RuleAction {
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(TransformerLoss.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.info("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			TO<?> inputTO = env.getPropertyValue(nativeTO, "input", TO.class);
			TO<?> primaryOutputTO = env.getPropertyValue(nativeTO, "primaryOutput", TO.class);
			TO<?> secondaryOutputTO = env.getPropertyValue(nativeTO, "secondaryOutput", TO.class);

			String sTotalInput = env.getPropertyValue(inputTO, "lastValue", String.class);
			String sPrimaryOutput = env.getPropertyValue(primaryOutputTO, "lastValue", String.class);
			String sSecondaryOutput = env.getPropertyValue(secondaryOutputTO, "lastValue", String.class);

			if(sTotalInput == null || sPrimaryOutput == null || sSecondaryOutput == null) {
				logger.warn("One of my dependencies is NULL!")
				return (double)0.0;
			}

			logger.info(calculated.getName() + " inputs: in: " + sTotalInput + " pOut: " + sPrimaryOutput + " sOut: " + sSecondaryOutput);
			double totalInput = Double.parseDouble(sTotalInput);
			double primaryOutput = Double.parseDouble(sPrimaryOutput);
			double secondaryOutput = Double.parseDouble(sSecondaryOutput);

			double totalOutput = primaryOutput + secondaryOutput;
			double totalLoss = totalInput - totalOutput;
			if(totalLoss < 0.0) {
				logger.warn("Total loss (" + totalLoss + ") less than 0!  Setting to 0.0");
				return (double)0.0;
			}
			double myLoss = (primaryOutput / totalOutput) * totalLoss;

			logger.info(calculated.getName() + " returning " + myLoss);
			return (double) myLoss;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}
