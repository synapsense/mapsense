
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;;

public class RunStatusCalc implements RuleAction {
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(RunStatusCalc.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.info("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			TO<?> soapMethod = env.getPropertyValue(nativeTO, "runStatus", TO.class);

			String running = env.getPropertyValue(soapMethod, "lastValue", String.class);
			if(running.equalsIgnoreCase("on")) {
				double value = env.getPropertyValue(nativeTO, "multiplier", Double.class);
				logger.info(calculated.getName() + " returning " + value);
				return (double)value;
			}
			logger.info(calculated.getName() + " returning 0.0");
			return (double) 0.0;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}
