
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;;

public class ChilledWaterTons implements RuleAction {
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(ChilledWaterTons.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.info("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			TO<?> soapFlow = env.getPropertyValue(nativeTO, "flow", TO.class);
			TO<?> soapSupplyT = env.getPropertyValue(nativeTO, "supplyTemp", TO.class);
			TO<?> soapReturnT = env.getPropertyValue(nativeTO, "returnTemp", TO.class);

			String sFlow = env.getPropertyValue(soapFlow, "lastValue", String.class);
			String sSupplyT = env.getPropertyValue(soapSupplyT, "lastValue", String.class);
			String sReturnT = env.getPropertyValue(soapReturnT, "lastValue", String.class);

			if(sFlow == null || sSupplyT == null || sReturnT == null) {
				logger.warn("Null input, can't calculate tons!");
				return null;
			}

			double flow = Double.parseDouble(sFlow); // assume GPM
			double supplyT = Double.parseDouble(sSupplyT);  // assume F
			double returnT = Double.parseDouble(sReturnT);  // assume F

			logger.info(calculated.getName() + " flow: " + flow + "gpm supplyT: " + supplyT + "f returnT: " + returnT + "f");
			double deltaT = (returnT - supplyT);

			// simpler calc: 8.34 BTU/gal/F
			/*
			double btu = 8.34 * flow * deltaT;
			double tons = btu/12000.0;
			*/
			double tons = (flow * deltaT) / 24; // why?
			logger.info(calculated.getName() + " returning " + tons);
			return (double) tons;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}
