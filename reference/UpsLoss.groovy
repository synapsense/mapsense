
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;;

public class UpsLoss implements RuleAction {
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(UpsLoss.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.info("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			TO<?> inputTO = env.getPropertyValue(nativeTO, "input", TO.class);
			TO<?> outputTO = env.getPropertyValue(nativeTO, "output", TO.class);

			String sInputPower = env.getPropertyValue(inputTO, "lastValue", String.class);
			String sOutputPower = env.getPropertyValue(outputTO, "lastValue", String.class);

			double inputPower = Double.parseDouble(sInputPower);
			double outputPower = Double.parseDouble(sOutputPower);

			if(inputPower == null || outputPower == null) {
				logger.warn("One of my dependencies is NULL!")
				return (double)0.0;
			}

			double loss = inputPower - outputPower;
			if(loss < 0.0) {
				logger.warn("Loss less than 0.0!  Setting to 0.0");
				return (double)0.0;
			}

			logger.info(calculated.getName() + " returning " + loss);
			return (double) loss;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}
