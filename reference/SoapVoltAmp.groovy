
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;;

public class SoapVoltAmp implements RuleAction {
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(SoapVoltAmp.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.info("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			TO<?> soapVolts = env.getPropertyValue(nativeTO, "volts", TO.class);
			TO<?> soapAmps = env.getPropertyValue(nativeTO, "amps", TO.class);

			String sVolts = env.getPropertyValue(soapVolts, "lastValue", String.class);
			String sAmps = env.getPropertyValue(soapAmps, "lastValue", String.class);

			if(sVolts == null || sAmps == null) {
				if(sVolts == null)
					logger.warn("Null volts, can't calculate kW");

				if(sAmps == null)
					logger.warn("Null amps, can't calculate kW");

				return null;
			}

			double powerFactor = env.getPropertyValue(nativeTO, "powerFactor", Double.class);

			double volts = Double.parseDouble(sVolts);
			double amps = Double.parseDouble(sAmps);

			logger.info(calculated.getName() + " volts: " + volts + " amps: " + amps + " power factor: " + powerFactor);

			double kw = (volts * amps) * powerFactor;

			logger.info(calculated.getName() + " returning " + kw);
			return (double) kw;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}
