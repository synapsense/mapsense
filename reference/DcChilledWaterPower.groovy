
package com.synapsense.deploymentlab;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;;

public class DcChilledWaterPower implements RuleAction {
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(DcChilledWaterPower.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.info("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + " running");

		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			TO<?> soapMethod = env.getPropertyValue(nativeTO, "plantTonnage", TO.class);
			String sPlantTonnage = env.getPropertyValue(soapMethod, "lastValue", String.class);

			if(sPlantTonnage == null) {
				logger.warn("plantTonnage for DcChilledWaterPower is NULL");
				return null;
			}
			double plantTonnage = Double.parseDouble(sPlantTonnage);

			double dcCoolingTons = env.getPropertyValue(nativeTO, "dcCoolingTons", Double.class);
			if(dcCoolingTons == 0.0) {
				Collection< TO<?> > tonnageInputs = env.getPropertyValue(nativeTO, "dcTonnageInputs", Collection.class);
				for(TO<?> input: tonnageInputs) {
					double inputValue = env.getPropertyValue(input, "lastValue", Double.class);
					if(inputValue != null)
						dcCoolingTons += inputValue;
				}
			}

			double totalPlantPower = 0.0;

			Collection< TO<?> > plantPowerInputs = env.getPropertyValue(nativeTO, "totalPlantPower", Collection.class);
			for(TO<?> input: plantPowerInputs) {
				if(input.getTypeName().equalsIgnoreCase("SOAPMETHOD")) {
					String sInput = env.getPropertyValue(input, "lastValue", String.class);
					if(sInput != null && sInput != "") {
						Double inputValue = Double.parseDouble(sInput);
						totalPlantPower += inputValue;
					}
				} else {
					Double inputValue = env.getPropertyValue(input, "lastValue", Double.class);
					if(inputValue != null)
						totalPlantPower += inputValue;
				}
			}

			logger.info(calculated.getName() + " total plant tonnage: " + plantTonnage + " DC Cooling Tons: " + dcCoolingTons + " total plant power: " + totalPlantPower);

			double dcCoolingPower = (totalPlantPower / plantTonnage) * dcCoolingTons;
			if(dcCoolingPower == Double.NaN || dcCoolingPower == Double.POSITIVE_INFINITY || dcCoolingPower == Double.NEGATIVE_INFINITY) {
				logger.warn(calculated.getName() + " value calculated to '" + dcCoolingPower + "' returning 0.0");
				return (double)0.0;
			} else {
				logger.info(calculated.getName() + " returning " + dcCoolingPower);
				return (double)dcCoolingPower;
			}
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}
