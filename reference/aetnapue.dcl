<?xml version="1.0" encoding="UTF-8"?>
<componentlib name="test objects" version="1.0" oversion="1.2">
	<palette>
		<group type="Aetna">
			<configuration name="aetna-pue" />
			<configuration name="aetna-pdu" />
			<configuration name="soap-power" />
			<configuration name="soap-runstatus-power" />
			<configuration name="soap-va-power" />
			<configuration name="soap-va-power-3phase" />
			<configuration name="soap-transformer-loss" />
			<configuration name="soap-ups-loss" />
			<configuration name="constant-power" />
			<configuration name="soap-chwpower" />
			<configuration name="soap-chw-tons" />
		</group>
	</palette>

	<component type="aetna-pue" classes="placeable">
		<display>
			<description>PUE Virtual object</description>
			<name>PUE</name>
			<shape>star_gray</shape>
		</display>

		<object type="pue" as="pue">
			<dlid>DLID:-1</dlid>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">PUE Virtual Object</property>
		<property name="x" display="x" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="y" display="y" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">PUE Virtual Object v0.1</property>

		<property name="itDesign" display="IT Power Design Limit" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="baselinePue" display="Baseline PUE" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="coolingDesign" display="Cooling Power Design Limit" type="java.lang.Double" editable="true" displayed="true">0.0</property>
		<property name="co2AbatedIndex" display="CO2 Abated Index" type="java.lang.Double" editable="true" displayed="true">0.0</property>

		<consumer id="itPower" datatype="power" name="IT Power Contributions" collection="true" property='$pue/properties[name="itPowerInputs"]' required="true" />
		<consumer id="powerLoss" datatype="power" name="Power Losses" collection="true" property='$pue/properties[name="powerLossInputs"]' required="true" />
		<consumer id="lightingPower" datatype="power" name="Lighting Power Contributions" collection="true" property='$pue/properties[name="lightingPowerInputs"]' required="true" />
		<consumer id="coolingPower" datatype="power" name="Cooling Power Contributions" collection="true" property='$pue/properties[name="coolingPowerInputs"]' required="true" />

		<varbinding vars="itDesign">
			<property>$pue/properties[name="itPowerDesign"]</property>
			<value>itDesign</value>
		</varbinding>
		<varbinding vars="baselinePue">
			<property>$pue/properties[name="baselinePue"]</property>
			<value>baselinePue</value>
		</varbinding>
		<varbinding vars="coolingDesign">
			<property>$pue/properties[name="coolingPowerDesign"]</property>
			<value>coolingDesign</value>
		</varbinding>
		<varbinding vars="co2AbatedIndex">
			<property>$pue/properties[name="co2AbatedIndex"]</property>
			<value>co2AbatedIndex</value>
		</varbinding>
	</component>

	<component type="aetna-pdu" classes="placeable">
		<display>
			<description>A PDU for the Aetna POC</description>
			<name>PDU</name>
			<shape>star_gray</shape>
		</display>

		<object type="aetna_pdu" as="pdu">
			<dlid>DLID:-1</dlid>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">PDU</property>
		<property name="x" display="x" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="y" display="y" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Aetna PDU</property>

		<consumer id="outputCircuits" datatype="power" name="PDU Output Circuits" collection="true" property='$pdu/properties[name="outputCircuits"]' required="true" />

		<producer id="outputPower" datatype="power" name="Total PDU Output Power" object='$pdu' />

		<varbinding vars="name">
			<property>$pdu/properties[name="name"]</property>
			<value>name</value>
		</varbinding>
	</component>

	<component type="soap-power" classes="placeable">
		<display>
			<description>kW via SOAP</description>
			<name>kW via SOAP</name>
			<shape>star_gray</shape>
		</display>

		<object type="soapmethod" as="soapmethod">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">SOAP Power</property>
		<property name="x" display="x" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="y" display="y" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">kW via SOAP</property>

		<property name="pullperiod" display="Pull Period (seconds)" type="java.lang.Integer" editable="true" displayed="true">900</property>
		<property name="username" display="User Name" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="passwd" display="Password" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="expression" display="Point Name" type="java.lang.String" editable="true" displayed="true"></property>

		<producer id="power" datatype="power" name="kW" object='$soapmethod' />

		<varbinding vars="pullperiod">
			<property>$soapmethod/properties[name="pullPeriod"]</property>
			<value>pullperiod</value>
		</varbinding>
		<varbinding vars="username,passwd,expression">
			<property>$soapmethod/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + expression</value>
		</varbinding>
	</component>

	<component type="soap-runstatus-power" classes="placeable">
		<display>
			<description>Run Status via SOAP</description>
			<name>Run Status via SOAP</name>
			<shape>star_gray</shape>
		</display>

		<object type="soapmethod" as="soapmethod">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>

		<object type="aetna_soaprunstatus" as="soaprunstatus">
			<dlid>DLID:-1</dlid>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">SOAP Run Status</property>
		<property name="x" display="x" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="y" display="y" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Run Status via SOAP</property>

		<property name="pullperiod" display="Pull Period (seconds)" type="java.lang.Integer" editable="true" displayed="true">900</property>
		<property name="username" display="User Name" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="passwd" display="Password" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="expression" display="Run Status Point" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="kw" display="Characteristic kW" type="java.lang.Double" editable="true" displayed="true"></property>

		<docking from='$soaprunstatus/properties[name="runStatus"]' to='$soapmethod' />

		<producer id="power" datatype="power" name="kW" object='$soaprunstatus' />

		<varbinding vars="pullperiod">
			<property>$soapmethod/properties[name="pullPeriod"]</property>
			<value>pullperiod</value>
		</varbinding>
		<varbinding vars="username,passwd,expression">
			<property>$soapmethod/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + expression</value>
		</varbinding>
		<varbinding vars="kw">
			<property>$soaprunstatus/properties[name="multiplier"]</property>
			<value>kw</value>
		</varbinding>
	</component>

	<component type="soap-va-power-3phase" classes="placeable">
		<display>
			<description>3-Phase Volts/Amps via SOAP</description>
			<name>3-Phase Volts/Amps via SOAP</name>
			<shape>star_gray</shape>
		</display>

		<object type="soapmethod" as="voltsAB_input">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>
		<object type="soapmethod" as="voltsBC_input">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>
		<object type="soapmethod" as="voltsCA_input">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>

		<object type="soapmethod" as="ampsA_input">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>
		<object type="soapmethod" as="ampsB_input">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>
		<object type="soapmethod" as="ampsC_input">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>

		<object type="aetna_soapva3phase" as="soapva">
			<dlid>DLID:-1</dlid>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">3-Phase SOAP Volts/Amps</property>
		<property name="x" display="x" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="y" display="y" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">3-Phase SOAP Volts/Amps</property>

		<property name="pullperiod" display="Pull Period (seconds)" type="java.lang.Integer" editable="true" displayed="true">900</property>
		<property name="username" display="User Name" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="passwd" display="Password" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="voltAB_point" display="AB Volts input" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="voltBC_point" display="BC Volts input" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="voltCA_point" display="CA Volts input" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="ampA_point" display="A Amps input" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="ampB_point" display="B Amps input" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="ampC_point" display="C Amps input" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="powerfactor" display="Power Factor" type="java.lang.Double" editable="true" displayed="true"></property>

		<docking from='$soapva/properties[name="voltsAB"]' to='$voltsAB_input' />
		<docking from='$soapva/properties[name="voltsBC"]' to='$voltsBC_input' />
		<docking from='$soapva/properties[name="voltsCA"]' to='$voltsCA_input' />
		<docking from='$soapva/properties[name="ampsA"]' to='$ampsA_input' />
		<docking from='$soapva/properties[name="ampsB"]' to='$ampsB_input' />
		<docking from='$soapva/properties[name="ampsC"]' to='$ampsC_input' />

		<producer id="power" datatype="power" name="kW" object='$soapva' />

		<varbinding vars="pullperiod">
			<property>$voltsAB_input/properties[name="pullPeriod"]</property>
			<property>$voltsBC_input/properties[name="pullPeriod"]</property>
			<property>$voltsCA_input/properties[name="pullPeriod"]</property>
			<property>$ampsA_input/properties[name="pullPeriod"]</property>
			<property>$ampsB_input/properties[name="pullPeriod"]</property>
			<property>$ampsC_input/properties[name="pullPeriod"]</property>
			<value>pullperiod</value>
		</varbinding>
		<varbinding vars="username,passwd,voltAB_point">
			<property>$voltsAB_input/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + voltAB_point</value>
		</varbinding>
		<varbinding vars="username,passwd,voltBC_point">
			<property>$voltsBC_input/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + voltBC_point</value>
		</varbinding>
		<varbinding vars="username,passwd,voltCA_point">
			<property>$voltsCA_input/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + voltCA_point</value>
		</varbinding>
		<varbinding vars="username,passwd,ampA_point">
			<property>$ampsA_input/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + ampA_point</value>
		</varbinding>
		<varbinding vars="username,passwd,ampB_point">
			<property>$ampsB_input/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + ampB_point</value>
		</varbinding>
		<varbinding vars="username,passwd,ampC_point">
			<property>$ampsC_input/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + ampC_point</value>
		</varbinding>
		<varbinding vars="powerfactor">
			<property>$soapva/properties[name="powerFactor"]</property>
			<value>powerfactor</value>
		</varbinding>
	</component>

	<component type="soap-va-power" classes="placeable">
		<display>
			<description>Volts/Amps via SOAP</description>
			<name>Volts/Amps via SOAP</name>
			<shape>star_gray</shape>
		</display>

		<object type="soapmethod" as="volts_input">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>

		<object type="soapmethod" as="amps_input">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>

		<object type="aetna_soapva" as="soapva">
			<dlid>DLID:-1</dlid>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">SOAP Volts/Amps</property>
		<property name="x" display="x" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="y" display="y" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">SOAP Volts/Amps</property>

		<property name="pullperiod" display="Pull Period (seconds)" type="java.lang.Integer" editable="true" displayed="true">900</property>
		<property name="username" display="User Name" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="passwd" display="Password" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="volt_point" display="Volts input" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="amp_point" display="Amps input" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="powerfactor" display="Power Factor" type="java.lang.Double" editable="true" displayed="true"></property>

		<docking from='$soapva/properties[name="volts"]' to='$volts_input' />
		<docking from='$soapva/properties[name="amps"]' to='$amps_input' />

		<producer id="power" datatype="power" name="kW" object='$soapva' />

		<varbinding vars="pullperiod">
			<property>$volts_input/properties[name="pullPeriod"]</property>
			<property>$amps_input/properties[name="pullPeriod"]</property>
			<value>pullperiod</value>
		</varbinding>
		<varbinding vars="username,passwd,volt_point">
			<property>$volts_input/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + volt_point</value>
		</varbinding>
		<varbinding vars="username,passwd,amp_point">
			<property>$amps_input/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + amp_point</value>
		</varbinding>
		<varbinding vars="powerfactor">
			<property>$soapva/properties[name="powerFactor"]</property>
			<value>powerfactor</value>
		</varbinding>
	</component>

	<component type="soap-transformer-loss" classes="placeable">
		<display>
			<description>SOAP Input Transformer Loss</description>
			<name>SOAP Input Transformer Loss</name>
			<shape>star_gray</shape>
		</display>

		<object type="soapmethod" as="primaryOutput">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>

		<object type="soapmethod" as="secondaryOutput">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>

		<object type="soapmethod" as="input">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>

		<object type="aetna_transformerloss" as="xformerloss">
			<dlid>DLID:-1</dlid>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">SOAP Input Transformer Loss</property>
		<property name="x" display="x" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="y" display="y" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">SOAP Input Transformer Loss</property>

		<property name="pullperiod" display="Pull Period (seconds)" type="java.lang.Integer" editable="true" displayed="true">900</property>
		<property name="username" display="User Name" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="passwd" display="Password" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="poutput_point" display="Primary Output kW" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="soutput_point" display="Secondary Output kW" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="input_point" display="Input kW" type="java.lang.String" editable="true" displayed="true"></property>

		<docking from='$xformerloss/properties[name="primaryOutput"]' to='$primaryOutput' />
		<docking from='$xformerloss/properties[name="secondaryOutput"]' to='$secondaryOutput' />
		<docking from='$xformerloss/properties[name="input"]' to='$input' />

		<producer id="power" datatype="power" name="kW" object='$xformerloss' />

		<varbinding vars="pullperiod">
			<property>$primaryOutput/properties[name="pullPeriod"]</property>
			<property>$secondaryOutput/properties[name="pullPeriod"]</property>
			<property>$input/properties[name="pullPeriod"]</property>
			<value>pullperiod</value>
		</varbinding>
		<varbinding vars="username,passwd,poutput_point">
			<property>$primaryOutput/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + poutput_point</value>
		</varbinding>
		<varbinding vars="username,passwd,soutput_point">
			<property>$secondaryOutput/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + soutput_point</value>
		</varbinding>
		<varbinding vars="username,passwd,input_point">
			<property>$input/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + input_point</value>
		</varbinding>
	</component>

	<component type="soap-ups-loss" classes="placeable">
		<display>
			<description>SOAP Input UPS Loss</description>
			<name>SOAP Input UPS Loss</name>
			<shape>star_gray</shape>
		</display>

		<object type="soapmethod" as="output">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>

		<object type="soapmethod" as="input">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>

		<object type="aetna_upsloss" as="upsloss">
			<dlid>DLID:-1</dlid>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">SOAP Input UPS Loss</property>
		<property name="x" display="x" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="y" display="y" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">SOAP Input UPS Loss</property>

		<property name="pullperiod" display="Pull Period (seconds)" type="java.lang.Integer" editable="true" displayed="true">900</property>
		<property name="username" display="User Name" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="passwd" display="Password" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="output_point" display="Output kW" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="input_point" display="Input kW" type="java.lang.String" editable="true" displayed="true"></property>

		<docking from='$upsloss/properties[name="output"]' to='$output' />
		<docking from='$upsloss/properties[name="input"]' to='$input' />

		<producer id="power" datatype="power" name="kW" object='$upsloss' />

		<varbinding vars="pullperiod">
			<property>$output/properties[name="pullPeriod"]</property>
			<property>$input/properties[name="pullPeriod"]</property>
			<value>pullperiod</value>
		</varbinding>
		<varbinding vars="username,passwd,output_point">
			<property>$output/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + output_point</value>
		</varbinding>
		<varbinding vars="username,passwd,input_point">
			<property>$input/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + input_point</value>
		</varbinding>
	</component>

	<component type="constant-power" classes="placeable">
		<display>
			<description>Constant Power Value</description>
			<name>Constant Power Value</name>
			<shape>star_gray</shape>
		</display>

		<object type="aetna_constantvalue" as="constant">
			<dlid>DLID:-1</dlid>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Constant Power Value</property>
		<property name="x" display="x" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="y" display="y" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Constant Power Value</property>

		<property name="value" display="kW" type="java.lang.Double" editable="true" displayed="true">0.0</property>

		<producer id="power" datatype="power" name="kW" object='$constant' />

		<varbinding vars="value">
			<property>$constant/properties[name="lastValue"]</property>
			<value>value</value>
		</varbinding>
	</component>

	<component type="soap-chwpower" classes="placeable">
		<display>
			<description>DC CHWP Power</description>
			<name>DC CHWP Power</name>
			<shape>star_gray</shape>
		</display>

		<object type="soapmethod" as="plant_tons">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>

		<object type="aetna_dcchwpower" as="dcchwpower">
			<dlid>DLID:-1</dlid>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">DC CHWP Power</property>
		<property name="x" display="x" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="y" display="y" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">DC CHWP Power</property>

		<property name="pullperiod" display="Pull Period (seconds)" type="java.lang.Integer" editable="true" displayed="true">900</property>
		<property name="username" display="User Name" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="passwd" display="Password" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="plant_tons_point" display="CHWP Plant Tons Input" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="dc_tons" display="DC Tons" type="java.lang.Double" editable="true" displayed="true"></property>

		<docking from='$dcchwpower/properties[name="plantTonnage"]' to='$plant_tons' />

		<consumer id="power" datatype="power" name="CHWP Power" collection="true" property='$dcchwpower/properties[name="totalPlantPower"]' required="true" />
		<consumer id="dctons" datatype="tons" name="DC Chilled Water Tonnage" collection="true" property='$dcchwpower/properties[name="dcTonnageInputs"]' required="true" />
		<producer id="power" datatype="power" name="DC CHWP Power" object='$dcchwpower' />

		<varbinding vars="pullperiod">
			<property>$plant_tons/properties[name="pullPeriod"]</property>
			<value>pullperiod</value>
		</varbinding>
		<varbinding vars="username,passwd,plant_tons_point">
			<property>$plant_tons/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + plant_tons_point</value>
		</varbinding>
		<varbinding vars="dc_tons">
			<property>$dcchwpower/properties[name="dcCoolingTons"]</property>
			<value>dc_tons</value>
		</varbinding>
	</component>

	<component type="soap-chw-tons" classes="placeable">
		<display>
			<description>Chilled Water Tonnage via SOAP</description>
			<name>Chilled Water Tonnage via SOAP</name>
			<shape>star_gray</shape>
		</display>

		<object type="soapmethod" as="flow">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>
		<object type="soapmethod" as="supplyTemp">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>
		<object type="soapmethod" as="returnTemp">
			<dlid>DLID:-1</dlid>
			<property name="methodName"><value>getDisplayValue</value></property>
			<property name="parameterNames"><value>user,passwd,expression</value></property>
		</object>

		<object type="aetna_chwtons" as="chwtons">
			<dlid>DLID:-1</dlid>
		</object>

		<property name="name" display="Name" type="java.lang.String" editable="true" displayed="true">Chilled Water Tonnage</property>
		<property name="x" display="x" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="y" display="y" type="java.lang.Integer" editable="true" displayed="true">0</property>
		<property name="configuration" display="Configuration" type="java.lang.String" editable="false" displayed="true">Chilled Water Tonnage</property>

		<property name="pullperiod" display="Pull Period (seconds)" type="java.lang.Integer" editable="true" displayed="true">900</property>
		<property name="username" display="User Name" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="passwd" display="Password" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="flow" display="Flow input" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="supplyT" display="Supply T input" type="java.lang.String" editable="true" displayed="true"></property>
		<property name="returnT" display="Return T input" type="java.lang.String" editable="true" displayed="true"></property>

		<docking from='$chwtons/properties[name="flow"]' to='$flow' />
		<docking from='$chwtons/properties[name="supplyTemp"]' to='$supplyTemp' />
		<docking from='$chwtons/properties[name="returnTemp"]' to='$returnTemp' />

		<producer id="tons" datatype="tons" name="Chilled Water Tons" object='$chwtons' />

		<varbinding vars="pullperiod">
			<property>$flow/properties[name="pullPeriod"]</property>
			<property>$supplyTemp/properties[name="pullPeriod"]</property>
			<property>$returnTemp/properties[name="pullPeriod"]</property>
			<value>pullperiod</value>
		</varbinding>
		<varbinding vars="username,passwd,flow">
			<property>$flow/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + flow</value>
		</varbinding>
		<varbinding vars="username,passwd,supplyT">
			<property>$supplyTemp/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + supplyT</value>
		</varbinding>
		<varbinding vars="username,passwd,returnT">
			<property>$returnTemp/properties[name="parameterValues"]</property>
			<value>"" + username + "," + passwd + "," + returnT</value>
		</varbinding>
	</component>

</componentlib>

