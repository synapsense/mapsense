
function arrMax(arr){
	var big = 0;
	var len = arr.length;
	for(var i = 0; i < len; i++){
		var n = parseInt(arr[i]);
		if(n > big){
			big = n;
		}
	}
	return Number(big);
}


function arrAvg(arr){
	var total = 0.0;
	var len = arr.length;
	for(var i = 0; i < len; i++){
		total += parseInt(arr[i]);
	}
	var result = total / len;
	return result;
}


function coolerTemp(arr,adjust){
	return arrMax(arr) + Number(adjust);
}


importPackage(java.util); 
function roll(count, sides) {
	var gygax = new Random();
	var total = 0;
	for(var i = 0; i < count; i++){
		total += parseInt( gygax.nextInt(sides+1) );
	}

	return Number(total);
}

importPackage(com.synapsense.plugin.messages);
function actuatorPosition(data, moving, status, timestamp) {
       return new WSNActuatorPositionBuilder(data, moving, status, timestamp).getMessage();
}
